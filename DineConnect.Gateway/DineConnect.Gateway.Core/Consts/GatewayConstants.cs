﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DineConnect.Gateway.Core.Consts
{
    public class GatewayConstants
    {
        public static string DeliveryHeroKey = "DeliveryHeroKey";
        public static string UrbanPiperApiKey = "UrbanPiperApiKey";
        public static string UrbanPiperApiUserName = "UrbanPiperApiUserName";
        public static string UrbanPiperUrl = "UrbanPiperUrl";
        public static string DineConnectUrl = "DineConnectUrl";
        public static string DineConnectLocationId = "DineConnectLocationId";


    }
}
