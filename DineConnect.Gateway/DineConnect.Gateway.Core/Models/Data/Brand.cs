﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DineConnect.Gateway.Core.Models
{
    [Table("Brands")]
    public class Brand : TrackedEntity
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string AddOn { get; set; }
    }

}
