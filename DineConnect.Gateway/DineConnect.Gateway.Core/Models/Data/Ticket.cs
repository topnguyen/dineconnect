﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DineConnect.Gateway.Core.Extensions;

namespace DineConnect.Gateway.Core.Models
{
    [Table("ExternalDeliveryTickets")]
    public class ExternalDeliveryTicket : TrackedEntity
    {
        public ExternalDeliveryTicket()
        {
            CreateDate  = DateTime.Now;
            UpdateDate  = DateTime.Now;

        }
        private IList<DeliveryContentValue> _deliveryContentValues;

        [Index("IX_ExternalDeliveryTicket", 1, IsUnique = true)]
        public virtual int LocationId { get; set; }

        [Index("IX_ExternalDeliveryTicket", 2, IsUnique = true)]
        [StringLength(40)]
        public virtual string TicketNumber { get; set; }

        [StringLength(2)]
        public virtual string Status { get; set; }
        public virtual bool Claimed { get; set; }
        public virtual string DeliveryContents { get; set; }

        internal IList<DeliveryContentValue> DeliveryContentValues
        {
            get
            {
                if (_deliveryContentValues == null)
                {
                    _deliveryContentValues = JsonHelper.Deserialize<List<DeliveryContentValue>>(DeliveryContents);
                }

                return _deliveryContentValues;
            }
        }

        public void AddDeliveryContentValue(DeliveryContentValue promotionDetailValue)
        {
            DeliveryContentValues.Add(promotionDetailValue);
            DeliveryContents = JsonHelper.Serialize(DeliveryContentValues);
        }

        public List<string> GetDeliveryContent()
        {
            return DeliveryContentValues.Select(c => c.Contents).ToList();
        }
       
    }

    public class DeliveryContentValue
    {
        public string TypeOfContent { get; set; }
        public string Contents { get; set; }
    }
}
