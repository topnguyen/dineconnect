﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DineConnect.Gateway.EntityFramework
{
    public class GatewayRole : IdentityRole
    {
        public GatewayRole() : base() { }
        public GatewayRole(string name) : base(name) { }
    }

    public class GatewayUser : IdentityUser
    {
        public DateTime RegistrationDate { get; set; }
    }
}