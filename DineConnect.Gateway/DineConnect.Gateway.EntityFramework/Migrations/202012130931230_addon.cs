﻿using System.Data.Entity.Migrations;

namespace DineConnect.Gateway.EntityFramework.Migrations
{
    public partial class addon : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Brands", "AddOn", c => c.String());
            AddColumn("dbo.Locations", "AddOn", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Locations", "AddOn");
            DropColumn("dbo.Brands", "AddOn");
        }
    }
}
