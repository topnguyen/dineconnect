﻿namespace DineConnect.XeroTenantGateway.EntityFramework.Migrations
{
    using DineConnect.Gateway.EntityFramework;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DineConnectGatewayDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(DineConnectGatewayDbContext context)
        {
            var userStore = new UserStore<GatewayUser>(context);
            var userManager = new UserManager<GatewayUser>(userStore);
            var user = new GatewayUser { UserName = "admin", RegistrationDate = DateTime.Now };
            userManager.Create(user, "123qwe");
        }
    }
}
