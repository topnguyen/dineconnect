﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DineConnect.Gateway.EntityFramework.Repository
{
    public interface IRepository<T>
    {
        IUnitOfWork UnitOfWork { get; }
        void Add(T entity);
        Task Delete(T entity);
        Task Update(T entity);
        Task<List<T>> GetAll();
        Task<T> GetByIdAsync(int id);
        IEnumerable<T> Find(Expression<Func<T, bool>> expression);
    }
}
