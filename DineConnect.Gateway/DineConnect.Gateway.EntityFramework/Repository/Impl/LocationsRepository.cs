﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DineConnect.Gateway.Core.Models;
using DineConnect.Gateway.Core.Models.Data;

namespace DineConnect.Gateway.EntityFramework.Repository.Impl
{
    public class LocationsRepository : IRepository<Location>
    {
        private readonly DineConnectGatewayDbContext _dbContext;

        public LocationsRepository(DineConnectGatewayDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IUnitOfWork UnitOfWork => _dbContext;

        public void Add(Location entity)
        {
            _dbContext.Locations.Add(entity);
        }

        public Task Delete(Location entity)
        {
            _dbContext.Locations.Remove(entity);
            return Task.CompletedTask;
        }

        public IEnumerable<Location> Find(Expression<Func<Location, bool>> expression)
        {
            return _dbContext.Locations.Where(expression);
        }

        public Task<List<Location>> GetAll()
        {
            return _dbContext.Locations.Include(c => c.Brand).ToListAsync();
        }

        public Task<Location> GetByIdAsync(int id)
        {
            return _dbContext.Locations.Where(e => e.Id == id).SingleOrDefaultAsync();
        }

        public async Task Update(Location entity)
        {
            var location = await GetByIdAsync(entity.Id);
            location.Name = entity.Name;
            location.Code = entity.Code;
            location.Country = entity.Country;
            location.PlatformName = entity.PlatformName;
            location.BrandId = entity.BrandId;
            location.City = entity.City;
            location.UpdateDate = DateTime.Now;
            location.AddOn = entity.AddOn;
        }
    }
}
