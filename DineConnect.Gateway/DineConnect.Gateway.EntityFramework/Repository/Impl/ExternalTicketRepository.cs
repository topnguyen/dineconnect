﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DineConnect.Gateway.Core.Models;

namespace DineConnect.Gateway.EntityFramework.Repository.Impl
{
    public class ExternalTicketRepository : IRepository<DineConnect.Gateway.Core.Models.ExternalDeliveryTicket>
    {
        private readonly DineConnectGatewayDbContext _dbContext;
        public IUnitOfWork UnitOfWork => _dbContext;

        public ExternalTicketRepository(DineConnectGatewayDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public void Add(ExternalDeliveryTicket entity)
        {
            _dbContext.ExternalDeliveryTickets.Add(entity);

        }

        public Task Delete(ExternalDeliveryTicket entity)
        {
            _dbContext.ExternalDeliveryTickets.Remove(entity);
            return Task.CompletedTask;
        }

        public IEnumerable<ExternalDeliveryTicket> Find(Expression<Func<ExternalDeliveryTicket, bool>> expression)
        {
            return _dbContext.ExternalDeliveryTickets.Where(expression);
        }

        public Task<List<ExternalDeliveryTicket>> GetAll()
        {
            return _dbContext.ExternalDeliveryTickets.ToListAsync();
        }

        public Task<ExternalDeliveryTicket> GetByIdAsync(int id)
        {
            return _dbContext.ExternalDeliveryTickets.Where(e => e.Id == id).SingleOrDefaultAsync();
        }

        public async Task Update(ExternalDeliveryTicket entity)
        {
            var brand = await GetByIdAsync(entity.Id);
            brand.UpdateDate = DateTime.Now;
        }
    }
}
