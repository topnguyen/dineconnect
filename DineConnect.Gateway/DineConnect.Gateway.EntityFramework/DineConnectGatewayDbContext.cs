﻿using DineConnect.Gateway.Core.Models;
using DineConnect.Gateway.EntityFramework;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DineConnect.Gateway.Core.Models.Data;
using DineConnect.Gateway.EntityFramework.Repository;
using Microsoft.AspNet.Identity;

namespace DineConnect.Gateway.EntityFramework
{


    /// <summary>
    /// EntityFramework6\Add-Migration users-locations
    /// EntityFramework6\Update Database
    /// </summary>
    public class DineConnectGatewayDbContext : IdentityDbContext<GatewayUser>, IUnitOfWork
    {

        public DbSet<Location> Locations { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<ExternalDeliveryTicket> ExternalDeliveryTickets { get; set; }

        public DineConnectGatewayDbContext() : base("DineConnectGatewayDbContext")
        {
            Database.SetInitializer<DineConnectGatewayDbContext>(new CreateDatabaseIfNotExists<DineConnectGatewayDbContext>());
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public new Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            return base.SaveChangesAsync();
        }

    }
}
