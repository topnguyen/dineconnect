﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DineConnect.Gateway.Application
{
    public class GatewayOperationResult<T>
    {
        public bool IsSuccessful { get; private set; }
        public string Message { get; private set; }
        public T Entity { get; private set; }

        private GatewayOperationResult() { }
        public static GatewayOperationResult<T> Success(T entity)
        {
            return new GatewayOperationResult<T>() { IsSuccessful = true, Message = "OK", Entity = entity };
        }

        public static GatewayOperationResult<T> Fail(string message)
        {
            return new GatewayOperationResult<T>() { IsSuccessful = false, Message = message };
        }
    }
}
