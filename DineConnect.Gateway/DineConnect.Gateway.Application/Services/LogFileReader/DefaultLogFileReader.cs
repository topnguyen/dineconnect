﻿using DineConnect.Gateway.Core.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DineConnect.Gateway.Application.Services.LogFileReader
{
    public class DefaultLogFileReader : ILogFileReader
    {     
        public string Read(LogFile logFile)
        {
            using (var fs = new FileStream(logFile.Location, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            using (var sr = new StreamReader(fs, Encoding.Default))
            {
                return sr.ReadToEnd();
            }
        }
    }
}
