﻿using DineConnect.Gateway.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DineConnect.Gateway.Application.Services
{
    public interface ILogFileProvider
    {
        string LoggingDirectory { get; }
        List<LogFile> GetLogFiles();
    }
}
