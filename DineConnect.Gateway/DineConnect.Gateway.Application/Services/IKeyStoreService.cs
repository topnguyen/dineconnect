﻿using DineConnect.Gateway.Core.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DineConnect.Gateway.Application.Services
{
    public interface IKeyStoreService<T> where T: WithAddon
    {

        void Add(int id, string key, string value);
        KeyValuePair<string, string> Retrieve();
    }
}
