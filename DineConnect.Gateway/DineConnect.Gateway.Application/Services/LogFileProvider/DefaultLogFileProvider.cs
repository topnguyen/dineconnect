﻿using DineConnect.Gateway.Core.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DineConnect.Gateway.Application.Services.LogFileProvider
{
    public class DefaultLogFileProvider : ILogFileProvider
    {
        private readonly string _loggingDir;

        public DefaultLogFileProvider(string loggingDir)
        {
            _loggingDir = loggingDir;
        }

        public string LoggingDirectory => _loggingDir;

        public List<LogFile> GetLogFiles()
        {
            string[] filePaths = Directory.GetFiles(_loggingDir);

            List<LogFile> logFiles = new List<LogFile>();

            foreach(string path in filePaths)
            {
                logFiles.Add(LogFile.Create(path));
            }

            return logFiles;
        }
    }
}
