﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DineConnect.XeroTenantGateway.Authorization
{
    public class APIResult<T>
    {
        public bool IsSuccessful { get; private set; }
        public string Message { get; private set; }
        public T Entity { get; private set; }

        private APIResult() { }
        public static APIResult<T> Success(T entity)
        {
            return new APIResult<T>() { IsSuccessful = true, Message = "OK", Entity = entity };
        }

        public static APIResult<T> Fail(string message)
        {
            return new APIResult<T>() { IsSuccessful = false, Message = message };
        }

    }
}
