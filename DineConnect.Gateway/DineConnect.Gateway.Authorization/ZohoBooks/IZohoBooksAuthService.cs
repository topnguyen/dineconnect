﻿using DineConnect.XeroTenantGateway.Authorization.OAuthv2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DineConnect.XeroTenantGateway.Authorization.ZohoBooks
{
    public interface IZohoBooksAuthService
    {
        APIResult<List<ZohoBookOrganization>> GetOrganisations(OAuthv2AccessToken token);

    }
}
