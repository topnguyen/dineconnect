﻿using DineConnect.XeroTenantGateway.Authorization.OAuthv2;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DineConnect.XeroTenantGateway.Authorization.ZohoBooks
{
    public class ZohoBooksAuthService : IZohoBooksAuthService
    {
        public APIResult<List<ZohoBookOrganization>> GetOrganisations(OAuthv2AccessToken token)
        {
            var _zohoBooksApiClient = new RestClient("https://books.zoho.com/api/v3/organizations")
            {
                Authenticator = new JwtAuthenticator(token.AccessToken)
            };
            var request = new RestRequest(Method.GET);

            try
            {
                var response = _zohoBooksApiClient.Execute(request);
                if (!response.IsSuccessful)
                {
                    return APIResult<List<ZohoBookOrganization>>.Fail(response.Content);
                }

                var rootObject = JObject.Parse(response.Content);
                var organizationsNode = rootObject["organizations"];

                var result = JsonConvert.DeserializeObject<List<ZohoBookOrganization>>(organizationsNode.ToString());

                return APIResult<List<ZohoBookOrganization>>.Success(result);
            }
            catch (Exception ex)
            {
                return APIResult<List<ZohoBookOrganization>>.Fail(ex.Message);
            }

            throw new NotImplementedException();
        }
    }
}
