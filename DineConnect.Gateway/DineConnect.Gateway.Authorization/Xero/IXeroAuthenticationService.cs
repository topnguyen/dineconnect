﻿using DineConnect.XeroTenantGateway.Authorization.OAuthv2;
using DineConnect.XeroTenantGateway.Authorization.ZohoBooks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DineConnect.XeroTenantGateway.Authorization
{
    public interface IXeroAuthenticationService
    {
        APIResult<List<XeroConnection>> GetXeroConnections(OAuthv2AccessToken token);
    }
}
