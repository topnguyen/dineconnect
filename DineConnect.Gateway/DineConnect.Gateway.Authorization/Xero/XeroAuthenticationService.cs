﻿using DineConnect.XeroTenantGateway.Authorization.OAuthv2;
using DineConnect.XeroTenantGateway.Authorization.ZohoBooks;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;

namespace DineConnect.XeroTenantGateway.Authorization
{
    public class XeroAuthenticationService : IXeroAuthenticationService
    {
        public APIResult<List<XeroConnection>> GetXeroConnections(OAuthv2AccessToken token)
        {
            var _xeroApiClient = new RestClient("https://api.xero.com/connections")
            {
                Authenticator = new JwtAuthenticator(token.AccessToken)
            };
            var request = new RestRequest(Method.GET);

            try
            {
                var response = _xeroApiClient.Execute(request);
                if (!response.IsSuccessful)
                {
                    return APIResult<List<XeroConnection>>.Fail(response.Content);
                }

                var xeroConnections = JsonConvert.DeserializeObject<List<XeroConnection>>(response.Content);

                return APIResult<List<XeroConnection>>.Success(xeroConnections);
            }
            catch (Exception ex)
            {
                return APIResult<List<XeroConnection>>.Fail(ex.Message);
            }

            throw new NotImplementedException();
        }
    }
}
