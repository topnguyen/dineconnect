﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DineConnect.XeroTenantGateway.Authorization
{
    public class XeroConnection
    {
        public string TenantId { get; set; }
        public string TenantType { get; set; }
        public string TenantName { get; set; }
    }
}
