﻿using DineConnect.Gateway.EntityFramework;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Owin;
using System;
using System.Threading.Tasks;

namespace DineConect.Gateway.Web
{
    public class GatewayUserManager : UserManager<GatewayUser>
    {
        public GatewayUserManager(IUserStore<GatewayUser> store)
            : base(store)
        {
        }

        public async Task ChangePassword(Guid id, string newPassword)
        {
            var currentUser = await base.FindByIdAsync(id.ToString());
            if (currentUser != null)
            {
                await base.RemovePasswordAsync(currentUser.Id);
                await base.AddPasswordAsync(currentUser.Id, newPassword);
            }
        }

       


        public static GatewayUserManager Create(
            IdentityFactoryOptions<GatewayUserManager> options, IOwinContext context)
        {
            var manager = new GatewayUserManager(
                new UserStore<GatewayUser>(context.Get<DineConnectGatewayDbContext>()));

            return manager;
        }
    }
}