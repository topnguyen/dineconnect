﻿using AutoMapper;
using DineConect.Gateway.Web.Controllers.Dto;
using DineConect.Gateway.Web.ViewModels.Location;
using DineConnect.Gateway.Core.Models;

namespace DineConect.Gateway.Web
{
    public class MappingProfiler 
    {
        private static readonly object SyncObj = new object();
        private static volatile bool _mappedBefore;
        public MappingProfiler()
        {
        }

        private static void CreateMappingsInternal()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ExternalDeliveryTicket, ApiExternalTicketOutput>().ReverseMap();
            });
        }
      

        public static void CreateMappings()
        {
            lock (SyncObj)
            {
                if (_mappedBefore)
                {
                    return;
                }

                CreateMappingsInternal();

                _mappedBefore = true;
            }
        }


    }

   
}