﻿using System.Web.Mvc;
using System.Web.Routing;

namespace DineConect.Gateway.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "OrderStatus",
                url: "remoteId/{remoteId}/remoteOrder/{remoteOrderId}/posOrderStatus",
                defaults: new { controller = "DeliveryHeroStatus", action = "Index"}
            );
            
            routes.MapRoute(
                name: "Order",
                url: "Order/{id}",
                defaults: new { controller = "DeliveryHero", action = "Index"}
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
