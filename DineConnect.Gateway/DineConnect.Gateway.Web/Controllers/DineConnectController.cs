﻿using System;
using System.Configuration;
using System.Web.Mvc;

namespace DineConect.Gateway.Web.Controllers
{
    [Route("dineconnect")]
    public class DineConnectController : Controller
    {
        readonly string XeroCallbackUrl = string.Format("{0}/callback/xero", ConfigurationManager.AppSettings["DineConnectGatewayUrl"].ToString());
        readonly string QuickbooksCallbackUrl = string.Format("{0}/callback/quickbooks", ConfigurationManager.AppSettings["DineConnectGatewayUrl"].ToString());
        readonly string ZohoBooksCallbackUrl = string.Format("{0}/callback/zohobooks", ConfigurationManager.AppSettings["DineConnectGatewayUrl"].ToString());
        readonly string xeroClientId = ConfigurationManager.AppSettings["XeroClientId"].ToString();
        readonly string QuickBooksClientId = ConfigurationManager.AppSettings["QuickBooksClientId"].ToString();
        readonly string ZohoBooksClientId = ConfigurationManager.AppSettings["ZohoBooksClientId"].ToString();
        readonly string ZohoBooksClientSecret = ConfigurationManager.AppSettings["ZohoBooksClientSecret"].ToString();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationType">Xero, QuickBooks</param>
        /// <param name="tenantId">DineConnect TenantId</param>
        /// <param name="applicationUrl">http://abc.dineconnect.net, http://ced.dineconnect.net</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index(string applicationType, string applicationUrl, string dineConnectTenant = "1")
        {
            string loginUrl = string.Empty;

            if (applicationType.ToLower().Equals("xero"))
            {
                var scopes = "openid%20profile%20email%20accounting.transactions%20accounting.settings%20accounting.contacts%20offline_access";
                loginUrl = string.Format("https://login.xero.com/identity/connect/authorize?response_type=code&client_id={0}&redirect_uri={1}&scope={2}&state={3}",
                     xeroClientId,
                     XeroCallbackUrl,
                     scopes,
                     applicationUrl);
            }

            else if (applicationType.ToLower().Equals("quickbooks"))
            {
                var scope = "com.intuit.quickbooks.accounting";

                loginUrl =
                    string.Format("https://appcenter.intuit.com/connect/oauth2?client_id={0}&scope={1}&redirect_uri={2}&response_type=code&state={3}",
                    QuickBooksClientId,
                    scope,
                    QuickbooksCallbackUrl,
                    applicationUrl);
            }


            else if(applicationType.ToLower().Equals("zohobooks"))
            {
                var scope = "ZohoBooks.fullaccess.all";
                loginUrl = $"https://accounts.zoho.com/oauth/v2/auth?client_id={ZohoBooksClientId}&scope={scope}&response_type=code&redirect_uri={ZohoBooksCallbackUrl}&access_type=offline&prompt=consent&state={applicationUrl}";
            }

            else
            {
                throw new InvalidOperationException("Please check the application type. It must be one of those: Xero, Quickbooks");
            }

            return Redirect(loginUrl);
        }
    }
}