﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using DineConect.Gateway.Web.Pagination;
using DineConnect.Gateway.Core.Consts;
using DineConnect.Gateway.Core.Models;
using DineConnect.Gateway.Core.Models.Data;
using DineConnect.Gateway.EntityFramework;
using DineConnect.Gateway.EntityFramework.Repository;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DineConect.Gateway.Web.Controllers.BackOffice
{
    [Authorize]
    public class LocationsController : Controller
    {
        private IRepository<Brand> _brandRepository;
        private IRepository<Location> _locationRepository;


        public LocationsController(IRepository<Brand> brandRepo,
            IRepository<Location> locationRepo)
        {
            _locationRepository = locationRepo;
            _brandRepository = brandRepo;
        }

        public async Task<ActionResult> Index(int page=1)
        {
            var paginationViewModel = new PaginationViewModel<Location>()  
            {  
                CurrentPage = page,  
                Contents = await _locationRepository.GetAll(),
                PerPage = 10
            };  
            return View(paginationViewModel);  
          
        }

        public ActionResult Create()
        {
            var viewModel = new ViewModels.Location.Create(_brandRepository);

            SetViewBagPlatformName();
            SetViewBagAddOnKey();

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Code,Name,City,Country,PlatformName,TenantId,BrandId,AddOn")] Location location)
        {
            if (ModelState.IsValid)
            {
                location.CreateDate = DateTime.Now;
                _locationRepository.Add(location);
                await _locationRepository.UnitOfWork.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return await Index(1);
        }

        public async Task<ActionResult> Details(int id)
        {

            Location location = await _locationRepository.GetByIdAsync(id);

            if (location == null)
            {
                return HttpNotFound();
            }
            
            SetViewBagPlatformName();
            SetViewBagAddOnKey();

            return View(location);
        }

        public async Task<ActionResult> Edit(int id)
        {
            Location location = await _locationRepository.GetByIdAsync(id);

            var viewModel = new ViewModels.Location.Edit(_brandRepository, location);

            if (location == null)
            {
                return HttpNotFound();
            }

            SetViewBagPlatformName();

            SetViewBagAddOnKey();

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([System.Web.Http.FromBody] Location location)
        {
            if (ModelState.IsValid)
            {
                await _locationRepository.Update(location);
                await _locationRepository.UnitOfWork.SaveChangesAsync();              
                return RedirectToAction("Index");
            }
            
            SetViewBagPlatformName();
            SetViewBagAddOnKey();

            return await Index();
        }

        public async Task<ActionResult> Delete(int id)
        {
            Location location = await _locationRepository.GetByIdAsync(id);

            if (location == null)
            {
                return HttpNotFound();
            }
            return View(location);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Location location = await _locationRepository.GetByIdAsync(id);
            await _locationRepository.Delete(location);
            await _locationRepository.UnitOfWork.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _locationRepository.UnitOfWork.Dispose();
            }

            base.Dispose(disposing);
        }

        private void SetViewBagPlatformName()
        {
            ViewBag.PlatformNames = new List<string>
            {
                "DeliveryHero",
                "UrbanPiper"
            };
        }

        private void SetViewBagAddOnKey()
        {
            var listAddOnKeys = new List<string>
            {
                GatewayConstants.DeliveryHeroKey,
                GatewayConstants.UrbanPiperApiKey,
                GatewayConstants.UrbanPiperApiUserName,
                GatewayConstants.UrbanPiperUrl,
                GatewayConstants.DineConnectUrl,
                GatewayConstants.DineConnectLocationId,
            };

            ViewBag.AddOnKeys = listAddOnKeys;
        }
    }
}
