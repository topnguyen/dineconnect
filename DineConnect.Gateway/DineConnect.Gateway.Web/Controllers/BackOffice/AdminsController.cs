﻿using DineConnect.Gateway.EntityFramework;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DineConect.Gateway.Web.Controllers.BackOffice
{
    [Authorize]
    public class AdminsController : Controller
    {
        readonly DineConnectGatewayDbContext _dbContext;
        public AdminsController(DineConnectGatewayDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public ActionResult Index()
        {
            var users = _dbContext.Users.ToList();
            return View(users);
        }

        [HttpPost]
        public ActionResult Index(string username, string email, string password)
        {
            var manager = new UserManager<GatewayUser>(new UserStore<GatewayUser>(new DineConnectGatewayDbContext()));
            var user = new GatewayUser() { Email = email, UserName = username };
            user.RegistrationDate = DateTime.Now;
            var result = manager.Create(user, password);
            var users = _dbContext.Users.ToList();
            return View(users);
        }
    }
}