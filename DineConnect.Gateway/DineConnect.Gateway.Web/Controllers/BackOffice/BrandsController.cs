﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using DineConect.Gateway.Web.ViewModels;
using DineConnect.Gateway.Core.Models;
using DineConnect.Gateway.Core.Models.Data;
using DineConnect.Gateway.EntityFramework;
using DineConnect.Gateway.EntityFramework.Repository;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DineConect.Gateway.Web.Controllers.BackOffice
{
    [Authorize]
    public class BrandsController : Controller
    {
        private IRepository<Brand> _brandRepository;
        private IRepository<Location> _locationRepository;

 
        public BrandsController(IRepository<Brand> brandRepo,
            IRepository<Location> locationRepo
            )
        {
            _locationRepository = locationRepo;
            _brandRepository = brandRepo;
        }

        public async Task<ActionResult> Index()
        {

            return View(await _brandRepository.GetAll());
        }

        public async Task<ActionResult> Details(int id)
        {
            var brand = await _brandRepository.GetByIdAsync(id);

            if (brand == null)
            {
                return HttpNotFound();
            }

            SetViewBagAddOnKey();

            SetViewBagAddOnData(null);

            return View(brand);
        }

        public ActionResult Create()
        {
            SetViewBagAddOnKey();

            SetViewBagAddOnData(null);

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Code,Name,AddOn")] Brand brand)
        {
            if (ModelState.IsValid)
            {
                brand.CreateDate = DateTime.Now;
                _brandRepository.Add(brand);
                await _brandRepository.UnitOfWork.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            SetViewBagAddOnKey();

            SetViewBagAddOnData(null);

            return View(brand);
        }

        // GET: Brands/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            Brand brand = await _brandRepository.GetByIdAsync(id);

            if (brand == null)
            {
                return HttpNotFound();
            }

            SetViewBagAddOnKey();

            SetViewBagAddOnData(brand.AddOn);

            return View(brand);
        }

        // POST: Brands/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Code,Name,Addon")] Brand brand)
        {
            if (ModelState.IsValid)
            {
                await _brandRepository.Update(brand);
                await _brandRepository.UnitOfWork.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            SetViewBagAddOnKey();

            SetViewBagAddOnData(brand.AddOn);

            return View(brand);
        }

        // GET: Brands/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
   
            Brand brand = await _brandRepository.GetByIdAsync(id);
            if (brand == null)
            {
                return HttpNotFound();
            }
            return View(brand);
        }


        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Brand brand = await _brandRepository.GetByIdAsync(id);
            await _brandRepository.Delete(brand);
            await _brandRepository.UnitOfWork.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _brandRepository.UnitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        private void SetViewBagAddOnKey()
        {
            var listAddOnKeys = new List<string>
            {
                "Brand_Test"
            };

            ViewBag.AddOnKeys = listAddOnKeys;
        }

        private void SetViewBagAddOnData(string addOnData)
        {
            var listAddOnData = string.IsNullOrWhiteSpace(addOnData) ? new List<AddOn>() : JsonConvert.DeserializeObject<List<AddOn>>(addOnData);

            ViewBag.AddOnData = listAddOnData;
        }
    }
}
