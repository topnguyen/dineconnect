﻿namespace DinePlan.ConnectDeliveryHero.DeliveryHero.Dto
{
    public class StringObject
    {
        public string Content { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorDescription { get; set; }

    }
}
