﻿using System.Collections.Generic;

namespace DineConect.Gateway.Web.Controllers.Dto
{
    public class UpdateOrderStatusInput
    {
        public UpdateOrderStatusInput()
        {
            OrderStatus = new OrderStatusInput();
        }

        public int LocationId { get; set; }

        public string TicketNumber { get; set; }
        public string Status { get; set; }
        public string NewStatus { get; set; }

        public string Message { get; set; }
        public int OrderId { get; set; }
        public OrderStatusInput OrderStatus { get; set; }
    }

    public class UbSyncInput
    {
        public UbSyncInput()
        {
            SyncRoot = new UbSyncRootDto();
        }

        public UbSyncRootDto SyncRoot{ get; set; }

        public int LocationId  { get; set; }
    }
   
    public class UbSyncRootDto
    {
        public bool flush_categories;

        public UbSyncRootDto()
        {
            categories = new List<UbCategory>();
            items = new List<UbItem>();
            option_groups = new List<UbOptionGroup>();
            options = new List<UbOptions>();
            taxes = new List<UbTax>();
            charges = new List<UbCharge>();
        }

        public List<UbCategory> categories { get; set; }

        public List<UbItem> items { get; set; }
        public List<UbOptionGroup> option_groups { get; set; }
        public List<UbOptions> options { get; set; }
        public List<UbTax> taxes { get; set; }
        public List<UbCharge> charges { get; set; }


        public bool flush_items { get; set; }
        public bool flush_options { get; set; }
        public bool flush_option_groups { get; set; }
        public bool flush_charges { get; set; }
        public bool flush_taxes { get; set; }
    }

    public class UbCategory
    {
        public string ref_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int sort_order { get; set; }
        public bool active { get; set; }
        public string img_url { get; set; }
        public string parent_ref_id { get; set; }
    }

    public class UbItem
    {
        public UbItem()
        {
            category_ref_ids = new List<string>();
            sold_at_store = true;
            current_stock = -1;
            available = true;
            recommended = true;
            tags = new UbTags();
        }

        public string ref_id { get; set; }
        public string title { get; set; }
        public bool available { get; set; }
        public string description { get; set; }
        public bool sold_at_store { get; set; }
        public decimal price { get; set; }
        public int current_stock { get; set; }
        public bool recommended { get; set; }
        public int food_type { get; set; }
        public List<string> category_ref_ids { get; set; }
        public List<string> included_platforms { get; set; }
        public UbTags tags { get; set; }
        public string img_url { get; set; }
    }

    public class UbTags
    {
        public string[] zomato { get; set; }
        public string[] swiggy { get; set; }
    }

    public class UbOptionGroup
    {
        public UbOptionGroup()
        {
            active = true;
        }

        public string ref_id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public int min_selectable { get; set; }
        public int max_selectable { get; set; }
        public int sort_order { get; set; }
        public bool active { get; set; }
        public List<string> item_ref_ids { get; set; }
    }


    public class UbOptions
    {
        public UbOptions()
        {
            available = true;
            sold_at_store = true;
            food_type = "4";
        }

        public string ref_id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public decimal price { get; set; }
        public bool available { get; set; }
        public string food_type { get; set; }

        public bool sold_at_store { get; set; }
        public List<string> opt_grp_ref_ids { get; set; }
    }

    public class UbTax
    {
        public UbTax()
        {
            type = "percentage";
            applicable_on = "item.price";
        }

        public string ref_id { get; set; }
        public string title { get; set; }
        public bool active { get; set; }

        public string type { get; set; }
        public string applicable_on { get; set; }
        public string value { get; set; }

        public List<string> item_ref_ids { get; set; }
    }

    public class UbCharge
    {
        public UbCharge()
        {
            type = "percentage";
            applicable_on = "order.order_subtotal";
        }

        public string ref_id { get; set; }
        public string title { get; set; }
        public bool active { get; set; }

        public string type { get; set; }

        public List<string> item_ref_ids { get; set; }
        public string applicable_on { get; set; }
        public string value { get; set; }

        public List<string> excluded_platforms { get; set; }
    }


    #region Timing Group

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class UbSlot
    {
        public string start_time { get; set; }
        public string end_time { get; set; }
    }

    public class UbDaySlot
    {
        public string day { get; set; }
        public List<UbSlot> slots { get; set; }
    }

    public class UbTimingGroup
    {
        public string title { get; set; }
        public List<string> category_ref_ids { get; set; }
        public List<UbDaySlot> day_slots { get; set; }
    }

    public class UbTimingRoot
    {
        public List<UbTimingGroup> timing_groups { get; set; }
    }

    #endregion
}