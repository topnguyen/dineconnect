﻿using System;
using System.Collections.Generic;

namespace DineConect.Gateway.Web.Controllers.Dto
{
    public class OrderOutput
    {
        public OrderOutput()
        {
            remoteResponse = new OrderRemoteResponse();
        }

        public string code { get; set; }
        public string message { get; set; }
        public OrderRemoteResponse remoteResponse { get; set; }
    }

    public class OrderRemoteResponse
    {
        public OrderRemoteResponse()
        {
            remoteOrderId = "";
        }
        public string remoteOrderId { get; set; }

    }
    public class OrderStatusInput
    {
        public string new_status { get; set; }
        public string status { get; set; }
        public string message { get; set; }
    }


    public class DeHeRootObject
    {
        public string token { get; set; }
        public string code { get; set; }
        public string shortCode { get; set; }
        public bool preOrder { get; set; }
        public DateTime expiryDate { get; set; }
        public DateTime createdAt { get; set; }
        public DeHeLocalInfo localInfo { get; set; }
        public DeHePlatformRestaurant platformRestaurant { get; set; }
        public DeHeCustomer customer { get; set; }
        public DeHePayment payment { get; set; }
        public string expeditionType { get; set; }
        public List<DeHeProduct> products { get; set; }
        public DeHeComments comments { get; set; }
        public List<DeDiscount> discounts { get; set; }
        public DeHePrice price { get; set; }
        public bool webOrder { get; set; }
        public bool mobileOrder { get; set; }
        public bool corporateOrder { get; set; }
        public DeHeIntegrationInfo integrationInfo { get; set; }
        public DeHeDelivery delivery { get; set; }
        public DeHePickup pickup { get; set; }
    }

    public class DeDiscount
    {
        public string amount { get; set; }
        public string name { get; set; }
        public string type { get; set; }
    }

    public class DePayment
    {
        public string requiredMoneyChange { get; set; }
        public string vatName { get; set; }
        public string vatId { get; set; }
        public string remoteCode { get; set; }
        public string type { get; set; }
        public string status { get; set; }
    }

    public class DeHeLocalInfo
    {
        public string platform { get; set; }
        public string countryCode { get; set; }
        public string currencySymbol { get; set; }
        public string currencySymbolPosition { get; set; }
        public string currencySymbolSpaces { get; set; }
        public string decimalSeparator { get; set; }
        public string decimalDigits { get; set; }
        public string thousandsSeparator { get; set; }
        public string website { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
    }

    public class DeHePlatformRestaurant
    {
        public string id { get; set; }
    }

    public class DeHeCustomer
    {
        public string id { get; set; }
        public string code { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string mobilePhoneCountryCode { get; set; }
        public string mobilePhone { get; set; }
        public string email { get; set; }
    }

    public class DeHePayment
    {
        public string type { get; set; }
        public string remoteCode { get; set; }
        public string status { get; set; }
        public string requiredMoneyChange { get; set; }
        public string vatName { get; set; }
        public string vatId { get; set; }
    }

    public class DeHeVariation
    {
        public string name { get; set; }
    }

    public class DeHeProduct
    {
        public string id { get; set; }
        public string remoteCode { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string comment { get; set; }
        public object categoryName { get; set; }
        public DeHeVariation variation { get; set; }
        public decimal unitPrice { get; set; }
        public string paidPrice { get; set; }
        public string discountAmount { get; set; }
        public decimal quantity { get; set; }
        public bool halfHalf { get; set; }
        public string vatPercentage { get; set; }
        public List<DeSelectedTopping> selectedToppings { get; set; }
    }

    public class DeSelectedTopping
    {
        public decimal quantity { get; set; }
        public decimal price { get; set; }
        public string name { get; set; }
    }

    public class DeHeComments
    {
        public string customerComment { get; set; }
        public string vendorComment { get; set; }
    }

    public class DeHePrice
    {
        public string minimumDeliveryValue { get; set; }
        public string comission { get; set; }
        public string deliveryFee { get; set; }
        public List<DeDeliveryFee> deliveryFees { get; set; }
        public string containerCharge { get; set; }
        public string deliveryFeeDiscount { get; set; }
        public string serviceFeePercent { get; set; }
        public string serviceFeeTotal { get; set; }
        public int serviceTax { get; set; }
        public int serviceTaxValue { get; set; }
        public string subTotal { get; set; }
        public bool vatVisible { get; set; }
        public string vatPercent { get; set; }
        public string vatTotal { get; set; }
        public string grandTotal { get; set; }
        public decimal discountAmountTotal { get; set; }
        public string differenceToMinimumDeliveryValue { get; set; }
        public string payRestaurant { get; set; }
    }

    public class DeDeliveryFee
    {
        public string name { get; set; }
        public decimal value { get; set; }
    }

    public class DeHeIntegrationInfo
    {
    }

    public class DeHeDelivery
    {
        public bool expressDelivery { get; set; }
        public object expectedDeliveryTime { get; set; }
        public DateTime riderPickupTime { get; set; }
        public object address { get; set; }
    }

    public class DeHePickup
    {
        public string pickupTime { get; set; }
    }


    public class DeHeRootObjectPushOrder
    {
        public string Contents { get; set; }
        public int LocationId { get; set; }
        public string OrderId { get; set; }
        public int Tenant { get; set; }
        public string ContentType { get; set; }
    }

    public class DeOrderDto
    {
        public DeOrderDto()
        {
            Tags = new List<DeSelectedTopping>();
        }

        public int MenuItemId { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public string PortionName { get; set; }
        public string OrderState { get; set; }
        public int DepartmentId { get; set; }
        public int OrderRef { get; set; }
        public int PortionId { get; set; }
        public string MenuItemName { get; set; }
        public bool IsComboOrder { get; set; }
        public bool NoTax { get; set; }
        public decimal Discount { get; set; }
        public List<DeSelectedTopping> Tags { get; set; }
        public string Note { get; set; }
    }
}