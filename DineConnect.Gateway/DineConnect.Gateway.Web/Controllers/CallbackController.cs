﻿using System;
using System.Configuration;
using System.Web.Mvc;
using DineConnect.XeroTenantGateway.Authorization;
using DineConnect.XeroTenantGateway.Authorization.OAuthv2;
using DineConnect.XeroTenantGateway.Authorization.ZohoBooks;

namespace DineConect.Gateway.Web.Controllers
{
    [Route("callback")]
    public class CallbackController : Controller
    {

        private readonly IXeroAuthenticationService _xeroAuthenticationService = new XeroAuthenticationService();
        private readonly IOAuthv2Client _oAuthv2Client = new OAuthv2Client();
        private readonly IZohoBooksAuthService _zohoBooksAuthService = new ZohoBooksAuthService();
        
        private readonly string _qbRedirectUrl =
            $"{ConfigurationManager.AppSettings["DineConnectGatewayUrl"].ToString()}/callback/quickbooks";
        private readonly string _quickBooksClientId = ConfigurationManager.AppSettings["QuickBooksClientId"];
        private readonly string _quickBooksClientSecret = ConfigurationManager.AppSettings["QuickBooksClientSecret"];


        private readonly string _zohoBooksUrl =
            $"{ConfigurationManager.AppSettings["DineConnectGatewayUrl"].ToString()}/callback/zohobooks";
        readonly string _zohoBooksClientId = ConfigurationManager.AppSettings["ZohoBooksClientId"].ToString();
        readonly string _zohoBooksClientSecret = ConfigurationManager.AppSettings["ZohoBooksClientSecret"].ToString();
       
        readonly string _xeroRedirectUrl =
            $"{ConfigurationManager.AppSettings["DineConnectGatewayUrl"].ToString()}/callback/xero";
        readonly string _xeroClientId = ConfigurationManager.AppSettings["XeroClientId"].ToString();
        readonly string _xeroClientSecret = ConfigurationManager.AppSettings["XeroClientSecret"].ToString();
       
        [Route("xero")]
        public ActionResult Xero(string code, string state)
        {
            //var dineConnectTenantId = Convert.ToInt32(state);
            var applicationUrl = state;

            var authResult = _oAuthv2Client.RequestAccessToken("https://identity.xero.com/connect/token", _xeroClientId, _xeroClientSecret, code, _xeroRedirectUrl);

            if (!authResult.IsSuccessful)
                return Json(new { message = "Couldn't get the token from Xero authorization server. Message: " + authResult.Message }, JsonRequestBehavior.AllowGet);

            if (string.IsNullOrEmpty(authResult.Entity.AccessToken))
                return Json(new { message = "Couldn't get the token from Xero authorization server. Message: " + authResult.Message }, JsonRequestBehavior.AllowGet);

            var fetchConnectionsResult = _xeroAuthenticationService.GetXeroConnections(authResult.Entity);

            if (!fetchConnectionsResult.IsSuccessful)
            {
                return Json(new { message = "Couldn't get the organizations from Xero API endpoint. Message: " + authResult.Message }, JsonRequestBehavior.AllowGet);
            }

            ViewBag.applicationUrl = applicationUrl;
            ViewBag.accessToken = authResult.Entity.AccessToken;
            ViewBag.refreshToken = authResult.Entity.RefreshToken;
            ViewBag.expiresIn = authResult.Entity.ExpiresIn;

            return View(fetchConnectionsResult.Entity);
        }
 
        [HttpPost]
        public ActionResult Xero(string accessToken, string expiresIn, string refreshToken, string xeroConnectionId, string applicationUrl)
        {

            try
            {
                var tenantAddonUrl = $"/gatewaycallback/xero";

                string urlBuiltForTenant = $"{applicationUrl}/{tenantAddonUrl}?access_token={accessToken}&expires_in={expiresIn}&refresh_token={refreshToken}&xero_tenant_id={xeroConnectionId}";
             
                return Redirect(urlBuiltForTenant);
            }

            catch (Exception e)
            {
                return Json(new { message = e.Message, detailedMessage = e.InnerException.Message }, JsonRequestBehavior.AllowGet);
            }

        }
       
        /// <summary>
        /// RealmId is the company id.
        /// </summary>
        /// <param name="code"></param>
        /// <param name="state"></param>
        /// <param name="realmId"></param>
        /// <returns></returns>
        [Route("quickbooks")]
        public ActionResult Quickbooks(string code, string state, string realmId)
        {
            var applicationUrl = state;

            var authResult = _oAuthv2Client.RequestAccessToken("https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer",
                    _quickBooksClientId, _quickBooksClientSecret, code, _qbRedirectUrl);

            if (!authResult.IsSuccessful)
                return Json(new { message = "Couldn't get the token from QuickBooks authorization server. Message: " + authResult.Message }, JsonRequestBehavior.AllowGet);

            try
            {

                var tenantAddonUrl = $"gatewaycallback/quickbooks";

                string urlBuiltForTenant =
                    $"{applicationUrl}/{tenantAddonUrl}?access_token={authResult.Entity.AccessToken}&expires_in={authResult.Entity.ExpiresIn}&refresh_token={authResult.Entity.RefreshToken}&realm_id={realmId}";

                return Redirect(urlBuiltForTenant);
            }

            catch (Exception e)
            {
                return Json(new { message = e.Message, detailedMessage = e.InnerException.Message }, JsonRequestBehavior.AllowGet);
            }
        }

      
        [Route("zohobooks")]
        public ActionResult ZohoBooks(string code, string state)
        {
            var applicationUrl = state;

            var authResult = _oAuthv2Client.RequestAccessToken(" https://accounts.zoho.com/oauth/v2/token",
                    _zohoBooksClientId, _zohoBooksClientSecret, code, _zohoBooksUrl);

            if (!authResult.IsSuccessful)
                return Json(new { message = "Couldn't get the token from ZohoBooks authorization server. Message: " + authResult.Message }, JsonRequestBehavior.AllowGet);

            if(string.IsNullOrEmpty(authResult.Entity.AccessToken))
                return Json(new { message = "Couldn't get the token from ZohoBooks authorization server. Message: " + authResult.Message }, JsonRequestBehavior.AllowGet);

            var fetchOrganisationsResult = _zohoBooksAuthService.GetOrganisations(authResult.Entity);
    
            
            if(!fetchOrganisationsResult.IsSuccessful)
            {
                return Json(new { message = "Couldn't get the organizations from ZohoBooks API endpoint. Message: " + authResult.Message }, JsonRequestBehavior.AllowGet);
            }

            ViewBag.applicationUrl = applicationUrl;
            ViewBag.accessToken = authResult.Entity.AccessToken;
            ViewBag.refreshToken = authResult.Entity.RefreshToken;
            ViewBag.expiresIn = authResult.Entity.ExpiresIn;

            return View(fetchOrganisationsResult.Entity);          
        }
    
        [HttpPost]
        public ActionResult ZohoBooks(string accessToken, string expiresIn, string refreshToken, string organizationId, string applicationUrl)
        {
            try
            {
                var tenantAddonUrl = $"/gatewaycallback/zohobooks";

                string urlBuiltForTenant = $"{applicationUrl}/{tenantAddonUrl}?access_token={accessToken}&expires_in={expiresIn}&refresh_token={refreshToken}&organization_id={organizationId}";

                return Redirect(urlBuiltForTenant);
            }

            catch (Exception e)
            {
                return Json(new { message = e.Message, detailedMessage = e.InnerException.Message }, JsonRequestBehavior.AllowGet);
            }

        }
        
    }
}