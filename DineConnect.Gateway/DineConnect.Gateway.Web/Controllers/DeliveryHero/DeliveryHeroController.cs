﻿using System;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;
using DineConect.Gateway.Web.Controllers.Dto;
using DineConnect.Gateway.Core.Models;
using DineConnect.Gateway.Core.Models.Data;
using DineConnect.Gateway.EntityFramework;
using DineConnect.Gateway.EntityFramework.Repository;
using JWT;
using JWT.Algorithms;
using JWT.Builder;
using JWT.Exceptions;
using JWT.Serializers;
using Newtonsoft.Json;

namespace DineConect.Gateway.Web.Controllers.DeliveryHero
{
    public class DeliveryHeroController : Controller
    {
        private IRepository<Location> _locRepo;
        private IRepository<ExternalDeliveryTicket> _edtRepo;
        readonly string _deliveryHeroSecret = ConfigurationManager.AppSettings["DeliveryHeroSecret"].ToString();

        public DeliveryHeroController(IRepository<Location> locRepo, 
            IRepository<ExternalDeliveryTicket> edtRepo)
        {
            _locRepo = locRepo;
            _edtRepo = edtRepo;
        }

        public async Task<JsonResult> Index( string id, [FromBody] DeHeRootObject input)
        {
            Response.StatusCode = 500;

            OrderOutput returnOutput = new OrderOutput();
            try
            {
                if (input == null)
                {
                    returnOutput = new OrderOutput()
                    {
                        code = "INVALID_REQUEST",
                        message = "Invalid Request"
                    };
                    return Json(returnOutput, JsonRequestBehavior.AllowGet);  
                }
                var accessToken = Request.Headers["Authorization"];

                if (string.IsNullOrEmpty(accessToken))
                {
                    returnOutput = new OrderOutput()
                    {
                        code = "INVALID_REQUEST",
                        message = "Invalid Request No Auth Token"
                    };
                    return Json(returnOutput, JsonRequestBehavior.AllowGet);  
                }
                

                var accessTokenValue = accessToken.Replace("Bearer", "").Trim();
                System.Diagnostics.Trace.TraceError("Access Token : " + accessTokenValue);

                try
                {
                    var json = new JwtBuilder()
                        .WithAlgorithm(new HMACSHA512Algorithm()) // symmetric
                        .WithSecret(_deliveryHeroSecret)
                        .MustVerifySignature()
                        .Decode(accessTokenValue);    

                        System.Diagnostics.Trace.TraceError("Verified Token : " + json);
                }
                catch (TokenExpiredException)
                {
                    returnOutput = new OrderOutput()
                    {
                        code = "INVALID_REQUEST",
                        message = "Token has expired"
                    };
                    return Json(returnOutput, JsonRequestBehavior.AllowGet);  
                }
                catch (SignatureVerificationException)
                {
                    returnOutput = new OrderOutput()
                    {
                        code = "INVALID_REQUEST",
                        message = "Token has invalid signature"
                    };
                    return Json(returnOutput, JsonRequestBehavior.AllowGet); 
                }

                if (!ValidatePushOrder(input))
                {
                    returnOutput = new OrderOutput()
                    {
                        code = "VALIDATION_ERROR",
                        message = "Validation Error"
                    };
                    return Json(returnOutput, JsonRequestBehavior.AllowGet);  

                }
               
                var locationForOrder =
                    _locRepo.Find(a => a.Code.Equals(id)).LastOrDefault();

                if (locationForOrder == null)
                {
                    returnOutput = new OrderOutput()
                    {
                        code = "POS_ERROR",
                        message = "Location is not available"
                    };
                    return Json(returnOutput, JsonRequestBehavior.AllowGet);  

                }
              
                var locationId = locationForOrder.Id;
                var externalId = "";

                var output = await CreateOrUpdateDeliveryTicket(new UpdateExternalDeliveryTicketInput
                {
                    OrderId = input.code,
                    LocationId = locationId,
                    ContentType = DeliveryOrderConsts.PushOrder,
                    ExternalId = externalId,
                    Contents = JsonConvert.SerializeObject(input)
                });
                returnOutput.remoteResponse.remoteOrderId = output.ToString();
                Response.StatusCode = 202;

                
               
            }
            catch (Exception ex)
            {
                returnOutput = new OrderOutput()
                {
                    code = "POS_ERROR",
                    message = ex.Message
                };
            }
            return Json(returnOutput, JsonRequestBehavior.AllowGet);  
        }

        private async Task<int> CreateOrUpdateDeliveryTicket(UpdateExternalDeliveryTicketInput input)
        {
            var output = 0;
            var count =  _edtRepo.Find(
                a =>
                    a.TicketNumber != null && a.TicketNumber.Equals(input.OrderId) &&
                    a.LocationId.Equals(input.LocationId)).Count();

            var updateTicket = count > 0;
            var createTicket = true;
            if (updateTicket)
            {
                var ticket =
                    _edtRepo.Find(
                        a =>
                            a.TicketNumber.Equals(input.OrderId) &&
                            a.LocationId.Equals(input.LocationId)).LastOrDefault();

                if (ticket != null)
                {
                    createTicket = false;
                    output = ticket.Id;
                    if (input.ContentType.Equals(DeliveryOrderConsts.PushOrder))
                        if (!ticket.Status.Equals(DeliveryConsts.Initial))
                            await UpdateTicket(ticket, input);
                }
            }

            if (createTicket)
                output = await CreateTicket(input);

            return output;
        }

        private async Task UpdateTicket(ExternalDeliveryTicket ticket, UpdateExternalDeliveryTicketInput input)
        {
            ticket.Claimed = false;
            ticket.AddDeliveryContentValue(new DeliveryContentValue
            {
                Contents = JsonConvert.SerializeObject(input),
                TypeOfContent = input.ContentType
            });
            await _edtRepo.UnitOfWork.SaveChangesAsync();
        }

        private async Task<int> CreateTicket(UpdateExternalDeliveryTicketInput input)
        {
            var extT = new ExternalDeliveryTicket
            {
                TicketNumber = input.OrderId,
                LocationId = input.LocationId,
                Claimed = false,
                Status = DeliveryConsts.Initial,
            };

            extT.AddDeliveryContentValue(new DeliveryContentValue
            {
                Contents = JsonConvert.SerializeObject(input),
                TypeOfContent = input.ContentType
            });

            _edtRepo.Add(extT);
            await _edtRepo.UnitOfWork.SaveChangesAsync();
            return extT.Id;
        }

        private bool ValidatePushOrder(DeHeRootObject input)
        {
            if (string.IsNullOrEmpty(input.code))
                return false;

            if (input.platformRestaurant == null)
                return false;

            if (string.IsNullOrEmpty(input.platformRestaurant.id))
                return false;

            return true;
        }
    }
}