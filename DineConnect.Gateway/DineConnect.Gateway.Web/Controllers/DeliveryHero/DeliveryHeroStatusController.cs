﻿using System;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;
using DineConect.Gateway.Web.Controllers.Dto;
using DineConnect.Gateway.Core.Models;
using DineConnect.Gateway.Core.Models.Data;
using DineConnect.Gateway.EntityFramework;
using DineConnect.Gateway.EntityFramework.Repository;
using JWT.Algorithms;
using JWT.Builder;
using JWT.Exceptions;
using OrderStatusInput = DineConect.Gateway.Web.Controllers.Dto.OrderStatusInput;

namespace DineConect.Gateway.Web.Controllers.DeliveryHero
{
    public class DeliveryHeroStatusController : Controller
    {
        private readonly IRepository<Location> _locRepo;
        private readonly IRepository<ExternalDeliveryTicket> _edtRepo;
        readonly string _deliveryHeroSecret = ConfigurationManager.AppSettings["DeliveryHeroSecret"].ToString();

        public DeliveryHeroStatusController(IRepository<Location> locRepo, 
            IRepository<ExternalDeliveryTicket> edtRepo)
        {
            _locRepo = locRepo;
            _edtRepo = edtRepo;
        }

        [System.Web.Http.HttpPut]
        public async Task<JsonResult> Index( string remoteId, string remoteOrderId, [FromBody] OrderStatusInput input)
        {

            OrderOutput orderOutput = new OrderOutput();
            Response.StatusCode = 500;

            try
            {
                var accessToken = Request.Headers["Authorization"];

                if (string.IsNullOrEmpty(accessToken))
                {
                    orderOutput = new OrderOutput()
                    {
                        code = "INVALID_REQUEST",
                        message = "Invalid Request No Auth Token"
                    };
                    return Json(orderOutput, JsonRequestBehavior.AllowGet);  
                }
                var accessTokenValue = accessToken.Replace("Bearer", "").Trim();
               
                System.Diagnostics.Trace.TraceError("Access Token : " + accessTokenValue);

                var json = new JwtBuilder()
                    .WithAlgorithm(new HMACSHA512Algorithm()) // symmetric
                    .WithSecret(_deliveryHeroSecret)
                    .MustVerifySignature()
                    .Decode(accessTokenValue);    

                System.Diagnostics.Trace.TraceError("Verified Token : " + json);
            }
            catch (TokenExpiredException)
            {
                orderOutput = new OrderOutput()
                {
                    code = "INVALID_REQUEST",
                    message = "Token has expired"
                };
                return Json(orderOutput, JsonRequestBehavior.AllowGet);  
            }
            catch (SignatureVerificationException)
            {
                orderOutput = new OrderOutput()
                {
                    code = "INVALID_REQUEST",
                    message = "Token has invalid signature"
                };
                return Json(orderOutput, JsonRequestBehavior.AllowGet); 
            }

            if (string.IsNullOrEmpty(remoteId))
            {
                orderOutput = new OrderOutput()
                {
                    code = "INVALID_REQUEST",
                    message = "No Remote Id"
                };
            }else if (string.IsNullOrEmpty(remoteOrderId))
            {
                orderOutput = new OrderOutput()
                {
                    code = "INVALID_REQUEST",
                    message = "No Remote Order Id"
                };
            }
            else
            {
                var locationForOrder =
                    _locRepo.Find(a => a.Code.Equals(remoteId)).LastOrDefault();

                if (locationForOrder == null)
                {
                    orderOutput = new OrderOutput()
                    {
                        code = "POS_ERROR",
                        message = "Location is not available"

                    };
                }
                else
                {
                    var myTicketId = Convert.ToInt32(remoteOrderId);

                    var ticket =
                        _edtRepo.Find(a => a.Id == myTicketId).LastOrDefault();

                    if (ticket == null)
                    {
                        orderOutput = new OrderOutput()
                        {
                            code = "POS_ERROR",
                            message = "Order is not available"
                        };
                    }
                    else
                    {
                        if (input?.status != null && input.status.Equals("ORDER_CANCELLED"))
                        {
                            ticket.Status = DeliveryConsts.CancelledByDeliveryHero;
                        }

                        await UpdateTicket(ticket);
                        orderOutput = new OrderOutput()
                        {
                            code = "",
                            message = "Completed",
                            remoteResponse = new OrderRemoteResponse
                            {
                                remoteOrderId = myTicketId.ToString()
                            }
                        };
                        Response.StatusCode = 202;
                    }
                }
            }
            return Json(orderOutput, JsonRequestBehavior.AllowGet);  
        }

        private async Task UpdateTicket(ExternalDeliveryTicket ticket)
        {
            ticket.Claimed = false;
            await _edtRepo.UnitOfWork.SaveChangesAsync();
        }
    }
}