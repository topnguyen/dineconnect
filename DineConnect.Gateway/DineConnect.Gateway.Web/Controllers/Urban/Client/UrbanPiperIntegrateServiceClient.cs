﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using DineConect.Gateway.Web.Controllers.Urban.Dto;
using DineConnect.Gateway.Core.Extensions;
using DineConnect.Gateway.Core.MimeTypes;
using Newtonsoft.Json;

namespace DineConect.Gateway.Web.Controllers.Urban.Client
{
    public class UPApiClient
    {
        private object LockObject;

        private readonly HttpClient _httpClient = new HttpClient(new HttpClientHandler())
        {
            Timeout = TimeSpan.FromSeconds(600)
        };

        private const string AuthenticationHeaderName = "Authorization";

        static UPApiClient()
        {
        }

        private UPApiClient()
        {
            LockObject = new object();
        }

        public static UPApiClient Instance { get; } = new UPApiClient();

        public async Task<T> Request<T>(string url, string upApiKey, object requestObj, string method = "POST",string baseAddress="https://pos-int.urbanpiper.com")
        {
            try
            {
                var request = new HttpRequestMessage(new HttpMethod(method), CreateFullUrl(url,baseAddress));
                lock (LockObject)
                {
                    request.Headers.Add(AuthenticationHeaderName, upApiKey);
                    request.Headers.Add("Cache-Control", "no-cache");
                    request.Content = new StringContent(JsonConvert.SerializeObject(requestObj), Encoding.UTF8, MimeTypeNames.ApplicationJson);
                    _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MimeTypeNames.ApplicationJson));
                }

                var response = await _httpClient.SendAsync(request);

                var success = await response.Content.ReadAsStringAsync();
                var exception = JsonConvert.DeserializeObject<RequestRespomse>(success);
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception(exception.message);
                }

                T result;
                lock (LockObject)
                {
                    result = JsonConvert.DeserializeObject<T>(success);
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private Uri CreateFullUrl(string url, string baseAddress)
        {
            return new Uri(baseAddress.EnsureEndsWith('/') + url);
        }
       
    }
}