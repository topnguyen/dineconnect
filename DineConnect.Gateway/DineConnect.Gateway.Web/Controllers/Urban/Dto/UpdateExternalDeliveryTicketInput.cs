﻿using DineConect.Gateway.Web.Controllers.Dto;

namespace DineConect.Gateway.Web.Controllers.Urban.Dto
{
    public class UrbanPiperInput : UpdateExternalDeliveryTicketInput
    {
        public IUrbanInput UrbanInput {get;set;}
      

    }

    public class CommonResponse<T>
    {
        public string status { get; set; }

        public string message { get; set; }

        public T reference { get; set; }

        public bool Success
        {
            get { return status == "success"; }
        }
    }

    public class RequestRespomse
    {
        public string status { get; set; }
        public string message { get; set; }
    }
}
