﻿namespace DineConect.Gateway.Web.Controllers.Urban.Dto
{
    public class UrbanPiperConsts
    {
        public static string PushOrder = "PushOrder";
        public static string PushRider = "PushRider";
        public static string PushOrderStatus = "PushOrderStatus";
    }

    public enum ProviderConsts
    {
        Zomato = 1,
        Swiggy = 2,
        FoodPanda = 4,
        Dunzo=5
    }

    public enum RejectReason
    {
        item_out_of_stock = 1,
        store_closed = 2,
        store_busy = 3,
        rider_not_available = 4,
        out_of_delivery_radius = 5,
        connectivity_issue = 6,
        total_missmatch = 7,
        invalid_item = 8,
        option_out_of_stock = 9,
        invalid_option = 10,
        unspecified = 11
    }
}