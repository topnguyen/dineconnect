﻿using System.Collections.Generic;

namespace DineConect.Gateway.Web.Controllers.Urban.Dto
{
    public abstract class IUrbanInput
    {
    }

    public class UrbanAddress
    {
        public string city { get; set; }
        public bool is_guest_mode { get; set; }
        public decimal latitude { get; set; }
        public string line_1 { get; set; }
        public string line_2 { get; set; }
        public decimal longitude { get; set; }
        public string pin { get; set; }
        public string sub_locality { get; set; }
        public string tag { get; set; }
    }

    public class UrbanCustomer
    {
        public UrbanAddress address { get; set; }
        public string email { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
    }

    public class UrbanCharge
    {
        public string title { get; set; }
        public decimal value { get; set; }
    }

    public class UrbanExtras
    {
        public string order_type { get; set; }
        public bool thirty_minutes_delivery { get; set; }
        public decimal cash_to_be_collected { get; set; }
    }

    public class UrbanDiscount
    {
        public bool is_merchant_discount { get; set; }
        public decimal rate { get; set; }
        public string title { get; set; }
        public decimal value { get; set; }
        public string code { get; set; }
    }

    public class UrbanExtPlatform
    {
        public string id { get; set; }
        public string kind { get; set; }
        public string name { get; set; }
        public string delivery_type { get; set; }
        public UrbanExtras extras { get; set; }
        public List<UrbanDiscount> discounts { get; set; }
    }

    public class UrbanDetails
    {
        public string channel { get; set; }
        public List<UrbanCharge> charges { get; set; }
        public string coupon { get; set; }
        public long created { get; set; }
        public long delivery_datetime { get; set; }
        public decimal discount { get; set; }
        public decimal total_external_discount { get; set; }
        public List<UrbanExtPlatform> ext_platforms { get; set; }
        public int id { get; set; }
        public string instructions { get; set; }
        public decimal item_level_total_charges { get; set; }
        public decimal item_level_total_taxes { get; set; }
        public decimal item_taxes { get; set; }
        public int merchant_ref_id { get; set; }
        public int order_level_total_charges { get; set; }
        public int order_level_total_taxes { get; set; }
        public string order_state { get; set; }
        public decimal order_subtotal { get; set; }
        public decimal order_total { get; set; }
        public string order_type { get; set; }
        public string state { get; set; }
        public List<object> taxes { get; set; }
        public decimal total_charges { get; set; }
        public decimal total_taxes { get; set; }
    }

    public class UrbanCharge2
    {
        public string title { get; set; }
        public decimal value { get; set; }
    }

    public class UrbanOptionsToAdd
    {
        public int id { get; set; }
        public string merchant_id { get; set; }
        public decimal price { get; set; }
        public string title { get; set; }
    }

    public class UrbanTax
    {
        public decimal rate { get; set; }
        public string title { get; set; }
        public decimal value { get; set; }
    }

    public class UrbanItem
    {
        public List<UrbanCharge2> charges { get; set; }
        public decimal discount { get; set; }
        public string food_type { get; set; }
        public int id { get; set; }
        public object image_landscape_url { get; set; }
        public object image_url { get; set; }
        public string merchant_id { get; set; }
        public List<UrbanOptionsToAdd> options_to_add { get; set; }
        public List<object> options_to_remove { get; set; }
        public decimal price { get; set; }
        public int quantity { get; set; }
        public List<UrbanTax> taxes { get; set; }
        public string title { get; set; }
        public decimal total { get; set; }
        public decimal total_with_tax { get; set; }
        public object unit_weight { get; set; }
    }

    public class UrbanPayment
    {
        public decimal amount { get; set; }
        public string option { get; set; }
        public object srvr_trx_id { get; set; }
    }

    public class UrbanStore
    {
        public string address { get; set; }
        public int id { get; set; }
        public decimal latitude { get; set; }
        public decimal longitude { get; set; }
        public string merchant_ref_id { get; set; }
        public string name { get; set; }
    }

    public class UrbanOrder
    {
        public UrbanDetails details { get; set; }
        public List<UrbanItem> items { get; set; }
        public string next_state { get; set; }
        public List<UrbanPayment> payment { get; set; }
        public UrbanStore store { get; set; }
    }

    public class UrbanPushHead : IUrbanInput
    {
        public UrbanCustomer customer { get; set; }
        public UrbanOrder order { get; set; }
    }

    public class UrbanExternalChannel
    {
        public string name { get; set; }
        public string order_id { get; set; }
    }

    public class UrbanPushAdditionalInfo
    {
        public UrbanExternalChannel external_channel { get; set; }
    }

    public class UrbanPushStatusHead : IUrbanInput
    {
        public string new_state { get; set; }
        public int order_id { get; set; }
        public string store_id { get; set; }
    }

    public class DeliveryPersonDetails
    {
        public object alt_phone { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
    }

    public class UrbanStatusUpdate
    {
        public object comments { get; set; }
        public object created { get; set; }
        public string status { get; set; }
    }

    public class UrbanRiderDeliveryInfo
    {
        public string current_state { get; set; }
        public DeliveryPersonDetails delivery_person_details { get; set; }
        public List<UrbanStatusUpdate> status_updates { get; set; }
    }

    public class UrbanPushRiderStatusHead : IUrbanInput
    {
        public UrbanPushAdditionalInfo additional_info { get; set; }
        public UrbanRiderDeliveryInfo delivery_info { get; set; }
        public int order_id { get; set; }
    }

    public class UrbanPiperConnectionInput
    {
        public string Url { get; set; }
        public string User { get; set; }
        public string Key { get; set; }
    }

  

   
}