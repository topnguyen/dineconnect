﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DineConect.Gateway.Web.Controllers.Urban.Dto
{
    public class UPApiUrls
    {
        public const string ManagingStores = "external/api/v1/stores";
        public const string ManagingCatalogs = "external/api/v1/inventory/locations/";
        public const string ManagingOrders = "external/api/v1/orders/";
    }
}