﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;
using DineConect.Gateway.Web.Controllers.Dto;
using DineConect.Gateway.Web.Controllers.Urban.Client;
using DineConect.Gateway.Web.Controllers.Urban.Dto;
using DineConect.Gateway.Web.ViewModels;
using DineConnect.Gateway.Core.Consts;
using DineConnect.Gateway.Core.Models;
using DineConnect.Gateway.Core.Models.Data;
using DineConnect.Gateway.EntityFramework.Repository;
using Newtonsoft.Json;

namespace DineConect.Gateway.Web.Controllers.Urban
{
    public class UrbanPiperController : Controller
    {
        private readonly IRepository<ExternalDeliveryTicket> _edtRepo;
        private readonly IRepository<Location> _locRepo;

        public UrbanPiperController(IRepository<Location> locRepo, IRepository<ExternalDeliveryTicket> edtRepo)
        {
            _locRepo = locRepo;
            _edtRepo = edtRepo;
        }

        public async Task<JsonResult> StoreUpdate([FromBody] string input)
        {
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> StoreStatusUpdate([FromBody] string input)
        {
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> CatalogueUpdate([FromBody] string input)
        {
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> ItemUpdate([FromBody] string input)
        {
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> PushOrder([FromBody] UrbanPushHead input)
        {
            if (input == null) throw new Exception("Wrong Object");
            if (!ValidatePushOrder(input)) throw new Exception("Validations Failed");

            var locationForOrder =
                _locRepo.Find(a => a.Code.Equals(input.order.store.merchant_ref_id)).LastOrDefault();

            if (locationForOrder == null) throw new Exception("Locations is not exist");

            var locationId = locationForOrder.Id;
            var orderId = input.order.details.id.ToString();
            var externalId = "";
            if (input.order.details.ext_platforms.Any())
            {
                var firtP = input.order.details.ext_platforms.Last();
                externalId = firtP.id;
            }

            await CreateOrUpdateDeliveryTicket(new UrbanPiperInput
            {
                UrbanInput = input,
                LocationId = locationId,
                OrderId = orderId,
                ContentType = UrbanPiperConsts.PushOrder,
                ExternalId = externalId
            });

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> PushOrderStatus(UrbanPushStatusHead input)
        {
            if (input == null) throw new Exception("Wrong Object");
            if (!ValidatePushOrderStatus(input))
                throw new Exception("Validations Failed : " + input.order_id);

            var locationForOrder =
                _locRepo.Find(a => a.Code.Equals(input.store_id)).LastOrDefault();

            if (locationForOrder == null) throw new Exception("Locations is not exist : " + input.order_id);

            var locationId = locationForOrder.Id;
            var orderId = input.order_id.ToString();

            await CreateOrUpdateDeliveryTicket(new UrbanPiperInput
            {
                UrbanInput = input,
                LocationId = locationId,
                OrderId = orderId,
                ContentType = UrbanPiperConsts.PushOrderStatus
            });


            return Json("", JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> PushRiderStatus(UrbanPushRiderStatusHead input)
        {
            if (input == null) throw new Exception("Wrong Object");
            if (!ValidatePushRiderStatus(input)) throw new Exception("Validations Failed");

            var deliveryTicket =
                _edtRepo.Find(a => a.TicketNumber.Equals(input.order_id.ToString())).LastOrDefault();
            var locationId = 0;

            if (deliveryTicket != null)
                locationId = deliveryTicket.LocationId;
            else
                throw new Exception("No Ticket Available : " + input.order_id);

            var orderId = input.order_id.ToString();
            await CreateOrUpdateDeliveryTicket(new UrbanPiperInput
            {
                UrbanInput = input,
                LocationId = locationId,
                OrderId = orderId,
                ContentType = UrbanPiperConsts.PushRider
            });
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> SendOrderStatus(UpdateOrderStatusInput input)
        {

            var apiUserName = "biz_adm_73577076culctpvsxe";
            var apiKey = "d62a09b3a72188add8ce7564287597791137a440";
            var baseAddress = "https://pos-int.urbanpiper.com";


            var ticket = _edtRepo.Find(a => a.LocationId.Equals(input.LocationId)
                                            && a.TicketNumber.Equals(input.TicketNumber)).LastOrDefault();

            var location = _locRepo.Find(a => a.Id.Equals(input.LocationId)).LastOrDefault();




            if (ticket != null && location != null)
            {

                if (!string.IsNullOrEmpty(location.AddOn))
                {
                    var allKeys = JsonConvert.DeserializeObject<List<AddOn>>(location.AddOn);
                    if (allKeys != null && allKeys.Any())
                    {
                        apiUserName = GetValueFromList(allKeys, GatewayConstants.UrbanPiperApiUserName);
                        apiKey = GetValueFromList(allKeys, GatewayConstants.UrbanPiperApiKey);
                        baseAddress = GetValueFromList(allKeys, GatewayConstants.UrbanPiperUrl);
                    }
                }

                ticket.Claimed = true;
                ticket.Status = input.Status;
                await _edtRepo.Update(ticket);
                await _edtRepo.UnitOfWork.SaveChangesAsync();

                try
                {
                    input.OrderStatus = new OrderStatusInput()
                    {
                        message = input.Message,
                        new_status = input.NewStatus
                    };
                    input.OrderId = Convert.ToInt32(input.TicketNumber);

                  
                    var apiFinal = $"apikey {apiUserName}:{apiKey}";
                    var urlRequest = $"{UPApiUrls.ManagingOrders}{input.OrderId}/status/";

                    var dto = input.OrderStatus;

                    System.Diagnostics.Trace.TraceWarning("UrbanPiper Status Input");

                    System.Diagnostics.Trace.TraceWarning("API      : " + apiFinal);
                    System.Diagnostics.Trace.TraceWarning("URL      : " + urlRequest);
                    System.Diagnostics.Trace.TraceWarning("OrderId  : " + input.OrderId);
                    System.Diagnostics.Trace.TraceWarning("Status   : " + input.NewStatus);
                    System.Diagnostics.Trace.TraceWarning("Message  : " + input.Message);


                    var result =
                        await UPApiClient.Instance.Request<CommonResponse<string>>(urlRequest, apiFinal, dto, "PUT",
                            baseAddress);

                    System.Diagnostics.Trace.TraceWarning("UrbanPiper Status Output");

                    System.Diagnostics.Trace.TraceWarning("Message  : " + result.status);
                    System.Diagnostics.Trace.TraceWarning("Message  : " + result.message);

                    System.Diagnostics.Trace.TraceWarning("UrbanPiper Status End");

                }
                catch (Exception exception)
                {
                    return Json(exception.Message, JsonRequestBehavior.AllowGet);
                }
                   
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }

        private string GetValueFromList(List<AddOn> listofKeys, string lastKey)
        {
            var myKey = listofKeys.LastOrDefault(a=>a.Key.Equals(lastKey));
            if (myKey != null)
            {
                return myKey.Value;
            }

            return "";
        }

        public async Task<JsonResult> PushMenu(UbSyncInput input)
        {
            try
            {
                var location = _locRepo.Find(a => a.Id == input.LocationId).LastOrDefault();
                if (location == null)
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }

                var apiUserName = "biz_adm_73577076culctpvsxe";
                var apiApi = "d62a09b3a72188add8ce7564287597791137a440";
                var baseAddress = "https://pos-int.urbanpiper.com";
                string urlUncatalogued = "external/api/v1/inventory/locations/";

                urlUncatalogued = urlUncatalogued + location.Code + "/";

                var items = input.SyncRoot.items;

                var dto = new UbSyncRootDto();

                var apiKey = $"apikey {apiUserName}:{apiApi}";
                const int batchSize = 150;
                var page = 0;

                while (true)
                {
                    var itemsToPush = items.Skip(page * batchSize).Take(batchSize).ToList();
                    dto.items = itemsToPush;
                    var result =
                        await UPApiClient.Instance.Request<CommonResponse<string>>(urlUncatalogued, apiKey, dto, "POST",
                            baseAddress);
                    if (!result.Success) throw new Exception("Request Failed");

                    Thread.Sleep(6000);
                    if (itemsToPush.Count() < batchSize) break;
                    page = page + 1;
                }
                return Json("", JsonRequestBehavior.AllowGet);
            }
            catch (Exception exception)
            {
                return Json(exception.Message, JsonRequestBehavior.AllowGet);

            }
          
        }


        #region Validation

        private bool ValidatePushOrder(UrbanPushHead input)
        {
            return true;
        }

        private bool ValidatePushOrderStatus(UrbanPushStatusHead input)
        {
            return true;
        }

        private bool ValidatePushRiderStatus(UrbanPushRiderStatusHead input)
        {
            return true;
        }

        #endregion Validation

        #region Ticket Creation

        private async Task CreateOrUpdateDeliveryTicket(UrbanPiperInput input)
        {
            var count = _edtRepo.Find(
                a =>
                    a.TicketNumber != null && a.TicketNumber.Equals(input.OrderId) &&
                    a.LocationId.Equals(input.LocationId)).Count();

            var updateTicket = count > 0;
            var createTicket = true;
            if (updateTicket)
            {
                var ticket =
                    _edtRepo.Find(
                        a =>
                            a.TicketNumber.Equals(input.OrderId) &&
                            a.LocationId.Equals(input.LocationId)).LastOrDefault();

                if (ticket != null)
                {
                    createTicket = false;
                    if (input.ContentType.Equals(UrbanPiperConsts.PushOrder))
                        if (!ticket.Status.Equals(DeliveryConsts.Initial))
                            await UpdateTicket(ticket, input);

                    if (input.ContentType.Equals(UrbanPiperConsts.PushOrderStatus)) await UpdateTicket(ticket, input);

                    if (input.ContentType.Equals(UrbanPiperConsts.PushRider))
                        if (!DeliveryConsts.CompletedTicketStatus.Contains(ticket.Status))
                            await UpdateTicket(ticket, input);
                }
            }

            if (createTicket) await CreateTicket(input);
        }

        private async Task<int> CreateTicket(UpdateExternalDeliveryTicketInput input)
        {
            var extT = new ExternalDeliveryTicket
            {
                TicketNumber = input.OrderId,
                LocationId = input.LocationId,
                Claimed = false,
                Status = DeliveryConsts.Initial
            };

            extT.AddDeliveryContentValue(new DeliveryContentValue
            {
                Contents = JsonConvert.SerializeObject(input),
                TypeOfContent = input.ContentType
            });

            _edtRepo.Add(extT);
            await _edtRepo.UnitOfWork.SaveChangesAsync();

            return extT.Id;
        }

        private async Task UpdateTicket(ExternalDeliveryTicket ticket, UpdateExternalDeliveryTicketInput input)
        {
            ticket.Claimed = false;
            ticket.AddDeliveryContentValue(new DeliveryContentValue
            {
                Contents = JsonConvert.SerializeObject(input),
                TypeOfContent = input.ContentType
            });
            await _edtRepo.UnitOfWork.SaveChangesAsync();
        }

        #endregion Ticket Creation
    }
}