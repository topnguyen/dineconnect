﻿using System;
using DineConnect.Gateway.Application.Services;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DineConect.Gateway.Web.Controllers
{
    public class HomeController : Controller
    {
        public async Task<ActionResult> Index()
        {
            ViewBag.Title = "DineConnect Gateway";
            return View();
        }

        public ActionResult Qb()
        {
            return View();
        }


        public ActionResult ZohoBooks()
        {
            return View();
        }
    }
}
