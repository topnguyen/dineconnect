﻿using DineConnect.Gateway.EntityFramework;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace DineConect.Gateway.Web.Controllers
{
    public class AccountsController : Controller
    {

        [HttpPost]
        [Route("reset-password")]
        public async Task<ActionResult> ResetPassword(string userId, string newPassword)
        {
            try
            {
                var userManager = HttpContext.GetOwinContext().GetUserManager<GatewayUserManager>();
                await userManager.ChangePassword(Guid.Parse(userId), newPassword);
                return new JsonResult() { Data = new { IsSuccessful = true } };
            }
            catch (Exception ex)
            {

                throw ex;
            }         
        }

        public ActionResult Logout()
        {
            var AuthenticationManager = HttpContext.GetOwinContext().Authentication;
            AuthenticationManager.SignOut();
            return RedirectToAction("Login");
        }

        public ActionResult Login(string returnUrl)
        {
            ViewBag.returnUrl = returnUrl;
            return View();
        }


        /// <summary>
        /// May get a redirect url as well.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Login(string username, string password, string returnUrl = "/Home")
        {
            var userManager = HttpContext.GetOwinContext().GetUserManager<GatewayUserManager>();
            var authManager = HttpContext.GetOwinContext().Authentication;

            GatewayUser user = userManager.Find(username, password);

            if (user != null)
            {
                var ident = userManager.CreateIdentity(user,
                    DefaultAuthenticationTypes.ApplicationCookie);
                //use the instance that has been created. 
                authManager.SignIn(
                    new AuthenticationProperties { IsPersistent = false }, ident);
                return Redirect(returnUrl);
            }

            ModelState.AddModelError("", "Invalid username or password");
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult Token(string username, string password)
        {
            var userManager = HttpContext.GetOwinContext().GetUserManager<GatewayUserManager>();

            GatewayUser user = userManager.Find(username, password);

            if (user == null)
            {
                return Json(new
                {
                    error = "Invalid username or password",
                    data = string.Empty
                });
            }

            string key = ConfigurationManager.AppSettings["JwtTokenSecret"];
            var issuer = typeof(Startup).Namespace;   

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            //Create a List of Claims, Keep claims name short    
            var permClaims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Sid, user.Id)
            };

            //Create Security Token object by giving required parameters    

            var expiresTime = DateTime.Now.AddDays(1);

            var token = new JwtSecurityToken(issuer, 
                issuer,
                permClaims,
                expires: expiresTime,
                signingCredentials: credentials);
            
            var jwt_token = new JwtSecurityTokenHandler().WriteToken(token);

            return Json( new
            {
                error = string.Empty,
                data = jwt_token,
                expires = expiresTime.ToString("G")
            });
        }
    }
}