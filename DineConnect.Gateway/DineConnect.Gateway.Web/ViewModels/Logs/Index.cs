﻿using DineConnect.Gateway.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DineConect.Gateway.Web.ViewModels.Logs
{
    public class Index
    {
        public List<LogFile> LogFiles { get; set; }
    }
}