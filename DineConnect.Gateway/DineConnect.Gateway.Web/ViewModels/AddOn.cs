﻿namespace DineConect.Gateway.Web.ViewModels
{
    public class AddOn
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }
}
