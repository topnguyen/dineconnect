﻿using DineConnect.Gateway.Core.Models;
using System.Collections.Generic;
using DineConnect.Gateway.EntityFramework.Repository;

namespace DineConect.Gateway.Web.ViewModels.Location
{
    public class Create
    {
        public Create(IRepository<Brand> brandRepo)
        {
            Brands = brandRepo.GetAll().Result;
        }

        public IList<Brand> Brands { get; set; }

        public List<AddOn> AddOns { get; set; } = new List<AddOn>();
    }
}