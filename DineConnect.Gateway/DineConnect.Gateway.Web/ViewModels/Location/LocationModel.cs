﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DineConect.Gateway.Web.ViewModels.Location
{
    public class LocationModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string PlatformName { get; set; }
        public int BrandId { get; set; }
        public string BrandName { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}