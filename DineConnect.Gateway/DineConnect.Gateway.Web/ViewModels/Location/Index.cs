﻿using DineConect.Gateway.Web.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DineConect.Gateway.Web.ViewModels.Location
{
    public class Index
    {
        public Index(List<DineConnect.Gateway.Core.Models.Data.Location> locations)
        {
            Locations = locations.ToViewModel();
        }
        public List<LocationModel> Locations { get; set; }
    }
}