﻿$(function () {
    $("#NewAddOnAddBtn").on("click", function (e) {

        var newAddOnKey = $("#NewAddOnKey").val();

        var newAddOnValue = $("#NewAddOnValue").val();

        if (!newAddOnKey || newAddOnKey.length <= 0 || !newAddOnValue || newAddOnValue.length <= 0) {
            e.preventDefault();
            return;
        }

        // Get add on template

        var addOnHtml = $('#addon-data-template').html();

        // Fill new add on template

        addOnHtml = addOnHtml.replace('{{Key}}', newAddOnKey);

        addOnHtml = addOnHtml.replace('{{Value}}', newAddOnValue);

        // Append new addon data to table

        var $addOnHtml = $(addOnHtml);

        $addOnHtml.insertBefore($('#addon-new-data'));

        // Init Events

        initDeleteAddOnBtn();

        // Reset Add On New Value

        $("#NewAddOnKey").val($("#NewAddOnKey option:first").val());

        $("#NewAddOnValue").val('');
    });

    // Delete Add On Button

    initDeleteAddOnBtn();

    function initDeleteAddOnBtn(e) {
        $(".DeleteAddOnBtn").on("click", function (e) {
            var closestTr = $(this).closest("tr");
            closestTr.remove();
        });
    }

    // Submit Form Have Add On Table

    $('.submitFormHaveAddOnTableBtn').on('click', function (e) {
        var listAddOnData = [];

        // Fill Add On input data
        $('.addon-data').each(function (index) {
            var $this = $(this);

            var keyHtml = $this.find('.addon-data-key')[0];
            var keyData = $(keyHtml).text();

            var valueHtml = $this.find('.addon-data-value')[0];
            var valueData = $(valueHtml).text();

            listAddOnData.push({
                key: keyData,
                value: valueData
            });
        });

        var addOnJsonString = JSON.stringify(listAddOnData);

        $("#AddOn").val(addOnJsonString);

        // Submit
        $(this).closest("form").submit();
    });
});