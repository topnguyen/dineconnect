// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DefaultRegistry.cs" company="Web Advanced">
// Copyright 2012 Web Advanced (www.webadvanced.com)
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using AutoMapper;
using DineConnect.Gateway.Core.Models.Data;
using DineConnect.Gateway.EntityFramework.Repository.Impl;

namespace DineConnect.Gateway.DependencyResolution {
    using DineConnect.Gateway.Application.Services;
    using DineConnect.Gateway.Application.Services.LogFileProvider;
    using DineConnect.Gateway.Application.Services.LogFileReader;
    using DineConnect.Gateway.EntityFramework;
    using DineConnect.Gateway.EntityFramework.Repository;
    using StructureMap.Configuration.DSL;
    using StructureMap.Graph;
	
    public class DefaultRegistry : Registry {
        #region Constructors and Destructors

        public DefaultRegistry() {
            Scan(
                scan => {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
					scan.With(new ControllerConvention());
                });

            For<ILogFileReader>().Use<DefaultLogFileReader>();
            For<ILogFileProvider>().Use<DefaultLogFileProvider>().Ctor<string>("loggingDir").Is(@"D:\Home\LogFiles\");
            For<DineConnectGatewayDbContext>().Use<DineConnectGatewayDbContext>(); 
            For<IRepository<Gateway.Core.Models.Brand>>().Use<BrandsRepository>();
            For<IRepository<Location>>().Use<LocationsRepository>();
            For<IRepository<Gateway.Core.Models.ExternalDeliveryTicket>>().Use<ExternalTicketRepository>();
            For<Mapper>().Use<Mapper>();

        }

        #endregion
    }
}