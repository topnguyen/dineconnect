﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DineConnect.Gateway.Core.Models;

namespace DineConect.Gateway.Web.Pagination
{
    public class PaginationViewModel<T> where T : TrackedEntity
    {
        public IEnumerable<T> Contents { get; set; }  
        public int PerPage { get; set; }  
        public int CurrentPage { get; set; }  
  
        public int PageCount()  
        {  
            return Convert.ToInt32(Math.Ceiling(Contents.Count() / (double)PerPage));  
        }  
        public IEnumerable<T> PaginatedContents()  
        {  
            int start = (CurrentPage - 1) * PerPage;  
            return Contents.OrderBy(b=>b.Id).Skip(start).Take(PerPage);  
        }  
    }
}