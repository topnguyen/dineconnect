﻿using System.Reflection;
using Abp.Modules;
using DinePlan.DineConnect;

namespace DineConnect.Regression
{
    [DependsOn(typeof (DineConnectDataModule))]
    public class RegressionModule : AbpModule
    {
       
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}