﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Abp.Application.Services.Dto;
using Abp.Dependency;
using Abp.Domain.Repositories;
using DineConnect.Regression.Utility;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Connect.Users;
using DinePlan.DineConnect.Engage.Gift;
using Newtonsoft.Json;

namespace DineConnect.Regression.Code
{
    public class Executor : ITransientDependency
    {
        private readonly IRepository<MenuItem> _menuRepo;
        private readonly IRepository<ScreenMenu> _screenMenu;
        private readonly IRepository<Location> _locRepo;
        private readonly IRepository<Promotion> _promotion;
        private readonly IRepository<Department> _dept;
        private readonly  IRepository<GiftVoucherType> _giftvoucher;
        private readonly IRepository<DinePlanUser> _user;
        private readonly IRepository<PriceTag> _tag;

        public Executor(
            IRepository<Location> locationRepo, IRepository<ScreenMenu> screenRepo, IRepository<MenuItem> menuRepo,
            IRepository<GiftVoucherType>  gType,
            IRepository<PriceTag> tag,
            IRepository<DinePlanUser>  user,
            IRepository<Department>  dept,
            IRepository<Promotion> promotion
           )
        {
            _locRepo = locationRepo;
            _screenMenu = screenRepo;
            _menuRepo = menuRepo;
            _promotion = promotion;
            _giftvoucher = gType;
            _dept = dept;
            _dept = dept;
            _user = user;
            _tag = tag;
        }


        public void Run()
        {
            var allLocations = _locRepo.GetAllList();

            Console.WriteLine("SCREEN MENU STARTS");
            foreach (var allMenu in _screenMenu.GetAllList())
            {
                Console.WriteLine("SCREEN MENU : " + allMenu.Id);
                List<SimpleLocationDto> allMyLocations = new List<SimpleLocationDto>();
                if (string.IsNullOrEmpty(allMenu.Locations)) continue;
                List<ComboboxItemDto> allDtos =
                    JsonConvert.DeserializeObject<List<ComboboxItemDto>>(allMenu.Locations);

                allMyLocations.AddRange(from allDto in allDtos
                                        let location = allLocations.FirstOrDefault(a => a.Id == Convert.ToInt32(allDto.Value))
                                        where location != null
                                        select new SimpleLocationDto
                                        {
                                            Id = location.Id,
                                            Code = location.Code,
                                            Name = location.Name
                                        });
                allMenu.Locations = JsonConvert.SerializeObject(allMyLocations);
                _screenMenu.Update(allMenu);
            }
            Console.WriteLine("SCREEN MENU ENDS");



            Console.WriteLine("PROMOTION STARTS");
            foreach (var allMenu in _promotion.GetAllList())
            {
                Console.WriteLine("PROMOTION : " + allMenu.Id);
                List<SimpleLocationDto> allMyLocations = new List<SimpleLocationDto>();
                if (string.IsNullOrEmpty(allMenu.Locations)) continue;
                List<LocationListDto> allDtos =
                    JsonConvert.DeserializeObject<List<LocationListDto>>(allMenu.Locations);

                allMyLocations.AddRange(from allDto in allDtos
                                        let location = allLocations.FirstOrDefault(a => a.Id == Convert.ToInt32(allDto.Id))
                                        where location != null
                                        select new SimpleLocationDto
                                        {
                                            Id = location.Id,
                                            Code = location.Code,
                                            Name = location.Name
                                        });
                allMenu.Locations = JsonConvert.SerializeObject(allMyLocations);
                _promotion.Update(allMenu);
            }
            Console.WriteLine("PROMOTION ENDS");


            Console.WriteLine("GIFT VOUCHER STARTS");
            foreach (var allMenu in _giftvoucher.GetAllList())
            {
                Console.WriteLine("GIFT VOUCHER : " + allMenu.Id);
                List<SimpleLocationDto> allMyLocations = new List<SimpleLocationDto>();
                if (string.IsNullOrEmpty(allMenu.Locations)) continue;
                List<LocationListDto> allDtos =
                    JsonConvert.DeserializeObject<List<LocationListDto>>(allMenu.Locations);

                allMyLocations.AddRange(from allDto in allDtos
                                        let location = allLocations.FirstOrDefault(a => a.Id == Convert.ToInt32(allDto.Id))
                                        where location != null
                                        select new SimpleLocationDto
                                        {
                                            Id = location.Id,
                                            Code = location.Code,
                                            Name = location.Name
                                        });
                allMenu.Locations = JsonConvert.SerializeObject(allMyLocations);
                _giftvoucher.Update(allMenu);
            }
            Console.WriteLine("GIFT VOUCHER ENDS");



            Console.WriteLine("DEPARTMENTS STARTS");
            foreach (var allMenu in _dept.GetAllList())
            {
                Console.WriteLine("DEPARTMENT : " + allMenu.Id);
                List<SimpleLocationDto> allMyLocations = new List<SimpleLocationDto>();
                if (string.IsNullOrEmpty(allMenu.Locations)) continue;
                List<ComboboxItemDto> allDtos =
                    JsonConvert.DeserializeObject<List<ComboboxItemDto>>(allMenu.Locations);

                allMyLocations.AddRange(from allDto in allDtos
                                        let location = allLocations.FirstOrDefault(a => a.Id == Convert.ToInt32(allDto.Value))
                                        where location != null
                                        select new SimpleLocationDto
                                        {
                                            Id = location.Id,
                                            Code = location.Code,
                                            Name = location.Name
                                        });
                allMenu.Locations = JsonConvert.SerializeObject(allMyLocations);
                _dept.Update(allMenu);
            }
            Console.WriteLine("DEPARTMENT ENDS");



            Console.WriteLine("DINEPLAN USER STARTS");
            foreach (var allMenu in _user.GetAllList())
            {
                Console.WriteLine("DINEPLAN USER : " + allMenu.Id);
                List<SimpleLocationDto> allMyLocations = new List<SimpleLocationDto>();
                if (string.IsNullOrEmpty(allMenu.Locations)) continue;
                List<LocationListDto> allDtos =
                    JsonConvert.DeserializeObject<List<LocationListDto>>(allMenu.Locations);

                allMyLocations.AddRange(from allDto in allDtos
                                        let location = allLocations.FirstOrDefault(a => a.Id == Convert.ToInt32(allDto.Id))
                                        where location != null
                                        select new SimpleLocationDto
                                        {
                                            Id = location.Id,
                                            Code = location.Code,
                                            Name = location.Name
                                        });
                allMenu.Locations = JsonConvert.SerializeObject(allMyLocations);
                _user.Update(allMenu);
            }
            Console.WriteLine("DINEPLAN USER ENDS");


            Console.WriteLine("MENUITEMS");
            foreach (var allMenu in _menuRepo.GetAllList())
            {
                Console.WriteLine("MENUITEMS: " + allMenu.Id);
                List<SimpleLocationDto> allMyLocations = new List<SimpleLocationDto>();
                if (string.IsNullOrEmpty(allMenu.Locations)) continue;
                List<ComboboxItemDto> allDtos =
                    JsonConvert.DeserializeObject<List<ComboboxItemDto>>(allMenu.Locations);

                allMyLocations.AddRange(from allDto in allDtos
                                        let location = allLocations.FirstOrDefault(a => a.Id == Convert.ToInt32(allDto.Value))
                                        where location != null
                                        select new SimpleLocationDto
                                        {
                                            Id = location.Id,
                                            Code = location.Code,
                                            Name = location.Name
                                        });
                allMenu.Locations = JsonConvert.SerializeObject(allMyLocations);
                _menuRepo.Update(allMenu);
            }
            Console.WriteLine("MENUITEMS ENDS");


            Console.WriteLine("PRICETAGS");
            foreach (var allMenu in _tag.GetAllList())
            {
                Console.WriteLine("PRICETAGS: " + allMenu.Id);
                List<SimpleLocationDto> allMyLocations = new List<SimpleLocationDto>();
                if (string.IsNullOrEmpty(allMenu.Locations)) continue;
                List<LocationListDto> allDtos =
                    JsonConvert.DeserializeObject<List<LocationListDto>>(allMenu.Locations);

                allMyLocations.AddRange(from allDto in allDtos
                                        let location = allLocations.FirstOrDefault(a => a.Id == Convert.ToInt32(allDto.Id))
                                        where location != null
                                        select new SimpleLocationDto
                                        {
                                            Id = location.Id,
                                            Code = location.Code,
                                            Name = location.Name
                                        });
                allMenu.Locations = JsonConvert.SerializeObject(allMyLocations);
                _tag.Update(allMenu);
            }
            Console.WriteLine("PRICETAGS ENDS");
        }

        public class LocationGroupDto
        {
            public LocationGroupDto()
            {
                Locations = new List<SimpleLocationDto>();
                Groups = new List<SimpleLocationGroupDto>();
                Group = false;
            }
            public List<SimpleLocationDto> Locations { get; set; }
            public List<SimpleLocationGroupDto> Groups { get; set; }
            public bool Group { get; set; }
        }

        public class SimpleLocationDto
        {
            public virtual int Id { get; set; }
            public virtual string Code { get; set; }
            public virtual string Name { get; set; }
        }

        public class SimpleLocationGroupDto
        {
            public virtual int Id { get; set; }
            public virtual string Name { get; set; }
        }

        public class LocationListDto : FullAuditedEntityDto
        {
            public int CompanyRefId { get; set; }
            public Company companies { get; set; }
            public string CompanyName { get; set; }
            public virtual string Code { get; set; }
            public virtual string Name { get; set; }
            public virtual string Address1 { get; set; }
            public virtual string Address2 { get; set; }
            public virtual string Address3 { get; set; }
            public virtual string City { get; set; }
            public virtual string State { get; set; }
            public virtual string Country { get; set; }
            public virtual string PhoneNumber { get; set; }
            public virtual decimal SharingPercentage { get; set; }
            public virtual string Fax { get; set; }
            public virtual string Website { get; set; }
            public virtual string Email { get; set; }
            public virtual bool IsPurchaseAllowed { get; set; }
            public virtual bool IsProductionAllowed { get; set; }
            public virtual int DefaultRequestLocationRefId { get; set; }
            public virtual string RequestedLocations { get; set; }
            public virtual DateTime? HouseTransactionDate { get; set; }
            public virtual string TallyCompanyName { get; set; }
            public bool IsProductionUnitAllowed { get; set; }

            public virtual int ExtendedBusinessHours { get; set; }
            public virtual bool IsDayCloseRecursiveUptoDateAllowed { get; set; }
            public int? LocationGroupId { get; set; }

            public string LocationGroupName { get; set; }

        }

    }
}