﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Abp.Dependency;
using Abp.Domain.Repositories;
using DineConnect.Regression.Utility;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Transaction;

namespace DineConnect.Regression
{
    public class Executor : ITransientDependency
    {
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<Company> _companyRepo;
        private readonly IRepository<MenuItemPortion> _portionRepo;
        private readonly IRepository<MenuItem> _menuRepo;

        
        private readonly IRepository<PaymentType> _paymentRepo;
        private readonly IRepository<TransactionType> _transactionRepo;
        private readonly IRepository<Ticket> _tManager;


        public Executor(
            IRepository<Location> locationRepo, IRepository<Ticket> tManager, IRepository<MenuItem> menuRepo,
            IRepository<Company> companyRepo, IRepository<MenuItemPortion> portionRepo, IRepository<PaymentType> paymentType, IRepository<TransactionType> tTypeRepo)
        {
            _locationRepo = locationRepo;
            _companyRepo = companyRepo;
            _portionRepo = portionRepo;
            _paymentRepo = paymentType;
            _transactionRepo = tTypeRepo;
            _tManager = tManager;
            _menuRepo = menuRepo;
        }

        public void Run(int noDays)
        {
            var tenants = _locationRepo.GetAllList();
        }

        public void ImportLocations(InputDto dto)
        {
            if (!string.IsNullOrEmpty(dto.OrganizationName))
            {
                int cId = 0;
                var companyExists = _companyRepo.FirstOrDefault(a => a.Name.ToUpper().Equals(dto.OrganizationName.ToUpper()));
                if (companyExists == null)
                {
                    Company company = new Company
                    {
                        Code = dto.OrganizationName.Substring(2).ToUpper(),
                        Name = dto.OrganizationName.ToUpper(),
                        TenantId = dto.TenantId
                    };

                    cId = _companyRepo.InsertAndGetId(company);
                }
                else
                {
                    cId = companyExists.Id;
                }

                var data = new ExcelData(dto.LocationPath, new FileStream(dto.LocationPath,FileMode.Open));
                var allList = data.GetData();

                foreach (var alLoc in allList)
                {
                    _locationRepo.InsertAndGetId(new Location
                    {
                        Code = alLoc["Code"].ToString(),
                        Name = alLoc["Name"].ToString(),
                        TenantId = dto.TenantId,
                        CompanyRefId = cId
                    });
                }
            }
        }

        public void Run(InputDto inputdto)
        {
            var allMenuItems = _menuRepo.GetAllList();
            var allPortions = _portionRepo.GetAllList();
            var allPaymentTypes = _paymentRepo.GetAllList();
            var allTransactionTypes = _transactionRepo.GetAllList();
            var allLocations = _locationRepo.GetAllList();

            var ticketCount = 10;
            var ticktNumber = 1;
            var orderNumber = 1;

            var endDate = DateTime.Now;
            var startDate = DateTime.Now.AddDays(-inputdto.TotalDays);

            for (var date = startDate; date.Date <= endDate; date = date.AddDays(1))
                for (var i = 1; i <= ticketCount; i++)
                {
                    var hour = i%23;
                    var createdDay = date.AddHours(i);

                    if (createdDay > DateTime.Now)
                        createdDay = DateTime.Now;

                    foreach (
                        var locationListDto in allLocations)
                    {
                        var ticket = new Ticket
                        {
                            LocationId = locationListDto.Id,
                            TaxIncluded = false,
                            TicketCreatedTime = createdDay,
                            LastModifiedUserName = locationListDto.Code + "_Admin",
                            TicketId = ticktNumber,
                            TicketNumber = ticktNumber.ToString(),
                            TerminalName = locationListDto.Code + "Server",
                            TicketTypeName = "Ticket",
                            LastUpdateTime = createdDay.AddMinutes(10),
                            LastOrderTime = createdDay.AddMinutes(5),
                            IsClosed = true,
                            IsLocked = false,
                            Note = "",
                            TicketTags = "",
                            TicketLogs = "",
                            TicketStates = "",
                            TicketEntities = "",
                            WorkPeriodId = 0,
                            LastPaymentTime = createdDay.AddMinutes(6),
                            DepartmentName = "RESTAURANT",
                            RemainingAmount = 0M,
                            TotalAmount = 0M,
                            TenantId = inputdto.TenantId,
                        };

                        ticktNumber++;
                        var ticketTotalAmount = 0M;

                        ticket.Orders = new List<Order>();
                        var random = new Random();
                        var orderCount = random.Next(1, 4);

                        for (var oCount = 1; oCount <= orderCount; oCount++)
                        {
                            var menuCount = random.Next(1, 45);
                            var portionCount = random.Next(1, 10);

                            var dto = allPortions[menuCount] ?? allPortions[0];

                            var odto = new Order
                            {
                                DepartmentName = "Restaurant",
                                MenuItemId = dto.MenuItemId,
                                Location_Id = locationListDto.Id,
                                MenuItemName = allMenuItems.First(a=>a.Id == dto.MenuItemId).Name,
                                PortionName = dto.Name,
                                CalculatePrice = true,
                                IncreaseInventory = false,
                                DecreaseInventory = true,
                                CostPrice = dto.Price - .5M,
                                PortionCount = 1,
                                Note = "",
                                Locked = false,
                                IsPromotionOrder = false,
                                PromotionAmount = 0M,
                                MenuItemPortionId = dto.Id,
                                PriceTag = "",
                                Taxes = "",
                                OrderTags = "",
                                OrderStates = "",
                                Price = dto.Price,
                                Quantity = portionCount,
                                OrderNumber = orderNumber.ToString(),
                                CreatingUserName = locationListDto.Code + "_Admin",
                                OrderCreatedTime = createdDay.AddMinutes(5)
                            };
                            ticket.Orders.Add(odto);
                            ticketTotalAmount += (odto.Quantity*odto.Price);
                            orderNumber++;
                        }
                        ticket.Payments = new List<Payment>();

                        var paymentCount = random.Next(1, 4);
                        var paymentValue = ticketTotalAmount/paymentCount;
                        for (var oCount = 1; oCount <= paymentCount; oCount++)
                        {
                            var payment = new Payment
                            {
                                PaymentCreatedTime = createdDay.AddMinutes(5),
                                TerminalName = locationListDto.Code + "Server",
                                PaymentUserName = locationListDto.Code + "_Admin",
                                Amount = paymentValue,
                                TenderedAmount = paymentValue,
                                PaymentTypeId = allPaymentTypes[oCount].Id
                            };
                            ticket.Payments.Add(payment);
                        }

                        ticket.Transactions = new List<TicketTransaction>();

                        var paymentId = 1;
                        var singleOrDefault = allTransactionTypes.FirstOrDefault(a => a.Name.Equals("PAYMENT"));
                        if (singleOrDefault != null)
                        {
                            paymentId = singleOrDefault.Id;
                        }

                        var salesId = 2;
                        singleOrDefault = allTransactionTypes.FirstOrDefault(a => a.Name.Equals("SALE"));
                        var salesAmount = ticketTotalAmount - (ticketTotalAmount*.11M);
                        if (singleOrDefault != null)
                        {
                            salesId = singleOrDefault.Id;
                        }


                        var serviceId = 3;
                        var serviceAmount = ticketTotalAmount*.1M;
                        singleOrDefault = allTransactionTypes.FirstOrDefault(a => a.Name.Equals("SERVICE CHARGE"));
                        if (singleOrDefault != null)
                        {
                            serviceId = singleOrDefault.Id;
                        }

                        var discountId = 5;
                        var discountAmount = ticketTotalAmount*.01M;
                        singleOrDefault = allTransactionTypes.FirstOrDefault(a => a.Name.Equals("DISCOUNT"));
                        if (singleOrDefault != null)
                        {
                            discountId = singleOrDefault.Id;
                        }

                        var roundId = 6;
                        var roundAmount = ticketTotalAmount - (salesAmount + serviceAmount + discountAmount);
                        singleOrDefault = allTransactionTypes.SingleOrDefault(a => a.Name.Equals("ROUND"));
                        if (singleOrDefault != null)
                        {
                            roundId = singleOrDefault.Id;
                        }

                        ticket.Transactions.Add(new TicketTransaction
                        {
                            TransactionTypeId = paymentId,
                            Amount = ticketTotalAmount
                        });

                        ticket.Transactions.Add(new TicketTransaction
                        {
                            TransactionTypeId = salesId,
                            Amount = salesAmount
                        });
                        ticket.Transactions.Add(new TicketTransaction
                        {
                            TransactionTypeId = serviceId,
                            Amount = serviceAmount
                        });
                        ticket.Transactions.Add(new TicketTransaction
                        {
                            TransactionTypeId = discountId,
                            Amount = discountAmount
                        });

                        ticket.Transactions.Add(new TicketTransaction
                        {
                            TransactionTypeId = roundId,
                            Amount = roundAmount
                        });

                        ticket.TotalAmount = ticketTotalAmount;
                        try
                        {
                            var tId = _tManager.InsertAndGetId(ticket);
                        }
                        catch (Exception exception)
                        {
                            var test = exception.Message;
                        }
                    }
                }
        }
    }
}