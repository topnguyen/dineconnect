﻿#region using

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Abp.UI;
using Excel;

#endregion

namespace DineConnect.Regression.Utility
{
    public class ExcelData
    {
        private readonly string _path;
        private readonly Stream _stream;

        public ExcelData(string path, Stream stream)
        {
            _path = path;
            _stream = stream;
        }

        private IExcelDataReader GetExcelReader()
        {
            IExcelDataReader reader = null;
            try
            {
                if (_path.EndsWith(".xls"))
                {
                    reader = ExcelReaderFactory.CreateBinaryReader(_stream);
                }
                if (_path.EndsWith(".xlsx"))
                {
                    reader = ExcelReaderFactory.CreateOpenXmlReader(_stream);
                }
                return reader;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<DataRow> GetData(bool firstRowIsColumnNames = true)
        {
            try
            {
                var reader = GetExcelReader();
                var workbook = reader.AsDataSet();
                var sheets = from DataTable sheet in workbook.Tables select sheet.TableName;
                reader.IsFirstRowAsColumnNames = firstRowIsColumnNames;
                var workSheet = reader.AsDataSet().Tables[sheets.First()];
                if (workSheet == null)
                    throw new UserFriendlyException("Records Not Found");
                var rows = from DataRow a in workSheet.Rows select a;
                return rows;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}