﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intuit.Ipp.Core;

namespace SampleApp_CRUD_DotNet
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        



            ServiceContext qboContextoAuth = QBOServiceInitializer.InitializeQBOServiceContextUsingoAuth();

            //For each request use a unique RequestId so that server diff between diff requests and does not creates duplicates
            //qboContextoAuth.RequestId = Helper.GetGuid();


            //To use minorversion which depicts latest schema used by service
            //Check docs on developer.intuit.com to use latest minorversion

            qboContextoAuth.IppConfiguration.MinorVersion.Qbo = "8";

            qboContextoAuth.IppConfiguration.Logger.RequestLog.EnableRequestResponseLogging = true;
            qboContextoAuth.IppConfiguration.Logger.RequestLog.ServiceRequestLoggingLocation = @"C:\LF\IdsLogs";
           

            CustomerCRUD customerTest = new CustomerCRUD();

            //Add
            qboContextoAuth.RequestId = Helper.GetGuid();
            customerTest.CustomerBatchUsingoAuth(qboContextoAuth);



          

        

    }
    }
}