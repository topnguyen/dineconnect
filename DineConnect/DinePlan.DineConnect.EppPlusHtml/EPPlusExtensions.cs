﻿using System.Text;
using DinePlan.DineConnect.EppPlusHtml.Converters;
using DinePlan.DineConnect.EppPlusHtml.Html;
using OfficeOpenXml;

namespace DinePlan.DineConnect.EppPlusHtml
{
    public static class CodeExtensions
    {
        public static string ToHtml(this ExcelWorksheet sheet)
        {
            int lastRow = sheet.Dimension.Rows;
            int lastCol = sheet.Dimension.Columns;

            StringBuilder builder = new StringBuilder();
            builder.Append(GetHtmlHeader());
            builder.Append("<body>");
          
            HtmlElement htmlTable = new HtmlElement("table")
            {
                Styles = {["white-space"] = "nowrap"}
            };

            
            //render rows
            for (int row = 1; row <= lastRow; row++)
            {
                ExcelRow excelRow = sheet.Row(row);
                HtmlElement htmlRow = htmlTable.AddChild("tr");

                htmlRow.Styles.Update(excelRow.ToCss());

                for (int col = 1; col <= lastCol; col++)
                {
                    ExcelRange excelCell = sheet.Cells[row, col];
                    HtmlElement htmlCell = htmlRow.AddChild("td");
                    htmlCell.Content = excelCell.Text;
                    htmlCell.Styles.Update(excelCell.ToCss());
                }
            }
            builder.Append(htmlTable.ToString());
            builder.Append("</body>");
            builder.Append(GetHtmlFooter());
            return builder.ToString();
        }

        public static string GetHtmlHeader()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<html>");
            builder.Append("<meta http-equiv='Content-Type' content='text/html'; charset='UTF-8'>");
            builder.Append("<head>");
            builder.Append("<title> </title>");

            builder.Append(@"<style> 
             body  { 
              font:400 14px 'Calibri','Arial';
              padding:10px;
            }
            table  { 
                margin-top: 10px;
                border-collapse: collapse;
                width: 100%;
                margin-bottom: 10px;
            }

            table td, table th {
                border: 1px solid #ddd;
                padding: 8px;
            }

            table tr:hover {background-color: #ddd;}

            table th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #364150;
                color: white;
            }
            thead {
                display: table-header-group;
            }
            tfoot {
                display: table-row-group;
            }
            tr{
                page-break-inside: avoid;
            }
            blockquote {
              color:white;
              text-align:center;
            }

            h3{
              text-align:center;
              text-transform: capitalize;
              font-size: medium;
            }



                </style>");

            return builder.ToString();
        }

        private static string GetHtmlFooter()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("</body>");
            builder.Append("</head>");
            builder.Append("</html>");
            return builder.ToString();
        }
       
    }
}
