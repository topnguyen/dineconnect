﻿using System.Text;

namespace DinePlan.DineConnect.EppPlusHtml.Html
{
    internal interface RenderElement
    {
        void Render(StringBuilder html);
    }
}
