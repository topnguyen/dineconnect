﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace PttPromotionConsole.Process.Model
{
    [System.Xml.Serialization.XmlRoot("PromotionMasterSend_MT")]
    public class PromotionMaster
    {
        [XmlElement("Promotion")]
        public List<PromotionChild> PromotionChilds { get; set; }

        public PromotionMaster()
        {
            PromotionChilds = new List<PromotionChild>();
        }
    }
    public class PromotionChild
    {
        [XmlElement("PROMO_ID")]
        public string PromotionId { get; set; }

        [XmlElement("PROMO_NAME")]
        public string PromotionName { get; set; }

        [XmlElement("PROMO_TYPE_ID")]
        public string PromotionTypeId { get; set; }

        [XmlElement("PROMO_TYPE_NAME")]
        public string PromotionTypeName { get; set; }

        [XmlElement("PROMO_CAT_CODE")]
        public string PromotionCatCode { get; set; }

        [XmlElement("PROMO_CAT_ID")]
        public string PromotionCatName { get; set; }
        
        [XmlElement("PROMO_CAT_REMARK")]
        public string PromotionCatRemark { get; set; }
        [XmlElement("PROMO_REMARK")]
        public string PromotionRemark { get; set; }

        [XmlElement("PROMO_FROM")]
        public string PromotionFrom { get; set; }

        [XmlElement("PROMO_TO")]
        public string PromotionTo { get; set; }

        [XmlElement("DEL_FLAG")]
        public string PromotionDeleted { get; set; }

        [XmlElement("USER_CREATED")]
        public string PromotionUserCreated { get; set; }
        [XmlElement("CREATED_DATE")]
        public string PromotionCreatedDate { get; set; }

        [XmlElement("CREATED_TIME")]
        public string PromotionCreatedTime { get; set; }

        [XmlElement("USER_CHANGE")]
        public string PromotionUserChange { get; set; }


        [XmlElement("CHANGED_DATE")]
        public string PromotionChangeDate { get; set; }

        [XmlElement("CHANGED_TIME")]
        public string PromotionChangeTime { get; set; }

    }
}