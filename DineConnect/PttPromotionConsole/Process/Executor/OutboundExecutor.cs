﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Abp.Extensions;
using DinePlan.DineConnect.Connect.Discount;
using Minio;
using PttDatabaseCommon.Data;
using PttPromotionConsole.Process.Model;

namespace PttPromotionConsole.Process.Executor
{
    public class OutboundExecutor
    {
        public static string DateFormat = "yyyyMMdd";
        public static string TimeFormat = "HHmmss";

        public static string Run(MinioClient client, AppConfig config, IUnitOfWork uow, PttInboundDbContext dbContext)
        {
            var myBuilderReason = new StringBuilder();
            try
            {
                var pm = new PromotionMaster();

                foreach (var myTenant in config.TenantIds.Split(','))
                {
                    var tenantId = Convert.ToInt32(myTenant);
                    var today = DateTime.Now.Date;
                    var promotions = uow.Promotions.Find(a => a.TenantId ==tenantId && !a.IsDeleted && (!a.Active || 
                        (a.LastModificationTime!=null && a.LastModificationTime.Value>=today)));

                    if(config.ConsoleConfig == 1)
                         promotions = uow.Promotions.Find(a => a.TenantId ==tenantId && !a.Active && !a.IsDeleted);

                    if(config.ConsoleConfig == 2)
                        promotions = uow.Promotions.Find(a => a.TenantId ==tenantId && a.Active && !a.IsDeleted && (a.LastModificationTime!=null
                            && a.LastModificationTime.Value>=today));


                    foreach (var promotion in promotions)
                    {
                        var myChild = new PromotionChild
                        {
                            PromotionId = promotion.Id.ToString(),
                            PromotionName = promotion.Name,
                            PromotionTypeId = promotion.PromotionTypeId.ToString(),
                            PromotionTypeName = ((PromotionTypes) promotion.PromotionTypeId).ToString(),
                            PromotionFrom = promotion.StartDate.ToString(DateFormat),
                            PromotionTo = promotion.EndDate.ToString(DateFormat),
                            PromotionChangeDate = promotion.LastModificationTime != null
                                ? promotion.LastModificationTime.Value.ToString(DateFormat)
                                :"",
                            PromotionChangeTime = promotion.LastModificationTime != null
                                ? promotion.LastModificationTime.Value.ToString(TimeFormat)
                                : "",
                            PromotionDeleted = promotion.IsDeleted ? "X" : "",
                            PromotionCreatedDate = promotion.CreationTime.ToString(DateFormat),
                            PromotionCreatedTime = promotion.CreationTime.ToString(TimeFormat),
                            PromotionUserChange = promotion.LastModifierUserId != null
                                ? promotion.LastModifierUserId.Value.ToString()
                                : "",
                            PromotionUserCreated = promotion.CreatorUserId != null
                                ? promotion.CreatorUserId.Value.ToString()
                                : "",
                            PromotionRemark =  "",
                            PromotionCatName = "",
                            PromotionCatCode = ""
                        };

                        if (promotion.PromotionCategory != null)
                        {
                            myChild.PromotionCatCode = promotion.PromotionCategory.Code;
                            myChild.PromotionCatName = promotion.PromotionCategory.Tag;
                            myChild.PromotionCatRemark = promotion.PromotionCategory.Name;

                        }

                        if (promotion.PromotionSchedules.Any())
                        {
                            var myLastSchedule = promotion.PromotionSchedules.Last();

                            if (!string.IsNullOrEmpty(myLastSchedule.Days))
                            {
                                var splitDays = myLastSchedule.Days.Split(",");
                                var myLastDays = splitDays.Last();
                                var myString = (DayOfWeek) Convert.ToInt32(myLastDays);
                                var myStringTest = myString + "  " + myLastSchedule.StartHour + ":" +
                                                   myLastSchedule.StartMinute + "-" +
                                                   myLastSchedule.EndHour + ":" + myLastSchedule.EndMinute;
                                myChild.PromotionRemark = myStringTest;
                            }
                        }
                        pm.PromotionChilds.Add(myChild);
                    }
                }

                if (pm.PromotionChilds.Any())
                {
                    var filePath = Path.Combine(config.InitialFolder,
                        "PromotionMaster_" + DateTime.Now.ToString("yyyyMMddHHmmss",
                                               CultureInfo.CreateSpecificCulture("en-EN"))
                                           + ".XML");

                    TextWriter writer = new StreamWriter(filePath);
                    XmlSerializer serializer =
                        new XmlSerializer(typeof(PromotionMaster));

                    serializer.Serialize(writer, pm);
                    writer.Close();

                    
                    if (File.Exists(filePath))
                    {
                        MoveToBucket(client, config, new FileInfo(filePath),config.MinioPromotionBucket);
                    }
                }
            }
            catch (Exception e)
            {
                myBuilderReason.Append(e.Message);
            }

            return myBuilderReason.ToString();
        }

        public static void MoveToBucket(MinioClient client, AppConfig _config, FileInfo fileInfo, string bucketName)
        {
            var processingFile = GetSafeFileName(_config.InitialFolder, fileInfo,false);

            var nameWithoutExtenstion = Path.GetFileName(processingFile);
            if (client != null)
            {
                client.PutObjectAsync(bucketName, nameWithoutExtenstion, fileInfo.FullName).Wait();
                if (File.Exists(fileInfo.FullName))
                    File.Delete(fileInfo.FullName);
            }

        }

        private static string GetSafeFileName(string path, FileInfo fi, bool appendDate = false)
        {
            var extension = fi.Extension;
            var nameWithoutExtenstion = Path.GetFileNameWithoutExtension(fi.FullName);
            nameWithoutExtenstion = nameWithoutExtenstion + extension;
            var safePath = path;
            safePath = !path.EndsWith("\\")
                ? $"{safePath}\\{nameWithoutExtenstion}"
                : $"{safePath}{nameWithoutExtenstion}";
            return safePath;
        }
    }
}