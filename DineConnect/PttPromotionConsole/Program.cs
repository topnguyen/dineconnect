﻿using System;
using System.Configuration;
using System.IO;
using Castle.Core.Logging;
using Castle.Facilities.Logging;
using Castle.MicroKernel.Registration;
using Castle.Services.Logging.Log4netIntegration;
using Castle.Windsor;
using PttDatabaseCommon.Data;
using PttDatabaseCommon.Data.Repositories;

namespace PttPromotionConsole

{
    public class Program
    {
        private static void Main(string[] args)
        {

            var container = new WindsorContainer();
            container.AddFacility<LoggingFacility>(f => f.LogUsing<Log4netFactory>());
            var logger = container.Resolve<ILogger>();
            var appConfig = GetAppConfig(logger);
            
            if (args != null && args.Length == 1)
            {
                appConfig.ConsoleConfig = Convert.ToInt32(args[0]);
            }

            try
            {
                container.Register(
                    Component.For<CoreService>().LifestyleSingleton(),
                    Component.For<AppConfig>().Instance(appConfig).LifestyleSingleton(),
                    Component.For<IWindsorContainer>().Instance(container).LifestyleSingleton(),
                    Component.For<PttInboundDbContext>().LifestyleTransient(),
                    Component.For<IUnitOfWork>().ImplementedBy<UnitOfWork>().LifestyleTransient()
                );

                var uow = container.Resolve<CoreService>();
                uow.Start();
            }
            catch (Exception ex)
            {
                logger.Info("Job Failure");
                logger.Error($"Error=true;{ex.Message}", ex);
            }
        }

        private static AppConfig GetAppConfig(ILogger logger)
        {
            try
            {
                var appConfig = new AppConfig
                {
                    InitialFolder = ConfigurationManager.AppSettings["InitialFolder"],
                    MinioServerUrl = ConfigurationManager.AppSettings["MinioServerUrl"],
                    MinioApiKey = ConfigurationManager.AppSettings["MinioApiKey"],
                    MinioApiSecret = ConfigurationManager.AppSettings["MinioApiSecret"],
                    MinioPromotionBucket = ConfigurationManager.AppSettings["MinioPromotionBucket"]
                };

                var tenant = ConfigurationManager.AppSettings["TenantIds"];
                if (string.IsNullOrEmpty(tenant))
                {
                    tenant = "1";
                }
                appConfig.TenantIds = tenant;

                ValidateFolder(logger, appConfig.InitialFolder, "InitialFolder - Available");
               

                return appConfig;
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                throw;
            }
        }


        private static void ValidateFolder(ILogger logger, string path, string folderName)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            logger.Info($"{folderName}'s information was checked successfully.");
        }
    }
}