﻿using System;
using System.Threading.Tasks;
using Minio;
using Minio.Exceptions;

namespace DineConnect.Minio
{
    internal class Program
    {
        private static void Main(string[] args)
        {
       
            var minio = new MinioClient("localhost:8489",
                "CMI4M3I1QZTPT8MLOJNT",
                "pEHMtpFAHvaCV8XWxp1g7Pjz+UI0bhyrnuvYQdAO"
                );

            var getListBucketsTask = minio.ListBucketsAsync();

            foreach (var bucket in getListBucketsTask.Result.Buckets)
            {
                Console.Out.WriteLine(bucket.Name + " " + bucket.CreationDateDateTime);
            }

            Run(minio).Wait();

            Console.ReadKey();
        }
        private async static Task Run(MinioClient minio)
        {
            var bucketName = "test";
            var objectName = "public/san";
            var filePath = @"C:\_Downloads\san.jpg";

            try
            {
                // Make a bucket on the server, if not already present.
                bool found = await minio.BucketExistsAsync(bucketName);
                await minio.PutObjectAsync(bucketName, objectName, filePath);
                Console.Out.WriteLine("Successfully uploaded " + objectName);
            }
            catch (MinioException e)
            {
                Console.WriteLine("File Upload Error: {0}", e.Message);
            }
        }
    }
}