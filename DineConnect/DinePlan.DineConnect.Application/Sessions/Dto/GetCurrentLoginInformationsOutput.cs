﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using System;

namespace DinePlan.DineConnect.Sessions.Dto
{
    public class GetCurrentLoginInformationsOutput : IOutputDto
    {
        public UserLoginInfoDto User { get; set; }

        public TenantLoginInfoDto Tenant { get; set; }

        public LocationListDto Location { get; set; }

        public PagedResultDto<LocationListDto> LocationsBasedOnUser { get; set; }
        public DateTime? LocationsBasedOnUserLastUpdate { get; set; }

        public ListResultDto<LocationListDto> LocationList { get; set; }
        public DateTime? LocationListLastUpdate { get; set; }
        public int Housedecimals { get; set; }
        public int Connectdecimals { get; set; }

        public AccountDateDto AccountDateDto { get; set; }
        public int? SessionId { get; set; }

    }
}