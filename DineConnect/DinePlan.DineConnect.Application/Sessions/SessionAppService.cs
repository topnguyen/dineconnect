﻿using Abp.Auditing;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Configuration;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.DayClose;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Session;
using DinePlan.DineConnect.Sessions.Dto;
using System;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Sessions
{
    [AbpAuthorize]
    public class SessionAppService : DineConnectAppServiceBase, ISessionAppService
    {
        private readonly ConnectSession _mySession;
        private readonly SettingManager _settingManager;
        private readonly IDayCloseAppService _dayCloseAppService;
        private readonly ILocationAppService _locationAppService;

        public SessionAppService(ConnectSession mySession, SettingManager settingManager, IDayCloseAppService dayCloseAppService, ILocationAppService locationAppService)
        {
            _mySession = mySession;
            _settingManager = settingManager;
            _dayCloseAppService = dayCloseAppService;
            _locationAppService = locationAppService;
        }

        [DisableAuditing]
        public async Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations()
        {
            var output = new GetCurrentLoginInformationsOutput
            {
                User = (await GetCurrentUserAsync()).MapTo<UserLoginInfoDto>()
            };

            if (output.User.Id > 0)
            {
                output.LocationsBasedOnUser = await _locationAppService.GetLocationBasedOnUser(new GetLocationInputBasedOnUser { UserId = output.User.Id });
                output.LocationsBasedOnUserLastUpdate = DateTime.Now;
            }

            output.LocationList = await _locationAppService.GetLocations();
            output.LocationListLastUpdate = DateTime.Now;

            bool mutlipleUomAllowed = PermissionChecker.IsGranted("Pages.Tenant.House.Transaction.MultipleUOMAllowed");
            output.User.MultipleUomEntryAllowed = mutlipleUomAllowed;
            output.User.LocationRefId = GetCurrentUserLocationAsync();
            output.Location = (await GetLocationInfo(output.User.LocationRefId)).MapTo<LocationListDto>();

            if (output.Location != null)
            {
                output.User.OrganisationRefId = output.Location.CompanyRefId;
                output.AccountDateDto = await _dayCloseAppService.GetDayCloseStatus(new Abp.Application.Services.Dto.IdInput
                {
                    Id = output.Location.Id
                });
            }

            if (AbpSession.TenantId.HasValue)
            {
                output.Tenant = (await GetCurrentTenantAsync()).MapTo<TenantLoginInfoDto>();
            }


            int decimals = await _settingManager.GetSettingValueAsync<int>(AppSettings.HouseSettings.Decimals);
            output.Housedecimals = decimals;

            decimals = await _settingManager.GetSettingValueAsync<int>(AppSettings.ConnectSettings.Decimals);
            output.Connectdecimals = decimals;

            return output;
        }

        public void SetOrganization(int orga)
        {
            var claimsPrincipal = Thread.CurrentPrincipal as ClaimsPrincipal;
        }
    }
}