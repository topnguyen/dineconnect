﻿using System;
using System.Reflection;
using Abp.AutoMapper;
using Abp.Modules;
using Abp.Threading.BackgroundWorkers;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Job;
using DinePlan.DineConnect.Job.Connect;

namespace DinePlan.DineConnect
{
    /// <summary>
    /// Application layer module of the application.
    /// </summary>
    [DependsOn(typeof(DineConnectCoreModule), typeof(AbpAutoMapperModule))]
    public class DineConnectApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            //Adding authorization providers
            Configuration.Auditing.IsEnabled = false;
            Configuration.Authorization.Providers.Add<AppAuthorizationProvider>();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());

            //Custom DTO auto-mappings
            CustomDtoMapper.CreateMappings();
        }

        public override void PostInitialize()
        {
            var workManager = IocManager.Resolve<IBackgroundWorkerManager>();
            workManager.Add(IocManager.Resolve<QuotaResetJob>());
        }
    }
}
