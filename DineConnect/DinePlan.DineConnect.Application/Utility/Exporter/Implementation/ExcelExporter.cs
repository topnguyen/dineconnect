﻿using Abp.UI;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using System.IO;

namespace DinePlan.DineConnect.Utility.Exporter.Implementation
{
    public class ExcelExporter : FileExporterBase, IExcelExporter
    {
        public FileDto ExportToFile(BaseExportObject document, string name)
        {
            try
            {
                if (document != null && document.ColumnNames != null)
                {
                    var file = new FileDto(name + ".xlsx",
                        MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

                    using (var package = new ExcelPackage())
                    {
                        var worksheet = package.Workbook.Worksheets.Add(name);
                        var row = 1;
                        var col = 1;

                        if (document.ColumnDisplayNames == null)
                            document.ColumnDisplayNames = document.ColumnNames;

                        foreach (var cn in document.ColumnDisplayNames)
                        {
                            worksheet.Cells[row, col].Value = L(cn);
                            worksheet.Column(col).AutoFit();
                            worksheet.Cells[1, col].Style.Font.Bold = true;
                            col++;
                        }
                        col = 1;
                        row++;
                        string OldColumnValue = "";
                        foreach (var exportObject in document.ExportObject)
                        {
                            if (!string.IsNullOrEmpty(document.BreakOnLineColumn))
                            {
                                var value = exportObject.GetType().GetProperty(document.BreakOnLineColumn).GetValue(exportObject, null);
                                if (string.IsNullOrEmpty(OldColumnValue) == false && !OldColumnValue.Equals(value.ToString()))
                                    row++;
                            }

                            foreach (var propName in document.ColumnNames)
                            {
                                var value = exportObject.GetType().GetProperty(propName).GetValue(exportObject, null);
                                worksheet.Cells[row, col].Value = value;
                                col++;
                                if (propName.Equals(document.BreakOnLineColumn))
                                {
                                    OldColumnValue = value.ToString();
                                }
                            }
                            col = 1;
                            row++;
                        }

                        col = 1;

                        foreach (var cn in document.ColumnNames)
                        {
                            worksheet.Column(col).AutoFit();
                            col++;
                        }

                        var filePath = Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken);
                        package.SaveAs(new FileInfo(filePath));
                    }
                    return file;
                }
                return null;
            }
            catch (System.Exception ex)
            {
                throw new UserFriendlyException(ex.Message + " " + ex.InnerException);
            }
        }
    }
}