﻿using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Utility.Exporter
{
    public interface IExcelExporter
    {
        FileDto ExportToFile(BaseExportObject exportObject, string name);
    }
}
