﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Utility
{
    public class BaseExportObject
    {
        public string[] ColumnNames { get; set; }
        public string[] ColumnDisplayNames { get; set; }

        public string BreakOnLineColumn { get; set; }
        public IEnumerable<FullAuditedEntityDto> ExportObject { get; set; }
    }
}