﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Addons.Qoyod.Dto
{
    public class CreateQoyodContactDto : IRestInputDto
    {
        public CreateQoyodContactDetailDto contact { get; set; }
    }
    public class CreateQoyodContactOutputDto : IRestOutputDto
    {
        public CreateQoyodContactDetailOutputDto contact { get; set; }
    }
    public class CreateQoyodContactDetailOutputDto
    {
        public int id { get; set; }
        public string name { get; set; }
        public string organization { get; set; }
        public string phone_number { get; set; }
        public string email { get; set; }

    }
    public class CreateQoyodContactDetailDto
    {
        public string name { get; set; }
        public string organization { get; set; }
        public string phone_number { get; set; }
        public string email { get; set; }

    }

    public class SyncOutput
    {
        public int CreateCount { get; set; }
        public int UpdateCount { get; set; }

        public SyncOutput()
        {
            CreateCount = 0;
            UpdateCount = 0;
        }
    }


    public class QoyodDebitAmount
    {
        public int account_id { get; set; }
        public decimal amount { get; set; }
        public string comment { get; set; }
    }

    public class QoyodCreditAmount
    {
        public int account_id { get; set; }
        public decimal amount { get; set; }
        public string comment { get; set; }
    }

    public class QoyodJournalEntry
    {
        public int Id { get; set; }
        public string description { get; set; }
        public string date { get; set; }
        public List<QoyodDebitAmount> debit_amounts { get; set; }
        public List<QoyodCreditAmount> credit_amounts { get; set; }

        public QoyodJournalEntry()
        {
            description = "";
            date = DateTime.Now.ToString("yyyy-MM-dd");
            debit_amounts = new List<QoyodDebitAmount>();
            credit_amounts = new List<QoyodCreditAmount>();

        }
    }



    public class QoyodJournalRootObject : IRestInputDto
    {
        public QoyodJournalEntry journal_entry { get; set; }

        public QoyodJournalRootObject()
        {
            journal_entry = new QoyodJournalEntry();
        }

    }

    public class QoyodJournalOutputDto : IRestOutputDto
    {
        public int id { get; set; }
        public string date { get; set; }
        public string description { get; set; }
        public string total_debit { get; set; }
        public string total_credit { get; set; }
        public List<QoyodDebitAmount> debit_amounts { get; set; }
        public List<QoyodCreditAmount> credit_amounts { get; set; }
    }
}
