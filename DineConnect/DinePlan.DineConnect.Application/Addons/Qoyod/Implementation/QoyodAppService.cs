﻿using System;
using System.Linq.Dynamic;
using System.Net;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Addon;
using DinePlan.DineConnect.Addons.Dto;
using DinePlan.DineConnect.Addons.Qoyod.Dto;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Period;
using DinePlan.DineConnect.House;
using DinePlan.DineConnect.Rest;
using Newtonsoft.Json;
using RestSharp;

namespace DinePlan.DineConnect.Addons.Qoyod.Implementation
{
    public class QoyodAppService : DineConnectAppServiceBase, IQoyodAppService
    {
        public static string Url = "https://www.qoyod.com/api/2.0/";
        private readonly IRepository<ConnectAddOn> _addOn;
        private readonly IRepository<Location> _lRepo;
        private readonly IRepository<Supplier> _sRepo;
        private readonly IRepository<Customer> _cRepo;
        private readonly IRepository<WorkPeriod> _wpRepo;

        public QoyodAppService(IRepository<ConnectAddOn> addOn, 
            IRepository<Supplier> supplier, IRepository<Customer> customer, IRepository<WorkPeriod> wpRepo,
            IRepository<Location> location)
        {
            _addOn = addOn;
            _sRepo = supplier;
            _lRepo = location;
            _cRepo = customer;
            _wpRepo = wpRepo;
        }

        public string GetOrUpdateToken(string accessToken)
        {
            if (string.IsNullOrEmpty(accessToken))
            {
                var qbc = _addOn.FirstOrDefault(a => a.Name.Equals("Qoyod"));
                if (qbc != null)
                {
                    return qbc.Settings;
                }
                return "";
            }
            else
            {
                var qbc = MyQuyod(accessToken);
                return qbc.Settings;
            }
        }

        public async Task<SyncOutput> SyncSuppliers()
        {
            SyncOutput output = new SyncOutput();
            var myQb = MyQuyod("");
            if (myQb != null)
            {
                foreach (var supplier in await _sRepo.GetAllListAsync(a => a.Id > 0))
                {
                    var addon = new AddOnOutput();
                    var add = false;
                    if (!string.IsNullOrEmpty(supplier.AddOn))
                    {
                        addon = JsonConvert.DeserializeObject<AddOnOutput>(supplier.AddOn);
                        if (string.IsNullOrEmpty(addon.Qoyod))
                        {
                            add = true;
                        }
                    }
                    else
                    {
                        add = true;
                    }
                    if (add)
                    {
                        var supp = CreateSupplier(supplier, myQb);
                        if (supp > 0)
                        {
                            addon.Qoyod = supp.ToString();
                            supplier.AddOn = JsonConvert.SerializeObject(addon);
                            await _sRepo.UpdateAsync(supplier);
                            output.CreateCount++;
                        }
                    }
                    else
                    {
                        var outputU = UpdateSupplier(supplier, myQb, addon);
                        if (outputU > 0)
                            output.UpdateCount++;
                    }
                }
            }

            return output;
        }
        public async Task<SyncOutput> SyncCustomers()
        {
            SyncOutput output = new SyncOutput();
            var myQb = MyQuyod("");
            if (myQb != null)
            {
                foreach (var supplier in await _cRepo.GetAllListAsync(a => a.Id > 0))
                {
                    var addon = new AddOnOutput();
                    var add = false;
                    if (!string.IsNullOrEmpty(supplier.AddOn))
                    {
                        addon = JsonConvert.DeserializeObject<AddOnOutput>(supplier.AddOn);
                        if (string.IsNullOrEmpty(addon.Qoyod))
                        {
                            add = true;
                        }
                    }
                    else
                    {
                        add = true;
                    }
                    if (add)
                    {
                        var supp = CreateCustomer(supplier, myQb);
                        if (supp > 0)
                        {
                            addon.Qoyod = supp.ToString();
                            supplier.AddOn = JsonConvert.SerializeObject(addon);
                            await _cRepo.UpdateAsync(supplier);
                            output.CreateCount++;
                        }
                    }
                    else
                    {
                        var outputU = UpdateCustomer(supplier, myQb, addon);
                        if(outputU>0)
                            output.UpdateCount++;
                    }
                }
            }

            return output;
        }
        public async Task<SyncOutput> SyncLocations()
        {
            SyncOutput output = new SyncOutput();
            var myQuyod = MyQuyod("");
            if (myQuyod != null)
            {

                foreach (var loc in await _lRepo.GetAllListAsync(a => a.Id > 0))
                {
                    var addon = new AddOnOutput();
                    var add = false;
                    if (!string.IsNullOrEmpty(loc.AddOn))
                    {
                        addon = JsonConvert.DeserializeObject<AddOnOutput>(loc.AddOn);
                        if (string.IsNullOrEmpty(addon.Qoyod))
                        {
                            add = true;
                        }
                    }
                    else
                    {
                        add = true;
                    }
                    if (add)
                    {
                        //var debit = crud.CreateAccount(context, loc, loc.Currency, AccountTypeEnum.OtherAsset,
                        //    AccountClassificationEnum.Asset);
                        //var credit = crud.CreateAccount(context, loc, loc.Currency, AccountTypeEnum.Income,
                        //    AccountClassificationEnum.Revenue);

                        addon.Qoyod = "1,2";
                        loc.AddOn = JsonConvert.SerializeObject(addon);
                        output.UpdateCount++;

                        await _lRepo.UpdateAsync(loc);
                    }
                }
            }

            return output;
        }

        public void SyncSales(int workPeriodId)
        {
            var myQb = MyQuyod("");
            if (myQb == null)
                return;

            if (workPeriodId > 0)
            {
                var workPeriod = _wpRepo.Get(workPeriodId);

                if (workPeriod!=null && workPeriod.LocationId>0 && workPeriod.TotalSales>0M)
                {
                    if (!string.IsNullOrEmpty(workPeriod.AddOn))
                    {
                        var addOn = JsonConvert.DeserializeObject<AddOnOutput>(workPeriod.AddOn);
                        if (addOn != null)
                        {
                            if (string.IsNullOrEmpty(addOn.Qoyod))
                            {
                                return;
                            }
                        }
                    }
                    var location = _lRepo.Get(workPeriod.LocationId);
                    if (!string.IsNullOrEmpty(location?.AddOn))
                    {
                        var addOn = JsonConvert.DeserializeObject<AddOnOutput>(location.AddOn);
                        if (!string.IsNullOrEmpty(addOn?.Qoyod))
                        {
                            var ouputCode = addOn.Qoyod.Split(',');
                            if (ouputCode.Length > 1)
                            {
                                var debitCode = ouputCode[0];
                                var creditCode = ouputCode[1];
                                var quyodJo = new QoyodJournalRootObject
                                {
                                    journal_entry = new QoyodJournalEntry
                                    {
                                        credit_amounts = new System.Collections.Generic.List<Dto.QoyodCreditAmount>()
                                        {
                                            new QoyodCreditAmount
                                            {
                                                account_id = Convert.ToInt32(creditCode),
                                                amount = workPeriod.TotalSales,
                                                comment =
                                                    location.Name + "-Sales-" +
                                                    workPeriod.StartTime.ToString("yyyy-MM-dd")
                                            }
                                        },
                                        date = workPeriod.StartTime.ToString("yyyy-MM-dd"),
                                        debit_amounts = new System.Collections.Generic.List<QoyodDebitAmount>()
                                        {
                                            new QoyodDebitAmount
                                            {
                                                account_id = Convert.ToInt32(debitCode),
                                                amount = workPeriod.TotalSales,
                                                comment =
                                                    location.Name + "-Sales-" +
                                                    workPeriod.StartTime.ToString("yyyy-MM-dd")
                                            }
                                        },
                                        description =
                                            location.Name + "-Sales-" + workPeriod.StartTime.ToString("yyyy-MM-dd")
                                    }
                                };
                                var response = GetResponse("journal_entries", myQb.Settings, Method.POST, quyodJo);
                                if (response != null)
                                {
                                    var output = JsonConvert.DeserializeObject<QoyodJournalOutputDto>(response.ToString());
                                    if (output != null && output.id>0)
                                    {
                                        AddOnOutput wpAdd = new AddOnOutput {Qoyod = output.id.ToString()};
                                        workPeriod.AddOn = JsonConvert.SerializeObject(wpAdd);
                                        _wpRepo.Update(workPeriod);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private int UpdateCustomer(Customer customer, ConnectAddOn myQb, AddOnOutput addon)
        {
            CreateQoyodContactDto mysupplier = new CreateQoyodContactDto
            {
                contact = new CreateQoyodContactDetailDto
                {
                    name = customer.CustomerName,
                    organization = customer.CustomerName,
                    phone_number = customer.PhoneNumber ?? "",
                    email = customer.Email ?? ""
                }
            };
            var response = GetResponse("customers/" + addon.Qoyod, myQb.Settings, Method.PUT, mysupplier);
            if (response != null)
            {
                CreateQoyodContactOutputDto output =
                    JsonConvert.DeserializeObject<CreateQoyodContactOutputDto>(response.ToString());

                if (output != null)
                {
                    return output.contact.id;
                }
            }

            return 0;
        }

        private int CreateCustomer(Customer customer, ConnectAddOn myQb)
        {
            CreateQoyodContactDto mysupplier = new CreateQoyodContactDto
            {
                contact = new CreateQoyodContactDetailDto
                {
                    name = customer.CustomerName,
                    organization = customer.CustomerName,
                    phone_number = customer.PhoneNumber ?? "",
                    email = customer.Email ?? ""
                }
            };
            var response = GetResponse("customers", myQb.Settings, Method.POST, mysupplier);
            if (response != null)
            {
                CreateQoyodContactOutputDto output =
                    JsonConvert.DeserializeObject<CreateQoyodContactOutputDto>(response.ToString());

                if (output != null)
                {
                    return output.contact.id;
                }
            }

            return 0;
        }

        private int UpdateSupplier(Supplier supplier, ConnectAddOn myQb, AddOnOutput addon)
        {
            CreateQoyodContactDto mysupplier = new CreateQoyodContactDto
            {
                contact = new CreateQoyodContactDetailDto
                {
                    name = supplier.SupplierCode ?? supplier.SupplierName,
                    organization = supplier.SupplierName,
                    phone_number = supplier.PhoneNumber1 ?? "121",
                    email = supplier.Email ?? ""
                }
            };
            var response = GetResponse("vendors/"+ addon.Qoyod, myQb.Settings, Method.PUT, mysupplier);
            if (response != null)
            {
                CreateQoyodContactOutputDto output =
                    JsonConvert.DeserializeObject<CreateQoyodContactOutputDto>(response.ToString());

                if (output != null)
                {
                    return output.contact.id;
                }
            }

            return 0;
        }

        private int CreateSupplier(Supplier supplier, ConnectAddOn myQb)
        {
            CreateQoyodContactDto mysupplier = new CreateQoyodContactDto
            {
                contact = new CreateQoyodContactDetailDto
                {
                    name = supplier.SupplierCode ?? supplier.SupplierName,
                    organization = supplier.SupplierName,
                    phone_number = supplier.PhoneNumber1 ?? "",
                    email = supplier.Email ?? ""
                }
            };
            var response = GetResponse("vendors", myQb.Settings, Method.POST, mysupplier);
            if (response!=null)
            {
                CreateQoyodContactOutputDto output =
                    JsonConvert.DeserializeObject<CreateQoyodContactOutputDto>(response.ToString());

                if (output != null)
                {
                    return output.contact.id;
                }
            }

            return 0;

        }


        private ConnectAddOn MyQuyod(string accessToken)
        {
            var qbc = _addOn.FirstOrDefault(a => a.Name.Equals("Qoyod"));
            if (qbc == null)
            {
                if (!string.IsNullOrEmpty(accessToken))
                {
                    qbc = new ConnectAddOn
                    {
                        Name = "Qoyod",
                        Settings = accessToken
                    };
                    _addOn.InsertAndGetId(qbc);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(accessToken))
                {
                    qbc.Settings = accessToken;
                    _addOn.Update(qbc);
                }
            }
            return qbc;
        }

        public object GetResponse(string method, string key,  Method rmethod, IRestInputDto input)
        {
            try
            {
                var client = new RestClient(Url);
                var request = new RestRequest(method, rmethod);
                request.AddHeader("API-KEY", $"{key}");
                request.AddHeader("Content-Type", "application/json");
                if (input != null)
                {
                    var body = JsonConvert.SerializeObject(input);
                    request.AddParameter("application/json", body, ParameterType.RequestBody);
                }
                var response = (RestResponse) client.Execute(request);
                if (ValidateRestResponse(response))
                {
                    return response.Content;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return null;
        }

        private bool ValidateRestResponse(RestResponse response)
        {
            if (response.StatusCode == HttpStatusCode.OK 
                || response.StatusCode == HttpStatusCode.Created)
            {
                return true;
            }
            return false;
        }
    }
}