﻿using System.Threading.Tasks;
using Abp.Application.Services;
using DinePlan.DineConnect.Addons.Qoyod.Dto;

namespace DinePlan.DineConnect.Addons.Qoyod
{
    public interface IQoyodAppService : IApplicationService
    {
        string GetOrUpdateToken(string accessToken);
        Task<SyncOutput> SyncSuppliers();
        Task<SyncOutput> SyncCustomers();
        Task<SyncOutput> SyncLocations();

        void SyncSales(int workPeriodId);
    }
}
