﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Addons.QuickBooks
{
    public class QuickBooksToken
    {
        public string id_token { get; set; }
        public string access_token { get; set; }
        public double expires_in { get; set; }
        public string refresh_token { get; set; }
        public string realm_id { get; set; }
        public DateTime ExpiresOn { get; set; }

        public string Scope { get; set; }

        [JsonIgnore]
        public bool IsExpired
        {
            get
            {
                return DateTime.UtcNow >= ExpiresOn;
            }
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
