﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Addons.QuickBooks
{
    public interface IQuickBooksAppService : IApplicationService
    {
        QuickBooksToken RefreshTokenAndSave(QuickBooksToken token);
        Task<bool> KeepTokenComingFromGateway(QuickBooksToken token);
        string GetLoginUrl();
        bool RemoveToken();
        bool IsTokenExists();
        Task<List<ComboboxItemDto>> GetAccounts();
        Task<List<ComboboxItemDto>> GetCustomers();
        Task SyncItem(int materialId);

    }
}
