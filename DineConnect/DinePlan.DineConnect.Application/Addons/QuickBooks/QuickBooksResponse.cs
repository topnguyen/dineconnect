﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Addons.QuickBooks
{
    public class QuickBooksResponse<T>
    {
        public string Id { get; set; }
        public string Status { get; set; }
        public string ProviderName { get; set; }
        public string DateTimeUTC { get; set; }
    }


    public class QuickBooksQueryResponse<T> : QuickBooksResponse<T>
    {
        public T Items { get; set; }
    }


    public class ItemResponse<T> : QuickBooksResponse<T>
    {
        public T Items { get; set; }

    }

    public class ContactResponse<T> : QuickBooksResponse<T>
    {
        public T Contacts { get; set; }

    }

    public class AccountResponse<T> : QuickBooksResponse<T>
    {
        public T Accounts { get; set; }

    }

    public class InvoiceResponse<T> : QuickBooksResponse<T>
    {
        public T Invoices { get; set; }

    }
}
