﻿using Newtonsoft.Json;

namespace DinePlan.DineConnect.Addons.QuickBooks.Model
{
    /// <summary>
    /// https://developer.intuit.com/app/developer/qbo/docs/api/accounting/most-commonly-used/account
    /// </summary>
    public class QuickBooksAccount
    {
        [JsonProperty("Id")]
        public string Id { get; set; }
        [JsonProperty("FullyQualifiedName")]
        public string FullyQualifiedName { get; set; }
        [JsonProperty("domain")]
        public string Domain { get; set; }
        [JsonProperty("Name")]
        public string Name { get; set; }
        [JsonProperty("Classification")]
        public string Classification { get; set; }
        [JsonProperty("AccountSubType")]
        public string AccountSubType { get; set; }

    }
}
