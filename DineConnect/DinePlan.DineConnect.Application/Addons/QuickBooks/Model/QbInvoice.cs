﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Addons.QuickBooks.Model
{
    /// <summary>
    /// https://developer.intuit.com/app/developer/qbo/docs/api/accounting/most-commonly-used/invoice
    /// </summary>
    public class QbInvoice
    {
        [JsonProperty("TxnDate")]
        public string TxnDate { get; set; }
        [JsonProperty("domain")]
        public string Domain { get; set; }
        [JsonProperty("PrintStatus")]
        public string PrintStatus { get; set; }
        [JsonProperty("SalesTermRef")]
        public QbSalesTermRef SalesTermRef { get; set; }
        [JsonProperty("TotalAmt")]
        public string TotalAmount { get; set; }
        [JsonProperty("Line")]
        public List<QbInvoiceLineItem> LineItems { get; set; }
        [JsonProperty("DueDate")]
        public string DueDate { get; set; }
        [JsonProperty("ApplyTaxAfterDiscount")]
        public bool ApplyTaxAfterDiscount { get; set; }
        [JsonProperty("Deposit")]
        public string Deposit { get; set; }
        [JsonProperty("Balance")]
        public string Balance { get; set; }

    }


    public class QbItemRef
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("valu")]
        public string Value { get; set; }
    }

    public class QbSalesTermRef
    {
        public string Value { get; set; }
    }

    public class QbTaxCodeRef
    {
        [JsonProperty("value")]
        public string Value { get; set; }
    }

    public class QbSalesItemLineDetail
    {
        [JsonProperty("TaxCodeRef")]
        public QbTaxCodeRef TaxCodeRef { get; set; }
        [JsonProperty("Qty")]
        public string Quantity { get; set; }
        [JsonProperty("UnitPrice")]
        public string UnitPrice { get; set; }
        [JsonProperty("ItemRef")]
        public QbItemRef ItemRef { get; set; }
    }

    public class QbInvoiceLineItem
    {
        [JsonProperty("Description")]
        public string Description { get; set; }
        [JsonProperty("DetailType")]
        public string DetailType { get; set; }
        [JsonProperty("SalesItemLineDetail")]
        public QbSalesItemLineDetail LineDetail { get; set; }
        [JsonProperty("LineNum")]
        public string LineNum { get; set; }
        [JsonProperty("Amount")]
        public string Amount { get; set; }
        [JsonProperty("Id")]
        public string Id { get; set; }
    }

   
}
