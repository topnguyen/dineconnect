﻿using Newtonsoft.Json;
namespace DinePlan.DineConnect.Addons.QuickBooks.Model
{
    /// <summary>
    /// https://developer.intuit.com/app/developer/qbo/docs/api/accounting/most-commonly-used/customer
    /// </summary>
    public class QbCustomer
    {
        [JsonProperty("Id")]
        public string Id { get; set; }
        [JsonProperty("domain")]
        public string Domain { get; set; }
        [JsonProperty("GivenName")]
        public string GivenName { get; set; }
        [JsonProperty("DisplayName")]
        public string DisplayName { get; set; }
        [JsonProperty("CompanyName")]
        public string CompanyName { get; set; }
        [JsonProperty("PrimaryEmailAddr")]
        public QbCustomerEmail Email { get; set; }
        [JsonProperty("PrimaryPhone")]
        public QbCustomerPhone Phone { get; set; }

    }

    public class QbCustomerEmail
    {
        [JsonProperty("Address")]
        public string Address { get; set; }
    }

    public class QbCustomerPhone
    {
        [JsonProperty("FreeFormNumber")]
        public string FreeFormNumber { get; set; }
    }

   
}
