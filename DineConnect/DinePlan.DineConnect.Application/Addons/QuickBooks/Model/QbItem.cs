﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Addons.QuickBooks.Model
{

    /// <summary>
    /// https://developer.intuit.com/app/developer/qbo/docs/api/accounting/most-commonly-used/item
    /// </summary>
    public class QbItem
    {
        [JsonProperty("Type")]
        public string Type { get; set; }
        [JsonProperty("Name")]
        public string Name { get; set; }
        [JsonProperty("TrackQtyOnHand")]
        public bool TrackQtyOnHand { get; set; }
        [JsonProperty("QtyOnHand")]
        public string QtyOnHand { get; set; }
        [JsonProperty("InvStartDate")]
        public string InvStartDate { get; set; }
        [JsonProperty("AssetAccountRef")]
        public QbItemAccountRef AssetAccountRef { get; set; }
        [JsonProperty("IncomeAccountRef")]
        public QbItemAccountRef IncomeAccountRef { get; set; }
        [JsonProperty("ExpenseAccountRef")]
        public QbItemAccountRef ExpenseAccountRef { get; set; }
    }


    public class QbItemAccountRef
    {
        public static QbItemAccountRef FromValue(string value)
        {
            return new QbItemAccountRef() { value = value };
        }

        [JsonProperty("value")]
        public string value { get; set; }
    }




}
