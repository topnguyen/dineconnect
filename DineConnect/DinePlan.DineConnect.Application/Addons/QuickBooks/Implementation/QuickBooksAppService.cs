﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using DinePlan.DineConnect.Addon;
using DinePlan.DineConnect.Addons.Dto;
using DinePlan.DineConnect.Addons.QuickBooks.Crud;
using DinePlan.DineConnect.House;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Addons.QuickBooks.Implementation
{
    public class QuickBooksAppService : IQuickBooksAppService
    {
        private const string AddOnName = "QuickBooks";
        private readonly string _clientId = ConfigurationManager.AppSettings["QuickBooksClientId"];
        private readonly string _clientSecret = ConfigurationManager.AppSettings["QuickBooksClientSecret"];
        private readonly IAbpSession _session;
        private readonly IRepository<ConnectAddOn> _addOn;
        private readonly string _dineConnectApplicationUrl = ConfigurationManager.AppSettings["DineConnectUrl"];
        private readonly IRepository<Material> _mRepo;


        public QuickBooksAppService(IAbpSession session,
            IRepository<ConnectAddOn> addonRepo,
            IRepository<Material> mRepo)
        {
            _mRepo = mRepo;
            _addOn = addonRepo;
            _session = session;                
        }

        private string GetGatewayUrl()
        {
            return ConfigurationManager.AppSettings["DineConnectGatewayUrl"];
        }

        public string GetLoginUrl()
        {
            var gatewayUrl = this.GetGatewayUrl();
            return
               $"{gatewayUrl}/dineconnect?applicationType=quickbooks&dineConnectTenant={_session.TenantId}&applicationUrl={_dineConnectApplicationUrl}";
        }

        public bool IsTokenExists()
        {
            var qbAddon = _addOn.FirstOrDefault(a => a.Name.Equals(AddOnName));
            if (qbAddon != null)
            {
                var settings = qbAddon.Settings;
                var token = JsonConvert.DeserializeObject<QuickBooksToken>(settings);
                return token != null;
            }

            return false;
        }

        public bool RemoveToken()
        {
            var qbAddon = _addOn.FirstOrDefault(a => a.Name.Equals(AddOnName));
            if (qbAddon != null) _addOn.Delete(qbAddon);

            return true;
        }

        public async Task<bool> KeepTokenComingFromGateway(QuickBooksToken token)
        {
            token.ExpiresOn = DateTime.UtcNow.AddSeconds(token.expires_in);

            var qbAddon = await _addOn.FirstOrDefaultAsync(a => a.Name.Equals(AddOnName));
            if (qbAddon == null)
            {
                qbAddon = new ConnectAddOn
                {
                    TenantId = _session.GetTenantId(),
                    CreationTime = DateTime.UtcNow,
                    Name = AddOnName,
                    Settings = token.ToJson()
                };
                await _addOn.InsertAndGetIdAsync(qbAddon);
                return true;
            }

            return false;
        }


        private QuickBooksToken GetQuickBooksTokenForTenant()
        {
            var qbAddon = _addOn.FirstOrDefault(a => a.Name.Equals(AddOnName));
            if (qbAddon != null)
            {
                var settings = qbAddon.Settings;
                var token = JsonConvert.DeserializeObject<QuickBooksToken>(settings);

                //Refresh if expired
                if (token.IsExpired) token = RefreshTokenAndSave(token);

                return token;
            }

            return null;
        }

        public QuickBooksToken RefreshTokenAndSave(QuickBooksToken token)
        {
            var client = new RestClient("https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer")
            {
                Authenticator = new HttpBasicAuthenticator(_clientId, _clientSecret)
            };

            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddParameter("grant_type", "refresh_token");
            request.AddParameter("refresh_token", token.refresh_token);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var response = (RestResponse)client.Execute(request);

            var qbAddon = _addOn.FirstOrDefault(a => a.Name.Equals(AddOnName));

            var tokenRefreshed = JsonConvert.DeserializeObject<QuickBooksToken>(response.Content);
            token.ExpiresOn = DateTime.UtcNow.AddSeconds(tokenRefreshed.expires_in);
            token.access_token = tokenRefreshed.access_token;
            token.refresh_token = tokenRefreshed.refresh_token;

            qbAddon.Settings = token.ToJson();
            _addOn.Update(qbAddon);

            return token;
        }

        #region Accounts 
        public async Task<List<ComboboxItemDto>> GetAccounts()
        {
            var output = new List<ComboboxItemDto>();

            var token = this.GetQuickBooksTokenForTenant();

            if (token != null)
                foreach (var accTye in AccountQuickBooksCrud.GetAll(token))
                    output.Add(new ComboboxItemDto
                    {
                        Value = accTye.Id,
                        DisplayText = accTye.Id + "-" + accTye.Name
                    });

            return output;
        }
        #endregion


        #region Customers
        public async Task<List<ComboboxItemDto>> GetCustomers()
        {
            var output = new List<ComboboxItemDto>();

            var token = this.GetQuickBooksTokenForTenant();

            if (token != null)
                foreach (var customer in CustomerQbCrud.GetAll(token))
                    output.Add(new ComboboxItemDto
                    {
                        Value = customer.Id,
                        DisplayText = customer.Id + "-" + customer.GivenName
                    });

            return output;
        }
        #endregion


        public async Task SyncItem(int materialId)
        {
            if (materialId > 0)
            {
                var material = await _mRepo.GetAsync(materialId);
                await QbMaterialSync(material);
            }
        }

        private async Task QbMaterialSync(Material material)
        {
            var addon = new AddOnOutput();
            var add = false;
            if (!string.IsNullOrEmpty(material.AddOn))
            {
                addon = JsonConvert.DeserializeObject<AddOnOutput>(material.AddOn);
                if (string.IsNullOrEmpty(addon.Xero)) add = true;
            }
            else
            {
                add = true;
            }

            var token = GetQuickBooksTokenForTenant();
            if (token != null)
            {
                var getCategoryId = GetMaterialCategory(material.MaterialGroupCategoryRefId);

                if (add && getCategoryId > 0)
                {
                    var supp = QbItemCrud.CreateMaterial(token, material);
                }
            }
        }

        private int GetMaterialCategory(int materialGroupCategoryRefId)
        {
            return 1;
        }
    }
}
