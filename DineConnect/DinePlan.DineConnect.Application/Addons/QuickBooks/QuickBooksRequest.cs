﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Addons.QuickBooks
{
    public class QuickBooksRequests
    {
        const string QUICK_BOOKS_API_MINOR_VERSION = "55";
        readonly static string API_BASE = ConfigurationManager.AppSettings["QuickBooksAPIBase"];

        public static string GenerateListAccountsEndpoint(string companyId)
        {
            return $"{API_BASE}/v3/company/{companyId}/query?query=select * from Account&minorversion={QUICK_BOOKS_API_MINOR_VERSION}";
        }

        public static string GenerateListCustomersEndpoint(string companyId)
        {
            return $"{API_BASE}/v3/company/{companyId}/query?query=select * from Customer&minorversion={QUICK_BOOKS_API_MINOR_VERSION}";
        }

        public static string GenerateCreateInvoiceEndpoint(string companyId)
        {
            return $"{API_BASE}/v3/company/{companyId}/invoice?minorversion={QUICK_BOOKS_API_MINOR_VERSION}";
        }

        public static string GenerateCreateItemEndpoint(string companyId)
        {
            return $"{API_BASE}/v3/company/{companyId}/item?minorversion={QUICK_BOOKS_API_MINOR_VERSION}";
        }
    }
}
