﻿using DinePlan.DineConnect.Addons.QuickBooks.Model;
using DinePlan.DineConnect.House;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Addons.QuickBooks.Crud
{
    public class QbItemCrud
    {
        /// <summary>
        /// Creates an item on Qb
        /// </summary>
        /// <param name="token"></param>
        /// <returns>Invoice id</returns>
        public static QbItem CreateMaterial(QuickBooksToken token, Material material)
        {

            /*
             * MINIMUM INPUT FOR CREATING AN ITEM ON QB
             * 
             * {
                  "TrackQtyOnHand": true, 
                  "QtyOnHand": 10, 
                  "Name": "Test Item", 
                  "Type": "Inventory", 
                  "InvStartDate": "2021-01-01",
                  "AssetAccountRef": {
                    "value": "81"
                  }, 
                  "IncomeAccountRef": {
                    "value": "79"
                  },
                  "ExpenseAccountRef": {
                    "value": "80"
                  }
                }
             * 
             */
            var qbMaterial = new QbItem()
            {
                Name = material.MaterialName,
                QtyOnHand = "10",
                TrackQtyOnHand = true,
                Type = "Inventory",
                InvStartDate = "2021-01-01",
                AssetAccountRef = QbItemAccountRef.FromValue("81"),
                IncomeAccountRef = QbItemAccountRef.FromValue("79"),
                ExpenseAccountRef = QbItemAccountRef.FromValue("80")
            };


            string endpoint = QuickBooksRequests.GenerateCreateItemEndpoint(token.realm_id);

            var client = new RestClient(endpoint)
            {
                Authenticator = new JwtAuthenticator(token.access_token)
            };

            var request = new RestRequest(Method.POST);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("content-type", "application/json");
            request.AddJsonBody(qbMaterial);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            var response = (RestResponse)client.Execute(request);

            if (!response.IsSuccessful)
            {
                return null;
            }

            
            JObject responseJson = JObject.Parse(response.Content);
            var itemResponse = responseJson["Item"].ToString();


            var qbItem = JsonConvert.DeserializeObject<QbItem>(itemResponse);
            return qbItem;
        }


    }
}
