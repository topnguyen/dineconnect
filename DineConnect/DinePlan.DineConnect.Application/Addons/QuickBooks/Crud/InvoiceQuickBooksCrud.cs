﻿using DinePlan.DineConnect.Addons.QuickBooks.Model;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Addons.QuickBooks.Crud
{
    public class InvoiceQuickBooksCrud
    {
        /// <summary>
        /// Creates an invoice
        /// </summary>
        /// <param name="token"></param>
        /// <returns>Invoice id</returns>
        public static string CreateInvoice(QuickBooksToken token, QbInvoice invoice)
        {
            string endpoint = QuickBooksRequests.GenerateCreateInvoiceEndpoint(token.realm_id);

            var client = new RestClient(endpoint)
            {
                Authenticator = new JwtAuthenticator(token.access_token)
            };

            var request = new RestRequest(Method.GET);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("content-type", "application/json");

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            var response = (RestResponse)client.Execute(request);

            if (!response.IsSuccessful)
            {
                return null;
            }

            /*
            JObject responseJson = JObject.Parse(response.Content);
            var queryResponse = responseJson["QueryResponse"];
            var accountsNode = queryResponse["Account"].ToString();


            var accountList = JsonConvert.DeserializeObject<List<QuickBooksAccount>>(accountsNode);


            return accountList;
            */
            return null;

        }
    }
}
