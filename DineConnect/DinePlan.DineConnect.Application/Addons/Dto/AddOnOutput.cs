﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Addons.Dto
{
    public class AddOnOutput
    {
        public string Qoyod { get; set; }
        public string Odoo { get; set; }
        public string Xero { get; set; }
        public string Ub { get; set; }
        public string UbMenu { get; set; }

       
        public UrbanPiperAddOn UrbanPiper
        {
            get => !string.IsNullOrEmpty(this.Ub) ? JsonConvert.DeserializeObject<UrbanPiperAddOn>(Ub) : new UrbanPiperAddOn();
            set
            {
                if (value != null)
                {
                    Ub = JsonConvert.SerializeObject(value);
                }
            }
        }
     
        public UrbanPiperMenuAddOn UrbanPiperMenuAddOn
        {
            get => !string.IsNullOrEmpty(this.UbMenu) ? JsonConvert.DeserializeObject<UrbanPiperMenuAddOn>(UbMenu) : new UrbanPiperMenuAddOn();
            set
            {
                if (value != null)
                {
                    UbMenu = JsonConvert.SerializeObject(value);
                }
            }
        }


        public AddOnOutput()
        {
            Ub = JsonConvert.SerializeObject(new UrbanPiperAddOn());
            UbMenu = JsonConvert.SerializeObject(new UrbanPiperMenuAddOn()); 
        }
    }
    [NotMapped]
    public class UrbanPiperAddOn
    {
        public bool Enabled { get; set; }

        public List<string> EnabledPlatforms { get; set; }

        public string[] AllPlatforms = {"zomato", "swiggy" };


    }
   
    [NotMapped]
    public class UrbanPiperMenuAddOn
    {
        public bool Recommend { get; set; }
        public string ReplyFiles { get; set; }
        public bool Enabled { get; set; }
        public bool Available { get; set; }

        public List<string> ZomatoTags { get; set; }
        public List<string> SwiggyTags { get; set; }

        public string[] AllZomatoTags =
        {
            "Beverage","Turbo","Packaged-Good","Chef Recommended","Spicy"
        };

        public string[] AllSwiggyTags => AllZomatoTags;
        public List<string> EnabledPlatforms { get; set; }

        public string[] AllPlatforms = {"zomato", "swiggy"};
        public UrbanPiperMenuAddOn()
        {
            ZomatoTags = new List<string>();
            SwiggyTags = new List<string>();
            Enabled = true;
            Available = true;
        }
    }

}
