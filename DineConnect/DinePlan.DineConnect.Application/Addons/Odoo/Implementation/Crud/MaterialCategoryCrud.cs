﻿using System;
using DinePlan.DineConnect.Addons.Odoo.Model;
using DinePlan.OdooService.Infrastructure.Extensions;
using DinePlan.OdooService.Models;

namespace DinePlan.DineConnect.Addons.Odoo.Implementation.Crud
{
    public class MaterialCategoryOdooCrud
    {
        public static int CreateMaterialCategory(string settings, string name)
        {
            var service = OdooCrud.GetConnect(settings);

            try
            {
                var matPresent = service.FirstOrDefault(OdooFilter<OdooMaterialCategory>.Where(x => x.Name == name));
                if (matPresent != null)
                {
                    return matPresent.Id;
                }
                var partner = new OdooMaterialCategory
                {
                    Name = name
                };
                var id = service.AddOrUpdate(partner);
                return id;
            }
            catch (Exception exception)
            {
                var messa = exception.Message;
            }

            var firstCategory = service.FirstOrDefault(OdooFilter<OdooMaterialCategory>.Where(x => x.Id > 0));
            if (firstCategory != null)
                return firstCategory.Id;
            return 0;
        }
    }
}