﻿using System;
using DinePlan.DineConnect.Addons.Odoo.Dto;
using DinePlan.DineConnect.Addons.Odoo.Model;
using DinePlan.DineConnect.House;
using DinePlan.OdooService.Context;
using DinePlan.OdooService.Infrastructure.Extensions;
using DinePlan.OdooService.Models;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Addons.Odoo.Implementation.Crud
{
    public class TaxOdooCrud
    {
        public static int CreateTax(string settings, Tax tax)
        {
            var service = OdooCrud.GetConnect(settings);
            var connectDto = JsonConvert.DeserializeObject<OdooConnectionDto>(settings);
            if (connectDto.TaxAccountId > 0)
            {
                var partner = new OdooTax
                {
                    Name = tax.TaxName,
                    TaxType = "percent",
                    TaxUse = "purchase",
                    Amount = Convert.ToDouble(tax.Percentage),
                    AccountId = connectDto.TaxAccountId,
                    Active = true
                };
                try
                {
                    var id = service.AddOrUpdate(partner);
                    return id;
                }
                catch (Exception exception)
                {
                    var messa = exception.Message;
                }
            }
            return 0;
        }

        public static void UpdateTax(string settings, Tax tax, string odoo)
        {
            var service = OdooCrud.GetConnect(settings);
            var connectDto = JsonConvert.DeserializeObject<OdooConnectionDto>(settings);
            if (connectDto.TaxAccountId > 0)
            {
                var partner = new OdooTax
                {
                    Name = tax.TaxName,
                    TaxType = "percent",
                    TaxUse = "purchase",
                    Amount = Convert.ToDouble(tax.Percentage),
                    Active = true,
                    AccountId = connectDto.TaxAccountId
                };
                service.AddOrUpdate(partner);

            }
        }
    }
}