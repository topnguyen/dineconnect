﻿using System;
using System.Collections.Generic;
using DinePlan.DineConnect.Addons.Odoo.Dto;
using DinePlan.DineConnect.Addons.Odoo.Model;
using DinePlan.DineConnect.House;
using DinePlan.OdooService.Context;
using DinePlan.OdooService.Infrastructure.Extensions;
using DinePlan.OdooService.Models;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Addons.Odoo.Implementation.Crud
{
    public class AccountOdooCrud
    {
        public static ICollection<OdooAccount> ReadAccounts(string settings)
        {
            var service = OdooCrud.GetConnect(settings);
            var accountJours = service.List<OdooAccount>
                   (OdooFilter<OdooAccount>.Where(x => true));
            return accountJours;
        }

        public static ICollection<OdooAccountType> ReadAccountTypes(string settings)
        {
            var service = OdooCrud.GetConnect(settings);
            var accountJours = service?.List<OdooAccountType>
                (OdooFilter<OdooAccountType>.Where(x => true));
            return accountJours;
        }

    }
}