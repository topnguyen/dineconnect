﻿using System;
using System.Collections.Generic;
using DinePlan.DineConnect.Addons.Odoo.Dto;
using DinePlan.DineConnect.Addons.Odoo.Model;
using DinePlan.DineConnect.House;
using DinePlan.OdooService.Context;
using DinePlan.OdooService.Infrastructure.Extensions;
using DinePlan.OdooService.Models;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Addons.Odoo.Implementation.Crud
{
    public class JournalOdooCrud
    {
        public static ICollection<OdooAccountJournal> ReadJournals(string settings)
        {
            var service = OdooCrud.GetConnect(settings);
            var accountJours = service.List<OdooAccountJournal>
                   (OdooFilter<OdooAccountJournal>.Where(x => true));
            return accountJours;
        }

       
    }
}