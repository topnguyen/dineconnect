﻿using System;
using DinePlan.DineConnect.Addons.Odoo.Dto;
using DinePlan.DineConnect.Addons.Odoo.Model;
using DinePlan.DineConnect.House;
using DinePlan.OdooService.Context;
using DinePlan.OdooService.Infrastructure.Extensions;
using DinePlan.OdooService.Models;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Addons.Odoo.Implementation.Crud
{
    public class SupplierOdooCrud
    {
        public static int CreateSupplier(string settings, Supplier supplier)
        {
            var service = OdooCrud.GetConnect(settings);
            var partner = new OdooPartner
            {
                Name = supplier.SupplierName,
                Email = supplier.Email
            };
            try
            {
                var id = service.AddOrUpdate(partner);
                return id;
            }
            catch (Exception exception)
            {
                var messa = exception.Message;
            }
            return 0;
        }

        public static void UpdateSupplier(string settings, Supplier supplier, string odoo)
        {
            var service = OdooCrud.GetConnect(settings);
            var partner = new OdooPartner
            {
                Id = Convert.ToInt32(odoo),
                Name = supplier.SupplierName,
                Email = supplier.Email
            };
            service.AddOrUpdate(partner);
        }
    }
}