﻿using System;
using DinePlan.DineConnect.Addons.Odoo.Model;
using DinePlan.DineConnect.House;
using DinePlan.OdooService.Infrastructure.Extensions;
using DinePlan.OdooService.Models;

namespace DinePlan.DineConnect.Addons.Odoo.Implementation.Crud
{
    public class MaterialOdooCrud
    {
        public static int CreateMaterial(string settings, Material material, int categoryId)
        {
            try
            {
                var service = OdooCrud.GetConnect(settings);
                var matPresent = service.FirstOrDefault(OdooFilter<OdooMaterial>.Where(x => x.Name == material.MaterialName));
                if (matPresent != null)
                {
                    return matPresent.Id;
                }

                var partner = new OdooMaterial
                {
                    Name = material.MaterialName,
                    CategoryId = categoryId,
                    Code = material.MaterialPetName
                };
                var id = service.AddOrUpdate(partner);
                return id;
            }
            catch (Exception exception)
            {
                var messa = exception.Message;
            }
            finally
            {
                
            }
            return 0;
        }

        public static void UpdateMaterial(string settings, Material material, string odoo, int categoryId)
        {
            var service = OdooCrud.GetConnect(settings);
            var partner = new OdooMaterial
            {
                Id = Convert.ToInt32(odoo),
                Name = material.MaterialName
            };
            service.AddOrUpdate(partner);
        }
    }
}