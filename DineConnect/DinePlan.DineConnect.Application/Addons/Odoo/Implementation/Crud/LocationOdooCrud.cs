﻿using System;
using DinePlan.DineConnect.Addons.Odoo.Dto;
using DinePlan.DineConnect.Addons.Odoo.Model;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.House;
using DinePlan.OdooService.Context;
using DinePlan.OdooService.Infrastructure.Extensions;
using DinePlan.OdooService.Models;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Addons.Odoo.Implementation.Crud
{
    public class LocationOdooCrud
    {
        public static string CreateLocation(string settings, Location location)
        {
            var service = OdooCrud.GetConnect(settings);
            var connectDto = JsonConvert.DeserializeObject<OdooConnectionDto>(settings);

            if (connectDto.AssetAccountType > 0 && connectDto.RevenueAccountType > 0)
            {
                var debitAccount = new OdooAccount
                {
                    Code = "D-"+location.Code.ToUpper(),
                    Name = location.Code + "-" + location.Name,
                    AccountType = connectDto.AssetAccountType,
                    Company = connectDto.Company
                };
                var debitAccountId = service.AddOrUpdate(debitAccount);

                var creditAccount = new OdooAccount
                {
                    Code = "C-"+location.Code.ToUpper(),
                    Name = location.Code + "-" + location.Name,
                    AccountType = connectDto.RevenueAccountType,
                    Company = connectDto.Company
                };
                var creditAccountId = service.AddOrUpdate(creditAccount);

                return debitAccountId + "," + creditAccountId;
            }

            return "";
        }
    }
}