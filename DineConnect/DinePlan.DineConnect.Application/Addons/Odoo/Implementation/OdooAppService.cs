﻿using System;
using System.Collections.Generic;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.UI;
using DinePlan.DineConnect.Addon;
using DinePlan.DineConnect.Addons.Dto;
using DinePlan.DineConnect.Addons.Odoo.Dto;
using DinePlan.DineConnect.Addons.Odoo.Implementation.Crud;
using DinePlan.DineConnect.Addons.Odoo.Model;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Period;
using DinePlan.DineConnect.House;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.House.Transaction.Implementation;
using DinePlan.OdooService.Context;
using DinePlan.OdooService.Infrastructure.Extensions;
using DinePlan.OdooService.Models;
using Newtonsoft.Json;
using Nito.AsyncEx.Synchronous;

namespace DinePlan.DineConnect.Addons.Odoo.Implementation
{
    public class OdooAppService : DineConnectAppServiceBase, IOdooAppService
    {
        private readonly IRepository<ConnectAddOn> _addOn;
        private readonly IRepository<Location> _lRepo;
        private readonly IRepository<MaterialGroupCategory> _mcRepo;
        private readonly IRepository<Material> _mRepo;
        private readonly IRepository<Supplier> _sRepo;
        private readonly IRepository<Tax> _taxRepo;
        private readonly IRepository<WorkPeriod> _wpRepo;
        private ISettingManager _settingManager;
        private IInvoiceAppService _invoiceAppService;

        public OdooAppService(IRepository<ConnectAddOn> addOn, IRepository<Supplier> supplier, IRepository<Tax> taxRepo,
            IRepository<WorkPeriod> wpRepo, IInvoiceAppService invoiceAppService,
            IRepository<Location> location, ISettingManager settingManager, IRepository<Material> mRepo,
            IRepository<MaterialGroupCategory> mcRepo)
        {
            _addOn = addOn;
            _sRepo = supplier;
            _lRepo = location;
            _settingManager = settingManager;
            _mRepo = mRepo;
            _wpRepo = wpRepo;
            _mcRepo = mcRepo;
            _taxRepo = taxRepo;
            _invoiceAppService = invoiceAppService;
        }

        #region Connection

        public async Task<OdooConnectionDto> UpdateConnection(OdooConnectionDto connectionDto)
        {
            var returnO = false;
            var qbc = MyOdoo(connectionDto);
            if (qbc == null)
                return connectionDto;

            returnO = !string.IsNullOrEmpty(qbc.Settings);
            try
            {
                if (returnO)
                {
                    var dto = JsonConvert.DeserializeObject<OdooConnectionDto>(qbc.Settings);
                    var conn = new OdooConnection
                    {
                        Database = dto.OdooDatabase,
                        Url = dto.OdooUrl,
                        Password = dto.OdooPassword,
                        Username = dto.OdooUsername
                    };
                    var service = new OdooHelperService(conn);
                    var intNumber = service.Login();
                    dto.Status = intNumber > 0;
                    return dto;
                }
            }
            catch (Exception exception)
            {
                var messa = exception.Message;
                returnO = false;
            }

            return connectionDto;
        }

        public async Task SyncLocations()
        {
            try
            {
                if (await MyOdooConnected())
                {
                    foreach (var location in await _lRepo.GetAllListAsync(a => a.Id > 0))
                    {
                        await OdooLocationSync(location);
                    }
                }
            }
            catch (Exception exception)
            {
                throw new UserFriendlyException(exception.Message);
            }
        }

        private async Task OdooLocationSync(Location location)
        {
            var addon = new AddOnOutput();
            var add = false;
            if (!string.IsNullOrEmpty(location.AddOn))
            {
                addon = JsonConvert.DeserializeObject<AddOnOutput>(location.AddOn);
                if (string.IsNullOrEmpty(addon.Odoo))
                {
                    add = true;
                }
            }
            else
            {
                add = true;
            }
            var qbc = MyOdoo(null);
            if (!string.IsNullOrEmpty(qbc?.Settings))
            {
                if (add)
                {
                    var loc = LocationOdooCrud.CreateLocation(qbc.Settings, location);
                    if (!string.IsNullOrEmpty(loc))
                    {
                        addon.Odoo = loc;
                    }
                    location.AddOn = JsonConvert.SerializeObject(addon);
                    await _lRepo.UpdateAsync(location);
                }
            }
        }




        private async Task<bool> MyOdooConnected()
        {
            var qbc = MyOdoo(null);
            if (qbc == null)
                return false;
            var returnO = !string.IsNullOrEmpty(qbc.Settings);
            return returnO;
        }
        private ConnectAddOn MyOdoo(OdooConnectionDto connectionDto)
        {
            var qbc = _addOn.FirstOrDefault(a => a.Name.Equals("Odoo"));
            if (qbc == null)
            {
                if (!string.IsNullOrEmpty(connectionDto?.OdooUrl))
                {
                    qbc = new ConnectAddOn
                    {
                        Name = "Odoo",
                        Settings = JsonConvert.SerializeObject(connectionDto)
                    };
                    _addOn.InsertAndGetId(qbc);
                }
            }
            else if (!string.IsNullOrEmpty(connectionDto?.OdooUrl))
            {
                qbc.Settings = JsonConvert.SerializeObject(connectionDto);
                _addOn.Update(qbc);
            }
            return qbc;
        }

        #endregion

        #region Supplier

        public async Task SyncSuppliers()
        {
            if (await MyOdooConnected())
            {
                foreach (var supplier in await _sRepo.GetAllListAsync(a => a.Id > 0))
                {
                    await OdooSupplierSync(supplier);
                }
            }
        }
        public void SyncSupplier(int supplierId)
        {
            if (supplierId > 0)
            {
                var supplier = _sRepo.Get(supplierId);
                OdooSupplierSync(supplier);
            }
        }
        private async Task OdooSupplierSync(Supplier supplier)
        {
            var addon = new AddOnOutput();
            var add = false;
            if (!string.IsNullOrEmpty(supplier.AddOn))
            {
                addon = JsonConvert.DeserializeObject<AddOnOutput>(supplier.AddOn);
                if (string.IsNullOrEmpty(addon.Odoo))
                {
                    add = true;
                }
            }
            else
            {
                add = true;
            }
            var qbc = MyOdoo(null);
            if (!string.IsNullOrEmpty(qbc?.Settings))
            {
                if (add)
                {
                    var supp = SupplierOdooCrud.CreateSupplier(qbc.Settings, supplier);
                    if (supp > 0)
                    {
                        addon.Odoo = supp.ToString();
                    }
                    supplier.AddOn = JsonConvert.SerializeObject(addon);
                    await _sRepo.UpdateAsync(supplier);
                }
                else
                {
                    SupplierOdooCrud.UpdateSupplier(qbc.Settings, supplier, addon.Odoo);
                }
            }
        }

        #endregion
        #region Material

        public async Task SyncMaterials()
        {
            if (await MyOdooConnected())
            {
                foreach (var material in await _mRepo.GetAllListAsync(a => a.Id > 0))
                {
                    await OdooMaterialSync(material);
                }
            }
        }

        private async Task OdooMaterialSync(Material material)
        {
            var addon = new AddOnOutput();
            var add = false;
            if (!string.IsNullOrEmpty(material.AddOn))
            {
                addon = JsonConvert.DeserializeObject<AddOnOutput>(material.AddOn);
                if (string.IsNullOrEmpty(addon.Odoo))
                {
                    add = true;
                }
            }
            else
            {
                add = true;
            }
            var qbc = MyOdoo(null);
            if (!string.IsNullOrEmpty(qbc?.Settings))
            {
                var getCategoryId = GetMaterialCategory(material.MaterialGroupCategoryRefId);

                if (add && getCategoryId > 0)
                {
                    var supp = MaterialOdooCrud.CreateMaterial(qbc.Settings, material, getCategoryId);
                    if (supp > 0)
                    {
                        addon.Odoo = supp.ToString();
                    }
                    material.AddOn = JsonConvert.SerializeObject(addon);
                    await _mRepo.UpdateAsync(material);
                }
            }
        }

        private int GetMaterialCategory(int materialGroupCategoryRefId)
        {
            var addon = new AddOnOutput();

            var qbc = MyOdoo(null);
            if (!string.IsNullOrEmpty(qbc?.Settings))
            {
                var materGroup = _mcRepo.Get(materialGroupCategoryRefId);
                if (materGroup != null)
                {
                    if (string.IsNullOrEmpty(materGroup.AddOn))
                    {
                        var categoryId =
                            MaterialCategoryOdooCrud.CreateMaterialCategory(qbc.Settings,
                                materGroup.MaterialGroupCategoryName);

                        if (categoryId > 0)
                        {
                            addon.Odoo = categoryId.ToString();
                        }
                        materGroup.AddOn = JsonConvert.SerializeObject(addon);
                        _mcRepo.Update(materGroup);

                        return categoryId;
                    }
                    var addOn = JsonConvert.DeserializeObject<AddOnOutput>(materGroup.AddOn);
                    return Convert.ToInt32(addOn.Odoo);
                }
            }
            return 0;
        }

        public void SyncMaterial(int materialId)
        {
            if (materialId > 0)
            {
                var Material = _mRepo.Get(materialId);
                OdooMaterialSync(Material);
            }
        }
        #endregion

        public async Task<List<ComboboxItemDto>> GetJournals()
        {
            if (await MyOdooConnected())
            {
            }
            return null;
        }

        public async Task<List<ComboboxItemDto>> GetAccountTypes()
        {
            var output = new List<ComboboxItemDto>();
            if (await MyOdooConnected())
            {
                var qbc = MyOdoo(null);
                if (!string.IsNullOrEmpty(qbc?.Settings))
                {
                    foreach (var accTye in AccountOdooCrud.ReadAccountTypes(qbc?.Settings))
                    {
                        output.Add(new ComboboxItemDto
                        {
                            DisplayText = accTye.Name,
                            Value = accTye.Id.ToString()
                        });
                    }
                }
            }
            return output;
        }
        public async Task<List<ComboboxItemDto>> GetAccounts()
        {
            var output = new List<ComboboxItemDto>();
            if (await MyOdooConnected())
            {
                var qbc = MyOdoo(null);
                if (!string.IsNullOrEmpty(qbc?.Settings))
                {
                    foreach (var accTye in AccountOdooCrud.ReadAccounts(qbc?.Settings))
                    {
                        output.Add(new ComboboxItemDto
                        {
                            DisplayText = accTye.Name,
                            Value = accTye.Id.ToString()
                        });
                    }
                }
            }
            return output;
        }

        public void SyncSales(int workPeriodId)
        {
            var myQb = MyOdoo(null);
            if (myQb == null)
                return;

            var connDto = JsonConvert.DeserializeObject<OdooConnectionDto>(myQb.Settings);
            if (workPeriodId > 0)
            {
                var workPeriod = _wpRepo.Get(workPeriodId);

                if (workPeriod != null && workPeriod.LocationId > 0 && workPeriod.TotalSales > 0M)
                {
                    if (!string.IsNullOrEmpty(workPeriod.AddOn))
                    {
                        var addOn = JsonConvert.DeserializeObject<AddOnOutput>(workPeriod.AddOn);
                        if (addOn != null)
                        {
                            if (string.IsNullOrEmpty(addOn.Odoo))
                            {
                                return;
                            }
                        }
                    }
                    var location = _lRepo.Get(workPeriod.LocationId);
                    if (!string.IsNullOrEmpty(location?.AddOn))
                    {
                        var addOn = JsonConvert.DeserializeObject<AddOnOutput>(location.AddOn);
                        if (!string.IsNullOrEmpty(addOn?.Odoo))
                        {
                            var ouputCode = addOn.Odoo.Split(',');
                            if (ouputCode.Length > 1)
                            {
                                var debitCode = ouputCode[0];
                                var creditCode = ouputCode[1];
                                var moveObje = new OdooAccountMove
                                {
                                    Date = workPeriod.StartTime.ToString("yyyy-MM-dd"),
                                    Name = location.Name + "-Sales-" +
                                           workPeriod.StartTime.ToString("yyyy-MM-dd"),
                                    Status = "draft",
                                    Journal = connDto.JournalType,
                                    Amount = Convert.ToDouble(workPeriod.TotalSales),
                                    Reference = location.Name + "-Sales-" +
                                                workPeriod.StartTime.ToString("yyyy-MM-dd")
                                };
                                var service = OdooCrud.GetConnect(myQb.Settings);
                                var moveId = service.AddOrUpdate(moveObje);

                                var debit = new OdooAccountMoveLine
                                {
                                    Name = location.Name + "-Sales-" +
                                           workPeriod.StartTime.ToString("yyyy-MM-dd"),
                                    MaturityDate = workPeriod.StartTime.ToString("yyyy-MM-dd"),
                                    MoveId = moveId,
                                    AccountId = Convert.ToInt32(debitCode),
                                    DebitAmount = Convert.ToDouble(workPeriod.TotalSales),
                                    CreditAmount = 0,
                                    Reference = location.Name + "-Sales-" +
                                                workPeriod.StartTime.ToString("yyyy-MM-dd"),
                                    Journal = connDto.JournalType
                                };
                                var debitId = service.AddOrUpdate(debit, false);

                                var credit = new OdooAccountMoveLine
                                {
                                    Name = location.Name + "-Sales-" +
                                           workPeriod.StartTime.ToString("yyyy-MM-dd"),
                                    MaturityDate = workPeriod.StartTime.ToString("yyyy-MM-dd"),
                                    MoveId = moveId,
                                    AccountId = Convert.ToInt32(creditCode),
                                    CreditAmount = Convert.ToDouble(workPeriod.TotalSales),
                                    DebitAmount = 0,
                                    Reference = location.Name + "-Sales-" +
                                                workPeriod.StartTime.ToString("yyyy-MM-dd"),
                                    Journal = connDto.JournalType
                                };
                                var creditId = service.AddOrUpdate(credit, true);

                                if (debitId > 0 && creditId > 0)
                                {
                                    var wpAdd = new AddOnOutput {Odoo = debitId + "," + creditId};
                                    workPeriod.AddOn = JsonConvert.SerializeObject(wpAdd);
                                    _wpRepo.Update(workPeriod);
                                }
                            }
                        }
                    }
                }
            }
        }

        public void SyncInvoice(int invoiceId)
        {
            var myQb = MyOdoo(null);
            if (myQb == null)
                return;

            var connDto = JsonConvert.DeserializeObject<OdooConnectionDto>(myQb.Settings);
            if (invoiceId > 0)
            {
                var task = _invoiceAppService.GetPurchaseInvoiceDetails(new InputInvoiceAnalysis
                {
                    InvoiceId = invoiceId
                });
                var service = OdooCrud.GetConnect(myQb.Settings);

                var result = task.WaitAndUnwrapException();

                if (result != null && result.Any())
                {
                    foreach (var myIn in result)
                    {
                        var odooVendor = _sRepo.Get(myIn.Invoice.SupplierRefId);
                        if (string.IsNullOrEmpty(odooVendor?.AddOn))
                            continue;
                        var supplierAdd = JsonConvert.DeserializeObject<AddOnOutput>(odooVendor.AddOn);
                        var odooRefId = Convert.ToInt32(supplierAdd.Odoo);

                        var myInvoice = new OdooAccountInvoice
                        {
                            InvoiceDate = myIn.Invoice.InvoiceDate.ToString("yyyy-MM-dd"),
                            AccountId = connDto.InvoiceAccountId,
                            VendorId = odooRefId,
                            Type = "in_invoice",
                            Name =myIn.Invoice.InvoiceNumber,
                            OutStanding = true
                        };
                        var invoiceMoveId = service.AddOrUpdate(myInvoice);

                        List<OdooAccountInvoiceLine> allLines = new List<OdooAccountInvoiceLine>();
                        List<OdooAccountInvoiceTax> allTaxLines = new List<OdooAccountInvoiceTax>();

                        Dictionary<string, decimal> taxes = new Dictionary<string, decimal>();

                        foreach (var myLine in myIn.InvoiceDetail)
                        {
                            var odooMaterial = _mRepo.Get(myLine.MaterialRefId);

                            if (string.IsNullOrEmpty(odooMaterial?.AddOn))
                                continue;

                            var materialAdd = JsonConvert.DeserializeObject<AddOnOutput>(odooMaterial.AddOn);
                            var odooMaterialRefId  = Convert.ToInt32(materialAdd.Odoo);

                            OdooAccountInvoiceLine line = new OdooAccountInvoiceLine
                            {
                                Name = myLine.MaterialRefName,
                                MoveId = invoiceMoveId,
                                ProductId = odooMaterialRefId,
                                Price = (double) myLine.Price,
                                Quantity = (double) myLine.TotalQty,
                                AccountId = connDto.InvoiceAccountId,
                            };
                            allLines.Add(line);

                            foreach (var myTax in myLine.TaxForMaterial)
                            {

                                if (taxes.ContainsKey(myTax.TaxName))
                                {
                                    var myValue = taxes[myTax.TaxName];
                                    myValue += myTax.TaxValue;
                                    taxes[myTax.TaxName] = myValue;
                                }
                                else
                                {
                                    taxes.Add(myTax.TaxName,myTax.TaxValue);
                                }
                            }
                        }

                        foreach (var odooTax in taxes.Keys)
                        {
                            var taxValue = taxes[odooTax];
                            var myTax = _taxRepo.FirstOrDefault(a => a.TaxName.Equals(odooTax));
                            if (string.IsNullOrEmpty(myTax?.AddOn))
                                continue;

                            var taxAdd = JsonConvert.DeserializeObject<AddOnOutput>(myTax.AddOn);
                            var odooTaxId = Convert.ToInt32(taxAdd.Odoo);
                            var invoicetax = new OdooAccountInvoiceTax
                            {
                                Name = myIn.Invoice.LocationRefName + "-" + myIn.Invoice.InvoiceNumber+":"+odooTax,
                                MoveId = invoiceMoveId,
                                TaxId = odooTaxId,
                                AccountId = connDto.TaxAccountId,
                                Amount = (double) taxValue
                            };
                            allTaxLines.Add(invoicetax);
                        }

                        foreach (var myOdooTax in allTaxLines)
                        {
                            service.AddOrUpdate(myOdooTax, false);
                        }

                        int lineCount = allLines.Count();
                        int myCount = 1;

                        if (lineCount > 1) { 
                            foreach (var myOdooLine in allLines.ToArray())
                            {
                                service.AddOrUpdate(myOdooLine, false);
                                myCount++;

                                if (myCount>=lineCount)
                                {
                                    break;
                                }
                            }
                        }
                        var lastLine = allLines.ToArray()[lineCount-1];
                        service.AddOrUpdate(lastLine, true);
                    }
                }
            }
        }


        #region Tax

        public async Task SyncTaxes()
        {
            if (await MyOdooConnected())
            {
                foreach (var tax in await _taxRepo.GetAllListAsync(a => a.Id > 0))
                {
                    await OdooTaxSync(tax);
                }
            }
        }
        public void SyncTax(int taxId)
        {
            if (taxId > 0)
            {
                var tax = _taxRepo.Get(taxId);
                OdooTaxSync(tax);
            }
        }
        private async Task OdooTaxSync(Tax tax)
        {
            var addon = new AddOnOutput();
            var add = false;
            if (!string.IsNullOrEmpty(tax.AddOn))
            {
                addon = JsonConvert.DeserializeObject<AddOnOutput>(tax.AddOn);
                if (string.IsNullOrEmpty(addon.Odoo))
                {
                    add = true;
                }
            }
            else
            {
                add = true;
            }
            var qbc = MyOdoo(null);
            if (!string.IsNullOrEmpty(qbc?.Settings))
            {
                if (add)
                {
                    var supp = TaxOdooCrud.CreateTax(qbc.Settings, tax);
                    if (supp > 0)
                    {
                        addon.Odoo = supp.ToString();
                    }
                    tax.AddOn = JsonConvert.SerializeObject(addon);
                    await _taxRepo.UpdateAsync(tax);
                }
                else
                {
                    TaxOdooCrud.UpdateTax(qbc.Settings, tax, addon.Odoo);
                }
            }
        }
        #endregion


    }
}