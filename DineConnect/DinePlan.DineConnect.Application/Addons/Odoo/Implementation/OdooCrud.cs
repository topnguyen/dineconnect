﻿using System;
using DinePlan.DineConnect.Addons.Odoo.Dto;
using DinePlan.OdooService.Context;
using DinePlan.OdooService.Models;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Addons.Odoo.Implementation
{
    public class OdooCrud
    {
        public static OdooHelperService GetConnect(string settings)
        {
            var connectDto = JsonConvert.DeserializeObject<OdooConnectionDto>(settings);

            if (connectDto != null)
            {
                OdooConnection conn = new OdooConnection
                {
                    Database = connectDto.OdooDatabase,
                    Url = connectDto.OdooUrl,
                    Password = connectDto.OdooPassword,
                    Username = connectDto.OdooUsername
                };

                var service = new OdooHelperService(conn);
                return service;
            }
            throw new Exception("Odoo Not Available");
        }
    }
}
