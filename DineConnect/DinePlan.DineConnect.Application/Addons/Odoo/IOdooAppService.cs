﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Addons.Odoo.Dto;

namespace DinePlan.DineConnect.Addons.Odoo
{
    public interface IOdooAppService : IApplicationService
    {
        Task<OdooConnectionDto> UpdateConnection(OdooConnectionDto connectionDto);
        Task SyncLocations();

        Task SyncSuppliers();
        void SyncSupplier(int supplierId);

        Task SyncMaterials();
        void SyncMaterial(int materialId);

        Task SyncTaxes();
        void SyncTax(int taxId);

        Task<List<ComboboxItemDto>> GetJournals();
        Task<List<ComboboxItemDto>> GetAccountTypes();
        Task<List<ComboboxItemDto>> GetAccounts();


        void SyncSales(int workPeriodId);
        void SyncInvoice(int invoiceId);
    }
}
