﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.OdooService.Infrastructure.Attributes;
using DinePlan.OdooService.Infrastructure.Interfaces;

namespace DinePlan.DineConnect.Addons.Odoo.Model
{
    [OdooMap("account.invoice")]
    public class OdooAccountInvoice : IOdooObject
    {
        [OdooMap("id")]
        public int Id { get; set; }

        [OdooMap("name")]
        public string Name { get; set; }


        [OdooMap("account_id")]
        public int AccountId { get; set; }

        [OdooMap("partner_id")]
        public int VendorId { get; set; }

        [OdooMap("date_invoice")]
        public string InvoiceDate { get; set; }

        [OdooMap("invoice_line_ids")]
        public ICollection<OdooAccountInvoiceLine> Lines { get; set; }

        [OdooMap("type")]
        public string Type { get; set; }

        [OdooMap("has_outstanding")]
        public bool OutStanding { get; set; }
    }

    [OdooMap("account.invoice.line")]
    public class OdooAccountInvoiceLine : IOdooObject
    {
        [OdooMap("id")]
        public int Id { get; set; }

        [OdooMap("name")]
        public string Name { get; set; }

        [OdooMap("account_id")]
        public int AccountId { get; set; }

        [OdooMap("product_id")]
        public int ProductId { get; set; }

        [OdooMap("price_unit")]
        public double Price { get; set; }

        [OdooMap("quantity")]
        public double Quantity { get; set; }

        [OdooMap("invoice_line_tax_ids")]
        public ICollection<OdooTax> Taxes { get; set; }

        [OdooMap("invoice_id")]
        public int MoveId { get; set; }



    }

    [OdooMap("account.invoice.tax")]
    public class OdooAccountInvoiceTax : IOdooObject
    {
        [OdooMap("id")]
        public int Id { get; set; }

        [OdooMap("name")]
        public string Name { get; set; }

        [OdooMap("invoice_id")]
        public int MoveId { get; set; }

        [OdooMap("account_id")]
        public int AccountId { get; set; }

        [OdooMap("tax_id")]
        public int TaxId { get; set; }

        [OdooMap("amount")]
        public double Amount { get; set; }

    }
}
