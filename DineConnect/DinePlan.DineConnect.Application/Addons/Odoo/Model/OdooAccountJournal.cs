﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.OdooService.Infrastructure.Attributes;
using DinePlan.OdooService.Infrastructure.Interfaces;

namespace DinePlan.DineConnect.Addons.Odoo.Model
{
    [OdooMap("account.journal")]
    public class OdooAccountJournal : IOdooObject
    {
        [OdooMap("id")]
        public int Id { get; set; }

        [OdooMap("name")]
        public string Name { get; set; }

    }

    [OdooMap("account.account.type")]
    public class OdooAccountType : IOdooObject
    {
        [OdooMap("id")]
        public int Id { get; set; }

        [OdooMap("name")]
        public string Name { get; set; }
    }
    [OdooMap("account.account")]
    public class OdooAccount : IOdooObject
    {
        [OdooMap("id")]
        public int Id { get; set; }

        [OdooMap("name")]
        public string Name { get; set; }

        [OdooMap("code")]
        public string Code { get; set; }

        [OdooMap("company_id")]
        public int Company { get; set; }

        [OdooMap("user_type_id")]
        public int AccountType { get; set; }
    }

    [OdooMap("account.move.line")]
    public class OdooAccountMoveLine : IOdooObject
    {
        [OdooMap("id")]
        public int Id { get; set; }

        [OdooMap("name")]
        public string Name { get; set; }

        [OdooMap("account_id")]
        public int AccountId { get; set; }

        [OdooMap("date_maturity")]
        public string MaturityDate { get; set; }

        [OdooMap("debit")]
        public double DebitAmount { get; set; }

        [OdooMap("move_id")]
        public int MoveId { get; set; }
        [OdooMap("credit")]
        public double CreditAmount { get; set; }

        [OdooMap("ref")]
        public string Reference { get; set; }
        [OdooMap("balance")]
        public double Balance { get; set; }

        [OdooMap("journal_id")]
        public int Journal { get; set; }
    }

    [OdooMap("account.move")]

    public class OdooAccountMove : IOdooObject
    {
        [OdooMap("date")]
        public string Date { get; set; }

        [OdooMap("id")]
        public int Id { get; set; }

        [OdooMap("name")]
        public string Name { get; set; }

        [OdooMap("state")]
        public string Status { get; set; }

        [OdooMap("journal_id")]
        public int Journal { get; set; }
        [OdooMap("amount")]
        public double Amount { get; set; }

        [OdooMap("line_ids")]
        public ICollection<OdooAccountMoveLine> Lines { get; set; }
        [OdooMap("ref")]
        public string Reference { get; set; }
    }

    


}
