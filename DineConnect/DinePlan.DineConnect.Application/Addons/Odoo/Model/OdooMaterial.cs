﻿using DinePlan.OdooService.Infrastructure.Attributes;
using DinePlan.OdooService.Infrastructure.Enums;
using DinePlan.OdooService.Infrastructure.Interfaces;

namespace DinePlan.DineConnect.Addons.Odoo.Model
{

    [OdooMap("product.product")]
    class OdooMaterial : IOdooObject
    {
        public OdooMaterial()
        {
            IsPurchase = true;
            Active = true;
            IsSale = false;
            ProductType = "consu";
            PurchaseUom = 1;
            Uom = 1;

        }
        [OdooMap("id")]
        public int Id { get; set; }
        [OdooMap("code")]
        public string Code { get; set; }
        [OdooMap("name")]
        public string Name { get; set; }

        [OdooMap("purchase_ok", OdooType.Boolean)]
        public bool IsPurchase { get; set; }

        [OdooMap("sale_ok", OdooType.Boolean)]
        public bool IsSale { get; set; }

        [OdooMap("active", OdooType.Boolean)]
        public bool Active { get; set; }

        [OdooMap("categ_id", OdooType.Many2One)]
        public int? CategoryId { get; set; }

        [OdooMap("uom_id", OdooType.Many2One)]
        public int? Uom { get; set; }

        [OdooMap("uom_po_id ", OdooType.Many2One)]
        public int? PurchaseUom { get; set; }

        [OdooMap("type")]
        public string ProductType { get; set; }

    }

    [OdooMap("product.category")]

    class OdooMaterialCategory : IOdooObject
    {
        public OdooMaterialCategory()
        {
        }
        [OdooMap("id")]
        public int Id { get; set; }
      
        [OdooMap("name")]
        public string Name { get; set; }
    }

}
