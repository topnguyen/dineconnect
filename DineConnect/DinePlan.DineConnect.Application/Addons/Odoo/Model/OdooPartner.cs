﻿using DinePlan.OdooService.Infrastructure.Attributes;
using DinePlan.OdooService.Infrastructure.Enums;
using DinePlan.OdooService.Infrastructure.Interfaces;

namespace DinePlan.DineConnect.Addons.Odoo.Model
{

    [OdooMap("res.partner")]
    public class OdooPartner : IOdooObject
    {
        public OdooPartner()
        {
            IsCompany = true;
            IsVendor = true;
            IsCustomer = false;
            Active = true;
        }
        [OdooMap("id")]
        public int Id { get; set; }

        [OdooMap("name")]
        public string Name { get; set; }

        [OdooMap("company_name")]
        public string CompanyName => Name;

        [OdooMap("is_company", OdooType.Boolean)]
        public bool IsCompany { get; set; }

        [OdooMap("supplier", OdooType.Boolean)]
        public bool IsVendor { get; set; }

        [OdooMap("customer", OdooType.Boolean)]
        public bool IsCustomer { get; set; }

        [OdooMap("active", OdooType.Boolean)]
        public bool Active { get; set; }

        [OdooMap("email")]
        public string Email { get; set; }

       
        

    }
}
