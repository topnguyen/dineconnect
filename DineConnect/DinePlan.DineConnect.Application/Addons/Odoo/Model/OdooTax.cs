﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.OdooService.Infrastructure.Attributes;
using DinePlan.OdooService.Infrastructure.Interfaces;

namespace DinePlan.DineConnect.Addons.Odoo.Model
{
    [OdooMap("account.tax")]
    public class OdooTax : IOdooObject
    {

        [OdooMap("id")]
        public int Id { get; set; }

        [OdooMap("name")]
        public string Name { get; set; }

        [OdooMap("account_id")]
        public int AccountId { get; set; }

        [OdooMap("active")]
        public bool Active { get; set; }

        [OdooMap("amount_type")]
        public string TaxType { get; set; }

        [OdooMap("type_tax_use")]
        public string TaxUse { get; set; }



        [OdooMap("amount")]
        public double Amount { get; set; }

    }


}
