﻿using System;

namespace DinePlan.DineConnect.Addons.Odoo.Dto
{
    public class OdooConnectionDto
    {
        public String OdooUrl { get; set; }
        public String OdooDatabase { get; set; }
        public String OdooUsername { get; set; }
        public String OdooPassword { get; set; }
        public bool Status { get; set; }
        public int AssetAccountType{ get; set; }
        public int RevenueAccountType { get; set; }
        public int Company { get; set; }
        public int JournalType { get; set; }
        public int InvoiceAccountId { get; set; }
        public int TaxAccountId { get; set; }

        public OdooConnectionDto()
        {
            Company = 1;
            JournalType = 2;
        }

    }

}
