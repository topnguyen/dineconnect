﻿using System;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Features;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Session;
using DinePlan.DineConnect.Addon;
using DinePlan.DineConnect.Addons.Grab.Dtos;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Audit;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.OrderTags;
using DinePlan.DineConnect.Connect.Period;
using Newtonsoft.Json;
using RestSharp;

namespace DinePlan.DineConnect.Addons.Grab.Implementation
{
    public class GrabAppService : IGrabAppService
    {
        private const string AddOnName = "Grab";
        private readonly IRepository<ConnectAddOn> _addOn;
        private readonly IRepository<ExternalLog> _logRepository;

        private readonly string _ApiMenuItemChangeProduction = "https://partner-api.grab.com/grabfood/partner/v1/menu";
        private readonly string _ApiMenuItemChangeStaging = "https://partner-api.stg-myteksi.com/grabfood-sandbox/partner/v1/menu";

        private readonly string _ApiNotificationProduction = "https://partner-api.stg-myteksi.com/grabfood/partner/v1/merchant/menu/notification";

        private readonly string _ApiNotificationStaging = "https://partner-api.stg-myteksi.com/grabfood-sandbox/partner/v1/merchant/menu/notification";

        private readonly string _ApiOAuthProduction = "https://api.grab.com/grabid/v1/oauth2/token";
        private readonly string _ApiOauthStaging = "https://api.stg-myteksi.com/grabid/v1/oauth2/token";

        private readonly string _ApiOrderStatusProduction  = "https://partner-api.grab.com/grabfood/partner/v1/order/prepare";
        private readonly string _ApiOrderStatusStaging = "https://partner-api.stg-myteksi.com/grabfood-sandbox/partner/v1/order/prepare";

        private readonly string _ApiReportDeliveryProduction = "https://partner-api.grab.com/grabfood/partner/v1/order/delivery";
        private readonly string _ApiReportDeliveryStaging = "https://partner-api.stg-myteksi.com/grabfood-sandbox/partner/v1/order/delivery";

        private readonly string _ApiRetrieveMenuProduction = "https://partner-api.grab.com/grabfood/partner/v1/order/delivery";
        private readonly string _ApiRetrieveMenuStaging = "https://partner-api.stg-myteksi.com/grabfood-sandbox/partner/v1/order/delivery";



        public GrabAppService(IRepository<ConnectAddOn> addOn, IRepository<ExternalLog> logRepository)
        {
            _addOn = addOn;
            _logRepository = logRepository;
        }

        public bool RemoveToken()
        {
            var granAddOn = _addOn.FirstOrDefault(a => a.Name.Equals(AddOnName));
            if (granAddOn != null) _addOn.Delete(granAddOn);
            return true;
        }

        public bool Connect(string clientId, string clientSecret, bool production)
        {
            try
            {
                var myToken = RefreshTokenAndSave(clientId, clientSecret, production, 0);
                if (myToken != null)
                    return true;
            }
            catch (Exception e)
            {
                LogGrab(e);
            }

            return false;
        }


        public bool IsTokenExists()
        {
            try
            {
                var grabAddOn = _addOn.FirstOrDefault(a => a.Name.Equals(AddOnName) && !a.IsDeleted);
                if (grabAddOn != null)
                {
                    var token = JsonConvert.DeserializeObject<GrabToken>(grabAddOn.Settings);
                    var grabSetting = JsonConvert.DeserializeObject<GrabSetting>(grabAddOn.AddOnSettings);

                    if (token != null && token.IsExpired && !string.IsNullOrEmpty(token.access_token))
                    {
                        var myRefresh = RefreshTokenAndSave(grabSetting.ClientId, grabSetting.ClientSecret,
                            grabSetting.Production, 0);
                        return myRefresh != null;
                    }

                    return token != null && !string.IsNullOrEmpty(token.access_token);
                }

                return false;
            }
            catch (Exception e)
            {
                LogGrab(e);
            }

            return false;
        }

        public async Task<bool> SyncToGrabForMenu(string merchantData)
        {
            var token = GetGrabToken();
            if (token != null && !token.access_token.IsNullOrEmpty())
            {
                var url = token.Production ? _ApiNotificationProduction : _ApiNotificationStaging;
                var jsonBody = new
                {
                    merchantID = merchantData
                };
                var client = new RestClient(url);

                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Authorization", $"Bearer {token.access_token}");

                var body = JsonConvert.SerializeObject(jsonBody);
                request.AddParameter("application/json", body, ParameterType.RequestBody);

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                var response = (RestResponse)client.Execute(request);

                return response.IsSuccessful;
            }

            return false;
        }

        public async Task<string> SyncMenuStatus(string merchantData)
        {
            var token = GetGrabToken();

            if (token != null && !token.access_token.IsNullOrEmpty())
            {
                var url = token
                    .Production
                    ? _ApiNotificationProduction
                    : _ApiNotificationStaging;
                var jsonBody = new
                {
                    merchantID = merchantData
                };
                var client = new RestClient(url);

                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Authorization", $"Bearer {token.access_token}");

                var body = JsonConvert.SerializeObject(jsonBody);
                request.AddParameter("application/json", body, ParameterType.RequestBody);

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                var response = (RestResponse)client.Execute(request);

                return response.Content;
            }

            return "";
        }


        public async Task<bool> ChangeGrabItem(GrabChangeTenantInput input)
        {
            var token = GetGrabToken(input.TenantId);

            if (token != null && !token.access_token.IsNullOrEmpty())
            {
                var url = token.Production ? _ApiMenuItemChangeProduction : _ApiMenuItemChangeStaging;
                var jsonBody = new
                {
                    merchantID = input.MerchantId,
                    field = "ITEM",
                    id = input.Id,
                    price = input.Price,
                    availableStatus = input.InActive ? "UNAVAILABLE" : "AVAILABLE"
                };
                var client = new RestClient(url);

                var request = new RestRequest(Method.PUT);
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Authorization", $"Bearer {token.access_token}");

                var body = JsonConvert.SerializeObject(jsonBody);
                request.AddParameter("application/json", body, ParameterType.RequestBody);

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                var response = (RestResponse)client.Execute(request);

                return response.IsSuccessful;
            }

            return false;
        }

        public async Task<bool> OrderAccept(GrabOrderChangeRequest input)
        {
            var token = GetGrabToken(input.TenantId);

            if (token != null && !token.access_token.IsNullOrEmpty())
            {
                var url = token.Production ? _ApiOrderStatusProduction : _ApiOrderStatusStaging;
                var jsonBody = new
                {
                    orderID = input.OrderId,
                    toState = input.Accepted ? "Accepted" : "Rejected"
                };
                var client = new RestClient(url);

                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Authorization", $"Bearer {token.access_token}");

                var body = JsonConvert.SerializeObject(jsonBody);
                request.AddParameter("application/json", body, ParameterType.RequestBody);

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                var response = (RestResponse)client.Execute(request);

                return response.IsSuccessful;
            }

            return false;
        }

        public async Task<bool> OrderByPartner(GrabOrderPartnerRequest input)
        {
            var token = GetGrabToken(input.TenantId);

            if (token != null && !token.access_token.IsNullOrEmpty())
            {
                var url = token.Production ? _ApiReportDeliveryProduction : _ApiReportDeliveryStaging;
                var jsonBody = new
                {
                    orderID = input.OrderId,
                    fromState = input.FromState,
                    toState = input.ToState
                };
                var client = new RestClient(url);

                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Authorization", $"Bearer {token.access_token}");
                var body = JsonConvert.SerializeObject(jsonBody);
                request.AddParameter("application/json", body, ParameterType.RequestBody);

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                var response = (RestResponse)client.Execute(request);
                return response.IsSuccessful;
            }

            return false;
        }


        private GrabToken GetGrabToken(int tenantId = 0)
        {
            var grabAddOn = _addOn.GetAll().OrderByDescending(c => c.CreationTime)
                .FirstOrDefault(a => a.Name.Equals(AddOnName) && !a.IsDeleted);

            if (tenantId > 0)
                grabAddOn = _addOn.GetAll().OrderByDescending(c => c.CreationTime)
                    .FirstOrDefault(a => a.Name.Equals(AddOnName) && !a.IsDeleted && a.TenantId == tenantId);

            if (grabAddOn != null)
            {
                var token = JsonConvert.DeserializeObject<GrabToken>(grabAddOn.Settings);
                var grabSetting = JsonConvert.DeserializeObject<GrabSetting>(grabAddOn.AddOnSettings);

                if (token != null && token.IsExpired && !string.IsNullOrEmpty(token.access_token))
                    token = RefreshTokenAndSave(grabSetting.ClientId, grabSetting.ClientSecret, grabSetting.Production,
                        tenantId);

                if (token != null && !token.access_token.IsNullOrEmpty())
                {
                    token.Production = grabSetting.Production;
                    return token;
                }
            }

            return null;
        }

        public GrabToken RefreshTokenAndSave(string clientId, string clientSecret, bool production, int tenantId)
        {
            try
            {
                var mySetting = new GrabSetting
                {
                    Production = production,
                    ClientId = clientId,
                    ClientSecret = clientSecret
                };

                var reconquest = new
                {
                    client_id = clientId,
                    client_secret = clientSecret,
                    grant_type = "client_credentials",
                    scope = "food.partner_api"
                };

                var url = production ? _ApiOAuthProduction : _ApiOauthStaging;
                var client = new RestClient(url);

                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");

                var body = JsonConvert.SerializeObject(reconquest);
                request.AddParameter("application/json", body, ParameterType.RequestBody);

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                var response = (RestResponse)client.Execute(request);

                if (ValidateRestResponse(response))
                {
                    var refreshedToken = JsonConvert.DeserializeObject<GrabToken>(response.Content);
                    if (refreshedToken != null)
                    {
                        var grabAddOn = _addOn.FirstOrDefault(a => a.Name.Equals(AddOnName) && !a.IsDeleted);
                        if (tenantId > 0)
                            grabAddOn = _addOn.FirstOrDefault(a =>
                                a.Name.Equals(AddOnName) && !a.IsDeleted && a.TenantId == tenantId);
                        refreshedToken.ExpiresOn = DateTime.UtcNow.AddSeconds(refreshedToken.expires_in);

                        if (grabAddOn == null)
                            grabAddOn = new ConnectAddOn
                            {
                                Name = AddOnName
                            };

                        grabAddOn.AddOnSettings = JsonConvert.SerializeObject(mySetting);
                        grabAddOn.Settings = refreshedToken.ToJson();
                        _addOn.InsertOrUpdate(grabAddOn);

                        return refreshedToken;
                    }
                }
            }
            catch (Exception e)
            {
                LogGrab(e);
            }

            return null;
        }

        private bool ValidateRestResponse(RestResponse response)
        {
            if (response.StatusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(response.Content)) return true;
            return false;
        }


        #region Error

        private void LogGrab(Exception e)
        {
            var myLog = new ExternalLog
            {
                AuditType = (int)ExternalLogType.Grab,
                LogData = e.Message,
                LogDescription = FlattenException(e),
                LogTime = DateTime.Now,
                ExternalLogTrunc = DateTime.Now.Date
            };
            _logRepository.InsertAndGetId(myLog);
        }

        private static string FlattenException(Exception exception)
        {
            var stringBuilder = new StringBuilder();

            while (exception != null)
            {
                stringBuilder.AppendLine(exception.Message);
                stringBuilder.AppendLine(exception.StackTrace);

                exception = exception.InnerException;
            }

            return stringBuilder.ToString();
        }

        #endregion Error
    }
}