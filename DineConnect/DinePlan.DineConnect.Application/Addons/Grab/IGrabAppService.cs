﻿using Abp.Application.Services;
using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Addons.Grab.Dtos;
using DinePlan.DineConnect.External.Delivery.Dto;

namespace DinePlan.DineConnect.Addons.Grab
{
    public interface IGrabAppService : IApplicationService
    {
        bool IsTokenExists();
        bool RemoveToken();
        bool Connect(string clientId, string clientSecret, bool production);
        Task<bool> SyncToGrabForMenu(string merchantData);
        Task<string> SyncMenuStatus(string merchantData);
        Task<bool> ChangeGrabItem(GrabChangeTenantInput input);
        Task<bool> OrderAccept(GrabOrderChangeRequest input);
        Task<bool> OrderByPartner(GrabOrderPartnerRequest input);

    }
}