﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Addons.Grab.Dtos
{
    public class GrabChangeItemInput
    {
        public string MerchantId { get; set; }
        public string Id { get; set; }
        public int Price { get; set; }
        public bool InActive{ get; set; }

    }

    public class GrabChangeTenantInput : GrabChangeItemInput
    {
        public int TenantId { get; set; }
    }

    public class GrabOrderChangeRequest
    {
        public int TenantId { get; set; }
        public string OrderId { get; set; }
        public bool Accepted { get; set; }


    }

    public class GrabOrderPartnerRequest
    {
        public int TenantId { get; set; }
        public string OrderId { get; set; }
        public string FromState { get; set; }
        public string ToState { get; set; }

    }
}
