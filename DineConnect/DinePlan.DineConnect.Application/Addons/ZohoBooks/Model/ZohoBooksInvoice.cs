﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Addons.ZohoBooks.Model
{
    /// <summary>
    /// https://www.zoho.com/books/api/v3/#Invoices
    /// </summary>
    public class ZohoBooksInvoice
    {
        [JsonProperty("invoice_number")]
        public string InvoiceNumber { get; set; }

        [JsonProperty("item_name")]
        public string ItemName { get; set; }

        [JsonProperty("item_id")]
        public long ItemId { get; set; }

        [JsonProperty("recurring_invoice_id")]
        public string RecurringInvoiceId { get; set; }

        [JsonProperty("custom_field")]
        public string CustomField { get; set; }

        [JsonProperty("search_text")]
        public string SearchText { get; set; }

        [JsonProperty("sort_column")]
        public string SortColumn { get; set; }
    }
}
