﻿using DinePlan.DineConnect.Addons.QuickBooks;
using DinePlan.DineConnect.Addons.ZohoBooks.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Addons.ZohoBooks.Crud
{
    public class AccountZohoBooksCrud
    {
        /// <summary>
        /// Lists all accounts on ZohoBooks
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static List<ZohoBooksBankAccount> GetAll(ZohoBooksToken token)
        {
            string endpoint = ZohoBooksRequests.GenerateBankAccountsEndpoint(token.organization_id);

            var client = new RestClient(endpoint)
            {
                Authenticator = new JwtAuthenticator(token.access_token)
            };

            var request = new RestRequest(Method.GET);
            request.AddHeader("Accept", "application/json");
            request.AddHeader("content-type", "application/json");

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            var response = (RestResponse)client.Execute(request);

            if (!response.IsSuccessful)
            {
                return null;
            }

            JObject responseJson = JObject.Parse(response.Content);
            var accountsNode = responseJson["bankaccounts"].ToString();

            var accountList = JsonConvert.DeserializeObject<List<ZohoBooksBankAccount>>(accountsNode);

            return accountList;

        }
    }
}
