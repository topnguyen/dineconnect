﻿using DinePlan.DineConnect.Addons.Util;
using DinePlan.DineConnect.Addons.ZohoBooks.Model;
using DinePlan.DineConnect.House;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Addons.ZohoBooks.Crud
{
    /// <summary>
    /// https://www.zoho.com/books/api/v3/#Items
    /// </summary>
    public class ZohoBooksItemCrud
    {
        /// <summary>
        /// Creates an item on ZohoBooks
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static ZohoBooksItem CreateMaterial(ZohoBooksToken token, Material material)
        {
            var zohoBooksMaterial = new ZohoBooksItem()
            {
                Description = material.MaterialName,
                ItemId = material.Id,
                Name = material.MaterialPetName,
                ProductType = "goods",
                Status = "active"
            };

            string endpoint = ZohoBooksRequests.GenerateItemEndpoint(token.organization_id);

            var client = new RestClient(endpoint)
            {
                Authenticator = new JwtAuthenticator(token.access_token)
            };

            var request = new RestRequest(Method.POST);
            request.JsonSerializer = new NewtonsoftJsonSerializer();
            request.AddHeader("Accept", "application/json");
            request.AddHeader("content-type", "application/json");
            request.AddJsonBody(zohoBooksMaterial);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            var response = (RestResponse)client.Execute(request);

            if (!response.IsSuccessful)
            {
                return null;
            }

            JObject responseJson = JObject.Parse(response.Content);
            var itemResponse = responseJson["item"].ToString();

            var zohoBooksItem = JsonConvert.DeserializeObject<ZohoBooksItem>(itemResponse);
            return zohoBooksItem;
        }
    }
}
