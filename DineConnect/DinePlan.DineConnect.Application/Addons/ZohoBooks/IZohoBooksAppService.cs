﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Addons.ZohoBooks
{
    public interface IZohoBooksAppService : IApplicationService
    {
        ZohoBooksToken RefreshTokenAndSave(ZohoBooksToken token);
        Task<bool> KeepTokenComingFromGateway(ZohoBooksToken token);
        string GetLoginUrl();
        bool RemoveToken();
        bool IsTokenExists();
        Task<List<ComboboxItemDto>> GetAccounts();
        Task<List<ComboboxItemDto>> GetCustomers();
        Task SyncItem(int materialId);

    }
}
