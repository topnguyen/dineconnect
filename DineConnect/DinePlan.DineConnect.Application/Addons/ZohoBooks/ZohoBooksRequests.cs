﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Addons.ZohoBooks
{

    /// <summary>
    /// For all endpoints visit https://www.zoho.com/books/api/v3
    /// </summary>
    public static class ZohoBooksRequests
    {
        readonly static string API_BASE = "https://books.zoho.com/api";

        public static string GenerateBankAccountsEndpoint(string organizationId)
        {
            return $"{API_BASE}/v3/bankaccounts?organization_id={organizationId}";
        }

        public static string GenerateContactsEndpoint(string organizationId)
        {
            return $"{API_BASE}/v3/contacts?organization_id={organizationId}";
        }

        public static string GenerateItemEndpoint(string organizationId)
        {
            return $"{API_BASE}/v3/items?organization_id={organizationId}";
        }
    }
}
