﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using DinePlan.DineConnect.Addon;
using DinePlan.DineConnect.Addons.Dto;
using DinePlan.DineConnect.Addons.ZohoBooks.Crud;
using DinePlan.DineConnect.House;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Addons.ZohoBooks.Implementation
{
    public class ZohoBooksAppService : IZohoBooksAppService
    {

        private const string AddOnName = "ZohoBooks";
        private readonly string _clientId = ConfigurationManager.AppSettings["ZohoBooksClientId"];
        private readonly string _clientSecret = ConfigurationManager.AppSettings["ZohoBooksClientSecret"];
        private readonly IAbpSession _session;
        private readonly IRepository<ConnectAddOn> _addOn;
        private readonly string _dineConnectApplicationUrl = ConfigurationManager.AppSettings["DineConnectUrl"];
        private readonly IRepository<Material> _mRepo;

        public ZohoBooksAppService(IAbpSession session,
             IRepository<ConnectAddOn> addonRepo,
             IRepository<Material> mRepo)
        {
            _mRepo = mRepo;
            _addOn = addonRepo;
            _session = session;
        }

        #region Accounts
        public async Task<List<ComboboxItemDto>> GetAccounts()
        {
            var output = new List<ComboboxItemDto>();

            var token = this.GetZohoBooksTokenForTenant();

            if (token != null)
                foreach (var accTye in AccountZohoBooksCrud.GetAll(token))
                    output.Add(new ComboboxItemDto
                    {
                        Value = accTye.AccountNumber,
                        DisplayText = accTye.AccountNumber + "-" + accTye.AccountName
                    });

            return output;
        }
        #endregion

        public async Task<List<ComboboxItemDto>> GetCustomers()
        {
            var output = new List<ComboboxItemDto>();

            var token = this.GetZohoBooksTokenForTenant();

            if (token != null)
                foreach (var customer in ContactsZohoBooksCrud.GetAll(token))
                    output.Add(new ComboboxItemDto
                    {
                        Value = customer.Id,
                        DisplayText = customer.Id + "-" + customer.ContactName
                    });

            return output;
        }

        private string GetGatewayUrl()
        {
            return ConfigurationManager.AppSettings["DineConnectGatewayUrl"];
        }

        public string GetLoginUrl()
        {
            var gatewayUrl = this.GetGatewayUrl();
            return
               $"{gatewayUrl}/dineconnect?applicationType=zohobooks&dineConnectTenant={_session.TenantId}&applicationUrl={_dineConnectApplicationUrl}";
        }

        public bool IsTokenExists()
        {
            var zohoBooksAddon = _addOn.FirstOrDefault(a => a.Name.Equals(AddOnName));
            if (zohoBooksAddon != null)
            {
                var settings = zohoBooksAddon.Settings;
                var token = JsonConvert.DeserializeObject<ZohoBooksToken>(settings);
                return token != null;
            }

            return false;
        }

        public async Task<bool> KeepTokenComingFromGateway(ZohoBooksToken token)
        {
            token.ExpiresOn = DateTime.UtcNow.AddSeconds(token.expires_in);

            var zohoBooksAddon = await _addOn.FirstOrDefaultAsync(a => a.Name.Equals(AddOnName));
            if (zohoBooksAddon == null)
            {
                zohoBooksAddon = new ConnectAddOn
                {
                    TenantId = _session.GetTenantId(),
                    CreationTime = DateTime.UtcNow,
                    Name = AddOnName,
                    Settings = token.ToJson()
                };
                await _addOn.InsertAndGetIdAsync(zohoBooksAddon);
                return true;
            }

            return false;
        }

        private ZohoBooksToken GetZohoBooksTokenForTenant()
        {
            var zohoBooksAddon = _addOn.FirstOrDefault(a => a.Name.Equals(AddOnName));
            if (zohoBooksAddon != null)
            {
                var settings = zohoBooksAddon.Settings;
                var token = JsonConvert.DeserializeObject<ZohoBooksToken>(settings);

                //Refresh if expired
                if (token.IsExpired) token = RefreshTokenAndSave(token);

                return token;
            }

            return null;
        }

        public ZohoBooksToken RefreshTokenAndSave(ZohoBooksToken token)
        {
            var client = new RestClient("https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer")
            {
                Authenticator = new HttpBasicAuthenticator(_clientId, _clientSecret)
            };

            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddParameter("grant_type", "refresh_token");
            request.AddParameter("refresh_token", token.refresh_token);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var response = (RestResponse)client.Execute(request);

            var qbAddon = _addOn.FirstOrDefault(a => a.Name.Equals(AddOnName));

            var tokenRefreshed = JsonConvert.DeserializeObject<ZohoBooksToken>(response.Content);
            token.ExpiresOn = DateTime.UtcNow.AddSeconds(tokenRefreshed.expires_in);
            token.access_token = tokenRefreshed.access_token;
            token.refresh_token = tokenRefreshed.refresh_token;

            qbAddon.Settings = token.ToJson();
            _addOn.Update(qbAddon);

            return token;
        }

        public bool RemoveToken()
        {
            var zohoBooksAddon = _addOn.FirstOrDefault(a => a.Name.Equals(AddOnName));
            if (zohoBooksAddon != null) _addOn.Delete(zohoBooksAddon);

            return true;
        }

        public async Task SyncItem(int materialId)
        {
            if (materialId > 0)
            {
                var material = await _mRepo.GetAsync(materialId);
                await ZohoBooksMaterialSync(material);
            }
        }

        private async Task ZohoBooksMaterialSync(Material material)
        {
            var addon = new AddOnOutput();
            var add = false;
            if (!string.IsNullOrEmpty(material.AddOn))
            {
                addon = JsonConvert.DeserializeObject<AddOnOutput>(material.AddOn);
                if (string.IsNullOrEmpty(addon.Xero)) add = true;
            }
            else
            {
                add = true;
            }

            var token = this.GetZohoBooksTokenForTenant();

            if (token != null)
            {
                var getCategoryId = GetMaterialCategory(material.MaterialGroupCategoryRefId);

                if (add && getCategoryId > 0)
                {
                    var supp = ZohoBooksItemCrud.CreateMaterial(token, material);
                }
            }
        }

        private int GetMaterialCategory(int materialGroupCategoryRefId)
        {
            return 1;
        }
    }
}
