﻿using System;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Addons.ZohoBooks
{
    public class ZohoBooksToken
    {
        public string organization_id { get; set; }
        public string access_token { get; set; }
        public double expires_in { get; set; }
        public string refresh_token { get; set; }
        public DateTime ExpiresOn { get; set; }

        public string Scope { get; set; }

        [JsonIgnore]
        public bool IsExpired
        {
            get
            {
                return DateTime.UtcNow >= ExpiresOn;
            }
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
