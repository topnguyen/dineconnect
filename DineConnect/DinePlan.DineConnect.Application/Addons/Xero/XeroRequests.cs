﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Addons.Xero
{
    public class XeroRequests
    {
        public static string Account = "https://api.xero.com/api.xro/2.0/Accounts";
        public static string Item = "https://api.xero.com/api.xro/2.0/Items";
        public static string Contact = "https://api.xero.com/api.xro/2.0/Contacts";
        public static string Invoice = "https://api.xero.com/api.xro/2.0/Invoices";
        public static string Payment = "https://api.xero.com/api.xro/2.0/Payments";

    }
}
