﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Addons.Xero.Crud.Token;
using DinePlan.DineConnect.Addons.Xero.Model;
using RestSharp;

namespace DinePlan.DineConnect.Addons.Xero.Crud
{
    public class ContactXeroCrud
    {
        public static List<XeroContact> GetAll(XeroToken token)
        {
            var response =
                TokenXeroCrud.CallXeroApi<string, ContactResponse<List<XeroContact>>>
                    ("", XeroRequests.Contact, token, Method.GET);
            return response.Contacts;
        }
    }
}
