﻿using DinePlan.DineConnect.Addons.Xero.Model;
using DinePlan.DineConnect.House;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Addons.Xero.Crud.Token;

namespace DinePlan.DineConnect.Addons.Xero.Crud
{
    public class ItemXeroCrud
    {
        public static int CreateMaterial(XeroToken token, Material material, int categoryId)
        {
            var xeroMaterial = new XeroItem()
            {
                Code = material.MaterialPetName,
                Name = material.MaterialName
            };
            var response =
                TokenXeroCrud.CallXeroApi<XeroItem, String>
                    (xeroMaterial, XeroRequests.Item, token, Method.POST);
            return 0;
        }

        public static void UpdateMaterial(string settings, Material material, string odoo, int categoryId)
        {
          
        }

     

        public static IEnumerable<XeroItem> GetAll(XeroToken token)
        {
            var response =
                TokenXeroCrud.CallXeroApi<string, ItemResponse<List<XeroItem>>>
                    ("", XeroRequests.Item, token, Method.GET);
            return response.Items;
        }
    }
}
