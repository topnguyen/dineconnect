﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Addons.Xero.Crud.Token;
using DinePlan.DineConnect.Addons.Xero.Model;
using RestSharp;

namespace DinePlan.DineConnect.Addons.Xero.Crud
{
    public class InvoiceXeroCrud
    {
        public static string CreateInvoice(XeroToken token, XeroInvoice invoice)
        {
            var response =
                TokenXeroCrud.CallXeroApi<XeroInvoice, InvoiceResponse<List<XeroInvoice>>>
                    (invoice, XeroRequests.Invoice, token, Method.POST);

            if (response?.Invoices != null && response.Invoices.Any())
            {
                return response.Invoices.First().InvoiceID;
            }
            return null;
        }

        public static string CreatePayment(XeroToken token, XeroPayment payment)
        {
            var response =
                TokenXeroCrud.CallXeroApi<XeroPayment, object>
                    (payment, XeroRequests.Payment, token, Method.PUT);

            return response.ToString();

        }
    }
}
