﻿using DinePlan.DineConnect.Addons.Xero.Model;
using DinePlan.DineConnect.House;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Addons.Xero.Crud.Token;

namespace DinePlan.DineConnect.Addons.Xero.Crud
{
    public class AccountXeroCrud
    {
        public static List<XeroAccount> GetAll(XeroToken token)
        {
            var response =
                TokenXeroCrud.CallXeroApi<string, AccountResponse<List<XeroAccount>>>
                    ("", XeroRequests.Account, token, Method.GET);
            return response.Accounts;
        }
    }
}
