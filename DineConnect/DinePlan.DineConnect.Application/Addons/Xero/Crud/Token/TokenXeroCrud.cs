﻿using System;
using System.Net;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;

namespace DinePlan.DineConnect.Addons.Xero.Crud.Token
{
    public class TokenXeroCrud
    {
        private static bool ValidateResponse(RestResponse response)
        {
            if (response.IsSuccessful)
                return true;
            return false;
        }

        public static SOutputObj CallXeroApi<RequestObj, SOutputObj>(RequestObj input,
            string apiRoute,XeroToken token, Method method)
        {
            SOutputObj result = default;
            var body = JsonConvert.SerializeObject(input, Formatting.None,
                new JsonSerializerSettings {NullValueHandling = NullValueHandling.Ignore});
            var client = new RestClient(apiRoute)
            {
                Authenticator = new JwtAuthenticator(token.access_token)
            };
            var request = new RestRequest(method);
            request.AddHeader("Xero-tenant-id", token.xero_tenant_id);
            request.AddHeader("content-type", "application/json");
            
            if(method!=Method.GET)
                request.AddParameter("application/json", body, ParameterType.RequestBody);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            var response = (RestResponse) client.Execute(request);
            if (ValidateResponse(response))
            {
                result = JsonConvert.DeserializeObject<SOutputObj>(response.Content);
            }
            else
            {
                throw new Exception(response.Content);
            }

            return result;

        }

    }
}
