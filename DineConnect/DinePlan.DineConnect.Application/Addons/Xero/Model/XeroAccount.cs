﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Humanizer;

namespace DinePlan.DineConnect.Addons.Xero.Model
{
    public class XeroAccount    {
        public string AccountID { get; set; } 
        public string Code { get; set; } 
        public string Name { get; set; } 
    }

    public class XeroContact
    {
        public string ContactID { get; set; } 
        public string ContactStatus { get; set; } 
        public string Name { get; set; } 
        public string FirstName { get; set; } 
        public string LastName { get; set; } 
        public string EmailAddress { get; set; } 
        public bool IsSupplier { get; set; } 
        public bool IsCustomer { get; set; } 
    }

    public class XeroTrackingCategory
    {
        public string Name { get; set; }
        public string Option { get; set; }
    }
  
}
