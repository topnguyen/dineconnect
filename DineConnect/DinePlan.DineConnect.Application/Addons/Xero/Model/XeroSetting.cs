﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Addons.Xero.Model
{
    public class XeroSetting
    {
        public string InvoiceContact {get;set;}
        public string SaleItem {get;set;}
        public string TaxItem {get;set;}
        public string DefaultPaymentAccount {get;set;}
        public bool CategoryTracking {get;set;}
        public string CategoryTrackingName { get; set; }
        public string InvoiceStatus { get; set; }
        public bool PaymentTypeTracking { get; set; }
        public bool DepartmentTracking { get; set; }
        public bool TaxAvailable {get;set;}
        public bool CreateSeparateInvoice {get;set;}

        public List<XeroPaymentTracking> PaymentTrackings { get; set; }
        public List<XeroDepartmentTracking> DepartmentTrackings { get; set; }

        public XeroSetting()
        {
            InvoiceStatus = "AUTHORISED";
            PaymentTrackings = new List<XeroPaymentTracking>();
            DepartmentTrackings = new List<XeroDepartmentTracking>();
            

        }

    }

    public class XeroPaymentTracking
    {
        public int PaymentTypeId { get; set; }
        public string PaymentAccount {get;set;}


    }

    public class XeroDepartmentTracking
    {
        public int DepartmentId { get; set; }
        public string SaleItem {get;set;}


    }
}
