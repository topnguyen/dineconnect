﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Addons.Xero.Implementation
{
    public class XeroConnection
    {
        public string TenantId { get; set; }
        public string TenantType { get; set; }
        public string TenantName { get; set; }
    }
}
