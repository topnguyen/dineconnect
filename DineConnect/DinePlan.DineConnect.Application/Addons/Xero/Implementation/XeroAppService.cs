﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Session;
using DinePlan.DineConnect.Addon;
using DinePlan.DineConnect.Addons.Dto;
using DinePlan.DineConnect.Addons.Xero.Crud;
using DinePlan.DineConnect.Addons.Xero.Model;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Audit;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Period;
using DinePlan.DineConnect.Connect.WorkPeriod.Dtos;
using DinePlan.DineConnect.House;
using DinePlan.DineConnect.Job.Connect;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;

namespace DinePlan.DineConnect.Addons.Xero.Implementation
{
    public class XeroAppService : IXeroAppService
    {
        private const string AddOnName = "Xero";
        private readonly IRepository<ConnectAddOn> _addOn;
        private readonly string _clientId = ConfigurationManager.AppSettings["XeroClientId"];
        private readonly string _clientSecret = ConfigurationManager.AppSettings["XeroClientSecret"];
        private readonly string _dineConnectApplicationUrl = "http://localhost:7301";
        private readonly IRepository<ExternalLog> _logRepository;
        private readonly IRepository<Location> _lRepo;
        private readonly IRepository<Material> _mRepo;
        private readonly IAbpSession _session;
        private readonly IRepository<WorkPeriod> _wpRepo;

        public XeroAppService(IRepository<ConnectAddOn> addOn, ISettingManager settingManager,
            IRepository<WorkPeriod> wpRepo, IRepository<ExternalLog> logRepository,
            IRepository<Location> lRepo,
            IRepository<Material> mRepo,
            IAbpSession session)
        {
            _mRepo = mRepo;
            _session = session;
            _addOn = addOn;
            var siteRootFormat = settingManager.GetSettingValue(AppSettings.General.WebSiteRootAddress)
                .EnsureEndsWith('/');
            if (!string.IsNullOrEmpty(siteRootFormat)) _dineConnectApplicationUrl = siteRootFormat;
            _wpRepo = wpRepo;
            _lRepo = lRepo;
            _logRepository = logRepository;
        }

        #region Accounts

        public async Task<List<ComboboxItemDto>> GetAccounts()
        {
            try
            {
                var output = new List<ComboboxItemDto>();
                var token = GetXeroTokenForTenant();
                if (token != null)
                    foreach (var accTye in AccountXeroCrud.GetAll(token))
                        output.Add(new ComboboxItemDto
                        {
                            Value = accTye.Code,
                            DisplayText = accTye.Code + "-" + accTye.Name
                        });
                return output;
            }
            catch (Exception e)
            {
                LogXero(e);
            }

            return null;
        }

        #endregion

        #region Sales

        public void SyncSales(ConnectJobArgs wpObject)
        {
            StringBuilder builder = new StringBuilder();
            WorkPeriod workPeriod = null;
            try
            {

                if (wpObject == null)
                    return;

                var workPeriodId = wpObject.WorkPeriodId;
                if (workPeriodId == 0)
                    return;
                var wpQuery = _wpRepo.GetAll().Where(a =>
                    a.Id == wpObject.WorkPeriodId && a.TenantId == wpObject.TenantId);
                workPeriod = wpQuery.ToList().LastOrDefault();

                if (workPeriod == null)
                    return;

                var xeroAddon = _addOn.FirstOrDefault(a =>
                    a.Name.Equals(AddOnName) && !a.IsDeleted && a.TenantId == wpObject.TenantId);
                if (xeroAddon != null)
                {
                    var settings = xeroAddon.AddOnSettings;
                    if (!string.IsNullOrEmpty(settings))
                    {
                        var mySetting = JsonConvert.DeserializeObject<XeroSetting>(settings);
                        var myToken = JsonConvert.DeserializeObject<XeroToken>(xeroAddon.Settings);
                        if (myToken.IsExpired)
                            myToken = RefreshTokenAndSave(myToken);

                        if (workPeriodId > 0)
                        {
                           
                            if (workPeriod.LocationId > 0 && workPeriod.TotalSales > 0M)
                            {
                                var addInvoice = false;
                                if (!string.IsNullOrEmpty(workPeriod.AddOn))
                                {
                                    var addOn = JsonConvert.DeserializeObject<AddOnOutput>(workPeriod.AddOn);
                                    if (addOn != null)
                                        if (string.IsNullOrEmpty(addOn.Xero))
                                            addInvoice = true;
                                }
                                else
                                {
                                    addInvoice = true;
                                }

                                if (addInvoice)
                                {
                                    var location = _lRepo.Get(workPeriod.LocationId);


                                    if (mySetting.CreateSeparateInvoice)
                                    {
                                        if (!string.IsNullOrEmpty(workPeriod.DepartmentTicketInformations) &&
                                            mySetting.DepartmentTrackings.Any())
                                        {
                                            var myDepExtract =
                                                JsonConvert.DeserializeObject<List<WorkPeriodDepartmentDto>>(
                                                    workPeriod.DepartmentTicketInformations);

                                            if (myDepExtract != null && myDepExtract.Any())
                                            {

                                                List<WorkPeriodDepartmentPayment> myPayments =
                                                    GetWorkPeriodPayments(myDepExtract);
                                                    

                                                foreach (var periodDepartmentDto in myPayments)
                                                {
                                                    var myTrackSetting =
                                                        mySetting.PaymentTrackings.LastOrDefault(a =>
                                                            a.PaymentTypeId == periodDepartmentDto.PaymentTypeSyncId);

                                                    var myAccountId = mySetting.DefaultPaymentAccount;

                                                    if (myTrackSetting != null)
                                                    {
                                                        myAccountId = myTrackSetting.PaymentAccount;
                                                    }


                                                    var invoice = new XeroInvoice
                                                    {
                                                        Date = workPeriod.StartTime.Date,
                                                        DueDate = workPeriod.EndTime.Date,
                                                        Type = "ACCREC",
                                                        Contact = new XeroInvoiceContact
                                                        {
                                                            ContactID = myAccountId
                                                        },
                                                        Status = mySetting.InvoiceStatus
                                                    };

                                                    foreach (var myDepartment in periodDepartmentDto.Departments)
                                                    {
                                                        var mySaleItem = mySetting.SaleItem;

                                                        var myDeptTrack =
                                                            mySetting.DepartmentTrackings.LastOrDefault(a =>
                                                                a.DepartmentId == myDepartment.DepartmentSyncId);


                                                        if (myDeptTrack != null)
                                                        {
                                                            mySaleItem = myDeptTrack.SaleItem;
                                                        }


                                                        var invoiceItem = new XeroInvoiceLineItem
                                                        {
                                                            ItemCode = mySaleItem,
                                                            Description = "Sale: " + location.Name + " Department "+
                                                                          myDepartment.DepartmentName,
                                                            Quantity = "1",
                                                            UnitAmount = myDepartment.TotalAmount.ToString(),
                                                            TaxType = mySetting.TaxAvailable ? "OUTPUT" : "INPUT"
                                                        };

                                                        if (mySetting.CategoryTracking)
                                                            invoiceItem.Tracking = new List<XeroInvoiceTracking>
                                                            {
                                                                new XeroInvoiceTracking()
                                                                {
                                                                    Name = mySetting.CategoryTrackingName,
                                                                    Option = location.Code
                                                                }
                                                            };
                                                        invoice.LineItems.Add(invoiceItem);
                                                    }


                                                    if (invoice.LineItems.Any())
                                                    {
                                                        var invoiceNo = InvoiceXeroCrud.CreateInvoice(myToken, invoice);
                                                        if (!string.IsNullOrEmpty(invoiceNo))
                                                        {
                                                            if (builder.Length > 0)
                                                            {
                                                                builder.Append(",");
                                                            }
                                                            builder.Append(invoiceNo);
                                                        }
                                                    }
                                                }

                                                if (builder.Length>0)
                                                {
                                                    var output = new AddOnOutput
                                                    {
                                                        Xero = builder.ToString()
                                                    };

                                                    workPeriod.AddOn = JsonConvert.SerializeObject(output);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var invoice = new XeroInvoice
                                            {
                                                Date = workPeriod.StartTime.Date,
                                                DueDate = workPeriod.EndTime.Date,
                                                Type = "ACCREC",
                                                Contact = new XeroInvoiceContact
                                                {
                                                    ContactID = mySetting.DefaultPaymentAccount
                                                },
                                                Status = mySetting.InvoiceStatus
                                            };

                                            var invoiceItem = new XeroInvoiceLineItem
                                            {
                                                ItemCode = mySetting.SaleItem,
                                                Description = "Sale: " + location.Name + " / Count: " +
                                                              workPeriod.TotalTicketCount,
                                                Quantity = "1",
                                                UnitAmount = workPeriod.TotalSales.ToString(),
                                                TaxType = mySetting.TaxAvailable ? "OUTPUT" : "INPUT"
                                            };

                                            if (mySetting.CategoryTracking)
                                                invoiceItem.Tracking = new List<XeroInvoiceTracking>
                                                {
                                                    new XeroInvoiceTracking()
                                                    {
                                                        Name = mySetting.CategoryTrackingName,
                                                        Option = location.Code
                                                    }
                                                };
                                            invoice.LineItems.Add(invoiceItem);

                                            var invoiceNo = InvoiceXeroCrud.CreateInvoice(myToken, invoice);
                                            if (invoiceNo != null)
                                            {
                                                var output = new AddOnOutput
                                                {
                                                    Xero = invoiceNo
                                                };

                                                workPeriod.AddOn = JsonConvert.SerializeObject(output);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        var invoice = new XeroInvoice
                                        {
                                            Date = workPeriod.StartTime.Date,
                                            DueDate = workPeriod.EndTime.Date,
                                            Type = "ACCREC",
                                            Contact = new XeroInvoiceContact
                                            {
                                                ContactID = mySetting.InvoiceContact
                                            },
                                            Status = mySetting.InvoiceStatus
                                        };
                                        if (!mySetting.DepartmentTracking)
                                        {
                                            var invoiceItem = new XeroInvoiceLineItem
                                            {
                                                ItemCode = mySetting.SaleItem,
                                                Description = "Sale: " + location.Name + " / Count: " +
                                                              workPeriod.TotalTicketCount,
                                                Quantity = "1",
                                                UnitAmount = workPeriod.TotalSales.ToString(),
                                                TaxType = mySetting.TaxAvailable ? "OUTPUT" : "INPUT"
                                            };

                                            if (mySetting.CategoryTracking)
                                                invoiceItem.Tracking = new List<XeroInvoiceTracking>
                                                {
                                                    new XeroInvoiceTracking()
                                                    {
                                                        Name = mySetting.CategoryTrackingName,
                                                        Option = location.Code
                                                    }
                                                };
                                            invoice.LineItems.Add(invoiceItem);
                                        }
                                        else
                                        {
                                            if (!string.IsNullOrEmpty(workPeriod.DepartmentTicketInformations) &&
                                                mySetting.DepartmentTrackings.Any())
                                            {
                                                var myDepExtract =
                                                    JsonConvert.DeserializeObject<List<WorkPeriodDepartmentDto>>(
                                                        workPeriod.DepartmentTicketInformations);

                                                if (myDepExtract != null && myDepExtract.Any())
                                                    foreach (var periodDepartmentDto in myDepExtract)
                                                    {
                                                        var myTrackSetting =
                                                            mySetting.DepartmentTrackings.LastOrDefault(a =>
                                                                a.DepartmentId == periodDepartmentDto.DepartmentSyncId);

                                                        if (myTrackSetting != null)
                                                        {
                                                            var invoiceItem = new XeroInvoiceLineItem
                                                            {
                                                                ItemCode = myTrackSetting.SaleItem,
                                                                Description =
                                                                    periodDepartmentDto.DepartmentName + " / Count: " +
                                                                    periodDepartmentDto.TotalTickets,
                                                                Quantity = "1",
                                                                UnitAmount = periodDepartmentDto.TotalAmount.ToString(),
                                                                TaxType = mySetting.TaxAvailable ? "OUTPUT" : "INPUT"
                                                            };
                                                            if (mySetting.CategoryTracking)
                                                                invoiceItem.Tracking = new List<XeroInvoiceTracking>
                                                                {
                                                                    new XeroInvoiceTracking()
                                                                    {
                                                                        Name = mySetting.CategoryTrackingName,
                                                                        Option = location.Code
                                                                    }
                                                                };
                                                            invoice.LineItems.Add(invoiceItem);
                                                        }
                                                        else
                                                        {
                                                            if (!string.IsNullOrEmpty(mySetting.SaleItem))
                                                            {
                                                                var invoiceItem = new XeroInvoiceLineItem
                                                                {
                                                                    ItemCode = mySetting.SaleItem,
                                                                    Description = "Sale: " + location.Name +
                                                                        " / Count: " +
                                                                        workPeriod.TotalTicketCount,
                                                                    Quantity = "1",
                                                                    UnitAmount = workPeriod.TotalSales.ToString(),
                                                                    TaxType = mySetting.TaxAvailable
                                                                        ? "OUTPUT"
                                                                        : "INPUT"
                                                                };
                                                                if (mySetting.CategoryTracking)
                                                                    invoiceItem.Tracking = new List<XeroInvoiceTracking>
                                                                    {
                                                                        new XeroInvoiceTracking()
                                                                        {
                                                                            Name = mySetting.CategoryTrackingName,
                                                                            Option = location.Code
                                                                        }
                                                                    };
                                                                invoice.LineItems.Add(invoiceItem);
                                                            }
                                                            else
                                                            {
                                                                throw new Exception("No Department Mapping : " +
                                                                    periodDepartmentDto.DepartmentName);
                                                            }
                                                        }
                                                    }
                                            }
                                            else
                                            {
                                                if (!string.IsNullOrEmpty(mySetting.SaleItem))
                                                {
                                                    var invoiceItem = new XeroInvoiceLineItem
                                                    {
                                                        ItemCode = mySetting.SaleItem,
                                                        Description = "Sale: " + location.Name + " / Count: " +
                                                                      workPeriod.TotalTicketCount,
                                                        Quantity = "1",
                                                        UnitAmount = workPeriod.TotalSales.ToString(),
                                                        TaxType = mySetting.TaxAvailable ? "OUTPUT" : "INPUT"
                                                    };
                                                    if (mySetting.CategoryTracking)
                                                        invoiceItem.Tracking = new List<XeroInvoiceTracking>
                                                        {
                                                            new XeroInvoiceTracking()
                                                            {
                                                                Name = mySetting.CategoryTrackingName,
                                                                Option = location.Code
                                                            }
                                                        };
                                                    invoice.LineItems.Add(invoiceItem);
                                                }
                                                else
                                                {
                                                    throw new Exception("No Departments  " +
                                                                        "in the Settings or WorkPeriod does not have Ticket Department Information. WorkPeriod Id is" +
                                                                        workPeriod.Id);
                                                }
                                            }
                                        }

                                        var invoiceNo = InvoiceXeroCrud.CreateInvoice(myToken, invoice);
                                        if (invoiceNo != null)
                                        {
                                            if (!string.IsNullOrEmpty(mySetting.InvoiceStatus) &&
                                                mySetting.InvoiceStatus.Equals("AUTHORISED"))
                                            {
                                                if (!mySetting.PaymentTypeTracking)
                                                {
                                                    var payment = new XeroPayment
                                                    {
                                                        Account = new XeroAccount
                                                        {
                                                            Code = mySetting.DefaultPaymentAccount
                                                        },
                                                        Amount = workPeriod.TotalSales,
                                                        Date = DateTime.Now.Date.ToString("yyyy-MM-dd"),
                                                        Invoice = new XeroInvoiceInput
                                                        {
                                                            InvoiceID = invoiceNo
                                                        }
                                                    };
                                                    InvoiceXeroCrud.CreatePayment(myToken, payment);
                                                }
                                                else
                                                {
                                                    if (!string.IsNullOrEmpty(workPeriod.WorkPeriodInformations) &&
                                                        mySetting.PaymentTrackings.Any())
                                                    {
                                                        var allWorkShifts =
                                                            JsonConvert.DeserializeObject<List<WorkShiftDto>>(
                                                                workPeriod.WorkPeriodInformations);

                                                        foreach (var workTimePaymentInformationDto in allWorkShifts
                                                            .SelectMany(a => a.PaymentInfos)
                                                            .GroupBy(a => a.PaymentType))
                                                        {
                                                            var totalAmount =
                                                                workTimePaymentInformationDto.Sum(a => a.Actual);

                                                            if (totalAmount > 0)
                                                            {
                                                                var myTrackSetting =
                                                                    mySetting.PaymentTrackings.LastOrDefault(a =>
                                                                        a.PaymentTypeId ==
                                                                        workTimePaymentInformationDto.Key);
                                                                if (myTrackSetting != null)
                                                                {
                                                                    var payment = new XeroPayment
                                                                    {
                                                                        Account = new XeroAccount
                                                                        {
                                                                            Code = myTrackSetting.PaymentAccount
                                                                        },
                                                                        Amount = workTimePaymentInformationDto.Sum(a =>
                                                                            a.Actual),
                                                                        Date = DateTime.Now.Date.ToString("yyyy-MM-dd"),
                                                                        Invoice = new XeroInvoiceInput
                                                                        {
                                                                            InvoiceID = invoiceNo
                                                                        }
                                                                    };
                                                                    InvoiceXeroCrud.CreatePayment(myToken, payment);
                                                                }
                                                                else
                                                                {
                                                                    if (!string.IsNullOrEmpty(mySetting
                                                                        .DefaultPaymentAccount))
                                                                    {
                                                                        var payment = new XeroPayment
                                                                        {
                                                                            Account = new XeroAccount
                                                                            {
                                                                                Code = mySetting.DefaultPaymentAccount
                                                                            },
                                                                            Amount = workPeriod.TotalSales,
                                                                            Date = DateTime.Now.Date.ToString(
                                                                                "yyyy-MM-dd"),
                                                                            Invoice = new XeroInvoiceInput
                                                                            {
                                                                                InvoiceID = invoiceNo
                                                                            }
                                                                        };
                                                                        InvoiceXeroCrud.CreatePayment(myToken, payment);
                                                                    }
                                                                    else
                                                                    {
                                                                        throw new Exception(
                                                                            "No PaymentType Mapping : " +
                                                                            workTimePaymentInformationDto.Key);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        throw new Exception("No Payment Types  " +
                                                                            "in the Settings or WorkPeriod does not have Payment Information. WorkPeriod Id is" +
                                                                            workPeriod.Id);
                                                    }
                                                }
                                            }

                                            var output = new AddOnOutput
                                            {
                                                Xero = invoiceNo
                                            };

                                            workPeriod.AddOn = JsonConvert.SerializeObject(output);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                if (builder.Length > 0)
                {
                    if (workPeriod != null)
                    {
                        workPeriod.AddOn = builder.ToString();
                    }
                }
                LogXero(e,workPeriod!=null?"WorkPeriod Id: " + workPeriod.Id.ToString():"");
            }
        }

        private List<WorkPeriodDepartmentPayment> GetWorkPeriodPayments(List<WorkPeriodDepartmentDto> myDepExtract)
        {
            List<WorkPeriodDepartmentPayment> payments = new List<WorkPeriodDepartmentPayment>();

            foreach (var deptExtract in myDepExtract)
            {
                foreach (var myPayment in deptExtract.Payments)
                {
                    var deptPayment = payments.LastOrDefault(a => a.PaymentTypeId == myPayment.PaymentTypeId);
                    if (deptPayment == null)
                    {
                        deptPayment = new WorkPeriodDepartmentPayment()
                        {
                            PaymentTypeId = myPayment.PaymentTypeId,
                            PaymentTypeSyncId = myPayment.PaymentTypeSyncId,
                            PaymentTypeName = myPayment.PaymentTypeName
                        };
                        payments.Add(deptPayment);
                    }

                    deptPayment.Departments.Add(new WorkPeriodDepartmentDto()
                    {
                        DepartmentId = deptExtract.DepartmentId,
                        DepartmentSyncId = deptExtract.DepartmentSyncId,
                        DepartmentName = deptExtract.DepartmentName,
                        TotalAmount = myPayment.PaymentAmount
                    });
                }
            }

            return payments;
        }

        public void SyncNonSales()
        {
            try
            {
                var myWorkPeriod = _wpRepo.GetAll().Where(a => string.IsNullOrEmpty(a.AddOn) && a.TotalSales>0 && !string.IsNullOrEmpty(a.DepartmentTicketInformations) && !string.IsNullOrEmpty(a.WorkPeriodInformations)).OrderByDescending(a=>a.Id).ToList();


                foreach (var workPeriod in myWorkPeriod.ToList())
                {
                    LogXero(null, "Starting for WorkPeriod : " + workPeriod.Id);
                    SyncSales(new ConnectJobArgs
                    {
                        WorkPeriodId = workPeriod.Id,
                        TenantId = workPeriod.TenantId
                    });
                }
            }
            catch (Exception e)
            {
                LogXero(e);
            }
        }

        #endregion

        #region Token

        public XeroToken RefreshTokenAndSave(XeroToken token)
        {
            try
            {
                var client = new RestClient("https://identity.xero.com/connect/token")
                {
                    Authenticator = new HttpBasicAuthenticator(_clientId, _clientSecret)
                };
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/x-www-form-urlencoded");
                request.AddParameter("application/x-www-form-urlencoded",
                    $"grant_type=refresh_token&refresh_token={token.refresh_token}", ParameterType.RequestBody);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                var response = (RestResponse) client.Execute(request);


                var refreshedToken = JsonConvert.DeserializeObject<XeroToken>(response.Content);
                if (refreshedToken != null && !string.IsNullOrEmpty(refreshedToken.access_token))
                {
                    var xeroAddon = _addOn.FirstOrDefault(a => a.Name.Equals(AddOnName) && !a.IsDeleted);
                    if (xeroAddon != null)
                    {
                        refreshedToken.ExpiresOn = DateTime.UtcNow.AddSeconds(token.expires_in);
                        refreshedToken.xero_tenant_id = token.xero_tenant_id;
                        xeroAddon.Settings = refreshedToken.ToJson();
                        _addOn.Update(xeroAddon);
                    }
                }

                return token;
            }
            catch (Exception e)
            {
                LogXero(e);
            }

            return null;
        }


        public bool IsTokenExists()
        {
            try
            {
                var xeroAddon = _addOn.FirstOrDefault(a => a.Name.Equals(AddOnName) && !a.IsDeleted);
                if (xeroAddon != null)
                {
                    var settings = xeroAddon.Settings;
                    var token = JsonConvert.DeserializeObject<XeroToken>(settings);
                    if (token != null && token.IsExpired) RefreshTokenAndSave(token);
                    return token != null;
                }

                return false;
            }
            catch (Exception e)
            {
                LogXero(e);
            }

            return false;
        }

        public bool RemoveToken()
        {
            var xeroAddon = _addOn.FirstOrDefault(a => a.Name.Equals(AddOnName));
            if (xeroAddon != null) _addOn.Delete(xeroAddon);
            return true;
        }


        /// <summary>
        ///     Gets the current xero token, refreshes if expired
        /// </summary>
        /// <returns></returns>
        private XeroToken GetXeroTokenForTenant()
        {
            try
            {
                var xeroAddon = _addOn.FirstOrDefault(a => a.Name.Equals(AddOnName) && !a.IsDeleted);
                if (xeroAddon != null)
                {
                    var settings = xeroAddon.Settings;
                    var token = JsonConvert.DeserializeObject<XeroToken>(settings);

                    //Refresh if expired
                    if (token.IsExpired) token = RefreshTokenAndSave(token);

                    return token;
                }
            }
            catch (Exception e)
            {
                LogXero(e);
            }

            return null;
        }

        #endregion

        #region Customers

        public async Task<List<ComboboxItemDto>> GetCustomers()
        {
            try
            {
                var output = new List<ComboboxItemDto>();

                var token = GetXeroTokenForTenant();
                if (token != null)
                    foreach (var accTye in ContactXeroCrud.GetAll(token))
                    {
                        var dText = accTye.Name;
                        if (string.IsNullOrEmpty(dText)) dText = accTye.FirstName + " " + accTye.LastName;
                        output.Add(new ComboboxItemDto
                        {
                            Value = accTye.ContactID,
                            DisplayText = dText
                        });
                    }

                return output;
            }
            catch (Exception e)
            {
                LogXero(e);
            }

            return null;
        }


        public string GetGatewayUrl()
        {
            return ConfigurationManager.AppSettings["DineConnectGatewayUrl"];
        }

        public string GetLoginUrl()
        {
            var gatewayUrl = GetGatewayUrl();
            return
                $"{gatewayUrl}/dineconnect?applicationType=xero&dineConnectTenant={_session.TenantId}&applicationUrl={_dineConnectApplicationUrl}";
        }

        public async Task<bool> KeepTokenComingFromGateway(XeroToken token)
        {
            token.ExpiresOn = DateTime.UtcNow.AddSeconds(token.expires_in);

            var xeroAddOn = await _addOn.FirstOrDefaultAsync(a => a.Name.Equals(AddOnName) && !a.IsDeleted);
            if (xeroAddOn == null)
            {
                xeroAddOn = new ConnectAddOn
                {
                    TenantId = _session.GetTenantId(),
                    CreationTime = DateTime.UtcNow,
                    Name = AddOnName,
                    Settings = token.ToJson()
                };
                await _addOn.InsertAndGetIdAsync(xeroAddOn);
                return true;
            }

            return false;
        }

        #endregion

        #region XeroSetting

        public async Task<XeroSetting> GetXeroSetting()
        {
            var xeroSetting = new XeroSetting();
            var xeroAddon = await _addOn.FirstOrDefaultAsync(a => a.Name.Equals(AddOnName) && !a.IsDeleted);
            if (xeroAddon != null)
            {
                var settings = xeroAddon.AddOnSettings;
                if (!string.IsNullOrEmpty(settings)) return JsonConvert.DeserializeObject<XeroSetting>(settings);
            }

            return xeroSetting;
        }

        public async Task UpdateXeroSetting(XeroSetting xeroSetting)
        {
            var xeroAddon = await _addOn.FirstOrDefaultAsync(a => a.Name.Equals(AddOnName) && !a.IsDeleted);
            if (xeroAddon != null) xeroAddon.AddOnSettings = JsonConvert.SerializeObject(xeroSetting);
        }

        #endregion

        #region Error

        private void LogXero(Exception e, string message ="")
        {
            var myLog = new ExternalLog
            {
                AuditType = (int) ExternalLogType.Xero,
                LogData = e!=null?e.Message+" : " +message : message,
                LogDescription = e!=null ? FlattenException(e) : "",
                LogTime = DateTime.Now,
                ExternalLogTrunc = DateTime.Now.Date
            };
            _logRepository.InsertAndGetId(myLog);
        }

        private static string FlattenException(Exception exception)
        {
            var stringBuilder = new StringBuilder();

            while (exception != null)
            {
                stringBuilder.AppendLine(exception.Message);
                stringBuilder.AppendLine(exception.StackTrace);

                exception = exception.InnerException;
            }

            return stringBuilder.ToString();
        }

        #endregion

        #region Items

        public async Task SyncItems()
        {
            if (IsTokenExists())
                foreach (var material in await _mRepo.GetAllListAsync(a => a.Id > 0))
                    await SyncItem(material.Id);
        }

        public async Task SyncItem(int materialId)
        {
            if (materialId > 0)
            {
                var material = await _mRepo.GetAsync(materialId);
                await XeroMaterialSync(material);
            }
        }

        private async Task XeroMaterialSync(Material material)
        {
            var addon = new AddOnOutput();
            var add = false;
            if (!string.IsNullOrEmpty(material.AddOn))
            {
                addon = JsonConvert.DeserializeObject<AddOnOutput>(material.AddOn);
                if (string.IsNullOrEmpty(addon.Xero)) add = true;
            }
            else
            {
                add = true;
            }

            var token = GetXeroTokenForTenant();
            if (token != null)
            {
                var getCategoryId = GetMaterialCategory(material.MaterialGroupCategoryRefId);

                if (add && getCategoryId > 0)
                {
                    var supp = ItemXeroCrud.CreateMaterial(token, material, getCategoryId);
                }
            }
        }

        private int GetMaterialCategory(int materialGroupCategoryRefId)
        {
            return 1;
        }

        public async Task<List<ComboboxItemDto>> GetItems()
        {
            try
            {
                var output = new List<ComboboxItemDto>();

                var token = GetXeroTokenForTenant();
                if (token != null)
                    foreach (var accTye in ItemXeroCrud.GetAll(token))
                        output.Add(new ComboboxItemDto
                        {
                            Value = accTye.Code,
                            DisplayText = accTye.Name
                        });
                return output;
            }
            catch (Exception e)
            {
                LogXero(e);
            }

            return null;
        }

        #endregion
    }
}