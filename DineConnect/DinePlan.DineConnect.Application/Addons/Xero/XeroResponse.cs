﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Addons.Xero
{
    public class XeroResponse<T>
    {

        public string Id { get; set; }
        public string Status { get; set; }
        public string ProviderName { get; set; }
        public string DateTimeUTC { get; set; }

    }

    public class ItemResponse<T> : XeroResponse<T>
    {
        public T Items { get; set; }

    }

    public class ContactResponse<T> : XeroResponse<T>
    {
        public T Contacts { get; set; }

    }

    public class AccountResponse<T> : XeroResponse<T>
    {
        public T Accounts { get; set; }

    }

    public class InvoiceResponse<T> : XeroResponse<T>
    {
        public T Invoices { get; set; }

    }
}
