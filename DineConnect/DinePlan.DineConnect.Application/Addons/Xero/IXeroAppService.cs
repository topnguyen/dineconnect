﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Addons.Qoyod.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Addons.Xero.Model;
using DinePlan.DineConnect.External.Delivery.Dto;
using DinePlan.DineConnect.Job.Connect;

namespace DinePlan.DineConnect.Addons.Xero
{
    public interface IXeroAppService : IApplicationService
    {
        string GetLoginUrl();
        bool RemoveToken();
        bool IsTokenExists();
        Task<bool> KeepTokenComingFromGateway(XeroToken token);
        XeroToken RefreshTokenAndSave(XeroToken token);
        Task<XeroSetting> GetXeroSetting();
        Task UpdateXeroSetting(XeroSetting xeroSetting);
        Task SyncItems();
        Task SyncItem(int materialId);
        Task<List<ComboboxItemDto>> GetItems();
        Task<List<ComboboxItemDto>> GetAccounts();
        Task<List<ComboboxItemDto>> GetCustomers();
        void SyncSales(ConnectJobArgs wpObject);
        void SyncNonSales();

    }
}
