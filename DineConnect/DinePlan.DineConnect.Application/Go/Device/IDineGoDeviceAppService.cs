﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Go.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Device.Dtos;
using DinePlan.DineConnect.Go.PaymentTypes.Dtos;

namespace DinePlan.DineConnect.Go.Device
{
    public interface IDineGoDeviceAppService : IApplicationService
    {
        Task<PagedResultOutput<DineGoDeviceListDto>> GetAll(GetDineGoDeviceInput inputDto);
        Task<FileDto> GetAllToExcel(GetDineGoDeviceInput input);
        Task<GetDineGoDeviceForEditOutput> GetDineGoDeviceForEdit(NullableIdInput nullableIdInput);
        Task<IdInput> CreateOrUpdateDineGoDevice(CreateOrUpdateDineGoDeviceInput input);
        Task DeleteDineGoDevice(IdInput input);
        Task<List<DineGoBrandListDto>> GetDineGoBrandForCombobox();
        Task<List<DineGoPaymentTypeListDto>> GetDineGoPaymentTypeForCombobox();
        Task<List<ComboboxItemDto>> ApiGetAllDevices();
        Task<ApiGoDevice> ApiGetDeviceDetail(IdInput input);

    }
}
