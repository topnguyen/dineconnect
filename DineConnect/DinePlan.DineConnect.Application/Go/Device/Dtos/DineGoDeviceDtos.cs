﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Dtos;
using DinePlan.DineConnect.Go.PaymentTypes.Dtos;

namespace DinePlan.DineConnect.Go.Device.Dtos
{
    [AutoMapFrom(typeof(DineGoDevice))]
    public class DineGoDeviceListDto : FullAuditedEntityDto
    {
        //TODO: DTO DineGoDevice Properties Missing
        public string Name { get; set; }
        public int IdleTime { get; set; }
        public string OrderLink { get; set; }
        public string PinCode { get; set; }
        public string ThemeSettings { get; set; }
        public Guid ChangeGuid { get; set; }
    }
    [AutoMapTo(typeof(DineGoDevice))]
    public class DineGoDeviceEditDto
    {
        public DineGoDeviceEditDto()
        {
            DineGoBrands = new Collection<DineGoBrandEditDto>();
            DineGoPaymentTypes =new  Collection<DineGoPaymentTypeEditDto>();
        }
        public int? Id { get; set; }
        public string Name { get; set; }
        public int IdleTime { get; set; }
        public string OrderLink { get; set; }
        public string PinCode { get; set; }
        public bool PushToConnect{ get; set; }
        public string ThemeSettings { get; set; }
        public Guid ChangeGuid { get; set; }
        public bool RefreshImage { get; set; }
        public Collection<DineGoBrandEditDto> DineGoBrands { get; set; }
        public Collection<DineGoPaymentTypeEditDto> DineGoPaymentTypes { get; set; }
    }
    public class DeviceThemeSettingsDto
    {
        public string IdleContents { get; set; }
        public string BannerContents { get; set; }
        public string BackgroundColor { get; set; }
        public string Logo { get; set; }
    }
    public class GetDineGoDeviceInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }
    public class GetDineGoDeviceForEditOutput : IOutputDto
    {
        public DineGoDeviceEditDto DineGoDevice { get; set; }
        public DeviceThemeSettingsDto DeviceThemeSettings { get; set; }
    }
    public class CreateOrUpdateDineGoDeviceInput : IInputDto
    {
        [Required]
        public DineGoDeviceEditDto DineGoDevice { get; set; }
        public DeviceThemeSettingsDto DeviceThemeSettings { get; set; }
    }
   
    public class DineGoDeviceBrandDetailEditDto : FullAuditedEntityDto
    {
        public int? Id { get; set; }
        public int DineGoDeviceId { get; set; }
        public int DineGoBrandId { get; set; }
        public string DineGoBrandName { get; set; }
    }
    
    public class DineGoDevicePaymentTypeDetailEditDto : FullAuditedEntityDto
    {
        public int? Id { get; set; }
        public int DineGoDeviceId { get; set; }
        public int DineGoPaymentTypeId { get; set; }
        public string DineGoPaymentTypeName { get; set; }
    }
    public class CreateDineGoDeviceBrandDetail : IInputDto
    {
        [Required]
        public DineGoDeviceBrandDetailEditDto DineGoDeviceBrandDetailEditDto { get; set; }
    }
    public class CreateDineGoDevicePaymentTypeDetail : IInputDto
    {
        [Required]
        public DineGoDevicePaymentTypeDetailEditDto DineGoDevicePaymentTypeDetailEditDto { get; set; }
    }


    [AutoMapTo(typeof(DineGoDevice))]
    public class ApiGoDevice
    {
        public ApiGoDevice()
        {
            DineGoBrands = new List<ApiGoBrand>();
            DineGoPaymentTypes = new List<ApiGoPaymentType>();
        }
        public int? Id { get; set; }
        public string Name { get; set; }
        public string BannerImage { get; set; }
        public string AdsImage { get; set; }
        public int IdleTime { get; set; }
        public string ThemeSettings { get; set; }
        public string PinCode { get; set; }

        public List<ApiGoBrand> DineGoBrands { get; set; }
        public List<ApiGoPaymentType> DineGoPaymentTypes { get; set; }
        public string OrderLink { get; set; }
    }

    [AutoMapTo(typeof(DineGoPaymentType))]
    public class ApiGoPaymentType
    {
        public string Label { get; set; }

        [ForeignKey("PaymentTypeId")]
        public PaymentType PaymentType { get; set; }
        public virtual int PaymentTypeId { get; set; }
    }
    
    [AutoMapTo(typeof(DineGoBrand))]
    public class ApiGoBrand
    {
        public string Label { get; set; }
        public virtual int LocationId { get; set; }
        public List<ApiGoDepartment> DineGoDepartments { get; set; }
        public virtual int ScreenMenuId { get; set; }
    }

    [AutoMapTo(typeof(DineGoDepartment))]
    public class ApiGoDepartment
    {
        public string Label { get; set; }
        public virtual int DepartmentId { get; set; }
        public virtual string DepartmentName { get; set; }
        public List<ApiGoCharge> DineGoCharges { get; set; }
    }

    [AutoMapTo(typeof(DineGoCharge))]
    public class ApiGoCharge
    {
        public string Label { get; set; }
        public bool IsPercent { get; set; }
        public bool TaxIncluded { get; set; }
        public decimal ChargeValue { get; set; }
        public virtual int TransactionTypeId { get; set; }
    }
}

