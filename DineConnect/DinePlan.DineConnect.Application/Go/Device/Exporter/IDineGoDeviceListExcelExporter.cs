﻿using System.Collections.Generic;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Device.Dtos;

namespace DinePlan.DineConnect.Go.Device.Exporter
{
    public interface IDineGoDeviceListExcelExporter
    {
        FileDto ExportToFile(List<DineGoDeviceListDto> dtos);
    }
}