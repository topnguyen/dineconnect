﻿using System.Collections.Generic;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Device.Dtos;

namespace DinePlan.DineConnect.Go.Device.Exporter
{
    public class DineGoDeviceListExcelExporter : FileExporterBase, IDineGoDeviceListExcelExporter
    {
        public FileDto ExportToFile(List<DineGoDeviceListDto> dtos)
        {
            return CreateExcelPackage(
                "DineGoDeviceList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("DineGoDevice"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Name"),
                        L("IdleTime"),
                       "OrderLink"
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                         _ => _.Name,
                          _ => _.IdleTime,
                           _ => _.OrderLink
                        );

                    for (var i = 1; i <= 10; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
