﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Device.Dtos;
using DinePlan.DineConnect.Go.Device.Exporter;
using DinePlan.DineConnect.Go.Dtos;
using DinePlan.DineConnect.Go.PaymentTypes.Dtos;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Go.Device
{
    public class DineGoDeviceAppService : DineConnectAppServiceBase, IDineGoDeviceAppService
    {
        private readonly IRepository<DineGoBrand> _dinegobrandManager;
        private readonly IDineGoDeviceListExcelExporter _dinegodeviceExporter;
        private readonly IRepository<DineGoDevice> _dinegodeviceManager;
        private readonly IRepository<DineGoPaymentType> _dinegopaymenttypeManager;
        private readonly IRepository<PaymentType> _pRepository;
        private readonly IRepository<Department> _dRepo;

        public DineGoDeviceAppService(IRepository<DineGoDevice> dinegodeviceManager, IRepository<PaymentType> pRepository,
            IDineGoDeviceListExcelExporter dinegodeviceExporter,
            IRepository<DineGoBrand> dinegobrandManager,
            IRepository<Department> dRepo,
            IRepository<DineGoPaymentType> dinegopaymenttypeManager)
        {
            _dinegodeviceManager = dinegodeviceManager;
            _dinegodeviceExporter = dinegodeviceExporter;
            _dinegobrandManager = dinegobrandManager;
            _dinegopaymenttypeManager = dinegopaymenttypeManager;
            _pRepository = pRepository;
            _dRepo = dRepo;
        }

        public async Task<PagedResultOutput<DineGoDeviceListDto>> GetAll(GetDineGoDeviceInput input)
        {
            var allItems = _dinegodeviceManager
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Name.Contains(input.Filter)
                );
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<DineGoDeviceListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<DineGoDeviceListDto>(
                allItemCount,
                allListDtos
            );
        }

        public async Task<FileDto> GetAllToExcel(GetDineGoDeviceInput input)
        {
            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<DineGoDeviceListDto>>();
            return _dinegodeviceExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDineGoDeviceForEditOutput> GetDineGoDeviceForEdit(NullableIdInput input)
        {
            DineGoDeviceEditDto editDto;
            DeviceThemeSettingsDto editDeviceThemeSettingDto=new DeviceThemeSettingsDto();

            if (input.Id.HasValue)
            {
                var hDto = await _dinegodeviceManager.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<DineGoDeviceEditDto>();
                if (editDto.ThemeSettings != null)
                    editDeviceThemeSettingDto = JsonConvert.DeserializeObject<DeviceThemeSettingsDto>(editDto.ThemeSettings);
            }
            else
            {
                editDto = new DineGoDeviceEditDto();
                editDeviceThemeSettingDto = new DeviceThemeSettingsDto();
            }

            return new GetDineGoDeviceForEditOutput
            {
                DineGoDevice = editDto,
                DeviceThemeSettings = editDeviceThemeSettingDto
            };
        }

        public async Task<IdInput> CreateOrUpdateDineGoDevice(CreateOrUpdateDineGoDeviceInput input)
        {
            IdInput returnId;
            if (input.DineGoDevice.Id.HasValue)
                returnId = await UpdateDineGoDevice(input);
            else
                returnId = await CreateDineGoDevice(input);
            return returnId;
        }

        public async Task DeleteDineGoDevice(IdInput input)
        {
            await _dinegodeviceManager.DeleteAsync(input.Id);
        }

        public async Task<List<DineGoBrandListDto>> GetDineGoBrandForCombobox()
        {
            var lstBrands = await _dinegobrandManager.GetAllListAsync(a => !a.IsDeleted);
            return lstBrands.MapTo<List<DineGoBrandListDto>>();
        }

        public async Task<List<DineGoPaymentTypeListDto>> GetDineGoPaymentTypeForCombobox()
        {
            var lstpaymenttype = await _dinegopaymenttypeManager.GetAllListAsync(a => !a.IsDeleted);
            return lstpaymenttype.MapTo<List<DineGoPaymentTypeListDto>>();
        }

        protected virtual async Task<IdInput> UpdateDineGoDevice(CreateOrUpdateDineGoDeviceInput input)
        {
            var dto = input.DineGoDevice;
            var item = await _dinegodeviceManager.GetAsync(input.DineGoDevice.Id.Value);

            item.Name = dto.Name;
            item.OrderLink = dto.OrderLink;
            item.IdleTime = dto.IdleTime;
            item.PinCode = dto.PinCode;
            item.PushToConnect = dto.PushToConnect;

            if (input.DineGoDevice.RefreshImage) item.ChangeGuid = Guid.NewGuid();
            if (!dto.DineGoBrands.Any())
            {
                item.DineGoBrands.Clear();
            }
            else
            {
                var allCurrentIds = dto.DineGoBrands.Select(a => a.Id).ToList();
                var allGoBrandsIds = item.DineGoBrands.Select(a => a.Id).ToList();

                var toRemove = item.DineGoBrands.Where(a => !allCurrentIds.Contains(a.Id)).ToList();
                foreach (var removeBrand in toRemove)
                {
                    item.DineGoBrands.Remove(removeBrand);
                }

                var toAdd = dto.DineGoBrands.Where(a => a.Id != null && !allGoBrandsIds.Contains(a.Id.Value)).ToList();

                foreach (var addId in toAdd)
                {
                    if (addId.Id != null)
                    {
                        var myDto = await _dinegobrandManager.GetAsync(addId.Id.Value);
                        item.DineGoBrands.Add(myDto);
                    }
                }
            }

            if (!dto.DineGoPaymentTypes.Any())
            {
                item.DineGoPaymentTypes.Clear();
            }
            else
            {
                var allCurrentIds = dto.DineGoPaymentTypes.Select(a => a.Id).ToList();
                var allGoPaymentTypesIds = item.DineGoPaymentTypes.Select(a => a.Id).ToList();

                var toRemove = item.DineGoPaymentTypes.Where(a => !allCurrentIds.Contains(a.Id)).ToList();
                foreach (var removePaymentType in toRemove)
                {
                    item.DineGoPaymentTypes.Remove(removePaymentType);
                }

                var toAdd = dto.DineGoPaymentTypes.Where(a => a.Id != null && !allGoPaymentTypesIds.Contains(a.Id.Value)).ToList();

                foreach (var addId in toAdd)
                {
                    if (addId.Id != null)
                    {
                        var myDto = await _dinegopaymenttypeManager.GetAsync(addId.Id.Value);
                        item.DineGoPaymentTypes.Add(myDto);
                    }
                }
            }
            DeviceThemeSettingsDto dtoJson = new DeviceThemeSettingsDto
            {
                IdleContents=input.DeviceThemeSettings.IdleContents,
                BannerContents=input.DeviceThemeSettings.BannerContents,
                BackgroundColor=input.DeviceThemeSettings.BackgroundColor,
                Logo=input.DeviceThemeSettings.Logo
            };

            var objectJson = JsonConvert.SerializeObject(dtoJson);
            item.ThemeSettings = objectJson;
            await _dinegodeviceManager.UpdateAsync(item);
            if (dto.Id != null) return new IdInput { Id = dto.Id.Value };

            return new IdInput { Id = 0 };
        }

        protected virtual async Task<IdInput> CreateDineGoDevice(CreateOrUpdateDineGoDeviceInput input)
        {
            var dto = new DineGoDevice
            {
                Name = input.DineGoDevice.Name,
                IdleTime = input.DineGoDevice.IdleTime,
                OrderLink=input.DineGoDevice.OrderLink,
                PinCode = input.DineGoDevice.PinCode,
                PushToConnect = input.DineGoDevice.PushToConnect
            };

            foreach (var dineGoBrand in input.DineGoDevice.DineGoBrands)
            {
                var myDto = await _dinegobrandManager.GetAsync(dineGoBrand.Id.Value);
                dto.DineGoBrands.Add(myDto);
            }
            foreach (var dineGoPaymentType in input.DineGoDevice.DineGoPaymentTypes)
            {
                var myDto = await _dinegopaymenttypeManager.GetAsync(dineGoPaymentType.Id.Value);
                dto.DineGoPaymentTypes.Add(myDto);
            }

            var retId = await _dinegodeviceManager.InsertAndGetIdAsync(dto);
            return new IdInput { Id = retId };
        }



        public async Task<List<ComboboxItemDto>> ApiGetAllDevices()
        {
            var allLists = await _dinegodeviceManager.GetAllListAsync();

            var allReturns = allLists.Select(a => new ComboboxItemDto
            {
                Value = a.Id.ToString(),
                DisplayText = a.Name
            }).ToList();

            return allReturns;
        }

        public async Task<ApiGoDevice> ApiGetDeviceDetail(IdInput input)
        {
            var allDevice = await _dinegodeviceManager.GetAsync(input.Id);
            if (allDevice != null)
            {
                var returnDevice = allDevice.MapTo<ApiGoDevice>();

                if (returnDevice?.DineGoPaymentTypes != null && returnDevice.DineGoPaymentTypes.Any())
                {
                    foreach (var returnDeviceDineGoPaymentType in returnDevice.DineGoPaymentTypes)
                    {
                        var myPaType = await _pRepository.GetAsync(returnDeviceDineGoPaymentType.PaymentTypeId);
                        if (myPaType != null)
                        {
                            returnDeviceDineGoPaymentType.PaymentType = myPaType;
                        }
                    }
                }

                if (returnDevice?.DineGoBrands != null && returnDevice.DineGoBrands.Any())
                {
                    foreach (var brand in returnDevice.DineGoBrands)
                    {
                        if (brand.DineGoDepartments != null && brand.DineGoDepartments.Any())
                        {
                            foreach (var dept in brand.DineGoDepartments)
                            {
                                var myDepartment = await _dRepo.GetAsync(dept.DepartmentId);
                                if (myDepartment != null)
                                {
                                    dept.DepartmentName= myDepartment.Name;
                                }
                            }
                        }
                    }
                }
                return returnDevice;

            }
                

            return null;
        }

        
    }
}