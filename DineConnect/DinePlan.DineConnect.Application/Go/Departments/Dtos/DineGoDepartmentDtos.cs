﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Charge.Dtos;

namespace DinePlan.DineConnect.Go.Departments.Dtos
{
    [AutoMapFrom(typeof(DineGoDepartment))]
    public class DineGoDepartmentListDto : FullAuditedEntityDto
    {
        //TODO: DTO DineGoDepartment Properties Missing
        public string Label { get; set; }
         public virtual int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
    }

    [AutoMapTo(typeof(DineGoDepartment))]
    public class DineGoDepartmentEditDto
    {
        public DineGoDepartmentEditDto()
        {
            DineGoCharges = new Collection<DineGoChargeEditDto>();
        }
        public int? Id { get; set; }
        public string Label { get; set; }
        public virtual int DepartmentId { get; set; }
        public Collection<DineGoChargeEditDto> DineGoCharges { get; set; }
        public int TenantId { get; set; }
        // public List<DineGoDepartmentDetailEditDto> DineGoDepartmentDetail { get; set; }
    }

    public class GetDineGoDepartmentInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime Desc";
            }
        }
    }

    public class GetDineGoDepartmentForEditOutput : IOutputDto
    {
        public DineGoDepartmentEditDto DineGoDepartment { get; set; }
        //public DineGoDepartmentDetailEditDto DineGoDepartmentDetailEditDto { get; set; }
    }
    public class CreateOrUpdateDineGoDepartmentInput : IInputDto
    {
        [Required]
        public DineGoDepartmentEditDto DineGoDepartment { get; set; }
    }

    public class CreateDineGoDepartmentDetail : IInputDto
    {
        [Required]
        public DineGoDepartmentDetailEditDto DineGoDepartmentDetailEditDto { get; set; }
    }

    //[AutoMapTo(typeof(DineGoDepartmentDetail))]
    public class DineGoDepartmentDetailEditDto : FullAuditedEntityDto
    {
        public int? Id { get; set; }
        public int DineGoDepartmentId { get; set; }
        public int DineGoChargeId { get; set; }
        public string DineGoChargeName { get; set; }
    }


    public class DineGoDepartmentDetailListDto: FullAuditedEntityDto
    {
        public int DineGoDepartmentId { get; set; }
        public int DineGoChargeId { get; set; }
        public string DineGoChargeName { get; set; }
    }
}

