﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Go.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Departments.Dtos;

namespace DinePlan.DineConnect.Go.Departments
{
    public interface IDineGoDepartmentAppService : IApplicationService
    {
        Task<PagedResultOutput<DineGoDepartmentListDto>> GetAll(GetDineGoDepartmentInput inputDto);
        Task<FileDto> GetAllToExcel(GetDineGoDepartmentInput input);
        Task<GetDineGoDepartmentForEditOutput> GetDineGoDepartmentForEdit(NullableIdInput nullableIdInput);
        Task<IdInput> CreateOrUpdateDineGoDepartment(CreateOrUpdateDineGoDepartmentInput input);
        Task DeleteDineGoDepartment(IdInput input);
        
    }
}