﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Go.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Departments.Exporter;
using DinePlan.DineConnect.Connect.Master;
using System.Linq;
using System;
using System.Collections.ObjectModel;
using Abp.UI;
using DinePlan.DineConnect.Go.Departments.Dtos;

namespace DinePlan.DineConnect.Go.Departments
{
    public class DineGoDepartmentAppService : DineConnectAppServiceBase, IDineGoDepartmentAppService
    {
        private readonly IDineGoDepartmentListExcelExporter _dinegodepartmentExporter;
        private readonly IRepository<DineGoDepartment> _dinegodepartmentManager;
        private readonly IRepository<DineGoCharge> _dinegochargeRepo;
        private readonly IRepository<Department> _departmentManager;

        public DineGoDepartmentAppService(IRepository<DineGoDepartment> dinegodepartmentManager,
            IDineGoDepartmentListExcelExporter dinegodepartmentExporter,
            IRepository<DineGoCharge> dinegochargeRepo,
            IRepository<Department> departmentManager)
        {
            _dinegodepartmentManager = dinegodepartmentManager;
            _dinegodepartmentExporter = dinegodepartmentExporter;
            _dinegochargeRepo = dinegochargeRepo;
            _departmentManager = departmentManager;
        }

        public async Task<PagedResultOutput<DineGoDepartmentListDto>> GetAll(GetDineGoDepartmentInput input)
        {
            IQueryable<DineGoDepartmentListDto> allItems;
            var rsDepartments = _dinegodepartmentManager
                .GetAll();
            allItems = (from dept in rsDepartments
                        join dt in _departmentManager.GetAll()
                        on dept.DepartmentId equals dt.Id
                        select new DineGoDepartmentListDto()
                        {
                            Id = dept.Id,
                            Label = dept.Label,
                            DepartmentId = dept.DepartmentId,
                            DepartmentName = dt.Name,
                            CreationTime = dept.CreationTime
                        }).WhereIf(
                       !input.Filter.IsNullOrEmpty(),
                       p => p.Label.Contains(input.Filter)
               );

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<DineGoDepartmentListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<DineGoDepartmentListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetDineGoDepartmentInput input)
        {
            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<DineGoDepartmentListDto>>();
            return _dinegodepartmentExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDineGoDepartmentForEditOutput> GetDineGoDepartmentForEdit(NullableIdInput input)
        {
            DineGoDepartmentEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _dinegodepartmentManager.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<DineGoDepartmentEditDto>();
            }
            else
            {
                editDto = new DineGoDepartmentEditDto();
            }

            return new GetDineGoDepartmentForEditOutput
            {
                DineGoDepartment = editDto
            };
        }

        public async Task<IdInput> CreateOrUpdateDineGoDepartment(CreateOrUpdateDineGoDepartmentInput input)
        {
            IdInput returnId;
            if (input.DineGoDepartment.Id.HasValue)
            {
                returnId = await UpdateDineGoDepartment(input);
            }
            else
            {
                returnId = await CreateDineGoDepartment(input);
            }
            return returnId;
        }

        public async Task DeleteDineGoDepartment(IdInput input)
        {
            await _dinegodepartmentManager.DeleteAsync(input.Id);
        }

        protected virtual async Task<IdInput> UpdateDineGoDepartment(CreateOrUpdateDineGoDepartmentInput input)
        {
            var dto = input.DineGoDepartment;
            var item = await _dinegodepartmentManager.GetAsync(input.DineGoDepartment.Id.Value);

            item.Label = dto.Label;
            item.DepartmentId = dto.DepartmentId;

            if (!dto.DineGoCharges.Any())
            {
                item.DineGoCharges.Clear();
            }
            else
            {
                var allCurrentIds = dto.DineGoCharges.Select(a => a.Id).ToList();
                var allGoChargesIds = item.DineGoCharges.Select(a => a.Id).ToList();

                var toRemove = item.DineGoCharges.Where(a =>!allCurrentIds.Contains(a.Id)).ToList();
                foreach (var removeCharge in toRemove)
                {
                    item.DineGoCharges.Remove(removeCharge);
                }

                var toAdd = dto.DineGoCharges.Where(a =>a.Id!=null && !allGoChargesIds.Contains(a.Id.Value)).ToList();

                foreach (var addId in toAdd)
                {
                    if (addId.Id != null)
                    {
                        var myDto = await _dinegochargeRepo.GetAsync(addId.Id.Value);
                        item.DineGoCharges.Add(myDto);
                    }
                }
            }
            await _dinegodepartmentManager.UpdateAsync(item);
            if (dto.Id != null) return new IdInput {Id = dto.Id.Value};

            return  new IdInput{Id = 0};
        }

        protected virtual async Task<IdInput> CreateDineGoDepartment(CreateOrUpdateDineGoDepartmentInput input)
        {
            var dto = new DineGoDepartment
            {
                Label = input.DineGoDepartment.Label,
                DepartmentId = input.DineGoDepartment.DepartmentId,
            };

            foreach (var dineGoCharge in input.DineGoDepartment.DineGoCharges)
            {
                var myDto = await _dinegochargeRepo.GetAsync(dineGoCharge.Id.Value);
                dto.DineGoCharges.Add(myDto);
            }

            var retId = await _dinegodepartmentManager.InsertAndGetIdAsync(dto);
            return new IdInput { Id = retId };
        }

    }
}