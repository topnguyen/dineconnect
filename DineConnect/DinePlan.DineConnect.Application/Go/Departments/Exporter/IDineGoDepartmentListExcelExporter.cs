﻿using System.Collections.Generic;
using DinePlan.DineConnect.Go.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Departments.Dtos;

namespace DinePlan.DineConnect.Go.Departments.Exporter
{
    public interface IDineGoDepartmentListExcelExporter
    {
        FileDto ExportToFile(List<DineGoDepartmentListDto> dtos);
    }
}