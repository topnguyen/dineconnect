﻿using System.Collections.Generic;
using DinePlan.DineConnect.Go.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Departments.Dtos;

namespace DinePlan.DineConnect.Go.Departments.Exporter
{
    public class DineGoDepartmentListExcelExporter : FileExporterBase, IDineGoDepartmentListExcelExporter
    {
        public FileDto ExportToFile(List<DineGoDepartmentListDto> dtos)
        {
            return CreateExcelPackage(
                "DineGoDepartmentList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("DineGoDepartment"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        "Label",
                        L("Department")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.Label,
                        _ => _.DepartmentName
                        );

                    for (var i = 1; i <= 10; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}