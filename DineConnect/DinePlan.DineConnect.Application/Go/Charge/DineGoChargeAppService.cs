﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Charge.Dtos;
using DinePlan.DineConnect.Go.Charge.Exporter;
using DinePlan.DineConnect.Go.Dtos;

namespace DinePlan.DineConnect.Go.Charge
{
    public class DineGoChargeAppService : DineConnectAppServiceBase, IDineGoChargeAppService
    {
        private readonly IDineGoChargeListExcelExporter _dinegochargeExporter;
        private readonly IRepository<DineGoCharge> _dinegochargeManager;
        private readonly IRepository<TransactionType> _transactiontypeManager;

        public DineGoChargeAppService(IRepository<DineGoCharge> dinegochargeManager,
            IDineGoChargeListExcelExporter dinegochargeExporter,
            IRepository<TransactionType> transactiontypeManager)
        {
            _dinegochargeManager = dinegochargeManager;
            _dinegochargeExporter = dinegochargeExporter;
            _transactiontypeManager = transactiontypeManager;
        }

        public async Task<PagedResultOutput<DineGoChargeListDto>> GetAll(GetDineGoChargeInput input)
        {
            IQueryable<DineGoChargeListDto> allItems;
            var rsCharges = _dinegochargeManager
                .GetAll();
            allItems = (from charges in rsCharges
                        join tt in _transactiontypeManager.GetAll()
                        on charges.TransactionTypeId equals tt.Id
                        select new DineGoChargeListDto()
                        {
                            Id = charges.Id,
                            Label = charges.Label,
                            TransactionTypeId = charges.TransactionTypeId,
                            IsPercent = charges.IsPercent,
                            ChargeValue = charges.ChargeValue,
                            TaxIncluded = charges.TaxIncluded,
                            TransactionTypeName = tt.Name,
                            CreationTime = charges.CreationTime
                        }).WhereIf(
                       !input.Filter.IsNullOrEmpty(),
                       p => p.Label.Contains(input.Filter)
               );
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<DineGoChargeListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<DineGoChargeListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetDineGoChargeInput input)
        {
            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<DineGoChargeListDto>>();
            return _dinegochargeExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDineGoChargeForEditOutput> GetDineGoChargeForEdit(NullableIdInput input)
        {
            DineGoChargeEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _dinegochargeManager.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<DineGoChargeEditDto>();
            }
            else
            {
                editDto = new DineGoChargeEditDto();
            }

            return new GetDineGoChargeForEditOutput
            {
                DineGoCharge = editDto
            };
        }

        public async Task<IdInput> CreateOrUpdateDineGoCharge(CreateOrUpdateDineGoChargeInput input)
        {
            IdInput returnId;
            if (input.DineGoCharge.Id.HasValue)
            {
                returnId = await UpdateDineGoCharge(input);
            }
            else
            {
                returnId = await CreateDineGoCharge(input);
            }
            return returnId;
        }

        public async Task DeleteDineGoCharge(IdInput input)
        {
            await _dinegochargeManager.DeleteAsync(input.Id);
        }

        protected virtual async Task<IdInput> UpdateDineGoCharge(CreateOrUpdateDineGoChargeInput input)
        {
            var dto = input.DineGoCharge;
            var item = await _dinegochargeManager.GetAsync(input.DineGoCharge.Id.Value);
            //TODO: SERVICE DineGoCharge Update Individually
            dto.MapTo(item);

            await _dinegochargeManager.UpdateAsync(item);
            return new IdInput { Id = dto.Id.Value };
        }

        protected virtual async Task<IdInput> CreateDineGoCharge(CreateOrUpdateDineGoChargeInput input)
        {
            var dto = input.DineGoCharge.MapTo<DineGoCharge>();
            var retId = await _dinegochargeManager.InsertAndGetIdAsync(dto);
            return new IdInput { Id = retId };
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetChargesForCombobox()
        {
            var lstLocation = await _dinegochargeManager.GetAllListAsync(a => !a.IsDeleted);

            return
                new ListResultOutput<ComboboxItemDto>(
                    lstLocation.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Label)).ToList());
        }

        public async Task<List<DineGoChargeListDto>> GetChargesListDtos()
        {
            var lstCharges = await _dinegochargeManager.GetAllListAsync(a => !a.IsDeleted);

            return lstCharges.MapTo<List<DineGoChargeListDto>>();
        }
    }
}