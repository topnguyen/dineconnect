﻿using System.Collections.Generic;
using DinePlan.DineConnect.Go.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Charge.Dtos;


namespace DinePlan.DineConnect.Go.Charge.Exporter
{
    public interface IDineGoChargeListExcelExporter
    {
        FileDto ExportToFile(List<DineGoChargeListDto> dtos);
    }
}
