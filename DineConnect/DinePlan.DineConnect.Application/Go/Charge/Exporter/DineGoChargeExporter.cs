﻿using System.Collections.Generic;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Charge.Dtos;
using DinePlan.DineConnect.Go.Dtos;

namespace DinePlan.DineConnect.Go.Charge.Exporter
{
    public class DineGoChargeListExcelExporter : FileExporterBase, IDineGoChargeListExcelExporter
    {
        public FileDto ExportToFile(List<DineGoChargeListDto> dtos)
        {
            return CreateExcelPackage(
                "DineGoChargeList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("DineGoCharge"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        "Label",
                        "TransactionTypeName",
                        "IsPercent",
                        "ChargeValue",
                        L("TaxIncluded")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                         _ => _.Label,
                          _ => _.TransactionTypeName    ,
                           _ => _.IsPercent,
                            _ => _.ChargeValue,
                            _ => _.TaxIncluded
                        );

                    for (var i = 1; i <= 10; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}