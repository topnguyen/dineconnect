﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Go.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using DinePlan.DineConnect.Go.Charge.Dtos;

namespace DinePlan.DineConnect.Go.Charge
{
    public interface IDineGoChargeAppService : IApplicationService
    {
        Task<PagedResultOutput<DineGoChargeListDto>> GetAll(GetDineGoChargeInput inputDto);
        Task<FileDto> GetAllToExcel(GetDineGoChargeInput input);
        Task<GetDineGoChargeForEditOutput> GetDineGoChargeForEdit(NullableIdInput nullableIdInput);
        Task<IdInput> CreateOrUpdateDineGoCharge(CreateOrUpdateDineGoChargeInput input);
        Task DeleteDineGoCharge(IdInput input);
        Task<ListResultOutput<ComboboxItemDto>> GetChargesForCombobox();
        Task<List<DineGoChargeListDto>> GetChargesListDtos();

    }
}