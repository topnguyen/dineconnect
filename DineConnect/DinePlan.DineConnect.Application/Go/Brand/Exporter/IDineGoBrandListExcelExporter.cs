﻿using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Go.Brand.Exporting
{
    public interface IDineGoBrandListExcelExporter
    {
        FileDto ExportToFile(List<DineGoBrandListDto> dtos);
    }
}
