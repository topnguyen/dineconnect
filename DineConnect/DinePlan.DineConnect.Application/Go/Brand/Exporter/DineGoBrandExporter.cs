﻿using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Brand.Exporting;
using DinePlan.DineConnect.Go.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;

namespace DinePlan.DineConnect.Go.Brand.Exporter
{
    public class DineGoBrandListExcelExporter : FileExporterBase, IDineGoBrandListExcelExporter
    {
        public FileDto ExportToFile(List<DineGoBrandListDto> dtos)
        {
            return CreateExcelPackage(
                "DineGoBrandList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("DineGoBrand"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        "Label",
                        L("Location"),
                        L("ScreenMenu")                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.Label,
                        _ => _.LocationName,
                        _ => _.ScreenMenuName
                        );

                    for (var i = 1; i <= 10; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
