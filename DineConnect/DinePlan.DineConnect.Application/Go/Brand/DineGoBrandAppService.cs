﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Go.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Brand.Exporting;
using DinePlan.DineConnect.Connect.Menu;
using System.Linq;
using System.Collections.ObjectModel;
using Abp.UI;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Go.Departments.Dtos;

namespace DinePlan.DineConnect.Go.Brand
{
    public class DineGoBrandAppService : DineConnectAppServiceBase, IDineGoBrandAppService
    {
        private readonly IDineGoBrandListExcelExporter _dinegobrandExporter;
        private readonly IRepository<DineGoBrand> _dinegobrandManager;
        private readonly IRepository<DineGoDepartment> _dinegodepartmentManager;
        private readonly IRepository<ScreenMenu> _screenMenuRepo;
        private readonly IRepository<Location> _locationRepo;

        public DineGoBrandAppService(IRepository<DineGoBrand> dinegobrandManager,
            IDineGoBrandListExcelExporter dinegobrandExporter,
            IRepository<DineGoDepartment> dinegodepartmentManager,
            IRepository<ScreenMenu> screenMenuRepo,
            IRepository<Location> locationRepo)
        {
            _dinegobrandManager = dinegobrandManager;
            _dinegobrandExporter = dinegobrandExporter;
            _dinegodepartmentManager = dinegodepartmentManager;
            _screenMenuRepo = screenMenuRepo;
            _locationRepo = locationRepo;
        }

        public async Task<PagedResultOutput<DineGoBrandListDto>> GetAll(GetDineGoBrandInput input)
        {

            IQueryable<DineGoBrandListDto> allItems;
            var rsBrands = _dinegobrandManager
                .GetAll();
            allItems = (from brand in rsBrands
                        join loc in _locationRepo.GetAll()
                        on brand.LocationId equals loc.Id
                        join sm in _screenMenuRepo.GetAll()
                        on brand.ScreenMenuId equals sm.Id
                        select new DineGoBrandListDto()
                        {
                            Id = brand.Id,
                            Label = brand.Label,
                            LocationId = brand.LocationId,
                            ScreenMenuId = brand.ScreenMenuId,
                            ScreenMenuName = sm.Name,
                            LocationName = loc.Name,
                            CreationTime = brand.CreationTime
                        }).WhereIf(
                       !input.Filter.IsNullOrEmpty(),
                       p => p.Label.Contains(input.Filter)
               );


            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<DineGoBrandListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<DineGoBrandListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetDineGoBrandInput input)
        {
            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<DineGoBrandListDto>>();
            return _dinegobrandExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDineGoBrandForEditOutput> GetDineGoBrandForEdit(NullableIdInput input)
        {
            DineGoBrandEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _dinegobrandManager.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<DineGoBrandEditDto>();

            }
            else
            {
                editDto = new DineGoBrandEditDto();
            }

            return new GetDineGoBrandForEditOutput
            {
                DineGoBrand = editDto
            };
        }

        public async Task<IdInput> CreateOrUpdateDineGoBrand(CreateOrUpdateDineGoBrandInput input)
        {
            IdInput returnId;
            if (input.DineGoBrand.Id.HasValue)
            {
                returnId = await UpdateDineGoBrand(input);
            }
            else
            {
                returnId = await CreateDineGoBrand(input);
            }
            return returnId;
        }

        public async Task DeleteDineGoBrand(IdInput input)
        {
            await _dinegobrandManager.DeleteAsync(input.Id);
        }

        protected virtual async Task<IdInput> UpdateDineGoBrand(CreateOrUpdateDineGoBrandInput input)
        {
            var dto = input.DineGoBrand;
            var item = await _dinegobrandManager.GetAsync(input.DineGoBrand.Id.Value);

            item.Label = dto.Label;
            item.LocationId = dto.LocationId;
            item.ScreenMenuId = dto.ScreenMenuId;

            if (!dto.DineGoDepartments.Any())
            {
                item.DineGoDepartments.Clear();
            }
            else
            {
                var allCurrentIds = dto.DineGoDepartments.Select(a => a.Id).ToList();
                var allGoDeptIds = item.DineGoDepartments.Select(a => a.Id).ToList();

                var toRemove = item.DineGoDepartments.Where(a => !allCurrentIds.Contains(a.Id)).ToList();
                foreach (var removeDept in toRemove)
                {
                    item.DineGoDepartments.Remove(removeDept);
                }

                var toAdd = dto.DineGoDepartments.Where(a => a.Id != null && !allGoDeptIds.Contains(a.Id.Value)).ToList();

                foreach (var addId in toAdd)
                {
                    if (addId.Id != null)
                    {
                        var myDto = await _dinegodepartmentManager.GetAsync(addId.Id.Value);
                        item.DineGoDepartments.Add(myDto);
                    }
                }
            }
            await _dinegobrandManager.UpdateAsync(item);
            if (dto.Id != null) return new IdInput { Id = dto.Id.Value };

            return new IdInput { Id = 0 };
        }

        protected virtual async Task<IdInput> CreateDineGoBrand(CreateOrUpdateDineGoBrandInput input)
        {
            var dto = new DineGoBrand
            {
                Label = input.DineGoBrand.Label,
                LocationId = input.DineGoBrand.LocationId,
                ScreenMenuId = input.DineGoBrand.ScreenMenuId
            };

            foreach (var dineGoDept in input.DineGoBrand.DineGoDepartments)
            {
                var myDto = await _dinegodepartmentManager.GetAsync(dineGoDept.Id.Value);
                dto.DineGoDepartments.Add(myDto);
            }

            var retId = await _dinegobrandManager.InsertAndGetIdAsync(dto);
            return new IdInput { Id = retId };
        }
        public async Task<ListResultOutput<ComboboxItemDto>> GetScreenMenuForCombobox()
        {
            var screenMenuLists = await _screenMenuRepo.GetAllListAsync(a => !a.IsDeleted);
            return
                new ListResultOutput<ComboboxItemDto>(
                    screenMenuLists.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }
        public async Task<List<DineGoDepartmentListDto>> GetDineGodepartmentForCombobox()
        {
            var lstDepartments = await _dinegodepartmentManager.GetAllListAsync(a => !a.IsDeleted);
            return lstDepartments.MapTo<List<DineGoDepartmentListDto>>();
        }

    }
}