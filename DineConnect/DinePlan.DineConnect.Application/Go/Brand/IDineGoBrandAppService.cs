﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Go.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Departments.Dtos;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Go.Brand
{
    public interface IDineGoBrandAppService : IApplicationService
    {
        Task<PagedResultOutput<DineGoBrandListDto>> GetAll(GetDineGoBrandInput inputDto);
        Task<FileDto> GetAllToExcel(GetDineGoBrandInput inputDto);
        Task<GetDineGoBrandForEditOutput> GetDineGoBrandForEdit(NullableIdInput nullableIdInput);
        Task<IdInput> CreateOrUpdateDineGoBrand(CreateOrUpdateDineGoBrandInput input);
        Task DeleteDineGoBrand(IdInput input);
        Task<ListResultOutput<ComboboxItemDto>> GetScreenMenuForCombobox();
        Task<List<DineGoDepartmentListDto>> GetDineGodepartmentForCombobox();
        
    }
}