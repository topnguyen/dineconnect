﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Go.PaymentTypes.Dtos
{
    [AutoMapFrom(typeof(DineGoPaymentType))]
    public class DineGoPaymentTypeListDto : FullAuditedEntityDto
    {
        //TODO: DTO DineGoPaymentType Properties Missing
        public string Label { get; set; }
        public virtual int PaymentTypeId { get; set; }
        public string PaymentTypeName { get; set; }
    }
    [AutoMapTo(typeof(DineGoPaymentType))]
    public class DineGoPaymentTypeEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO DineGoPaymentType Properties Missing
        public string Label { get; set; }
        public virtual int PaymentTypeId { get; set; }
        public string PaymentTypeName { get; set; }
    }

    public class GetDineGoPaymentTypeInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id Desc";
            }
        }
    }
    public class GetDineGoPaymentTypeForEditOutput : IOutputDto
    {
        public DineGoPaymentTypeEditDto DineGoPaymentType { get; set; }
    }
    public class CreateOrUpdateDineGoPaymentTypeInput : IInputDto
    {
        [Required]
        public DineGoPaymentTypeEditDto DineGoPaymentType { get; set; }
    }
}

