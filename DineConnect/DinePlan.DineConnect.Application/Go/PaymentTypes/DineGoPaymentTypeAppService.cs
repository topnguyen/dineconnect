﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.PaymentTypes.Dtos;
using DinePlan.DineConnect.Go.PaymentTypes.Exporter;

namespace DinePlan.DineConnect.Go.PaymentTypes
{
    public class DineGoPaymentTypeAppService : DineConnectAppServiceBase, IDineGoPaymentTypeAppService
    {
        private readonly IDineGoPaymentTypeListExcelExporter _dinegopaymenttypeExporter;
        private readonly IRepository<DineGoPaymentType> _dinegopaymenttypeManager;
        private readonly IRepository<PaymentType> _paymenttypeManager;

        public DineGoPaymentTypeAppService(IRepository<DineGoPaymentType> dinegopaymenttypeManager,
            IDineGoPaymentTypeListExcelExporter dinegopaymenttypeExporter,
            IRepository<PaymentType> paymenttypeManager
            )
        {
            _dinegopaymenttypeManager = dinegopaymenttypeManager;
            _dinegopaymenttypeExporter = dinegopaymenttypeExporter;
            _paymenttypeManager = paymenttypeManager;
        }

        public async Task<PagedResultOutput<DineGoPaymentTypeListDto>> GetAll(GetDineGoPaymentTypeInput input)
        {
            IQueryable<DineGoPaymentTypeListDto> allItems;
            var rsPaymentType = _dinegopaymenttypeManager
                .GetAll();
            allItems = (from dinegopaytype in rsPaymentType
                            join pt in _paymenttypeManager.GetAll()
                            on dinegopaytype.PaymentTypeId equals pt.Id
                            select new DineGoPaymentTypeListDto()
                            {
                                Id = dinegopaytype.Id,
                                Label = dinegopaytype.Label,
                                PaymentTypeId = dinegopaytype.PaymentTypeId,
                                PaymentTypeName = pt.Name
                            }).WhereIf(
                       !input.Filter.IsNullOrEmpty(),
                       p => p.Label.Contains(input.Filter)
               );
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<DineGoPaymentTypeListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<DineGoPaymentTypeListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetDineGoPaymentTypeInput input)
        {
            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<DineGoPaymentTypeListDto>>();
            return _dinegopaymenttypeExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDineGoPaymentTypeForEditOutput> GetDineGoPaymentTypeForEdit(NullableIdInput input)
        {
            DineGoPaymentTypeEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _dinegopaymenttypeManager.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<DineGoPaymentTypeEditDto>();
            }
            else
            {
                editDto = new DineGoPaymentTypeEditDto();
            }

            return new GetDineGoPaymentTypeForEditOutput
            {
                DineGoPaymentType = editDto
            };
        }

        public async Task<IdInput> CreateOrUpdateDineGoPaymentType(CreateOrUpdateDineGoPaymentTypeInput input)
        {
            IdInput returnId;
            if (input.DineGoPaymentType.Id.HasValue)
            {
                returnId = await UpdateDineGoPaymentType(input);
            }
            else
            {
                returnId = await CreateDineGoPaymentType(input);
            }
            return returnId;
        }

        public async Task DeleteDineGoPaymentType(IdInput input)
        {
            await _dinegopaymenttypeManager.DeleteAsync(input.Id);
        }

        protected virtual async Task<IdInput> UpdateDineGoPaymentType(CreateOrUpdateDineGoPaymentTypeInput input)
        {
            var dto = input.DineGoPaymentType;
            var item = await _dinegopaymenttypeManager.GetAsync(input.DineGoPaymentType.Id.Value);

            //TODO: SERVICE DineGoPaymentType Update Individually
            dto.MapTo(item);

            await _dinegopaymenttypeManager.UpdateAsync(item);
            return new IdInput { Id = dto.Id.Value };
        }

        protected virtual async Task<IdInput> CreateDineGoPaymentType(CreateOrUpdateDineGoPaymentTypeInput input)
        {
            var dto = input.DineGoPaymentType.MapTo<DineGoPaymentType>();
            var retId = await _dinegopaymenttypeManager.InsertAndGetIdAsync(dto);
            return new IdInput { Id = retId };
        }
    }
}