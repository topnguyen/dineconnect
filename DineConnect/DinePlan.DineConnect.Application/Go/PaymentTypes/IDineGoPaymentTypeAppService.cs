﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.PaymentTypes.Dtos;

namespace DinePlan.DineConnect.Go.PaymentTypes
{
    public interface IDineGoPaymentTypeAppService : IApplicationService
    {
        Task<PagedResultOutput<DineGoPaymentTypeListDto>> GetAll(GetDineGoPaymentTypeInput inputDto);
        Task<FileDto> GetAllToExcel(GetDineGoPaymentTypeInput input);
        Task<GetDineGoPaymentTypeForEditOutput> GetDineGoPaymentTypeForEdit(NullableIdInput nullableIdInput);
        Task<IdInput> CreateOrUpdateDineGoPaymentType(CreateOrUpdateDineGoPaymentTypeInput input);
        Task DeleteDineGoPaymentType(IdInput input);
    }
}
