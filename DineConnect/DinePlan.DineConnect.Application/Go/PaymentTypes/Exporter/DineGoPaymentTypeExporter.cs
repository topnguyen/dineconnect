﻿using System.Collections.Generic;
using DinePlan.DineConnect.Go.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.PaymentTypes.Dtos;

namespace DinePlan.DineConnect.Go.PaymentTypes.Exporter
{
    public class DineGoPaymentTypeListExcelExporter : FileExporterBase, IDineGoPaymentTypeListExcelExporter
    {
        public FileDto ExportToFile(List<DineGoPaymentTypeListDto> dtos)
        {
            return CreateExcelPackage(
                "DineGoPaymentTypeList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("DineGoPaymentType"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                         L("Name"),
                          "Payment Type"
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.Label,
                        _ => _.PaymentTypeName
                        );

                    for (var i = 1; i <= 10; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}