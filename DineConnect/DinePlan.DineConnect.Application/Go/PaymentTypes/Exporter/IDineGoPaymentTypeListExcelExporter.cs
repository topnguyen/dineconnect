﻿using System.Collections.Generic;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.PaymentTypes.Dtos;

namespace DinePlan.DineConnect.Go.PaymentTypes.Exporter
{
    public interface IDineGoPaymentTypeListExcelExporter
    {
        FileDto ExportToFile(List<DineGoPaymentTypeListDto> dtos);
    }
}
