﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Document;
using DinePlan.DineConnect.House.Upload.Dtos;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Utility.Exporter;

namespace DinePlan.DineConnect.House.Upload.Implementation
{
    public class UploadDocumentAppService : DineConnectAppServiceBase, IUploadDocumentAppService
    {
        private readonly IExcelExporter _exporter;
        private readonly IUploadDocumentManager _uploaddocumentManager;
        private readonly IRepository<UploadDocument> _uploaddocumentRepo;

        public UploadDocumentAppService(IUploadDocumentManager uploaddocumentManager,
            IRepository<UploadDocument> uploadDocumentRepo,
            IExcelExporter exporter)
        {
            _uploaddocumentManager = uploaddocumentManager;
            _uploaddocumentRepo = uploadDocumentRepo;
            _exporter = exporter;
        }

        public async Task<PagedResultOutput<UploadDocumentListDto>> GetAll(GetUploadDocumentInput input)
        {
            List<UploadDocumentListDto> outputDtos = null;
            var allItems = _uploaddocumentRepo.GetAll();

            if (input.TenantId > 0)
            {
                allItems = allItems.Where(a => a.TenantId == input.TenantId);
            }

            if (input.Location > 0)
            {
                allItems =
                    allItems.Where(a => a.LocationId == input.Location);
            }

            if (input.Supplier > 0)
            {
                allItems =
                    allItems.Where(a => a.SupplierId != null && a.SupplierId == input.Supplier);
            }

            if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
            {
                allItems =
                    allItems.Where(
                        a =>
                            DbFunctions.TruncateTime(a.CreationTime) >=
                            DbFunctions.TruncateTime(input.StartDate)
                            &&
                            DbFunctions.TruncateTime(a.CreationTime) <=
                            DbFunctions.TruncateTime(input.EndDate));
            }


            input.Sorting = "DocumentStatus";
            var sortedDocs = await allItems.OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();
            outputDtos = sortedDocs.MapTo<List<UploadDocumentListDto>>();

            var outputCount = await allItems.CountAsync();
            return new PagedResultOutput<UploadDocumentListDto>(
                outputCount,
                outputDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _uploaddocumentRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<UploadDocumentListDto>>();
            var baseE = new BaseExportObject
            {
                ExportObject = allListDtos,
                ColumnNames = new[] {"Id"}
            };
            return _exporter.ExportToFile(baseE, L("UploadDocument"));
        }

        public async Task<GetUploadDocumentForEditOutput> GetUploadDocumentForEdit(NullableIdInput input)
        {
            UploadDocumentEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _uploaddocumentRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<UploadDocumentEditDto>();
            }
            else
            {
                editDto = new UploadDocumentEditDto();
            }

            return new GetUploadDocumentForEditOutput
            {
                UploadDocument = editDto
            };
        }

        public async Task CreateOrUpdateUploadDocument(CreateOrUpdateUploadDocumentInput input)
        {
            if (input.UploadDocument.Id.HasValue)
            {
                await UpdateUploadDocument(input);
            }
            else
            {
                await CreateUploadDocument(input);
            }
        }

        public async Task DeleteUploadDocument(IdInput input)
        {
            await _uploaddocumentRepo.DeleteAsync(input.Id);
        }

        public async Task<ListResultOutput<UploadDocumentListDto>> GetCodes()
        {
            var lstUploadDocument = await _uploaddocumentRepo.GetAll().ToListAsync();
            return new ListResultOutput<UploadDocumentListDto>(lstUploadDocument.MapTo<List<UploadDocumentListDto>>());
        }

        protected virtual async Task UpdateUploadDocument(CreateOrUpdateUploadDocumentInput input)
        {
            var item = await _uploaddocumentRepo.GetAsync(input.UploadDocument.Id.Value);
            var dto = input.UploadDocument;
            item.Url = dto.Url;
            CheckErrors(await _uploaddocumentManager.CreateOrUpdateSync(item));
        }

        protected virtual async Task CreateUploadDocument(CreateOrUpdateUploadDocumentInput input)
        {
            var dto = input.UploadDocument.MapTo<UploadDocument>();

            CheckErrors(await _uploaddocumentManager.CreateOrUpdateSync(dto));
        }

      
    }
}