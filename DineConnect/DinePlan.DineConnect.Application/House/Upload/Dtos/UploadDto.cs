﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Document;

namespace DinePlan.DineConnect.House.Upload.Dtos
{
    [AutoMapFrom(typeof (UploadDocument))]
    public class UploadDocumentListDto : FullAuditedEntityDto
    {
        public int? Id { get; set; }
        public int LocationId { get; set; }
        public string Url { get; set; }
        public int DocumentStatus { get; set; }
        public int? InvoiceId { get; set; }
        public int? SupplierId { get; set; }
    }

    [AutoMapTo(typeof (UploadDocument))]
    public class UploadDocumentEditDto
    {
        public int? Id { get; set; }
        public int TenantId { get; set; }
        public int LocationId { get; set; }
        public string Url { get; set; }
        public int? InvoiceId { get; set; }
        public int? SupplierId { get; set; }
        public int DocumentStatus { get; set; }
        //1 - ActionRequired, 2-Processing, 3-Completed
        

    }

    public class GetUploadDocumentInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Location { get; set; }
        public int Supplier { get; set; }
        public int TenantId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    public class GetUploadDocumentForEditOutput : IOutputDto
    {
        public UploadDocumentEditDto UploadDocument { get; set; }
    }

    public class CreateOrUpdateUploadDocumentInput : IInputDto
    {
        [Required]
        public UploadDocumentEditDto UploadDocument { get; set; }
    }
}