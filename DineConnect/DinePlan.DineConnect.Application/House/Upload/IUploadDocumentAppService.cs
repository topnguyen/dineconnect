﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Upload.Dtos;

namespace DinePlan.DineConnect.House.Upload
{
    public interface IUploadDocumentAppService : IApplicationService
    {
        Task<PagedResultOutput<UploadDocumentListDto>> GetAll(GetUploadDocumentInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetUploadDocumentForEditOutput> GetUploadDocumentForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateUploadDocument(CreateOrUpdateUploadDocumentInput input);
        Task DeleteUploadDocument(IdInput input);
        Task<ListResultOutput<UploadDocumentListDto>> GetCodes();
    }
}