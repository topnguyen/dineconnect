﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.House.Master.Dtos;

namespace DinePlan.DineConnect.House.MaterialStock.Implementation
{
    public class MaterialStockAppService : DineConnectAppServiceBase, IMaterialStockAppService
    {
        private readonly IRepository<CustomerTagDefinition> _customertagdefinitionRepo;
        private readonly IRepository<CustomerTagMaterialPrice> _customertagmaterialpriceRepo;
        private readonly IRepository<MaterialLocationWiseStock> _materiallocationwisestockRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<Location> _locationRepo;

        public MaterialStockAppService(
            IRepository<MaterialLocationWiseStock> materialLocationWiseStockRepo
            , IRepository<Material> materialRepo,
            IRepository<CustomerTagMaterialPrice> customertagmaterialpriceRepo,
            IRepository<CustomerTagDefinition> customertagdefinitionRepo,
            IRepository<Location> locationRepo
            )
        {
            _materiallocationwisestockRepo = materialLocationWiseStockRepo;
            _materialRepo = materialRepo;
            _customertagmaterialpriceRepo = customertagmaterialpriceRepo;
            _customertagdefinitionRepo = customertagdefinitionRepo;
            _locationRepo = locationRepo;
        }

        public async Task IncludeAllMaterial(IdInput input)
        {
            var existAlreadyList = await _materiallocationwisestockRepo.GetAllListAsync(a => a.LocationRefId == input.Id);

            var existAlreadyMaterials = existAlreadyList.Select(t => t.Id).ToList();

            var notExistList = await _materialRepo.GetAll().Where(m => !existAlreadyMaterials.Contains(m.Id)).ToListAsync();

            foreach (var lst in notExistList)
            {
                var isExists = existAlreadyList.FirstOrDefault(t => t.MaterialRefId == lst.Id && t.LocationRefId == input.Id);
                if (isExists == null)
                {
                    var newDto = new MaterialLocationWiseStock();
                    {
                        newDto.LocationRefId = input.Id;
                        newDto.MaterialRefId = lst.Id;
                        newDto.CurrentInHand = 0;
                        newDto.MaximumStock = 0;
                        newDto.MinimumStock = 0;
                        newDto.ReOrderLevel = 0;
                        newDto.IsOrderPlaced = false;
                        newDto.IsActiveInLocation = true;
                        newDto.ConvertAsZeroStockWhenClosingStockNegative = lst.ConvertAsZeroStockWhenClosingStockNegative;
                    }
                    var retId2 = await _materiallocationwisestockRepo.InsertAndGetIdAsync(newDto);
                }
            }
        }


        public async Task InitialiseLocationWiseMaterials(HouseLocationWiseMaterialInitialiseDto input)
        {
            if (input.LocationRefId.HasValue)
            {
                var existAlreadyList = await _materiallocationwisestockRepo.GetAll().Where(a => a.LocationRefId == input.LocationRefId.Value).ToListAsync();

                var existAlreadyMaterials = existAlreadyList.Select(t => t.MaterialRefId).ToList();

                var notExistList = await _materialRepo.GetAll().Where(m => !existAlreadyMaterials.Contains(m.Id) && m.TenantId==input.TenantId).ToListAsync();

                foreach (var lst in notExistList)
                {
                    var isExists = existAlreadyList.FirstOrDefault(t => t.MaterialRefId == lst.Id && t.LocationRefId == input.LocationRefId.Value);
                    if (isExists == null)
                    {
                        var newDto = new MaterialLocationWiseStock();
                        {
                            newDto.LocationRefId = input.LocationRefId.Value;
                            newDto.MaterialRefId = lst.Id;
                            newDto.CurrentInHand = 0;
                            newDto.MaximumStock = 0;
                            newDto.MinimumStock = 0;
                            newDto.ReOrderLevel = 0;
                            newDto.IsOrderPlaced = false;
                            newDto.IsActiveInLocation = true;
                            newDto.ConvertAsZeroStockWhenClosingStockNegative = lst.ConvertAsZeroStockWhenClosingStockNegative;
                            newDto.CreatorUserId = input.UserId;
                        }
                        var retId2 = await _materiallocationwisestockRepo.InsertAndGetIdAsync(newDto);
                    }
                }
            }
            else if (input.MaterialRefId.HasValue)
            {
                var locationDetails = await _locationRepo.GetAllListAsync(t=>t.TenantId==input.TenantId);
                var material = await _materialRepo.FirstOrDefaultAsync(t => t.Id == input.MaterialRefId.Value && t.TenantId==input.TenantId);

                foreach (var l in locationDetails)
                {
                    var newDto = new MaterialLocationWiseStock();
                    {
                        newDto.LocationRefId = l.Id;
                        newDto.MaterialRefId = input.MaterialRefId.Value;
                        newDto.CurrentInHand = 0;
                        newDto.MaximumStock = 0;
                        newDto.MinimumStock = 0;
                        newDto.ReOrderLevel = 0;
                        newDto.IsOrderPlaced = false;
                        newDto.IsActiveInLocation = true;
                        newDto.ConvertAsZeroStockWhenClosingStockNegative = material.ConvertAsZeroStockWhenClosingStockNegative;
                        newDto.CreatorUserId = input.UserId;                        
                    }
                    ;
                    var retId2 = await _materiallocationwisestockRepo.InsertAndGetIdAsync(newDto);
                }
            }
            else if (input.LocationRefId.HasValue ==false && input.MaterialRefId.HasValue == false)
            {
                var locationDetails = await _locationRepo.GetAllListAsync(t=>t.TenantId==input.TenantId);
                foreach(var loc in locationDetails)
                {
                    var existAlreadyList = await _materiallocationwisestockRepo.GetAll().Where(a => a.LocationRefId == loc.Id).ToListAsync();

                    var existAlreadyMaterials = existAlreadyList.Select(t => t.MaterialRefId).ToList();

                    var notExistList = await _materialRepo.GetAll().Where(m => !existAlreadyMaterials.Contains(m.Id)).ToListAsync();

                    foreach (var lst in notExistList)
                    {
                        var isExists = existAlreadyList.FirstOrDefault(t => t.MaterialRefId == lst.Id && t.LocationRefId == loc.Id);
                        if (isExists == null)
                        {
                            var newDto = new MaterialLocationWiseStock();
                            {
                                newDto.LocationRefId = loc.Id;
                                newDto.MaterialRefId = lst.Id;
                                newDto.CurrentInHand = 0;
                                newDto.MaximumStock = 0;
                                newDto.MinimumStock = 0;
                                newDto.ReOrderLevel = 0;
                                newDto.IsOrderPlaced = false;
                                newDto.IsActiveInLocation = true;
                                newDto.ConvertAsZeroStockWhenClosingStockNegative = lst.ConvertAsZeroStockWhenClosingStockNegative;
                                newDto.CreatorUserId = input.UserId;
                            }
                            var retId2 = await _materiallocationwisestockRepo.InsertAndGetIdAsync(newDto);
                        }
                    }
                }
            }
        }

    }
}