﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;

namespace DinePlan.DineConnect.House.MaterialStock
{
    public interface IMaterialStockAppService : IApplicationService
    {
        Task IncludeAllMaterial(IdInput input);
        Task InitialiseLocationWiseMaterials(HouseLocationWiseMaterialInitialiseDto input);

    }
}
