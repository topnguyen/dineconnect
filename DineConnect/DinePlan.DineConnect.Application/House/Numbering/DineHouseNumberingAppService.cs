﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Dto;
using System.Linq;
using DinePlan.DineConnect.Go.Departments.Dtos;
using DinePlan.DineConnect.House.Numbering.Exporter;

namespace DinePlan.DineConnect.House.Numbering
{
    public class DineHouseNumberingAppService : DineConnectAppServiceBase, IDineHouseNumberingAppService
    {
        private readonly IRepository<HouseNumber> _houseNumberRepository;
        private readonly IDineHouseNumberingExcelExporter _numberingExporter;
        public DineHouseNumberingAppService(
             IRepository<HouseNumber> houseNumberRepository,
             IDineHouseNumberingExcelExporter numberingExporter
        )
        {
            _houseNumberRepository = houseNumberRepository;
             _numberingExporter = numberingExporter;
        }

        public async Task<PagedResultOutput<DineHouseNumberingDto>> GetAll(GetDineHouseNumberingInput input)
        {
            var allItems = _houseNumberRepository.GetAll().WhereIf(
                       !input.Filter.IsNullOrEmpty(),
                           p => p.Name.Contains(input.Filter) ||
                           p.Prefix.Contains(input.Filter) ||
                           p.Length.ToString().Contains(input.Filter) ||
                            p.Suffix.Contains(input.Filter) ||
                           p.Begining.ToString().Contains(input.Filter));
                           
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<DineHouseNumberingDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<DineHouseNumberingDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetDineHouseNumberingInput input)
        {
            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<DineHouseNumberingDto>>();
            return _numberingExporter.ExportToFile(allListDtos);
        }

        public async Task<DineHouseNumberingDto> GetDineHouseNumberingForEdit(NullableIdInput input)
        {
            DineHouseNumberingDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _houseNumberRepository.FirstOrDefaultAsync(input.Id.Value);
                editDto = hDto.MapTo<DineHouseNumberingDto>();
            }
            else
            {
                editDto = new DineHouseNumberingDto();
            }

            return editDto;
        }

        public async Task<IdInput> CreateOrUpdateDineHouseNumbering(DineHouseNumberingDto input)
        {
            IdInput returnId;
            if (input.Id > 0)
            {
                returnId = await UpdateDinHouseNumbering(input);
            }
            else
            {
                returnId = await CreateDineHouseNumbering(input);
            }
            return returnId;
        }

        public async Task DeleteDineHouseNumbering(IdInput input)
        {
            await _houseNumberRepository.DeleteAsync(input.Id);
        }

        private async Task<IdInput> CreateDineHouseNumbering(DineHouseNumberingDto input)
        {
            var numbering = new HouseNumber();
            numbering.TenantId = AbpSession.TenantId ?? 1;
            numbering.Name = input.Name;
            numbering.Active = input.Active;
            numbering.LocationPrefix = input.LocationPrefix;
            numbering.Prefix = input.Prefix;
            numbering.Length = input.Length;
            numbering.Suffix = input.Suffix;
            numbering.Begining = input.Begining;
            var numberingId = await _houseNumberRepository.InsertAndGetIdAsync(numbering);
            await CurrentUnitOfWork.SaveChangesAsync();
            return new IdInput { Id = numberingId };
        }
        private async Task<IdInput> UpdateDinHouseNumbering(DineHouseNumberingDto input)
        {
            var numbering = await _houseNumberRepository.FirstOrDefaultAsync(input.Id);
            numbering.Name = input.Name;
            numbering.Active = input.Active;
            numbering.LocationPrefix = input.LocationPrefix;
            numbering.Prefix = input.Prefix;
            numbering.Length = input.Length;
            numbering.Suffix = input.Suffix;
            numbering.Begining = input.Begining;
            await _houseNumberRepository.UpdateAsync(numbering);
            return new IdInput { Id = numbering.Id };
        }
    }
}