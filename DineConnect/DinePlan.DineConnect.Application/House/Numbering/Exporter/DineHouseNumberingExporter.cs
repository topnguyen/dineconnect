﻿using System.Collections.Generic;
using DinePlan.DineConnect.Go.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Departments.Dtos;

namespace DinePlan.DineConnect.House.Numbering.Exporter
{
    public class DineHouseNumberingExcelExporter : FileExporterBase, IDineHouseNumberingExcelExporter
    {
        public FileDto ExportToFile(List<DineHouseNumberingDto> dtos)
        {
            return CreateExcelPackage(
                "DineHouseNumbering.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("DineHouseNumbering"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("Active"),
                        L("LocationPrefix"),
                        L("Prefix"),
                        L("Length"),
                        L("Suffix"),
                        L("Begining")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Name,
                        _ => _.Active ? "True" : "False",
                        _ => _.LocationPrefix ? "True" : "False",
                        _ => _.Prefix,
                        _ => _.Length,
                        _ => _.Suffix,
                        _ => _.Begining
                        );

                    for (var i = 1; i <= 7; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}