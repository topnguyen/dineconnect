﻿using System.Collections.Generic;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Departments.Dtos;

namespace DinePlan.DineConnect.House.Numbering.Exporter
{
    public interface IDineHouseNumberingExcelExporter
    {
        FileDto ExportToFile(List<DineHouseNumberingDto> dtos);
    }
}