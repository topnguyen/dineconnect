﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Go.Departments.Dtos;

namespace DinePlan.DineConnect.House.Numbering
{
    public interface IDineHouseNumberingAppService : IApplicationService
    {
        Task<PagedResultOutput<DineHouseNumberingDto>> GetAll(GetDineHouseNumberingInput inputDto);
        Task<FileDto> GetAllToExcel(GetDineHouseNumberingInput input);
        Task<DineHouseNumberingDto> GetDineHouseNumberingForEdit(NullableIdInput nullableIdInput);
        Task<IdInput> CreateOrUpdateDineHouseNumbering(DineHouseNumberingDto input);
        Task DeleteDineHouseNumbering(IdInput input);
        
    }
}