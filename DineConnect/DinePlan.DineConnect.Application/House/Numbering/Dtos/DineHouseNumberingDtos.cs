﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House;

namespace DinePlan.DineConnect.Go.Departments.Dtos
{
    [AutoMapFrom(typeof(HouseNumber))]
    public class DineHouseNumberingDto : FullAuditedEntityDto
    {
        public virtual string Name { get; set; }
        public bool Active { get; set; }
        public bool LocationPrefix { get; set; }
        public string Prefix { get; set; }
        public int Length { get; set; }
        public string Suffix { get; set; }
        public int Begining { get; set; }
        public virtual int? TenantId { get; set; }
    }

    public class GetDineHouseNumberingInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime Desc";
            }
        }
    }
}

