﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master
{
    public interface ITemplateAppService : IApplicationService
    {
        Task<PagedResultOutput<TemplateListDto>> GetAll(GetTemplateInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetTemplateForEditOutput> GetTemplateForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateTemplate(CreateOrUpdateTemplateInput input);

        Task DeleteTemplate(IdInput input);

        Task<ListResultOutput<TemplateListDto>> GetTemplateDetail(GetTemplateList input);

        Task CreateOrUpdateDayCloseTemplate(CreateOrUpdateDayCloseTemplateInput input);

        Task<ListResultOutput<TemplateInfoDto>> GetTemplateInfo(GetTemplateList input);
        Task<ListResultOutput<ComboboxItemDto>> GetTemplateTypeForCombobox();


    }
}