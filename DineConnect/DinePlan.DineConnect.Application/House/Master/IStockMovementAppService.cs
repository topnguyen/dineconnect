﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.House.Master
{
    public interface IStockMovementAppService : IApplicationService
    {
        //Task<List<StockMovementErrorDto>> StockMovementErrorFinding(StockMovementInputDto input);
        Task<List<MaterialTrackConsolidatedDto>> GetMaterialTrack(GetMaterialTrackDto input);

		Task<GetVerifyMaterialLedgerDto> VerifyAndRectifyClosingStock(VerifyMaterialLedgerClosingStockDto input);

	}
}