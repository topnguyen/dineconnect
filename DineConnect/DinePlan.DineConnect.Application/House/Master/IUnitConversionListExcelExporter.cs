﻿using System.Collections.Generic;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master
{
    public interface IUnitConversionListExcelExporter
    {
        FileDto ExportToFile(List<UnitConversionListDto> dtos);
    }
}