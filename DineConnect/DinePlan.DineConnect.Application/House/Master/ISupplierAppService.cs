﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Sync.Dtos;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master
{
    public interface ISupplierAppService : IApplicationService
    {
        Task<PagedResultOutput<SupplierListDto>> GetAll(GetSupplierInput inputDto);
        Task<FileDto> GetAllToExcel(GetSupplierInput input);
        Task<GetSupplierForEditOutput> GetSupplierForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateSupplier(CreateOrUpdateSupplierInput input);
        Task DeleteSupplier(IdInput input);

        Task<ListResultOutput<SupplierListDto>> GetSupplierNames();
        IdInput GetSupplierIdByName(string suppliername);
        Task<ListResultOutput<ComboboxItemDto>> GetSupplierForCombobox();
        //Task<ListResultOutput<SupplierListDto>> GetSimpleSupplierList();

        Task<ListResultOutput<ComboboxItemDto>> GetDocumentTypeForCombobox(NullableIdInput ninput);

        Task<ListResultOutput<ComboboxItemDto>> GetConstitutionStatusForCombobox(NullableIdInput ninput);
        Task<ListResultOutput<ComboboxItemDto>> GetGstStatusForCombobox(NullableIdInput ninput);
        Task<ListResultOutput<SupplierListDto>> ApiGetSupplier(TenantInput input);
        Task<PagedResultOutput<SimpleSupplierListDto>> GetAllSimpleSupplier(GetSupplierInput input);
        Task<string> GetSupplierNameForParticularCode(EntityDto input);
        Task<ListResultOutput<SimpleSupplierListDto>> GetSimpleSupplierListDtos();


    }
}