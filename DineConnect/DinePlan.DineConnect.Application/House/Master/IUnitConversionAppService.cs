﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Linq;

namespace DinePlan.DineConnect.House.Master
{
    public interface IUnitConversionAppService : IApplicationService
    {
        Task<PagedResultOutput<UnitConversionListDto>> GetAll(GetUnitConversionInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetUnitConversionForEditOutput> GetUnitConversionForEdit(NullableIdInput nullableIdInput);
        Task<List<IdInput>> CreateOrUpdateUnitConversion(CreateOrUpdateUnitConversionInput input);
        Task<bool> DeleteUnitConversion(IdInput input);
        Task<bool> DeleteUnitConversionMaterial(CreateOrUpdateUnitConversionInput input);
        Task<PagedResultOutput<UnitConversionWithNames>> GetAllWithNames(GetUnitConversionInput input);
        Task<ListResultOutput<UnitConversionListDto>> GetReferenceUnitIds(IdInput input);
        Task<ListResultOutput<ComboboxItemDto>> GetReferenceUnitForCombobox(IdInput input);

        Task<List<UnitConversionListDto>> GetAllUnitConversionWithBaseIds();
        IQueryable<UnitConversion> GetAllUnitConversionWithBaseIdsQueryable();

		Task<PagedResultOutput<UnitConversionListDto>> GetAllList(GetUnitConversionInput input);
        Task<DefaultUomErrorMessageDto> CheckMaterialDefaultUOM(IdInput input);
        Task<List<UnitConversionEditDto>> GetLinkedUnitCoversionForGivenMaterial(IdInput input);
        Task<List<DefaultUomErrorMessageDto>> ErrorMaterialDefaultUOMReport();

    }
}
