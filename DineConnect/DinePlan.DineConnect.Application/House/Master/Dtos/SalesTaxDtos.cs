﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.House.Master.Dtos
{
    [AutoMapFrom(typeof(SalesTax))]
    public class SalesTaxListDto : FullAuditedEntityDto
    {
        public string TaxName { get; set; }
        public decimal Percentage { get; set; }
        public string TaxCalculationMethod { get; set; }
        public int SortOrder { get; set; }

        public int Rounding { get; set; }
    }

    [AutoMapTo(typeof(SalesTax))]
    public class SalesTaxEditDto
    {
        public int? Id { get; set; }
        public string TaxName { get; set; }
        public decimal Percentage { get; set; }
        public string TaxCalculationMethod { get; set; }
        public int SortOrder { get; set; }
        public int Rounding { get; set; }
    }

    [AutoMapTo(typeof(SalesTax))]
    public class SalesTaxForMaterial
    {
        public int MaterialRefId { get; set; }
        public int SalesTaxRefId { get; set; }
        public string TaxName { get; set; }
        public decimal TaxRate { get; set; }
        public decimal TaxValue { get; set; }
        public int SortOrder { get; set; }
        public int Rounding { get; set; }
        public string TaxCalculationMethod { get; set; }

    }


    [AutoMapTo(typeof(SalesTax))]
    public class ApplicableSalesTaxesForMaterial
    {
        public int SalesTaxRefId { get; set; }
        public string TaxName { get; set; }
        public decimal TaxRate { get; set; }
        public int SortOrder { get; set; }
        public int Rounding { get; set; }
        public string TaxCalculationMethod { get; set; }
    }


    public class GetSalesTaxInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetSalesTaxForEditOutput : IOutputDto
    {
        public SalesTaxEditDto SalesTax { get; set; }
        public List<SalesTaxTemplateMappingEditDto> SalesTaxTemplateMapping { get; set; }
    }
    public class CreateOrUpdateSalesTaxInput : IInputDto
    {
        [Required]
        public SalesTaxEditDto SalesTax { get; set; }

        [Required]
        public List<SalesTaxTemplateMapping> SalesTaxTemplateMapping { get; set; }
    }
}
