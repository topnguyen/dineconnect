﻿

using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;

namespace DinePlan.DineConnect.House.Master.Dtos
{
    [AutoMapTo(typeof(MaterialGroupCategory))]
    public class MaterialGroupCategoryViewDto : FullAuditedEntityDto
    {
        //TODO: DTO MaterialGroupCategory Properties Missing
        public string MaterialGroupCategoryName { get; set; }
        public string MaterialGroupRefName { get; set; }
		public int? SyncId { get; set; }
		public DateTime? SyncLastModification { get; set; }

	}


	[AutoMapFrom(typeof(MaterialGroupCategory))]
    public class MaterialGroupCategoryListDto : FullAuditedEntityDto
    {
        //TODO: DTO MaterialGroupCategory Properties Missing
        //YOU CAN REFER ATTRIBUTES IN 
        public string MaterialGroupCategoryName { get; set; }
        public int MaterialGroupRefId { get; set; }

		public int? SyncId { get; set; }
		public DateTime? SyncLastModification { get; set; }

	}
	[AutoMapTo(typeof(MaterialGroupCategory))]
    public class MaterialGroupCategoryEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO MaterialGroupCategory Properties Missing
        public string MaterialGroupCategoryName { get; set; }
        public int MaterialGroupRefId { get; set; }

		public int? SyncId { get; set; }
		public DateTime? SyncLastModification { get; set; }

	}

	public class GetMaterialGroupCategoryInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetMaterialGroupCategoryForEditOutput : IOutputDto
    {
        public MaterialGroupCategoryEditDto MaterialGroupCategory { get; set; }
    }
    public class CreateOrUpdateMaterialGroupCategoryInput : IInputDto
    {
        [Required]
        public MaterialGroupCategoryEditDto MaterialGroupCategory { get; set; }
    }
}

