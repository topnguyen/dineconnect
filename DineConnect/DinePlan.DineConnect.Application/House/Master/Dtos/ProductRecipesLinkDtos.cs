﻿
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using DinePlan.DineConnect.House.Master.Dtos;
using System.Collections.Generic;

namespace DinePlan.DineConnect.House.Master.Dtos
{
    [AutoMapFrom(typeof(ProductRecipesLink))]
    public class ProductRecipesLinkListDto : FullAuditedEntityDto
    {
        public int PosRefId { get; set; }
        public string PosRefName { get; set; }
        public decimal UserSerialNumber { get; set; }
        public int RecipeRefId { get; set; }
        public string RecipeRefName { get; set; }
        public decimal PortionQty { get; set; }
        public int PortionUnitId { get; set; }
        public string PortionUnitName { get; set; }
        public string RecipeType { get; set; }
        public decimal WastageExpected { get; set; }
    }
    [AutoMapTo(typeof(ProductRecipesLink))]
    public class ProductRecipesLinkEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO ProductRecipesLink Properties Missing
        public int PosRefId { get; set; }
        public decimal UserSerialNumber { get; set; }
        public int RecipeRefId { get; set; }
        public decimal PortionQty { get; set; }
        public int PortionUnitId { get; set; }
        public string RecipeType { get; set; }
        public decimal WastageExpected { get; set; }
    }



    public class GetProductRecipesLinkInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetProductRecipesLinkForEditOutput : IOutputDto
    {
        public List<ProductRecipesLinkListDto> ProductRecipesLinkDetail { get; set; }
    }
    public class CreateOrUpdateProductRecipesLinkInput : IInputDto
    {
        [Required]
        public List<ProductRecipesLinkListDto> ProductRecipesLinkDetail { get; set; }
    }
}

