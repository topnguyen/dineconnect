﻿

using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;

namespace DinePlan.DineConnect.House.Master.Dtos
{
    [AutoMapFrom(typeof(MaterialBrandsLink))]
    public class MaterialBrandsLinkListDto : FullAuditedEntityDto
    {
        //TODO: DTO MaterialBrandsLink Properties Missing
        //YOU CAN REFER ATTRIBUTES IN 
        public int MaterialRefId { get; set; }
        public int BrandRefId { get; set; }

    }
    [AutoMapTo(typeof(MaterialBrandsLink))]
    public class MaterialBrandsLinkEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO MaterialBrandsLink Properties Missing
        public int MaterialRefId { get; set; }

        public int BrandRefId { get; set; }

    }
    [AutoMapTo(typeof(MaterialBrandsLink))]
    public class MaterialBrandsLinkViewDto
    {
        public int? Id { get; set; }
        //TODO: DTO MaterialBrandsLink Properties Missing
        public string MaterialRefName { get; set; }

        public string BrandRefName { get; set; }

    }
    public class GetMaterialBrandsLinkInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
    public class GetMaterialBrandsLinkForEditOutput : IOutputDto
    {
        public MaterialBrandsLinkEditDto MaterialBrandsLink { get; set; }
    }
    public class CreateOrUpdateMaterialBrandsLinkInput : IInputDto
    {
        [Required]
        public MaterialBrandsLinkEditDto MaterialBrandsLink { get; set; }
    }
}

