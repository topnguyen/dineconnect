﻿

using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;

namespace DinePlan.DineConnect.House.Master.Dtos
{
    [AutoMapFrom(typeof(TaxTemplateMapping))]
    public class TaxTemplateMappingListDto : FullAuditedEntityDto
    {
        public int TaxRefId { get; set; }
        public int? CompanyRefId { get; set; }
        public int? LocationRefId { get; set; }
        public int? MaterialGroupRefId { get; set; }
        public int? MaterialGroupCategoryRefId { get; set; }
        public int? MaterialRefId { get; set; }

    }
    [AutoMapTo(typeof(TaxTemplateMapping))]
    public class TaxTemplateMappingEditDto
    {
        public int? Id { get; set; }
        public int TaxRefId { get; set; }
        public int? CompanyRefId { get; set; }
        public int? LocationRefId { get; set; }

        public int? MaterialGroupRefId { get; set; }

        public int? MaterialGroupCategoryRefId { get; set; }
      
        public int? MaterialRefId { get; set; }
     
    }

  

    public class GetTaxTemplateMappingInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "TaxRefId";
            }
        }
    }
    public class GetTaxTemplateMappingForEditOutput : IOutputDto
    {
        public TaxTemplateMappingEditDto TaxTemplateMapping { get; set; }
    }
    public class CreateOrUpdateTaxTemplateMappingInput : IInputDto
    {
        [Required]
        public TaxTemplateMappingEditDto TaxTemplateMapping { get; set; }
    }
}

