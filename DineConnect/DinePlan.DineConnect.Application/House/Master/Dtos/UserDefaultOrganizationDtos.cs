﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using DinePlan.DineConnect.Connect.Master.Dtos;
using System.Collections.Generic;

namespace DinePlan.DineConnect.House.Master.Dtos
{
    [AutoMapFrom(typeof(UserDefaultOrganization))]
    public class UserDefaultOrganizationListDto : FullAuditedEntityDto
    {
		public virtual int? DefaultOrganisationRefId { get; set; }
        public virtual string OrganizationUnitName { get; set; }
        public virtual string UserName { get; set; }
        public virtual long UserId { get; set; }
		public virtual long? DefaultLocationRefId { get; set; }
		public virtual string DefaultLocationName { get; set; }
		public virtual List<LocationListDto> LocationList { get; set; }
		public virtual List<ComboboxItemDto> CompanyList { get; set; }
		public virtual string LocationCodeList { get; set; }
		public virtual int LocationCount { get; set; }
	}


	[AutoMapTo(typeof(UserDefaultOrganization))]
    public class UserDefaultOrganizationEditDto
    {
        public int? Id { get; set; }
        public virtual long OrganizationUnitId { get; set; }

        public virtual long UserId { get; set; }

    }

	[AutoMapTo(typeof(UserDefaultOrganization))]
    public class UserDefaultOrganizationViewDto
    {
        public int? Id { get; set; }
        //TODO: DTO UserDefaultOrganization Properties Missing
        public virtual string OrganizationUnitName { get; set; }

        public virtual string UserName { get; set; }

    }

    public class GetUserDefaultOrganizationInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetUserDefaultOrganizationForEditOutput : IOutputDto
    {
        public UserDefaultOrganizationEditDto UserDefaultOrganization { get; set; }
    }
    public class CreateOrUpdateUserDefaultOrganizationInput : IInputDto
    {
        [Required]
        public UserDefaultOrganizationEditDto UserDefaultOrganization { get; set; }
    }
}

