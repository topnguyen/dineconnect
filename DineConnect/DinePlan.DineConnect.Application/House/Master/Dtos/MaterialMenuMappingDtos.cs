﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Master.Dtos;

namespace DinePlan.DineConnect.House.Master.Dtos
{
    [AutoMapFrom(typeof(MaterialMenuMapping))]
    public class MaterialMenuMappingListDto : FullAuditedEntityDto
    {
        public int PosMenuPortionRefId { get; set; }
        public string MenuItemName { get; set; }
        public string PortionName { get; set; }
        public string PosRefName { get; set; }
        public int MenuQuantitySold { get; set; }

        public decimal UserSerialNumber { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialRefCode { get; set; }
        public string MaterialRefName { get; set; }
        public int MaterialTypeId { get; set; }
        public string MaterialTypeName { get; set; }

        public decimal PortionQty { get; set; }
        public int PortionUnitId { get; set; }
        public string UnitRefName { get; set; }
        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }
        public decimal WastageExpected { get; set; }
        public bool AutoSalesDeduction { get; set; }
        public string DeptNames { get; set; }
        public List<ComboboxItemDto> DeptList { get; set; }

        public int? LocationRefId { get; set; }

    }

    [AutoMapTo(typeof(MaterialMenuMapping))]
    public class MaterialMenuMappingEditDto
    {
        public int? Id { get; set; }
        public int PosMenuPortionRefId { get; set; }
        public int MenuQuantitySold { get; set; }

        public decimal UserSerialNumber { get; set; }
        public int MaterialRefId { get; set; }
        public decimal PortionQty { get; set; }
        public int PortionUnitId { get; set; }
        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }
        public decimal WastageExpected { get; set; }
        public bool AutoSalesDeduction { get; set; }

        public List<ComboboxItemDto> DeptList { get; set; }
        public int? LocationRefId { get; set; }
    }

    [AutoMapTo(typeof(MenuMappingDepartment))]
    public class MenuMappingDepartmentListDto
    {
        public int MenuMappingRefId { get; set; }

        public int DepartmentRefId { get; set; }

        public string DepartmentName { get; set; }
    }


	public class GetMaterialMenuMappingInput : PagedAndSortedInputDto, IShouldNormalize
	{
        public int? LocationRefId { get; set; }
		public string Filter { get; set; }

		public string Operation { get; set; }

		public string ListFilter {get;set;}

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "PosRefName";
            }
        }
    }
    public class GetMaterialMenuMappingForEditOutput : IOutputDto
    {
        public List<MaterialMenuMappingListDto> MaterialMenuMappingDetail { get; set; }
    }
    public class CreateOrUpdateMaterialMenuMappingInput : IInputDto
    {
        [Required]
        public List<MaterialMenuMappingListDto> MaterialMenuMappingDetail { get; set; }
    }

    public class ProductListDto
    {
        public int PortionId { get; set; }
        public string PortionName { get; set; }
        public string AliasName { get; set; }
        public string AliasCode { get; set; }

    }

    [AutoMapFrom(typeof(MaterialMenuMapping))]
    public class MaterialMenuMappingWithWipeOut 
    {
        public int? LocationRefId { get; set; }
        public string DepartmentName { get; set; }
        public int PosMenuPortionRefId { get; set; }
        public string PosRefName { get; set; }
        public int MenuQuantitySold { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public decimal PortionPerUnitOfSales { get; set; }
        public decimal PortionQty { get; set; }
        public int PortionUnitId { get; set; }
        public string UnitRefName { get; set; }
        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }
        public decimal WastageExpected { get; set; }
        public bool AutoSalesDeduction { get; set; }
        public int NoOfPax { get; set; }
        public decimal TotalSaleQuantity { get; set; }
    }

    [AutoMapFrom(typeof(MaterialMenuMapping))]
    public class MaterialMenuMappingDto
    {
        public int? LocationRefId { get; set; }
        public string DepartmentName { get; set; }
        public int PosMenuPortionRefId { get; set; }

        public string PosRefName { get; set; }
        public string MenuItemName { get; set; }
        public string Portion { get; set; }

        public int MenuQuantitySold { get; set; }

        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public decimal PortionQty { get; set; }
        public int PortionUnitId { get; set; }
        public string UnitRefName { get; set; }
        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }
        public decimal WastageExpected { get; set; }
        public bool AutoSalesDeduction { get; set; }
        public decimal TotalSaleQuantity { get; set; }
        public decimal TotalSaleValue { get; set; }
        public decimal CostPerUnit { get; set; }
        public decimal CostTotal { get; set; }

    }

    public class MaterialMenuMappingViewDto : FullAuditedEntityDto
    {
        public int PosMenuPortionRefId { get; set; }

        public string PosRefName { get; set; }
        public bool AutoSales { get; set; }
    }

	public class MenuMappingWithMaterialDto 
	{
		public int MenuPortionId { get; set; }
		public string Name { get; set; }
		public int MaterialRefId { get; set; }
        public int? LocationRefId { get; set; }

	}

	public class MenuMappingWithMaterialCountViewDto : FullAuditedEntityDto
    {
        public int PosMenuPortionRefId { get; set; }

        public string PosRefName { get; set; }

        public int NumberOfMaterials { get; set; }

        public bool AutoSales { get; set; }
        public List<MenuMappingWithMaterialDto> MaterialList { get; set; }
    }

    public class MaterialMappingPortionDto
    {
        public int PortionId { get; set; }
        public bool AutoSales { get; set; }
    }


    [AutoMapTo(typeof(MaterialMenuMapping))]
    public class MaterialMappingTemplateDto
    {
        public int? Id { get; set; }
        public int? LocationRefId { get; set; }
        public int PosMenuPortionRefId { get; set; }
        public int MenuQuantitySold { get; set; }
        public decimal UserSerialNumber { get; set; }
        public int MaterialRefId { get; set; }
        public decimal PortionQty { get; set; }
        public int PortionUnitId { get; set; }
        public decimal WastageExpected { get; set; }
        public bool AutoSalesDeduction { get; set; }
    }
    public class MaterialMappingTemplate
    {
        public List<MaterialMappingTemplateDto> MappingList { get; set; }
    }

	[AutoMapTo(typeof(MenuMappingDepartment))]
	public class MenuMappingDepartmentTemplateDto
	{
		public int? Id { get; set; }
		public int MenuMappingRefId { get; set; }
		public int DepartmentRefId { get; set; }
	}
	

	public class MenuMappingDepartmentTemplate
	{
		public List<MenuMappingDepartmentTemplateDto> MenuMappingDepartment { get; set; }
	}

	public class MaterialUsageDetailDto
	{
        public int? LocationRefId { get; set; }
        public string DepartmentName { get; set; }
		public int PosMenuPortionRefId { get; set; }
		public string PosRefName { get; set; }
		public decimal MenuQuantitySold { get; set; }
		public decimal PortionPerQtySold { get; set; }
		public int RecipeRefId { get; set; }
		public string RecipeRefName { get; set; }
		public decimal RecipeUsedQty { get; set; }
		public int RecipeUsedUnitRefId { get; set; }
		public string RecipeUsedUnitRefName { get; set; }
		public decimal RecipePortionPerQtySold { get; set; }
        public int MaterialGroupCategoryRefId { get; set; }
		public int MaterialTypeId { get; set; }
		public string MaterialTypeName { get; set; }
		public int MaterialRefId { get; set; }
		public string MaterialRefName { get; set; }
		public decimal PortionQty { get; set; }
		public int PortionUnitId { get; set; }
		public int UnitRefId { get; set; }
		public string UnitRefName { get; set; }
		public int DefaultUnitId { get; set; }
		public string DefaultUnitName { get; set; }
		public decimal CostPerUnit { get; set; }
		public decimal CostTotal { get; set; }

	}

    public class MaterialMenuWithLocation : IInputDto
    {
        public int PosMenuPortionRefId { get; set; }

        public int? LocationRefId { get; set; }

        public int MaterialRefId { get; set; }

        public List<LocationListDto> SelectedLocations { get; set; }
    }

    public class DeleteMaterialMenuWithLocationList : IInputDto
    {
        public List<MaterialMenuWithLocation> MaterialMenuWithLocations { get; set; }
    }

}