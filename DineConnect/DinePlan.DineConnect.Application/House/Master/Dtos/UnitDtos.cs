﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;

namespace DinePlan.DineConnect.House.Master.Dtos
{
    [AutoMapFrom(typeof(Unit))]
    public class UnitListDto : FullAuditedEntityDto
    {
        //TODO: DTO Unit Properties Missing
        //YOU CAN REFER ATTRIBUTES IN 
        public virtual string Name { get; set; }
        public virtual int NoOfConversions { get; set; }
		public int? SyncId { get; set; }
		public DateTime? SyncLastModification { get; set; }

	}
	[AutoMapTo(typeof(Unit))]
    public class UnitEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO Unit Properties Missing
        public virtual string Name { get; set; }
		public int? SyncId { get; set; }
		public DateTime? SyncLastModification { get; set; }


	}

	public class GetUnitInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetUnitForEditOutput : IOutputDto
    {
        public UnitEditDto Unit { get; set; }
    }
    public class CreateOrUpdateUnitInput : IInputDto
    {
        [Required]
        public UnitEditDto Unit { get; set; }
    }
}

