﻿

using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.House.Transaction.Dtos
{

    [AutoMapFrom(typeof(MaterialLedger))]
    public class MaterialLedgerListDto : FullAuditedEntityDto
    {
        //TODO: DTO MaterialLedger Properties Missing
        //YOU CAN REFER ATTRIBUTES IN 
        public int LocationRefId { get; set; }
        public DateTime? LedgerDate { get; set; }
        public int MaterialRefId { get; set; }
        public decimal OpenBalance { get; set; }
        public decimal Received { get; set; }
        public decimal Issued { get; set; }
        public decimal Sales { get; set; }
        public decimal TransferIn { get; set; }
        public decimal TransferOut { get; set; }
        public decimal Damaged { get; set; }
        public decimal SupplierReturn { get; set; }
        public decimal ExcessReceived { get; set; }
        public decimal Shortage { get; set; }
        public decimal Return { get; set; }
        public decimal ClBalance { get; set; }
    }
    [AutoMapTo(typeof(MaterialLedger))]
    public class MaterialLedgerEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO MaterialLedger Properties Missing
        public int LocationRefId { get; set; }
        public DateTime? LedgerDate { get; set; }
        public int MaterialRefId { get; set; }
        public decimal OpenBalance { get; set; }
        public decimal Received { get; set; }
        public decimal Issued { get; set; }
        public decimal Sales { get; set; }
        public decimal TransferIn { get; set; }
        public decimal TransferOut { get; set; }
        public decimal Damaged { get; set; }
        public decimal SupplierReturn { get; set; }
        public decimal ExcessReceived { get; set; }
        public decimal Shortage { get; set; }
        public decimal Return { get; set; }
        public decimal ClBalance { get; set; }
    }

	[AutoMapTo(typeof(MaterialLedger))]
    public class MaterialLedgerViewDto
    {
        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }
        public DateTime? LedgerDate { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialRefName{ get; set; }
		public int DefaultUnitRefId { get; set; }
        public string Uom { get; set; }
        public decimal OpenBalance { get; set; }
        public decimal Received { get; set; }
        public decimal Issued { get; set; }
        public decimal Sales { get; set; }
        public decimal TransferIn { get; set; }
        public decimal TransferOut { get; set; }
        public decimal Damaged { get; set; }
        public decimal SupplierReturn { get; set; }
        public decimal ExcessReceived { get; set; }
        public decimal Shortage { get; set; }
        public decimal Return { get; set; }
        public decimal ClBalance { get; set; }

        public decimal OverAllReceived { get; set; }
        public decimal OverAllIssued { get; set; }

		public int TransactionUnitRefId { get; set; }
		public string TransactionUnitRefName { get; set; }
    }

    public class GetMaterialLedgerInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public int? LocationRefId { get; set; }

        public int? MaterialRefId { get; set; }
        public List<int> MaterialRefIds { get; set; }

        public string Filter { get; set; }

        public string Operation { get; set; }

        public int UserId { get; set; }

        public string CalledReason { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "MaterialRefName";
            }
        }
    }
    public class GetMaterialLedgerForEditOutput : IOutputDto
    {
        public MaterialLedgerEditDto MaterialLedger { get; set; }
    }
    public class CreateOrUpdateMaterialLedgerInput : IInputDto
    {
        [Required]
        public MaterialLedgerEditDto MaterialLedger { get; set; }
    }

    //public class GetMaterialLedgerInfo : PagedAndSortedInputDto, IShouldNormalize
    //{
    //    public DateTime StartDate { get; set; }
    //    public DateTime EndDate { get; set; }

    //    public int? LocationRefId { get; set; }

    //    public int? MaterialRefId { get; set; }

    //    public void Normalize()
    //    {
    //        if (string.IsNullOrEmpty(Sorting))
    //        {
    //            Sorting = "CreationTime DESC";
    //        }
    //    }
    //}

    public class RequiredLocationWiseMaterialLedgerDto
    {
        public RequiredLocationWiseMaterialLedgerDto()
        {

        }
        public RequiredLocationWiseMaterialLedgerDto(int argLocationRefId, int argMaterialRefId, bool argSuccessFlag)
        {
            this.LocationRefId = argLocationRefId;
            this.MaterialRefId = argMaterialRefId;
            this.SuccessFlag = argSuccessFlag;
        }

        public int LocationRefId { get; set; }
        public int MaterialRefId { get; set; }
        public bool SuccessFlag { get; set; }
        public MaterialLedger MaterialLedger { get; set; }
        public DateTime? MaximumLedgerDate => MaterialLedger == null ? null : MaterialLedger.LedgerDate;
    }

    public class LocationWiseMaterialLedgerListDto
    {
        public int LocationRefId { get; set; }
        public List<MaterialLedgerListDto> MaterialLedgerListDtos { get; set; }
    }
}

