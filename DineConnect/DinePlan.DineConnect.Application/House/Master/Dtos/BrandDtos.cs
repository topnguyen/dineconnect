﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master.Dtos
{
    [AutoMapFrom(typeof(Brand))]
    public class BrandListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
    }
    [AutoMapTo(typeof(Brand))]
    public class BrandEditDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
    }

    public class GetBrandInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }
    public class GetBrandForEditOutput : IOutputDto
    {
        public BrandEditDto Brand { get; set; }
    }
    public class CreateOrUpdateBrandInput : IInputDto
    {
        [Required]
        public BrandEditDto Brand { get; set; }
    }
}
