﻿

using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;

namespace DinePlan.DineConnect.House.Master.Dtos
{
    [AutoMapFrom(typeof(CustomerTagDefinition))]
    public class CustomerTagDefinitionListDto : FullAuditedEntityDto
    {
        public string TagCode { get; set; }             //  Short Description

        public string TagName { get; set; }      //  Tag Name
        //TODO: DTO CustomerTagDefinition Properties Missing
        //YOU CAN REFER ATTRIBUTES IN 
    }
    [AutoMapTo(typeof(CustomerTagDefinition))]
    public class CustomerTagDefinitionEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO CustomerTagDefinition Properties Missing
        public string TagCode { get; set; }             //  Short Description

        public string TagName { get; set; }      //  Tag Name

    }

    public class GetCustomerTagDefinitionInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetCustomerTagDefinitionForEditOutput : IOutputDto
    {
        public CustomerTagDefinitionEditDto CustomerTagDefinition { get; set; }
    }
    public class CreateOrUpdateCustomerTagDefinitionInput : IInputDto
    {
        [Required]
        public CustomerTagDefinitionEditDto CustomerTagDefinition { get; set; }
    }
}

