﻿
//using System.ComponentModel.DataAnnotations;
//using Abp.Application.Services.Dto;
//using Abp.AutoMapper;
//using Abp.Runtime.Validation;
//using DinePlan.DineConnect.Dto;
//namespace DinePlan.DineConnect.House.Master.Dtos
//{
//    [AutoMapFrom(typeof(StorageLocation))]
//    public class StorageLocationListDto : FullAuditedEntityDto
//    {
//        //TODO: DTO StorageLocation Properties Missing
//        //YOU CAN REFER ATTRIBUTES IN 

//        [Required, MaxLength(30)]
//        public string StorageName { get; set; }

//        [Required, MaxLength(10)]
//        public string StorageReference { get; set; }

//        [Required]
//        public int LocationRefId { get; set; }

//        public string BinId { get; set; }

//        public string SortId { get; set; }

//        [Required]
//        public bool IsFreezerAvailable { get; set; }
//    }
//    [AutoMapTo(typeof(StorageLocation))]
//    public class StorageLocationEditDto
//    {
//        public int? Id { get; set; }
//        //TODO: DTO StorageLocation Properties Missing

//        [Required, MaxLength(30)]
//        public string StorageName { get; set; }

//        [Required, MaxLength(10)]
//        public string StorageReference { get; set; }

//        [Required]
//        public int LocationRefId { get; set; }

//        public string BinId { get; set; }

//        public string SortId { get; set; }

//        [Required]
//        public bool IsFreezerAvailable { get; set; }

//    }

//    public class GetStorageLocationInput : PagedAndSortedInputDto, IShouldNormalize
//    {
//        public string Filter { get; set; }

//        public string Operation { get; set; }

//        public void Normalize()
//        {
//            if (string.IsNullOrEmpty(Sorting))
//            {
//                Sorting = "Id";
//            }
//        }
//    }
//    public class GetStorageLocationForEditOutput : IOutputDto
//    {
//        public StorageLocationEditDto StorageLocation { get; set; }
//    }
//    public class CreateOrUpdateStorageLocationInput : IInputDto
//    {
//        [Required]
//        public StorageLocationEditDto StorageLocation { get; set; }
//    }
//}

