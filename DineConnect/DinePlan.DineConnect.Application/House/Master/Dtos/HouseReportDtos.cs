﻿

using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.House.Transaction.Dtos;

namespace DinePlan.DineConnect.House.Master.Dtos
{
    [AutoMapFrom(typeof(HouseReport))]
    public class HouseReportListDto : FullAuditedEntityDto
    {
        //TODO: DTO HouseReport Properties Missing
        //YOU CAN REFER ATTRIBUTES IN 
        public string ReportKey { get; set; }
        public string Description { get; set; }
        public string ReportRole { get; set; }

    }
    [AutoMap(typeof(HouseReport))]
    public class HouseReportEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO HouseReport Properties Missing
        public string ReportKey { get; set; }
        public string Description { get; set; }
        public string ReportRole { get; set; }

    }

    public class GetHouseReportInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int LocationRefId { get; set; }

        public List<LocationListDto> Locations { get; set; }

        public string ReportKey { get; set; }

        public string Filter { get; set; }

        public string Operation { get; set; }

        public string MaterialName { get; set; }
        public int? MaterialRefId { get; set; }

        public List<int> MaterialRefIds { get; set; }

        public List<ComboboxItemDto> MaterialTypeList { get; set; }

        public bool IsHighValueItemOnly { get; set; }

        public bool ExcludeRateView { get; set; }

        public bool VarianceAndTheoreticalUsageNeeded { get; set; }
        public int ExportType { get; set; }
        public bool CallfromIndex { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "MaterialName";
            }
        }

        public bool LiveStock { get; set; }

        public List<Material> RequestedMaterialList { get; set; }

        public List<MaterialRateViewListDto> MaterialRateView { get; set; }

        public bool CallFromInternalLoop { get; set; }
        public bool RunInBackGround { get; set; }
        public int TenantId { get; set; }

    }

    public class GetMaterialTrackDto
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int LocationRefId { get; set; }

        public List<LocationListDto> Locations { get; set; }

        public List<ComboboxItemDto> Materials { get; set; }

        public int? MaterialRefId { get; set; }

        public int? MaterialGroupCategoryRefId { get; set; }

        public int? MaterialGroupRefId { get; set; }

    }

    public class GetHouseReportMaterialRateInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public List<LocationListDto> Locations { get; set; }

        public List<int> MaterialRefIds { get; set; }

        public List<ComboboxItemDto> MaterialTypeList { get; set; }

        public int? LocationRefId { get; set; }

        public string MaterialRefNameLike { get; set; }
        public bool IsHighValueItemOnly { get; set; }

        public bool ForceRefreshFlag { get; set; }

        public int RecursiveCount { get; set; }

        public string FunctionCalledBy { get; set; }
        public bool DoesIngredientsDetailsRequired { get; set; }

        public bool RunInBackGround { get; set; }
        public int TenantId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                // Sorting = "ReportKey";
            }
        }
    }

    public class GetCostReportInput : PagedAndSortedInputDto
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<LocationListDto> Locations { get; set; }
        public List<ComboboxItemDto> Recipes { get; set; }
        public List<MaterialRateViewListDto> MaterialRateView { get; set; }
        public List<ComboboxItemDto> MenuItems { get; set; }
        public bool GiftIncludeFlag { get; set; }
        public bool CompIncludeFlag { get; set; }
        public int? LocationRefId { get; set; }
        public int? MenuPortionRefId { get; set; }
        public bool IncludeFinishWastage { get; set; }
    }

    public class GetProductionUnitCostReportInput : PagedAndSortedInputDto
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<LocationListDto> Locations { get; set; }
        public List<ProductionUnitListDto> ProductionUnits { get; set; }
        public List<MaterialRateViewListDto> MaterialRateView { get; set; }
    }

    public class MaterialRateView
    {
        public int LocationRefId { get; set; }
        public int MaterialRefId { get; set; }
        public int AveragePriceTagRefId { get; set; }
        public decimal BilledQty { get; set; }
        public int DefaultUnitId { get; set; }
        public decimal NetAmount { get; set; }
        public decimal AvgRate { get; set; }
        public bool RefreshRequird { get; set; }
        public DateTime LastUpdatedTime { get; set; }

    }

    [AutoMapFrom(typeof(LocationWiseMaterialRateViewListDto))]
    public class MaterialRateViewListDto
    {
        public int IssueGroupCategoryId { get; set; }
        public string IssueGroupCategoryName { get; set; }
        public int MaterialTypeId { get; set; }
        public string MaterialTypeRefName { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialName { get; set; }
        public decimal BilledQty { get; set; }
        public string Uom { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal NetAmount { get; set; }
        public decimal AvgRate { get; set; }
        public DateTime PurchaseDate { get; set; }
        public int SortId { get; set; }
        public string InvoiceDateRange { get; set; }
        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }
        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }
        public int AveragePriceTagRefId { get; set; }
        public string AveragePriceTagRefName { get; set; }
        public int NoOfMonthAvgTaken { get; set; }
        public DateTime LastUpdatedTime { get; set; }
        public decimal TotalIngrCost { get; set; }
        public decimal PrdBatchQty { get; set; }
        public List<MaterialIngredientRequiredDto> MaterialIngredientRequiredDtos { get; set; }
    }

    public class GetStartDateAndEndDate
    {
        public int MaterialRefId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal StockHand { get; set; }
        public decimal RunningStock { get; set; }
        public bool InterTransferExist { get; set; }
    }

    public class VarianceReportDto
    {
        public int MaterialGroupCategoryRefId { get; set; }
        public string MaterialGroupCategoryRefName { get; set; }
        public List<MaterialVarianceReportViewDto> VarianceReport { get; set; }
    }

    [AutoMap(typeof(MaterialLedgerDto))]
    public class MaterialVarianceReportViewDto
    {
        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int MaterialGroupCategoryRefId { get; set; }
        public string MaterialGroupCategoryRefName { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public int DefaultUnitRefId { get; set; }
        public string Uom { get; set; }
        public decimal AvgRate { get; set; }
        public MaterialRateViewListDto MaterialRateDto { get; set; }
        public decimal OpenBalance { get; set; }
        public decimal Received { get; set; }
        public decimal Issued { get; set; }
        public decimal Sales { get; set; }
        public decimal TransferIn { get; set; }
        public decimal TransferOut { get; set; }
        public decimal Damaged { get; set; }
        public decimal SupplierReturn { get; set; }
        public decimal ExcessReceived { get; set; }
        public decimal Shortage { get; set; }
        public decimal Return { get; set; }
        public decimal ClBalance { get; set; }

        public decimal OverAllReceived { get; set; }
        public decimal OverAllIssued { get; set; }

        public int TransactionUnitRefId { get; set; }
        public string TransactionUnitRefName { get; set; }
        public bool IsClosingStockEntered { get; set; }
        public decimal ClosingStockEntered { get; set; }
        public decimal Variance { get; set; }
        public decimal VarianceCost { get; set; }

    }

    public class MaterialUsageWithPriceAndDateRange
    {
        public int MaterialGroupCategoryRefId { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public decimal UsagePerUnit { get; set; }
        public string Uom { get; set; }
        public decimal AvgRate { get; set; }

        public decimal UsageQty { get; set; }

        public decimal NetAmount { get; set; }
        public decimal CostPercentage { get; set; }
        public string InvoiceDateRange { get; set; }

        public List<MaterialMenuMappingDto> MenuMappingList { get; set; }
    }

    public class MaterialStandardVsActualUsageWithPriceAndDateRange
    {
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public string Uom { get; set; }
        public decimal AvgRate { get; set; }
        public decimal StandardQty { get; set; }
        public decimal StandardAmount { get; set; }
        public decimal StandardCostPercentage { get; set; }
        public decimal ActualQty { get; set; }
        public decimal ActualAmount { get; set; }
        public decimal ActualCostPercentage { get; set; }

        public decimal QuantityDifference { get; set; }
        public decimal CostDifference { get; set; }
        public string InvoiceDateRange { get; set; }

        public List<MaterialUsageDetailDto> StandardMaterialUsageDetail { get; set; }

        public List<MaterialLedgerDto> ActualMaterialUsageDetail { get; set; }


    }

    public class SupplierRateViewListDto
    {
        public int MaterialRefId { get; set; }
        public string MaterialName { get; set; }
        public int SupplierRefId { get; set; }
        public string SupplierName { get; set; }
        public decimal BilledQty { get; set; }
        public decimal NetAmount { get; set; }
        public decimal Rate { get; set; }
        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }
        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }
        public DateTime InvoiceDate { get; set; }
    }

    public class ProductAnalysisViewDto
    {
        public int MaterialRefId { get; set; }
        public string MaterialName { get; set; }
        public decimal AvgQty { get; set; }
        public decimal TotalQty { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal AvgPrice { get; set; }
        public decimal MinPrice { get; set; }
        public decimal MaxPrice { get; set; }
        public decimal OpenBalance { get; set; }
        public decimal Receipt { get; set; }
        public decimal Issue { get; set; }
        public decimal ClBalance { get; set; }

        public decimal Price { get; set; }

        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }

        public string Uom { get; set; }

        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }

        public List<SupplierAveragePriceViewForMaterialListDto> SupplierAveragePrice { get; set; }
        public List<PurchaseParticularMaterialSupplierWise> PurchaseDetailForParticularMaterial { get; set; }
    }

    public class SupplierAveragePriceViewForMaterialListDto
    {
        public int MaterialRefId { get; set; }
        public string MaterialName { get; set; }
        public int SupplierRefId { get; set; }
        public string SupplierName { get; set; }
        public decimal BilledQty { get; set; }
        public decimal Price { get; set; }
        public decimal PurchaseAmount { get; set; }
        public decimal MaterialAveragePrice { get; set; }
        public decimal StandardDeviation { get; set; }

        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }
        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }

    }


    public class PurchaseParticularMaterialSupplierWise
    {
        public int MaterialRefId { get; set; }
        public string MaterialName { get; set; }
        public int SupplierRefId { get; set; }
        public string SupplierName { get; set; }
        public string InvoiceRef { get; set; }
        public DateTime PurchaseDate { get; set; }
        public decimal BilledQty { get; set; }
        public decimal Price { get; set; }
        public decimal PurchaseAmount { get; set; }
        public decimal MaterialAveragePrice { get; set; }
        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }
        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }

    }


    public class GetProductAnalysisViewDtoOutput
    {
        public List<ProductAnalysisViewDto> ProductAnalysis { get; set; }
        public string ColumnDefs { get; set; }

        public int totalCount { get; set; }

        public int LocationRefId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

    public class GetSupplierRateViewDtoOutput
    {
        public List<SupplierRateViewListDto> SupplierRateView { get; set; }
        public string ColumnDefs { get; set; }

        public int totalCount { get; set; }
    }

    public class GetMaterialRateViewDtoOutput
    {
        public List<MaterialRateViewListDto> MaterialRateView { get; set; }
        public string ColumnDefs { get; set; }

        public int totalCount { get; set; }
        public string FileName { get; set; }
        public int ExportType { get; set; }
    }

    [AutoMapFrom(typeof(LocationWiseMaterialRateView))]
    public class LocationWiseMaterialRateViewListDto
    {
        public int MaterialTypeId { get; set; }
        public int? LocationRefId { get; set; }

        public int MaterialRefId { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public int AveragePriceTagRefId { get; set; }

        public decimal BilledQty { get; set; }
        public int DefaultUnitId { get; set; }
        public decimal NetAmount { get; set; }
        public decimal AvgRate { get; set; }
        public bool RefreshRequired { get; set; }
        public DateTime LastUpdatedTime { get; set; }
        [MaxLength(50)] public string InvoiceDateRange { get; set; }
        public bool IsOmitFOCMaterialQuantities { get; set; }
        public bool IsCalculateAverageRateWithoutTax { get; set; }

    }

    public class GetStockSummaryDtoOutput
    {
        public GetStockSummaryDtoOutput()
        {
            StockSummary = new List<MaterialLedgerDto>();
            CompData = new List<MaterialSalesWithWipeOutCloseDayDto>();
            MenuWastageData = new List<MaterialSalesWithWipeOutCloseDayDto>();
            MenuWastageConsolidated = new List<MenuItemWastageDetailEditDto>();
        }
        public List<MaterialLedgerDto> StockSummary { get; set; }
        public string ColumnDefs { get; set; }
        public int TotalCount { get; set; }
        public string DateRange { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsNeedToBeDisplayedInSingleSheet { get; set; }

        public List<MaterialSalesWithWipeOutCloseDayDto> CompData;

        public List<MaterialSalesWithWipeOutCloseDayDto> MenuWastageData;

        public List<MenuItemWastageDetailEditDto> MenuWastageConsolidated;

        public string VarianceStatusBeforeOrAfterStatus { get; set; }
        public List<MaterialRateViewListDto> MaterialRateView { get; set; }
    }


    public class FileDownLoadDto
    {
        public FileDto ExcelFile { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class GetHouseReportForEditOutput
    {
        public HouseReportEditDto HouseReport { get; set; }
    }
    public class CreateOrUpdateHouseReportInput
    {
        [Required]
        public HouseReportEditDto HouseReport { get; set; }
    }

    public class CostingReportInputDto
    {
        public int LocationRefId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }



    public class CostingReportOutPutDto
    {
        public List<LocationListDto> Locations { get; set; }
        public decimal OpeningCostValue { get; set; }
        public decimal OverAllReceivedValue { get; set; }
        public decimal OverAllIssueValue { get; set; }
        public decimal ClosingValue { get; set; }

        public decimal SalesValueWithoutTax { get; set; }
        public decimal FoodCostValue { get; set; }
        public decimal FoodCostPercentage { get; set; }
        public decimal ActualFoodCostValue { get; set; }
        public decimal ActualFoodCostPercentage { get; set; }

        public decimal VarianceValue { get; set; }
        public decimal VariancePercentage { get; set; }


        public List<MaterialUsageWithPriceAndDateRange> MaterialWiseValueAndPercentage;

        public List<MaterialUsageWithPriceAndDateRange> ActualMaterialWiseValueAndPercentage;

        public List<MaterialStandardVsActualUsageWithPriceAndDateRange> StandardVsActual;

        public List<MaterialGroupCategoryCosting> StandardCategoryWiseCost;

        public List<MaterialGroupCategoryCosting> ActualCategoryWiseCost;

        public List<MaterialGroupCategorySalesVsIssueCosting> SalesVsIsueCategoryWiseCosting;

        public string MappingRemarks { get; set; }
        public string PricingRemarks { get; set; }
        public string Remarks { get; set; }

        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime PrintedTime { get; set; }
        public long? PreparedUserId { get; set; }
        public string PreparedUserName { get; set; }
    }


    public class MaterialGroupCategoryCosting
    {
        public int MaterialGroupCategoryRefId { get; set; }
        public string MaterialGroupCategoryRefName { get; set; }
        public decimal NetAmount { get; set; }
        public decimal CostPercentage { get; set; }
    }

    public class MaterialGroupCategorySalesVsIssueCosting
    {
        public int MaterialGroupCategoryRefId { get; set; }
        public string MaterialGroupCategoryRefName { get; set; }
        public decimal SalesAmount { get; set; }
        public decimal CostAmount { get; set; }
        public string CostPercentage { get; set; }
    }

    public class RecipeCostingDtoConsolidated
    {
        public List<RecipeCostingDto> RecipeWiseCosting;
        public string Remarks { get; set; }
    }



    public class RecipeCostingDto
    {
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public string Uom { get; set; }
        public decimal ProductionQty { get; set; }
        public decimal SaleQty { get; set; }
        public decimal WastageQty { get; set; }
        public decimal CostForProduction { get; set; }
        public decimal CostPercentage { get; set; }
        public decimal CostPerUom { get; set; }
        public decimal WastageCost { get; set; }
        public decimal SaleConvertedCost { get; set; }
        public decimal TotalSales { get; set; }

        public List<ProductionDetailViewDto> ProductionDetails;

        public List<MaterialUsageWithPriceAndDateRange> ActualMaterialWiseValueAndPercentage;

        public List<MaterialUsageWithPriceAndDateRange> StandardMaterialWiseValueAndPercentage;

        public List<MaterialStandardVsActualUsageWithPriceAndDateRange> StandardVsActual;

        public List<MaterialMenuMappingDto> RecipeSalesDetail;
    }

    public class MenuItemCostingDtoConsolidated
    {
        public List<MenuItemCostingDto> RecipeWiseCosting;
        public decimal TotalSales { get; set; }
        public decimal TotalCost { get; set; }
        public decimal TotalProfit { get; set; }
        public decimal TotalProfitMargin { get; set; }
        public string Remarks { get; set; }
        public List<MaterialRateViewListDto> MaterialRateView { get; set; }
    }

    public class MenuItemCostingDto
    {
        public int DepartmentRefId { get; set; }
        public string DepartmentRefName { get; set; }
        public int MenuItemRefId { get; set; }
        public string MenuItemRefName { get; set; }
        public decimal QtySold { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal SalesAmount { get; set; }
        public decimal CostPerUnit { get; set; }
        public decimal TotalCost { get; set; }
        public decimal Profit { get; set; }
        public decimal ProfitMargin { get; set; }

        public List<MaterialUsageWithPriceAndDateRange> StandardMaterialWiseValueAndPercentage;

    }


    public class CostOfSalesReport
    {
        public List<CostingReportOutPutDto> CostOfSales { get; set; }

        public int indexId { get; set; }
    }

    public class ProductionUnitCostingDtoConsolidated
    {
        public List<ProductionUnitCostingDto> ProductionWiseCosting;
        public string Remarks { get; set; }
    }

    public class ProductionUnitCostingDto
    {
        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }
        public int ProductionUnitRefId { get; set; }
        public string ProductionUnitRefName { get; set; }
        public decimal MaterialCost { get; set; }

        public List<MaterialIssueAndReturnWithPriceAndDateRange> MaterialUsage;

        public List<ProductionDetailViewDto> ProductionDetails;
    }

    public class MaterialIssueAndReturnWithPriceAndDateRange
    {
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public decimal IssueQty { get; set; }
        public decimal ReturnQty { get; set; }
        public decimal UsageQty { get; set; }
        public string Uom { get; set; }
        public decimal AvgRate { get; set; }
        public decimal NetAmount { get; set; }
        public decimal CostPercentage { get; set; }
        public string InvoiceDateRange { get; set; }
    }

    public class GetHouseDashboardInput : PagedAndSortedInputDto
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public bool AllLocationFlag { get; set; }
        public List<LocationListDto> Locations { get; set; }

        public bool AllSupplierFlag { get; set; }
        public List<SupplierListDto> Suppliers { get; set; }

        public int DrillDownLevel { get; set; }
        public int TopCount { get; set; }
        public int CurrentLevel { get; set; }

        public List<MaterialRateViewListDto> MaterialRateView { get; set; }
    }

    public class HouseDashBoardOutputDto
    {
        public string DateRemarks { get; set; }
        public List<SalesAndExpensesDto> SalesAndExpenses { get; set; }
    }

    public class SalesAndExpensesDto
    {

        public string DateRange { get; set; }
        public decimal Sales { get; set; }
        public decimal SalesDifferent { get; set; }
        public decimal DiffSalesPercentage { get; set; }

        public decimal Purchases { get; set; }
        public decimal PurchasesDifferent { get; set; }
        public decimal DiffPurchasesPercentage { get; set; }

        public decimal FoodCostPercentage { get; set; }

        public int CurrentLevel { get; set; }

        public List<SupplierWisePurchase> SupplierWisePurchases { get; set; }

        public List<MaterialWisePurchase> MaterialWisePurchases { get; set; }

    }

    public class SupplierWisePurchase
    {
        public int SupplierRefId { get; set; }
        public string SupplierRefName { get; set; }
        public decimal Purchase { get; set; }
    }

    public class MaterialWisePurchase
    {
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public decimal Quantity { get; set; }
        public decimal Purchase { get; set; }
        public int DefaultUnitId { get; set; }
        public decimal AvgPrice
        {
            get { return Purchase / Quantity; }
        }

    }

    public class HouseDashBoardArgumentsOutputDto
    {
        public string DateRemarks { get; set; }
        public List<SalesAndExpensesDto> SalesAndExpenses { get; set; }
    }

    public class VerifyMaterialLedgerClosingStockDto
    {
        public DateTime StartDate { get; set; }

        public int LocationRefId { get; set; }

        public List<ComboboxItemDto> MaterialsList { get; set; }
        public bool CanChangeDbValue { get; set; }
        public int? MaterialRefId { get; set; }
    }

    public class GetVerifyMaterialLedgerDto
    {
        public List<string> OutputChangesList { get; set; }
    }

    public class GetVarianceStockSummaryDtoOutput
    {
        public List<MaterialLedgerDto> StockSummary { get; set; }
        public string ColumnDefs { get; set; }
        public int totalCount { get; set; }
        public string DateRange { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsNeedToBeDisplayedInSingleSheet { get; set; }
    }

    public class MaterialTrackExportReportDto
    {
        public List<MaterialTrackWithValueReportDto> MaterialTrackWithValueReportDtos { get; set; }
        public ClosingStockExportGroupCategoryMaterialDto InputFilterDto { get; set; }
    }

    public class MaterialTrackWithValueReportDto
    {
        public int LocationRefId { get; set; }
        public string LocationRefCode { get; set; }
        public int MaterialGroupRefId { get; set; }
        public string MaterialGroupRefName { get; set; }
        public int MaterialGroupCategoryRefId { get; set; }
        public string MaterialGroupCategoryRefName { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public decimal Qty { get; set; }
        public decimal AvgPrice { get; set; }
        public decimal TotalAmount { get; set; }
        public List<MaterialTrackWithInvoiceAndInterTransferAnalysisDto> ReceivedDetails { get; set; }
    }


    public class MaterialTrackWithInvoiceAndInterTransferAnalysisDto
    {
        public DateTime TransactionTime { get; set; }
        public string Action { get; set; } // + Purchase / Recd , - Issue / Return
        public int LocationRefId { get; set; }
        public string LocationRefCode { get; set; }
        public string LocationRefName { get; set; }
        public string LinedEntityName { get; set; }
        public int MaterialGroupRefId { get; set; }
        public string MaterialGroupRefName { get; set; }
        public int MaterialGroupCategoryRefId { get; set; }
        public string MaterialGroupCategoryRefName { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialPetName { get; set; }
        public string MaterialRefName { get; set; }
        public decimal Qty { get; set; }
        public int UnitRefId { get; set; }

        public string UnitRefName { get; set; }
        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }
        public int TransferUnitId { get; set; }
        public string TransferUnitName { get; set; }
        public decimal Price { get; set; }
        public decimal TotalAmount { get; set; }
        public string Remarks { get; set; }
        public int RefernceId { get; set; }
        public string ReferenceNumber { get; set; }
    }
}