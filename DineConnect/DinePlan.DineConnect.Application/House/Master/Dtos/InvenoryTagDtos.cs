﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;

namespace DinePlan.DineConnect.House.Master.Dtos
{
	[AutoMapFrom(typeof(InventoryCycleTag))]
	public class InventoryCycleTagListDto : FullAuditedEntityDto
	{
		public virtual string InventoryCycleTagCode { get; set; }
		public virtual string InventoryCycleTagDescription { get; set; }
		public virtual int InventoryCycleMode { get; set; }
		public virtual string InventoryCycleModeRefName { get; set; }

		public virtual string InventoryModeSchedule { get; set; }

		//TODO: DTO InventoryCycleTag Properties Missing
	}
	[AutoMapTo(typeof(InventoryCycleTag))]
	public class InventoryCycleTagEditDto
	{
		public int? Id { get; set; }
		//TODO: DTO InventoryCycleTag Properties Missing
		public virtual string InventoryCycleTagCode { get; set; }
		public virtual string InventoryCycleTagDescription { get; set; }
		public virtual int InventoryCycleMode { get; set; }
		public virtual string InventoryModeSchedule { get; set; }

        public virtual bool DoesApplicableToAllLocations { get; set; }
        public List<SimpleLocationGroupDto> LocationGroups { get; set; }
        public List<LocationTag> LocationTags { get; set; }
        public List<SimpleLocationDto> Locations { get; set; }
    }

	public class GetInventoryCycleTagInput : PagedAndSortedInputDto, IShouldNormalize
	{
		public string Filter { get; set; }

		public void Normalize()
		{
			if (string.IsNullOrEmpty(Sorting))
			{
				Sorting = "Id";
			}
		}
	}
	public class GetInventoryCycleTagForEditOutput : IOutputDto
	{
		public InventoryCycleTagEditDto InventoryCycleTag { get; set; }
        
        public List<int> CycleData { get; set; }
	}
	public class CreateOrUpdateInventoryCycleTagInput : IInputDto
	{
		[Required]
		public InventoryCycleTagEditDto InventoryCycleTag { get; set; }
        
        public List<int> CycleData { get; set; }
	}

    public class InventoryCycleLinkWithLocationListDto 
    {
        public int InventoryCycleTagRefId { get; set; }
      
    }

    public enum StockCycle
	{
		Daily,
		Weekly,
		Monthly,
        End_Of_Month
		//Yearly
	};

}

