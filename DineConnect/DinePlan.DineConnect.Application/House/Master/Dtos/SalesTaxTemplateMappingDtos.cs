﻿

using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;

namespace DinePlan.DineConnect.House.Master.Dtos
{
    [AutoMapFrom(typeof(SalesTaxTemplateMapping))]
    public class SalesTaxTemplateMappingListDto : FullAuditedEntityDto
    {
        public int SalesTaxRefId { get; set; }
        public int? CompanyRefId { get; set; }
        public int? LocationRefId { get; set; }
        public int? MaterialGroupRefId { get; set; }
        public int? MaterialGroupCategoryRefId { get; set; }
        public int? MaterialRefId { get; set; }

    }
    [AutoMapTo(typeof(SalesTaxTemplateMapping))]
    public class SalesTaxTemplateMappingEditDto
    {
        public int? Id { get; set; }
        public int SalesTaxRefId { get; set; }
        public int? CompanyRefId { get; set; }
        public int? LocationRefId { get; set; }

        public int? MaterialGroupRefId { get; set; }

        public int? MaterialGroupCategoryRefId { get; set; }
      
        public int? MaterialRefId { get; set; }
     
    }

  

    public class GetSalesTaxTemplateMappingInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "SalesTaxRefId";
            }
        }
    }
    public class GetSalesTaxTemplateMappingForEditOutput : IOutputDto
    {
        public SalesTaxTemplateMappingEditDto SalesTaxTemplateMapping { get; set; }
    }
    public class CreateOrUpdateSalesTaxTemplateMappingInput : IInputDto
    {
        [Required]
        public SalesTaxTemplateMappingEditDto SalesTaxTemplateMapping { get; set; }
    }
}

