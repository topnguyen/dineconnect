﻿
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.House.Master.Dtos
{
    [AutoMapFrom(typeof(Recipe))]
    public class RecipeViewDto 
    {
        public int Id { get; set; }
        public string GroupName { get; set; }
        public string RecipeName { get; set; }
        public string RecipeShortName { get; set; }
        public decimal PrdBatchQty { get; set; }
        public string PrdUnitName { get; set; }
        public string SelUnitName { get; set; }
        public int LifeTimeInMinutes { get; set; }
        public bool AlarmFlag { get; set; }
        public int AlarmTimeInMinutes { get; set; }
        public decimal FixedCost { get; set; }
        public DateTime CreationTime { get; set; }

    }


    [AutoMapFrom(typeof(Recipe))]
    public class RecipeListDto : FullAuditedEntityDto
    {
        //TODO: DTO Recipe Properties Missing
        //YOU CAN REFER ATTRIBUTES IN 
        public string RecipeName { get; set; }
        public int RecipeGroupRefId { get; set; }
        public string RecipeShortName { get; set; }
        public decimal PrdBatchQty { get; set; }
        public int PrdUnitId { get; set; }
        public int SelUnitId { get; set; }
        public int LifeTimeInMinutes { get; set; }
        public bool AlarmFlag { get; set; }
        public int AlarmTimeInMinutes { get; set; }
        public decimal FixedCost { get; set; }

    }

    public class RecipeCostingReportDto
    {
        public string RecipeName { get; set; }
        public decimal PrdBatchQty { get; set; }
        public int PrdUnitId { get; set; }
        public string PrdUnitName { get; set; }
        public decimal BatchCost { get; set; }
        public decimal PerUnitCost { get; set; }
        public decimal FixedCost { get; set; }
    }

    [AutoMapTo(typeof(Recipe))]
    public class RecipeEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO Recipe Properties Missing
        public string RecipeName { get; set; }
        public int RecipeGroupRefId { get; set; }
        public string RecipeShortName { get; set; }
        public decimal PrdBatchQty { get; set; }
        public int PrdUnitId { get; set; }
        public int SelUnitId { get; set; }
        public int LifeTimeInMinutes { get; set; }
        public bool AlarmFlag { get; set; }
        public int AlarmTimeInMinutes { get; set; }
        public decimal FixedCost { get; set; }

    }

    public class GetRecipeInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetRecipeForEditOutput : IOutputDto
    {
        public RecipeEditDto Recipe { get; set; }
        public List<RecipeIngredientViewDto> RecipeIngredientList { get; set; }
    }
    public class CreateOrUpdateRecipeInput : IInputDto
    {
        [Required]
        public RecipeEditDto Recipe { get; set; }
        
        public List<RecipeIngredientViewDto> RecipeIngredientList { get; set; }
    }
}

