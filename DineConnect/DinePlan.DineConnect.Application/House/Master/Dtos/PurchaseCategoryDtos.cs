﻿
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;

namespace DinePlan.DineConnect.House.Master.Dtos
{
	[AutoMapFrom(typeof(PurchaseCategory))]
	public class PurchaseCategoryListDto : FullAuditedEntityDto
	{
		public virtual string PurchaseCategoryName { get; set; }
		//TODO: DTO PurchaseCategory Properties Missing
		//YOU CAN REFER ATTRIBUTES IN 
	}
	[AutoMapTo(typeof(PurchaseCategory))]
	public class PurchaseCategoryEditDto
	{
		public int? Id { get; set; }
		public virtual string PurchaseCategoryName { get; set; }
		//TODO: DTO PurchaseCategory Properties Missing
	}

	public class GetPurchaseCategoryInput : PagedAndSortedInputDto, IShouldNormalize
	{
		public string Filter { get; set; }

		public string Operation { get; set; }

		public void Normalize()
		{
			if (string.IsNullOrEmpty(Sorting))
			{
				Sorting = "PurchaseCategoryName";
			}
		}
	}
	public class GetPurchaseCategoryForEditOutput : IOutputDto
	{
		public PurchaseCategoryEditDto PurchaseCategory { get; set; }
	}
	public class CreateOrUpdatePurchaseCategoryInput : IInputDto
	{
		[Required]
		public PurchaseCategoryEditDto PurchaseCategory { get; set; }
	}
}

