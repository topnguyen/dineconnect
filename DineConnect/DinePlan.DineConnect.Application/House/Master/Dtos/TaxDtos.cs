﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.House.Master.Dtos
{
    [AutoMapFrom(typeof(Tax))]
    public class TaxListDto : FullAuditedEntityDto
    {
        public string TaxName { get; set; }
        public decimal Percentage { get; set; }
        public string TaxCalculationMethod { get; set; }
        public int SortOrder { get; set; }

        public int Rounding { get; set; }
		public int? SyncId { get; set; }
		public DateTime? SyncLastModification { get; set; }

	}

	[AutoMapTo(typeof(Tax))]
    public class TaxEditDto
    {
        public int? Id { get; set; }
        public string TaxName { get; set; }
        public decimal Percentage { get; set; }
        public string TaxCalculationMethod { get; set; }
        public int SortOrder { get; set; }
        public int Rounding { get; set; }

		public int? SyncId { get; set; }
		public DateTime? SyncLastModification { get; set; }

	}

	[AutoMapTo(typeof(Tax))]
    public class TaxForMaterial
    {
        public int MaterialRefId { get; set; }
        public int TaxRefId { get; set; }
        public string TaxName { get; set; }
        public decimal TaxRate { get; set; }
        public decimal TaxValue { get; set; }
        public int SortOrder { get; set; }
        public int Rounding { get; set; }
        public string TaxCalculationMethod { get; set; }

    }


    [AutoMapTo(typeof(Tax))]
    public class ApplicableTaxesForMaterial
    {
        public int TaxRefId { get; set; }
        public string TaxName { get; set; }
        public decimal TaxRate { get; set; }
        public int SortOrder { get; set; }
        public int Rounding { get; set; }
        public string TaxCalculationMethod { get; set; }
    }


    public class GetTaxInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetTaxForEditOutput : IOutputDto
    {
        public TaxEditDto Tax { get; set; }
        public List<TaxTemplateMappingEditDto> TaxTemplateMapping { get; set; }
    }
    public class CreateOrUpdateTaxInput : IInputDto
    {
        [Required]
        public TaxEditDto Tax { get; set; }

        [Required]
        public List<TaxTemplateMapping> TaxTemplateMapping { get; set; }
    }
}
