﻿
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.House.Master.Dtos
{
    [AutoMapFrom(typeof(CustomerTagMaterialPrice))]
    public class CustomerTagMaterialPriceListDto : FullAuditedEntityDto
    {
        [Required]
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public string TagCode { get; set; }             //  Short Description
        public decimal MaterialPrice { get; set; }
    }

    public class TagWithPrice
    {
        public string TagCode { get; set; }

        public decimal TagPrice { get; set; }
    }

    [AutoMapFrom(typeof(CustomerTagMaterialPrice))]
    public class CustomerTagMaterialPriceViewEditDto 
    {
        [Required]
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }

        public List<string>  TagList { get; set; }
        public List<TagWithPrice> TagWithPrice { get; set; }

        public string[] HeaderData { get; set; }

        public string[,] DetailData { get; set; }
    }

    [AutoMapFrom(typeof(CustomerTagMaterialPrice))]
    public class CustomerTagMaterialPriceArrayDto 
    {

        public string[] HeaderData { get; set; }

        public string[,] DetailData { get; set; }
    }


    [AutoMapTo(typeof(CustomerTagMaterialPrice))]
    public class CustomerTagMaterialPriceEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO CustomerTagMaterialPrice Properties Missing
        [Required]
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public string TagCode { get; set; }             //  Short Description

        public decimal MaterialPrice { get; set; }
        public bool AvgRateBasedPrice { get; set; }
        public decimal MarginPercentage { get; set; }
        public decimal MinimumPrice { get; set; }
    }

    public class CreateOrUpdateCustomerTagPriceDefinitionInput
    {
        public CustomerTagMaterialPriceEditDto CustomerTagMaterialPriceEditDto { get; set; }
    }

    public class GetCustomerTagMaterialPriceInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public string SelectedPriceTagForEdit { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }

    public class GetTagMaterialDynamic : IOutputDto
    {
        public List<dynamic> DynamicTagList { get; set; }

        public List<dynamic> ColumnDefList { get; set; }

        public string TagToBeUpdate { get; set; }

    }

    public class GetCustomerTagMaterialPriceForEditOutput : IOutputDto
    {
        public CustomerTagMaterialPriceEditDto CustomerTagMaterialPrice { get; set; }
    }
    public class CreateOrUpdateCustomerTagMaterialPriceInput : IInputDto
    {
        [Required]
        public CustomerTagMaterialPriceEditDto CustomerTagMaterialPrice { get; set; }
    }
}

