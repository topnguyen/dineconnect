﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;

namespace DinePlan.DineConnect.House.Master.Dtos
{
    [AutoMapFrom(typeof(Supplier))]
    public class SimpleSupplierListDto
    {
        public virtual int Id { get; set; }
        public virtual string SupplierName { get; set; }
        public virtual string Name => SupplierName;
        public virtual bool TaxApplicable { get; set; }
        public int DefaultPayModeEnumRefId { get; set; }
    }

    [AutoMapFrom(typeof(Supplier))]
    public class SupplierListDto : FullAuditedEntityDto
    {
        public virtual string SupplierName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public string PhoneNumber1 { get; set; }
        public int DefaultCreditDays { get; set; }
        public string OrderPlacedThrough { get; set; }
        public string Email { get; set; }
        public string FaxNumber { get; set; }
        public string Website { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual string SupplierCode { get; set; }
        public virtual string TaxRegistrationNumber { get; set; }
        public virtual bool TaxApplicable { get; set; }
        public virtual bool LoadOnlyLinkedMaterials { get; set; }
        public int DefaultPayModeEnumRefId { get; set; }
        public string DefaultPayModeEnumRefName { get; set; }
        public int? SyncId { get; set; }
        public DateTime? SyncLastModification { get; set; }
        
    }
    [AutoMapTo(typeof(Supplier))]
    public class SupplierEditDto
    {
        public int? Id { get; set; }
        public virtual string SupplierName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public string PhoneNumber1 { get; set; }
        public int DefaultCreditDays { get; set; }
        public string OrderPlacedThrough { get; set; }
        public string Email { get; set; }
        public string FaxNumber { get; set; }
        public string Website { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual string SupplierCode { get; set; }
        public virtual string TaxRegistrationNumber { get; set; }
        public virtual bool TaxApplicable { get; set; }
        public virtual bool LoadOnlyLinkedMaterials { get; set; }
        public int DefaultPayModeEnumRefId { get; set; }
        public string AddOn { get; set; }
        public int? SyncId { get; set; }
        public DateTime? SyncLastModification { get; set; }

    }

    [AutoMapTo(typeof(SupplierGSTInformation))]
    public class SupplierGSTInformationEditDto
    {
        public int? Id { get; set; }
        public int SupplierRefId { get; set; }
        public string SupplierLegalName { get; set; }
        public int? GstStatus { get; set; }
        public int? ConstitutionStatus { get; set; }
        public string Gstin { get; set; }
        public Guid? GstCertificate { get; set; }
        public bool ReverseChargeApplicable { get; set; }
        public bool PayGstApplicable { get; set; }
    }

    public class GetSupplierInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetSupplierForEditOutput : IOutputDto
    {
        public SupplierEditDto Supplier { get; set; }
        public SupplierGSTInformationEditDto Suppliergstinformation { get; set; }
    }
    public class CreateOrUpdateSupplierInput : IInputDto
    {

        public SupplierEditDto Supplier { get; set; }
        public SupplierGSTInformationEditDto Suppliergstinformation { get; set; }
    }

    public enum SupplierDocumentType
    {
        PO,
        DC,
        INVOICE
    };

    public enum Supplier_Gst_Status
    {
        REGISTERED,
        UNREGISTERED,
        REGISTERED_UNDER,
        COMPOUNDING_SCHEME,
        CASUAL_DEALER,
        NON_RESIDENT_DEALER
    };

    public enum Supplier_Constitution
    {
        INDIVIDUAL,
        PARTNERSHIP_FIRM,
        HUF,
        COMPANY,
        OTHER,
    };

    public class FileUploadDto : IInputDto
    {
        public int Id { get; set; }
        public int gstIn { get; set; }
        public Guid? GstCertificate { get; set; }
    }
}

