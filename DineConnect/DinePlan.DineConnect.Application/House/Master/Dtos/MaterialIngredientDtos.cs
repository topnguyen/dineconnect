﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;

namespace DinePlan.DineConnect.House.Master.Dtos
{
    [AutoMapFrom(typeof(MaterialIngredient))]
    public class MaterialIngredientViewDto
    {

        public int Id { get; set; }

        public int RecipeRefId { get; set; }

        public string RecipeRefName { get; set; }

        public int MaterialRefId { get; set; }

        public string MaterialRefName { get; set; }

        public int MaterialTypeId { get; set; }
        public string MaterialTypeName { get; set; }

        public decimal MaterialUsedQty { get; set; }

        public int UnitRefId { get; set; }

        public string UnitRefName { get; set; }

        public bool VariationflagForProduction { get; set; }

        public decimal UserSerialNumber { get; set; }
        public DateTime CreationTime { get; set; }

        public bool Ingreditflag { get; set; }

    }

    [AutoMapFrom(typeof(MaterialIngredient))]
    public class MaterialIngredientListDto : FullAuditedEntityDto
    {

        public int RecipeRefId { get; set; }

        public int MaterialRefId { get; set; }

        public decimal MaterialUsedQty { get; set; }

        public int UnitRefId { get; set; }


        public bool VariationflagForProduction { get; set; }

        public decimal UserSerialNumber { get; set; }
    }

    [AutoMapTo(typeof(MaterialIngredient))]
    public class MaterialIngredientEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO MaterialIngredient Properties Missing
        public int RecipeRefId { get; set; }
        public string RecipeRefName { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public decimal MaterialUsedQty { get; set; }
        public int UnitRefId { get; set; }
        public bool VariationflagForProduction { get; set; }
        public decimal UserSerialNumber { get; set; }
    }

    public class GetMaterialIngredientInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
    public class GetMaterialIngredientForEditOutput : IOutputDto
    {
        public MaterialIngredientEditDto MaterialIngredient { get; set; }
    }
    public class CreateOrUpdateMaterialIngredientInput : IInputDto
    {
        [Required]
        public MaterialIngredientEditDto MaterialIngredient { get; set; }
    }

    [AutoMapFrom(typeof(MaterialIngredient))]
    public class MaterialIngredientRequiredDto : FullAuditedEntityDto
    {
        public int RecipeRefId { get; set; }
        public int MaterialTypeId { get; set; }
        public string MaterialTypeRefName { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public decimal MaterialUsedQty { get; set; }
        public decimal AvgPrice { get; set; }
        public string InvoiceDateRange { get; set; }
        public int UnitRefId { get; set; }
        public string Uom { get; set; }
        public int DefaultUnitId { get; set; }
        public bool VariationflagForProduction { get; set; }
        public decimal UserSerialNumber { get; set; }
        public int AveragePriceTagRefId { get; set; }
        public string AveragePriceTagRefName { get; set; }
        public int NoOfMonthAvgTaken { get; set; }
        public decimal TotalPrice { get; set; }

    }

    public class MaterialWithQty
    {
        public string DepartmentName { get; set; }
        public int PosMenuPortionRefId { get; set; }

        public string PosRefName { get; set; }

        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public decimal UsedQty { get; set; }
        public int DefaultUnitId { get; set; }
        public int UnitRefId { get; set; }
    }

}

