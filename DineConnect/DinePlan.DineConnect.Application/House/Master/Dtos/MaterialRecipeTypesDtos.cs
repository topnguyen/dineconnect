﻿

using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;

namespace DinePlan.DineConnect.House.Master.Dtos
{
    [AutoMapFrom(typeof(MaterialRecipeTypes))]
    public class MaterialRecipeTypesListDto : FullAuditedEntityDto
    {
        public int MaterailRefId { get; set; }
        public string MaterialRefName { get; set; }

        public decimal PrdBatchQty { get; set; }

        public int LifeTimeInMinutes { get; set; }

        public bool AlarmFlag { get; set; }

        public int AlarmTimeInMinutes { get; set; }

        public decimal FixedCost { get; set; }
    }
    [AutoMapTo(typeof(MaterialRecipeTypes))]
    public class MaterialRecipeTypesEditDto
    {
        public int? Id { get; set; }

        public int MaterailRefId { get; set; }

        public string MaterialRefName { get; set; }

        public decimal PrdBatchQty { get; set; }

        public int LifeTimeInMinutes { get; set; }

        public bool AlarmFlag { get; set; }

        public int AlarmTimeInMinutes { get; set; }

        public decimal FixedCost { get; set; }
    }

    public class GetMaterialRecipeTypesInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
    public class GetMaterialRecipeTypesForEditOutput : IOutputDto
    {
        public MaterialRecipeTypesEditDto MaterialRecipeTypes { get; set; }
    }
    public class CreateOrUpdateMaterialRecipeTypesInput : IInputDto
    {
        [Required]
        public MaterialRecipeTypesEditDto MaterialRecipeTypes { get; set; }
    }
}

