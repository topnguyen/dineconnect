﻿
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;

namespace DinePlan.DineConnect.House.Master.Dtos
{

    [AutoMapFrom(typeof(LocationContact))]
    public class LocationContactViewDto
    {
        public int Id { get; set; }

        public int LocationRefId { get; set; }

        public string LocationName { get; set; }

        public string   ContactPerson{ get; set; }

        public int Priority { get; set; }

        public string PhoneNumber { get; set; }

        public string EmpRefCode { get; set; }

        public string Email { get; set; }

        public string Designation { get; set; }
    }


    [AutoMapFrom(typeof(LocationContact))]
    public class LocationContactListDto : FullAuditedEntityDto
    {
        //TODO: DTO LocationContact Properties Missing
        //YOU CAN REFER ATTRIBUTES IN 

        public int LocationRefId { get; set; }

        public string ContactPersonName { get; set; }

        public string Designation { get; set; }

        public string PhoneNumber { get; set; }

        public string EmpRefCode { get; set; }

        public string Email { get; set; }

        public int Priority { get; set; }

    }
    [AutoMapTo(typeof(LocationContact))]
    public class LocationContactEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO LocationContact Properties Missing

        public int LocationRefId { get; set; }

        public string ContactPersonName { get; set; }

        public string Designation { get; set; }

        public string PhoneNumber { get; set; }

        public string EmpRefCode { get; set; }

        public string Email { get; set; }


        public int Priority { get; set; }


    }

    public class GetLocationContactInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
    public class GetLocationContactForEditOutput : IOutputDto
    {
        public LocationContactEditDto LocationContact { get; set; }
    }
    public class CreateOrUpdateLocationContactInput : IInputDto
    {
        [Required]
        public LocationContactEditDto LocationContact { get; set; }
    }
}

