﻿

using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using DinePlan.DineConnect.Connect.Master.Dtos;
using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Location;

namespace DinePlan.DineConnect.House.Master.Dtos
{
    [AutoMapFrom(typeof(MaterialLocationWiseStock))]
    public class MaterialLocationWiseStockListDto : FullAuditedEntityDto
    {
        public int LocationRefId { get; set; }
        public string LocationRefCode { get; set; }
        public string LocationRefName { get; set; }

        public int MaterialRefId { get; set; }

        public decimal CurrentInHand { get; set; }

        public decimal MinimumStock { get; set; }

        public decimal MaximumStock { get; set; }

        public decimal ReOrderLevel { get; set; }

        public bool IsOrderPlaced { get; set; }
        public bool IsActiveInLocation { get; set; }
        public bool ConvertAsZeroStockWhenClosingStockNegative { get; set; }
    }

    [AutoMapTo(typeof(MaterialLocationWiseStock))]
    public class MaterialLocationWiseStockEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO MaterialStockIndividualStore Properties Missing
        public int LocationRefId { get; set; }

        public int MaterialRefId { get; set; }

        public int MaterialRefName { get; set; }

        public string Uom { get; set; }

        public decimal CurrentInHand { get; set; }

        public decimal MinimumStock { get; set; }

        public decimal MaximumStock { get; set; }

        public decimal ReOrderLevel { get; set; }

        public bool IsOrderPlaced { get; set; }
        public bool IsActiveInLocation { get; set; }
        public DateTime? AccountDate { get; set; }
        public bool ConvertAsZeroStockWhenClosingStockNegative { get; set; }

    }

    [AutoMapTo(typeof(MaterialLocationWiseStock))]
    public class MaterialLocationWiseStockViewDto
    {
        public int? Id { get; set; }
        //TODO: DTO MaterialStockIndividualStore Properties Missing
        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }
        public int MaterialTypeRefId { get; set; }
        public string MaterialTypeRefName { get; set; }

        public int MaterialRefId { get; set; }
        public string Barcode { get; set; }
        public string MaterialRefName { get; set; }

		public int DefaultUnitRefId { get; set; }
        public string Uom { get; set; }

        public decimal CurrentInHand { get; set; }

        public decimal MinimumStock { get; set; }

        public decimal MaximumStock { get; set; }

        public decimal ReOrderLevel { get; set; }

        public bool IsOrderPlaced { get; set; }
        public bool IsActiveInLocation { get; set; }

        public decimal Sales { get; set; }

        public decimal LiveStock { get; set; }

		public decimal LiveStock_TransactionUnit { get; set; }
		public int TransactionUnitRefId { get; set; }
		public string TransactionUnitName { get; set; }


        public decimal LiveStock_TransferUnit { get; set; }
        public int TransferUnitRefId { get; set; }
        public string TransferUnitRefName { get; set; }
        public bool ConvertAsZeroStockWhenClosingStockNegative { get; set; }
    }

    public class GetMaterialLocationWiseStockInput : PagedAndSortedInputDto, IShouldNormalize
    {

        public GetMaterialLocationWiseStockInput()
        {
            MaterialRefIds = new List<int>();
        }

        public string Operation { get; set; }
        public int LocationRefId { get; set; }
        public List<LocationListDto> Locations { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public long UserId { get; set; }
        public bool LiveStock { get; set; }
        public bool IsHighValueItemOnly { get; set; }
        public bool LessThanReorderLevel { get; set; }
        public bool LessThanMinimumStock { get; set; }
        public int? MaterialTypeRefId { get; set; }
        public string Material { get; set; }
        public List<int> MaterialRefIds { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }

    public class GetMaterialLocationWiseStockForEditOutput : IOutputDto
    {
        public MaterialLocationWiseStockEditDto MaterialLocationWiseStock { get; set; }
    }
    public class CreateOrUpdateMaterialLocationWiseStockInput : IInputDto
    {
        [Required]
        public MaterialLocationWiseStockEditDto MaterialLocationWiseStock { get; set; }
    }


    [AutoMapTo(typeof(MaterialLocationWiseStockViewDto))]
    public class MaterialAllLocationWiseStockViewDto
    {
        public int MaterialTypeRefId { get; set; }
        public string MaterialTypeRefName { get; set; }
        public int MaterialRefId { get; set; }
        public string Barcode { get; set; }
        public string MaterialRefName { get; set; }
        public int DefaultUnitRefId { get; set; }
        public string Uom { get; set; }
        public decimal CurrentInHand { get; set; }
        public decimal MinimumStock { get; set; }
        public decimal MaximumStock { get; set; }
        public decimal ReOrderLevel { get; set; }
        public bool IsOrderPlaced { get; set; }
        public bool IsActiveInLocation { get; set; }
        public decimal Sales { get; set; }
        public decimal LiveStock { get; set; }
        public decimal LiveStock_TransactionUnit { get; set; }
        public int TransactionUnitRefId { get; set; }
        public string TransactionUnitName { get; set; }
        public List<MaterialLocationWiseStockViewDto> LocationWiseStock { get; set; }

    }

}

