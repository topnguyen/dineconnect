﻿

using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.House.Master.Dtos
{
    [AutoMapFrom(typeof(CustomerMaterial))]
    public class CustomerMaterialListDto : FullAuditedEntityDto
    {
        //TODO: DTO CustomerMaterial Properties Missing
        //YOU CAN REFER ATTRIBUTES IN 
        public int CustomerRefId { get; set; }
        public string CustomerRefName { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public decimal MaterialPrice { get; set; }

        public string LastQuoteRefNo { get; set; }

        public DateTime LastQuoteDate { get; set; }
        public List<TaxForMaterial> TaxForMaterial { get; set; }
        public List<ApplicableTaxesForMaterial> ApplicableTaxes { get; set; }
    }
    [AutoMapTo(typeof(CustomerMaterial))]
    public class CustomerMaterialEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO CustomerMaterial Properties Missing
        public int CustomerRefId { get; set; }
        public int MaterialRefId { get; set; }
        public decimal MaterialPrice { get; set; }

        public string LastQuoteRefNo { get; set; }

        public DateTime LastQuoteDate { get; set; }
    }

    [AutoMapFrom(typeof(CustomerMaterial))]
    public class CustomerMaterialViewDto
    {
        public int? Id { get; set; }

        public string CustomerRefName { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public int MaterialGroupRefId { get; set; }
        public int MaterialGroupCategoryRefId { get; set; }
        public decimal MaterialPrice { get; set; }
        public decimal MinimumOrderQuantity { get; set; }
        public decimal MaximumOrderQuantity { get; set; }
        public bool IsQuoteNeededForSales { get; set; }

        public bool IsFractional { get; set; }

        public bool IsBranded { get; set; }
        public string LastQuoteRefNo { get; set; }
        public DateTime LastQuoteDate { get; set; }
        public decimal CurrentInHand { get; set; }
        public decimal AlreadyOrderedQuantity { get; set; }
        public int DefaultUnitId { get; set; }
        public string Uom { get; set; }
        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }
        public bool AvgRateBasedPrice { get; set; }
        public decimal MarginPercentage { get; set; }
        public decimal MinimumPrice { get; set; }
        public List<TaxForMaterial> TaxForMaterial { get; set; }
        public List<ApplicableTaxesForMaterial> ApplicableTaxes { get; set; }

    }

    public class GetCustomerMaterialInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetCustomerMaterialForEditOutput : IOutputDto
    {
        public CustomerMaterialEditDto CustomerMaterial { get; set; }
    }
    public class CreateOrUpdateCustomerMaterialInput : IInputDto
    {
        [Required]
        public CustomerMaterialEditDto CustomerMaterial { get; set; }
    }
}

