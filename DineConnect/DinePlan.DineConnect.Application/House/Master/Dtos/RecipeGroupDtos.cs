﻿
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
namespace DinePlan.DineConnect.House.Master.Dtos
{
    [AutoMapFrom(typeof(RecipeGroup))]
    public class RecipeGroupListDto : FullAuditedEntityDto
    {
        //TODO: DTO RecipeGroup Properties Missing
        //YOU CAN REFER ATTRIBUTES IN 
      
        public string RecipeGroupName { get; set; }

        public string RecipeGroupShortName { get; set; }


    }
    [AutoMapTo(typeof(RecipeGroup))]
    public class RecipeGroupEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO RecipeGroup Properties Missing
      
        public string RecipeGroupName { get; set; }

        public string RecipeGroupShortName { get; set; }


    }

    public class GetRecipeGroupInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetRecipeGroupForEditOutput : IOutputDto
    {
        public RecipeGroupEditDto RecipeGroup { get; set; }
    }
    public class CreateOrUpdateRecipeGroupInput : IInputDto
    {
        [Required]
        public RecipeGroupEditDto RecipeGroup { get; set; }
    }
}

