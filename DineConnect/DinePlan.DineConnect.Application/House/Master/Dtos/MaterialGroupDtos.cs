﻿

using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;

namespace DinePlan.DineConnect.House.Master.Dtos
{
    [AutoMapFrom(typeof(MaterialGroup))]
    public class MaterialGroupListDto : FullAuditedEntityDto
    {
        public string MaterialGroupName { get; set; }
		public int? SyncId { get; set; }
		public DateTime? SyncLastModification { get; set; }
	}

	[AutoMapTo(typeof(MaterialGroup))]
    public class MaterialGroupEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO MaterialGroup Properties Missing
        public string MaterialGroupName { get; set; }

		public int? SyncId { get; set; }
		public DateTime? SyncLastModification { get; set; }

	}

	public class GetMaterialGroupInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetMaterialGroupForEditOutput : IOutputDto
    {
        public MaterialGroupEditDto MaterialGroup { get; set; }
    }
    public class CreateOrUpdateMaterialGroupInput : IInputDto
    {
        [Required]
        public MaterialGroupEditDto MaterialGroup { get; set; }
    }
}

