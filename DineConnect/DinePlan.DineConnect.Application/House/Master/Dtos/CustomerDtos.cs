﻿


using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;

namespace DinePlan.DineConnect.House.Master.Dtos
{
    [AutoMapFrom(typeof(Customer))]
    public class CustomerListDto : FullAuditedEntityDto
    {
        //TODO: DTO Customer Properties Missing
        //YOU CAN REFER ATTRIBUTES IN 
        public virtual string CustomerName { get; set; }

        public int CustomerTagRefId { get; set; }
		public string CustomerTagRefName { get; set; }


		public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public virtual string City { get; set; }

        public virtual string State { get; set; }

        public virtual string Country { get; set; }

        public string ZipCode { get; set; }

        public string PhoneNumber { get; set; }

        public int DefaultCreditDays { get; set; }

        public string OrderPlacedThrough { get; set; }

        public string Email { get; set; }

        public string FaxNumber { get; set; }

        public string Website { get; set; }

    }
    [AutoMapTo(typeof(Customer))]
    public class CustomerEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO Customer Properties Missing
        public virtual string CustomerName { get; set; }

        public int CustomerTagRefId { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public virtual string City { get; set; }

        public virtual string State { get; set; }

        public virtual string Country { get; set; }

        public string ZipCode { get; set; }

        public string PhoneNumber { get; set; }

        public int DefaultCreditDays { get; set; }

        public string OrderPlacedThrough { get; set; }

        public string Email { get; set; }

        public string FaxNumber { get; set; }

        public string Website { get; set; }

    }

    public class GetCustomerInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetCustomerForEditOutput : IOutputDto
    {
        public CustomerEditDto Customer { get; set; }
    }
    public class CreateOrUpdateCustomerInput : IInputDto
    {
        [Required]
        public CustomerEditDto Customer { get; set; }
    }
}

