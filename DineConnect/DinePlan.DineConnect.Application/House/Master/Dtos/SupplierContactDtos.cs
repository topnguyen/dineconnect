﻿
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;

namespace DinePlan.DineConnect.House.Master.Dtos
{
    [AutoMapFrom(typeof(SupplierContact))]
    public class SupplierContactListDto : FullAuditedEntityDto
    {
        //TODO: DTO SupplierContact Properties Missing
        //YOU CAN REFER ATTRIBUTES IN 
        public int SupplierRefId { get; set; }
        public string ContactPersonName { get; set; }

        public string Designation { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }
    }
    [AutoMapTo(typeof(SupplierContact))]
    public class SupplierContactEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO SupplierContact Properties Missing
        public int SupplierRefId { get; set; }

        public string ContactPersonName { get; set; }

        public string Designation { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }


    }
    [AutoMapTo(typeof(SupplierContact))]
    public class SupplierContactViewDto
    {
        public int? Id { get; set; }
        //TODO: DTO SupplierContact Properties Missing

        public int SupplierRefId { get; set; }
        public string SupplierRefName { get; set; }
        public string ContactPersonName { get; set; }
        public string Designation { get; set; }
        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public DateTime CreationTime { get; set; }
    }

    public class GetSupplierContactInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetSupplierContactForEditOutput : IOutputDto
    {
        public SupplierContactEditDto SupplierContact { get; set; }
    }
    public class CreateOrUpdateSupplierContactInput : IInputDto
    {
        [Required]
        public SupplierContactEditDto SupplierContact { get; set; }
    }
}

