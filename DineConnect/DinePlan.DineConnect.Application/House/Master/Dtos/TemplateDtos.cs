﻿

using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;

namespace DinePlan.DineConnect.House.Master.Dtos
{
    [AutoMapFrom(typeof(Template))]
    public class TemplateInfoDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
        public int TemplateType { get; set; }
        public int? LocationRefId { get; set; }
    }

    [AutoMapFrom(typeof(Template))]
    public class TemplateListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
        public int TemplateType { get; set; }
        public string TemplateTypeRefName { get; set; }
        public string TemplateData { get; set; }

        public int? LocationRefId { get; set; }
        public string LocationRefName { get; set; }
    }

    [AutoMapTo(typeof(Template))]
    public class TemplateEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO Template Properties Missing
        public string Name { get; set; }
        public int TemplateType { get; set; }
        public string TemplateData { get; set; }

        public int? LocationRefId { get; set; }
    }

    [AutoMapTo(typeof(DayCloseTemplate))]
    public class DayCloseTemplateEditDto
    {
        public int? Id { get; set; }
        [Required]
        public int LocationRefId { get; set; }
        [Required]
        public DateTime ClosedDate { get; set; }
        [Required]
        [MaxLength(100)]
        public string TableName { get; set; }
        [Required]
        public int TemplateType { get; set; }
        [Required]
        public string TemplateData { get; set; }
    }

    public class DayCloseDtoView : PagedAndSortedInputDto, IShouldNormalize
    {
        public int LocationRefId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }

    [AutoMapTo(typeof(DayCloseTemplate))]
    public class DayCloseTemplateListDto : FullAuditedEntityDto
    {
        [Required]
        public int LocationRefId { get; set; }

        public string LocationRefName { get; set; }

        [Required]
        public DateTime ClosedDate { get; set; }

        [Required]
        [MaxLength(100)]
        public string TableName { get; set; }

        [Required]
        public int TemplateType { get; set; }

        [Required]
        public string TemplateData { get; set; }

    }


    [AutoMapTo(typeof(Template))]
    public class TemplateSaveDto
    {
        public bool Templateflag { get; set; }
        public string Templatename { get; set; }
        public bool Templatescope { get; set; }

        public int TemplateType { get; set; }

    }



    public class GetTemplateInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public int? TemplateType { get; set; }
        public string TemplateData { get; set; }

        public int? LocationRefId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }

    }

    public class GetTemplateForEditOutput : IOutputDto
    {
        public TemplateEditDto Template { get; set; }
    }
    public class CreateOrUpdateTemplateInput : IInputDto
    {
        [Required]
        public TemplateEditDto Template { get; set; }
    }

    public class CreateOrUpdateDayCloseTemplateInput : IInputDto
    {
        [Required]
        public DayCloseTemplateEditDto DayCloseTemplate { get; set; }
    }

    public class GetTemplateList : IInputDto
    {
        public int TemplateType { get; set; }

        public int? LocationRefId { get; set; }
    }

    public enum TemplateType
    {
        SupplierPurchaseOrder,
        RequestInterLocation,
        Yield,
        ProductionEntry,
        InterTransferApprovalAndReceived,
        ClosingStockEntry,
        CustomerSalesOrder,
        Invoice,
        SalesDeliveryOrder,
        DirectInterTransfer,
        BulkPurchaseOrder = 10
    };

    public enum DayCloseTemplateType
    {
        MaterialMenuMappings,
        UnitConversions,
        MenuMappingDepartment,
        DashboardTicketDto
    };

}

