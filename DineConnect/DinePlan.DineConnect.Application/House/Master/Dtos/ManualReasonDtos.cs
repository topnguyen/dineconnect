﻿
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
namespace DinePlan.DineConnect.House.Master.Dtos
{
    [AutoMapFrom(typeof(ManualReason))]
    public class ManualReasonListDto : FullAuditedEntityDto
    {
        public virtual int ManualReasonCategoryRefId { get; set; }
        public virtual string ManualReasonCategoryRefName { get; set; }
        public virtual string ManualReasonName { get; set; }
        public virtual bool IsActive { get; set; }
    }
    [AutoMapTo(typeof(ManualReason))]
    public class ManualReasonEditDto
    {
        public int? Id { get; set; }
        public virtual int ManualReasonCategoryRefId { get; set; }
        public virtual string ManualReasonCategoryRefName { get; set; }
        public virtual string ManualReasonName { get; set; }
        public virtual bool IsActive { get; set; }
    }

    public class GetManualReasonInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public virtual int ManualReasonCategoryRefId { get; set; }
        public virtual string ManualReasonCategoryRefName { get; set; }
        public bool GeneralIncluded { get; set; }
        
        public string Filter { get; set; }

        public string Operation { get; set; }

        public bool FullOptions { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetManualReasonForEditOutput : IOutputDto
    {
        public ManualReasonEditDto ManualReason { get; set; }
    }
    public class CreateOrUpdateManualReasonInput : IInputDto
    {
        [Required]
        public ManualReasonEditDto ManualReason { get; set; }
    }

    public class FilterManualReasonDto
    {
        public virtual int ManualReasonCategoryRefId { get; set; }
        public bool IncludeGeneralCategoryAlso { get; set; }
    }
}

