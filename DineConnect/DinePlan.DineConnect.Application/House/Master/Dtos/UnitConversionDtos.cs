﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.House.Master.Dtos
{
    [AutoMapFrom(typeof(UnitConversion))]
    public class UnitConversionListDto : FullAuditedEntityDto
    {
        public int? MaterialRefId { get; set; }
        public int BaseUnitId { get; set; }
        public string BaseUom { get; set; }
        public int RefUnitId { get; set; }
        public string RefUom { get; set; }
        public decimal Conversion { get; set; }
        public byte DecimalPlaceRounding { get; set; }
        public int? SyncId { get; set; }
        public DateTime? SyncLastModification { get; set; }
        public bool CreatedBasedOnRecursive { get; set; }
        public int? ReferenceRecursiveId { get; set; }
        public List<int> LinkedSupplierIds { get; set; }
        public List<SimpleSupplierListDto> LinkedSupplierList { get; set; }
        public bool DoesThisGlobalConversion { get; set; }
        public bool DoesThisMaterialBasedConversion { get; set; }
        public bool DoesThisSupplierBasedConversion { get; set; }
        public UnitConversionListDto()
        {
            LinkedSupplierIds = new List<int>();
            LinkedSupplierList = new List<SimpleSupplierListDto>();
        }
    }

    public class DefaultUomErrorMessageDto
    {
        public SimpleMaterialListDto Material { get; set; }
        public List<UnitConversionEditDto> UnitConversionEditDtos { get; set; }
        public string ErrorMessage { get; set; }
        public bool ErrorFlag => ErrorMessage==null || ErrorMessage.Length == 0 ? false : true;
        public string UnitConversionErrorMessage { get; set; }
        public DefaultUomErrorMessageDto()
        {
            UnitConversionEditDtos = new List<UnitConversionEditDto>();
        }
        public int? DefaultUnitMightBeRefId { get; set; }
        public string DefaultUnitMightBeRefName { get; set; }
    }


	[AutoMapTo(typeof(UnitConversion))]
    public class UnitConversionEditDto
    {
        public int? Id { get; set; }
        public int? MaterialRefId { get; set; }
        public int BaseUnitId { get; set; }
        public string BaseUom { get; set; }
        public int RefUnitId { get; set; }
        public string RefUom { get; set; }
        public decimal Conversion { get; set; }
		public byte DecimalPlaceRounding { get; set; }
		public int? SyncId { get; set; }
		public DateTime? SyncLastModification { get; set; }

        public decimal DefaultConversion { get; set; }
        public byte DefaultDecimalPlaceRounding { get; set; }
        public bool CreatedBasedOnRecursive { get; set; }
        public int? ReferenceRecursiveId { get; set; }
        public List<SimpleSupplierListDto> LinkedSupplierList { get; set; }
        
        public UnitConversionEditDto()
        {
            LinkedSupplierList = new List<SimpleSupplierListDto>();
        }
        public bool DoesUpdateRequired { get; set; }

        public string ErrorRemarks { get; set; }
        
    }

	[AutoMapFrom(typeof(UnitConversionListDto))]
    public class UnitConversionTemplateDto 
    {
        public int Id { get; set; }
        public int? MaterialRefId { get; set; }
        public int BaseUnitId { get; set; }
        public int RefUnitId { get; set; }
        public decimal Conversion { get; set; }
        public string Name { get; set; }
		public byte DecimalPlaceRounding { get; set; }
		public int? SyncId { get; set; }
		public DateTime? SyncLastModification { get; set; }
        public bool CreatedBasedOnRecursive { get; set; }
        public int? ReferenceRecursiveId { get; set; }
    }


	public class UnitConversionTemplate
    {
        public List<UnitConversionTemplateDto> UcList { get; set; }
    }

    public class GetUnitConversionInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetUnitConversionForEditOutput : IOutputDto
    {
        public UnitConversionEditDto UnitConversion { get; set; }
    }

    public class CreateOrUpdateUnitConversionInput : IInputDto
    {
        [Required]
        public UnitConversionEditDto UnitConversion { get; set; }

        public bool RecursiveFlag { get; set; }
        public int? SupplierRefId { get; set; }
    }

    [AutoMapFrom(typeof(UnitConversion))]
    public class UnitConversionWithNames 
    {
        public int Id { get; set; }
        public int BaseUnitId { get; set; }
        public int RefUnitId { get; set; }
        public decimal Conversion { get; set; }
        public string BaseUnitName { get; set; }
        public string ConvertedUnitName { get; set; }
        public DateTime CreationTime { get; set; }
		public byte DecimalPlaceRounding { get; set; }
        public bool CreatedBasedOnRecursive { get; set; }
        public int? ReferenceRecursiveId { get; set; }
    }

    public class Supplier_Material_Units : IInputDto
    {
        public int SupplierRefId { get; set; }
        public int MaterialRefId { get; set; }
        public int BaseUnitId { get; set; }
        public int RefUnitId { get; set; }
        public decimal Conversion { get; set; }
    }

    public class GetConversionBasedOnBaseUnitRefUnit : IInputDto
    {
        public int BaseUnitId { get; set; }
        public int RefUnitId { get; set; }
        public int? MaterialRefId { get; set; }
        public string MateialRefName { get; set; }
        public int? SupplierRefId { get; set; } 
        public string SupplierRefName { get; set; }
        public List<UnitConversionListDto> ConversionUnitList { get; set; }
        public GetConversionBasedOnBaseUnitRefUnit()
        {
            ConversionUnitList = new List<UnitConversionListDto>();
        }
    }
}
