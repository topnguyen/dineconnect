﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Master.Dtos;

namespace DinePlan.DineConnect.House.Master.Dtos
{
    [AutoMapFrom(typeof(SupplierMaterial))]
    public class SupplierMaterialListDto : FullAuditedEntityDto
    {
        public int SupplierRefId { get; set; }

        public string SupplierRefName { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }

        public decimal MaterialPrice { get; set; }
        public decimal MinimumOrderQuantity { get; set; }
        public decimal MaximumOrderQuantity { get; set; }
        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }
        public string LastQuoteRefNo { get; set; }

        public DateTime LastQuoteDate { get; set; }
        public virtual decimal? YieldPercentage { get; set; }
        public int? LocationRefId { get; set; }
        public string SupplierMaterialAliasName { get; set; }

    }
    [AutoMapTo(typeof(SupplierMaterial))]
    public class SupplierMaterialEditDto
    {
        public int? Id { get; set; }
        public int SupplierRefId { get; set; }

        public string SupplierRefName { get; set; }
        public int MaterialRefId { get; set; }

        public string MaterialRefName { get; set; }

        public string Uom { get; set; }

        public decimal MaterialPrice { get; set; }
        public decimal MinimumOrderQuantity { get; set; }
        public decimal MaximumOrderQuantity { get; set; }
        public int UnitRefId { get; set; }
        public string LastQuoteRefNo { get; set; }

        public DateTime LastQuoteDate { get; set; }
        public virtual decimal? YieldPercentage { get; set; }
        public int? LocationRefId { get; set; }
        public string SupplierMaterialAliasName { get; set; }
    }

    public class GetSupplierMaterialInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int? SupplierRefId { get; set; }
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetSupplierMaterialForEditOutput : IOutputDto
    {
        public List<SupplierMaterialEditDto> SupplierMaterial { get; set; }
    }

    public class CreateOrUpdateSupplierMaterialInput : IInputDto
    {
        [Required]
        public List<SupplierMaterialEditDto> SupplierMaterial { get; set; }
    }

    public class CheckSupplierMaterial : IInputDto
    {
        [Required]
        public int SupplierRefId { get; set; }

        [Required]
        public int MaterialRefId { get; set; }
    }

    public class SupplierWithLocation : IInputDto
    {
        public int SupplierRefId { get; set; }

        public int? LocationRefId { get; set; }

        public int MaterialRefId { get; set; }

        public List<LocationListDto> SelectedLocations { get; set; }
    }

    public class DeleteSupplierWithLocationList : IInputDto
    {
        public List<SupplierWithLocation> SupplierWithLocations { get; set; }
    }

    public class SentLocationAndSupplier : IInputDto
    {
        [Required]
        public int SupplierRefId { get; set; }
        [Required]
        public int LocationRefId { get; set; }
        public int? PoRefId { get; set; }
        public int? InvoiceRefId { get; set; }
        public int? SalesInvoiceRefId { get; set; }
        public bool LoadOnlyLinkedMaterials { get; set; }
        public int MaterialRefId { get; set; }
        public List<int> MaterialRefIds { get; set; }

        public bool PurchaseReturnFlag { get; set; }
    }

    public class GetSupplierWithLocationAndMaterialInput : IInputDto
    {
        public List<int> MaterialRefIdList { get; set; }
        [Required]
        public int LocationRefId { get; set; }
        public int? PoRefId { get; set; }
        public int? InvoiceRefId { get; set; }
        public int? SalesInvoiceRefId { get; set; }

        public bool PurchaseReturnFlag { get; set; }
        public bool LoadOnlyLinkedSupplier { get; set; }
    }

    public class SentLocationAndCustomer : IInputDto
    {
        [Required]
        public int CustomerRefId { get; set; }

        [Required]
        public int LocationRefId { get; set; }
        public int? SoRefId { get; set; }
        public int? InvoiceRefId { get; set; }
        public int? SalesInvoiceRefId { get; set; }
        public DateTime TransactionDate { get; set; }
    }

    [AutoMapFrom(typeof(SupplierMaterial))]
    public class SupplierMaterialViewDto
    {
        public int? Id { get; set; }
        public int SupplierRefId { get; set; }
        public string SupplierRefName { get; set; }
        public string Barcode { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public int MaterialGroupRefId { get; set; }
        public int MaterialGroupCategoryRefId { get; set; }
        public decimal MaterialPrice { get; set; }
        public decimal MinimumOrderQuantity { get; set; }
        public decimal MaximumOrderQuantity { get; set; }
        public int UnitRefIdForMinOrder { get; set; }
        public string UnitRefNameForMinOrder { get; set; }
        public int UnitRefId { get; set; }
        public bool IsQuoteNeededForPurchase { get; set; }
        public decimal AlreadyReturnedQuantity { get; set; }
        public bool IsFractional { get; set; }
        public bool IsBranded { get; set; }
        public string LastQuoteRefNo { get; set; }
        public DateTime LastQuoteDate { get; set; }
        public virtual decimal? YieldPercentage { get; set; }
        public decimal CurrentInHand { get; set; }
        public decimal AlreadyOrderedQuantity { get; set; }
        public List<PipelineOrderDetails> PipelineDetails { get; set; }
        public int DefaultUnitId { get; set; }
        public string Uom { get; set; }
        public string UnitRefName { get; set; }
        public List<TaxForMaterial> TaxForMaterial { get; set; }
        public List<ApplicableTaxesForMaterial> ApplicableTaxes { get; set; }
        public virtual string Hsncode { get; set; }
        public int? LocationRefId { get; set; }
        public string SupplierMaterialAliasName { get; set; }
    }

    public class PipelineOrderDetails
    {
        public int SupplierRefId { get; set; }
        public string SupplierRefName { get; set; }
        public decimal QtyOrdered { get; set; }
        public decimal QtyReceived { get; set; }
        public decimal QtyPending { get; set; }
        public DateTime PoDate { get; set; }
        public DateTime ExpectedDeliveryDate { get; set; }
    }

    public class SupplierReturnedMaterialViewDto
    {
        public string SupplierRefName { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public string SupplierMaterialAliasName { get; set; }
        public decimal MaterialPrice { get; set; }
        public decimal AlreadyReturnedQuantity { get; set; }
        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }
        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }

    }

    public class PurchaseProjectionDto
    {
        public List<MaterialPoProjectionViewDto> ProjecttionData { get; set; }
        public string FileName { get; set; }
    }
}

