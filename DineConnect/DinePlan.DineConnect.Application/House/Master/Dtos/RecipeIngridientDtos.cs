﻿
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;

namespace DinePlan.DineConnect.House.Master.Dtos
{
    [AutoMapFrom(typeof(RecipeIngredient))]
    public class RecipeIngredientViewDto 
    {
       
        public int Id { get; set; }

        public int RecipeRefId { get; set; }

        public string RecipeRefName { get; set; }

        public int MaterialRefId { get; set; }
       
        public string MaterialRefName { get; set; }

        public decimal MaterialUsedQty { get; set; }

        public int UnitRefId { get; set; }

        public string UnitRefName { get; set; }

        public bool VariationflagForProduction { get; set; }
       
        public decimal UserSerialNumber { get; set; }
        public DateTime CreationTime { get; set; }

    }

    [AutoMapFrom(typeof(RecipeIngredient))]
    public class RecipeIngredientListDto : FullAuditedEntityDto
    {
        //TODO: DTO RecipeIngredient Properties Missing
        //YOU CAN REFER ATTRIBUTES IN 
       
        public int RecipeRefId { get; set; }

       
        public int MaterialRefId { get; set; }

       
        public decimal MaterialUsedQty { get; set; }

       
        public int UnitRefId { get; set; }

       
        public bool VariationflagForProduction { get; set; }

       
        public decimal UserSerialNumber { get; set; }

    }
    [AutoMapTo(typeof(RecipeIngredient))]
    public class RecipeIngredientEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO RecipeIngredient Properties Missing
       
        public int RecipeRefId { get; set; }

       
        public int MaterialRefId { get; set; }

       
        public decimal MaterialUsedQty { get; set; }

       
        public int UnitRefId { get; set; }

       
        public bool VariationflagForProduction { get; set; }

       
        public decimal UserSerialNumber { get; set; }

    }

    public class GetRecipeIngredientInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetRecipeIngredientForEditOutput : IOutputDto
    {
        public RecipeIngredientEditDto RecipeIngredient { get; set; }
    }
    public class CreateOrUpdateRecipeIngredientInput : IInputDto
    {
       
        public RecipeIngredientEditDto RecipeIngredient { get; set; }
    }
}

