﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System;
using System.Collections.ObjectModel;
using DinePlan.DineConnect.House.Transaction.Dtos;

namespace DinePlan.DineConnect.House.Master.Dtos
{
    public class MaterialTypeViewDto
    {
        public int MaterialTypeId { get; set; }
        public string MaterialTypeName { get; set; }

    }

    public class MaterialViewWithBarCode
    {
        public List<MaterialViewDto> MaterialViewList { get; set; }
        public List<MaterialBarCodeListDto> BarcodeList { get; set; }
    }

    public class BarCodeListDto : IOutputDto
    {
        public List<MaterialBarCodeListDto> BarcodeList { get; set; }
    }

    [AutoMapFrom(typeof(Material))]
    public class MaterialViewDto : FullAuditedEntityDto
    {

        public int MaterialTypeId { get; set; }

        public string MaterialTypeName { get; set; }
        public string Barcode { get; set; }
        public string MaterialName { get; set; }

        public string MaterialGroupName { get; set; }
        public int MaterialGroupCategoryRefId { get; set; }
        public string MaterialGroupCategoryName { get; set; }

        public string MaterialPetName { get; set; }
        public string MaterialSupplierAliasName { get; set; }
        public string SupplierList { get; set; }
        public int UnitRefId { get; set; }
        public string DefaultUnitName { get; set; }
        public int DefaultUnitId { get; set; }

        public int IssueUnitId { get; set; }
        public string IssueUnitName { get; set; }

        public int ReceivingUnitId { get; set; }

        public string ReceivingUnitRefName { get; set; }

        public int TransferUnitId { get; set; }

        public string TransferUnitRefName { get; set; }

        public int StockAdjustmentUnitId { get; set; }

        public string StockAdjustentUnitRefName { get; set; }

        public decimal UserSerialNumber { get; set; }

        public bool WipeOutStockOnClosingDay { get; set; }

        public decimal OnHand { get; set; }

        public string Uom { get; set; }

        public bool OwnPreparation { get; set; }
        public bool IsHighValueItem { get; set; }
        public decimal Price { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual int? SyncId { get; set; }
        public virtual DateTime? SyncLastModification { get; set; }

        public List<MaterialUsagePattern> UsagePatternList { get; set; }
        public virtual decimal? YieldPercentage { get; set; }
        public int AveragePriceTagRefId { get; set; }
        public string AveragePriceTagRefName { get; set; }
        public int NoOfMonthAvgTaken { get; set; }
        public string StockTakenReason { get; set; }
        public MaterialViewDto()
        {
            Barcodes = new Collection<MaterialBarCodeEditDto>();
            SupplierMaterialEditDtos = new List<SupplierMaterialEditDto>();
        }

        public Collection<MaterialBarCodeEditDto> Barcodes { get; set; }

        public decimal ClBalance { get; set; }
        public decimal ReservedIssueQuantity { get; set; }
        public decimal ReservedYieldQuantity { get; set; }
        public decimal ReservedQuantity => ReservedIssueQuantity + ReservedYieldQuantity;
        public decimal Difference { get; set; }
        public string Status { get; set; }
        public List<MaterialUnitsLinkViewDto> UnitsLinkViewDtos = new List<MaterialUnitsLinkViewDto>();
        public List<ClosingStockUnitWiseDetailViewDto> UnitWiseDetails { get; set; }
        public bool ConvertAsZeroStockWhenClosingStockNegative { get; set; }
        public string Code { get; set; }
        public int ClosingStockDetailRefId { get; set; }
        //var editSupplierMaterialDtos = new List<SupplierMaterialEditDto>();
        public List<SupplierMaterialEditDto> SupplierMaterialEditDtos { get; set; }
    }

    [AutoMapFrom(typeof(Material))]
    public class SimpleMaterialViewDto
    {
        public int Id { get; set; }
        public int MaterialTypeId { get; set; }
        public string MaterialTypeName { get; set; }
        public string Barcode { get; set; }
        public string MaterialName { get; set; }
        public string MaterialGroupCategoryName { get; set; }

        public string MaterialPetName { get; set; }

        public int UnitRefId { get; set; }
        public string DefaultUnitName { get; set; }
        public int IssueUnitId { get; set; }
        public string IssueUnitName { get; set; }

        public int ReceivingUnitId { get; set; }

        public string ReceivingUnitRefName { get; set; }

        public int TransferUnitId { get; set; }

        public string TransferUnitRefName { get; set; }

        public int StockAdjustmentUnitId { get; set; }

        public string StockAdjustentUnitRefName { get; set; }

        public decimal OnHand { get; set; }

        public string Uom { get; set; }

        public decimal Price { get; set; }
        public decimal ReservedIssueQuantity { get; set; }
        public decimal ReservedYieldQuantity { get; set; }
        public decimal ReservedQuantity => ReservedIssueQuantity + ReservedYieldQuantity;
    }

    [AutoMap(typeof(MaterialBarCode))]
    public class MaterialBarCodeEditDto
    {
        public int? Id { get; set; }
        public string Barcode { get; set; }
    }

    [AutoMap(typeof(MaterialBarCode))]
    public class MaterialBarCodeListDto
    {
        public int? Id { get; set; }
        public int MaterialRefId { get; set; }
        public string Barcode { get; set; }
    }

    public class MaterialExportDto : FullAuditedEntityDto
    {
        public int MaterialTypeId { get; set; }
        public string MaterialType { get; set; }
        public string Name { get; set; }
        public string Group { get; set; }
        public string Category { get; set; }
        public string AliasName { get; set; }
        public string Unit { get; set; }
        public decimal SortOrder { get; set; }
        public bool OwnPreparation { get; set; }
        public bool WipeOutStock { get; set; }
        public int UnitRefId { get; set; }
        public decimal OnHand { get; set; }
        public string Uom { get; set; }
        public bool IsHighValueItem { get; set; }
        public string Barcode { get; set; }
        public int IssueUnitId { get; set; }

        public string IssueUnitRefName { get; set; }

        public int ReceivingUnitId { get; set; }

        public string ReceivingUnitRefName { get; set; }

        public int TransferUnitId { get; set; }

        public string TransferUnitRefName { get; set; }

        public int StockAdjustmentUnitId { get; set; }
        public string StockAdjustmentUnitRefName { get; set; }

    }

    [AutoMapFrom(typeof(Material))]
    public class MaterialPoProjectionViewDto
    {
        public string Barcode { get; set; }
        public int MaterialRefId { get; set; }

        public int MaterialTypeId { get; set; }

        public string MaterialTypeName { get; set; }

        public string MaterialName { get; set; }
        public string MaterialAliasName { get; set; }

        public string MaterialGroupCategoryName { get; set; }

        public string MaterialPetName { get; set; }

        public int UnitRefId { get; set; }

        public string Uom { get; set; }

        public decimal UserSerialNumber { get; set; }

        public bool WipeOutStockOnClosingDay { get; set; }

        public decimal OnHand { get; set; }



        public List<MaterialUsagePattern> UsagePatternList { get; set; }

        public decimal AlreadyOrderedQuantity { get; set; }

        public decimal LastNDaysConsumption { get; set; }

        public decimal PerDayConsumption { get; set; }

        public decimal NextNDaysProjectedConsumption { get; set; }

        public bool OmitQueueInOrder { get; set; }
        public decimal NextNDaysRoundedProjection { get; set; }

        public int? SupplierRefId { get; set; }

        public string SupplierRefName { get; set; }
        public decimal? Price { get; set; }

        public decimal? Amount => Price * NextNDaysRoundedProjection;

        public int? LastSupplierRefId { get; set; }

        public string LastSupplierRefName { get; set; }

        public decimal? LastSupplierPrice { get; set; }

        public decimal? LastSupplierAmount => Price * NextNDaysRoundedProjection;


    }

    [AutoMapFrom(typeof(MaterialLedger))]
    public class MaterialSalesWithWipeOutCloseDayDto
    {
        public int LocationRefId { get; set; }
        public DateTime? LedgerDate { get; set; }
        public string Barcode { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public decimal OpenBalance { get; set; }
        public decimal Received { get; set; }
        public decimal Issued { get; set; }
        public decimal TransferIn { get; set; }
        public decimal TransferOut { get; set; }
        public decimal Damaged { get; set; }
        public decimal SupplierReturn { get; set; }
        public decimal ExcessReceived { get; set; }
        public decimal Shortage { get; set; }
        public decimal Return { get; set; }
        public decimal ClBalance { get; set; }
        public string Uom { get; set; }
        public decimal Sales { get; set; }
        public decimal CompWastage { get; set; }
        public decimal MenuWastage { get; set; }
        public decimal Difference { get; set; }
        public string Remarks { get; set; }
        public bool WipeOutStockOnClosingDay { get; set; }
        public bool AutoSalesDeduction { get; set; }
        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }

        public int IssueUnitId { get; set; }
        public string IssueUnitName { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual int? SyncId { get; set; }
        public virtual DateTime? SyncLastModification { get; set; }
        public virtual decimal? YieldPercentage { get; set; }
        public int AveragePriceTagRefId { get; set; }
        public string AveragePriceTagRefName { get; set; }
        public int NoOfMonthAvgTaken { get; set; }
    }



    [AutoMapFrom(typeof(Material))]
    public class MaterialListDto : FullAuditedEntityDto
    {
        //YOU CAN REFER ATTRIBUTES IN 
        public int MaterialTypeId { get; set; }
        public string MaterialName { get; set; }
        public string Barcode { get; set; }
        public int MaterialGroupCategoryRefId { get; set; }
        public bool IsFractional { get; set; }

        public bool IsBranded { get; set; }

        public bool IsQuoteNeededForPurchase { get; set; }

        public int GeneralLifeDays { get; set; }
        public int BrandRefId { get; set; }

        public string MaterialPetName { get; set; }

        public int DefaultUnitId { get; set; }
        public int IssueUnitId { get; set; }
        public int ReceivingUnitId { get; set; }

        public int TransferUnitId { get; set; }
        public string TransferUnitRefName { get; set; }
        public int StockAdjustmentUnitId { get; set; }
        public string StockAdjustentUnitRefName { get; set; }
        public string DefaultUnitName { get; set; }

        public decimal UserSerialNumber { get; set; }

        public bool OwnPreparation { get; set; }

        public bool IsNeedtoKeptinFreezer { get; set; }

        public bool MRPPriceExists { get; set; }

        public int PosReferencecodeifany { get; set; }

        public bool IsMfgDateExists { get; set; }

        public bool WipeOutStockOnClosingDay { get; set; }
        public bool IsHighValueItem { get; set; }

        public List<TaxForMaterial> TaxForMaterial { get; set; }

        public MaterialListDto()
        {
            Barcodes = new Collection<MaterialBarCodeEditDto>();
        }

        public Collection<MaterialBarCodeEditDto> Barcodes { get; set; }

        public virtual string Hsncode { get; set; }

        public virtual bool IsActive { get; set; }

        public virtual int? SyncId { get; set; }
        public virtual DateTime? SyncLastModification { get; set; }
        public virtual decimal? YieldPercentage { get; set; }
        public int AveragePriceTagRefId { get; set; }
        public string AveragePriceTagRefName { get; set; }
        public int NoOfMonthAvgTaken { get; set; }
        public bool ConvertAsZeroStockWhenClosingStockNegative { get; set; }
        public string MaterialGroupName { get; set; }

        public string MaterialCategoryName { get; set; }
        public List<ComboboxItemDto> UnitList { get; set; }
    }


    [AutoMapFrom(typeof(Material))]
    public class MaterialInfoDto
    {
        public string Barcode { get; set; }
        public int MaterialRefId { get; set; }
        public int MaterialTypeId { get; set; }
        public string MaterialRefName { get; set; }
        public bool IsFractional { get; set; }
        public bool IsBranded { get; set; }
        public bool IsQuoteNeededForPurchase { get; set; }
        public string MaterialPetName { get; set; }
        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }
        public int IssueUnitId { get; set; }
        public int ReceivingUnitId { get; set; }

        public int TransferUnitId { get; set; }
        public string TransferUnitRefName { get; set; }
        public int StockAdjustmentUnitId { get; set; }
        public string StockAdjustentUnitRefName { get; set; }
        public string IssueUnitName { get; set; }
        public bool IsNeedtoKeptinFreezer { get; set; }
        public bool IsMfgDateExists { get; set; }
        public bool WipeOutStockOnClosingDay { get; set; }
        public bool IsHighValueItem { get; set; }
        public decimal CurrentInHand { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual int? SyncId { get; set; }
        public virtual DateTime? SyncLastModification { get; set; }
        public virtual decimal? YieldPercentage { get; set; }
        public bool ConvertAsZeroStockWhenClosingStockNegative { get; set; }
    }


    [AutoMapTo(typeof(Material))]
    public class MaterialEditDto
    {

        public MaterialEditDto()
        {
            Barcodes = new Collection<MaterialBarCodeEditDto>();
        }

        public Collection<MaterialBarCodeEditDto> Barcodes { get; set; }

        public int? Id { get; set; }
        //TODO: DTO Material Properties Missing
        public int MaterialTypeId { get; set; }
        public string MaterialName { get; set; }

        public int MaterialGroupCategoryRefId { get; set; }

        public bool IsFractional { get; set; }

        public bool IsBranded { get; set; }

        public bool IsQuoteNeededForPurchase { get; set; }

        public int GeneralLifeDays { get; set; }

        public string MaterialPetName { get; set; }

        public int DefaultUnitId { get; set; }

        public string Uom { get; set; }
        public int IssueUnitId { get; set; }
        public string IssueUom { get; set; }

        public int ReceivingUnitId { get; set; }

        public int TransferUnitId { get; set; }
        public string TransferUnitRefName { get; set; }
        public int StockAdjustmentUnitId { get; set; }

        public string StockAdjustentUnitRefName { get; set; }

        public int BrandRefId { get; set; }

        public decimal UserSerialNumber { get; set; }

        public bool OwnPreparation { get; set; }

        public bool IsNeedtoKeptinFreezer { get; set; }

        public bool MRPPriceExists { get; set; }

        public int PosReferencecodeifany { get; set; }

        public bool IsMfgDateExists { get; set; }
        public bool WipeOutStockOnClosingDay { get; set; }
        public bool IsHighValueItem { get; set; }
        public string Barcode { get; set; }
        public virtual string Hsncode { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual int? SyncId { get; set; }
        public virtual DateTime? SyncLastModification { get; set; }
        public virtual decimal? YieldPercentage { get; set; }
        public int AveragePriceTagRefId { get; set; }
        public int NoOfMonthAvgTaken { get; set; }
        public bool ConvertAsZeroStockWhenClosingStockNegative { get; set; }
        public string MaterialGroupName { get; set; }
        public string MaterialCategoryName { get; set; }
        public List<ComboboxItemDto> UnitList { get; set; }

    }

    [AutoMapTo(typeof(Material))]
    public class MaterialImportDto
    {
        public int? Id { get; set; }
        //TODO: DTO Material Properties Missing
        public int MaterialTypeId { get; set; }
        public string MaterialName { get; set; }

        public int MaterialGroupCategoryRefId { get; set; }

        public string MaterialGroupName { get; set; }

        public string MaterialCategoryName { get; set; }

        public string Uom { get; set; }

        public bool IsFractional { get; set; }

        public bool IsBranded { get; set; }

        public bool IsQuoteNeededForPurchase { get; set; }

        public int GeneralLifeDays { get; set; }

        public string MaterialPetName { get; set; }

        public int DefaultUnitId { get; set; }

        public int IssueUnitId { get; set; }
        public int TransferUnitId { get; set; }

        public int StockAdjustmentUnitId { get; set; }

        public int BrandRefId { get; set; }

        public decimal UserSerialNumber { get; set; }

        public bool OwnPreparation { get; set; }

        public bool IsNeedtoKeptinFreezer { get; set; }

        public bool MRPPriceExists { get; set; }

        public int PosReferencecodeifany { get; set; }

        public bool IsMfgDateExists { get; set; }
        public bool WipeOutStockOnClosingDay { get; set; }
        public bool IsHighValueItem { get; set; }
        public string Barcode { get; set; }
        public List<ComboboxItemDto> UnitList { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual int? SyncId { get; set; }
        public virtual DateTime? SyncLastModification { get; set; }
        public virtual decimal? YieldPercentage { get; set; }
        public bool ConvertAsZeroStockWhenClosingStockNegative { get; set; }

    }

    public class GetMaterialInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetMaterialForEditOutput : IOutputDto
    {

        [Required]
        public MaterialEditDto Material { get; set; }

        //[Required]
        //public List<MaterialUnitsLinkEditDto> MaterialUnitsLink_DetailList { get; set; }

        public List<ComboboxItemDto> UnitList { get; set; }

        public List<ComboboxItemDto> BrandList { get; set; }

        public List<ComboboxItemDto> StockCycle { get; set; }

        public List<SupplierMaterialEditDto> SupplierMaterial { get; set; }

        public List<CustomerMaterialEditDto> CustomerMaterial { get; set; }

        public List<CustomerTagMaterialPriceEditDto> CustomerTagMaterialPrice { get; set; }

        public List<MaterialIngredientViewDto> MaterialIngredient { get; set; }

        public MaterialRecipeTypesEditDto MaterialRecipeType { get; set; }
        public List<UnitConversionEditDto> UnitConversionList { get; set; }


    }
    public class CreateOrUpdateMaterialInput : IInputDto
    {
        public CreateOrUpdateMaterialInput()
        {
            UnitList = new List<ComboboxItemDto>();
            UnitConversionList = new List<UnitConversionEditDto>();
            StockCycle = new List<ComboboxItemDto>();
            SupplierMaterial = new List<SupplierMaterialEditDto>();
            CustomerMaterial = new List<CustomerMaterialEditDto>();
        }

        [Required]
        public MaterialEditDto Material { get; set; }
        //[Required]
        //public List<MaterialUnitsLinkEditDto> MaterialUnitsLink_DetailList { get; set; }
        //public List<MaterialBrandsLinkEditDto> MaterialBrandsLink_DetailList { get; set; }
        public List<ComboboxItemDto> UnitList { get; set; }
        public List<UnitConversionEditDto> UnitConversionList { get; set; }
        public List<ComboboxItemDto> BrandList { get; set; }
        public List<ComboboxItemDto> StockCycle { get; set; }
        public List<SupplierMaterialEditDto> SupplierMaterial { get; set; }

        public List<CustomerMaterialEditDto> CustomerMaterial { get; set; }

        public List<CustomerTagMaterialPriceEditDto> CustomerTagMaterialPrice { get; set; }

        public List<MaterialIngredientEditDto> MaterialIngredient { get; set; }

        public bool UpdateSerialNumberRequired { get; set; }

        public MaterialRecipeTypesEditDto MaterialRecipeType { get; set; }



    }

    public class ClosingStockExcelReadOutputDto:IOutputDto
    {
        public List<MaterialAdjustmentStockDto> MaterialStockList { get; set; }
        public List<MaterialSuccessOrErrorMessageOutput> ErrorList { get; set; }
    }

    [AutoMapFrom(typeof(MaterialLedger))]
    public class MaterialAdjustmentStockDto
    {
        [Required]
        public int LocationRefId { get; set; }

        [Required]
        public DateTime LedgerDate { get; set; }

        [Required]
        public int MaterialRefId { get; set; }

        public string MaterialName { get; set; }

        public string MaterialPetName { get; set; }

        public int MaterialGroupCategoryRefId { get; set; }
        public string MaterialGroupCategoryRefName { get; set; }

        [Required]
        public decimal ClBalance { get; set; }

        [Required]
        public decimal CurrentStock { get; set; }

        [Required]
        public decimal DifferenceQuantity { get; set; }

        [Required]
        public int UnitRefId { get; set; }

        [Required]
        public string UnitRefName { get; set; }

        [Required]
        public int DefaultUnitId { get; set; }

        [Required]
        public string DefaultUnitName { get; set; }

        public string AdjustmentStatus { get; set; }

        public List<UnitWiseQuantity> UnitWiseQuantityList { get; set; }
    }

    public class UnitWiseQuantity
    {
        public int MaterialRefId { get; set; }
        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }
        public decimal OnHand { get; set; }
        public decimal Conversion { get; set; }
        public decimal ConvertedQuantity { get; set; }
    }

    [AutoMapFrom(typeof(MaterialLedger))]
    public class MaterialLedgerDto
    {
        public int Id { get; set; }

        [Required]
        public int LocationRefId { get; set; }
        
        [Required]
        public DateTime LedgerDate { get; set; }

        [Required]
        public int MaterialRefId { get; set; }

        public string MaterialPetName { get; set; }
        public string MaterialName { get; set; }

        public int MaterialTypeRefId { get; set; }

        public string MaterialTypeRefName { get; set; }

        public int MaterialGroupRefId { get; set; }
        public string MaterialGroupRefName { get; set; }

        public int MaterialGroupCategoryRefId { get; set; }
        public string MaterialGroupCategoryRefName { get; set; }

        [Required]
        public decimal OpenBalance { get; set; }

        [Required]
        public decimal Received { get; set; }   //  Dc + Invoice + Yield + Production

        [Required]
        public decimal Issued { get; set; }     //  Issue For Production + Issue For Yield (Yield Input)

        [Required]
        public decimal Sales { get; set; }

        [Required]
        public decimal TransferIn { get; set; } //  Transfer In

        [Required]
        public decimal TransferOut { get; set; }    //  Transfer Out

        [Required]
        public decimal Damaged { get; set; }

        [Required]
        public decimal SupplierReturn { get; set; }

        [Required]
        public decimal ExcessReceived { get; set; }
        [Required]
        public decimal Shortage { get; set; }
        [Required]
        public decimal Return { get; set; }

        [Required]
        public decimal ClBalance { get; set; }

        public string PlusMinus { get; set; }

        public decimal AvgPrice { get; set; }

        public decimal StockValue { get; set; }

        public decimal TotalConsumption { get; set; }
        public decimal AvgConsumption { get; set; }

        public bool NegativeStockAccepted { get; set; }
        public bool DoesBackGroundDayCloseProcess { get; set; }
        public int? UnitId { get; set; }
        public int DefaultUnitRefId { get; set; }
        public string Uom { get; set; }

        public DateTime LedgerFromDate { get; set; }
        public DateTime LedgerToDate { get; set; }

        public bool ForceAdjustmentFlag { get; set; }

        public bool CarryOverFlag { get; set; }

        public decimal EnteredClosingStock { get; set; }
        public string EnteredClosingStockDisplay { get; set; }

        public decimal TheoreticalUsage { get; set; }   //   TheoreticalUsage  = Usage from Sales + Finished Waste + Raw Waste + BRUSage

        public decimal TheoreticalUsageCost => Math.Round(TheoreticalUsage * AvgPrice, 3);

        public decimal ExpectedClosingStock { get; set; } // 

        public decimal ActualUsage { get; set; }    //  Op + Purchase + Tin - Tout - Return - EnteredClosingStock

        public decimal ActualUsageCost => Math.Round(ActualUsage * AvgPrice, 3);
        public string ActualUsageDisplay { get; set; }

        public decimal VarianceStock { get; set; }
        public string VarianceStockDisplay { get; set; }
        public decimal VarianceCost => Math.Round(VarianceStock * AvgPrice, 3);
        public int? SupplierRefId { get; set; }
        public decimal ConversionWithDefaultUnit { get; set; }
    }

    public class MaterialTrackDto
    {
        public int LocationRefId { get; set; }

        public DateTime LedgerDate { get; set; }

        public DateTime? AccountDate { get; set; }

        public DateTime ModifyTime { get; set; }

        public int MaterialRefId { get; set; }

        public string MaterialName { get; set; }

        public string TrackStatus { get; set; }

        public string TrackDesc { get; set; }

        public decimal Issued_BasedOnActualUnit { get; set; }

        public decimal Issued { get; set; }     //  Issued, TransferOut , Damaged, SupplierReturn, Shortage
        public decimal Recived_BasedOnActualUnit { get; set; }
        public decimal Recived { get; set; }    //   OpenBalance    , Received, TransferIn, ExcessReceived, Return, ClBalance
        public string Remarks { get; set; }
        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }
        public decimal OpenBalance { get; set; }
        public decimal ClosingBalance { get; set; }

        public decimal SortOrder { get; set; }
        public decimal ConversionWithDefaultUnit { get; set; }
    }

    public class MaterialTrackConsolidatedDto
    {
        public int LocationRefId { get; set; }

        public string LocationRefName { get; set; }

        public int MaterialRefId { get; set; }

        public string MaterialName { get; set; }

        public string Uom { get; set; }

        public decimal OpeningBalance { get; set; }
        public decimal ClosingBalance { get; set; }
        public decimal TotalReceived { get; set; }
        public decimal TotalIssued { get; set; }
        public string Remarks { get; set; }
        public DateTime? ErrorDate { get; set; }
        public bool ErrorInClosingBalanceCarryOver { get; set; }
        public List<MaterialTrackDto> TrackDetail { get; set; }
    }

    public class InputLocationAndRecipe
    {
        public int LocationRefId { get; set; }

        public int RecipeRefId { get; set; }
    }

    public enum MaterialType
    {
        RAW,
        SEMI,
    };

    public enum AveragePriceTag
    {
        Given_Dates_Average = 0,    //  Default 
        Last_Purchase = 1,             //  Just Take the Last Purchase Price From Invoice / InterTransfer / Yield
        Purchase_Avg_Last_N_Months = 2,//  Average Cost  = Last N Month Purchase Value / Last N Month Purchase Qty
        Moving_Average = 3,       //  Take the Last N Purchase until the current stock value reaches and Average Cost = Purchase Cost / Purchase Qty
    }

    public class InputLocationAndMaterialId
    {
        public int LocationRefId { get; set; }

        public int MaterialRefId { get; set; }
    }

    public class InputPurchaseProjection
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int LocationRefId { get; set; }

        public int NextNDays { get; set; }

        public bool IsHighValueItemOnly { get; set; }

        public string RequestQtyBasedOn { get; set; }
        public bool OmitQueueInOrder { get; set; }
    }



    public class MaterialUsagePattern
    {
        public int Days { get; set; }

        public string Description { get; set; }

        public decimal Consumption { get; set; }
    }

    public class StockMovementErrorDto
    {
        public int LocationRefId { get; set; }
        public int MaterialRefId { get; set; }
        public DateTime ErrorDate { get; set; }
        public string ErrorDesciption { get; set; }
        public DateTime EntryTime { get; set; }
    }

    public class StockMovementInputDto
    {
        public bool AllLocationVerification { get; set; }
        public int? UserId { get; set; }
        public DateTime? MovementDateFrom { get; set; }
        public DateTime? MovementDateTo { get; set; }

    }

    public class UserIdInput
    {
        public int UserId { get; set; }
    }

    public class GetRawMaterailBasedOnRecipeInput : IInputDto
    {
        public int RecipeRefId { get; set; }

        public decimal OutputQty { get; set; }
        public MaterialRecipeTypes MaterialRecipeType { get; set; }
        public List<MaterialIngredient> MaterialIngredients { get; set; }
        public List<Material> Materials { get; set; }
        public List<UnitConversionListDto> UnitConversionLists { get; set; }
    }


    public class MaterialLinkWithOtherMaterialViewDto : FullAuditedEntityDto
    {
        public int? LocationRefId { get; set; }
        public string LocationRefName { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public decimal MaterialUsageQuantity { get; set; }
        public string Uom { get; set; }
        public int RecipeTypeRefId { get; set; }
        public string RecipeTypeRefName { get; set; }
        public int RecipeRefId { get; set; }
        public string RecipeRefName { get; set; }
        public decimal OutputQuantity { get; set; }
        public string OutputUom { get; set; }


    }

    public class LocationWithMaterialList : IInputDto
    {
        public int LocationRefId { get; set; }
        public List<int> MaterialRefIds { get; set; }
        public bool CalculateIssueReservedQuantity { get; set; }
        public bool CalculateYieldReservedQuantity { get; set; }
    }

    public class LocationWithPriceDto : IInputDto
    {
        public int LocationRefId { get; set; }

        public bool PriceFlagNeeded { get; set; }
        public bool CalculateIssueReservedQuantity { get; set; }
        public bool CalculateYieldReservedQuantity { get; set; }
    }

    public class LocationInputDto : IInputDto
    {
        public int LocationRefId { get; set; }
    }

    public class DateInput : IInputDto
    {
        public DateTime? Dt { get; set; }
        public DateTime? DateId { get; set; }
    }


    public class GetClosingStockExistInput : IInputDto
    {
        public DateTime? Dt { get; set; }
        public DateTime? DateId { get; set; }
        public int LocationRefId { get; set; }
    }


    public class DateWithProductionUnit : IInputDto
    {
        public DateTime? Dt { get; set; }
        public int? ProductionUnitRefId { get; set; }
    }

    public class MaterialUnitChangeDto : IInputDto
    {
        public int MaterialRefId { get; set; }
        public int ExistUnitRefId { get; set; }
        public int RevisedUnitRefId { get; set; }

    }

    public class MaterialChangeOutputDto : IOutputDto
    {
        public string OutputMessage { get; set; }
        public bool MenuMappingExist { get; set; }
        public FileDto ExcelFile { get; set; }
    }

    public class MaterialInputDto : IInputDto
    {
        public int MaterialRefId { get; set; }
    }

    [AutoMapTo(typeof(MaterialStockCycleLink))]
    public class MaterialStockCycleLinkListDto : FullAuditedEntityDto
    {
        //public int? LocationGroupRefId { get; set; }

        //public int? LocationRefId { get; set; }

        public int MaterialRefId { get; set; }

        public int InventoryCycleTagRefId { get; set; }

    }

    [AutoMapFrom(typeof(Material))]
    public class SimpleMaterialListDto
    {
        public int Id { get; set; }
        public int MaterialTypeId { get; set; }
        public string MaterialName { get; set; }
        public string Barcode { get; set; }
        public int MaterialGroupCategoryRefId { get; set; }
        public string MaterialPetName { get; set; }
        public int DefaultUnitId { get; set; }
        public int IssueUnitId { get; set; }
        public string DefaultUnitName { get; set; }
        public bool IsHighValueItem { get; set; }

    }

    public class MessageOutput : IOutputDto
    {
        public bool SuccessFlag { get; set; }
        public string Message { get; set; }
        public string SuccessMessage { get; set; }
        public string HtmlMessage { get; set; }
        public string ErrorMessage { get; set; }
        public int Count { get; set; }
        public List<SimpleMaterialListDto> Materials { get; set; }

        public int EmployeeRefId { get; set; }
        public string EmployeeRefName { get; set; }
        public string Email { get; set; }
        public string PersonalEmail { get; set; }
        public string HrOperationHealthText { get; set; }
        public string HrOperationHealthHtml { get; set; }

        public int Id { get; set; }

    }

    public class MaterialSuccessOrErrorMessageOutput : IOutputDto
    {
        public bool SuccessFlag { get; set; }
        public string Message { get; set; }
        public string SuccessMessage { get; set; }
        public string HtmlMessage { get; set; }
        public string ErrorMessage { get; set; }
        public int Count { get; set; }
        public List<SimpleMaterialListDto> Materials { get; set; }

        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public int Id { get; set; }

    }

    public class GetRequestAsPO
    {
        public int LocationRefId { get; set; }
        public int ShippingLocationId { get; set; }
        public DateTime DeliveryDateExpected { get; set; }
        public List<InterTransferPOGenerationDto> RequestMaterialsToBeOrdered { get; set; }

    }

    public class GetMaterialMappedList : IInputDto
    {
        public List<MaterialListDto> MaterialList { get; set; }
    }

    public class HouseLocationWiseMaterialInitialiseDto
    {
        public int? LocationRefId { get; set; }
        public long? UserId { get; set; }
        public int TenantId { get; set; }
        public int? MaterialRefId { get; set; }
    }

    public class ImportDataResult
    {
        public List<ImportDetailDataDto> ImportDetailDataDtos { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class ImportDetailDataDto
    {
        public int RowNumber { get; set; }
        public string Description { get; set; }
        public string DataValue { get; set; }
        public string Status { get; set; }
        public int? AppendedId { get; set; }
        public string ErrorRemarks { get; set; }
    }


    public class ComboBoxListInput
    {
        public List<ComboboxItemDto> ComboboxItemList { get; set; }
        public ComboBoxListInput()
        {
            ComboboxItemList = new List<ComboboxItemDto>();
        }
    }

}

