﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;

namespace DinePlan.DineConnect.House.Master.Dtos
{

    [AutoMapTo(typeof(MaterialUnitsLink))]
    public class MaterialUnitsLinkViewDto
    {
        public int Id { get; set; }
         public int MaterialRefId { get; set; }
        public string MaterialName { get; set; }
         public int UnitRefId { get; set; }
        public string UnitName { get; set; }
        public DateTime CreationTime { get; set; }
    }

    [AutoMapFrom(typeof(MaterialUnitsLink))]
    public class MaterialUnitsLinkListDto : FullAuditedEntityDto
    {
        public int MaterialRefId { get; set; }
        public int UnitRefId { get; set; }
    }
    [AutoMapTo(typeof(MaterialUnitsLink))]
    public class MaterialUnitsLinkEditDto
    {
        public int? Id { get; set; }
        public int MaterialRefId { get; set; }
        public int UnitRefId { get; set; }

    }

    public class GetMaterialUnitsLinkInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
    public class GetMaterialUnitsLinkForEditOutput : IOutputDto
    {
        public MaterialUnitsLinkEditDto MaterialUnitsLink { get; set; }
    }
    public class CreateOrUpdateMaterialUnitsLinkInput : IInputDto
    {
        [Required]
        public MaterialUnitsLinkEditDto MaterialUnitsLink { get; set; }
    }
}

