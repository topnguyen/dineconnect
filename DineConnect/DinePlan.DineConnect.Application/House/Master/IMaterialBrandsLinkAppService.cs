﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master
{
    public interface IMaterialBrandsLinkAppService : IApplicationService
    {
        Task<PagedResultOutput<MaterialBrandsLinkListDto>> GetAll(GetMaterialBrandsLinkInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetMaterialBrandsLinkForEditOutput> GetMaterialBrandsLinkForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateMaterialBrandsLink(CreateOrUpdateMaterialBrandsLinkInput input);
        Task DeleteMaterialBrandsLink(IdInput input);

        Task<ListResultOutput<MaterialBrandsLinkListDto>> GetIds();
        Task<ListResultOutput<MaterialBrandsLinkListDto>> GetMaterialForBrand(IdInput input);
        Task<ListResultOutput<MaterialBrandsLinkListDto>> GetBrandForMaterial(IdInput input);
        Task<PagedResultOutput<MaterialBrandsLinkViewDto>> GetView(GetMaterialBrandsLinkInput input);
    }
}