﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master
{
    public interface IMaterialGroupCategoryAppService : IApplicationService
    {
        Task<PagedResultOutput<MaterialGroupCategoryListDto>> GetAll(GetMaterialGroupCategoryInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetMaterialGroupCategoryForEditOutput> GetMaterialGroupCategoryForEdit(NullableIdInput nullableIdInput);
        Task<IdInput> CreateOrUpdateMaterialGroupCategory(CreateOrUpdateMaterialGroupCategoryInput input);
        Task DeleteMaterialGroupCategory(IdInput input);

        Task<ListResultOutput<MaterialGroupCategoryListDto>> GetMaterialGroupCategoryNames();
        Task<PagedResultOutput<MaterialGroupCategoryViewDto>> GetView(GetMaterialGroupCategoryInput input);
        Task<MaterialGroupCategoryEditDto> GetOrCreateByName(string categoryName, string groupname);
    }
}