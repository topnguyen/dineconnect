﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using System.Collections.Generic;

namespace DinePlan.DineConnect.House.Transaction
{
    public interface IMaterialLedgerAppService : IApplicationService
    {
        Task<PagedResultOutput<MaterialLedgerViewDto>> GetAll(GetMaterialLedgerInput inputDto);
        Task<FileDto> GetAllToExcel(GetMaterialLedgerInput input);
        Task<GetMaterialLedgerForEditOutput> GetMaterialLedgerForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateMaterialLedger(CreateOrUpdateMaterialLedgerInput input);
        Task DeleteMaterialLedger(IdInput input);
        Task<List<MaterialLinkWithOtherMaterialViewDto>> GetMaterialLinkWithOtherMaterialsList(IdInput input);
        Task<FileDto> GetMaterialLinkWithOtherMateriaalsToExcel(IdInput input);

    }
}