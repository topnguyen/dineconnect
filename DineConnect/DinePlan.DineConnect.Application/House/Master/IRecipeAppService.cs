﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master
{
    public interface IRecipeAppService : IApplicationService
    {
        Task<PagedResultOutput<RecipeListDto>> GetAll(GetRecipeInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetRecipeForEditOutput> GetRecipeForEdit(NullableIdInput nullableIdInput);
        Task<IdInput> CreateOrUpdateRecipe(CreateOrUpdateRecipeInput input);
        Task DeleteRecipe(IdInput input);

        Task<ListResultOutput<RecipeListDto>> GetRecipeNames();
        Task<PagedResultOutput<RecipeViewDto>> GetView(GetUnitConversionInput input);

        Task<PagedResultOutput<RecipeIngredientViewDto>> GetIngredientDto();
        Task<PagedResultOutput<RecipeCostingReportDto>> GetRecipeCosting(GetRecipeInput input);
    }
}