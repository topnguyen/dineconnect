﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master
{
    public interface ITaxAppService : IApplicationService
    {
        Task<PagedResultOutput<TaxListDto>> GetAll(GetTaxInput inputDto);
        Task<FileDto> GetAllToExcel(GetTaxInput input);
        Task<GetTaxForEditOutput> GetTaxForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateTax(CreateOrUpdateTaxInput input);
        Task DeleteTax(IdInput input);

        Task<ListResultOutput<TaxListDto>> GetTaxNames();
    }
}