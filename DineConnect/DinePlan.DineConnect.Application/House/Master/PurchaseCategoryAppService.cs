﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Utility.Exporter;
using DinePlan.DineConnect.Utility;

namespace DinePlan.DineConnect.House.Master.Implementation
{
	public class PurchaseCategoryAppService : DineConnectAppServiceBase, IPurchaseCategoryAppService
	{

		private readonly IExcelExporter _exporter;
		private readonly IPurchaseCategoryManager _purchasecategoryManager;
		private readonly IRepository<PurchaseCategory> _purchasecategoryRepo;

		public PurchaseCategoryAppService(IPurchaseCategoryManager purchasecategoryManager,
			IRepository<PurchaseCategory> purchaseCategoryRepo,
			IExcelExporter exporter)
		{
			_purchasecategoryManager = purchasecategoryManager;
			_purchasecategoryRepo = purchaseCategoryRepo;
			_exporter = exporter;
		}

		public async Task<PagedResultOutput<PurchaseCategoryListDto>> GetAll(GetPurchaseCategoryInput input)
		{
			var allItems = _purchasecategoryRepo.GetAll();
			if (input.Operation == "SEARCH")
			{
				allItems = _purchasecategoryRepo
			   .GetAll()
			   .WhereIf(
				   !input.Filter.IsNullOrEmpty(),
				   p => p.PurchaseCategoryName.Equals(input.Filter)
			   );
			}
			else
			{
				allItems = _purchasecategoryRepo
			   .GetAll()
			   .WhereIf(
				   !input.Filter.IsNullOrEmpty(),
				   p => p.PurchaseCategoryName.Contains(input.Filter)
			   );
			}
			var sortMenuItems = await allItems
				.OrderBy(input.Sorting)
				.PageBy(input)
				.ToListAsync();

			var allListDtos = sortMenuItems.MapTo<List<PurchaseCategoryListDto>>();

			var allItemCount = await allItems.CountAsync();

			return new PagedResultOutput<PurchaseCategoryListDto>(
				allItemCount,
				allListDtos
				);
		}

		public async Task<FileDto> GetAllToExcel()
		{


			var allList = await _purchasecategoryRepo.GetAll().ToListAsync();
			var allListDtos = allList.MapTo<List<PurchaseCategoryListDto>>();
			var baseE = new BaseExportObject()
			{
				ExportObject = allListDtos,
				ColumnNames = new string[] { "Id" }
			};
			return _exporter.ExportToFile(baseE, L("PurchaseCategory"));

		}

		public async Task<GetPurchaseCategoryForEditOutput> GetPurchaseCategoryForEdit(NullableIdInput input)
		{
			PurchaseCategoryEditDto editDto;

			if (input.Id.HasValue)
			{
				var hDto = await _purchasecategoryRepo.GetAsync(input.Id.Value);
				editDto = hDto.MapTo<PurchaseCategoryEditDto>();
			}
			else
			{
				editDto = new PurchaseCategoryEditDto();
			}

			return new GetPurchaseCategoryForEditOutput
			{
				PurchaseCategory = editDto
			};
		}

		public async Task CreateOrUpdatePurchaseCategory(CreateOrUpdatePurchaseCategoryInput input)
		{
			if (input.PurchaseCategory.Id.HasValue)
			{
				await UpdatePurchaseCategory(input);
			}
			else
			{
				await CreatePurchaseCategory(input);
			}
		}

		public async Task DeletePurchaseCategory(IdInput input)
		{
			await _purchasecategoryRepo.DeleteAsync(input.Id);
		}

		protected virtual async Task UpdatePurchaseCategory(CreateOrUpdatePurchaseCategoryInput input)
		{
			var item = await _purchasecategoryRepo.GetAsync(input.PurchaseCategory.Id.Value);
			var dto = input.PurchaseCategory;
			item.PurchaseCategoryName = dto.PurchaseCategoryName.ToUpper();

			//TODO: SERVICE PurchaseCategory Update Individually

			CheckErrors(await _purchasecategoryManager.CreateSync(item));
		}

		protected virtual async Task CreatePurchaseCategory(CreateOrUpdatePurchaseCategoryInput input)
		{
			var dto = input.PurchaseCategory.MapTo<PurchaseCategory>();
			dto.PurchaseCategoryName = dto.PurchaseCategoryName.ToUpper();

			CheckErrors(await _purchasecategoryManager.CreateSync(dto));
		}

		public async Task<ListResultOutput<PurchaseCategoryListDto>> GetNames()
		{
			var lstPurchaseCategory = await _purchasecategoryRepo.GetAll().ToListAsync();
			return new ListResultOutput<PurchaseCategoryListDto>(lstPurchaseCategory.MapTo<List<PurchaseCategoryListDto>>());
		}




	}
}
