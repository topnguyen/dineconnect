﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.House.Master
{
    public interface IMaterialMenuMappingAppService : IApplicationService
    {
        Task<PagedResultOutput<MaterialMenuMappingViewDto>> GetAll(GetMaterialMenuMappingInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<FileDto> GetNonMappedToExcel();
        Task<GetMaterialMenuMappingForEditOutput> GetMaterialMenuMappingForEdit(MaterialMenuWithLocation input);
        Task CreateOrUpdateMaterialMenuMapping(CreateOrUpdateMaterialMenuMappingInput input);
        Task DeleteMaterialMenuMapping(IdInput input);
        Task<ListResultOutput<MaterialMenuMappingListDto>> GetPosMenuPortionRefIds();
        Task<List<MaterialMenuMappingViewDto>> GetNonMappedMenuList();
        Task<FileDto> GetMappedToExcel();
        Task<PagedResultOutput<MenuMappingWithMaterialCountViewDto>> GetAllWithMenuWithMaterialCount(GetMaterialMenuMappingInput input);
        Task<List<MaterialMenuMappingListDto>> GetMappedMenuList();
        Task<List<ComboboxItemDto>> GetConnectDepartments();
        Task<bool> DeleteMaterialMenuMappingList(DeleteMaterialMenuWithLocationList input);
        Task DeleteMaterialMenuMappingBasedOnMaterial(MaterialMenuMappingListDto input);
    }
}