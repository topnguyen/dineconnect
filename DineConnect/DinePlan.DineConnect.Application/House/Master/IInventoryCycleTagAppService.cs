﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master
{
	public interface IInventoryCycleTagAppService : IApplicationService
	{
		Task<PagedResultOutput<InventoryCycleTagListDto>> GetAll(GetInventoryCycleTagInput inputDto);
		Task<FileDto> GetAllToExcel();
		Task<GetInventoryCycleTagForEditOutput> GetInventoryCycleTagForEdit(NullableIdInput nullableIdInput);
		Task CreateOrUpdateInventoryCycleTag(CreateOrUpdateInventoryCycleTagInput input);
		Task DeleteInventoryCycleTag(IdInput input);
		Task<ListResultOutput<ComboboxItemDto>> GetStockCycleForCombobox();

	}
}
