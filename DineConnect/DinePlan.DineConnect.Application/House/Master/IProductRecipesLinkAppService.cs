﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master
{
    public interface IProductRecipesLinkAppService : IApplicationService
    {
        Task<PagedResultOutput<ProductRecipesLinkListDto>> GetAll(GetProductRecipesLinkInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetProductRecipesLinkForEditOutput> GetProductRecipesLinkForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateProductRecipesLink(CreateOrUpdateProductRecipesLinkInput input);
        Task DeleteProductRecipesLink(IdInput input);

        Task<ListResultOutput<ProductRecipesLinkListDto>> GetIds();
    }
}