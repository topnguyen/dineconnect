﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.DayClose.Dto;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.House.Transaction.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.House.Master
{
    public interface IMaterialAppService : IApplicationService
    {
        Task<PagedResultDto<MaterialListDto>> GetAll(GetMaterialInput inputDto);

        Task<PagedResultDto<MaterialViewDto>> GetView(GetMaterialInput input);

        Task<FileDto> GetAllToExcel(GetMaterialInput input);

        Task<GetMaterialForEditOutput> GetMaterialForEdit(NullableIdInput NullableIdDto);

        Task<EntityDto> CreateOrUpdateMaterial(CreateOrUpdateMaterialInput input);

        Task DeleteMaterial(EntityDto input);

        Task<ListResultDto<MaterialListDto>> GetMaterialNames();

        Task<ListResultDto<MaterialListDto>> GetMaterialRecipeTypesNames();

        Task<ListResultDto<MaterialListDto>> GetMaterialForRecIngredient(NullableIdInput input);

        Task<EntityDto> GetUserSerialNumberMax();

        Task<MaterialLedgerDto> UpdateMaterialLedger(MaterialLedgerDto input);

        Task<List<MaterialViewDto>> GetMaterialWithBarcode(GetMaterialInput input);

        EntityDto GetMaterialIdByName(string materialname);

        Task<PagedResultDto<MaterialViewDto>> GetViewWithOnHand(EntityDto input);

        Task<PagedResultDto<MaterialViewDto>> GetMaterialViewWithOnHandForRecipe(InputLocationAndRecipe input);

        Task<PagedResultDto<MaterialViewDto>> GetMaterialViewWithUom();

        Task<PagedResultDto<MaterialViewDto>> GetViewWithOnHandAndUsagePattern(LocationWithPriceDto input);

        Task<List<MaterialUsagePattern>> MaterialUsagePattern(InputLocationAndMaterialId input);

        Task<PagedResultDto<MaterialViewDto>> GetRecipeViewWithOnHand(EntityDto input);

        Task<DayCloseResultDto> VerifyWipeOutStockForCloseDay(DayCloseDto input);

        Task<ListResultDto<ComboboxItemDto>> GetMaterialForCombobox();

        Task<string> GetMaterialNameForParticularCode(EntityDto input);

        Task<ListResultDto<ComboboxItemDto>> GetMaterialTypeForCombobox(NullableIdInput ninput);

        Task<ListResultDto<ComboboxItemDto>> GetUnitForCombobox();

//        Task<ListResultDto<ComboboxItemDto>> GetMaterialGroupCategoryForCombobox();

        Task<ListResultDto<ComboboxItemDto>> GetUnitForMaterialLinkCombobox(EntityDto input);

        Task<ListResultDto<ComboboxItemDto>> GetMaterialRecipeTypesForCombobox();

        Task<List<MaterialInfoDto>> GetRecipeMaterial(LocationInputDto input);

        Task<List<MaterialMenuMappingWithWipeOut>> GetLiveStockSales(GetMaterialLocationWiseStockInput input);

        Task<List<MaterialMenuMappingWithWipeOut>> GetSalesDetail(DayCloseMaterialDto input);

        Task<GetIssueForStandardRecipe> GetIssueForCategory(GetCategoryMaterial input);

        Task<bool> IsBarcodeExists(string barcode);

        Task<MaterialViewWithBarCode> GetMaterialWithBarcodeListSeparate(GetMaterialInput input);

        Task<bool> CloneMenuItemAsMaterial(MenuItem menuinput);

        Task<BarCodeListDto> GetBarcodesForMaterial();

        Task<MaterialChangeOutputDto> DefaultUnitChangeProcess(MaterialUnitChangeDto input);

        Task<FileDto> GetMappedForParticularMaterialToExcel(GetMaterialMappedList input);

        Task<List<MaterialMenuMappingListDto>> GetMappedMenuForParticularMaterialList(GetMaterialMappedList input);

        Task<ListResultDto<ComboboxItemDto>> GetInventoryStockCycles();

        Task<ListResultDto<ComboboxItemDto>> GetMenuPortionForCombobox(EntityDto input);

        Task<List<DayCloseSyncReportOutputDto>> GetDayCloseSyncReport(DayCloseSyncReportDto input);

        Task<EntityDto> CarryOverUpdateMaterialLedger(MaterialLedgerDto input);

        Task<DayCloseResultDto> IncludeSalesIntoLedgerIfMissing(SalesMissingIntoLedgerDto input);

        Task<PagedResultDto<DayCloseTemplateListDto>> GetAllDayClose(DayCloseDtoView input);

        Task<ListResultDto<ComboboxItemDto>> GetMaterialAveragePriceTagForCombobox();

        Task<PagedResultDto<MaterialViewDto>> GetMaterialViewWithUnitList(GetMaterialInput input);

        Task<PagedResultDto<MaterialViewDto>> GetReservedQuantityFromIssueAndYield(LocationWithMaterialList input);

        Task<DayCloseResultDto> DayCloseInBackGround(DayCloseDto input);
        Task UpdateMaterialAverageDirtyFlag(LocationAndMaterialDto input);
        //Task HelloWorldTest(IdInput input);
        Task ResetDayCloseRequiredInBackGround(LocationInputDto input);
        Task<GetVerifyMaterialLedgerDto> VerifyAndRectifyOpeningStockIssueForGivenDate(VerifyMaterialLedgerClosingStockDto input);
        Task ActivateMaterial(IdInput input);
        Task<UnitConversionEditDto> GetDefaultUnitConversionDataForGivenData(UnitConversionEditDto input);
        //Task<List<UnitConversionEditDto>> GetLinkedUnitCoversionForGivenMaterial(IdInput input);
        Task<List<UnitConversionEditDto>> GetLinkedUnitCoversionForGivenSupplier_Material(SupplierMaterialEditDto input);
        Task<List<UnitConversionEditDto>> GetLinkedUnitCoversionForGivenSupplier_Material_Units(Supplier_Material_Units input);
        Task<MaterialEditDto> GetMaterialForGivenMaterialId(EntityDto input);
        //Task<DefaultUomErrorMessageDto> CheckMaterialDefaultUOM(IdInput input);
        Task<PagedResultDto<SimpleMaterialViewDto>> GetSimpleMaterialList(LocationWithPriceDto input);

        Task<ListResultDto<ComboboxItemDto>> GetMaterialGroupForCombobox();

        Task<ListResultDto<ComboboxItemDto>> GetMaterialGroupCategoryForCombobox(NullableIdInput input);

        Task<List<MaterialLocationWiseStockListDto>> GetMaterialActiveStatusLocationWise(IdInput input);

        Task<MaterialLocationWiseStockListDto> ChangeMaterialActiveStatusInLocation(MaterialLocationWiseStockListDto input);
        Task<UnitConversionListDto> GetConversion(GetConversionBasedOnBaseUnitRefUnit input);
        Task<ListResultDto<ComboboxItemDto>> GetMaterialGroupCategoryForGivenInput(ComboBoxListInput input);
        Task<MaterialLedgerListDto> GetLatestLedgerForGivenLocationMaterialRefId(LocationAndMaterialDto input);

        Task<List<LocationWiseMaterialLedgerListDto>> GetLatestLedgerForGivenMultipleLocationMultipleMaterialRefId(MultipleLocationAndMultipleMaterialDto input);
        Task<ListResultDto<ComboboxItemDto>> GetInvoicePayModeEnumForCombobox();

    }
}