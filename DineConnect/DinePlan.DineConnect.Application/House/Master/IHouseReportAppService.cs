﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Connect.Master.Dtos;

namespace DinePlan.DineConnect.House.Master
{
    public interface IHouseReportAppService : IApplicationService
    {
        Task<GetStockSummaryDtoOutput> GetStockSummary(GetHouseReportInput input);
        Task<GetMaterialRateViewDtoOutput> GetMaterialRateView(GetHouseReportMaterialRateInput input);
        Task<GetSupplierRateViewDtoOutput> GetSupplierRateView(GetHouseReportInput input);
        Task<FileDto> GetCloseStockToExcel(GetStockSummaryDtoOutput input);
        Task<List<MaterialAdjustmentStockDto>> ReadStockFromExcel(string filename);
        Task<GetProductAnalysisViewDtoOutput> GetProductAnalysisView(GetHouseReportInput input);
		Task<FileDto> GetExcelOfProductAnalysis(GetProductAnalysisViewDtoOutput input);
		Task<FileDto> GetPurchaseSummary(InputInvoiceAnalysis input);
		Task<FileDto> GetPurchaseConsolidatedSummary(InputInvoiceAnalysis input);
		Task<FileDto> GetStockTakenReportExcel(LocationWithDate input);
        Task<CostingReportOutPutDto> GetCategoryCostingReport(GetCostReportInput input);
        Task<CostOfSalesReport> GetCostOfSalesReport(GetCostReportInput input);
        Task<ProductionUnitCostingDtoConsolidated> GetProductionUnitCostingReport(GetProductionUnitCostReportInput input);
        Task<List<MaterialIngredientRequiredDto>> GetRawMaterialRequiredForRecipe(GetRawMaterailBasedOnRecipeInput input);
        Task<HouseDashBoardOutputDto> GetHouseDashboardReport(GetHouseDashboardInput input);

        Task<SalesAndExpensesDto> GetPurchaseVsSalesReport(GetHouseDashboardInput input);
        Task<RecipeCostingDtoConsolidated> GetRecipeCostingReport(GetCostReportInput input);
        Task<MenuItemCostingDtoConsolidated> GetMenuItemCostingReport(GetCostReportInput input);
        Task<FileDto> GetProductionUnitCostingReportExcel(ProductionUnitCostingDtoConsolidated input);
        Task<FileDto> GetExcelOfMenuCost(MenuItemCostingDtoConsolidated input);
        Task<FileDto> GetExcelOfCostOfSales(CostOfSalesReport input);
		Task<AdjustmentDtosWithExcelFile> GetClosingStockTakenReportExcel(LocationWithDate input);
		Task<FileDto> GetStockToExcel(GetStockSummaryDtoOutput input);
		Task<FileDto> RateViewToExcel(GetMaterialRateViewDtoOutput input);
		Task<FileDto> SupplierRateViewToExcel(GetSupplierRateViewDtoOutput input);
		Task<List<VarianceReportDto>> GetVarianceReportAll(GetMaterialLedgerInput input);
		Task<FileDto> GetVarianceAllToExcel(GetMaterialLedgerInput input);
		Task<OverAllMaterialWastageReport> GetMaterailWastageAll(InputAdjustmentReport input);
		Task<List<FileDto>> GetMaterialWastageToExcel(InputAdjustmentReport input);
		Task<MenuItemCostingDtoConsolidated> GetMenuCostingReportForGivenMenuPortionIds(GetCostReportInput input);

		Task<List<ComboboxItemDto>> GetEntryTypes();

        Task<AdjustmentDtosWithExcelFile> GetClosingStockVarianceReportExcel(LocationWithDate input);

        Task<FileDto> GetLocationWiseConsolidatedClosingStockVarianceReportExcel(ClosingStockConsolidatedDto input);

        Task<ApiBatchMessageReturn> ApiPhysicalStockBatchFileForGivenDatePeriod(ApiPhysicalStockInputDto input);
        Task<ApiBatchMessageReturn> ApiInventoryTransferBatchFileForGivenDatePeriod(ApiPhysicalStockInputDto input);
        Task<ApiBatchGoodsTextFile> ApiGoodsReceivedReturnedBatchFileForGivenDatePeriod(ApiPhysicalStockInputDto input);
        Task<GetScheduleMaterialList> GetStockTakenScheduledMaterialList(LocationWithDate input);
        Task<FileDownLoadDto> GetClosingStockVarianceBeforeOrAfterDayCloseReportExcel(GetHouseReportInput input);
        Task<ListResultDto<ComboboxItemDto>> GetUnitTypeForCombobox();
        Task<FileDto> GetClosingStockReportGroupOrCategoryOrMaterialWiseForGivenInputToExcel(ClosingStockExportGroupCategoryMaterialDto input);
        Task<LocationWiseClosingStockGroupCateogryMaterialDto> GetClosingStockReportGroupOrCategoryOrMaterialWiseForGivenInput(ClosingStockExportGroupCategoryMaterialDto input);

        Task<FileDto> GetInterTransferReceivedDetailReportMaterialWiseForGivenInputToExcel(ClosingStockExportGroupCategoryMaterialDto input);
        Task<MaterialTrackExportReportDto> GetMaterialTrackReceivedDetailReportForGivenMaterialWise(ClosingStockExportGroupCategoryMaterialDto input);
        Task<FileDto> GetMaterialTrackReceivedDetailReportMaterialWiseForGivenInputToExcel(ClosingStockExportGroupCategoryMaterialDto input);
        Task<GetStockSummaryDtoOutput> GetStockSummary_Version1(GetHouseReportInput input);
        Task<FileDto> GetCloseStockGroupCategoryExcel(ClosingStockExportGroupCategoryMaterialDto input);
        Task<FileDto> GetBackGroundMaterialTrackReceivedDetailReportMaterialWiseForGivenInputToExcel(ClosingStockExportGroupCategoryMaterialDto input);

    }
}

