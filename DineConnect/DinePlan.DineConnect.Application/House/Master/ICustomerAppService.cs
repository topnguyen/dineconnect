﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master
{
    public interface ICustomerAppService : IApplicationService
    {
        Task<PagedResultOutput<CustomerListDto>> GetAll(GetCustomerInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetCustomerForEditOutput> GetCustomerForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateCustomer(CreateOrUpdateCustomerInput input);
        Task DeleteCustomer(IdInput input);
        Task<ListResultOutput<ComboboxItemDto>> GetCustomerForCombobox();
        Task<ListResultOutput<CustomerListDto>> GetNames();
    }
}