﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master
{
    public interface IUnitAppService : IApplicationService
    {
        Task<PagedResultOutput<UnitListDto>> GetAll(GetUnitInput inputDto);

        Task<ListResultOutput<UnitListDto>> GetMaterialUnitNames(int argMaterialCode);
        Task<FileDto> GetAllToExcel(GetUnitInput input);
        Task<GetUnitForEditOutput> GetUnitForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateUnit(CreateOrUpdateUnitInput input);
        Task DeleteUnit(IdInput input);

        Task<ListResultOutput<UnitListDto>> GetNames();
        Task<ListResultOutput<ComboboxItemDto>> GetRefernceUnitName(IdInput input);
        IdInput GetUnitIdByName(string unitname);
        Task CreateUnitsForTenant(int id);
        string GetUnitName(int UnitId);
    }
}

