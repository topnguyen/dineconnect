﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Master.Dtos;

namespace DinePlan.DineConnect.House.Master
{
    public interface IBrandAppService : IApplicationService
    {
        Task<PagedResultOutput<BrandListDto>> GetAll(GetBrandInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetBrandForEditOutput> GetBrandForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateBrand(CreateOrUpdateBrandInput input);
        Task DeleteBrand(IdInput input);

        Task<ListResultOutput<BrandListDto>> GetNames();
    }
}
