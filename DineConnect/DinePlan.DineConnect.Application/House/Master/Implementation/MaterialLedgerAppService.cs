﻿
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Connect.Master;
using Abp.Authorization.Users;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Utility.Exporter;
using DinePlan.DineConnect.Utility;

namespace DinePlan.DineConnect.House.Transaction.Implementation
{
    public class MaterialLedgerAppService : DineConnectAppServiceBase, IMaterialLedgerAppService
    {

        private readonly IMaterialLedgerListExcelExporter _materialledgerExporter;
        private readonly IMaterialLedgerManager _materialledgerManager;
        private readonly IRepository<MaterialLedger> _materialledgerRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<Unit> _unitrepo;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IRepository<MaterialIngredient> _materialingredientRepo;
        private readonly IRepository<MaterialRecipeTypes> _materialrecipetypeRepo;
        private readonly IRepository<MaterialMenuMapping> _materialmenumappingRepo;
        private readonly IRepository<MenuItemPortion> _menuitemportionRepo;
        private readonly IRepository<MenuItem> _menuitemRepo;
        private readonly IExcelExporter _excelExporter;

        public MaterialLedgerAppService(IMaterialLedgerManager materialledgerManager,
            IRepository<MaterialLedger> materialLedgerRepo,
            IMaterialLedgerListExcelExporter materialledgerExporter,
            IRepository<Material> materialRepo,
            IRepository<Unit> unitrepo,
            IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository,
            IRepository<MaterialIngredient> materialingredientRepo,
            IRepository<MaterialRecipeTypes> materialrecipetypeRepo,
            IRepository<MaterialMenuMapping> materialmenumappingRepo,
             IRepository<MenuItemPortion> menuitemportionRepo,
             IExcelExporter excelExporter,
             IRepository<MenuItem> menuitemRepo,
        IRepository<Location> locationRepo)
        {
            _materialledgerManager = materialledgerManager;
            _materialledgerRepo = materialLedgerRepo;
            _materialledgerExporter = materialledgerExporter;
            _materialRepo = materialRepo;
            _locationRepo = locationRepo;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _unitrepo = unitrepo;
            _materialingredientRepo = materialingredientRepo;
            _materialrecipetypeRepo = materialrecipetypeRepo;
            _materialmenumappingRepo = materialmenumappingRepo;
            _menuitemportionRepo = menuitemportionRepo;
            _menuitemRepo = menuitemRepo;
            _excelExporter = excelExporter;
        }

        public async Task<PagedResultOutput<MaterialLedgerViewDto>> GetAll(GetMaterialLedgerInput input)
        {
            //var allLedger = _materialledgerRepo.GetAll().Where(ml => DbFunctions.TruncateTime(ml.LedgerDate) >= DbFunctions.TruncateTime(input.StartDate) && DbFunctions.TruncateTime(ml.LedgerDate) <= DbFunctions.TruncateTime(input.EndDate)).ToList();

            var allMaterialsQb = _materialRepo.GetAll();

            allMaterialsQb = allMaterialsQb.WhereIf(input.MaterialRefId > 0 && input.MaterialRefId != null, m => m.Id == input.MaterialRefId);
            if (input.MaterialRefId.HasValue && input.MaterialRefId > 0)
            {
                if (input.MaterialRefIds == null)
                    input.MaterialRefIds = new List<int>();

                input.MaterialRefIds.Add(input.MaterialRefId.Value);
            }

            if (input.MaterialRefIds != null)
                allMaterialsQb = allMaterialsQb.WhereIf(input.MaterialRefIds != null && input.MaterialRefIds.Any(), t => input.MaterialRefIds.Contains(t.Id));

            //var allMaterial = await allMaterialsQb.ToListAsync();

            List<Location> allLocations = (from loc in _locationRepo.GetAll()
                                           join ou in _userOrganizationUnitRepository.GetAll().Where(a => a.UserId == input.UserId)
                                               on loc.Id equals ou.OrganizationUnitId
                                           select loc).WhereIf(input.LocationRefId > 0 && input.LocationRefId != null, l => l.Id == input.LocationRefId).ToList();


            var allItems = (from matledger in _materialledgerRepo.GetAll()
                .Where(ml => DbFunctions.TruncateTime(ml.LedgerDate) >= DbFunctions.TruncateTime(input.StartDate) && DbFunctions.TruncateTime(ml.LedgerDate) <= DbFunctions.TruncateTime(input.EndDate))
                            join mat in allMaterialsQb on matledger.MaterialRefId equals mat.Id
                            join loc in _locationRepo.GetAll().WhereIf(input.LocationRefId > 0 && input.LocationRefId != null, l => l.Id == input.LocationRefId)
                            on matledger.LocationRefId equals loc.Id
                            join un in _unitrepo.GetAll() on mat.DefaultUnitId equals un.Id
                            join uiss in _unitrepo.GetAll() on mat.IssueUnitId equals uiss.Id
                            select new MaterialLedgerViewDto
                            {
                                LedgerDate = matledger.LedgerDate,
                                LocationRefId = matledger.LocationRefId,
                                LocationRefName = loc.Name,
                                MaterialRefId = matledger.MaterialRefId,
                                MaterialRefName = mat.MaterialName,
                                DefaultUnitRefId = mat.DefaultUnitId,
                                Uom = un.Name,
                                TransactionUnitRefId = mat.IssueUnitId,
                                TransactionUnitRefName = uiss.Name,
                                OpenBalance = matledger.OpenBalance,
                                Received = matledger.Received,
                                Issued = matledger.Issued,
                                Sales = matledger.Sales,
                                TransferIn = matledger.TransferIn,
                                TransferOut = matledger.TransferOut,
                                Damaged = matledger.Damaged,
                                SupplierReturn = matledger.SupplierReturn,
                                ExcessReceived = matledger.ExcessReceived,
                                Shortage = matledger.Shortage,
                                Return = matledger.Return,
                                ClBalance = matledger.ClBalance,
                                OverAllIssued = matledger.Issued + matledger.Sales + matledger.TransferOut + matledger.Damaged + matledger.Shortage + matledger.SupplierReturn,
                                OverAllReceived = matledger.Received + matledger.TransferIn + matledger.ExcessReceived + matledger.Return,

                            }).WhereIf(!input.Filter.IsNullOrEmpty(), p => p.MaterialRefName.Contains(input.Filter));

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                //.PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<MaterialLedgerViewDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<MaterialLedgerViewDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetMaterialLedgerInput input)
        {
            input.MaxResultCount = AppConsts.MaxPageSize;

            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<MaterialLedgerViewDto>>();
            return _materialledgerExporter.ExportToFile(allListDtos);
        }

        public async Task<GetMaterialLedgerForEditOutput> GetMaterialLedgerForEdit(NullableIdInput input)
        {
            MaterialLedgerEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _materialledgerRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<MaterialLedgerEditDto>();
            }
            else
            {
                editDto = new MaterialLedgerEditDto();
            }

            return new GetMaterialLedgerForEditOutput
            {
                MaterialLedger = editDto
            };
        }

        public async Task CreateOrUpdateMaterialLedger(CreateOrUpdateMaterialLedgerInput input)
        {
            if (input.MaterialLedger.Id.HasValue)
            {
                //  await UpdateMaterialLedger(input);
            }
            else
            {
                await CreateMaterialLedger(input);
            }
        }

        public async Task DeleteMaterialLedger(IdInput input)
        {
            await _materialledgerRepo.DeleteAsync(input.Id);
        }

        //protected virtual async Task UpdateMaterialLedger(CreateOrUpdateMaterialLedgerInput input)
        //{
        //    var item = await _materialledgerRepo.GetAsync(input.MaterialLedger.Id.Value);
        //    var dto = input.MaterialLedger;

        //    //TODO: SERVICE MaterialLedger Update Individually
        //    item.LocationRefId = dto.LocationRefId;
        //    item.MaterialRefId = dto.MaterialRefId;
        //    item.LedgerDate = dto.LedgerDate;
        //    item.Return = dto.Return;
        //    item.SupplierReturn = dto.SupplierReturn;
        //    item.TransferIn = dto.TransferIn;
        //    item.TransferOut = dto.TransferOut;
        //    item.Shortage = dto.Shortage;
        //    item.Received = dto.Received;
        //    item.OpenBalance = dto.OpenBalance;
        //    item.ExcessReceived = dto.ExcessReceived;
        //    item.ClBalance = dto.ClBalance;
        //    item.Damaged = dto.Damaged;
        //    item.Issued = dto.Issued;
        //    item.Sales = dto.Sales;

        //    CheckErrors(await _materialledgerManager.CreateSync(item));
        //}

        protected virtual async Task CreateMaterialLedger(CreateOrUpdateMaterialLedgerInput input)
        {
            var dto = input.MaterialLedger.MapTo<MaterialLedger>();

            CheckErrors(await _materialledgerManager.CreateSync(dto));
        }



        public async Task<List<MaterialLinkWithOtherMaterialViewDto>> GetMaterialLinkWithOtherMaterialsList(IdInput input)
        {
            var materialList = await _materialRepo.GetAll().Where(t => t.Id == input.Id).ToListAsync();

            List<MaterialLinkWithOtherMaterialViewDto> output = new List<MaterialLinkWithOtherMaterialViewDto>();

            var unitList = await _unitrepo.GetAll().ToListAsync();

            foreach (var mat in materialList)
            {
                var materialIngrList = await _materialingredientRepo.GetAll().Where(t => t.MaterialRefId == mat.Id).ToListAsync();

                foreach (var ingr in materialIngrList)
                {
                    var recipe = await _materialrecipetypeRepo.FirstOrDefaultAsync(t => t.MaterailRefId == ingr.RecipeRefId);
                    var recMaterial = await _materialRepo.FirstOrDefaultAsync(t => t.Id == recipe.MaterailRefId);

                    MaterialLinkWithOtherMaterialViewDto newDto = new MaterialLinkWithOtherMaterialViewDto
                    {
                        MaterialRefId = mat.Id,
                        MaterialRefName = mat.MaterialName,
                        RecipeTypeRefId = recMaterial.MaterialTypeId,
                        RecipeTypeRefName = "",
                        RecipeRefId = recMaterial.Id,
                        RecipeRefName = recMaterial.MaterialName,
                        MaterialUsageQuantity = ingr.MaterialUsedQty,
                        Uom = unitList.FirstOrDefault(t => t.Id == mat.DefaultUnitId).Name,
                        OutputQuantity = recipe.PrdBatchQty,
                        OutputUom = unitList.FirstOrDefault(t => t.Id == ingr.UnitRefId).Name,
                    };
                    if (recMaterial.MaterialTypeId == (int)MaterialType.RAW)
                        newDto.RecipeTypeRefName = L("RAW");
                    else if (recMaterial.MaterialTypeId == (int)MaterialType.SEMI)
                        newDto.RecipeTypeRefName = L("SEMI");


                    output.Add(newDto);

                }

                var menuList = await _materialmenumappingRepo.GetAll().Where(t => t.MaterialRefId == mat.Id).ToListAsync();
                var rsLocation = await _locationRepo.GetAllListAsync();

                foreach (var mm in menuList)
                {
                    var menuportion = await _menuitemportionRepo.FirstOrDefaultAsync(t => t.Id == mm.PosMenuPortionRefId);
                    var menuitem = await _menuitemRepo.FirstOrDefaultAsync(t => t.Id == menuportion.MenuItemId);

                    string locationRefName = L("All");
                    if (mm.LocationRefId.HasValue)
                    {
                        var loc = rsLocation.FirstOrDefault(t => t.Id == mm.LocationRefId);
                        locationRefName = loc.Code;
                    }

                    MaterialLinkWithOtherMaterialViewDto newDto = new MaterialLinkWithOtherMaterialViewDto
                    {
                        LocationRefId = mm.LocationRefId,
                        LocationRefName = locationRefName,
                        MaterialRefId = mat.Id,
                        MaterialRefName = mat.MaterialName,
                        RecipeTypeRefId = 99,
                        RecipeTypeRefName = "Menu",
                        RecipeRefId = menuportion.Id,
                        RecipeRefName = string.Concat(menuitem.Name, "-", menuportion.Name),
                        MaterialUsageQuantity = mm.PortionQty,
                        Uom = unitList.FirstOrDefault(t => t.Id == mm.PortionUnitId).Name,
                        OutputQuantity = mm.MenuQuantitySold,
                    };
                    output.Add(newDto);
                }

            }

            return output;
        }

        public async Task<FileDto> GetMaterialLinkWithOtherMateriaalsToExcel(IdInput input)
        {
            var allListDtos = await GetMaterialLinkWithOtherMaterialsList(input);

            var baseE = new BaseExportObject()
            {
                ExportObject = allListDtos,
                ColumnNames = new string[] { "LocationRefName", "MaterialRefName", "MaterialUsageQuantity", "Uom", "RecipeTypeRefName", "RecipeRefName", "OutputQuantity", "OutputUom" },
                ColumnDisplayNames = new string[] { "Location", "Material", "Quantity Used", "UOM", "Output Type", "RecipeName", "Output Quantity", "Output UOM" }
            };

            return _excelExporter.ExportToFile(baseE, L("MaterialUsageInfo"));
        }

    }
}