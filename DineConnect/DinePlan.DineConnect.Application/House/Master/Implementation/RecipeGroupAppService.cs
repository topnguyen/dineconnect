﻿
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;


namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class RecipeGroupAppService : DineConnectAppServiceBase, IRecipeGroupAppService
    {

        private readonly IRecipeGroupListExcelExporter _recipegroupExporter;
        private readonly IRecipeGroupManager _recipegroupManager;
        private readonly IRepository<RecipeGroup> _recipegroupRepo;

        public RecipeGroupAppService(IRecipeGroupManager recipegroupManager,
            IRepository<RecipeGroup> recipeGroupRepo,
            IRecipeGroupListExcelExporter recipegroupExporter)
        {
            _recipegroupManager = recipegroupManager;
            _recipegroupRepo = recipeGroupRepo;
            _recipegroupExporter = recipegroupExporter;

        }

        public async Task<PagedResultOutput<RecipeGroupListDto>> GetAll(GetRecipeGroupInput input)
        {
            var allItems = _recipegroupRepo.GetAll();
            if (!string.IsNullOrEmpty(input.Operation) && input.Operation.Equals("SEARCH"))
            {
                allItems = _recipegroupRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.RecipeGroupName.Equals(input.Filter)
               );
            }
            else
            {
                allItems = _recipegroupRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.RecipeGroupName.Contains(input.Filter)
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<RecipeGroupListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<RecipeGroupListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _recipegroupRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<RecipeGroupListDto>>();
            return _recipegroupExporter.ExportToFile(allListDtos);
        }

        public async Task<GetRecipeGroupForEditOutput> GetRecipeGroupForEdit(NullableIdInput input)
        {
            RecipeGroupEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _recipegroupRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<RecipeGroupEditDto>();
            }
            else
            {
                editDto = new RecipeGroupEditDto();
            }

            return new GetRecipeGroupForEditOutput
            {
                RecipeGroup = editDto
            };
        }

        public async Task<IdInput> CreateOrUpdateRecipeGroup(CreateOrUpdateRecipeGroupInput input)
        {
            if (input.RecipeGroup.Id.HasValue)
            {
               return await UpdateRecipeGroup(input);
            }
            else
            {
               return await CreateRecipeGroup(input);
            }
        }

        public async Task DeleteRecipeGroup(IdInput input)
        {
            await _recipegroupRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task<IdInput> UpdateRecipeGroup(CreateOrUpdateRecipeGroupInput input)
        {
            var item = await _recipegroupRepo.GetAsync(input.RecipeGroup.Id.Value);
            var dto = input.RecipeGroup;

            //TODO: SERVICE RecipeGroup Update Individually
            item.RecipeGroupName = dto.RecipeGroupName;
            item.RecipeGroupShortName = dto.RecipeGroupShortName;

            CheckErrors(await _recipegroupManager.CreateSync(item));

            return new IdInput
            {
                Id = input.RecipeGroup.Id.Value
            };
        }

        protected virtual async Task<IdInput> CreateRecipeGroup(CreateOrUpdateRecipeGroupInput input)
        {
            var dto = input.RecipeGroup.MapTo<RecipeGroup>();

            CheckErrors(await _recipegroupManager.CreateSync(dto));

            int returnId = dto.Id;

            return new IdInput
            {
                Id = returnId
            };

        }

        public async Task<ListResultOutput<RecipeGroupListDto>> GetRecipeGroupNames()
        {
            var lstRecipeGroup = await _recipegroupRepo.GetAll().ToListAsync();
            return new ListResultOutput<RecipeGroupListDto>(lstRecipeGroup.MapTo<List<RecipeGroupListDto>>());
        }
    }
}
