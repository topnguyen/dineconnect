﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Common.Dto;
using Abp.MultiTenancy;
using Abp.Domain.Uow;
using DinePlan.DineConnect.Connect.Master;
using Abp.Configuration;
using DinePlan.DineConnect.Configuration;
using System;
using Abp.UI;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Connect.Master.Dtos;

namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class SupplierMaterialAppService : DineConnectAppServiceBase, ISupplierMaterialAppService
    {

        private readonly ISupplierMaterialListExcelExporter _suppliermaterialExporter;
        private readonly ISupplierMaterialManager _suppliermaterialManager;
        private readonly IRepository<SupplierMaterial> _suppliermaterialRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<MaterialGroupCategory> _materialgroupcategoryRepo;
        private readonly IRepository<MaterialGroup> _materialgroupRepo;
        private readonly IRepository<Supplier> _supplierRepo;
        private readonly IRepository<MaterialLocationWiseStock> _materiallocationwisestockRepo;
        private readonly IRepository<PurchaseOrderDetail> _purchaseorderdetailRepo;
        private readonly IRepository<PurchaseOrder> _purchaseorderRepo;
        private readonly IRepository<Unit> _unitRepo;
        private readonly IRepository<UnitConversion> _unitConversionRepo;
        private readonly IRepository<Tax> _taxRepo;
        private readonly IRepository<TaxTemplateMapping> _taxtemplatemappingRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly SettingManager _settingManager;
        private readonly IRepository<MaterialLedger> _materialLedgerRepo;
        private readonly IRepository<Customer> _customerRepo;
        private readonly IRepository<CustomerMaterial> _customermaterialRepo;
        private readonly IRepository<SalesTax> _salestaxRepo;
        private readonly IRepository<SalesTaxTemplateMapping> _salestaxtemplatemappingRepo;
        private readonly IRepository<SalesOrder> _salesorderRepo;
        private readonly IRepository<SalesOrderDetail> _salesorderdetailRepo;
        private readonly IRepository<CustomerTagDefinition> _customerTagDefinitionRepo;
        private readonly IRepository<CustomerTagMaterialPrice> _customerTagMaterialpriceRepo;
        private readonly IUnitConversionAppService _unitConversionAppService;
        private readonly IHouseReportAppService _housereportAppService;
        private readonly IMaterialAppService _materialAppService;

        public SupplierMaterialAppService(ISupplierMaterialManager suppliermaterialManager,
            IRepository<SupplierMaterial> supplierMaterialRepo,
            ISupplierMaterialListExcelExporter suppliermaterialExporter, IRepository<CustomerMaterial> customermaterialRepo,
            IRepository<Customer> customerRepo
           , IRepository<Material> material, IRepository<Supplier> supplier,
            IRepository<MaterialLocationWiseStock> materiallocationwisestockRepo,
            IRepository<PurchaseOrderDetail> purchaseorderdetailRepo,
            IRepository<PurchaseOrder> purchaseorderRepo,
            IRepository<Unit> unitRepo,
            IRepository<Tax> taxRepo, IRepository<SalesTax> salestaxRepo,
            IRepository<TaxTemplateMapping> taxtemplatemappingRepo, IRepository<SalesTaxTemplateMapping> salestaxtemplatemappingRepo,
            IRepository<MaterialGroupCategory> materialgroupcategoryRepo,
             SettingManager settingManager,
             IRepository<MaterialGroup> materialgroupRepo,
             IRepository<MaterialLedger> materialLedgerRepo,
            IRepository<Location> locationRepo,
            IRepository<UnitConversion> unitConversionRepo, IRepository<SalesOrder> salesorderRepo,
          IRepository<SalesOrderDetail> salesorderdetailRepo,
          IRepository<CustomerTagDefinition> customerTagDefinitionRepo,
          IRepository<CustomerTagMaterialPrice> customerTagMaterialpriceRepo,
          IUnitConversionAppService unitConversionAppService,
          IHouseReportAppService houseReportAppService,
          IMaterialAppService materialAppService
          )
        {
            _suppliermaterialManager = suppliermaterialManager;
            _suppliermaterialRepo = supplierMaterialRepo;
            _suppliermaterialExporter = suppliermaterialExporter;
            _materialRepo = material;
            _supplierRepo = supplier;
            _customerRepo = customerRepo;
            _customermaterialRepo = customermaterialRepo;
            _materiallocationwisestockRepo = materiallocationwisestockRepo;
            _purchaseorderdetailRepo = purchaseorderdetailRepo;
            _purchaseorderRepo = purchaseorderRepo;
            _unitRepo = unitRepo;
            _taxRepo = taxRepo;
            _salestaxRepo = salestaxRepo;
            _taxtemplatemappingRepo = taxtemplatemappingRepo;
            _salestaxtemplatemappingRepo = salestaxtemplatemappingRepo;
            _materialgroupcategoryRepo = materialgroupcategoryRepo;
            _locationRepo = locationRepo;
            _settingManager = settingManager;
            _materialLedgerRepo = materialLedgerRepo;
            _materialgroupRepo = materialgroupRepo;
            _unitConversionRepo = unitConversionRepo;
            _salesorderRepo = salesorderRepo;
            _salesorderdetailRepo = salesorderdetailRepo;
            _customerTagDefinitionRepo = customerTagDefinitionRepo;
            _customerTagMaterialpriceRepo = customerTagMaterialpriceRepo;
            _unitConversionAppService = unitConversionAppService;
            _housereportAppService = houseReportAppService;
            _materialAppService = materialAppService;
        }

        public async Task<PagedResultOutput<SupplierMaterialListDto>> GetAll(GetSupplierMaterialInput input)
        {
            var allItems = _suppliermaterialRepo.GetAll();
            if (!string.IsNullOrEmpty(input.Operation) && input.Operation.Equals("SEARCH"))
            {
                allItems = _suppliermaterialRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Id.ToString().Equals(input.Filter)
               );
            }
            else
            {
                allItems = _suppliermaterialRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Id.ToString().Contains(input.Filter)
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<SupplierMaterialListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<SupplierMaterialListDto>(
                allItemCount,
                allListDtos
                );
        }
        public async Task<PagedResultOutput<SupplierMaterialViewDto>> GetView(GetSupplierMaterialInput input)
        {

            var allItems = (from sm in _suppliermaterialRepo.GetAll()
                            join s in _supplierRepo.GetAll().WhereIf(input.SupplierRefId.HasValue, t => t.Id == input.SupplierRefId) on
                            sm.SupplierRefId equals s.Id
                            join mt in _materialRepo.GetAll() on
                            sm.MaterialRefId equals mt.Id
                            join un in _unitRepo.GetAll() on sm.UnitRefId equals un.Id
                            select new SupplierMaterialViewDto()
                            {
                                Id = sm.Id,

                                SupplierRefName = s.SupplierName,
                                MaterialRefName = mt.MaterialName,
                                Barcode = mt.Barcode,
                                MaterialPrice = sm.MaterialPrice,
                                MinimumOrderQuantity = sm.MinimumOrderQuantity,
                                MaximumOrderQuantity = sm.MaximumOrderQuantity,
                                UnitRefId = sm.UnitRefId,
                                UnitRefName = un.Name,
                                LastQuoteRefNo = sm.LastQuoteRefNo,
                                LastQuoteDate = sm.LastQuoteDate,
                                YieldPercentage = sm.YieldPercentage,
                                Hsncode = mt.Hsncode,
                                SupplierMaterialAliasName = sm.SupplierMaterialAliasName
                            }).WhereIf(
                               !input.Filter.IsNullOrEmpty(),
                               p => p.SupplierRefName.Contains(input.Filter)

              );


            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<SupplierMaterialViewDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<SupplierMaterialViewDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetSupplierMaterialInput input)
        {
            input.MaxResultCount = AppConsts.MaxPageSize;
            input.Sorting = "SupplierRefName";
            var allList = await GetView(input);
            var allListDtos = allList.Items.MapTo<List<SupplierMaterialViewDto>>();
            return _suppliermaterialExporter.ExportToFile(allListDtos);
        }

        public async Task<FileDto> PurchaseProjectionToExcel(InputPurchaseProjection input)
        {
            var allListDtos = await GetPurchaseProjection(input);
            PurchaseProjectionDto rdto = new PurchaseProjectionDto();
            rdto.ProjecttionData = allListDtos;
            var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
            rdto.FileName = location.Name + "_" + input.StartDate.ToString("ddMMMyyyy") + "_" + input.EndDate.ToString("ddMMMyyyy") + L("PurchaseProjection");
            return _suppliermaterialExporter.PurchaseProjectionExportToFile(rdto);
        }


        public async Task<GetSupplierMaterialForEditOutput> GetSupplierMaterialForEdit(SupplierWithLocation input)
        {
            List<SupplierMaterialEditDto> editDto;
            IQueryable<SupplierMaterial> supplierMaterials;

            if (input.SelectedLocations == null || input.SelectedLocations.Count == 0)
            {
                supplierMaterials = _suppliermaterialRepo.GetAll().Where(t => t.SupplierRefId == input.SupplierRefId && t.LocationRefId == input.LocationRefId);
            }
            else
            {
                List<int> locationRefIds = input.SelectedLocations.Select(t => t.Id).ToList();
                supplierMaterials = _suppliermaterialRepo.GetAll().Where(t => t.SupplierRefId == input.SupplierRefId && t.LocationRefId.HasValue
                    && locationRefIds.Contains(t.LocationRefId.Value));
            }

            editDto = await (from supmat in supplierMaterials
                             join mat in _materialRepo.GetAll() on supmat.MaterialRefId equals mat.Id
                             join un in _unitRepo.GetAll() on supmat.UnitRefId equals un.Id
                             select new SupplierMaterialEditDto
                             {
                                 MaterialRefId = supmat.MaterialRefId,
                                 MaterialRefName = mat.MaterialName,
                                 Uom = un.Name,
                                 SupplierRefId = supmat.SupplierRefId,
                                 MaterialPrice = supmat.MaterialPrice,
                                 UnitRefId = supmat.UnitRefId,
                                 MinimumOrderQuantity = supmat.MinimumOrderQuantity,
                                 MaximumOrderQuantity = supmat.MaximumOrderQuantity,
                                 LastQuoteDate = supmat.LastQuoteDate,
                                 LastQuoteRefNo = supmat.LastQuoteRefNo,
                                 YieldPercentage = supmat.YieldPercentage,
                                 SupplierMaterialAliasName = supmat.SupplierMaterialAliasName
                             }
                       ).ToListAsync();


            return new GetSupplierMaterialForEditOutput
            {
                SupplierMaterial = editDto
            };
        }

        public async Task CreateOrUpdateSupplierMaterial(CreateOrUpdateSupplierMaterialInput input)
        {
            await UpdateSupplierMaterial(input);
        }

        public async Task DeleteSupplierMaterial(SupplierWithLocation input)
        {
            var exists = await _suppliermaterialRepo.FirstOrDefaultAsync(t => t.LocationRefId == input.LocationRefId && t.SupplierRefId == input.SupplierRefId && t.MaterialRefId == input.MaterialRefId);
            if (exists != null)
                await _suppliermaterialRepo.DeleteAsync(exists.Id);
        }

        public async Task DeleteSupplierMaterialList(DeleteSupplierWithLocationList input)
        {
            foreach (var lst in input.SupplierWithLocations)
            {
                var exists = await _suppliermaterialRepo.FirstOrDefaultAsync(t => t.LocationRefId == lst.LocationRefId && t.SupplierRefId == lst.SupplierRefId && t.MaterialRefId == lst.MaterialRefId);
                if (exists != null)
                    await _suppliermaterialRepo.DeleteAsync(exists.Id);
            }
        }

        protected virtual async Task UpdateSupplierMaterial(CreateOrUpdateSupplierMaterialInput input)
        {
            int supplierRefid = 0;
            int? locationRefId = null;

            List<int> materialsToBeRetained = new List<int>();

            if (input.SupplierMaterial != null && input.SupplierMaterial.Count > 0)
            {
                supplierRefid = input.SupplierMaterial[0].SupplierRefId;

                foreach (var supmat in input.SupplierMaterial)
                {
                    locationRefId = supmat.LocationRefId;
                    int materialrefid = supmat.MaterialRefId;
                    materialsToBeRetained.Add(materialrefid);

                    var editDetailDto = await _suppliermaterialRepo.FirstOrDefaultAsync(u => u.SupplierRefId == supmat.SupplierRefId && u.MaterialRefId == supmat.MaterialRefId && u.LocationRefId == supmat.LocationRefId);

                    if (editDetailDto == null)  //  Add New Material
                    {
                        SupplierMaterial newDto = new SupplierMaterial();

                        newDto.MaterialRefId = supmat.MaterialRefId;
                        newDto.SupplierRefId = supmat.SupplierRefId;
                        newDto.MaterialPrice = supmat.MaterialPrice;
                        newDto.MinimumOrderQuantity = supmat.MinimumOrderQuantity;
                        newDto.MaximumOrderQuantity = supmat.MaximumOrderQuantity;
                        newDto.UnitRefId = supmat.UnitRefId;
                        newDto.LastQuoteDate = supmat.LastQuoteDate;
                        newDto.LastQuoteRefNo = supmat.LastQuoteRefNo;
                        newDto.YieldPercentage = supmat.YieldPercentage;
                        newDto.LocationRefId = supmat.LocationRefId;
                        newDto.SupplierMaterialAliasName = supmat.SupplierMaterialAliasName;
                        CheckErrors(await _suppliermaterialManager.CreateSync(newDto));
                    }
                    else
                    {
                        editDetailDto.SupplierRefId = supmat.SupplierRefId;
                        editDetailDto.MaterialRefId = supmat.MaterialRefId;
                        editDetailDto.MaterialPrice = supmat.MaterialPrice;
                        editDetailDto.MinimumOrderQuantity = supmat.MinimumOrderQuantity;
                        editDetailDto.MaximumOrderQuantity = supmat.MaximumOrderQuantity;
                        editDetailDto.UnitRefId = supmat.UnitRefId;
                        editDetailDto.LastQuoteRefNo = supmat.LastQuoteRefNo;
                        editDetailDto.LastQuoteDate = supmat.LastQuoteDate;
                        editDetailDto.YieldPercentage = supmat.YieldPercentage;
                        editDetailDto.LocationRefId = supmat.LocationRefId;
                        editDetailDto.SupplierMaterialAliasName = supmat.SupplierMaterialAliasName;
                        CheckErrors(await _suppliermaterialManager.CreateSync(editDetailDto));
                    }
                }
            }

            //var delList = await _suppliermaterialRepo.GetAll().Where(a => a.SupplierRefId == supplierRefid && a.LocationRefId == locationRefId && !materialsToBeRetained.Contains(a.MaterialRefId)).ToListAsync();

            //foreach (var a in delList)
            //{
            //    _suppliermaterialRepo.Delete(a.Id);
            //}

        }

        public async Task<PagedResultOutput<NameValueDto>> FindMaterials(FindUsersInput input)
        {
            if (AbpSession.MultiTenancySide == MultiTenancySides.Host && input.TenantId.HasValue)
            {
                CurrentUnitOfWork.SetFilterParameter(AbpDataFilters.MayHaveTenant, AbpDataFilters.Parameters.TenantId, input.TenantId.Value);
            }
            var query = await (from mat in _materialRepo.GetAll()
                               select new NameValueDto()
                               {
                                   Name = mat.MaterialName,
                                   Value = mat.Id.ToString()
                               }).ToListAsync();

            var allItemCount = query.Count();

            return new PagedResultOutput<NameValueDto>(
                allItemCount,
                query
                );
        }

        public async Task<bool> isInSupplierMaterial(CheckSupplierMaterial input)
        {
            return await UserManager.IsInOrganizationUnitAsync(input.MaterialRefId, input.SupplierRefId);
        }

        public async Task<ListResultOutput<SupplierMaterialViewDto>> GetMaterialBasedOnSupplierWithPrice(SentLocationAndSupplier input)
        {
            var supplier = await _supplierRepo.FirstOrDefaultAsync(t => t.Id == input.SupplierRefId);

            if (supplier == null)
            {
                return new ListResultOutput<SupplierMaterialViewDto>();
            }

            List<SupplierMaterialViewDto> lstMaterial;
            var rsSupplierMaterials = await _suppliermaterialRepo.GetAllListAsync(sp => sp.SupplierRefId == input.SupplierRefId);

            if (supplier.LoadOnlyLinkedMaterials == true || input.LoadOnlyLinkedMaterials == true)
            {
                if (input.MaterialRefIds != null && input.MaterialRefIds.Count > 0)
                    rsSupplierMaterials = rsSupplierMaterials.Where(t => input.MaterialRefIds.Contains(t.MaterialRefId)).ToList();

                var materialRefIds = rsSupplierMaterials.Select(t => t.MaterialRefId).ToList();
                lstMaterial = await (from mat in _materialRepo.GetAll().Where(t => materialRefIds.Contains(t.Id))
                                     join matloc in _materiallocationwisestockRepo.GetAll()
                                     .Where(t => t.IsActiveInLocation == true && t.LocationRefId == input.LocationRefId
                                        && materialRefIds.Contains(t.MaterialRefId))
                                     on mat.Id equals matloc.MaterialRefId
                                     join un in _unitRepo.GetAll()
                                     on mat.DefaultUnitId equals un.Id
                                     join matgroupCat in _materialgroupcategoryRepo.GetAll()
                                     on mat.MaterialGroupCategoryRefId equals matgroupCat.Id


                                     select new SupplierMaterialViewDto
                                     {
                                         Barcode = mat.Barcode,
                                         MaterialRefId = mat.Id,
                                         MaterialRefName = mat.MaterialName,
                                         MaterialGroupRefId = matgroupCat.MaterialGroupRefId,
                                         MaterialGroupCategoryRefId = mat.MaterialGroupCategoryRefId,
                                         YieldPercentage = mat.YieldPercentage,
                                         CurrentInHand = matloc.CurrentInHand,
                                         AlreadyOrderedQuantity = 0,
                                         DefaultUnitId = mat.DefaultUnitId,
                                         Uom = un.Name,
                                         UnitRefNameForMinOrder = un.Name,
                                         UnitRefName = un.Name,
                                         IsQuoteNeededForPurchase = mat.IsQuoteNeededForPurchase,
                                         IsFractional = mat.IsFractional,
                                         IsBranded = mat.IsBranded,
                                         Hsncode = mat.Hsncode
                                     }).Distinct().ToListAsync();

            }
            else
            {
                lstMaterial = await (from mat in _materialRepo.GetAll()
                                     join matloc in _materiallocationwisestockRepo.GetAll().Where(t => t.IsActiveInLocation == true && t.LocationRefId == input.LocationRefId)
                                     on mat.Id equals matloc.MaterialRefId
                                     join un in _unitRepo.GetAll()
                                     on mat.DefaultUnitId equals un.Id
                                     join matgroupCat in _materialgroupcategoryRepo.GetAll()
                                     on mat.MaterialGroupCategoryRefId equals matgroupCat.Id

                                     select new SupplierMaterialViewDto
                                     {
                                         Barcode = mat.Barcode,
                                         MaterialRefId = mat.Id,
                                         MaterialRefName = mat.MaterialName,
                                         SupplierMaterialAliasName = "",
                                         MaterialGroupRefId = matgroupCat.MaterialGroupRefId,
                                         MaterialGroupCategoryRefId = mat.MaterialGroupCategoryRefId,
                                         UnitRefId = mat.DefaultUnitId,
                                         YieldPercentage = mat.YieldPercentage,
                                         CurrentInHand = matloc.CurrentInHand,
                                         AlreadyOrderedQuantity = 0,
                                         DefaultUnitId = mat.DefaultUnitId,
                                         Uom = un.Name,
                                         UnitRefNameForMinOrder = un.Name,
                                         UnitRefName = un.Name,
                                         IsQuoteNeededForPurchase = mat.IsQuoteNeededForPurchase,
                                         IsFractional = mat.IsFractional,
                                         IsBranded = mat.IsBranded,
                                         Hsncode = mat.Hsncode
                                     }).Distinct().ToListAsync();
            }

            int[] matArray = lstMaterial.Select(t => t.MaterialRefId).ToArray();


            List<PurchaseOrderDetailViewDto> existPurchaseDetail = new List<PurchaseOrderDetailViewDto>();
            List<PurchaseOrderListDto> existPurchaseOrder = new List<PurchaseOrderListDto>();
            if (input.PurchaseReturnFlag == false)
            {
                //Setting Required For Delivery Date Expected 
                DateTime minDeliveryDate = DateTime.Now.Date.AddDays(-30);
                DateTime maxDeliveryDate = DateTime.Now.Date.AddDays(2);
                var tempexistPurchaseDetail = await (from poDet in _purchaseorderdetailRepo.GetAll()
                                       .Where(t => matArray.Contains(t.MaterialRefId) && t.QtyOrdered > t.QtyReceived)
                                       .WhereIf(input.PoRefId != null, t => t.PoRefId != input.PoRefId)
                                                     join poMas in _purchaseorderRepo.GetAll()
                                                     .Where(t => t.LocationRefId == input.LocationRefId && (t.PoCurrentStatus == "P" || t.PoCurrentStatus == "A" || t.PoCurrentStatus == "H") && DbFunctions.TruncateTime(t.DeliveryDateExpected) >= DbFunctions.TruncateTime(minDeliveryDate) && DbFunctions.TruncateTime(t.DeliveryDateExpected) <= DbFunctions.TruncateTime(maxDeliveryDate))
                                                     on poDet.PoRefId equals poMas.Id
                                                     select poDet).ToListAsync();
                existPurchaseDetail = tempexistPurchaseDetail.MapTo<List<PurchaseOrderDetailViewDto>>();

                List<int> PoRefIds = existPurchaseDetail.Select(t => t.PoRefId).ToList();
                var tempPoMaster = await _purchaseorderRepo.GetAllListAsync(t => PoRefIds.Contains(t.Id));
                existPurchaseOrder = tempPoMaster.MapTo<List<PurchaseOrderListDto>>();
            }

            //List<PurchaseOrderDetail> existPurchaseDetail = new List<PurchaseOrderDetail>();

            //if (input.PurchaseReturnFlag == false)
            //{
            //    existPurchaseDetail = await (from poDet in _purchaseorderdetailRepo.GetAll()
            //                           .Where(t => matArray.Contains(t.MaterialRefId) && t.QtyOrdered > t.QtyReceived)
            //                           .WhereIf(input.PoRefId != null, t => t.PoRefId != input.PoRefId)
            //                                 join poMas in _purchaseorderRepo.GetAll()
            //                                 .Where(t => t.PoCurrentStatus == "P" || t.PoCurrentStatus == "A"
            //                                 || t.PoCurrentStatus == "H")
            //                                 on poDet.PoRefId equals poMas.Id
            //                                 select poDet).ToListAsync();
            //}

            var supplierRefIds = rsSupplierMaterials.Select(t => t.SupplierRefId).ToList();
            supplierRefIds.AddRange(existPurchaseOrder.Select(t => t.SupplierRefId).ToList());
            var rsSupplier = await _supplierRepo.GetAllListAsync(t => supplierRefIds.Contains(t.Id));

            var rsTaxes = await _taxRepo.GetAll().ToListAsync();
            var rsTaxTemplates = await _taxtemplatemappingRepo.GetAll().ToListAsync();
            var rsUnits = await _unitRepo.GetAll().ToListAsync();
            var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();

            int companyRefid = await _locationRepo.GetAll().Where(l => l.Id == input.LocationRefId).Select(l => l.CompanyRefId).FirstOrDefaultAsync();

            foreach (var mat in lstMaterial)
            {
                var particularLocationSupplierMaterialExists = rsSupplierMaterials.FirstOrDefault(t => t.MaterialRefId == mat.MaterialRefId && t.LocationRefId == input.LocationRefId);

                if (particularLocationSupplierMaterialExists == null)
                {
                    particularLocationSupplierMaterialExists = rsSupplierMaterials.FirstOrDefault(t => t.LocationRefId == null && t.MaterialRefId == mat.MaterialRefId);
                }
                if (particularLocationSupplierMaterialExists != null)
                {
                    mat.MaterialPrice = particularLocationSupplierMaterialExists.MaterialPrice;
                    mat.MinimumOrderQuantity = particularLocationSupplierMaterialExists.MinimumOrderQuantity;
                    mat.MaximumOrderQuantity = particularLocationSupplierMaterialExists.MaximumOrderQuantity;
                    mat.UnitRefIdForMinOrder = particularLocationSupplierMaterialExists.UnitRefId;
                    mat.LastQuoteDate = particularLocationSupplierMaterialExists.LastQuoteDate;
                    mat.LastQuoteRefNo = particularLocationSupplierMaterialExists.LastQuoteRefNo;
                    mat.YieldPercentage = particularLocationSupplierMaterialExists.YieldPercentage == null ? mat.YieldPercentage : particularLocationSupplierMaterialExists.YieldPercentage;
                    mat.UnitRefId = particularLocationSupplierMaterialExists.UnitRefId;
                    var unit = rsUnits.FirstOrDefault(t => t.Id == mat.UnitRefId);
                    mat.UnitRefName = unit.Name;
                    mat.SupplierMaterialAliasName = particularLocationSupplierMaterialExists.SupplierMaterialAliasName;
                }
                else
                {
                    mat.MaterialPrice = 0;
                    mat.MinimumOrderQuantity = 0;
                    mat.UnitRefIdForMinOrder = mat.DefaultUnitId;
                    mat.UnitRefId = mat.DefaultUnitId;
                    mat.YieldPercentage = mat.YieldPercentage;

                    if (mat.DefaultUnitId != mat.UnitRefId)
                    {
                        var unit = rsUnits.FirstOrDefault(t => t.Id == mat.UnitRefId);
                        mat.UnitRefName = unit.Name;

                    }
                


                if (mat.DefaultUnitId != mat.UnitRefIdForMinOrder)
                {
                    var unit = rsUnits.FirstOrDefault(t => t.Id == mat.UnitRefIdForMinOrder);
                    mat.UnitRefNameForMinOrder = unit.Name;

                    decimal conversionFactor = 1;
                    //var conv = rsUnitConversion.FirstOrDefault(t => t.BaseUnitId == mat.UnitRefIdForMinOrder && t.RefUnitId == mat.DefaultUnitId);

                    var unitConversion = await _materialAppService.GetConversion(new GetConversionBasedOnBaseUnitRefUnit
                    {
                        BaseUnitId = mat.UnitRefIdForMinOrder,
                        RefUnitId = mat.DefaultUnitId,
                        ConversionUnitList = rsUc,
                        MaterialRefId = mat.MaterialRefId,
                        SupplierRefId = input.SupplierRefId
                    });
                    if (unitConversion == null)
                    {

                    }
                    else
                    {
                        conversionFactor = unitConversion.Conversion;
                    }

                    //if (conv != null)
                    //{
                    //    conversionFactor = conv.Conversion;
                    //}
                    //else
                    //{
                    //    var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == mat.UnitRefIdForMinOrder);
                    //    if (baseUnit == null)
                    //    {
                    //        throw new UserFriendlyException(L("UnitIdDoesNotExist", mat.UnitRefIdForMinOrder));
                    //    }
                    //    var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == mat.DefaultUnitId);
                    //    if (refUnit == null)
                    //    {
                    //        throw new UserFriendlyException(L("UnitIdDoesNotExist", mat.DefaultUnitId));
                    //    }
                    //}
                    mat.MaterialPrice = Math.Round(mat.MaterialPrice * 1 / conversionFactor, 3);
                }
            }
                List<TaxForMaterial> taxtoAdded = new List<TaxForMaterial>();
                List<TaxForMaterial> sortedTaxToAdded = new List<TaxForMaterial>();

                List<int> outputTaxTemplates = new List<int>();

                var taxTemplates = rsTaxTemplates.Where(t => t.MaterialRefId == mat.MaterialRefId).Select(t => t.TaxRefId).ToArray();

                outputTaxTemplates.AddRange(taxTemplates);
                taxTemplates = null;

                taxTemplates = rsTaxTemplates.Where(t => t.MaterialGroupCategoryRefId == mat.MaterialGroupCategoryRefId && t.MaterialRefId == null).Select(t => t.TaxRefId).ToArray();
                outputTaxTemplates.AddRange(taxTemplates);
                taxTemplates = null;

                taxTemplates = rsTaxTemplates.Where(t => t.MaterialGroupRefId == mat.MaterialGroupRefId && t.MaterialGroupCategoryRefId == null && t.MaterialRefId == null).Select(t => t.TaxRefId).ToArray();
                outputTaxTemplates.AddRange(taxTemplates);
                taxTemplates = null;

                taxTemplates = rsTaxTemplates.Where(t => t.LocationRefId == input.LocationRefId && t.MaterialGroupRefId == null).Select(t => t.TaxRefId).ToArray();
                outputTaxTemplates.AddRange(taxTemplates);
                taxTemplates = null;

                taxTemplates = rsTaxTemplates.Where(t => t.CompanyRefId == companyRefid && t.LocationRefId == input.LocationRefId).Select(t => t.TaxRefId).ToArray();
                outputTaxTemplates.AddRange(taxTemplates);
                taxTemplates = null;

                taxTemplates = rsTaxTemplates.Where(t => t.LocationRefId == null && t.CompanyRefId == null && t.MaterialGroupRefId == null && t.MaterialGroupCategoryRefId == null && t.MaterialRefId == null).Select(t => t.TaxRefId).ToArray();
                outputTaxTemplates.AddRange(taxTemplates);
                taxTemplates = null;

                outputTaxTemplates = outputTaxTemplates.Distinct().MapTo<List<int>>();

                List<ApplicableTaxesForMaterial> applicabletaxes = new List<ApplicableTaxesForMaterial>();

                if (outputTaxTemplates.Count() > 0)
                {
                    foreach (var taxtemplate in outputTaxTemplates)
                    {
                        var tax = rsTaxes.Single(t => t.Id == taxtemplate);
                        taxtoAdded.Add(new TaxForMaterial
                        {
                            MaterialRefId = mat.MaterialRefId,
                            TaxName = tax.TaxName,
                            TaxRefId = taxtemplate,
                            TaxRate = tax.Percentage,
                            SortOrder = tax.SortOrder,
                            Rounding = tax.Rounding,
                            TaxCalculationMethod = tax.TaxCalculationMethod,
                        }
                        );
                    }
                    sortedTaxToAdded = taxtoAdded.OrderBy(t => t.SortOrder).ToList();

                    foreach (var tax in sortedTaxToAdded)
                    {
                        applicabletaxes.Add(new ApplicableTaxesForMaterial
                        {
                            TaxName = tax.TaxName,
                            TaxRefId = tax.TaxRefId,
                            TaxRate = tax.TaxRate,
                            SortOrder = tax.SortOrder,
                            Rounding = tax.Rounding,
                            TaxCalculationMethod = tax.TaxCalculationMethod,
                        });
                    }
                }
                //@@Pending Sort Based On SortOrder

                mat.TaxForMaterial = sortedTaxToAdded;
                mat.ApplicableTaxes = applicabletaxes;

                #region ExistOrder Detail
                if (input.PurchaseReturnFlag == false)
                {
                    var poRaisedAlready = existPurchaseDetail.Where(t => t.MaterialRefId == mat.MaterialRefId);

                    if (poRaisedAlready == null || poRaisedAlready.Count() == 0)
                    {
                        mat.AlreadyOrderedQuantity = 0;
                    }
                    else
                    {
                        List<PipelineOrderDetails> pipelineOrderDetails = new List<PipelineOrderDetails>();
                        bool diffUnitsExistFlag = poRaisedAlready.Where(t => t.UnitRefId != mat.DefaultUnitId).Count() > 0 ? true : false;
                        if (diffUnitsExistFlag == false)
                        {
                            mat.AlreadyOrderedQuantity = poRaisedAlready.Sum(t => t.QtyOrdered - t.QtyReceived);
                            foreach (var pl in poRaisedAlready)
                            {
                                var poMaster = existPurchaseOrder.FirstOrDefault(t => t.Id == pl.PoRefId);
                                var sup = rsSupplier.FirstOrDefault(t => t.Id == poMaster.SupplierRefId);
                                PipelineOrderDetails newPl = new PipelineOrderDetails
                                {
                                    SupplierRefId = poMaster.SupplierRefId,
                                    SupplierRefName = sup.SupplierName,
                                    PoDate = poMaster.PoDate,
                                    ExpectedDeliveryDate = poMaster.DeliveryDateExpected,
                                    QtyOrdered = pl.QtyOrdered,
                                    QtyReceived = pl.QtyReceived,
                                    QtyPending = pl.QtyOrdered - pl.QtyReceived,
                                };
                                pipelineOrderDetails.Add(newPl);
                            }
                            mat.PipelineDetails = pipelineOrderDetails;
                        }
                        else
                        {
                            var existOrderedData = (from data in poRaisedAlready
                                                    group data by new { data.PoRefId, data.UnitRefId } into g
                                                    select new PurchaseOrderDetail
                                                    {
                                                        PoRefId = g.Key.PoRefId,
                                                        UnitRefId = g.Key.UnitRefId,
                                                        QtyOrdered = g.Sum(t => t.QtyOrdered),
                                                        QtyReceived = g.Sum(t => t.QtyReceived)
                                                    }).ToList();
                            foreach (var pl in existOrderedData)
                            {
                                decimal conversionFactor = 1;
                                //if (pl.UnitRefId == mat.DefaultUnitId)
                                //    conversionFactor = 1;
                                //else
                                //{
                                //    var conv = rsUnitConversion.FirstOrDefault(
                                //            t => t.BaseUnitId == pl.UnitRefId && t.RefUnitId == mat.DefaultUnitId);
                                //    if (conv != null)
                                //    {
                                //        conversionFactor = conv.Conversion;
                                //    }
                                //    else
                                //    {
                                //        var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == pl.UnitRefId);
                                //        if (baseUnit == null)
                                //        {
                                //            throw new UserFriendlyException(L("UnitIdDoesNotExist", pl.UnitRefId));
                                //        }
                                //        var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == mat.DefaultUnitId);
                                //        if (refUnit == null)
                                //        {
                                //            throw new UserFriendlyException(L("UnitIdDoesNotExist", mat.DefaultUnitId));
                                //        }
                                //        //throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
                                //        //conversionFactor = 1;
                                //    }
                                //}

                                var unitConversion = await _materialAppService.GetConversion(new GetConversionBasedOnBaseUnitRefUnit
                                {
                                    BaseUnitId = pl.UnitRefId,
                                    RefUnitId = mat.DefaultUnitId,
                                    ConversionUnitList = rsUc,
                                    MaterialRefId = mat.MaterialRefId,
                                    SupplierRefId = input.SupplierRefId
                                });
                                if (unitConversion == null)
                                {

                                }
                                else
                                {
                                    conversionFactor = unitConversion.Conversion;
                                }

                                pl.QtyOrdered = pl.QtyOrdered * conversionFactor;
                                pl.QtyReceived = pl.QtyReceived * conversionFactor;
                                {
                                    var poMaster = existPurchaseOrder.FirstOrDefault(t => t.Id == pl.PoRefId);
                                    if (poMaster != null)
                                    {
                                        var sup = rsSupplier.FirstOrDefault(t => t.Id == poMaster.SupplierRefId);
                                        PipelineOrderDetails newPl = new PipelineOrderDetails
                                        {
                                            SupplierRefId = poMaster.SupplierRefId,
                                            SupplierRefName = sup.SupplierName,
                                            PoDate = poMaster.PoDate,
                                            ExpectedDeliveryDate = poMaster.DeliveryDateExpected,
                                            QtyOrdered = pl.QtyOrdered,
                                            QtyReceived = pl.QtyReceived,
                                            QtyPending = pl.QtyOrdered - pl.QtyReceived
                                        };
                                        pipelineOrderDetails.Add(newPl);
                                    }
                                }
                            }

                            mat.AlreadyOrderedQuantity = existOrderedData.Sum(t => t.QtyOrdered - t.QtyReceived);
                        }
                    }
                }
                #endregion

                outputTaxTemplates = null;
                taxtoAdded = null;
            }

            var retlst = new ListResultOutput<SupplierMaterialViewDto>(lstMaterial.MapTo<List<SupplierMaterialViewDto>>());

            return retlst;

        }


        public async Task<List<SupplierMaterialViewDto>> GetSupplierWithPriceBasedOnMaterial(GetSupplierWithLocationAndMaterialInput input)
        {
            if (input.LocationRefId == 0)
            {
                throw new UserFriendlyException("Location Reference Not Exists");
            }

            List<int> arrMaterialRefIds = input.MaterialRefIdList.ToList();
            if (arrMaterialRefIds == null || arrMaterialRefIds.Count == 0)
            {
                return new List<SupplierMaterialViewDto>();
            }
            var tempMaterial = await _materialRepo.GetAllListAsync(t => arrMaterialRefIds.Contains(t.Id));
            List<MaterialListDto> rsMaterials = tempMaterial.MapTo<List<MaterialListDto>>();
            if (rsMaterials.Count == 0)
            {
                return new List<SupplierMaterialViewDto>();
            }

            List<SupplierMaterialViewDto> lstMaterial;
            var rsSupplierMaterials = await _suppliermaterialRepo.GetAllListAsync(sp => arrMaterialRefIds.Contains(sp.MaterialRefId));


            lstMaterial = await (from mat in _materialRepo.GetAll().Where(t => arrMaterialRefIds.Contains(t.Id))
                                 join supmat in _suppliermaterialRepo.GetAll().Where(t => arrMaterialRefIds.Contains(t.MaterialRefId))
                                 on mat.Id equals supmat.MaterialRefId
                                 join matloc in _materiallocationwisestockRepo.GetAll()
                                    .Where(t => t.IsActiveInLocation == true && t.LocationRefId == input.LocationRefId && arrMaterialRefIds.Contains(t.MaterialRefId))
                                 on mat.Id equals matloc.MaterialRefId
                                 join un in _unitRepo.GetAll() on mat.DefaultUnitId equals un.Id
                                 join matgroupCat in _materialgroupcategoryRepo.GetAll() on mat.MaterialGroupCategoryRefId equals matgroupCat.Id
                                 select new SupplierMaterialViewDto
                                 {
                                     Barcode = mat.Barcode,
                                     MaterialRefId = mat.Id,
                                     MaterialRefName = mat.MaterialName,
                                     MaterialGroupRefId = matgroupCat.MaterialGroupRefId,
                                     MaterialGroupCategoryRefId = mat.MaterialGroupCategoryRefId,
                                     YieldPercentage = mat.YieldPercentage,
                                     CurrentInHand = matloc.CurrentInHand,
                                     AlreadyOrderedQuantity = 0,
                                     DefaultUnitId = mat.DefaultUnitId,
                                     Uom = un.Name,
                                     UnitRefNameForMinOrder = un.Name,
                                     UnitRefName = un.Name,
                                     IsQuoteNeededForPurchase = mat.IsQuoteNeededForPurchase,
                                     IsFractional = mat.IsFractional,
                                     IsBranded = mat.IsBranded,
                                     Hsncode = mat.Hsncode,
                                     SupplierRefId = supmat.SupplierRefId,
                                     SupplierMaterialAliasName = supmat.SupplierMaterialAliasName
                                 }).Distinct().ToListAsync();

            #region SSNPending - Needs to Include Non Linked Supplier , but Purchase Done 

            #endregion

            List<PurchaseOrderDetailViewDto> existPurchaseDetail = new List<PurchaseOrderDetailViewDto>();
            List<PurchaseOrderListDto> existPurchaseOrder = new List<PurchaseOrderListDto>();
            if (input.PurchaseReturnFlag == false)
            {
                DateTime minDeliveryDate = DateTime.Now.Date.AddDays(-30);
                DateTime maxDeliveryDate = DateTime.Now.Date.AddDays(2);

                var tempexistPurchaseDetail = await (from poDet in _purchaseorderdetailRepo.GetAll()
                                       .Where(t => arrMaterialRefIds.Contains(t.MaterialRefId) && t.QtyOrdered > t.QtyReceived)
                                       .WhereIf(input.PoRefId != null, t => t.PoRefId != input.PoRefId)
                                                     join poMas in _purchaseorderRepo.GetAll()
                                                     .Where(t => t.LocationRefId == input.LocationRefId && (t.PoCurrentStatus == "P" || t.PoCurrentStatus == "A" || t.PoCurrentStatus == "H") && DbFunctions.TruncateTime(t.DeliveryDateExpected) >= DbFunctions.TruncateTime(minDeliveryDate) && DbFunctions.TruncateTime(t.DeliveryDateExpected) <= DbFunctions.TruncateTime(maxDeliveryDate))
                                                     on poDet.PoRefId equals poMas.Id
                                                     select poDet).ToListAsync();
                existPurchaseDetail = tempexistPurchaseDetail.MapTo<List<PurchaseOrderDetailViewDto>>();

                List<int> PoRefIds = existPurchaseDetail.Select(t => t.PoRefId).ToList();
                var tempPoMaster = await _purchaseorderRepo.GetAllListAsync(t => PoRefIds.Contains(t.Id));
                existPurchaseOrder = tempPoMaster.MapTo<List<PurchaseOrderListDto>>();
            }

            var supplierRefIds = rsSupplierMaterials.Select(t => t.SupplierRefId).ToList();
            supplierRefIds.AddRange(existPurchaseOrder.Select(t => t.SupplierRefId).ToList());
            var rsSupplier = await _supplierRepo.GetAllListAsync(t => supplierRefIds.Contains(t.Id));

            var rsTaxes = await _taxRepo.GetAll().ToListAsync();
            var rsTaxTemplates = await _taxtemplatemappingRepo.GetAll().ToListAsync();
            var rsUnits = await _unitRepo.GetAll().ToListAsync();
            var rsUnitConversion = await _unitConversionRepo.GetAll().ToListAsync();

            int companyRefid = await _locationRepo.GetAll().Where(l => l.Id == input.LocationRefId).Select(l => l.CompanyRefId).FirstOrDefaultAsync();

            foreach (var mat in lstMaterial)
            {
                var particularLocationSupplierMaterialExists = rsSupplierMaterials.FirstOrDefault(t => t.SupplierRefId == mat.SupplierRefId && t.MaterialRefId == mat.MaterialRefId && t.LocationRefId == input.LocationRefId);
                var supplier = rsSupplier.FirstOrDefault(t => t.Id == mat.SupplierRefId);
                mat.SupplierRefName = supplier.SupplierName;
                if (particularLocationSupplierMaterialExists == null)
                {
                    particularLocationSupplierMaterialExists = rsSupplierMaterials.FirstOrDefault(t => t.SupplierRefId == mat.SupplierRefId && t.LocationRefId == null && t.MaterialRefId == mat.MaterialRefId);
                }
                if (particularLocationSupplierMaterialExists != null)
                {
                    mat.MaterialPrice = particularLocationSupplierMaterialExists.MaterialPrice;
                    mat.MinimumOrderQuantity = particularLocationSupplierMaterialExists.MinimumOrderQuantity;
                    mat.MaximumOrderQuantity = particularLocationSupplierMaterialExists.MaximumOrderQuantity;
                    mat.UnitRefIdForMinOrder = particularLocationSupplierMaterialExists.UnitRefId;
                    mat.LastQuoteDate = particularLocationSupplierMaterialExists.LastQuoteDate;
                    mat.LastQuoteRefNo = particularLocationSupplierMaterialExists.LastQuoteRefNo;
                    mat.YieldPercentage = particularLocationSupplierMaterialExists.YieldPercentage == null ? mat.YieldPercentage : particularLocationSupplierMaterialExists.YieldPercentage;
                    mat.UnitRefId = particularLocationSupplierMaterialExists.UnitRefId;
                }
                else
                {
                    mat.MaterialPrice = 0;
                    mat.MinimumOrderQuantity = 0;
                    mat.MaximumOrderQuantity = 0;
                    mat.UnitRefIdForMinOrder = mat.DefaultUnitId;
                    mat.UnitRefId = mat.DefaultUnitId;
                    mat.YieldPercentage = mat.YieldPercentage;
                }

                if (mat.DefaultUnitId != mat.UnitRefId)
                {
                    var unit = rsUnits.FirstOrDefault(t => t.Id == mat.UnitRefId);
                    mat.UnitRefName = unit.Name;
                }

                if (mat.DefaultUnitId != mat.UnitRefIdForMinOrder)
                {
                    var unit = rsUnits.FirstOrDefault(t => t.Id == mat.UnitRefIdForMinOrder);
                    mat.UnitRefNameForMinOrder = unit.Name;

                    decimal conversionFactor = 1;
                    var conv = rsUnitConversion.FirstOrDefault(t => t.BaseUnitId == mat.UnitRefIdForMinOrder && t.RefUnitId == mat.DefaultUnitId);

                    if (conv != null)
                    {
                        conversionFactor = conv.Conversion;
                    }
                    else
                    {
                        var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == mat.UnitRefIdForMinOrder);
                        if (baseUnit == null)
                        {
                            throw new UserFriendlyException(L("UnitIdDoesNotExist", mat.UnitRefIdForMinOrder));
                        }
                        var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == mat.DefaultUnitId);
                        if (refUnit == null)
                        {
                            throw new UserFriendlyException(L("UnitIdDoesNotExist", mat.DefaultUnitId));
                        }
                    }
                    mat.MaterialPrice = Math.Round(mat.MaterialPrice * 1 / conversionFactor, 3);
                }

                #region Tax Including 
                List<TaxForMaterial> taxtoAdded = new List<TaxForMaterial>();
                List<TaxForMaterial> sortedTaxToAdded = new List<TaxForMaterial>();

                List<int> outputTaxTemplates = new List<int>();

                var taxTemplates = rsTaxTemplates.Where(t => t.MaterialRefId == mat.MaterialRefId).Select(t => t.TaxRefId).ToArray();

                outputTaxTemplates.AddRange(taxTemplates);
                taxTemplates = null;

                taxTemplates = rsTaxTemplates.Where(t => t.MaterialGroupCategoryRefId == mat.MaterialGroupCategoryRefId && t.MaterialRefId == null).Select(t => t.TaxRefId).ToArray();
                outputTaxTemplates.AddRange(taxTemplates);
                taxTemplates = null;

                taxTemplates = rsTaxTemplates.Where(t => t.MaterialGroupRefId == mat.MaterialGroupRefId && t.MaterialGroupCategoryRefId == null && t.MaterialRefId == null).Select(t => t.TaxRefId).ToArray();
                outputTaxTemplates.AddRange(taxTemplates);
                taxTemplates = null;

                taxTemplates = rsTaxTemplates.Where(t => t.LocationRefId == input.LocationRefId && t.MaterialGroupRefId == null).Select(t => t.TaxRefId).ToArray();
                outputTaxTemplates.AddRange(taxTemplates);
                taxTemplates = null;

                taxTemplates = rsTaxTemplates.Where(t => t.CompanyRefId == companyRefid && t.LocationRefId == input.LocationRefId).Select(t => t.TaxRefId).ToArray();
                outputTaxTemplates.AddRange(taxTemplates);
                taxTemplates = null;

                taxTemplates = rsTaxTemplates.Where(t => t.LocationRefId == null && t.CompanyRefId == null && t.MaterialGroupRefId == null && t.MaterialGroupCategoryRefId == null && t.MaterialRefId == null).Select(t => t.TaxRefId).ToArray();
                outputTaxTemplates.AddRange(taxTemplates);
                taxTemplates = null;

                outputTaxTemplates = outputTaxTemplates.Distinct().MapTo<List<int>>();

                List<ApplicableTaxesForMaterial> applicabletaxes = new List<ApplicableTaxesForMaterial>();

                if (outputTaxTemplates.Count() > 0)
                {
                    foreach (var taxtemplate in outputTaxTemplates)
                    {
                        var tax = rsTaxes.Single(t => t.Id == taxtemplate);
                        taxtoAdded.Add(new TaxForMaterial
                        {
                            MaterialRefId = mat.MaterialRefId,
                            TaxName = tax.TaxName,
                            TaxRefId = taxtemplate,
                            TaxRate = tax.Percentage,
                            SortOrder = tax.SortOrder,
                            Rounding = tax.Rounding,
                            TaxCalculationMethod = tax.TaxCalculationMethod,
                        }
                        );
                    }
                    sortedTaxToAdded = taxtoAdded.OrderBy(t => t.SortOrder).ToList();

                    foreach (var tax in sortedTaxToAdded)
                    {
                        applicabletaxes.Add(new ApplicableTaxesForMaterial
                        {
                            TaxName = tax.TaxName,
                            TaxRefId = tax.TaxRefId,
                            TaxRate = tax.TaxRate,
                            SortOrder = tax.SortOrder,
                            Rounding = tax.Rounding,
                            TaxCalculationMethod = tax.TaxCalculationMethod,
                        });
                    }
                }
                //@@Pending Sort Based On SortOrder

                mat.TaxForMaterial = sortedTaxToAdded;
                mat.ApplicableTaxes = applicabletaxes;

                #endregion

                #region ExistOrder Detail
                if (input.PurchaseReturnFlag == false)
                {
                    var poRaisedAlready = existPurchaseDetail.Where(t => t.MaterialRefId == mat.MaterialRefId);

                    if (poRaisedAlready == null || poRaisedAlready.Count() == 0)
                    {
                        mat.AlreadyOrderedQuantity = 0;
                        mat.PipelineDetails = new List<PipelineOrderDetails>();
                    }
                    else
                    {
                        List<PipelineOrderDetails> pipelineOrderDetails = new List<PipelineOrderDetails>();
                        bool diffUnitsExistFlag = poRaisedAlready.Where(t => t.UnitRefId != mat.DefaultUnitId).Count() > 0 ? true : false;
                        if (diffUnitsExistFlag == false)
                        {
                            mat.AlreadyOrderedQuantity = poRaisedAlready.Sum(t => t.QtyOrdered - t.QtyReceived);
                            foreach (var pl in poRaisedAlready)
                            {
                                var poMaster = existPurchaseOrder.FirstOrDefault(t => t.Id == pl.PoRefId);
                                var sup = rsSupplier.FirstOrDefault(t => t.Id == poMaster.SupplierRefId);
                                PipelineOrderDetails newPl = new PipelineOrderDetails
                                {
                                    SupplierRefId = poMaster.SupplierRefId,
                                    SupplierRefName = sup.SupplierName,
                                    PoDate = poMaster.PoDate,
                                    ExpectedDeliveryDate = poMaster.DeliveryDateExpected,
                                    QtyOrdered = pl.QtyOrdered,
                                    QtyReceived = pl.QtyReceived,
                                    QtyPending = pl.QtyOrdered - pl.QtyReceived,
                                };
                                pipelineOrderDetails.Add(newPl);
                            }
                            mat.PipelineDetails = pipelineOrderDetails;
                        }
                        else
                        {
                            var existOrderedData = (from data in poRaisedAlready
                                                    group data by new { data.UnitRefId } into g
                                                    select new PurchaseOrderDetail
                                                    {
                                                        UnitRefId = g.Key.UnitRefId,
                                                        QtyOrdered = g.Sum(t => t.QtyOrdered),
                                                        QtyReceived = g.Sum(t => t.QtyReceived)
                                                    }).ToList();
                            //int internalloop = 0;
                            foreach (var pl in existOrderedData)
                            {
                                decimal conversionFactor = 1;
                                if (pl.UnitRefId == mat.DefaultUnitId)
                                    conversionFactor = 1;
                                else
                                {
                                    var conv =
                                        rsUnitConversion.FirstOrDefault(
                                            t => t.BaseUnitId == pl.UnitRefId && t.RefUnitId == mat.DefaultUnitId);


                                    if (conv != null)
                                    {
                                        conversionFactor = conv.Conversion;
                                    }
                                    else
                                    {
                                        var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == pl.UnitRefId);
                                        if (baseUnit == null)
                                        {
                                            throw new UserFriendlyException(L("UnitIdDoesNotExist", pl.UnitRefId));
                                        }
                                        var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == mat.DefaultUnitId);
                                        if (refUnit == null)
                                        {
                                            throw new UserFriendlyException(L("UnitIdDoesNotExist", mat.DefaultUnitId));
                                        }
                                        //throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
                                        //conversionFactor = 1;
                                    }
                                }
                                pl.QtyOrdered = pl.QtyOrdered * conversionFactor;
                                pl.QtyReceived = pl.QtyReceived * conversionFactor;

                                {
                                    var poMaster = existPurchaseOrder.FirstOrDefault(t => t.Id == pl.PoRefId);
                                    if (poMaster != null)
                                    {
                                        var sup = rsSupplier.FirstOrDefault(t => t.Id == poMaster.SupplierRefId);
                                        PipelineOrderDetails newPl = new PipelineOrderDetails
                                        {
                                            SupplierRefId = poMaster.SupplierRefId,
                                            SupplierRefName = sup.SupplierName,
                                            PoDate = poMaster.PoDate,
                                            ExpectedDeliveryDate = poMaster.DeliveryDateExpected,
                                            QtyOrdered = pl.QtyOrdered,
                                            QtyReceived = pl.QtyReceived,
                                            QtyPending = pl.QtyOrdered - pl.QtyReceived
                                        };

                                        pipelineOrderDetails.Add(newPl);
                                    }
                                }
                            }

                            mat.AlreadyOrderedQuantity = existOrderedData.Sum(t => t.QtyOrdered - t.QtyReceived);
                            mat.PipelineDetails = pipelineOrderDetails;
                        }
                    }
                }
                #endregion

                outputTaxTemplates = null;
                taxtoAdded = null;
            }

            var retlst = lstMaterial;
            return retlst;
        }

        public async Task<ListResultOutput<CustomerMaterialViewDto>> GetMaterialBasedOnCustomerWithPrice(SentLocationAndCustomer input)
        {
            var customer = await _customerRepo.FirstOrDefaultAsync(t => t.Id == input.CustomerRefId);
            if (customer == null)
                return null;

            var priceTag = await _customerTagDefinitionRepo.FirstOrDefaultAsync(t => t.Id == customer.CustomerTagRefId);
            if (priceTag == null)
            {
                throw new UserFriendlyException(L("PriceTag") + " ?");
            }

            List<CustomerMaterialViewDto> lstMaterial;

            lstMaterial = await (from mat in _materialRepo.GetAll()
                                 join tagprice in _customerTagMaterialpriceRepo.GetAll()
                                 .Where(sp => sp.TagCode == priceTag.TagCode)
                                 on mat.Id equals tagprice.MaterialRefId into leftjoinmat
                                 from supwithprice in leftjoinmat.DefaultIfEmpty()
                                 join matloc in _materiallocationwisestockRepo.GetAll().Where(t => t.IsActiveInLocation == true && t.LocationRefId == input.LocationRefId)
                                 on mat.Id equals matloc.MaterialRefId
                                 join un in _unitRepo.GetAll()
                                 on mat.DefaultUnitId equals un.Id
                                 join matgroupCat in _materialgroupcategoryRepo.GetAll()
                                 on mat.MaterialGroupCategoryRefId equals matgroupCat.Id
                                 select new CustomerMaterialViewDto
                                 {
                                     MaterialRefId = mat.Id,
                                     MaterialRefName = mat.MaterialName,
                                     MaterialGroupRefId = matgroupCat.MaterialGroupRefId,
                                     MaterialGroupCategoryRefId = mat.MaterialGroupCategoryRefId,
                                     MaterialPrice = supwithprice.MaterialPrice.Equals(null) ? 0 : supwithprice.MaterialPrice,
                                     CurrentInHand = matloc.CurrentInHand,
                                     AlreadyOrderedQuantity = 0,
                                     Uom = un.Name,
                                     UnitRefId = mat.DefaultUnitId,
                                     DefaultUnitId = mat.DefaultUnitId,
                                     IsFractional = mat.IsFractional,
                                     IsBranded = mat.IsBranded,
                                     AvgRateBasedPrice = supwithprice.AvgRateBasedPrice,
                                     MarginPercentage = supwithprice.MarginPercentage,
                                     MinimumPrice = supwithprice.MinimumPrice
                                 }).ToListAsync();

            #region Get Avg Rate Material
            List<Location> locs = await _locationRepo.GetAll().Where(t => t.Id == input.LocationRefId).ToListAsync();
            if (locs.Count == 0)
            {
                throw new UserFriendlyException(L("NotExistForId", L("Location"), input.LocationRefId.ToString()));
            }
            List<LocationListDto> locinput = locs.MapTo<List<LocationListDto>>();
            List<int> materialRefIdsRateRequired = lstMaterial.Where(t => t.AvgRateBasedPrice == true).Select(t => t.MaterialRefId).ToList();

            DateTime transactionDateTime = input.TransactionDate;
            if (locs.FirstOrDefault().HouseTransactionDate.HasValue)
                input.TransactionDate = locs.FirstOrDefault().HouseTransactionDate.Value;
            else
                input.TransactionDate = DateTime.Today;

            var matView = await _housereportAppService.GetMaterialRateView(new GetHouseReportMaterialRateInput
            {
                StartDate = input.TransactionDate,
                EndDate = input.TransactionDate,
                Locations = locinput,
                MaterialRefIds = materialRefIdsRateRequired,
                FunctionCalledBy = "Customer Tag Wise Price"
            });

            var materialRate = matView.MaterialRateView;

            var rsUnitConversion = await _unitConversionRepo.GetAll().ToListAsync();
            foreach (var lst in lstMaterial.Where(t => t.AvgRateBasedPrice == true))
            {
                var matprice = materialRate.FirstOrDefault(t => t.MaterialRefId == lst.MaterialRefId);

                if (lst.DefaultUnitId == 0)
                {
                    var mat = await _materialRepo.FirstOrDefaultAsync(t => t.Id == lst.MaterialRefId);
                    lst.DefaultUnitId = mat.DefaultUnitId;
                }

                decimal conversionFactor = 1;
                if (lst.DefaultUnitId == lst.UnitRefId)
                    conversionFactor = 1;
                else
                {
                    var conv = rsUnitConversion.First(t => t.BaseUnitId == lst.DefaultUnitId && t.RefUnitId == lst.UnitRefId);
                    if (conv != null)
                    {
                        conversionFactor = conv.Conversion;
                    }
                    else
                    {
                        var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == lst.DefaultUnitId);
                        if (baseUnit == null)
                        {
                            throw new UserFriendlyException(L("UnitIdDoesNotExist", lst.DefaultUnitId));
                        }
                        var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == lst.UnitRefId);
                        if (refUnit == null)
                        {
                            throw new UserFriendlyException(L("UnitIdDoesNotExist", lst.UnitRefId));
                        }
                        throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
                    }
                    lst.CurrentInHand = lst.CurrentInHand * conversionFactor;
                }

                var templst = lst;
                if (lst.DefaultUnitId == lst.UnitRefId)
                    lst.MaterialPrice = matprice.AvgRate;
                else
                {
                    lst.MaterialPrice = Math.Round(matprice.AvgRate * 1 / conversionFactor, 3);
                }
                if (lst.MarginPercentage > 0)
                    lst.MaterialPrice = Math.Round(lst.MaterialPrice + Math.Round(lst.MaterialPrice * lst.MarginPercentage / 100, 2), 2);
                if (lst.MinimumPrice > lst.MaterialPrice)
                    lst.MaterialPrice = lst.MinimumPrice;
            }

            #endregion

            int[] matArray = lstMaterial.Select(t => t.MaterialRefId).ToArray();

            var existPurchaseDetail = await (from poDet in _salesorderdetailRepo.GetAll()
                                       .Where(t => matArray.Contains(t.MaterialRefId) && t.QtyOrdered > t.QtyReceived)
                                       .WhereIf(input.SoRefId != null, t => t.SoRefId != input.SoRefId)
                                             join poMas in _salesorderRepo.GetAll()
                                                 .Where(t => t.SoCurrentStatus == "P" || t.SoCurrentStatus == "A" || t.SoCurrentStatus == "H")
                                                 on poDet.SoRefId equals poMas.Id
                                             select poDet).ToListAsync();

            var rsTaxes = await _salestaxRepo.GetAll().ToListAsync();

            var rsTaxTemplates = await _salestaxtemplatemappingRepo.GetAll().ToListAsync();

            int loopcnt = 0;

            int companyRefid = locs.FirstOrDefault().CompanyRefId;

            foreach (var mat in lstMaterial)
            {
                List<TaxForMaterial> taxtoAdded = new List<TaxForMaterial>();
                List<TaxForMaterial> sortedTaxToAdded = new List<TaxForMaterial>();

                // taxtemplate;
                // Find any specific TAX For particular Material
                //List<int> taxTemplates;
                List<int> outputTaxTemplates = new List<int>();

                var taxTemplates = rsTaxTemplates.Where(t => t.MaterialRefId == mat.MaterialRefId).Select(t => t.SalesTaxRefId).ToArray();

                outputTaxTemplates.AddRange(taxTemplates);
                taxTemplates = null;

                taxTemplates = rsTaxTemplates.Where(t => t.MaterialGroupCategoryRefId == mat.MaterialGroupCategoryRefId && t.MaterialRefId == null).Select(t => t.SalesTaxRefId).ToArray();
                outputTaxTemplates.AddRange(taxTemplates);
                taxTemplates = null;

                taxTemplates = rsTaxTemplates.Where(t => t.MaterialGroupRefId == mat.MaterialGroupRefId && t.MaterialGroupCategoryRefId == null && t.MaterialRefId == null).Select(t => t.SalesTaxRefId).ToArray();
                outputTaxTemplates.AddRange(taxTemplates);
                taxTemplates = null;

                taxTemplates = rsTaxTemplates.Where(t => t.LocationRefId == input.LocationRefId && t.MaterialGroupRefId == null).Select(t => t.SalesTaxRefId).ToArray();
                outputTaxTemplates.AddRange(taxTemplates);
                taxTemplates = null;

                taxTemplates = rsTaxTemplates.Where(t => t.CompanyRefId == companyRefid && t.LocationRefId == input.LocationRefId).Select(t => t.SalesTaxRefId).ToArray();
                outputTaxTemplates.AddRange(taxTemplates);
                taxTemplates = null;

                taxTemplates = rsTaxTemplates.Where(t => t.LocationRefId == null && t.CompanyRefId == null && t.MaterialGroupRefId == null && t.MaterialGroupCategoryRefId == null && t.MaterialRefId == null).Select(t => t.SalesTaxRefId).ToArray();
                outputTaxTemplates.AddRange(taxTemplates);
                taxTemplates = null;


                //if (taxTemplates.Count() == 0 || taxTemplates == null)
                //{
                //    // If not specific for mateial then need to  Find Any Sepcific Tax For Material Group Category id
                //    taxTemplates = await _taxtemplatemappingRepo.GetAll().Where(t => t.MaterialGroupCategoryRefId == mat.MaterialGroupCategoryRefId && t.MaterialRefId == null).ToListAsync();
                //}

                //if (taxTemplates.Count() == 0 || taxTemplates == null)
                //{
                //    // If not specific for mateial Group Category then need to  Find Any Sepcific Tax For Material Group id
                //    taxTemplates = await _taxtemplatemappingRepo.GetAll().Where(t => t.MaterialGroupRefId == mat.MaterialGroupRefId && t.MaterialGroupCategoryRefId == null && t.MaterialRefId == null).ToListAsync();
                //}

                //if (taxTemplates.Count() == 0 || taxTemplates == null)
                //{
                //    // If not specific for mateial Group then need to  Find Any Sepcific Tax For Location
                //    taxTemplates = await _taxtemplatemappingRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId && t.MaterialGroupRefId==null).ToListAsync();
                //}

                //if (taxTemplates.Count() == 0 || taxTemplates == null)
                //{
                //    // If not specific for Location then need to  Find Any Sepcific Tax For Organization
                //    taxTemplates = await _taxtemplatemappingRepo.GetAll().Where(t => t.CompanyRefId == companyRefid).ToListAsync();
                //}

                //if (taxTemplates.Count() == 0 || taxTemplates == null)
                //{
                //    // If not specific for all above need to find the default any
                //    taxTemplates = await _taxtemplatemappingRepo.GetAll().Where(t => t.LocationRefId==null && t.CompanyRefId == null &&  t.MaterialGroupRefId == null && t.MaterialGroupCategoryRefId == null && t.MaterialRefId == null).ToListAsync();
                //}

                outputTaxTemplates = outputTaxTemplates.Distinct().MapTo<List<int>>();



                List<ApplicableTaxesForMaterial> applicabletaxes = new List<ApplicableTaxesForMaterial>();

                if (outputTaxTemplates.Count() > 0)
                {
                    foreach (var taxtemplate in outputTaxTemplates)
                    {
                        var tax = rsTaxes.Single(t => t.Id == taxtemplate);
                        taxtoAdded.Add(new TaxForMaterial
                        {
                            MaterialRefId = mat.MaterialRefId,
                            TaxName = tax.TaxName,
                            TaxRefId = taxtemplate,
                            TaxRate = tax.Percentage,
                            SortOrder = tax.SortOrder,
                            Rounding = tax.Rounding,
                            TaxCalculationMethod = tax.TaxCalculationMethod,
                        }
                        );
                    }
                    sortedTaxToAdded = taxtoAdded.OrderBy(t => t.SortOrder).ToList();

                    foreach (var tax in sortedTaxToAdded)
                    {
                        applicabletaxes.Add(new ApplicableTaxesForMaterial
                        {
                            TaxName = tax.TaxName,
                            TaxRefId = tax.TaxRefId,
                            TaxRate = tax.TaxRate,
                            SortOrder = tax.SortOrder,
                            Rounding = tax.Rounding,
                            TaxCalculationMethod = tax.TaxCalculationMethod,
                        });
                    }
                }
                //@@Pending Sort Based On SortOrder

                lstMaterial[loopcnt].TaxForMaterial = sortedTaxToAdded;
                lstMaterial[loopcnt].ApplicableTaxes = applicabletaxes;

                //var pur = await (from poDet in _purchaseorderdetailRepo.GetAll()
                //                    .Where(t => t.MaterialRefId == mat.MaterialRefId && t.QtyOrdered > t.QtyReceived)
                //                 join poMas in _purchaseorderRepo.GetAll()
                //                    .Where(t => t.PoCurrentStatus == "P" || t.PoCurrentStatus == "A" || t.PoCurrentStatus == "H")
                //                 on poDet.PoRefId equals poMas.Id
                //                 select poDet).ToListAsync();

                var poRaisedAlready = existPurchaseDetail.Where(t => t.MaterialRefId == mat.MaterialRefId);

                if (poRaisedAlready == null || poRaisedAlready.Count() == 0)
                {
                    lstMaterial[loopcnt].AlreadyOrderedQuantity = 0;
                }
                else
                {
                    bool diffUnitsExistFlag = poRaisedAlready.Where(t => t.UnitRefId != mat.DefaultUnitId).Count() > 0 ? true : false;
                    if (diffUnitsExistFlag == false)
                    {
                        lstMaterial[loopcnt].AlreadyOrderedQuantity = poRaisedAlready.Sum(t => t.QtyOrdered - t.QtyReceived);
                    }
                    else
                    {
                        var existOrderedData = (from data in poRaisedAlready
                                                group data by new { data.UnitRefId } into g
                                                select new PurchaseOrderDetail
                                                {
                                                    UnitRefId = g.Key.UnitRefId,
                                                    QtyOrdered = g.Sum(t => t.QtyOrdered),
                                                    QtyReceived = g.Sum(t => t.QtyReceived)
                                                }).ToList();
                        int internalloop = 0;
                        foreach (var ao in existOrderedData)
                        {
                            decimal conversionFactor = 1;
                            if (ao.UnitRefId == mat.DefaultUnitId)
                                conversionFactor = 1;
                            else
                            {
                                var conv =
                                    rsUnitConversion.First(
                                        t => t.BaseUnitId == ao.UnitRefId && t.RefUnitId == mat.DefaultUnitId);
                                if (conv != null)
                                    conversionFactor = conv.Conversion;
                            }

                            existOrderedData[internalloop].QtyOrdered = existOrderedData[internalloop].QtyOrdered * conversionFactor;
                            existOrderedData[internalloop].QtyReceived = existOrderedData[internalloop].QtyReceived * conversionFactor;
                            internalloop++;
                        }

                        lstMaterial[loopcnt].AlreadyOrderedQuantity = existOrderedData.Sum(t => t.QtyOrdered - t.QtyReceived);

                    }

                }
                outputTaxTemplates = null;
                taxtoAdded = null;
                loopcnt++;
            }

            var retlst = new ListResultOutput<CustomerMaterialViewDto>(lstMaterial.MapTo<List<CustomerMaterialViewDto>>());

            return retlst;

        }

        public async Task<List<MaterialPoProjectionViewDto>> GetPurchaseProjection(InputPurchaseProjection input)
        {
            var allItems = (from matgroupCat in _materialgroupcategoryRepo.GetAll()
                            join matgroup in _materialgroupRepo.GetAll().Where(t => t.TenantId == AbpSession.TenantId)
                            on matgroupCat.MaterialGroupRefId equals matgroup.Id
                            join mat in _materialRepo.GetAll().Where(t => t.OwnPreparation == false).WhereIf(input.IsHighValueItemOnly, t => t.IsHighValueItem == true)
                            on matgroupCat.Id equals mat.MaterialGroupCategoryRefId
                            join unit in _unitRepo.GetAll()
                            on mat.DefaultUnitId equals unit.Id
                            join matloc in _materiallocationwisestockRepo.GetAll()
                                .Where(t => t.IsActiveInLocation == true && t.LocationRefId == input.LocationRefId) on mat.Id equals matloc.MaterialRefId
                            select new MaterialPoProjectionViewDto
                            {
                                MaterialRefId = mat.Id,
                                MaterialTypeId = mat.MaterialTypeId,
                                MaterialTypeName = "",
                                MaterialName = mat.MaterialName,
                                MaterialPetName = mat.MaterialPetName,
                                MaterialGroupCategoryName = matgroupCat.MaterialGroupCategoryName,
                                UserSerialNumber = mat.UserSerialNumber,
                                WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                                OnHand = matloc.CurrentInHand,
                                UnitRefId = unit.Id,
                                Uom = unit.Name,
                                AlreadyOrderedQuantity = 0,
                            });

            List<MaterialPoProjectionViewDto> sortMenuItems;

            sortMenuItems = await allItems.ToListAsync();

            var rsSupplier = await _supplierRepo.GetAllListAsync();
            var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();

            foreach (var lst in sortMenuItems)
            {
                string materialTypeName = lst.MaterialTypeId == (int)MaterialType.RAW ? L("RAW") : L("SEMI");

                lst.MaterialTypeName = materialTypeName;

                DateTime fromDate = input.StartDate.Date;
                DateTime toDate = input.EndDate.Date;
                TimeSpan diffBetweenDays = toDate - fromDate;
                double numberofDays = diffBetweenDays.TotalDays;
                if (numberofDays == 0)
                    numberofDays = 1;


                var matLedger = await _materialLedgerRepo.GetAll().Where(a => a.MaterialRefId == lst.MaterialRefId && a.LocationRefId == input.LocationRefId && (DbFunctions.TruncateTime(a.LedgerDate) >= fromDate.Date && DbFunctions.TruncateTime(a.LedgerDate) <= toDate)).ToListAsync();

                lst.LastNDaysConsumption = 0;
                lst.PerDayConsumption = 0;


                if (matLedger.Count > 0)
                {
                    lst.LastNDaysConsumption = ((matLedger.Sum(a => a.Issued + a.Sales + a.TransferOut) - matLedger.Sum(a => a.Return + a.TransferIn)));
                    if (lst.LastNDaysConsumption < 0)
                    {
                        lst.LastNDaysConsumption = 0;
                        lst.PerDayConsumption = 0;
                    }
                    else if (lst.LastNDaysConsumption > 0)
                    {
                        lst.PerDayConsumption = Math.Round(((matLedger.Sum(a => a.Issued + a.Sales + a.TransferOut) - matLedger.Sum(a => a.Return + a.TransferIn)) / (decimal)numberofDays), 2);
                    }
                }

                if (lst.MaterialRefId == 379)
                {
                    int i = 1;
                }

                var rsMatsupplier = await _suppliermaterialRepo.GetAll().Where(t => t.MaterialRefId == lst.MaterialRefId && t.MaterialPrice > 0 && t.LocationRefId == input.LocationRefId).OrderBy(t => t.MaterialPrice).ToListAsync();
                if (rsMatsupplier == null)
                {
                    rsMatsupplier = await _suppliermaterialRepo.GetAll().Where(t => t.MaterialRefId == lst.MaterialRefId && t.MaterialPrice > 0 && t.LocationRefId == null).OrderBy(t => t.MaterialPrice).ToListAsync();
                }
                var matsupplier = rsMatsupplier.MapTo<List<SupplierMaterialListDto>>();

                if (matsupplier.Count() == 0)
                {
                    lst.SupplierRefId = null;
                    lst.SupplierRefName = "N/A";
                    lst.Price = 0;
                }
                else
                {
                    foreach (var slst in matsupplier)
                    {
                        if (slst.UnitRefId != lst.UnitRefId)
                        {
                            var conv = rsUc.FirstOrDefault(t => t.BaseUnitId == slst.UnitRefId && t.RefUnitId == lst.UnitRefId);
                            if (conv == null)
                            {
                                var baseunit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == lst.UnitRefId);
                                var refunit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == slst.UnitRefId);
                                throw new UserFriendlyException(L("ConversionFactorNotExist", baseunit.Name, refunit.Name));
                            }
                            slst.MaterialPrice = slst.MaterialPrice * 1 / conv.Conversion;
                        }
                    }
                    var ms = matsupplier.Where(t => t.MaterialPrice > 0).OrderBy(t => t.MaterialPrice).FirstOrDefault();
                    if (ms != null)
                    {
                        lst.SupplierRefId = ms.SupplierRefId;
                        lst.SupplierRefName = rsSupplier.FirstOrDefault(t => t.Id == ms.SupplierRefId).SupplierName;
                        lst.Price = Math.Round(ms.MaterialPrice, 2);
                    }

                }

                var pur = await (from poDet in _purchaseorderdetailRepo.GetAll().Where(t => t.MaterialRefId == lst.MaterialRefId)
                                 join poMas in _purchaseorderRepo.GetAll().Where(t => t.PoCurrentStatus == "P" || t.PoCurrentStatus == "A" || t.PoCurrentStatus == "H")
                                 on poDet.PoRefId equals poMas.Id
                                 select poDet).ToListAsync();

                if (pur == null || pur.Count == 0)
                {
                    lst.AlreadyOrderedQuantity = 0;
                }
                else
                {
                    lst.AlreadyOrderedQuantity = Math.Round(pur.Sum(t => t.QtyOrdered - t.QtyReceived), 2);
                }


                if (lst.PerDayConsumption > 0)
                {
                    lst.NextNDaysProjectedConsumption = Math.Ceiling(input.NextNDays * lst.PerDayConsumption);
                    lst.NextNDaysRoundedProjection = Math.Ceiling(lst.NextNDaysProjectedConsumption - lst.AlreadyOrderedQuantity - lst.OnHand);

                    if (lst.NextNDaysRoundedProjection < 0)
                        lst.NextNDaysRoundedProjection = 0;
                }

            }

            var allListDtos = sortMenuItems.MapTo<List<MaterialPoProjectionViewDto>>();
            allListDtos = allListDtos.OrderByDescending(t => t.NextNDaysRoundedProjection).ToList();
            var allItemCount = await allItems.CountAsync();

            return allListDtos;
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetSupplierForCombobox()
        {
            var lstSupplier = await _supplierRepo.GetAll().ToListAsync();
            return
                new ListResultOutput<ComboboxItemDto>(
                    lstSupplier.Select(e => new ComboboxItemDto(e.Id.ToString(), e.SupplierName)).ToList());
        }

        public async Task<List<ApplicableTaxesForMaterial>> GetTaxForMaterial()
        {
            var lstTax = await _taxRepo.GetAll().ToListAsync();

            List<ApplicableTaxesForMaterial> returntaxes = new List<ApplicableTaxesForMaterial>();

            foreach (var tax in lstTax)
            {
                var newtax = new ApplicableTaxesForMaterial
                {
                    TaxName = tax.TaxName,
                    TaxRate = tax.Percentage,
                    TaxRefId = tax.Id,
                    TaxCalculationMethod = tax.TaxCalculationMethod,
                    Rounding = tax.Rounding,
                    SortOrder = tax.SortOrder
                };

                returntaxes.Add(newtax);
            }
            return returntaxes;
        }

        public async Task<List<ApplicableTaxesForMaterial>> GetSaleTaxForMaterial()
        {
            var lstTax = await _salestaxRepo.GetAll().ToListAsync();

            List<ApplicableTaxesForMaterial> returntaxes = new List<ApplicableTaxesForMaterial>();

            foreach (var tax in lstTax)
            {
                var newtax = new ApplicableTaxesForMaterial
                {
                    TaxName = tax.TaxName,
                    TaxRate = tax.Percentage,
                    TaxRefId = tax.Id,
                    TaxCalculationMethod = tax.TaxCalculationMethod,
                    Rounding = tax.Rounding,
                    SortOrder = tax.SortOrder
                };

                returntaxes.Add(newtax);
            }
            return returntaxes;
        }

    }
}



