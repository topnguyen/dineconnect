﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using System.Linq;
using DinePlan.DineConnect.House.Impl;
using Abp.UI;

namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class SalesTaxAppService : DineConnectAppServiceBase, ISalesTaxAppService
    {
        private readonly ISalesTaxListExcelExporter _salestaxExporter;
        private readonly ISalesTaxManager _salestaxManager;
        private readonly IRepository<SalesTax> _salestaxRepo;
        private readonly IRepository<SalesTaxTemplateMapping> _salestaxTemplateMappingRepo;

        public SalesTaxAppService(ISalesTaxManager taxManager,
            IRepository<SalesTax> taxRepo,
            ISalesTaxListExcelExporter taxExporter,
            IRepository<SalesTaxTemplateMapping> taxTemplateMappingRepo)
        {
            _salestaxManager = taxManager;
            _salestaxRepo = taxRepo;
            _salestaxExporter = taxExporter;
            _salestaxTemplateMappingRepo = taxTemplateMappingRepo;

        }
        public async Task<PagedResultOutput<SalesTaxListDto>> GetAll(GetSalesTaxInput input)
        {
            var allItems = _salestaxRepo.GetAll();
            if (!string.IsNullOrEmpty(input.Operation) && input.Operation.Equals("SEARCH"))
            {
                allItems = _salestaxRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.TaxName.Equals(input.Filter)
               );
            }
            else
            {
                allItems = _salestaxRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.TaxName.Contains(input.Filter)
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<SalesTaxListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<SalesTaxListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetSalesTaxInput input)
        {
            input.MaxResultCount = AppConsts.MaxPageSize;

            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<SalesTaxListDto>>();
            return _salestaxExporter.ExportToFile(allListDtos);
        }

        public async Task<GetSalesTaxForEditOutput> GetSalesTaxForEdit(NullableIdInput input)
        {
            SalesTaxEditDto editDto;
            List<SalesTaxTemplateMappingEditDto> editDetailDto;

            if (input.Id.HasValue)
            {
                var hDto = await _salestaxRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<SalesTaxEditDto>();

                editDetailDto = await (from tt in _salestaxTemplateMappingRepo.GetAll().Where(a => a.SalesTaxRefId == input.Id.Value)
                                       select new SalesTaxTemplateMappingEditDto
                                       {
                                           Id=tt.Id,
                                           SalesTaxRefId=tt.SalesTaxRefId,
                                           CompanyRefId=tt.CompanyRefId,
                                           LocationRefId=tt.LocationRefId,
                                           MaterialGroupCategoryRefId=tt.MaterialGroupCategoryRefId,
                                           MaterialGroupRefId=tt.MaterialGroupRefId,
                                           MaterialRefId=tt.MaterialRefId
                                           
                                       }).ToListAsync();
            }
            else
            {
                editDto = new SalesTaxEditDto();

                editDetailDto = new List<SalesTaxTemplateMappingEditDto>();
            }

            return new GetSalesTaxForEditOutput
            {
                SalesTax = editDto,
                SalesTaxTemplateMapping=editDetailDto
            };
        }

        public async Task CreateOrUpdateSalesTax(CreateOrUpdateSalesTaxInput input)
        {
            if (input.SalesTax.Id.HasValue)
            {
                await UpdateSalesTax(input);
            }
            else
            {
                await CreateSalesTax(input);
            }
        }

        public async Task DeleteSalesTax(IdInput input)
        {
          
            var mapdetail = await _salestaxTemplateMappingRepo.GetAll().Where(t => t.SalesTaxRefId == input.Id).ToListAsync();

            foreach (var map in mapdetail)
            {
                await _salestaxTemplateMappingRepo.DeleteAsync(map.Id);
            }

            await _salestaxRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateSalesTax(CreateOrUpdateSalesTaxInput input)
        {
            var item = await _salestaxRepo.GetAsync(input.SalesTax.Id.Value);
            var dto = input.SalesTax;

            int newSortOrder = dto.SortOrder;
            int incrementby = 1;
            if (dto.SortOrder < 0)      //  Refer Grand Total Or Sub Total
            {
                newSortOrder = newSortOrder + 12;
                incrementby = 20;
            }
            else
            {
                var reftax = await _salestaxRepo.GetAll().Where(t => t.TaxName.Equals(dto.TaxCalculationMethod)).ToListAsync();

                if (reftax.Count() == 0)
                {
                    throw new UserFriendlyException("ReferenceSalesTaxIsWrong");
                }
                else
                {
                    newSortOrder = reftax[0].SortOrder + 1;
                }
            }

            bool loopFlag = true;
            do
            {
                var sortOrderexist = _salestaxRepo.GetAll().Where(t => t.SortOrder == newSortOrder && t.Id != dto.Id ).Select(t => t.SortOrder).ToList();
                if (sortOrderexist.Count() == 0)
                {
                    loopFlag = false;
                }
                else
                {
                    newSortOrder = sortOrderexist[0] + incrementby;
                }
            } while (loopFlag);

            dto.SortOrder = newSortOrder;

            bool taxNameChangedflag= false;
            List<SalesTax> taxrefList = new List<SalesTax>();
            if (!item.TaxName.Equals(dto.TaxName))
            {
                taxNameChangedflag = true;
                taxrefList = _salestaxRepo.GetAll().Where(t => t.TaxCalculationMethod.Equals(item.TaxName)).OrderBy(t=>t.SortOrder).ToList();
            }

            //TODO: SERVICE SalesTax Update Individually
            item.TaxName = dto.TaxName;
            item.Percentage = dto.Percentage;
            item.TaxCalculationMethod = dto.TaxCalculationMethod;
            item.SortOrder = dto.SortOrder;
            item.Rounding = dto.Rounding;

            if (taxNameChangedflag)
            {
                string newSalesTaxName = dto.TaxName;
                foreach (var taxref in taxrefList)
                {
                    newSortOrder++;
                    var refitem = await _salestaxRepo.GetAsync(taxref.Id);
                    refitem.TaxCalculationMethod = newSalesTaxName;
                    refitem.SortOrder = newSortOrder;
                }
            }
            
            List<int> recordsToBeRetained = new List<int>();
            if (input.SalesTaxTemplateMapping != null && input.SalesTaxTemplateMapping.Count > 0)
            {
                foreach(var items in  input.SalesTaxTemplateMapping)
                {
                    if (items.Id != 0)
                    {
                        var recId = (int)items.Id;
                        recordsToBeRetained.Add(recId);
                    }
                    var existingIssueDetail = _salestaxTemplateMappingRepo.GetAllList(tt => tt.SalesTaxRefId == dto.Id && tt.CompanyRefId == items.CompanyRefId && tt.LocationRefId == items.LocationRefId && tt.MaterialGroupRefId == items.MaterialGroupRefId && tt.MaterialGroupCategoryRefId == items.MaterialGroupCategoryRefId && tt.MaterialRefId == items.MaterialRefId  );

                    if (existingIssueDetail.Count == 0)  //Add new record
                    {
                        SalesTaxTemplateMapping tm = new SalesTaxTemplateMapping();
                        tm.SalesTaxRefId = (int) dto.Id;
                        tm.CompanyRefId = items.CompanyRefId;
                        tm.LocationRefId = items.LocationRefId;
                        tm.MaterialGroupCategoryRefId = items.MaterialGroupCategoryRefId;
                        tm.MaterialGroupRefId = items.MaterialGroupRefId;
                        tm.MaterialRefId = items.MaterialRefId;

                        var ret2 = await _salestaxTemplateMappingRepo.InsertOrUpdateAndGetIdAsync(tm);
                        recordsToBeRetained.Add(ret2);
                    }
                    else
                    {
                        var editDetailDto = await _salestaxTemplateMappingRepo.GetAsync(existingIssueDetail[0].Id);

                        editDetailDto.SalesTaxRefId = (int)dto.Id;
                        editDetailDto.CompanyRefId = items.CompanyRefId;
                        editDetailDto.LocationRefId = items.LocationRefId;
                        editDetailDto.MaterialGroupCategoryRefId = items.MaterialGroupCategoryRefId;
                        editDetailDto.MaterialGroupRefId = items.MaterialGroupRefId;
                        editDetailDto.MaterialRefId = items.MaterialRefId;
                        var ret3 = await _salestaxTemplateMappingRepo.InsertOrUpdateAndGetIdAsync(editDetailDto);
                        recordsToBeRetained.Add(ret3);
                    }
                }
            }

            var delTTList = _salestaxTemplateMappingRepo.GetAll().Where(a => a.SalesTaxRefId == input.SalesTax.Id.Value && !recordsToBeRetained.Contains((int)a.Id)).ToList();
            foreach (var a in delTTList)
            {
                _salestaxTemplateMappingRepo.Delete(a.Id);
            }


           CheckErrors(await _salestaxManager.CreateSync(item));
        }

        protected virtual async Task CreateSalesTax(CreateOrUpdateSalesTaxInput input)
        {
            var dto = input.SalesTax.MapTo<SalesTax>();

            if (input.SalesTaxTemplateMapping == null || input.SalesTaxTemplateMapping.Count() == 0)
            {
                throw new UserFriendlyException(L("MinimumOneDetail"));
            }

            int newSortOrder = dto.SortOrder;
            int incrementby = 1;
            if (dto.SortOrder < 0)      //  Refer Grand Total Or Sub Total
            {
                newSortOrder = newSortOrder + 12;
                incrementby = 20;
            }
            else
            {
                if (dto.TaxCalculationMethod.Equals("GT"))
                {
                    newSortOrder = -1;
                    incrementby = 3;
                }
                else if (dto.TaxCalculationMethod.Equals("ST"))
                {
                    newSortOrder = -2;
                    incrementby = 3;
                }
                else
                {
                    var reftax = await _salestaxRepo.GetAll().Where(t => t.TaxName.Equals(dto.TaxCalculationMethod)).ToListAsync();

                    if (reftax.Count() == 0 )
                    {
                        throw new UserFriendlyException("ReferenceSalesTaxIsWrong");
                    }
                    else
                    {
                        newSortOrder = reftax[0].SortOrder + 1;
                    }
                }
            }

            bool loopFlag = true;
            do
            {
                var sortOrderexist = _salestaxRepo.GetAll().Where(t => t.SortOrder == newSortOrder).Select(t => t.SortOrder).ToList();
                if (sortOrderexist.Count() == 0)
                {
                    loopFlag = false;
                }
                else
                {
                    newSortOrder = sortOrderexist[0] + incrementby;
                }
            } while (loopFlag);

            if (newSortOrder < 0)
                newSortOrder = 1;

            dto.SortOrder = newSortOrder;

            int retId = await _salestaxRepo.InsertAndGetIdAsync(dto);

            foreach(SalesTaxTemplateMapping items in input.SalesTaxTemplateMapping.ToList())
            {
                SalesTaxTemplateMapping tm = new SalesTaxTemplateMapping();
                tm.SalesTaxRefId = (int)retId;
                tm.CompanyRefId = items.CompanyRefId;
                tm.LocationRefId = items.LocationRefId;
                tm.MaterialGroupCategoryRefId = items.MaterialGroupCategoryRefId;
                tm.MaterialGroupRefId = items.MaterialGroupRefId;
                tm.MaterialRefId = items.MaterialRefId;

                var ret2 = await _salestaxTemplateMappingRepo.InsertAndGetIdAsync(tm);
            }

            CheckErrors(await _salestaxManager.CreateSync(dto));
        }

        public async Task<ListResultOutput<SalesTaxListDto>> GetSalesTaxNames()
        {
            var lstSalesTax = await _salestaxRepo.GetAll().ToListAsync();
            return new ListResultOutput<SalesTaxListDto>(lstSalesTax.MapTo<List<SalesTaxListDto>>());
        }
    }
}
