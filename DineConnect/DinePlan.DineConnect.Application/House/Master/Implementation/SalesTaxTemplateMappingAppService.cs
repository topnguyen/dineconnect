﻿
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;


namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class SalesTaxTemplateMappingAppService : DineConnectAppServiceBase, ISalesTaxTemplateMappingAppService
    {

        private readonly ISalesTaxTemplateMappingListExcelExporter _salestaxtemplatemappingExporter;
        private readonly ISalesTaxTemplateMappingManager _salestaxtemplatemappingManager;
        private readonly IRepository<SalesTaxTemplateMapping> _salestaxtemplatemappingRepo;

        public SalesTaxTemplateMappingAppService(ISalesTaxTemplateMappingManager taxtemplatemappingManager,
            IRepository<SalesTaxTemplateMapping> taxTemplateMappingRepo,
            ISalesTaxTemplateMappingListExcelExporter taxtemplatemappingExporter)
        {
            _salestaxtemplatemappingManager = taxtemplatemappingManager;
            _salestaxtemplatemappingRepo = taxTemplateMappingRepo;
            _salestaxtemplatemappingExporter = taxtemplatemappingExporter;

        }

        public async Task<PagedResultOutput<SalesTaxTemplateMappingListDto>> GetAll(GetSalesTaxTemplateMappingInput input)
        {
            var allItems = _salestaxtemplatemappingRepo.GetAll();
            if (input.Operation == "SEARCH")
            {
                allItems = _salestaxtemplatemappingRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Id.Equals(input.Filter)
               );
            }
            else
            {
                allItems = _salestaxtemplatemappingRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Id.ToString().Contains(input.Filter)
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<SalesTaxTemplateMappingListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<SalesTaxTemplateMappingListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _salestaxtemplatemappingRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<SalesTaxTemplateMappingListDto>>();
            return _salestaxtemplatemappingExporter.ExportToFile(allListDtos);
        }

        public async Task<GetSalesTaxTemplateMappingForEditOutput> GetSalesTaxTemplateMappingForEdit(NullableIdInput input)
        {
            SalesTaxTemplateMappingEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _salestaxtemplatemappingRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<SalesTaxTemplateMappingEditDto>();
            }
            else
            {
                editDto = new SalesTaxTemplateMappingEditDto();
            }

            return new GetSalesTaxTemplateMappingForEditOutput
            {
                SalesTaxTemplateMapping = editDto
            };
        }

        public async Task CreateOrUpdateSalesTaxTemplateMapping(CreateOrUpdateSalesTaxTemplateMappingInput input)
        {
            if (input.SalesTaxTemplateMapping.Id.HasValue)
            {
                await UpdateSalesTaxTemplateMapping(input);
            }
            else
            {
                await CreateSalesTaxTemplateMapping(input);
            }
        }

        public async Task DeleteSalesTaxTemplateMapping(IdInput input)
        {
            await _salestaxtemplatemappingRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateSalesTaxTemplateMapping(CreateOrUpdateSalesTaxTemplateMappingInput input)
        {
            var item = await _salestaxtemplatemappingRepo.GetAsync(input.SalesTaxTemplateMapping.Id.Value);
            var dto = input.SalesTaxTemplateMapping;

            //TODO: SERVICE SalesTaxTemplateMapping Update Individually

            CheckErrors(await _salestaxtemplatemappingManager.CreateSync(item));
        }

        protected virtual async Task CreateSalesTaxTemplateMapping(CreateOrUpdateSalesTaxTemplateMappingInput input)
        {
            var dto = input.SalesTaxTemplateMapping.MapTo<SalesTaxTemplateMapping>();

            CheckErrors(await _salestaxtemplatemappingManager.CreateSync(dto));
        }

        public async Task<ListResultOutput<SalesTaxTemplateMappingListDto>> GetSosMenuPortionRefIds()
        {
            var lstSalesTaxTemplateMapping = await _salestaxtemplatemappingRepo.GetAll().ToListAsync();
            return new ListResultOutput<SalesTaxTemplateMappingListDto>(lstSalesTaxTemplateMapping.MapTo<List<SalesTaxTemplateMappingListDto>>());
        }
    }
}
