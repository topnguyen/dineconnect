﻿

using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;


namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class RecipeIngredientAppService : DineConnectAppServiceBase, IRecipeIngredientAppService
    {

        private readonly IRecipeIngredientListExcelExporter _recipeingredientExporter;
        private readonly IRecipeIngredientManager _recipeingredientManager;
        private readonly IRepository<RecipeIngredient> _recipeingredientRepo;
        private readonly IRepository<Material> _material;
        private readonly IRepository<Recipe> _recipe;
        private readonly IRepository<Unit> _unit;
        public RecipeIngredientAppService(IRecipeIngredientManager recipeingredientManager,
            IRepository<RecipeIngredient> recipeIngredientRepo,
            IRecipeIngredientListExcelExporter recipeingredientExporter,
            IRepository<Material> material,
            IRepository<Recipe> recipe,
            IRepository<Unit> unit)
        {
            _recipeingredientManager = recipeingredientManager;
            _recipeingredientRepo = recipeIngredientRepo;
            _recipeingredientExporter = recipeingredientExporter;
            _material = material;
            _recipe = recipe;
            _unit = unit;

        }

        public async Task<PagedResultOutput<RecipeIngredientListDto>> GetAll(GetRecipeIngredientInput input)
        {
            var allItems = _recipeingredientRepo.GetAll();
            if (!string.IsNullOrEmpty(input.Operation) && input.Operation.Equals("SEARCH"))
            {
                allItems = _recipeingredientRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Id.Equals(input.Filter)
               );
            }
            else
            {
                allItems = _recipeingredientRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Id.ToString().Contains(input.Filter)
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<RecipeIngredientListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<RecipeIngredientListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _recipeingredientRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<RecipeIngredientListDto>>();
            return _recipeingredientExporter.ExportToFile(allListDtos);
        }

        public async Task<GetRecipeIngredientForEditOutput> GetRecipeIngredientForEdit(NullableIdInput input)
        {
            RecipeIngredientEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _recipeingredientRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<RecipeIngredientEditDto>();
            }
            else
            {
                editDto = new RecipeIngredientEditDto();
            }

            return new GetRecipeIngredientForEditOutput
            {
                RecipeIngredient = editDto
            };
        }

        public async Task CreateOrUpdateRecipeIngredient(CreateOrUpdateRecipeIngredientInput input)
        {
            if (input.RecipeIngredient.Id.HasValue)
            {
                await UpdateRecipeIngredient(input);
            }
            else
            {
                await CreateRecipeIngredient(input);
            }
        }

        public async Task DeleteRecipeIngredient(IdInput input)
        {
            await _recipeingredientRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateRecipeIngredient(CreateOrUpdateRecipeIngredientInput input)
        {
            var item = await _recipeingredientRepo.GetAsync(input.RecipeIngredient.Id.Value);
            var dto = input.RecipeIngredient;

            //TODO: SERVICE RecipeIngredient Update Individually
            item.RecipeRefId = dto.RecipeRefId;
            item.MaterialRefId = dto.MaterialRefId;
            item.MaterialUsedQty = dto.MaterialUsedQty;
            item.UnitRefId = dto.UnitRefId;
            item.UserSerialNumber = dto.UserSerialNumber;
            item.VariationflagForProduction = dto.VariationflagForProduction;

            CheckErrors(await _recipeingredientManager.CreateSync(item));
        }

        protected virtual async Task CreateRecipeIngredient(CreateOrUpdateRecipeIngredientInput input)
        {
            var dto = input.RecipeIngredient.MapTo<RecipeIngredient>();

            CheckErrors(await _recipeingredientManager.CreateSync(dto));
        }

        public async Task<ListResultOutput<RecipeIngredientListDto>> GetIds()
        {
            var lstRecipeIngredient = await _recipeingredientRepo.GetAll().ToListAsync();
            return new ListResultOutput<RecipeIngredientListDto>(lstRecipeIngredient.MapTo<List<RecipeIngredientListDto>>());
        }
        public async Task<PagedResultOutput<RecipeIngredientViewDto>> GetView(GetUnitConversionInput input)
        {
            var allItems = (from r in _recipeingredientRepo.GetAll()
                            join m in _material.GetAll()
                            on r.MaterialRefId equals m.Id
                            join rec in _recipe.GetAll() on
                            r.RecipeRefId equals rec.Id
                            join u in _unit.GetAll()
                            on r.UnitRefId equals u.Id
                            select new RecipeIngredientViewDto()
                            {
                                Id = r.Id,
                                RecipeRefName = rec.RecipeName,
                                MaterialRefName = m.MaterialName,
                                MaterialUsedQty = r.MaterialUsedQty,
                                UserSerialNumber = r.UserSerialNumber,
                                CreationTime = r.CreationTime,
                                VariationflagForProduction = r.VariationflagForProduction,
                                UnitRefName = u.Name,
                            }).WhereIf(
                            !input.Filter.IsNullOrEmpty(),
                            p => p.RecipeRefName.Contains(input.Filter)
                        );

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.ToList();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<RecipeIngredientViewDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<PagedResultOutput<RecipeIngredientViewDto>> GetViewByRefRecipeId(IdInput input)
        {
            var allItems = (from r in _recipeingredientRepo.GetAll()
                            join m in _material.GetAll()
                            on r.MaterialRefId equals m.Id
                            join rec in _recipe.GetAll() on
                            r.RecipeRefId equals rec.Id
                            join u in _unit.GetAll()
                            on r.UnitRefId equals u.Id
                            select new RecipeIngredientViewDto()
                            {
                                Id = r.Id,
                                RecipeRefId = r.RecipeRefId,
                                RecipeRefName = rec.RecipeName,
                                MaterialRefId=r.MaterialRefId,
                                MaterialRefName = m.MaterialName,
                                MaterialUsedQty = r.MaterialUsedQty,
                                UserSerialNumber = r.UserSerialNumber,
                                CreationTime = r.CreationTime,
                                VariationflagForProduction = r.VariationflagForProduction,
                                UnitRefId=u.Id,
                                UnitRefName = u.Name,
                            }).Where(p => p.RecipeRefId == input.Id);

            var sortMenuItems = await allItems.ToListAsync();

            var allListDtos = sortMenuItems.ToList();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<RecipeIngredientViewDto>(
                allItemCount,
                allListDtos
                );
        }
    }
}
