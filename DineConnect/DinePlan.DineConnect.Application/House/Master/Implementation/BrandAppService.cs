﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using Abp.UI;

namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class BrandAppService : DineConnectAppServiceBase, IBrandAppService
    {
        private readonly IBrandListExcelExporter _brandExporter;
        private readonly IBrandManager _brandManager;
        private readonly IRepository<Brand> _brandRepo;

        public BrandAppService(IBrandManager brandManager, 
            IRepository<Brand> brandRepo,
            IBrandListExcelExporter brandExporter)
        {
            _brandManager = brandManager;
            _brandRepo = brandRepo;
            _brandExporter = brandExporter;
        }

        public async Task<PagedResultOutput<BrandListDto>> GetAll(GetBrandInput input)
        {
            var allItems = _brandRepo
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Name.Contains(input.Filter)
                );
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<BrandListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<BrandListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _brandRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<BrandListDto>>();
            return _brandExporter.ExportToFile(allListDtos);
        }

        public async Task<GetBrandForEditOutput> GetBrandForEdit(NullableIdInput input)
        {
            BrandEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _brandRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<BrandEditDto>();
            }
            else
            {
                editDto = new BrandEditDto();
            }

            return new GetBrandForEditOutput
            {
                Brand = editDto
            };
        }

        public async Task CreateOrUpdateBrand(CreateOrUpdateBrandInput input)
        {
            if (input.Brand.Id.HasValue)
            {
                //  In edit mode, the updated Name should not be Equal to existing Name
                var alreadyExist = _brandRepo.FirstOrDefault(p => p.Name == input.Brand.Name && p.Id != input.Brand.Id );
                if (alreadyExist != null)
                {
                    throw new UserFriendlyException(L("NameAlreadyExists"));
                }

                await UpdateBrand(input);
            }
            else
            {
                var alreadyExist = _brandRepo.FirstOrDefault(p => p.Name == input.Brand.Name);
                if (alreadyExist != null)
                {
                    throw new UserFriendlyException(L("NameAlreadyExists"));
                }

                await CreateBrand(input);
            }
        }

        public async Task DeleteBrand(IdInput input)
        {
            await _brandRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateBrand(CreateOrUpdateBrandInput input)
        {
            var item = await _brandRepo.GetAsync(input.Brand.Id.Value);
            var dto = input.Brand;
            item.Name = dto.Name;
        }

        protected virtual async Task CreateBrand(CreateOrUpdateBrandInput input)
        {
            var dto = input.Brand.MapTo<Brand>();
            CheckErrors(await _brandManager.CreateSync(dto));
        }


        public async Task<ListResultOutput<BrandListDto>> GetNames()
        {
            var lstBrand = await _brandRepo.GetAll().ToListAsync();
            return new ListResultOutput<BrandListDto>(lstBrand.MapTo<List<BrandListDto>>());
        }
    }
}