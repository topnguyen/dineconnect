﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using System;
using System.Linq;
using Abp.UI;
using DinePlan.DineConnect.Common;
using DinePlan.DineConnect.Utility.Exporter;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using Abp.Domain.Uow;

namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class InventoryCycleTagAppService : DineConnectAppServiceBase, IInventoryCycleTagAppService
    {
        private readonly IExcelExporter _exporter;
        private readonly IInventoryCycleTagManager _inventorycycletagManager;
        private readonly IRepository<InventoryCycleTag> _inventorycycletagRepo;
        private readonly IXmlAndJsonConvertor _xmlandjsonConvertorAppService;
        private readonly IRepository<InventoryCycleLinkWithLocation> _inventoryCycleLinkLocationsRepo;
        private readonly IRepository<LocationGroup> _locationGroupRepo;
        private readonly IRepository<LocationTag> _locationTagRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<MaterialStockCycleLink> _materialStockCycleRepo;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public InventoryCycleTagAppService(IInventoryCycleTagManager inventorycycletagManager,
        IRepository<InventoryCycleTag> inventoryCycleTagRepo,
        IXmlAndJsonConvertor xmlandjsonConvertorAppService,
        IRepository<InventoryCycleLinkWithLocation> inventoryCycleLinkLocationsRepo,
        IRepository<LocationGroup> locationGroupRepo,
        IRepository<LocationTag> locationTagRepo,
        IRepository<Location> locationRepo,
        IRepository<Material> materialRepo,
        IRepository<MaterialStockCycleLink> materialStockCycleRepo,
         IUnitOfWorkManager unitOfWorkManager,
        IExcelExporter exporter)
        {
            _inventorycycletagManager = inventorycycletagManager;
            _inventorycycletagRepo = inventoryCycleTagRepo;
            _exporter = exporter;
            _xmlandjsonConvertorAppService = xmlandjsonConvertorAppService;
            _inventoryCycleLinkLocationsRepo = inventoryCycleLinkLocationsRepo;
            _locationGroupRepo = locationGroupRepo;
            _locationTagRepo = locationTagRepo;
            _locationRepo = locationRepo;
            _materialRepo = materialRepo;
            _materialStockCycleRepo = materialStockCycleRepo;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task<PagedResultOutput<InventoryCycleTagListDto>> GetAll(GetInventoryCycleTagInput input)
        {
            var allItems = _inventorycycletagRepo
                    .GetAll()
                    .WhereIf(
                            !input.Filter.IsNullOrEmpty(),
                            p => p.InventoryCycleTagCode.Contains(input.Filter)
                    );
            var sortMenuItems = await allItems
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<InventoryCycleTagListDto>>();

            var allItemCount = await allItems.CountAsync();
            foreach (var lst in allListDtos)
            {
                if (lst.InventoryCycleMode == 0)
                    lst.InventoryCycleModeRefName = "Daily";
                else if (lst.InventoryCycleMode == 1)
                    lst.InventoryCycleModeRefName = "Weekly";
                else if (lst.InventoryCycleMode == 2)
                    lst.InventoryCycleModeRefName = "Monthly";
                else if (lst.InventoryCycleMode == 3)
                    lst.InventoryCycleModeRefName = "Yearly";
            }
            return new PagedResultOutput<InventoryCycleTagListDto>(
                    allItemCount,
                    allListDtos
                    );
        }

        public async Task<GetInventoryCycleTagForEditOutput> GetInventoryCycleTagForEdit(NullableIdInput input)
        {
            try
            {
                InventoryCycleTagEditDto editDto;
                bool doesApplicableToAllLocations = false;
                List<int> cycleData = new List<int>();

                if (input.Id.HasValue)
                {
                    var hDto = await _inventorycycletagRepo.GetAsync(input.Id.Value);
                    editDto = hDto.MapTo<InventoryCycleTagEditDto>();

                    var editDetails = await _inventoryCycleLinkLocationsRepo.GetAllListAsync(t => t.InventoryCycleTagRefId == editDto.Id);
                    if (editDetails.Count == 0)
                        editDto.DoesApplicableToAllLocations = true;
                    else
                    {
                        editDto.DoesApplicableToAllLocations = false;
                        List<int> locationGroupRefIds = editDetails.Where(t => t.LocationGroupRefId.HasValue).Select(t => t.LocationGroupRefId.Value).ToList();
                        if (locationGroupRefIds.Count > 0)
                        {
                            var locGroups = await _locationGroupRepo.GetAllListAsync(t => locationGroupRefIds.Contains(t.Id));
                            editDto.LocationGroups = locGroups.MapTo<List<SimpleLocationGroupDto>>();
                        }
                        else
                            editDto.LocationGroups = new List<SimpleLocationGroupDto>();

                        List<int> locationTagRefIds = editDetails.Where(t => t.LocationTagRefId.HasValue).Select(t => t.LocationTagRefId.Value).ToList();
                        if (locationTagRefIds.Count > 0)
                            editDto.LocationTags = await _locationTagRepo.GetAllListAsync(t => locationTagRefIds.Contains(t.Id));
                        else
                            editDto.LocationTags = new List<LocationTag>();

                        List<int> locationRefIds = editDetails.Where(t => t.LocationRefId.HasValue).Select(t => t.LocationRefId.Value).ToList();
                        if (locationRefIds.Count > 0)
                        {
                            var locs = await _locationRepo.GetAllListAsync(t => locationRefIds.Contains(t.Id));
                            editDto.Locations = locs.MapTo<List<SimpleLocationDto>>();
                        }
                        else
                            editDto.Locations = new List<SimpleLocationDto>();
                    }

                    cycleData = _xmlandjsonConvertorAppService.DeSerializeFromJSON<List<int>>(editDto.InventoryModeSchedule);
                }
                else
                {
                    editDto = new InventoryCycleTagEditDto();
                    editDto.DoesApplicableToAllLocations = true;
                }

                return new GetInventoryCycleTagForEditOutput
                {
                    InventoryCycleTag = editDto,
                    CycleData = cycleData,
                };
            }
            catch (Exception ex)
            {

                throw new UserFriendlyException(ex.Message);
            }

        }

        public bool ExistAllServerLevelBusinessRules(CreateOrUpdateInventoryCycleTagInput input)
        {
            if (input.InventoryCycleTag.InventoryCycleMode == 0)
            {
                if (input.CycleData.Count > 0)
                {
                    throw new UserFriendlyException("ScheduleDataNotNeededForDailyMode");
                }
            }
            else
            {
                input.CycleData = input.CycleData.OrderBy(t => t).ToList();
                if (input.CycleData == null)
                {
                    throw new UserFriendlyException("ScheduleDataMissing");
                }
            }

            return true;

        }

        public async Task CreateOrUpdateInventoryCycleTag(CreateOrUpdateInventoryCycleTagInput input)
        {
            if (ExistAllServerLevelBusinessRules(input))
            {
                if (input.InventoryCycleTag.Id.HasValue)
                {
                    await UpdateInventoryCycleTag(input);
                }
                else
                {
                    await CreateInventoryCycleTag(input);
                }
            }
        }

        public async Task DeleteInventoryCycleTag(IdInput input)
        {
            var mappedMaterialCount = await _materialStockCycleRepo.GetAllListAsync(t => t.InventoryCycleTagRefId == input.Id);
            if (mappedMaterialCount.Count > 0)
            {
                throw new UserFriendlyException("This Stock Cycle Mapped With " + mappedMaterialCount.Count + " Material(s). So you can not delete this stock cycle.");
            }
            var delList = await _inventoryCycleLinkLocationsRepo.GetAllListAsync(t => t.InventoryCycleTagRefId == input.Id);
            foreach (var lst in delList)
            {
                await _inventoryCycleLinkLocationsRepo.DeleteAsync(lst.Id);
            }
            await _inventorycycletagRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateInventoryCycleTag(CreateOrUpdateInventoryCycleTagInput input)
        {
            var item = await _inventorycycletagRepo.GetAsync(input.InventoryCycleTag.Id.Value);
            var dto = input.InventoryCycleTag;
            string templateJson = _xmlandjsonConvertorAppService.SerializeToJSON(input.CycleData);
            dto.InventoryModeSchedule = templateJson;
            item.InventoryCycleTagCode = dto.InventoryCycleTagCode;
            item.InventoryCycleTagDescription = dto.InventoryCycleTagDescription;
            item.InventoryCycleMode = dto.InventoryCycleMode;
            item.InventoryModeSchedule = dto.InventoryModeSchedule;
            CheckErrors(await _inventorycycletagManager.CreateSync(item));

            #region Location Group Details
            {
                List<int> arrLocationGroupToBeRetained = new List<int>();
                var locationGroups = input.InventoryCycleTag.LocationGroups;
                if (locationGroups.Count > 0)
                {
                    foreach (var lst in locationGroups)
                    {
                        arrLocationGroupToBeRetained.Add(lst.Id);
                        using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                        {
                            var existRecord = await _inventoryCycleLinkLocationsRepo.FirstOrDefaultAsync(t => t.InventoryCycleTagRefId == item.Id && t.LocationGroupRefId.HasValue && t.LocationGroupRefId.Value == lst.Id);
                            if (existRecord == null)
                            {
                                InventoryCycleLinkWithLocation newDetailDto = new InventoryCycleLinkWithLocation
                                {
                                    InventoryCycleTagRefId = item.Id,
                                    LocationGroupRefId = lst.Id
                                };
                                await _inventoryCycleLinkLocationsRepo.InsertOrUpdateAndGetIdAsync(newDetailDto);
                            }
                            else
                            {
                                if (existRecord.DeletionTime.HasValue)
                                {
                                    existRecord.DeletionTime = null;
                                    existRecord.DeleterUserId = null;
                                    existRecord.IsDeleted = false;
                                }
                                await _inventoryCycleLinkLocationsRepo.InsertOrUpdateAndGetIdAsync(existRecord);
                            }
                        }
                      
                    }
                }
                var delList = await _inventoryCycleLinkLocationsRepo.GetAllListAsync(t => t.InventoryCycleTagRefId == item.Id && t.LocationGroupRefId.HasValue && !arrLocationGroupToBeRetained.Contains(t.LocationGroupRefId.Value));
                foreach (var lst in delList)
                {
                    await _inventoryCycleLinkLocationsRepo.DeleteAsync(lst.Id);
                }
            }
            #endregion

            #region Location Tag Details
            {
                List<int> arrLocationTagToBeRetained = new List<int>();
                var locationTags = input.InventoryCycleTag.LocationTags;
                if (locationTags.Count > 0)
                {
                    foreach (var lst in locationTags)
                    {
                        arrLocationTagToBeRetained.Add(lst.Id);
                        using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                        {
                            var existRecord = await _inventoryCycleLinkLocationsRepo.FirstOrDefaultAsync(t => t.InventoryCycleTagRefId == item.Id && t.LocationTagRefId.HasValue && t.LocationTagRefId.Value == lst.Id);
                            if (existRecord == null)
                            {
                                InventoryCycleLinkWithLocation newDetailDto = new InventoryCycleLinkWithLocation
                                {
                                    InventoryCycleTagRefId = item.Id,
                                    LocationTagRefId = lst.Id
                                };
                                await _inventoryCycleLinkLocationsRepo.InsertOrUpdateAndGetIdAsync(newDetailDto);
                            }
                            else
                            {
                                if (existRecord.DeletionTime.HasValue)
                                {
                                    existRecord.DeletionTime = null;
                                    existRecord.DeleterUserId = null;
                                    existRecord.IsDeleted = false;
                                }
                                await _inventoryCycleLinkLocationsRepo.InsertOrUpdateAndGetIdAsync(existRecord);
                            }
                        }
                    }
                }
                var delList = await _inventoryCycleLinkLocationsRepo.GetAllListAsync(t => t.InventoryCycleTagRefId == item.Id && t.LocationTagRefId.HasValue && !arrLocationTagToBeRetained.Contains(t.LocationTagRefId.Value));
                foreach (var lst in delList)
                {
                    await _inventoryCycleLinkLocationsRepo.DeleteAsync(lst.Id);
                }
            }
            #endregion

            #region Location Details
            {
                List<int> arrLocationToBeRetained = new List<int>();
                var locations = input.InventoryCycleTag.Locations;
                if (locations.Count > 0)
                {
                    foreach (var lst in locations)
                    {
                        arrLocationToBeRetained.Add(lst.Id);
                        using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                        {
                            var existRecord = await _inventoryCycleLinkLocationsRepo.FirstOrDefaultAsync(t => t.InventoryCycleTagRefId == item.Id && t.LocationRefId.HasValue && t.LocationRefId.Value == lst.Id);
                            if (existRecord == null)
                            {
                                InventoryCycleLinkWithLocation newDetailDto = new InventoryCycleLinkWithLocation
                                {
                                    InventoryCycleTagRefId = item.Id,
                                    LocationRefId = lst.Id
                                };
                                await _inventoryCycleLinkLocationsRepo.InsertOrUpdateAndGetIdAsync(newDetailDto);
                            }
                            else
                            {
                                if (existRecord.DeletionTime.HasValue)
                                {
                                    existRecord.DeletionTime = null;
                                    existRecord.DeleterUserId = null;
                                    existRecord.IsDeleted = false;
                                }
                                await _inventoryCycleLinkLocationsRepo.InsertOrUpdateAndGetIdAsync(existRecord);
                            }
                        }
                    }
                }
                var delList = await _inventoryCycleLinkLocationsRepo.GetAllListAsync(t => t.InventoryCycleTagRefId == item.Id && t.LocationRefId.HasValue && !arrLocationToBeRetained.Contains(t.LocationRefId.Value));
                foreach (var lst in delList)
                {
                    await _inventoryCycleLinkLocationsRepo.DeleteAsync(lst.Id);
                }
            }
            #endregion
        }

        protected virtual async Task CreateInventoryCycleTag(CreateOrUpdateInventoryCycleTagInput input)
        {

            var dto = input.InventoryCycleTag.MapTo<InventoryCycleTag>();
            string templateJson = _xmlandjsonConvertorAppService.SerializeToJSON(input.CycleData);
            dto.InventoryModeSchedule = templateJson;
            CheckErrors(await _inventorycycletagManager.CreateSync(dto));

            #region Location Group Details
            {
                List<int> arrLocationGroupToBeRetained = new List<int>();
                var locationGroups = input.InventoryCycleTag.LocationGroups;
                if (locationGroups.Count > 0)
                {
                    foreach (var lst in locationGroups)
                    {
                        arrLocationGroupToBeRetained.Add(lst.Id);
                        var existRecord = await _inventoryCycleLinkLocationsRepo.FirstOrDefaultAsync(t => t.InventoryCycleTagRefId == dto.Id && t.LocationGroupRefId.HasValue && t.LocationGroupRefId.Value == lst.Id);
                        if (existRecord == null)
                        {
                            InventoryCycleLinkWithLocation newDetailDto = new InventoryCycleLinkWithLocation
                            {
                                InventoryCycleTagRefId = dto.Id,
                                LocationGroupRefId = lst.Id
                            };
                            await _inventoryCycleLinkLocationsRepo.InsertOrUpdateAndGetIdAsync(newDetailDto);
                        }
                    }
                }
                var delList = await _inventoryCycleLinkLocationsRepo.GetAllListAsync(t => t.InventoryCycleTagRefId == dto.Id && t.LocationGroupRefId.HasValue && !arrLocationGroupToBeRetained.Contains(t.LocationGroupRefId.Value));
                foreach (var lst in delList)
                {
                    await _inventoryCycleLinkLocationsRepo.DeleteAsync(lst.Id);
                }
            }
            #endregion

            #region Location Tag Details
            {
                List<int> arrLocationTagToBeRetained = new List<int>();
                var locationTags = input.InventoryCycleTag.LocationTags;
                if (locationTags.Count > 0)
                {
                    foreach (var lst in locationTags)
                    {
                        arrLocationTagToBeRetained.Add(lst.Id);
                        var existRecord = await _inventoryCycleLinkLocationsRepo.FirstOrDefaultAsync(t => t.InventoryCycleTagRefId == dto.Id && t.LocationTagRefId.HasValue && t.LocationTagRefId.Value == lst.Id);
                        if (existRecord == null)
                        {
                            InventoryCycleLinkWithLocation newDetailDto = new InventoryCycleLinkWithLocation
                            {
                                InventoryCycleTagRefId = dto.Id,
                                LocationTagRefId = lst.Id
                            };
                            await _inventoryCycleLinkLocationsRepo.InsertOrUpdateAndGetIdAsync(newDetailDto);
                        }
                    }
                }
                var delList = await _inventoryCycleLinkLocationsRepo.GetAllListAsync(t => t.InventoryCycleTagRefId == dto.Id && t.LocationTagRefId.HasValue && !arrLocationTagToBeRetained.Contains(t.LocationTagRefId.Value));
                foreach (var lst in delList)
                {
                    await _inventoryCycleLinkLocationsRepo.DeleteAsync(lst.Id);
                }
            }
            #endregion

            #region Location Details
            {
                List<int> arrLocationToBeRetained = new List<int>();
                var locations = input.InventoryCycleTag.Locations;
                if (locations.Count > 0)
                {
                    foreach (var lst in locations)
                    {
                        arrLocationToBeRetained.Add(lst.Id);
                        var existRecord = await _inventoryCycleLinkLocationsRepo.FirstOrDefaultAsync(t => t.InventoryCycleTagRefId == dto.Id && t.LocationRefId.HasValue && t.LocationRefId.Value == lst.Id);
                        if (existRecord == null)
                        {
                            InventoryCycleLinkWithLocation newDetailDto = new InventoryCycleLinkWithLocation
                            {
                                InventoryCycleTagRefId = dto.Id,
                                LocationRefId = lst.Id
                            };
                            await _inventoryCycleLinkLocationsRepo.InsertOrUpdateAndGetIdAsync(newDetailDto);
                        }
                    }
                }
                var delList = await _inventoryCycleLinkLocationsRepo.GetAllListAsync(t => t.InventoryCycleTagRefId == dto.Id && t.LocationRefId.HasValue && !arrLocationToBeRetained.Contains(t.LocationRefId.Value));
                foreach (var lst in delList)
                {
                    await _inventoryCycleLinkLocationsRepo.DeleteAsync(lst.Id);
                }
            }
            #endregion


        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetStockCycleForCombobox()
        {
            List<ComboboxItemDto> retList = new List<ComboboxItemDto>();

            string enumstring;
            Array EnumValues = System.Enum.GetValues(typeof(StockCycle));
            foreach (int EnumValue in EnumValues)
            {
                enumstring = Enum.GetName(typeof(StockCycle), EnumValue);
                retList.Add(new ComboboxItemDto { Value = EnumValue.ToString(), DisplayText = enumstring });
            }

            return
                         new ListResultOutput<ComboboxItemDto>(
                                 retList.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText)).ToList());
        }


        public async Task<FileDto> GetAllToExcel()
        {

            var allList = await _inventorycycletagRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<InventoryCycleTagListDto>>();
            var baseE = new BaseExportObject()
            {
                ExportObject = allListDtos,
                ColumnNames = new string[] { "Id" }
            };
            return _exporter.ExportToFile(baseE, L("InventoryCycleTag"));

        }

    }
}

