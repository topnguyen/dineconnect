﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using AutoMapper;
using Castle.Core.Logging;
using DinePlan.DineConnect.Common;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.DayClose;
using DinePlan.DineConnect.Connect.DayClose.Dto;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Job.House;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Utility.Exporter;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class MaterialAppService : DineConnectAppServiceBase, IMaterialAppService
    {
        private readonly IRepository<AdjustmentDetail> _adjustmentdetailRepo;
        private readonly IRepository<Adjustment> _adjustmentRepo;
        private readonly IRepository<MaterialBrandsLink> _brandlinkRepo;
        private readonly IRepository<Brand> _brandReop;
        private readonly IConnectReportAppService _connectReportAppService;
        private readonly IRepository<CustomerMaterial> _customermaterialRepo;
        private readonly IRepository<CustomerTagDefinition> _customertagdefinitionRepo;
        private readonly IRepository<CustomerTagMaterialPrice> _customertagmaterialpriceRepo;
        private readonly IRepository<DayCloseTemplate> _daycloseTemplateRepo;
        private readonly IRepository<Department> _departmentRepo;
        private readonly IExcelExporter _excelExporter;
        private readonly IRepository<InventoryCycleTag> _inventorycycletagRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<MaterialBarCode> _materialbarcodeRepo;
        private readonly IMaterialGroupCategoryAppService _materialgroupcategoryAppService;
        private readonly IRepository<MaterialGroupCategory> _materialGroupCategoryRepo;
        private readonly IRepository<MaterialGroup> _materialgroupRepo;
        private readonly IRepository<MaterialIngredient> _materialingredientRepo;
        private readonly IRepository<MaterialLedger> _materialLedgerRepo;
        private readonly IRepository<MaterialLocationWiseStock> _materiallocationwisestockRepo;
        private readonly IMaterialManager _materialManager;
        private readonly IRepository<MaterialMenuMapping> _materialmenumappingRepo;
        private readonly IRepository<MaterialRecipeTypes> _materialrecipetypeRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<MaterialStockCycleLink> _materialStockCycleLinkRepo;
        private readonly IRepository<MaterialUnitsLink> _materialUnitLinkRepo;
        private readonly IRepository<MenuBarCode> _menubarcodeRepo;
        private readonly IRepository<MenuItemPortion> _menuitemportionRepo;
        private readonly IRepository<MenuItem> _menuitemRepo;
        private readonly IRepository<MenuItemWastageDetail> _menuitemwastageDetailRepo;
        private readonly IRepository<MenuItemWastage> _menuitemwastageRepo;
        private readonly IRepository<MenuMappingDepartment> _menumappingDepartmentRepo;
        private readonly IRepository<RecipeIngredient> _recipeIngredientManager;
        private readonly IRepository<SupplierMaterial> _suppliermaterialRepo;
        private readonly IRepository<Supplier> _supplierRepo;
        private readonly ITemplateAppService _templateAppService;
        private readonly IUnitConversionAppService _unitConversionAppService;
        private readonly IRepository<UnitConversion> _unitconversionRepo;
        private readonly IRepository<UnitConversionVsSupplier> _unitConversionVsSupplierRepo;
        private readonly IRepository<MaterialUnitsLink> _unitlinkRepo;
        private readonly IRepository<Unit> _unitRepo;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IXmlAndJsonConvertor _xmlandjsonConvertorAppService;
        private readonly SettingManager _settingManager;
        private readonly IBackgroundJobManager _bgm;
        private readonly IRepository<LocationWiseMaterialRateView> _locationMaterialRateViewRepo;
        private readonly IRepository<Yield> _yieldRepo;
        private readonly IRepository<YieldInput> _yieldinputRepo;
        private readonly IRepository<YieldOutput> _yieldoutputRepo;
        private readonly IRepository<Issue> _issueRepo;
        private readonly IRepository<IssueRecipeDetail> _issuerecipedetailRepo;
        private readonly IRepository<IssueDetail> _issueDetailRepo;
        private readonly IMaterialMenuMappingListExcelExporter _materialmenumappingExporter;
        private readonly IBackgroundJobManager _bgmDayClose;
        private readonly IDayCloseAppService _dayCloseService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<InterTransfer> _intertransferRepo;
        private readonly IRepository<InterTransferDetail> _intertransferDetailRepo;
        private readonly IRepository<PurchaseOrder> _purchaseOrderRepo;
        private readonly ILogger _logger;

        public MaterialAppService(IMaterialManager materialManager,
            IRepository<Material> materialRepo,
            IExcelExporter excelExporter,
            IRepository<MaterialUnitsLink> materialunitlinkManager,
            IRepository<MaterialBrandsLink> materialbrandlinkManager,
            IRepository<RecipeIngredient> recipeIngredient
            , IRepository<Unit> unitRepo, IRepository<Brand> brandRepo,
            IRepository<MaterialGroupCategory> materialGroupCategory,
            IRepository<MaterialGroup> materialgroupRepo,
            IRepository<MaterialLedger> ledgerRepo, IRepository<Location> locationRepo,
            IRepository<MaterialLocationWiseStock> materiallocationwisestockRepo,
            IRepository<SupplierMaterial> supplierMaterialrepo,
            IRepository<CustomerMaterial> customermaterialRepo,
            IRepository<CustomerTagMaterialPrice> customertagmaterialpriceRepo,
            IRepository<CustomerTagDefinition> customertagdefinitionRepo,
            IRepository<MaterialIngredient> materialingredientRepo,
            IRepository<MaterialRecipeTypes> materialrecipetypeRepo,
            IRepository<MaterialLedger> materialLedgerRepo,
            IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository,
            IConnectReportAppService connectReportAppService,
            IRepository<MaterialMenuMapping> materialmenumappingRepo,
            IRepository<Supplier> supplierRepo,
            IRepository<UnitConversion> unitconversionRepo,
            IRepository<MaterialUnitsLink> materialUnitLinkRepo,
            IUnitConversionAppService unitConversionAppService,
            IRepository<Adjustment> adjustmentRepo,
            IRepository<AdjustmentDetail> adjustmentdetailRepo,
            IXmlAndJsonConvertor xmlandjsonConvertorAppService,
            ITemplateAppService templateAppService,
            IRepository<DayCloseTemplate> daycloseTemplateRepo,
            IRepository<MenuItemWastage> menuitemwastageRepo,
            IRepository<MenuItemWastageDetail> menuitemwastageDetailRepo,
            IRepository<MenuItemPortion> menuitemportionRepo,
            IRepository<MenuItem> menuitemRepo,
            IRepository<MaterialBarCode> materialbarcodeRepo,
            IRepository<MenuBarCode> menubarcodeRepo,
            IMaterialGroupCategoryAppService materialgroupcategoryAppService,
            IRepository<MenuMappingDepartment> menumappingDepartmentRepo,
            IRepository<Department> departmentRepo,
            IRepository<InventoryCycleTag> inventorycycletagRepo,
            IRepository<MaterialStockCycleLink> materialStockCycleRepo,
            SettingManager settingManager,
            IBackgroundJobManager bgm,
            IRepository<LocationWiseMaterialRateView> locationMaterialRateViewRepo,
            IRepository<Yield> yieldRepo,
            IRepository<YieldInput> yieldinputRepo,
            IRepository<YieldOutput> yieldoutputRepo,
            IRepository<Issue> issueRepo,
            IRepository<IssueRecipeDetail> issuerecipedetailRepo,
            IRepository<Request> requestRepo,
            IRepository<ProductionDetail> productiondetailRepo,
            IRepository<IssueDetail> issueDetailRepo,
            IMaterialMenuMappingListExcelExporter materialmenumappingExporter,
            IBackgroundJobManager bgmDayClose,
            IDayCloseAppService dayCloseService,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<UnitConversionVsSupplier> unitConversionVsSupplierRepo,
            IRepository<InterTransfer> intertransferRepo,
            IRepository<InterTransferDetail> intertransferDetailRepo,
            IRepository<PurchaseOrder> purchaseOrderRepo,
            ILogger logger
            )
        {
            //Debug.WriteLine("Material Service Initialisation Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
            _menuitemportionRepo = menuitemportionRepo;
            _menuitemRepo = menuitemRepo;
            _materialManager = materialManager;
            _materialRepo = materialRepo;
            _excelExporter = excelExporter;
            _unitlinkRepo = materialunitlinkManager;
            _brandlinkRepo = materialbrandlinkManager;
            _recipeIngredientManager = recipeIngredient;
            _unitRepo = unitRepo;
            _brandReop = brandRepo;
            _materialGroupCategoryRepo = materialGroupCategory;
            _locationRepo = locationRepo;
            _materiallocationwisestockRepo = materiallocationwisestockRepo;
            _suppliermaterialRepo = supplierMaterialrepo;
            _customermaterialRepo = customermaterialRepo;
            _customertagdefinitionRepo = customertagdefinitionRepo;
            _customertagmaterialpriceRepo = customertagmaterialpriceRepo;
            _materialingredientRepo = materialingredientRepo;
            _materialrecipetypeRepo = materialrecipetypeRepo;
            _materialLedgerRepo = materialLedgerRepo;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _connectReportAppService = connectReportAppService;
            _materialmenumappingRepo = materialmenumappingRepo;
            _supplierRepo = supplierRepo;
            _materialgroupRepo = materialgroupRepo;
            _unitconversionRepo = unitconversionRepo;
            _materialUnitLinkRepo = materialUnitLinkRepo;
            _unitConversionAppService = unitConversionAppService;
            _adjustmentRepo = adjustmentRepo;
            _adjustmentdetailRepo = adjustmentdetailRepo;
            _xmlandjsonConvertorAppService = xmlandjsonConvertorAppService;
            _templateAppService = templateAppService;
            _daycloseTemplateRepo = daycloseTemplateRepo;
            _menuitemwastageRepo = menuitemwastageRepo;
            _menuitemwastageDetailRepo = menuitemwastageDetailRepo;
            _materialbarcodeRepo = materialbarcodeRepo;
            _menubarcodeRepo = menubarcodeRepo;
            _materialgroupcategoryAppService = materialgroupcategoryAppService;
            _menumappingDepartmentRepo = menumappingDepartmentRepo;
            _departmentRepo = departmentRepo;
            _materialStockCycleLinkRepo = materialStockCycleRepo;
            _inventorycycletagRepo = inventorycycletagRepo;
            _settingManager = settingManager;
            _bgm = bgm;
            _locationMaterialRateViewRepo = locationMaterialRateViewRepo;
            _yieldRepo = yieldRepo;
            _yieldinputRepo = yieldinputRepo;
            _yieldoutputRepo = yieldoutputRepo;
            _issueRepo = issueRepo;
            _issueDetailRepo = issueDetailRepo;
            _issuerecipedetailRepo = issuerecipedetailRepo;
            _materialmenumappingExporter = materialmenumappingExporter;
            _bgmDayClose = bgmDayClose;
            _dayCloseService = dayCloseService;
            _unitOfWorkManager = unitOfWorkManager;
            _unitConversionVsSupplierRepo = unitConversionVsSupplierRepo;
            _intertransferRepo = intertransferRepo;
            _intertransferDetailRepo = intertransferDetailRepo;
            _purchaseOrderRepo = purchaseOrderRepo;
            _logger = logger;
            //Debug.WriteLine("Material Service Initialisation End Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
        }

        public async Task<PagedResultDto<MaterialListDto>> GetAll(GetMaterialInput input)
        {
            Debug.WriteLine("Material Service GetAll Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var allItems = _materialRepo.GetAll().Where(i => i.IsDeleted == input.IsDeleted);
                if (!string.IsNullOrEmpty(input.Operation) && input.Operation.Equals("SEARCH"))
                {
                    allItems = _materialRepo
                        .GetAll()
                        .WhereIf(
                            !input.Filter.IsNullOrEmpty(),
                            p => p.MaterialName.Equals(input.Filter)
                        );
                }
                else
                {
                    allItems = _materialRepo
                        .GetAll()
                        .WhereIf(
                            !input.Filter.IsNullOrEmpty(),
                            p => p.MaterialName.Contains(input.Filter)
                        );
                }
                var sortMenuItems = await allItems
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var allListDtos = sortMenuItems.MapTo<List<MaterialListDto>>();

                var allItemCount = await allItems.CountAsync();

                return new PagedResultDto<MaterialListDto>(
                    allItemCount,
                    allListDtos
                    );
            }
            Debug.WriteLine("Material Service GetAll Stop Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
        }

        public async Task<PagedResultDto<MaterialViewDto>> GetView(GetMaterialInput input)
        {
            Debug.WriteLine("Material Service GetView Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var rawName = L("RAW");
                var semiName = L("SEMI");

                var allItems = (from matgroupcategory in _materialGroupCategoryRepo.GetAll()
                                join matgroup in _materialgroupRepo.GetAll().Where(t => t.TenantId == AbpSession.TenantId)
                                    on matgroupcategory.MaterialGroupRefId equals matgroup.Id
                                join mat in _materialRepo.GetAll().Where(i => i.IsDeleted == input.IsDeleted)
                                    on matgroupcategory.Id equals mat.MaterialGroupCategoryRefId
                                join unit in _unitRepo.GetAll()
                                    on mat.DefaultUnitId equals unit.Id
                                join issunit in _unitRepo.GetAll()
                                    on mat.DefaultUnitId equals issunit.Id
                                select new MaterialViewDto
                                {
                                    Id = mat.Id,
                                    Code = mat.MaterialPetName,
                                    MaterialTypeId = mat.MaterialTypeId,
                                    MaterialTypeName = mat.MaterialTypeId == (int)MaterialType.RAW ? rawName : semiName,
                                    MaterialGroupName = matgroup.MaterialGroupName,
                                    MaterialGroupCategoryName = matgroupcategory.MaterialGroupCategoryName,
                                    Barcode = mat.Barcode,
                                    MaterialName = mat.MaterialName,
                                    MaterialPetName = mat.MaterialPetName,
                                    DefaultUnitName = unit.Name,
                                    Uom = unit.Name,
                                    UnitRefId = mat.DefaultUnitId,
                                    IssueUnitId = mat.IssueUnitId,
                                    IssueUnitName = issunit.Name,
                                    UserSerialNumber = mat.UserSerialNumber,
                                    WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                                    OwnPreparation = mat.OwnPreparation,
                                    IsHighValueItem = mat.IsHighValueItem,
                                    CreationTime = mat.CreationTime,
                                    AveragePriceTagRefId = mat.AveragePriceTagRefId,
                                    NoOfMonthAvgTaken = mat.NoOfMonthAvgTaken,
                                    DeletionTime = mat.DeletionTime
                                }).WhereIf(
                        !input.Filter.IsNullOrEmpty(),
                        p => p.MaterialName.Contains(input.Filter) || p.Barcode.Contains(input.Filter)
                    );
                List<MaterialViewDto> sortMenuItems;

                if (input.Operation != null && input.Operation.Equals("FillAll"))
                {
                    sortMenuItems = await allItems
                        .OrderBy(input.Sorting)
                        .ToListAsync();
                }
                else
                {
                    sortMenuItems = await allItems
                        .OrderBy(input.Sorting)
                        .PageBy(input)
                        .ToListAsync();
                }

                var allListDtos = sortMenuItems.MapTo<List<MaterialViewDto>>();

                var allItemCount = await allItems.CountAsync();

                int[] arrMaterialRefIds = allListDtos.Select(t => t.Id).ToArray();

                var rsBarcodes = await _materialbarcodeRepo.GetAllListAsync(t => arrMaterialRefIds.Contains(t.MaterialRefId));
                foreach (var lst in allListDtos)
                {
                    var matBarcodes = rsBarcodes.Where(t => t.MaterialRefId == lst.Id);
                    if (matBarcodes.Count() > 0)
                    {
                        foreach (var bc in matBarcodes)
                        {
                            lst.Barcode = lst.Barcode + bc.Barcode + " , ";
                        }
                        if (lst.Barcode.Length > 0)
                            lst.Barcode = lst.Barcode.Left(lst.Barcode.Length - 3);
                    }
                    lst.AveragePriceTagRefName = lst.AveragePriceTagRefId == 0 ? "Given_Dates_Average" : lst.AveragePriceTagRefId == 1 ? "Last_Purchase" : lst.AveragePriceTagRefId == 2 ? "Purchase_Avg_Last_" + lst.NoOfMonthAvgTaken + "_Months" : lst.AveragePriceTagRefId == 3 ? "Moving_Average" : "";
                }

                return new PagedResultDto<MaterialViewDto>(
                    allItemCount,
                    allListDtos
                    );
            }
            Debug.WriteLine("Material Service GetView Stop Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
        }

        public async Task<PagedResultDto<MaterialViewDto>> GetMaterialViewWithUnitList(GetMaterialInput input)
        {
            var rawName = L("RAW");
            var semiName = L("SEMI");

            var allItems = (from matgroupcategory in _materialGroupCategoryRepo.GetAll()
                            join matgroup in _materialgroupRepo.GetAll().Where(t => t.TenantId == AbpSession.TenantId)
                                on matgroupcategory.MaterialGroupRefId equals matgroup.Id
                            join mat in _materialRepo.GetAll()
                                on matgroupcategory.Id equals mat.MaterialGroupCategoryRefId
                            join unit in _unitRepo.GetAll()
                                on mat.DefaultUnitId equals unit.Id
                            join issunit in _unitRepo.GetAll()
                                on mat.DefaultUnitId equals issunit.Id
                            select new MaterialViewDto
                            {
                                Id = mat.Id,
                                MaterialTypeId = mat.MaterialTypeId,
                                MaterialTypeName = mat.MaterialTypeId == (int)MaterialType.RAW ? rawName : semiName,
                                MaterialGroupName = matgroup.MaterialGroupName,
                                MaterialGroupCategoryRefId = mat.MaterialGroupCategoryRefId,
                                MaterialGroupCategoryName = matgroupcategory.MaterialGroupCategoryName,
                                Barcode = mat.Barcode,
                                MaterialName = mat.MaterialName,
                                MaterialPetName = mat.MaterialPetName,
                                DefaultUnitName = unit.Name,
                                Uom = unit.Name,
                                UnitRefId = mat.DefaultUnitId,
                                IssueUnitId = mat.IssueUnitId,
                                IssueUnitName = issunit.Name,
                                UserSerialNumber = mat.UserSerialNumber,
                                WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                                OwnPreparation = mat.OwnPreparation,
                                IsHighValueItem = mat.IsHighValueItem,
                                CreationTime = mat.CreationTime,
                                AveragePriceTagRefId = mat.AveragePriceTagRefId,
                                NoOfMonthAvgTaken = mat.NoOfMonthAvgTaken,
                                //Barcodes = mat.Barcodes.MapTo<Collection<MaterialBarCodeEditDto>>(),
                                //AveragePriceTagRefName = mat.AveragePriceTagRefId == 0 ? "Given_Dates_Average" : mat.AveragePriceTagRefId == 1 ? "Last_Purchase" : mat.AveragePriceTagRefId == 2 ? "Purchase_Avg_Last_" + mat.NoOfMonthAvgTaken + "_Months" : mat.AveragePriceTagRefId == 3 ? "Moving_Average" : ""
                            }).WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.MaterialName.Contains(input.Filter) || p.Barcode.Contains(input.Filter)
                );
            List<MaterialViewDto> sortMenuItems;

            if (input.Operation != null && input.Operation.Equals("FillAll"))
            {
                sortMenuItems = await allItems
                    .OrderBy(input.Sorting)
                    .ToListAsync();
            }
            else
            {
                sortMenuItems = await allItems
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();
            }

            var allListDtos = sortMenuItems.MapTo<List<MaterialViewDto>>();

            var allItemCount = await allItems.CountAsync();

            int[] arrMaterialRefIds = allListDtos.Select(t => t.Id).ToArray();

            var rsClStkUnitWiseDetails = await (from matunitlink in _materialUnitLinkRepo.GetAll()
                                                join uiss in _unitRepo.GetAll() on matunitlink.UnitRefId equals uiss.Id
                                                select new ClosingStockUnitWiseDetailViewDto
                                                {
                                                    MaterialRefId = matunitlink.MaterialRefId,
                                                    ClosingStockDetailRefId = 0,
                                                    OnHand = 0,
                                                    UnitRefId = matunitlink.UnitRefId,
                                                    UnitRefName = uiss.Name,
                                                }).ToListAsync();

            foreach (var lst in allListDtos)
            {
                var unitWiseDetails = rsClStkUnitWiseDetails.Where(t => t.MaterialRefId == lst.Id).ToList();
                lst.UnitWiseDetails = unitWiseDetails;
            }

            return new PagedResultDto<MaterialViewDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<List<MaterialViewDto>> GetMaterialWithBarcode(GetMaterialInput input)
        {
            var rawName = L("RAW");
            var semiName = L("SEMI");

            var allItems = (from matgroupcategory in _materialGroupCategoryRepo.GetAll()
                            join matgroup in _materialgroupRepo.GetAll()
                                on matgroupcategory.MaterialGroupRefId equals matgroup.Id
                            join mat in _materialRepo.GetAll()
                                on matgroupcategory.Id equals mat.MaterialGroupCategoryRefId
                            join unit in _unitRepo.GetAll()
                                on mat.DefaultUnitId equals unit.Id
                            join issunit in _unitRepo.GetAll()
                                on mat.DefaultUnitId equals issunit.Id
                            select new MaterialViewDto
                            {
                                Id = mat.Id,
                                MaterialTypeId = mat.MaterialTypeId,
                                MaterialTypeName = mat.MaterialTypeId == (int)MaterialType.RAW ? rawName : semiName,
                                MaterialGroupName = matgroup.MaterialGroupName,
                                MaterialGroupCategoryName = matgroupcategory.MaterialGroupCategoryName,
                                Barcode = mat.Barcode,
                                MaterialName = mat.MaterialName,
                                MaterialPetName = mat.MaterialPetName,
                                DefaultUnitName = unit.Name,
                                Uom = unit.Name,
                                UnitRefId = mat.DefaultUnitId,
                                IssueUnitId = mat.IssueUnitId,
                                IssueUnitName = issunit.Name,
                                UserSerialNumber = mat.UserSerialNumber,
                                WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                                OwnPreparation = mat.OwnPreparation,
                                IsHighValueItem = mat.IsHighValueItem,
                                CreationTime = mat.CreationTime,
                                AveragePriceTagRefId = mat.AveragePriceTagRefId,
                                NoOfMonthAvgTaken = mat.NoOfMonthAvgTaken,
                                Barcodes = mat.Barcodes.MapTo<Collection<MaterialBarCodeEditDto>>(),
                                AveragePriceTagRefName = mat.AveragePriceTagRefId == 0 ? "Given_Dates_Average" : mat.AveragePriceTagRefId == 1 ? "Last_Purchase" : mat.AveragePriceTagRefId == 2 ? "Purchase_Avg_Last_" + mat.NoOfMonthAvgTaken + "_Months" : mat.AveragePriceTagRefId == 3 ? "Moving_Average" : ""
                            }).WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.MaterialName.Contains(input.Filter)
                );
            List<MaterialViewDto> sortMenuItems;

            sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<MaterialViewDto>>();

            var allItemCount = await allItems.CountAsync();

            return new List<MaterialViewDto>(
                allListDtos
                );
        }

        public async Task<MaterialViewWithBarCode> GetMaterialWithBarcodeListSeparate(GetMaterialInput input)
        {
            var rawName = L("RAW");
            var semiName = L("SEMI");

            var allItems = (from matgroupcategory in _materialGroupCategoryRepo.GetAll()
                            join matgroup in _materialgroupRepo.GetAll()
                                on matgroupcategory.MaterialGroupRefId equals matgroup.Id
                            join mat in _materialRepo.GetAll()
                                on matgroupcategory.Id equals mat.MaterialGroupCategoryRefId
                            join unit in _unitRepo.GetAll()
                                on mat.DefaultUnitId equals unit.Id
                            join issunit in _unitRepo.GetAll()
                                on mat.DefaultUnitId equals issunit.Id
                            select new MaterialViewDto
                            {
                                Id = mat.Id,
                                MaterialTypeId = mat.MaterialTypeId,
                                MaterialTypeName = mat.MaterialTypeId == (int)MaterialType.RAW ? rawName : semiName,
                                MaterialGroupName = matgroup.MaterialGroupName,
                                MaterialGroupCategoryName = matgroupcategory.MaterialGroupCategoryName,
                                Barcode = mat.Barcode,
                                MaterialName = mat.MaterialName,
                                MaterialPetName = mat.MaterialPetName,
                                DefaultUnitName = unit.Name,
                                Uom = unit.Name,
                                UnitRefId = mat.DefaultUnitId,
                                IssueUnitId = mat.IssueUnitId,
                                IssueUnitName = issunit.Name,
                                UserSerialNumber = mat.UserSerialNumber,
                                WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                                OwnPreparation = mat.OwnPreparation,
                                IsHighValueItem = mat.IsHighValueItem,
                                CreationTime = mat.CreationTime
                            }).WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.MaterialName.Contains(input.Filter)
                );
            List<MaterialViewDto> sortMenuItems;

            sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<MaterialViewDto>>();

            var barcodeList = await _materialbarcodeRepo.GetAllListAsync();

            var allItemCount = await allItems.CountAsync();

            return new MaterialViewWithBarCode
            {
                MaterialViewList = allListDtos,
                BarcodeList = barcodeList.MapTo<List<MaterialBarCodeListDto>>()
            };
        }

        public async Task<BarCodeListDto> GetBarcodesForMaterial()
        {
            var barcodeList = await _materialbarcodeRepo.GetAllListAsync();

            BarCodeListDto output = new BarCodeListDto();

            output.BarcodeList = barcodeList.MapTo<List<MaterialBarCodeListDto>>();

            var isMaterialCodeTreatedAsBarCode = await _settingManager.GetSettingValueAsync<bool>(AppSettings.HouseSettings.IsMaterialCodeTreatedAsBarCode);

            if (isMaterialCodeTreatedAsBarCode)
            {
                var lstMaterial = await _materialRepo.GetAllListAsync();
                foreach (var lst in lstMaterial)
                {
                    output.BarcodeList.Add(new MaterialBarCodeListDto { MaterialRefId = lst.Id, Barcode = lst.MaterialPetName });
                }
                var blist = output.BarcodeList.Select(t => t.Barcode).ToList();
                var duplicates = output.BarcodeList.GroupBy(x => x.Barcode)
                                 .Where(g => g.Count() > 1)
                                 .ToList();
                if (duplicates.Count > 0)
                {
                    foreach (var lst in duplicates)
                    {
                        string bcode = lst.Key;
                        var dup = lst.ToList();
                        int materialRefId1 = dup[0].MaterialRefId;
                        int materialRefId2 = dup[1].MaterialRefId;
                        var mat1 = await _materialRepo.FirstOrDefaultAsync(t => t.Id == materialRefId1);
                        var mat2 = await _materialRepo.FirstOrDefaultAsync(t => t.Id == materialRefId2);
                        throw new UserFriendlyException(L("DuplicateBarCodeAndMaterialCode", lst.Key, mat1.MaterialName, mat2.MaterialName));
                    }
                }
            }
            return output;
        }

        public async Task<PagedResultDto<MaterialViewDto>> GetViewWithOnHand(EntityDto input)
        {
            var RawType = L("RAW");
            var SemiType = L("SEMI");

            var allItems = (from matgroupcategory in _materialGroupCategoryRepo.GetAll()
                            join mat in _materialRepo.GetAll()
                                on matgroupcategory.Id equals mat.MaterialGroupCategoryRefId
                            join unit in _unitRepo.GetAll()
                                on mat.DefaultUnitId equals unit.Id
                            join uiss in _unitRepo.GetAll()
                                on mat.IssueUnitId equals uiss.Id
                            join matloc in _materiallocationwisestockRepo.GetAll()
                                .Where(mls => mls.LocationRefId == input.Id) on mat.Id equals matloc.MaterialRefId
                            select new MaterialViewDto
                            {
                                Id = mat.Id,
                                MaterialTypeId = mat.MaterialTypeId,
                                MaterialTypeName = mat.MaterialTypeId == 0 ? RawType : SemiType,
                                Barcode = mat.Barcode,
                                MaterialName = mat.MaterialName,
                                MaterialPetName = mat.MaterialPetName,
                                MaterialGroupCategoryName = matgroupcategory.MaterialGroupCategoryName,
                                UserSerialNumber = mat.UserSerialNumber,
                                WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                                OnHand = matloc.CurrentInHand,
                                UnitRefId = unit.Id,
                                DefaultUnitName = unit.Name,
                                IssueUnitId = mat.IssueUnitId,
                                IssueUnitName = uiss.Name
                            });
            List<MaterialViewDto> sortMenuItems;

            sortMenuItems = await allItems.ToListAsync();

            //int loopcnt = 0;
            //foreach (var lst in sortMenuItems)
            //{
            //    string materialTypeName = lst.MaterialTypeId == (int)MaterialType.RAW ? L("RAW") : L("SEMI");
            //    sortMenuItems[loopcnt].MaterialTypeName = materialTypeName;
            //    loopcnt++;
            //}
            var allListDtos = sortMenuItems.MapTo<List<MaterialViewDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultDto<MaterialViewDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<PagedResultDto<MaterialViewDto>> GetRecipeViewWithOnHand(EntityDto input)
        {
            var RawType = L("RAW");
            var SemiType = L("SEMI");

            var allItems = (from matgroupcategory in _materialGroupCategoryRepo.GetAll()
                            join matgroup in _materialgroupRepo.GetAll()
                                on matgroupcategory.MaterialGroupRefId equals matgroup.Id
                            join mat in _materialRepo.GetAll().Where(t => t.MaterialTypeId != 0)
                                on matgroupcategory.Id equals mat.MaterialGroupCategoryRefId
                            join unit in _unitRepo.GetAll()
                                on mat.DefaultUnitId equals unit.Id
                            join uiss in _unitRepo.GetAll()
                                on mat.IssueUnitId equals uiss.Id
                            join matloc in _materiallocationwisestockRepo.GetAll()
                                .Where(mls => mls.LocationRefId == input.Id) on mat.Id equals matloc.MaterialRefId
                            select new MaterialViewDto
                            {
                                Id = mat.Id,
                                MaterialTypeId = mat.MaterialTypeId,
                                MaterialTypeName = mat.MaterialTypeId == 0 ? RawType : SemiType,
                                Barcode = mat.Barcode,
                                MaterialName = mat.MaterialName,
                                MaterialPetName = mat.MaterialPetName,
                                UnitRefId = unit.Id,
                                DefaultUnitName = unit.Name,
                                MaterialGroupCategoryName = matgroupcategory.MaterialGroupCategoryName,
                                UserSerialNumber = mat.UserSerialNumber,
                                WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                                OnHand = matloc.CurrentInHand,
                                IssueUnitId = mat.IssueUnitId,
                                IssueUnitName = uiss.Name
                            });
            List<MaterialViewDto> sortMenuItems;

            sortMenuItems = await allItems.ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<MaterialViewDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultDto<MaterialViewDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<PagedResultDto<MaterialViewDto>> GetMaterialViewWithUom()
        {
            var RawType = L("RAW");
            var SemiType = L("SEMI");
            var allItems = (from matgroupcategory in _materialGroupCategoryRepo.GetAll()
                            join mat in _materialRepo.GetAll()
                                on matgroupcategory.Id equals mat.MaterialGroupCategoryRefId
                            join unit in _unitRepo.GetAll()
                                on mat.DefaultUnitId equals unit.Id
                            join uiss in _unitRepo.GetAll()
                                on mat.IssueUnitId equals uiss.Id
                            select new MaterialViewDto
                            {
                                Id = mat.Id,
                                MaterialTypeId = mat.MaterialTypeId,
                                MaterialTypeName = mat.MaterialTypeId == 0 ? RawType : SemiType,
                                Barcode = mat.Barcode,
                                MaterialName = mat.MaterialName,
                                MaterialPetName = mat.MaterialPetName,
                                DefaultUnitName = unit.Name,
                                Uom = unit.Name,
                                MaterialGroupCategoryName = matgroupcategory.MaterialGroupCategoryName,
                                UserSerialNumber = mat.UserSerialNumber,
                                WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                                UnitRefId = unit.Id,
                                IssueUnitId = mat.IssueUnitId,
                                IssueUnitName = uiss.Name
                            });
            List<MaterialViewDto> sortMenuItems;

            sortMenuItems = await allItems.ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<MaterialViewDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultDto<MaterialViewDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<PagedResultDto<MaterialViewDto>> GetMaterialViewWithOnHandForRecipe(
            InputLocationAndRecipe input)
        {
            var RawType = L("RAW");
            var SemiType = L("SEMI");

            var allItems = (from matgroupcategory in _materialGroupCategoryRepo.GetAll()
                            join mat in _materialRepo.GetAll()
                                on matgroupcategory.Id equals mat.MaterialGroupCategoryRefId
                            join recIng in _materialingredientRepo.GetAll().Where(t => t.RecipeRefId == input.RecipeRefId)
                                on mat.Id equals recIng.MaterialRefId
                            join unit in _unitRepo.GetAll()
                                on mat.DefaultUnitId equals unit.Id
                            join matloc in _materiallocationwisestockRepo.GetAll()
                                .Where(mls => mls.LocationRefId == input.LocationRefId) on mat.Id equals matloc.MaterialRefId
                            select new MaterialViewDto
                            {
                                Id = mat.Id,
                                MaterialTypeId = mat.MaterialTypeId,
                                MaterialTypeName = mat.MaterialTypeId == 0 ? RawType : SemiType,
                                Barcode = mat.Barcode,
                                MaterialName = mat.MaterialName,
                                MaterialPetName = mat.MaterialPetName,
                                DefaultUnitName = unit.Name,
                                MaterialGroupCategoryName = matgroupcategory.MaterialGroupCategoryName,
                                UserSerialNumber = mat.UserSerialNumber,
                                WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                                OnHand = matloc.CurrentInHand,
                                UnitRefId = unit.Id
                            });
            List<MaterialViewDto> sortMenuItems;

            sortMenuItems = await allItems.ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<MaterialViewDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultDto<MaterialViewDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetMaterialInput input)
        {
            input.MaxResultCount = AppConsts.MaxPageSize;
            var RawType = L("RAW");
            var SemiType = L("SEMI");
            var allItems = (from matgroupcategory in _materialGroupCategoryRepo.GetAll()
                            join matgroup in _materialgroupRepo.GetAll().Where(t => t.TenantId == AbpSession.TenantId)
                                on matgroupcategory.MaterialGroupRefId equals matgroup.Id
                            join mat in
                                _materialRepo.GetAll()
                                    .WhereIf(!input.Filter.IsNullOrEmpty(), t => t.MaterialName.Contains(input.Filter))
                                on matgroupcategory.Id equals mat.MaterialGroupCategoryRefId
                            join unit in _unitRepo.GetAll()
                                on mat.DefaultUnitId equals unit.Id
                            join issueUnit in _unitRepo.GetAll()
                                on mat.IssueUnitId equals issueUnit.Id
                            join recUnit in _unitRepo.GetAll()
                                  on mat.ReceivingUnitId equals recUnit.Id
                            join trnUnit in _unitRepo.GetAll()
                                on mat.TransferUnitId equals trnUnit.Id
                            join stkAdjUnit in _unitRepo.GetAll()
                                on mat.TransferUnitId equals stkAdjUnit.Id
                            select new MaterialExportDto
                            {
                                Id = mat.Id,
                                MaterialTypeId = mat.MaterialTypeId,
                                MaterialType = mat.MaterialTypeId == 0 ? RawType : SemiType,
                                Group = matgroup.MaterialGroupName,
                                Category = matgroupcategory.MaterialGroupCategoryName,
                                Name = mat.MaterialName,
                                AliasName = mat.MaterialPetName,
                                Unit = unit.Name,
                                Uom = unit.Name,
                                UnitRefId = mat.DefaultUnitId,
                                TransferUnitId = mat.TransferUnitId,
                                TransferUnitRefName = trnUnit.Name,
                                IssueUnitId = mat.IssueUnitId,
                                IssueUnitRefName = issueUnit.Name,
                                StockAdjustmentUnitId = mat.StockAdjustmentUnitId,
                                StockAdjustmentUnitRefName = stkAdjUnit.Name,
                                ReceivingUnitId = mat.ReceivingUnitId,
                                ReceivingUnitRefName = recUnit.Name,
                                SortOrder = mat.UserSerialNumber,
                                WipeOutStock = mat.WipeOutStockOnClosingDay,
                                OwnPreparation = mat.OwnPreparation,
                                IsHighValueItem = mat.IsHighValueItem,
                                CreationTime = mat.CreationTime,
                                Barcode = mat.Barcode,
                            });

            List<MaterialExportDto> sortMenuItems;

            sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<MaterialExportDto>>();
            var rsBarcodes = await _materialbarcodeRepo.GetAllListAsync();
            foreach (var lst in allListDtos)
            {
                var matBarcodes = rsBarcodes.Where(t => t.MaterialRefId == lst.Id);
                if (matBarcodes.Count() > 0)
                {
                    foreach (var bc in matBarcodes)
                    {
                        lst.Barcode = lst.Barcode + bc.Barcode + " , ";
                    }
                    if (lst.Barcode.Length > 0)
                        lst.Barcode = lst.Barcode.Left(lst.Barcode.Length - 3);
                }
            }

            var baseE = new BaseExportObject
            {
                ExportObject = allListDtos,
                ColumnNames =
                    new[]
                    {
                        "Id", "MaterialType", "Group", "Category", "Name", "AliasName", "Unit", "ReceivingUnitRefName", "IssueUnitRefName", "TransferUnitRefName", "StockAdjustmentUnitRefName", "SortOrder",
                        "OwnPreparation", "WipeOutStock", "Barcode"
                    },
                ColumnDisplayNames = new string[] { "Id", "MaterialType", "Group", "Category", "Name", "AliasName", "Unit", "ReceivingUnit", "DefaultTransactionUnit", "TransferUnit", "StockAdjustmentUnit", "SortOrder",
                        "OwnPreparation", "WipeOutStockOnClosingDay", "Barcode" }
            };
            return _excelExporter.ExportToFile(baseE, L("Material"));
        }

        public async Task<GetMaterialForEditOutput> GetMaterialForEdit(NullableIdInput input)
        {
            MaterialEditDto editDto;
            var editUnitLinkComboDtos = new List<ComboboxItemDto>();
            var editBrandLinkComboDtos = new List<ComboboxItemDto>();
            var editSupplierMaterialDtos = new List<SupplierMaterialEditDto>();
            var editCustomerMaterialDtos = new List<CustomerMaterialEditDto>();
            var editCustomerTagMaterialPriceDtos = new List<CustomerTagMaterialPriceEditDto>();
            var editMaterialIngredientDtos = new List<MaterialIngredientViewDto>();
            MaterialRecipeTypesEditDto editMaterialRecipeTypeDto;
            var editStockCycleDtos = new List<ComboboxItemDto>();
            List<UnitConversionEditDto> editUnitConversionList = new List<UnitConversionEditDto>();

            if (input.Id.HasValue)
            {
                var materialIdToBeEdit = input.Id.Value;
                var hDto = await _materialRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<MaterialEditDto>();

                var unit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == editDto.DefaultUnitId);

                if (unit != null)
                    editDto.Uom = unit.Name;

                editUnitConversionList = await _unitConversionAppService.GetLinkedUnitCoversionForGivenMaterial(new IdInput { Id = materialIdToBeEdit });

                var matRecipe = await _materialrecipetypeRepo.FirstOrDefaultAsync(mm => mm.MaterailRefId == input.Id.Value);

                if (matRecipe != null)
                {
                    var lst = await _materialrecipetypeRepo.FirstOrDefaultAsync(a => a.MaterailRefId == input.Id.Value);

                    var mrtype = await _materialrecipetypeRepo.GetAsync(lst.Id);
                    editMaterialRecipeTypeDto = mrtype.MapTo<MaterialRecipeTypesEditDto>();
                }
                else
                {
                    editMaterialRecipeTypeDto = new MaterialRecipeTypesEditDto();
                }

                //  Get Data From Material Unit Link
                var mUnitListDto = (from mul in _unitlinkRepo.GetAll()
                                    where mul.MaterialRefId == editDto.Id
                                    select new MaterialUnitsLinkEditDto
                                    {
                                        Id = mul.Id,
                                        MaterialRefId = mul.MaterialRefId,
                                        UnitRefId = mul.UnitRefId
                                    }).ToList();

                if (mUnitListDto != null && mUnitListDto.Count() > 0)
                {
                    foreach (var lst in mUnitListDto)
                    {
                        var unitName = _unitRepo.Get(lst.UnitRefId);

                        editUnitLinkComboDtos.Add(new ComboboxItemDto
                        {
                            Value = lst.UnitRefId.ToString(),
                            DisplayText = unitName.Name,
                            IsSelected = true
                        });
                    }
                }

                var mStockCycleList = (from stocycle in _materialStockCycleLinkRepo.GetAll()
                    .Where(t => t.MaterialRefId == materialIdToBeEdit)
                                       select new MaterialStockCycleLinkListDto
                                       {
                                           //LocationGroupRefId = null,
                                           //LocationRefId = null,
                                           MaterialRefId = stocycle.MaterialRefId,
                                           InventoryCycleTagRefId = stocycle.InventoryCycleTagRefId
                                       }).ToList();

                if (mStockCycleList != null && mStockCycleList.Count() > 0)
                {
                    foreach (var lst in mStockCycleList)
                    {
                        var tag =
                            await _inventorycycletagRepo.FirstOrDefaultAsync(t => t.Id == lst.InventoryCycleTagRefId);

                        editStockCycleDtos.Add(new ComboboxItemDto
                        {
                            Value = lst.InventoryCycleTagRefId.ToString(),
                            DisplayText = tag.InventoryCycleTagCode,
                            IsSelected = true
                        });
                    }
                }
                //  Get Data From Material Brand Link
                var mBrandListDto = (from mbl in _brandlinkRepo.GetAll()
                                     where mbl.MaterialRefId == editDto.Id
                                     select new MaterialBrandsLinkEditDto
                                     {
                                         Id = mbl.Id,
                                         MaterialRefId = mbl.MaterialRefId,
                                         BrandRefId = mbl.BrandRefId
                                     }).ToList();

                if (mBrandListDto != null && mBrandListDto.Count() > 0)
                {
                    foreach (var lst in mBrandListDto)
                    {
                        var brandName = _brandReop.GetAll().Where(a => a.Id == lst.BrandRefId).FirstOrDefault();
                        editBrandLinkComboDtos.Add(new ComboboxItemDto
                        {
                            Value = lst.BrandRefId.ToString(),
                            DisplayText = brandName.Name,
                            IsSelected = true
                        });
                    }
                }

                //  Get Data From Supplier Material Link
                editSupplierMaterialDtos = (from smat in _suppliermaterialRepo
                    .GetAll().Where(sm => sm.MaterialRefId == materialIdToBeEdit)
                                            join sup in _supplierRepo.GetAll() on smat.SupplierRefId equals sup.Id
                                            join un in _unitRepo.GetAll() on smat.UnitRefId equals un.Id
                                            select new SupplierMaterialEditDto
                                            {
                                                Id = smat.Id,
                                                SupplierRefId = smat.SupplierRefId,
                                                SupplierRefName = sup.SupplierName,
                                                MaterialPrice = smat.MaterialPrice,
                                                MinimumOrderQuantity = smat.MinimumOrderQuantity,
                                                MaximumOrderQuantity = smat.MaximumOrderQuantity,
                                                UnitRefId = smat.UnitRefId,
                                                MaterialRefId = smat.MaterialRefId,
                                                LastQuoteDate = smat.LastQuoteDate,
                                                LastQuoteRefNo = smat.LastQuoteRefNo,
                                                Uom = un.Name,
                                                YieldPercentage = smat.YieldPercentage,
                                                SupplierMaterialAliasName = smat.SupplierMaterialAliasName
                                            }).ToList();

                //  Get Data From Material Ingredient Link
                editMaterialIngredientDtos =
                    (from r in _materialingredientRepo.GetAll().Where(mig => mig.RecipeRefId == materialIdToBeEdit)
                     join m in _materialRepo.GetAll() on r.MaterialRefId equals m.Id
                     join rec in _materialRepo.GetAll() on r.RecipeRefId equals rec.Id
                     join u in _unitRepo.GetAll() on r.UnitRefId equals u.Id
                     select new MaterialIngredientViewDto
                     {
                         Id = r.Id,
                         RecipeRefId = r.RecipeRefId,
                         RecipeRefName = rec.MaterialName,
                         MaterialRefId = r.MaterialRefId,
                         MaterialRefName = m.MaterialName,
                         MaterialTypeId = m.MaterialTypeId,
                         MaterialUsedQty = r.MaterialUsedQty,
                         UserSerialNumber = r.UserSerialNumber,
                         CreationTime = r.CreationTime,
                         VariationflagForProduction = r.VariationflagForProduction,
                         UnitRefId = u.Id,
                         UnitRefName = u.Name,
                         Ingreditflag = false
                     }).ToList();

                var loopcnt = 0;
                foreach (var lst in editMaterialIngredientDtos)
                {
                    var materialTypeName = lst.MaterialTypeId == (int)MaterialType.RAW ? L("RAW") : L("SEMI");
                    editMaterialIngredientDtos[loopcnt].MaterialTypeName = materialTypeName;
                    loopcnt++;
                }

                //  Get Data From Supplier Material Link
                editCustomerMaterialDtos =
                    (from cusmat in _customermaterialRepo.GetAll().Where(sm => sm.MaterialRefId == materialIdToBeEdit)
                     select new CustomerMaterialEditDto
                     {
                         Id = cusmat.Id,
                         CustomerRefId = cusmat.CustomerRefId,
                         MaterialPrice = cusmat.MaterialPrice,
                         MaterialRefId = cusmat.MaterialRefId,
                         LastQuoteDate = cusmat.LastQuoteDate,
                         LastQuoteRefNo = cusmat.LastQuoteRefNo
                     }).ToList();

                //  Get Data From Customer Tag Material Link
                editCustomerTagMaterialPriceDtos =
                    (from cusmat in
                        _customertagmaterialpriceRepo.GetAll().Where(sm => sm.MaterialRefId == materialIdToBeEdit)
                     select new CustomerTagMaterialPriceEditDto
                     {
                         TagCode = cusmat.TagCode,
                         MaterialPrice = cusmat.MaterialPrice,
                         MaterialRefId = cusmat.MaterialRefId,
                         Id = cusmat.Id
                     }).ToList();
            }
            else
            {
                editDto = new MaterialEditDto();
                editUnitLinkComboDtos = new List<ComboboxItemDto>();
                editBrandLinkComboDtos = new List<ComboboxItemDto>();
                editSupplierMaterialDtos = new List<SupplierMaterialEditDto>();
                editCustomerMaterialDtos = new List<CustomerMaterialEditDto>();
                editCustomerTagMaterialPriceDtos = new List<CustomerTagMaterialPriceEditDto>();
                editMaterialIngredientDtos = new List<MaterialIngredientViewDto>();
                editMaterialRecipeTypeDto = new MaterialRecipeTypesEditDto();
                editStockCycleDtos = new List<ComboboxItemDto>();
                var maxCount = await _materialRepo.CountAsync();
                maxCount++;
                var maxUserSno = maxCount;
                editDto.UserSerialNumber = maxUserSno;

                var isMaterialCodeTreatedAsBarCode = await _settingManager.GetSettingValueAsync<bool>(AppSettings.HouseSettings.IsMaterialCodeTreatedAsBarCode);

                if (isMaterialCodeTreatedAsBarCode)
                {
                    string matCode = maxCount.ToString("D4");
                    do
                    {
                        matCode = maxCount.ToString("D4");
                        var existMatCode = await _materialRepo.FirstOrDefaultAsync(t => t.MaterialPetName.Equals(matCode));
                        if (existMatCode == null)
                            break;
                        else
                        {
                            maxCount++;
                        }
                    } while (true);
                    editDto.MaterialPetName = matCode;
                }
            }

            if (editCustomerTagMaterialPriceDtos.Count() == 0)
            {
                //  Get Data From Customer Tag Material Link
                editCustomerTagMaterialPriceDtos = (from cusmat in _customertagdefinitionRepo.GetAll()
                                                    select new CustomerTagMaterialPriceEditDto
                                                    {
                                                        TagCode = cusmat.TagCode,
                                                        MaterialPrice = 0,
                                                        MaterialRefId = 0,
                                                        Id = 0
                                                    }).ToList();
            }

            return new GetMaterialForEditOutput
            {
                Material = editDto,
                UnitList = editUnitLinkComboDtos,
                UnitConversionList = editUnitConversionList,
                BrandList = editBrandLinkComboDtos,
                SupplierMaterial = editSupplierMaterialDtos,
                CustomerMaterial = editCustomerMaterialDtos,
                CustomerTagMaterialPrice = editCustomerTagMaterialPriceDtos,
                MaterialIngredient = editMaterialIngredientDtos,
                MaterialRecipeType = editMaterialRecipeTypeDto,
                StockCycle = editStockCycleDtos
            };
        }

        public EntityDto GetMaterialIdByName(string materialname)
        {
            var lstMatid = _materialRepo.GetAllList(a => a.MaterialName.Equals(materialname)).ToList().FirstOrDefault();
            if (lstMatid != null)
            {
                return new EntityDto
                {
                    Id = lstMatid.Id
                };
            }
            return new EntityDto
            {
                Id = -1
            };
        }

        public async Task ChangetheSettingOfConvertAsZeroStockIfNegativeStockInClosingStock(MaterialEditDto input)
        {
            var locationDetails = await _locationRepo.GetAllListAsync();
            foreach (var l in locationDetails)
            {
                var existDto = await _materiallocationwisestockRepo.FirstOrDefaultAsync(t => t.LocationRefId == l.Id && t.MaterialRefId == input.Id);
                if (existDto != null)
                {
                    existDto.ConvertAsZeroStockWhenClosingStockNegative = input.ConvertAsZeroStockWhenClosingStockNegative;
                    var retId2 = await _materiallocationwisestockRepo.UpdateAsync(existDto);
                }
            }
        }

        public async Task<EntityDto> CreateOrUpdateMaterial(CreateOrUpdateMaterialInput input)
        {
            bool result = await ExistAllServerLevelBusinessRules(input);
            if (result == false)
            {
                return new EntityDto { Id = -1 };
            }
            EntityDto output = null;
            if (input.Material.Id.HasValue)
            {
                output = await UpdateMaterial(input);
            }
            else
            {
                output = await CreateMaterial(input);
            }

            await _bgm.EnqueueAsync<HouseIdentificationJob, HouseIdJobArgs>(new HouseIdJobArgs
            {
                MaterialId = (int)output.Id
            });

            return output;
        }

        [UnitOfWork]
        public async Task ActivateMaterial(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _materialRepo.GetAsync(input.Id);
                item.IsDeleted = false;
                item.DeletionTime = null;
                item.DeleterUserId = null;
                await _materialRepo.UpdateAsync(item);

                var locationwiseStock =
                   await _materiallocationwisestockRepo.GetAllListAsync(t => t.MaterialRefId == input.Id);
                foreach (var det in locationwiseStock)
                {
                    det.IsDeleted = false;
                    det.DeletionTime = null;
                    det.DeleterUserId = null;
                    await _materiallocationwisestockRepo.UpdateAsync(det);
                }

                var materialUnitLinks = await _materialUnitLinkRepo.GetAllListAsync(t => t.MaterialRefId == input.Id);
                foreach (var det in materialUnitLinks)
                {
                    det.IsDeleted = false;
                    det.DeletionTime = null;
                    det.DeleterUserId = null;
                    await _materialUnitLinkRepo.UpdateAsync(det);
                }

                var supMaterial = await _suppliermaterialRepo.GetAllListAsync(t => t.MaterialRefId == input.Id);
                foreach (var det in supMaterial)
                {
                    det.IsDeleted = false;
                    det.DeletionTime = null;
                    det.DeleterUserId = null;
                    await _suppliermaterialRepo.UpdateAsync(det);
                }

                var rsMatIngrs = await _materialingredientRepo.GetAllListAsync(t => t.MaterialRefId == input.Id);
                foreach (var det in rsMatIngrs)
                {
                    det.IsDeleted = false;
                    det.DeletionTime = null;
                    det.DeleterUserId = null;
                    await _materialingredientRepo.UpdateAsync(det);
                }

                var rsMatmenuMapping = await _materialmenumappingRepo.GetAllListAsync(t => t.MaterialRefId == input.Id);
                foreach (var det in rsMatmenuMapping)
                {
                    det.IsDeleted = false;
                    det.DeletionTime = null;
                    det.DeleterUserId = null;
                    await _materialmenumappingRepo.UpdateAsync(det);
                }

                var rsmatRecipeType = await _materialrecipetypeRepo.GetAllListAsync(t => t.MaterailRefId == input.Id);
                foreach (var det in rsmatRecipeType)
                {
                    det.IsDeleted = false;
                    det.DeletionTime = null;
                    det.DeleterUserId = null;
                    await _materialrecipetypeRepo.UpdateAsync(det);
                }
            }
        }

        [UnitOfWork]
        public async Task DeleteMaterial(EntityDto input)
        {
            var materialLedger = await _materialLedgerRepo.FirstOrDefaultAsync(t => t.MaterialRefId == input.Id);

            if (materialLedger == null)
            {
                var locationwiseStock =
                    await _materiallocationwisestockRepo.GetAllListAsync(t => t.MaterialRefId == input.Id);
                foreach (var det in locationwiseStock)
                {
                    await _materiallocationwisestockRepo.DeleteAsync(det.Id);
                }

                var materialUnitLinks = await _materialUnitLinkRepo.GetAllListAsync(t => t.MaterialRefId == input.Id);
                foreach (var det in materialUnitLinks)
                {
                    await _materialUnitLinkRepo.DeleteAsync(det.Id);
                }

                var supMaterial = await _suppliermaterialRepo.GetAllListAsync(t => t.MaterialRefId == input.Id);
                foreach (var det in supMaterial)
                {
                    await _suppliermaterialRepo.DeleteAsync(det.Id);
                }

                var rsMatIngrs = await _materialingredientRepo.GetAllListAsync(t => t.MaterialRefId == input.Id);
                foreach (var det in rsMatIngrs)
                {
                    await _materialingredientRepo.DeleteAsync(det.Id);
                }

                var rsMatmenuMapping = await _materialmenumappingRepo.GetAllListAsync(t => t.MaterialRefId == input.Id);
                foreach (var det in rsMatmenuMapping)
                {
                    await _materialmenumappingRepo.DeleteAsync(det.Id);
                }

                await _materialrecipetypeRepo.DeleteAsync(t => t.MaterailRefId == input.Id);
                await _materialRepo.DeleteAsync(input.Id);
            }
            else
            {
                throw new UserFriendlyException(
                    L("StockAvailableDeleteNotPossible"));
            }
        }

        public async Task<ListResultDto<MaterialListDto>> GetMaterialNames()
        {
            var lstMaterial = await _materialRepo.GetAll().ToListAsync();
            return new ListResultDto<MaterialListDto>(lstMaterial.MapTo<List<MaterialListDto>>());
        }

        public async Task<MaterialEditDto> GetMaterialForGivenMaterialId(EntityDto input)
        {
            var lstMaterial = await _materialRepo.FirstOrDefaultAsync(t => t.Id == input.Id);
            if (lstMaterial == null)
                return null;
            return lstMaterial.MapTo<MaterialEditDto>();
        }


        public async Task<string> GetMaterialNameForParticularCode(EntityDto input)
        {
            var lstMaterial = await _materialRepo.FirstOrDefaultAsync(t => t.Id == input.Id);
            if (lstMaterial == null)
                return "";
            return lstMaterial.MaterialName;
        }

        public async Task<ListResultDto<MaterialListDto>> GetMaterialRecipeTypesNames()
        {
            var lstMaterial =
                await _materialRepo.GetAll().Where(m => m.MaterialTypeId != (int)MaterialType.RAW).ToListAsync();
            return new ListResultDto<MaterialListDto>(lstMaterial.MapTo<List<MaterialListDto>>());
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetMaterialRecipeTypesForCombobox()
        {
            var lst = await GetMaterialRecipeTypesNames();
            return
                new ListResultDto<ComboboxItemDto>(
                    lst.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.MaterialName)).ToList());
        }

        public async Task<List<MaterialInfoDto>> GetRecipeMaterial(LocationInputDto input)
        {
            var allItems = (from mat in _materialRepo.GetAll()
                .Where(t => t.MaterialTypeId != (int)MaterialType.RAW)
                            join un in _unitRepo.GetAll()
                                on mat.DefaultUnitId equals un.Id
                            join issunit in _unitRepo.GetAll()
                                on mat.IssueUnitId equals issunit.Id
                            join matloc in
                                _materiallocationwisestockRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId)
                                on mat.Id equals matloc.MaterialRefId
                            select new MaterialInfoDto
                            {
                                MaterialRefId = mat.Id,
                                MaterialRefName = mat.MaterialName,
                                MaterialTypeId = mat.MaterialTypeId,
                                IsFractional = mat.IsFractional,
                                IsBranded = mat.IsFractional,
                                IsQuoteNeededForPurchase = mat.IsQuoteNeededForPurchase,
                                MaterialPetName = mat.MaterialPetName,
                                DefaultUnitId = mat.DefaultUnitId,
                                DefaultUnitName = un.Name,
                                IssueUnitId = mat.IssueUnitId,
                                IssueUnitName = issunit.Name,
                                IsNeedtoKeptinFreezer = mat.IsNeedtoKeptinFreezer,
                                IsMfgDateExists = mat.IsMfgDateExists,
                                WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                                CurrentInHand = matloc.CurrentInHand,
                                IsHighValueItem = mat.IsHighValueItem
                            });

            var matRecipeList = await allItems.ToListAsync();

            return matRecipeList;
        }

        public async Task<ListResultDto<MaterialListDto>> GetMaterialForRecIngredient(NullableIdInput input)
        {
            var lstMaterial = await _materialRepo.GetAll().ToListAsync();
            return new ListResultDto<MaterialListDto>(lstMaterial.MapTo<List<MaterialListDto>>());
        }

        public async Task<EntityDto> GetUserSerialNumberMax()
        {
            var lstMaterial = await _materialRepo.GetAll().ToListAsync();
            var maxUserSno = lstMaterial.Count + 1;
            return new EntityDto
            {
                Id = maxUserSno
            };
        }

        public async Task<MaterialLedgerDto> UpdateMaterialLedger(MaterialLedgerDto input)
        {
            var stockDate = DateTime.Parse(input.LedgerDate.ToString()).Date;
            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
            if (loc.HouseTransactionDate.HasValue)
            {
                if (loc.HouseTransactionDate.Value.Date != input.LedgerDate.Date && input.ForceAdjustmentFlag == false)
                {
                    throw new UserFriendlyException(L("MaterialLedger_Account_Date_Error",
                        loc.HouseTransactionDate.Value.ToString("yyyy-MMM-dd"),
                        input.LedgerDate.Date.ToString("yyyy-MMM-dd")));
                }
            }
            else
            {
                throw new UserFriendlyException(L("AccountDateNotSetForLocation"), loc.Name);
            }
            if (loc.DoesDayCloseRunInBackGround == true)
            {
                if (input.DoesBackGroundDayCloseProcess == true)
                {

                }
                else
                {
                    throw new UserFriendlyException("Background Day Close is processing for " + loc.HouseTransactionDate.Value.Date.ToString("yyyy-MMM-dd"));
                }
            }
            decimal closingBalance;

            var NegativeStockAdjustmentAuthorization = false;
            if (PermissionChecker.IsGranted("Pages.Tenant.House.Master.MaterialLedger.NegativeStock") || input.NegativeStockAccepted)
            {
                NegativeStockAdjustmentAuthorization = true;
            }

            var material = await _materialRepo.FirstOrDefaultAsync(t => t.Id == input.MaterialRefId);

            decimal conversionWithDefaultUnit = 1;

            if (input.UnitId != null && input.UnitId != 0)
            {
                var defaultUnitId = material.DefaultUnitId;

                if (defaultUnitId == input.UnitId)
                {

                }
                else
                {
                    decimal conversionFactor;
                    decimal reverseConversion;
                    UnitConversion unitConversion;
                    unitConversion = await _unitconversionRepo.FirstOrDefaultAsync(t => t.BaseUnitId == input.UnitId && t.RefUnitId == defaultUnitId && t.MaterialRefId == input.MaterialRefId); // Check for particular material
                    if (unitConversion == null)
                    {
                        unitConversion = await _unitconversionRepo.FirstOrDefaultAsync(t => t.BaseUnitId == input.UnitId && t.RefUnitId == defaultUnitId && t.MaterialRefId == null); // Check for default conversion
                    }
                    else if (unitConversion != null)
                    {
                        unitConversion = null;
                        var ucList = await _unitconversionRepo.GetAllListAsync(t => t.BaseUnitId == input.UnitId && t.RefUnitId == defaultUnitId && t.MaterialRefId == input.MaterialRefId);
                        if (input.SupplierRefId.HasValue) // Check any particular supplierbased
                        {
                            List<int> arrUcIds = ucList.Select(t => t.Id).ToList();
                            var linkedSupplierRefIds = await _unitConversionVsSupplierRepo.GetAllListAsync(t => arrUcIds.Contains(t.UnitConversionRefId) && t.SupplierRefId == input.SupplierRefId.Value);
                            if (linkedSupplierRefIds.Count > 0)
                            {
                                ucList = ucList.Where(t => t.Id == linkedSupplierRefIds.FirstOrDefault().UnitConversionRefId).ToList();
                                if (ucList.Count == 1)
                                    unitConversion = ucList.FirstOrDefault();
                            }
                            else // This supplier no more link, so take the default UC of material for this Supplier
                            {
                                var linkedSupplierOtherThanThisSupplier = await _unitConversionVsSupplierRepo.GetAllListAsync(t => arrUcIds.Contains(t.UnitConversionRefId));
                                List<int> otherSupplierLinkedUcIds = linkedSupplierOtherThanThisSupplier.Select(t => t.UnitConversionRefId).ToList();
                                ucList = ucList.Where(t => !otherSupplierLinkedUcIds.Contains(t.Id)).ToList();
                                if (ucList.Count == 1)
                                {
                                    unitConversion = ucList.FirstOrDefault();
                                }
                                else if (ucList.Count == 0)
                                {
                                    unitConversion = await _unitconversionRepo.FirstOrDefaultAsync(t => t.BaseUnitId == input.UnitId && t.RefUnitId == defaultUnitId && t.MaterialRefId == null); // Check for default conversion
                                }
                            }
                        }
                        if (ucList.Count > 1)
                        {
                            var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == input.UnitId);
                            var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == defaultUnitId);
                            if (input.SupplierRefId.HasValue)
                            {
                                var supplier = await _supplierRepo.FirstOrDefaultAsync(t => t.Id == input.SupplierRefId);
                                throw new UserFriendlyException(L("MoreThanOneConversionFactorForSupplierMaterial", baseUnit.Name, refUnit.Name, material.MaterialName, supplier.SupplierName));
                            }
                            else
                            {
                                List<int> arrUcIds = ucList.Select(t => t.Id).ToList();
                                var linkedSupplierOtherThanThisSupplier = await _unitConversionVsSupplierRepo.GetAllListAsync(t => arrUcIds.Contains(t.UnitConversionRefId));
                                List<int> otherSupplierLinkedUcIds = linkedSupplierOtherThanThisSupplier.Select(t => t.UnitConversionRefId).ToList();
                                ucList = ucList.Where(t => !otherSupplierLinkedUcIds.Contains(t.Id)).ToList();
                                if (ucList.Count == 1)
                                {
                                    unitConversion = ucList.FirstOrDefault();
                                }
                                else if (ucList.Count == 0)
                                {
                                    unitConversion = await _unitconversionRepo.FirstOrDefaultAsync(t => t.BaseUnitId == input.UnitId && t.RefUnitId == defaultUnitId && t.MaterialRefId == null); // Check for default conversion
                                }
                                else if (ucList.Count > 1)
                                {
                                    throw new UserFriendlyException(L("MoreThanOneConversionFactorForMaterial", baseUnit.Name, refUnit.Name, material.MaterialName));
                                }
                            }
                            unitConversion = ucList.FirstOrDefault();
                        }
                    }


                    if (unitConversion != null)
                    {
                        conversionFactor = unitConversion.Conversion;
                        reverseConversion = Math.Round(1 / unitConversion.Conversion, 14);
                    }
                    else
                    {
                        //unitConversion = await _unitconversionRepo.FirstOrDefaultAsync(t => t.BaseUnitId == defaultUnitId && t.RefUnitId == input.UnitId && t.MaterialRefId==input.MaterialRefId);
                        //if (unitConversion == null)
                        //{
                        //    unitConversion = await _unitconversionRepo.FirstOrDefaultAsync(t => t.BaseUnitId == defaultUnitId && t.RefUnitId == input.UnitId && t.MaterialRefId == null);
                        //}
                        //else
                        //{

                        //}

                        //if (unitConversion == null)
                        //{
                        //    var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == input.UnitId);
                        //    var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == defaultUnitId);

                        //    throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
                        //}
                        unitConversion = await _unitconversionRepo.FirstOrDefaultAsync(t => t.BaseUnitId == defaultUnitId && t.RefUnitId == input.UnitId && t.MaterialRefId == input.MaterialRefId); // Check for particular material
                        if (unitConversion == null)
                        {
                            unitConversion = await _unitconversionRepo.FirstOrDefaultAsync(t => t.BaseUnitId == defaultUnitId && t.RefUnitId == input.UnitId && t.MaterialRefId == null); // Check for default conversion
                        }
                        else if (unitConversion != null)
                        {
                            var ucList = await _unitconversionRepo.GetAllListAsync(t => t.BaseUnitId == defaultUnitId && t.RefUnitId == input.UnitId && t.MaterialRefId == input.MaterialRefId);
                            if (input.SupplierRefId.HasValue) // Check any particular supplierbased
                            {
                                List<int> arrUcIds = ucList.Select(t => t.Id).ToList();
                                var linkedSupplierRefIds = await _unitConversionVsSupplierRepo.GetAllListAsync(t => arrUcIds.Contains(t.UnitConversionRefId) && t.SupplierRefId == input.SupplierRefId.Value);
                                if (linkedSupplierRefIds.Count > 0)
                                {
                                    ucList = ucList.Where(t => t.Id == linkedSupplierRefIds.FirstOrDefault().UnitConversionRefId).ToList();
                                }
                            }
                            if (ucList.Count == 1)
                            {
                                unitConversion = ucList.FirstOrDefault();
                            }
                        }
                        if (unitConversion == null)
                        {
                            var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == input.UnitId);
                            var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == defaultUnitId);

                            throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name) + System.Environment.NewLine + "While Update Material : " + material.MaterialName);
                        }
                        conversionFactor = Math.Round(1 / unitConversion.Conversion, 14); // 1 / unitConversion.Conversion;
                        reverseConversion = unitConversion.Conversion;
                    }
                    if (conversionFactor == 0)
                    {
                        var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == input.UnitId);
                        var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == defaultUnitId);

                        throw new UserFriendlyException(L("ConversionFactorShouldNotBeZero", baseUnit.Name, refUnit.Name));
                    }

                    var cfString = conversionFactor.ToString();
                    if (cfString.Right(1).Equals("0"))
                    {
                        input.OpenBalance = input.OpenBalance * conversionFactor;
                        input.Received = Math.Round(input.Received * conversionFactor, 14);
                        input.Issued = input.Issued * conversionFactor;
                        input.Sales = input.Sales * conversionFactor;
                        input.TransferIn = input.TransferIn * conversionFactor;
                        input.TransferOut = input.TransferOut * conversionFactor;
                        input.Damaged = input.Damaged * conversionFactor;
                        input.SupplierReturn = input.SupplierReturn * conversionFactor;
                        input.ExcessReceived = input.ExcessReceived * conversionFactor;
                        input.Shortage = input.Shortage * conversionFactor;
                        input.Return = input.Return * conversionFactor;
                        input.ClBalance = input.ClBalance * conversionFactor;
                    }
                    else
                    {
                        input.OpenBalance = input.OpenBalance == 0.0m ? input.OpenBalance : (input.OpenBalance / reverseConversion);
                        input.Received = input.Received == 0.0m ? input.Received : Math.Round(input.Received / reverseConversion, 14);
                        input.Issued = input.Issued == 0.0m ? input.Issued : input.Issued / reverseConversion;
                        input.Sales = input.Sales == 0.0m ? input.Sales : input.Sales / reverseConversion;
                        input.TransferIn = input.TransferIn == 0.0m ? input.TransferIn : input.TransferIn / reverseConversion;
                        input.TransferOut = input.TransferOut == 0.0m ? input.TransferOut : input.TransferOut / reverseConversion;
                        input.Damaged = input.Damaged == 0.0m ? input.Damaged : input.Damaged / reverseConversion;
                        input.SupplierReturn = input.SupplierReturn == 0.0m ? input.SupplierReturn : input.SupplierReturn / reverseConversion;
                        input.ExcessReceived = input.ExcessReceived == 0.0m ? input.ExcessReceived : input.ExcessReceived / reverseConversion;
                        input.Shortage = input.Shortage == 0.0m ? input.Shortage : input.Shortage / reverseConversion;
                        input.Return = input.Return == 0.0m ? input.Return : input.Return / reverseConversion;
                        input.ClBalance = input.ClBalance == 0.0m ? input.ClBalance : input.ClBalance / reverseConversion;
                    }
                    conversionWithDefaultUnit = conversionFactor;
                }

            }

            var futureDateExists =
                await
                    _materialLedgerRepo.FirstOrDefaultAsync(
                        t =>
                            DbFunctions.TruncateTime(t.LedgerDate) > stockDate && t.MaterialRefId == input.MaterialRefId &&
                            t.LocationRefId == input.LocationRefId);
            if (futureDateExists != null)
            {
                throw new UserFriendlyException(L("FutureDateExistInLedger", material.MaterialName,
                    futureDateExists.LedgerDate.Value.ToString("yyyy-MMM-dd"), input.LedgerDate.ToString("yyyy-MMM-dd")));
            }

            var editParticularMaterial =
                _materialLedgerRepo.FirstOrDefault(
                    l =>
                        DbFunctions.TruncateTime(l.LedgerDate) == stockDate && l.MaterialRefId == input.MaterialRefId &&
                        l.LocationRefId == input.LocationRefId);

            if (editParticularMaterial == null) //  No Records Found Needs to add material for the Date
            {
                //  Get Close Balance from Yester records

                var newOpenBalance = 0.0m;

                //var dayMonthBefore = input.LedgerDate.AddDays(-15);
                //var latestLedgerDate = (from led in _materialLedgerRepo.GetAll()
                //                        orderby led.LedgerDate descending
                //                        where led.MaterialRefId == input.MaterialRefId && led.LocationRefId == input.LocationRefId && DbFunctions.TruncateTime(led.LedgerDate) <= DbFunctions.TruncateTime(input.LedgerDate) && DbFunctions.TruncateTime(led.LedgerDate) >= DbFunctions.TruncateTime(dayMonthBefore)
                //                        select led.LedgerDate).ToList();
                //if (latestLedgerDate.Count() == 0)
                //{
                //    latestLedgerDate = (from led in _materialLedgerRepo.GetAll()
                //                        orderby led.LedgerDate descending
                //                        where led.MaterialRefId == input.MaterialRefId && led.LocationRefId == input.LocationRefId && DbFunctions.TruncateTime(led.LedgerDate) <= DbFunctions.TruncateTime(input.LedgerDate)
                //                        select led.LedgerDate).ToList();
                //}

                var maxLedgerDate = await GetLatestLedgerForGivenLocationMaterialRefId(new LocationAndMaterialDto { MaterialRefId = input.MaterialRefId, LocationRefId = input.LocationRefId, TransactionDate = input.LedgerDate.Date });

                //var maxLedgerDate = latestLedgerDate.Max();
                if (maxLedgerDate == null)
                {
                    newOpenBalance = input.OpenBalance;
                }
                else
                {
                    //var lastDaysRecord =
                    //    _materialLedgerRepo.FirstOrDefault(
                    //        l =>
                    //            //(l.LedgerDate.Value.Year == maxLedgerDate.Value.Year &&
                    //            // l.LedgerDate.Value.Month == maxLedgerDate.Value.Month &&
                    //            // l.LedgerDate.Value.Day == maxLedgerDate.Value.Day)
                    //            DbFunctions.TruncateTime(l.LedgerDate) == DbFunctions.TruncateTime(maxLedgerDate)
                    //            && l.MaterialRefId == input.MaterialRefId
                    //            && l.LocationRefId == input.LocationRefId);
                    newOpenBalance = maxLedgerDate.ClBalance;
                }

                var item = new MaterialLedger();

                item.OpenBalance = newOpenBalance;
                input.OpenBalance = newOpenBalance;

                closingBalance = (input.OpenBalance + input.Received + input.ExcessReceived + input.Return +
                                  input.TransferIn) -
                                 (input.Issued + input.Sales + input.Damaged + input.SupplierReturn + input.Shortage +
                                  input.TransferOut);

                if (closingBalance < 0)
                {
                    if (NegativeStockAdjustmentAuthorization == false)
                    {
                        throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " + L("ClBalance") + " : " + closingBalance);
                    }
                }

                if (input.Issued < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("Issue"));
                }

                if (input.Sales < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("Sales"));
                }

                if (input.Damaged < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("Damage"));
                }

                if (input.SupplierReturn < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("SupplierReturn"));
                }

                if (input.Shortage < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("Shortage"));
                }

                if (input.TransferOut < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("TransferOut"));
                }

                if (input.TransferIn < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("TransferIn"));
                }

                if (input.Return < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("Return"));
                }

                if (input.ExcessReceived < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("ExcessReceived"));
                }

                if (input.Received < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("Received"));
                }

                if (input.OpenBalance < 0)
                {
                    if (NegativeStockAdjustmentAuthorization == false)
                    {
                        throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " + L("OpenBalance"));
                    }
                }

                item.ClBalance = closingBalance;
                item.Damaged = input.Damaged;
                item.ExcessReceived = input.ExcessReceived;
                item.Issued = input.Issued;
                item.Sales = input.Sales;
                item.LedgerDate = input.LedgerDate.Date;
                item.LocationRefId = input.LocationRefId;
                item.MaterialRefId = input.MaterialRefId;
                item.Received = input.Received;
                item.Return = input.Return;
                item.Shortage = input.Shortage;
                item.SupplierReturn = input.SupplierReturn;
                item.TransferIn = input.TransferIn;
                item.TransferOut = input.TransferOut;

                await _materialLedgerRepo.InsertAsync(item);
            }
            else //   Modify the Existing Records
            {
                var item = await _materialLedgerRepo.GetAsync(editParticularMaterial.Id);
                var dto = input;

                item.Received = item.Received + dto.Received;
                item.Issued = item.Issued + dto.Issued;
                item.Sales = item.Sales + dto.Sales;
                item.Damaged = item.Damaged + dto.Damaged;
                item.SupplierReturn = item.SupplierReturn + dto.SupplierReturn;
                item.ExcessReceived = item.ExcessReceived + dto.ExcessReceived;
                item.Shortage = item.Shortage + dto.Shortage;
                item.Return = item.Return + dto.Return;
                item.TransferIn = item.TransferIn + dto.TransferIn;
                item.TransferOut = item.TransferOut + dto.TransferOut;

                closingBalance = (item.OpenBalance + item.Received + item.ExcessReceived + item.Return + item.TransferIn) -
                                 (item.Issued + item.Sales + item.Damaged + item.SupplierReturn + item.Shortage +
                                  item.TransferOut);

                if (closingBalance < 0)
                {
                    if (NegativeStockAdjustmentAuthorization == false)
                    {
                        throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName));
                    }
                }

                if (item.Issued < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("Issue"));
                }

                if (item.Sales < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("Sales"));
                }

                if (item.Damaged < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("Damage"));
                }

                if (item.SupplierReturn < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("SupplierReturn"));
                }

                if (item.Shortage < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("Shortage"));
                }

                if (item.TransferOut < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("TransferOut"));
                }

                if (input.TransferIn < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("TransferIn"));
                }

                if (item.Return < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("Return"));
                }

                if (item.ExcessReceived < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("ExcessReceived"));
                }

                if (item.Received < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("Received"));
                }

                if (item.OpenBalance < 0)
                {
                    if (NegativeStockAdjustmentAuthorization == false)
                    {
                        throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                        L("OpenBalance"));
                    }
                }

                item.ClBalance = closingBalance;

                await _materialLedgerRepo.InsertOrUpdateAsync(item);
            }

            //  Set Current In Hand Stock

            var setInHand =
                _materiallocationwisestockRepo.FirstOrDefault(
                    l => l.MaterialRefId == input.MaterialRefId && l.LocationRefId == input.LocationRefId);

            //  @@Pending If not records in Material Location Wise Stock , then automatically add into and try once again

            if (setInHand == null)
            {
                throw new UserFriendlyException(L("StockSettingError"));
            }

            if (setInHand.IsActiveInLocation == false)
            {
                throw new UserFriendlyException(material.MaterialName + " " + L("Not") + " " + L("Active") + " " + L("In") + " " + L("Location") + " " + loc.Name);
            }

            var particularMaterial = await _materiallocationwisestockRepo.GetAsync(setInHand.Id);
            particularMaterial.CurrentInHand = closingBalance;

            await _materiallocationwisestockRepo.InsertOrUpdateAndGetIdAsync(particularMaterial);

            await UpdateMaterialAverageDirtyFlag(new LocationAndMaterialDto { MaterialRefId = input.MaterialRefId, LocationRefId = input.LocationRefId });

            return new MaterialLedgerDto { Id = 1, ConversionWithDefaultUnit = conversionWithDefaultUnit }; //  1 is Success , 0 For Failure
        }

        public async Task<DateTime?> GetLatestLedgerDateForGivenLocationMaterialRefId(LocationAndMaterialDto input)
        {
            var dayMonthBefore = input.TransactionDate.AddDays(-15);
            DateTime? latestLedgerDate = _materialLedgerRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId && t.MaterialRefId == input.MaterialRefId && DbFunctions.TruncateTime(t.LedgerDate) >= DbFunctions.TruncateTime(dayMonthBefore) && DbFunctions.TruncateTime(t.LedgerDate) <= DbFunctions.TruncateTime(input.TransactionDate)).DefaultIfEmpty().Max(t => t.LedgerDate);

            if (latestLedgerDate == null)
            {
                dayMonthBefore = input.TransactionDate.AddDays(-30);
                latestLedgerDate = _materialLedgerRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId && t.MaterialRefId == input.MaterialRefId && DbFunctions.TruncateTime(t.LedgerDate) >= DbFunctions.TruncateTime(dayMonthBefore) && DbFunctions.TruncateTime(t.LedgerDate) <= DbFunctions.TruncateTime(input.TransactionDate)).DefaultIfEmpty().Max(t => t.LedgerDate);
                if (latestLedgerDate == null)
                {
                    dayMonthBefore = input.TransactionDate.AddDays(-90);
                    latestLedgerDate = _materialLedgerRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId && t.MaterialRefId == input.MaterialRefId && DbFunctions.TruncateTime(t.LedgerDate) >= DbFunctions.TruncateTime(dayMonthBefore) && DbFunctions.TruncateTime(t.LedgerDate) <= DbFunctions.TruncateTime(input.TransactionDate)).DefaultIfEmpty().Max(t => t.LedgerDate);
                    if (latestLedgerDate == null)
                    {
                        latestLedgerDate = _materialLedgerRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId && t.MaterialRefId == input.MaterialRefId && DbFunctions.TruncateTime(t.LedgerDate) <= DbFunctions.TruncateTime(input.TransactionDate)).DefaultIfEmpty().Max(t => t.LedgerDate);
                        if (latestLedgerDate == null)
                            return null;
                    }
                }
            }
            return latestLedgerDate;
        }

        public async Task<MaterialLedgerListDto> GetLatestLedgerForGivenLocationMaterialRefId(LocationAndMaterialDto input)
        {
            MaterialLedgerListDto latestLedger = new MaterialLedgerListDto();
            var dayMonthBefore = input.TransactionDate.AddDays(-5);
            var latestLedgerDate = await _materialLedgerRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId && t.MaterialRefId == input.MaterialRefId && DbFunctions.TruncateTime(t.LedgerDate) >= DbFunctions.TruncateTime(dayMonthBefore) && DbFunctions.TruncateTime(t.LedgerDate) <= DbFunctions.TruncateTime(input.TransactionDate)).OrderByDescending(t => t.LedgerDate).FirstOrDefaultAsync();

            if (latestLedgerDate == null)
            {
                dayMonthBefore = input.TransactionDate.AddDays(-15);
                latestLedgerDate = await _materialLedgerRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId && t.MaterialRefId == input.MaterialRefId && DbFunctions.TruncateTime(t.LedgerDate) >= DbFunctions.TruncateTime(dayMonthBefore) && DbFunctions.TruncateTime(t.LedgerDate) <= DbFunctions.TruncateTime(input.TransactionDate)).OrderByDescending(t => t.LedgerDate).FirstOrDefaultAsync();
                if (latestLedgerDate == null)
                {
                    dayMonthBefore = input.TransactionDate.AddDays(-30);
                    latestLedgerDate = await _materialLedgerRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId && t.MaterialRefId == input.MaterialRefId && DbFunctions.TruncateTime(t.LedgerDate) >= DbFunctions.TruncateTime(dayMonthBefore) && DbFunctions.TruncateTime(t.LedgerDate) <= DbFunctions.TruncateTime(input.TransactionDate)).OrderByDescending(t => t.LedgerDate).FirstOrDefaultAsync();
                    if (latestLedgerDate == null)
                    {
                        dayMonthBefore = input.TransactionDate.AddDays(-90);
                        latestLedgerDate = await _materialLedgerRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId && t.MaterialRefId == input.MaterialRefId && DbFunctions.TruncateTime(t.LedgerDate) >= DbFunctions.TruncateTime(dayMonthBefore) && DbFunctions.TruncateTime(t.LedgerDate) <= DbFunctions.TruncateTime(input.TransactionDate)).OrderByDescending(t => t.LedgerDate).FirstOrDefaultAsync();
                        if (latestLedgerDate == null)
                        {
                            //    latestLedgerDate = (from led in _materialLedgerRepo.GetAll()
                            //                        orderby led.LedgerDate descending
                            //                        where led.MaterialRefId == input.MaterialRefId && led.LocationRefId == input.LocationRefId && DbFunctions.TruncateTime(led.LedgerDate) <= DbFunctions.TruncateTime(input.LedgerDate)
                            //                        select led.LedgerDate).ToList();

                            latestLedgerDate = await _materialLedgerRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId && t.MaterialRefId == input.MaterialRefId && DbFunctions.TruncateTime(t.LedgerDate) <= DbFunctions.TruncateTime(input.TransactionDate)).OrderByDescending(t => t.LedgerDate).FirstOrDefaultAsync();
                            if (latestLedgerDate == null)
                                return null;
                        }
                    }
                }
            }
            return latestLedgerDate.MapTo<MaterialLedgerListDto>();
        }

        public async Task<List<LocationWiseMaterialLedgerListDto>> GetLatestLedgerForGivenMultipleLocationMultipleMaterialRefId(MultipleLocationAndMultipleMaterialDto input)
        {
            #region Initialisation
            if (input.MaterialRefId > 0)
            {
                input.MaterialRefIdList.Add(input.MaterialRefId);
            }
            input.MaterialRefIdList = input.MaterialRefIdList.Distinct().ToList();

            //if (input.LocationRefId > 0)
            //{
            //    input.LocationRefIdList.Add(input.LocationRefId);
            //}
            List<int> pendingMaterialRefIds = input.MaterialRefIdList;
            //int distinctRecordCountShouldBe = input.LocationRefIdList.Count * input.MaterialRefIdList.Count;
            pendingMaterialRefIds = input.MaterialRefIdList;
            List<RequiredLocationWiseMaterialLedgerDto> requiredList = new List<RequiredLocationWiseMaterialLedgerDto>();
            //foreach (var location in input.LocationRefIdList)
            {
                var tempList = new List<RequiredLocationWiseMaterialLedgerDto>(
                    input.MaterialRefIdList.Select(e => new RequiredLocationWiseMaterialLedgerDto(input.LocationRefId, e, false)).ToList());
                requiredList.AddRange(tempList);
            }
            #endregion

            List<RequiredLocationWiseMaterialLedgerDto> outputLedgers = new List<RequiredLocationWiseMaterialLedgerDto>();
            var queryableMaterialLedger = _materialLedgerRepo.GetAll().Where(t => input.LocationRefId == t.LocationRefId);

            DateTime dayRangeFrom = input.TransactionDate.AddDays(-30);
            DateTime dayRangeTo = input.TransactionDate;

            if (requiredList.Count > 0)
            {
                for (int loopIndex = 1; loopIndex < 5; loopIndex++)
                {
                    //var tempResult2 = await (from matLedger in queryableMaterialLedger
                    //              .Where(t => DbFunctions.TruncateTime(t.LedgerDate) >= DbFunctions.TruncateTime(dayRangeFrom) && DbFunctions.TruncateTime(t.LedgerDate) <= DbFunctions.TruncateTime(dayRangeTo) && input.MaterialRefIdList.Contains(t.MaterialRefId))
                    //                         group matLedger by new { matLedger.LocationRefId, matLedger.MaterialRefId, matLedger.LedgerDate } into g
                    //                         select g).ToListAsync();

                    //var tempResult1 = await (from matLedger in queryableMaterialLedger
                    //              .Where(t => DbFunctions.TruncateTime(t.LedgerDate) >= DbFunctions.TruncateTime(dayRangeFrom) && DbFunctions.TruncateTime(t.LedgerDate) <= DbFunctions.TruncateTime(dayRangeTo))
                    //                        join reqLocMaterial in requiredList.Where(t => t.SuccessFlag == false)
                    //                          on new { matLedger.LocationRefId, matLedger.MaterialRefId }
                    //                        equals new { reqLocMaterial.LocationRefId, reqLocMaterial.MaterialRefId }
                    //                        group matLedger by new { matLedger.LocationRefId, matLedger.MaterialRefId, matLedger.LedgerDate } into g
                    //                       select g).ToListAsync();
                    List<int> arrMaterialRefIds = requiredList.Select(t => t.MaterialRefId).ToList();
                    var tempResult = await (from matLedger in queryableMaterialLedger.Where(t => arrMaterialRefIds.Contains(t.MaterialRefId))
                                      .Where(t => DbFunctions.TruncateTime(t.LedgerDate) >= DbFunctions.TruncateTime(dayRangeFrom) && DbFunctions.TruncateTime(t.LedgerDate) <= DbFunctions.TruncateTime(dayRangeTo))
                                            group matLedger by new { matLedger.LocationRefId, matLedger.MaterialRefId } into g
                                            select new RequiredLocationWiseMaterialLedgerDto
                                            {
                                                LocationRefId = g.Key.LocationRefId,
                                                MaterialRefId = g.Key.MaterialRefId,
                                                SuccessFlag = true,
                                                MaterialLedger = g.OrderByDescending(t => t.LedgerDate).FirstOrDefault()
                                            }).ToListAsync();

                    outputLedgers.AddRange(tempResult);
                    requiredList = FilterTheRequiredMaterial(requiredList, outputLedgers);
                    dayRangeTo = dayRangeFrom.AddDays(-1);
                    dayRangeFrom = dayRangeTo.AddDays(-30);
                    if (requiredList.Count == 0)
                        break;
                }
                if (requiredList.Count > 0)
                {
                    dayRangeTo = dayRangeFrom.AddDays(-1);
                    dayRangeFrom = dayRangeTo.AddDays(-15);
                    List<int> arrMaterialRefIds = requiredList.Select(t => t.MaterialRefId).ToList();
                    var tempResult = await (from matLedger in queryableMaterialLedger.Where(t => arrMaterialRefIds.Contains(t.MaterialRefId))
                                      .Where(t => DbFunctions.TruncateTime(t.LedgerDate) <= DbFunctions.TruncateTime(dayRangeTo))
                                            group matLedger by new { matLedger.LocationRefId, matLedger.MaterialRefId, matLedger.LedgerDate } into g
                                            select new RequiredLocationWiseMaterialLedgerDto
                                            {
                                                LocationRefId = g.Key.LocationRefId,
                                                MaterialRefId = g.Key.MaterialRefId,
                                                SuccessFlag = true,
                                                MaterialLedger = g.OrderByDescending(t => t.LedgerDate).FirstOrDefault()
                                            }).ToListAsync();

                    outputLedgers.AddRange(tempResult);
                    requiredList = FilterTheRequiredMaterial(requiredList, outputLedgers);
                }
            }

            List<LocationWiseMaterialLedgerListDto> resultOutput = new List<LocationWiseMaterialLedgerListDto>();
            foreach (var gp in outputLedgers.GroupBy(t => t.LocationRefId))
            {
                resultOutput.Add(new LocationWiseMaterialLedgerListDto
                {
                    LocationRefId = gp.Key,
                    MaterialLedgerListDtos = gp.Select(t => t.MaterialLedger.MapTo<MaterialLedgerListDto>()).ToList()
                });
            }
            return resultOutput;
        }

        public List<RequiredLocationWiseMaterialLedgerDto> FilterTheRequiredMaterial(List<RequiredLocationWiseMaterialLedgerDto> requiredList, List<RequiredLocationWiseMaterialLedgerDto> processedList)
        {
            var result = requiredList.Where(p => !processedList.Any(x => x.LocationRefId == p.LocationRefId && x.MaterialRefId == p.MaterialRefId)).ToList();
            return result;
        }

        public async Task UpdateMaterialAverageDirtyFlag(LocationAndMaterialDto input)
        {
            List<int> arrMaterialRefIds = new List<int>();
            arrMaterialRefIds.Add(input.MaterialRefId);
            var rsRecipeList = await _materialingredientRepo.GetAllListAsync(t => t.MaterialRefId == input.MaterialRefId);
            if (rsRecipeList != null && rsRecipeList.Any())
                arrMaterialRefIds.AddRange(rsRecipeList.Select(t => t.RecipeRefId));
            List<LocationWiseMaterialRateView> avgPriceList = new List<LocationWiseMaterialRateView>();
            if (input.LocationRefId > 0)
                avgPriceList = await _locationMaterialRateViewRepo.GetAllListAsync(t => arrMaterialRefIds.Contains(t.MaterialRefId) && t.LocationRefId == input.LocationRefId);
            else
                avgPriceList = await _locationMaterialRateViewRepo.GetAllListAsync(t => arrMaterialRefIds.Contains(t.MaterialRefId));

            foreach (var avgPrice in avgPriceList)
            {
                avgPrice.RefreshRequired = true;
                await _locationMaterialRateViewRepo.UpdateAsync(avgPrice);
            }
        }

        public async Task<PagedResultDto<MaterialViewDto>> GetViewWithOnHandAndUsagePattern(LocationWithPriceDto input)
        {
            var allItems = (from matgroupcategory in _materialGroupCategoryRepo.GetAll()
                            join mat in _materialRepo.GetAll() on matgroupcategory.Id equals mat.MaterialGroupCategoryRefId
                            join unit in _unitRepo.GetAll() on mat.DefaultUnitId equals unit.Id
                            join uiss in _unitRepo.GetAll() on mat.IssueUnitId equals uiss.Id
                            join utrn in _unitRepo.GetAll() on mat.TransferUnitId equals utrn.Id
                            join urecd in _unitRepo.GetAll() on mat.ReceivingUnitId equals urecd.Id
                            join ustkadj in _unitRepo.GetAll() on mat.StockAdjustmentUnitId equals ustkadj.Id
                            join matloc in _materiallocationwisestockRepo.GetAll().Where(mls => mls.LocationRefId == input.LocationRefId && mls.IsActiveInLocation == true)
                                on mat.Id equals matloc.MaterialRefId
                            select new MaterialViewDto
                            {
                                Id = mat.Id,
                                MaterialTypeId = mat.MaterialTypeId,
                                MaterialTypeName = "",
                                Barcode = mat.Barcode,
                                MaterialName = mat.MaterialName,
                                MaterialPetName = mat.MaterialPetName,
                                MaterialGroupCategoryName = matgroupcategory.MaterialGroupCategoryName,
                                UserSerialNumber = mat.UserSerialNumber,
                                WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                                OnHand = matloc.CurrentInHand,
                                UnitRefId = mat.DefaultUnitId,
                                DefaultUnitName = unit.Name,
                                IssueUnitName = uiss.Name,
                                IssueUnitId = uiss.Id,
                                ReceivingUnitId = mat.ReceivingUnitId,
                                ReceivingUnitRefName = urecd.Name,
                                TransferUnitId = mat.TransferUnitId,
                                TransferUnitRefName = utrn.Name,
                                StockAdjustmentUnitId = mat.StockAdjustmentUnitId,
                                StockAdjustentUnitRefName = ustkadj.Name
                            });
            List<MaterialViewDto> sortMenuItems;

            var materialRate = new List<MaterialRateViewListDto>();

            if (input.PriceFlagNeeded)
            {
                var loc = await _locationRepo.GetAll().Where(t => t.Id == input.LocationRefId).ToListAsync();
                var locinput = loc.MapTo<List<LocationListDto>>();

                var matView = new GetMaterialRateViewDtoOutput();

                materialRate = matView.MaterialRateView;
            }
            sortMenuItems = await allItems.ToListAsync();

            List<int> arrMaterialRefIds = sortMenuItems.Select(t => t.Id).ToList();

            List<MaterialViewDto> reservedQuantityMaterials = new List<MaterialViewDto>();
            if (input.CalculateYieldReservedQuantity == true || input.CalculateIssueReservedQuantity == true)
            {
                var resultReserved = await GetReservedQuantityFromIssueAndYield(
              new LocationWithMaterialList
              {
                  LocationRefId = input.LocationRefId,
                  CalculateIssueReservedQuantity = input.CalculateIssueReservedQuantity,
                  CalculateYieldReservedQuantity = input.CalculateYieldReservedQuantity,
                  MaterialRefIds = arrMaterialRefIds
              });
                reservedQuantityMaterials = resultReserved.Items.MapTo<List<MaterialViewDto>>();
            }

            foreach (var lst in sortMenuItems)
            {
                if (input.PriceFlagNeeded)
                {
                    var matprice = materialRate.FirstOrDefault(t => t.MaterialRefId == lst.Id);
                    if (matprice != null)
                        lst.Price = matprice.AvgRate;
                }

                var materialTypeName = lst.MaterialTypeId == (int)MaterialType.RAW ? L("RAW") : L("SEMI");
                lst.MaterialTypeName = materialTypeName;

                if (input.CalculateYieldReservedQuantity == true || input.CalculateIssueReservedQuantity == true)
                {
                    var reserved = reservedQuantityMaterials.FirstOrDefault(t => t.Id == lst.Id);
                    if (reserved != null)
                    {
                        lst.ReservedIssueQuantity = reserved.ReservedIssueQuantity;
                        lst.ReservedYieldQuantity = reserved.ReservedYieldQuantity;
                    }
                }
            }

            var allListDtos = sortMenuItems.MapTo<List<MaterialViewDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultDto<MaterialViewDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<PagedResultDto<SimpleMaterialViewDto>> GetSimpleMaterialList(LocationWithPriceDto input)
        {
            var allItems = (from matgroupcategory in _materialGroupCategoryRepo.GetAll()
                            join mat in _materialRepo.GetAll() on matgroupcategory.Id equals mat.MaterialGroupCategoryRefId
                            join unit in _unitRepo.GetAll() on mat.DefaultUnitId equals unit.Id
                            join uiss in _unitRepo.GetAll() on mat.IssueUnitId equals uiss.Id
                            join utrn in _unitRepo.GetAll() on mat.TransferUnitId equals utrn.Id
                            join urecd in _unitRepo.GetAll() on mat.ReceivingUnitId equals urecd.Id
                            join ustkadj in _unitRepo.GetAll() on mat.StockAdjustmentUnitId equals ustkadj.Id
                            join matloc in _materiallocationwisestockRepo.GetAll().Where(mls => mls.LocationRefId == input.LocationRefId && mls.IsActiveInLocation == true)
                                on mat.Id equals matloc.MaterialRefId
                            select new SimpleMaterialViewDto
                            {
                                Id = mat.Id,
                                MaterialTypeId = mat.MaterialTypeId,
                                MaterialTypeName = "",
                                Barcode = mat.Barcode,
                                MaterialName = mat.MaterialName,
                                MaterialPetName = mat.MaterialPetName,
                                MaterialGroupCategoryName = matgroupcategory.MaterialGroupCategoryName,
                                OnHand = matloc.CurrentInHand,
                                UnitRefId = mat.DefaultUnitId,
                                DefaultUnitName = unit.Name,
                                IssueUnitName = uiss.Name,
                                IssueUnitId = uiss.Id,
                                ReceivingUnitId = mat.ReceivingUnitId,
                                ReceivingUnitRefName = urecd.Name,
                                TransferUnitId = mat.TransferUnitId,
                                TransferUnitRefName = utrn.Name,
                                StockAdjustmentUnitId = mat.StockAdjustmentUnitId,
                                StockAdjustentUnitRefName = ustkadj.Name
                            });
            List<SimpleMaterialViewDto> sortMenuItems;

            var materialRate = new List<MaterialRateViewListDto>();

            if (input.PriceFlagNeeded)
            {
                var loc = await _locationRepo.GetAll().Where(t => t.Id == input.LocationRefId).ToListAsync();
                var locinput = loc.MapTo<List<LocationListDto>>();

                var matView = new GetMaterialRateViewDtoOutput();

                materialRate = matView.MaterialRateView;
            }

            sortMenuItems = await allItems.ToListAsync();

            List<int> arrMaterialRefIds = sortMenuItems.Select(t => t.Id).ToList();

            List<MaterialViewDto> reservedQuantityMaterials = new List<MaterialViewDto>();
            if (input.CalculateYieldReservedQuantity == true || input.CalculateIssueReservedQuantity == true)
            {
                var resultReserved = await GetReservedQuantityFromIssueAndYield(
              new LocationWithMaterialList
              {
                  LocationRefId = input.LocationRefId,
                  CalculateIssueReservedQuantity = input.CalculateIssueReservedQuantity,
                  CalculateYieldReservedQuantity = input.CalculateYieldReservedQuantity,
                  MaterialRefIds = arrMaterialRefIds
              });
                reservedQuantityMaterials = resultReserved.Items.MapTo<List<MaterialViewDto>>();
            }

            foreach (var lst in sortMenuItems)
            {
                if (input.PriceFlagNeeded)
                {
                    var matprice = materialRate.FirstOrDefault(t => t.MaterialRefId == lst.Id);
                    if (matprice != null)
                        lst.Price = matprice.AvgRate;
                }

                var materialTypeName = lst.MaterialTypeId == (int)MaterialType.RAW ? L("RAW") : L("SEMI");
                lst.MaterialTypeName = materialTypeName;

                if (input.CalculateYieldReservedQuantity == true || input.CalculateIssueReservedQuantity == true)
                {
                    var reserved = reservedQuantityMaterials.FirstOrDefault(t => t.Id == lst.Id);
                    if (reserved != null)
                    {
                        lst.ReservedIssueQuantity = reserved.ReservedIssueQuantity;
                        lst.ReservedYieldQuantity = reserved.ReservedYieldQuantity;
                    }
                }
            }

            var allItemCount = await allItems.CountAsync();

            return new PagedResultDto<SimpleMaterialViewDto>(
                allItemCount,
                sortMenuItems
                );
        }

        public async Task<PagedResultDto<MaterialViewDto>> GetReservedQuantityFromIssueAndYield(LocationWithMaterialList input)
        {
            List<YieldInputViewDto> editYieldInputDtos = new List<YieldInputViewDto>();
            List<YieldOutputViewDto> editYieldOutputDtos = new List<YieldOutputViewDto>();

            if (input.CalculateYieldReservedQuantity)
            {
                string poStatus = L("Pending");
                DateTime yieldFromDate = DateTime.Today.AddDays(-3);

                var lstpending = await _yieldRepo.GetAll().Where(p => p.LocationRefId == input.LocationRefId && (p.CreationTime >= yieldFromDate || p.IssueTime >= yieldFromDate) && p.Status.Equals(poStatus)).ToListAsync();
                int pendingCount = lstpending.Count();
                List<int> pendingRefIds = lstpending.Select(t => t.Id).ToList();

                editYieldInputDtos = await (from yieldinput in _yieldinputRepo.GetAll()

                                 .Where(a => pendingRefIds.Contains(a.YieldRefId))
                                            join mat in _materialRepo.GetAll()
                                            on yieldinput.InputMaterialRefId equals mat.Id
                                            join un in _unitRepo.GetAll()
                                            on mat.DefaultUnitId equals un.Id
                                            join uiss in _unitRepo.GetAll() on yieldinput.UnitRefId equals uiss.Id
                                            select new YieldInputViewDto
                                            {
                                                Sno = yieldinput.Sno,
                                                YieldRefId = (int)yieldinput.YieldRefId,
                                                InputMaterialRefId = yieldinput.InputMaterialRefId,
                                                MaterialRefName = mat.MaterialName,
                                                InputQty = yieldinput.InputQty,
                                                Uom = un.Name,
                                                UnitRefId = yieldinput.UnitRefId,
                                                UnitRefName = uiss.Name,
                                                DefaultUnitId = mat.DefaultUnitId,
                                                DefaultUnitName = un.Name
                                            }).ToListAsync();

                editYieldOutputDtos = await (from yieldoutput in _yieldoutputRepo.GetAll()
                               .Where(a => pendingRefIds.Contains(a.YieldRefId))
                                             join mat in _materialRepo.GetAll()
                                         on yieldoutput.OutputMaterialRefId equals mat.Id
                                             join locmat in _materiallocationwisestockRepo.GetAll()
                                                  .Where(l => l.LocationRefId == input.LocationRefId)
                                             on mat.Id equals locmat.MaterialRefId
                                             join un in _unitRepo.GetAll()
                                             on mat.DefaultUnitId equals un.Id
                                             join uiss in _unitRepo.GetAll()
                                             on yieldoutput.UnitRefId equals uiss.Id
                                             select new YieldOutputViewDto
                                             {
                                                 Sno = yieldoutput.Sno,
                                                 YieldRefId = (int)yieldoutput.YieldRefId,
                                                 OutputMaterialRefId = yieldoutput.OutputMaterialRefId,
                                                 MaterialRefName = mat.MaterialName,
                                                 OutputQty = yieldoutput.OutputQty,
                                                 YieldPrice = yieldoutput.YieldPrice,
                                                 Uom = un.Name,
                                                 CurrentInHand = locmat.CurrentInHand,
                                                 UnitRefId = yieldoutput.UnitRefId,
                                                 UnitRefName = uiss.Name,
                                                 DefaultUnitId = mat.DefaultUnitId,
                                                 DefaultUnitName = un.Name
                                             }).ToListAsync();
            }
            List<int> arrMaterialRefIds = new List<int>();
            arrMaterialRefIds.AddRange(editYieldInputDtos.Select(t => t.InputMaterialRefId).ToList());

            List<IssueRecipeDetailListDto> editRecipeDetailDtos = new List<IssueRecipeDetailListDto>();
            List<IssueDetailViewDto> editIssueDetailDto = new List<IssueDetailViewDto>();

            if (input.CalculateIssueReservedQuantity)
            {
                DateTime issueFromDate = DateTime.Today.AddDays(-3);
                var lstpending = await _issueRepo.GetAll().Where(p => p.LocationRefId == input.LocationRefId && (p.CreationTime >= issueFromDate || p.IssueTime >= issueFromDate) && p.CompletedStatus == false).ToListAsync();
                int pendingCount = lstpending.Count();
                List<int> pendingRefIds = lstpending.Select(t => t.Id).ToList();

                editRecipeDetailDtos = await (from issRecipeDet in _issuerecipedetailRepo.GetAll().Where(a => pendingRefIds.Contains(a.IssueRefId))
                                              join mat in _materialRepo.GetAll() on issRecipeDet.RecipeRefId equals mat.Id
                                              join un in _unitRepo.GetAll() on mat.DefaultUnitId equals un.Id
                                              select new IssueRecipeDetailListDto
                                              {
                                                  Id = issRecipeDet.Id,
                                                  IssueRefId = issRecipeDet.IssueRefId,
                                                  RecipeRefId = issRecipeDet.RecipeRefId,
                                                  RecipeRefName = mat.MaterialName,
                                                  Sno = issRecipeDet.Sno,
                                                  RecipeProductionQty = issRecipeDet.RecipeProductionQty,
                                                  UnitRefId = issRecipeDet.UnitRefId,
                                                  Uom = un.Name,
                                                  CompletedQty = issRecipeDet.CompletedQty,
                                                  CompletionFlag = issRecipeDet.CompletionFlag,
                                                  MultipleBatchProductionAllowed = issRecipeDet.MultipleBatchProductionAllowed,
                                                  Remarks = issRecipeDet.Remarks,
                                                  CurrentInHand = 0,
                                                  DefaultUnitId = mat.DefaultUnitId,
                                              }).ToListAsync();

                editIssueDetailDto = await (from issDet in _issueDetailRepo.GetAll().Where(a => pendingRefIds.Contains(a.IssueRefId))
                                            join mat in _materialRepo.GetAll()
                                            on issDet.MaterialRefId equals mat.Id
                                            join un in _unitRepo.GetAll()
                                            on mat.DefaultUnitId equals un.Id
                                            join uiss in _unitRepo.GetAll()
                                            on issDet.UnitRefId equals uiss.Id
                                            select new IssueDetailViewDto
                                            {
                                                Id = issDet.Id,
                                                IssueQty = issDet.IssueQty,
                                                IssueRefId = issDet.IssueRefId,
                                                MaterialRefId = issDet.MaterialRefId,
                                                MaterialRefName = mat.MaterialName,
                                                RecipeRefId = issDet.RecipeRefId,
                                                RequestQty = issDet.RequestQty,
                                                UnitRefId = issDet.UnitRefId,
                                                UnitRefName = uiss.Name,
                                                CurrentInHand = 0,
                                                Sno = issDet.Sno,
                                                DefaultUnitName = un.Name,
                                                DefaultUnitId = mat.DefaultUnitId
                                            }).ToListAsync();
            }
            arrMaterialRefIds.AddRange(editRecipeDetailDtos.Select(t => t.RecipeRefId).ToList());
            arrMaterialRefIds.AddRange(editIssueDetailDto.Select(t => t.MaterialRefId).ToList());

            var allItems = (from matgroupcategory in _materialGroupCategoryRepo.GetAll()
                            join mat in _materialRepo.GetAll().Where(t => arrMaterialRefIds.Contains(t.Id)) on matgroupcategory.Id equals mat.MaterialGroupCategoryRefId
                            join unit in _unitRepo.GetAll() on mat.DefaultUnitId equals unit.Id
                            join uiss in _unitRepo.GetAll() on mat.IssueUnitId equals uiss.Id
                            join utrn in _unitRepo.GetAll() on mat.TransferUnitId equals utrn.Id
                            join urecd in _unitRepo.GetAll() on mat.ReceivingUnitId equals urecd.Id
                            join ustkadj in _unitRepo.GetAll() on mat.StockAdjustmentUnitId equals ustkadj.Id
                            select new MaterialViewDto
                            {
                                Id = mat.Id,
                                MaterialTypeId = mat.MaterialTypeId,
                                MaterialTypeName = "",
                                Barcode = mat.Barcode,
                                MaterialName = mat.MaterialName,
                                MaterialPetName = mat.MaterialPetName,
                                MaterialGroupCategoryName = matgroupcategory.MaterialGroupCategoryName,
                                UserSerialNumber = mat.UserSerialNumber,
                                WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                                ReservedIssueQuantity = 0,
                                ReservedYieldQuantity = 0,
                                UnitRefId = mat.DefaultUnitId,
                                DefaultUnitId = mat.DefaultUnitId,
                                DefaultUnitName = unit.Name,
                                IssueUnitName = uiss.Name,
                                IssueUnitId = uiss.Id,
                                ReceivingUnitId = mat.ReceivingUnitId,
                                ReceivingUnitRefName = urecd.Name,
                                TransferUnitId = mat.TransferUnitId,
                                TransferUnitRefName = utrn.Name,
                                StockAdjustmentUnitId = mat.StockAdjustmentUnitId,
                                StockAdjustentUnitRefName = ustkadj.Name
                            });
            List<MaterialViewDto> sortMenuItems;

            sortMenuItems = await allItems.ToListAsync();

            if (sortMenuItems.Count > 0)
            {
                var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();
                decimal conversionFactor = 0;
                foreach (var material in sortMenuItems)
                {
                    var materialTypeName = material.MaterialTypeId == (int)MaterialType.RAW ? L("RAW") : L("SEMI");
                    material.MaterialTypeName = materialTypeName;

                    {
                        #region Yield

                        var yieldList = editYieldInputDtos.Where(t => t.InputMaterialRefId == material.Id).ToList();
                        foreach (var yld in yieldList)
                        {
                            if (yld.DefaultUnitId == yld.UnitRefId)
                            {
                                conversionFactor = 1;
                            }
                            else
                            {
                                var unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == yld.UnitRefId && t.RefUnitId == yld.DefaultUnitId);
                                if (unitConversion != null)
                                {
                                    conversionFactor = unitConversion.Conversion;
                                }
                                else
                                {
                                    unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == yld.UnitRefId && t.RefUnitId == yld.DefaultUnitId);
                                    if (unitConversion == null)
                                    {
                                        var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == yld.UnitRefId);
                                        var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == yld.DefaultUnitId);

                                        throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
                                    }
                                    conversionFactor = 1 / unitConversion.Conversion;
                                }
                            }

                            material.ReservedYieldQuantity = material.ReservedYieldQuantity + yld.InputQty * conversionFactor;
                        }

                        #endregion Yield
                    }

                    {
                        #region Issue

                        var issueList = editIssueDetailDto.Where(t => t.MaterialRefId == material.Id).ToList();
                        foreach (var issList in issueList)
                        {
                            if (issList.DefaultUnitId == issList.UnitRefId)
                            {
                                conversionFactor = 1;
                            }
                            else
                            {
                                var unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == issList.UnitRefId && t.RefUnitId == issList.DefaultUnitId);
                                if (unitConversion != null)
                                {
                                    conversionFactor = unitConversion.Conversion;
                                }
                                else
                                {
                                    unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == issList.UnitRefId && t.RefUnitId == issList.DefaultUnitId);
                                    if (unitConversion == null)
                                    {
                                        var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == issList.UnitRefId);
                                        var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == issList.DefaultUnitId);

                                        throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
                                    }
                                    conversionFactor = 1 / unitConversion.Conversion;
                                }
                            }
                            material.ReservedIssueQuantity = material.ReservedIssueQuantity + issList.IssueQty * conversionFactor;
                        }

                        #endregion Issue
                    }
                }
            }
            var allListDtos = sortMenuItems.MapTo<List<MaterialViewDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultDto<MaterialViewDto>(allItemCount, allListDtos);
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetMaterialForCombobox()
        {
            var lstMaterial = await _materialRepo.GetAll().ToListAsync();
            return
                new ListResultDto<ComboboxItemDto>(
                    lstMaterial.Select(e => new ComboboxItemDto(e.Id.ToString(), e.MaterialName)).ToList());
        }

        public async Task<List<MaterialUsagePattern>> MaterialUsagePattern(InputLocationAndMaterialId input)
        {
            var usagePattern = new List<MaterialUsagePattern>();
            var DaysList = new List<int>();
            DaysList.Add(1);
            DaysList.Add(2);
            //DaysList.Add(3);
            DaysList.Add(7);
            DaysList.Add(14);
            DaysList.Add(30);

            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
            if (loc == null)
            {
                throw new UserFriendlyException(L("LocationErr") + " " + L("Id") + " " + input.LocationRefId);
            }

            foreach (var d in DaysList)
            {
                var fromDate = loc.HouseTransactionDate.Value.AddDays(-1 * d);
                var toDate = loc.HouseTransactionDate.Value;
                var usage = new MaterialUsagePattern();

                var matLedger =
                    await
                        _materialLedgerRepo.GetAll()
                            .Where(
                                a =>
                                    a.MaterialRefId == input.MaterialRefId && a.LocationRefId == input.LocationRefId &&
                                    (DbFunctions.TruncateTime(a.LedgerDate) >= fromDate.Date &&
                                     DbFunctions.TruncateTime(a.LedgerDate) < toDate))
                            .ToListAsync();

                usage.Days = d;

                usage.Consumption = 0;

                if (matLedger.Count > 0)
                {
                    usage.Consumption =
                        Math.Round(((matLedger.Sum(a => a.Issued) + matLedger.Sum(a => a.Sales) - matLedger.Sum(a => a.Return)) /
                             d), 6);
                }

                switch (d)
                {
                    case 1:
                        usage.Description = L("Yesterday");
                        break;

                    case 2:
                        usage.Description = L("InLast2Days");
                        break;

                    case 7:
                        usage.Description = L("InLastOneWeek");
                        break;

                    case 14:
                        usage.Description = L("InFortnight");
                        break;

                    case 30:
                        usage.Description = L("InLastOneMonth");
                        break;

                    default:
                        usage.Description = L("InLastNDays", d);
                        break;
                }

                usagePattern.Add(usage);
            }

            return usagePattern;
        }

        public async Task<DayCloseResultDto> DayCloseInBackGround(DayCloseDto input)
        {
            var output = await VerifyWipeOutStockForCloseDay(input);
            var dayClose = await _dayCloseService.SetCloseDay(new DayCloseDto
            {
                LocationRefId = input.LocationRefId,
                TransactionDate = input.TransactionDate
            });
            return output;
        }

        //public async Task HelloWorldTest(IdInput input)
        //{
        //    await HangFireBackGroundTest(input);
        //}

        //public async Task HangFireBackGroundTest(IdInput input)
        //{
        //    Hangfire.BackgroundJob.Enqueue(() => Console.WriteLine("Hello world from Hangfire! " + DateTime.Now.ToLongDateString() + " Test " + input.Id));

        //    Hangfire.BackgroundJob.Enqueue(() => Console.WriteLine("Hello world 2 from Hangfire! " + DateTime.Now.ToLongDateString() + " Test 2 " + input.Id));

        //}



        [UnitOfWork]
        public async Task<DayCloseResultDto> VerifyWipeOutStockForCloseDay(DayCloseDto input)
        {
            bool sameDayCloseAllowed = PermissionChecker.IsGranted("Pages.Tenant.House.Transaction.DayClose.SameDayClose");

            #region User Id Handling
            int? userId = null;
            if (AbpSession != null && AbpSession.UserId.HasValue)
            {
                userId = (int)AbpSession.UserId;
            }
            if (input.UserId.HasValue && userId == null)
            {
                userId = input.UserId;
            }
            if (userId == null)
            {
                throw new UserFriendlyException("User Id Should Not be Null ");
            }
            #endregion

            #region Tenant Id Handling
            int tenantId = 0;
            if (AbpSession != null && AbpSession.TenantId.HasValue)
            {
                tenantId = (int)AbpSession.TenantId.Value;
            }
            if (input.TenantId > 0 && tenantId == 0)
            {
                tenantId = input.TenantId;
            }
            if (tenantId == 0)
            {
                throw new UserFriendlyException("Tenant Id Should Not be Null ");
            }

            #endregion

            if (input.LedgerAffect)
            {
                if (input.TransactionDate.Date == DateTime.Today && sameDayCloseAllowed == false)
                //@@Pending Upto Synchranize With Sales BackUp
                {
                    throw new UserFriendlyException(L("YouCanNotCloseDateForTodayAsOfNow"));
                }

                if (input.TransactionDate.Date == DateTime.Today && sameDayCloseAllowed == true)
                //@@Pending Upto Synchranize With Sales BackUp
                {
                    if (DateTime.Now.Hour < 15)
                    //@@Pending Upto Synchranize With Sales BackUp
                    {
                        throw new UserFriendlyException(L("YouCanNotCloseDateForTodayAsOfNow"));
                    }
                }

                var currentDate = input.TransactionDate.Date.AddDays(1);

                if (currentDate > DateTime.Today.AddDays(1))
                {
                    throw new UserFriendlyException(L("YouCanNotCloseDateForFuture"));
                }
            }

            var result = new DayCloseResultDto();
            result.DayCloseSuccessFlag = false;
            List<int> locIds = new List<int>();
            locIds.Add(input.LocationRefId);

            var mainLoc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
            var loc = mainLoc.MapTo<LocationListDto>();
            var locinput = new List<LocationListDto>();
            locinput.Add(loc);

            var tempLocs = await _locationRepo.GetAll()
                    .Where(t => t.DayCloseSalesStockAdjustmentLocationRefId.HasValue && t.DayCloseSalesStockAdjustmentLocationRefId == input.LocationRefId).ToListAsync();
            var linkedLocs = tempLocs.MapTo<List<LocationListDto>>();
            locinput.AddRange(linkedLocs);

            var fromDt = input.TransactionDate.Date;
            var toDt = input.TransactionDate.Date;

            //  Get Sales
            AllItemSalesDto daySales = new AllItemSalesDto();
            TicketStatsDto sales = new TicketStatsDto();
            List<UnitConversionListDto> rsUc = new List<UnitConversionListDto>();
            List<DayCloseSyncReportOutputDto> lastDayCloseSyncResult = new List<DayCloseSyncReportOutputDto>();
            if (input.DoesDayCloseRequiredInBackGround == false)
            {
                daySales = await _connectReportAppService.GetAllItemSales(
                new GetItemInput
                {
                    StartDate = fromDt,
                    EndDate = toDt,
                    Locations = locinput.MapTo<List<SimpleLocationDto>>(),
                    Sales = true,
                    Comp = true,
                    Void = true,
                    Gift = true
                });

                sales = await _connectReportAppService.GetTickets(new GetTicketInput
                {
                    StartDate = fromDt,
                    EndDate = toDt,
                    Locations = locinput.MapTo<List<SimpleLocationDto>>()
                }, true);

                rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();

                lastDayCloseSyncResult = await GetDayCloseSyncReport(new DayCloseSyncReportDto
                {
                    StartDate = fromDt.AddDays(-5),
                    EndDate = fromDt.AddDays(-1),
                    LocationRefId = input.LocationRefId
                });
            }
            else
            {
                daySales = input.AllItemSalesDto;
                sales = input.TicketStatsDto;
                rsUc = input.UnitConversionListDtos;
                lastDayCloseSyncResult = input.DayCloseSyncReportOutputDtos;
            }


            result.DayCloseSyncErrorList = null;
            result.LastDayCloseErrorFlag = false;

            var errorDayCloseSyncList = lastDayCloseSyncResult.Where(t => t.SyncErrorFlag == true).ToList();

            if (errorDayCloseSyncList.Count > 0)
            {
                result.DayCloseSyncErrorList = errorDayCloseSyncList;
                result.LastDayCloseErrorFlag = true;
            }
            string locationCombinedName = string.Join(" , ", locinput.Select(t => t.Name).ToList());
            if (locinput.Count > 1)
            {
                locationCombinedName = locinput.Count + " Locations. ( " + locationCombinedName + " ) ";
            }

            if (sales.DashBoardDto.TotalAmount > 0)
            {
                result.SalesExistFlag = true;
                result.SalesAmount = sales.DashBoardDto.TotalAmount;
                if (PermissionChecker.IsGranted("Pages.Tenant.House.Transaction.DayClose.HideSalesOnDayClose"))
                {
                    result.SalesMessage = L("DayCloseHideSalesMessage");
                }
                else
                {
                    result.SalesMessage = L("DaySalesForLocation", fromDt.ToString("dd-MMM-yy"), locationCombinedName,
                        result.SalesAmount);
                }
            }
            else
            {
                result.SalesExistFlag = false;
                result.SalesAmount = 0;
                result.SalesMessage = L("NoSalesAlertMessage", fromDt.ToString("dd-MMM-yy"), locationCombinedName);

                if (input.ResetFlag)
                {
                    input.LedgerAffect = false;
                }
            }

            var rsMaterials = await _materialRepo.GetAllListAsync();
            var rsMaterialMappings = await _materialmenumappingRepo.GetAllListAsync();
            var rsMenumappingDepartment = await _menumappingDepartmentRepo.GetAllListAsync();
            var rsDeptList = await _departmentRepo.GetAll().Select(t => new { t.Id, t.Name }).ToListAsync();

            #region DayCloseTemplateSave

            if (input.LedgerAffect && input.DoesDayCloseRequiredInBackGround == false)
            {
                {
                    #region UcTemplate

                    var existUcTemplate =
                        await
                            _daycloseTemplateRepo.FirstOrDefaultAsync(
                                t =>
                                    DbFunctions.TruncateTime(t.ClosedDate) ==
                                    DbFunctions.TruncateTime(input.TransactionDate) &&
                                    t.TemplateType == (int)DayCloseTemplateType.UnitConversions && t.LocationRefId == input.LocationRefId);

                    int? daycloseUcExistId = null;
                    if (existUcTemplate != null)
                        daycloseUcExistId = existUcTemplate.Id;

                    var uct = new UnitConversionTemplate
                    {
                        UcList = rsUc.MapTo<List<UnitConversionTemplateDto>>()
                    };

                    var templateJsonUc = _xmlandjsonConvertorAppService.SerializeToJSON(uct);

                    var newDcDtoUc = new DayCloseTemplateEditDto
                    {
                        Id = daycloseUcExistId,
                        LocationRefId = input.LocationRefId,
                        TableName = "UnitConversions",
                        TemplateData = templateJsonUc,
                        TemplateType = (int)DayCloseTemplateType.UnitConversions,
                        ClosedDate = input.TransactionDate.Date
                    };

                    await _templateAppService.CreateOrUpdateDayCloseTemplate(
                        new CreateOrUpdateDayCloseTemplateInput
                        {
                            DayCloseTemplate = newDcDtoUc
                        });

                    #endregion UcTemplate
                }

                {
                    #region MenuMappingDepartmentTemplate

                    var existMenuDepartmentTemplate =
                        await
                            _daycloseTemplateRepo.FirstOrDefaultAsync(
                                t =>
                                    DbFunctions.TruncateTime(t.ClosedDate) ==
                                    DbFunctions.TruncateTime(input.TransactionDate) &&
                                    t.TemplateType == (int)DayCloseTemplateType.MenuMappingDepartment && t.LocationRefId == input.LocationRefId);

                    int? daycloseMMExistId = null;
                    if (existMenuDepartmentTemplate != null)
                        daycloseMMExistId = existMenuDepartmentTemplate.Id;

                    var mmdept = new MenuMappingDepartmentTemplate
                    {
                        MenuMappingDepartment = rsMenumappingDepartment.MapTo<List<MenuMappingDepartmentTemplateDto>>()
                    };

                    var templateJsonMMDept = _xmlandjsonConvertorAppService.SerializeToJSON(mmdept);

                    var newMMDeptDto = new DayCloseTemplateEditDto
                    {
                        Id = daycloseMMExistId,
                        LocationRefId = input.LocationRefId,
                        TableName = "MenuMappingDepartment",
                        TemplateData = templateJsonMMDept,
                        TemplateType = (int)DayCloseTemplateType.MenuMappingDepartment,
                        ClosedDate = input.TransactionDate.Date
                    };

                    await _templateAppService.CreateOrUpdateDayCloseTemplate(
                        new CreateOrUpdateDayCloseTemplateInput
                        {
                            DayCloseTemplate = newMMDeptDto
                        });

                    #endregion MenuMappingDepartmentTemplate
                }

                {
                    #region MaterialMenuMappingTemplate

                    var existTemplate =
                        await
                            _daycloseTemplateRepo.FirstOrDefaultAsync(
                                t =>
                                    DbFunctions.TruncateTime(t.ClosedDate) ==
                                    DbFunctions.TruncateTime(input.TransactionDate) &&
                                    t.TemplateType == (int)DayCloseTemplateType.MaterialMenuMappings && t.LocationRefId == input.LocationRefId);

                    int? daycloseExistId = null;
                    if (existTemplate != null)
                        daycloseExistId = existTemplate.Id;

                    var mmt = new MaterialMappingTemplate
                    {
                        MappingList = rsMaterialMappings.MapTo<List<MaterialMappingTemplateDto>>()
                    };

                    var templateJsonMM = _xmlandjsonConvertorAppService.SerializeToJSON(mmt);

                    var newDcDto = new DayCloseTemplateEditDto
                    {
                        Id = daycloseExistId,
                        LocationRefId = input.LocationRefId,
                        TableName = "MaterialMenuMappings",
                        TemplateData = templateJsonMM,
                        TemplateType = (int)DayCloseTemplateType.MaterialMenuMappings,
                        ClosedDate = input.TransactionDate.Date
                    };

                    await _templateAppService.CreateOrUpdateDayCloseTemplate(
                        new CreateOrUpdateDayCloseTemplateInput
                        {
                            DayCloseTemplate = newDcDto
                        });

                    #endregion MaterialMenuMappingTemplate
                }

                #region DashBoardSalesDto

                {
                    var existSalesTemplate =
                        await
                            _daycloseTemplateRepo.FirstOrDefaultAsync(
                                t =>
                                    DbFunctions.TruncateTime(t.ClosedDate) ==
                                    DbFunctions.TruncateTime(input.TransactionDate) &&
                                    t.TemplateType == (int)DayCloseTemplateType.DashboardTicketDto && t.LocationRefId == input.LocationRefId);

                    int? daycloseExistId = null;
                    if (existSalesTemplate != null)
                        daycloseExistId = existSalesTemplate.Id;

                    var dbdto = sales.DashBoardDto;

                    var templateJsonMM = _xmlandjsonConvertorAppService.SerializeToJSON(dbdto);

                    var newDcDto = new DayCloseTemplateEditDto
                    {
                        Id = daycloseExistId,
                        LocationRefId = input.LocationRefId,
                        TableName = "DashboardTicketDto",
                        TemplateData = templateJsonMM,
                        TemplateType = (int)DayCloseTemplateType.DashboardTicketDto,
                        ClosedDate = input.TransactionDate.Date
                    };

                    await _templateAppService.CreateOrUpdateDayCloseTemplate(
                        new CreateOrUpdateDayCloseTemplateInput
                        {
                            DayCloseTemplate = newDcDto
                        });
                }

                #endregion DashBoardSalesDto
            }

            bool negativeStockAccepted = false;
            bool doesBackGroundDayClose = false;
            if (input.LedgerAffect)
            {
                if (input.RunInBackGround)
                {
                    if (loc.DoesDayCloseRunInBackGround == false)
                    {
                        var isBackGroundDayCloseCanBeAllowed = await _settingManager.GetSettingValueAsync<bool>(AppSettings.HouseSettings.BackGroundDayCloseCanBeAllowed);
                        if (input.RunInBackGround && isBackGroundDayCloseCanBeAllowed)
                        {
                            var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
                            location.DoesDayCloseRunInBackGround = true;
                            location.BackGroundStartTime = DateTime.Now;
                            await _locationRepo.UpdateAsync(location);


                            if (PermissionChecker.IsGranted("Pages.Tenant.House.Master.MaterialLedger.NegativeStock"))
                            {
                                negativeStockAccepted = true;
                            }

                            //await Hangfire.BackgroundJob.Equals(()=> )

                            await _bgmDayClose.EnqueueAsync<HouseDayCloseJob, HouseDayCloseIdJobArgs>(new HouseDayCloseIdJobArgs
                            {
                                LocationRefId = input.LocationRefId,
                                TransactionDate = input.TransactionDate,
                                DayCloseWastageData = input.DayCloseWastageData,
                                LedgerAffect = input.LedgerAffect,
                                Remarks = input.Remarks,
                                ResetFlag = input.ResetFlag,
                                AllItemSalesDto = daySales,
                                TicketStatsDto = sales,
                                UnitConversionListDtos = rsUc,
                                DayCloseSyncReportOutputDtos = lastDayCloseSyncResult,
                                NegativeStockAccepted = negativeStockAccepted,
                                UserId = userId,
                                TenantId = tenantId
                            });
                            return new DayCloseResultDto { DoesRunInBackGround = true };
                        }
                    }
                    else
                    {
                        throw new UserFriendlyException("Already Background process running");
                    }
                }
                else
                {
                    if (PermissionChecker.IsGranted("Pages.Tenant.House.Master.MaterialLedger.NegativeStock") || input.NegativeStockAccepted)
                    {
                        negativeStockAccepted = true;
                    }
                    if (input.DoesDayCloseRequiredInBackGround == true)
                    {
                        doesBackGroundDayClose = true;
                    }
                }
            }

            #endregion DayCloseTemplateSave

            #region AutoCompWastageCalculation

            var wastagemapwithMenuList = new List<MaterialMenuMappingWithWipeOut>();

            var itemComp = daySales.Comp.MapTo<List<MenuListDto>>();

            foreach (var item in itemComp)
            {
                var outputlist =
                    rsMaterialMappings.Where(
                        t => t.PosMenuPortionRefId == item.MenuItemPortionId && t.AutoSalesDeduction && t.LocationRefId == input.LocationRefId).ToList();

                if (outputlist == null || outputlist.Count == 0)
                {
                    outputlist =
                    rsMaterialMappings.Where(
                        t => t.PosMenuPortionRefId == item.MenuItemPortionId && t.AutoSalesDeduction && t.LocationRefId == null).ToList();
                }

                foreach (var a in outputlist)
                {
                    var newmm = new MaterialMenuMappingWithWipeOut();
                    var mat = rsMaterials.FirstOrDefault(t => t.Id == a.MaterialRefId);

                    if (mat == null)
                    {
                        using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                        {
                            var deletedMaterial = await _materialRepo.FirstOrDefaultAsync(t => t.Id == a.MaterialRefId);
                            var deletedMaterialName = deletedMaterial == null ? "" : deletedMaterial.MaterialName;
                            throw new UserFriendlyException(L("SomeOfTheLinkedMaterialsInMenuMappingDeleted") + " " + L("MATERIALNAMEISWRONG", deletedMaterialName));
                        }
                        throw new UserFriendlyException(L("SomeOfTheLinkedMaterialsInMenuMappingDeleted"));
                    }

                    newmm.PosMenuPortionRefId = a.PosMenuPortionRefId;
                    newmm.MenuQuantitySold = a.MenuQuantitySold;
                    newmm.MaterialRefId = a.MaterialRefId;
                    newmm.PortionQty = Math.Round(a.PortionQty / a.MenuQuantitySold * item.Quantity, 14);
                    newmm.PortionUnitId = a.PortionUnitId;
                    newmm.TotalSaleQuantity = item.Quantity;
                    newmm.AutoSalesDeduction = a.AutoSalesDeduction;
                    newmm.DefaultUnitId = mat.DefaultUnitId;

                    wastagemapwithMenuList.Add(newmm);
                }
            }

            var mapwithCompMenuListGroupWithUnitConversion
                = (from compmenu in wastagemapwithMenuList
                   join uc in rsUc
                       on compmenu.PortionUnitId equals uc.BaseUnitId
                   where uc.RefUnitId == compmenu.DefaultUnitId
                   group compmenu by new
                   {
                       compmenu.MaterialRefId,
                       compmenu.MaterialRefName,
                       compmenu.DefaultUnitId,
                       compmenu.PortionUnitId,
                       compmenu.UnitRefName,
                       compmenu.DefaultUnitName,
                       uc.Conversion
                   }
                    into g
                   select new MaterialMenuMappingWithWipeOut
                   {
                       MaterialRefId = g.Key.MaterialRefId,
                       PortionUnitId = g.Key.PortionUnitId,
                       UnitRefName = g.Key.UnitRefName,
                       DefaultUnitId = g.Key.DefaultUnitId,
                       DefaultUnitName = g.Key.DefaultUnitName,
                       PortionQty = g.Sum(t => t.PortionQty * g.Key.Conversion)
                   }).ToList().OrderBy(t => t.MaterialRefId);

            var mapWithCompMenuListSumGroup = (from compmenu in mapwithCompMenuListGroupWithUnitConversion
                                               group compmenu by compmenu.MaterialRefId
                into g
                                               select new MaterialMenuMappingWithWipeOut
                                               {
                                                   MaterialRefId = g.Key,
                                                   PortionQty = g.Sum(t => t.PortionQty)
                                               }).ToList();

            List<MaterialSalesWithWipeOutCloseDayDto> allCompItems;
            allCompItems = await (from mat in _materialRepo.GetAll().
                Where(m => m.MaterialTypeId != (int)MaterialType.RAW && m.TenantId == tenantId)
                                  join unit in _unitRepo.GetAll()
                                      on mat.DefaultUnitId equals unit.Id
                                  join uiss in _unitRepo.GetAll()
                                      on mat.IssueUnitId equals uiss.Id
                                  select new MaterialSalesWithWipeOutCloseDayDto
                                  {
                                      MaterialRefId = mat.Id,
                                      MaterialRefName = mat.MaterialName,
                                      Uom = unit.Name,
                                      ClBalance = 0,
                                      WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                                      AutoSalesDeduction = false,
                                      DefaultUnitId = mat.DefaultUnitId,
                                      DefaultUnitName = unit.Name,
                                      IssueUnitId = mat.IssueUnitId,
                                      IssueUnitName = uiss.Name
                                  }).ToListAsync();

            List<MaterialSalesWithWipeOutCloseDayDto> allCompRawItemsAutoSales;
            allCompRawItemsAutoSales = await (from mat in _materialRepo.GetAll()
                .Where(m => m.MaterialTypeId == (int)MaterialType.RAW && m.TenantId == tenantId)
                                              join unit in _unitRepo.GetAll() on mat.DefaultUnitId equals unit.Id
                                              join menumap in _materialmenumappingRepo
                                                  .GetAll().Where(t => t.AutoSalesDeduction && (t.LocationRefId == null || t.LocationRefId == input.LocationRefId))
                                                  on mat.Id equals menumap.MaterialRefId
                                              join uiss in _unitRepo.GetAll()
                                                  on mat.IssueUnitId equals uiss.Id
                                              select new MaterialSalesWithWipeOutCloseDayDto
                                              {
                                                  MaterialRefId = mat.Id,
                                                  MaterialRefName = mat.MaterialName,
                                                  Uom = unit.Name,
                                                  ClBalance = 0,
                                                  WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                                                  AutoSalesDeduction = menumap.AutoSalesDeduction,
                                                  DefaultUnitId = mat.DefaultUnitId,
                                                  DefaultUnitName = unit.Name,
                                                  IssueUnitId = mat.IssueUnitId,
                                                  IssueUnitName = uiss.Name
                                              }).Distinct().ToListAsync();

            //#region GetAuto Comp Sales True - Location Ref Id Particular - Or All
            //tempList = new List<MaterialSalesWithWipeOutCloseDayDto>();
            //tempList = null;
            //foreach (var mm in allCompRawItemsAutoSales)
            //{
            //    var outputItem =
            //        rsMaterialMappings.FirstOrDefault(
            //            t => t.MaterialRefId == mm.MaterialRefId && t.AutoSalesDeduction && t.LocationRefId == input.LocationRefId);

            //    if (outputItem == null)
            //    {
            //        outputItem =
            //        rsMaterialMappings.FirstOrDefault(
            //            t => t.MaterialRefId == mm.MaterialRefId && t.AutoSalesDeduction && t.LocationRefId == null);
            //    }
            //    if (outputItem == null)
            //    {
            //        continue;
            //    }
            //    mm.AutoSalesDeduction = true;
            //    tempList.Add(mm);
            //}
            //allCompRawItemsAutoSales = tempList;

            //#endregion

            var wastageSortCompMenuItems = allCompItems.Union(allCompRawItemsAutoSales).ToList();

            foreach (var wastageitem in wastageSortCompMenuItems)
            {
                var wastagefromComp =
                    mapWithCompMenuListSumGroup.FirstOrDefault(t => t.MaterialRefId == wastageitem.MaterialRefId);
                if (wastagefromComp == null)
                {
                    wastageitem.Damaged = 0;
                }
                else
                {
                    wastageitem.Damaged = wastagefromComp.PortionQty;

                    //var negativeStock = negativeStockConvertAsZeroStockList.FirstOrDefault(t => t.MaterialRefId == wastageitem.MaterialRefId);
                    //if (negativeStock != null)
                    //    negativeStock.ClBalance = negativeStock.ClBalance + (-1 * wastageitem.Damaged);
                }
            }

            wastageSortCompMenuItems = wastageSortCompMenuItems.Where(t => t.Damaged > 0).ToList();

            var allWastageListDtos = wastageSortCompMenuItems.MapTo<List<MaterialSalesWithWipeOutCloseDayDto>>();

            allWastageListDtos = allWastageListDtos.OrderBy(t => t.MaterialRefId).OrderBy(t => t.Remarks).ToList();
            result.CompData = allWastageListDtos;

            #endregion AutoCompWastageCalculation

            #region AutoMenuWastageCalculation

            var menuwastagemapwithMenuList = new List<MaterialMenuMappingWithWipeOut>();

            var mas = await _menuitemwastageRepo.GetAll()
                .Where(
                    t =>
                        DbFunctions.TruncateTime(t.SalesDate) == DbFunctions.TruncateTime(input.TransactionDate) &&
                        t.LocationRefId == input.LocationRefId && t.AdjustmentRefId == null).ToListAsync();

            var masRefIds = mas.Select(t => t.Id).ToArray();

            var menuWastages = await (from det in _menuitemwastageDetailRepo.GetAll()
                .Where(t => masRefIds.Contains(t.MenuItemWastageRefId))
                                      group det by new { det.PosMenuPortionRefId }
                into g
                                      select new MenuItemWastageDetailEditDto
                                      {
                                          PosMenuPortionRefId = g.Key.PosMenuPortionRefId,
                                          WastageQty = g.Sum(t => t.WastageQty)
                                      }).ToListAsync();

            if (menuWastages.Count > 0)
            {
                var menus = await (from menuportion in _menuitemportionRepo.GetAll()
                                   join menu in _menuitemRepo.GetAll().Where(a => a.ProductType == 1)
                                       on menuportion.MenuItemId equals menu.Id
                                   select new ProductListDto
                                   {
                                       PortionId = menuportion.Id,
                                       PortionName = string.Concat(menu.Name, " - ", menuportion.Name)
                                   }).ToListAsync();
                foreach (var lst in menuWastages)
                {
                    var menu = menus.FirstOrDefault(t => t.PortionId == lst.PosMenuPortionRefId);
                    if (menu != null)
                        lst.PosMenuPortionRefName = menu.PortionName;
                }
            }

            result.MenuWastageConsolidated = menuWastages;

            foreach (var imw in menuWastages)
            {
                var outputlist =
                    rsMaterialMappings.Where(
                        t => t.PosMenuPortionRefId == imw.PosMenuPortionRefId && t.AutoSalesDeduction && t.LocationRefId == input.LocationRefId).ToList();

                if (outputlist == null || outputlist.Count == 0)
                {
                    outputlist =
                    rsMaterialMappings.Where(
                        t => t.PosMenuPortionRefId == imw.PosMenuPortionRefId && t.AutoSalesDeduction && t.LocationRefId == null).ToList();
                }

                foreach (var a in outputlist)
                {
                    var newmm = new MaterialMenuMappingWithWipeOut();
                    var mat = rsMaterials.FirstOrDefault(t => t.Id == a.MaterialRefId);

                    if (mat == null)
                    {
                        using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                        {
                            var deletedMaterial = await _materialRepo.FirstOrDefaultAsync(t => t.Id == a.MaterialRefId);
                            var deletedMaterialName = deletedMaterial == null ? "" : deletedMaterial.MaterialName;
                            throw new UserFriendlyException(L("SomeOfTheLinkedMaterialsInMenuMappingDeleted") + " " + L("MATERIALNAMEISWRONG", deletedMaterialName));
                        }
                        throw new UserFriendlyException(L("SomeOfTheLinkedMaterialsInMenuMappingDeleted"));
                    }

                    newmm.PosMenuPortionRefId = a.PosMenuPortionRefId;
                    newmm.MenuQuantitySold = a.MenuQuantitySold;
                    newmm.MaterialRefId = a.MaterialRefId;
                    newmm.PortionQty = Math.Round(a.PortionQty / a.MenuQuantitySold * imw.WastageQty, 14);
                    newmm.PortionUnitId = a.PortionUnitId;
                    newmm.TotalSaleQuantity = imw.WastageQty;
                    newmm.AutoSalesDeduction = a.AutoSalesDeduction;
                    newmm.DefaultUnitId = mat.DefaultUnitId;

                    menuwastagemapwithMenuList.Add(newmm);
                }
            }

            var mapwithwastMenuListGroupWithUnitConversion
                = (from wastmenu in menuwastagemapwithMenuList
                   join uc in rsUc
                       on wastmenu.PortionUnitId equals uc.BaseUnitId
                   where uc.RefUnitId == wastmenu.DefaultUnitId
                   group wastmenu by new
                   {
                       wastmenu.MaterialRefId,
                       wastmenu.MaterialRefName,
                       wastmenu.DefaultUnitId,
                       wastmenu.PortionUnitId,
                       wastmenu.UnitRefName,
                       wastmenu.DefaultUnitName,
                       uc.Conversion
                   }
                    into g
                   select new MaterialMenuMappingWithWipeOut
                   {
                       MaterialRefId = g.Key.MaterialRefId,
                       PortionUnitId = g.Key.PortionUnitId,
                       UnitRefName = g.Key.UnitRefName,
                       DefaultUnitId = g.Key.DefaultUnitId,
                       DefaultUnitName = g.Key.DefaultUnitName,
                       PortionQty = g.Sum(t => t.PortionQty * g.Key.Conversion)
                   }).ToList().OrderBy(t => t.MaterialRefId);

            var mapWithwastMenuListSumGroup = (from wastmenu in mapwithwastMenuListGroupWithUnitConversion
                                               group wastmenu by wastmenu.MaterialRefId
                into g
                                               select new MaterialMenuMappingWithWipeOut
                                               {
                                                   MaterialRefId = g.Key,
                                                   PortionQty = g.Sum(t => t.PortionQty)
                                               }).ToList();

            List<MaterialSalesWithWipeOutCloseDayDto> allWasteItems;
            allWasteItems = await (from mat in _materialRepo.GetAll().
                Where(m => m.MaterialTypeId != (int)MaterialType.RAW)
                                   join unit in _unitRepo.GetAll()
                                       on mat.DefaultUnitId equals unit.Id
                                   join uiss in _unitRepo.GetAll()
                                       on mat.IssueUnitId equals uiss.Id
                                   select new MaterialSalesWithWipeOutCloseDayDto
                                   {
                                       MaterialRefId = mat.Id,
                                       MaterialRefName = mat.MaterialName,
                                       Uom = unit.Name,
                                       ClBalance = 0,
                                       WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                                       AutoSalesDeduction = false,
                                       DefaultUnitId = mat.DefaultUnitId,
                                       DefaultUnitName = unit.Name,
                                       IssueUnitId = mat.IssueUnitId,
                                       IssueUnitName = uiss.Name
                                   }).ToListAsync();

            List<MaterialSalesWithWipeOutCloseDayDto> allWastRawItemsAutoSales;
            allWastRawItemsAutoSales = await (from mat in _materialRepo.GetAll()
                .Where(m => m.MaterialTypeId == (int)MaterialType.RAW)
                                              join unit in _unitRepo.GetAll() on mat.DefaultUnitId equals unit.Id
                                              //join menumap in _materialmenumappingRepo
                                              //    .GetAll().Where(t => t.AutoSalesDeduction && (t.LocationRefId == null || t.LocationRefId == input.LocationRefId))
                                              //    on mat.Id equals menumap.MaterialRefId
                                              join uiss in _unitRepo.GetAll()
                                                  on mat.IssueUnitId equals uiss.Id
                                              select new MaterialSalesWithWipeOutCloseDayDto
                                              {
                                                  MaterialRefId = mat.Id,
                                                  MaterialRefName = mat.MaterialName,
                                                  Uom = unit.Name,
                                                  ClBalance = 0,
                                                  WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                                                  //AutoSalesDeduction = menumap.AutoSalesDeduction,
                                                  DefaultUnitId = mat.DefaultUnitId,
                                                  DefaultUnitName = unit.Name,
                                                  IssueUnitId = mat.IssueUnitId,
                                                  IssueUnitName = uiss.Name
                                              }).Distinct().ToListAsync();

            var wastageMenuItems = allWasteItems.Union(allWastRawItemsAutoSales).ToList();

            foreach (var wastageitem in wastageMenuItems)
            {
                var wastage =
                    mapWithwastMenuListSumGroup.FirstOrDefault(t => t.MaterialRefId == wastageitem.MaterialRefId);
                if (wastage == null)
                {
                    wastageitem.Damaged = 0;
                }
                else
                {
                    wastageitem.Damaged = wastage.PortionQty;
                }
            }

            var allMenuWastageListDtos = wastageMenuItems.MapTo<List<MaterialSalesWithWipeOutCloseDayDto>>();

            allMenuWastageListDtos = allMenuWastageListDtos.Where(t => t.Damaged > 0).ToList();

            allMenuWastageListDtos =
                allMenuWastageListDtos.OrderBy(t => t.MaterialRefId).OrderBy(t => t.Remarks).ToList();
            result.MenuWastageData = allMenuWastageListDtos;

            #endregion AutoMenuWastageCalculation

            #region AutoSalesCalculation

            var mapwithMenuList = new List<MaterialMenuMappingWithWipeOut>();
            var itemSales = daySales.Sales.MapTo<List<MenuListDto>>();

            if (daySales.Gift.Count > 0)
                itemSales.AddRange(daySales.Gift);

            foreach (var item in itemSales)
            {
                var menuMapping = rsMaterialMappings.Where(t => t.PosMenuPortionRefId == item.MenuItemPortionId
                                                                && t.AutoSalesDeduction && t.LocationRefId == input.LocationRefId).ToList();
                if (menuMapping == null || menuMapping.Count == 0)
                {
                    menuMapping = rsMaterialMappings.Where(t => t.PosMenuPortionRefId == item.MenuItemPortionId
                                                                && t.AutoSalesDeduction && t.LocationRefId == null).ToList();
                }

                var department = rsDeptList.FirstOrDefault(t => t.Name.ToUpper().Equals(item.DepartmentName.ToUpper()));

                if (department == null)
                {
                    throw new UserFriendlyException(L("Department") + " " + L("NotExist"), item.DepartmentName);
                }

                var deptId = department.Id;

                var menuMappingRefIds = menuMapping.Select(t => t.Id).ToArray();

                var deptList =
                    rsMenumappingDepartment.Where(t => menuMappingRefIds.Contains(t.MenuMappingRefId)).ToList();

                bool AllDepartmentFlag;
                if (deptList.Count() == 0)
                {
                    AllDepartmentFlag = true;
                }
                else
                {
                    AllDepartmentFlag = false;
                }

                foreach (var MmMaterial in menuMapping)
                {
                    if (AllDepartmentFlag == false)
                    {
                        var deptLinkedMM = deptList.Where(t => t.MenuMappingRefId == MmMaterial.Id);
                        if (deptLinkedMM.Count() > 0)
                        {
                            deptLinkedMM = deptLinkedMM.Where(t => t.DepartmentRefId == deptId);
                            if (deptLinkedMM.Count() == 0)
                            {
                                continue;
                            }
                        }
                    }

                    var newmm = new MaterialMenuMappingWithWipeOut();
                    var mat = rsMaterials.FirstOrDefault(t => t.Id == MmMaterial.MaterialRefId);

                    if (mat == null)
                    {
                        using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                        {
                            var deletedMaterial = await _materialRepo.FirstOrDefaultAsync(t => t.Id == MmMaterial.MaterialRefId);
                            var deletedMaterialName = deletedMaterial == null ? "" : deletedMaterial.MaterialName;
                            throw new UserFriendlyException(L("SomeOfTheLinkedMaterialsInMenuMappingDeleted") + " " + L("MATERIALNAMEISWRONG", deletedMaterialName));
                        }
                        throw new UserFriendlyException(L("SomeOfTheLinkedMaterialsInMenuMappingDeleted"));
                    }

                    newmm.PosMenuPortionRefId = MmMaterial.PosMenuPortionRefId;
                    newmm.MenuQuantitySold = MmMaterial.MenuQuantitySold;
                    newmm.MaterialRefId = MmMaterial.MaterialRefId;
                    newmm.PortionQty = Math.Round(MmMaterial.PortionQty / MmMaterial.MenuQuantitySold * item.Quantity, 14);
                    newmm.PortionUnitId = MmMaterial.PortionUnitId;
                    newmm.TotalSaleQuantity = item.Quantity;
                    newmm.AutoSalesDeduction = MmMaterial.AutoSalesDeduction;
                    newmm.DefaultUnitId = mat.DefaultUnitId;

                    mapwithMenuList.Add(newmm);
                }
            }

            var mapwithMenuListGroupWithUnitConversion = (from salemenu in mapwithMenuList
                                                          join uc in rsUc
                                                              on salemenu.PortionUnitId equals uc.BaseUnitId
                                                          where uc.RefUnitId == salemenu.DefaultUnitId
                                                          group salemenu by new
                                                          {
                                                              salemenu.MaterialRefId,
                                                              salemenu.MaterialRefName,
                                                              salemenu.DefaultUnitId,
                                                              salemenu.PortionUnitId,
                                                              salemenu.UnitRefName,
                                                              salemenu.DefaultUnitName,
                                                              uc.Conversion
                                                          }
                into g
                                                          select new MaterialMenuMappingWithWipeOut
                                                          {
                                                              MaterialRefId = g.Key.MaterialRefId,
                                                              PortionUnitId = g.Key.PortionUnitId,
                                                              UnitRefName = g.Key.UnitRefName,
                                                              DefaultUnitId = g.Key.DefaultUnitId,
                                                              DefaultUnitName = g.Key.DefaultUnitName,
                                                              PortionQty = g.Sum(t => t.PortionQty * g.Key.Conversion)
                                                          }).ToList().OrderBy(t => t.MaterialRefId);

            var mapWithMenuListSumGroup = (from salemenu in mapwithMenuListGroupWithUnitConversion
                                           group salemenu by salemenu.MaterialRefId
                into g
                                           select new MaterialMenuMappingWithWipeOut
                                           {
                                               MaterialRefId = g.Key,
                                               PortionQty = g.Sum(t => t.PortionQty)
                                           }).ToList();

            //  Select all Semi Materials
            List<MaterialSalesWithWipeOutCloseDayDto> allItems;
            allItems = await (from mat in _materialRepo.GetAll().
                Where(m => m.MaterialTypeId != (int)MaterialType.RAW)
                              join unit in _unitRepo.GetAll()
                                  on mat.DefaultUnitId equals unit.Id
                              join uiss in _unitRepo.GetAll()
                                  on mat.IssueUnitId equals uiss.Id
                              select new MaterialSalesWithWipeOutCloseDayDto
                              {
                                  MaterialRefId = mat.Id,
                                  MaterialRefName = mat.MaterialName,
                                  Uom = unit.Name,
                                  ClBalance = 0,
                                  WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                                  AutoSalesDeduction = false,
                                  DefaultUnitId = mat.DefaultUnitId,
                                  DefaultUnitName = unit.Name,
                                  IssueUnitId = mat.IssueUnitId,
                                  IssueUnitName = uiss.Name
                              }).ToListAsync();

            List<MaterialSalesWithWipeOutCloseDayDto> allRawItemsAutoSales;

            allRawItemsAutoSales =
                await (from mat in _materialRepo.GetAll().Where(m => m.MaterialTypeId == (int)MaterialType.RAW)
                       join unit in _unitRepo.GetAll() on mat.DefaultUnitId equals unit.Id
                       join menumap in _materialmenumappingRepo.GetAll().Where(t => t.AutoSalesDeduction
                       && (t.LocationRefId == null || t.LocationRefId == input.LocationRefId))
                           on mat.Id equals menumap.MaterialRefId
                       join uiss in _unitRepo.GetAll()
                           on mat.IssueUnitId equals uiss.Id
                       select new MaterialSalesWithWipeOutCloseDayDto
                       {
                           MaterialRefId = mat.Id,
                           MaterialRefName = mat.MaterialName,
                           Uom = unit.Name,
                           ClBalance = 0,
                           WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                           AutoSalesDeduction = menumap.AutoSalesDeduction,
                           DefaultUnitId = mat.DefaultUnitId,
                           DefaultUnitName = unit.Name,
                           IssueUnitId = mat.IssueUnitId,
                           IssueUnitName = uiss.Name
                       }).Distinct().ToListAsync();

            //#region GetAuto Sales True - Location Ref Id Particular - Or All
            //List<MaterialSalesWithWipeOutCloseDayDto> tempList = new List<MaterialSalesWithWipeOutCloseDayDto>();
            //foreach (var mm in allRawItemsAutoSales)
            //{
            //    var outputList =
            //        rsMaterialMappings.Where(
            //            t => t.MaterialRefId == mm.MaterialRefId && t.AutoSalesDeduction && t.LocationRefId == input.LocationRefId).ToList();

            //    if (outputList == null || outputList.Count == 0)
            //    {
            //        outputList =
            //        rsMaterialMappings.Where(
            //            t => t.MaterialRefId == mm.MaterialRefId && t.AutoSalesDeduction && t.LocationRefId == null).ToList();
            //    }
            //    //else
            //    //{
            //    //    List<int> posReferenceId = outputList.Select(t => t.PosMenuPortionRefId).ToList();
            //    //    outputList =
            //    //    rsMaterialMappings.Where(
            //    //        t => t.MaterialRefId == mm.MaterialRefId && t.AutoSalesDeduction && t.LocationRefId == null).ToList();
            //    //}
            //    if (outputList == null || outputList.Count==0)
            //    {
            //        continue;
            //    }
            //    mm.AutoSalesDeduction = true;
            //    tempList.Add(mm);
            //}
            //allRawItemsAutoSales = tempList;

            //#endregion

            var sortMenuItems = allItems.Union(allRawItemsAutoSales).ToList();
            List<int> arrMaterialRefIds = sortMenuItems.Select(t => t.MaterialRefId).ToList();
            var rsMaterialLocationWiseStock = await _materiallocationwisestockRepo.GetAllListAsync(t => arrMaterialRefIds.Contains(t.MaterialRefId));
            var dayMonthBefore = input.TransactionDate.AddDays(-15);
            //var loopIndex = 0;
            foreach (var wipeoutitem in sortMenuItems)
            {
                var editParticularMaterial =
                    await
                        _materialLedgerRepo.FirstOrDefaultAsync(
                            l =>
                                DbFunctions.TruncateTime(l.LedgerDate) == input.TransactionDate.Date &&
                                l.MaterialRefId == wipeoutitem.MaterialRefId && l.LocationRefId == input.LocationRefId);

                var newClBalance = 0.0m;

                if (editParticularMaterial == null) //  No Records Found Needs to add material for the Date
                {
                    //  Get Close Balance from Yester records

                    //var latestLedgerDate = (from led in _materialLedgerRepo.GetAll()
                    //                        orderby led.LedgerDate descending
                    //                        where led.MaterialRefId == wipeoutitem.MaterialRefId
                    //                              && led.LocationRefId == input.LocationRefId
                    //                              && DbFunctions.TruncateTime(led.LedgerDate) <= DbFunctions.TruncateTime(input.TransactionDate.Date) 
                    //                              && DbFunctions.TruncateTime(led.LedgerDate) >= DbFunctions.TruncateTime(dayMonthBefore)
                    //                        select led.LedgerDate).ToList(); 

                    //if (latestLedgerDate.Count() == 0)
                    //{
                    //    latestLedgerDate = (from led in _materialLedgerRepo.GetAll()
                    //                        orderby led.LedgerDate descending
                    //                        where led.MaterialRefId == wipeoutitem.MaterialRefId
                    //                              && led.LocationRefId == input.LocationRefId
                    //                              && DbFunctions.TruncateTime(led.LedgerDate) <=
                    //                              DbFunctions.TruncateTime(input.TransactionDate.Date)
                    //                        select led.LedgerDate).ToList();
                    //}

                    var maxLedgerDate = await GetLatestLedgerForGivenLocationMaterialRefId(new LocationAndMaterialDto { MaterialRefId = wipeoutitem.MaterialRefId, LocationRefId = input.LocationRefId, TransactionDate = input.TransactionDate.Date });

                    //var maxLedgerDate = latestLedgerDate.Max();
                    if (maxLedgerDate == null)
                    {
                        newClBalance = 0;
                    }
                    else
                    {
                        //var lastDaysRecord =
                        //    _materialLedgerRepo.FirstOrDefault(
                        //        l =>
                        //             DbFunctions.TruncateTime(l.LedgerDate.Value) == DbFunctions.TruncateTime(maxLedgerDate.Value)
                        //            && l.MaterialRefId == wipeoutitem.MaterialRefId
                        //            && l.LocationRefId == input.LocationRefId);
                        newClBalance = maxLedgerDate.ClBalance;
                    }
                }
                else
                {
                    newClBalance = editParticularMaterial.ClBalance;
                }

                wipeoutitem.ClBalance = newClBalance;

                var salefromconnect =
                    mapWithMenuListSumGroup.FirstOrDefault(t => t.MaterialRefId == wipeoutitem.MaterialRefId);
                if (salefromconnect == null)
                {
                    wipeoutitem.Sales = 0;
                }
                else
                {
                    wipeoutitem.Sales = salefromconnect.PortionQty;
                }

                var compWastage = allWastageListDtos.FirstOrDefault(t => t.MaterialRefId == wipeoutitem.MaterialRefId);
                if (compWastage != null)
                    wipeoutitem.CompWastage = compWastage.Damaged;
                var menuWastage = allMenuWastageListDtos.FirstOrDefault(t => t.MaterialRefId == wipeoutitem.MaterialRefId);
                if (menuWastage != null)
                    wipeoutitem.MenuWastage = menuWastage.Damaged;

                if (wipeoutitem.WipeOutStockOnClosingDay)
                {
                    if (wipeoutitem.Sales == (wipeoutitem.ClBalance - wipeoutitem.CompWastage - wipeoutitem.MenuWastage))
                        wipeoutitem.Remarks = L("Tallied");
                    else if (wipeoutitem.Sales > (wipeoutitem.ClBalance - wipeoutitem.CompWastage - wipeoutitem.MenuWastage))
                    {
                        wipeoutitem.Remarks = L("ReceivedEntryMissing");
                        wipeoutitem.Difference = wipeoutitem.ClBalance - wipeoutitem.Sales;
                    }
                    else
                    {
                        if (wipeoutitem.Sales == 0)
                        {
                            wipeoutitem.Remarks = L("SalesMightNotUploaded");
                        }
                        else
                        {
                            wipeoutitem.Remarks = L("WastageEntryMissing");
                        }
                        wipeoutitem.Difference = wipeoutitem.ClBalance - wipeoutitem.CompWastage - wipeoutitem.MenuWastage -
                                                              wipeoutitem.Sales;
                    }
                }
                else
                {
                    wipeoutitem.Difference = wipeoutitem.ClBalance -
                                                          wipeoutitem.Sales - wipeoutitem.CompWastage - wipeoutitem.MenuWastage;
                    if (wipeoutitem.Difference == 0)
                    {
                        wipeoutitem.Remarks = L("AllStockSold");
                    }
                    else if (wipeoutitem.Difference < 0)
                    {
                        var matLoc = rsMaterialLocationWiseStock.FirstOrDefault(t => t.MaterialRefId == wipeoutitem.MaterialRefId);

                        if (matLoc == null)
                        {
                            throw new UserFriendlyException("Location Stock is Empty for Material : " + wipeoutitem.MaterialRefId + " / " + wipeoutitem.MaterialRefName);
                        }
                        if (matLoc.ConvertAsZeroStockWhenClosingStockNegative)
                        {
                            wipeoutitem.Remarks = L("NegativeStockBecomesZero");
                        }
                        else
                        {
                            wipeoutitem.Remarks = L("ReceivedOrProductionEntryMissing");
                        }
                    }
                    else
                    {
                        wipeoutitem.Remarks = L("CarryOverToNextDay");
                    }
                }

                if (input.LedgerAffect && wipeoutitem.Sales > 0)
                {
                    var ml = new MaterialLedgerDto();
                    ml.LocationRefId = input.LocationRefId;
                    ml.MaterialRefId = wipeoutitem.MaterialRefId;
                    ml.LedgerDate = input.TransactionDate;
                    ml.Sales = wipeoutitem.Sales;
                    ml.NegativeStockAccepted = negativeStockAccepted;
                    ml.DoesBackGroundDayCloseProcess = doesBackGroundDayClose;
                    var matLedgerId = await UpdateMaterialLedger(ml);
                }
            }

            var allListDtos = sortMenuItems.MapTo<List<MaterialSalesWithWipeOutCloseDayDto>>();

            allListDtos = allListDtos.OrderBy(t => t.WipeOutStockOnClosingDay).OrderBy(t => t.Remarks).ToList();

            var allItemCount = sortMenuItems.Count();

            result.SalesData = allListDtos;

            #endregion AutoSalesCalculation

            var negativeStockConvertAsZeroStockList = sortMenuItems.Where(t => t.Remarks.Equals(L("NegativeStockBecomesZero"))).ToList();

            #region AdjustmentForCompWastage

            var adjustmentDetailDtos = new List<AdjustmentDetailViewDto>();

            if (input.LedgerAffect)
            {
                var adjSno = 1;

                var newAdjDto = new CreateOrUpdateAdjustmentInput();
                if (allWastageListDtos.Count > 0)
                {
                    var masDto = new AdjustmentEditDto();

                    if (input.TransactionDate == null)
                    {
                        var tloc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
                        masDto.AdjustmentDate = tloc.HouseTransactionDate.Value.Date;
                    }
                    else
                        masDto.AdjustmentDate = input.TransactionDate.Date;

                    masDto.AdjustmentRemarks = L("CompWastage");
                    masDto.ApprovedPersonId = (int)userId;
                    masDto.LocationRefId = input.LocationRefId;
                    masDto.EnteredPersonId = (int)userId;
                    masDto.TokenRefNumber = 555;
                    masDto.TenantId = tenantId;

                    newAdjDto.Adjustment = masDto;
                    newAdjDto.AdjustmentDetail = adjustmentDetailDtos;

                    var adjMasDto = masDto.MapTo<Adjustment>();

                    var locationAdj = await _locationRepo.FirstOrDefaultAsync(t => t.Id == adjMasDto.LocationRefId);
                    adjMasDto.AccountDate = locationAdj.HouseTransactionDate.Value;

                    var adjId = await _adjustmentRepo.InsertOrUpdateAndGetIdAsync(adjMasDto);

                    foreach (var items in allWastageListDtos)
                    {
                        var det = new AdjustmentDetail();
                        var adjustmentQty = items.Damaged;
                        var adjustmentMode = L("Damaged");

                        if (items.DefaultUnitName.Length == 0 || items.DefaultUnitId == 0)
                        {
                            throw new UserFriendlyException("UnitErr");
                        }
                        det.AdjustmentRefIf = adjId;
                        det.MaterialRefId = items.MaterialRefId;
                        det.AdjustmentQty = adjustmentQty;
                        det.Sno = adjSno++;
                        det.AdjustmentMode = adjustmentMode;
                        det.AdjustmentApprovedRemarks = "";
                        det.UnitRefId = items.DefaultUnitId;

                        await _adjustmentdetailRepo.InsertAndGetIdAsync(det);
                        var ml = new MaterialLedgerDto
                        {
                            LocationRefId = input.LocationRefId,
                            MaterialRefId = det.MaterialRefId,
                            LedgerDate = masDto.AdjustmentDate
                        };
                        if (det.AdjustmentMode.Equals(L("Excess")))
                            ml.ExcessReceived = det.AdjustmentQty;
                        else if (det.AdjustmentMode.Equals(L("Shortage")))
                            ml.Shortage = det.AdjustmentQty;
                        else if (det.AdjustmentMode.Equals(L("Wastage")))
                            ml.Shortage = det.AdjustmentQty;
                        else if (det.AdjustmentMode.Equals(L("Damaged")))
                            ml.Damaged = det.AdjustmentQty;
                        else
                            throw new UserFriendlyException(L("AdjustmentMode") + " " + L("Error") + det.AdjustmentMode);
                        ml.UnitId = det.UnitRefId;
                        ml.NegativeStockAccepted = negativeStockAccepted;
                        ml.DoesBackGroundDayCloseProcess = doesBackGroundDayClose;
                        var matLedgerId = await UpdateMaterialLedger(ml);
                    }
                }
            }

            #endregion AdjustmentForCompWastage

            #region AdjustmentForMenuWastage

            if (input.LedgerAffect)
            {
                var adjSno = 1;

                if (allMenuWastageListDtos.Count > 0)
                {
                    var masDto = new AdjustmentEditDto();

                    if (input.TransactionDate == null)
                    {
                        var tloc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
                        masDto.AdjustmentDate = tloc.HouseTransactionDate.Value.Date;
                    }
                    else
                        masDto.AdjustmentDate = input.TransactionDate.Date;

                    masDto.AdjustmentRemarks = L("MenuWastage");
                    masDto.ApprovedPersonId = (int)userId;
                    masDto.LocationRefId = input.LocationRefId;
                    masDto.EnteredPersonId = (int)userId;
                    masDto.TokenRefNumber = 444;
                    masDto.TenantId = tenantId;

                    var adjMasDto = masDto.MapTo<Adjustment>();

                    var locationAdj = await _locationRepo.FirstOrDefaultAsync(t => t.Id == adjMasDto.LocationRefId);
                    adjMasDto.AccountDate = locationAdj.HouseTransactionDate.Value;

                    var adjId = await _adjustmentRepo.InsertOrUpdateAndGetIdAsync(adjMasDto);

                    foreach (var items in allMenuWastageListDtos)
                    {
                        var det = new AdjustmentDetail();
                        var adjustmentQty = items.Damaged;
                        var adjustmentMode = L("Damaged");

                        if (items.DefaultUnitName.Length == 0 || items.DefaultUnitId == 0)
                        {
                            throw new UserFriendlyException("UnitErr");
                        }
                        det.AdjustmentRefIf = adjId;
                        det.MaterialRefId = items.MaterialRefId;
                        det.AdjustmentQty = adjustmentQty;
                        det.Sno = adjSno++;
                        det.AdjustmentMode = adjustmentMode;
                        det.AdjustmentApprovedRemarks = "";
                        det.UnitRefId = items.DefaultUnitId;

                        await _adjustmentdetailRepo.InsertAndGetIdAsync(det);
                        var ml = new MaterialLedgerDto
                        {
                            LocationRefId = input.LocationRefId,
                            MaterialRefId = det.MaterialRefId,
                            LedgerDate = masDto.AdjustmentDate
                        };
                        if (det.AdjustmentMode.Equals(L("Excess")))
                            ml.ExcessReceived = det.AdjustmentQty;
                        else if (det.AdjustmentMode.Equals(L("Shortage")))
                            ml.Shortage = det.AdjustmentQty;
                        else if (det.AdjustmentMode.Equals(L("Wastage")))
                            ml.Shortage = det.AdjustmentQty;
                        else if (det.AdjustmentMode.Equals(L("Damaged")))
                            ml.Damaged = det.AdjustmentQty;
                        else
                            throw new UserFriendlyException(L("AdjustmentMode") + " " + L("Error") + det.AdjustmentMode);
                        ml.UnitId = det.UnitRefId;
                        ml.NegativeStockAccepted = negativeStockAccepted;
                        ml.DoesBackGroundDayCloseProcess = doesBackGroundDayClose;
                        var matLedgerId = await UpdateMaterialLedger(ml);
                    }

                    var existMenuWastages = await _menuitemwastageRepo.GetAll()
                        .Where(
                            t =>
                                DbFunctions.TruncateTime(t.SalesDate) == DbFunctions.TruncateTime(input.TransactionDate))
                        .ToListAsync();
                    foreach (var a in existMenuWastages)
                    {
                        var editMenuWastage = await _menuitemwastageRepo.GetAsync(a.Id);
                        editMenuWastage.AccountDate = input.TransactionDate.Date;
                        editMenuWastage.AdjustmentRefId = adjId;

                        await _menuitemwastageRepo.InsertOrUpdateAndGetIdAsync(editMenuWastage);
                    }
                }
            }

            #endregion AdjustmentForMenuWastage

            #region Adjustment For Negative Stock Becomes Zero

            if (input.LedgerAffect)
            {
                var adjSno = 1;
                if (negativeStockConvertAsZeroStockList.Count > 0)
                {
                    var masDto = new AdjustmentEditDto();

                    if (input.TransactionDate == null)
                    {
                        var tloc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
                        masDto.AdjustmentDate = tloc.HouseTransactionDate.Value.Date;
                    }
                    else
                        masDto.AdjustmentDate = input.TransactionDate.Date;

                    masDto.AdjustmentRemarks = L("NegativeStockBecomesZero");
                    masDto.ApprovedPersonId = (int)userId;
                    masDto.LocationRefId = input.LocationRefId;
                    masDto.EnteredPersonId = (int)userId;
                    masDto.TokenRefNumber = 222;
                    masDto.TenantId = tenantId;

                    var adjMasDto = masDto.MapTo<Adjustment>();

                    var locationAdj = await _locationRepo.FirstOrDefaultAsync(t => t.Id == adjMasDto.LocationRefId);
                    adjMasDto.AccountDate = locationAdj.HouseTransactionDate.Value;

                    var adjId = await _adjustmentRepo.InsertOrUpdateAndGetIdAsync(adjMasDto);
                    var adjustmentMode = L("Excess");

                    foreach (var items in negativeStockConvertAsZeroStockList)
                    {
                        var det = new AdjustmentDetail();
                        var adjustmentQty = Math.Abs(items.Difference);

                        if (items.DefaultUnitName.Length == 0 || items.DefaultUnitId == 0)
                        {
                            throw new UserFriendlyException("UnitErr");
                        }
                        det.AdjustmentRefIf = adjId;
                        det.MaterialRefId = items.MaterialRefId;
                        det.AdjustmentQty = adjustmentQty;
                        det.Sno = adjSno++;
                        det.AdjustmentMode = adjustmentMode;
                        det.AdjustmentApprovedRemarks = "";
                        det.UnitRefId = items.DefaultUnitId;

                        await _adjustmentdetailRepo.InsertAndGetIdAsync(det);
                        var ml = new MaterialLedgerDto
                        {
                            LocationRefId = input.LocationRefId,
                            MaterialRefId = det.MaterialRefId,
                            LedgerDate = masDto.AdjustmentDate
                        };
                        if (det.AdjustmentMode.Equals(L("Excess")))
                            ml.ExcessReceived = det.AdjustmentQty;
                        else if (det.AdjustmentMode.Equals(L("Shortage")))
                            ml.Shortage = det.AdjustmentQty;
                        else if (det.AdjustmentMode.Equals(L("Wastage")))
                            ml.Shortage = det.AdjustmentQty;
                        else if (det.AdjustmentMode.Equals(L("Damaged")))
                            ml.Damaged = det.AdjustmentQty;
                        else
                            throw new UserFriendlyException(L("AdjustmentMode") + " " + L("Error") + det.AdjustmentMode);
                        ml.UnitId = det.UnitRefId;
                        ml.NegativeStockAccepted = negativeStockAccepted;
                        ml.DoesBackGroundDayCloseProcess = doesBackGroundDayClose;
                        var matLedgerId = await UpdateMaterialLedger(ml);
                    }
                }
            }

            #endregion Adjustment For Negative Stock Becomes Zero

            result.DayCloseSuccessFlag = true;

            if (input.LedgerAffect == true && input.DoesDayCloseRequiredInBackGround)
            {
                if (input.TransactionDate.Date == DateTime.Today && sameDayCloseAllowed == false)
                //@@Pending Upto Synchranize With Sales BackUp
                {
                    throw new UserFriendlyException(L("YouCanNotCloseDateForTodayAsOfNow"));
                }

                if (input.TransactionDate.Date == DateTime.Today && sameDayCloseAllowed == true)
                //@@Pending Upto Synchranize With Sales BackUp
                {
                    if (DateTime.Now.Hour < 15)
                    //@@Pending Upto Synchranize With Sales BackUp
                    {
                        throw new UserFriendlyException(L("YouCanNotCloseDateForTodayAsOfNow"));
                    }
                }

                var dayToBeAssignedAsTransactionDate = input.TransactionDate.Date.AddDays(1);

                if (dayToBeAssignedAsTransactionDate > DateTime.Today.AddDays(1))
                {
                    throw new UserFriendlyException(L("YouCanNotCloseDateForFuture"));
                }

                var salesAccountDate = input.TransactionDate.Date;

                var locations = await _locationRepo.GetAll().Where(t => t.Id == input.LocationRefId).ToListAsync();

                var locList = locations.MapTo<List<LocationListDto>>();

                var item = await _locationRepo.GetAsync(input.LocationRefId);

                item.HouseTransactionDate = dayToBeAssignedAsTransactionDate;
                item.DoesDayCloseRunInBackGround = false;
                item.BackGrandEndTime = DateTime.Now;
                await _locationRepo.UpdateAsync(item);
            }
            return result;
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetMaterialTypeForCombobox(NullableIdInput ninput)
        {
            var retList = new List<ComboboxItemDto>();

            string enumstring;
            var EnumValues = Enum.GetValues(typeof(MaterialType));
            foreach (int EnumValue in EnumValues)
            {
                enumstring = Enum.GetName(typeof(MaterialType), EnumValue);
                retList.Add(new ComboboxItemDto { Value = EnumValue.ToString(), DisplayText = enumstring });
            }

            return
                new ListResultDto<ComboboxItemDto>(
                    retList.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText)).ToList());
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetUnitForCombobox()
        {
            var lst = await _unitRepo.GetAll().ToListAsync();

            return
                new ListResultDto<ComboboxItemDto>(
                    lst.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }

        public async Task<UnitConversionEditDto> GetDefaultUnitConversionDataForGivenData(UnitConversionEditDto input)
        {
            var uc = await _unitconversionRepo.FirstOrDefaultAsync(t => t.BaseUnitId == input.BaseUnitId && t.RefUnitId == input.RefUnitId && t.MaterialRefId == null);
            if (uc != null)
            {
                var a = uc.MapTo<UnitConversionEditDto>();
                return a;
            }

            else
                return new UnitConversionEditDto();
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetInventoryStockCycles()
        {
            var lst = await _inventorycycletagRepo.GetAll().ToListAsync();

            return
                new ListResultDto<ComboboxItemDto>(
                    lst.Select(e => new ComboboxItemDto(e.Id.ToString(), e.InventoryCycleTagCode)).ToList());
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetMaterialGroupForCombobox()
        {
            var lstMaterialGroupCategory = await (from matgroupcat in _materialgroupRepo.GetAll()
                                                  select new MaterialGroupListDto
                                                  {
                                                      Id = matgroupcat.Id,
                                                      MaterialGroupName = matgroupcat.MaterialGroupName
                                                  }).ToListAsync();

            return
                new ListResultDto<ComboboxItemDto>(
                    lstMaterialGroupCategory.Select(
                        e => new ComboboxItemDto(e.Id.ToString(), e.MaterialGroupName)).ToList());
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetMaterialGroupCategoryForCombobox(NullableIdInput input)
        {

            var lstMaterialGroupCategory = await (from matgroupcat in _materialGroupCategoryRepo.GetAll()
                                                  join matgroup in _materialgroupRepo.GetAll()
                                                    .Where(t => t.TenantId == AbpSession.TenantId)
                                                    .WhereIf(input.Id.HasValue && input.Id > 0, t => t.Id == input.Id.Value)
                                                      on matgroupcat.MaterialGroupRefId equals matgroup.Id
                                                  select new MaterialGroupCategoryListDto
                                                  {
                                                      Id = matgroupcat.Id,
                                                      MaterialGroupRefId = matgroupcat.MaterialGroupRefId,
                                                      MaterialGroupCategoryName = matgroupcat.MaterialGroupCategoryName
                                                  }).ToListAsync();

            return
                new ListResultDto<ComboboxItemDto>(
                    lstMaterialGroupCategory.Select(
                        e => new ComboboxItemDto(e.Id.ToString(), e.MaterialGroupCategoryName)).ToList());
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetMaterialGroupCategoryForGivenInput(ComboBoxListInput input)
        {
            List<int> arrGroupRefIds = new List<int>();
            if (input.ComboboxItemList.Count > 0)
            {
                arrGroupRefIds = input.ComboboxItemList.Select(t => int.Parse(t.Value)).ToList();
            }
            var lstMaterialGroupCategory = await (from matgroupcat in _materialGroupCategoryRepo.GetAll()
                                                  join matgroup in _materialgroupRepo.GetAll()
                                                    .Where(t => t.TenantId == AbpSession.TenantId)
                                                    .WhereIf(input.ComboboxItemList.Count > 0 && arrGroupRefIds.Count > 0, t => arrGroupRefIds.Contains(t.Id))
                                                      on matgroupcat.MaterialGroupRefId equals matgroup.Id
                                                  select new MaterialGroupCategoryListDto
                                                  {
                                                      Id = matgroupcat.Id,
                                                      MaterialGroupRefId = matgroupcat.MaterialGroupRefId,
                                                      MaterialGroupCategoryName = matgroupcat.MaterialGroupCategoryName
                                                  }).ToListAsync();

            return
                new ListResultDto<ComboboxItemDto>(
                    lstMaterialGroupCategory.Select(
                        e => new ComboboxItemDto(e.Id.ToString(), e.MaterialGroupCategoryName)).ToList());
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetUnitForMaterialLinkCombobox(EntityDto input)
        {
            var lst = await (from u in _unitRepo.GetAll()
                             join mu in _materialUnitLinkRepo.GetAll().Where(a => a.MaterialRefId == input.Id)
                                 on u.Id equals mu.UnitRefId
                             select new ComboboxItemDto
                             {
                                 Value = u.Id.ToString(),
                                 DisplayText = u.Name
                             }).ToListAsync();

            if (lst.Count == 0)
            {
                lst = await (from u in _unitRepo.GetAll()
                             join mu in _materialRepo.GetAll().Where(a => a.Id == input.Id)
                                 on u.Id equals mu.DefaultUnitId
                             select new ComboboxItemDto
                             {
                                 Value = u.Id.ToString(),
                                 DisplayText = u.Name
                             }).ToListAsync();
            }

            return new ListResultDto<ComboboxItemDto>(lst.MapTo<List<ComboboxItemDto>>());
        }

        public async Task<List<MaterialMenuMappingWithWipeOut>> GetLiveStockSales(GetMaterialLocationWiseStockInput input)
        {
            var mapWithMenuListSumGroup = new List<MaterialMenuMappingWithWipeOut>();

            if (input.LiveStock)
            {
                var loc = await _locationRepo.GetAll().Where(t => t.Id == input.LocationRefId).ToListAsync();
                var locinput = loc.MapTo<List<LocationListDto>>();

                var tempLocs = await _locationRepo.GetAll()
        .Where(t => t.DayCloseSalesStockAdjustmentLocationRefId.HasValue && t.DayCloseSalesStockAdjustmentLocationRefId == input.LocationRefId).ToListAsync();
                var linkedLocs = tempLocs.MapTo<List<LocationListDto>>();
                locinput.AddRange(linkedLocs);

                var fromDt = DateTime.Today;
                var toDt = DateTime.Today;

                if (loc[0].HouseTransactionDate != null)
                {
                    fromDt = loc[0].HouseTransactionDate.Value;
                    if (input.EndDate == null)
                        toDt = DateTime.Today;
                    else
                        toDt = input.EndDate.Value.Date;
                }

                List<int> arrMenuItemPortionIds = new List<int>();
                if (input.MaterialRefIds != null && input.MaterialRefIds.Any())
                {
                    var rsMaterialMenuMappings = await _materialmenumappingRepo.GetAllListAsync(t => input.MaterialRefIds.Contains(t.MaterialRefId));
                    arrMenuItemPortionIds = rsMaterialMenuMappings.Select(t => t.PosMenuPortionRefId).ToList();
                }

                var daySales = await _connectReportAppService.GetAllItemSales(
                  new GetItemInput
                  {
                      StartDate = fromDt,
                      EndDate = toDt,
                      Locations = locinput.MapTo<List<SimpleLocationDto>>(),
                      Sales = true,
                      Comp = true,
                      //Void = true,
                      Gift = true,
                      MenuItemPortionIds = arrMenuItemPortionIds
                  });

                var rsMaterials = await _materialRepo.GetAllListAsync();
                var rsMaterialMappings = await _materialmenumappingRepo.GetAllListAsync();
                var rsMenumappingDepartment = await _menumappingDepartmentRepo.GetAllListAsync();
                var rsDeptList = await _departmentRepo.GetAll().Select(t => new { t.Id, t.Name }).ToListAsync();

                var mapwithMenuList = new List<MaterialMenuMappingWithWipeOut>();
                var itemSales = daySales.Sales.MapTo<List<MenuListDto>>();

                if (daySales.Gift.Count > 0)
                    itemSales.AddRange(daySales.Gift);
                if (daySales.Comp.Count > 0)
                    itemSales.AddRange(daySales.Comp);

                #region Old

                //foreach (var item in itemSales)
                //{
                //    var outputlist =
                //        rsMaterialMenuMapping.Where(
                //            t => t.PosMenuPortionRefId == item.MenuItemPortionId && t.AutoSalesDeduction && t.LocationRefId == input.LocationRefId).ToList();

                //    if (outputlist == null || outputlist.Count == 0)
                //    {
                //        outputlist = rsMaterialMenuMapping.Where(
                //            t => t.PosMenuPortionRefId == item.MenuItemPortionId && t.AutoSalesDeduction && t.LocationRefId == null).ToList();
                //    }

                //    foreach (var a in outputlist)
                //    {
                //        var newmm = new MaterialMenuMappingWithWipeOut();
                //        if (a.MaterialRefId==15)
                //        {
                //            int tempa = 45;
                //        }
                //        newmm.PosMenuPortionRefId = a.PosMenuPortionRefId;
                //        newmm.MenuQuantitySold = a.MenuQuantitySold;
                //        newmm.MaterialRefId = a.MaterialRefId;
                //        newmm.PortionQty = Math.Round(a.PortionQty / a.MenuQuantitySold * item.Quantity, 14);
                //        newmm.PortionUnitId = a.PortionUnitId;
                //        newmm.TotalSaleQuantity = item.Quantity;
                //        newmm.AutoSalesDeduction = a.AutoSalesDeduction;

                //        var mat = rsMaterial.FirstOrDefault(t => t.Id == a.MaterialRefId);
                //        if (mat == null)
                //        {
                //            throw new UserFriendlyException(L("Material") + " " + L("NotExist") + " " + L("Id") + " " +
                //                                            newmm.MaterialRefId);
                //        }

                //        newmm.DefaultUnitId = mat.DefaultUnitId;
                //        newmm.MaterialRefName = mat.MaterialName;

                //        mapwithMenuList.Add(newmm);
                //    }
                //}

                #endregion Old

                foreach (var item in itemSales)
                {
                    var menuMapping = rsMaterialMappings.Where(t => t.PosMenuPortionRefId == item.MenuItemPortionId
                                               && t.AutoSalesDeduction && t.LocationRefId == input.LocationRefId).ToList();
                    if (menuMapping == null || menuMapping.Count == 0)
                    {
                        menuMapping = rsMaterialMappings.Where(t => t.PosMenuPortionRefId == item.MenuItemPortionId
                                               && t.AutoSalesDeduction && t.LocationRefId == null).ToList();
                    }

                    var department = rsDeptList.FirstOrDefault(t => t.Name.ToUpper().Equals(item.DepartmentName.ToUpper()));

                    if (department == null)
                    {
                        throw new UserFriendlyException(L("Department") + " " + L("NotExist"), item.DepartmentName);
                    }

                    var deptId = department.Id;

                    var menuMappingRefIds = menuMapping.Select(t => t.Id).ToArray();

                    var deptList =
                        rsMenumappingDepartment.Where(t => menuMappingRefIds.Contains(t.MenuMappingRefId)).ToList();

                    bool AllDepartmentFlag;
                    if (deptList.Count() == 0)
                    {
                        AllDepartmentFlag = true;
                    }
                    else
                    {
                        AllDepartmentFlag = false;
                    }

                    foreach (var MmMaterial in menuMapping)
                    {
                        if (AllDepartmentFlag == false)
                        {
                            var deptLinkedMM = deptList.Where(t => t.MenuMappingRefId == MmMaterial.Id);
                            if (deptLinkedMM.Count() > 0)
                            {
                                deptLinkedMM = deptLinkedMM.Where(t => t.DepartmentRefId == deptId);
                                if (deptLinkedMM.Count() == 0)
                                {
                                    continue;
                                }
                            }
                        }

                        var newmm = new MaterialMenuMappingWithWipeOut();
                        var mat = rsMaterials.FirstOrDefault(t => t.Id == MmMaterial.MaterialRefId);

                        if (mat == null)
                        {
                            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                            {
                                var deletedMaterial = await _materialRepo.FirstOrDefaultAsync(t => t.Id == MmMaterial.MaterialRefId);
                                var deletedMaterialName = deletedMaterial == null ? "" : deletedMaterial.MaterialName;
                                throw new UserFriendlyException(L("SomeOfTheLinkedMaterialsInMenuMappingDeleted") + " " + L("MATERIALNAMEISWRONG", deletedMaterialName));
                            }
                            throw new UserFriendlyException(L("SomeOfTheLinkedMaterialsInMenuMappingDeleted"));
                        }

                        newmm.PosMenuPortionRefId = MmMaterial.PosMenuPortionRefId;
                        newmm.MenuQuantitySold = MmMaterial.MenuQuantitySold;
                        newmm.MaterialRefId = MmMaterial.MaterialRefId;
                        newmm.PortionQty = Math.Round(MmMaterial.PortionQty / MmMaterial.MenuQuantitySold * item.Quantity, 14);
                        newmm.PortionUnitId = MmMaterial.PortionUnitId;
                        newmm.TotalSaleQuantity = item.Quantity;
                        newmm.AutoSalesDeduction = MmMaterial.AutoSalesDeduction;
                        newmm.DefaultUnitId = mat.DefaultUnitId;

                        mapwithMenuList.Add(newmm);
                    }
                }

                var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();

                var mapwithMenuListGroupWithUC = (from salemenu in mapwithMenuList
                                                  join uc in rsUc
                                                      on salemenu.PortionUnitId equals uc.BaseUnitId
                                                  where uc.RefUnitId == salemenu.DefaultUnitId
                                                  group salemenu by new
                                                  {
                                                      salemenu.MaterialRefId,
                                                      salemenu.MaterialRefName,
                                                      salemenu.DefaultUnitId,
                                                      salemenu.PortionUnitId,
                                                      salemenu.UnitRefName,
                                                      salemenu.DefaultUnitName,
                                                      uc.Conversion
                                                  }
                    into g
                                                  select new MaterialMenuMappingDto
                                                  {
                                                      MaterialRefId = g.Key.MaterialRefId,
                                                      MaterialRefName = g.Key.MaterialRefName,
                                                      PortionUnitId = g.Key.PortionUnitId,
                                                      UnitRefName = g.Key.UnitRefName,
                                                      DefaultUnitId = g.Key.DefaultUnitId,
                                                      DefaultUnitName = g.Key.DefaultUnitName,
                                                      PortionQty = Math.Round(g.Sum(t => t.PortionQty * g.Key.Conversion), 14)
                                                  }).ToList().OrderBy(t => t.MaterialRefId);

                mapWithMenuListSumGroup = (from salemenu in mapwithMenuListGroupWithUC
                                           group salemenu by new { salemenu.MaterialRefId, salemenu.MaterialRefName }
                    into g
                                           select new MaterialMenuMappingWithWipeOut
                                           {
                                               MaterialRefId = g.Key.MaterialRefId,
                                               MaterialRefName = g.Key.MaterialRefName,
                                               PortionQty = g.Sum(t => t.PortionQty)
                                           }).ToList();
            }

            return mapWithMenuListSumGroup;
        }

        public async Task<GetIssueForStandardRecipe> GetIssueForCategory(GetCategoryMaterial input)
        {
            List<IssueDetailViewDto> editDetailDto;

            var rawMaterialType = L("RAW");
            var semiMaterialType = L("SEMI");
            var iqueryMaterialCategory = _materialGroupCategoryRepo.GetAll();
            if (input.MaterialGroupRefId > 0)
            {
                iqueryMaterialCategory = iqueryMaterialCategory.Where(t => t.MaterialGroupRefId == input.MaterialGroupRefId);
            }

            editDetailDto =
                await
                    (from mat in
                        _materialRepo.GetAll()
                            .Where(t => t.MaterialGroupCategoryRefId == input.MaterialGroupCategoryRefId)
                            .WhereIf(input.SemiFinishedOnly, t => t.MaterialTypeId == (int)MaterialType.SEMI)
                     join mgc in iqueryMaterialCategory on mat.MaterialGroupCategoryRefId equals mgc.Id
                     join un in _unitRepo.GetAll() on mat.DefaultUnitId equals un.Id
                     join uiss in _unitRepo.GetAll() on mat.IssueUnitId equals uiss.Id
                     join utrn in _unitRepo.GetAll() on mat.TransferUnitId equals utrn.Id
                     join matloc in _materiallocationwisestockRepo.GetAll()
                         .Where(mls => mls.LocationRefId == input.LocationRefId) on mat.Id equals
                         matloc.MaterialRefId
                     select new IssueDetailViewDto
                     {
                         MaterialRefId = mat.Id,
                         MaterialRefName = mat.MaterialName,
                         MaterialTypeRefId = mat.MaterialTypeId,
                         MaterialTypeRefName = mat.MaterialTypeId == (int)MaterialType.RAW
                             ? rawMaterialType
                             : semiMaterialType,
                         RecipeRefId = 0,
                         RequestQty = 0,
                         IssueQty = 0,
                         IssueRefId = 0,
                         Sno = 0,
                         DefaultUnitId = mat.DefaultUnitId,
                         DefaultUnitName = un.Name,
                         CurrentInHand = matloc.CurrentInHand,
                         UnitRefId = mat.IssueUnitId,
                         UnitRefName = uiss.Name,
                         TransferUnitId = mat.TransferUnitId,
                         TransferUnitRefName = utrn.Name
                     }).ToListAsync();

            return new GetIssueForStandardRecipe
            {
                IssueDetail = editDetailDto
            };
        }

        public async Task<List<MaterialMenuMappingWithWipeOut>> GetSalesDetail(DayCloseMaterialDto input)
        {
            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);

            var locinput = new List<LocationListDto>();
            locinput.Add(loc.MapTo<LocationListDto>());

            var fromDt = input.TransactionStartDate.Date;
            var toDt = input.TransactionEndDate.Date;

            if (input.MaterialRefId > 0)
            {
                if (input.MaterialRefIds == null)
                    input.MaterialRefIds = new List<int>();
                input.MaterialRefIds.Add(input.MaterialRefId);
            }

            //  Get Sales
            var rsMaterials = await _materialRepo.GetAllListAsync(t => input.MaterialRefIds.Contains(t.Id));
            var rsDeptList = await _departmentRepo.GetAll().Select(t => new { t.Id, t.Name }).ToListAsync();

            #region Template Menu Mapping

            var templateMappings = await _daycloseTemplateRepo.FirstOrDefaultAsync(
                        t => t.TemplateType == (int)DayCloseTemplateType.MaterialMenuMappings && DbFunctions.TruncateTime(t.ClosedDate) ==
                             DbFunctions.TruncateTime(input.TransactionStartDate.Date));

            List<MaterialMappingTemplateDto> rsMaterialMappings = new List<MaterialMappingTemplateDto>();
            if (templateMappings == null)
            {
                var rsTempMaterialMappings = await _materialmenumappingRepo.GetAllListAsync();
                rsMaterialMappings = rsTempMaterialMappings.MapTo<List<MaterialMappingTemplateDto>>();
            }
            else
            {
                var mmTemplate = _xmlandjsonConvertorAppService.DeSerializeFromJSON<MaterialMappingTemplate>(
                        templateMappings.TemplateData);
                rsMaterialMappings = mmTemplate.MappingList;
                rsMaterialMappings = rsMaterialMappings.Where(t => input.MaterialRefIds.Contains(t.MaterialRefId)).ToList();
            }

            #endregion Template Menu Mapping

            #region Template Menu Mapping

            var templateMappingDepartment = await _daycloseTemplateRepo.FirstOrDefaultAsync(
                        t => t.TemplateType == (int)DayCloseTemplateType.MenuMappingDepartment
                             && DbFunctions.TruncateTime(t.ClosedDate) == DbFunctions.TruncateTime(input.TransactionStartDate.Date));

            List<MenuMappingDepartmentTemplateDto> rsMenumappingDepartment = new List<MenuMappingDepartmentTemplateDto>();
            if (templateMappings == null)
            {
                var tempMenuMappingDepartment = await _menumappingDepartmentRepo.GetAllListAsync();
                rsMenumappingDepartment = tempMenuMappingDepartment.MapTo<List<MenuMappingDepartmentTemplateDto>>();
            }
            else
            {
                var mmTemplate = _xmlandjsonConvertorAppService.DeSerializeFromJSON<MenuMappingDepartmentTemplate>(
                        templateMappingDepartment.TemplateData);
                rsMenumappingDepartment = mmTemplate.MenuMappingDepartment;
            }

            #endregion Template Menu Mapping

            var templateUc = await _daycloseTemplateRepo.FirstOrDefaultAsync(
                        t => t.TemplateType == (int)DayCloseTemplateType.UnitConversions
                             && DbFunctions.TruncateTime(t.ClosedDate) == DbFunctions.TruncateTime(input.TransactionStartDate.Date));
            List<UnitConversionTemplateDto> rsUc = new List<UnitConversionTemplateDto>();
            if (templateUc == null)
            {
                var rsTempUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();
                rsUc = rsTempUc.MapTo<List<UnitConversionTemplateDto>>();
            }
            else
            {
                var ucTemplate = _xmlandjsonConvertorAppService.DeSerializeFromJSON<UnitConversionTemplate>(templateUc.TemplateData);
                rsUc = ucTemplate.UcList;
            }

            List<int> arrMenuPortionIds = new List<int>();
            rsMaterialMappings = rsMaterialMappings.Where(t => input.MaterialRefIds.Contains(t.MaterialRefId)).ToList();
            arrMenuPortionIds = rsMaterialMappings.Select(t => t.PosMenuPortionRefId).ToList();

            #region AutoSalesCalculation

            var daySales = await _connectReportAppService.GetAllItemSales(
                    new GetItemInput
                    {
                        StartDate = fromDt,
                        EndDate = toDt,
                        Locations = locinput.MapTo<List<SimpleLocationDto>>(),
                        Sales = true,
                        Comp = true,
                        //Void = true,
                        Gift = true,
                        MenuItemPortionIds = arrMenuPortionIds
                    });

            var mapwithMenuList = new List<MaterialMenuMappingWithWipeOut>();
            var itemSales = daySales.Sales.MapTo<List<MenuListDto>>();

            if (daySales.Gift.Count > 0)
                itemSales.AddRange(daySales.Gift);
            if (daySales.Comp.Count > 0)
                itemSales.AddRange(daySales.Comp);

            var menuRefIds = rsMaterialMappings.Select(t => t.PosMenuPortionRefId).ToArray();
            itemSales = itemSales.Where(t => menuRefIds.Contains(t.MenuItemPortionId)).ToList();

            #region Old

            //foreach (var item in itemSales)
            //{
            //    var outputlist = rsMaterialMappings.Where(t => t.PosMenuPortionRefId == item.MenuItemPortionId && t.MaterialRefId == input.MaterialRefId).ToList();

            //    foreach (var a in outputlist)
            //    {
            //        var newmm = new MaterialMenuMappingWithWipeOut();
            //        var mat = rsMaterials.FirstOrDefault(t => t.Id == a.MaterialRefId);

            //        if (mat == null)
            //        {
            //using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            //{
            //    var deletedMaterial = await _materialRepo.FirstOrDefaultAsync(t => t.Id == a.MaterialRefId);
            //    var deletedMaterialName = deletedMaterial == null ? "" : deletedMaterial.MaterialName;
            //    throw new UserFriendlyException(L("SomeOfTheLinkedMaterialsInMenuMappingDeleted") + " " + L("MATERIALNAMEISWRONG", deletedMaterialName));
            //}
            //            throw new UserFriendlyException(L("SomeOfTheLinkedMaterialsInMenuMappingDeleted"));
            //        }

            //        newmm.DepartmentName = item.DepartmentName;
            //        newmm.PosMenuPortionRefId = a.PosMenuPortionRefId;
            //        newmm.PosRefName = item.MenuItemName + " " + item.MenuItemPortionName;
            //        newmm.MenuQuantitySold = a.MenuQuantitySold;
            //        newmm.MaterialRefId = a.MaterialRefId;
            //        newmm.PortionPerUnitOfSales = a.PortionQty;
            //        newmm.PortionQty = Math.Round(a.PortionQty / a.MenuQuantitySold
            //                                      * item.Quantity, 8);
            //        newmm.PortionUnitId = a.PortionUnitId;
            //        newmm.TotalSaleQuantity = item.Quantity;
            //        newmm.AutoSalesDeduction = a.AutoSalesDeduction;
            //        newmm.DefaultUnitId = mat.DefaultUnitId;

            //        mapwithMenuList.Add(newmm);
            //    }
            //}

            #endregion Old

            foreach (var item in itemSales)
            {
                var menuMapping = rsMaterialMappings.Where(t => t.PosMenuPortionRefId == item.MenuItemPortionId
                                                                && t.AutoSalesDeduction && t.LocationRefId == input.LocationRefId).ToList();
                if (menuMapping == null || menuMapping.Count == 0)
                {
                    menuMapping = rsMaterialMappings.Where(t => t.PosMenuPortionRefId == item.MenuItemPortionId
                                                                && t.AutoSalesDeduction && t.LocationRefId == null).ToList();
                }

                var department = rsDeptList.FirstOrDefault(t => t.Name.ToUpper().Equals(item.DepartmentName.ToUpper()));

                if (department == null)
                {
                    throw new UserFriendlyException(L("Department") + " " + L("NotExist"), item.DepartmentName);
                }

                var deptId = department.Id;

                var menuMappingRefIds = menuMapping.Select(t => t.Id).ToArray();

                var deptList =
                    rsMenumappingDepartment.Where(t => menuMappingRefIds.Contains(t.MenuMappingRefId)).ToList();

                bool AllDepartmentFlag;
                if (deptList.Count() == 0)
                {
                    AllDepartmentFlag = true;
                }
                else
                {
                    AllDepartmentFlag = false;
                }

                foreach (var MmMaterial in menuMapping)
                {
                    if (AllDepartmentFlag == false)
                    {
                        var deptLinkedMM = deptList.Where(t => t.MenuMappingRefId == MmMaterial.Id);
                        if (deptLinkedMM.Count() > 0)
                        {
                            deptLinkedMM = deptLinkedMM.Where(t => t.DepartmentRefId == deptId);
                            if (deptLinkedMM.Count() == 0)
                            {
                                continue;
                            }
                        }
                    }

                    var newmm = new MaterialMenuMappingWithWipeOut();
                    var mat = rsMaterials.FirstOrDefault(t => t.Id == MmMaterial.MaterialRefId);

                    if (mat == null)
                    {
                        using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                        {
                            var deletedMaterial = await _materialRepo.FirstOrDefaultAsync(t => t.Id == MmMaterial.MaterialRefId);
                            var deletedMaterialName = deletedMaterial == null ? "" : deletedMaterial.MaterialName;
                            throw new UserFriendlyException(L("SomeOfTheLinkedMaterialsInMenuMappingDeleted") + " " + L("MATERIALNAMEISWRONG", deletedMaterialName));
                        }
                        throw new UserFriendlyException(L("SomeOfTheLinkedMaterialsInMenuMappingDeleted"));
                    }

                    //        newmm.MenuQuantitySold = a.MenuQuantitySold;
                    //        newmm.MaterialRefId = a.MaterialRefId;
                    //        newmm.PortionQty = Math.Round(a.PortionQty / a.MenuQuantitySold
                    //                                      * item.Quantity, 8);
                    //        newmm.PortionUnitId = a.PortionUnitId;
                    newmm.DepartmentName = item.DepartmentName;
                    newmm.PosRefName = item.MenuItemName + " " + item.MenuItemPortionName;
                    newmm.PortionPerUnitOfSales = MmMaterial.PortionQty;
                    newmm.PosMenuPortionRefId = MmMaterial.PosMenuPortionRefId;
                    newmm.MenuQuantitySold = MmMaterial.MenuQuantitySold;
                    newmm.MaterialRefId = MmMaterial.MaterialRefId;
                    newmm.PortionQty = Math.Round(MmMaterial.PortionQty / MmMaterial.MenuQuantitySold * item.Quantity, 14);
                    newmm.PortionUnitId = MmMaterial.PortionUnitId;
                    newmm.TotalSaleQuantity = item.Quantity;
                    newmm.AutoSalesDeduction = MmMaterial.AutoSalesDeduction;
                    newmm.DefaultUnitId = mat.DefaultUnitId;

                    mapwithMenuList.Add(newmm);
                }
            }
            var mapwithMenuListGroupWithUnitConversion
                = (from salemenu in mapwithMenuList
                   join uc in rsUc
                       on salemenu.PortionUnitId equals uc.BaseUnitId
                   where uc.RefUnitId == salemenu.DefaultUnitId
                   group salemenu by new
                   {
                       salemenu.DepartmentName,
                       salemenu.PosMenuPortionRefId,
                       salemenu.PosRefName,
                       salemenu.MaterialRefId,
                       salemenu.MaterialRefName,
                       salemenu.PortionPerUnitOfSales,
                       salemenu.DefaultUnitId,
                       salemenu.PortionUnitId,
                       salemenu.UnitRefName,
                       salemenu.DefaultUnitName,
                       uc.Conversion
                   }
                    into g
                   select new MaterialMenuMappingWithWipeOut
                   {
                       DepartmentName = g.Key.DepartmentName,
                       PosMenuPortionRefId = g.Key.PosMenuPortionRefId,
                       PosRefName = g.Key.PosRefName,
                       TotalSaleQuantity = g.Sum(t => t.TotalSaleQuantity),
                       MaterialRefId = g.Key.MaterialRefId,
                       MaterialRefName = g.Key.MaterialRefName,
                       PortionPerUnitOfSales = g.Key.PortionPerUnitOfSales * g.Key.Conversion,
                       PortionUnitId = g.Key.PortionUnitId,
                       UnitRefName = g.Key.UnitRefName,
                       DefaultUnitId = g.Key.DefaultUnitId,
                       DefaultUnitName = g.Key.DefaultUnitName,
                       PortionQty = g.Sum(t => t.PortionQty * g.Key.Conversion)
                   }).ToList().OrderBy(t => t.MaterialRefId);

            var mapWithMenuListSumGroup
                = (from salemenu in mapwithMenuListGroupWithUnitConversion
                   group salemenu by new
                   {
                       salemenu.DepartmentName,
                       salemenu.MaterialRefId,
                       salemenu.MaterialRefName,
                       salemenu.PosMenuPortionRefId,
                       salemenu.PosRefName,
                       salemenu.PortionPerUnitOfSales,
                       salemenu.DefaultUnitId,
                       salemenu.DefaultUnitName
                   }
                    into g
                   select new MaterialMenuMappingWithWipeOut
                   {
                       DepartmentName = g.Key.DepartmentName,
                       PosMenuPortionRefId = g.Key.PosMenuPortionRefId,
                       PosRefName = g.Key.PosRefName,
                       TotalSaleQuantity = g.Sum(t => t.TotalSaleQuantity),
                       MaterialRefId = g.Key.MaterialRefId,
                       MaterialRefName = g.Key.MaterialRefName,
                       PortionPerUnitOfSales = g.Key.PortionPerUnitOfSales,
                       PortionQty = g.Sum(t => t.PortionQty),
                       DefaultUnitName = g.Key.DefaultUnitName,
                       DefaultUnitId = g.Key.DefaultUnitId
                   }).ToList();

            return mapWithMenuListSumGroup;

            #endregion AutoSalesCalculation
        }

        public async Task<bool> IsBarcodeExists(string barcode)
        {
            if (!string.IsNullOrEmpty(barcode))
            {
                var count = await _materialbarcodeRepo.CountAsync(a => a.Barcode.Equals(barcode));
                if (count > 0)
                    return true;
                else
                {
                    var isMaterialCodeTreatedAsBarCode = await _settingManager.GetSettingValueAsync<bool>(AppSettings.HouseSettings.IsMaterialCodeTreatedAsBarCode);

                    if (isMaterialCodeTreatedAsBarCode)
                    {
                        count = await _materialRepo.CountAsync(t => t.MaterialPetName.Equals(barcode));
                        return count > 0;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            return false;
        }

        public async Task<bool> CloneMenuItemAsMaterial(MenuItem menuinput)
        {
            var unit =
                await
                    _unitRepo.FirstOrDefaultAsync(t => t.Name.ToUpper().Equals("PC") || t.Name.ToUpper().Equals("PCS"));

            if (unit == null)
            {
                throw new UserFriendlyException("PC Unit" + L("NotExist"));
            }

            var menuPortions = await _menuitemportionRepo.GetAllListAsync(t => t.MenuItemId == menuinput.Id);
            if (menuPortions.Count > 1)
            {
                throw new UserFriendlyException(L("MoreThanOnePortionsCanNotBeCloned"));
            }

            var rsmenubarcodes = await _menubarcodeRepo.GetAllListAsync(t => t.MenuItemId == menuinput.Id);

            var mgcdto
                = await _materialgroupcategoryAppService.GetOrCreateByName("GENERAL", "GENERAL");

            CreateOrUpdateMaterialInput input = null;
            var matCount = await _materialRepo.CountAsync();

            if (mgcdto.Id != null)
            {
                var materialDto = new MaterialEditDto
                {
                    MaterialGroupCategoryRefId = (int)mgcdto.Id,
                    MaterialName = menuinput.Name,
                    MaterialPetName = menuinput.AliasName,
                    MaterialTypeId = (int)MaterialType.RAW,
                    DefaultUnitId = unit.Id,
                    IssueUnitId = unit.Id,
                    Barcode = menuinput.BarCode,
                    UserSerialNumber = matCount + 1
                };

                var mabarcode = new Collection<MaterialBarCodeEditDto>();
                foreach (var mb in menuinput.Barcodes)
                {
                    mabarcode.Add(new MaterialBarCodeEditDto { Barcode = mb.Barcode });
                }
                materialDto.Barcodes = mabarcode;

                input = new CreateOrUpdateMaterialInput
                {
                    Material = materialDto
                };
            }

            if (input != null)
            {
                var matid = await CreateOrUpdateMaterial(input);
                return true;
            }
            return false;
        }

        [UnitOfWork]
        public async Task<MaterialChangeOutputDto> DefaultUnitChangeProcess(MaterialUnitChangeDto input)
        {
            var output = new MaterialChangeOutputDto();

            var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();

            #region CheckInputDataConstraints

            // Check Data
            var material = await _materialRepo.FirstOrDefaultAsync(t => t.Id == input.MaterialRefId);
            if (material == null)
            {
                throw new UserFriendlyException(L("Material") + " " + L("NotExists"));
            }

            if (input.RevisedUnitRefId == input.ExistUnitRefId)
            {
                throw new UserFriendlyException(L("Same") + " " + L("Unit"));
            }

            var revisedUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == input.RevisedUnitRefId);
            if (revisedUnit == null)
            {
                throw new UserFriendlyException(L("Revised") + " " + L("Unit") + " " + L("NotExists"));
            }

            var oldExistUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == input.ExistUnitRefId);
            if (oldExistUnit == null)
            {
                throw new UserFriendlyException(L("Exist") + " " + L("Unit") + " " + L("NotExists"));
            }

            var unitConversionExist =
                rsUc.FirstOrDefault(t => t.BaseUnitId == input.ExistUnitRefId && t.RefUnitId == input.RevisedUnitRefId);

            if (unitConversionExist == null)
            {
                throw new UserFriendlyException(L("UnitConversion") + " " + oldExistUnit.Name + " " + revisedUnit.Name +
                                                " " + L("NotExists"));
            }

            #endregion CheckInputDataConstraints

            #region CheckData_OlderThan_Specified_MonthCount

            //	Check whether Ledgers data With in Last Month Only //@@@@Pending  - App Setting for this value

            var noofMonths = 12;
            var lastMonthFirstDate = new DateTime(DateTime.Today.AddMonths(-1 * noofMonths).Year,
                DateTime.Today.AddMonths(-1 * noofMonths).Month, 1);
            var olderThanLastMonthExist =
                await
                    _materialLedgerRepo.GetAllListAsync(
                        t => t.MaterialRefId == input.MaterialRefId && t.LedgerDate < lastMonthFirstDate);

            if (olderThanLastMonthExist.Count > 0)
            {
                throw new UserFriendlyException(L("CanNotChangeUnitIftheRecordsExistMoreThanNMonth", noofMonths));
            }

            #endregion CheckData_OlderThan_Specified_MonthCount

            #region GetConversionFactor

            decimal conversionFactor;
            var unitConversion =
                await
                    _unitconversionRepo.FirstOrDefaultAsync(
                        t => t.BaseUnitId == input.ExistUnitRefId && t.RefUnitId == input.RevisedUnitRefId);
            if (unitConversion != null)
            {
                conversionFactor = unitConversion.Conversion;
            }
            else
            {
                unitConversion =
                    await
                        _unitconversionRepo.FirstOrDefaultAsync(
                            t => t.BaseUnitId == input.RevisedUnitRefId && t.RefUnitId == input.ExistUnitRefId);
                if (unitConversion == null)
                {
                    var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == input.ExistUnitRefId);
                    var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == input.RevisedUnitRefId);

                    throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
                }
                conversionFactor = 1 / unitConversion.Conversion;
            }

            if (revisedUnit.Name.ToUpper().Equals("PC") || revisedUnit.Name.ToUpper().Equals("PCS") ||
                revisedUnit.Name.ToUpper().Equals("PIECE") || revisedUnit.Name.ToUpper().Equals("PIECES") ||
                revisedUnit.Name.ToUpper().Equals("NUMBER") || revisedUnit.Name.ToUpper().Equals("NUMBERS"))
            {
                conversionFactor = Math.Round(conversionFactor, 0);
            }

            #endregion GetConversionFactor

            #region updateLedgersValues

            var lstLedgers = await _materialLedgerRepo.GetAllListAsync(t => t.MaterialRefId == input.MaterialRefId);

            foreach (var group in lstLedgers.GroupBy(t => t.LocationRefId))
            {
                decimal closingBalance = 0;

                #region ChangeByConversion

                foreach (var lst in group.OrderBy(t => t.LedgerDate).ToList())
                {
                    if (revisedUnit.Name.ToUpper().Equals("PC") || revisedUnit.Name.ToUpper().Equals("PCS") ||
                        revisedUnit.Name.ToUpper().Equals("PIECE") || revisedUnit.Name.ToUpper().Equals("PIECES") ||
                        revisedUnit.Name.ToUpper().Equals("NUMBER") || revisedUnit.Name.ToUpper().Equals("NUMBERS"))
                    {
                        lst.OpenBalance = Math.Round(lst.OpenBalance * conversionFactor);
                        lst.Received = Math.Round(lst.Received * conversionFactor);
                        lst.Issued = Math.Round(lst.Issued * conversionFactor);
                        lst.Sales = Math.Round(lst.Sales * conversionFactor);
                        lst.TransferIn = Math.Round(lst.TransferIn * conversionFactor);
                        lst.TransferOut = Math.Round(lst.TransferOut * conversionFactor);
                        lst.Damaged = Math.Round(lst.Damaged * conversionFactor);
                        lst.SupplierReturn = Math.Round(lst.SupplierReturn * conversionFactor);
                        lst.ExcessReceived = Math.Round(lst.ExcessReceived * conversionFactor);
                        lst.Shortage = Math.Round(lst.Shortage * conversionFactor);
                        lst.Return = Math.Round(lst.Return * conversionFactor);
                        lst.ClBalance = Math.Round(lst.ClBalance * conversionFactor);
                    }
                    else
                    {
                        lst.OpenBalance = Math.Round(lst.OpenBalance * conversionFactor, 14);
                        lst.Received = Math.Round(lst.Received * conversionFactor, 14);
                        lst.Issued = Math.Round(lst.Issued * conversionFactor, 14);
                        lst.Sales = Math.Round(lst.Sales * conversionFactor, 14);
                        lst.TransferIn = Math.Round(lst.TransferIn * conversionFactor, 14);
                        lst.TransferOut = Math.Round(lst.TransferOut * conversionFactor, 14);
                        lst.Damaged = Math.Round(lst.Damaged * conversionFactor, 14);
                        lst.SupplierReturn = Math.Round(lst.SupplierReturn * conversionFactor, 14);
                        lst.ExcessReceived = Math.Round(lst.ExcessReceived * conversionFactor, 14);
                        lst.Shortage = Math.Round(lst.Shortage * conversionFactor, 14);
                        lst.Return = Math.Round(lst.Return * conversionFactor, 14);
                        lst.ClBalance = Math.Round(lst.ClBalance * conversionFactor, 14);
                    }
                    closingBalance = lst.ClBalance;
                    await _materialLedgerRepo.UpdateAsync(lst);
                }

                #endregion ChangeByConversion

                #region CurrentOnHandLocationWise

                var locationStock =
                    await
                        _materiallocationwisestockRepo.FirstOrDefaultAsync(
                            t => t.MaterialRefId == input.MaterialRefId && t.LocationRefId == group.Key);

                if (locationStock == null)
                {
                    var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == group.Key);
                    throw new UserFriendlyException(L("Error") + loc == null
                        ? ""
                        : loc.Name + " " + L("Location") + L("Stock"));
                }

                var revisedLocCurrentOnHand = locationStock.CurrentInHand * conversionFactor;

                var diff = Math.Abs(revisedLocCurrentOnHand - closingBalance);
                if (diff >= 0.50M)
                {
                }
                else
                {
                    var clDto = await _materiallocationwisestockRepo.GetAsync(locationStock.Id);
                    clDto.CurrentInHand = closingBalance;
                    await _materiallocationwisestockRepo.UpdateAsync(clDto);
                }

                #endregion CurrentOnHandLocationWise
            }

            #endregion updateLedgersValues

            #region UpdateMaterialMaster

            var matDto = await _materialRepo.GetAsync(material.Id);
            matDto.DefaultUnitId = input.RevisedUnitRefId;
            await _materialRepo.UpdateAsync(matDto);

            //var supplierMaterialDtos = await _suppliermaterialRepo.GetAll().Where(t => t.MaterialRefId == material.Id).ToListAsync();
            //foreach(var supmat in supplierMaterialDtos)
            //{
            //	var matPriceDto = await _suppliermaterialRepo.GetAsync(supmat.Id);
            //	matPriceDto.MaterialPrice = matPriceDto.MaterialPrice * 1/conversionFactor;
            //	await _suppliermaterialRepo.UpdateAsync(matPriceDto);
            //}

            #endregion UpdateMaterialMaster

            output.MenuMappingExist = false;

            #region CheckMappingAnyFoundWithOldUnitRefId

            var rsMappingRecords =
                await
                    _materialmenumappingRepo.GetAll()
                        .Where(t => t.MaterialRefId == input.MaterialRefId && t.PortionUnitId == input.ExistUnitRefId)
                        .ToListAsync();
            if (rsMappingRecords.Count > 0)
            {
                var mappedmatDto = new MaterialListDto { Id = input.MaterialRefId };
                var matList = new List<MaterialListDto>();
                matList.Add(mappedmatDto);

                output.ExcelFile = await GetMappedForParticularMaterialToExcel(new GetMaterialMappedList { MaterialList = matList });
                output.OutputMessage = L("PleaseVerifyTheExcelFile");
                output.MenuMappingExist = true;
            }

            #endregion CheckMappingAnyFoundWithOldUnitRefId

            //output.MaterialLedgerList = lstLedgers;
            var chkDefaultUom = await _unitConversionAppService.CheckMaterialDefaultUOM(new IdInput { Id = material.Id });
            if (chkDefaultUom.ErrorFlag)
            {
                throw new UserFriendlyException(chkDefaultUom.ErrorMessage);
            }


            return output;
        }

        public async Task<FileDto> GetMappedForParticularMaterialToExcel(GetMaterialMappedList input)
        {
            var allListDtos = await GetMappedMenuForParticularMaterialList(input);

            var baseE = new BaseExportObject
            {
                ExportObject = allListDtos,
                ColumnNames =
                    new[]
                    {
                        "PosMenuPortionRefId", "PosRefName", "MenuQuantitySold", "MaterialRefName", "MaterialTypeName",
                        "PortionQty", "UnitRefName", "AutoSalesDeduction"
                    },
                ColumnDisplayNames =
                    new[]
                    {
                        "MenuId", "MenuName", "MenuQuantity", "MaterialName", "MaterialType", "Portion Quantity", "UOM",
                        L("AutomaticIssue")
                    },
                BreakOnLineColumn = "PosRefName"
            };

            //return _excelExporter.ExportToFile(baseE, L("MenuMappedListForParticularMaterial"));
            return _materialmenumappingExporter.ExportParticularMaterialMenuMappingToFile(allListDtos);
        }

        public async Task<List<MaterialMenuMappingListDto>> GetMappedMenuForParticularMaterialList(
            GetMaterialMappedList input)
        {
            if (input.MaterialList == null || input.MaterialList.Count == 0)
                return null;

            var refMaterialRefIds = input.MaterialList.Select(t => t.Id).ToArray();

            var allMappedForParticularMaterialItems = (from menumap in _materialmenumappingRepo.GetAll()
                                                       join menuportion in _menuitemportionRepo.GetAll()
                                                           on menumap.PosMenuPortionRefId equals menuportion.Id
                                                       join menuitem in _menuitemRepo.GetAll()
                                                           on menuportion.MenuItemId equals menuitem.Id
                                                       join mat in _materialRepo.GetAll().Where(t => refMaterialRefIds.Contains(t.Id))
                                                           on menumap.MaterialRefId equals mat.Id
                                                       join un in _unitRepo.GetAll()
                                                           on menumap.PortionUnitId equals un.Id
                                                       select new MaterialMenuMappingListDto
                                                       {
                                                           Id = menumap.Id,
                                                           PosMenuPortionRefId = menuportion.Id,
                                                           MenuItemName = menuitem.Name,
                                                           PortionName = menuportion.Name,
                                                           PosRefName = string.Concat(menuitem.Name, "-", menuportion.Name),
                                                           MaterialRefId = mat.Id,
                                                           MaterialRefCode = mat.MaterialPetName,
                                                           MaterialRefName = mat.MaterialName,
                                                           MaterialTypeId = mat.MaterialTypeId,
                                                           MaterialTypeName = "",
                                                           MenuQuantitySold = menumap.MenuQuantitySold,
                                                           PortionQty = menumap.PortionQty,
                                                           PortionUnitId = menumap.PortionUnitId,
                                                           UnitRefName = un.Name,
                                                           AutoSalesDeduction = menumap.AutoSalesDeduction,
                                                           UserSerialNumber = menumap.UserSerialNumber
                                                       });

            var sortMenuItems = await allMappedForParticularMaterialItems
                .OrderBy(t => t.PosRefName)
                .ToListAsync();

            List<int> lstMenumappingRefIds = sortMenuItems.Select(t => t.Id).ToList();
            var menumappingDepartment = await _menumappingDepartmentRepo.GetAllListAsync(t => lstMenumappingRefIds.Contains(t.MenuMappingRefId));
            var deptList = await _departmentRepo.GetAll().ToListAsync();

            foreach (var lst in sortMenuItems)
            {
                var materialTypeName = lst.MaterialTypeId == (int)MaterialType.RAW ? L("RAW") : L("SEMI");
                lst.MaterialTypeName = materialTypeName;

                var deptDtos = (from md in menumappingDepartment.Where(t => t.MenuMappingRefId == lst.Id)
                                join de in deptList on md.DepartmentRefId equals de.Id
                                select new ComboboxItemDto
                                {
                                    Value = md.DepartmentRefId.ToString(),
                                    DisplayText = de.Name
                                }).ToList();
                if (deptDtos == null || deptDtos.Count == 0)
                {
                    deptDtos.Add(new ComboboxItemDto { Value = "0", DisplayText = L("All") });
                    lst.DeptNames = L("All");
                }
                else
                {
                    foreach (var dept in deptDtos)
                    {
                        lst.DeptNames = lst.DeptNames + dept.DisplayText + ",";
                    }
                    if (lst.DeptNames.Length > 0)
                        lst.DeptNames = lst.DeptNames.Left(lst.DeptNames.Length - 1);
                }
                lst.DeptList = deptDtos;
            }

            var allListDtos = sortMenuItems.MapTo<List<MaterialMenuMappingListDto>>();

            return allListDtos;
        }

        public async Task<bool> ExistAllServerLevelBusinessRules(CreateOrUpdateMaterialInput input)
        {
            if (input.Material.Barcode != null && input.Material.Barcode != "")
            {
                var barcodeAlreadyExists =
                   await _materialRepo.FirstOrDefaultAsync(t => t.Barcode == input.Material.Barcode && t.Id != input.Material.Id);
                if (barcodeAlreadyExists != null)
                {
                    throw new UserFriendlyException(L("BarcodeAlreadyExists", barcodeAlreadyExists.MaterialName));
                }
                else
                {
                    var bcExists = await _materialbarcodeRepo.FirstOrDefaultAsync(t => t.Barcode == input.Material.Barcode);
                    if (bcExists != null)
                    {
                        barcodeAlreadyExists = await _materialRepo.FirstOrDefaultAsync(t => t.Id == bcExists.MaterialRefId);
                        throw new UserFriendlyException(L("BarcodeAlreadyExists", barcodeAlreadyExists.MaterialName));
                    }
                }
            }

            var isMaterialCodeTreatedAsBarCode = await _settingManager.GetSettingValueAsync<bool>(AppSettings.HouseSettings.IsMaterialCodeTreatedAsBarCode);
            if (isMaterialCodeTreatedAsBarCode)
            {
                {
                    var barcodeAlreadyExists =
                      await _materialRepo.FirstOrDefaultAsync(t => t.Barcode == input.Material.MaterialPetName && t.Id != input.Material.Id);

                    if (barcodeAlreadyExists != null)
                        throw new UserFriendlyException(L("BarcodeAlreadyExists", barcodeAlreadyExists.MaterialName));
                    else
                    {
                        //var existMatBarcode = await _materialbarcodeRepo.FirstOrDefaultAsync(t => t.Barcode == input.Material.MaterialPetName && t.Id != input.Material.Id);
                        var bcExists = await _materialbarcodeRepo.FirstOrDefaultAsync(t => t.Barcode == input.Material.MaterialPetName);
                        if (bcExists != null)
                        {
                            barcodeAlreadyExists = await _materialRepo.FirstOrDefaultAsync(t => t.Id == bcExists.MaterialRefId);
                            throw new UserFriendlyException(L("BarcodeAlreadyExists", barcodeAlreadyExists.MaterialName));
                        }
                        barcodeAlreadyExists =
                              await _materialRepo.FirstOrDefaultAsync(t => t.MaterialPetName == input.Material.Barcode && t.Id != input.Material.Id);
                        if (barcodeAlreadyExists != null)
                            throw new UserFriendlyException(L("BarcodeAlreadyExists", barcodeAlreadyExists.MaterialName));
                    }
                }
            }

            var rsUnitConversion = await _unitconversionRepo.GetAllListAsync();

            if (input.Material.DefaultUnitId != input.Material.IssueUnitId)
            {
                var convUnit =
                    rsUnitConversion.Where(
                        t => (t.BaseUnitId == input.Material.DefaultUnitId && t.RefUnitId == input.Material.IssueUnitId))
                        .ToList();

                if (convUnit.Count == 0)
                {
                    var du = await _unitRepo.FirstOrDefaultAsync(t => t.Id == input.Material.DefaultUnitId);
                    var iu = await _unitRepo.FirstOrDefaultAsync(t => t.Id == input.Material.IssueUnitId);

                    throw new UserFriendlyException(L("ConversionFactorNotExist", du.Name, iu.Name));
                }

                convUnit =
                    rsUnitConversion.Where(
                        t => (t.BaseUnitId == input.Material.IssueUnitId && t.RefUnitId == input.Material.DefaultUnitId))
                        .ToList();

                if (convUnit.Count == 0)
                {
                    var du = _unitRepo.FirstOrDefault(t => t.Id == input.Material.DefaultUnitId);
                    var iu = _unitRepo.FirstOrDefault(t => t.Id == input.Material.IssueUnitId);

                    throw new UserFriendlyException(L("ConversionFactorNotExist", iu.Name, du.Name));
                }
            }

            if (input.Material.DefaultUnitId != input.Material.TransferUnitId)
            {
                var convUnit =
                    rsUnitConversion.Where(
                        t => (t.BaseUnitId == input.Material.DefaultUnitId && t.RefUnitId == input.Material.TransferUnitId))
                        .ToList();

                if (convUnit.Count == 0)
                {
                    var du = await _unitRepo.FirstOrDefaultAsync(t => t.Id == input.Material.DefaultUnitId);
                    var iu = await _unitRepo.FirstOrDefaultAsync(t => t.Id == input.Material.TransferUnitId);

                    throw new UserFriendlyException(L("ConversionFactorNotExist", du.Name, iu.Name));
                }

                convUnit =
                    rsUnitConversion.Where(
                        t => (t.BaseUnitId == input.Material.TransferUnitId && t.RefUnitId == input.Material.DefaultUnitId))
                        .ToList();

                if (convUnit.Count == 0)
                {
                    var du = _unitRepo.FirstOrDefault(t => t.Id == input.Material.DefaultUnitId);
                    var iu = _unitRepo.FirstOrDefault(t => t.Id == input.Material.TransferUnitId);

                    throw new UserFriendlyException(L("ConversionFactorNotExist", iu.Name, du.Name));
                }
            }

            if (input.Material.DefaultUnitId != input.Material.StockAdjustmentUnitId)
            {
                var convUnit =
                    rsUnitConversion.Where(
                        t => (t.BaseUnitId == input.Material.DefaultUnitId && t.RefUnitId == input.Material.StockAdjustmentUnitId))
                        .ToList();

                if (convUnit.Count == 0)
                {
                    var du = await _unitRepo.FirstOrDefaultAsync(t => t.Id == input.Material.DefaultUnitId);
                    var iu = await _unitRepo.FirstOrDefaultAsync(t => t.Id == input.Material.StockAdjustmentUnitId);

                    throw new UserFriendlyException(L("ConversionFactorNotExist", du.Name, iu.Name));
                }

                convUnit =
                    rsUnitConversion.Where(
                        t => (t.BaseUnitId == input.Material.StockAdjustmentUnitId && t.RefUnitId == input.Material.DefaultUnitId))
                        .ToList();

                if (convUnit.Count == 0)
                {
                    var du = _unitRepo.FirstOrDefault(t => t.Id == input.Material.DefaultUnitId);
                    var iu = _unitRepo.FirstOrDefault(t => t.Id == input.Material.StockAdjustmentUnitId);

                    throw new UserFriendlyException(L("ConversionFactorNotExist", iu.Name, du.Name));
                }
            }
            if (input.Material.ReceivingUnitId == 0)
            {
                input.Material.ReceivingUnitId = input.Material.DefaultUnitId;
            }

            var dto = input.Material;

            if (input.UnitList == null)
            {
                input.UnitList = new List<ComboboxItemDto>();
                input.UnitList.Add(new ComboboxItemDto { Value = dto.DefaultUnitId.ToString(), DisplayText = "" });
            }

            var defaultUnitExists = false;

            if (input.UnitList != null)
                defaultUnitExists = input.UnitList.Where(t => t.Value.Equals(dto.DefaultUnitId.ToString())).Count() > 0
                    ? true
                    : false;

            if (defaultUnitExists == false)
                input.UnitList.Add(new ComboboxItemDto { Value = dto.DefaultUnitId.ToString(), DisplayText = "" });

            var issueUnitExists = false;
            if (input.UnitList != null)
                issueUnitExists = input.UnitList.Where(t => t.Value.Equals(dto.IssueUnitId.ToString())).Count() > 0
                    ? true
                    : false;

            if (issueUnitExists == false)
                input.UnitList.Add(new ComboboxItemDto { Value = dto.IssueUnitId.ToString(), DisplayText = "" });

            var transferUnitExists = input.UnitList.Where(t => t.Value.Equals(dto.TransferUnitId.ToString())).Count() > 0
            ? true
            : false;
            if (transferUnitExists == false)
                input.UnitList.Add(new ComboboxItemDto { Value = dto.TransferUnitId.ToString(), DisplayText = "" });

            var stkAdjUnitExists = input.UnitList.Where(t => t.Value.Equals(dto.StockAdjustmentUnitId.ToString())).Count() > 0
                ? true
                : false;
            if (stkAdjUnitExists == false)
                input.UnitList.Add(new ComboboxItemDto { Value = dto.StockAdjustmentUnitId.ToString(), DisplayText = "" });

            var receivingUnitExists = input.UnitList.Where(t => t.Value.Equals(dto.ReceivingUnitId.ToString())).Count() > 0
              ? true
              : false;
            if (receivingUnitExists == false)
                input.UnitList.Add(new ComboboxItemDto { Value = dto.ReceivingUnitId.ToString(), DisplayText = "" });

            if (input.Material.AveragePriceTagRefId == 2 && input.Material.NoOfMonthAvgTaken == 0)
            {
                throw new UserFriendlyException(L("Number") + ' ' + L("Of") + ' ' + L("Months") + " ?");
            }

            return true;
        }

        protected virtual async Task<EntityDto> UpdateMaterial(CreateOrUpdateMaterialInput input)
        {
            var item = await _materialRepo.GetAsync(input.Material.Id.Value);

            var dto = input.Material;

            bool activeFlagChangesRequired = false;
            if (dto.IsActive != item.IsActive)
            {
                activeFlagChangesRequired = true;
            }
            //if (dto.AveragePriceTagRefId != item.AveragePriceTagRefId || dto.NoOfMonthAvgTaken != item.NoOfMonthAvgTaken)
            {
                await UpdateMaterialAverageDirtyFlag(new LocationAndMaterialDto { MaterialRefId = input.Material.Id.Value, LocationRefId = 0 });
            }

            item.MaterialName = dto.MaterialName;
            item.MaterialGroupCategoryRefId = dto.MaterialGroupCategoryRefId;
            item.MaterialPetName = dto.MaterialPetName;
            item.MRPPriceExists = dto.MRPPriceExists;
            item.OwnPreparation = dto.OwnPreparation;
            item.PosReferencecodeifany = dto.PosReferencecodeifany;
            item.UserSerialNumber = dto.UserSerialNumber;
            item.IsQuoteNeededForPurchase = dto.IsQuoteNeededForPurchase;
            item.IsNeedtoKeptinFreezer = dto.IsNeedtoKeptinFreezer;
            item.IsMfgDateExists = dto.IsMfgDateExists;
            item.IsFractional = dto.IsFractional;
            item.IsBranded = dto.IsBranded;
            item.Hsncode = dto.Hsncode;
            item.DefaultUnitId = dto.DefaultUnitId;
            item.GeneralLifeDays = dto.GeneralLifeDays;
            item.MaterialTypeId = dto.MaterialTypeId;
            item.WipeOutStockOnClosingDay = dto.WipeOutStockOnClosingDay;
            item.IsHighValueItem = dto.IsHighValueItem;
            item.IssueUnitId = dto.IssueUnitId;
            item.Barcode = dto.Barcode;
            item.IsActive = dto.IsActive;
            item.SyncLastModification = dto.SyncLastModification;
            item.SyncId = dto.SyncId;
            item.YieldPercentage = dto.YieldPercentage;
            item.AveragePriceTagRefId = dto.AveragePriceTagRefId;
            item.NoOfMonthAvgTaken = dto.NoOfMonthAvgTaken;
            item.ReceivingUnitId = dto.ReceivingUnitId;
            item.TransferUnitId = dto.TransferUnitId;
            item.StockAdjustmentUnitId = dto.StockAdjustmentUnitId;
            item.ConvertAsZeroStockWhenClosingStockNegative = dto.ConvertAsZeroStockWhenClosingStockNegative;

            #region StockCycle

            var stockcycleToBeRetained = new List<int>();
            if (input.StockCycle != null && input.StockCycle.Count > 0)
            {
                foreach (var b in input.StockCycle)
                {
                    var icrefid = int.Parse(b.Value);
                    stockcycleToBeRetained.Add(icrefid);

                    var existInvTag = await _materialStockCycleLinkRepo.FirstOrDefaultAsync(t => t.MaterialRefId == input.Material.Id.Value && t.InventoryCycleTagRefId == icrefid /*&& t.LocationRefId == null && t.LocationGroupRefId == null*/);
                    if (existInvTag == null)
                    {
                        var t = new MaterialStockCycleLink
                        {
                            //LocationGroupRefId = null,
                            //LocationRefId = null,
                            MaterialRefId = (int)dto.Id,
                            InventoryCycleTagRefId = int.Parse(b.Value)
                        };

                        await _materialStockCycleLinkRepo.InsertAsync(t);
                    }
                }
            }

            var delStockCyclelst =
                await
                    _materialStockCycleLinkRepo.GetAll()
                        .Where(
                            a => /*a.LocationGroupRefId == null && a.LocationRefId == null &&*/ a.MaterialRefId == input.Material.Id.Value &&
                                !stockcycleToBeRetained.Contains(a.InventoryCycleTagRefId))
                        .ToListAsync();

            foreach (var a in delStockCyclelst)
            {
                await _materialStockCycleLinkRepo.DeleteAsync(a.Id);
            }

            #endregion StockCycle

            if (input.Material.MaterialTypeId != 0)
            {
                var lst =
                    await _materialrecipetypeRepo.FirstOrDefaultAsync(a => a.MaterailRefId == input.Material.Id.Value);

                if (lst != null)
                {
                    var itemRecipeType = await _materialrecipetypeRepo.GetAsync(lst.Id);
                    var dtoitemRecipeType = input.MaterialRecipeType;

                    itemRecipeType.AlarmFlag = dtoitemRecipeType.AlarmFlag;
                    itemRecipeType.PrdBatchQty = dtoitemRecipeType.PrdBatchQty;
                    itemRecipeType.LifeTimeInMinutes = dtoitemRecipeType.LifeTimeInMinutes;
                    itemRecipeType.AlarmTimeInMinutes = dtoitemRecipeType.AlarmTimeInMinutes;
                    itemRecipeType.FixedCost = dtoitemRecipeType.FixedCost;
                }
                else if (input.MaterialRecipeType != null)
                {
                    var dtoitemRecipeType = input.MaterialRecipeType;
                    var t = new MaterialRecipeTypes
                    {
                        MaterailRefId = input.Material.Id.Value,
                        PrdBatchQty = dtoitemRecipeType.PrdBatchQty,
                        AlarmFlag = dtoitemRecipeType.AlarmFlag,
                        AlarmTimeInMinutes = dtoitemRecipeType.AlarmTimeInMinutes,
                        FixedCost = dtoitemRecipeType.FixedCost,
                        LifeTimeInMinutes = dtoitemRecipeType.LifeTimeInMinutes
                    };
                    await _materialrecipetypeRepo.InsertAsync(t);
                }
            }

            //  Update Material Unit Link

            //if (input.UnitList == null)
            //{
            //    input.UnitList = new List<ComboboxItemDto>();
            //    input.UnitList.Add(new ComboboxItemDto { Value = dto.DefaultUnitId.ToString(), DisplayText = "" });
            //}

            //var defaultUnitExists = input.UnitList.Where(t => t.Value.Equals(dto.DefaultUnitId.ToString())).Count() > 0
            //    ? true
            //    : false;
            //if (defaultUnitExists == false)
            //    input.UnitList.Add(new ComboboxItemDto { Value = dto.DefaultUnitId.ToString(), DisplayText = "" });

            //var issueUnitExists = input.UnitList.Where(t => t.Value.Equals(dto.IssueUnitId.ToString())).Count() > 0
            //    ? true
            //    : false;
            //if (issueUnitExists == false)
            //    input.UnitList.Add(new ComboboxItemDto { Value = dto.IssueUnitId.ToString(), DisplayText = "" });

            //var transferUnitExists = input.UnitList.Where(t => t.Value.Equals(dto.TransferUnitId.ToString())).Count() > 0
            //    ? true
            //    : false;
            //if (transferUnitExists==false)
            //    input.UnitList.Add(new ComboboxItemDto { Value = dto.TransferUnitId.ToString(), DisplayText = "" });

            //var stkAdjUnitExists = input.UnitList.Where(t => t.Value.Equals(dto.StockAdjustmentUnitId.ToString())).Count() > 0
            //    ? true
            //    : false;
            //if (stkAdjUnitExists == false)
            //    input.UnitList.Add(new ComboboxItemDto { Value = dto.StockAdjustmentUnitId.ToString(), DisplayText = "" });

            //var unitConversionToBeRetained = new List<IdInput>();

            //if (input.UnitConversionList != null && input.UnitConversionList.Count > 0)
            //{
            //    foreach (var uc in input.UnitConversionList)
            //    {
            //        var retUcIdList = await _unitConversionAppService.CreateOrUpdateUnitConversion(new CreateOrUpdateUnitConversionInput
            //        {
            //            UnitConversion = new UnitConversionEditDto
            //            {
            //                Id= uc.Id,
            //                BaseUnitId = uc.BaseUnitId,
            //                RefUnitId = uc.RefUnitId,
            //                Conversion = uc.Conversion,
            //                DecimalPlaceRounding = uc.DecimalPlaceRounding,
            //                MaterialRefId = item.Id,
            //                LinkedSupplierList = uc.LinkedSupplierList,
            //            },
            //            RecursiveFlag = false
            //        });
            //        unitConversionToBeRetained.AddRange(retUcIdList);
            //        if (input.UnitList.Count > 0)
            //        {
            //            var unitrefid = uc.BaseUnitId;
            //            var alreadyExist = input.UnitList.Exists(t => t.Value.ToString() == unitrefid.ToString());
            //            if (alreadyExist == false)
            //                input.UnitList.Add(new ComboboxItemDto { Value = unitrefid.ToString() });

            //            unitrefid = uc.RefUnitId;
            //            alreadyExist = input.UnitList.Exists(t => t.Value.ToString() == unitrefid.ToString());
            //            if (alreadyExist == false)
            //                input.UnitList.Add(new ComboboxItemDto { Value = unitrefid.ToString() });
            //        }
            //    }
            //}
            //List<int> idsToBeRetiainedForUnitConversion = unitConversionToBeRetained.Select(t => t.Id).ToList().Distinct().ToList();
            //var delUnitConversionlst =
            //  await
            //      _unitconversionRepo.GetAll()
            //          .Where(
            //              a => a.MaterialRefId == input.Material.Id.Value && !idsToBeRetiainedForUnitConversion.Contains(a.Id))
            //          .ToListAsync();

            //foreach (var a in delUnitConversionlst)
            //{
            //    await _unitconversionRepo.DeleteAsync(a.Id);
            //}


            var unitsToBeRetained = new List<int>();
            if (input.UnitList != null && input.UnitList.Count > 0)
            {
                foreach (var b in input.UnitList)
                {
                    var unitrefid = int.Parse(b.Value);
                    unitsToBeRetained.Add(unitrefid);

                    if (
                        _unitlinkRepo.GetAllList(
                            u => u.MaterialRefId == input.Material.Id.Value && u.UnitRefId.Equals(unitrefid)).Count == 0)
                    {
                        var t = new MaterialUnitsLink
                        {
                            MaterialRefId = (int)dto.Id,
                            UnitRefId = int.Parse(b.Value)
                        };

                        await _unitlinkRepo.InsertAsync(t);
                    }
                }
            }

            var delUnitlst =
                await
                    _unitlinkRepo.GetAll()
                        .Where(
                            a => a.MaterialRefId == input.Material.Id.Value && !unitsToBeRetained.Contains(a.UnitRefId))
                        .ToListAsync();

            foreach (var a in delUnitlst)
            {
                await _unitlinkRepo.DeleteAsync(a.Id);
            }

            //  Update Material Brand Link
            var brandsToBeRetained = new List<int>();

            if (input.BrandList != null && input.BrandList.Count > 0)
            {
                foreach (var b in input.BrandList)
                {
                    var brandrefid = int.Parse(b.Value);
                    brandsToBeRetained.Add(brandrefid);
                    if (
                        _brandlinkRepo.GetAllList(
                            u => u.MaterialRefId == input.Material.Id.Value && u.BrandRefId.Equals(brandrefid)).Count ==
                        0)
                    {
                        var t = new MaterialBrandsLink
                        {
                            MaterialRefId = (int)dto.Id,
                            BrandRefId = int.Parse(b.Value)
                        };
                        await _brandlinkRepo.InsertAsync(t);
                    }
                }
            }

            var delBrandList =
                await
                    _brandlinkRepo.GetAll()
                        .Where(
                            a =>
                                a.MaterialRefId == input.Material.Id.Value && !brandsToBeRetained.Contains(a.BrandRefId))
                        .ToListAsync();

            foreach (var a in delBrandList)
            {
                await _brandlinkRepo.DeleteAsync(a.Id);
            }

            //  Update Supplier Material Link
            var supplierToBeRetained = new List<int>();

            if (input.SupplierMaterial != null && input.SupplierMaterial.Count > 0)
            {
                foreach (var b in input.SupplierMaterial)
                {
                    var supplierRefId = b.SupplierRefId;
                    supplierToBeRetained.Add(supplierRefId);
                    if (
                        _suppliermaterialRepo.GetAllList(
                            u => u.MaterialRefId == input.Material.Id.Value && u.SupplierRefId.Equals(supplierRefId))
                            .Count == 0)
                    {
                        var t = new SupplierMaterial
                        {
                            MaterialRefId = (int)dto.Id,
                            SupplierRefId = b.SupplierRefId,
                            MaterialPrice = b.MaterialPrice,
                            MinimumOrderQuantity = b.MinimumOrderQuantity,
                            MaximumOrderQuantity = b.MaximumOrderQuantity,
                            UnitRefId = b.UnitRefId,
                            LastQuoteDate = DateTime.UtcNow,
                            LastQuoteRefNo = "Default",
                            YieldPercentage = b.YieldPercentage,
                            SupplierMaterialAliasName = b.SupplierMaterialAliasName
                        };

                        await _suppliermaterialRepo.InsertAsync(t);
                    }
                    else
                    {
                        var editSupMaterial =
                            _suppliermaterialRepo.FirstOrDefault(
                                sm => sm.MaterialRefId == input.Material.Id.Value && sm.SupplierRefId == supplierRefId);

                        var itemSupplierMaterial = await _suppliermaterialRepo.GetAsync(editSupMaterial.Id);
                        itemSupplierMaterial.MaterialPrice = b.MaterialPrice;
                        itemSupplierMaterial.MinimumOrderQuantity = b.MinimumOrderQuantity;
                        itemSupplierMaterial.MaximumOrderQuantity = b.MaximumOrderQuantity;
                        itemSupplierMaterial.UnitRefId = b.UnitRefId;
                        itemSupplierMaterial.LastQuoteDate = DateTime.Now;
                        itemSupplierMaterial.YieldPercentage = b.YieldPercentage;
                        itemSupplierMaterial.SupplierMaterialAliasName = b.SupplierMaterialAliasName;
                    }
                }
            }

            var delSupplierMaterialList =
                await
                    _suppliermaterialRepo.GetAll()
                        .Where(
                            a =>
                                a.MaterialRefId == input.Material.Id.Value &&
                                !supplierToBeRetained.Contains(a.SupplierRefId))
                        .ToListAsync();

            foreach (var a in delSupplierMaterialList)
            {
                await _suppliermaterialRepo.DeleteAsync(a.Id);
            }

            //  Update Material Ingredient

            var ingredientToBeRetained = new List<int>();
            if (input.MaterialIngredient != null && input.MaterialIngredient.Count > 0)
            {
                foreach (var b in input.MaterialIngredient)
                {
                    var ingredientRefId = b.MaterialRefId;
                    ingredientToBeRetained.Add(ingredientRefId);
                    if (
                        _materialingredientRepo.GetAllList(
                            u => u.RecipeRefId == input.Material.Id.Value && u.MaterialRefId.Equals(ingredientRefId))
                            .Count == 0)
                    {
                        var t = new MaterialIngredient
                        {
                            RecipeRefId = (int)dto.Id,
                            MaterialRefId = b.MaterialRefId,
                            MaterialUsedQty = b.MaterialUsedQty,
                            UnitRefId = b.UnitRefId,
                            VariationflagForProduction = b.VariationflagForProduction,
                            UserSerialNumber = b.UserSerialNumber
                        };

                        await _materialingredientRepo.InsertAsync(t);
                    }
                    else
                    {
                        var editIngr =
                            _materialingredientRepo.FirstOrDefault(
                                t => t.RecipeRefId == input.Material.Id.Value && t.MaterialRefId == ingredientRefId);

                        var itemmaterialingr = await _materialingredientRepo.GetAsync(editIngr.Id);
                        itemmaterialingr.MaterialUsedQty = b.MaterialUsedQty;
                        itemmaterialingr.UnitRefId = b.UnitRefId;
                        itemmaterialingr.VariationflagForProduction = b.VariationflagForProduction;
                        itemmaterialingr.UserSerialNumber = b.UserSerialNumber;

                        await _materialingredientRepo.InsertOrUpdateAndGetIdAsync(itemmaterialingr);
                    }
                }
            }

            var delMaterialIngrList =
                _materialingredientRepo.GetAll()
                    .Where(
                        a =>
                            a.RecipeRefId == input.Material.Id.Value &&
                            !ingredientToBeRetained.Contains(a.MaterialRefId))
                    .ToList();

            foreach (var a in delMaterialIngrList)
            {
                _materialingredientRepo.Delete(a.Id);
            }

            //  Update Customer Material Link
            var customerToBeRetained = new List<int>();

            if (input.CustomerMaterial != null && input.CustomerMaterial.Count > 0)
            {
                foreach (var b in input.CustomerMaterial)
                {
                    var customerRefId = b.CustomerRefId;
                    customerToBeRetained.Add(customerRefId);
                    if (
                        _customermaterialRepo.GetAllList(
                            u => u.MaterialRefId == input.Material.Id.Value && u.CustomerRefId.Equals(customerRefId))
                            .Count == 0)
                    {
                        var t = new CustomerMaterial
                        {
                            MaterialRefId = (int)dto.Id,
                            CustomerRefId = b.CustomerRefId,
                            MaterialPrice = b.MaterialPrice,
                            LastQuoteDate = DateTime.UtcNow,
                            LastQuoteRefNo = "Default"
                        };

                        await _customermaterialRepo.InsertAsync(t);
                    }
                    else
                    {
                        var editCusMaterial =
                            _customermaterialRepo.FirstOrDefault(
                                sm => sm.MaterialRefId == input.Material.Id.Value && sm.CustomerRefId == customerRefId);

                        var itemSupplierMaterial = await _customermaterialRepo.GetAsync(editCusMaterial.Id);
                        itemSupplierMaterial.MaterialPrice = b.MaterialPrice;
                        itemSupplierMaterial.LastQuoteDate = DateTime.UtcNow;
                    }
                }
            }

            var delCustomerMaterialList =
                _customermaterialRepo.GetAll()
                    .Where(
                        a =>
                            a.MaterialRefId == input.Material.Id.Value &&
                            !customerToBeRetained.Contains(a.CustomerRefId))
                    .ToList();

            foreach (var a in delCustomerMaterialList)
            {
                _customermaterialRepo.Delete(a.Id);
            }

            //  Update Customer Tag Material Link
            var customerTagToBeRetained = new List<int>();

            if (input.CustomerTagMaterialPrice != null && input.CustomerTagMaterialPrice.Count > 0)
            {
                foreach (var b in input.CustomerTagMaterialPrice)
                {
                    if (
                        _customertagmaterialpriceRepo.GetAllList(
                            u => u.MaterialRefId == input.Material.Id.Value && u.TagCode.Equals(b.TagCode)).Count == 0)
                    {
                        var t = new CustomerTagMaterialPrice
                        {
                            MaterialRefId = (int)dto.Id,
                            MaterialPrice = b.MaterialPrice,
                            TagCode = b.TagCode
                        };

                        await _customertagmaterialpriceRepo.InsertAsync(t);
                    }
                    else
                    {
                        var editCusMaterial =
                            _customertagmaterialpriceRepo.FirstOrDefault(
                                sm => sm.MaterialRefId == input.Material.Id.Value && sm.TagCode.Equals(b.TagCode));

                        var itemcmp = await _customertagmaterialpriceRepo.GetAsync(editCusMaterial.Id);
                        itemcmp.MaterialPrice = b.MaterialPrice;

                        await _customertagmaterialpriceRepo.InsertOrUpdateAsync(itemcmp);
                    }
                }
            }

            if (item.UserSerialNumber != dto.UserSerialNumber)
            {
                await UpdateUserSerialNumber(new EntityDto { Id = item.Id });
            }

            #region barcodeList

            if (dto?.Barcodes != null && dto.Barcodes.Any())
            {
                var oids = new List<int>();
                foreach (var otdto in dto.Barcodes)
                {
                    if (otdto.Id != null && otdto.Id > 0)
                    {
                        oids.Add(otdto.Id.Value);
                        var fromUpdation = item.Barcodes.SingleOrDefault(a => a.Id.Equals(otdto.Id));
                        if (fromUpdation != null)
                            UpdateMaterialItemBarcode(fromUpdation, otdto);
                    }
                    else
                    {
                        item.Barcodes.Add(Mapper.Map<MaterialBarCodeEditDto, MaterialBarCode>(otdto));
                    }
                }
                var tobeRemoved = item.Barcodes.Where(a => !a.Id.Equals(0) && !oids.Contains(a.Id)).ToList();
                foreach (var menuItemPortion in tobeRemoved)
                {
                    await _materialbarcodeRepo.DeleteAsync(menuItemPortion.Id);
                }
            }

            #endregion barcodeList

            if (activeFlagChangesRequired)
            {
                var allLoc = await _materiallocationwisestockRepo.GetAllListAsync(t => t.MaterialRefId == input.Material.Id.Value);
                foreach (var lst in allLoc)
                {
                    lst.IsActiveInLocation = dto.IsActive;
                }
            }
            CheckErrors(await _materialManager.CreateSync(item));

            return new EntityDto
            {
                Id = input.Material.Id.Value
            };
        }

        public virtual async Task<List<MaterialLocationWiseStockListDto>> GetMaterialActiveStatusLocationWise(IdInput input)
        {
            var allLocationMaterialStock = await (from matLoc in _materiallocationwisestockRepo
                                                  .GetAll().Where(t => t.MaterialRefId == input.Id)
                                                  join loc in _locationRepo.GetAll() on matLoc.LocationRefId equals loc.Id
                                                  select new MaterialLocationWiseStockListDto
                                                  {
                                                      LocationRefId = loc.Id,
                                                      LocationRefCode = loc.Code,
                                                      LocationRefName = loc.Name,
                                                      MaterialRefId = matLoc.MaterialRefId,
                                                      IsActiveInLocation = matLoc.IsActiveInLocation
                                                  }).ToListAsync();
            return allLocationMaterialStock;
        }

        public virtual async Task<MaterialLocationWiseStockListDto> ChangeMaterialActiveStatusInLocation(MaterialLocationWiseStockListDto input)
        {
            var matLoc = await _materiallocationwisestockRepo.FirstOrDefaultAsync(t => t.MaterialRefId == input.MaterialRefId && t.LocationRefId == input.LocationRefId);
            if (matLoc != null)
            {
                matLoc.IsActiveInLocation = input.IsActiveInLocation;
                await _materiallocationwisestockRepo.UpdateAsync(matLoc);
                return matLoc.MapTo<MaterialLocationWiseStockListDto>();
            }
            else
                return null;
        }

        private void UpdateMaterialItemBarcode(MaterialBarCode fromUpdation, MaterialBarCodeEditDto otdto)
        {
            if (otdto?.Barcode != null)
                fromUpdation.Barcode = otdto.Barcode;
        }

        protected virtual async Task<EntityDto> CreateMaterial(CreateOrUpdateMaterialInput input)
        {
            var dto = input.Material.MapTo<Material>();
            dto.IsActive = true;

            CheckErrors(await _materialManager.CreateSync(dto));

            var materialId = dto.Id;// await _materialRepo.InsertOrUpdateAndGetIdAsync(dto);

            //  Recipe Database

            if (input.Material.MaterialTypeId != 0)
            {
                var t = new MaterialRecipeTypes();

                if (input.MaterialRecipeType == null)
                {
                    t = new MaterialRecipeTypes
                    {
                        MaterailRefId = dto.Id,
                        PrdBatchQty = 0,
                        AlarmFlag = false,
                        AlarmTimeInMinutes = 0,
                        FixedCost = 0,
                        LifeTimeInMinutes = 0
                    };
                }
                else
                {
                    var dtoitemRecipeType = input.MaterialRecipeType;
                    t = new MaterialRecipeTypes
                    {
                        MaterailRefId = materialId,
                        PrdBatchQty = dtoitemRecipeType.PrdBatchQty,
                        AlarmFlag = dtoitemRecipeType.AlarmFlag,
                        AlarmTimeInMinutes = dtoitemRecipeType.AlarmTimeInMinutes,
                        FixedCost = dtoitemRecipeType.FixedCost,
                        LifeTimeInMinutes = dtoitemRecipeType.LifeTimeInMinutes
                    };
                }
                await _materialrecipetypeRepo.InsertAsync(t);
            }

            //  Add Default unit Id into the List

            //if (input.UnitList != null && input.UnitList.Count > 0)
            //{
            //    input.UnitList.Add(new ComboboxItemDto { Value = dto.DefaultUnitId.ToString(), DisplayText = "" });
            //}

            //if (input.UnitConversionList != null && input.UnitConversionList.Count > 0)
            //{
            //    foreach (var uc in input.UnitConversionList)
            //    {
            //        await _unitConversionAppService.CreateOrUpdateUnitConversion(new CreateOrUpdateUnitConversionInput
            //        {
            //            UnitConversion = new UnitConversionEditDto
            //            {
            //                Id = uc.Id,
            //                BaseUnitId = uc.BaseUnitId,
            //                RefUnitId = uc.RefUnitId,
            //                Conversion = uc.Conversion,
            //                DecimalPlaceRounding = uc.DecimalPlaceRounding,
            //                MaterialRefId = dto.Id,
            //                LinkedSupplierList = uc.LinkedSupplierList
            //            },
            //            RecursiveFlag = false
            //        });

            //        if (input.UnitList.Count > 0)
            //        {
            //            var unitrefid = uc.BaseUnitId;
            //            var alreadyExist = input.UnitList.Exists(t => t.Value.ToString() == unitrefid.ToString());
            //            if (alreadyExist == false)
            //                input.UnitList.Add(new ComboboxItemDto { Value = unitrefid.ToString() });

            //            unitrefid = uc.RefUnitId;
            //            alreadyExist = input.UnitList.Exists(t => t.Value.ToString() == unitrefid.ToString());
            //            if (alreadyExist == false)
            //                input.UnitList.Add(new ComboboxItemDto { Value = unitrefid.ToString() });
            //        }
            //    }
            //}


            if (input.UnitList != null && input.UnitList.Count > 0)
            {
                foreach (var b in input.UnitList)
                {
                    var unitrefid = int.Parse(b.Value);

                    var unitLinkExist = await _unitlinkRepo.FirstOrDefaultAsync(t => t.MaterialRefId == input.Material.Id.Value && t.UnitRefId == unitrefid);
                    if (unitLinkExist == null)
                    {
                        var t = new MaterialUnitsLink
                        {
                            MaterialRefId = dto.Id,
                            UnitRefId = int.Parse(b.Value)
                        };

                        await _unitlinkRepo.InsertAsync(t);
                    }
                }
            }


            if (input.BrandList != null && input.BrandList.Count > 0)
            {
                foreach (var b in input.BrandList)
                {
                    var t = new MaterialBrandsLink
                    {
                        MaterialRefId = dto.Id,
                        BrandRefId = int.Parse(b.Value)
                    };

                    await _brandlinkRepo.InsertAsync(t);
                }
            }

            if (input.StockCycle != null && input.StockCycle.Count > 0)
            {
                foreach (var b in input.StockCycle)
                {
                    var t = new MaterialStockCycleLink
                    {
                        //LocationGroupRefId = null,
                        //LocationRefId = null,
                        MaterialRefId = dto.Id,
                        InventoryCycleTagRefId = int.Parse(b.Value)
                    };

                    await _materialStockCycleLinkRepo.InsertAsync(t);
                }
            }

            if (input.SupplierMaterial != null && input.SupplierMaterial.Count > 0)
            {
                foreach (var b in input.SupplierMaterial)
                {
                    var t = new SupplierMaterial
                    {
                        MaterialRefId = dto.Id,
                        SupplierRefId = b.SupplierRefId,
                        MaterialPrice = b.MaterialPrice,
                        MinimumOrderQuantity = b.MinimumOrderQuantity,
                        MaximumOrderQuantity = b.MaximumOrderQuantity,
                        UnitRefId = b.UnitRefId,
                        LastQuoteDate = DateTime.Today,
                        LastQuoteRefNo = "Default",
                        YieldPercentage = b.YieldPercentage,
                        SupplierMaterialAliasName = b.SupplierMaterialAliasName
                    };

                    await _suppliermaterialRepo.InsertAsync(t);
                }
            }

            if (input.MaterialIngredient != null && input.MaterialIngredient.Count > 0)
            {
                foreach (var b in input.MaterialIngredient)
                {
                    var t = new MaterialIngredient
                    {
                        RecipeRefId = dto.Id,
                        MaterialRefId = b.MaterialRefId,
                        MaterialUsedQty = b.MaterialUsedQty,
                        UnitRefId = b.UnitRefId,
                        VariationflagForProduction = b.VariationflagForProduction,
                        UserSerialNumber = b.UserSerialNumber
                    };

                    await _materialingredientRepo.InsertAsync(t);
                }
            }

            if (input.CustomerMaterial != null && input.CustomerMaterial.Count > 0)
            {
                foreach (var b in input.CustomerMaterial)
                {
                    var t = new CustomerMaterial
                    {
                        MaterialRefId = dto.Id,
                        CustomerRefId = b.CustomerRefId,
                        MaterialPrice = b.MaterialPrice,
                        LastQuoteDate = DateTime.Today,
                        LastQuoteRefNo = "Default"
                    };

                    await _customermaterialRepo.InsertAsync(t);
                }
            }

            if (input.CustomerTagMaterialPrice != null && input.CustomerTagMaterialPrice.Count > 0)
            {
                foreach (var b in input.CustomerTagMaterialPrice)
                {
                    var t = new CustomerTagMaterialPrice
                    {
                        MaterialRefId = dto.Id,
                        TagCode = b.TagCode,
                        MaterialPrice = b.MaterialPrice
                    };

                    await _customertagmaterialpriceRepo.InsertAsync(t);
                }
            }

            var customertagList = _customertagdefinitionRepo.GetAll().ToList();
            if (customertagList.Count() == 0)
            {
                var newctd = new CustomerTagDefinition();
                {
                    newctd.TagCode = L("DEFAULT");
                    newctd.TagName = L("DEFAULT");
                }
                var retId2 = await _customertagdefinitionRepo.InsertAndGetIdAsync(newctd);
                customertagList.Add(new CustomerTagDefinition { TagName = L("DEFAULT"), TagCode = L("DEFAULT") });
            }

            foreach (var tag in customertagList)
            {
                if (
                    _customertagmaterialpriceRepo.GetAllList(
                        m => m.MaterialRefId == materialId && m.TagCode.Equals(tag.TagCode)).Count == 0)
                {
                    var newDto = new CustomerTagMaterialPrice();
                    {
                        newDto.MaterialRefId = materialId;
                        newDto.TagCode = tag.TagCode;
                        newDto.MaterialPrice = 0;
                    }
                    ;
                    var retId3 = await _customertagmaterialpriceRepo.InsertAndGetIdAsync(newDto);
                }
            }

            if (input.UpdateSerialNumberRequired)
                await UpdateUserSerialNumber(new EntityDto { Id = dto.Id });

            //CheckErrors(await _materialManager.CreateSync(dto));
            await _bgm.EnqueueAsync<HouseLocationWiseMaterialInitialiseJob, HouseLocationWiseMaterialInitialiseJobArgs>(new HouseLocationWiseMaterialInitialiseJobArgs
            {
                LocationRefId = null,
                MaterialRefId = dto.Id,
                UserId = AbpSession.UserId,
                TenantId = AbpSession.TenantId.Value
            });

            //var locationDetails = await _locationRepo.GetAllListAsync();

            //foreach (var l in locationDetails)
            //{
            //    var newDto = new MaterialLocationWiseStock();
            //    {
            //        newDto.LocationRefId = l.Id;
            //        newDto.MaterialRefId = materialId;
            //        newDto.CurrentInHand = 0;
            //        newDto.MaximumStock = 0;
            //        newDto.MinimumStock = 0;
            //        newDto.ReOrderLevel = 0;
            //        newDto.IsOrderPlaced = false;
            //        newDto.IsActiveInLocation = true;
            //        newDto.ConvertAsZeroStockWhenClosingStockNegative = input.Material.ConvertAsZeroStockWhenClosingStockNegative;
            //    }
            //    ;
            //    var retId2 = await _materiallocationwisestockRepo.InsertAndGetIdAsync(newDto);
            //}


            return new EntityDto
            {
                Id = materialId
            };
        }

        public async Task UpdateUserSerialNumber(EntityDto input)
        {
            var material = await _materialRepo.FirstOrDefaultAsync(m => m.Id == input.Id);

            if (material == null)
                return;

            var lstMaterial =
                await
                    _materialRepo.GetAll()
                        .Where(m => m.Id != input.Id && m.UserSerialNumber >= material.UserSerialNumber)
                        .OrderBy(m => m.UserSerialNumber)
                        .ToListAsync();

            var userSnoToSet = (int)material.UserSerialNumber + 1;

            foreach (var lstmat in lstMaterial)
            {
                var editDto = await _materialRepo.GetAsync(lstmat.Id);
                editDto.UserSerialNumber = userSnoToSet;
                userSnoToSet++;
            }
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetMenuPortionForCombobox(EntityDto input)
        {
            List<ProductListDto> menus = new List<ProductListDto>();

            if (input.Id == 0)
            {
                menus = await (from menuportion in _menuitemportionRepo.GetAll()
                               join menu in _menuitemRepo.GetAll().Where(a => a.ProductType == 1)
                               on menuportion.MenuItemId equals menu.Id
                               select new ProductListDto
                               {
                                   PortionId = menuportion.Id,
                                   PortionName = string.Concat(menu.Name, " - ", menuportion.Name)
                               }).ToListAsync();
            }
            else
            {
                int[] menuMap = _materialmenumappingRepo.GetAll().Select(t => t.PosMenuPortionRefId).Distinct().ToArray();

                menus = await (from menuportion in _menuitemportionRepo.GetAll().Where(t => !menuMap.Contains(t.Id))
                               join menu in _menuitemRepo.GetAll().Where(a => a.ProductType == 1)
                               on menuportion.MenuItemId equals menu.Id
                               select new ProductListDto()
                               {
                                   PortionId = menuportion.Id,
                                   PortionName = string.Concat(menu.Name, "-", menuportion.Name),
                               }).ToListAsync();
            }

            return
                new ListResultDto<ComboboxItemDto>(
                    menus.Select(e => new ComboboxItemDto(e.PortionId.ToString(), e.PortionName)).ToList());
        }

        public async Task<List<DayCloseSyncReportOutputDto>> GetDayCloseSyncReport(DayCloseSyncReportDto input)
        {
            List<DayCloseSyncReportOutputDto> output = new List<DayCloseSyncReportOutputDto>();

            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
            if (loc == null)
            {
                throw new UserFriendlyException(L("Location") + "?");
            }

            var locinput = new List<LocationListDto>();
            locinput.Add(loc.MapTo<LocationListDto>());

            var lstDayClose = await _daycloseTemplateRepo.GetAll().Where(t => t.TemplateType == (int)DayCloseTemplateType.DashboardTicketDto &&
                     t.ClosedDate >= input.StartDate.Date && t.ClosedDate <= input.EndDate.Date && t.LocationRefId == input.LocationRefId).ToListAsync();
            foreach (var closeDate in lstDayClose)
            {
                DayCloseSyncReportOutputDto dto = new DayCloseSyncReportOutputDto();
                dto.AccountDate = closeDate.ClosedDate;
                dto.DayCloseTime = closeDate.CreationTime;

                DashboardTicketDto dayCloseSales = _xmlandjsonConvertorAppService.DeSerializeFromJSON<DashboardTicketDto>(closeDate.TemplateData);
                TicketStatsDto sales = await _connectReportAppService.GetTickets(new GetTicketInput
                {
                    StartDate = dto.AccountDate,
                    EndDate = dto.AccountDate,
                    Locations = locinput.MapTo<List<SimpleLocationDto>>()
                }, true);

                if (dayCloseSales.TotalTicketCount == sales.DashBoardDto.TotalTicketCount && Math.Round(dayCloseSales.TotalAmount, 2) == Math.Round(sales.DashBoardDto.TotalAmount, 2))
                {
                    dto.SyncErrorFlag = false;
                }
                else
                {
                    dto.SyncErrorFlag = true;
                }

                dto.DayCloseSalesDashBoard = dayCloseSales;
                dto.ActualSalesDashBoard = sales.DashBoardDto;
                output.Add(dto);
            }
            return output;
        }

        public async Task<EntityDto> CarryOverUpdateMaterialLedger(MaterialLedgerDto input)
        {
            var stockDate = DateTime.Parse(input.LedgerDate.ToString()).Date;
            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);

            if (loc.HouseTransactionDate.HasValue)
            {
                if (input.CarryOverFlag == false)
                {
                    if (loc.HouseTransactionDate.Value.Date != input.LedgerDate.Date && input.ForceAdjustmentFlag == false)
                    {
                        throw new UserFriendlyException(L("MaterialLedger_Account_Date_Error",
                            loc.HouseTransactionDate.Value.ToString("yyyy-MMM-dd"),
                            input.LedgerDate.Date.ToString("yyyy-MMM-dd")));
                    }
                }
            }
            else
            {
                throw new UserFriendlyException(L("AccountDateNotSetForLocation"), loc.Name);
            }

            var material = await _materialRepo.FirstOrDefaultAsync(t => t.Id == input.MaterialRefId);

            #region UnitConversion

            if (input.UnitId != null && input.UnitId != 0)
            {
                var defaultUnitId = material.DefaultUnitId;

                if (defaultUnitId == input.UnitId)
                {
                }
                else
                {
                    decimal conversionFactor;
                    var unitConversion =
                        await
                            _unitconversionRepo.FirstOrDefaultAsync(
                                t => t.BaseUnitId == input.UnitId && t.RefUnitId == defaultUnitId);
                    if (unitConversion != null)
                    {
                        conversionFactor = unitConversion.Conversion;
                    }
                    else
                    {
                        unitConversion =
                            await
                                _unitconversionRepo.FirstOrDefaultAsync(
                                    t => t.BaseUnitId == defaultUnitId && t.RefUnitId == input.UnitId);
                        if (unitConversion == null)
                        {
                            var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == input.UnitId);
                            var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == defaultUnitId);

                            throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
                        }
                        conversionFactor = 1 / unitConversion.Conversion;
                    }
                    if (conversionFactor == 0)
                    {
                        var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == input.UnitId);
                        var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == defaultUnitId);

                        throw new UserFriendlyException(L("ConversionFactorShouldNotBeZero", baseUnit.Name, refUnit.Name));
                    }

                    input.OpenBalance = input.OpenBalance * conversionFactor;
                    input.Received = input.Received * conversionFactor;
                    input.Issued = input.Issued * conversionFactor;
                    input.Sales = input.Sales * conversionFactor;
                    input.TransferIn = input.TransferIn * conversionFactor;
                    input.TransferOut = input.TransferOut * conversionFactor;
                    input.Damaged = input.Damaged * conversionFactor;
                    input.SupplierReturn = input.SupplierReturn * conversionFactor;
                    input.ExcessReceived = input.ExcessReceived * conversionFactor;
                    input.Shortage = input.Shortage * conversionFactor;
                    input.Return = input.Return * conversionFactor;
                    input.ClBalance = input.ClBalance * conversionFactor;
                }
            }

            #endregion UnitConversion

            #region Check Carry Over Flag and Future Date Already Exists In Ledger

            if (input.CarryOverFlag == false)
            {
                var futureDateExists =
                    await
                        _materialLedgerRepo.FirstOrDefaultAsync(
                            t =>
                                DbFunctions.TruncateTime(t.LedgerDate) > stockDate && t.MaterialRefId == input.MaterialRefId &&
                                t.LocationRefId == input.LocationRefId);
                if (futureDateExists != null)
                {
                    throw new UserFriendlyException(L("FutureDateExistInLedger", material.MaterialName,
                        futureDateExists.LedgerDate.Value.ToString("yyyy-MMM-dd"), input.LedgerDate.ToString("yyyy-MMM-dd")));
                }
            }

            #endregion Check Carry Over Flag and Future Date Already Exists In Ledger

            var editParticularMaterial =
                _materialLedgerRepo.FirstOrDefault(
                    l =>
                        DbFunctions.TruncateTime(l.LedgerDate) == stockDate && l.MaterialRefId == input.MaterialRefId &&
                        l.LocationRefId == input.LocationRefId);
            decimal closingBalance;
            if (editParticularMaterial == null) //  No Records Found Needs to add material for the Date
            {
                //  Get Close Balance from Yester records

                var newOpenBalance = 0.0m;

                var latestLedgerDate = (from led in _materialLedgerRepo.GetAll()
                                        orderby led.LedgerDate descending
                                        where led.MaterialRefId == input.MaterialRefId && led.LocationRefId == input.LocationRefId
                                              && DbFunctions.TruncateTime(led.LedgerDate) <= DbFunctions.TruncateTime(input.LedgerDate)
                                        select led.LedgerDate).ToList();

                if (latestLedgerDate.Count() == 0)
                {
                    //var lastDaysRecord = _materialLedgerRepo.FirstOrDefault(l => l.MaterialRefId == input.MaterialRefId && l.LocationRefId == input.LocationRefId);
                    newOpenBalance = input.OpenBalance;
                }
                else
                {
                    var maxLedgerDate = latestLedgerDate.Max();
                    var lastDaysRecord =
                        _materialLedgerRepo.FirstOrDefault(
                            l =>
                                (l.LedgerDate.Value.Year == maxLedgerDate.Value.Year &&
                                 l.LedgerDate.Value.Month == maxLedgerDate.Value.Month &&
                                 l.LedgerDate.Value.Day == maxLedgerDate.Value.Day)
                                && l.MaterialRefId == input.MaterialRefId
                                && l.LocationRefId == input.LocationRefId);
                    newOpenBalance = lastDaysRecord.ClBalance;
                }

                var item = new MaterialLedger();

                item.OpenBalance = newOpenBalance;
                input.OpenBalance = newOpenBalance;

                closingBalance = (input.OpenBalance + input.Received + input.ExcessReceived + input.Return +
                                  input.TransferIn) -
                                 (input.Issued + input.Sales + input.Damaged + input.SupplierReturn + input.Shortage +
                                  input.TransferOut);

                if (closingBalance < 0)
                {
                    var NegativeStockAdjustmentAuthorization = false;
                    if (PermissionChecker.IsGranted("Pages.Tenant.House.Master.MaterialLedger.NegativeStock"))
                    {
                        NegativeStockAdjustmentAuthorization = true;
                    }
                    if (NegativeStockAdjustmentAuthorization == false)
                    {
                        throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName));
                    }
                }

                if (input.Issued < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("Issue"));
                }

                if (input.Sales < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("Sales"));
                }

                if (input.Damaged < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("Damage"));
                }

                if (input.SupplierReturn < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("SupplierReturn"));
                }

                if (input.Shortage < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("Shortage"));
                }

                if (input.TransferOut < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("TransferOut"));
                }

                if (input.TransferIn < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("TransferIn"));
                }

                if (input.Return < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("Return"));
                }

                if (input.ExcessReceived < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("ExcessReceived"));
                }

                if (input.Received < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("Received"));
                }

                if (input.OpenBalance < 0)
                {
                    var NegativeStockAdjustmentAuthorization = false;
                    if (PermissionChecker.IsGranted("Pages.Tenant.House.Master.MaterialLedger.NegativeStock"))
                    {
                        NegativeStockAdjustmentAuthorization = true;
                    }
                    if (NegativeStockAdjustmentAuthorization == false)
                    {
                        throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                        L("OpenBalance"));
                    }
                }

                item.ClBalance = closingBalance;
                item.Damaged = input.Damaged;
                item.ExcessReceived = input.ExcessReceived;
                item.Issued = input.Issued;
                item.Sales = input.Sales;
                item.LedgerDate = input.LedgerDate.Date;
                item.LocationRefId = input.LocationRefId;
                item.MaterialRefId = input.MaterialRefId;
                item.Received = input.Received;
                item.Return = input.Return;
                item.Shortage = input.Shortage;
                item.SupplierReturn = input.SupplierReturn;
                item.TransferIn = input.TransferIn;
                item.TransferOut = input.TransferOut;

                await _materialLedgerRepo.InsertAsync(item);
            }
            else //   Modify the Existing Records
            {
                var item = await _materialLedgerRepo.GetAsync(editParticularMaterial.Id);
                var dto = input;

                item.Received = item.Received + dto.Received;
                item.Issued = item.Issued + dto.Issued;
                item.Sales = item.Sales + dto.Sales;
                item.Damaged = item.Damaged + dto.Damaged;
                item.SupplierReturn = item.SupplierReturn + dto.SupplierReturn;
                item.ExcessReceived = item.ExcessReceived + dto.ExcessReceived;
                item.Shortage = item.Shortage + dto.Shortage;
                item.Return = item.Return + dto.Return;
                item.TransferIn = item.TransferIn + dto.TransferIn;
                item.TransferOut = item.TransferOut + dto.TransferOut;

                closingBalance = (item.OpenBalance + item.Received + item.ExcessReceived + item.Return + item.TransferIn) -
                                 (item.Issued + item.Sales + item.Damaged + item.SupplierReturn + item.Shortage +
                                  item.TransferOut);

                if (closingBalance < 0)
                {
                    var NegativeStockAdjustmentAuthorization = false;
                    if (PermissionChecker.IsGranted("Pages.Tenant.House.Master.MaterialLedger.NegativeStock"))
                    {
                        NegativeStockAdjustmentAuthorization = true;
                    }
                    if (NegativeStockAdjustmentAuthorization == false)
                    {
                        throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName));
                    }
                }

                if (item.Issued < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("Issue"));
                }

                if (item.Sales < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("Sales"));
                }

                if (item.Damaged < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("Damage"));
                }

                if (item.SupplierReturn < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("SupplierReturn"));
                }

                if (item.Shortage < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("Shortage"));
                }

                if (item.TransferOut < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("TransferOut"));
                }

                if (input.TransferIn < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("TransferIn"));
                }

                if (item.Return < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("Return"));
                }

                if (item.ExcessReceived < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("ExcessReceived"));
                }

                if (item.Received < 0)
                {
                    throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                    L("Received"));
                }

                if (item.OpenBalance < 0)
                {
                    var NegativeStockAdjustmentAuthorization = false;
                    if (PermissionChecker.IsGranted("Pages.Tenant.House.Master.MaterialLedger.NegativeStock"))
                    {
                        NegativeStockAdjustmentAuthorization = true;
                    }
                    if (NegativeStockAdjustmentAuthorization == false)
                    {
                        throw new UserFriendlyException(L("StockShouldNotLessThanZero", material.MaterialName) + " " +
                                                        L("OpenBalance"));
                    }
                }

                item.ClBalance = closingBalance;

                await _materialLedgerRepo.InsertOrUpdateAsync(item);
            }

            if (input.CarryOverFlag)
            {
                var carryOverList = await _materialLedgerRepo.GetAllListAsync(
                    l => DbFunctions.TruncateTime(l.LedgerDate) > stockDate && l.MaterialRefId == input.MaterialRefId &&
                        l.LocationRefId == input.LocationRefId);
                foreach (var item in carryOverList)
                {
                    item.OpenBalance = closingBalance;
                    closingBalance = (item.OpenBalance + item.Received + item.ExcessReceived + item.Return + item.TransferIn) -
                                (item.Issued + item.Sales + item.Damaged + item.SupplierReturn + item.Shortage +
                                 item.TransferOut);
                    item.ClBalance = closingBalance;
                }
            }

            //  Set Current In Hand Stock

            var setInHand =
                _materiallocationwisestockRepo.FirstOrDefault(
                    l => l.MaterialRefId == input.MaterialRefId && l.LocationRefId == input.LocationRefId);

            //  @@Pending If not records in Material Location Wise Stock , then automatically add into and try once again

            if (setInHand == null)
            {
                throw new UserFriendlyException(L("StockSettingError"));
            }
            var particularMaterial = await _materiallocationwisestockRepo.GetAsync(setInHand.Id);
            particularMaterial.CurrentInHand = closingBalance;

            await _materiallocationwisestockRepo.InsertOrUpdateAndGetIdAsync(particularMaterial);

            return new EntityDto { Id = 1 }; //  1 is Success , 0 For Failure
        }

        [UnitOfWork]
        public async Task<DayCloseResultDto> IncludeSalesIntoLedgerIfMissing(SalesMissingIntoLedgerDto input)
        {
            bool salesIntoLedgerIfMissing = false;
            if (PermissionChecker.IsGranted("Pages.Tenant.House.Transaction.DayClose.MissingSalesIntoDayClose"))
            {
                salesIntoLedgerIfMissing = true;
            }

            if (salesIntoLedgerIfMissing == false)
            {
                throw new UserFriendlyException(L("IncludeSalesIntoLedgerIfMissing") + " " + L("Permission") + " " + L("Error"));
            }

            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);

            #region Sales Date Validation With Current Date and Future Date

            if (input.SalesDate.Date >= loc.HouseTransactionDate.Value.Date)
            //@@Pending Upto Synchranize With Sales BackUp
            {
                throw new UserFriendlyException(L("SalesDateShouldBeLessThanHouseTransactionDate"));
            }

            if (input.SalesDate.Date > DateTime.Today)
            {
                {
                    throw new UserFriendlyException(L("SalesDateShouldBeLessThanHouseTransactionDate"));
                }
            }

            if (input.SalesDate.Date == DateTime.Today)
            //@@Pending Upto Synchranize With Sales BackUp
            {
                throw new UserFriendlyException(L("Today") + " " + L("Error"));
            }

            if (input.SalesDate.Date > DateTime.Today)
            {
                {
                    throw new UserFriendlyException(L("SalesDateShouldBeLessThanHouseTransactionDate"));
                }
            }

            #endregion Sales Date Validation With Current Date and Future Date

            bool autoCompAndAutoWastageFlag = true;

            #region NeedToVerifyAnySales Records Already Available on the given date In Ledger

            var salesDataAlreadyExists = await _materialLedgerRepo.GetAllListAsync(t => DbFunctions.TruncateTime(t.LedgerDate) == DbFunctions.TruncateTime(input.SalesDate.Date)
                && t.Sales > 0 && t.LocationRefId == input.LocationRefId);
            if (salesDataAlreadyExists.Count > 0)
            {
                autoCompAndAutoWastageFlag = false;
                var saleMaterial = salesDataAlreadyExists.First();
                var mat = await _materialRepo.FirstOrDefaultAsync(t => t.Id == saleMaterial.MaterialRefId);
                //throw new UserFriendlyException(L("SalesDataAlreadyExists", salesDataAlreadyExists[0].LedgerDate.Value.ToString("ddd dd-MMM-yyyy"), mat.MaterialName, salesDataAlreadyExists[0].Sales));
            }

            #endregion NeedToVerifyAnySales Records Already Available on the given date In Ledger

            #region Get Sales From Connect
            var locinput = new List<LocationListDto>();
            locinput.Add(loc.MapTo<LocationListDto>());

            var fromDt = input.SalesDate.Date;
            var toDt = input.SalesDate.Date;

            //  Get Sales
            var daySales = await _connectReportAppService.GetAllItemSales(
                new GetItemInput
                {
                    StartDate = fromDt,
                    EndDate = toDt,
                    Locations = locinput.MapTo<List<SimpleLocationDto>>(),
                    Sales = true,
                    Comp = true,
                    Void = true,
                    Gift = true
                });

            var sales = await _connectReportAppService.GetTickets(new GetTicketInput
            {
                StartDate = fromDt,
                EndDate = toDt,
                Locations = locinput.MapTo<List<SimpleLocationDto>>()
            }, true);

            var result = new DayCloseResultDto();
            result.DayCloseSuccessFlag = false;

            result.DayCloseSyncErrorList = null;
            result.LastDayCloseErrorFlag = false;

            if (sales.DashBoardDto.TotalAmount > 0)
            {
                result.SalesExistFlag = true;
                result.SalesAmount = sales.DashBoardDto.TotalAmount;
                if (PermissionChecker.IsGranted("Pages.Tenant.House.Transaction.DayClose.HideSalesOnDayClose"))
                {
                    result.SalesMessage = L("DayCloseHideSalesMessage");
                }
                else
                {
                    result.SalesMessage = L("DaySalesForLocation", fromDt.ToString("dd-MMM-yy"), loc.Name,
                        result.SalesAmount);
                }
            }
            else
            {
                result.SalesExistFlag = false;
                result.SalesAmount = 0;
                result.SalesMessage = L("NoSalesAlertMessage", fromDt.ToString("dd-MMM-yy"), loc.Name);

                if (input.ResetFlag)
                {
                    input.LedgerAffect = false;
                }
            }

            #endregion

            var rsMaterials = await _materialRepo.GetAllListAsync();
            var rsMaterialMappings = await _materialmenumappingRepo.GetAllListAsync();
            var rsMenumappingDepartment = await _menumappingDepartmentRepo.GetAllListAsync();
            var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();
            var rsDeptList = await _departmentRepo.GetAll().Select(t => new { t.Id, t.Name }).ToListAsync();

            #region DayCloseTemplateSave

            if (input.LedgerAffect)
            {
                {
                    #region UcTemplate

                    var existUcTemplate =
                        await
                            _daycloseTemplateRepo.FirstOrDefaultAsync(
                                t =>
                                    DbFunctions.TruncateTime(t.ClosedDate) ==
                                    DbFunctions.TruncateTime(input.SalesDate) &&
                                    t.TemplateType == (int)DayCloseTemplateType.UnitConversions);

                    int? daycloseUcExistId = null;
                    if (existUcTemplate != null)
                        daycloseUcExistId = existUcTemplate.Id;

                    var uct = new UnitConversionTemplate
                    {
                        UcList = rsUc.MapTo<List<UnitConversionTemplateDto>>()
                    };

                    var templateJsonUc = _xmlandjsonConvertorAppService.SerializeToJSON(uct);

                    var newDcDtoUc = new DayCloseTemplateEditDto
                    {
                        Id = daycloseUcExistId,
                        LocationRefId = input.LocationRefId,
                        TableName = "UnitConversions",
                        TemplateData = templateJsonUc,
                        TemplateType = (int)DayCloseTemplateType.UnitConversions,
                        ClosedDate = input.SalesDate.Date
                    };

                    await _templateAppService.CreateOrUpdateDayCloseTemplate(
                        new CreateOrUpdateDayCloseTemplateInput
                        {
                            DayCloseTemplate = newDcDtoUc
                        });

                    #endregion UcTemplate
                }

                {
                    #region MenuMappingDepartmentTemplate

                    var existMenuDepartmentTemplate =
                        await
                            _daycloseTemplateRepo.FirstOrDefaultAsync(
                                t =>
                                    DbFunctions.TruncateTime(t.ClosedDate) ==
                                    DbFunctions.TruncateTime(input.SalesDate) &&
                                    t.TemplateType == (int)DayCloseTemplateType.MenuMappingDepartment);

                    int? daycloseMMExistId = null;
                    if (existMenuDepartmentTemplate != null)
                        daycloseMMExistId = existMenuDepartmentTemplate.Id;

                    var mmdept = new MenuMappingDepartmentTemplate
                    {
                        MenuMappingDepartment = rsMenumappingDepartment.MapTo<List<MenuMappingDepartmentTemplateDto>>()
                    };

                    var templateJsonMMDept = _xmlandjsonConvertorAppService.SerializeToJSON(mmdept);

                    var newMMDeptDto = new DayCloseTemplateEditDto
                    {
                        Id = daycloseMMExistId,
                        LocationRefId = input.LocationRefId,
                        TableName = "MenuMappingDepartment",
                        TemplateData = templateJsonMMDept,
                        TemplateType = (int)DayCloseTemplateType.MenuMappingDepartment,
                        ClosedDate = input.SalesDate.Date
                    };

                    await _templateAppService.CreateOrUpdateDayCloseTemplate(
                        new CreateOrUpdateDayCloseTemplateInput
                        {
                            DayCloseTemplate = newMMDeptDto
                        });

                    #endregion MenuMappingDepartmentTemplate
                }

                {
                    #region MaterialMenuMappingTemplate

                    var existTemplate =
                        await
                            _daycloseTemplateRepo.FirstOrDefaultAsync(
                                t =>
                                    DbFunctions.TruncateTime(t.ClosedDate) ==
                                    DbFunctions.TruncateTime(input.SalesDate) &&
                                    t.TemplateType == (int)DayCloseTemplateType.MaterialMenuMappings);

                    int? daycloseExistId = null;
                    if (existTemplate != null)
                        daycloseExistId = existTemplate.Id;

                    var mmt = new MaterialMappingTemplate
                    {
                        MappingList = rsMaterialMappings.MapTo<List<MaterialMappingTemplateDto>>()
                    };

                    var templateJsonMM = _xmlandjsonConvertorAppService.SerializeToJSON(mmt);

                    var newDcDto = new DayCloseTemplateEditDto
                    {
                        Id = daycloseExistId,
                        LocationRefId = input.LocationRefId,
                        TableName = "MaterialMenuMappings",
                        TemplateData = templateJsonMM,
                        TemplateType = (int)DayCloseTemplateType.MaterialMenuMappings,
                        ClosedDate = input.SalesDate.Date
                    };

                    await _templateAppService.CreateOrUpdateDayCloseTemplate(
                        new CreateOrUpdateDayCloseTemplateInput
                        {
                            DayCloseTemplate = newDcDto
                        });

                    #endregion MaterialMenuMappingTemplate
                }

                #region DashBoardSalesDto

                {
                    var existSalesTemplate =
                        await
                            _daycloseTemplateRepo.FirstOrDefaultAsync(
                                t =>
                                    DbFunctions.TruncateTime(t.ClosedDate) ==
                                    DbFunctions.TruncateTime(input.SalesDate) &&
                                    t.TemplateType == (int)DayCloseTemplateType.DashboardTicketDto);

                    int? daycloseExistId = null;
                    if (existSalesTemplate != null)
                        daycloseExistId = existSalesTemplate.Id;

                    var dbdto = sales.DashBoardDto;

                    var templateJsonMM = _xmlandjsonConvertorAppService.SerializeToJSON(dbdto);

                    var newDcDto = new DayCloseTemplateEditDto
                    {
                        Id = daycloseExistId,
                        LocationRefId = input.LocationRefId,
                        TableName = "DashboardTicketDto",
                        TemplateData = templateJsonMM,
                        TemplateType = (int)DayCloseTemplateType.DashboardTicketDto,
                        ClosedDate = input.SalesDate.Date
                    };

                    await _templateAppService.CreateOrUpdateDayCloseTemplate(
                        new CreateOrUpdateDayCloseTemplateInput
                        {
                            DayCloseTemplate = newDcDto
                        });
                }

                #endregion DashBoardSalesDto
            }

            #endregion DayCloseTemplateSave

            #region AutoSalesCalculation

            var mapwithMenuList = new List<MaterialMenuMappingWithWipeOut>();
            var itemSales = daySales.Sales.MapTo<List<MenuListDto>>();

            if (daySales.Gift.Count > 0)
                itemSales.AddRange(daySales.Gift);

            foreach (var item in itemSales)
            {
                var menuMapping = rsMaterialMappings.Where(t => t.PosMenuPortionRefId == item.MenuItemPortionId
                                                                && t.AutoSalesDeduction && t.LocationRefId == input.LocationRefId).ToList();
                if (menuMapping == null || menuMapping.Count == 0)
                {
                    menuMapping = rsMaterialMappings.Where(t => t.PosMenuPortionRefId == item.MenuItemPortionId
                                                                && t.AutoSalesDeduction && t.LocationRefId == null).ToList();
                }
                var department = rsDeptList.FirstOrDefault(t => t.Name.ToUpper().Equals(item.DepartmentName.ToUpper()));

                if (department == null)
                {
                    throw new UserFriendlyException(L("Department") + " " + L("NotExist"), item.DepartmentName);
                }

                var deptId = department.Id;

                var menuMappingRefIds = menuMapping.Select(t => t.Id).ToArray();

                var deptList =
                    rsMenumappingDepartment.Where(t => menuMappingRefIds.Contains(t.MenuMappingRefId)).ToList();

                bool AllDepartmentFlag;
                if (deptList.Count() == 0)
                {
                    AllDepartmentFlag = true;
                }
                else
                {
                    AllDepartmentFlag = false;
                }

                foreach (var MmMaterial in menuMapping)
                {
                    if (AllDepartmentFlag == false)
                    {
                        var deptLinkedMM = deptList.Where(t => t.MenuMappingRefId == MmMaterial.Id);
                        if (deptLinkedMM.Count() > 0)
                        {
                            deptLinkedMM = deptLinkedMM.Where(t => t.DepartmentRefId == deptId);
                            if (deptLinkedMM.Count() == 0)
                            {
                                continue;
                            }
                        }
                    }

                    var newmm = new MaterialMenuMappingWithWipeOut();
                    var mat = rsMaterials.FirstOrDefault(t => t.Id == MmMaterial.MaterialRefId);

                    if (mat == null)
                    {
                        using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                        {
                            var deletedMaterial = await _materialRepo.FirstOrDefaultAsync(t => t.Id == MmMaterial.MaterialRefId);
                            var deletedMaterialName = deletedMaterial == null ? "" : deletedMaterial.MaterialName;
                            throw new UserFriendlyException(L("SomeOfTheLinkedMaterialsInMenuMappingDeleted") + " " + L("MATERIALNAMEISWRONG", deletedMaterialName));
                        }
                        throw new UserFriendlyException(L("SomeOfTheLinkedMaterialsInMenuMappingDeleted"));
                    }

                    newmm.PosMenuPortionRefId = MmMaterial.PosMenuPortionRefId;
                    newmm.MenuQuantitySold = MmMaterial.MenuQuantitySold;
                    newmm.MaterialRefId = MmMaterial.MaterialRefId;
                    newmm.PortionQty = Math.Round(MmMaterial.PortionQty / MmMaterial.MenuQuantitySold * item.Quantity, 14);
                    newmm.PortionUnitId = MmMaterial.PortionUnitId;
                    newmm.TotalSaleQuantity = item.Quantity;
                    newmm.AutoSalesDeduction = MmMaterial.AutoSalesDeduction;
                    newmm.DefaultUnitId = mat.DefaultUnitId;

                    mapwithMenuList.Add(newmm);
                }
            }

            var mapwithMenuListGroupWithUnitConversion = (from salemenu in mapwithMenuList
                                                          join uc in rsUc
                                                              on salemenu.PortionUnitId equals uc.BaseUnitId
                                                          where uc.RefUnitId == salemenu.DefaultUnitId
                                                          group salemenu by new
                                                          {
                                                              salemenu.MaterialRefId,
                                                              salemenu.MaterialRefName,
                                                              salemenu.DefaultUnitId,
                                                              salemenu.PortionUnitId,
                                                              salemenu.UnitRefName,
                                                              salemenu.DefaultUnitName,
                                                              uc.Conversion
                                                          }
                into g
                                                          select new MaterialMenuMappingWithWipeOut
                                                          {
                                                              MaterialRefId = g.Key.MaterialRefId,
                                                              PortionUnitId = g.Key.PortionUnitId,
                                                              UnitRefName = g.Key.UnitRefName,
                                                              DefaultUnitId = g.Key.DefaultUnitId,
                                                              DefaultUnitName = g.Key.DefaultUnitName,
                                                              PortionQty = g.Sum(t => t.PortionQty * g.Key.Conversion)
                                                          }).ToList().OrderBy(t => t.MaterialRefId);

            var mapWithMenuListSumGroup = (from salemenu in mapwithMenuListGroupWithUnitConversion
                                           group salemenu by salemenu.MaterialRefId
                into g
                                           select new MaterialMenuMappingWithWipeOut
                                           {
                                               MaterialRefId = g.Key,
                                               PortionQty = g.Sum(t => t.PortionQty)
                                           }).ToList();

            //  Select all Semi Materials
            List<MaterialSalesWithWipeOutCloseDayDto> allItems;
            allItems = await (from mat in _materialRepo.GetAll().
                Where(m => m.MaterialTypeId != (int)MaterialType.RAW)
                              join unit in _unitRepo.GetAll()
                                  on mat.DefaultUnitId equals unit.Id
                              join uiss in _unitRepo.GetAll()
                                  on mat.IssueUnitId equals uiss.Id
                              select new MaterialSalesWithWipeOutCloseDayDto
                              {
                                  MaterialRefId = mat.Id,
                                  MaterialRefName = mat.MaterialName,
                                  Uom = unit.Name,
                                  ClBalance = 0,
                                  WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                                  AutoSalesDeduction = false,
                                  DefaultUnitId = mat.DefaultUnitId,
                                  DefaultUnitName = unit.Name,
                                  IssueUnitId = mat.IssueUnitId,
                                  IssueUnitName = uiss.Name
                              }).ToListAsync();

            List<MaterialSalesWithWipeOutCloseDayDto> allRawItemsAutoSales;

            allRawItemsAutoSales =
                await (from mat in _materialRepo.GetAll().Where(m => m.MaterialTypeId == (int)MaterialType.RAW)
                       join unit in _unitRepo.GetAll() on mat.DefaultUnitId equals unit.Id
                       join menumap in _materialmenumappingRepo.GetAll().Where(t => t.AutoSalesDeduction)
                           on mat.Id equals menumap.MaterialRefId
                       join uiss in _unitRepo.GetAll()
                           on mat.IssueUnitId equals uiss.Id
                       select new MaterialSalesWithWipeOutCloseDayDto
                       {
                           MaterialRefId = mat.Id,
                           MaterialRefName = mat.MaterialName,
                           Uom = unit.Name,
                           ClBalance = 0,
                           WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                           AutoSalesDeduction = menumap.AutoSalesDeduction,
                           DefaultUnitId = mat.DefaultUnitId,
                           DefaultUnitName = unit.Name,
                           IssueUnitId = mat.IssueUnitId,
                           IssueUnitName = uiss.Name
                       }).Distinct().ToListAsync();

            var sortMenuItems = allItems.Union(allRawItemsAutoSales).ToList();

            foreach (var wipeoutitem in sortMenuItems)
            {
                if (wipeoutitem.MaterialRefId == 82)
                {
                    var K = 5;
                }
                var editParticularMaterial =
                    await
                        _materialLedgerRepo.FirstOrDefaultAsync(
                            l =>
                                DbFunctions.TruncateTime(l.LedgerDate) == input.SalesDate.Date &&
                                l.MaterialRefId == wipeoutitem.MaterialRefId && l.LocationRefId == input.LocationRefId);

                var newClBalance = 0.0m;

                if (editParticularMaterial == null) //  No Records Found Needs to add material for the Date
                {
                    //  Get Close Balance from Yester records

                    var latestLedgerDate = (from led in _materialLedgerRepo.GetAll()
                                            orderby led.LedgerDate descending
                                            where led.MaterialRefId == wipeoutitem.MaterialRefId
                                                  && led.LocationRefId == input.LocationRefId
                                                  &&
                                                  DbFunctions.TruncateTime(led.LedgerDate) <=
                                                  DbFunctions.TruncateTime(input.SalesDate.Date)
                                            select led.LedgerDate).ToList();

                    if (latestLedgerDate.Count() == 0)
                    {
                        //var lastDaysRecord = _materialLedgerRepo.FirstOrDefault(l => l.MaterialRefId == input.MaterialRefId && l.LocationRefId == input.LocationRefId);
                        newClBalance = 0;
                    }
                    else
                    {
                        var maxLedgerDate = latestLedgerDate.Max();
                        var lastDaysRecord =
                            _materialLedgerRepo.FirstOrDefault(
                                l =>
                                    (l.LedgerDate.Value.Year == maxLedgerDate.Value.Year &&
                                     l.LedgerDate.Value.Month == maxLedgerDate.Value.Month &&
                                     l.LedgerDate.Value.Day == maxLedgerDate.Value.Day)
                                    && l.MaterialRefId == wipeoutitem.MaterialRefId
                                    && l.LocationRefId == input.LocationRefId);
                        newClBalance = lastDaysRecord.ClBalance;
                    }
                }
                else
                {
                    newClBalance = editParticularMaterial.ClBalance;
                }

                wipeoutitem.ClBalance = newClBalance;

                var salefromconnect = mapWithMenuListSumGroup.FirstOrDefault(t => t.MaterialRefId == wipeoutitem.MaterialRefId);
                if (salefromconnect == null)
                {
                    wipeoutitem.Sales = 0;
                }
                else
                {
                    wipeoutitem.Sales = salefromconnect.PortionQty;
                }
                var salesQty = wipeoutitem.Sales;
                var salesAlreadyAdded = salesDataAlreadyExists.FirstOrDefault(t => t.MaterialRefId == wipeoutitem.MaterialRefId);
                if (salesAlreadyAdded == null)
                    salesQty = wipeoutitem.Sales;
                else
                    salesQty = wipeoutitem.Sales - salesAlreadyAdded.Sales;
                wipeoutitem.Sales = salesQty;

                if (wipeoutitem.WipeOutStockOnClosingDay)
                {
                    if (wipeoutitem.Sales == wipeoutitem.ClBalance)
                        wipeoutitem.Remarks = L("Tallied");
                    else if (wipeoutitem.Sales > wipeoutitem.ClBalance)
                    {
                        wipeoutitem.Remarks = L("ReceivedEntryMissing");
                        wipeoutitem.Difference = wipeoutitem.ClBalance - wipeoutitem.Sales;
                    }
                    else
                    {
                        if (wipeoutitem.Sales == 0)
                        {
                            wipeoutitem.Remarks = L("SalesMightNotUploaded");
                        }
                        else
                        {
                            wipeoutitem.Remarks = L("WastageEntryMissing");
                        }
                        wipeoutitem.Difference = wipeoutitem.ClBalance -
                                                              wipeoutitem.Sales;
                    }
                }
                else
                {
                    wipeoutitem.Difference = wipeoutitem.ClBalance -
                                                          wipeoutitem.Sales;
                    if (wipeoutitem.Difference == 0)
                    {
                        wipeoutitem.Remarks = L("AllStockSold");
                    }
                    else if (wipeoutitem.Difference < 0)
                    {
                        wipeoutitem.Remarks = L("ReceivedOrProductionEntryMissing");
                    }
                    else
                    {
                        wipeoutitem.Remarks = L("CarryOverToNextDay");
                    }
                }

                if (input.LedgerAffect && salesQty != 0)
                {
                    var ml = new MaterialLedgerDto();
                    ml.LocationRefId = input.LocationRefId;
                    ml.MaterialRefId = wipeoutitem.MaterialRefId;
                    ml.LedgerDate = input.SalesDate;
                    ml.Sales = salesQty;
                    ml.CarryOverFlag = true;
                    var matLedgerId = await CarryOverUpdateMaterialLedger(ml);
                }

            }

            var allListDtos = sortMenuItems.MapTo<List<MaterialSalesWithWipeOutCloseDayDto>>();

            allListDtos = allListDtos.OrderBy(t => t.WipeOutStockOnClosingDay).OrderBy(t => t.Remarks).ToList();

            var allItemCount = sortMenuItems.Count();

            result.SalesData = allListDtos;

            #endregion AutoSalesCalculation

            if (autoCompAndAutoWastageFlag)
            {
                #region AutoCompWastageCalculation


                var wastagemapwithMenuList = new List<MaterialMenuMappingWithWipeOut>();

                var itemComp = daySales.Comp.MapTo<List<MenuListDto>>();

                ////if (itemComp == null || itemComp.Count==0 )
                ////    itemComp = daySales.Sales.Take(2).MapTo<List<MenuListDto>>();

                foreach (var item in itemComp)
                {
                    var outputlist =
                        rsMaterialMappings.Where(
                            t => t.PosMenuPortionRefId == item.MenuItemPortionId && t.AutoSalesDeduction && t.LocationRefId == input.LocationRefId).ToList();
                    if (outputlist == null || outputlist.Count == 0)
                    {
                        outputlist =
                        rsMaterialMappings.Where(
                            t => t.PosMenuPortionRefId == item.MenuItemPortionId && t.AutoSalesDeduction && t.LocationRefId == null).ToList();
                    }
                    foreach (var a in outputlist)
                    {
                        var newmm = new MaterialMenuMappingWithWipeOut();
                        var mat = rsMaterials.FirstOrDefault(t => t.Id == a.MaterialRefId);

                        if (mat == null)
                        {
                            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                            {
                                var deletedMaterial = await _materialRepo.FirstOrDefaultAsync(t => t.Id == a.MaterialRefId);
                                var deletedMaterialName = deletedMaterial == null ? "" : deletedMaterial.MaterialName;
                                throw new UserFriendlyException(L("SomeOfTheLinkedMaterialsInMenuMappingDeleted") + " " + L("MATERIALNAMEISWRONG", deletedMaterialName));
                            }
                            //throw new UserFriendlyException(L("SomeOfTheLinkedMaterialsInMenuMappingDeleted"));
                        }

                        newmm.PosMenuPortionRefId = a.PosMenuPortionRefId;
                        newmm.MenuQuantitySold = a.MenuQuantitySold;
                        newmm.MaterialRefId = a.MaterialRefId;
                        newmm.PortionQty = Math.Round(a.PortionQty / a.MenuQuantitySold * item.Quantity, 14);
                        newmm.PortionUnitId = a.PortionUnitId;
                        newmm.TotalSaleQuantity = item.Quantity;
                        newmm.AutoSalesDeduction = a.AutoSalesDeduction;
                        newmm.DefaultUnitId = mat.DefaultUnitId;

                        wastagemapwithMenuList.Add(newmm);
                    }
                }

                var mapwithCompMenuListGroupWithUnitConversion
                    = (from compmenu in wastagemapwithMenuList
                       join uc in rsUc
                           on compmenu.PortionUnitId equals uc.BaseUnitId
                       where uc.RefUnitId == compmenu.DefaultUnitId
                       group compmenu by new
                       {
                           compmenu.MaterialRefId,
                           compmenu.MaterialRefName,
                           compmenu.DefaultUnitId,
                           compmenu.PortionUnitId,
                           compmenu.UnitRefName,
                           compmenu.DefaultUnitName,
                           uc.Conversion
                       }
                        into g
                       select new MaterialMenuMappingWithWipeOut
                       {
                           MaterialRefId = g.Key.MaterialRefId,
                           PortionUnitId = g.Key.PortionUnitId,
                           UnitRefName = g.Key.UnitRefName,
                           DefaultUnitId = g.Key.DefaultUnitId,
                           DefaultUnitName = g.Key.DefaultUnitName,
                           PortionQty = g.Sum(t => t.PortionQty * g.Key.Conversion)
                       }).ToList().OrderBy(t => t.MaterialRefId);

                var mapWithCompMenuListSumGroup = (from compmenu in mapwithCompMenuListGroupWithUnitConversion
                                                   group compmenu by compmenu.MaterialRefId into g
                                                   select new MaterialMenuMappingWithWipeOut
                                                   {
                                                       MaterialRefId = g.Key,
                                                       PortionQty = g.Sum(t => t.PortionQty)
                                                   }).ToList();

                List<MaterialSalesWithWipeOutCloseDayDto> allCompItems;
                allCompItems = await (from mat in _materialRepo.GetAll().
                    Where(m => m.MaterialTypeId != (int)MaterialType.RAW)
                                      join unit in _unitRepo.GetAll()
                                          on mat.DefaultUnitId equals unit.Id
                                      join uiss in _unitRepo.GetAll()
                                          on mat.IssueUnitId equals uiss.Id
                                      select new MaterialSalesWithWipeOutCloseDayDto
                                      {
                                          MaterialRefId = mat.Id,
                                          MaterialRefName = mat.MaterialName,
                                          Uom = unit.Name,
                                          ClBalance = 0,
                                          WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                                          AutoSalesDeduction = false,
                                          DefaultUnitId = mat.DefaultUnitId,
                                          DefaultUnitName = unit.Name,
                                          IssueUnitId = mat.IssueUnitId,
                                          IssueUnitName = uiss.Name
                                      }).ToListAsync();

                List<MaterialSalesWithWipeOutCloseDayDto> allCompRawItemsAutoSales;
                allCompRawItemsAutoSales = await (from mat in _materialRepo.GetAll()
                    .Where(m => m.MaterialTypeId == (int)MaterialType.RAW)
                                                  join unit in _unitRepo.GetAll() on mat.DefaultUnitId equals unit.Id
                                                  join menumap in _materialmenumappingRepo
                                                      .GetAll().Where(t => t.AutoSalesDeduction)
                                                      on mat.Id equals menumap.MaterialRefId
                                                  join uiss in _unitRepo.GetAll()
                                                      on mat.IssueUnitId equals uiss.Id
                                                  select new MaterialSalesWithWipeOutCloseDayDto
                                                  {
                                                      MaterialRefId = mat.Id,
                                                      MaterialRefName = mat.MaterialName,
                                                      Uom = unit.Name,
                                                      ClBalance = 0,
                                                      WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                                                      AutoSalesDeduction = menumap.AutoSalesDeduction,
                                                      DefaultUnitId = mat.DefaultUnitId,
                                                      DefaultUnitName = unit.Name,
                                                      IssueUnitId = mat.IssueUnitId,
                                                      IssueUnitName = uiss.Name
                                                  }).Distinct().ToListAsync();

                var wastageSortCompMenuItems = allCompItems.Union(allCompRawItemsAutoSales).ToList();

                foreach (var wastageitem in wastageSortCompMenuItems)
                {
                    var wastagefromComp =
                        mapWithCompMenuListSumGroup.FirstOrDefault(t => t.MaterialRefId == wastageitem.MaterialRefId);
                    if (wastagefromComp == null)
                    {
                        wastageitem.Damaged = 0;
                    }
                    else
                    {
                        wastageitem.Damaged = wastagefromComp.PortionQty;
                    }
                }

                var allWastageListDtos = wastageSortCompMenuItems.MapTo<List<MaterialSalesWithWipeOutCloseDayDto>>();

                allWastageListDtos = allWastageListDtos.Where(t => t.Damaged > 0).ToList();

                allWastageListDtos = allWastageListDtos.OrderBy(t => t.MaterialRefId).OrderBy(t => t.Remarks).ToList();
                result.CompData = allWastageListDtos;

                #endregion AutoCompWastageCalculation

                #region AdjustmentForCompWastage

                var adjustmentDetailDtos = new List<AdjustmentDetailViewDto>();

                if (input.LedgerAffect)
                {
                    var adjSno = 1;

                    var newAdjDto = new CreateOrUpdateAdjustmentInput();
                    if (allWastageListDtos.Count > 0)
                    {
                        var masDto = new AdjustmentEditDto();

                        if (input.SalesDate == null)
                        {
                            var tloc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
                            masDto.AdjustmentDate = tloc.HouseTransactionDate.Value.Date;
                        }
                        else
                            masDto.AdjustmentDate = input.SalesDate.Date;

                        masDto.AdjustmentRemarks = L("CompWastage");
                        masDto.ApprovedPersonId = (int)AbpSession.UserId;
                        masDto.LocationRefId = input.LocationRefId;
                        masDto.EnteredPersonId = (int)AbpSession.UserId;
                        masDto.TokenRefNumber = 555;

                        newAdjDto.Adjustment = masDto;
                        newAdjDto.AdjustmentDetail = adjustmentDetailDtos;

                        var adjMasDto = masDto.MapTo<Adjustment>();

                        var locationAdj = await _locationRepo.FirstOrDefaultAsync(t => t.Id == adjMasDto.LocationRefId);
                        adjMasDto.AccountDate = locationAdj.HouseTransactionDate.Value;

                        var adjId = await _adjustmentRepo.InsertOrUpdateAndGetIdAsync(adjMasDto);

                        foreach (var items in allWastageListDtos)
                        {
                            var det = new AdjustmentDetail();
                            var adjustmentQty = items.Damaged;
                            var adjustmentMode = L("Wastage");

                            if (items.DefaultUnitName.Length == 0 || items.DefaultUnitId == 0)
                            {
                                throw new UserFriendlyException("UnitErr");
                            }
                            det.AdjustmentRefIf = adjId;
                            det.MaterialRefId = items.MaterialRefId;
                            det.AdjustmentQty = adjustmentQty;
                            det.Sno = adjSno++;
                            det.AdjustmentMode = adjustmentMode;
                            det.AdjustmentApprovedRemarks = "";
                            det.UnitRefId = items.DefaultUnitId;

                            await _adjustmentdetailRepo.InsertAndGetIdAsync(det);
                            var ml = new MaterialLedgerDto
                            {
                                LocationRefId = input.LocationRefId,
                                MaterialRefId = det.MaterialRefId,
                                LedgerDate = masDto.AdjustmentDate
                            };
                            if (det.AdjustmentMode.Equals(L("Excess")))
                                ml.ExcessReceived = det.AdjustmentQty;
                            else if (det.AdjustmentMode.Equals(L("Shortage")))
                                ml.Shortage = det.AdjustmentQty;
                            else if (det.AdjustmentMode.Equals(L("Wastage")))
                                ml.Damaged = det.AdjustmentQty;
                            else if (det.AdjustmentMode.Equals(L("Damaged")))
                                ml.Damaged = det.AdjustmentQty;
                            else
                                throw new UserFriendlyException(L("AdjustmentMode") + " " + L("Error") + det.AdjustmentMode);
                            ml.UnitId = det.UnitRefId;

                            ml.CarryOverFlag = true;
                            var matLedgerId = await CarryOverUpdateMaterialLedger(ml);
                        }
                        //var adjId = await _adjustmentAppService.CreateOrUpdateAdjustment(newAdjDto);
                    }
                }

                #endregion AdjustmentForCompWastage
            }

            result.DayCloseSuccessFlag = true;
            return result;
        }

        public async Task<PagedResultDto<DayCloseTemplateListDto>> GetAllDayClose(DayCloseDtoView input)
        {
            var allItems = _daycloseTemplateRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId);
            allItems = allItems.Where(t => t.TemplateType == 3);
            var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
            allItems = allItems.Where(t => t.ClosedDate != location.HouseTransactionDate);
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<DayCloseTemplateListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultDto<DayCloseTemplateListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetMaterialAveragePriceTagForCombobox()
        {
            var retList = new List<ComboboxItemDto>();

            string enumstring;
            var EnumValues = Enum.GetValues(typeof(AveragePriceTag));
            foreach (int EnumValue in EnumValues)
            {
                enumstring = Enum.GetName(typeof(AveragePriceTag), EnumValue);
                retList.Add(new ComboboxItemDto { Value = EnumValue.ToString(), DisplayText = enumstring });
            }

            return
                new ListResultDto<ComboboxItemDto>(
                    retList.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText)).ToList());
        }


        public async Task ResetDayCloseRequiredInBackGround(LocationInputDto input)
        {
            var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
            location.DoesDayCloseRunInBackGround = false;
            location.BackGrandEndTime = DateTime.Now;
            await _locationRepo.UpdateAsync(location);

            //Hangfire.JobStorage.Current

        }

        public async Task<GetVerifyMaterialLedgerDto> VerifyAndRectifyOpeningStockIssueForGivenDate(VerifyMaterialLedgerClosingStockDto input)
        {
            List<string> OutputChangesList = new List<string>();
            input.CanChangeDbValue = true;

            #region VerifyConstraints
            if (input.LocationRefId == 0)
            {
                throw new UserFriendlyException(L("LocationIdDoesNotExist", input.LocationRefId));
            }
            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
            if (loc == null)
            {
                throw new UserFriendlyException(L("LocationIdDoesNotExist", input.LocationRefId));
            }
            #endregion

            if (input.StartDate == DateTime.MinValue)
            {
                throw new UserFriendlyException("DateErr");
            }

            List<int> arrMaterialRefIds = new List<int>();

            if (input.MaterialsList != null)
                arrMaterialRefIds = input.MaterialsList.Select(t => int.Parse(t.Value)).ToList();

            if (input.MaterialRefId.HasValue)
                arrMaterialRefIds.Add(input.MaterialRefId.Value);

            List<MaterialLedger> lstLedgers = new List<MaterialLedger>();
            var iquery = _materialLedgerRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId
                    && DbFunctions.TruncateTime(t.LedgerDate.Value) == DbFunctions.TruncateTime(input.StartDate.Date));
            if (arrMaterialRefIds.Count > 0)
            {
                iquery = iquery.Where(t => arrMaterialRefIds.Contains(t.MaterialRefId));
            }
            lstLedgers = await iquery.ToListAsync();
            if (lstLedgers.Count == 0)
            {
                return new GetVerifyMaterialLedgerDto();
            }

            lstLedgers = lstLedgers.OrderBy(t => t.MaterialRefId).ToList();

            arrMaterialRefIds = lstLedgers.Select(t => t.MaterialRefId).Distinct().ToList();
            var rsMaterials = await _materialRepo.GetAllListAsync(t => arrMaterialRefIds.Contains(t.Id));

            DateTime prevDay = input.StartDate.AddDays(-1);
            #region ForLooop
            foreach (var ledger in lstLedgers)
            {
                #region GetPreviousOpenBalance
                decimal newOpenBalance = 0.0m;
                //DateTime lastClosingDate = DateTime.Today;

                var maxLedgerDate = await GetLatestLedgerForGivenLocationMaterialRefId(
                    new LocationAndMaterialDto
                    {
                        MaterialRefId = ledger.MaterialRefId,
                        LocationRefId = input.LocationRefId,
                        TransactionDate = prevDay
                    });

                if (maxLedgerDate == null)
                {
                    continue;
                }
                else
                {
                    newOpenBalance = maxLedgerDate.ClBalance;
                }
                if (newOpenBalance == ledger.OpenBalance)
                    continue;
                #endregion

                var difference = newOpenBalance - ledger.OpenBalance;
                if (difference == 0)
                    continue;

                ledger.OpenBalance = newOpenBalance;
                decimal closingBalance = 0;
                closingBalance = (newOpenBalance + ledger.Received + ledger.ExcessReceived + ledger.Return + ledger.TransferIn) - (ledger.Issued + ledger.Sales + ledger.Damaged + ledger.SupplierReturn + ledger.Shortage + ledger.TransferOut);

                ledger.OpenBalance = newOpenBalance;
                ledger.ClBalance = closingBalance;
                await _materialLedgerRepo.UpdateAsync(ledger);

                var mat = rsMaterials.FirstOrDefault(t => t.Id == ledger.MaterialRefId);
                var errorlistMaterialLedger = await _materialLedgerRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId && t.MaterialRefId == ledger.MaterialRefId && DbFunctions.TruncateTime(t.LedgerDate) > DbFunctions.TruncateTime(input.StartDate)).OrderBy(t => t.LedgerDate).ToListAsync();

                bool firstDayFlag = true;
                foreach (var lst in errorlistMaterialLedger)
                {
                    if (firstDayFlag == true)
                    {
                        if (difference > 0)
                        {
                            closingBalance = lst.ClBalance + difference;
                            newOpenBalance = lst.OpenBalance + difference;
                        }
                        else if (difference < 0)
                        {
                            newOpenBalance = lst.OpenBalance - Math.Abs(difference);
                            closingBalance = lst.ClBalance - Math.Abs(difference);
                        }
                        else
                            break;
                    }
                    firstDayFlag = false;

                    //closingBalance = (input.OpenBalance + input.Received + input.ExcessReceived + input.Return +
                    //           input.TransferIn) -
                    //          (input.Issued + input.Sales + input.Damaged + input.SupplierReturn + input.Shortage +
                    //           input.TransferOut);

                    closingBalance = (newOpenBalance + lst.Received + lst.ExcessReceived + lst.Return + lst.TransferIn) - (lst.Issued + lst.Sales + lst.Damaged + lst.SupplierReturn + lst.Shortage + lst.TransferOut);

                    var ledDto = await _materialLedgerRepo.GetAsync(lst.Id);
                    ledDto.OpenBalance = newOpenBalance;
                    ledDto.ClBalance = closingBalance;
                    await _materialLedgerRepo.UpdateAsync(ledDto);
                    newOpenBalance = closingBalance;
                }
                #region UpdateCurrentStock_MaterialLocation

                //if (input.CanChangeDbValue == true)
                {
                    var matlocstock = await _materiallocationwisestockRepo.FirstOrDefaultAsync(t => t.LocationRefId == input.LocationRefId && t.MaterialRefId == ledger.MaterialRefId);
                    if (matlocstock == null)
                    {
                        throw new UserFriendlyException(L("MaterialLocationWiseStockError"));
                    }
                    matlocstock.CurrentInHand = closingBalance;
                    await _materiallocationwisestockRepo.UpdateAsync(matlocstock);
                }
                #endregion
            }
            #endregion

            GetVerifyMaterialLedgerDto output = new GetVerifyMaterialLedgerDto();
            output.OutputChangesList = OutputChangesList;

            return output;
        }



        public async Task<List<UnitConversionEditDto>> GetLinkedUnitCoversionForGivenSupplier_Material(SupplierMaterialEditDto input)
        {
            List<UnitConversionEditDto> returnUnitConversionList = new List<UnitConversionEditDto>();

            var supplierLinkedUcs = await _unitConversionVsSupplierRepo.GetAllListAsync(t => t.SupplierRefId == input.SupplierRefId);
            if (supplierLinkedUcs.Count == 0)
            {
                UnitConversionEditDto newUcDto = new UnitConversionEditDto();
                newUcDto.MaterialRefId = input.MaterialRefId;

                var otherSupplierUnitConversionForThisMaterialList = (from uc in _unitconversionRepo.GetAll().Where(t => t.MaterialRefId == input.Id)
                                                                      join baseUnit in _unitRepo.GetAll() on uc.BaseUnitId equals baseUnit.Id
                                                                      join refUnit in _unitRepo.GetAll() on uc.RefUnitId equals refUnit.Id
                                                                      select new UnitConversionEditDto
                                                                      {
                                                                          Id = uc.Id,
                                                                          BaseUnitId = uc.BaseUnitId,
                                                                          BaseUom = baseUnit.Name,
                                                                          RefUnitId = uc.RefUnitId,
                                                                          RefUom = refUnit.Name,
                                                                          Conversion = uc.Conversion,
                                                                          DecimalPlaceRounding = uc.DecimalPlaceRounding,
                                                                          MaterialRefId = uc.MaterialRefId,
                                                                          DoesUpdateRequired = false,
                                                                          CreatedBasedOnRecursive = uc.CreatedBasedOnRecursive,
                                                                      }).ToList();

                if (otherSupplierUnitConversionForThisMaterialList.Count > 0)
                {
                    List<int> ucIds = otherSupplierUnitConversionForThisMaterialList.Select(t => (int)t.Id).ToList();
                    var rsLinkedWithSupplier = await _unitConversionVsSupplierRepo.GetAll().Where(t => ucIds.Contains(t.UnitConversionRefId)).ToListAsync();
                    List<int> supIds = rsLinkedWithSupplier.Select(t => t.SupplierRefId).ToList();
                    var rsSuppliers = await _supplierRepo.GetAll().Where(t => supIds.Contains(t.Id)).ToListAsync();
                    newUcDto.LinkedSupplierList = rsSuppliers.MapTo<List<SimpleSupplierListDto>>();
                }
                returnUnitConversionList.Add(new UnitConversionEditDto { MaterialRefId = input.MaterialRefId });
            }
            else
            {
                List<int> linkedUcs = supplierLinkedUcs.Select(t => t.UnitConversionRefId).ToList();
                returnUnitConversionList = (from uc in _unitconversionRepo.GetAll().Where(t => t.MaterialRefId == input.Id && linkedUcs.Contains(t.Id))
                                            join baseUnit in _unitRepo.GetAll() on uc.BaseUnitId equals baseUnit.Id
                                            join refUnit in _unitRepo.GetAll() on uc.RefUnitId equals refUnit.Id
                                            select new UnitConversionEditDto
                                            {
                                                Id = uc.Id,
                                                BaseUnitId = uc.BaseUnitId,
                                                BaseUom = baseUnit.Name,
                                                RefUnitId = uc.RefUnitId,
                                                RefUom = refUnit.Name,
                                                Conversion = uc.Conversion,
                                                DecimalPlaceRounding = uc.DecimalPlaceRounding,
                                                MaterialRefId = uc.MaterialRefId,
                                                DoesUpdateRequired = false,
                                                CreatedBasedOnRecursive = uc.CreatedBasedOnRecursive,
                                            }).ToList();

                if (returnUnitConversionList.Count > 0)
                {
                    List<int> ucIds = returnUnitConversionList.Select(t => (int)t.Id).ToList();
                    var rsLinkedWithSupplier = await _unitConversionVsSupplierRepo.GetAll().Where(t => ucIds.Contains(t.UnitConversionRefId)).ToListAsync();
                    List<int> supIds = rsLinkedWithSupplier.Select(t => t.SupplierRefId).ToList();
                    var rsSuppliers = await _supplierRepo.GetAll().Where(t => supIds.Contains(t.Id)).ToListAsync();
                    foreach (var uc in returnUnitConversionList)
                    {
                        var defaultUc = await _unitconversionRepo.FirstOrDefaultAsync(t => t.BaseUnitId == uc.BaseUnitId && t.RefUnitId == uc.RefUnitId && t.MaterialRefId == null);
                        if (defaultUc != null)
                        {
                            uc.DefaultConversion = defaultUc.Conversion;
                            uc.DefaultDecimalPlaceRounding = defaultUc.DecimalPlaceRounding;
                        }

                        var linkedWithSupplier = rsLinkedWithSupplier.Where(t => t.UnitConversionRefId == uc.Id).ToList();
                        if (linkedWithSupplier.Count > 0)
                        {
                            supIds = linkedWithSupplier.Select(t => t.SupplierRefId).ToList();
                            uc.LinkedSupplierList = rsSuppliers.Where(t => supIds.Contains(t.Id)).ToList().MapTo<List<SimpleSupplierListDto>>();
                        }
                    }
                }
            }
            return returnUnitConversionList;
        }

        public async Task AutoDayClose(LocationInputDto locationInputDto)
        {
            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == locationInputDto.LocationRefId);

            //materialService.verifyWipeOutStockForCloseDay(
            //daycloseService.setCloseDay(
            //daycloseService.getDayClose(
            //daycloseService.getDayCloseStatus({
            //closingstockService.getClosingExists({

        }

        public async Task<List<UnitConversionEditDto>> GetLinkedUnitCoversionForGivenSupplier_Material_Units(Supplier_Material_Units input)
        {
            List<UnitConversionEditDto> returnUnitConversionList = new List<UnitConversionEditDto>();

            var supplierLinkedUcs = await _unitConversionVsSupplierRepo.GetAllListAsync(t => t.SupplierRefId == input.SupplierRefId);
            if (supplierLinkedUcs.Count == 0)
            {
                UnitConversionEditDto newUcDto = new UnitConversionEditDto();
                newUcDto.MaterialRefId = input.MaterialRefId;

                var otherSupplierUnitConversionForThisMaterialList = (from uc in _unitconversionRepo.GetAll().Where(t => t.MaterialRefId == input.MaterialRefId)
                                                                      join baseUnit in _unitRepo.GetAll() on uc.BaseUnitId equals baseUnit.Id
                                                                      join refUnit in _unitRepo.GetAll() on uc.RefUnitId equals refUnit.Id
                                                                      select new UnitConversionEditDto
                                                                      {
                                                                          Id = uc.Id,
                                                                          BaseUnitId = uc.BaseUnitId,
                                                                          BaseUom = baseUnit.Name,
                                                                          RefUnitId = uc.RefUnitId,
                                                                          RefUom = refUnit.Name,
                                                                          Conversion = uc.Conversion,
                                                                          DecimalPlaceRounding = uc.DecimalPlaceRounding,
                                                                          MaterialRefId = uc.MaterialRefId,
                                                                          DoesUpdateRequired = false,
                                                                          CreatedBasedOnRecursive = uc.CreatedBasedOnRecursive,
                                                                      }).ToList();

                if (otherSupplierUnitConversionForThisMaterialList.Count > 0)
                {
                    List<int> ucIds = otherSupplierUnitConversionForThisMaterialList.Select(t => (int)t.Id).ToList();
                    var rsLinkedWithSupplier = await _unitConversionVsSupplierRepo.GetAll().Where(t => ucIds.Contains(t.UnitConversionRefId)).ToListAsync();
                    List<int> supIds = rsLinkedWithSupplier.Select(t => t.SupplierRefId).ToList();
                    var rsSuppliers = await _supplierRepo.GetAll().Where(t => supIds.Contains(t.Id)).ToListAsync();
                    newUcDto.LinkedSupplierList = rsSuppliers.MapTo<List<SimpleSupplierListDto>>();
                }
                returnUnitConversionList.Add(new UnitConversionEditDto { MaterialRefId = input.MaterialRefId });
            }
            else
            {
                List<int> linkedUcs = supplierLinkedUcs.Select(t => t.UnitConversionRefId).ToList();
                returnUnitConversionList = (from uc in _unitconversionRepo.GetAll().Where(t => t.MaterialRefId == input.MaterialRefId && linkedUcs.Contains(t.Id))
                                            join baseUnit in _unitRepo.GetAll() on uc.BaseUnitId equals baseUnit.Id
                                            join refUnit in _unitRepo.GetAll() on uc.RefUnitId equals refUnit.Id
                                            select new UnitConversionEditDto
                                            {
                                                Id = uc.Id,
                                                BaseUnitId = uc.BaseUnitId,
                                                BaseUom = baseUnit.Name,
                                                RefUnitId = uc.RefUnitId,
                                                RefUom = refUnit.Name,
                                                Conversion = uc.Conversion,
                                                DecimalPlaceRounding = uc.DecimalPlaceRounding,
                                                MaterialRefId = uc.MaterialRefId,
                                                DoesUpdateRequired = false,
                                                CreatedBasedOnRecursive = uc.CreatedBasedOnRecursive,
                                            }).ToList();

                if (returnUnitConversionList.Count > 0)
                {
                    List<int> ucIds = returnUnitConversionList.Select(t => (int)t.Id).ToList();
                    var rsLinkedWithSupplier = await _unitConversionVsSupplierRepo.GetAll().Where(t => ucIds.Contains(t.UnitConversionRefId)).ToListAsync();
                    List<int> supIds = rsLinkedWithSupplier.Select(t => t.SupplierRefId).ToList();
                    var rsSuppliers = await _supplierRepo.GetAll().Where(t => supIds.Contains(t.Id)).ToListAsync();
                    foreach (var uc in returnUnitConversionList)
                    {
                        var defaultUc = await _unitconversionRepo.FirstOrDefaultAsync(t => t.BaseUnitId == uc.BaseUnitId && t.RefUnitId == uc.RefUnitId && t.MaterialRefId == null);
                        if (defaultUc != null)
                        {
                            uc.DefaultConversion = defaultUc.Conversion;
                            uc.DefaultDecimalPlaceRounding = defaultUc.DecimalPlaceRounding;
                        }

                        var linkedWithSupplier = rsLinkedWithSupplier.Where(t => t.UnitConversionRefId == uc.Id).ToList();
                        if (linkedWithSupplier.Count > 0)
                        {
                            supIds = linkedWithSupplier.Select(t => t.SupplierRefId).ToList();
                            uc.LinkedSupplierList = rsSuppliers.Where(t => supIds.Contains(t.Id)).ToList().MapTo<List<SimpleSupplierListDto>>();
                        }
                    }
                }
            }
            return returnUnitConversionList;
        }


        public async Task<UnitConversionListDto> GetConversion(GetConversionBasedOnBaseUnitRefUnit input)
        {
            UnitConversionListDto unitConversion;
            if (input.ConversionUnitList.Count == 0)
            {
                input.ConversionUnitList = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();
            }
            unitConversion = input.ConversionUnitList.FirstOrDefault(t => t.BaseUnitId == input.BaseUnitId && t.RefUnitId == input.RefUnitId && t.MaterialRefId == input.MaterialRefId); // Check for particular material
            if (unitConversion == null)
            {
                unitConversion = input.ConversionUnitList.FirstOrDefault(t => t.BaseUnitId == input.BaseUnitId && t.RefUnitId == input.RefUnitId && t.MaterialRefId == null); // Check for default conversion
                if (unitConversion != null)
                    unitConversion.DoesThisGlobalConversion = true;
            }
            else if (unitConversion != null)
            {
                unitConversion = null;
                var ucList = input.ConversionUnitList.Where(t => t.BaseUnitId == input.BaseUnitId && t.RefUnitId == input.RefUnitId && t.MaterialRefId == input.MaterialRefId).ToList();
                if (input.SupplierRefId.HasValue) // Check any particular supplierbased
                {
                    List<int> arrUcIds = ucList.Select(t => t.Id).ToList();
                    var linkedSupplierRefIds = await _unitConversionVsSupplierRepo.GetAllListAsync(t => arrUcIds.Contains(t.UnitConversionRefId) && t.SupplierRefId == input.SupplierRefId.Value);
                    if (linkedSupplierRefIds.Count > 0)
                    {
                        ucList = ucList.Where(t => t.Id == linkedSupplierRefIds.FirstOrDefault().UnitConversionRefId).ToList();
                        if (ucList.Count == 1)
                        {
                            unitConversion = ucList.FirstOrDefault();
                            unitConversion.DoesThisSupplierBasedConversion = true;
                        }
                    }
                    else // This supplier no more link, so take the default UC of material for this Supplier
                    {
                        var linkedSupplierOtherThanThisSupplier = await _unitConversionVsSupplierRepo.GetAllListAsync(t => arrUcIds.Contains(t.UnitConversionRefId));
                        List<int> otherSupplierLinkedUcIds = linkedSupplierOtherThanThisSupplier.Select(t => t.UnitConversionRefId).ToList();
                        ucList = ucList.Where(t => !otherSupplierLinkedUcIds.Contains(t.Id)).ToList();
                        if (ucList.Count == 1)
                        {
                            unitConversion = ucList.FirstOrDefault();
                        }
                        else if (ucList.Count == 0)
                        {
                            unitConversion = input.ConversionUnitList.FirstOrDefault(t => t.BaseUnitId == input.BaseUnitId && t.RefUnitId == input.RefUnitId && t.MaterialRefId == null); // Check for default conversion
                        }
                    }
                }
                if (ucList.Count > 1)
                {
                    var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == input.BaseUnitId);
                    var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == input.RefUnitId);
                    if (input.SupplierRefId.HasValue)
                    {
                        string supplierName = input.SupplierRefName;
                        if (supplierName.IsNullOrEmpty() && input.SupplierRefId.HasValue)
                        {
                            var supplier = await _supplierRepo.FirstOrDefaultAsync(t => t.Id == input.SupplierRefId);
                            supplierName = supplier.SupplierName;
                        }
                        string materialName = input.MateialRefName;
                        if (materialName.IsNullOrEmpty() && input.MaterialRefId.HasValue)
                        {
                            var material = await _materialRepo.FirstOrDefaultAsync(t => t.Id == input.MaterialRefId);
                            materialName = material.MaterialName;
                        }
                        throw new UserFriendlyException(L("MoreThanOneConversionFactorForSupplierMaterial", baseUnit.Name, refUnit.Name, materialName, supplierName));
                    }
                    else
                    {
                        List<int> arrUcIds = ucList.Select(t => t.Id).ToList();
                        var linkedSupplierOtherThanThisSupplier = await _unitConversionVsSupplierRepo.GetAllListAsync(t => arrUcIds.Contains(t.UnitConversionRefId));
                        List<int> otherSupplierLinkedUcIds = linkedSupplierOtherThanThisSupplier.Select(t => t.UnitConversionRefId).ToList();
                        ucList = ucList.Where(t => !otherSupplierLinkedUcIds.Contains(t.Id)).ToList();
                        if (ucList.Count == 1)
                        {
                            unitConversion = ucList.FirstOrDefault();
                        }
                        else if (ucList.Count == 0)
                        {
                            unitConversion = input.ConversionUnitList.FirstOrDefault(t => t.BaseUnitId == input.BaseUnitId && t.RefUnitId == input.RefUnitId && t.MaterialRefId == null); // Check for default conversion
                        }
                        else if (ucList.Count > 1)
                        {
                            string materialName = input.MateialRefName;
                            if (materialName.IsNullOrEmpty() && input.MaterialRefId.HasValue)
                            {
                                var material = await _materialRepo.FirstOrDefaultAsync(t => t.Id == input.MaterialRefId);
                                materialName = material.MaterialName;
                            }
                            throw new UserFriendlyException(L("MoreThanOneConversionFactorForMaterial", baseUnit.Name, refUnit.Name, materialName));
                        }
                    }
                    unitConversion = ucList.FirstOrDefault();
                }
            }
            return unitConversion;
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetInvoicePayModeEnumForCombobox()
        {
            var retList = new List<ComboboxItemDto>();

            string enumstring;
            var EnumValues = Enum.GetValues(typeof(InvoicePayModeEnum));
            foreach (int EnumValue in EnumValues)
            {
                enumstring = Enum.GetName(typeof(InvoicePayModeEnum), EnumValue);
                retList.Add(new ComboboxItemDto { Value = EnumValue.ToString(), DisplayText = enumstring });
            }

            return
                new ListResultDto<ComboboxItemDto>(
                    retList.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText)).ToList());
        }
    }
}