﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;


namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class MaterialRecipeTypesAppService : DineConnectAppServiceBase, IMaterialRecipeTypesAppService
    {

        private readonly IMaterialRecipeTypesListExcelExporter _materialrecipetypesExporter;
        private readonly IMaterialRecipeTypesManager _materialrecipetypesManager;
        private readonly IRepository<MaterialRecipeTypes> _materialrecipetypesRepo;

        public MaterialRecipeTypesAppService(IMaterialRecipeTypesManager materialrecipetypesManager,
            IRepository<MaterialRecipeTypes> materialRecipeTypesRepo,
            IMaterialRecipeTypesListExcelExporter materialrecipetypesExporter)
        {
            _materialrecipetypesManager = materialrecipetypesManager;
            _materialrecipetypesRepo = materialRecipeTypesRepo;
            _materialrecipetypesExporter = materialrecipetypesExporter;

        }

        public async Task<PagedResultOutput<MaterialRecipeTypesListDto>> GetAll(GetMaterialRecipeTypesInput input)
        {
            var allItems = _materialrecipetypesRepo.GetAll();
            if (input.Operation == "SEARCH")
            {
                allItems = _materialrecipetypesRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Id.Equals(input.Filter)
               );
            }
            else
            {
                allItems = _materialrecipetypesRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Id.ToString().Contains(input.Filter)
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<MaterialRecipeTypesListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<MaterialRecipeTypesListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _materialrecipetypesRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<MaterialRecipeTypesListDto>>();
            return _materialrecipetypesExporter.ExportToFile(allListDtos);
        }

        public async Task<GetMaterialRecipeTypesForEditOutput> GetMaterialRecipeTypesForEdit(NullableIdInput input)
        {
            MaterialRecipeTypesEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _materialrecipetypesRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<MaterialRecipeTypesEditDto>();
            }
            else
            {
                editDto = new MaterialRecipeTypesEditDto();
            }

            return new GetMaterialRecipeTypesForEditOutput
            {
                MaterialRecipeTypes = editDto
            };
        }

        public async Task CreateOrUpdateMaterialRecipeTypes(CreateOrUpdateMaterialRecipeTypesInput input)
        {
            if (input.MaterialRecipeTypes.Id.HasValue)
            {
                await UpdateMaterialRecipeTypes(input);
            }
            else
            {
                await CreateMaterialRecipeTypes(input);
            }
        }

        public async Task DeleteMaterialRecipeTypes(IdInput input)
        {
            await _materialrecipetypesRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateMaterialRecipeTypes(CreateOrUpdateMaterialRecipeTypesInput input)
        {
            var item = await _materialrecipetypesRepo.GetAsync(input.MaterialRecipeTypes.Id.Value);
            var dto = input.MaterialRecipeTypes;

            //TODO: SERVICE MaterialRecipeTypes Update Individually

            CheckErrors(await _materialrecipetypesManager.CreateSync(item));
        }

        protected virtual async Task CreateMaterialRecipeTypes(CreateOrUpdateMaterialRecipeTypesInput input)
        {
            var dto = input.MaterialRecipeTypes.MapTo<MaterialRecipeTypes>();

            CheckErrors(await _materialrecipetypesManager.CreateSync(dto));
        }

        public async Task<ListResultOutput<MaterialRecipeTypesListDto>> GetIds()
        {
            var lstMaterialRecipeTypes = await _materialrecipetypesRepo.GetAll().ToListAsync();
            return new ListResultOutput<MaterialRecipeTypesListDto>(lstMaterialRecipeTypes.MapTo<List<MaterialRecipeTypesListDto>>());
        }
    }
}
