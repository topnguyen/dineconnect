﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using System;
using System.Dynamic;
using Newtonsoft.Json.Linq;
using DinePlan.Infrastructure.Helpers;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Xml;
using System.Xml.Linq;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;

namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class CustomerTagMaterialPriceAppService : DineConnectAppServiceBase, ICustomerTagMaterialPriceAppService
    {

        private readonly ICustomerTagMaterialPriceListExcelExporter _customertagmaterialpriceExporter;
        private readonly ICustomerTagMaterialPriceManager _customertagmaterialpriceManager;
        private readonly IRepository<CustomerTagMaterialPrice> _customertagmaterialpriceRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<CustomerTagDefinition> _customertagdefinitionRepo;


        public CustomerTagMaterialPriceAppService(ICustomerTagMaterialPriceManager customertagmaterialpriceManager,
            IRepository<CustomerTagMaterialPrice> customerTagMaterialPriceRepo,
            ICustomerTagMaterialPriceListExcelExporter customertagmaterialpriceExporter
            , IRepository<Material> materialRepo,
            IRepository<CustomerTagDefinition> customertagdefinitionRepo)
        {
            _customertagmaterialpriceManager = customertagmaterialpriceManager;
            _customertagmaterialpriceRepo = customerTagMaterialPriceRepo;
            _customertagmaterialpriceExporter = customertagmaterialpriceExporter;
            _materialRepo = materialRepo;
            _customertagdefinitionRepo = customertagdefinitionRepo;

        }

        public async Task<PagedResultOutput<CustomerTagMaterialPriceListDto>> GetAll(GetCustomerTagMaterialPriceInput input)
        {
            var allItems = _customertagmaterialpriceRepo.GetAll();
            if (input.Operation == "SEARCH")
            {
                allItems = _customertagmaterialpriceRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.TagCode.Equals(input.Filter)
               );
            }
            else
            {
                allItems = _customertagmaterialpriceRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.TagCode.Contains(input.Filter)
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<CustomerTagMaterialPriceListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<CustomerTagMaterialPriceListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _customertagmaterialpriceRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<CustomerTagMaterialPriceListDto>>();
            return _customertagmaterialpriceExporter.ExportToFile(allListDtos);
        }

        public async Task<GetCustomerTagMaterialPriceForEditOutput> GetCustomerTagMaterialPriceForEdit(NullableIdInput input)
        {
            CustomerTagMaterialPriceEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _customertagmaterialpriceRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<CustomerTagMaterialPriceEditDto>();
            }
            else
            {
                editDto = new CustomerTagMaterialPriceEditDto();
            }

            return new GetCustomerTagMaterialPriceForEditOutput
            {
                CustomerTagMaterialPrice = editDto
            };
        }

        public async Task CreateOrUpdateCustomerTagMaterialPrice(CreateOrUpdateCustomerTagMaterialPriceInput input)
        {
            if (input.CustomerTagMaterialPrice.Id.HasValue)
            {
                await UpdateCustomerTagMaterialPrice(input);
            }
            else
            {
                await CreateCustomerTagMaterialPrice(input);
            }
        }

        public async Task DeleteCustomerTagMaterialPrice(IdInput input)
        {
            await _customertagmaterialpriceRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateCustomerTagMaterialPrice(CreateOrUpdateCustomerTagMaterialPriceInput input)
        {
            var item = await _customertagmaterialpriceRepo.GetAsync(input.CustomerTagMaterialPrice.Id.Value);
            var dto = input.CustomerTagMaterialPrice;

            //TODO: SERVICE CustomerTagMaterialPrice Update Individually

            CheckErrors(await _customertagmaterialpriceManager.CreateSync(item));
        }

        protected virtual async Task CreateCustomerTagMaterialPrice(CreateOrUpdateCustomerTagMaterialPriceInput input)
        {
            var dto = input.CustomerTagMaterialPrice.MapTo<CustomerTagMaterialPrice>();

            CheckErrors(await _customertagmaterialpriceManager.CreateSync(dto));
        }

        public async Task<ListResultOutput<CustomerTagMaterialPriceListDto>> GetIds()
        {
            var lstCustomerTagMaterialPrice = await _customertagmaterialpriceRepo.GetAll().ToListAsync();
            return new ListResultOutput<CustomerTagMaterialPriceListDto>(lstCustomerTagMaterialPrice.MapTo<List<CustomerTagMaterialPriceListDto>>());
        }
        public async Task<List<CustomerTagMaterialPriceViewEditDto>> GetMaterialPriceForEdit(GetCustomerTagMaterialPriceInput input)
        {
            var pricetagList = await _customertagdefinitionRepo.GetAll().ToListAsync();

            var matList = await _materialRepo.GetAll().ToListAsync();
            List<CustomerTagMaterialPriceViewEditDto> retList = new List<CustomerTagMaterialPriceViewEditDto>();

            List<string> headerList = new List<string>();

            headerList.Add("MaterialRefId");
            headerList.Add("MaterialRefName");
            foreach (var tag in pricetagList)
            {
                headerList.Add(tag.TagCode);
            }

            string[] arrayHeader = headerList.ToArray();

            string[,] arrayData;
            int arrayLength = matList.Count();
            int arrayWidth = arrayHeader.Length;

            arrayData = new string[arrayLength, arrayWidth];

            List<string> dataList = new List<string>();

            int loopcnt = 0;

            foreach (var mat in matList)
            {

                CustomerTagMaterialPriceViewEditDto newAddChild = new CustomerTagMaterialPriceViewEditDto();

                newAddChild.MaterialRefId = mat.Id;
                newAddChild.MaterialRefName = mat.MaterialName;

                arrayData[loopcnt, 0] = mat.Id.ToString();
                arrayData[loopcnt, 1] = mat.MaterialName;


                dataList.Add(mat.Id.ToString());
                dataList.Add(mat.MaterialName);

                List<TagWithPrice> tagpricelist = new List<TagWithPrice>();
                List<string> tagList = new List<string>();

                int tagLoopCnt = 2;

                foreach (var tag in pricetagList)
                {
                    var customertagprice = _customertagmaterialpriceRepo.GetAll().Where(a => a.TagCode.Equals(tag.TagCode) && a.MaterialRefId == mat.Id).ToList();

                    TagWithPrice newTag = new TagWithPrice();

                    if (customertagprice.Count() == 0)
                    {
                        newTag.TagCode = tag.TagCode;
                        newTag.TagPrice = 0M;
                    }
                    else
                    {
                        newTag.TagCode = tag.TagCode;
                        newTag.TagPrice = customertagprice[0].MaterialPrice;
                    }

                    tagpricelist.Add(newTag);
                    tagList.Add(tag.TagCode);

                    dataList.Add(newTag.TagPrice.ToString());
                    arrayData[loopcnt, tagLoopCnt] = newTag.TagPrice.ToString();
                    tagLoopCnt++;
                }

                newAddChild.TagWithPrice = tagpricelist;
                newAddChild.TagList = tagList;

                retList.Add(newAddChild);


                loopcnt++;
            }
            return retList;
        }

        public async Task<CustomerTagMaterialPriceArrayDto> GetMaterialPriceForEditInArray(GetCustomerTagMaterialPriceInput input)
        {
            var pricetagList = await _customertagdefinitionRepo.GetAll().ToListAsync();

            var matList = await _materialRepo.GetAll().ToListAsync();
            List<CustomerTagMaterialPriceViewEditDto> retList = new List<CustomerTagMaterialPriceViewEditDto>();

            CustomerTagMaterialPriceArrayDto ret = new CustomerTagMaterialPriceArrayDto();

            dynamic dynTagList = new DynamicDictionary();



            List<string> headerList = new List<string>();

            headerList.Add("MaterialRefId");
            headerList.Add("MaterialRefName");
            foreach (var tag in pricetagList)
            {
                headerList.Add(tag.TagCode);
            }

            string[] arrayHeader = headerList.ToArray();

            ret.HeaderData = arrayHeader;


            string[,] arrayData;
            int arrayLength = matList.Count();
            int arrayWidth = arrayHeader.Length;

            arrayData = new string[arrayLength, arrayWidth];

            List<string> dataList = new List<string>();

            int loopcnt = 0;

            foreach (var mat in matList)
            {

                CustomerTagMaterialPriceViewEditDto newAddChild = new CustomerTagMaterialPriceViewEditDto();

                newAddChild.MaterialRefId = mat.Id;
                newAddChild.MaterialRefName = mat.MaterialName;

                arrayData[loopcnt, 0] = mat.Id.ToString();
                arrayData[loopcnt, 1] = mat.MaterialName;

                dynTagList.MaterialRefId = mat.Id;
                dynTagList.MaterialRefName = mat.MaterialName;


                dataList.Add(mat.Id.ToString());
                dataList.Add(mat.MaterialName);

                List<TagWithPrice> tagpricelist = new List<TagWithPrice>();
                List<string> tagList = new List<string>();

                int tagLoopCnt = 2;

                foreach (var tag in pricetagList)
                {
                    var customertagprice = _customertagmaterialpriceRepo.GetAll().Where(a => a.TagCode.Equals(tag.TagCode) && a.MaterialRefId == mat.Id).ToList();

                    TagWithPrice newTag = new TagWithPrice();

                    if (customertagprice.Count() == 0)
                    {
                        newTag.TagCode = tag.TagCode;
                        newTag.TagPrice = 0M;
                    }
                    else
                    {
                        newTag.TagCode = tag.TagCode;
                        newTag.TagPrice = customertagprice[0].MaterialPrice;
                    }

                    tagpricelist.Add(newTag);
                    tagList.Add(tag.TagCode);

                    dataList.Add(newTag.TagPrice.ToString());
                    arrayData[loopcnt, tagLoopCnt] = newTag.TagPrice.ToString();
                    tagLoopCnt++;
                }

                newAddChild.TagWithPrice = tagpricelist;
                newAddChild.TagList = tagList;

                retList.Add(newAddChild);


                loopcnt++;
            }

            ret.DetailData = arrayData;


            return ret;
        }

        public async Task<List<CustomerTagMaterialPriceEditDto>> GetMaterialPriceForGivenTag(GetCustomerTagMaterialPriceInput input)
        {
            List<CustomerTagMaterialPriceEditDto> returnList = new List<CustomerTagMaterialPriceEditDto>();
            CustomerTagDefinition priceTag;
            if (input.SelectedPriceTagForEdit == null || input.SelectedPriceTagForEdit.Length == 0)
            {
                return new List<CustomerTagMaterialPriceEditDto>();
            }
            else
            {
                priceTag = await _customertagdefinitionRepo.FirstOrDefaultAsync(t => t.TagCode.Equals(input.SelectedPriceTagForEdit));
            }
            //var matList = await _materialRepo.GetAll().ToListAsync();

            var allItems =await (from material in _materialRepo.GetAll().WhereIf(!input.Filter.IsNullOrEmpty(),t=>t.MaterialName.Contains(input.Filter) || t.MaterialPetName.Contains(input.Filter))
                            join ctm in _customertagmaterialpriceRepo.GetAll()
                                .Where(t => t.TagCode.Equals(priceTag.TagCode)) on material.Id equals ctm.MaterialRefId into ctmGroup
                            from ct in ctmGroup.DefaultIfEmpty()
                            select new CustomerTagMaterialPriceEditDto
                            {
                                Id = ct.Id,
                                MaterialRefId = material.Id,
                                MaterialRefName = material.MaterialName,
                                MaterialPrice = ct.MaterialPrice,
                                AvgRateBasedPrice = ct.AvgRateBasedPrice,
                                MarginPercentage = ct.MarginPercentage,
                                MinimumPrice = ct.MinimumPrice,
                                TagCode = ct.TagCode
                            }).ToListAsync();
            return allItems;
        }

        public async Task UpdateCustomerMaterialPriceForGivenTag(CreateOrUpdateCustomerTagPriceDefinitionInput inputDto)
        {

            var input = inputDto.CustomerTagMaterialPriceEditDto;

            var exists = await _customertagmaterialpriceRepo.FirstOrDefaultAsync(t => t.TagCode == input.TagCode && t.MaterialRefId == input.MaterialRefId);
            if (exists != null)
            {
                exists.MaterialPrice = input.MaterialPrice;
                exists.AvgRateBasedPrice = input.AvgRateBasedPrice;
                exists.MinimumPrice = input.MinimumPrice;
                exists.MarginPercentage = input.MarginPercentage;
                await _customertagmaterialpriceRepo.UpdateAsync(exists);
            }
            else
            {
                CustomerTagMaterialPrice customerTagMaterialPrice = new CustomerTagMaterialPrice
                {
                    MaterialRefId = input.MaterialRefId,
                    MaterialPrice = input.MaterialPrice,
                    MarginPercentage = input.MarginPercentage,
                    TagCode = input.TagCode,
                    AvgRateBasedPrice = input.AvgRateBasedPrice,
                    MinimumPrice = input.MinimumPrice,
                };
                await _customertagmaterialpriceRepo.InsertOrUpdateAndGetIdAsync(customerTagMaterialPrice);

            }
        }

        public async Task<GetTagMaterialDynamic> GetMaterialPriceDynamic(GetCustomerTagMaterialPriceInput input)
        {
            List<CustomerTagDefinition> pricetagList;
            if (input.SelectedPriceTagForEdit == null || input.SelectedPriceTagForEdit.Length == 0)
            {
                pricetagList = await _customertagdefinitionRepo.GetAll().ToListAsync();
            }
            else
            {
                pricetagList = await _customertagdefinitionRepo.GetAll().Where(t => t.TagCode.Equals(input.SelectedPriceTagForEdit)).ToListAsync();
            }
            var matList = await _materialRepo.GetAll().ToListAsync();

            List<dynamic> TagListDynamic = new List<dynamic>();

            int loopcnt = 0;

            foreach (var mat in matList)
            {
                dynamic dynTagList = new ExpandoObject();

                dynTagList.MaterialRefId = mat.Id;
                dynTagList.MaterialRefName = mat.MaterialName;

                foreach (var tag in pricetagList)
                {
                    var customertagprice = _customertagmaterialpriceRepo.GetAll().Where(ctm => ctm.TagCode.Equals(tag.TagCode) && ctm.MaterialRefId == mat.Id).ToList();

                    TagWithPrice newTag = new TagWithPrice();

                    string tagValueAsColumnValue = tag.TagCode;

                    if (input.SelectedPriceTagForEdit != null)
                        tagValueAsColumnValue = "customerPrice";

                    if (customertagprice.Count() == 0)
                    {
                        newTag.TagCode = tagValueAsColumnValue;
                        newTag.TagPrice = 0M;
                    }
                    else
                    {
                        newTag.TagCode = tagValueAsColumnValue;
                        newTag.TagPrice = customertagprice[0].MaterialPrice;
                    }

                    string dynColName = tagValueAsColumnValue.Replace(" ", "_");


                    ((IDictionary<string, Object>)dynTagList).Add(dynColName, newTag.TagPrice);

                }

                loopcnt++;
                TagListDynamic.Add(dynTagList);

            }

            List<dynamic> colDefList = new List<dynamic>();



            dynamic colDef = new ExpandoObject();

            ((IDictionary<string, Object>)colDef).Add("name", "MaterialName");
            ((IDictionary<string, Object>)colDef).Add("field", "materialRefName");
            //((IDictionary<string, Object>)colDef).Add("width", "340");
            colDefList.Add(colDef);

            string matColDefn = "{name: app.localize('MaterialName'),field: 'materialRefName',width:340},";
            //string editColDefn = " {name: app.localize('Edit'),width: 90,cellTemplate: '<div class=\"ui-grid-cell-contents text-center\">' +             '  <button ng-click=\"grid.appScope.editEntry(row.entity)\" class=\"btn btn-default btn-xs\" title=\"' + app.localize('Edit') + '\"><i class=\"fa fa-edit\"></i></button>' + '</div>'},";

            string priceColsTemplate;

            if (input.SelectedPriceTagForEdit == null)
            {
                priceColsTemplate = "{name: app.localize('dyndisplayname'),field: 'dynfieldname'},";

            }
            else
            {
                priceColsTemplate = "{name: app.localize('dyndisplayname'),field: 'customerPrice'},";
            }

            string colsDefs = "";
            foreach (var tag in pricetagList)
            {
                string tagCodedisplay = tag.TagCode.Replace(" ", "_");
                string tagcamelcase = tagCodedisplay.ToLower();

                if (input.SelectedPriceTagForEdit != null)
                    tagcamelcase = "customerPrice";

                string defn = priceColsTemplate.Replace("dyndisplayname", tagCodedisplay);
                defn = priceColsTemplate.Replace("dynfieldname", tagcamelcase);
                colsDefs = colsDefs + " " + defn;

                dynamic colDefdyn = new ExpandoObject();

                ((IDictionary<string, Object>)colDefdyn).Add("name", tagCodedisplay);
                ((IDictionary<string, Object>)colDefdyn).Add("field", tagcamelcase);
                //((IDictionary<string, Object>)colDefdyn).Add("width", "150");
                //   ((IDictionary<string, Object>)colDefdyn).Add("cellClass", "ui-ralign");
                //((IDictionary<string, Object>)colDefdyn).Add("enableCellEdit", true);
                colDefList.Add(colDefdyn);

            }

            dynamic colDefedit = new ExpandoObject();
            ((IDictionary<string, Object>)colDefedit).Add("name", "Edit");

            //((IDictionary<string, Object>)colDefedit).Add("type", "string");
            ((IDictionary<string, Object>)colDefedit).Add("cellTemplate", "<div class=\"ui-grid-cell-contents text-center\">  <button ng-click=\"grid.appScope.openTextEditModal(row.entity)\" class=\"btn btn-default btn-xs\" title=\"Edit\"><i class=\"fa fa-edit\"></i></button></div>");
            if (input.SelectedPriceTagForEdit == null)
            {
                ((IDictionary<string, Object>)colDefedit).Add("width", "0");
                //  colDefList.Add(colDefedit);
            }
            else
            {
                //((IDictionary<string, Object>)colDefedit).Add("width", "90");
                colDefList.Add(colDefedit);
            }



            GetTagMaterialDynamic retDyn = new GetTagMaterialDynamic();
            retDyn.DynamicTagList = TagListDynamic;
            retDyn.ColumnDefList = colDefList;

            return retDyn;

        }

        public async Task UpdateCustomerMaterialPrice(GetTagMaterialDynamic input)
        {
            List<CustomerTagDefinition> pricetagList;

            if (input.TagToBeUpdate.Length > 0)
            {
                pricetagList = await _customertagdefinitionRepo.GetAll().Where(t => t.TagCode.Equals(input.TagToBeUpdate)).ToListAsync();
            }
            else
            {
                pricetagList = await _customertagdefinitionRepo.GetAll().ToListAsync();

            }
            List<string> tagListForSearch = new List<string>();
            foreach (var tag in pricetagList)
            {
                string dynColName = tag.TagCode.Replace(" ", "_");
                dynColName = dynColName.ToUpperInvariant();
                tagListForSearch.Add(dynColName);
            }

            var datalistforedit = input.DynamicTagList;




            foreach (var data in datalistforedit)
            {
                string materialWithPrice = data.ToString();
                int materialRefid = 0;
                string materialRefName = "";
                decimal materialPrice = 0;
                bool avgRateBasedPrice = false;
                decimal marginPercentage = 0;
                decimal minimumPrice = 0;

                if (input.TagToBeUpdate.Length > 0)
                {
                    materialWithPrice = materialWithPrice.Replace("customerPrice", input.TagToBeUpdate);
                }

                var xml = XDocument.Load(JsonReaderWriterFactory.CreateJsonReader(
                    new MemoryStream(Encoding.ASCII.GetBytes(materialWithPrice)),
                    new XmlDictionaryReaderQuotas()));

                List<XElement> elementList = xml.Descendants().ToList();
                List<XNode> nodeList = xml.DescendantNodes().ToList();



                foreach (XElement element in xml.Descendants())
                {
                    Console.WriteLine(element);
                    Console.WriteLine(element.Name);
                    Console.WriteLine(element.Value);

                    string columnname = element.Name.ToString();
                    columnname = columnname.Replace("{", "");
                    columnname = columnname.Replace("}", "");
                    columnname = columnname.ToUpperInvariant();

                    string columnvalue = element.Value.ToString();

                    switch (columnname.ToUpperInvariant())
                    {
                        case "MATERIALREFID":
                            materialRefid = Int32.Parse(element.Value);
                            break;
                        case "MATERIALREFNAME":
                            materialRefName = element.Value;
                            break;
                        case "REVISEDPRICE":
                            bool isNumerical = decimal.TryParse(columnvalue, out materialPrice);
                            break;
                        case "AVGRATEBASEDPRICE":
                            if (element.Value.ToUpperInvariant().Equals("TRUE"))
                                avgRateBasedPrice = true;
                            else
                                avgRateBasedPrice = false;
                            break;
                        case "MARGINPERCENTAGE":
                            bool isNumericalMarginPercentage = decimal.TryParse(columnvalue, out marginPercentage);
                            break;
                        case "MINIMUMPRICE":
                            bool isNumericalMinimumPrice = decimal.TryParse(columnvalue, out minimumPrice);
                            break;
                    }
                }
                {
                    if (materialPrice < 0)
                    {
                        throw new UserFriendlyException(L("NegativePriceNotAccepted", materialRefName, materialPrice));
                    }
                    var lst = _customertagmaterialpriceRepo.FirstOrDefault(c => c.MaterialRefId == materialRefid && c.TagCode.ToUpper().Equals(input.TagToBeUpdate.ToUpper()));

                    if (lst != null)
                    {
                        if (lst.MaterialPrice != materialPrice || lst.AvgRateBasedPrice != avgRateBasedPrice || lst.MinimumPrice != minimumPrice || lst.MarginPercentage != marginPercentage)
                        {
                            var editDto = await _customertagmaterialpriceRepo.GetAsync(lst.Id);
                            editDto.MaterialPrice = materialPrice;
                            editDto.AvgRateBasedPrice = avgRateBasedPrice;
                            editDto.MinimumPrice = minimumPrice;
                            editDto.MarginPercentage = marginPercentage;
                            await _customertagmaterialpriceRepo.InsertOrUpdateAndGetIdAsync(editDto);
                        }
                    }
                }

            }



            //string aaa = " {{  \"materialRefId\": 1,  \"materialRefName\": \"ALL PURPOSE FLOUR\",  \"default\": 85,  \"catering\": 0,  \"premium\": 0,  \"golD_CLASS\": 0,  \"$$hashKey\": \"uiGrid-000H\"}}";

            //foreach (var tag in pricetagList)
            //    {
            //        var customertagprice = _customertagmaterialpriceRepo.GetAll().Where(ctm => ctm.TagCode.Equals(tag.TagCode) && ctm.MaterialRefId == mat.Id).ToList();

            //        TagWithPrice newTag = new TagWithPrice();

            //        if (customertagprice.Count() == 0)
            //        {
            //            newTag.TagCode = tag.TagCode;
            //            newTag.TagPrice = 0M;
            //        }
            //        else
            //        {
            //            newTag.TagCode = tag.TagCode;
            //            newTag.TagPrice = customertagprice[0].MaterialPrice;
            //        }

            //        string dynColName = tag.TagCode.Replace(" ", "_");

            //        ((IDictionary<string, Object>)dynTagList).Add(dynColName, newTag.TagPrice);

            //    }

            //    loopcnt++;
            //    TagListDynamic.Add(dynTagList);
            //    KeyList = ((IDictionary<string, Object>)dynTagList).Keys;

            //}

            //GetTagMaterialDynamic retDyn = new GetTagMaterialDynamic();
            //retDyn.DynamicTagList = TagListDynamic;




            //       dynamic getd = new ExpandoObject();

            //       getd = datalistforedit[0];

            //       ICollection<string> icoll = getd.Values;

            //       dynamic stuff = JObject.Parse("{ 'Name': 'Jon Smith', 'Address': { 'City': 'New York', 'State': 'NY' }, 'Age': 42 }");

            //       string name = stuff.Name;
            //       string address = stuff.Address.City;




            //       foreach (XNode node in xml.DescendantNodes())
            //       {
            //           Console.WriteLine(node);
            //           Console.WriteLine(node.ToString());
            //           Console.WriteLine(node);
            //       }

            //       XmlDocument doc = new XmlDocument();
            ////       xml.DescendantNodes();



            //      XmlNodeList nodelist = xml.DescendantNodes().ToList();


            // dynamic expObj = new ExpandoObject();
            //expObj = karanish;



            //// print the dynamically added properties
            //foreach (KeyValuePair<string, object> kvp in karanish) // enumerating over it exposes the Properties and Values as a KeyValuePair
            //    Console.WriteLine("{0} = {1}", kvp.Key, kvp.Value);

            //// cast to an IDictionary<string, object>
            //IDictionary<string, object> expDict = expObj;

            //// add a new property via the dictionary reference
            //expDict["Foo"] = "Bar";

            //// verify it's been added on the original dynamic reference
            //Console.WriteLine(expObj.Foo);

            ////using System.Web.Script.Serialization;
            //JavaScriptSerializer jss = new JavaScriptSerializer();
            //var d = jss.Deserialize<dynamic>(getd);


            ////var serializer = new JavaScriptSerializer();
            ////serializer.RegisterConverters(new[] { new DynamicJsonConverter() });

            //dynamic obj = serializer.Deserialize(getd, typeof(object));

            //((ICollection<KeyValuePair<string, Object>>)getd).CopyTo(KeyValuePair < string, Object >[] array, int arrayIndex)  );


            //KeyList = ((IDictionary<string, Object>)getd).Keys;



            //foreach( )
            //getd = datalistforedit[0];

            //var jsparsed = JsonConvert.DeserializeObject<T>(getd);

            //var TESTJSON = JsonConvert.DeserializeObject<dynamic>(datalistforedit[0]);

            //   string aaa = " {{  \"materialRefId\": 1,  \"materialRefName\": \"ALL PURPOSE FLOUR\",  \"default\": 85,  \"catering\": 0,  \"premium\": 0,  \"golD_CLASS\": 0,  \"$$hashKey\": \"uiGrid-000H\"}}";


            //   string bbb = " {\"MaterialTag\":{  \"materialRefId\": 1,  \"materialRefName\": \"ALL PURPOSE FLOUR\",  \"default\": 85,  \"catering\": 0,  \"premium\": 0,  \"golD_CLASS\": 0,  \"$$hashKey\": \"uiGrid-000H\"}}";

            //   var serializer1 = new JavaScriptSerializer(); //using System.Web.Script.Serialization;

            ////   Dictionary<string, string> bbbvalues = serializer.Deserialize<Dictionary<string, string>>(bbb);
            //   Dictionary<string, string> aaavalues = serializer1.Deserialize<Dictionary<string, string>>(aaa);
            //   Dictionary<string, string> getvalllues = serializer1.Deserialize<Dictionary<string, string>>(getd);

            //   //try
            //{
            //    var jsparsed1 = JsonConvert.DeserializeObject(bbb);
            //    var jsparsed2 = JsonConvert.DeserializeObject(aaa);
            //}
            //catch (Exception ex)
            //{
            //    string errmsg = ex.Message + ex.InnerException;
            //    throw;
            //}

            //    Dictionary<string, object> myDictionary = new Dictionary<string, object>();

            //foreach (JObject content in getd.Children<JObject>())
            //{
            //    foreach (JProperty prop in content.Properties())
            //    {
            //        myDictionary.Add(prop.Name, prop.Value);
            //    }
            //}

            //ICollection<string> KeyList = ((IDictionary<string, Object>)getd).Keys;
            //Newtonsoft.Json.Linq.JObject outputvalue;

            //foreach (var key in KeyList)
            //{
            //    Newtonsoft.Json.Linq.JObject a;

            //    bool isRetKey = ((IDictionary<string, Newtonsoft.Json.Linq.JObject>)getd).TryGetValue(key, out outputvalue);

            //}


            //return retDyn;

        }
    }


    // The class derived from DynamicObject.
    public class DynamicDictionary : DynamicObject
    {
        // The inner dictionary.
        Dictionary<string, object> dictionary
            = new Dictionary<string, object>();


        // This property returns the number of elements
        // in the inner dictionary.
        public int Count
        {
            get
            {
                return dictionary.Count;
            }
        }

        // If you try to get a value of a property 
        // not defined in the class, this method is called.
        public override bool TryGetMember(
            GetMemberBinder binder, out object result)
        {
            // Converting the property name to lowercase
            // so that property names become case-insensitive.
            string name = binder.Name.ToLower();

            // If the property name is found in a dictionary,
            // set the result parameter to the property value and return true.
            // Otherwise, return false.
            return dictionary.TryGetValue(name, out result);
        }

        // If you try to set a value of a property that is
        // not defined in the class, this method is called.
        public override bool TrySetMember(
            SetMemberBinder binder, object value)
        {
            // Converting the property name to lowercase
            // so that property names become case-insensitive.
            dictionary[binder.Name.ToLower()] = value;

            // You can always add a value to a dictionary,
            // so this method always returns true.
            return true;
        }
    }

}

