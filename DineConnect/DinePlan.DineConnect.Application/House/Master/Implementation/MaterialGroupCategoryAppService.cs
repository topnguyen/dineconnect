﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;


namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class MaterialGroupCategoryAppService : DineConnectAppServiceBase, IMaterialGroupCategoryAppService
    {

        private readonly IMaterialGroupCategoryListExcelExporter _materialgroupcategoryExporter;
        private readonly IRepository<MaterialGroupCategory> _materialgroupcategoryRepo;
        private readonly IRepository<MaterialGroup> _materialgroupRepo;
        private readonly IMaterialGroupAppService _materialgroupAppService;
        private readonly IRepository<Material> _materialRepo;
        public MaterialGroupCategoryAppService(
            IRepository<MaterialGroupCategory> materialGroupCategoryRepo,
            IMaterialGroupCategoryListExcelExporter materialgroupcategoryExporter
             ,IRepository<MaterialGroup> materialgroupManager,
            IMaterialGroupAppService materialgroupAppService,
            IRepository<Material> materialRepo)
        {
            _materialgroupcategoryRepo = materialGroupCategoryRepo;
            _materialgroupcategoryExporter = materialgroupcategoryExporter;
            _materialgroupRepo = materialgroupManager;
            _materialgroupAppService = materialgroupAppService;
            _materialRepo = materialRepo;
        }

        public async Task<PagedResultOutput<MaterialGroupCategoryListDto>> GetAll(GetMaterialGroupCategoryInput input)
        {
            IQueryable<MaterialGroupCategoryListDto> allItems;


            if (!string.IsNullOrEmpty(input.Operation) && input.Operation.Equals("SEARCH"))
            {
               allItems =  (from matgroupcategory in _materialgroupcategoryRepo.GetAll().WhereIf(!input.Filter.IsNullOrEmpty(), p => p.MaterialGroupCategoryName.Equals(input.Filter))  
                 select new MaterialGroupCategoryListDto
                 {
                     Id = matgroupcategory.Id,
                     MaterialGroupCategoryName = matgroupcategory.MaterialGroupCategoryName,
                     MaterialGroupRefId = matgroupcategory.MaterialGroupRefId,
					 LastModificationTime = matgroupcategory.LastModificationTime == null ? matgroupcategory.CreationTime : matgroupcategory.LastModificationTime
				 });
            }
            else
            {
                allItems = (from matgroupcategory in _materialgroupcategoryRepo.GetAll().WhereIf(!input.Filter.IsNullOrEmpty(), p => p.MaterialGroupCategoryName.Contains(input.Filter))
                            select new MaterialGroupCategoryListDto
                            {
                                Id = matgroupcategory.Id,
                                MaterialGroupCategoryName = matgroupcategory.MaterialGroupCategoryName,
                                MaterialGroupRefId = matgroupcategory.MaterialGroupRefId
                            });
            }

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<MaterialGroupCategoryListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<MaterialGroupCategoryListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
           var allItems = await (from matgroupcategory in _materialgroupcategoryRepo.GetAll() 
                        join matgroup in _materialgroupRepo.GetAll().Where(t => t.TenantId == AbpSession.TenantId)
                        on matgroupcategory.MaterialGroupRefId equals matgroup.Id
                        select new MaterialGroupCategoryListDto
                        {
                            Id = matgroupcategory.Id,
                            MaterialGroupCategoryName = matgroupcategory.MaterialGroupCategoryName,
                            MaterialGroupRefId = matgroupcategory.MaterialGroupRefId
                        }).ToListAsync();
            var allListDtos = allItems.MapTo<List<MaterialGroupCategoryListDto>>();
            return _materialgroupcategoryExporter.ExportToFile(allListDtos);
        }

        public async Task<GetMaterialGroupCategoryForEditOutput> GetMaterialGroupCategoryForEdit(NullableIdInput input)
        {
            MaterialGroupCategoryEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _materialgroupcategoryRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<MaterialGroupCategoryEditDto>();
            }
            else
            {
                editDto = new MaterialGroupCategoryEditDto();
            }

            return new GetMaterialGroupCategoryForEditOutput
            {
                MaterialGroupCategory = editDto
            };
        }

        public async Task<IdInput> CreateOrUpdateMaterialGroupCategory(CreateOrUpdateMaterialGroupCategoryInput input)
        {
            

            if (input.MaterialGroupCategory.Id.HasValue)
            {
                return await UpdateMaterialGroupCategory(input);
            }
            else
            {
                return await CreateMaterialGroupCategory(input);
            }
        }

        public async Task DeleteMaterialGroupCategory(IdInput input)
        {
            var rsMaterialGroupCategory = await _materialRepo.FirstOrDefaultAsync(t => t.MaterialGroupCategoryRefId == input.Id);

            if (rsMaterialGroupCategory == null)
                await _materialgroupcategoryRepo.DeleteAsync(input.Id);
            else
                throw new UserFriendlyException(L("ReferenceExists"));

        }

        protected virtual async Task<IdInput> UpdateMaterialGroupCategory(CreateOrUpdateMaterialGroupCategoryInput input)
        {
            var item = await _materialgroupcategoryRepo.GetAsync(input.MaterialGroupCategory.Id.Value);
            var dto = input.MaterialGroupCategory;

            var result = await (from matgroupcat in _materialgroupcategoryRepo.GetAll().Where(t => t.MaterialGroupCategoryName.Equals(dto.MaterialGroupCategoryName) && t.Id!=item.Id )
                                join matgroup in _materialgroupRepo.GetAll().Where(t => t.TenantId == AbpSession.TenantId)
                                on matgroupcat.MaterialGroupRefId equals matgroup.Id
                                select new MaterialGroupCategoryListDto
                                {
                                    Id = matgroupcat.Id,
                                    MaterialGroupRefId = matgroupcat.MaterialGroupRefId,
                                    MaterialGroupCategoryName = matgroupcat.MaterialGroupCategoryName,
                                }).FirstOrDefaultAsync();

            if (result != null)
            {
                throw new UserFriendlyException(L("NameAlreadyExists") + " " +result.Id  + " - " + result.MaterialGroupCategoryName );
            }

            item.MaterialGroupCategoryName = dto.MaterialGroupCategoryName;
            item.MaterialGroupRefId = dto.MaterialGroupRefId;
			item.SyncLastModification = dto.SyncLastModification;
			item.SyncId = dto.SyncId;

			await _materialgroupcategoryRepo.InsertOrUpdateAndGetIdAsync(item);

            //CheckErrors(await _materialgroupcategoryManager.CreateSync(item));
            return new IdInput
            {
                Id = input.MaterialGroupCategory.Id.Value
            };
        }

        protected virtual async Task<IdInput> CreateMaterialGroupCategory(CreateOrUpdateMaterialGroupCategoryInput input)
        {

            var dto = input.MaterialGroupCategory.MapTo<MaterialGroupCategory>();

            var result = await (from matgroupcat in _materialgroupcategoryRepo.GetAll().Where(t=>t.MaterialGroupCategoryName.Equals(dto.MaterialGroupCategoryName)) 
                                join matgroup in _materialgroupRepo.GetAll().Where(t => t.TenantId == AbpSession.TenantId)
                                on matgroupcat.MaterialGroupRefId equals matgroup.Id
                                select new MaterialGroupCategoryListDto
                                {
                                    Id = matgroupcat.Id,
                                    MaterialGroupRefId = matgroupcat.MaterialGroupRefId,
                                    MaterialGroupCategoryName = matgroupcat.MaterialGroupCategoryName,
                                }).FirstOrDefaultAsync();

            if (result!=null)
            {
                throw new UserFriendlyException(L("NameAlreadyExists"));
            }

            await _materialgroupcategoryRepo.InsertAndGetIdAsync(dto);

            var returnId = dto.Id;

            return new IdInput
            {
                Id = returnId
            };
        }

        public async Task<MaterialGroupCategoryEditDto> GetOrCreateByName(string categoryName, string groupname)
        {
            MaterialGroupCategoryEditDto editDto;

            var allItems = _materialgroupcategoryRepo
               .GetAll()
               .WhereIf(
                   !categoryName.IsNullOrEmpty(),
                   p => p.MaterialGroupCategoryName.Contains(categoryName)
               );

            List<MaterialGroupCategory> sortMenuItems = new List<MaterialGroupCategory>(allItems);

            if (DynamicQueryable.Any(sortMenuItems))
            {
                editDto = sortMenuItems.First().MapTo<MaterialGroupCategoryEditDto>();
            }
            else
            {
                MaterialGroupEditDto matgroupeditDto;
                var matgroup = _materialgroupRepo.GetAll().Where(t => t.MaterialGroupName.Equals(groupname));

                List<MaterialGroup> matgrouplst = new List<MaterialGroup>(matgroup);

                if (DynamicQueryable.Any(matgrouplst))
                {
                    matgroupeditDto = matgrouplst.First().MapTo<MaterialGroupEditDto>();
                }
                else
                {
                    matgroupeditDto = await _materialgroupAppService.GetOrCreateByName(groupname);
                }

                editDto = new MaterialGroupCategoryEditDto()
                {
                    MaterialGroupCategoryName = categoryName,
                    MaterialGroupRefId = (int) matgroupeditDto.Id,
                };

                var output = await CreateMaterialGroupCategory(new CreateOrUpdateMaterialGroupCategoryInput()
                {
                    MaterialGroupCategory = editDto
                });
                editDto.Id = output.Id;
            }
            return editDto;
        }

        public async Task<ListResultOutput<MaterialGroupCategoryListDto>> GetMaterialGroupCategoryNames()
        {
            var lstMaterialGroupCategory = await (from matgroupcat in _materialgroupcategoryRepo.GetAll()
                                                  join matgroup in _materialgroupRepo.GetAll()
                                                  on matgroupcat.MaterialGroupRefId equals matgroup.Id
                                                  select new MaterialGroupCategoryListDto
                                                  {
                                                      Id = matgroupcat.Id,
                                                      MaterialGroupRefId = matgroupcat.MaterialGroupRefId,
                                                      MaterialGroupCategoryName = matgroupcat.MaterialGroupCategoryName,
													  LastModificationTime = matgroupcat.LastModificationTime==null? matgroupcat.CreationTime: matgroupcat.LastModificationTime
                                                  }).ToListAsync();

            return new ListResultOutput<MaterialGroupCategoryListDto>(lstMaterialGroupCategory.MapTo<List<MaterialGroupCategoryListDto>>());
        }
        public async Task<PagedResultOutput<MaterialGroupCategoryViewDto>> GetView(GetMaterialGroupCategoryInput input)
        {
            var allItems = (from mgcm in _materialgroupcategoryRepo.GetAll()
                            join mgm in _materialgroupRepo.GetAll() 
                            on mgcm.MaterialGroupRefId equals mgm.Id
                            select new MaterialGroupCategoryViewDto()
                            {
                                Id = mgcm.Id,
                                CreationTime = mgcm.CreationTime,
                                MaterialGroupCategoryName = mgcm.MaterialGroupCategoryName,
                                MaterialGroupRefName = mgm.MaterialGroupName,
								LastModificationTime = mgcm.LastModificationTime==null?mgcm.CreationTime:mgcm.LastModificationTime
                            }).WhereIf(
                               !input.Filter.IsNullOrEmpty(),
                               p => p.MaterialGroupCategoryName.Contains(input.Filter)
                               );

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<MaterialGroupCategoryViewDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<MaterialGroupCategoryViewDto>(
                allItemCount,
                allListDtos
                );
        }
    }
}
