﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using System.Linq;
using Abp.BackgroundJobs;
using DinePlan.DineConnect.House.Impl;
using Abp.UI;
using DinePlan.DineConnect.Job.House;

namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class TaxAppService : DineConnectAppServiceBase, ITaxAppService
    {
        private readonly ITaxListExcelExporter _taxExporter;
        private readonly ITaxManager _taxManager;
        private readonly IRepository<Tax> _taxRepo;
        private readonly IRepository<TaxTemplateMapping> _taxTemplateMappingRepo;
        private readonly IBackgroundJobManager _bgm;

        public TaxAppService(ITaxManager taxManager,
            IRepository<Tax> taxRepo,
            IBackgroundJobManager bgm,
            ITaxListExcelExporter taxExporter,
            IRepository<TaxTemplateMapping> taxTemplateMappingRepo)
        {
            _taxManager = taxManager;
            _taxRepo = taxRepo;
            _taxExporter = taxExporter;
            _taxTemplateMappingRepo = taxTemplateMappingRepo;
            _bgm = bgm;

        }
        public async Task<PagedResultOutput<TaxListDto>> GetAll(GetTaxInput input)
        {
            var allItems = _taxRepo.GetAll();
            if (!string.IsNullOrEmpty(input.Operation) && input.Operation.Equals("SEARCH"))
            {
                allItems = _taxRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.TaxName.Equals(input.Filter)
               );
            }
            else
            {
                allItems = _taxRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.TaxName.Contains(input.Filter)
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<TaxListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<TaxListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetTaxInput input)
        {
            input.MaxResultCount = AppConsts.MaxPageSize;

            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<TaxListDto>>();
            return _taxExporter.ExportToFile(allListDtos);
        }

        public async Task<GetTaxForEditOutput> GetTaxForEdit(NullableIdInput input)
        {
            TaxEditDto editDto;
            List<TaxTemplateMappingEditDto> editDetailDto;

            if (input.Id.HasValue)
            {
                var hDto = await _taxRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<TaxEditDto>();

                editDetailDto = await (from tt in _taxTemplateMappingRepo.GetAll().Where(a => a.TaxRefId == input.Id.Value)
                                       select new TaxTemplateMappingEditDto
                                       {
                                           Id=tt.Id,
                                           TaxRefId=tt.TaxRefId,
                                           CompanyRefId=tt.CompanyRefId,
                                           LocationRefId=tt.LocationRefId,
                                           MaterialGroupCategoryRefId=tt.MaterialGroupCategoryRefId,
                                           MaterialGroupRefId=tt.MaterialGroupRefId,
                                           MaterialRefId=tt.MaterialRefId
                                           
                                       }).ToListAsync();
            }
            else
            {
                editDto = new TaxEditDto();

                editDetailDto = new List<TaxTemplateMappingEditDto>();
            }

            return new GetTaxForEditOutput
            {
                Tax = editDto,
                TaxTemplateMapping=editDetailDto
            };
        }

		public bool ExistAllServerLevelBusinessRules(CreateOrUpdateTaxInput input)
		{
			bool AllMaterialFlag = false;
			foreach(var tt in input.TaxTemplateMapping)
			{
				if (tt.CompanyRefId==null && tt.LocationRefId==null && tt.MaterialGroupRefId==null && tt.MaterialGroupCategoryRefId==null && tt.MaterialRefId==null)
				{
					if (input.TaxTemplateMapping.Count>1)
					{
						throw new UserFriendlyException(L("MappingCanNotBeMoreThanOneIfAllMaterialSelectedAlready"));
					}
				}
			}
			return true;

		}
		public async Task CreateOrUpdateTax(CreateOrUpdateTaxInput input)
        {
			if (ExistAllServerLevelBusinessRules(input) == true)
			{
			    int taxId = 0;
				if (input.Tax.Id.HasValue)
				{
                    taxId = input.Tax.Id.Value;
					await UpdateTax(input);
				}
				else
				{
					taxId =  await CreateTax(input);
				}

                await _bgm.EnqueueAsync<HouseIdentificationJob, HouseIdJobArgs>(new HouseIdJobArgs
                {
                    TaxId = taxId
                });

            }
        }

        public async Task DeleteTax(IdInput input)
        {
          
            var mapdetail = await _taxTemplateMappingRepo.GetAll().Where(t => t.TaxRefId == input.Id).ToListAsync();

            foreach (var map in mapdetail)
            {
                await _taxTemplateMappingRepo.DeleteAsync(map.Id);
            }

            await _taxRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateTax(CreateOrUpdateTaxInput input)
        {
            var item = await _taxRepo.GetAsync(input.Tax.Id.Value);
            var dto = input.Tax;

            int newSortOrder = dto.SortOrder;
            int incrementby = 1;
            if (dto.SortOrder < 0)      //  Refer Grand Total Or Sub Total
            {
                newSortOrder = newSortOrder + 12;
                incrementby = 20;
            }
            else
            {
				if (dto.TaxCalculationMethod.Equals("GT"))
				{
					newSortOrder = -1;
					incrementby = 3;
				}
				else if (dto.TaxCalculationMethod.Equals("ST"))
				{
					newSortOrder = -2;
					incrementby = 3;
				}
				else
				{
					var reftax = await _taxRepo.GetAll().Where(t => t.TaxName.Equals(dto.TaxCalculationMethod)).ToListAsync();

					if (reftax.Count() == 0)
					{
						throw new UserFriendlyException("ReferenceTaxIsWrong");
					}
					else
					{
						newSortOrder = reftax[0].SortOrder + 1;
					}
				}
            }

            bool loopFlag = true;
            do
            {
                var sortOrderexist = _taxRepo.GetAll().Where(t => t.SortOrder == newSortOrder && t.Id != dto.Id ).Select(t => t.SortOrder).ToList();
                if (sortOrderexist.Count() == 0)
                {
                    loopFlag = false;
                }
                else
                {
                    newSortOrder = sortOrderexist[0] + incrementby;
                }
            } while (loopFlag);

            dto.SortOrder = newSortOrder;

            bool taxNameChangedflag= false;
            List<Tax> taxrefList = new List<Tax>();
            if (!item.TaxName.Equals(dto.TaxName))
            {
                taxNameChangedflag = true;
                taxrefList = _taxRepo.GetAll().Where(t => t.TaxCalculationMethod.Equals(item.TaxName)).OrderBy(t=>t.SortOrder).ToList();
            }

            //TODO: SERVICE Tax Update Individually
            item.TaxName = dto.TaxName;
            item.Percentage = dto.Percentage;
            item.TaxCalculationMethod = dto.TaxCalculationMethod;
            item.SortOrder = dto.SortOrder;
            item.Rounding = dto.Rounding;

            if (taxNameChangedflag)
            {
                string newTaxName = dto.TaxName;
                foreach (var taxref in taxrefList)
                {
                    newSortOrder++;
                    var refitem = await _taxRepo.GetAsync(taxref.Id);
                    refitem.TaxCalculationMethod = newTaxName;
                    refitem.SortOrder = newSortOrder;
                }
            }
            
            List<int> recordsToBeRetained = new List<int>();
            if (input.TaxTemplateMapping != null && input.TaxTemplateMapping.Count > 0)
            {
                foreach(var items in  input.TaxTemplateMapping)
                {
					List<TaxTemplateMapping> existingIssueDetail;
					if (items.Id != 0)
					{
						var recId = (int)items.Id;
						recordsToBeRetained.Add(recId);
						existingIssueDetail = _taxTemplateMappingRepo.GetAllList(tt => tt.TaxRefId == dto.Id && tt.Id == recId);
					}
					else
					{
						existingIssueDetail = _taxTemplateMappingRepo.GetAllList(tt => tt.TaxRefId == dto.Id && tt.CompanyRefId == items.CompanyRefId && tt.LocationRefId == items.LocationRefId && tt.MaterialGroupRefId == items.MaterialGroupRefId && tt.MaterialGroupCategoryRefId == items.MaterialGroupCategoryRefId && tt.MaterialRefId == items.MaterialRefId);
					}

                    if (existingIssueDetail.Count == 0)  //Add new record
                    {
                        TaxTemplateMapping tm = new TaxTemplateMapping();
                        tm.TaxRefId = (int) dto.Id;
                        tm.CompanyRefId = items.CompanyRefId;
                        tm.LocationRefId = items.LocationRefId;
                        tm.MaterialGroupCategoryRefId = items.MaterialGroupCategoryRefId;
                        tm.MaterialGroupRefId = items.MaterialGroupRefId;
                        tm.MaterialRefId = items.MaterialRefId;

                        var ret2 = await _taxTemplateMappingRepo.InsertOrUpdateAndGetIdAsync(tm);
                        recordsToBeRetained.Add(ret2);
                    }
                    else
                    {
                        var editDetailDto = await _taxTemplateMappingRepo.GetAsync(existingIssueDetail[0].Id);

                        editDetailDto.TaxRefId = (int)dto.Id;
                        editDetailDto.CompanyRefId = items.CompanyRefId;
                        editDetailDto.LocationRefId = items.LocationRefId;
                        editDetailDto.MaterialGroupCategoryRefId = items.MaterialGroupCategoryRefId;
                        editDetailDto.MaterialGroupRefId = items.MaterialGroupRefId;
                        editDetailDto.MaterialRefId = items.MaterialRefId;
                        var ret3 = await _taxTemplateMappingRepo.InsertOrUpdateAndGetIdAsync(editDetailDto);
                        recordsToBeRetained.Add(ret3);
                    }
                }
            }

            var delTTList = _taxTemplateMappingRepo.GetAll().Where(a => a.TaxRefId == input.Tax.Id.Value && !recordsToBeRetained.Contains((int)a.Id)).ToList();
            foreach (var a in delTTList)
            {
                _taxTemplateMappingRepo.Delete(a.Id);
            }


           CheckErrors(await _taxManager.CreateSync(item));
        }

        protected virtual async Task<int> CreateTax(CreateOrUpdateTaxInput input)
        {
            var dto = input.Tax.MapTo<Tax>();

            if (input.TaxTemplateMapping == null || input.TaxTemplateMapping.Count() == 0)
            {
                throw new UserFriendlyException(L("MinimumOneDetail"));
            }

            int newSortOrder = dto.SortOrder;
            int incrementby = 1;
            if (dto.SortOrder < 0)      //  Refer Grand Total Or Sub Total
            {
                newSortOrder = newSortOrder + 12;
                incrementby = 20;
            }
            else
            {
                if (dto.TaxCalculationMethod.Equals("GT"))
                {
                    newSortOrder = -1;
                    incrementby = 3;
                }
                else if (dto.TaxCalculationMethod.Equals("ST"))
                {
                    newSortOrder = -2;
                    incrementby = 3;
                }
                else
                {
                    var reftax = await _taxRepo.GetAll().Where(t => t.TaxName.Equals(dto.TaxCalculationMethod)).ToListAsync();

                    if (!reftax.Any() )
                    {
                        throw new UserFriendlyException("ReferenceTaxIsWrong");
                    }
                    else
                    {
                        newSortOrder = reftax[0].SortOrder + 1;
                    }
                }
            }

            bool loopFlag = true;
            do
            {
                var sortOrderexist = _taxRepo.GetAll().Where(t => t.SortOrder == newSortOrder).Select(t => t.SortOrder).ToList();
                if (!sortOrderexist.Any())
                {
                    loopFlag = false;
                }
                else
                {
                    newSortOrder = sortOrderexist[0] + incrementby;
                }
            } while (loopFlag);

            if (newSortOrder < 0)
                newSortOrder = 1;

            dto.SortOrder = newSortOrder;

            int retId = await _taxRepo.InsertAndGetIdAsync(dto);

            foreach(TaxTemplateMapping items in input.TaxTemplateMapping.ToList())
            {
                TaxTemplateMapping tm = new TaxTemplateMapping();
                tm.TaxRefId = (int)retId;
                tm.CompanyRefId = items.CompanyRefId;
                tm.LocationRefId = items.LocationRefId;
                tm.MaterialGroupCategoryRefId = items.MaterialGroupCategoryRefId;
                tm.MaterialGroupRefId = items.MaterialGroupRefId;
                tm.MaterialRefId = items.MaterialRefId;

                var ret2 = await _taxTemplateMappingRepo.InsertAndGetIdAsync(tm);
            }

            CheckErrors(await _taxManager.CreateSync(dto));

            return dto.Id;
        }

        public async Task<ListResultOutput<TaxListDto>> GetTaxNames()
        {
            var lstTax = await _taxRepo.GetAll().ToListAsync();
            return new ListResultOutput<TaxListDto>(lstTax.MapTo<List<TaxListDto>>());
        }
    }
}
