﻿
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;


namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class CustomerAppService : DineConnectAppServiceBase, ICustomerAppService
    {

        private readonly ICustomerListExcelExporter _customerExporter;
        private readonly ICustomerManager _customerManager;
		private readonly IRepository<Customer> _customerRepo;
		private readonly IRepository<CustomerTagDefinition> _tagdefnRepo;

        public CustomerAppService(ICustomerManager customerManager,
            IRepository<Customer> customerRepo,
			IRepository<CustomerTagDefinition> tagdefnRepo,
			ICustomerListExcelExporter customerExporter)
        {
            _customerManager = customerManager;
            _customerRepo = customerRepo;
            _customerExporter = customerExporter;
			_tagdefnRepo = tagdefnRepo;

		}

        public async Task<PagedResultOutput<CustomerListDto>> GetAll(GetCustomerInput input)
        {
            var allItems = _customerRepo.GetAll();
            if (input.Operation == "SEARCH")
            {
                allItems = _customerRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.CustomerName.Equals(input.Filter)
               );
            }
            else
            {
                allItems = _customerRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.CustomerName.Contains(input.Filter)
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<CustomerListDto>>();

            var allItemCount = await allItems.CountAsync();

			var rsTags = await _tagdefnRepo.GetAllListAsync();
			foreach (var lst in allListDtos)
			{
				lst.CustomerTagRefName = rsTags.FirstOrDefault(t => t.Id == lst.CustomerTagRefId).TagCode;
			}

            return new PagedResultOutput<CustomerListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _customerRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<CustomerListDto>>();
            return _customerExporter.ExportToFile(allListDtos);
        }

        public async Task<GetCustomerForEditOutput> GetCustomerForEdit(NullableIdInput input)
        {
            CustomerEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _customerRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<CustomerEditDto>();
            }
            else
            {
                editDto = new CustomerEditDto();
            }

            return new GetCustomerForEditOutput
            {
                Customer = editDto
            };
        }

        public async Task CreateOrUpdateCustomer(CreateOrUpdateCustomerInput input)
        {
            if (input.Customer.Id.HasValue)
            {
                await UpdateCustomer(input);
            }
            else
            {
                await CreateCustomer(input);
            }
        }

        public async Task DeleteCustomer(IdInput input)
        {
            await _customerRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateCustomer(CreateOrUpdateCustomerInput input)
        {
            var item = await _customerRepo.GetAsync(input.Customer.Id.Value);
            var dto = input.Customer;

            //TODO: SERVICE Customer Update Individually
            item.CustomerName = dto.CustomerName;
            item.CustomerTagRefId = dto.CustomerTagRefId;
            item.Address1 = dto.Address1;
            item.Address2 = dto.Address2;
            item.Address3 = dto.Address3;
            item.City = dto.City;
            item.State = dto.State;
            item.Country = dto.Country;
            item.ZipCode = dto.ZipCode;
            item.PhoneNumber = dto.PhoneNumber;
            item.Email = dto.Email;
            item.FaxNumber = dto.FaxNumber;
            item.Website = dto.Website;
            item.DefaultCreditDays = dto.DefaultCreditDays;
            item.OrderPlacedThrough = dto.OrderPlacedThrough;

            CheckErrors(await _customerManager.CreateSync(item));
        }

        protected virtual async Task CreateCustomer(CreateOrUpdateCustomerInput input)
        {
            var dto = input.Customer.MapTo<Customer>();

            CheckErrors(await _customerManager.CreateSync(dto));
        }

        public async Task<ListResultOutput<CustomerListDto>> GetNames()
        {
            var lstCustomer = await _customerRepo.GetAll().ToListAsync();
            return new ListResultOutput<CustomerListDto>(lstCustomer.MapTo<List<CustomerListDto>>());
        }
        public async Task<ListResultOutput<ComboboxItemDto>> GetCustomerForCombobox()
        {
            var lstCustomer = await _customerRepo.GetAll().ToListAsync();
            return
                new ListResultOutput<ComboboxItemDto>(
                    lstCustomer.Select(e => new ComboboxItemDto(e.Id.ToString(), e.CustomerName)).ToList());
        }

    }
}