﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;


namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class MaterialGroupAppService : DineConnectAppServiceBase, IMaterialGroupAppService
    {

        private readonly IMaterialGroupListExcelExporter _materialgroupExporter;
        private readonly IMaterialGroupManager _materialgroupManager;
        private readonly IRepository<MaterialGroup> _materialgroupRepo;
        private readonly IRepository<MaterialGroupCategory> _materialGroupCategoryRepo;

        public MaterialGroupAppService(IMaterialGroupManager materialgroupManager,
            IRepository<MaterialGroup> materialGroupRepo,
            IMaterialGroupListExcelExporter materialgroupExporter,
            IRepository<MaterialGroupCategory> materialGroupCategoryRepo)
        {
            _materialgroupManager = materialgroupManager;
            _materialgroupRepo = materialGroupRepo;
            _materialgroupExporter = materialgroupExporter;
            _materialGroupCategoryRepo = materialGroupCategoryRepo;
        }

        public async Task<PagedResultOutput<MaterialGroupListDto>> GetAll(GetMaterialGroupInput input)
        {
            var allItems = _materialgroupRepo.GetAll();
            if (!string.IsNullOrEmpty(input.Operation) && input.Operation.Equals("SEARCH"))
            {
                allItems = _materialgroupRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.MaterialGroupName.Equals(input.Filter)
               );
            }
            else
            {
                allItems = _materialgroupRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.MaterialGroupName.Contains(input.Filter)
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<MaterialGroupListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<MaterialGroupListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _materialgroupRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<MaterialGroupListDto>>();
            return _materialgroupExporter.ExportToFile(allListDtos);
        }

        public async Task<GetMaterialGroupForEditOutput> GetMaterialGroupForEdit(NullableIdInput input)
        {
            MaterialGroupEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _materialgroupRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<MaterialGroupEditDto>();
            }
            else
            {
                editDto = new MaterialGroupEditDto();
            }

            return new GetMaterialGroupForEditOutput
            {
                MaterialGroup = editDto
            };
        }

        public async Task<IdInput> CreateOrUpdateMaterialGroup(CreateOrUpdateMaterialGroupInput input)
        {
            if (input.MaterialGroup.Id.HasValue)
            {
                    return await UpdateMaterialGroup(input);
            }
            else
            {
                   return await CreateMaterialGroup(input);
            }
        }

        public async Task DeleteMaterialGroup(IdInput input)
        {
            var rsMaterialGroupCategory = await _materialGroupCategoryRepo.FirstOrDefaultAsync(t => t.MaterialGroupRefId == input.Id);

            if (rsMaterialGroupCategory == null)
                await _materialgroupRepo.DeleteAsync(input.Id);
            else
                throw new UserFriendlyException(L("ReferenceExists"));
        }


        protected virtual async Task<IdInput> UpdateMaterialGroup(CreateOrUpdateMaterialGroupInput input)
        {
            var item = await _materialgroupRepo.GetAsync(input.MaterialGroup.Id.Value);
            var dto = input.MaterialGroup;
            item.MaterialGroupName = dto.MaterialGroupName;

			item.SyncLastModification = dto.SyncLastModification;
			item.SyncId = dto.SyncId;
			//TODO: SERVICE MaterialGroup Update Individually

			CheckErrors(await _materialgroupManager.CreateSync(item));

            return new IdInput
            {
                Id = input.MaterialGroup.Id.Value
            };
        }

        protected virtual async Task<IdInput> CreateMaterialGroup(CreateOrUpdateMaterialGroupInput input)
        {
            var dto = input.MaterialGroup.MapTo<MaterialGroup>();

          

            CheckErrors(await _materialgroupManager.CreateSync(dto));

            var returnId = dto.Id;

            return new IdInput
            {
                Id = returnId
            };
        }

        public async Task<MaterialGroupEditDto> GetOrCreateByName(string groupname)
        {
            MaterialGroupEditDto editDto;

            var allItems = _materialgroupRepo
               .GetAll()
               .WhereIf(
                   !groupname.IsNullOrEmpty(),
                   p => p.MaterialGroupName.Contains(groupname)
               );

            List<MaterialGroup> sortMenuItems = new List<MaterialGroup>(allItems);

            if (DynamicQueryable.Any(sortMenuItems))
            {
                editDto = sortMenuItems.First().MapTo<MaterialGroupEditDto>();
            }
            else
            {
                editDto = new MaterialGroupEditDto()
                {
                    MaterialGroupName = groupname,
                };
                var output = await CreateMaterialGroup(new CreateOrUpdateMaterialGroupInput()
                {
                    MaterialGroup = editDto
                });

                editDto.Id = output.Id;
            }
            return editDto;
        }

        public async Task<ListResultOutput<MaterialGroupListDto>> GetMaterialGroupNames()
        {
            var lstMaterialGroup = await _materialgroupRepo.GetAll().ToListAsync();
            return new ListResultOutput<MaterialGroupListDto>(lstMaterialGroup.MapTo<List<MaterialGroupListDto>>());
        }
    }
}
