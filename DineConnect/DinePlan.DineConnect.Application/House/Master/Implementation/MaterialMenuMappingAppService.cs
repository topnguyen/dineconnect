﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Utility.Exporter;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Connect.Master;

namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class MaterialMenuMappingAppService : DineConnectAppServiceBase, IMaterialMenuMappingAppService
    {
        private readonly IExcelExporter _excelExporter;
        private readonly IMaterialMenuMappingListExcelExporter _materialmenumappingExporter;
        private readonly IMaterialMenuMappingManager _materialmenumappingManager;
        private readonly IRepository<MaterialMenuMapping> _materialmenumappingRepo;
        private readonly IRepository<MenuMappingDepartment> _menumappingdepartmentRepo;
        private readonly IRepository<Department> _departmentRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<Unit> _unitRepo;
        private readonly IRepository<MenuItemPortion> _menuitemportionRepo;
        private readonly IRepository<MenuItem> _menuitemRepo;

		public MaterialMenuMappingAppService(IMaterialMenuMappingManager materialmenumappingManager,
            IRepository<MaterialMenuMapping> materialMenuMappingRepo,
            IRepository<MenuMappingDepartment> menumappingdepartmentRepo,
        IExcelExporter excelExporter,
            IMaterialMenuMappingListExcelExporter materialmenumappingExporter,
            IRepository<Material> materialRepo,
            IRepository<Unit> unitRepo,
            IRepository<Department> departmentRepo,
            IRepository<MenuItemPortion> menuitemportionRepo,
            IRepository<MenuItem> menuitemRepo)
        {
            _materialmenumappingManager = materialmenumappingManager;
            _materialmenumappingRepo = materialMenuMappingRepo;
            _materialmenumappingExporter = materialmenumappingExporter;
            _materialRepo = materialRepo;
            _excelExporter = excelExporter;
            _unitRepo = unitRepo;
            _menuitemportionRepo = menuitemportionRepo;
            _menuitemRepo = menuitemRepo;
            _menumappingdepartmentRepo = menumappingdepartmentRepo;
            _departmentRepo = departmentRepo;
        }

        public async Task<PagedResultOutput<MaterialMenuMappingViewDto>> GetAll(GetMaterialMenuMappingInput input)
        {
            var outputM  = _materialmenumappingRepo.GetAll()
                .Select(t => new {PortionId = t.PosMenuPortionRefId, AutoSales = t.AutoSalesDeduction})
                .Distinct();
            var allItems = (from mMap in outputM
                            join menuPortion in _menuitemportionRepo.GetAll()
                            on mMap.PortionId equals menuPortion.Id
                            select new MaterialMenuMappingViewDto()
                            {
                                PosMenuPortionRefId = menuPortion.Id,
                                PosRefName = string.Concat(menuPortion.MenuItem.Name, "-", menuPortion.Name),
                                AutoSales = mMap.AutoSales
                            }).WhereIf(
                           !input.Filter.IsNullOrEmpty(),
                           p => p.PosRefName.Contains(input.Filter));
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();
            var allListDtos = sortMenuItems.MapTo<List<MaterialMenuMappingViewDto>>();
            var allItemCount = await allItems.CountAsync();
            return new PagedResultOutput<MaterialMenuMappingViewDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<PagedResultOutput<MenuMappingWithMaterialCountViewDto>> GetAllWithMenuWithMaterialCount(GetMaterialMenuMappingInput input)
        {
			IQueryable<MenuMappingWithMaterialDto> allItems;
			if (input.ListFilter == null)
				input.ListFilter = "All";

			if (input.ListFilter.Equals("Mapped"))
			{
				allItems = (from mMap in _materialmenumappingRepo.GetAll()
							join menuPortion in _menuitemportionRepo.GetAll()
							on mMap.PosMenuPortionRefId equals menuPortion.Id
							select new MenuMappingWithMaterialDto
							{
								MenuPortionId = menuPortion.Id,
								Name = string.Concat(menuPortion.MenuItem.Name, "-", menuPortion.Name),
								MaterialRefId = mMap.MaterialRefId
							}).WhereIf(
								  !input.Filter.IsNullOrEmpty(),
								  p => p.Name.Contains(input.Filter));
			}
			else if (input.ListFilter.Equals("NonMapped"))
			{
				var arrMappedAlreadyRefIds = await _materialmenumappingRepo.GetAll().Select(t => t.PosMenuPortionRefId).ToListAsync();
				allItems = (from menuPortion in _menuitemportionRepo.GetAll().Where(t=>!arrMappedAlreadyRefIds.Contains(t.Id))
							select new MenuMappingWithMaterialDto
							{
								MenuPortionId = menuPortion.Id,
								Name = string.Concat(menuPortion.MenuItem.Name, "-", menuPortion.Name),
								MaterialRefId = 0
							}).WhereIf(
								  !input.Filter.IsNullOrEmpty(),
								  p => p.Name.Contains(input.Filter));
			}
			else if (input.ListFilter.Equals("All"))
			{
				allItems = (from menuPortion in _menuitemportionRepo.GetAll()
								join mMap in _materialmenumappingRepo.GetAll()
								on menuPortion.Id equals mMap.PosMenuPortionRefId into leftjoined
								from lj in leftjoined.DefaultIfEmpty()
								select new MenuMappingWithMaterialDto
								{
									MenuPortionId = menuPortion.Id,
									Name = string.Concat(menuPortion.MenuItem.Name, "-", menuPortion.Name),
									MaterialRefId = lj == null ? 0 : lj.MaterialRefId,
								}).WhereIf(
					  !input.Filter.IsNullOrEmpty(),
					  p => p.Name.Contains(input.Filter));
			}
			else
			{
				allItems = null;
				return new PagedResultOutput<MenuMappingWithMaterialCountViewDto>(
				0,
				new List<MenuMappingWithMaterialCountViewDto>()
				);
			}

			var allItemsConsolidated = (from a in allItems
                                        group a by new { a.Name, a.MenuPortionId } into g
                                        select new MenuMappingWithMaterialCountViewDto
                                        {
                                            PosRefName = g.Key.Name,
                                            PosMenuPortionRefId = g.Key.MenuPortionId,
                                            MaterialList = g.ToList(),
                                            NumberOfMaterials = g.Where(t=>t.MaterialRefId!=0).Select(t=>t.MaterialRefId).Distinct().Count() 
										});
			
            var fallItemsConsolidated = await allItemsConsolidated.OrderBy(input.Sorting).PageBy(input).ToListAsync();
            var allListDtos = fallItemsConsolidated.MapTo<List<MenuMappingWithMaterialCountViewDto>>();
            var allItemCount = allItemsConsolidated.Count();
            return new PagedResultOutput<MenuMappingWithMaterialCountViewDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<List<MaterialMenuMappingViewDto>> GetNonMappedMenuList()
        {
            int[] menuMap = _materialmenumappingRepo.GetAll().Select(t => t.PosMenuPortionRefId).Distinct().ToArray();

            var allNonMappedItems = (from menuportion in _menuitemportionRepo.GetAll().Where(t => !menuMap.Contains(t.Id))
                            join menuitem in _menuitemRepo.GetAll()
                            on menuportion.MenuItemId equals menuitem.Id
                            select new MaterialMenuMappingViewDto()
                            {
                                PosMenuPortionRefId = menuportion.Id,
                                PosRefName = string.Concat(menuitem.Name, "-", menuportion.Name),
                            });

            var sortMenuItems = await allNonMappedItems
                .OrderBy(t=>t.PosRefName)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<MaterialMenuMappingViewDto>>();


            return allListDtos;
        }

        public async Task<List<MaterialMenuMappingListDto>> GetMappedMenuList()
        {
            var allMappedItems = (from menumap in _materialmenumappingRepo.GetAll().Where(t=>!t.LocationRefId.HasValue)
                                     join menuportion in _menuitemportionRepo.GetAll() 
                                     on menumap.PosMenuPortionRefId equals menuportion.Id 
                                     join menuitem in _menuitemRepo.GetAll()
                                     on menuportion.MenuItemId equals menuitem.Id
                                     join mat in _materialRepo.GetAll() 
                                     on menumap.MaterialRefId equals mat.Id 
                                     join un in _unitRepo.GetAll() 
                                     on menumap.PortionUnitId equals un.Id 
                                     select new MaterialMenuMappingListDto()
                                     {
                                         PosMenuPortionRefId = menuportion.Id,
                                         PosRefName = string.Concat(menuitem.Name, "-", menuportion.Name),
                                         MaterialRefId = mat.Id,
                                         MaterialRefName = mat.MaterialName,
                                         MaterialTypeId = mat.MaterialTypeId,
                                         MaterialTypeName = "",
                                         MenuQuantitySold = menumap.MenuQuantitySold,
                                         PortionQty = menumap.PortionQty,
                                         PortionUnitId = menumap.PortionUnitId,
                                         UnitRefName = un.Name,                                         
                                         AutoSalesDeduction = menumap.AutoSalesDeduction,
                                         UserSerialNumber = menumap.UserSerialNumber,
                                     });

            var sortMenuItems = await allMappedItems
                .OrderBy(t => t.PosRefName)
                .ToListAsync();

            foreach (var lst in sortMenuItems)
            {
                string materialTypeName = lst.MaterialTypeId == (int)MaterialType.RAW ? L("RAW") : L("SEMI");
                lst.MaterialTypeName = materialTypeName;
            }

            var allListDtos = sortMenuItems.MapTo<List<MaterialMenuMappingListDto>>();

            return allListDtos;
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _materialmenumappingRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<MaterialMenuMappingListDto>>();
            return _materialmenumappingExporter.ExportToFile(allListDtos);

        }

        public async Task<FileDto> GetMappedToExcel()
        {
            var allList = await GetMappedMenuList();
            var allListDtos = allList.MapTo<List<MaterialMenuMappingListDto>>();

            var baseE = new BaseExportObject()
            {
                ExportObject = allListDtos,
                ColumnNames = new string[] { "PosMenuPortionRefId", "PosRefName","MenuQuantitySold", "MaterialRefName", "MaterialTypeName","PortionQty", "UnitRefName", "AutoSalesDeduction" },
                ColumnDisplayNames = new string[] { "MenuId", "MenuName","MenuQuantity", "MaterialName", "MaterialType", "Portion Quantity", "UOM", L("AutomaticIssue") },
                BreakOnLineColumn = "PosRefName"
            };

            return _excelExporter.ExportToFile(baseE, L("MenuMappedList"));
        }

        public async Task<FileDto> GetNonMappedToExcel()
        {
            var allList = await GetNonMappedMenuList();
            var allListDtos = allList.MapTo<List<MaterialMenuMappingViewDto>>();

            var baseE = new BaseExportObject()
            {
                ExportObject = allListDtos,
                ColumnNames = new string[] { "PosMenuPortionRefId", "PosRefName" },
                ColumnDisplayNames = new string[] { "MenuId", "MenuName" }
            };

            return _excelExporter.ExportToFile(baseE, L("NonMappedList"));
        }

        public async Task<GetMaterialMenuMappingForEditOutput> GetMaterialMenuMappingForEdit(MaterialMenuWithLocation input)
        {
            List<MaterialMenuMappingListDto> editDto;

            if (input.PosMenuPortionRefId>0)
            {
                var rsMenuMapping = _materialmenumappingRepo.GetAll().Where(p => p.PosMenuPortionRefId == input.PosMenuPortionRefId && p.LocationRefId==input.LocationRefId);

                var hDto = (from a in rsMenuMapping
                            join mat in _materialRepo.GetAll()
                            on a.MaterialRefId equals mat.Id
                            join un in _unitRepo.GetAll()
                            on mat.DefaultUnitId equals un.Id
                            join uiss in _unitRepo.GetAll()
                            on a.PortionUnitId equals uiss.Id
                            select new MaterialMenuMappingListDto
                            {
                                Id = a.Id,
                                PosMenuPortionRefId = a.PosMenuPortionRefId,
                                MenuQuantitySold = a.MenuQuantitySold,
                                PortionQty = a.PortionQty,
                                UserSerialNumber = a.UserSerialNumber,
                                MaterialRefId = a.MaterialRefId,
                                MaterialRefName = mat.MaterialName,
                                MaterialTypeId =mat.MaterialTypeId,
                                PortionUnitId = a.PortionUnitId,
                                UnitRefName = uiss.Name,
                                DefaultUnitId = mat.DefaultUnitId,
                                DefaultUnitName = un.Name,
                                WastageExpected = a.WastageExpected,
                                AutoSalesDeduction =a.AutoSalesDeduction
                            }).ToList();

                editDto = hDto;
                List<int> lstMenumappingRefIds = hDto.Select(t => t.Id).ToList();
                var menumappingDepartment = await _menumappingdepartmentRepo.GetAllListAsync(t => lstMenumappingRefIds.Contains(t.MenuMappingRefId));
                var deptList = await _departmentRepo.GetAll().ToListAsync();

                foreach (var lst in editDto)
                {
                    string materialTypeName = lst.MaterialTypeId == (int)MaterialType.RAW ? L("RAW") : L("SEMI");
                    lst.MaterialTypeName = materialTypeName;


                    var deptDtos =  (from md in menumappingDepartment.Where(t => t.MenuMappingRefId == lst.Id)
                                          join de in deptList on md.DepartmentRefId equals de.Id
                                          select new ComboboxItemDto
                                          {
                                              Value = md.DepartmentRefId.ToString(),
                                              DisplayText = de.Name
                                          }).ToList();
                    if (deptDtos==null || deptDtos.Count==0)
                    {
                        deptDtos.Add(new ComboboxItemDto { Value = "0", DisplayText = L("All") });
                    }

                    lst.DeptList = deptDtos;
                }
            }
            else
            {
                editDto = new List<MaterialMenuMappingListDto>();
            }

            return new GetMaterialMenuMappingForEditOutput
            {
                MaterialMenuMappingDetail = editDto
            };

        }

        public async Task CreateOrUpdateMaterialMenuMapping(CreateOrUpdateMaterialMenuMappingInput input)
        {
            await CreateMaterialMenuMapping(input);
        }

        public async Task DeleteMaterialMenuMapping(IdInput input)
        {
            var lstMenu = await _materialmenumappingRepo.GetAll().Where(t => t.PosMenuPortionRefId == input.Id).ToListAsync();
            foreach(var m in lstMenu)
            {
                await _materialmenumappingRepo.DeleteAsync(m.Id);
            }
        }

        public async Task DeleteMaterialMenuMappingBasedOnMaterial(MaterialMenuMappingListDto input)
        {
            var lstMenu = await _materialmenumappingRepo.GetAll().Where(t => t.PosMenuPortionRefId == input.PosMenuPortionRefId && t.MaterialRefId==input.MaterialRefId).ToListAsync();
            foreach (var m in lstMenu)
            {
                await _menumappingdepartmentRepo.DeleteAsync(t => t.MenuMappingRefId == m.Id);
                await _materialmenumappingRepo.DeleteAsync(m.Id);
            }
        }

        public async Task<bool> DeleteMaterialMenuMappingList(DeleteMaterialMenuWithLocationList input)
        {
            bool returnStatus = false;
            if (input.MaterialMenuWithLocations != null)
            {
                foreach (var lst in input.MaterialMenuWithLocations)
                {
                    var exists = await _materialmenumappingRepo.FirstOrDefaultAsync(t => t.LocationRefId == lst.LocationRefId && t.PosMenuPortionRefId == lst.PosMenuPortionRefId && t.MaterialRefId == lst.MaterialRefId);
                    if (exists != null)
                    {
                        await _materialmenumappingRepo.DeleteAsync(exists.Id);
                        returnStatus = true;
                    }
                }
            }
            return returnStatus;
        }



        protected virtual async Task CreateMaterialMenuMapping(CreateOrUpdateMaterialMenuMappingInput input)
        {
            int posreferenceid = 0;

            if (input.MaterialMenuMappingDetail != null && input.MaterialMenuMappingDetail.Count > 0)
            {
                posreferenceid = input.MaterialMenuMappingDetail[0].PosMenuPortionRefId;

                foreach (var items in input.MaterialMenuMappingDetail)
                {
                    int materialrefid = items.MaterialRefId;
                    var existingDetailEntry = _materialmenumappingRepo.GetAllList(u => u.PosMenuPortionRefId == items.PosMenuPortionRefId && u.MaterialRefId == items.MaterialRefId && u.LocationRefId==items.LocationRefId);

                    int menuMappingId;

                    if (existingDetailEntry.Count == 0)  //  Add New Material
                    {
                        MaterialMenuMapping ab = new MaterialMenuMapping();

                        ab.PosMenuPortionRefId = items.PosMenuPortionRefId;
                        ab.MenuQuantitySold = items.MenuQuantitySold;
                        ab.UserSerialNumber = items.UserSerialNumber;
                        ab.MaterialRefId = items.MaterialRefId;
                        ab.PortionQty = items.PortionQty;
                        ab.PortionUnitId = items.PortionUnitId;
                        ab.WastageExpected = items.WastageExpected;
                        ab.AutoSalesDeduction = items.AutoSalesDeduction;
                        ab.LocationRefId = items.LocationRefId;

                        menuMappingId = await _materialmenumappingRepo.InsertAndGetIdAsync(ab);

                        CheckErrors(await _materialmenumappingManager.CreateSync(ab));
                    }
                    else
                    {
                        var editDetailDto = await _materialmenumappingRepo.GetAsync(existingDetailEntry[0].Id);

                        editDetailDto.PosMenuPortionRefId = items.PosMenuPortionRefId;
                        editDetailDto.MenuQuantitySold = items.MenuQuantitySold;
                        editDetailDto.UserSerialNumber = items.UserSerialNumber;
                        editDetailDto.MaterialRefId = items.MaterialRefId;
                        editDetailDto.PortionQty = items.PortionQty;
                        editDetailDto.PortionUnitId = items.PortionUnitId;
                        editDetailDto.WastageExpected = items.WastageExpected;
                        editDetailDto.AutoSalesDeduction = items.AutoSalesDeduction;
                        editDetailDto.LocationRefId = items.LocationRefId;

                        menuMappingId = await _materialmenumappingRepo.InsertOrUpdateAndGetIdAsync(editDetailDto);

                        CheckErrors(await _materialmenumappingManager.CreateSync(editDetailDto));
                    }

                    List<int> deptToBeRetained = new List<int>();
                    if (items.DeptList == null)
                    {
                        var delList = await _menumappingdepartmentRepo.GetAll().Where(a => a.MenuMappingRefId == items.Id).ToListAsync();

                        foreach (var a in delList)
                        {
                           await _menumappingdepartmentRepo.DeleteAsync(a.Id);
                        }
                    }
                    else
                    {
                        foreach (var dept in items.DeptList)
                        {
                            int deptrefid = int.Parse(dept.Value);

                            if (deptrefid == 0)
                            {
                                continue;
                            }

                            deptToBeRetained.Add(deptrefid);

                            var existingDepartmentDetailEntry =await _menumappingdepartmentRepo.FirstOrDefaultAsync(u => u.MenuMappingRefId == menuMappingId && u.DepartmentRefId == deptrefid);

                            if (existingDepartmentDetailEntry == null)  //  Add New Material
                            {
                                MenuMappingDepartment md = new MenuMappingDepartment();

                                md.MenuMappingRefId = menuMappingId;
                                md.DepartmentRefId = deptrefid;

                                var retId2 = await _menumappingdepartmentRepo.InsertAndGetIdAsync(md);

                            }
                            else
                            {
                                var mdEdit = await _menumappingdepartmentRepo.GetAsync(existingDepartmentDetailEntry.Id);

                                mdEdit.MenuMappingRefId = menuMappingId;
                                mdEdit.DepartmentRefId = deptrefid;

                                var retId2 = await _menumappingdepartmentRepo.InsertOrUpdateAndGetIdAsync(mdEdit);
                            }
                        }

                        var delDeptDetailList = await _menumappingdepartmentRepo.GetAll().Where(a => a.MenuMappingRefId == menuMappingId && !deptToBeRetained.Contains(a.DepartmentRefId)).ToListAsync();

                        foreach (var a in delDeptDetailList)
                        {
                            await   _menumappingdepartmentRepo.DeleteAsync(a.Id);
                        }
                    }
                }
            }
        }

        public async Task<ListResultOutput<MaterialMenuMappingListDto>> GetPosMenuPortionRefIds()
        {
            var lstMaterialMenuMapping = await _materialmenumappingRepo.GetAll().ToListAsync();
            return new ListResultOutput<MaterialMenuMappingListDto>(lstMaterialMenuMapping.MapTo<List<MaterialMenuMappingListDto>>());
        }

		public async Task<List<ComboboxItemDto>> GetConnectDepartments()
		{
			var roles = await _departmentRepo.GetAllListAsync();
			return
				new List<ComboboxItemDto>(
					roles.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
		}
	}
}
