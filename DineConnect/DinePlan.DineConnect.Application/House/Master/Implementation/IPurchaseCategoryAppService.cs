﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master
{
	public interface IPurchaseCategoryAppService : IApplicationService
	{
		Task<PagedResultOutput<PurchaseCategoryListDto>> GetAll(GetPurchaseCategoryInput inputDto);
		Task<FileDto> GetAllToExcel();
		Task<GetPurchaseCategoryForEditOutput> GetPurchaseCategoryForEdit(NullableIdInput nullableIdInput);
		Task CreateOrUpdatePurchaseCategory(CreateOrUpdatePurchaseCategoryInput input);
		Task DeletePurchaseCategory(IdInput input);

		Task<ListResultOutput<PurchaseCategoryListDto>> GetNames();
	}
}

