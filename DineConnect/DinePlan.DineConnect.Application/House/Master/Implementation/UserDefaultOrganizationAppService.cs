﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.MultiTenancy;
using Abp.Authorization.Users;
using DinePlan.DineConnect.Connect.Master.Dtos;

namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class UserDefaultOrganizationAppService : DineConnectAppServiceBase, IUserDefaultOrganizationAppService
    {

        private readonly IUserDefaultOrganizationListExcelExporter _userdefaultorganizationExporter;
        private readonly IUserDefaultOrganizationManager _userdefaultorganizationManager;
        private readonly IRepository<UserDefaultOrganization> _userdefaultorganizationRepo;
        private readonly IRepository<Location> _locationRepo;
		private readonly IRepository<Company> _companyRepo;
		private readonly UserManager _userManager;
		private readonly ILocationAppService _locationAppService;
		


        public UserDefaultOrganizationAppService(IUserDefaultOrganizationManager userdefaultorganizationManager,
            IRepository<UserDefaultOrganization> userDefaultOrganizationRepo,
            IUserDefaultOrganizationListExcelExporter userdefaultorganizationExporter,
            IRepository<Location> locationRepo,
            UserManager userManager,
			ILocationAppService locationAppService,
			IRepository<Company> companyRepo
			)
        {
            _userdefaultorganizationManager = userdefaultorganizationManager;
            _userdefaultorganizationRepo = userDefaultOrganizationRepo;
            _userdefaultorganizationExporter = userdefaultorganizationExporter;
            _locationRepo = locationRepo;
            _userManager = UserManager;
			_locationAppService = locationAppService;
			_companyRepo = companyRepo;
		}

        public async Task<PagedResultOutput<UserDefaultOrganizationListDto>> GetAll(GetUserDefaultOrganizationInput input)
        {
			var userlist = UserManager.Users;
			if (!input.Filter.IsNullOrEmpty())
				userlist = userlist.Where(t => t.UserName.Contains(input.Filter));

			var allItems = from ul in userlist 
				           join udo in _userdefaultorganizationRepo.GetAll()
						   on ul.Id equals udo.UserId into leftjoined 
						   from lj in leftjoined.DefaultIfEmpty() 
						   select new UserDefaultOrganizationListDto
						   {
							   UserId = ul.Id,
							   UserName = ul.Name,
							   LastModificationTime = ul.LastModificationTime,
							   DefaultLocationRefId = lj.OrganizationUnitId,
							   //DefaultLocationName = loc.Name,
							   //OrganizationUnitName = loc.Name,
						   };


			var sortMenuItems = await allItems
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<UserDefaultOrganizationListDto>>();

            var allItemCount = await allItems.CountAsync();

			foreach(var lst in allListDtos)
			{
				var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == lst.DefaultLocationRefId);
				if (location != null)
				{
					lst.DefaultLocationName = location.Name;
					var company = await _companyRepo.FirstOrDefaultAsync(t => t.Id == location.CompanyRefId);
					lst.OrganizationUnitName = company.Name;
				}
				var locList = await _locationAppService.GetLocationBasedOnUser(new Connect.Master.Dtos.GetLocationInputBasedOnUser { UserId = lst.UserId });
				lst.LocationList = locList.Items.MapTo<List<LocationListDto>>();
				foreach(var loc in lst.LocationList)
				{
					lst.LocationCodeList = lst.LocationCodeList + loc.Code + ", "  ;
				}
				if (lst.LocationCodeList.Length>0)
				{
					lst.LocationCodeList = lst.LocationCodeList.Substring(0,lst.LocationCodeList.Length - 2);
				}
				lst.LocationCount = locList.TotalCount;

				var companiesRefIds = lst.LocationList.Select(t => t.CompanyRefId).Distinct().ToArray();
				List<ComboboxItemDto> companyList = new List<ComboboxItemDto>();
				foreach(var com in companiesRefIds)
				{
					var company = await _companyRepo.FirstOrDefaultAsync(t => t.Id == com);
					if (company != null)
						companyList.Add(new ComboboxItemDto { Value = com.ToString(), DisplayText = company.Name });
				}
				lst.CompanyList = companyList;
			}

            return new PagedResultOutput<UserDefaultOrganizationListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _userdefaultorganizationRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<UserDefaultOrganizationListDto>>();
            return _userdefaultorganizationExporter.ExportToFile(allListDtos);
        }

        public async Task<GetUserDefaultOrganizationForEditOutput> GetUserDefaultOrganizationForEdit(NullableIdInput input)
        {
            UserDefaultOrganizationEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _userdefaultorganizationRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<UserDefaultOrganizationEditDto>();
            }
            else
            {
                editDto = new UserDefaultOrganizationEditDto();
            }

            return new GetUserDefaultOrganizationForEditOutput
            {
                UserDefaultOrganization = editDto
            };
        }

        public async Task CreateOrUpdateUserDefaultOrganization(CreateOrUpdateUserDefaultOrganizationInput input)
        {
            if (input.UserDefaultOrganization.Id.HasValue)
            {
                await UpdateUserDefaultOrganization(input);
            }
            else
            {
                await CreateUserDefaultOrganization(input);
            }
        }

        public async Task DeleteUserDefaultOrganization(IdInput input)
        {
            await _userdefaultorganizationRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateUserDefaultOrganization(CreateOrUpdateUserDefaultOrganizationInput input)
        {
            var item = await _userdefaultorganizationRepo.GetAsync(input.UserDefaultOrganization.Id.Value);
            var dto = input.UserDefaultOrganization;

            //TODO: SERVICE UserDefaultOrganization Update Individually
            item.OrganizationUnitId = dto.OrganizationUnitId;
            dto.UserId = dto.UserId;

            CheckErrors(await _userdefaultorganizationManager.CreateSync(item));
        }

        protected virtual async Task CreateUserDefaultOrganization(CreateOrUpdateUserDefaultOrganizationInput input)
        {
            var dto = input.UserDefaultOrganization.MapTo<UserDefaultOrganization>();

            CheckErrors(await _userdefaultorganizationManager.CreateSync(dto));
        }

        public async Task<ListResultOutput<UserDefaultOrganizationListDto>> GetIds()
        {
            var lstUserDefaultOrganization = await _userdefaultorganizationRepo.GetAll().ToListAsync();
            return new ListResultOutput<UserDefaultOrganizationListDto>(lstUserDefaultOrganization.MapTo<List<UserDefaultOrganizationListDto>>());
        }

		public async Task<UserDefaultOrganizationListDto> GetUserInfo(IdInput input)
		{
			UserDefaultOrganizationListDto lst = new UserDefaultOrganizationListDto();
			var user = UserManager.Users.FirstOrDefault(t => t.Id == input.Id);
			if (user == null)
			{
				return null;
			}
			var udo = await _userdefaultorganizationRepo.FirstOrDefaultAsync(t => t.UserId == input.Id);
			lst.UserId = user.Id;
			lst.UserName = user.Name;
			lst.LastModificationTime = user.LastModificationTime;
			if (udo!=null)
			{
				lst.DefaultLocationRefId = udo.OrganizationUnitId;
				var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == lst.DefaultLocationRefId);
				if (location != null)
				{
					lst.DefaultLocationName = location.Name;
					var company = await _companyRepo.FirstOrDefaultAsync(t => t.Id == location.CompanyRefId);
					lst.OrganizationUnitName = company.Name;
					lst.DefaultOrganisationRefId = location.CompanyRefId;
				}

			}

			var locList = await _locationAppService.GetLocationBasedOnUser(new Connect.Master.Dtos.GetLocationInputBasedOnUser { UserId = lst.UserId });
			lst.LocationList = locList.Items.MapTo<List<LocationListDto>>();
			lst.LocationCount = locList.TotalCount;

			var companiesRefIds = lst.LocationList.Select(t => t.CompanyRefId).Distinct().ToArray();
			List<ComboboxItemDto> companyList = new List<ComboboxItemDto>();
			foreach (var com in companiesRefIds)
			{
				var company = await _companyRepo.FirstOrDefaultAsync(t => t.Id == com);
				if (company != null)
					companyList.Add(new ComboboxItemDto { Value = com.ToString(), DisplayText = company.Name });
			}
			lst.CompanyList = companyList;
			return lst;
		}
	}
}
