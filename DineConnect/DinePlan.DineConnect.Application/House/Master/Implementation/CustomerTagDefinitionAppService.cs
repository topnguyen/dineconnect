﻿
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using System.Dynamic;

namespace DinePlan.DineConnect.House.Master.Implementation
{

    public class CustomerTagDefinitionAppService : DineConnectAppServiceBase, ICustomerTagDefinitionAppService
    {

        private readonly ICustomerTagDefinitionListExcelExporter _customertagdefinitionExporter;
        private readonly ICustomerTagDefinitionManager _customertagdefinitionManager;
        private readonly IRepository<CustomerTagDefinition> _customertagdefinitionRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<CustomerTagMaterialPrice> _customertagmaterialpriceRepo;


        public CustomerTagDefinitionAppService(ICustomerTagDefinitionManager customertagdefinitionManager,
            IRepository<CustomerTagDefinition> customerTagDefinitionRepo,
            ICustomerTagDefinitionListExcelExporter customertagdefinitionExporter,
            IRepository<Material> materialRepo,
            IRepository<CustomerTagMaterialPrice> customertagmaterialpriceRepo)
        {
            _customertagdefinitionManager = customertagdefinitionManager;
            _customertagdefinitionRepo = customerTagDefinitionRepo;
            _customertagdefinitionExporter = customertagdefinitionExporter;
            _materialRepo = materialRepo;
            _customertagmaterialpriceRepo = customertagmaterialpriceRepo;
        }

        public async Task<PagedResultOutput<CustomerTagDefinitionListDto>> GetAll(GetCustomerTagDefinitionInput input)
        {
            var allItems = _customertagdefinitionRepo.GetAll();
            if (input.Operation == "SEARCH")
            {
                allItems = _customertagdefinitionRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.TagCode.Equals(input.Filter)
               );
            }
            else
            {
                allItems = _customertagdefinitionRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.TagCode.Contains(input.Filter)
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<CustomerTagDefinitionListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<CustomerTagDefinitionListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _customertagdefinitionRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<CustomerTagDefinitionListDto>>();
            return _customertagdefinitionExporter.ExportToFile(allListDtos);
        }

        public async Task<GetCustomerTagDefinitionForEditOutput> GetCustomerTagDefinitionForEdit(NullableIdInput input)
        {
            CustomerTagDefinitionEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _customertagdefinitionRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<CustomerTagDefinitionEditDto>();
            }
            else
            {
                editDto = new CustomerTagDefinitionEditDto();
            }

            return new GetCustomerTagDefinitionForEditOutput
            {
                CustomerTagDefinition = editDto
            };
        }

        public async Task CreateOrUpdateCustomerTagDefinition(CreateOrUpdateCustomerTagDefinitionInput input)
        {
            if (input.CustomerTagDefinition.Id.HasValue)
            {
                await UpdateCustomerTagDefinition(input);
            }
            else
            {
                await CreateCustomerTagDefinition(input);
            }
        }

        public async Task DeleteCustomerTagDefinition(IdInput input)
        {
            await _customertagdefinitionRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateCustomerTagDefinition(CreateOrUpdateCustomerTagDefinitionInput input)
        {
            var item = await _customertagdefinitionRepo.GetAsync(input.CustomerTagDefinition.Id.Value);
            item.TagCode = item.TagCode.Replace(" ", "");

            string tagNeedsToChange = item.TagCode;

            var dto = input.CustomerTagDefinition;

            item.TagCode = dto.TagCode;
            item.TagName = dto.TagName;

            item.TagCode = item.TagCode.Replace(" ", "");


            var itemcpt = await _customertagmaterialpriceRepo.GetAll().Where(a => a.TagCode.Equals(tagNeedsToChange)).ToListAsync();
            foreach(var lst in itemcpt)
            {
                var edittag = await _customertagmaterialpriceRepo.GetAsync(lst.Id);
                edittag.TagCode = dto.TagCode;
                edittag.TagCode = edittag.TagCode.Replace(" ", "");
                //await _customertagmaterialpriceRepo.InsertAndGetIdAsync(edittag);
            }


            CheckErrors(await _customertagdefinitionManager.CreateSync(item));
        }

        protected virtual async Task CreateCustomerTagDefinition(CreateOrUpdateCustomerTagDefinitionInput input)
        {
            var dto = input.CustomerTagDefinition.MapTo<CustomerTagDefinition>();
            dto.TagCode = dto.TagCode.Replace(" ", "");

            CheckErrors(await _customertagdefinitionManager.CreateSync(dto));
        }

        public async Task<ListResultOutput<CustomerTagDefinitionListDto>> GetTagCodes()
        {
            var lstCustomerTagDefinition = await _customertagdefinitionRepo.GetAll().ToListAsync();
            return new ListResultOutput<CustomerTagDefinitionListDto>(lstCustomerTagDefinition.MapTo<List<CustomerTagDefinitionListDto>>());
        }

        public async Task AddTagIntoAllMaterial()
        {
            var matlist = await _materialRepo.GetAll().ToListAsync();

            var customertagList = _customertagdefinitionRepo.GetAll().ToList();

            foreach (var lst in matlist)
            {
                foreach (var tag in customertagList)
                {
                    if (_customertagmaterialpriceRepo.GetAllList(m => m.MaterialRefId == lst.Id && m.TagCode.Equals(tag.TagCode)).Count == 0)
                    {
                        CustomerTagMaterialPrice newDto = new CustomerTagMaterialPrice();
                        {
                            newDto.MaterialRefId = lst.Id;
                            newDto.TagCode = tag.TagCode;
                            newDto.MaterialPrice = 0;
                        };
                        var retId2 = await _customertagmaterialpriceRepo.InsertAndGetIdAsync(newDto);
                    }
                }
            }
        }

        public async Task CreateCustomerTagForTenant(int tenantId)
        {
            CustomerTagDefinition paymentType = new CustomerTagDefinition { TagCode = "DEFAULT", TagName = "DEFAULT", TenantId = tenantId};
            await _customertagdefinitionRepo.InsertAsync(paymentType);
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetCustomerTagDefinitionForCombobox()
        {
            var lst = await _customertagdefinitionRepo.GetAllListAsync();
            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Select(e => new ComboboxItemDto(e.Id.ToString(), e.TagCode)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetCustomerTagForCombobox()
        {
            var lst = await GetTagCodes();
            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.TagCode)).ToList());
        }

    }


}
