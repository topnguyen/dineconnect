﻿
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;


namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class CustomerMaterialAppService : DineConnectAppServiceBase, ICustomerMaterialAppService
    {

        private readonly ICustomerMaterialListExcelExporter _customermaterialExporter;
        private readonly ICustomerMaterialManager _customermaterialManager;
        private readonly IRepository<CustomerMaterial> _customermaterialRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<Customer> _customerRepo;

        public CustomerMaterialAppService(ICustomerMaterialManager customermaterialManager,
            IRepository<CustomerMaterial> customerMaterialRepo,
            ICustomerMaterialListExcelExporter customermaterialExporter
             , IRepository<Material> material, IRepository<Customer> customer)
        {
            _customermaterialManager = customermaterialManager;
            _customermaterialRepo = customerMaterialRepo;
            _customermaterialExporter = customermaterialExporter;
            _materialRepo = material;
            _customerRepo = customer;
        }

        public async Task<PagedResultOutput<CustomerMaterialListDto>> GetAll(GetCustomerMaterialInput input)
        {
            var allItems = (from cm in _customermaterialRepo.GetAll()
                            join c in _customerRepo.GetAll() on
                            cm.CustomerRefId equals c.Id
                            join mt in _materialRepo.GetAll() on
                            cm.MaterialRefId equals mt.Id
                            select new CustomerMaterialListDto()
                            {
                                Id = cm.Id,
                                CustomerRefName = c.CustomerName,
                                MaterialRefName = mt.MaterialName,
                                MaterialPrice = cm.MaterialPrice,
                                LastQuoteRefNo = cm.LastQuoteRefNo,
                                LastQuoteDate = cm.LastQuoteDate,
                                CreationTime=cm.CreationTime
                            }).WhereIf(
                               !input.Filter.IsNullOrEmpty(),
                               p => p.CustomerRefName.Contains(input.Filter)

              );
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<CustomerMaterialListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<CustomerMaterialListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _customermaterialRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<CustomerMaterialListDto>>();
            return _customermaterialExporter.ExportToFile(allListDtos);
        }

        public async Task<GetCustomerMaterialForEditOutput> GetCustomerMaterialForEdit(NullableIdInput input)
        {
            CustomerMaterialEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _customermaterialRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<CustomerMaterialEditDto>();
            }
            else
            {
                editDto = new CustomerMaterialEditDto();
            }

            return new GetCustomerMaterialForEditOutput
            {
                CustomerMaterial = editDto
            };
        }

        public async Task CreateOrUpdateCustomerMaterial(CreateOrUpdateCustomerMaterialInput input)
        {
            if (input.CustomerMaterial.Id.HasValue)
            {
                await UpdateCustomerMaterial(input);
            }
            else
            {
                await CreateCustomerMaterial(input);
            }
        }

        public async Task DeleteCustomerMaterial(IdInput input)
        {
            await _customermaterialRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateCustomerMaterial(CreateOrUpdateCustomerMaterialInput input)
        {
            var item = await _customermaterialRepo.GetAsync(input.CustomerMaterial.Id.Value);
            var dto = input.CustomerMaterial;

            //TODO: SERVICE CustomerMaterial Update Individually
            item.CustomerRefId = dto.CustomerRefId;
            item.MaterialRefId = dto.MaterialRefId;
            item.MaterialPrice = dto.MaterialPrice;
            item.LastQuoteDate = dto.LastQuoteDate;
            item.LastQuoteRefNo = dto.LastQuoteRefNo;


            CheckErrors(await _customermaterialManager.CreateSync(item));
        }

        protected virtual async Task CreateCustomerMaterial(CreateOrUpdateCustomerMaterialInput input)
        {
            var dto = input.CustomerMaterial.MapTo<CustomerMaterial>();

            CheckErrors(await _customermaterialManager.CreateSync(dto));
        }

        public async Task<ListResultOutput<CustomerMaterialListDto>> GetNames()
        {
            var lstCustomerMaterial = await _customermaterialRepo.GetAll().ToListAsync();
            return new ListResultOutput<CustomerMaterialListDto>(lstCustomerMaterial.MapTo<List<CustomerMaterialListDto>>());
        }
    }
}