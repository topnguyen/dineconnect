﻿
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Connect.Menu;

namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class ProductRecipesLinkAppService : DineConnectAppServiceBase, IProductRecipesLinkAppService
    {

        private readonly IProductRecipesLinkListExcelExporter _productrecipeslinkExporter;
        private readonly IProductRecipesLinkManager _productrecipeslinkManager;
        private readonly IRepository<ProductRecipesLink> _productrecipeslinkRepo;
        private readonly IRepository<Recipe> _recipeRepo;
        private readonly IRepository<Unit> _unitRepo;
        private readonly IRepository<MenuItem> _menuItemRepo;


        public ProductRecipesLinkAppService(IProductRecipesLinkManager productrecipeslinkManager,
            IRepository<ProductRecipesLink> productRecipesLinkRepo,
            IProductRecipesLinkListExcelExporter productrecipeslinkExporter,IRepository<Recipe> recipeRepo,
         IRepository<Unit> unitRepo, IRepository<MenuItem> menuItemRepo)
        {
            _productrecipeslinkManager = productrecipeslinkManager;
            _productrecipeslinkRepo = productRecipesLinkRepo;
            _productrecipeslinkExporter = productrecipeslinkExporter;
            _recipeRepo = recipeRepo;
            _unitRepo = unitRepo;
            _menuItemRepo = menuItemRepo;
        }

        public async Task<PagedResultOutput<ProductRecipesLinkListDto>> GetAll(GetProductRecipesLinkInput input)
        {
            var allItems = (from prl in _productrecipeslinkRepo.GetAll()
                            join re in _recipeRepo.GetAll() on
                            prl.RecipeRefId equals re.Id
                            join un in _unitRepo.GetAll() on
                             prl.PortionUnitId equals un.Id
                            join mi in _menuItemRepo.GetAll() on 
                            prl.PosRefId equals mi.Id
                           
                            select new ProductRecipesLinkListDto()
                            {
                                Id = prl.Id,
                                PosRefId = prl.PosRefId,
                                PosRefName=mi.Name,
                                UserSerialNumber=prl.UserSerialNumber,
                                RecipeRefId=prl.RecipeRefId,
                                RecipeRefName=re.RecipeName,
                                PortionQty=prl.PortionQty,
                                PortionUnitId=prl.PortionUnitId,
                                PortionUnitName=un.Name,
                                RecipeType=prl.RecipeType,
                                WastageExpected=prl.WastageExpected,
                                CreationTime=prl.CreationTime

                            }).WhereIf(
                               !input.Filter.IsNullOrEmpty(),
                               p => p.PosRefName.Contains(input.Filter));

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<ProductRecipesLinkListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<ProductRecipesLinkListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _productrecipeslinkRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<ProductRecipesLinkListDto>>();
            return _productrecipeslinkExporter.ExportToFile(allListDtos);
        }

        public async Task<GetProductRecipesLinkForEditOutput> GetProductRecipesLinkForEdit(NullableIdInput input)
        {
            List<ProductRecipesLinkListDto> editDto;

            if (input.Id.HasValue)
            {
                var hDto = (from a in _productrecipeslinkRepo.GetAll().Where(p=>p.PosRefId == input.Id.Value)
                            join r in _recipeRepo.GetAll()
                            on a.RecipeRefId equals r.Id 
                            join u in _unitRepo.GetAll() 
                            on a.PortionUnitId equals u.Id 
                           select new ProductRecipesLinkListDto
                           {
                               Id = a.Id,
                               PosRefId = a.PosRefId,
                               PortionQty = a.PortionQty,
                               UserSerialNumber = a.UserSerialNumber,
                               RecipeRefId = a.RecipeRefId,
                               RecipeRefName = r.RecipeName,
                               PortionUnitId = a.PortionUnitId,
                               PortionUnitName = u.Name,
                               RecipeType = a.RecipeType,
                               WastageExpected = a.WastageExpected,

                           }).ToList();
                editDto = hDto;
                //editDto = hDto; // hDto.MapTo<ProductRecipesLinkEditDto>();
            }
            else
            {
                editDto = new List<ProductRecipesLinkListDto>();
            }

            return new GetProductRecipesLinkForEditOutput
            {
                ProductRecipesLinkDetail = editDto
            };
        }

        public async Task CreateOrUpdateProductRecipesLink(CreateOrUpdateProductRecipesLinkInput input)
        {
            await CreateProductRecipesLink(input);

            //if (input.ProductRecipesLinkDetail[0].Id)
            //{
            //    await CreateProductRecipesLink(input);
            //}
            //else
            //{
            //    await CreateProductRecipesLink(input);
            //}
        }

        public async Task DeleteProductRecipesLink(IdInput input)
        {
            await _productrecipeslinkRepo.DeleteAsync(input.Id);
        }

     
        protected virtual async Task CreateProductRecipesLink(CreateOrUpdateProductRecipesLinkInput input)
        {
            //  Update Detail Link
            List<int> recipeToBeRetained = new List<int>();
            int posreferenceid = 0;

            if (input.ProductRecipesLinkDetail != null && input.ProductRecipesLinkDetail.Count > 0)
            {
                posreferenceid = input.ProductRecipesLinkDetail[0].PosRefId;

                foreach (var items in input.ProductRecipesLinkDetail)
                {
                    int reciperefid = items.RecipeRefId;
                    recipeToBeRetained.Add(reciperefid);

                    var existingDetailEntry = _productrecipeslinkRepo.GetAllList(u => u.PosRefId == items.PosRefId && u.RecipeRefId == items.RecipeRefId);

                    if (existingDetailEntry.Count == 0)  //  Add New Material
                    {
                        ProductRecipesLink ab = new ProductRecipesLink();

                        ab.PosRefId = items.PosRefId;
                        ab.UserSerialNumber = items.UserSerialNumber;
                        ab.RecipeRefId = items.RecipeRefId;
                        ab.PortionQty = items.PortionQty;
                        ab.PortionUnitId = items.PortionUnitId;
                        ab.RecipeType = items.RecipeType;
                        ab.WastageExpected = items.WastageExpected;

                        var retId2 = await _productrecipeslinkRepo.InsertAndGetIdAsync(ab);

                        CheckErrors(await _productrecipeslinkManager.CreateSync(ab));
                    }
                    else
                    {
                        var editDetailDto = await _productrecipeslinkRepo.GetAsync(existingDetailEntry[0].Id);

                        editDetailDto.PosRefId = items.PosRefId;
                        editDetailDto.UserSerialNumber = items.UserSerialNumber;
                        editDetailDto.RecipeRefId = items.RecipeRefId;
                        editDetailDto.PortionQty = items.PortionQty;
                        editDetailDto.PortionUnitId = items.PortionUnitId;
                        editDetailDto.RecipeType = items.RecipeType;
                        editDetailDto.WastageExpected = items.WastageExpected;

                        var retId2 = await _productrecipeslinkRepo.InsertOrUpdateAndGetIdAsync(editDetailDto);

                        CheckErrors(await _productrecipeslinkManager.CreateSync(editDetailDto));
                    }
                }
            }

            var delDetailList = _productrecipeslinkRepo.GetAll().Where(a => a.PosRefId == posreferenceid && !recipeToBeRetained.Contains(a.RecipeRefId)).ToList();

            foreach (var a in delDetailList)
            {
                _productrecipeslinkRepo.Delete(a.Id);
            }


            //CheckErrors(await _productrecipeslinkManager.CreateSync(dto));
        }

        public async Task<ListResultOutput<ProductRecipesLinkListDto>> GetIds()
        {
            var lstProductRecipesLink = await _productrecipeslinkRepo.GetAll().ToListAsync();
            return new ListResultOutput<ProductRecipesLinkListDto>(lstProductRecipesLink.MapTo<List<ProductRecipesLinkListDto>>());
        }
    }
}
