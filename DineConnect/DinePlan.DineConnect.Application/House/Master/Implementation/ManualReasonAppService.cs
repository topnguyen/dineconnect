﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using System;

namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class ManualReasonAppService : DineConnectAppServiceBase, IManualReasonAppService
    {

        private readonly IManualReasonListExcelExporter _manualreasonExporter;
        private readonly IManualReasonManager _manualreasonManager;
        private readonly IRepository<ManualReason> _manualreasonRepo;

        public ManualReasonAppService(IManualReasonManager manualreasonManager,
            IRepository<ManualReason> manualReasonsRepo,
            IManualReasonListExcelExporter manualreasonExporter)
        {
            _manualreasonManager = manualreasonManager;
            _manualreasonRepo = manualReasonsRepo;
            _manualreasonExporter = manualreasonExporter;

        }

        public async Task<PagedResultOutput<ManualReasonListDto>> GetAll(GetManualReasonInput input)
        {
            if (input.FullOptions)
                input.SkipCount = AppConsts.MaxPageSize;

            IQueryable<ManualReason> allItems;
            allItems = _manualreasonRepo.GetAll();
            if (input.GeneralIncluded)
            {
                if (input.ManualReasonCategoryRefId > 0)
                    allItems = allItems.Where(t => t.ManualReasonCategoryRefId == (int)ManualReasonCategory.General || t.ManualReasonCategoryRefId == input.ManualReasonCategoryRefId).WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.ManualReasonName.Contains(input.Filter));
                else
                    allItems = allItems.Where(t => t.ManualReasonCategoryRefId == (int)ManualReasonCategory.General).WhereIf(
               !input.Filter.IsNullOrEmpty(),
               p => p.ManualReasonName.Contains(input.Filter));
            }
            else if (input.ManualReasonCategoryRefId>0)
            {
                allItems = allItems.Where(t => t.ManualReasonCategoryRefId == input.ManualReasonCategoryRefId).WhereIf(
                  !input.Filter.IsNullOrEmpty(),
                  p => p.ManualReasonName.Contains(input.Filter));
            }

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<ManualReasonListDto>>();

            var allItemCount = await allItems.CountAsync();
            var temp = await GetManualReasonCategoryForCombobox();
            var manualReasonDtos = temp.Items.MapTo<List<ComboboxItemDto>>();
            foreach (var gp in allListDtos.GroupBy(t=>t.ManualReasonCategoryRefId))
            {
                var mr = manualReasonDtos.FirstOrDefault(t => t.Value == gp.Key.ToString());
                if (mr != null)
                {
                    foreach (var lst in gp.ToList())
                    {
                        lst.ManualReasonCategoryRefName = mr.DisplayText;
                    }
                }
            }
            return new PagedResultOutput<ManualReasonListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetManualReasonCategoryForCombobox()
        {
            var retList = new List<ComboboxItemDto>();

            string enumstring;
            var EnumValues = Enum.GetValues(typeof(ManualReasonCategory));
            foreach (int EnumValue in EnumValues)
            {
                enumstring = Enum.GetName(typeof(ManualReasonCategory), EnumValue);
                retList.Add(new ComboboxItemDto { Value = EnumValue.ToString(), DisplayText = enumstring });
            }

            return
                new ListResultOutput<ComboboxItemDto>(
                    retList.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText)).ToList());
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _manualreasonRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<ManualReasonListDto>>();
            return _manualreasonExporter.ExportToFile(allListDtos);
        }

        public async Task<GetManualReasonForEditOutput> GetManualReasonForEdit(NullableIdInput input)
        {
            ManualReasonEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _manualreasonRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<ManualReasonEditDto>();
            }
            else
            {
                editDto = new ManualReasonEditDto();
            }

            return new GetManualReasonForEditOutput
            {
                ManualReason = editDto
            };
        }

        public async Task<ManualReason> CreateOrUpdateManualReason(CreateOrUpdateManualReasonInput input)
        {
            if (input.ManualReason.Id.HasValue)
            {
                return await UpdateManualReason(input);
            }
            else
            {
                return await CreateManualReason(input);
            }
        }

        public async Task DeleteManualReason(IdInput input)
        {
            await _manualreasonRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task<ManualReason> UpdateManualReason(CreateOrUpdateManualReasonInput input)
        {
            var item = await _manualreasonRepo.GetAsync(input.ManualReason.Id.Value);
            var dto = input.ManualReason;
            item.ManualReasonCategoryRefId = dto.ManualReasonCategoryRefId;
            item.ManualReasonName = dto.ManualReasonName;

            CheckErrors(await _manualreasonManager.CreateSync(item));

            return item;

        }

        protected virtual async Task<ManualReason> CreateManualReason(CreateOrUpdateManualReasonInput input)
        {
            var dto = input.ManualReason.MapTo<ManualReason>();

            CheckErrors(await _manualreasonManager.CreateSync(dto));

            return dto;
        }

        public async Task<ListResultOutput<ManualReasonListDto>> GetManualReasonNames(FilterManualReasonDto input)
        {
            var iqueryiable = _manualreasonRepo.GetAll();
            if (input.IncludeGeneralCategoryAlso)
            {
                iqueryiable = iqueryiable.Where(t => t.ManualReasonCategoryRefId == (int)ManualReasonCategory.General || t.ManualReasonCategoryRefId == input.ManualReasonCategoryRefId);
            }
            else
            {
                iqueryiable = iqueryiable.Where(t => t.ManualReasonCategoryRefId == input.ManualReasonCategoryRefId);
            }
            var result = await iqueryiable.ToListAsync();
            var allListDtos = result.MapTo<List<ManualReasonListDto>>();
            var temp = await GetManualReasonCategoryForCombobox();
            var manualReasonDtos = temp.MapTo<List<ComboboxItemDto>>();
            foreach (var lst in allListDtos)
            {
                var mr = manualReasonDtos.FirstOrDefault(t => t.Value == lst.ManualReasonCategoryRefId.ToString());
                lst.ManualReasonCategoryRefName = mr.DisplayText;
            }
            return new ListResultOutput<ManualReasonListDto>(allListDtos.MapTo<List<ManualReasonListDto>>());
        }
    }
}