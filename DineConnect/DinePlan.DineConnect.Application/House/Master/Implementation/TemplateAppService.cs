﻿
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using System;
using DinePlan.DineConnect.Connect.Master;

namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class TemplateAppService : DineConnectAppServiceBase, ITemplateAppService
    {


        private readonly ITemplateListExcelExporter _templateExporter;
        private readonly ITemplateManager _templateManager;
        private readonly IRepository<Template> _templateRepo;
        private readonly IRepository<DayCloseTemplate> _daycloseTemplateRepo;
        private readonly IRepository<Location> _locationRepo;

        public TemplateAppService(ITemplateManager templateManager,
            IRepository<Template> templateRepo,
            ITemplateListExcelExporter templateExporter,
            IRepository<DayCloseTemplate> daycloseTemplateRepo,
            IRepository<Location> locationRepo)
        {
            _templateManager = templateManager;
            _templateRepo = templateRepo;
            _templateExporter = templateExporter;
            _daycloseTemplateRepo = daycloseTemplateRepo;
            _locationRepo = locationRepo;
        }

        public async Task<PagedResultOutput<TemplateListDto>> GetAll(GetTemplateInput input)
        {

            var allItems = _templateRepo.GetAll()
             .WhereIf(
                 !input.Filter.IsNullOrEmpty(),
                 p => p.Name.Contains(input.Filter)
             );

            if (input.LocationRefId.HasValue)
                allItems = allItems.Where(t => t.LocationRefId.HasValue && t.LocationRefId == input.LocationRefId);

            if (input.TemplateType.HasValue)
                allItems = allItems.Where(t => t.TemplateType == input.TemplateType);

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<TemplateListDto>>();
            var comboList = await GetTemplateTypeForCombobox();
            var rsLoc = await _locationRepo.GetAll().Select(t => new { t.Id, t.Code, t.Name }).ToListAsync();
            foreach (var lst in allListDtos)
            {
                var typeName = comboList.Items.FirstOrDefault(t => t.Value == lst.TemplateType.ToString());
                if (typeName != null)
                {
                    lst.TemplateTypeRefName = typeName.DisplayText;
                }
                var location = rsLoc.FirstOrDefault(t => t.Id == lst.LocationRefId);
                if (location != null)
                {
                    lst.LocationRefName = location.Name;
                }
                else
                {
                    lst.LocationRefName = L("GlobalTemplate");
                }
            }
            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<TemplateListDto>(
                allItemCount,
                allListDtos
                );
        }
        public async Task<ListResultOutput<ComboboxItemDto>> GetTemplateTypeForCombobox()
        {
            var retList = new List<ComboboxItemDto>();

            string enumstring;
            var EnumValues = Enum.GetValues(typeof(TemplateType));
            foreach (int EnumValue in EnumValues)
            {
                enumstring = Enum.GetName(typeof(TemplateType), EnumValue);
                retList.Add(new ComboboxItemDto { Value = EnumValue.ToString(), DisplayText = enumstring });
            }

            return
                new ListResultOutput<ComboboxItemDto>(
                    retList.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText)).ToList());
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _templateRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<TemplateListDto>>();
            return _templateExporter.ExportToFile(allListDtos);
        }

        public async Task<GetTemplateForEditOutput> GetTemplateForEdit(NullableIdInput input)
        {
            TemplateEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _templateRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<TemplateEditDto>();
            }
            else
            {
                editDto = new TemplateEditDto();
            }

            return new GetTemplateForEditOutput
            {
                Template = editDto
            };
        }

        public async Task CreateOrUpdateTemplate(CreateOrUpdateTemplateInput input)
        {
            if (input.Template.Id.HasValue)
            {
                await UpdateTemplate(input);
            }
            else
            {
                await CreateTemplate(input);
            }
        }

        public async Task DeleteTemplate(IdInput input)
        {
            await _templateRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateTemplate(CreateOrUpdateTemplateInput input)
        {
            var item = await _templateRepo.GetAsync(input.Template.Id.Value);
            var dto = input.Template;

            //TODO: SERVICE Template Update Individually
            item.Name = dto.Name;
            item.TemplateType = dto.TemplateType;
            item.TemplateData = dto.TemplateData;


            CheckErrors(await _templateManager.CreateSync(item));
        }

        protected virtual async Task CreateTemplate(CreateOrUpdateTemplateInput input)
        {
            var dto = input.Template.MapTo<Template>();

            CheckErrors(await _templateManager.CreateSync(dto));
        }

        public async Task<ListResultOutput<TemplateListDto>> GetTemplateDetail(GetTemplateList input)
        {
            var lstTemplate = await _templateRepo.GetAll().Where(t=> (t.LocationRefId == input.LocationRefId || t.LocationRefId == null || t.LocationRefId==0) && t.TemplateType == input.TemplateType).ToListAsync();
            return new ListResultOutput<TemplateListDto>(lstTemplate.MapTo<List<TemplateListDto>>());
        }

        public async Task<ListResultOutput<TemplateInfoDto>> GetTemplateInfo(GetTemplateList input)
        {
            var lstTemplate = await _templateRepo.GetAll().Where(t => (t.LocationRefId == input.LocationRefId || t.LocationRefId == null || t.LocationRefId == 0) && t.TemplateType == input.TemplateType).ToListAsync();
            return new ListResultOutput<TemplateInfoDto>(lstTemplate.MapTo<List<TemplateInfoDto>>());
        }

        public async Task CreateOrUpdateDayCloseTemplate(CreateOrUpdateDayCloseTemplateInput input)
        {
            if (input.DayCloseTemplate.Id.HasValue)
            {
                await UpdateDayCloseTemplate(input);
            }
            else
            {
                await CreateDayCloseTemplate(input);
            }
        }

        public async Task DeleteDayCloseTemplate(IdInput input)
        {
            await _daycloseTemplateRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateDayCloseTemplate(CreateOrUpdateDayCloseTemplateInput input)
        {
            var item = await _daycloseTemplateRepo.GetAsync(input.DayCloseTemplate.Id.Value);
            var dto = input.DayCloseTemplate;

            //TODO: SERVICE Template Update Individually
            item.LocationRefId = dto.LocationRefId;
            item.ClosedDate = dto.ClosedDate;
            item.TableName = dto.TableName;
            item.TemplateType = dto.TemplateType;
            item.TemplateData = dto.TemplateData;
            await _daycloseTemplateRepo.InsertOrUpdateAndGetIdAsync(item);

        }

        protected virtual async Task CreateDayCloseTemplate(CreateOrUpdateDayCloseTemplateInput input)
        {
            var dto = input.DayCloseTemplate.MapTo<DayCloseTemplate>();

            CheckErrors(await _templateManager.CreateDayCloseTemplateSync(dto));
        }


    }
}