﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using Castle.Core.Logging;
using DinePlan.DineConnect.Common;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Departments.Dtos;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.Ticket.CategoryReport;
using DinePlan.DineConnect.Connect.Ticket.CategoryReport.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Features;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.House.Transaction;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.House.Transaction.Implementation;
using DinePlan.DineConnect.HouseReportName;
using DinePlan.DineConnect.Job.House;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class HouseReportAppService : DineConnectAppServiceBase, IHouseReportAppService
    {
        private readonly IHouseReportListExcelExporter _housereportExporter;
        private readonly IHouseReportManager _housereportManager;
        private readonly InvoiceAppService _invoiceAppService;
        private readonly IRepository<HouseReport> _housereportRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<LocationGroup> _locationGroupRepo;
        private readonly IRepository<MaterialLedger> _materialLedgerRepo;
        private readonly IRepository<MaterialGroup> _materialGroupRepo;
        private readonly IRepository<MaterialGroupCategory> _materialGroupCategoryRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<Invoice> _invoiceRepo;
        private readonly IRepository<InvoiceDetail> _invoiceDetailRepo;
        private readonly IRepository<Supplier> _supplierRepo;
        private readonly IRepository<SupplierMaterial> _supplierMaterialRepo;
        private readonly IRepository<Unit> _unitrepo;
        private readonly IRepository<UnitConversion> _unitConversionRepo;
        private readonly IConnectReportAppService _connectReportAppService;
        private readonly IRepository<MaterialMenuMapping> _materialmenumappingRepo;
        private readonly IRepository<MenuMappingDepartment> _menumappingdepartmentRepo;
        private readonly IRepository<Department> _departmentRepo;
        private readonly IRepository<Issue> _issueRepo;
        private readonly IRepository<IssueDetail> _issuedetailRepo;
        private readonly IRepository<Production> _productionRepo;
        private readonly IRepository<ProductionDetail> _productindetailRepo;
        private readonly IRepository<Yield> _yieldRepo;
        private readonly IRepository<YieldInput> _yieldinputRepo;
        private readonly IRepository<YieldOutput> _yieldoutputRepo;
        private readonly IRepository<ProductionUnit> _productionunitRepo;
        private readonly IRepository<Return> _returnRepo;
        private readonly IRepository<ReturnDetail> _returndetailRepo;
        private readonly IRepository<MaterialRecipeTypes> _materialRecipeTypeRepo;
        private readonly IRepository<MaterialIngredient> _materialingredientRepo;
        private readonly IUnitConversionAppService _unitConversionAppService;
        private readonly IRepository<MenuItemPortion> _menuitemportionRepo;
        private readonly IRepository<MenuItem> _menuitemRepo;
        private readonly IRepository<InterTransfer> _intertransferRepo;
        private readonly IRepository<InterTransferDetail> _intertransferDetailRepo;
        private readonly IRepository<InterTransferReceivedDetail> _intertransferReceivedRepo;
        private readonly ILogger _logger;
        private readonly IRepository<ClosingStock> _closingStockRepo;
        private readonly IRepository<ClosingStockDetail> _closingStockDetailRepo;
        private readonly IRepository<ClosingStockUnitWiseDetail> _closingStockUnitWiseDetailRepo;
        private readonly IRepository<Adjustment> _adjustmentRepo;
        private readonly IRepository<AdjustmentDetail> _adjustmentDetailRepo;
        private readonly IRepository<MaterialStockCycleLink> _materialStockCycleRepo;
        private readonly IRepository<InventoryCycleLinkWithLocation> _inventoryCycleLinkLocation;
        private readonly IRepository<InventoryCycleTag> _inventoryCycleTagRepo;
        private readonly IXmlAndJsonConvertor _xmlandjsonConvertorAppService;
        private readonly IRepository<MaterialUnitsLink> _materialUnitLinkRepo;
        private readonly IMaterialLedgerAppService _materialLedgerAppService;
        private readonly IRepository<MaterialLocationWiseStock> _materialLocationWiseStockRepo;
        private readonly IRepository<LocationWiseMaterialRateView> _locationWiseMaterialRateViewRepo;
        private readonly IRepository<PurchaseReturn> _purchaseReturn;
        private readonly IRepository<PurchaseReturnDetail> _purchaseReturnDetailRepo;
        private readonly IPurchaseReturnAppService _purchaseReturnAppService;
        private readonly IMaterialAppService _materialAppService;
        private readonly IRepository<MenuItemWastageDetail> _menuitemwastageDetailRepo;
        private readonly IRepository<MenuItemWastage> _menuitemwastageRepo;
        private readonly ICategoryReportAppService _categoryReportAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ILocationAppService _locService;
        private readonly IReportBackgroundAppService _rbas;
        private readonly IBackgroundJobManager _bgm;
        private SettingManager _settingManager;
        private int roundDecimals = 2;

        public HouseReportAppService(IHouseReportManager housereportManager,
        IRepository<HouseReport> houseReportRepo,
        IHouseReportListExcelExporter housereportExporter,
        IRepository<MaterialLedger> materialLedgerRepo,
        IRepository<Location> locationRepo,
        IRepository<Material> materialRepo,
        IRepository<Invoice> invoiceRepo,
        IRepository<InvoiceDetail> invoiceDetailRepo,
        IRepository<Supplier> supplierRepo,
        InvoiceAppService invoiceAppService,
        IConnectReportAppService connectReportAppService,
        IRepository<MaterialMenuMapping> materialmenumappingRepo,
        IRepository<IssueDetail> issuedetailRepo,
        IRepository<Issue> issueRepo,
        IRepository<Production> productionRepo,
        IRepository<ProductionDetail> productindetailRepo,
        IRepository<Yield> yieldRepo,
        IRepository<YieldInput> yieldinputRepo,
        IRepository<YieldOutput> yieldoutputRepo,
        IRepository<Unit> unitrepo,
        IRepository<UnitConversion> unitConversionRepo,
        IRepository<ProductionUnit> productionunitRepo,
        IRepository<Return> returnRepo,
        IRepository<ReturnDetail> returndetailRepo,
        IRepository<MaterialRecipeTypes> materialRecipeTypeRepo,
        IRepository<MaterialIngredient> materialingredientRepo,
        IRepository<SupplierMaterial> supplierMaterialRepo,
        IUnitConversionAppService unitConversionAppService,
        IRepository<MenuItemPortion> menuitemportionRepo,
        IRepository<MenuItem> menuitemRepo,
        IRepository<MenuMappingDepartment> menumappingdepartmentRepo,
        IRepository<InterTransfer> intertransferRepo,
        IRepository<InterTransferReceivedDetail> intertransferReceivedRepo,
        IRepository<Department> departmentRepo, ILogger logger,
        IRepository<ClosingStock> closingStockRepo,
        IRepository<ClosingStockDetail> closingStockDetailRepo,
        IRepository<Adjustment> adjustmentRepo,
        IRepository<MaterialStockCycleLink> materialStockCycleRepo,
        IRepository<InventoryCycleTag> inventoryCycleTagRepo,
        IXmlAndJsonConvertor xmlandjsonConvertorAppService,
        IMaterialLedgerAppService materialLedgerAppService,
        IRepository<AdjustmentDetail> adjustmentDetailRepo,
        IRepository<MaterialGroupCategory> materialGroupCategoryRepo,
        IRepository<MaterialUnitsLink> materialUnitLinkRepo,
        IRepository<InterTransferDetail> intertransferDetailRepo,
        SettingManager settingManager,
        IRepository<MaterialLocationWiseStock> materialLocationWiseStockRepo,
        IRepository<LocationWiseMaterialRateView> locationWiseMaterialRateViewRepo,
        IRepository<PurchaseReturn> purchaseReturn,
        IPurchaseReturnAppService purchaseReturnAppService,
        IRepository<LocationGroup> locationGroupRepo,
        IRepository<InventoryCycleLinkWithLocation> inventoryCycleLinkLocation,
        IMaterialAppService materialAppService,
        IRepository<MenuItemWastageDetail> menuitemwastageDetailRepo,
        IRepository<MenuItemWastage> menuitemwastageRepo,
        IRepository<ClosingStockUnitWiseDetail> closingStockUnitWiseDetailRepo,
        ICategoryReportAppService categoryReportAppService,
        IUnitOfWorkManager unitOfWorkManager,
        IRepository<MaterialGroup> materialGroupRepo,
        IRepository<PurchaseReturnDetail> purchaseReturnDetailRepo,
        ILocationAppService locService,
        IReportBackgroundAppService rbas,
        IBackgroundJobManager bgm
            )
        {
            _housereportManager = housereportManager;
            _housereportRepo = houseReportRepo;
            _housereportExporter = housereportExporter;
            _materialLedgerRepo = materialLedgerRepo;
            _materialRepo = materialRepo;
            _invoiceRepo = invoiceRepo;
            _invoiceDetailRepo = invoiceDetailRepo;
            _supplierRepo = supplierRepo;
            _unitrepo = unitrepo;
            _invoiceAppService = invoiceAppService;
            _locationRepo = locationRepo;
            _connectReportAppService = connectReportAppService;
            _materialmenumappingRepo = materialmenumappingRepo;
            _issuedetailRepo = issuedetailRepo;
            _issueRepo = issueRepo;
            _productionRepo = productionRepo;
            _productindetailRepo = productindetailRepo;
            _yieldRepo = yieldRepo;
            _yieldinputRepo = yieldinputRepo;
            _yieldoutputRepo = yieldoutputRepo;
            _productionunitRepo = productionunitRepo;
            _returndetailRepo = returndetailRepo;
            _returnRepo = returnRepo;
            _materialRecipeTypeRepo = materialRecipeTypeRepo;
            _materialingredientRepo = materialingredientRepo;
            _supplierMaterialRepo = supplierMaterialRepo;
            _unitConversionRepo = unitConversionRepo;
            _unitConversionAppService = unitConversionAppService;
            _menuitemportionRepo = menuitemportionRepo;
            _menuitemRepo = menuitemRepo;
            _intertransferRepo = intertransferRepo;
            _intertransferDetailRepo = intertransferDetailRepo;
            _intertransferReceivedRepo = intertransferReceivedRepo;
            _departmentRepo = departmentRepo;
            _menumappingdepartmentRepo = menumappingdepartmentRepo;
            _logger = logger;
            _closingStockRepo = closingStockRepo;
            _closingStockDetailRepo = closingStockDetailRepo;
            _adjustmentRepo = adjustmentRepo;
            _materialStockCycleRepo = materialStockCycleRepo;
            _inventoryCycleTagRepo = inventoryCycleTagRepo;
            _xmlandjsonConvertorAppService = xmlandjsonConvertorAppService;
            _materialLedgerAppService = materialLedgerAppService;
            _adjustmentDetailRepo = adjustmentDetailRepo;
            _materialGroupCategoryRepo = materialGroupCategoryRepo;
            _settingManager = settingManager;
            _materialUnitLinkRepo = materialUnitLinkRepo;
            _materialLocationWiseStockRepo = materialLocationWiseStockRepo;
            roundDecimals = _settingManager.GetSettingValue<int>(AppSettings.HouseSettings.Decimals);
            _locationWiseMaterialRateViewRepo = locationWiseMaterialRateViewRepo;
            _purchaseReturn = purchaseReturn;
            _purchaseReturnAppService = purchaseReturnAppService;
            _locationGroupRepo = locationGroupRepo;
            _inventoryCycleLinkLocation = inventoryCycleLinkLocation;
            _materialAppService = materialAppService;
            _menuitemwastageRepo = menuitemwastageRepo;
            _menuitemwastageDetailRepo = menuitemwastageDetailRepo;
            _closingStockUnitWiseDetailRepo = closingStockUnitWiseDetailRepo;
            _categoryReportAppService = categoryReportAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _materialGroupRepo = materialGroupRepo;
            _locService = locService;
            _purchaseReturnDetailRepo = purchaseReturnDetailRepo;
            _rbas = rbas;
            _bgm = bgm;
        }

        public async Task<GetProductAnalysisViewDtoOutput> GetProductAnalysisView(GetHouseReportInput input)
        {
            List<ProductAnalysisViewDto> prda = new List<ProductAnalysisViewDto>();

            decimal OpenBalance = 0.0m;
            decimal ReceivedQty = 0.0m;
            decimal IssuedTotQty = 0.0m;
            decimal ClBalance = 0.0m;

            var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();
            var rsUnits = await _unitrepo.GetAll().ToListAsync();

            var rsUnQuery = _unitConversionAppService.GetAllUnitConversionWithBaseIdsQueryable();

            var rsMaterial = _materialRepo.GetAll().WhereIf(input.IsHighValueItemOnly, t => t.IsHighValueItem == true);

            if (!input.MaterialName.IsNullOrEmpty())
                rsMaterial = rsMaterial.Where(t => t.MaterialName.ToUpper().Contains(input.MaterialName.ToUpper()));

            var rsInvoices = (await _invoiceRepo.GetAll().Where(inv => DbFunctions.TruncateTime(inv.AccountDate) >= input.StartDate.Date && DbFunctions.TruncateTime(inv.AccountDate) <= input.EndDate.Date && inv.LocationRefId == input.LocationRefId).ToListAsync());

            int[] invRefIds = rsInvoices.Select(t => t.Id).ToArray();
            int[] materialRefIds = rsMaterial.Select(t => t.Id).ToArray();

            var rsInvoiceDetail = _invoiceDetailRepo.GetAll().Where(t => invRefIds.Contains(t.InvoiceRefId) && materialRefIds.Contains(t.MaterialRefId));

            var matInvListGroup = (from inv in rsInvoices
                                   join invDet in rsInvoiceDetail on inv.Id equals invDet.InvoiceRefId
                                   join mat in rsMaterial
                                   on invDet.MaterialRefId equals mat.Id
                                   join un in _unitrepo.GetAll() on mat.DefaultUnitId equals un.Id
                                   join uiss in _unitrepo.GetAll() on invDet.UnitRefId equals uiss.Id
                                   group invDet by new { invDet.MaterialRefId, mat.MaterialName, un.Id, un.Name, invDet.UnitRefId, invDet.Price } into g
                                   select new ProductAnalysisViewDto
                                   {
                                       MaterialRefId = g.Key.MaterialRefId,
                                       MaterialName = g.Key.MaterialName,
                                       TotalQty = g.Sum(s => s.TotalQty),
                                       TotalAmount = g.Sum(s => s.TotalAmount),
                                       Price = g.Key.Price,
                                       Uom = g.Key.Name,
                                       DefaultUnitName = g.Key.Name,
                                       DefaultUnitId = g.Key.Id,
                                       UnitRefId = g.Key.UnitRefId
                                   }).ToList();

            var matInvListGroupWithUnitConversion = (from salemenu in matInvListGroup
                                                     join uc in rsUc
                                                     on salemenu.UnitRefId equals uc.BaseUnitId
                                                     where uc.RefUnitId == salemenu.DefaultUnitId
                                                     group salemenu by new
                                                     {
                                                         salemenu.MaterialRefId,
                                                         salemenu.MaterialName,
                                                         salemenu.DefaultUnitId,
                                                         salemenu.DefaultUnitName,
                                                         uc.Conversion,
                                                         salemenu.Price,
                                                     } into g
                                                     select new ProductAnalysisViewDto
                                                     {
                                                         MaterialRefId = g.Key.MaterialRefId,
                                                         MaterialName = g.Key.MaterialName,
                                                         DefaultUnitId = g.Key.DefaultUnitId,
                                                         DefaultUnitName = g.Key.DefaultUnitName,
                                                         Price = g.Key.Price * 1 / g.Key.Conversion,
                                                         TotalQty = g.Sum(t => t.TotalQty * g.Key.Conversion),
                                                         TotalAmount = g.Sum(t => t.TotalAmount),
                                                         AvgQty = Math.Round(g.Sum(s => s.TotalQty * g.Key.Conversion) / g.Count(), 14),
                                                         AvgPrice = g.Sum(s => s.TotalAmount) > 0 ? Math.Round(g.Sum(s => s.TotalAmount) / g.Sum(s => s.TotalQty * g.Key.Conversion), roundDecimals) : 0M,
                                                     }).ToList().OrderBy(t => t.MaterialRefId);

            var matInvList = (from salemenu in matInvListGroupWithUnitConversion
                              group salemenu by new { salemenu.MaterialRefId, salemenu.MaterialName, salemenu.DefaultUnitId, salemenu.DefaultUnitName } into g
                              select new ProductAnalysisViewDto
                              {
                                  MaterialRefId = g.Key.MaterialRefId,
                                  MaterialName = g.Key.MaterialName,
                                  DefaultUnitName = g.Key.DefaultUnitName,
                                  Uom = g.Key.DefaultUnitName,
                                  DefaultUnitId = g.Key.DefaultUnitId,
                                  TotalQty = g.Sum(t => t.TotalQty),
                                  TotalAmount = g.Sum(t => t.TotalAmount),
                                  MinPrice = g.Min(t => t.Price),
                                  MaxPrice = g.Max(t => t.Price),
                                  AvgPrice = Math.Round(g.Sum(s => s.TotalAmount) / g.Sum(s => s.TotalQty), roundDecimals),
                                  AvgQty = Math.Round(g.Sum(s => s.TotalQty) / g.Count(), 14),
                              }).ToList();

            foreach (var items in matInvList)
            {
                ProductAnalysisViewDto m = new ProductAnalysisViewDto();
                m.MaterialRefId = items.MaterialRefId;
                m.MaterialName = items.MaterialName;
                m.AvgQty = items.AvgQty;
                m.AvgPrice = Math.Round(items.AvgPrice, roundDecimals, MidpointRounding.AwayFromZero);
                m.TotalQty = items.TotalQty;
                m.TotalAmount = items.TotalAmount;
                m.Uom = items.DefaultUnitName;

                var matLedger = _materialLedgerRepo.GetAll()
                    .Where(a => a.MaterialRefId == items.MaterialRefId && a.LocationRefId == input.LocationRefId && (DbFunctions.TruncateTime(a.LedgerDate) >= input.StartDate.Date && DbFunctions.TruncateTime(a.LedgerDate) <= input.EndDate.Date)).ToList();

                if (matLedger.Count > 0)
                {
                    OpenBalance = matLedger[0].OpenBalance;
                    ReceivedQty = matLedger.Sum(a => a.Received + a.ExcessReceived + a.Return + a.TransferIn);
                    IssuedTotQty = matLedger.Sum(a => a.Issued + a.Sales + a.Damaged + a.SupplierReturn + a.Shortage + a.TransferOut);
                    ClBalance = matLedger[matLedger.Count - 1].ClBalance;
                }
                else
                {
                    ReceivedQty = 0;
                    IssuedTotQty = 0;
                    OpenBalance = 0;
                    ClBalance = 0;
                }

                m.OpenBalance = OpenBalance;
                m.Issue = IssuedTotQty;
                m.MaxPrice = Math.Round(items.MaxPrice, roundDecimals, MidpointRounding.AwayFromZero);
                m.MinPrice = Math.Round(items.MinPrice, roundDecimals, MidpointRounding.AwayFromZero);
                m.Receipt = ReceivedQty;
                m.ClBalance = ClBalance;

                if (m.ClBalance != (m.OpenBalance + m.Receipt - m.Issue))
                {
                    int i = 0;
                }

                List<SupplierAveragePriceViewForMaterialListDto> matSupList = new List<SupplierAveragePriceViewForMaterialListDto>();

                var supavgListGroup = (from inv in _invoiceRepo.GetAll()
                                       where (DbFunctions.TruncateTime(inv.AccountDate) >= input.StartDate.Date && DbFunctions.TruncateTime(inv.AccountDate) <= input.EndDate.Date)
                                       join invDet in _invoiceDetailRepo.GetAll() on inv.Id equals invDet.InvoiceRefId
                                       join mat in _materialRepo.GetAll().Where(t => t.Id == m.MaterialRefId) on invDet.MaterialRefId equals mat.Id
                                       join un in _unitrepo.GetAll() on mat.DefaultUnitId equals un.Id
                                       join sup in _supplierRepo.GetAll() on inv.SupplierRefId equals sup.Id
                                       group invDet by new { invDet.MaterialRefId, invDet.UnitRefId, mat.MaterialName, inv.SupplierRefId, sup.SupplierName, mat.DefaultUnitId, un.Name } into g
                                       select new SupplierAveragePriceViewForMaterialListDto
                                       {
                                           SupplierRefId = g.Key.SupplierRefId,
                                           SupplierName = g.Key.SupplierName,
                                           MaterialRefId = g.Key.MaterialRefId,
                                           MaterialName = g.Key.MaterialName,
                                           UnitRefId = g.Key.UnitRefId,
                                           DefaultUnitId = g.Key.DefaultUnitId,
                                           DefaultUnitName = g.Key.Name,
                                           BilledQty = g.Sum(s => s.TotalQty),
                                           MaterialAveragePrice = m.AvgPrice,
                                           PurchaseAmount = g.Sum(s => s.TotalAmount),
                                       }).Distinct().ToList();

                var supavgListGroupWithUnitConversion = (from salemenu in supavgListGroup
                                                         join uc in rsUc
                                                             on salemenu.UnitRefId equals uc.BaseUnitId
                                                         where uc.RefUnitId == salemenu.DefaultUnitId
                                                         group salemenu by new
                                                         {
                                                             salemenu.SupplierRefId,
                                                             salemenu.SupplierName,
                                                             salemenu.MaterialRefId,
                                                             salemenu.MaterialName,
                                                             salemenu.DefaultUnitId,
                                                             salemenu.DefaultUnitName,
                                                             uc.Conversion,
                                                         } into g
                                                         select new SupplierAveragePriceViewForMaterialListDto
                                                         {
                                                             SupplierRefId = g.Key.SupplierRefId,
                                                             SupplierName = g.Key.SupplierName,
                                                             MaterialRefId = g.Key.MaterialRefId,
                                                             MaterialName = g.Key.MaterialName,
                                                             DefaultUnitId = g.Key.DefaultUnitId,
                                                             DefaultUnitName = g.Key.DefaultUnitName,
                                                             BilledQty = g.Sum(t => t.BilledQty * g.Key.Conversion),
                                                             PurchaseAmount = g.Sum(t => t.PurchaseAmount),
                                                             Price = g.Sum(t => t.PurchaseAmount) > 0 ? g.Sum(t => t.PurchaseAmount) / g.Sum(t => t.BilledQty * g.Key.Conversion) : 0
                                                         }).ToList().OrderBy(t => t.MaterialRefId);

                var supavgList = (from salemenu in supavgListGroupWithUnitConversion
                                  group salemenu by new
                                  {
                                      salemenu.SupplierRefId,
                                      salemenu.SupplierName,
                                      salemenu.MaterialRefId,
                                      salemenu.MaterialName,
                                      salemenu.DefaultUnitId,
                                      salemenu.DefaultUnitName,
                                  } into g
                                  select new SupplierAveragePriceViewForMaterialListDto
                                  {
                                      SupplierRefId = g.Key.SupplierRefId,
                                      SupplierName = g.Key.SupplierName,
                                      MaterialRefId = g.Key.MaterialRefId,
                                      MaterialName = g.Key.MaterialName,
                                      DefaultUnitId = g.Key.DefaultUnitId,
                                      DefaultUnitName = g.Key.DefaultUnitName,
                                      BilledQty = g.Sum(t => t.BilledQty),
                                      PurchaseAmount = g.Sum(t => t.PurchaseAmount),
                                      Price = g.Sum(t => t.PurchaseAmount) / g.Sum(t => t.BilledQty),
                                      MaterialAveragePrice = m.AvgPrice
                                  }).ToList();

                foreach (var sup in supavgList)
                {
                    SupplierAveragePriceViewForMaterialListDto s = new SupplierAveragePriceViewForMaterialListDto();
                    s.MaterialRefId = sup.MaterialRefId;
                    s.MaterialName = sup.MaterialName;
                    s.BilledQty = sup.BilledQty;
                    s.SupplierRefId = sup.SupplierRefId;
                    s.SupplierName = sup.SupplierName;
                    s.Price = sup.Price;
                    s.MaterialAveragePrice = sup.MaterialAveragePrice;
                    s.PurchaseAmount = sup.PurchaseAmount;
                    matSupList.Add(s);
                }

                List<PurchaseParticularMaterialSupplierWise> purchaseDetailsForMaterial = new List<PurchaseParticularMaterialSupplierWise>();

                var supdet = (from inv in _invoiceRepo.GetAll()
                              where (DbFunctions.TruncateTime(inv.AccountDate) >= input.StartDate.Date && DbFunctions.TruncateTime(inv.AccountDate) <= input.EndDate.Date)
                              join invDet in _invoiceDetailRepo.GetAll() on inv.Id equals invDet.InvoiceRefId
                              join mat in _materialRepo.GetAll().Where(t => t.Id == m.MaterialRefId) on invDet.MaterialRefId equals mat.Id
                              join sup in _supplierRepo.GetAll() on inv.SupplierRefId equals sup.Id
                              join un in _unitrepo.GetAll() on mat.DefaultUnitId equals un.Id
                              join uiss in _unitrepo.GetAll() on invDet.UnitRefId equals uiss.Id
                              select new PurchaseParticularMaterialSupplierWise
                              {
                                  SupplierRefId = sup.Id,
                                  SupplierName = sup.SupplierName,
                                  MaterialRefId = mat.Id,
                                  MaterialName = mat.MaterialName,
                                  PurchaseDate = inv.AccountDate,
                                  InvoiceRef = inv.InvoiceNumber,
                                  BilledQty = invDet.TotalQty,
                                  Price = invDet.Price,
                                  PurchaseAmount = invDet.TotalAmount,
                                  MaterialAveragePrice = m.AvgPrice,
                                  DefaultUnitId = mat.DefaultUnitId,
                                  DefaultUnitName = un.Name,
                                  UnitRefId = invDet.UnitRefId,
                                  UnitRefName = uiss.Name
                              }).ToList();

                m.SupplierAveragePrice = matSupList;
                m.PurchaseDetailForParticularMaterial = supdet;

                prda.Add(m);
            }
            string columnDefs = "{name: app.localize('MaterialName'),field: 'materialName',width:340},{name: app.localize('OpeningStock'),field: 'openBalance'},{name: app.localize('Received'),field: 'received'},{name: app.localize('Excess'),field: 'excessReceived'},{name: app.localize('Issued'),field: 'issued'},{name: app.localize('Damage'),field: 'damaged'},{name: app.localize('Shortage'),field: 'shortage'},{name: app.localize('Return'),field: 'return'},{name: app.localize('ClosingStock'),field: 'clBalance'}";
            if (input.Sorting == null)
                input.Sorting = "MaterialName";
            prda = prda.OrderBy(input.Sorting).ToList();

            return new GetProductAnalysisViewDtoOutput
            {
                ProductAnalysis = prda,
                totalCount = prda.Count(),
                ColumnDefs = columnDefs
            };
        }

        public async Task<GetSupplierRateViewDtoOutput> GetSupplierRateView(GetHouseReportInput input)
        {
            var rsUnitConversion = await _unitConversionRepo.GetAllListAsync();

            var rsUnit = await _unitrepo.GetAllListAsync();

            List<SupplierRateViewListDto> mrv = new List<SupplierRateViewListDto>();

            List<SupplierRateViewListDto> returnOutput = new List<SupplierRateViewListDto>();

            var allMaterialsQb = _materialRepo.GetAll();

            if (!input.Filter.IsNullOrEmpty())
            {
                allMaterialsQb = allMaterialsQb.Where(t => t.MaterialName.Contains(input.Filter));
            }

            if (input.IsHighValueItemOnly)
            {
                allMaterialsQb = allMaterialsQb.Where(t => t.IsHighValueItem == true);
            }

            if (input.MaterialTypeList != null && input.MaterialTypeList.Count > 0)
            {
                var stridList = input.MaterialTypeList.Select(t => t.Value).ToArray();
                int[] myMaterialIdList = Array.ConvertAll(stridList, s => int.Parse(s));

                allMaterialsQb = allMaterialsQb.Where(t => myMaterialIdList.Contains(t.MaterialTypeId));
            }

            var matInvList = (from inv in _invoiceRepo.GetAll()
                              where (DbFunctions.TruncateTime(inv.AccountDate) >= input.StartDate.Date &&
                          DbFunctions.TruncateTime(inv.AccountDate) <= input.EndDate.Date && inv.LocationRefId == input.LocationRefId)
                              join invDet in _invoiceDetailRepo.GetAll() on inv.Id equals invDet.InvoiceRefId
                              join mat in allMaterialsQb
                          on invDet.MaterialRefId equals mat.Id
                              join sup in _supplierRepo.GetAll() on inv.SupplierRefId equals sup.Id
                              group invDet by new { inv.InvoiceDate, invDet.MaterialRefId, mat.MaterialName, inv.SupplierRefId, sup.SupplierName, invDet.UnitRefId, mat.DefaultUnitId } into g
                              select new SupplierRateViewListDto
                              {
                                  MaterialRefId = g.Key.MaterialRefId,
                                  MaterialName = g.Key.MaterialName,
                                  SupplierRefId = g.Key.SupplierRefId,
                                  SupplierName = g.Key.SupplierName,
                                  BilledQty = g.Sum(s => s.TotalQty),
                                  NetAmount = g.Sum(s => s.Price * s.TotalQty),
                                  UnitRefId = g.Key.UnitRefId,
                                  DefaultUnitId = g.Key.DefaultUnitId
                              }).Distinct();

            SupplierRateViewListDto matDto = new SupplierRateViewListDto();
            decimal conversionFactor;
            decimal totalQty = 0;
            decimal netAmount = 0;
            decimal avgRate = 0;

            foreach (var lst in matInvList.OrderBy(t => t.UnitRefId))
            {
                matDto = lst;
                if (lst.DefaultUnitId == lst.UnitRefId)
                {
                    conversionFactor = 1;
                }
                else
                {
                    var unitConversion = rsUnitConversion.FirstOrDefault(t => t.BaseUnitId == lst.UnitRefId && t.RefUnitId == lst.DefaultUnitId);
                    if (unitConversion != null)
                    {
                        conversionFactor = unitConversion.Conversion;
                    }
                    else
                    {
                        unitConversion = rsUnitConversion.FirstOrDefault(t => t.BaseUnitId == lst.UnitRefId && t.RefUnitId == lst.DefaultUnitId);
                        if (unitConversion == null)
                        {
                            var baseUnit = await _unitrepo.FirstOrDefaultAsync(t => t.Id == lst.UnitRefId);
                            var refUnit = await _unitrepo.FirstOrDefaultAsync(t => t.Id == lst.DefaultUnitId);

                            throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
                        }
                        conversionFactor = 1 / unitConversion.Conversion;
                    }
                }
                totalQty = (lst.BilledQty * conversionFactor);
                netAmount = lst.NetAmount;
                avgRate = netAmount > 0 ? netAmount / totalQty : 0M;

                matDto.Rate = avgRate;
                matDto.BilledQty = totalQty;
                returnOutput.Add(matDto);
            }

            if (input.Sorting == null)
            {
                input.Sorting = "MaterialName";
            }
            returnOutput = returnOutput.OrderBy(input.Sorting).ToList();

            string columnDefs = "{name: app.localize('MaterialName'),field: 'materialName',width:340},{name: app.localize('OpeningStock'),field: 'openBalance'},{name: app.localize('Received'),field: 'received'},{name: app.localize('Excess'),field: 'excessReceived'},{name: app.localize('Issued'),field: 'issued'},{name: app.localize('Damage'),field: 'damaged'},{name: app.localize('Shortage'),field: 'shortage'},{name: app.localize('Return'),field: 'return'},{name: app.localize('ClosingStock'),field: 'clBalance'}";

            return new GetSupplierRateViewDtoOutput
            {
                SupplierRateView = returnOutput,
                totalCount = mrv.Count(),
                ColumnDefs = columnDefs
            };
        }

        public async Task<GetMaterialRateViewDtoOutput> GetMaterialRateView(GetHouseReportMaterialRateInput input)
        {
            var calledBy = input.FunctionCalledBy;
            if (input.RecursiveCount > 0)
            {
                if (input.RecursiveCount > 5)
                    return new GetMaterialRateViewDtoOutput { MaterialRateView = new List<MaterialRateViewListDto>() };
            }

            var isOmitFOCMaterialQuantities = await _settingManager.GetSettingValueAsync<bool>(AppSettings.HouseSettings.IsOmitFOCMaterialQuantities);
            var isCalculateAverageRateWithoutTax = await _settingManager.GetSettingValueAsync<bool>(AppSettings.HouseSettings.IsCalculateAverageRateWithoutTax);

            #region ValidateInput

            if (input.MaterialRefIds == null)
            {
                input.MaterialRefIds = new List<int>();
            }

            if (!input.MaterialRefNameLike.IsNullOrEmpty())
            {
                List<int> materialList = await _materialRepo.GetAll().Where(t => t.MaterialName.Contains(input.MaterialRefNameLike) || t.MaterialPetName.Contains(input.MaterialRefNameLike)).Select(t => t.Id).ToListAsync();
                if (materialList.Count > 0)
                {
                    input.MaterialRefIds.AddRange(materialList);
                }
            }

            if (input.StartDate.Equals(DateTime.MinValue))
            {
                input.StartDate = DateTime.Now;
            }

            if (input.EndDate.Equals(DateTime.MinValue))
            {
                input.EndDate = DateTime.Now;
            }

            var totalHours = input.EndDate.Subtract(input.StartDate).TotalHours;
            if (totalHours <= 24 && !input.EndDate.Day.Equals(input.StartDate.Day))
            {
                input.StartDate = input.StartDate;
                input.EndDate = input.StartDate;
            }

            DateTime startDate;
            DateTime endDate;

            startDate = input.StartDate;
            endDate = input.EndDate;

            DateTime argStartDate;
            DateTime argEndDate;

            argStartDate = startDate;
            argEndDate = endDate;

            if (input.LocationRefId.HasValue)
            {
                var locs = await _locationRepo.GetAll().Where(t => t.Id == input.LocationRefId).ToListAsync();
                if (locs.Count > 0)
                {
                    if (locs[0].AvgPriceLocationRefId.HasValue)
                    {
                        var reqLocationRefId = locs[0].AvgPriceLocationRefId.Value;
                        var tempLocs = await _locationRepo.GetAll().Where(t => t.Id == reqLocationRefId).ToListAsync();
                        locs = tempLocs;
                    }
                }

                input.Locations = new List<LocationListDto>();
                var loclist = locs.MapTo<List<LocationListDto>>();
                input.Locations.AddRange(loclist);
            }
            else if (input.LocationRefId.HasValue == false)
            {
                if (input.Locations == null)
                {
                    throw new UserFriendlyException(L("For Getting Material Rate , Location List or Specific Location Need to specify "));
                }
                if (input.Locations.Count() == 1)
                {
                    input.LocationRefId = input.Locations.FirstOrDefault().Id;
                }
            }

            #endregion ValidateInput

            #region SelectMaterialList

            if (input.DoesIngredientsDetailsRequired == true && input.MaterialRefIds != null && input.MaterialRefIds.Count < 10)
            {
                input.ForceRefreshFlag = true;
            }

            List<int> rateRequiredMaterialList = new List<int>();
            var allMaterialsQb = _materialRepo.GetAll().WhereIf(input.IsHighValueItemOnly, t => t.IsHighValueItem == true);

            if (input.RunInBackGround && input.TenantId > 0)
                allMaterialsQb = allMaterialsQb.Where(t => t.TenantId == input.TenantId);

            List<int> arrNeededSemiMaterialList = new List<int>();
            List<int> extraRawIngredients = new List<int>();
            if (input.MaterialRefIds != null && input.MaterialRefIds.Count > 0)
            {
                var arrMaterialTypes = await _materialRepo.GetAll().Where(t => input.MaterialRefIds.Contains(t.Id))
                        .Select(t => t.MaterialTypeId).Distinct().ToListAsync();
                //List<int> arrMaterialTypes = materialTypes.Select(t => t.MaterialTypeId).ToList();
                if (!arrMaterialTypes.Contains((int)MaterialType.SEMI))
                {
                    allMaterialsQb = allMaterialsQb.Where(t => input.MaterialRefIds.Contains(t.Id));
                }
                else
                {
                    var materialSemi = await _materialRepo.GetAll().Where(t => t.MaterialTypeId == (int)MaterialType.SEMI && input.MaterialRefIds.Contains(t.Id)).Select(t => t.Id).ToListAsync();
                    arrNeededSemiMaterialList.AddRange(materialSemi);
                    input.MaterialRefIds.AddRange(materialSemi);

                    materialSemi = await _materialRepo.GetAll().Where(t => t.MaterialTypeId == (int)MaterialType.SEMI && input.MaterialRefIds.Contains(t.Id)).Select(t => t.Id).ToListAsync();
                    arrNeededSemiMaterialList.AddRange(materialSemi);
                    input.MaterialRefIds.AddRange(materialSemi);
                }

                var arrIngrMaterialRefIds = await _materialingredientRepo.GetAll().Where(t => input.MaterialRefIds.Contains(t.RecipeRefId)).Select(t => t.MaterialRefId).Distinct().ToListAsync();
                arrMaterialTypes = await _materialRepo.GetAll().Where(t => arrIngrMaterialRefIds.Contains(t.Id))
                        .Select(t => t.MaterialTypeId).Distinct().ToListAsync();
                if (!arrMaterialTypes.Contains((int)MaterialType.SEMI))
                {
                    allMaterialsQb = allMaterialsQb.Where(t => input.MaterialRefIds.Contains(t.Id));
                }
                else
                {
                    List<int> arrRevisedList = new List<int>();
                    arrRevisedList.AddRange(arrIngrMaterialRefIds);
                    arrRevisedList.AddRange(input.MaterialRefIds);

                    arrIngrMaterialRefIds = await _materialingredientRepo.GetAll().Where(t => arrRevisedList.Contains(t.RecipeRefId)).Select(t => t.MaterialRefId).Distinct().ToListAsync();
                    arrRevisedList.AddRange(arrIngrMaterialRefIds);

                    arrIngrMaterialRefIds = await _materialingredientRepo.GetAll().Where(t => arrRevisedList.Contains(t.RecipeRefId)).Select(t => t.MaterialRefId).Distinct().ToListAsync();
                    arrRevisedList.AddRange(arrIngrMaterialRefIds);

                    input.MaterialRefIds = arrRevisedList;
                    allMaterialsQb = allMaterialsQb.Where(t => input.MaterialRefIds.Contains(t.Id));
                }
            }
            rateRequiredMaterialList = await allMaterialsQb.Select(t => t.Id).ToListAsync();

            #endregion SelectMaterialList

            var rsAveragePriceRequiredMaterialList = await allMaterialsQb.ToListAsync();

            string columnDefs = "{name: app.localize('MaterialName'),field: 'materialName',width:340},{name: app.localize('OpeningStock'),field: 'openBalance'},{name: app.localize('Received'),field: 'received'},{name: app.localize('Excess'),field: 'excessReceived'},{name: app.localize('Issued'),field: 'issued'},{name: app.localize('Damage'),field: 'damaged'},{name: app.localize('Shortage'),field: 'shortage'},{name: app.localize('Return'),field: 'return'},{name: app.localize('ClosingStock'),field: 'clBalance'}";

            GetMaterialRateViewDtoOutput resultoutput = new GetMaterialRateViewDtoOutput();
            resultoutput.ColumnDefs = columnDefs;

            var unitQueryable = _unitrepo.GetAll();
            if (input.RunInBackGround)
                unitQueryable = unitQueryable.Where(t => t.TenantId == input.TenantId);

            var rsUnits = await unitQueryable.ToListAsync();

            List<LocationWiseMaterialRateViewListDto> locationWiseMaterialRateView = new List<LocationWiseMaterialRateViewListDto>();
            if (input.ForceRefreshFlag)
            {
                locationWiseMaterialRateView = new List<LocationWiseMaterialRateViewListDto>();
            }
            else
            {
                var tempLocRateView = await _locationWiseMaterialRateViewRepo.GetAllListAsync(t => t.LocationRefId == input.LocationRefId && t.IsOmitFOCMaterialQuantities == isOmitFOCMaterialQuantities && t.IsCalculateAverageRateWithoutTax == isCalculateAverageRateWithoutTax);
                tempLocRateView = tempLocRateView.Where(t => t.StartDate.Date == argStartDate.Date && t.EndDate.Date == argEndDate).ToList();
                tempLocRateView = tempLocRateView.Where(t => t.IsOmitFOCMaterialQuantities == isOmitFOCMaterialQuantities && t.IsCalculateAverageRateWithoutTax == isCalculateAverageRateWithoutTax).ToList();
                locationWiseMaterialRateView = tempLocRateView.MapTo<List<LocationWiseMaterialRateViewListDto>>();
                var tempAllMaterialRefIds = locationWiseMaterialRateView.Select(t => t.MaterialRefId).ToList();
                var tempSemiMaterialRefIds = await _materialRepo.GetAll().Where(t => t.MaterialTypeId == (int)MaterialType.SEMI && tempAllMaterialRefIds.Contains(t.Id)).Select(t => t.Id).ToListAsync();
                foreach (var lst in locationWiseMaterialRateView.Where(t => tempSemiMaterialRefIds.Contains(t.MaterialRefId)))
                {
                    lst.MaterialTypeId = (int)MaterialType.SEMI;
                }
            }

            List<MaterialRateViewListDto> materialRateViewListDtos = new List<MaterialRateViewListDto>();

            var SemiMaterialTypeList = rsAveragePriceRequiredMaterialList.Where(t => t.MaterialTypeId == (int)MaterialType.SEMI).ToList();
            List<int> materialRefIds_SemiMaterialTypeList = SemiMaterialTypeList.Select(t => t.Id).ToList();

            //var MaterialToBeGetPriceBasedOnTags = rsAveragePriceRequiredMaterialList.Where(t => t.MaterialTypeId == (int)MaterialType.RAW).ToList();
            var MaterialToBeGetPriceBasedOnTags = rsAveragePriceRequiredMaterialList.ToList();
            List<int> materialRefIds_MaterialToBeGetPriceBasedOnTags = MaterialToBeGetPriceBasedOnTags.Select(t => t.Id).ToList();
            var alreadyRefreshedMaterialToBeGetPriceBasedOnTags = locationWiseMaterialRateView.Where(t => t.RefreshRequired == false && materialRefIds_MaterialToBeGetPriceBasedOnTags.Contains(t.MaterialRefId)).ToList();
            materialRateViewListDtos.AddRange(alreadyRefreshedMaterialToBeGetPriceBasedOnTags.MapTo<List<MaterialRateViewListDto>>());
            List<int> refreshed_MaterialToBeGetPriceBasedOnTags = alreadyRefreshedMaterialToBeGetPriceBasedOnTags.Select(t => t.MaterialRefId).ToList();
            materialRefIds_MaterialToBeGetPriceBasedOnTags = materialRefIds_MaterialToBeGetPriceBasedOnTags.Except(refreshed_MaterialToBeGetPriceBasedOnTags).ToList();
            MaterialToBeGetPriceBasedOnTags = MaterialToBeGetPriceBasedOnTags.Where(t => materialRefIds_MaterialToBeGetPriceBasedOnTags.Contains(t.Id)).ToList();

            if (MaterialToBeGetPriceBasedOnTags.Count > 0)
            {
                foreach (var lstBasedOnAveragePriceTag in MaterialToBeGetPriceBasedOnTags.GroupBy(t => t.AveragePriceTagRefId))
                {
                    if (lstBasedOnAveragePriceTag.Key == (int)AveragePriceTag.Given_Dates_Average)
                    {
                        GetHouseReportMaterialRateInput given_dates_average_input = input;
                        var tempMaterialList = lstBasedOnAveragePriceTag.ToList();
                        List<int> materialRefIds_given_dates_average = tempMaterialList.Select(t => t.Id).ToList();
                        var alreadyRefreshedMaterials = locationWiseMaterialRateView.Where(t => t.AveragePriceTagRefId == (int)AveragePriceTag.Given_Dates_Average && t.RefreshRequired == false && materialRefIds_given_dates_average.Contains(t.MaterialRefId)).ToList();
                        materialRateViewListDtos.AddRange(alreadyRefreshedMaterials.MapTo<List<MaterialRateViewListDto>>());
                        List<int> refreshedMaterialRefIds = alreadyRefreshedMaterials.Select(t => t.MaterialRefId).ToList();
                        materialRefIds_given_dates_average = materialRefIds_given_dates_average.Except(refreshedMaterialRefIds).ToList();
                        given_dates_average_input.MaterialRefIds = materialRefIds_given_dates_average;
                        if (materialRefIds_given_dates_average.Count > 0)
                        {
                            var given_dates_average = await GetMaterialRateView_Given_Dates_Average(given_dates_average_input);
                            materialRateViewListDtos.AddRange(given_dates_average.MaterialRateView);
                        }
                    }
                    else if (lstBasedOnAveragePriceTag.Key == (int)AveragePriceTag.Last_Purchase)
                    {
                        GetHouseReportMaterialRateInput last_purchase_average_input = input;
                        var tempMaterialList = lstBasedOnAveragePriceTag.ToList();
                        List<int> materialRefIds_last_purchase = tempMaterialList.Select(t => t.Id).ToList();
                        var alreadyRefreshedMaterials = locationWiseMaterialRateView.Where(t => t.AveragePriceTagRefId == (int)AveragePriceTag.Last_Purchase && t.RefreshRequired == false && materialRefIds_last_purchase.Contains(t.MaterialRefId)).ToList();
                        materialRateViewListDtos.AddRange(alreadyRefreshedMaterials.MapTo<List<MaterialRateViewListDto>>());
                        List<int> refreshedMaterialRefIds = alreadyRefreshedMaterials.Select(t => t.MaterialRefId).ToList();
                        materialRefIds_last_purchase = materialRefIds_last_purchase.Except(refreshedMaterialRefIds).ToList();
                        last_purchase_average_input.MaterialRefIds = materialRefIds_last_purchase;
                        if (materialRefIds_last_purchase.Count > 0)
                        {
                            var given_dates_average = await GetMaterialRateView_Last_Purchase(last_purchase_average_input);
                            materialRateViewListDtos.AddRange(given_dates_average.MaterialRateView);
                        }
                    }
                    else if (lstBasedOnAveragePriceTag.Key == (int)AveragePriceTag.Purchase_Avg_Last_N_Months)
                    {
                        GetHouseReportMaterialRateInput purchase_avg_last_n_months = input;
                        var tempMaterialList = lstBasedOnAveragePriceTag.ToList();
                        List<int> materialRefIds_purchase_avg_last_n_months = tempMaterialList.Select(t => t.Id).ToList();
                        var alreadyRefreshedMaterials = locationWiseMaterialRateView.Where(t => t.AveragePriceTagRefId == (int)AveragePriceTag.Purchase_Avg_Last_N_Months && t.RefreshRequired == false && materialRefIds_purchase_avg_last_n_months.Contains(t.MaterialRefId)).ToList();
                        materialRateViewListDtos.AddRange(alreadyRefreshedMaterials.MapTo<List<MaterialRateViewListDto>>());
                        List<int> refreshedMaterialRefIds = alreadyRefreshedMaterials.Select(t => t.MaterialRefId).ToList();
                        materialRefIds_purchase_avg_last_n_months = materialRefIds_purchase_avg_last_n_months.Except(refreshedMaterialRefIds).ToList();
                        purchase_avg_last_n_months.MaterialRefIds = materialRefIds_purchase_avg_last_n_months;
                        if (materialRefIds_purchase_avg_last_n_months.Count > 0)
                        {
                            var given_dates_average = await GetMaterialRateView_Purchase_Avg_Last_N_Months(purchase_avg_last_n_months);
                            materialRateViewListDtos.AddRange(given_dates_average.MaterialRateView);
                        }
                    }
                    else if (lstBasedOnAveragePriceTag.Key == (int)AveragePriceTag.Moving_Average)
                    {
                        GetHouseReportMaterialRateInput moving_Average = input;
                        var tempMaterialList = lstBasedOnAveragePriceTag.ToList();
                        List<int> materialRefIds_purchase_Moving_Average = tempMaterialList.Select(t => t.Id).ToList();
                        var alreadyRefreshedMaterials = locationWiseMaterialRateView.Where(t => t.AveragePriceTagRefId == (int)AveragePriceTag.Moving_Average && t.RefreshRequired == false && materialRefIds_purchase_Moving_Average.Contains(t.MaterialRefId)).ToList();
                        materialRateViewListDtos.AddRange(alreadyRefreshedMaterials.MapTo<List<MaterialRateViewListDto>>());
                        List<int> refreshedMaterialRefIds = alreadyRefreshedMaterials.Select(t => t.MaterialRefId).ToList();
                        materialRefIds_purchase_Moving_Average = materialRefIds_purchase_Moving_Average.Except(refreshedMaterialRefIds).ToList();
                        moving_Average.MaterialRefIds = materialRefIds_purchase_Moving_Average;
                        if (materialRefIds_purchase_Moving_Average.Count > 0)
                        {
                            var given_dates_average = await GetMaterialRateView_Moving_Average(moving_Average);
                            materialRateViewListDtos.AddRange(given_dates_average.MaterialRateView);
                        }
                    }
                }
            }
            else if (MaterialToBeGetPriceBasedOnTags.Count == 0)
            {
                var existMaterialRefIds = materialRateViewListDtos.Select(t => t.MaterialRefId).ToList();
                var alreadyRefreshedSemiMaterialTypeList = locationWiseMaterialRateView.Where(t => t.RefreshRequired == false && materialRefIds_SemiMaterialTypeList.Contains(t.MaterialRefId) && !existMaterialRefIds.Contains(t.MaterialRefId)).ToList();
                if (alreadyRefreshedSemiMaterialTypeList != null || alreadyRefreshedSemiMaterialTypeList.Count > 0)
                {
                    materialRateViewListDtos.AddRange(alreadyRefreshedSemiMaterialTypeList.MapTo<List<MaterialRateViewListDto>>());
                    List<int> refreshed_SemiMaterialTypeList = alreadyRefreshedSemiMaterialTypeList.Select(t => t.MaterialRefId).ToList();
                    materialRefIds_SemiMaterialTypeList = materialRefIds_SemiMaterialTypeList.Except(refreshed_SemiMaterialTypeList).ToList();
                    SemiMaterialTypeList = SemiMaterialTypeList.Where(t => materialRefIds_SemiMaterialTypeList.Contains(t.Id)).ToList();
                }
            }

            #region Semi Finished Material Rate

            //  1. Identify the Semi Finished Materials Purchased
            //  2. Identify the Semi Finished Materials InterTransfer
            //  3. Identify the Semi Finished Materials Production
            //  4. Identify the Semi Finished Materials Yield Production

            bool secondTimeCheckFlag = false;
            List<int> semiFinishedRateZero = materialRateViewListDtos.Where(t => t.MaterialTypeId == (int)MaterialType.SEMI && t.AvgRate == (decimal)0.0m).Select(t => t.MaterialRefId).ToList();
            SemiMaterialTypeList = SemiMaterialTypeList.Where(t => semiFinishedRateZero.Contains(t.Id)).ToList();
            againCheckSemi:
            if (MaterialToBeGetPriceBasedOnTags.Count > 0 && SemiMaterialTypeList.Count > 0)
            {
                var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();

                var rsIngrList = await _materialingredientRepo.GetAllListAsync(t => semiFinishedRateZero.Contains(t.RecipeRefId));
                List<int> requiredRawMaterials = rsIngrList.Select(t => t.MaterialRefId).ToList();
                List<int> rateAvaiableRawMaterial = materialRateViewListDtos.Select(t => t.MaterialRefId).ToList();

                #region Get Missing Ingredients - Raw Materials Price

                List<int> pricerequiredRawMaterials = requiredRawMaterials.Except(rateAvaiableRawMaterial).ToList();
                List<MaterialRateViewListDto> missingMaterialRateList = new List<MaterialRateViewListDto>();
                if (pricerequiredRawMaterials != null && pricerequiredRawMaterials.Any())
                {
                    var missingMaterialRateDto = await GetMaterialRateView(new GetHouseReportMaterialRateInput
                    {
                        LocationRefId = input.LocationRefId,
                        StartDate = input.StartDate,
                        EndDate = input.EndDate,
                        Locations = input.Locations,
                        MaterialRefIds = pricerequiredRawMaterials,
                        RecursiveCount = input.RecursiveCount + 1,
                        FunctionCalledBy = "Get Material Rate View - Recursive"
                    });
                    missingMaterialRateList = missingMaterialRateDto.MaterialRateView;
                }

                #endregion Get Missing Ingredients - Raw Materials Price

                var rsMaterials = await _materialRepo.GetAllListAsync(t => requiredRawMaterials.Contains(t.Id));

                #region Semi Finished Materials Rate Fixation based on its Recipes

                var rsRecipeMaterial = await _materialRecipeTypeRepo.GetAllListAsync();
                foreach (var lst in SemiMaterialTypeList.Where(t => t.MaterialTypeId == (int)MaterialType.SEMI))
                {
                    var recipeMaterial = rsRecipeMaterial.FirstOrDefault(t => t.MaterailRefId == lst.Id);
                    if (recipeMaterial == null)
                    {
                        continue;
                    }
                    //if (recipeMaterial.Id == 564)
                    //{
                    //    int I = 0;
                    //}
                    if (lst.Id == 9 || lst.Id == 4001)
                    {
                        int j = 0;
                    }

                    var ingrList = rsIngrList.Where(t => t.RecipeRefId == lst.Id).ToList();
                    GetRawMaterailBasedOnRecipeInput inp = new GetRawMaterailBasedOnRecipeInput
                    {
                        RecipeRefId = lst.Id,
                        OutputQty = recipeMaterial.PrdBatchQty,
                        UnitConversionLists = rsUc,
                        MaterialIngredients = ingrList,
                        Materials = rsMaterials
                    };
                    inp.MaterialRecipeType = recipeMaterial;
                    var getMatDetailforRecipe = await GetRawMaterialRequiredForRecipe(inp);

                    decimal totalprice = 0;

                    foreach (var m in getMatDetailforRecipe)
                    {
                        var materialSelected = materialRateViewListDtos.FirstOrDefault(t => t.MaterialRefId == m.MaterialRefId);
                        if (materialSelected == null)
                        {
                            materialSelected = missingMaterialRateList.FirstOrDefault(t => t.MaterialRefId == m.MaterialRefId);
                            if (materialSelected == null)
                            {
                                continue;
                            }
                            //var material = await _materialRepo.FirstOrDefaultAsync(t => t.Id == m.MaterialRefId);
                            //throw new UserFriendlyException("Material " + material.MaterialName + " Price not in the List");
                        }
                        decimal matPrice = materialSelected.AvgRate;
                        m.InvoiceDateRange = materialSelected.InvoiceDateRange;
                        m.AvgPrice = materialSelected.AvgRate;
                        m.AveragePriceTagRefId = materialSelected.AveragePriceTagRefId;
                        m.AveragePriceTagRefName = materialSelected.AveragePriceTagRefName;
                        m.NoOfMonthAvgTaken = materialSelected.NoOfMonthAvgTaken;
                        m.MaterialTypeId = materialSelected.MaterialTypeId;
                        m.MaterialTypeRefName = materialSelected.MaterialTypeRefName;
                        var unit = rsUnits.FirstOrDefault(t => t.Id == m.DefaultUnitId);
                        if (unit != null)
                            m.Uom = unit.Name;
                        else
                        {
                            var j = 10;
                            j++;
                        }
                        m.TotalPrice = Math.Round(matPrice * m.MaterialUsedQty, 14);
                        totalprice = totalprice + m.TotalPrice;
                    }

                    decimal avgPrice = 0;
                    if (recipeMaterial != null && totalprice > 0)
                    {
                        if (recipeMaterial.PrdBatchQty != 0)
                            avgPrice = Math.Round(totalprice / recipeMaterial.PrdBatchQty, roundDecimals, MidpointRounding.AwayFromZero);
                    }
                    var existSemiInList = materialRateViewListDtos.FirstOrDefault(t => t.MaterialRefId == lst.Id);
                    if (existSemiInList == null)
                    {
                        MaterialRateViewListDto semiDto = new MaterialRateViewListDto
                        {
                            MaterialTypeId = lst.MaterialTypeId,
                            MaterialRefId = lst.Id,
                            MaterialName = lst.MaterialName,
                            UnitRefId = lst.IssueUnitId,
                            DefaultUnitId = lst.DefaultUnitId,
                            InvoiceDateRange = "Ingredient Based",
                            MaterialIngredientRequiredDtos = getMatDetailforRecipe
                        };
                        semiDto.AvgRate = Math.Round(avgPrice, roundDecimals, MidpointRounding.AwayFromZero);
                        semiDto.PrdBatchQty = recipeMaterial.PrdBatchQty;
                        semiDto.TotalIngrCost = totalprice;
                        if (input.DoesIngredientsDetailsRequired == false)
                            semiDto.MaterialIngredientRequiredDtos = new List<MaterialIngredientRequiredDto>();
                        materialRateViewListDtos.Add(semiDto);
                    }
                    else
                    {
                        existSemiInList.AvgRate = Math.Round(avgPrice, roundDecimals, MidpointRounding.AwayFromZero);
                        existSemiInList.InvoiceDateRange = "Ingredient Based";
                        existSemiInList.MaterialIngredientRequiredDtos = getMatDetailforRecipe;
                        existSemiInList.PrdBatchQty = recipeMaterial.PrdBatchQty;
                        existSemiInList.TotalIngrCost = totalprice;
                        if (input.DoesIngredientsDetailsRequired == false)
                            existSemiInList.MaterialIngredientRequiredDtos = new List<MaterialIngredientRequiredDto>();
                    }
                }

                #endregion Semi Finished Materials Rate Fixation based on its Recipes
            }

            semiFinishedRateZero = materialRateViewListDtos.Where(t => t.MaterialTypeId == (int)MaterialType.SEMI && t.AvgRate == (decimal)0.0m).Select(t => t.MaterialRefId).ToList();
            SemiMaterialTypeList = SemiMaterialTypeList.Where(t => semiFinishedRateZero.Contains(t.Id)).ToList();
            if (SemiMaterialTypeList.Count > 0 && secondTimeCheckFlag == false)
            {
                secondTimeCheckFlag = true;
                goto againCheckSemi;
            }

            #endregion Semi Finished Material Rate

            if (input.Sorting != null)
                materialRateViewListDtos = materialRateViewListDtos.OrderBy(input.Sorting).ToList();

            if (input.MaterialTypeList != null && input.MaterialTypeList.Count > 0)
            {
                var stridList = input.MaterialTypeList.Select(t => t.Value).ToArray();
                int[] myMaterialIdList = Array.ConvertAll(stridList, s => int.Parse(s));

                allMaterialsQb = allMaterialsQb.Where(t => myMaterialIdList.Contains(t.MaterialTypeId));

                materialRateViewListDtos = materialRateViewListDtos.Where(t => myMaterialIdList.Contains(t.MaterialTypeId)).ToList();
            }

            int loopCnt = 0;
            List<int> materialFilter = materialRateViewListDtos.Select(t => t.MaterialRefId).ToList();

            var rsLocationWiseRateView = await _locationWiseMaterialRateViewRepo.GetAllListAsync(t => t.LocationRefId == input.LocationRefId && materialFilter.Contains(t.MaterialRefId));

            int maxMaterialCountToUpdate = _settingManager.GetSettingValue<int>(AppSettings.HouseSettings.MaterialCountToUpdate);
            if (maxMaterialCountToUpdate < 5)
                maxMaterialCountToUpdate = 5;
            foreach (var lst in materialRateViewListDtos)
            {
                loopCnt++;
                if (lst.LastUpdatedTime == DateTime.MinValue)
                {
                    lst.LastUpdatedTime = DateTime.Now;
                }
                var mat = rsAveragePriceRequiredMaterialList.FirstOrDefault(t => t.Id == lst.MaterialRefId);
                if (mat == null)
                    continue;
                lst.MaterialTypeId = mat.MaterialTypeId;
                if (mat.MaterialTypeId == 0)
                    lst.MaterialTypeRefName = L("RAW");
                else if (mat.MaterialTypeId == 1)
                    lst.MaterialTypeRefName = L("SEMI");
                lst.MaterialName = mat.MaterialName;
                lst.UnitRefId = mat.IssueUnitId;
                lst.DefaultUnitId = mat.DefaultUnitId;
                if (lst.UnitRefName.IsNullOrEmpty())
                {
                    var unit = rsUnits.FirstOrDefault(t => t.Id == lst.UnitRefId);
                    lst.UnitRefName = unit.Name;
                }
                if (lst.DefaultUnitName.IsNullOrEmpty())
                {
                    var unit = rsUnits.FirstOrDefault(t => t.Id == lst.DefaultUnitId);
                    lst.DefaultUnitName = unit.Name;
                    lst.Uom = unit.Name;
                }

                lst.AveragePriceTagRefId = mat.AveragePriceTagRefId;
                lst.NoOfMonthAvgTaken = mat.NoOfMonthAvgTaken;
                lst.AveragePriceTagRefName = lst.AveragePriceTagRefId == 0 ? "Given_Dates_Average" : lst.AveragePriceTagRefId == 1 ? "Last_Purchase" : lst.AveragePriceTagRefId == 2 ? "Purchase_Avg_Last_" + lst.NoOfMonthAvgTaken + "_Months" : lst.AveragePriceTagRefId == 3 ? "Moving_Average" : "";

                if (loopCnt < maxMaterialCountToUpdate)
                {
                    try
                    {
                        var existRecord = rsLocationWiseRateView.FirstOrDefault(t => t.LocationRefId == input.LocationRefId && t.MaterialRefId == lst.MaterialRefId && t.AveragePriceTagRefId == mat.AveragePriceTagRefId);
                        if (existRecord != null)
                        {
                            if (existRecord.BilledQty != lst.BilledQty || existRecord.AvgRate != lst.AvgRate || existRecord.InvoiceDateRange != lst.InvoiceDateRange || existRecord.AveragePriceTagRefId != lst.AveragePriceTagRefId)
                            {
                                existRecord.BilledQty = lst.BilledQty;
                                existRecord.DefaultUnitId = mat.DefaultUnitId;
                                existRecord.NetAmount = lst.NetAmount;
                                existRecord.AvgRate = lst.AvgRate;
                                existRecord.InvoiceDateRange = lst.InvoiceDateRange;
                                existRecord.RefreshRequired = false;
                                existRecord.LastUpdatedTime = DateTime.Now;
                                existRecord.StartDate = argEndDate;
                                existRecord.EndDate = argEndDate;
                                existRecord.IsOmitFOCMaterialQuantities = isOmitFOCMaterialQuantities;
                                existRecord.IsCalculateAverageRateWithoutTax = isCalculateAverageRateWithoutTax;
                                await _locationWiseMaterialRateViewRepo.UpdateAsync(existRecord);
                            }
                            else
                            {
                                var iTest = 1;
                                //Do nothig
                            }
                        }
                        else
                        {
                            var newDto = new LocationWiseMaterialRateView
                            {
                                LocationRefId = input.LocationRefId,
                                MaterialRefId = mat.Id,
                                AveragePriceTagRefId = mat.AveragePriceTagRefId,
                                BilledQty = lst.BilledQty,
                                DefaultUnitId = mat.DefaultUnitId,
                                NetAmount = lst.NetAmount,
                                AvgRate = lst.AvgRate,
                                InvoiceDateRange = lst.InvoiceDateRange,
                                RefreshRequired = false,
                                LastUpdatedTime = DateTime.Now,
                                StartDate = argStartDate,
                                EndDate = argEndDate,
                                IsOmitFOCMaterialQuantities = isOmitFOCMaterialQuantities,
                                IsCalculateAverageRateWithoutTax = isCalculateAverageRateWithoutTax
                            };
                            var retId = await _locationWiseMaterialRateViewRepo.InsertOrUpdateAndGetIdAsync(newDto);
                        }
                    }
                    catch (Exception ex)
                    {
                        // Get stack trace for the exception with source file information
                        var st = new StackTrace(ex, true);
                        // Get the top stack frame
                        var frame = st.GetFrame(0);
                        // Get the line number from the stack frame
                        var lineNumber = frame.GetFileLineNumber();

                        MethodBase m = MethodBase.GetCurrentMethod();
                        string innerExceptionMessage = ex.InnerException != null ? ex.InnerException.Message : "";
                        throw new UserFriendlyException(lst.MaterialName + " Method : " + m.ReflectedType.Name + " : " + m.Name + " " + ex.Message + " InnerException : " + innerExceptionMessage + " Count : " + loopCnt);
                    }
                }
                else
                {
                    int aaaa = 10;
                }
            }

            return new GetMaterialRateViewDtoOutput
            {
                MaterialRateView = materialRateViewListDtos,
                totalCount = materialRateViewListDtos.Count(),
                ColumnDefs = columnDefs
            };
        }

        public async Task<GetMaterialRateViewDtoOutput> GetMaterialRateView_Given_Dates_Average(GetHouseReportMaterialRateInput input)
        {
            var isOmitFOCMaterialQuantities = await _settingManager.GetSettingValueAsync<bool>(AppSettings.HouseSettings.IsOmitFOCMaterialQuantities);
            var isCalculateAverageRateWithoutTax = await _settingManager.GetSettingValueAsync<bool>(AppSettings.HouseSettings.IsCalculateAverageRateWithoutTax);

            DateTime startDate;
            DateTime endDate;

            startDate = input.StartDate;
            endDate = input.EndDate;

            //#region SelectMaterialList
            //List<int> rateRequiredMaterialList = new List<int>();
            //var allMaterialsQb = _materialRepo.GetAll().WhereIf(input.IsHighValueItemOnly, t => t.IsHighValueItem == true);
            //if (input.MaterialRefIds != null && input.MaterialRefIds.Count > 0)
            //{
            //    List<int> arrMaterialTypes = _materialRepo.GetAll().Where(t => input.MaterialRefIds.Contains(t.Id)).ToList().Select(t => t.MaterialTypeId).ToList();
            //    if (!arrMaterialTypes.Contains((int)MaterialType.SEMI))
            //    {
            //        allMaterialsQb = allMaterialsQb.Where(t => input.MaterialRefIds.Contains(t.Id));
            //    }
            //    var arrIngrMaterialTypes = await _materialingredientRepo.GetAll().Where(t => input.MaterialRefIds.Contains(t.RecipeRefId)).Select(t => t.MaterialRefId).Distinct().ToListAsync();
            //    arrMaterialTypes = await _materialRepo.GetAll().Where(t => arrIngrMaterialTypes.Contains(t.Id))
            //            .Select(t => t.MaterialTypeId).Distinct().ToListAsync();
            //    if (!arrMaterialTypes.Contains((int)MaterialType.SEMI))
            //    {
            //        allMaterialsQb = allMaterialsQb.Where(t => input.MaterialRefIds.Contains(t.Id));
            //    }

            //}
            //rateRequiredMaterialList = await allMaterialsQb.Select(t => t.Id).ToListAsync();
            //#endregion

            #region SelectMaterialList

            List<int> rateRequiredMaterialList = new List<int>();
            var allMaterialsQb = _materialRepo.GetAll().WhereIf(input.IsHighValueItemOnly, t => t.IsHighValueItem == true);

            List<int> arrNeededSemiMaterialList = new List<int>();
            List<int> extraRawIngredients = new List<int>();
            if (input.MaterialRefIds != null && input.MaterialRefIds.Count > 0)
            {
                var arrMaterialTypes = await _materialRepo.GetAll().Where(t => input.MaterialRefIds.Contains(t.Id))
                        .Select(t => t.MaterialTypeId).Distinct().ToListAsync();
                //List<int> arrMaterialTypes = materialTypes.Select(t => t.MaterialTypeId).ToList();
                if (!arrMaterialTypes.Contains((int)MaterialType.SEMI))
                {
                    allMaterialsQb = allMaterialsQb.Where(t => input.MaterialRefIds.Contains(t.Id));
                }
                else
                {
                    var materialSemi = await _materialRepo.GetAll().Where(t => t.MaterialTypeId == (int)MaterialType.SEMI && input.MaterialRefIds.Contains(t.Id)).Select(t => t.Id).ToListAsync();
                    arrNeededSemiMaterialList.AddRange(materialSemi);
                    input.MaterialRefIds.AddRange(materialSemi);

                    materialSemi = await _materialRepo.GetAll().Where(t => t.MaterialTypeId == (int)MaterialType.SEMI && input.MaterialRefIds.Contains(t.Id)).Select(t => t.Id).ToListAsync();
                    arrNeededSemiMaterialList.AddRange(materialSemi);
                    input.MaterialRefIds.AddRange(materialSemi);
                }

                var arrIngrMaterialRefIds = await _materialingredientRepo.GetAll().Where(t => input.MaterialRefIds.Contains(t.RecipeRefId)).Select(t => t.MaterialRefId).Distinct().ToListAsync();
                arrMaterialTypes = await _materialRepo.GetAll().Where(t => arrIngrMaterialRefIds.Contains(t.Id))
                        .Select(t => t.MaterialTypeId).Distinct().ToListAsync();
                if (!arrMaterialTypes.Contains((int)MaterialType.SEMI))
                {
                    allMaterialsQb = allMaterialsQb.Where(t => input.MaterialRefIds.Contains(t.Id));
                }
                else
                {
                    List<int> arrRevisedList = new List<int>();
                    arrRevisedList.AddRange(arrIngrMaterialRefIds);
                    arrRevisedList.AddRange(input.MaterialRefIds);

                    arrIngrMaterialRefIds = await _materialingredientRepo.GetAll().Where(t => arrRevisedList.Contains(t.RecipeRefId)).Select(t => t.MaterialRefId).Distinct().ToListAsync();
                    arrRevisedList.AddRange(arrIngrMaterialRefIds);

                    arrIngrMaterialRefIds = await _materialingredientRepo.GetAll().Where(t => arrRevisedList.Contains(t.RecipeRefId)).Select(t => t.MaterialRefId).Distinct().ToListAsync();
                    arrRevisedList.AddRange(arrIngrMaterialRefIds);

                    input.MaterialRefIds = arrRevisedList;
                    allMaterialsQb = allMaterialsQb.Where(t => input.MaterialRefIds.Contains(t.Id));
                }
            }
            rateRequiredMaterialList = await allMaterialsQb.Select(t => t.Id).ToListAsync();

            #endregion SelectMaterialList

            List<MaterialRateViewListDto> output = new List<MaterialRateViewListDto>();
            var rsSupplierMaterial = await _supplierMaterialRepo.GetAll()
                    .Where(t => rateRequiredMaterialList.Contains(t.MaterialRefId)).ToListAsync();
            var invoices = _invoiceRepo.GetAll();
            var rsInvoiceDetail = _invoiceDetailRepo.GetAll();
            if (isOmitFOCMaterialQuantities == true)
                rsInvoiceDetail = rsInvoiceDetail.Where(t => t.Price > 0);

            var rsInterTransferMaster = _intertransferRepo.GetAll();
            var rsYieldMaster = _yieldRepo.GetAll();

            if (input.Locations != null && input.Locations.Any())
            {
                var allLocations = input.Locations.Select(a => a.Id).ToArray();
                invoices = invoices.Where(a => allLocations.Contains(a.LocationRefId));
                rsInterTransferMaster = rsInterTransferMaster.Where(a => allLocations.Contains(a.LocationRefId));
                rsYieldMaster = rsYieldMaster.Where(a => allLocations.Contains(a.LocationRefId));
            }

            #region Get The Rate from specified Range

            #region Based On Purchase Invoice

            var matInvList = (from inv in invoices
                              where (DbFunctions.TruncateTime(inv.AccountDate) >= startDate
                              && DbFunctions.TruncateTime(inv.AccountDate) <= endDate)
                              join invDet in rsInvoiceDetail on inv.Id equals invDet.InvoiceRefId
                              join mat in allMaterialsQb
                      on invDet.MaterialRefId equals mat.Id
                              group invDet by new { invDet.MaterialRefId, mat.MaterialTypeId, mat.MaterialName, invDet.UnitRefId, mat.DefaultUnitId } into g
                              select new MaterialRateViewListDto
                              {
                                  MaterialTypeId = g.Key.MaterialTypeId,
                                  MaterialRefId = g.Key.MaterialRefId,
                                  MaterialName = g.Key.MaterialName,
                                  UnitRefId = g.Key.UnitRefId,
                                  DefaultUnitId = g.Key.DefaultUnitId,
                                  BilledQty = g.Sum(s => s.TotalQty),
                                  TotalAmount = g.Sum(s => s.TotalAmount),
                                  NetAmount = g.Sum(s => s.NetAmount),
                                  //AvgRate = g.Sum(s => s.NetAmount) > 0 ? Math.Round(g.Sum(s => s.NetAmount) / g.Sum(s => s.TotalQty), 2) : 0
                              });

            foreach (var items in matInvList)
            {
                MaterialRateViewListDto m = new MaterialRateViewListDto();
                m.MaterialTypeId = items.MaterialTypeId;
                if (m.MaterialTypeId == 0)
                    m.MaterialTypeRefName = L("RAW");
                else if (m.MaterialTypeId == 1)
                    m.MaterialTypeRefName = L("SEMI");
                m.MaterialRefId = items.MaterialRefId;
                m.MaterialName = items.MaterialName;
                if (m.MaterialName.Contains("Oil"))
                {
                    int i = 10;
                }
                m.BilledQty = items.BilledQty;
                m.TotalAmount = items.TotalAmount;
                m.NetAmount = items.NetAmount;
                m.UnitRefId = items.UnitRefId;
                m.DefaultUnitId = items.DefaultUnitId;
                m.InvoiceDateRange = startDate.ToString("MMM-dd") + " - " + endDate.ToString("MMM-dd");
                output.Add(m);
            }

            #endregion Based On Purchase Invoice

            #region Based On InterTransfer

            // InterTransfer Received
            var matTrasferRecdList = (from rcd in rsInterTransferMaster
                                      where (DbFunctions.TruncateTime(rcd.ReceivedTime) >= startDate
                                      && DbFunctions.TruncateTime(rcd.ReceivedTime) <= endDate)
                                      join rcdDet in _intertransferReceivedRepo.GetAll().Where(t => rateRequiredMaterialList.Contains(t.MaterialRefId))
                                      on rcd.Id equals rcdDet.InterTransferRefId
                                      join mat in _materialRepo.GetAll().Where(t => rateRequiredMaterialList.Contains(t.Id))
                                      on rcdDet.MaterialRefId equals mat.Id
                                      group rcdDet by new { rcdDet.MaterialRefId, mat.MaterialTypeId, mat.MaterialName, rcdDet.UnitRefId, mat.DefaultUnitId } into g
                                      select new MaterialRateViewListDto
                                      {
                                          MaterialTypeId = g.Key.MaterialTypeId,
                                          MaterialRefId = g.Key.MaterialRefId,
                                          MaterialName = g.Key.MaterialName,
                                          UnitRefId = g.Key.UnitRefId,
                                          DefaultUnitId = g.Key.DefaultUnitId,
                                          BilledQty = g.Sum(s => s.ReceivedQty),
                                          TotalAmount = g.Sum(s => s.ReceivedValue),
                                          NetAmount = g.Sum(s => s.ReceivedValue),
                                      });

            foreach (var items in matTrasferRecdList)
            {
                MaterialRateViewListDto m = new MaterialRateViewListDto();
                m.MaterialTypeId = items.MaterialTypeId;
                m.MaterialRefId = items.MaterialRefId;
                m.MaterialName = items.MaterialName;
                m.BilledQty = items.BilledQty;
                m.TotalAmount = items.TotalAmount;
                m.NetAmount = items.NetAmount;
                m.UnitRefId = items.UnitRefId;
                m.DefaultUnitId = items.DefaultUnitId;
                if (items.NetAmount == 0 || items.BilledQty == 0)
                    continue;

                m.InvoiceDateRange = L("Transfer") + " " + startDate.ToString("MMM-dd") + " - " + endDate.ToString("MMM-dd");
                output.Add(m);
            }

            #endregion Based On InterTransfer

            #region Based On Yield

            // Yield Output Received
            {
                IQueryable<MaterialRateViewListDto> rsYieldList;
                rsYieldList = (from yld in rsYieldMaster
                               where (DbFunctions.TruncateTime(yld.CompletedTime) >= startDate.Date
                            && DbFunctions.TruncateTime(yld.CompletedTime) <= endDate.Date)
                               join yldOutput in _yieldoutputRepo.GetAll()
                                   .Where(t => rateRequiredMaterialList.Contains(t.OutputMaterialRefId))
                               on yld.Id equals yldOutput.YieldRefId
                               join mat in _materialRepo.GetAll().Where(t => rateRequiredMaterialList.Contains(t.Id))
                               on yldOutput.OutputMaterialRefId equals mat.Id
                               group yldOutput by new { yldOutput.OutputMaterialRefId, mat.MaterialTypeId, mat.MaterialName, yldOutput.UnitRefId, mat.DefaultUnitId } into g
                               select new MaterialRateViewListDto
                               {
                                   MaterialTypeId = g.Key.MaterialTypeId,
                                   MaterialRefId = g.Key.OutputMaterialRefId,
                                   MaterialName = g.Key.MaterialName,
                                   UnitRefId = g.Key.UnitRefId,
                                   DefaultUnitId = g.Key.DefaultUnitId,
                                   BilledQty = g.Sum(s => s.OutputQty),
                                   TotalAmount = g.Sum(s => s.YieldPrice * s.OutputQty),
                                   NetAmount = g.Sum(s => s.YieldPrice * s.OutputQty),
                               });

                foreach (var items in rsYieldList)
                {
                    MaterialRateViewListDto m = new MaterialRateViewListDto();
                    m.MaterialTypeId = items.MaterialTypeId;
                    m.MaterialRefId = items.MaterialRefId;
                    m.MaterialName = items.MaterialName;
                    m.BilledQty = items.BilledQty;
                    m.TotalAmount = items.TotalAmount;
                    m.NetAmount = items.NetAmount;
                    m.UnitRefId = items.UnitRefId;
                    m.DefaultUnitId = items.DefaultUnitId;
                    if (items.NetAmount == 0 || items.BilledQty == 0)
                        continue;

                    m.InvoiceDateRange = L("Yield") + " " + startDate.ToString("MMM-dd") + " - " + endDate.ToString("MMM-dd");
                    output.Add(m);
                }
            }

            #endregion Based On Yield

            #endregion Get The Rate from specified Range

            List<int> availableMaterial = output.Select(a => a.MaterialRefId).ToList();
            var notExist = availableMaterial.Except(rateRequiredMaterialList).ToList();
            var pendingMaterials = rateRequiredMaterialList.Except(availableMaterial).ToList();

            endDate = startDate.AddDays(-1);
            startDate = startDate.AddDays(-31);

            if (pendingMaterials.Count > 0)
            {
                int loopCnt = 0;
                do
                {
                    loopCnt++;
                    //  Get The Rate from Last 30 Days Range

                    #region Based On Invoice

                    matInvList = (from inv in invoices
                                  where (DbFunctions.TruncateTime(inv.AccountDate) >= startDate && DbFunctions.TruncateTime(inv.AccountDate) <= endDate)
                                  join invDet in rsInvoiceDetail
                                  on inv.Id equals invDet.InvoiceRefId
                                  join mat in _materialRepo.GetAll().Where(a => pendingMaterials.Contains(a.Id))
                                  on invDet.MaterialRefId equals mat.Id
                                  group invDet by new
                                  {
                                      invDet.MaterialRefId,
                                      mat.MaterialTypeId,
                                      mat.MaterialName,
                                      mat.DefaultUnitId,
                                      invDet.UnitRefId
                                  } into g
                                  select new MaterialRateViewListDto
                                  {
                                      MaterialTypeId = g.Key.MaterialTypeId,
                                      MaterialRefId = g.Key.MaterialRefId,
                                      MaterialName = g.Key.MaterialName,
                                      UnitRefId = g.Key.UnitRefId,
                                      DefaultUnitId = g.Key.DefaultUnitId,
                                      BilledQty = g.Sum(s => s.TotalQty),
                                      TotalAmount = g.Sum(s => s.TotalAmount),
                                      NetAmount = g.Sum(s => s.NetAmount),
                                  });

                    foreach (var items in matInvList)
                    {
                        MaterialRateViewListDto m = new MaterialRateViewListDto();
                        m.MaterialTypeId = items.MaterialTypeId;
                        m.MaterialRefId = items.MaterialRefId;
                        m.MaterialName = items.MaterialName;
                        m.BilledQty = items.BilledQty;
                        m.TotalAmount = items.TotalAmount;
                        m.NetAmount = items.NetAmount;
                        m.UnitRefId = items.UnitRefId;
                        m.DefaultUnitId = items.DefaultUnitId;
                        m.InvoiceDateRange = startDate.ToString("MMM-dd") + " - " + endDate.ToString("MMM-dd");
                        output.Add(m);
                    }

                    #endregion Based On Invoice

                    // InterTransfer Received

                    #region Based On InterTransfer

                    matTrasferRecdList = (from rcd in rsInterTransferMaster
                                          where (DbFunctions.TruncateTime(rcd.ReceivedTime) >= startDate
                                          && DbFunctions.TruncateTime(rcd.ReceivedTime) <= endDate)
                                          join rcdDet in _intertransferReceivedRepo.GetAll().Where(t => t.Price > 0 && pendingMaterials.Contains(t.MaterialRefId))
                                          on rcd.Id equals rcdDet.InterTransferRefId
                                          join mat in _materialRepo.GetAll().Where(a => pendingMaterials.Contains(a.Id))
                                          on rcdDet.MaterialRefId equals mat.Id
                                          group rcdDet by new
                                          {
                                              rcdDet.MaterialRefId,
                                              mat.MaterialTypeId,
                                              mat.MaterialName,
                                              rcdDet.UnitRefId,
                                              mat.DefaultUnitId
                                          } into g
                                          select new MaterialRateViewListDto
                                          {
                                              MaterialTypeId = g.Key.MaterialTypeId,
                                              MaterialRefId = g.Key.MaterialRefId,
                                              MaterialName = g.Key.MaterialName,
                                              UnitRefId = g.Key.UnitRefId,
                                              DefaultUnitId = g.Key.DefaultUnitId,
                                              BilledQty = g.Sum(s => s.ReceivedQty),
                                              TotalAmount = g.Sum(s => s.ReceivedValue),
                                              NetAmount = g.Sum(s => s.ReceivedValue),
                                          });

                    foreach (var items in matTrasferRecdList)
                    {
                        MaterialRateViewListDto m = new MaterialRateViewListDto();
                        m.MaterialTypeId = items.MaterialTypeId;
                        m.MaterialRefId = items.MaterialRefId;
                        m.MaterialName = items.MaterialName;
                        m.BilledQty = items.BilledQty;
                        m.TotalAmount = items.TotalAmount;
                        m.NetAmount = items.NetAmount;
                        m.UnitRefId = items.UnitRefId;
                        m.DefaultUnitId = items.DefaultUnitId;
                        if (items.NetAmount == 0 || items.BilledQty == 0)
                            continue;

                        m.InvoiceDateRange = L("Transfer") + " " + startDate.ToString("MMM-dd") + " - " + endDate.ToString("MMM-dd");
                        output.Add(m);
                    }

                    #endregion Based On InterTransfer

                    //Yield

                    #region Based On Yield

                    // Yield Output Received
                    var rsPendingYieldList = (from yld in rsYieldMaster
                                              where (DbFunctions.TruncateTime(yld.CompletedTime) >= startDate
                                           && DbFunctions.TruncateTime(yld.CompletedTime) <= endDate)
                                              join yldOutput in _yieldoutputRepo.GetAll()
                                                  .Where(t => rateRequiredMaterialList.Contains(t.OutputMaterialRefId))
                                              on yld.Id equals yldOutput.YieldRefId
                                              join mat in _materialRepo.GetAll().Where(t => rateRequiredMaterialList.Contains(t.Id))
                                              on yldOutput.OutputMaterialRefId equals mat.Id
                                              group yldOutput by new { yldOutput.OutputMaterialRefId, mat.MaterialTypeId, mat.MaterialName, yldOutput.UnitRefId, mat.DefaultUnitId } into g
                                              select new MaterialRateViewListDto
                                              {
                                                  MaterialTypeId = g.Key.MaterialTypeId,
                                                  MaterialRefId = g.Key.OutputMaterialRefId,
                                                  MaterialName = g.Key.MaterialName,
                                                  UnitRefId = g.Key.UnitRefId,
                                                  DefaultUnitId = g.Key.DefaultUnitId,
                                                  BilledQty = g.Sum(s => s.OutputQty),
                                                  TotalAmount = g.Sum(s => s.YieldPrice * s.OutputQty),
                                                  NetAmount = g.Sum(s => s.YieldPrice * s.OutputQty),
                                              });

                    foreach (var items in rsPendingYieldList)
                    {
                        MaterialRateViewListDto m = new MaterialRateViewListDto();
                        m.MaterialTypeId = items.MaterialTypeId;
                        m.MaterialRefId = items.MaterialRefId;
                        m.MaterialName = items.MaterialName;
                        m.BilledQty = items.BilledQty;
                        m.TotalAmount = items.TotalAmount;
                        m.NetAmount = items.NetAmount;
                        m.UnitRefId = items.UnitRefId;
                        m.DefaultUnitId = items.DefaultUnitId;
                        if (items.NetAmount == 0 || items.BilledQty == 0)
                            continue;

                        m.InvoiceDateRange = L("Yield") + " " + startDate.ToString("MMM-dd") + " - " + endDate.ToString("MMM-dd");
                        output.Add(m);
                    }

                    #endregion Based On Yield

                    availableMaterial = output.Select(a => a.MaterialRefId).ToList();

                    endDate = startDate.AddDays(-1);
                    startDate = startDate.AddDays(-31);

                    pendingMaterials = rateRequiredMaterialList.Except(availableMaterial).ToList();
                    if (pendingMaterials.Count == 0)
                    {
                        loopCnt = 5;
                        break;
                    }
                } while (loopCnt <= 4);
            }

            //  Get The Material Rate For Purchase Before the filters + 120 Days
            if (pendingMaterials.Count > 0)
            {
                matInvList = (from inv in invoices
                              where (DbFunctions.TruncateTime(inv.AccountDate) < startDate)
                              join invDet in rsInvoiceDetail.Where(t => pendingMaterials.Contains(t.MaterialRefId))
                                  on inv.Id equals invDet.InvoiceRefId
                              join mat in _materialRepo.GetAll().Where(a => pendingMaterials.Contains(a.Id))
                              on invDet.MaterialRefId equals mat.Id
                              select new MaterialRateViewListDto
                              {
                                  MaterialTypeId = mat.MaterialTypeId,
                                  MaterialRefId = invDet.MaterialRefId,
                                  MaterialName = mat.MaterialName,
                                  BilledQty = invDet.TotalQty,
                                  TotalAmount = invDet.TotalAmount,
                                  NetAmount = invDet.NetAmount,
                                  UnitRefId = invDet.UnitRefId,
                                  DefaultUnitId = mat.DefaultUnitId,
                                  InvoiceDateRange = inv.InvoiceDate.ToString()
                              });

                foreach (var items in matInvList)
                {
                    MaterialRateViewListDto m = new MaterialRateViewListDto();
                    m.MaterialTypeId = items.MaterialTypeId;
                    m.MaterialRefId = items.MaterialRefId;
                    m.MaterialName = items.MaterialName;
                    m.BilledQty = items.BilledQty;
                    m.TotalAmount = items.TotalAmount;
                    m.NetAmount = items.NetAmount;
                    m.UnitRefId = items.UnitRefId;
                    m.DefaultUnitId = items.DefaultUnitId;
                    m.InvoiceDateRange = items.InvoiceDateRange;
                    output.Add(m);
                }
            }
            availableMaterial = output.Select(a => a.MaterialRefId).ToList();
            pendingMaterials = rateRequiredMaterialList.Except(availableMaterial).ToList();
            var rsUnitConversion = await _unitConversionRepo.GetAllListAsync();

            if (pendingMaterials.Count() > 0)
            {
                var material = _materialRepo.GetAll();
                material = material.Where(a => pendingMaterials.Contains(a.Id));

                foreach (var mat in material)
                {
                    MaterialRateViewListDto m = new MaterialRateViewListDto();
                    m.MaterialTypeId = mat.MaterialTypeId;
                    m.MaterialRefId = mat.Id;
                    m.MaterialName = mat.MaterialName;
                    m.DefaultUnitId = mat.DefaultUnitId;
                    m.UnitRefId = mat.DefaultUnitId;

                    //var supplierPrice = rsSupplierMaterial.Where(t => t.MaterialRefId == mat.Id && t.LocationRefId == input.LocationRefId).OrderByDescending(t => t.MaterialPrice).FirstOrDefault();
                    //if (supplierPrice == null)
                    //{
                    //    supplierPrice = rsSupplierMaterial.Where(t => t.MaterialRefId == mat.Id && t.LocationRefId == null).OrderByDescending(t => t.MaterialPrice).FirstOrDefault();
                    //}
                    var tempsupplierPriceList = rsSupplierMaterial.Where(t => t.MaterialRefId == mat.Id && t.LocationRefId == input.LocationRefId).ToList();
                    if (tempsupplierPriceList.Count == 0)
                        tempsupplierPriceList = rsSupplierMaterial.Where(t => t.MaterialRefId == mat.Id && t.LocationRefId == null).ToList();
                    List<SupplierMaterialListDto> supplierPriceList = tempsupplierPriceList.MapTo<List<SupplierMaterialListDto>>();
                    decimal conversionFactor = 0.0m;
                    foreach (var sm in supplierPriceList)
                    {
                        if (mat.DefaultUnitId == sm.UnitRefId)
                        {
                            conversionFactor = 1;
                        }
                        else
                        {
                            var unitConversion = rsUnitConversion.FirstOrDefault(t => t.BaseUnitId == mat.DefaultUnitId && t.RefUnitId == sm.UnitRefId);
                            if (unitConversion != null)
                            {
                                conversionFactor = unitConversion.Conversion;
                            }
                            else
                            {
                                continue;
                            }
                            sm.MaterialPrice = sm.MaterialPrice * conversionFactor;
                        }
                    }
                    var supplierPrice = supplierPriceList.OrderByDescending(t => t.MaterialPrice).FirstOrDefault();
                    if (supplierPrice == null)
                    {
                        m.BilledQty = 0;
                        m.TotalAmount = 0;
                        m.NetAmount = 0;
                        m.AvgRate = 0;
                        m.InvoiceDateRange = "N/A";
                    }
                    else
                    {

                        m.BilledQty = 1;
                        m.TotalAmount = supplierPrice.MaterialPrice;
                        m.NetAmount = supplierPrice.MaterialPrice;
                        m.AvgRate = supplierPrice.MaterialPrice;
                        m.InvoiceDateRange = L("QuoteReference");
                    }

                    output.Add(m);
                }
            }

            List<MaterialRateViewListDto> materialoutput = new List<MaterialRateViewListDto>();



            materialoutput = (from lst in output
                              join mat in _materialRepo.GetAll().WhereIf(input.IsHighValueItemOnly, t => t.IsHighValueItem == true)
                              on lst.MaterialRefId equals mat.Id
                              join un in _unitrepo.GetAll() on mat.DefaultUnitId equals un.Id
                              join uiss in _unitrepo.GetAll() on lst.UnitRefId equals uiss.Id
                              select new MaterialRateViewListDto
                              {
                                  MaterialTypeId = lst.MaterialTypeId,
                                  MaterialRefId = lst.MaterialRefId,
                                  MaterialName = lst.MaterialName,
                                  BilledQty = lst.BilledQty,
                                  TotalAmount = lst.TotalAmount,
                                  Uom = un.Name,
                                  NetAmount = lst.NetAmount,
                                  InvoiceDateRange = lst.InvoiceDateRange,
                                  UnitRefId = lst.UnitRefId,
                                  UnitRefName = uiss.Name,
                                  DefaultUnitId = mat.DefaultUnitId,
                                  DefaultUnitName = un.Name
                              }).OrderBy(t => t.MaterialRefId).ToList();

            List<MaterialRateViewListDto> returnoutput = new List<MaterialRateViewListDto>();

            foreach (var group in materialoutput.GroupBy(t => t.MaterialRefId))
            {
                MaterialRateViewListDto matDto = new MaterialRateViewListDto();
                decimal conversionFactor;
                decimal totalQty = 0;
                decimal netAmount = 0;
                decimal totalAmount = 0;

                foreach (var lst in group.OrderBy(t => t.UnitRefId))
                {
                    matDto = lst;
                    if (lst.DefaultUnitId == lst.UnitRefId)
                    {
                        conversionFactor = 1;
                    }
                    else
                    {
                        var unitConversion = rsUnitConversion.FirstOrDefault(t => t.BaseUnitId == lst.UnitRefId && t.RefUnitId == lst.DefaultUnitId);
                        if (unitConversion != null)
                        {
                            conversionFactor = unitConversion.Conversion;
                        }
                        else
                        {
                            continue;
                        }
                    }
                    totalQty = totalQty + (lst.BilledQty * conversionFactor);
                    netAmount = netAmount + lst.NetAmount;
                    totalAmount = totalAmount + lst.TotalAmount;
                }

                matDto.NetAmount = netAmount;
                matDto.BilledQty = totalQty;
                matDto.TotalAmount = totalAmount;

                if (isCalculateAverageRateWithoutTax == true)
                {
                    if (matDto.TotalAmount > 0 && matDto.BilledQty > 0)
                        matDto.AvgRate = Math.Round(matDto.TotalAmount / matDto.BilledQty, roundDecimals, MidpointRounding.AwayFromZero);
                }
                else
                {
                    if (matDto.NetAmount > 0 && matDto.BilledQty > 0)
                        matDto.AvgRate = Math.Round(matDto.NetAmount / matDto.BilledQty, roundDecimals, MidpointRounding.AwayFromZero);
                }
                returnoutput.Add(matDto);
            }

            return new GetMaterialRateViewDtoOutput
            {
                MaterialRateView = returnoutput,
                totalCount = output.Count(),
            };
        }

        public async Task<GetMaterialRateViewDtoOutput> GetMaterialRateView_Last_Purchase(GetHouseReportMaterialRateInput input)
        {
            var isOmitFOCMaterialQuantities = await _settingManager.GetSettingValueAsync<bool>(AppSettings.HouseSettings.IsOmitFOCMaterialQuantities);
            var isCalculateAverageRateWithoutTax = await _settingManager.GetSettingValueAsync<bool>(AppSettings.HouseSettings.IsCalculateAverageRateWithoutTax);

            DateTime startDate;
            DateTime endDate;

            startDate = input.StartDate;
            endDate = input.EndDate;

            //#region SelectMaterialList
            //List<int> rateRequiredMaterialList = new List<int>();
            //var allMaterialsQb = _materialRepo.GetAll().WhereIf(input.IsHighValueItemOnly, t => t.IsHighValueItem == true);
            //if (input.MaterialRefIds != null && input.MaterialRefIds.Count > 0)
            //{
            //    List<int> arrMaterialTypes = _materialRepo.GetAll().Where(t => input.MaterialRefIds.Contains(t.Id)).ToList().Select(t => t.MaterialTypeId).ToList();
            //    if (!arrMaterialTypes.Contains((int)MaterialType.SEMI))
            //    {
            //        allMaterialsQb = allMaterialsQb.Where(t => input.MaterialRefIds.Contains(t.Id));
            //    }
            //    var arrIngrMaterialTypes = await _materialingredientRepo.GetAll().Where(t => input.MaterialRefIds.Contains(t.RecipeRefId)).Select(t => t.MaterialRefId).Distinct().ToListAsync();
            //    arrMaterialTypes = await _materialRepo.GetAll().Where(t => arrIngrMaterialTypes.Contains(t.Id))
            //            .Select(t => t.MaterialTypeId).Distinct().ToListAsync();
            //    if (!arrMaterialTypes.Contains((int)MaterialType.SEMI))
            //    {
            //        allMaterialsQb = allMaterialsQb.Where(t => input.MaterialRefIds.Contains(t.Id));
            //    }

            //}
            //rateRequiredMaterialList = await allMaterialsQb.Select(t => t.Id).ToListAsync();
            //#endregion

            #region SelectMaterialList

            List<int> rateRequiredMaterialList = new List<int>();
            var allMaterialsQb = _materialRepo.GetAll().WhereIf(input.IsHighValueItemOnly, t => t.IsHighValueItem == true);

            List<int> arrNeededSemiMaterialList = new List<int>();
            List<int> extraRawIngredients = new List<int>();
            if (input.MaterialRefIds != null && input.MaterialRefIds.Count > 0)
            {
                var arrMaterialTypes = await _materialRepo.GetAll().Where(t => input.MaterialRefIds.Contains(t.Id))
                        .Select(t => t.MaterialTypeId).Distinct().ToListAsync();
                //List<int> arrMaterialTypes = materialTypes.Select(t => t.MaterialTypeId).ToList();
                if (!arrMaterialTypes.Contains((int)MaterialType.SEMI))
                {
                    allMaterialsQb = allMaterialsQb.Where(t => input.MaterialRefIds.Contains(t.Id));
                }
                else
                {
                    var materialSemi = await _materialRepo.GetAll().Where(t => t.MaterialTypeId == (int)MaterialType.SEMI && input.MaterialRefIds.Contains(t.Id)).Select(t => t.Id).ToListAsync();
                    arrNeededSemiMaterialList.AddRange(materialSemi);
                    input.MaterialRefIds.AddRange(materialSemi);

                    materialSemi = await _materialRepo.GetAll().Where(t => t.MaterialTypeId == (int)MaterialType.SEMI && input.MaterialRefIds.Contains(t.Id)).Select(t => t.Id).ToListAsync();
                    arrNeededSemiMaterialList.AddRange(materialSemi);
                    input.MaterialRefIds.AddRange(materialSemi);
                }

                var arrIngrMaterialRefIds = await _materialingredientRepo.GetAll().Where(t => input.MaterialRefIds.Contains(t.RecipeRefId)).Select(t => t.MaterialRefId).Distinct().ToListAsync();
                arrMaterialTypes = await _materialRepo.GetAll().Where(t => arrIngrMaterialRefIds.Contains(t.Id))
                        .Select(t => t.MaterialTypeId).Distinct().ToListAsync();
                if (!arrMaterialTypes.Contains((int)MaterialType.SEMI))
                {
                    allMaterialsQb = allMaterialsQb.Where(t => input.MaterialRefIds.Contains(t.Id));
                }
                else
                {
                    List<int> arrRevisedList = new List<int>();
                    arrRevisedList.AddRange(arrIngrMaterialRefIds);
                    arrRevisedList.AddRange(input.MaterialRefIds);

                    arrIngrMaterialRefIds = await _materialingredientRepo.GetAll().Where(t => arrRevisedList.Contains(t.RecipeRefId)).Select(t => t.MaterialRefId).Distinct().ToListAsync();
                    arrRevisedList.AddRange(arrIngrMaterialRefIds);

                    arrIngrMaterialRefIds = await _materialingredientRepo.GetAll().Where(t => arrRevisedList.Contains(t.RecipeRefId)).Select(t => t.MaterialRefId).Distinct().ToListAsync();
                    arrRevisedList.AddRange(arrIngrMaterialRefIds);

                    input.MaterialRefIds = arrRevisedList;
                    allMaterialsQb = allMaterialsQb.Where(t => input.MaterialRefIds.Contains(t.Id));
                }
            }
            rateRequiredMaterialList = await allMaterialsQb.Select(t => t.Id).ToListAsync();

            #endregion SelectMaterialList

            List<MaterialRateViewListDto> output = new List<MaterialRateViewListDto>();
            var rsSupplierMaterial = await _supplierMaterialRepo.GetAll()
                    .Where(t => rateRequiredMaterialList.Contains(t.MaterialRefId)).ToListAsync();
            var invoices = _invoiceRepo.GetAll();
            var rsInvoiceDetail = _invoiceDetailRepo.GetAll();
            if (isOmitFOCMaterialQuantities == true)
                rsInvoiceDetail = rsInvoiceDetail.Where(t => t.Price > 0);

            var rsInterTransferMaster = _intertransferRepo.GetAll();
            var rsYieldMaster = _yieldRepo.GetAll();

            if (input.Locations != null && input.Locations.Any())
            {
                var allLocations = input.Locations.Select(a => a.Id).ToArray();
                invoices = invoices.Where(a => allLocations.Contains(a.LocationRefId));
                rsInterTransferMaster = rsInterTransferMaster.Where(a => allLocations.Contains(a.LocationRefId));
                rsYieldMaster = rsYieldMaster.Where(a => allLocations.Contains(a.LocationRefId));
            }

            #region Get The Rate from specified Range

            #region Based On Purchase Invoice

            var matInvList = (from inv in invoices
                              where (DbFunctions.TruncateTime(inv.AccountDate) >= startDate
                              && DbFunctions.TruncateTime(inv.AccountDate) <= endDate)
                              join invDet in rsInvoiceDetail on inv.Id equals invDet.InvoiceRefId
                              join mat in allMaterialsQb
                      on invDet.MaterialRefId equals mat.Id
                              group invDet by new { invDet.MaterialRefId, inv.AccountDate, mat.MaterialTypeId, mat.MaterialName, invDet.UnitRefId, mat.DefaultUnitId } into g
                              select new MaterialRateViewListDto
                              {
                                  MaterialTypeId = g.Key.MaterialTypeId,
                                  MaterialRefId = g.Key.MaterialRefId,
                                  MaterialName = g.Key.MaterialName,
                                  UnitRefId = g.Key.UnitRefId,
                                  DefaultUnitId = g.Key.DefaultUnitId,
                                  BilledQty = g.Sum(s => s.TotalQty),
                                  TotalAmount = g.Sum(s => s.TotalAmount),
                                  NetAmount = g.Sum(s => s.NetAmount),
                                  PurchaseDate = g.Key.AccountDate,
                                  AvgRate = g.Sum(s => s.NetAmount) > 0 ? Math.Round(g.Sum(s => s.NetAmount) / g.Sum(s => s.TotalQty), roundDecimals) : 0
                              });

            foreach (var items in matInvList)
            {
                MaterialRateViewListDto m = new MaterialRateViewListDto();
                m.MaterialTypeId = items.MaterialTypeId;
                if (m.MaterialTypeId == 0)
                    m.MaterialTypeRefName = L("RAW");
                else if (m.MaterialTypeId == 1)
                    m.MaterialTypeRefName = L("SEMI");
                m.MaterialRefId = items.MaterialRefId;
                m.MaterialName = items.MaterialName;
                if (m.MaterialName.Contains("Oil"))
                {
                    int i = 10;
                }
                m.BilledQty = items.BilledQty;
                m.TotalAmount = items.TotalAmount;
                m.NetAmount = items.NetAmount;
                m.UnitRefId = items.UnitRefId;
                m.DefaultUnitId = items.DefaultUnitId;
                m.AvgRate = items.AvgRate;
                m.PurchaseDate = items.PurchaseDate;
                m.InvoiceDateRange = startDate.ToString("MMM-dd") + " - " + endDate.ToString("MMM-dd");
                output.Add(m);
            }

            #endregion Based On Purchase Invoice

            #region Based On InterTransfer

            // InterTransfer Received
            var matTrasferRecdList = (from rcd in rsInterTransferMaster
                                      where (rcd.ReceivedTime.HasValue && DbFunctions.TruncateTime(rcd.ReceivedTime) >= startDate
                                      && DbFunctions.TruncateTime(rcd.ReceivedTime) <= endDate)
                                      join rcdDet in _intertransferReceivedRepo.GetAll().Where(t => rateRequiredMaterialList.Contains(t.MaterialRefId))
                                      on rcd.Id equals rcdDet.InterTransferRefId
                                      join mat in _materialRepo.GetAll().Where(t => rateRequiredMaterialList.Contains(t.Id))
                                      on rcdDet.MaterialRefId equals mat.Id
                                      group rcdDet by new { rcd.ReceivedTime, rcdDet.MaterialRefId, mat.MaterialTypeId, mat.MaterialName, rcdDet.UnitRefId, mat.DefaultUnitId } into g
                                      select new MaterialRateViewListDto
                                      {
                                          MaterialTypeId = g.Key.MaterialTypeId,
                                          MaterialRefId = g.Key.MaterialRefId,
                                          MaterialName = g.Key.MaterialName,
                                          UnitRefId = g.Key.UnitRefId,
                                          DefaultUnitId = g.Key.DefaultUnitId,
                                          BilledQty = g.Sum(s => s.ReceivedQty),
                                          NetAmount = g.Sum(s => s.ReceivedValue),
                                          PurchaseDate = g.Key.ReceivedTime.Value
                                      });

            foreach (var items in matTrasferRecdList)
            {
                MaterialRateViewListDto m = new MaterialRateViewListDto();
                m.MaterialTypeId = items.MaterialTypeId;
                m.MaterialRefId = items.MaterialRefId;
                m.MaterialName = items.MaterialName;
                m.BilledQty = items.BilledQty;
                m.TotalAmount = items.TotalAmount;
                m.NetAmount = items.NetAmount;
                m.UnitRefId = items.UnitRefId;
                m.DefaultUnitId = items.DefaultUnitId;
                m.PurchaseDate = items.PurchaseDate;
                if (items.NetAmount == 0 || items.BilledQty == 0)
                    continue;

                m.AvgRate = Math.Round(items.NetAmount / items.BilledQty, roundDecimals, MidpointRounding.AwayFromZero);
                m.InvoiceDateRange = L("Transfer") + " " + startDate.ToString("MMM-dd") + " - " + endDate.ToString("MMM-dd");
                output.Add(m);
            }

            #endregion Based On InterTransfer

            #region Based On Yield

            // Yield Output Received
            {
                IQueryable<MaterialRateViewListDto> rsYieldList;
                rsYieldList = (from yld in rsYieldMaster
                               where (yld.CompletedTime.HasValue && DbFunctions.TruncateTime(yld.CompletedTime) >= startDate.Date
                            && DbFunctions.TruncateTime(yld.CompletedTime) <= endDate.Date)
                               join yldOutput in _yieldoutputRepo.GetAll()
                                   .Where(t => rateRequiredMaterialList.Contains(t.OutputMaterialRefId))
                               on yld.Id equals yldOutput.YieldRefId
                               join mat in _materialRepo.GetAll().Where(t => rateRequiredMaterialList.Contains(t.Id))
                               on yldOutput.OutputMaterialRefId equals mat.Id
                               group yldOutput by new { yld.CompletedTime, yldOutput.OutputMaterialRefId, mat.MaterialTypeId, mat.MaterialName, yldOutput.UnitRefId, mat.DefaultUnitId } into g
                               select new MaterialRateViewListDto
                               {
                                   MaterialTypeId = g.Key.MaterialTypeId,
                                   MaterialRefId = g.Key.OutputMaterialRefId,
                                   MaterialName = g.Key.MaterialName,
                                   UnitRefId = g.Key.UnitRefId,
                                   DefaultUnitId = g.Key.DefaultUnitId,
                                   BilledQty = g.Sum(s => s.OutputQty),
                                   NetAmount = g.Sum(s => s.YieldPrice * s.OutputQty),
                                   PurchaseDate = g.Key.CompletedTime.Value
                               });

                foreach (var items in rsYieldList)
                {
                    MaterialRateViewListDto m = new MaterialRateViewListDto();
                    m.MaterialTypeId = items.MaterialTypeId;
                    m.MaterialRefId = items.MaterialRefId;
                    m.MaterialName = items.MaterialName;
                    m.BilledQty = items.BilledQty;
                    m.TotalAmount = items.TotalAmount;
                    m.NetAmount = items.NetAmount;
                    m.UnitRefId = items.UnitRefId;
                    m.DefaultUnitId = items.DefaultUnitId;
                    if (items.NetAmount == 0 || items.BilledQty == 0)
                        continue;

                    m.AvgRate = Math.Round(items.NetAmount / items.BilledQty, roundDecimals, MidpointRounding.AwayFromZero);
                    m.PurchaseDate = items.PurchaseDate;
                    m.InvoiceDateRange = L("Yield") + " " + startDate.ToString("MMM-dd") + " - " + endDate.ToString("MMM-dd");
                    output.Add(m);
                }
            }

            #endregion Based On Yield

            #endregion Get The Rate from specified Range

            List<int> availableMaterial = output.Select(a => a.MaterialRefId).ToList();
            var notExist = availableMaterial.Except(rateRequiredMaterialList).ToList();
            var pendingMaterials = rateRequiredMaterialList.Except(availableMaterial).ToList();

            endDate = startDate.AddDays(-1);
            startDate = startDate.AddDays(-31);

            if (pendingMaterials.Count > 0)
            {
                int loopCnt = 0;
                do
                {
                    loopCnt++;
                    //  Get The Rate from Last 30 Days Range

                    #region Based On Invoice

                    matInvList = (from inv in invoices
                                  where (DbFunctions.TruncateTime(inv.AccountDate) >= startDate && DbFunctions.TruncateTime(inv.AccountDate) <= endDate)
                                  join invDet in rsInvoiceDetail
                                  on inv.Id equals invDet.InvoiceRefId
                                  join mat in _materialRepo.GetAll().Where(a => pendingMaterials.Contains(a.Id))
                                  on invDet.MaterialRefId equals mat.Id
                                  group invDet by new
                                  {
                                      inv.AccountDate,
                                      invDet.MaterialRefId,
                                      mat.MaterialTypeId,
                                      mat.MaterialName,
                                      mat.DefaultUnitId,
                                      invDet.UnitRefId
                                  } into g
                                  select new MaterialRateViewListDto
                                  {
                                      MaterialTypeId = g.Key.MaterialTypeId,
                                      MaterialRefId = g.Key.MaterialRefId,
                                      MaterialName = g.Key.MaterialName,
                                      UnitRefId = g.Key.UnitRefId,
                                      DefaultUnitId = g.Key.DefaultUnitId,
                                      BilledQty = g.Sum(s => s.TotalQty),
                                      TotalAmount = g.Sum(s => s.TotalAmount),
                                      NetAmount = g.Sum(s => s.NetAmount),
                                      AvgRate = g.Sum(s => s.NetAmount) > 0 ? Math.Round(g.Sum(s => s.NetAmount) / g.Sum(s => s.TotalQty), roundDecimals) : 0,
                                      PurchaseDate = g.Key.AccountDate
                                  });

                    foreach (var items in matInvList)
                    {
                        MaterialRateViewListDto m = new MaterialRateViewListDto();
                        m.MaterialTypeId = items.MaterialTypeId;
                        m.MaterialRefId = items.MaterialRefId;
                        m.MaterialName = items.MaterialName;
                        m.BilledQty = items.BilledQty;
                        m.TotalAmount = items.TotalAmount;
                        m.NetAmount = items.NetAmount;
                        m.UnitRefId = items.UnitRefId;
                        m.DefaultUnitId = items.DefaultUnitId;
                        m.AvgRate = items.AvgRate;
                        m.InvoiceDateRange = startDate.ToString("MMM-dd") + " - " + endDate.ToString("MMM-dd");
                        m.PurchaseDate = items.PurchaseDate;
                        output.Add(m);
                    }

                    #endregion Based On Invoice

                    // InterTransfer Received

                    #region Based On InterTransfer

                    matTrasferRecdList = (from rcd in rsInterTransferMaster
                                          where (rcd.ReceivedTime.HasValue && DbFunctions.TruncateTime(rcd.ReceivedTime) >= startDate
                                          && DbFunctions.TruncateTime(rcd.ReceivedTime) <= endDate)
                                          join rcdDet in _intertransferReceivedRepo.GetAll().Where(t => t.Price > 0 && pendingMaterials.Contains(t.MaterialRefId))
                                          on rcd.Id equals rcdDet.InterTransferRefId
                                          join mat in _materialRepo.GetAll().Where(a => pendingMaterials.Contains(a.Id))
                                          on rcdDet.MaterialRefId equals mat.Id
                                          group rcdDet by new
                                          {
                                              rcdDet.MaterialRefId,
                                              mat.MaterialTypeId,
                                              mat.MaterialName,
                                              rcdDet.UnitRefId,
                                              mat.DefaultUnitId,
                                              rcd.ReceivedTime
                                          } into g
                                          select new MaterialRateViewListDto
                                          {
                                              MaterialTypeId = g.Key.MaterialTypeId,
                                              MaterialRefId = g.Key.MaterialRefId,
                                              MaterialName = g.Key.MaterialName,
                                              UnitRefId = g.Key.UnitRefId,
                                              DefaultUnitId = g.Key.DefaultUnitId,
                                              BilledQty = g.Sum(s => s.ReceivedQty),
                                              NetAmount = g.Sum(s => s.ReceivedValue),
                                              PurchaseDate = g.Key.ReceivedTime.Value
                                          });

                    foreach (var items in matTrasferRecdList)
                    {
                        MaterialRateViewListDto m = new MaterialRateViewListDto();
                        m.MaterialTypeId = items.MaterialTypeId;
                        m.MaterialRefId = items.MaterialRefId;
                        m.MaterialName = items.MaterialName;
                        m.BilledQty = items.BilledQty;
                        m.TotalAmount = items.TotalAmount;
                        m.NetAmount = items.NetAmount;
                        m.UnitRefId = items.UnitRefId;
                        m.DefaultUnitId = items.DefaultUnitId;
                        if (items.NetAmount == 0 || items.BilledQty == 0)
                            continue;

                        m.AvgRate = Math.Round(items.NetAmount / items.BilledQty, roundDecimals, MidpointRounding.AwayFromZero);
                        m.PurchaseDate = items.PurchaseDate;
                        m.InvoiceDateRange = L("Transfer") + " " + startDate.ToString("MMM-dd") + " - " + endDate.ToString("MMM-dd");
                        output.Add(m);
                    }

                    #endregion Based On InterTransfer

                    //Yield

                    #region Based On Yield

                    // Yield Output Received
                    var rsPendingYieldList = (from yld in rsYieldMaster
                                              where (yld.CompletedTime.HasValue && DbFunctions.TruncateTime(yld.CompletedTime) >= startDate
                                           && DbFunctions.TruncateTime(yld.CompletedTime) <= endDate)
                                              join yldOutput in _yieldoutputRepo.GetAll()
                                                  .Where(t => rateRequiredMaterialList.Contains(t.OutputMaterialRefId))
                                              on yld.Id equals yldOutput.YieldRefId
                                              join mat in _materialRepo.GetAll().Where(t => rateRequiredMaterialList.Contains(t.Id))
                                              on yldOutput.OutputMaterialRefId equals mat.Id
                                              group yldOutput by new
                                              {
                                                  yld.CompletedTime,
                                                  yldOutput.OutputMaterialRefId,
                                                  mat.MaterialTypeId,
                                                  mat.MaterialName,
                                                  yldOutput.UnitRefId,
                                                  mat.DefaultUnitId
                                              } into g
                                              select new MaterialRateViewListDto
                                              {
                                                  MaterialTypeId = g.Key.MaterialTypeId,
                                                  MaterialRefId = g.Key.OutputMaterialRefId,
                                                  MaterialName = g.Key.MaterialName,
                                                  UnitRefId = g.Key.UnitRefId,
                                                  DefaultUnitId = g.Key.DefaultUnitId,
                                                  BilledQty = g.Sum(s => s.OutputQty),
                                                  NetAmount = g.Sum(s => s.YieldPrice * s.OutputQty),
                                                  PurchaseDate = g.Key.CompletedTime.Value
                                              });

                    foreach (var items in rsPendingYieldList)
                    {
                        MaterialRateViewListDto m = new MaterialRateViewListDto();
                        m.MaterialTypeId = items.MaterialTypeId;
                        m.MaterialRefId = items.MaterialRefId;
                        m.MaterialName = items.MaterialName;
                        m.BilledQty = items.BilledQty;
                        m.TotalAmount = items.TotalAmount;
                        m.NetAmount = items.NetAmount;
                        m.UnitRefId = items.UnitRefId;
                        m.DefaultUnitId = items.DefaultUnitId;
                        if (items.NetAmount == 0 || items.BilledQty == 0)
                            continue;

                        m.AvgRate = Math.Round(items.NetAmount / items.BilledQty, roundDecimals, MidpointRounding.AwayFromZero);
                        m.PurchaseDate = items.PurchaseDate;
                        m.InvoiceDateRange = L("Yield") + " " + startDate.ToString("MMM-dd") + " - " + endDate.ToString("MMM-dd");
                        output.Add(m);
                    }

                    #endregion Based On Yield

                    availableMaterial = output.Select(a => a.MaterialRefId).ToList();

                    endDate = startDate.AddDays(-1);
                    startDate = startDate.AddDays(-31);

                    pendingMaterials = rateRequiredMaterialList.Except(availableMaterial).ToList();
                    if (pendingMaterials.Count == 0)
                    {
                        loopCnt = 5;
                        break;
                    }
                } while (loopCnt <= 4);
            }

            //  Get The Material Rate For Purchase Before the filters + 120 Days
            if (pendingMaterials.Count > 0)
            {
                matInvList = (from inv in invoices
                              where (DbFunctions.TruncateTime(inv.AccountDate) < startDate)
                              join invDet in rsInvoiceDetail.Where(t => pendingMaterials.Contains(t.MaterialRefId))
                                  on inv.Id equals invDet.InvoiceRefId
                              join mat in _materialRepo.GetAll().Where(a => pendingMaterials.Contains(a.Id))
                              on invDet.MaterialRefId equals mat.Id
                              select new MaterialRateViewListDto
                              {
                                  MaterialTypeId = mat.MaterialTypeId,
                                  MaterialRefId = invDet.MaterialRefId,
                                  MaterialName = mat.MaterialName,
                                  BilledQty = invDet.TotalQty,
                                  TotalAmount = invDet.TotalAmount,
                                  NetAmount = invDet.NetAmount,
                                  UnitRefId = invDet.UnitRefId,
                                  DefaultUnitId = mat.DefaultUnitId,
                                  AvgRate = invDet.NetAmount > 0 ? Math.Round(invDet.NetAmount / invDet.TotalQty, roundDecimals) : 0,
                                  InvoiceDateRange = inv.InvoiceDate.ToString(),
                                  PurchaseDate = inv.AccountDate
                              });

                foreach (var items in matInvList)
                {
                    MaterialRateViewListDto m = new MaterialRateViewListDto();
                    m.MaterialTypeId = items.MaterialTypeId;
                    m.MaterialRefId = items.MaterialRefId;
                    m.MaterialName = items.MaterialName;
                    m.BilledQty = items.BilledQty;
                    m.TotalAmount = items.TotalAmount;
                    m.NetAmount = items.NetAmount;
                    m.AvgRate = items.AvgRate;
                    m.UnitRefId = items.UnitRefId;
                    m.DefaultUnitId = items.DefaultUnitId;
                    m.InvoiceDateRange = items.InvoiceDateRange;
                    m.PurchaseDate = items.PurchaseDate;
                    output.Add(m);
                }
            }
            availableMaterial = output.Select(a => a.MaterialRefId).ToList();
            pendingMaterials = rateRequiredMaterialList.Except(availableMaterial).ToList();
            var rsUnitConversion = await _unitConversionRepo.GetAllListAsync();

            if (pendingMaterials.Count() > 0)
            {
                var material = _materialRepo.GetAll();
                material = material.Where(a => pendingMaterials.Contains(a.Id));

                foreach (var mat in material)
                {
                    MaterialRateViewListDto m = new MaterialRateViewListDto();
                    m.MaterialTypeId = mat.MaterialTypeId;
                    m.MaterialRefId = mat.Id;
                    m.MaterialName = mat.MaterialName;
                    m.DefaultUnitId = mat.DefaultUnitId;
                    m.UnitRefId = mat.DefaultUnitId;

                    //var supplierPrice = rsSupplierMaterial.Where(t => t.MaterialRefId == mat.Id && t.LocationRefId == input.LocationRefId).OrderByDescending(t => t.MaterialPrice).FirstOrDefault();
                    //if (supplierPrice == null)
                    //{
                    //    supplierPrice = rsSupplierMaterial.Where(t => t.MaterialRefId == mat.Id && t.LocationRefId == null).OrderByDescending(t => t.MaterialPrice).FirstOrDefault();
                    //}
                    var tempsupplierPriceList = rsSupplierMaterial.Where(t => t.MaterialRefId == mat.Id && t.LocationRefId == input.LocationRefId).ToList();
                    if (tempsupplierPriceList.Count == 0)
                        tempsupplierPriceList = rsSupplierMaterial.Where(t => t.MaterialRefId == mat.Id && t.LocationRefId == null).ToList();
                    List<SupplierMaterialListDto> supplierPriceList = tempsupplierPriceList.MapTo<List<SupplierMaterialListDto>>();
                    decimal conversionFactor = 0.0m;
                    foreach (var sm in supplierPriceList)
                    {
                        if (mat.DefaultUnitId == sm.UnitRefId)
                        {
                            conversionFactor = 1;
                        }
                        else
                        {
                            var unitConversion = rsUnitConversion.FirstOrDefault(t => t.BaseUnitId == mat.DefaultUnitId && t.RefUnitId == sm.UnitRefId);
                            if (unitConversion != null)
                            {
                                conversionFactor = unitConversion.Conversion;
                            }
                            else
                            {
                                continue;
                            }
                            sm.MaterialPrice = sm.MaterialPrice * conversionFactor;
                        }
                    }
                    var supplierPrice = supplierPriceList.OrderByDescending(t => t.MaterialPrice).FirstOrDefault();

                    if (supplierPrice == null)
                    {
                        m.BilledQty = 0;
                        m.TotalAmount = 0;
                        m.NetAmount = 0;
                        m.AvgRate = 0;
                        m.InvoiceDateRange = "N/A";
                        m.PurchaseDate = mat.CreationTime;
                    }
                    else
                    {
                        m.BilledQty = 1;
                        m.TotalAmount = supplierPrice.MaterialPrice;
                        m.NetAmount = supplierPrice.MaterialPrice;
                        m.AvgRate = supplierPrice.MaterialPrice;
                        m.InvoiceDateRange = L("QuoteReference");
                        m.PurchaseDate = supplierPrice.CreationTime;
                    }

                    output.Add(m);
                }
            }

            List<MaterialRateViewListDto> tempList = new List<MaterialRateViewListDto>();
            List<int> MaterialListIds = output.Select(t => t.MaterialRefId).Distinct().ToList();

            foreach (var matId in MaterialListIds)
            {
                var maxDateRecordsFilter = output.Where(t => t.MaterialRefId == matId);
                if (maxDateRecordsFilter.Count() > 0)
                {
                    var maxRecord = maxDateRecordsFilter.OrderByDescending(t => t.PurchaseDate).ThenByDescending(t => t.AvgRate).FirstOrDefault();
                    tempList.Add(maxRecord);
                }
            }

            output = tempList;

            List<MaterialRateViewListDto> materialoutput = new List<MaterialRateViewListDto>();



            materialoutput = (from lst in output
                              join mat in _materialRepo.GetAll().WhereIf(input.IsHighValueItemOnly, t => t.IsHighValueItem == true)
                              on lst.MaterialRefId equals mat.Id
                              join un in _unitrepo.GetAll() on mat.DefaultUnitId equals un.Id
                              join uiss in _unitrepo.GetAll() on lst.UnitRefId equals uiss.Id
                              select new MaterialRateViewListDto
                              {
                                  MaterialTypeId = lst.MaterialTypeId,
                                  MaterialRefId = lst.MaterialRefId,
                                  MaterialName = lst.MaterialName,
                                  BilledQty = lst.BilledQty,
                                  Uom = un.Name,
                                  TotalAmount = lst.TotalAmount,
                                  NetAmount = lst.NetAmount,
                                  InvoiceDateRange = lst.InvoiceDateRange,
                                  UnitRefId = lst.UnitRefId,
                                  UnitRefName = uiss.Name,
                                  DefaultUnitId = mat.DefaultUnitId,
                                  DefaultUnitName = un.Name,
                                  PurchaseDate = lst.PurchaseDate
                              }).OrderBy(t => t.MaterialRefId).ToList();

            List<MaterialRateViewListDto> returnoutput = new List<MaterialRateViewListDto>();

            foreach (var group in materialoutput.GroupBy(t => t.MaterialRefId))
            {
                MaterialRateViewListDto matDto = new MaterialRateViewListDto();
                decimal conversionFactor;
                decimal totalQty = 0;
                decimal netAmount = 0;
                decimal totalAmount = 0;

                foreach (var lst in group.OrderBy(t => t.UnitRefId))
                {
                    matDto = lst;
                    if (lst.DefaultUnitId == lst.UnitRefId)
                    {
                        conversionFactor = 1;
                    }
                    else
                    {
                        var unitConversion = rsUnitConversion.FirstOrDefault(t => t.BaseUnitId == lst.UnitRefId && t.RefUnitId == lst.DefaultUnitId);
                        if (unitConversion != null)
                        {
                            conversionFactor = unitConversion.Conversion;
                        }
                        else
                        {
                            continue;
                        }
                    }
                    totalQty = totalQty + (lst.BilledQty * conversionFactor);
                    netAmount = netAmount + lst.NetAmount;
                    totalAmount = totalAmount + lst.TotalAmount;
                }

                matDto.NetAmount = netAmount;
                matDto.BilledQty = totalQty;
                matDto.TotalAmount = totalAmount;

                if (isCalculateAverageRateWithoutTax == true)
                {
                    if (matDto.TotalAmount > 0 && matDto.BilledQty > 0)
                        matDto.AvgRate = Math.Round(matDto.TotalAmount / matDto.BilledQty, roundDecimals, MidpointRounding.AwayFromZero);
                }
                else
                {
                    if (matDto.NetAmount > 0 && matDto.BilledQty > 0)
                        matDto.AvgRate = Math.Round(matDto.NetAmount / matDto.BilledQty, roundDecimals, MidpointRounding.AwayFromZero);
                }

                returnoutput.Add(matDto);
            }

            string columnDefs = "{name: app.localize('MaterialName'),field: 'materialNa.............me',width:340},{name: app.localize('OpeningStock'),field: 'openBalance'},{name: app.localize('Received'),field: 'received'},{name: app.localize('Excess'),field: 'excessReceived'},{name: app.localize('Issued'),field: 'issued'},{name: app.localize('Damage'),field: 'damaged'},{name: app.localize('Shortage'),field: 'shortage'},{name: app.localize('Return'),field: 'return'},{name: app.localize('ClosingStock'),field: 'clBalance'}";

            return new GetMaterialRateViewDtoOutput
            {
                MaterialRateView = returnoutput,
                totalCount = output.Count(),
                ColumnDefs = columnDefs
            };
        }

        public async Task<GetMaterialRateViewDtoOutput> GetMaterialRateView_Purchase_Avg_Last_N_Months(GetHouseReportMaterialRateInput input)
        {
            var isOmitFOCMaterialQuantities = await _settingManager.GetSettingValueAsync<bool>(AppSettings.HouseSettings.IsOmitFOCMaterialQuantities);
            var isCalculateAverageRateWithoutTax = await _settingManager.GetSettingValueAsync<bool>(AppSettings.HouseSettings.IsCalculateAverageRateWithoutTax);

            //#region SelectMaterialList
            //List<int> rateRequiredMaterialList = new List<int>();
            //var allMaterialsQb = _materialRepo.GetAll().WhereIf(input.IsHighValueItemOnly, t => t.IsHighValueItem == true);
            //if (input.MaterialRefIds != null && input.MaterialRefIds.Count > 0)
            //{
            //    List<int> arrMaterialTypes = _materialRepo.GetAll().Where(t => input.MaterialRefIds.Contains(t.Id)).ToList().Select(t => t.MaterialTypeId).ToList();
            //    if (!arrMaterialTypes.Contains((int)MaterialType.SEMI))
            //    {
            //        allMaterialsQb = allMaterialsQb.Where(t => input.MaterialRefIds.Contains(t.Id));
            //    }
            //    var arrIngrMaterialTypes = await _materialingredientRepo.GetAll().Where(t => input.MaterialRefIds.Contains(t.RecipeRefId)).Select(t => t.MaterialRefId).Distinct().ToListAsync();
            //    arrMaterialTypes = await _materialRepo.GetAll().Where(t => arrIngrMaterialTypes.Contains(t.Id))
            //            .Select(t => t.MaterialTypeId).Distinct().ToListAsync();
            //    if (!arrMaterialTypes.Contains((int)MaterialType.SEMI))
            //    {
            //        allMaterialsQb = allMaterialsQb.Where(t => input.MaterialRefIds.Contains(t.Id));
            //    }

            //}
            //rateRequiredMaterialList = await allMaterialsQb.Select(t => t.Id).ToListAsync();
            //var rsMaterialList_PurchaseAvgLastNMonths = await allMaterialsQb.ToListAsync();
            //#endregion

            #region SelectMaterialList

            List<int> rateRequiredMaterialList = new List<int>();
            var allMaterialsQb = _materialRepo.GetAll().WhereIf(input.IsHighValueItemOnly, t => t.IsHighValueItem == true);

            List<int> arrNeededSemiMaterialList = new List<int>();
            List<int> extraRawIngredients = new List<int>();
            if (input.MaterialRefIds != null && input.MaterialRefIds.Count > 0)
            {
                var arrMaterialTypes = await _materialRepo.GetAll().Where(t => input.MaterialRefIds.Contains(t.Id))
                        .Select(t => t.MaterialTypeId).Distinct().ToListAsync();
                //List<int> arrMaterialTypes = materialTypes.Select(t => t.MaterialTypeId).ToList();
                if (!arrMaterialTypes.Contains((int)MaterialType.SEMI))
                {
                    allMaterialsQb = allMaterialsQb.Where(t => input.MaterialRefIds.Contains(t.Id));
                }
                else
                {
                    var materialSemi = await _materialRepo.GetAll().Where(t => t.MaterialTypeId == (int)MaterialType.SEMI && input.MaterialRefIds.Contains(t.Id)).Select(t => t.Id).ToListAsync();
                    arrNeededSemiMaterialList.AddRange(materialSemi);
                    input.MaterialRefIds.AddRange(materialSemi);

                    materialSemi = await _materialRepo.GetAll().Where(t => t.MaterialTypeId == (int)MaterialType.SEMI && input.MaterialRefIds.Contains(t.Id)).Select(t => t.Id).ToListAsync();
                    arrNeededSemiMaterialList.AddRange(materialSemi);
                    input.MaterialRefIds.AddRange(materialSemi);
                }

                var arrIngrMaterialRefIds = await _materialingredientRepo.GetAll().Where(t => input.MaterialRefIds.Contains(t.RecipeRefId)).Select(t => t.MaterialRefId).Distinct().ToListAsync();
                arrMaterialTypes = await _materialRepo.GetAll().Where(t => arrIngrMaterialRefIds.Contains(t.Id))
                        .Select(t => t.MaterialTypeId).Distinct().ToListAsync();
                if (!arrMaterialTypes.Contains((int)MaterialType.SEMI))
                {
                    allMaterialsQb = allMaterialsQb.Where(t => input.MaterialRefIds.Contains(t.Id));
                }
                else
                {
                    List<int> arrRevisedList = new List<int>();
                    arrRevisedList.AddRange(arrIngrMaterialRefIds);
                    arrRevisedList.AddRange(input.MaterialRefIds);

                    arrIngrMaterialRefIds = await _materialingredientRepo.GetAll().Where(t => arrRevisedList.Contains(t.RecipeRefId)).Select(t => t.MaterialRefId).Distinct().ToListAsync();
                    arrRevisedList.AddRange(arrIngrMaterialRefIds);

                    arrIngrMaterialRefIds = await _materialingredientRepo.GetAll().Where(t => arrRevisedList.Contains(t.RecipeRefId)).Select(t => t.MaterialRefId).Distinct().ToListAsync();
                    arrRevisedList.AddRange(arrIngrMaterialRefIds);

                    input.MaterialRefIds = arrRevisedList;
                    allMaterialsQb = allMaterialsQb.Where(t => input.MaterialRefIds.Contains(t.Id));
                }
            }
            rateRequiredMaterialList = await allMaterialsQb.Select(t => t.Id).ToListAsync();
            var rsMaterialList_PurchaseAvgLastNMonths = await allMaterialsQb.ToListAsync();

            #endregion SelectMaterialList

            #region Queryiable Definition

            List<MaterialRateViewListDto> output = new List<MaterialRateViewListDto>();
            var rsSupplierMaterial = await _supplierMaterialRepo.GetAll()
                    .Where(t => rateRequiredMaterialList.Contains(t.MaterialRefId)).ToListAsync();
            var invoices = _invoiceRepo.GetAll();

            var rsInvoiceDetail = _invoiceDetailRepo.GetAll();
            if (isOmitFOCMaterialQuantities == true)
                rsInvoiceDetail = rsInvoiceDetail.Where(t => t.Price > 0);

            var rsInterTransferMaster = _intertransferRepo.GetAll();
            var rsYieldMaster = _yieldRepo.GetAll();

            if (input.Locations != null && input.Locations.Any())
            {
                var allLocations = input.Locations.Select(a => a.Id).ToArray();
                invoices = invoices.Where(a => allLocations.Contains(a.LocationRefId));
                rsInterTransferMaster = rsInterTransferMaster.Where(a => allLocations.Contains(a.LocationRefId));
                rsYieldMaster = rsYieldMaster.Where(a => allLocations.Contains(a.LocationRefId));
            }

            #endregion Queryiable Definition
            var rsUnitConversion = await _unitConversionRepo.GetAllListAsync();

            foreach (var lstMaterialGroup in rsMaterialList_PurchaseAvgLastNMonths.GroupBy(t => t.NoOfMonthAvgTaken))
            {
                var rateRequiredMaterialList_N_Months = rsMaterialList_PurchaseAvgLastNMonths.Where(t => t.NoOfMonthAvgTaken == lstMaterialGroup.Key).Select(t => t.Id).ToList();

                #region Get Rates Based On Average Price Tag

                DateTime startDate;
                DateTime endDate;

                //startDate = input.StartDate;
                startDate = input.EndDate.AddMonths(-1 * lstMaterialGroup.Key);
                endDate = input.EndDate;

                #region Get The Rate from specified Range

                #region Based On Purchase Invoice

                var matInvList = (from inv in invoices
                                  where (DbFunctions.TruncateTime(inv.AccountDate) >= startDate
                                  && DbFunctions.TruncateTime(inv.AccountDate) <= endDate)
                                  join invDet in rsInvoiceDetail on inv.Id equals invDet.InvoiceRefId
                                  join mat in allMaterialsQb.Where(t => rateRequiredMaterialList_N_Months.Contains(t.Id))
                          on invDet.MaterialRefId equals mat.Id
                                  group invDet by new { invDet.MaterialRefId, mat.MaterialTypeId, mat.MaterialName, invDet.UnitRefId, mat.DefaultUnitId } into g
                                  select new MaterialRateViewListDto
                                  {
                                      MaterialTypeId = g.Key.MaterialTypeId,
                                      MaterialRefId = g.Key.MaterialRefId,
                                      MaterialName = g.Key.MaterialName,
                                      UnitRefId = g.Key.UnitRefId,
                                      DefaultUnitId = g.Key.DefaultUnitId,
                                      BilledQty = g.Sum(s => s.TotalQty),
                                      TotalAmount = g.Sum(s => s.TotalAmount),
                                      NetAmount = g.Sum(s => s.NetAmount),
                                      AvgRate = g.Sum(s => s.NetAmount) > 0 ? Math.Round(g.Sum(s => s.NetAmount) / g.Sum(s => s.TotalQty), roundDecimals) : 0
                                  });

                foreach (var items in matInvList)
                {
                    MaterialRateViewListDto m = new MaterialRateViewListDto();
                    m.MaterialTypeId = items.MaterialTypeId;
                    if (m.MaterialTypeId == 0)
                        m.MaterialTypeRefName = L("RAW");
                    else if (m.MaterialTypeId == 1)
                        m.MaterialTypeRefName = L("SEMI");
                    m.MaterialRefId = items.MaterialRefId;
                    m.MaterialName = items.MaterialName;
                    if (m.MaterialName.Contains("Oil"))
                    {
                        int i = 10;
                    }
                    m.BilledQty = items.BilledQty;
                    m.TotalAmount = items.TotalAmount;
                    m.NetAmount = items.NetAmount;
                    m.UnitRefId = items.UnitRefId;
                    m.DefaultUnitId = items.DefaultUnitId;
                    m.AvgRate = items.AvgRate;
                    m.InvoiceDateRange = startDate.ToString("MMM-dd") + " - " + endDate.ToString("MMM-dd");
                    output.Add(m);
                }

                #endregion Based On Purchase Invoice

                #region Based On InterTransfer

                // InterTransfer Received
                var matTrasferRecdList = (from rcd in rsInterTransferMaster
                                          where (DbFunctions.TruncateTime(rcd.ReceivedTime) >= startDate
                                          && DbFunctions.TruncateTime(rcd.ReceivedTime) <= endDate)
                                          join rcdDet in _intertransferReceivedRepo.GetAll().Where(t => rateRequiredMaterialList_N_Months.Contains(t.MaterialRefId))
                                          on rcd.Id equals rcdDet.InterTransferRefId
                                          join mat in _materialRepo.GetAll().Where(t => rateRequiredMaterialList_N_Months.Contains(t.Id) && t.NoOfMonthAvgTaken == lstMaterialGroup.Key)
                                          on rcdDet.MaterialRefId equals mat.Id
                                          group rcdDet by new { rcdDet.MaterialRefId, mat.MaterialTypeId, mat.MaterialName, rcdDet.UnitRefId, mat.DefaultUnitId } into g
                                          select new MaterialRateViewListDto
                                          {
                                              MaterialTypeId = g.Key.MaterialTypeId,
                                              MaterialRefId = g.Key.MaterialRefId,
                                              MaterialName = g.Key.MaterialName,
                                              UnitRefId = g.Key.UnitRefId,
                                              DefaultUnitId = g.Key.DefaultUnitId,
                                              BilledQty = g.Sum(s => s.ReceivedQty),
                                              TotalAmount = g.Sum(s => s.ReceivedValue),
                                              NetAmount = g.Sum(s => s.ReceivedValue),
                                          });

                foreach (var items in matTrasferRecdList)
                {
                    MaterialRateViewListDto m = new MaterialRateViewListDto();
                    m.MaterialTypeId = items.MaterialTypeId;
                    m.MaterialRefId = items.MaterialRefId;
                    m.MaterialName = items.MaterialName;
                    m.BilledQty = items.BilledQty;
                    m.TotalAmount = items.TotalAmount;
                    m.NetAmount = items.NetAmount;
                    m.UnitRefId = items.UnitRefId;
                    m.DefaultUnitId = items.DefaultUnitId;
                    if (items.NetAmount == 0 || items.BilledQty == 0)
                        continue;

                    m.InvoiceDateRange = L("Transfer") + " " + startDate.ToString("MMM-dd") + " - " + endDate.ToString("MMM-dd");
                    output.Add(m);
                }

                #endregion Based On InterTransfer

                #region Based On Yield

                // Yield Output Received
                {
                    IQueryable<MaterialRateViewListDto> rsYieldList;
                    rsYieldList = (from yld in rsYieldMaster
                                   where (DbFunctions.TruncateTime(yld.CompletedTime) >= startDate.Date
                                && DbFunctions.TruncateTime(yld.CompletedTime) <= endDate.Date)
                                   join yldOutput in _yieldoutputRepo.GetAll()
                                       .Where(t => rateRequiredMaterialList_N_Months.Contains(t.OutputMaterialRefId))
                                   on yld.Id equals yldOutput.YieldRefId
                                   join mat in _materialRepo.GetAll().Where(t => rateRequiredMaterialList_N_Months.Contains(t.Id))
                                   on yldOutput.OutputMaterialRefId equals mat.Id
                                   group yldOutput by new { yldOutput.OutputMaterialRefId, mat.MaterialTypeId, mat.MaterialName, yldOutput.UnitRefId, mat.DefaultUnitId } into g
                                   select new MaterialRateViewListDto
                                   {
                                       MaterialTypeId = g.Key.MaterialTypeId,
                                       MaterialRefId = g.Key.OutputMaterialRefId,
                                       MaterialName = g.Key.MaterialName,
                                       UnitRefId = g.Key.UnitRefId,
                                       DefaultUnitId = g.Key.DefaultUnitId,
                                       BilledQty = g.Sum(s => s.OutputQty),
                                       TotalAmount = g.Sum(s => s.YieldPrice * s.OutputQty),
                                       NetAmount = g.Sum(s => s.YieldPrice * s.OutputQty),
                                   });

                    foreach (var items in rsYieldList)
                    {
                        MaterialRateViewListDto m = new MaterialRateViewListDto();
                        m.MaterialTypeId = items.MaterialTypeId;
                        m.MaterialRefId = items.MaterialRefId;
                        m.MaterialName = items.MaterialName;
                        m.BilledQty = items.BilledQty;
                        m.TotalAmount = items.TotalAmount;
                        m.NetAmount = items.NetAmount;
                        m.UnitRefId = items.UnitRefId;
                        m.DefaultUnitId = items.DefaultUnitId;
                        if (items.NetAmount == 0 || items.BilledQty == 0)
                            continue;

                        m.InvoiceDateRange = L("Yield") + " " + startDate.ToString("MMM-dd") + " - " + endDate.ToString("MMM-dd");
                        output.Add(m);
                    }
                }

                #endregion Based On Yield

                #endregion Get The Rate from specified Range

                List<int> availableMaterial = output.Select(a => a.MaterialRefId).ToList();
                var notExist = availableMaterial.Except(rateRequiredMaterialList_N_Months).ToList();
                var pendingMaterials = rateRequiredMaterialList_N_Months.Except(availableMaterial).ToList();

                endDate = startDate.AddDays(-1);
                startDate = endDate.AddMonths(-1 * lstMaterialGroup.Key);

                if (pendingMaterials.Count > 0)
                {
                    int loopCnt = 0;
                    do
                    {
                        loopCnt++;
                        //  Get The Rate from Last 30 Days Range

                        #region Based On Invoice

                        matInvList = (from inv in invoices
                                      where (DbFunctions.TruncateTime(inv.AccountDate) >= startDate && DbFunctions.TruncateTime(inv.AccountDate) <= endDate)
                                      join invDet in rsInvoiceDetail
                                      on inv.Id equals invDet.InvoiceRefId
                                      join mat in _materialRepo.GetAll().Where(a => pendingMaterials.Contains(a.Id))
                                      on invDet.MaterialRefId equals mat.Id
                                      group invDet by new
                                      {
                                          invDet.MaterialRefId,
                                          mat.MaterialTypeId,
                                          mat.MaterialName,
                                          mat.DefaultUnitId,
                                          invDet.UnitRefId
                                      } into g
                                      select new MaterialRateViewListDto
                                      {
                                          MaterialTypeId = g.Key.MaterialTypeId,
                                          MaterialRefId = g.Key.MaterialRefId,
                                          MaterialName = g.Key.MaterialName,
                                          UnitRefId = g.Key.UnitRefId,
                                          DefaultUnitId = g.Key.DefaultUnitId,
                                          BilledQty = g.Sum(s => s.TotalQty),
                                          TotalAmount = g.Sum(s => s.TotalAmount),
                                          NetAmount = g.Sum(s => s.NetAmount),
                                          AvgRate = g.Sum(s => s.NetAmount) > 0 ? Math.Round(g.Sum(s => s.NetAmount) / g.Sum(s => s.TotalQty), roundDecimals) : 0
                                      });

                        foreach (var items in matInvList)
                        {
                            MaterialRateViewListDto m = new MaterialRateViewListDto();
                            m.MaterialTypeId = items.MaterialTypeId;
                            m.MaterialRefId = items.MaterialRefId;
                            m.MaterialName = items.MaterialName;
                            m.BilledQty = items.BilledQty;
                            m.TotalAmount = items.TotalAmount;
                            m.NetAmount = items.NetAmount;
                            m.UnitRefId = items.UnitRefId;
                            m.DefaultUnitId = items.DefaultUnitId;
                            m.AvgRate = items.AvgRate;
                            m.InvoiceDateRange = startDate.ToString("MMM-dd") + " - " + endDate.ToString("MMM-dd");
                            output.Add(m);
                        }

                        #endregion Based On Invoice

                        // InterTransfer Received

                        #region Based On InterTransfer

                        matTrasferRecdList = (from rcd in rsInterTransferMaster
                                              where (DbFunctions.TruncateTime(rcd.ReceivedTime) >= startDate
                                              && DbFunctions.TruncateTime(rcd.ReceivedTime) <= endDate)
                                              join rcdDet in _intertransferReceivedRepo.GetAll().Where(t => t.Price > 0 && pendingMaterials.Contains(t.MaterialRefId))
                                              on rcd.Id equals rcdDet.InterTransferRefId
                                              join mat in _materialRepo.GetAll().Where(a => pendingMaterials.Contains(a.Id))
                                              on rcdDet.MaterialRefId equals mat.Id
                                              group rcdDet by new
                                              {
                                                  rcdDet.MaterialRefId,
                                                  mat.MaterialTypeId,
                                                  mat.MaterialName,
                                                  rcdDet.UnitRefId,
                                                  mat.DefaultUnitId
                                              } into g
                                              select new MaterialRateViewListDto
                                              {
                                                  MaterialTypeId = g.Key.MaterialTypeId,
                                                  MaterialRefId = g.Key.MaterialRefId,
                                                  MaterialName = g.Key.MaterialName,
                                                  UnitRefId = g.Key.UnitRefId,
                                                  DefaultUnitId = g.Key.DefaultUnitId,
                                                  BilledQty = g.Sum(s => s.ReceivedQty),
                                                  TotalAmount = g.Sum(s => s.ReceivedValue),
                                                  NetAmount = g.Sum(s => s.ReceivedValue),
                                              });

                        foreach (var items in matTrasferRecdList)
                        {
                            MaterialRateViewListDto m = new MaterialRateViewListDto();
                            m.MaterialTypeId = items.MaterialTypeId;
                            m.MaterialRefId = items.MaterialRefId;
                            m.MaterialName = items.MaterialName;
                            m.BilledQty = items.BilledQty;
                            m.TotalAmount = items.TotalAmount;
                            m.NetAmount = items.NetAmount;
                            m.UnitRefId = items.UnitRefId;
                            m.DefaultUnitId = items.DefaultUnitId;
                            if (items.NetAmount == 0 || items.BilledQty == 0)
                                continue;

                            m.InvoiceDateRange = L("Transfer") + " " + startDate.ToString("MMM-dd") + " - " + endDate.ToString("MMM-dd");
                            output.Add(m);
                        }

                        #endregion Based On InterTransfer

                        //Yield

                        #region Based On Yield

                        // Yield Output Received
                        var rsPendingYieldList = (from yld in rsYieldMaster
                                                  where (DbFunctions.TruncateTime(yld.CompletedTime) >= startDate
                                               && DbFunctions.TruncateTime(yld.CompletedTime) <= endDate)
                                                  join yldOutput in _yieldoutputRepo.GetAll()
                                                      .Where(t => pendingMaterials.Contains(t.OutputMaterialRefId))
                                                  on yld.Id equals yldOutput.YieldRefId
                                                  join mat in _materialRepo.GetAll().Where(t => pendingMaterials.Contains(t.Id))
                                                  on yldOutput.OutputMaterialRefId equals mat.Id
                                                  group yldOutput by new { yldOutput.OutputMaterialRefId, mat.MaterialTypeId, mat.MaterialName, yldOutput.UnitRefId, mat.DefaultUnitId } into g
                                                  select new MaterialRateViewListDto
                                                  {
                                                      MaterialTypeId = g.Key.MaterialTypeId,
                                                      MaterialRefId = g.Key.OutputMaterialRefId,
                                                      MaterialName = g.Key.MaterialName,
                                                      UnitRefId = g.Key.UnitRefId,
                                                      DefaultUnitId = g.Key.DefaultUnitId,
                                                      BilledQty = g.Sum(s => s.OutputQty),
                                                      TotalAmount = g.Sum(s => s.YieldPrice * s.OutputQty),
                                                      NetAmount = g.Sum(s => s.YieldPrice * s.OutputQty),
                                                  });

                        foreach (var items in rsPendingYieldList)
                        {
                            MaterialRateViewListDto m = new MaterialRateViewListDto();
                            m.MaterialTypeId = items.MaterialTypeId;
                            m.MaterialRefId = items.MaterialRefId;
                            m.MaterialName = items.MaterialName;
                            m.BilledQty = items.BilledQty;
                            m.TotalAmount = items.TotalAmount;
                            m.NetAmount = items.NetAmount;
                            m.UnitRefId = items.UnitRefId;
                            m.DefaultUnitId = items.DefaultUnitId;
                            if (items.NetAmount == 0 || items.BilledQty == 0)
                                continue;

                            m.InvoiceDateRange = L("Yield") + " " + startDate.ToString("MMM-dd") + " - " + endDate.ToString("MMM-dd");
                            output.Add(m);
                        }

                        #endregion Based On Yield

                        availableMaterial = output.Select(a => a.MaterialRefId).ToList();

                        //endDate = startDate.AddDays(-1);
                        //startDate = startDate.AddDays(-31);
                        endDate = startDate.AddDays(-1);
                        startDate = endDate.AddMonths(-1 * lstMaterialGroup.Key);

                        pendingMaterials = rateRequiredMaterialList_N_Months.Except(availableMaterial).ToList();
                        if (pendingMaterials.Count == 0)
                        {
                            loopCnt = 5;
                            break;
                        }
                    } while (loopCnt <= 4);
                }

                //  Get The Material Rate For Purchase Before the filters + 120 Days
                if (pendingMaterials.Count > 0)
                {
                    matInvList = (from inv in invoices
                                  where (DbFunctions.TruncateTime(inv.AccountDate) < startDate)
                                  join invDet in rsInvoiceDetail.Where(t => pendingMaterials.Contains(t.MaterialRefId))
                                      on inv.Id equals invDet.InvoiceRefId
                                  join mat in _materialRepo.GetAll().Where(a => pendingMaterials.Contains(a.Id))
                                  on invDet.MaterialRefId equals mat.Id
                                  select new MaterialRateViewListDto
                                  {
                                      MaterialTypeId = mat.MaterialTypeId,
                                      MaterialRefId = invDet.MaterialRefId,
                                      MaterialName = mat.MaterialName,
                                      BilledQty = invDet.TotalQty,
                                      TotalAmount = invDet.TotalAmount,
                                      NetAmount = invDet.NetAmount,
                                      UnitRefId = invDet.UnitRefId,
                                      DefaultUnitId = mat.DefaultUnitId,
                                      AvgRate = invDet.NetAmount > 0 ? Math.Round(invDet.NetAmount / invDet.TotalQty, roundDecimals) : 0,
                                      InvoiceDateRange = inv.InvoiceDate.ToString()
                                  });

                    foreach (var items in matInvList)
                    {
                        MaterialRateViewListDto m = new MaterialRateViewListDto();
                        m.MaterialTypeId = items.MaterialTypeId;
                        m.MaterialRefId = items.MaterialRefId;
                        m.MaterialName = items.MaterialName;
                        m.BilledQty = items.BilledQty;
                        m.TotalAmount = items.TotalAmount;
                        m.NetAmount = items.NetAmount;
                        m.AvgRate = items.AvgRate;
                        m.UnitRefId = items.UnitRefId;
                        m.DefaultUnitId = items.DefaultUnitId;
                        m.InvoiceDateRange = items.InvoiceDateRange;
                        output.Add(m);
                    }
                }
                availableMaterial = output.Select(a => a.MaterialRefId).ToList();
                pendingMaterials = rateRequiredMaterialList_N_Months.Except(availableMaterial).ToList();

                if (pendingMaterials.Count() > 0)
                {
                    var material = _materialRepo.GetAll();
                    material = material.Where(a => pendingMaterials.Contains(a.Id));

                    foreach (var mat in material)
                    {
                        MaterialRateViewListDto m = new MaterialRateViewListDto();
                        m.MaterialTypeId = mat.MaterialTypeId;
                        m.MaterialRefId = mat.Id;
                        m.MaterialName = mat.MaterialName;
                        m.DefaultUnitId = mat.DefaultUnitId;
                        m.UnitRefId = mat.DefaultUnitId;

                        //var supplierPrice = rsSupplierMaterial.Where(t => t.MaterialRefId == mat.Id && t.LocationRefId == input.LocationRefId).OrderByDescending(t => t.MaterialPrice).FirstOrDefault();
                        //if (supplierPrice == null)
                        //{
                        //    supplierPrice = rsSupplierMaterial.Where(t => t.MaterialRefId == mat.Id && t.LocationRefId == null).OrderByDescending(t => t.MaterialPrice).FirstOrDefault();
                        //}
                        var tempsupplierPriceList = rsSupplierMaterial.Where(t => t.MaterialRefId == mat.Id && t.LocationRefId == input.LocationRefId).ToList();
                        if (tempsupplierPriceList.Count == 0)
                            tempsupplierPriceList = rsSupplierMaterial.Where(t => t.MaterialRefId == mat.Id && t.LocationRefId == null).ToList();
                        List<SupplierMaterialListDto> supplierPriceList = tempsupplierPriceList.MapTo<List<SupplierMaterialListDto>>();
                        decimal conversionFactor = 0.0m;
                        foreach (var sm in supplierPriceList)
                        {
                            if (mat.DefaultUnitId == sm.UnitRefId)
                            {
                                conversionFactor = 1;
                            }
                            else
                            {
                                var unitConversion = rsUnitConversion.FirstOrDefault(t => t.BaseUnitId == mat.DefaultUnitId && t.RefUnitId == sm.UnitRefId);
                                if (unitConversion != null)
                                {
                                    conversionFactor = unitConversion.Conversion;
                                }
                                else
                                {
                                    continue;
                                }
                                sm.MaterialPrice = sm.MaterialPrice * conversionFactor;
                            }
                        }
                        var supplierPrice = supplierPriceList.OrderByDescending(t => t.MaterialPrice).FirstOrDefault();

                        if (supplierPrice == null)
                        {
                            m.BilledQty = 0;
                            m.TotalAmount = 0;
                            m.NetAmount = 0;
                            m.AvgRate = 0;
                            m.InvoiceDateRange = "N/A";
                        }
                        else
                        {
                            m.BilledQty = 1;
                            m.TotalAmount = supplierPrice.MaterialPrice;
                            m.NetAmount = supplierPrice.MaterialPrice;
                            m.AvgRate = supplierPrice.MaterialPrice;
                            m.InvoiceDateRange = L("QuoteReference");
                        }

                        output.Add(m);
                    }
                }

                #endregion Get Rates Based On Average Price Tag
            }

            List<MaterialRateViewListDto> materialoutput = new List<MaterialRateViewListDto>();



            materialoutput = (from lst in output
                              join mat in _materialRepo.GetAll().WhereIf(input.IsHighValueItemOnly, t => t.IsHighValueItem == true)
                              on lst.MaterialRefId equals mat.Id
                              join un in _unitrepo.GetAll() on mat.DefaultUnitId equals un.Id
                              join uiss in _unitrepo.GetAll() on lst.UnitRefId equals uiss.Id
                              select new MaterialRateViewListDto
                              {
                                  MaterialTypeId = lst.MaterialTypeId,
                                  MaterialRefId = lst.MaterialRefId,
                                  MaterialName = lst.MaterialName,
                                  BilledQty = lst.BilledQty,
                                  TotalAmount = lst.TotalAmount,
                                  Uom = un.Name,
                                  AvgRate = lst.AvgRate, // Math.Round(lst.AvgRate, roundDecimals),
                                  NetAmount = lst.NetAmount,
                                  InvoiceDateRange = lst.InvoiceDateRange,
                                  UnitRefId = lst.UnitRefId,
                                  UnitRefName = uiss.Name,
                                  DefaultUnitId = mat.DefaultUnitId,
                                  DefaultUnitName = un.Name
                              }).OrderBy(t => t.MaterialRefId).ToList();

            List<MaterialRateViewListDto> returnoutput = new List<MaterialRateViewListDto>();

            foreach (var group in materialoutput.GroupBy(t => t.MaterialRefId))
            {
                MaterialRateViewListDto matDto = new MaterialRateViewListDto();
                decimal conversionFactor;
                decimal totalQty = 0;
                decimal netAmount = 0;
                decimal totalAmount = 0;

                foreach (var lst in group.OrderBy(t => t.UnitRefId))
                {
                    matDto = lst;
                    if (lst.DefaultUnitId == lst.UnitRefId)
                    {
                        conversionFactor = 1;
                    }
                    else
                    {
                        var unitConversion = rsUnitConversion.FirstOrDefault(t => t.BaseUnitId == lst.UnitRefId && t.RefUnitId == lst.DefaultUnitId);
                        if (unitConversion != null)
                        {
                            conversionFactor = unitConversion.Conversion;
                        }
                        else
                        {
                            continue;
                        }
                    }
                    totalQty = totalQty + (lst.BilledQty * conversionFactor);
                    netAmount = netAmount + lst.NetAmount;
                    totalAmount = totalAmount + lst.TotalAmount;
                }

                matDto.NetAmount = netAmount;
                matDto.BilledQty = totalQty;
                matDto.TotalAmount = totalAmount;

                if (isCalculateAverageRateWithoutTax == true)
                {
                    if (matDto.TotalAmount > 0 && matDto.BilledQty > 0)
                        matDto.AvgRate = Math.Round(matDto.TotalAmount / matDto.BilledQty, roundDecimals, MidpointRounding.AwayFromZero);
                }
                else
                {
                    if (matDto.NetAmount > 0 && matDto.BilledQty > 0)
                        matDto.AvgRate = Math.Round(matDto.NetAmount / matDto.BilledQty, roundDecimals, MidpointRounding.AwayFromZero);
                }

                returnoutput.Add(matDto);
            }

            if (input.Sorting != null)
                returnoutput = returnoutput.OrderBy(input.Sorting).ToList();

            if (input.MaterialTypeList != null && input.MaterialTypeList.Count > 0)
            {
                var stridList = input.MaterialTypeList.Select(t => t.Value).ToArray();
                int[] myMaterialIdList = Array.ConvertAll(stridList, s => int.Parse(s));

                allMaterialsQb = allMaterialsQb.Where(t => myMaterialIdList.Contains(t.MaterialTypeId));

                returnoutput = returnoutput.Where(t => myMaterialIdList.Contains(t.MaterialTypeId)).ToList();
            }

            string columnDefs = "{name: app.localize('MaterialName'),field: 'materialNa.............me',width:340},{name: app.localize('OpeningStock'),field: 'openBalance'},{name: app.localize('Received'),field: 'received'},{name: app.localize('Excess'),field: 'excessReceived'},{name: app.localize('Issued'),field: 'issued'},{name: app.localize('Damage'),field: 'damaged'},{name: app.localize('Shortage'),field: 'shortage'},{name: app.localize('Return'),field: 'return'},{name: app.localize('ClosingStock'),field: 'clBalance'}";

            return new GetMaterialRateViewDtoOutput
            {
                MaterialRateView = returnoutput,
                totalCount = output.Count(),
                ColumnDefs = columnDefs
            };
        }

        public async Task<GetMaterialRateViewDtoOutput> GetMaterialRateView_Moving_Average(GetHouseReportMaterialRateInput input)
        {
            var isOmitFOCMaterialQuantities = await _settingManager.GetSettingValueAsync<bool>(AppSettings.HouseSettings.IsOmitFOCMaterialQuantities);
            var isCalculateAverageRateWithoutTax = await _settingManager.GetSettingValueAsync<bool>(AppSettings.HouseSettings.IsCalculateAverageRateWithoutTax);

            DateTime startDate;
            DateTime endDate;

            startDate = input.StartDate;
            endDate = input.EndDate;

            //#region SelectMaterialList
            //List<int> rateRequiredMaterialList = new List<int>();
            //var allMaterialsQb = _materialRepo.GetAll().WhereIf(input.IsHighValueItemOnly, t => t.IsHighValueItem == true);
            ////if (input.MaterialRefIds != null && input.MaterialRefIds.Count > 0)
            ////{
            ////    List<int> arrMaterialTypes = _materialRepo.GetAll().Where(t => input.MaterialRefIds.Contains(t.Id)).ToList().Select(t => t.MaterialTypeId).ToList();
            ////    if (!arrMaterialTypes.Contains((int)MaterialType.SEMI))
            ////    {
            ////        allMaterialsQb = allMaterialsQb.Where(t => input.MaterialRefIds.Contains(t.Id));
            ////    }
            ////    var arrIngrMaterialTypes = await _materialingredientRepo.GetAll().Where(t => input.MaterialRefIds.Contains(t.RecipeRefId)).Select(t => t.MaterialRefId).Distinct().ToListAsync();
            ////    arrMaterialTypes = await _materialRepo.GetAll().Where(t => arrIngrMaterialTypes.Contains(t.Id))
            ////            .Select(t => t.MaterialTypeId).Distinct().ToListAsync();
            ////    if (!arrMaterialTypes.Contains((int)MaterialType.SEMI))
            ////    {
            ////        allMaterialsQb = allMaterialsQb.Where(t => input.MaterialRefIds.Contains(t.Id));
            ////    }

            ////}
            ////rateRequiredMaterialList = await allMaterialsQb.Select(t => t.Id).ToListAsync();

            //List<int> arrNeededSemiMaterialList = new List<int>();
            //List<int> extraRawIngredients = new List<int>();
            //if (input.MaterialRefIds != null && input.MaterialRefIds.Count > 0)
            //{
            //    var arrMaterialTypes = await _materialRepo.GetAll().Where(t => input.MaterialRefIds.Contains(t.Id))
            //            .Select(t => t.MaterialTypeId).Distinct().ToListAsync();
            //    //List<int> arrMaterialTypes = materialTypes.Select(t => t.MaterialTypeId).ToList();
            //    if (!arrMaterialTypes.Contains((int)MaterialType.SEMI))
            //    {
            //        allMaterialsQb = allMaterialsQb.Where(t => input.MaterialRefIds.Contains(t.Id));
            //    }
            //    else
            //    {
            //        var materialSemi = await _materialRepo.GetAll().Where(t => t.MaterialTypeId == (int)MaterialType.SEMI && input.MaterialRefIds.Contains(t.Id)).Select(t => t.Id).ToListAsync();
            //        arrNeededSemiMaterialList.AddRange(materialSemi);
            //    }

            //    var arrIngrMaterialRefIds = await _materialingredientRepo.GetAll().Where(t => input.MaterialRefIds.Contains(t.RecipeRefId)).Select(t => t.MaterialRefId).Distinct().ToListAsync();
            //    arrMaterialTypes = await _materialRepo.GetAll().Where(t => arrIngrMaterialRefIds.Contains(t.Id))
            //            .Select(t => t.MaterialTypeId).Distinct().ToListAsync();
            //    if (!arrMaterialTypes.Contains((int)MaterialType.SEMI))
            //    {
            //        allMaterialsQb = allMaterialsQb.Where(t => input.MaterialRefIds.Contains(t.Id));
            //    }
            //    else
            //    {
            //        var materialSemi = await _materialRepo.GetAll().Where(t => t.MaterialTypeId == (int)MaterialType.SEMI && arrIngrMaterialRefIds.Contains(t.Id)).Select(t => t.Id).ToListAsync();
            //        arrNeededSemiMaterialList.AddRange(materialSemi);
            //        input.MaterialRefIds.AddRange(arrNeededSemiMaterialList);
            //        List<int> arrRevisedList = new List<int>();
            //        arrRevisedList.AddRange(arrIngrMaterialRefIds);
            //        arrRevisedList.AddRange(arrNeededSemiMaterialList);
            //        arrRevisedList.AddRange(input.MaterialRefIds);
            //        input.MaterialRefIds = arrRevisedList;
            //        allMaterialsQb = allMaterialsQb.Where(t => input.MaterialRefIds.Contains(t.Id));
            //    }
            //}
            //rateRequiredMaterialList = await allMaterialsQb.Select(t => t.Id).ToListAsync();

            //var rsMaterialList_Weighted_Avg = await allMaterialsQb.ToListAsync();
            //var rsLocationWiseStock = await _materialLocationWiseStockRepo.GetAllListAsync(t => rateRequiredMaterialList.Contains(t.MaterialRefId) && t.LocationRefId == input.LocationRefId);
            //#endregion

            #region SelectMaterialList

            List<int> rateRequiredMaterialList = new List<int>();
            var allMaterialsQb = _materialRepo.GetAll().WhereIf(input.IsHighValueItemOnly, t => t.IsHighValueItem == true);

            List<int> arrNeededSemiMaterialList = new List<int>();
            List<int> extraRawIngredients = new List<int>();
            if (input.MaterialRefIds != null && input.MaterialRefIds.Count > 0)
            {
                var arrMaterialTypes = await _materialRepo.GetAll().Where(t => input.MaterialRefIds.Contains(t.Id))
                        .Select(t => t.MaterialTypeId).Distinct().ToListAsync();
                //List<int> arrMaterialTypes = materialTypes.Select(t => t.MaterialTypeId).ToList();
                if (!arrMaterialTypes.Contains((int)MaterialType.SEMI))
                {
                    allMaterialsQb = allMaterialsQb.Where(t => input.MaterialRefIds.Contains(t.Id));
                }
                else
                {
                    var materialSemi = await _materialRepo.GetAll().Where(t => t.MaterialTypeId == (int)MaterialType.SEMI && input.MaterialRefIds.Contains(t.Id)).Select(t => t.Id).ToListAsync();
                    arrNeededSemiMaterialList.AddRange(materialSemi);
                    input.MaterialRefIds.AddRange(materialSemi);

                    materialSemi = await _materialRepo.GetAll().Where(t => t.MaterialTypeId == (int)MaterialType.SEMI && input.MaterialRefIds.Contains(t.Id)).Select(t => t.Id).ToListAsync();
                    arrNeededSemiMaterialList.AddRange(materialSemi);
                    input.MaterialRefIds.AddRange(materialSemi);
                }

                var arrIngrMaterialRefIds = await _materialingredientRepo.GetAll().Where(t => input.MaterialRefIds.Contains(t.RecipeRefId)).Select(t => t.MaterialRefId).Distinct().ToListAsync();
                arrMaterialTypes = await _materialRepo.GetAll().Where(t => arrIngrMaterialRefIds.Contains(t.Id))
                        .Select(t => t.MaterialTypeId).Distinct().ToListAsync();
                if (!arrMaterialTypes.Contains((int)MaterialType.SEMI))
                {
                    allMaterialsQb = allMaterialsQb.Where(t => input.MaterialRefIds.Contains(t.Id));
                }
                else
                {
                    List<int> arrRevisedList = new List<int>();
                    arrRevisedList.AddRange(arrIngrMaterialRefIds);
                    arrRevisedList.AddRange(input.MaterialRefIds);

                    arrIngrMaterialRefIds = await _materialingredientRepo.GetAll().Where(t => arrRevisedList.Contains(t.RecipeRefId)).Select(t => t.MaterialRefId).Distinct().ToListAsync();
                    arrRevisedList.AddRange(arrIngrMaterialRefIds);

                    arrIngrMaterialRefIds = await _materialingredientRepo.GetAll().Where(t => arrRevisedList.Contains(t.RecipeRefId)).Select(t => t.MaterialRefId).Distinct().ToListAsync();
                    arrRevisedList.AddRange(arrIngrMaterialRefIds);

                    input.MaterialRefIds = arrRevisedList;
                    allMaterialsQb = allMaterialsQb.Where(t => input.MaterialRefIds.Contains(t.Id));
                }
            }
            rateRequiredMaterialList = await allMaterialsQb.Select(t => t.Id).ToListAsync();

            #endregion SelectMaterialList

            var rsMaterialList_Weighted_Avg = await allMaterialsQb.ToListAsync();
            var rsLocationWiseStock = await _materialLocationWiseStockRepo.GetAllListAsync(t => rateRequiredMaterialList.Contains(t.MaterialRefId) && t.LocationRefId == input.LocationRefId);

            #region Queryiable Filter

            List<MaterialRateViewListDto> output = new List<MaterialRateViewListDto>();
            var rsSupplierMaterial = await _supplierMaterialRepo.GetAll()
                    .Where(t => rateRequiredMaterialList.Contains(t.MaterialRefId)).ToListAsync();
            var invoices = _invoiceRepo.GetAll();
            var rsInvoiceDetail = _invoiceDetailRepo.GetAll();
            if (isOmitFOCMaterialQuantities == true)
                rsInvoiceDetail = rsInvoiceDetail.Where(t => t.Price > 0);

            var rsInterTransferMaster = _intertransferRepo.GetAll();
            var rsYieldMaster = _yieldRepo.GetAll();

            if (input.Locations != null && input.Locations.Any())
            {
                var allLocations = input.Locations.Select(a => a.Id).ToArray();
                invoices = invoices.Where(a => allLocations.Contains(a.LocationRefId));
                rsInterTransferMaster = rsInterTransferMaster.Where(a => allLocations.Contains(a.LocationRefId));
                rsYieldMaster = rsYieldMaster.Where(a => allLocations.Contains(a.LocationRefId));
            }

            #endregion Queryiable Filter

            #region Ledger Filter

            List<GetStartDateAndEndDate> startDateAndEndDatesForMaterial = new List<GetStartDateAndEndDate>();

            DateTime ledgerStartDate = input.EndDate.AddDays(-730);
            DateTime ledgerEndDate = input.EndDate;
            var ledger = _materialLedgerRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId && t.LedgerDate >= ledgerStartDate && t.LedgerDate <= ledgerEndDate);
            if (rateRequiredMaterialList.Count > 0)
                ledger = ledger.Where(t => rateRequiredMaterialList.Contains(t.MaterialRefId));
            var rsLedger = await ledger.ToListAsync();
            foreach (var mat in rsMaterialList_Weighted_Avg)
            {
                var matStock = rsLocationWiseStock.FirstOrDefault(t => t.MaterialRefId == mat.Id);
                if (matStock == null)
                    continue;
                GetStartDateAndEndDate newDto = new GetStartDateAndEndDate();
                newDto.MaterialRefId = mat.Id;
                if (matStock.CurrentInHand > 0)
                {
                    newDto.StockHand = matStock.CurrentInHand;
                    newDto.RunningStock = matStock.CurrentInHand;
                }
                else
                {
                    newDto.StockHand = 1;
                    newDto.RunningStock = 1;
                }
                var ledgerDetail = rsLedger.Where(t => t.MaterialRefId == mat.Id).OrderByDescending(t => t.LedgerDate).ToList();
                if (ledgerDetail.Count > 0)
                {
                    newDto.EndDate = ledgerDetail.FirstOrDefault().LedgerDate.Value;
                    foreach (var led in ledgerDetail)
                    {
                        newDto.RunningStock = newDto.RunningStock - (led.Received + led.TransferIn);
                        if (led.TransferIn > 0)
                        {
                            newDto.InterTransferExist = true;
                        }
                        if (newDto.RunningStock <= 0)
                        {
                            newDto.StartDate = led.LedgerDate.Value;
                            break;
                        }
                    }
                    if (newDto.StartDate != DateTime.MinValue)      // If ledger do not have all values
                        startDateAndEndDatesForMaterial.Add(newDto);
                    else
                    {
                        newDto.StartDate = newDto.EndDate.AddDays(-30);
                        startDateAndEndDatesForMaterial.Add(newDto);
                    }
                }
            }

            #endregion Ledger Filter

            #region Get The Rate from specified Range

            foreach (var matavg in startDateAndEndDatesForMaterial)
            {
                startDate = matavg.StartDate;
                endDate = matavg.EndDate;

                #region Based On Purchase Invoice

                var matInvList = (from inv in invoices
                                  where (DbFunctions.TruncateTime(inv.AccountDate) >= startDate
                                  && DbFunctions.TruncateTime(inv.AccountDate) <= endDate)
                                  join invDet in rsInvoiceDetail on inv.Id equals invDet.InvoiceRefId
                                  join mat in allMaterialsQb.Where(t => t.Id == matavg.MaterialRefId)
                          on invDet.MaterialRefId equals mat.Id
                                  group invDet by new { inv.AccountDate, invDet.MaterialRefId, mat.MaterialTypeId, mat.MaterialName, invDet.UnitRefId, mat.DefaultUnitId, inv.CreationTime } into g
                                  select new MaterialRateViewListDto
                                  {
                                      MaterialTypeId = g.Key.MaterialTypeId,
                                      MaterialRefId = g.Key.MaterialRefId,
                                      MaterialName = g.Key.MaterialName,
                                      UnitRefId = g.Key.UnitRefId,
                                      DefaultUnitId = g.Key.DefaultUnitId,
                                      BilledQty = g.Sum(s => s.TotalQty),
                                      TotalAmount = g.Sum(t => t.TotalAmount),
                                      NetAmount = g.Sum(s => s.NetAmount),
                                      AvgRate = g.Sum(s => s.NetAmount) > 0 ? Math.Round(g.Sum(s => s.NetAmount) / g.Sum(s => s.TotalQty), roundDecimals) : 0,
                                      PurchaseDate = g.Key.AccountDate,
                                      SortId = 1,
                                      LastUpdatedTime = g.Key.CreationTime
                                  });

                foreach (var items in matInvList)
                {
                    MaterialRateViewListDto m = new MaterialRateViewListDto();
                    m.MaterialTypeId = items.MaterialTypeId;
                    if (m.MaterialTypeId == 0)
                        m.MaterialTypeRefName = L("RAW");
                    else if (m.MaterialTypeId == 1)
                        m.MaterialTypeRefName = L("SEMI");
                    m.MaterialRefId = items.MaterialRefId;
                    m.MaterialName = items.MaterialName;
                    if (m.MaterialName.Contains("Oil"))
                    {
                        int i = 10;
                    }
                    m.BilledQty = items.BilledQty;
                    m.TotalAmount = items.TotalAmount;
                    m.NetAmount = items.NetAmount;
                    m.UnitRefId = items.UnitRefId;
                    m.DefaultUnitId = items.DefaultUnitId;
                    m.PurchaseDate = items.PurchaseDate;
                    m.SortId = items.SortId;
                    m.InvoiceDateRange = startDate.ToString("MMM-dd") + " - " + endDate.ToString("MMM-dd");
                    m.LastUpdatedTime = items.LastUpdatedTime;
                    output.Add(m);
                }

                #endregion Based On Purchase Invoice

                #region Based On InterTransfer

                // InterTransfer Received
                if (matavg.InterTransferExist)
                {
                    var matTrasferRecdList = (from rcd in rsInterTransferMaster
                                              where (rcd.ReceivedTime.HasValue && DbFunctions.TruncateTime(rcd.ReceivedTime) >= startDate
                                              && DbFunctions.TruncateTime(rcd.ReceivedTime) <= endDate)
                                              join rcdDet in _intertransferReceivedRepo.GetAll().Where(t => matavg.MaterialRefId == t.MaterialRefId)
                                              on rcd.Id equals rcdDet.InterTransferRefId
                                              join mat in _materialRepo.GetAll().Where(t => matavg.MaterialRefId == t.Id)
                                              on rcdDet.MaterialRefId equals mat.Id
                                              group rcdDet by new { rcd.ReceivedTime, rcdDet.MaterialRefId, mat.MaterialTypeId, mat.MaterialName, rcdDet.UnitRefId, mat.DefaultUnitId, rcd.ReceivedTime.Value, rcd.CreationTime } into g
                                              select new MaterialRateViewListDto
                                              {
                                                  MaterialTypeId = g.Key.MaterialTypeId,
                                                  MaterialRefId = g.Key.MaterialRefId,
                                                  MaterialName = g.Key.MaterialName,
                                                  UnitRefId = g.Key.UnitRefId,
                                                  DefaultUnitId = g.Key.DefaultUnitId,
                                                  BilledQty = g.Sum(s => s.ReceivedQty),
                                                  TotalAmount = g.Sum(s => s.ReceivedValue),
                                                  NetAmount = g.Sum(s => s.ReceivedValue),
                                                  PurchaseDate = g.Key.ReceivedTime.Value,
                                                  SortId = 2,
                                                  LastUpdatedTime = g.Key.CreationTime
                                              });

                    foreach (var items in matTrasferRecdList)
                    {
                        MaterialRateViewListDto m = new MaterialRateViewListDto();
                        m.MaterialTypeId = items.MaterialTypeId;
                        m.MaterialRefId = items.MaterialRefId;
                        m.MaterialName = items.MaterialName;
                        m.BilledQty = items.BilledQty;
                        m.TotalAmount = items.TotalAmount;
                        m.NetAmount = items.NetAmount;
                        m.UnitRefId = items.UnitRefId;
                        m.DefaultUnitId = items.DefaultUnitId;
                        if (items.NetAmount == 0 || items.BilledQty == 0)
                            continue;

                        m.InvoiceDateRange = L("Transfer") + " " + startDate.ToString("MMM-dd") + " - " + endDate.ToString("MMM-dd");
                        m.PurchaseDate = items.PurchaseDate;
                        m.SortId = items.SortId;
                        m.LastUpdatedTime = items.LastUpdatedTime;
                        output.Add(m);
                    }
                }

                #endregion Based On InterTransfer

                #region Based On Yield

                // Yield Output Received
                {
                    IQueryable<MaterialRateViewListDto> rsYieldList;
                    rsYieldList = (from yld in rsYieldMaster
                                   where (yld.CompletedTime.HasValue && DbFunctions.TruncateTime(yld.CompletedTime) >= startDate.Date
                                && DbFunctions.TruncateTime(yld.CompletedTime) <= endDate.Date)
                                   join yldOutput in _yieldoutputRepo.GetAll()
                                       .Where(t => matavg.MaterialRefId == t.OutputMaterialRefId)
                                   on yld.Id equals yldOutput.YieldRefId
                                   join mat in _materialRepo.GetAll().Where(t => matavg.MaterialRefId == t.Id)
                                   on yldOutput.OutputMaterialRefId equals mat.Id
                                   group yldOutput by new { yld.CompletedTime, yldOutput.OutputMaterialRefId, mat.MaterialTypeId, mat.MaterialName, yldOutput.UnitRefId, mat.DefaultUnitId, yldOutput.CreationTime } into g
                                   select new MaterialRateViewListDto
                                   {
                                       MaterialTypeId = g.Key.MaterialTypeId,
                                       MaterialRefId = g.Key.OutputMaterialRefId,
                                       MaterialName = g.Key.MaterialName,
                                       UnitRefId = g.Key.UnitRefId,
                                       DefaultUnitId = g.Key.DefaultUnitId,
                                       BilledQty = g.Sum(s => s.OutputQty),
                                       TotalAmount = g.Sum(s => s.YieldPrice * s.OutputQty),
                                       NetAmount = g.Sum(s => s.YieldPrice * s.OutputQty),
                                       PurchaseDate = g.Key.CompletedTime.Value,
                                       SortId = 3,
                                       LastUpdatedTime = g.Key.CreationTime
                                   });

                    foreach (var items in rsYieldList)
                    {
                        MaterialRateViewListDto m = new MaterialRateViewListDto();
                        m.MaterialTypeId = items.MaterialTypeId;
                        m.MaterialRefId = items.MaterialRefId;
                        m.MaterialName = items.MaterialName;
                        m.BilledQty = items.BilledQty;
                        m.TotalAmount = items.TotalAmount;
                        m.NetAmount = items.NetAmount;
                        m.UnitRefId = items.UnitRefId;
                        m.DefaultUnitId = items.DefaultUnitId;
                        if (items.NetAmount == 0 || items.BilledQty == 0)
                            continue;

                        m.InvoiceDateRange = L("Yield") + " " + startDate.ToString("MMM-dd") + " - " + endDate.ToString("MMM-dd");
                        m.PurchaseDate = items.PurchaseDate;
                        m.SortId = items.SortId;
                        m.LastUpdatedTime = items.LastUpdatedTime;
                        output.Add(m);
                    }
                }

                #endregion Based On Yield
            }

            #endregion Get The Rate from specified Range

            var availableMaterial = output.Select(a => a.MaterialRefId).ToList();
            var pendingMaterials = rateRequiredMaterialList.Except(availableMaterial).ToList();
            var rsUnitConversion = await _unitConversionRepo.GetAllListAsync();

            #region Supplier Material Rate

            if (pendingMaterials.Count() > 0)
            {
                var material = _materialRepo.GetAll();
                material = material.Where(a => pendingMaterials.Contains(a.Id));

                foreach (var mat in material)
                {
                    MaterialRateViewListDto m = new MaterialRateViewListDto();
                    m.MaterialTypeId = mat.MaterialTypeId;
                    m.MaterialRefId = mat.Id;
                    m.MaterialName = mat.MaterialName;
                    m.DefaultUnitId = mat.DefaultUnitId;
                    m.UnitRefId = mat.DefaultUnitId;

                    //var supplierPrice = rsSupplierMaterial.Where(t => t.MaterialRefId == mat.Id && t.LocationRefId == input.LocationRefId).OrderByDescending(t => t.MaterialPrice).FirstOrDefault();
                    //if (supplierPrice == null)
                    //{
                    //    supplierPrice = rsSupplierMaterial.Where(t => t.MaterialRefId == mat.Id && t.LocationRefId == null).OrderByDescending(t => t.MaterialPrice).FirstOrDefault();
                    //}
                    var tempsupplierPriceList = rsSupplierMaterial.Where(t => t.MaterialRefId == mat.Id && t.LocationRefId == input.LocationRefId).ToList();
                    if (tempsupplierPriceList.Count == 0)
                        tempsupplierPriceList = rsSupplierMaterial.Where(t => t.MaterialRefId == mat.Id && t.LocationRefId == null).ToList();
                    List<SupplierMaterialListDto> supplierPriceList = tempsupplierPriceList.MapTo<List<SupplierMaterialListDto>>();
                    decimal conversionFactor = 0.0m;
                    foreach (var sm in supplierPriceList)
                    {
                        if (mat.DefaultUnitId == sm.UnitRefId)
                        {
                            conversionFactor = 1;
                        }
                        else
                        {
                            var unitConversion = rsUnitConversion.FirstOrDefault(t => t.BaseUnitId == mat.DefaultUnitId && t.RefUnitId == sm.UnitRefId);
                            if (unitConversion != null)
                            {
                                conversionFactor = unitConversion.Conversion;
                            }
                            else
                            {
                                continue;
                            }
                            sm.MaterialPrice = sm.MaterialPrice * conversionFactor;
                        }
                    }
                    var supplierPrice = supplierPriceList.OrderByDescending(t => t.MaterialPrice).FirstOrDefault();

                    if (supplierPrice == null)
                    {
                        m.BilledQty = 0;
                        m.TotalAmount = 0;
                        m.NetAmount = 0;
                        m.AvgRate = 0;
                        m.InvoiceDateRange = "N/A";
                    }
                    else
                    {
                        m.BilledQty = 1;
                        m.TotalAmount = supplierPrice.MaterialPrice;
                        m.NetAmount = supplierPrice.MaterialPrice;
                        m.AvgRate = supplierPrice.MaterialPrice;
                        m.InvoiceDateRange = L("QuoteReference");
                    }

                    output.Add(m);
                }
            }

            #endregion Supplier Material Rate

            List<MaterialRateViewListDto> materialoutput = new List<MaterialRateViewListDto>();



            materialoutput = (from lst in output
                              join mat in _materialRepo.GetAll().WhereIf(input.IsHighValueItemOnly, t => t.IsHighValueItem == true)
                              on lst.MaterialRefId equals mat.Id
                              join un in _unitrepo.GetAll() on mat.DefaultUnitId equals un.Id
                              join uiss in _unitrepo.GetAll() on lst.UnitRefId equals uiss.Id
                              select new MaterialRateViewListDto
                              {
                                  MaterialTypeId = lst.MaterialTypeId,
                                  MaterialRefId = lst.MaterialRefId,
                                  MaterialName = lst.MaterialName,
                                  BilledQty = lst.BilledQty,
                                  TotalAmount = lst.TotalAmount,
                                  Uom = un.Name,
                                  NetAmount = lst.NetAmount,
                                  InvoiceDateRange = lst.InvoiceDateRange,
                                  UnitRefId = lst.UnitRefId,
                                  UnitRefName = uiss.Name,
                                  DefaultUnitId = mat.DefaultUnitId,
                                  DefaultUnitName = un.Name,
                                  LastUpdatedTime = lst.LastUpdatedTime
                              }).OrderBy(t => t.MaterialRefId).ThenByDescending(t => t.LastUpdatedTime).ToList();

            List<MaterialRateViewListDto> returnoutput = new List<MaterialRateViewListDto>();

            foreach (var group in materialoutput.GroupBy(t => t.MaterialRefId))
            {
                decimal stockOnHand = 0;
                var materialStockRequired = startDateAndEndDatesForMaterial.FirstOrDefault(t => t.MaterialRefId == group.Key);
                if (materialStockRequired == null)
                {
                    materialStockRequired = new GetStartDateAndEndDate();
                    stockOnHand = 1;
                }
                else
                {
                    stockOnHand = materialStockRequired.StockHand;
                }

                MaterialRateViewListDto matDto = new MaterialRateViewListDto();
                decimal conversionFactor;
                decimal totalQty = 0;
                decimal netAmount = 0;
                decimal totalAmount = 0;

                foreach (var lst in group.OrderByDescending(t => t.LastUpdatedTime))
                {
                    matDto = lst;
                    if (lst.DefaultUnitId == lst.UnitRefId)
                    {
                        conversionFactor = 1;
                    }
                    else
                    {
                        var unitConversion = rsUnitConversion.FirstOrDefault(t => t.BaseUnitId == lst.UnitRefId && t.RefUnitId == lst.DefaultUnitId);
                        if (unitConversion != null)
                        {
                            conversionFactor = unitConversion.Conversion;
                        }
                        else
                        {
                            continue;
                        }
                    }
                    totalQty = totalQty + (lst.BilledQty * conversionFactor);
                    netAmount = netAmount + lst.NetAmount;
                    totalAmount = totalAmount + lst.TotalAmount;

                    if (totalQty >= stockOnHand)
                        break;
                }

                matDto.NetAmount = netAmount;
                matDto.BilledQty = totalQty;
                matDto.TotalAmount = totalAmount;

                if (isCalculateAverageRateWithoutTax == true)
                {
                    if (matDto.TotalAmount > 0 && matDto.BilledQty > 0)
                        matDto.AvgRate = Math.Round(matDto.TotalAmount / matDto.BilledQty, roundDecimals, MidpointRounding.AwayFromZero);
                }
                else
                {
                    if (matDto.NetAmount > 0 && matDto.BilledQty > 0)
                        matDto.AvgRate = Math.Round(matDto.NetAmount / matDto.BilledQty, roundDecimals, MidpointRounding.AwayFromZero);
                }

                returnoutput.Add(matDto);
            }

            if (input.Sorting != null)
                returnoutput = returnoutput.OrderBy(input.Sorting).ToList();

            if (input.MaterialTypeList != null && input.MaterialTypeList.Count > 0)
            {
                var stridList = input.MaterialTypeList.Select(t => t.Value).ToArray();
                int[] myMaterialIdList = Array.ConvertAll(stridList, s => int.Parse(s));

                allMaterialsQb = allMaterialsQb.Where(t => myMaterialIdList.Contains(t.MaterialTypeId));

                returnoutput = returnoutput.Where(t => myMaterialIdList.Contains(t.MaterialTypeId)).ToList();
            }

            string columnDefs = "{name: app.localize('MaterialName'),field: 'materialNa.............me',width:340},{name: app.localize('OpeningStock'),field: 'openBalance'},{name: app.localize('Received'),field: 'received'},{name: app.localize('Excess'),field: 'excessReceived'},{name: app.localize('Issued'),field: 'issued'},{name: app.localize('Damage'),field: 'damaged'},{name: app.localize('Shortage'),field: 'shortage'},{name: app.localize('Return'),field: 'return'},{name: app.localize('ClosingStock'),field: 'clBalance'}";

            return new GetMaterialRateViewDtoOutput
            {
                MaterialRateView = returnoutput,
                totalCount = output.Count(),
                ColumnDefs = columnDefs
            };
        }

        public async Task<GetStockSummaryDtoOutput> GetStockSummary(GetHouseReportInput input)
        {
            GetStockSummaryDtoOutput result = new GetStockSummaryDtoOutput();
            List<MaterialMenuMappingWithWipeOut> mapWithMenuListSumGroup = new List<MaterialMenuMappingWithWipeOut>();

            List<MaterialMenuMappingWithWipeOut> saleMenuMappings = new List<MaterialMenuMappingWithWipeOut>();

            var loc = await _locationRepo.GetAll().Where(t => t.Id == input.LocationRefId).ToListAsync();

            var loclst = loc.MapTo<List<LocationListDto>>();

            int noOfDays = int.Parse(Math.Ceiling(input.EndDate.Date.Subtract(input.StartDate.Date).TotalDays).ToString());
            if (noOfDays == 0)
                noOfDays = 1;

            decimal OpStock = 0.0m;
            decimal DamageQty = 0.0m;
            decimal ExcessQty = 0.0m;
            decimal ShortageQty = 0.0m;
            decimal IssuedTotQty = 0.0m;
            decimal SalesTotQty = 0.0m;
            decimal ReceivedQty = 0.0m;
            decimal ReturnQty = 0.0m;
            decimal SupplierReturnQty = 0.0m;
            decimal TotQty = 0.0m;
            decimal CurStock = 0.0m;
            decimal TransferIn = 0.0m;
            decimal TransferOut = 0.0m;
            decimal TotConsumption = 0.0m;
            decimal ClStock = 0.0m;

            var allMaterialsQb = _materialRepo.GetAll()
                                .WhereIf(input.IsHighValueItemOnly, t => t.IsHighValueItem == true);

            allMaterialsQb = allMaterialsQb.WhereIf(!input.MaterialName.IsNullOrEmpty(), t => t.MaterialName.Contains(input.MaterialName));

            if (input.MaterialRefId.HasValue)
            {
                if (input.MaterialRefIds == null)
                    input.MaterialRefIds = new List<int>();
                input.MaterialRefIds.Add(input.MaterialRefId.Value);
            }

            if (input.MaterialRefIds != null && input.MaterialRefIds.Any())
                allMaterialsQb = allMaterialsQb.WhereIf(input.MaterialRefIds != null && input.MaterialRefIds.Any(), t => input.MaterialRefIds.Contains(t.Id));

            if (input.MaterialTypeList != null && input.MaterialTypeList.Count > 0)
            {
                var stridList = input.MaterialTypeList.Select(t => t.Value).ToArray();
                int[] myMaterialIdList = Array.ConvertAll(stridList, s => int.Parse(s));

                allMaterialsQb = allMaterialsQb.Where(t => myMaterialIdList.Contains(t.MaterialTypeId));
            }

            if (input.RequestedMaterialList != null)
            {
                if (input.RequestedMaterialList.Count > 0)
                {
                    int[] arrMaterialRefIds = input.RequestedMaterialList.Select(t => t.Id).ToArray();
                    allMaterialsQb = allMaterialsQb.Where(t => arrMaterialRefIds.Contains(t.Id));
                }
            }

            List<Material> allMaterials = new List<Material>();
            if (input.CallfromIndex)
            {
                allMaterials = await allMaterialsQb
               .OrderBy(input.Sorting)
               .PageBy(input)
               .ToListAsync();
                result.TotalCount = await allMaterialsQb.CountAsync();
            }
            else
            {
                allMaterials = await allMaterialsQb.ToListAsync();
                result.TotalCount = allMaterials.Count();
            }

            if (allMaterials.Count == 0)
            {
                throw new UserFriendlyException(L("NoMaterialFound"));
            }

            List<int> MaterialRefIdFilter = allMaterials.Select(t => t.Id).ToList();

            List<MaterialLedgerDto> stk = new List<MaterialLedgerDto>();
            string materialNameList = "";

            GetMaterialRateViewDtoOutput output;
            if (input.ExcludeRateView == true)
            {
                output = new GetMaterialRateViewDtoOutput();
            }
            else
            {
                if (input.MaterialRateView == null)
                {
                    output = await GetMaterialRateView(new GetHouseReportMaterialRateInput
                    {
                        StartDate = input.StartDate.Date,
                        EndDate = input.EndDate.Date,
                        Locations = loclst,
                        MaterialRefIds = MaterialRefIdFilter,
                        FunctionCalledBy = "Get Stock Summary"
                    });
                }
                else
                {
                    output = new GetMaterialRateViewDtoOutput
                    {
                        MaterialRateView = input.MaterialRateView,
                    };
                }
            }

            var materialListWithRate = output.MaterialRateView;

            var rsMaterialGroupCategory = await _materialGroupCategoryRepo.GetAllListAsync();
            var rsMaterialGroup = await _materialGroupRepo.GetAllListAsync();
            var rsUnits = await _unitrepo.GetAllListAsync();

            #region Get Sales For Non Day Close Days

            result.VarianceStatusBeforeOrAfterStatus = "After";
            if (input.VarianceAndTheoreticalUsageNeeded)
            {
                #region Sales From Non Closed Dates

                DateTime saleStartDate = input.StartDate;
                DateTime saleEndDate = input.EndDate;
                if (input.StartDate > loc[0].HouseTransactionDate.Value)
                {
                    throw new UserFriendlyException("Start Date : " + input.StartDate.ToString("dd-MMM-yyyy") + " Should Be Less Than or Equal to House Account Date : " + loc[0].HouseTransactionDate.Value.ToString("dd-MMM-yyyy") + " For Variance Stock Report");
                }
                else if (input.StartDate <= loc[0].HouseTransactionDate.Value)
                {
                    saleStartDate = loc[0].HouseTransactionDate.Value.Date;
                }

                if (input.EndDate < loc[0].HouseTransactionDate)
                {
                    result.VarianceStatusBeforeOrAfterStatus = "After";
                    //  Not Needed To Get Sales, Because already Sales will in Material Ledger while doing Day Close Every day
                }
                else if (input.EndDate >= loc[0].HouseTransactionDate)
                {
                    result.VarianceStatusBeforeOrAfterStatus = "Before";
                    saleEndDate = input.EndDate;
                    saleMenuMappings = await _materialAppService.GetLiveStockSales(new GetMaterialLocationWiseStockInput
                    {
                        StartDate = saleStartDate,
                        EndDate = saleEndDate,
                        LocationRefId = input.LocationRefId,
                        LiveStock = true,
                        MaterialRefIds = MaterialRefIdFilter
                    });
                }

                #endregion Sales From Non Closed Dates

                var rsMaterialMappings = await _materialmenumappingRepo.GetAllListAsync();
                var rsMaterials = await _materialRepo.GetAllListAsync();
                var rsMenumappingDepartment = await _menumappingdepartmentRepo.GetAllListAsync();
                var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();
                var rsDeptList = await _departmentRepo.GetAll().Select(t => new { t.Id, t.Name }).ToListAsync();

                #region AutoCompWastageCalculation  Its Already Comp included in Sales , So its avoid the comp here.


                #endregion AutoCompWastageCalculation  Its Already Comp included in Sales , So its avoid the comp here.

                #region AutoMenuWastageCalculation

                var menuwastagemapwithMenuList = new List<MaterialMenuMappingWithWipeOut>();

                var mas = await _menuitemwastageRepo.GetAll().Where(t => DbFunctions.TruncateTime(t.SalesDate) >= DbFunctions.TruncateTime(saleStartDate) && DbFunctions.TruncateTime(t.SalesDate) <= DbFunctions.TruncateTime(saleEndDate) && t.LocationRefId == input.LocationRefId && t.AdjustmentRefId == null).ToListAsync();

                var masRefIds = mas.Select(t => t.Id).ToArray();

                var menuWastages = await (from det in _menuitemwastageDetailRepo.GetAll()
                    .Where(t => masRefIds.Contains(t.MenuItemWastageRefId))
                                          group det by new { det.PosMenuPortionRefId }
                    into g
                                          select new MenuItemWastageDetailEditDto
                                          {
                                              PosMenuPortionRefId = g.Key.PosMenuPortionRefId,
                                              WastageQty = g.Sum(t => t.WastageQty)
                                          }).ToListAsync();

                if (menuWastages.Count > 0)
                {
                    var menus = await (from menuportion in _menuitemportionRepo.GetAll()
                                       join menu in _menuitemRepo.GetAll().Where(a => a.ProductType == 1)
                                           on menuportion.MenuItemId equals menu.Id
                                       select new ProductListDto
                                       {
                                           PortionId = menuportion.Id,
                                           PortionName = string.Concat(menu.Name, " - ", menuportion.Name)
                                       }).ToListAsync();
                    foreach (var lst in menuWastages)
                    {
                        var menu = menus.FirstOrDefault(t => t.PortionId == lst.PosMenuPortionRefId);
                        if (menu != null)
                            lst.PosMenuPortionRefName = menu.PortionName;
                    }
                }

                result.MenuWastageConsolidated = menuWastages;

                foreach (var imw in menuWastages)
                {
                    var outputlist =
                        rsMaterialMappings.Where(
                            t => t.PosMenuPortionRefId == imw.PosMenuPortionRefId && t.AutoSalesDeduction && t.LocationRefId == input.LocationRefId).ToList();

                    if (outputlist == null || outputlist.Count == 0)
                    {
                        outputlist =
                        rsMaterialMappings.Where(
                            t => t.PosMenuPortionRefId == imw.PosMenuPortionRefId && t.AutoSalesDeduction && t.LocationRefId == null).ToList();
                    }

                    foreach (var a in outputlist)
                    {
                        var newmm = new MaterialMenuMappingWithWipeOut();
                        var mat = rsMaterials.FirstOrDefault(t => t.Id == a.MaterialRefId);

                        if (mat == null)
                        {
                            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                            {
                                var deletedMaterial = await _materialRepo.FirstOrDefaultAsync(t => t.Id == a.MaterialRefId);
                                var deletedMaterialName = deletedMaterial == null ? "" : deletedMaterial.MaterialName;
                                throw new UserFriendlyException(L("SomeOfTheLinkedMaterialsInMenuMappingDeleted") + " " + L("MATERIALNAMEISWRONG", deletedMaterialName));
                            }

                        }

                        newmm.PosMenuPortionRefId = a.PosMenuPortionRefId;
                        newmm.MenuQuantitySold = a.MenuQuantitySold;
                        newmm.MaterialRefId = a.MaterialRefId;
                        newmm.PortionQty = Math.Round(a.PortionQty / a.MenuQuantitySold * imw.WastageQty, 14);
                        newmm.PortionUnitId = a.PortionUnitId;
                        newmm.TotalSaleQuantity = imw.WastageQty;
                        newmm.AutoSalesDeduction = a.AutoSalesDeduction;
                        newmm.DefaultUnitId = mat.DefaultUnitId;

                        menuwastagemapwithMenuList.Add(newmm);
                    }
                }

                var mapwithwastMenuListGroupWithUnitConversion
                    = (from wastmenu in menuwastagemapwithMenuList
                       join uc in rsUc
                           on wastmenu.PortionUnitId equals uc.BaseUnitId
                       where uc.RefUnitId == wastmenu.DefaultUnitId
                       group wastmenu by new
                       {
                           wastmenu.MaterialRefId,
                           wastmenu.MaterialRefName,
                           wastmenu.DefaultUnitId,
                           wastmenu.PortionUnitId,
                           wastmenu.UnitRefName,
                           wastmenu.DefaultUnitName,
                           uc.Conversion
                       }
                        into g
                       select new MaterialMenuMappingWithWipeOut
                       {
                           MaterialRefId = g.Key.MaterialRefId,
                           PortionUnitId = g.Key.PortionUnitId,
                           UnitRefName = g.Key.UnitRefName,
                           DefaultUnitId = g.Key.DefaultUnitId,
                           DefaultUnitName = g.Key.DefaultUnitName,
                           PortionQty = g.Sum(t => t.PortionQty * g.Key.Conversion)
                       }).ToList().OrderBy(t => t.MaterialRefId);

                var mapWithwastMenuListSumGroup = (from wastmenu in mapwithwastMenuListGroupWithUnitConversion
                                                   group wastmenu by wastmenu.MaterialRefId
                    into g
                                                   select new MaterialMenuMappingWithWipeOut
                                                   {
                                                       MaterialRefId = g.Key,
                                                       PortionQty = g.Sum(t => t.PortionQty)
                                                   }).ToList();

                List<MaterialSalesWithWipeOutCloseDayDto> allWasteItems;
                allWasteItems = await (from mat in _materialRepo.GetAll().
                    Where(m => m.MaterialTypeId != (int)MaterialType.RAW)
                                       join unit in _unitrepo.GetAll()
                                           on mat.DefaultUnitId equals unit.Id
                                       join uiss in _unitrepo.GetAll()
                                           on mat.IssueUnitId equals uiss.Id
                                       select new MaterialSalesWithWipeOutCloseDayDto
                                       {
                                           MaterialRefId = mat.Id,
                                           MaterialRefName = mat.MaterialName,
                                           Uom = unit.Name,
                                           ClBalance = 0,
                                           WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                                           AutoSalesDeduction = false,
                                           DefaultUnitId = mat.DefaultUnitId,
                                           DefaultUnitName = unit.Name,
                                           IssueUnitId = mat.IssueUnitId,
                                           IssueUnitName = uiss.Name
                                       }).ToListAsync();

                List<MaterialSalesWithWipeOutCloseDayDto> allWastRawItemsAutoSales;
                allWastRawItemsAutoSales = await (from mat in _materialRepo.GetAll()
                    .Where(m => m.MaterialTypeId == (int)MaterialType.RAW)
                                                  join unit in _unitrepo.GetAll() on mat.DefaultUnitId equals unit.Id
                                                  join uiss in _unitrepo.GetAll()
                                                      on mat.IssueUnitId equals uiss.Id
                                                  select new MaterialSalesWithWipeOutCloseDayDto
                                                  {
                                                      MaterialRefId = mat.Id,
                                                      MaterialRefName = mat.MaterialName,
                                                      Uom = unit.Name,
                                                      ClBalance = 0,
                                                      WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                                                      //AutoSalesDeduction = menumap.AutoSalesDeduction,
                                                      DefaultUnitId = mat.DefaultUnitId,
                                                      DefaultUnitName = unit.Name,
                                                      IssueUnitId = mat.IssueUnitId,
                                                      IssueUnitName = uiss.Name
                                                  }).Distinct().ToListAsync();

                var wastageMenuItems = allWasteItems.Union(allWastRawItemsAutoSales).ToList();

                foreach (var wastageitem in wastageMenuItems)
                {
                    var wastage = mapWithwastMenuListSumGroup.FirstOrDefault(t => t.MaterialRefId == wastageitem.MaterialRefId);
                    if (wastage == null)
                    {
                        wastageitem.Damaged = 0;
                    }
                    else
                    {
                        wastageitem.Damaged = wastage.PortionQty;
                    }
                }

                var allMenuWastageListDtos = wastageMenuItems.MapTo<List<MaterialSalesWithWipeOutCloseDayDto>>();

                allMenuWastageListDtos = allMenuWastageListDtos.Where(t => t.Damaged > 0).ToList();

                allMenuWastageListDtos =
                    allMenuWastageListDtos.OrderBy(t => t.MaterialRefId).OrderBy(t => t.Remarks).ToList();
                result.MenuWastageData = allMenuWastageListDtos;

                #endregion AutoMenuWastageCalculation
            }

            #endregion Get Sales For Non Day Close Days

            #region Closing Stock Taken

            List<MaterialViewDto> closingStockDetailList = new List<MaterialViewDto>();
            if (input.VarianceAndTheoreticalUsageNeeded)
            {
                var closingstocks = await _closingStockRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId && DbFunctions.TruncateTime(t.StockDate) == DbFunctions.TruncateTime(input.EndDate)).ToListAsync();

                int[] closingIds = closingstocks.Select(t => t.Id).ToArray();

                closingStockDetailList = (from mat in _materialRepo.GetAll().Where(t => t.WipeOutStockOnClosingDay == false)
                                          join matCategoryGroup in _materialGroupCategoryRepo.GetAll() on mat.MaterialGroupCategoryRefId equals matCategoryGroup.Id
                                          join un in _unitrepo.GetAll() on mat.DefaultUnitId equals un.Id
                                          join cldetail in _closingStockDetailRepo.GetAll().Where(t => closingIds.Contains(t.ClosingStockRefId)) on mat.Id equals cldetail.MaterialRefId
                                          select new MaterialViewDto
                                          {
                                              Id = mat.Id,
                                              MaterialName = mat.MaterialName,
                                              MaterialPetName = mat.MaterialPetName,
                                              MaterialGroupCategoryName = matCategoryGroup.MaterialGroupCategoryName,
                                              DefaultUnitId = mat.DefaultUnitId,
                                              DefaultUnitName = un.Name,
                                              OnHand = cldetail.OnHand,
                                              IsHighValueItem = mat.IsHighValueItem
                                          }).ToList();

                if (input.IsHighValueItemOnly == true)
                    closingStockDetailList = closingStockDetailList.Where(t => t.IsHighValueItem == true).ToList();
            }

            #endregion Closing Stock Taken

            foreach (var items in allMaterials)
            {
                //var maxLedgerDate = await _materialAppService.GetLatestLedgerForGivenLocationMaterialRefId(new LocationAndMaterialDto { MaterialRefId = items.Id, LocationRefId = input.LocationRefId, TransactionDate = input.StartDate.Date });

                var latestLedgerDate = await (from led in _materialLedgerRepo.GetAll()
                                              orderby led.LedgerDate descending
                                              where led.MaterialRefId == items.Id && led.LocationRefId == input.LocationRefId
                                              && DbFunctions.TruncateTime(led.LedgerDate) < DbFunctions.TruncateTime(input.StartDate)
                                              select led.LedgerDate).ToListAsync();

                if (latestLedgerDate.Count() == 0)
                {
                    var currDayRecord = _materialLedgerRepo.FirstOrDefault(l => (l.LedgerDate.Value.Year == input.StartDate.Year && l.LedgerDate.Value.Month == input.EndDate.Month && l.LedgerDate.Value.Day == input.StartDate.Day)
                        && l.MaterialRefId == items.Id && l.LocationRefId == input.LocationRefId);

                    OpStock = 0.0m;

                    if (currDayRecord != null)
                        OpStock = currDayRecord.OpenBalance;
                }
                else
                {
                    var maxLedgerDate = latestLedgerDate.Max();
                    var lastDaysRecord = _materialLedgerRepo.FirstOrDefault(l => (l.LedgerDate.Value.Year == maxLedgerDate.Value.Year && l.LedgerDate.Value.Month == maxLedgerDate.Value.Month && l.LedgerDate.Value.Day == maxLedgerDate.Value.Day)
                    && l.MaterialRefId == items.Id
                    && l.LocationRefId == input.LocationRefId);
                    OpStock = lastDaysRecord.ClBalance;
                }

                var matLedger = await _materialLedgerRepo.GetAll().Where(a => a.MaterialRefId == items.Id && a.LocationRefId == input.LocationRefId && (DbFunctions.TruncateTime(a.LedgerDate) >= input.StartDate.Date && DbFunctions.TruncateTime(a.LedgerDate) <= input.EndDate.Date)).ToListAsync();

                if (matLedger.Count > 0)
                {
                    ReceivedQty = matLedger.Sum(a => a.Received);
                    IssuedTotQty = matLedger.Sum(a => a.Issued);
                    SalesTotQty = matLedger.Sum(a => a.Sales);
                    DamageQty = matLedger.Sum(a => a.Damaged);
                    ExcessQty = matLedger.Sum(a => a.ExcessReceived);
                    SupplierReturnQty = matLedger.Sum(a => a.SupplierReturn);
                    ShortageQty = matLedger.Sum(a => a.Shortage);
                    ReturnQty = matLedger.Sum(a => a.Return);
                    TransferIn = matLedger.Sum(a => a.TransferIn);
                    TransferOut = matLedger.Sum(a => a.TransferOut);
                }
                else
                {
                    ReceivedQty = 0;
                    IssuedTotQty = 0;
                    SalesTotQty = 0;
                    DamageQty = 0;
                    ExcessQty = 0;
                    ShortageQty = 0;
                    ReturnQty = 0;
                    TransferIn = 0;
                    TransferOut = 0;
                    SupplierReturnQty = 0;
                }

                if (input.VarianceAndTheoreticalUsageNeeded)
                {
                    if (saleMenuMappings != null && saleMenuMappings.Any())
                    {
                        var salefromconnect = saleMenuMappings.FirstOrDefault(t => t.MaterialRefId == items.Id);
                        if (salefromconnect == null)
                        {
                            SalesTotQty = SalesTotQty + 0;
                        }
                        else
                        {
                            SalesTotQty = SalesTotQty + salefromconnect.PortionQty;
                        }
                    }

                    if (result.MenuWastageData != null && result.MenuWastageData.Any())
                    {
                        var damageFromMenuWastage = result.MenuWastageData.FirstOrDefault(t => t.MaterialRefId == items.Id);
                        if (damageFromMenuWastage != null)
                        {
                            DamageQty = DamageQty + damageFromMenuWastage.Damaged;
                        }
                    }
                }

                TotQty = OpStock + ReceivedQty + ExcessQty + ReturnQty + TransferIn;
                TotConsumption = IssuedTotQty + SalesTotQty + SupplierReturnQty + DamageQty + ShortageQty + TransferOut;

                CurStock = TotQty - TotConsumption;

                decimal ConsumptionWithOutTransferOut = (OpStock + ReceivedQty + ExcessQty + ReturnQty + TransferIn) - (IssuedTotQty + DamageQty + ShortageQty);

                var latestClosingLedgerDate = await (from led in _materialLedgerRepo.GetAll()
                                                     orderby led.LedgerDate descending
                                                     where led.MaterialRefId == items.Id && led.LocationRefId == input.LocationRefId
                                                     && DbFunctions.TruncateTime(led.LedgerDate) <= DbFunctions.TruncateTime(input.EndDate)
                                                     select led.LedgerDate).ToListAsync();

                if (input.VarianceAndTheoreticalUsageNeeded == true)
                {
                    ClStock = CurStock;
                }
                else
                {
                    if (latestClosingLedgerDate.Count() == 0)
                    {
                        ClStock = 0.0m;
                    }
                    else
                    {
                        var maxLedgerDate = latestClosingLedgerDate.Max();
                        var lastDaysRecord = _materialLedgerRepo.FirstOrDefault(l => (l.LedgerDate.Value.Year == maxLedgerDate.Value.Year && l.LedgerDate.Value.Month == maxLedgerDate.Value.Month && l.LedgerDate.Value.Day == maxLedgerDate.Value.Day)
                        && l.MaterialRefId == items.Id
                        && l.LocationRefId == input.LocationRefId);
                        ClStock = lastDaysRecord.ClBalance;
                    }
                }

                if (CurStock != ClStock)
                {
                    materialNameList = materialNameList + items.MaterialName + ",";
                    //throw new UserFriendlyException(L("ErrorInStockReport", items.MaterialName));
                }

                decimal materialprice = 0;
                if (materialListWithRate != null)
                    materialprice = materialListWithRate.FirstOrDefault(t => t.MaterialRefId == items.Id).AvgRate;
                else
                    materialprice = 0;

                var matGroupCategory = rsMaterialGroupCategory.FirstOrDefault(t => t.Id == items.MaterialGroupCategoryRefId);
                if (matGroupCategory == null)
                {
                    throw new UserFriendlyException("NotExistForId", L("MaterialGroupCategory", items.MaterialGroupCategoryRefId));
                }

                var matGroup = rsMaterialGroup.FirstOrDefault(t => t.Id == matGroupCategory.MaterialGroupRefId);
                if (matGroup == null)
                {
                    throw new UserFriendlyException("NotExistForId", L("MaterialGroup", matGroupCategory.MaterialGroupRefId));
                }

                MaterialLedgerDto ml = new MaterialLedgerDto();
                ml.LocationRefId = input.LocationRefId;
                ml.MaterialGroupRefId = matGroupCategory.MaterialGroupRefId;
                ml.MaterialGroupRefName = matGroup.MaterialGroupName;
                ml.MaterialGroupCategoryRefId = items.MaterialGroupCategoryRefId;
                ml.MaterialGroupCategoryRefName = matGroupCategory.MaterialGroupCategoryName;
                ml.MaterialRefId = items.Id;
                ml.MaterialPetName = items.MaterialPetName;
                ml.MaterialName = items.MaterialName;
                ml.MaterialTypeRefId = items.MaterialTypeId;
                if (ml.MaterialTypeRefId == 0)
                    ml.MaterialTypeRefName = L("RAW");
                else if (ml.MaterialTypeRefId == 1)
                    ml.MaterialTypeRefName = L("SEMI");
                ml.OpenBalance = OpStock;
                ml.Received = ReceivedQty;
                ml.ExcessReceived = ExcessQty;
                ml.Issued = IssuedTotQty;
                ml.Sales = SalesTotQty;
                ml.Damaged = DamageQty;
                ml.Shortage = ShortageQty;
                ml.Return = ReturnQty;
                ml.ClBalance = CurStock;
                ml.TransferIn = TransferIn;
                ml.TransferOut = TransferOut;
                ml.AvgPrice = materialprice;
                ml.SupplierReturn = SupplierReturnQty;
                ml.StockValue = materialprice * CurStock;
                ml.TotalConsumption = (IssuedTotQty - ReturnQty);
                ml.DefaultUnitRefId = items.DefaultUnitId;
                var unit = rsUnits.FirstOrDefault(t => t.Id == items.DefaultUnitId);
                ml.Uom = unit.Name;

                if (IssuedTotQty > 0)
                {
                    if ((IssuedTotQty - ReturnQty) > 0 && noOfDays > 0)
                        ml.AvgConsumption = (IssuedTotQty - ReturnQty) / noOfDays;
                    else
                        ml.AvgConsumption = 0;
                }
                else
                    ml.AvgConsumption = 0;

                if (ml.AvgConsumption < 0)
                {
                    ml.AvgConsumption = 0;
                    ml.TotalConsumption = 0;
                }

                decimal? closingStockEntryDoneByUser = 0m;
                decimal varianceStock = 0m;
                decimal actualUsage = 0m;
                if (input.VarianceAndTheoreticalUsageNeeded)
                {
                    var theoreticalUsage = TotConsumption;
                    ml.TheoreticalUsage = TotConsumption;

                    var expectedClosingStock = ClStock;
                    ml.ExpectedClosingStock = ClStock;

                    var clstkEntered = closingStockDetailList.FirstOrDefault(t => t.Id == items.Id);
                    if (clstkEntered == null)
                    {
                        closingStockEntryDoneByUser = null;
                        ml.VarianceStockDisplay = "N/A";
                        ml.ActualUsageDisplay = "N/A";
                        ml.EnteredClosingStockDisplay = "N/A";
                    }
                    else
                    {
                        closingStockEntryDoneByUser = clstkEntered.OnHand;
                        varianceStock = expectedClosingStock - closingStockEntryDoneByUser.Value;

                        actualUsage = TotQty - TotConsumption + (varianceStock);

                        //TotQty = OpStock + ReceivedQty + ExcessQty + ReturnQty + TransferIn;
                        //TotConsumption = IssuedTotQty + SalesTotQty + SupplierReturnQty + DamageQty + ShortageQty + TransferOut;
                        actualUsage = OpStock + ReceivedQty + ExcessQty + ReturnQty + TransferIn - IssuedTotQty - SupplierReturnQty - /*DamageQty - ShortageQty -*/ TransferOut - closingStockEntryDoneByUser.Value;

                        ml.EnteredClosingStock = closingStockEntryDoneByUser.Value;
                        ml.EnteredClosingStockDisplay = closingStockEntryDoneByUser.Value.ToString("#####0.000");
                        ml.VarianceStock = varianceStock;
                        ml.VarianceStockDisplay = varianceStock.ToString("#####0.000");
                        ml.ActualUsage = actualUsage;
                        ml.ActualUsageDisplay = actualUsage.ToString("#####0.000");
                    }
                }

                stk.Add(ml);
            }

            //string columnDefs = "{name: app.localize('MaterialName'),field: 'materialName',width:340},{name: app.localize('OpeningStock'),field: 'openBalance'},{name: app.localize('Received'),field: 'received'},{name: app.localize('Excess'),field: 'excessReceived'},{name: app.localize('Issued'),field: 'issued'},{name: app.localize('Damage'),field: 'damaged'},{name: app.localize('Shortage'),field: 'shortage'},{name: app.localize('Return'),field: 'return'},{name: app.localize('ClosingStock'),field: 'clBalance'}";

            string dateRange = input.StartDate.ToString("yyyy-MMM-dd") + " - " + input.EndDate.ToString("yyyy-MMM-dd");

            result.StockSummary = stk;
            //result.TotalCount = stk.Count();
            result.DateRange = dateRange;
            result.StartDate = input.StartDate;
            result.EndDate = input.EndDate;

            return result;
        }


        public async Task<GetStockSummaryDtoOutput> GetStockSummary_Version1(GetHouseReportInput input) // Based on Get All Data by group by
        {
            GetStockSummaryDtoOutput result = new GetStockSummaryDtoOutput();
            List<MaterialMenuMappingWithWipeOut> mapWithMenuListSumGroup = new List<MaterialMenuMappingWithWipeOut>();

            List<MaterialMenuMappingWithWipeOut> saleMenuMappings = new List<MaterialMenuMappingWithWipeOut>();

            var loc = await _locationRepo.GetAll().Where(t => t.Id == input.LocationRefId).ToListAsync();

            var loclst = loc.MapTo<List<LocationListDto>>();

            int noOfDays = int.Parse(Math.Ceiling(input.EndDate.Date.Subtract(input.StartDate.Date).TotalDays).ToString());
            if (noOfDays == 0)
                noOfDays = 1;
            List<int> arrMaterialRefIds = new List<int>();
            decimal OpStock = 0.0m;
            decimal DamageQty = 0.0m;
            decimal ExcessQty = 0.0m;
            decimal ShortageQty = 0.0m;
            decimal IssuedTotQty = 0.0m;
            decimal SalesTotQty = 0.0m;
            decimal ReceivedQty = 0.0m;
            decimal ReturnQty = 0.0m;
            decimal SupplierReturnQty = 0.0m;
            decimal TotQty = 0.0m;
            decimal CurStock = 0.0m;
            decimal TransferIn = 0.0m;
            decimal TransferOut = 0.0m;
            decimal TotConsumption = 0.0m;
            decimal ClStock = 0.0m;

            var allMaterialsQb = _materialRepo.GetAll()
                                .WhereIf(input.IsHighValueItemOnly, t => t.IsHighValueItem == true);
            if (input.RunInBackGround && input.TenantId > 0)
                allMaterialsQb = allMaterialsQb.Where(t => t.TenantId == input.TenantId);

            allMaterialsQb = allMaterialsQb.WhereIf(!input.MaterialName.IsNullOrEmpty(), t => t.MaterialName.Contains(input.MaterialName));

            if (input.MaterialRefId.HasValue)
            {
                if (input.MaterialRefIds == null)
                    input.MaterialRefIds = new List<int>();
                input.MaterialRefIds.Add(input.MaterialRefId.Value);
            }

            if (input.MaterialRefIds != null && input.MaterialRefIds.Any())
                allMaterialsQb = allMaterialsQb.WhereIf(input.MaterialRefIds != null && input.MaterialRefIds.Any(), t => input.MaterialRefIds.Contains(t.Id));

            if (input.MaterialTypeList != null && input.MaterialTypeList.Count > 0)
            {
                var stridList = input.MaterialTypeList.Select(t => t.Value).ToArray();
                int[] myMaterialIdList = Array.ConvertAll(stridList, s => int.Parse(s));

                allMaterialsQb = allMaterialsQb.Where(t => myMaterialIdList.Contains(t.MaterialTypeId));
            }

            if (input.RequestedMaterialList != null)
            {
                if (input.RequestedMaterialList.Count > 0)
                {
                    arrMaterialRefIds = input.RequestedMaterialList.Select(t => t.Id).ToList();
                    allMaterialsQb = allMaterialsQb.Where(t => arrMaterialRefIds.Contains(t.Id));
                }
            }

            List<Material> allMaterials = new List<Material>();
            if (input.CallfromIndex)
            {
                allMaterials = await allMaterialsQb
               .OrderBy(input.Sorting)
               .PageBy(input)
               .ToListAsync();
                result.TotalCount = await allMaterialsQb.CountAsync();
            }
            else
            {
                allMaterials = await allMaterialsQb.ToListAsync();
                result.TotalCount = allMaterials.Count();
            }

            if (allMaterials.Count == 0)
            {
                throw new UserFriendlyException(L("NoMaterialFound"));
            }

            List<int> MaterialRefIdFilter = allMaterials.Select(t => t.Id).ToList();

            List<MaterialLedgerDto> stk = new List<MaterialLedgerDto>();
            string materialNameList = "";

            GetMaterialRateViewDtoOutput materialRateViewDtoOutput;
            if (input.ExcludeRateView == true)
            {
                materialRateViewDtoOutput = new GetMaterialRateViewDtoOutput();
            }
            else
            {
                if (input.MaterialRateView == null || input.MaterialRateView.Count == 0)
                {
                    materialRateViewDtoOutput = await GetMaterialRateView(new GetHouseReportMaterialRateInput
                    {
                        StartDate = input.StartDate.Date,
                        EndDate = input.EndDate.Date,
                        Locations = loclst,
                        MaterialRefIds = MaterialRefIdFilter,
                        FunctionCalledBy = "Get Stock Summary",
                        RunInBackGround = input.RunInBackGround,
                        TenantId = input.TenantId
                    });
                }
                else
                {
                    materialRateViewDtoOutput = new GetMaterialRateViewDtoOutput
                    {
                        MaterialRateView = input.MaterialRateView,
                    };
                }
            }

            var materialListWithRate = materialRateViewDtoOutput.MaterialRateView;

            var rsMaterialGroupCategory = await _materialGroupCategoryRepo.GetAllListAsync();
            var rsMaterialGroup = await _materialGroupRepo.GetAllListAsync();
            var rsUnits = await _unitrepo.GetAllListAsync();

            #region Get Sales For Non Day Close Days

            result.VarianceStatusBeforeOrAfterStatus = "After";
            if (input.VarianceAndTheoreticalUsageNeeded)
            {
                #region Sales From Non Closed Dates

                DateTime saleStartDate = input.StartDate;
                DateTime saleEndDate = input.EndDate;
                if (input.StartDate > loc[0].HouseTransactionDate.Value)
                {
                    throw new UserFriendlyException("Start Date : " + input.StartDate.ToString("dd-MMM-yyyy") + " Should Be Less Than or Equal to House Account Date : " + loc[0].HouseTransactionDate.Value.ToString("dd-MMM-yyyy") + " For Variance Stock Report");
                }
                else if (input.StartDate <= loc[0].HouseTransactionDate.Value)
                {
                    saleStartDate = loc[0].HouseTransactionDate.Value.Date;
                }

                if (input.EndDate < loc[0].HouseTransactionDate)
                {
                    result.VarianceStatusBeforeOrAfterStatus = "After";
                    //  Not Needed To Get Sales, Because already Sales will in Material Ledger while doing Day Close Every day
                }
                else if (input.EndDate >= loc[0].HouseTransactionDate)
                {
                    result.VarianceStatusBeforeOrAfterStatus = "Before";
                    saleEndDate = input.EndDate;
                    saleMenuMappings = await _materialAppService.GetLiveStockSales(new GetMaterialLocationWiseStockInput
                    {
                        StartDate = saleStartDate,
                        EndDate = saleEndDate,
                        LocationRefId = input.LocationRefId,
                        LiveStock = true,
                        MaterialRefIds = MaterialRefIdFilter
                    });
                }

                #endregion Sales From Non Closed Dates

                var rsMaterialMappings = await _materialmenumappingRepo.GetAllListAsync();
                var rsMaterials = await _materialRepo.GetAllListAsync();
                var rsMenumappingDepartment = await _menumappingdepartmentRepo.GetAllListAsync();
                var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();
                var rsDeptList = await _departmentRepo.GetAll().Select(t => new { t.Id, t.Name }).ToListAsync();

                #region AutoCompWastageCalculation  Its Already Comp included in Sales , So its avoid the comp here.


                #endregion AutoCompWastageCalculation  Its Already Comp included in Sales , So its avoid the comp here.

                #region AutoMenuWastageCalculation

                var menuwastagemapwithMenuList = new List<MaterialMenuMappingWithWipeOut>();

                var mas = await _menuitemwastageRepo.GetAll().Where(t => DbFunctions.TruncateTime(t.SalesDate) >= DbFunctions.TruncateTime(saleStartDate) && DbFunctions.TruncateTime(t.SalesDate) <= DbFunctions.TruncateTime(saleEndDate) && t.LocationRefId == input.LocationRefId && t.AdjustmentRefId == null).ToListAsync();

                var masRefIds = mas.Select(t => t.Id).ToArray();

                var menuWastages = await (from det in _menuitemwastageDetailRepo.GetAll()
                    .Where(t => masRefIds.Contains(t.MenuItemWastageRefId))
                                          group det by new { det.PosMenuPortionRefId }
                    into g
                                          select new MenuItemWastageDetailEditDto
                                          {
                                              PosMenuPortionRefId = g.Key.PosMenuPortionRefId,
                                              WastageQty = g.Sum(t => t.WastageQty)
                                          }).ToListAsync();

                if (menuWastages.Count > 0)
                {
                    var menus = await (from menuportion in _menuitemportionRepo.GetAll()
                                       join menu in _menuitemRepo.GetAll().Where(a => a.ProductType == 1)
                                           on menuportion.MenuItemId equals menu.Id
                                       select new ProductListDto
                                       {
                                           PortionId = menuportion.Id,
                                           PortionName = string.Concat(menu.Name, " - ", menuportion.Name)
                                       }).ToListAsync();
                    foreach (var lst in menuWastages)
                    {
                        var menu = menus.FirstOrDefault(t => t.PortionId == lst.PosMenuPortionRefId);
                        if (menu != null)
                            lst.PosMenuPortionRefName = menu.PortionName;
                    }
                }

                result.MenuWastageConsolidated = menuWastages;

                foreach (var imw in menuWastages)
                {
                    var outputlist =
                        rsMaterialMappings.Where(
                            t => t.PosMenuPortionRefId == imw.PosMenuPortionRefId && t.AutoSalesDeduction && t.LocationRefId == input.LocationRefId).ToList();

                    if (outputlist == null || outputlist.Count == 0)
                    {
                        outputlist =
                        rsMaterialMappings.Where(
                            t => t.PosMenuPortionRefId == imw.PosMenuPortionRefId && t.AutoSalesDeduction && t.LocationRefId == null).ToList();
                    }

                    foreach (var a in outputlist)
                    {
                        var newmm = new MaterialMenuMappingWithWipeOut();
                        var mat = rsMaterials.FirstOrDefault(t => t.Id == a.MaterialRefId);

                        if (mat == null)
                        {
                            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                            {
                                var deletedMaterial = await _materialRepo.FirstOrDefaultAsync(t => t.Id == a.MaterialRefId);
                                var deletedMaterialName = deletedMaterial == null ? "" : deletedMaterial.MaterialName;
                                throw new UserFriendlyException(L("SomeOfTheLinkedMaterialsInMenuMappingDeleted") + " " + L("MATERIALNAMEISWRONG", deletedMaterialName));
                            }

                        }

                        newmm.PosMenuPortionRefId = a.PosMenuPortionRefId;
                        newmm.MenuQuantitySold = a.MenuQuantitySold;
                        newmm.MaterialRefId = a.MaterialRefId;
                        newmm.PortionQty = Math.Round(a.PortionQty / a.MenuQuantitySold * imw.WastageQty, 14);
                        newmm.PortionUnitId = a.PortionUnitId;
                        newmm.TotalSaleQuantity = imw.WastageQty;
                        newmm.AutoSalesDeduction = a.AutoSalesDeduction;
                        newmm.DefaultUnitId = mat.DefaultUnitId;

                        menuwastagemapwithMenuList.Add(newmm);
                    }
                }

                var mapwithwastMenuListGroupWithUnitConversion
                    = (from wastmenu in menuwastagemapwithMenuList
                       join uc in rsUc
                           on wastmenu.PortionUnitId equals uc.BaseUnitId
                       where uc.RefUnitId == wastmenu.DefaultUnitId
                       group wastmenu by new
                       {
                           wastmenu.MaterialRefId,
                           wastmenu.MaterialRefName,
                           wastmenu.DefaultUnitId,
                           wastmenu.PortionUnitId,
                           wastmenu.UnitRefName,
                           wastmenu.DefaultUnitName,
                           uc.Conversion
                       }
                        into g
                       select new MaterialMenuMappingWithWipeOut
                       {
                           MaterialRefId = g.Key.MaterialRefId,
                           PortionUnitId = g.Key.PortionUnitId,
                           UnitRefName = g.Key.UnitRefName,
                           DefaultUnitId = g.Key.DefaultUnitId,
                           DefaultUnitName = g.Key.DefaultUnitName,
                           PortionQty = g.Sum(t => t.PortionQty * g.Key.Conversion)
                       }).ToList().OrderBy(t => t.MaterialRefId);

                var mapWithwastMenuListSumGroup = (from wastmenu in mapwithwastMenuListGroupWithUnitConversion
                                                   group wastmenu by wastmenu.MaterialRefId
                    into g
                                                   select new MaterialMenuMappingWithWipeOut
                                                   {
                                                       MaterialRefId = g.Key,
                                                       PortionQty = g.Sum(t => t.PortionQty)
                                                   }).ToList();

                List<MaterialSalesWithWipeOutCloseDayDto> allWasteItems;
                allWasteItems = await (from mat in _materialRepo.GetAll().
                    Where(m => m.MaterialTypeId != (int)MaterialType.RAW)
                                       join unit in _unitrepo.GetAll()
                                           on mat.DefaultUnitId equals unit.Id
                                       join uiss in _unitrepo.GetAll()
                                           on mat.IssueUnitId equals uiss.Id
                                       select new MaterialSalesWithWipeOutCloseDayDto
                                       {
                                           MaterialRefId = mat.Id,
                                           MaterialRefName = mat.MaterialName,
                                           Uom = unit.Name,
                                           ClBalance = 0,
                                           WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                                           AutoSalesDeduction = false,
                                           DefaultUnitId = mat.DefaultUnitId,
                                           DefaultUnitName = unit.Name,
                                           IssueUnitId = mat.IssueUnitId,
                                           IssueUnitName = uiss.Name
                                       }).ToListAsync();

                List<MaterialSalesWithWipeOutCloseDayDto> allWastRawItemsAutoSales;
                allWastRawItemsAutoSales = await (from mat in _materialRepo.GetAll()
                    .Where(m => m.MaterialTypeId == (int)MaterialType.RAW)
                                                  join unit in _unitrepo.GetAll() on mat.DefaultUnitId equals unit.Id
                                                  join uiss in _unitrepo.GetAll()
                                                      on mat.IssueUnitId equals uiss.Id
                                                  select new MaterialSalesWithWipeOutCloseDayDto
                                                  {
                                                      MaterialRefId = mat.Id,
                                                      MaterialRefName = mat.MaterialName,
                                                      Uom = unit.Name,
                                                      ClBalance = 0,
                                                      WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                                                      //AutoSalesDeduction = menumap.AutoSalesDeduction,
                                                      DefaultUnitId = mat.DefaultUnitId,
                                                      DefaultUnitName = unit.Name,
                                                      IssueUnitId = mat.IssueUnitId,
                                                      IssueUnitName = uiss.Name
                                                  }).Distinct().ToListAsync();

                var wastageMenuItems = allWasteItems.Union(allWastRawItemsAutoSales).ToList();

                foreach (var wastageitem in wastageMenuItems)
                {
                    var wastage = mapWithwastMenuListSumGroup.FirstOrDefault(t => t.MaterialRefId == wastageitem.MaterialRefId);
                    if (wastage == null)
                    {
                        wastageitem.Damaged = 0;
                    }
                    else
                    {
                        wastageitem.Damaged = wastage.PortionQty;
                    }
                }

                var allMenuWastageListDtos = wastageMenuItems.MapTo<List<MaterialSalesWithWipeOutCloseDayDto>>();

                allMenuWastageListDtos = allMenuWastageListDtos.Where(t => t.Damaged > 0).ToList();

                allMenuWastageListDtos =
                    allMenuWastageListDtos.OrderBy(t => t.MaterialRefId).OrderBy(t => t.Remarks).ToList();
                result.MenuWastageData = allMenuWastageListDtos;

                #endregion AutoMenuWastageCalculation
            }

            #endregion Get Sales For Non Day Close Days

            #region Closing Stock Taken

            List<MaterialViewDto> closingStockDetailList = new List<MaterialViewDto>();
            if (input.VarianceAndTheoreticalUsageNeeded)
            {
                var closingstocks = await _closingStockRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId && DbFunctions.TruncateTime(t.StockDate) == DbFunctions.TruncateTime(input.EndDate)).ToListAsync();

                int[] closingIds = closingstocks.Select(t => t.Id).ToArray();

                closingStockDetailList = (from mat in _materialRepo.GetAll().Where(t => t.WipeOutStockOnClosingDay == false)
                                          join matCategoryGroup in _materialGroupCategoryRepo.GetAll() on mat.MaterialGroupCategoryRefId equals matCategoryGroup.Id
                                          join un in _unitrepo.GetAll() on mat.DefaultUnitId equals un.Id
                                          join cldetail in _closingStockDetailRepo.GetAll().Where(t => closingIds.Contains(t.ClosingStockRefId)) on mat.Id equals cldetail.MaterialRefId
                                          select new MaterialViewDto
                                          {
                                              Id = mat.Id,
                                              MaterialName = mat.MaterialName,
                                              MaterialPetName = mat.MaterialPetName,
                                              MaterialGroupCategoryName = matCategoryGroup.MaterialGroupCategoryName,
                                              DefaultUnitId = mat.DefaultUnitId,
                                              DefaultUnitName = un.Name,
                                              OnHand = cldetail.OnHand,
                                              IsHighValueItem = mat.IsHighValueItem
                                          }).ToList();

                if (input.IsHighValueItemOnly == true)
                    closingStockDetailList = closingStockDetailList.Where(t => t.IsHighValueItem == true).ToList();
            }

            #endregion Closing Stock Taken

            arrMaterialRefIds = allMaterials.Select(t => t.Id).ToList();
            var templedgersBetweenDates = await _materialLedgerRepo.GetAll()
                    .Where(t => t.LocationRefId == input.LocationRefId && arrMaterialRefIds.Contains(t.MaterialRefId)
                        && (DbFunctions.TruncateTime(t.LedgerDate) >= input.StartDate.Date
                        && DbFunctions.TruncateTime(t.LedgerDate) <= input.EndDate.Date)).ToListAsync();
            var ledgersBetweenDates = templedgersBetweenDates.MapTo<List<MaterialLedgerListDto>>();


            //var arrMaterialThoseHaveOpenBalanceOnStartDate = ledgersBetweenDates.Where(t => t.OpenBalance > 0 && t.LedgerDate.Value.Date == input.StartDate.Date).ToList();
            //arrMaterialThoseHaveOpenBalanceOnStartDate = new List<MaterialLedgerListDto>();
            //List<int> materialRefIdsMissedOpenBalance = arrMaterialRefIds.Except(arrMaterialThoseHaveOpenBalanceOnStartDate.Select(t => t.MaterialRefId).ToList()).ToList();

            var locationLedgers = await _materialAppService.GetLatestLedgerForGivenMultipleLocationMultipleMaterialRefId(
                new MultipleLocationAndMultipleMaterialDto
                {
                    LocationRefId = input.LocationRefId,
                    MaterialRefIdList = arrMaterialRefIds,
                    TransactionDate = input.StartDate
                });

            List<MaterialLedgerListDto> ledgersForOpenBalances = new List<MaterialLedgerListDto>();
            if (locationLedgers != null && locationLedgers.Count > 0)
                ledgersForOpenBalances = locationLedgers.FirstOrDefault(t => t.LocationRefId == input.LocationRefId).MaterialLedgerListDtos;
            //if (arrMaterialThoseHaveOpenBalanceOnStartDate.Count>0)
            //    ledgersForOpenBalances.AddRange(arrMaterialThoseHaveOpenBalanceOnStartDate);
            ledgersForOpenBalances = ledgersForOpenBalances.OrderBy(t => t.LocationRefId).ThenBy(t => t.LedgerDate).ThenBy(t => t.MaterialRefId).ToList();
            List<MaterialLedgerListDto> overAllLedgers = ledgersForOpenBalances;
            overAllLedgers.AddRange(ledgersBetweenDates);
            foreach (var items in allMaterials)
            {
                //var latestLedgerDate =  await (from led in _materialLedgerRepo.GetAll()
                //                              orderby led.LedgerDate descending
                //                              where led.MaterialRefId == items.Id && led.LocationRefId == input.LocationRefId
                //                              && DbFunctions.TruncateTime(led.LedgerDate) < DbFunctions.TruncateTime(input.StartDate)
                //                              select led.LedgerDate).ToListAsync();

                //if (latestLedgerDate.Count() == 0)
                //{
                //    var currDayRecord = _materialLedgerRepo.FirstOrDefault(l => (l.LedgerDate.Value.Year == input.StartDate.Year && l.LedgerDate.Value.Month == input.EndDate.Month && l.LedgerDate.Value.Day == input.StartDate.Day)
                //        && l.MaterialRefId == items.Id && l.LocationRefId == input.LocationRefId);

                //    OpStock = 0.0m;

                //    if (currDayRecord != null)
                //        OpStock = currDayRecord.OpenBalance;
                //}
                //else
                //{
                //    var maxLedgerDate = latestLedgerDate.Max();
                //    var lastDaysRecord = _materialLedgerRepo.FirstOrDefault(l => (l.LedgerDate.Value.Year == maxLedgerDate.Value.Year && l.LedgerDate.Value.Month == maxLedgerDate.Value.Month && l.LedgerDate.Value.Day == maxLedgerDate.Value.Day)
                //    && l.MaterialRefId == items.Id
                //    && l.LocationRefId == input.LocationRefId);
                //    OpStock = lastDaysRecord.ClBalance;
                //}

                var openingDayRecord = ledgersForOpenBalances.FirstOrDefault(t => t.MaterialRefId == items.Id && t.LocationRefId == input.LocationRefId);
                if (openingDayRecord == null)
                {
                    OpStock = 0.0m;
                }
                else
                {
                    OpStock = openingDayRecord.OpenBalance;
                }

                var matLedger = ledgersBetweenDates.Where(t => t.MaterialRefId == items.Id && t.LocationRefId == input.LocationRefId && t.LedgerDate.Value.Date >= input.StartDate.Date && t.LedgerDate.Value.Date <= input.EndDate.Date).ToList();

                if (matLedger.Count > 0)
                {
                    ReceivedQty = matLedger.Sum(a => a.Received);
                    IssuedTotQty = matLedger.Sum(a => a.Issued);
                    SalesTotQty = matLedger.Sum(a => a.Sales);
                    DamageQty = matLedger.Sum(a => a.Damaged);
                    ExcessQty = matLedger.Sum(a => a.ExcessReceived);
                    SupplierReturnQty = matLedger.Sum(a => a.SupplierReturn);
                    ShortageQty = matLedger.Sum(a => a.Shortage);
                    ReturnQty = matLedger.Sum(a => a.Return);
                    TransferIn = matLedger.Sum(a => a.TransferIn);
                    TransferOut = matLedger.Sum(a => a.TransferOut);
                }
                else
                {
                    ReceivedQty = 0;
                    IssuedTotQty = 0;
                    SalesTotQty = 0;
                    DamageQty = 0;
                    ExcessQty = 0;
                    ShortageQty = 0;
                    ReturnQty = 0;
                    TransferIn = 0;
                    TransferOut = 0;
                    SupplierReturnQty = 0;
                }

                if (input.VarianceAndTheoreticalUsageNeeded)
                {
                    if (saleMenuMappings != null && saleMenuMappings.Any())
                    {
                        var salefromconnect = saleMenuMappings.FirstOrDefault(t => t.MaterialRefId == items.Id);
                        if (salefromconnect == null)
                        {
                            SalesTotQty = SalesTotQty + 0;
                        }
                        else
                        {
                            SalesTotQty = SalesTotQty + salefromconnect.PortionQty;
                        }
                    }

                    if (result.MenuWastageData != null && result.MenuWastageData.Any())
                    {
                        var damageFromMenuWastage = result.MenuWastageData.FirstOrDefault(t => t.MaterialRefId == items.Id);
                        if (damageFromMenuWastage != null)
                        {
                            DamageQty = DamageQty + damageFromMenuWastage.Damaged;
                        }
                    }
                }

                TotQty = OpStock + ReceivedQty + ExcessQty + ReturnQty + TransferIn;
                TotConsumption = IssuedTotQty + SalesTotQty + SupplierReturnQty + DamageQty + ShortageQty + TransferOut;

                CurStock = TotQty - TotConsumption;

                decimal ConsumptionWithOutTransferOut = (OpStock + ReceivedQty + ExcessQty + ReturnQty + TransferIn) - (IssuedTotQty + DamageQty + ShortageQty);

                if (input.VarianceAndTheoreticalUsageNeeded == true)
                {
                    ClStock = CurStock;
                }
                else
                {
                    //var latestClosingLedgerDate = await (from led in _materialLedgerRepo.GetAll()
                    //                                     orderby led.LedgerDate descending
                    //                                     where led.MaterialRefId == items.Id && led.LocationRefId == input.LocationRefId
                    //                                     && DbFunctions.TruncateTime(led.LedgerDate) <= DbFunctions.TruncateTime(input.EndDate)
                    //                                     select led.LedgerDate).ToListAsync();
                    var lastDaysRecord = overAllLedgers.Where(t => t.MaterialRefId == items.Id && t.LocationRefId == input.LocationRefId && t.LedgerDate.Value.Date <= input.EndDate.Date).OrderByDescending(t => t.LedgerDate.Value).FirstOrDefault();
                    if (lastDaysRecord == null)
                    {
                        ClStock = 0.0m;
                    }
                    else
                    {
                        //var maxLedgerDate = latestClosingLedgerDate.Max();
                        //var lastDaysRecord = _materialLedgerRepo.FirstOrDefault(l => (l.LedgerDate.Value.Year == maxLedgerDate.Value.Year && l.LedgerDate.Value.Month == maxLedgerDate.Value.Month && l.LedgerDate.Value.Day == maxLedgerDate.Value.Day)
                        //&& l.MaterialRefId == items.Id
                        //&& l.LocationRefId == input.LocationRefId);
                        ClStock = lastDaysRecord.ClBalance;
                    }
                }

                if (CurStock != ClStock)
                {
                    materialNameList = materialNameList + items.MaterialName + ",";
                    //throw new UserFriendlyException(L("ErrorInStockReport", items.MaterialName));
                }

                decimal materialprice = 0;
                if (materialListWithRate != null)
                    materialprice = materialListWithRate.FirstOrDefault(t => t.MaterialRefId == items.Id).AvgRate;
                else
                    materialprice = 0;

                var matGroupCategory = rsMaterialGroupCategory.FirstOrDefault(t => t.Id == items.MaterialGroupCategoryRefId);
                if (matGroupCategory == null)
                {
                    throw new UserFriendlyException("NotExistForId", L("MaterialGroupCategory", items.MaterialGroupCategoryRefId));
                }

                var matGroup = rsMaterialGroup.FirstOrDefault(t => t.Id == matGroupCategory.MaterialGroupRefId);
                if (matGroup == null)
                {
                    throw new UserFriendlyException("NotExistForId", L("MaterialGroup", matGroupCategory.MaterialGroupRefId));
                }

                MaterialLedgerDto ml = new MaterialLedgerDto();
                ml.LocationRefId = input.LocationRefId;
                ml.MaterialGroupRefId = matGroupCategory.MaterialGroupRefId;
                ml.MaterialGroupRefName = matGroup.MaterialGroupName;
                ml.MaterialGroupCategoryRefId = items.MaterialGroupCategoryRefId;
                ml.MaterialGroupCategoryRefName = matGroupCategory.MaterialGroupCategoryName;
                ml.MaterialRefId = items.Id;
                ml.MaterialPetName = items.MaterialPetName;
                ml.MaterialName = items.MaterialName;
                ml.MaterialTypeRefId = items.MaterialTypeId;
                if (ml.MaterialTypeRefId == 0)
                    ml.MaterialTypeRefName = L("RAW");
                else if (ml.MaterialTypeRefId == 1)
                    ml.MaterialTypeRefName = L("SEMI");
                ml.OpenBalance = OpStock;
                ml.Received = ReceivedQty;
                ml.ExcessReceived = ExcessQty;
                ml.Issued = IssuedTotQty;
                ml.Sales = SalesTotQty;
                ml.Damaged = DamageQty;
                ml.Shortage = ShortageQty;
                ml.Return = ReturnQty;
                ml.ClBalance = CurStock;
                ml.TransferIn = TransferIn;
                ml.TransferOut = TransferOut;
                ml.AvgPrice = materialprice;
                ml.SupplierReturn = SupplierReturnQty;
                ml.StockValue = materialprice * CurStock;
                ml.TotalConsumption = (IssuedTotQty - ReturnQty);
                ml.DefaultUnitRefId = items.DefaultUnitId;
                var unit = rsUnits.FirstOrDefault(t => t.Id == items.DefaultUnitId);
                ml.Uom = unit.Name;

                if (IssuedTotQty > 0)
                {
                    if ((IssuedTotQty - ReturnQty) > 0 && noOfDays > 0)
                        ml.AvgConsumption = (IssuedTotQty - ReturnQty) / noOfDays;
                    else
                        ml.AvgConsumption = 0;
                }
                else
                    ml.AvgConsumption = 0;

                if (ml.AvgConsumption < 0)
                {
                    ml.AvgConsumption = 0;
                    ml.TotalConsumption = 0;
                }

                decimal? closingStockEntryDoneByUser = 0m;
                decimal varianceStock = 0m;
                decimal actualUsage = 0m;
                if (input.VarianceAndTheoreticalUsageNeeded)
                {
                    var theoreticalUsage = TotConsumption;
                    ml.TheoreticalUsage = TotConsumption;

                    var expectedClosingStock = ClStock;
                    ml.ExpectedClosingStock = ClStock;

                    var clstkEntered = closingStockDetailList.FirstOrDefault(t => t.Id == items.Id);
                    if (clstkEntered == null)
                    {
                        closingStockEntryDoneByUser = null;
                        ml.VarianceStockDisplay = "N/A";
                        ml.ActualUsageDisplay = "N/A";
                        ml.EnteredClosingStockDisplay = "N/A";
                    }
                    else
                    {
                        closingStockEntryDoneByUser = clstkEntered.OnHand;
                        varianceStock = expectedClosingStock - closingStockEntryDoneByUser.Value;

                        actualUsage = TotQty - TotConsumption + (varianceStock);

                        //TotQty = OpStock + ReceivedQty + ExcessQty + ReturnQty + TransferIn;
                        //TotConsumption = IssuedTotQty + SalesTotQty + SupplierReturnQty + DamageQty + ShortageQty + TransferOut;
                        actualUsage = OpStock + ReceivedQty + ExcessQty + ReturnQty + TransferIn - IssuedTotQty - SupplierReturnQty - /*DamageQty - ShortageQty -*/ TransferOut - closingStockEntryDoneByUser.Value;

                        ml.EnteredClosingStock = closingStockEntryDoneByUser.Value;
                        ml.EnteredClosingStockDisplay = closingStockEntryDoneByUser.Value.ToString("#####0.000");
                        ml.VarianceStock = varianceStock;
                        ml.VarianceStockDisplay = varianceStock.ToString("#####0.000");
                        ml.ActualUsage = actualUsage;
                        ml.ActualUsageDisplay = actualUsage.ToString("#####0.000");
                    }
                }

                stk.Add(ml);
            }

            //string columnDefs = "{name: app.localize('MaterialName'),field: 'materialName',width:340},{name: app.localize('OpeningStock'),field: 'openBalance'},{name: app.localize('Received'),field: 'received'},{name: app.localize('Excess'),field: 'excessReceived'},{name: app.localize('Issued'),field: 'issued'},{name: app.localize('Damage'),field: 'damaged'},{name: app.localize('Shortage'),field: 'shortage'},{name: app.localize('Return'),field: 'return'},{name: app.localize('ClosingStock'),field: 'clBalance'}";

            string dateRange = input.StartDate.ToString("yyyy-MMM-dd") + " - " + input.EndDate.ToString("yyyy-MMM-dd");

            result.StockSummary = stk;
            //result.TotalCount = stk.Count();
            result.DateRange = dateRange;
            result.StartDate = input.StartDate;
            result.EndDate = input.EndDate;
            result.MaterialRateView = materialListWithRate;
            return result;
        }

        public async Task<FileDto> GetCloseStockToExcel(GetStockSummaryDtoOutput input)
        {
            var allList = input.StockSummary;
            FileDto fileExcel = _housereportExporter.CloseStockToExcelFile(allList);
            return fileExcel;
        }

        public async Task<FileDto> RateViewToExcel(GetMaterialRateViewDtoOutput input)
        {
            FileDto fileExcel = await _housereportExporter.RateViewToExcelFile(input);
            return fileExcel;
        }

        public async Task<FileDto> SupplierRateViewToExcel(GetSupplierRateViewDtoOutput input)
        {
            var allList = input.SupplierRateView;
            FileDto fileExcel = _housereportExporter.SupplierRateViewToExcelFile(allList);
            return fileExcel;
        }

        public async Task<FileDto> GetStockToExcel(GetStockSummaryDtoOutput input)
        {
            var allList = input.StockSummary;
            int locationRefId = 0;

            if (allList.Count > 0)
                locationRefId = allList[0].LocationRefId;

            string locationName = "";

            var location = await _locationRepo.GetAll().Where(t => t.Id == locationRefId).ToListAsync();
            if (location.Count > 0)
                locationName = location[0].Name;

            List<SimpleLocationDto> locinput = location.MapTo<List<SimpleLocationDto>>();

            //  Get Sales
            var salesOut = await _connectReportAppService.GetTickets(
                new GetTicketInput
                {
                    StartDate = input.StartDate.Date,
                    EndDate = input.EndDate.Date,
                    Locations = locinput,
                }, true);

            decimal totalSalesWithoutTax = salesOut.DashBoardDto.TotalAmount;

            //Task<InterTransferReportDto> GetTransferDetailReport(InputInterTransferReport input)
            var toLocations = await _locationRepo.GetAllListAsync(t => t.Id != locationRefId);

            InputInterTransferReport transferOutDto = new InputInterTransferReport
            {
                LocationRefId = locationRefId,
                ToLocations = toLocations.MapTo<List<LocationListDto>>(),
                StartDate = input.StartDate.Date,
                EndDate = input.EndDate.Date
            };

            var transferOut = await GetTransferOutLocationWiseDetailReport(transferOutDto);

            List<InterTransferReportDetailOutPut> locTrasferOut = transferOut.LocationWiseConsolidatedTransferReport;

            InputInterTransferReport transferInDto = new InputInterTransferReport
            {
                LocationRefId = locationRefId,
                ToLocations = toLocations.MapTo<List<LocationListDto>>(),
                StartDate = input.StartDate.Date,
                EndDate = input.EndDate.Date
            };
            var transferIn = await GetTransferInLocationWiseDetailReport(transferInDto);
            List<InterTransferReportDetailOutPut> locTrasferIn = transferIn.LocationWiseConsolidatedTransferReport;

            FileDto fileExcel = new FileDto();
            if (input.IsNeedToBeDisplayedInSingleSheet)
            {
                fileExcel = _housereportExporter.StockToExcelFile_MergedIntoSingleSheet(allList, locationName,
                    input.DateRange, totalSalesWithoutTax, locTrasferOut, locTrasferIn);
            }
            else
            {
                fileExcel = _housereportExporter.StockToExcelFile(allList, locationName,
                    input.DateRange, totalSalesWithoutTax, locTrasferOut, locTrasferIn);
            }
            return fileExcel;
        }

        public async Task<GetScheduleMaterialList> GetStockTakenScheduledMaterialList(LocationWithDate input)
        {
            var isWhileClosingStockTakenCanAllUnitAllowed = await _settingManager.GetSettingValueAsync<bool>(AppSettings.HouseSettings.WhileClosingStockTakenCanAllUnitAllowed);

            string stockListRemarks = "";
            var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
            if (location == null)
            {
                throw new UserFriendlyException(L("NotExistForId", L("Location"), input.LocationRefId));
            }
            string locationName = location.Name.PadRight(10, '-');

            var QMaterial = _materialRepo.GetAll().Where(t => t.WipeOutStockOnClosingDay == false);
            if (input.IsHighValueItemOnly == true)
                QMaterial = QMaterial.Where(t => t.IsHighValueItem == true);


            var matList = await (from mat in QMaterial
                                 join matGroup in _materialGroupCategoryRepo.GetAll()
                           on mat.MaterialGroupCategoryRefId equals matGroup.Id
                                 join un in _unitrepo.GetAll()
                                 on mat.DefaultUnitId equals un.Id
                                 join matLoc in _materialLocationWiseStockRepo.GetAll().Where(t => t.LocationRefId == location.Id && t.IsActiveInLocation == true)
                                 on mat.Id equals matLoc.MaterialRefId
                                 select new MaterialViewDto
                                 {
                                     Id = mat.Id,
                                     MaterialName = mat.MaterialName,
                                     MaterialPetName = mat.MaterialPetName,
                                     MaterialGroupCategoryRefId = mat.MaterialGroupCategoryRefId,
                                     MaterialGroupCategoryName = matGroup.MaterialGroupCategoryName,
                                     UnitRefId = mat.DefaultUnitId,
                                     Uom = un.Name,
                                     IsHighValueItem = mat.IsHighValueItem,
                                     StockTakenReason = "",
                                     DefaultUnitId = mat.DefaultUnitId,
                                     IssueUnitId = mat.IssueUnitId,
                                     StockAdjustmentUnitId = mat.StockAdjustmentUnitId,
                                     TransferUnitId = mat.TransferUnitId
                                 })
                            .ToListAsync();

            var rsClStkUnitWiseDetails = await (from matunitlink in _materialUnitLinkRepo.GetAll()
                                                join uiss in _unitrepo.GetAll() on matunitlink.UnitRefId equals uiss.Id
                                                select new ClosingStockUnitWiseDetailViewDto
                                                {
                                                    MaterialRefId = matunitlink.MaterialRefId,
                                                    ClosingStockDetailRefId = 0,
                                                    OnHand = 0,
                                                    UnitRefId = matunitlink.UnitRefId,
                                                    UnitRefName = uiss.Name,
                                                }).ToListAsync();

            foreach (var lst in matList)
            {
                var unitWiseDetails = rsClStkUnitWiseDetails.Where(t => t.MaterialRefId == lst.Id).ToList();
                unitWiseDetails = unitWiseDetails.Distinct().ToList();
                if (isWhileClosingStockTakenCanAllUnitAllowed && input.UomTypeFilters.Count() == 0)
                {
                    lst.UnitWiseDetails = unitWiseDetails;
                }
                else if (input.UomTypeFilters.Count() == 0)
                {
                    List<ClosingStockUnitWiseDetailViewDto> unitListtobeShown = new List<ClosingStockUnitWiseDetailViewDto>();
                    List<int> arrUnitIds = new List<int>();
                    arrUnitIds.Add(lst.DefaultUnitId);  // Purchase Unit
                    arrUnitIds.Add(lst.StockAdjustmentUnitId);  //  Inventory Unit
                    //arrUnitIds.Add(lst.TransferUnitId);
                    arrUnitIds.Add(lst.IssueUnitId);        //  Recipe Unit
                    arrUnitIds = arrUnitIds.Distinct().ToList();
                    lst.UnitWiseDetails = unitWiseDetails.Where(t => arrUnitIds.Contains(t.UnitRefId)).ToList();
                }
                else
                {
                    List<ClosingStockUnitWiseDetailViewDto> unitListtobeShown = new List<ClosingStockUnitWiseDetailViewDto>();
                    List<int> arrUnitIds = new List<int>();
                    foreach (var unitFilter in input.UomTypeFilters)
                    {
                        int unitType = 0; // = int.Parse(unitFilter.Value);
                        if (Int32.TryParse(unitFilter.Value, out unitType))
                        {
                            if (unitType == (int)UnitTypeEnum.Default)
                            {
                                arrUnitIds.Add(lst.DefaultUnitId);  // Default Unit
                                arrUnitIds = arrUnitIds.Distinct().ToList();

                            }
                            else if (unitType == (int)UnitTypeEnum.Transaction)
                            {
                                arrUnitIds.Add(lst.IssueUnitId);  // Transaction Unit
                                arrUnitIds = arrUnitIds.Distinct().ToList();
                            }
                            else if (unitType == (int)UnitTypeEnum.Transfer)
                            {
                                arrUnitIds.Add(lst.TransferUnitId);  // Transfer Unit
                                arrUnitIds = arrUnitIds.Distinct().ToList();
                            }
                            else if (unitType == (int)UnitTypeEnum.Stock_Adjustment)
                            {
                                arrUnitIds.Add(lst.StockAdjustmentUnitId);  // Stock Adjustment Unit
                                arrUnitIds = arrUnitIds.Distinct().ToList();
                            }
                        }
                    }
                    lst.UnitWiseDetails = unitWiseDetails.Where(t => arrUnitIds.Contains(t.UnitRefId)).ToList();
                }
            }

            List<int> arrMaterialRefIds = matList.Select(t => t.Id).ToList();

            var rsInventoryCycleTags = await _inventoryCycleTagRepo.GetAllListAsync();
            var rsLocationGroup = await _locationGroupRepo.GetAllListAsync();

            List<MaterialViewDto> outputDtos = new List<MaterialViewDto>();
            DateTime lastDayOfMonth = input.StockTakenDate.Date;
            DateTime temp = new DateTime(lastDayOfMonth.Year, lastDayOfMonth.Month, 1);
            lastDayOfMonth = temp.AddMonths(1).AddDays(-1);

            var rsMaterialStockCycle = await _materialStockCycleRepo.GetAllListAsync(t => arrMaterialRefIds.Contains(t.MaterialRefId));
            var rsStockCycleLinkLocations = await _inventoryCycleLinkLocation.GetAllListAsync();
            var rsLocations = await _locationRepo.GetAllListAsync();

            var rsSupplierMaterialDtos = (from smat in _supplierMaterialRepo.GetAll()
                                          join sup in _supplierRepo.GetAll() on smat.SupplierRefId equals sup.Id
                                          join un in _unitrepo.GetAll() on smat.UnitRefId equals un.Id
                                          select new SupplierMaterialEditDto
                                          {
                                              Id = smat.Id,
                                              SupplierRefId = smat.SupplierRefId,
                                              SupplierRefName = sup.SupplierName,
                                              MaterialPrice = smat.MaterialPrice,
                                              MinimumOrderQuantity = smat.MinimumOrderQuantity,
                                              MaximumOrderQuantity = smat.MaximumOrderQuantity,
                                              UnitRefId = smat.UnitRefId,
                                              MaterialRefId = smat.MaterialRefId,
                                              LastQuoteDate = smat.LastQuoteDate,
                                              LastQuoteRefNo = smat.LastQuoteRefNo,
                                              Uom = un.Name,
                                              YieldPercentage = smat.YieldPercentage,
                                              SupplierMaterialAliasName = smat.SupplierMaterialAliasName
                                          }).ToList();

            foreach (var lst in matList)
            {
                var supplierMaterialDtos = rsSupplierMaterialDtos.Where(t => t.MaterialRefId == lst.Id).ToList();
                var anySplAliasName = supplierMaterialDtos.Count(t => t.SupplierMaterialAliasName.IsNullOrEmpty() == false && t.SupplierMaterialAliasName.Length > 0);
                if (anySplAliasName > 0)
                {
                    lst.SupplierMaterialEditDtos = supplierMaterialDtos;
                }
                var mc = rsMaterialStockCycle.Where(t => t.MaterialRefId == lst.Id /*&& t.LocationRefId == input.LocationRefId*/).ToList();
                //if (mc.Count == 0)
                //{
                //    mc = rsMaterialStockCycle.Where(t => t.MaterialRefId == lst.Id).ToList();
                //}

                if (mc.Count == 0)
                {
                    if (input.ExcludeMaterialWhichStockCycleNotAssigned != true)
                        outputDtos.Add(lst);
                }
                else
                {
                    foreach (var det in mc)
                    {
                        var applicalbeForThisLocation = false;
                        var sc = rsInventoryCycleTags.FirstOrDefault(t => t.Id == det.InventoryCycleTagRefId);
                        var linkLocations = rsStockCycleLinkLocations.Where(t => t.InventoryCycleTagRefId == det.InventoryCycleTagRefId).ToList();
                        if (linkLocations.Count() == 0)
                            applicalbeForThisLocation = true;
                        else
                        {
                            var locationExists = linkLocations.Exists(t => t.LocationRefId == input.LocationRefId);
                            if (locationExists)
                                applicalbeForThisLocation = true;
                            else
                            {
                                var allLgids = linkLocations.Where(t => t.LocationGroupRefId.HasValue).Select(a => a.LocationGroupRefId).ToList();
                                var allLoca =
                                   await _locationRepo.GetAll()
                                        .Where(a => a.LocationGroups.Where(lg => allLgids.Contains(lg.Id)).Any()).ToListAsync();
                                locationExists = allLoca.Exists(t => t.Id == input.LocationRefId);
                                if (locationExists)
                                    applicalbeForThisLocation = true;

                                var tempLoca = rsLocations.Where(a => a.LocationGroups.Where(lg => allLgids.Contains(lg.Id)).Any()).ToList();
                                locationExists = tempLoca.Exists(t => t.Id == input.LocationRefId);
                                if (locationExists)
                                    applicalbeForThisLocation = true;
                            }
                        }

                        if (applicalbeForThisLocation == false)
                            break;
                        //var locGroup = rsLocationGroup.FirstOrDefault(t=>t.locatio)
                        //if (location.LocationGroupId.HasValue)
                        //{
                        //    mc = rsMaterialStockCycle.Where(t => t.MaterialRefId == lst.Id && t.LocationGroupRefId == location.LocationGroupId).ToList();
                        //}
                        if (sc == null)
                        {
                            if (input.ExcludeMaterialWhichStockCycleNotAssigned)
                                break;
                            else
                            {
                                outputDtos.Add(lst);
                                break;
                            }
                        }
                        if (sc.InventoryCycleMode == (int)StockCycle.Daily)
                        {
                            outputDtos.Add(lst);
                            break;
                        }
                        else if (sc.InventoryCycleMode == (int)StockCycle.Weekly)
                        {
                            List<int> lstWeek = _xmlandjsonConvertorAppService.DeSerializeFromJSON<List<int>>(sc.InventoryModeSchedule);
                            int dow = (int)input.StockTakenDate.DayOfWeek;
                            var exists = lstWeek.Exists(item => item == dow);
                            if (exists == true)
                            {
                                outputDtos.Add(lst);
                                break;
                            }
                        }
                        else if (sc.InventoryCycleMode == (int)StockCycle.Monthly)
                        {
                            List<int> lstWeek = _xmlandjsonConvertorAppService.DeSerializeFromJSON<List<int>>(sc.InventoryModeSchedule);
                            int dom = input.StockTakenDate.Day;
                            var exists = lstWeek.Exists(item => item == dom);
                            if (exists == true)
                            {
                                outputDtos.Add(lst);
                                break;
                            }
                        }
                        else if (sc.InventoryCycleMode == (int)StockCycle.End_Of_Month)
                        {
                            if (lastDayOfMonth.Date == input.StockTakenDate.Date)
                            {
                                outputDtos.Add(lst);
                                break;
                            }
                        }
                    }
                }
            }

            if (input.MultipleUomAllowed)
            {
                arrMaterialRefIds = outputDtos.Select(t => t.Id).ToList();
                var allItems = await (from mulm in _materialUnitLinkRepo.GetAll().Where(t => arrMaterialRefIds.Contains(t.MaterialRefId))
                                      join un in _unitrepo.GetAll()
                                      on mulm.UnitRefId equals un.Id
                                      join material in _materialRepo.GetAll().Where(t => arrMaterialRefIds.Contains(t.Id))
                                      on mulm.MaterialRefId equals material.Id
                                      select new MaterialUnitsLinkViewDto()
                                      {
                                          Id = mulm.Id,
                                          UnitRefId = mulm.UnitRefId,
                                          MaterialRefId = mulm.MaterialRefId,
                                          MaterialName = material.MaterialName,
                                          UnitName = un.Name,
                                          CreationTime = mulm.CreationTime,
                                      }).ToListAsync();

                foreach (var lst in outputDtos)
                {
                    var lstUnits = allItems.Where(t => t.MaterialRefId == lst.Id && t.UnitRefId != lst.UnitRefId).ToList();
                    if (input.UomTypeFilters.Count() == 0)
                    {
                        lst.UnitsLinkViewDtos = lstUnits;
                    }
                    else
                    {
                        List<int> arrUnitIds = new List<int>();
                        foreach (var unitFilter in input.UomTypeFilters)
                        {
                            int unitType = 0; // = int.Parse(unitFilter.Value);
                            if (Int32.TryParse(unitFilter.Value, out unitType))
                            {
                                if (unitType == (int)UnitTypeEnum.Default)
                                {
                                    arrUnitIds.Add(lst.DefaultUnitId);  // Default Unit
                                }
                                else if (unitType == (int)UnitTypeEnum.Transaction)
                                {
                                    arrUnitIds.Add(lst.IssueUnitId);  // Transaction Unit
                                }
                                else if (unitType == (int)UnitTypeEnum.Transfer)
                                {
                                    arrUnitIds.Add(lst.TransferUnitId);  // Transfer Unit
                                }
                                else if (unitType == (int)UnitTypeEnum.Stock_Adjustment)
                                {
                                    arrUnitIds.Add(lst.StockAdjustmentUnitId);  // Stock Adjustment Unit
                                }
                            }
                        }
                        arrUnitIds = arrUnitIds.Distinct().ToList();
                        var unitListToBeLinked = lstUnits.Where(t => arrUnitIds.Contains(t.UnitRefId)).ToList();
                        if (unitListToBeLinked.Count == 1)
                        {
                            lst.UnitsLinkViewDtos = new List<MaterialUnitsLinkViewDto>();
                            lst.UnitRefId = unitListToBeLinked[0].UnitRefId;
                            lst.Uom = unitListToBeLinked[0].UnitName;
                        }
                        else if (unitListToBeLinked.Count > 1)
                        {
                            lst.UnitsLinkViewDtos = new List<MaterialUnitsLinkViewDto>();
                            lst.UnitRefId = unitListToBeLinked[0].UnitRefId;
                            lst.Uom = unitListToBeLinked[0].UnitName;
                            lst.UnitsLinkViewDtos = unitListToBeLinked.Where(t => t.UnitRefId != lst.UnitRefId).ToList();
                        }

                    }
                }
            }

            if (input.DownLoadMultipleUOMMultipleSupplierMaterialOnly == true)
            {
                outputDtos = outputDtos.Where(t => t.SupplierMaterialEditDtos.Count > 1).ToList();
            }

            GetScheduleMaterialList output = new GetScheduleMaterialList
            {
                MaterialStockList = outputDtos,
                LocationName = locationName,
                StockListRemarks = stockListRemarks
            };
            return output;
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetUnitTypeForCombobox()
        {
            var retList = new List<ComboboxItemDto>();

            string enumstring;
            var EnumValues = Enum.GetValues(typeof(UnitTypeEnum));
            foreach (int EnumValue in EnumValues)
            {
                enumstring = Enum.GetName(typeof(UnitTypeEnum), EnumValue);
                retList.Add(new ComboboxItemDto { Value = EnumValue.ToString(), DisplayText = enumstring });
            }

            return
                new ListResultDto<ComboboxItemDto>(
                    retList.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText)).ToList());
        }

        public async Task<FileDto> GetStockTakenReportExcel(LocationWithDate input)
        {
            var result = await GetStockTakenScheduledMaterialList(input);

            var fileName = result.LocationName + " " + (input.IsHighValueItemOnly == true ? L("HighValueItem") : "");
            FileDto fileExcel = _housereportExporter.StockTakenToExcelFile(result.MaterialStockList, fileName, input.StockTakenDate, input.MultipleUomAllowed);
            return fileExcel;
        }

        public async Task<AdjustmentDtosWithExcelFile> GetClosingStockTakenReportExcel(LocationWithDate input)
        {
            var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
            input.LocationInfo = location.MapTo<LocationListDto>();
            string locationName = input.LocationInfo.Name.PadRight(10, '-');
            input.PreparedUserId = AbpSession.UserId;
            var user = UserManager.Users.FirstOrDefault(t => t.Id == AbpSession.UserId.Value);
            string userName = "";
            if (user != null)
                userName = user.Name;
            input.PreparedUserName = userName;
            input.PrintedTime = DateTime.Now;
            var customReport = await FeatureChecker.GetValueAsync(AppFeatures.Custom);
            bool customCrgReport = false;
            if (customReport.ToUpper() == "TRUE")
                customCrgReport = true;
            input.CustomReportFlag = customCrgReport;

            var closingstocks = await _closingStockRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId && DbFunctions.TruncateTime(t.StockDate) == DbFunctions.TruncateTime(input.StockTakenDate)).ToListAsync();

            if (closingstocks == null || closingstocks.Count == 0)
            {
                throw new UserFriendlyException(L("NoMoreClosingStockTakenOnTheDate", input.StockTakenDate.ToString("dd-MMM-yyyy")));
            }

            int[] closingIds = closingstocks.Select(t => t.Id).ToArray();

            var closingStockDetailList = (from mat in _materialRepo.GetAll().Where(t => t.WipeOutStockOnClosingDay == false)
                                          join matGroup in _materialGroupCategoryRepo.GetAll()
                                          on mat.MaterialGroupCategoryRefId equals matGroup.Id
                                          join un in _unitrepo.GetAll()
                                          on mat.DefaultUnitId equals un.Id
                                          join cldetail in _closingStockDetailRepo.GetAll()
                                      .Where(t => closingIds.Contains(t.ClosingStockRefId))
                                      on mat.Id equals cldetail.MaterialRefId
                                          select new MaterialViewDto
                                          {
                                              Id = mat.Id,
                                              MaterialName = mat.MaterialName,
                                              MaterialGroupCategoryName = matGroup.MaterialGroupCategoryName,
                                              Uom = un.Name,
                                              OnHand = cldetail.OnHand,
                                              IsHighValueItem = mat.IsHighValueItem,
                                              ClosingStockDetailRefId = cldetail.Id
                                          }).ToList();

            if (input.IsHighValueItemOnly == true)
                closingStockDetailList = closingStockDetailList.Where(t => t.IsHighValueItem == true).ToList();

            List<int> arrMaterialRefIds = closingStockDetailList.Select(t => t.Id).ToList();
            GetStockSummaryDtoOutput outputClosingStock =
                await GetStockSummary(new GetHouseReportInput
                {
                    StartDate = input.StockTakenDate,
                    EndDate = input.StockTakenDate,
                    LocationRefId = input.LocationRefId,
                    ExcludeRateView = true,
                    MaterialRefIds = arrMaterialRefIds
                });

            List<MaterialLedgerDto> StockSummary = outputClosingStock.StockSummary;

            var closingStockConsolidated = (from det in closingStockDetailList
                                            join clstk in StockSummary
                                            on det.Id equals clstk.MaterialRefId
                                            group det by new
                                            {
                                                det.Id,
                                                det.MaterialName,
                                                det.MaterialGroupCategoryName,
                                                det.Uom,
                                                det.IsHighValueItem,
                                                clstk.ClBalance
                                            } into g
                                            select new MaterialViewDto
                                            {
                                                Id = g.Key.Id,
                                                MaterialName = g.Key.MaterialName,
                                                MaterialGroupCategoryName = g.Key.MaterialGroupCategoryName,
                                                Uom = g.Key.Uom,
                                                IsHighValueItem = g.Key.IsHighValueItem,
                                                OnHand = g.Sum(t => t.OnHand),
                                                ClBalance = g.Key.ClBalance
                                            }).ToList();

            var rsMaterialList = await _materialRepo.GetAll().Where(t => arrMaterialRefIds.Contains(t.Id)).ToListAsync();
            var rsMaterialCategory = await _materialGroupCategoryRepo.GetAllListAsync();
            var rsUnit = await _unitrepo.GetAllListAsync();
            var dtos = new List<MaterialAdjustmentStockDto>();

            List<ClosingStockDetailViewDto> closingCustomReportDetails = new List<ClosingStockDetailViewDto>();
            List<ClosingStockUnitWiseDetail> rsClosingStockUnitWiseDetails = new List<ClosingStockUnitWiseDetail>();
            if (input.CustomReportFlag)
            {
                List<int> arrClosingDetailRefId = closingStockDetailList.Select(t => t.ClosingStockDetailRefId).ToList();
                rsClosingStockUnitWiseDetails = await _closingStockUnitWiseDetailRepo.GetAllListAsync(t => arrClosingDetailRefId.Contains(t.ClosingStockDetailRefId));
            }

            foreach (var lst in closingStockConsolidated)
            {
                decimal diffStockvalue = Math.Round(lst.OnHand - lst.ClBalance, 14, MidpointRounding.AwayFromZero);

                lst.Difference = diffStockvalue;

                string diffStatus;
                if (diffStockvalue < 0)
                    diffStatus = L("Shortage");
                else if (diffStockvalue == 0)
                    diffStatus = L("Equal");
                else
                    diffStatus = L("Excess");

                lst.Status = diffStatus;

                int materialRefId = lst.Id;
                var mat = rsMaterialList.FirstOrDefault(t => t.Id == materialRefId);

                var unit = rsUnit.FirstOrDefault(t => t.Id == mat.DefaultUnitId);
                if (unit == null)
                {
                    throw new UserFriendlyException(L("Unit") + " " + L("Error") + " " + mat.MaterialName);
                }

                var stk = new MaterialAdjustmentStockDto
                {
                    MaterialRefId = materialRefId,
                    MaterialName = mat.MaterialName,
                    ClBalance = lst.ClBalance,
                    CurrentStock = lst.OnHand,
                    DifferenceQuantity = Math.Abs(diffStockvalue),
                    AdjustmentStatus = diffStatus,
                    DefaultUnitId = mat.DefaultUnitId,
                    DefaultUnitName = unit.Name,
                    UnitRefId = mat.DefaultUnitId,
                    UnitRefName = unit.Name
                };

                dtos.Add(stk);

                if (input.CustomReportFlag)
                {
                    var matCategory = rsMaterialCategory.FirstOrDefault(t => t.Id == mat.MaterialGroupCategoryRefId);
                    ClosingStockDetailViewDto newDto = new ClosingStockDetailViewDto
                    {
                        MaterialGroupCategoryRefId = mat.MaterialGroupCategoryRefId,
                        MaterialGroupCategoryRefName = matCategory.MaterialGroupCategoryName,
                        MaterialPetName = mat.MaterialPetName,
                        MaterialRefName = mat.MaterialName,
                    };
                    var matwithclosingRefids = closingStockDetailList.Where(t => t.Id == mat.Id);
                    var arrClRefIds = matwithclosingRefids.Select(t => t.ClosingStockDetailRefId).ToList();
                    var unitWiseDetails = rsClosingStockUnitWiseDetails.Where(t => arrClRefIds.Contains(t.ClosingStockDetailRefId));
                    List<int> arrUnitRefIds = unitWiseDetails.Select(t => t.UnitRefId).ToList();
                    foreach (var ui in arrUnitRefIds)
                    {
                        decimal unitQty = unitWiseDetails.Where(t => ui == t.UnitRefId).Sum(t => t.OnHand);
                        if (mat.DefaultUnitId == ui)
                            newDto.QuantityInPurchaseUnit = unitQty;
                        else if (mat.StockAdjustmentUnitId == ui)
                            newDto.QuantityInStkAdjustedInventoryUnit = unitQty;
                        else if (mat.IssueUnitId == ui)
                            newDto.QuantityInIssueUnit = unitQty;
                        else
                        {
                            unit = rsUnit.FirstOrDefault(t => t.Id == ui);
                            throw new UserFriendlyException(mat.MaterialName + " Quantity :" + unitQty + " " + unit.Name + " Not exist in Material Default/Issue/Transfer/Stock Adj Unit");
                        }
                    }
                    closingCustomReportDetails.Add(newDto);
                }
            }

            FileDto fileExcel = _housereportExporter.ClosingStockTakenToExcelFile(closingStockDetailList,
                closingStockConsolidated, locationName, input.StockTakenDate, input, closingCustomReportDetails);

            AdjustmentDtosWithExcelFile output = new AdjustmentDtosWithExcelFile();
            output.ExcelFile = fileExcel;
            output.ClosingAdjustmentDtos = dtos;

            foreach (var det in closingstocks)
            {
                if (det.AdjustmentRefId != null)
                {
                    output.AdjustmentRefId = det.AdjustmentRefId;
                    var rsAdj = await _adjustmentRepo.FirstOrDefaultAsync(t => t.Id == det.AdjustmentRefId);
                    output.ErrorMessage = L("ClosingStockAlreadyAdjusted", rsAdj.AdjustmentDate.ToString("dd-MMM-yy"), rsAdj.Id);
                }
            }

            if (output.AdjustmentRefId == null)
            {
                var rsFutureAdjustedAlready = await _closingStockRepo.FirstOrDefaultAsync(t => t.LocationRefId == input.LocationRefId && t.StockDate > input.StockTakenDate && t.AdjustmentRefId != null);
                if (rsFutureAdjustedAlready != null)
                {
                    output.ErrorMessage = L("FutureClosingStockAlreadyAdjustedErr", rsFutureAdjustedAlready.StockDate.ToString("dd-MMM-yy"), rsFutureAdjustedAlready.AdjustmentRefId, input.StockTakenDate.ToString("dd-MMM-yy"));
                }
            }

            return output;
        }

        public async Task<FileDto> GetPurchaseSummary(InputInvoiceAnalysis input)
        {
            return await _housereportExporter.ExportToFilePurchaseSummary(input, _invoiceAppService);
        }

        public async Task<FileDto> GetPurchaseConsolidatedSummary(InputInvoiceAnalysis input)
        {
            return await _housereportExporter.ExportToFilePurchaseConsolidatedSummary(input, _invoiceAppService);
        }

        public async Task<List<MaterialAdjustmentStockDto>> ReadStockFromExcel(string filename)
        {
            //var allList = input.StockSummary;
            //var allListDtos = allList.MapTo<List<HouseReportListDto>>();
            List<MaterialAdjustmentStockDto> dtos = _housereportExporter.ReadFromExcelFile(filename);

            //   _housereportExporter.ReadFromExcelFile(fileExcel);

            return dtos;
        }

        public async Task<CostOfSalesReport> GetCostOfSalesReport(GetCostReportInput input)
        {
            List<CostingReportOutPutDto> output = new List<CostingReportOutPutDto>();

            CostingReportOutPutDto allLocDto = new CostingReportOutPutDto();

            allLocDto = await GetCategoryCostingReport(new GetCostReportInput
            {
                StartDate = input.StartDate,
                EndDate = input.EndDate,
                Locations = input.Locations,
                CompIncludeFlag = input.CompIncludeFlag,
                GiftIncludeFlag = input.GiftIncludeFlag,
            });

            output.Add(allLocDto);

            if (input.Locations.Count > 1)
            {
                foreach (var loc in input.Locations)
                {
                    CostingReportOutPutDto newDto = new CostingReportOutPutDto();
                    List<LocationListDto> indloclist = new List<LocationListDto>();
                    indloclist.Add(loc);

                    newDto = await GetCategoryCostingReport(new GetCostReportInput
                    {
                        StartDate = input.StartDate,
                        EndDate = input.EndDate,
                        Locations = indloclist,
                    });

                    output.Add(newDto);
                }
            }

            return new CostOfSalesReport { CostOfSales = output };
        }

        public async Task<CostingReportOutPutDto> GetCategoryCostingReport(GetCostReportInput input)
        {
            int[] matListtoVerify = { 81/*, 82, 130, 132*/ };

            decimal dectotalFoodCostValue = 0;
            decimal totalSalesWithoutTax;
            string remarks = "";

            if (input.Locations == null)
            {
                throw new UserFriendlyException("LocationErr");
            }

            List<LocationListDto> locinput = input.Locations;

            DateTime fromDt = input.StartDate.Date;
            DateTime toDt = input.EndDate.Date;

            #region GetSales

            var allSales = await _connectReportAppService.GetAllItemSales(
                new GetItemInput
                {
                    StartDate = fromDt,
                    EndDate = toDt,
                    Locations = locinput.MapTo<List<SimpleLocationDto>>(),
                    Sales = true,
                    Comp = input.CompIncludeFlag,
                    Void = true,
                    Gift = input.GiftIncludeFlag
                });

            //int[] posRefIds = { *, 46, 48*/ };
            //allSales.Sales = allSales.Sales.Where(t => posRefIds.Contains(t.MenuItemPortionId)).ToList();
            totalSalesWithoutTax = allSales.Sales.Sum(t => t.Total);

            #endregion GetSales

            #region Get_StandardCost

            List<MaterialUsageDetailDto> StandardMaterialUsageDetail = new List<MaterialUsageDetailDto>();

            List<MaterialMenuMappingDto> mapwithMenuList = new List<MaterialMenuMappingDto>();

            List<MenuListDto> itemSales = new List<MenuListDto>();
            itemSales.AddRange(allSales.Sales);
            itemSales.AddRange(allSales.Void);
            if (input.GiftIncludeFlag)
                itemSales.AddRange(allSales.Gift);

            if (input.CompIncludeFlag)
                itemSales.AddRange(allSales.Comp);

            var rsMaterial = await _materialRepo.GetAllListAsync();
            var rsUnits = await _unitrepo.GetAllListAsync();

            var rsMenuMappingMaterials = await _materialmenumappingRepo.GetAllListAsync();

            var rsMenumappingDepartment = await _menumappingdepartmentRepo.GetAll().ToListAsync();

            var rsDeptList = await _departmentRepo.GetAll().ToListAsync();

            #region Menu Item Wastage Material List

            List<MenuListDto> menuItemWastageList = new List<MenuListDto>();
            var menuWastageString = L("MenuWastage");
            List<int> arrLocationRefIds = locinput.Select(t => t.Id).ToList();
            menuItemWastageList = await (from mas in _menuitemwastageRepo.GetAll()
                            .Where(t => DbFunctions.TruncateTime(t.SalesDate) >= DbFunctions.TruncateTime(input.StartDate) && DbFunctions.TruncateTime(t.SalesDate) <= DbFunctions.TruncateTime(input.EndDate) && arrLocationRefIds.Contains(t.LocationRefId))
                                         join det in _menuitemwastageDetailRepo.GetAll() on mas.Id equals det.MenuItemWastageRefId
                                         join loc in _locationRepo.GetAll() on mas.LocationRefId equals loc.Id
                                         join menuportion in _menuitemportionRepo.GetAll()
                                             on det.PosMenuPortionRefId equals menuportion.Id
                                         join menu in _menuitemRepo.GetAll().Where(a => a.ProductType == 1)
                                            on menuportion.MenuItemId equals menu.Id
                                         select new MenuListDto
                                         {
                                             MenuItemId = menu.Id,
                                             AliasCode = menu.AliasCode,
                                             MenuItemName = menu.Name,
                                             MenuItemPortionId = menuportion.Id,
                                             MenuItemPortionName = string.Concat(menu.Name, " - ", menuportion.Name),
                                             Quantity = det.WastageQty
                                             //LocationRefId = mas.LocationRefId,
                                             //LocationRefName = loc.Name,
                                             //MenuItemWastageRefId = mas.Id,
                                             //PosMenuPortionRefId = det.PosMenuPortionRefId,
                                             //PosMenuPortionCode = menu.AliasCode,
                                             //PosMenuPortionRefName = string.Concat(menu.Name, " - ", menuportion.Name),
                                             //WastageQty = det.WastageQty,
                                             //WastageRemarks = det.WastageRemarks,
                                             //UserId = mas.CreatorUserId.Value,
                                             //MenuWastageMode = menuWastageString,
                                             //SalesDate = mas.SalesDate,
                                             //CostPerUnit = det.Cost
                                         }
                                ).ToListAsync();

            #endregion Menu Item Wastage Material List

            itemSales = itemSales.OrderBy(t => t.MenuItemPortionId).ToList();
            //int[] posIds =
            //itemSales= itemSales.Where(t=>t.MenuItemPortionId)
            string materialmappingRemarks = "";

            #region Get_StandardMaterialUsage_Based_On_MaterialMapping

            foreach (var item in itemSales)
            {
                #region Verify_Sales_Department

                var menuMapping = rsMenuMappingMaterials.Where(t => t.PosMenuPortionRefId == item.MenuItemPortionId & t.LocationRefId == input.LocationRefId).ToList();
                if (menuMapping == null || menuMapping.Count == 0)
                {
                    menuMapping = rsMenuMappingMaterials.Where(t => t.PosMenuPortionRefId == item.MenuItemPortionId & t.LocationRefId == null).ToList();
                }
                var department = rsDeptList.FirstOrDefault(t => t.Name.ToUpper().Equals(item.DepartmentName.ToUpper()));

                if (department == null)
                {
                    throw new UserFriendlyException(L("Department") + " " + L("NotExist"), item.DepartmentName);
                }

                int deptId = department.Id;

                int[] menuMappingRefIds = menuMapping.Select(t => t.Id).ToArray();

                var deptList = rsMenumappingDepartment.Where(t => menuMappingRefIds.Contains(t.MenuMappingRefId)).ToList();

                bool AllDepartmentFlag;
                if (deptList.Count() == 0)
                {
                    AllDepartmentFlag = true;
                }
                else
                {
                    AllDepartmentFlag = false;
                }

                #endregion Verify_Sales_Department

                if (menuMapping.Count == 0)
                {
                    materialmappingRemarks = materialmappingRemarks + item.MenuItemName + "<br>";
                }
                else
                {
                    foreach (var mmmaterial in menuMapping)
                    {
                        if (AllDepartmentFlag == false)
                        {
                            var deptLinkedMM = deptList.Where(t => t.MenuMappingRefId == mmmaterial.Id);
                            if (deptLinkedMM.Count() > 0)
                            {
                                deptLinkedMM = deptLinkedMM.Where(t => t.DepartmentRefId == deptId);
                                if (deptLinkedMM.Count() == 0)
                                {
                                    continue;
                                }
                            }
                        }

                        MaterialMenuMappingDto newmm = new MaterialMenuMappingDto();

                        var posName = itemSales.FirstOrDefault(t => t.MenuItemPortionId == mmmaterial.PosMenuPortionRefId);

                        if (matListtoVerify.Contains(mmmaterial.MaterialRefId))
                        {
                            int adf = 1;
                            adf++;
                        }

                        newmm.DepartmentName = item.DepartmentName;
                        newmm.PosMenuPortionRefId = mmmaterial.PosMenuPortionRefId;
                        newmm.PosRefName = posName.MenuItemName + " - " + posName.MenuItemPortionName;
                        newmm.MenuQuantitySold = mmmaterial.MenuQuantitySold;
                        newmm.MaterialRefId = mmmaterial.MaterialRefId;
                        newmm.PortionQty = Math.Round(mmmaterial.PortionQty / mmmaterial.MenuQuantitySold * item.Quantity, 14, MidpointRounding.AwayFromZero);
                        newmm.PortionUnitId = mmmaterial.PortionUnitId;
                        newmm.TotalSaleQuantity = item.Quantity;
                        newmm.AutoSalesDeduction = mmmaterial.AutoSalesDeduction;

                        var mat = rsMaterial.FirstOrDefault(t => t.Id == mmmaterial.MaterialRefId);
                        if (mat == null)
                        {
                            //throw new UserFriendlyException(L("Material") + " " + L("NotExist") + " " + L("Id") + " " + newmm.MaterialRefId);
                            continue;
                        }

                        var unit = rsUnits.FirstOrDefault(t => t.Id == mmmaterial.PortionUnitId);

                        newmm.UnitRefName = unit.Name;

                        var defaultUnit = rsUnits.FirstOrDefault(t => t.Id == mat.DefaultUnitId);
                        newmm.DefaultUnitId = mat.DefaultUnitId;
                        newmm.DefaultUnitName = defaultUnit.Name;
                        newmm.MaterialRefName = mat.MaterialName;
                        mapwithMenuList.Add(newmm);

                        #region DetailForStandard

                        MaterialUsageDetailDto mud = new MaterialUsageDetailDto();
                        mud.DepartmentName = item.DepartmentName;
                        mud.PosMenuPortionRefId = newmm.PosMenuPortionRefId;
                        mud.PosRefName = newmm.PosRefName;
                        mud.MenuQuantitySold = item.Quantity;
                        mud.PortionPerQtySold = mmmaterial.PortionQty;
                        mud.MaterialRefId = newmm.MaterialRefId;
                        mud.MaterialRefName = newmm.MaterialRefName;
                        mud.PortionQty = newmm.PortionQty;
                        mud.UnitRefId = newmm.PortionUnitId;
                        mud.UnitRefName = newmm.UnitRefName;
                        mud.DefaultUnitId = newmm.DefaultUnitId;
                        mud.DefaultUnitName = newmm.DefaultUnitName;
                        mud.MaterialTypeId = mat.MaterialTypeId;
                        mud.MaterialTypeName = mat.MaterialTypeId == 0 ? L("RAW") : L("SEMI");
                        mud.RecipeRefId = 0;
                        mud.MaterialGroupCategoryRefId = mat.MaterialGroupCategoryRefId;

                        StandardMaterialUsageDetail.Add(mud);

                        #endregion DetailForStandard

                        //mud.UnitRefId =
                    }
                }
            }

            if (materialmappingRemarks.IsNullOrEmpty() == false)
            {
                materialmappingRemarks = L("MaterialMenuMappingErr") + " <BR> " + materialmappingRemarks;
            }

            #endregion Get_StandardMaterialUsage_Based_On_MaterialMapping

            #region Consolidate_To_Default_Unit

            var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();

            var mapwithMenuListGroupWithUnitConversion = (from salemenu in mapwithMenuList
                                                          join uc in rsUc
                                                          on salemenu.PortionUnitId equals uc.BaseUnitId
                                                          where uc.RefUnitId == salemenu.DefaultUnitId
                                                          group salemenu by new
                                                          {
                                                              salemenu.DepartmentName,
                                                              salemenu.PosMenuPortionRefId,
                                                              salemenu.PosRefName,
                                                              salemenu.MaterialRefId,
                                                              salemenu.MaterialRefName,
                                                              salemenu.DefaultUnitId,
                                                              salemenu.PortionUnitId,
                                                              salemenu.UnitRefName,
                                                              salemenu.DefaultUnitName,
                                                              uc.Conversion
                                                          } into g
                                                          select new MaterialMenuMappingDto
                                                          {
                                                              DepartmentName = g.Key.DepartmentName,
                                                              PosMenuPortionRefId = g.Key.PosMenuPortionRefId,
                                                              PosRefName = g.Key.PosRefName,
                                                              MaterialRefId = g.Key.MaterialRefId,
                                                              MaterialRefName = g.Key.MaterialRefName,
                                                              PortionUnitId = g.Key.PortionUnitId,
                                                              UnitRefName = g.Key.UnitRefName,
                                                              DefaultUnitId = g.Key.DefaultUnitId,
                                                              DefaultUnitName = g.Key.DefaultUnitName,
                                                              PortionQty = g.Sum(t => Math.Round(t.PortionQty * g.Key.Conversion, 14)),
                                                          }).ToList().OrderBy(t => t.MaterialRefId);

            var mapWithMenuListSumGroup = (from salemenu in mapwithMenuListGroupWithUnitConversion
                                           group salemenu by new
                                           {
                                               salemenu.DepartmentName,
                                               salemenu.MaterialRefId,
                                               salemenu.MaterialRefName,
                                               salemenu.PosMenuPortionRefId,
                                               salemenu.PortionUnitId,
                                               salemenu.PosRefName
                                           } into g
                                           select new MaterialMenuMappingDto
                                           {
                                               DepartmentName = g.Key.DepartmentName,
                                               MaterialRefId = g.Key.MaterialRefId,
                                               MaterialRefName = g.Key.MaterialRefName,
                                               PosMenuPortionRefId = g.Key.PosMenuPortionRefId,
                                               PosRefName = g.Key.PosRefName,
                                               PortionQty = g.Sum(t => t.PortionQty),
                                               PortionUnitId = g.Key.PortionUnitId
                                           }).ToList();

            #endregion Consolidate_To_Default_Unit

            #region Get_Material_Usage_Based_On_Semi

            List<MaterialWithQty> outputUsageMaterial = new List<MaterialWithQty>();

            var nonrawList = (from a in mapWithMenuListSumGroup
                              join mat in rsMaterial.Where(t => t.MaterialTypeId != (int)MaterialType.RAW)
                              on a.MaterialRefId equals mat.Id
                              select new MaterialWithQty
                              {
                                  DepartmentName = a.DepartmentName,
                                  PosMenuPortionRefId = a.PosMenuPortionRefId,
                                  PosRefName = a.PosRefName,
                                  MaterialRefId = a.MaterialRefId,
                                  MaterialRefName = mat.MaterialName,
                                  UsedQty = a.PortionQty,
                                  UnitRefId = a.PortionUnitId,
                                  DefaultUnitId = mat.DefaultUnitId
                              }).ToList();

            var rawlist = (from a in mapWithMenuListSumGroup
                           join mat in rsMaterial.Where(t => t.MaterialTypeId == (int)MaterialType.RAW)
                               on a.MaterialRefId equals mat.Id
                           select new MaterialWithQty
                           {
                               DepartmentName = a.DepartmentName,
                               PosMenuPortionRefId = a.PosMenuPortionRefId,
                               PosRefName = a.PosRefName,
                               MaterialRefId = a.MaterialRefId,
                               MaterialRefName = mat.MaterialName,
                               UsedQty = a.PortionQty,
                               DefaultUnitId = mat.DefaultUnitId,
                           }).ToList();

            outputUsageMaterial.AddRange(rawlist);

            foreach (var recipe in nonrawList)
            {
                GetRawMaterailBasedOnRecipeInput inp = new GetRawMaterailBasedOnRecipeInput();
                inp.RecipeRefId = recipe.MaterialRefId;
                inp.OutputQty = recipe.UsedQty;
                var recipeMat = rsMaterial.FirstOrDefault(t => t.Id == recipe.MaterialRefId);
                var getMatDetailforRecipe = await GetRawMaterialRequiredForRecipe(inp);

                foreach (var m in getMatDetailforRecipe)
                {
                    MaterialUsageDetailDto mud = new MaterialUsageDetailDto();
                    mud.DepartmentName = recipe.DepartmentName;
                    mud.PosMenuPortionRefId = recipe.PosMenuPortionRefId;
                    mud.PosRefName = recipe.PosRefName;
                    mud.MenuQuantitySold = itemSales.Where(t => t.MenuItemPortionId == recipe.PosMenuPortionRefId && t.DepartmentName.ToUpper().Equals(recipe.DepartmentName)).Sum(t => t.Quantity);
                    mud.MaterialRefId = m.MaterialRefId;
                    mud.MaterialRefName = m.MaterialRefName;
                    mud.PortionQty = m.MaterialUsedQty;
                    mud.UnitRefId = m.DefaultUnitId;
                    mud.RecipeRefId = recipe.MaterialRefId;
                    mud.RecipeRefName = recipeMat.MaterialName;
                    mud.RecipeUsedQty = recipe.UsedQty;
                    mud.RecipeUsedUnitRefId = recipe.UnitRefId;
                    mud.RecipeUsedUnitRefName = rsUnits.FirstOrDefault(t => t.Id == recipe.UnitRefId).Name;
                    mud.RecipePortionPerQtySold = Math.Round(m.MaterialUsedQty / mud.MenuQuantitySold, 14, MidpointRounding.AwayFromZero);
                    //mud.RecipeUsedUnitRefId = m.DefaultUnitId;
                    mud.RecipeUsedUnitRefName = rsUnits.FirstOrDefault(t => t.Id == m.DefaultUnitId).Name;

                    var unit = rsUnits.FirstOrDefault(t => t.Id == mud.UnitRefId);
                    if (unit != null)
                        mud.UnitRefName = unit.Name;

                    mud.DefaultUnitId = m.DefaultUnitId;

                    var defaultUnit = rsUnits.FirstOrDefault(t => t.Id == m.DefaultUnitId);
                    mud.DefaultUnitName = defaultUnit.Name;

                    var mat = rsMaterial.FirstOrDefault(t => t.Id == m.MaterialRefId);

                    mud.MaterialTypeId = mat.MaterialTypeId;
                    mud.MaterialTypeName = mat.MaterialTypeId == 0 ? L("RAW") : L("SEMI");
                    mud.MaterialGroupCategoryRefId = mat.MaterialGroupCategoryRefId;
                    StandardMaterialUsageDetail.Add(mud);

                    if (matListtoVerify.Contains(m.MaterialRefId))
                    {
                        int adf = 1;
                        adf++;
                    }
                    outputUsageMaterial.Add(new MaterialWithQty
                    {
                        MaterialRefId = m.MaterialRefId,
                        UsedQty = m.MaterialUsedQty,
                    });
                }
            }

            #endregion Get_Material_Usage_Based_On_Semi

            var materiallistforusage = (from mat in outputUsageMaterial
                                        group mat by mat.MaterialRefId into g
                                        select new MaterialWithQty
                                        {
                                            MaterialRefId = g.Key,
                                            UsedQty = g.Sum(t => t.UsedQty)
                                        }).ToList();

            List<MaterialRateViewListDto> materialRate;

            if (input.MaterialRateView == null || input.MaterialRateView.Count == 0)
            {
                //List<int> materialFilter = materiallistforusage.Select(t => t.MaterialRefId).ToList();
                //if (materialFilter.Count > 0)
                {
                    var matView = await GetMaterialRateView(new GetHouseReportMaterialRateInput
                    {
                        StartDate = input.StartDate,
                        EndDate = input.EndDate,
                        Locations = locinput,
                        //MaterialRefIds = materialFilter,
                        FunctionCalledBy = "Get Category Costing Summary"
                    });
                    materialRate = matView.MaterialRateView;
                }
                //else
                //{
                //    materialRate = new List<MaterialRateViewListDto>();
                //}
            }
            else
            {
                materialRate = input.MaterialRateView;
            }

            List<MaterialUsageWithPriceAndDateRange> detailStandardOutput = new List<MaterialUsageWithPriceAndDateRange>();

            List<string> priceremarks = new List<string>();

            foreach (var usage in materiallistforusage)
            {
                var matrate = materialRate.FirstOrDefault(t => t.MaterialRefId == usage.MaterialRefId);

                if (matListtoVerify.Contains(usage.MaterialRefId))
                {
                    int adf = 1;
                    adf++;
                }

                decimal materialcostvalue;

                if (matrate == null)
                {
                    throw new UserFriendlyException("NoRateAvaiableIssueErr", usage.MaterialRefName);
                }

                if (matrate.AvgRate == 0)
                {
                    priceremarks.Add(matrate.MaterialName);
                }

                var mat = rsMaterial.FirstOrDefault(t => t.Id == usage.MaterialRefId);

                MaterialUsageWithPriceAndDateRange matwithrate = new MaterialUsageWithPriceAndDateRange();
                matwithrate.MaterialRefId = usage.MaterialRefId;
                matwithrate.MaterialRefName = matrate.MaterialName;
                matwithrate.UsageQty = usage.UsedQty;
                matwithrate.Uom = matrate.Uom;
                matwithrate.AvgRate = matrate.AvgRate;
                materialcostvalue = Math.Round(matrate.AvgRate * usage.UsedQty, roundDecimals, MidpointRounding.AwayFromZero);
                matwithrate.NetAmount = materialcostvalue;
                matwithrate.InvoiceDateRange = matrate.InvoiceDateRange;
                matwithrate.CostPercentage = Math.Round(materialcostvalue / totalSalesWithoutTax * 100, roundDecimals, MidpointRounding.AwayFromZero);
                matwithrate.MenuMappingList = mapwithMenuList.Where(t => t.MaterialRefId == usage.MaterialRefId).ToList();
                matwithrate.MaterialGroupCategoryRefId = mat.MaterialGroupCategoryRefId;
                dectotalFoodCostValue = dectotalFoodCostValue + materialcostvalue;

                detailStandardOutput.Add(matwithrate);
            }

            var output = new CostingReportOutPutDto();

            output.Locations = input.Locations;
            output.FromDate = input.StartDate;
            output.ToDate = input.EndDate;
            output.PreparedUserId = AbpSession.UserId;
            var user = UserManager.Users.FirstOrDefault(t => t.Id == AbpSession.UserId.Value);
            string userName = "";
            if (user != null)
                userName = user.Name;
            output.PreparedUserName = userName;
            output.PrintedTime = DateTime.Now;

            output.SalesValueWithoutTax = totalSalesWithoutTax;
            output.FoodCostValue = dectotalFoodCostValue;
            if (dectotalFoodCostValue != 0)
                output.FoodCostPercentage = Math.Round(dectotalFoodCostValue / output.SalesValueWithoutTax * 100, roundDecimals, MidpointRounding.AwayFromZero);
            else
                output.FoodCostPercentage = 0;

            output.MaterialWiseValueAndPercentage = detailStandardOutput.OrderBy(t => t.MaterialRefName).ToList();

            #endregion Get_StandardCost

            #region Get StandardCost Category Wise

            var categoryWiseStandardCost = (from catStd in detailStandardOutput
                                            group catStd by catStd.MaterialGroupCategoryRefId into g
                                            select new MaterialGroupCategoryCosting
                                            {
                                                MaterialGroupCategoryRefId = g.Key,
                                                NetAmount = g.Sum(t => t.NetAmount),
                                            }).ToList();
            var arrCategoryRefIds = categoryWiseStandardCost.Select(t => t.MaterialGroupCategoryRefId).ToList();
            var rsCategory = await _materialGroupCategoryRepo.GetAllListAsync(t => arrCategoryRefIds.Contains(t.Id));

            foreach (var categoryStandard in categoryWiseStandardCost)
            {
                var category = rsCategory.FirstOrDefault(t => t.Id == categoryStandard.MaterialGroupCategoryRefId);
                categoryStandard.MaterialGroupCategoryRefName = category.MaterialGroupCategoryName;
                if (categoryStandard.NetAmount > 0 && output.SalesValueWithoutTax > 0)
                {
                    categoryStandard.CostPercentage = Math.Round(categoryStandard.NetAmount / output.SalesValueWithoutTax * 100, roundDecimals, MidpointRounding.AwayFromZero);
                }
            }
            output.StandardCategoryWiseCost = categoryWiseStandardCost;

            #endregion Get StandardCost Category Wise

            dectotalFoodCostValue = 0;

            //  Taken Issue from Actual Usage

            //  Taken Actual From Stock Summary Report

            #region Get_Actual_Cost

            List<MaterialLedgerDto> StockSummary = new List<MaterialLedgerDto>();
            foreach (var loc in input.Locations)
            {
                var retstockSummary = await GetStockSummary(new GetHouseReportInput
                {
                    StartDate = input.StartDate,
                    EndDate = input.EndDate,
                    LocationRefId = loc.Id,
                    MaterialRateView = materialRate
                });
                StockSummary.AddRange(retstockSummary.StockSummary);
            }

            List<MaterialUsageWithPriceAndDateRange> detailActualOutput = new List<MaterialUsageWithPriceAndDateRange>();

            decimal openingCostValue = 0;
            decimal closingCostValue = 0;
            decimal overAllIssuedValue = 0;
            decimal overAllReceivedValue = 0;

            List<MaterialLedgerDto> ActualMaterialUsageDetail = new List<MaterialLedgerDto>();

            decimal decActualTotalFoodCostValue = 0;
            foreach (var matledger in StockSummary)
            {
                decimal ConsumptionQty = matledger.Issued + matledger.Sales - matledger.Return + matledger.Damaged + matledger.Shortage;
                //decimal ConsumptionQty = matledger.Issued + matledger.Sales - matledger.Return;
                decimal TotalIssued = matledger.Issued + matledger.Sales + matledger.TransferOut + matledger.Damaged + matledger.Shortage + matledger.SupplierReturn;
                decimal TotalReceived = matledger.Received + matledger.TransferIn + matledger.ExcessReceived + matledger.Return;

                var matrate = materialRate.FirstOrDefault(t => t.MaterialRefId == matledger.MaterialRefId);

                if (matListtoVerify.Contains(matledger.MaterialRefId))
                {
                    int adf = 1;
                }

                decimal materialcostvalue;

                if (matrate == null)
                {
                    throw new UserFriendlyException("NoRateAvaiableIssueErr", matledger.MaterialName);
                }

                if (matrate.AvgRate == 0)
                {
                    priceremarks.Add(matrate.MaterialName);
                }

                MaterialUsageWithPriceAndDateRange matwithrate = new MaterialUsageWithPriceAndDateRange();
                matwithrate.MaterialRefId = matledger.MaterialRefId;
                matwithrate.MaterialRefName = matrate.MaterialName;
                matwithrate.UsageQty = ConsumptionQty;
                matwithrate.Uom = matrate.Uom;
                matwithrate.AvgRate = matrate.AvgRate;
                materialcostvalue = Math.Round(matrate.AvgRate * ConsumptionQty, roundDecimals, MidpointRounding.AwayFromZero);
                matwithrate.NetAmount = materialcostvalue;
                matwithrate.InvoiceDateRange = matrate.InvoiceDateRange;
                if (totalSalesWithoutTax == 0)
                {
                    matwithrate.CostPercentage = 0;
                }
                else
                {
                    matwithrate.CostPercentage = Math.Round(materialcostvalue / totalSalesWithoutTax * 100, roundDecimals, MidpointRounding.AwayFromZero);
                }

                decActualTotalFoodCostValue = decActualTotalFoodCostValue + materialcostvalue;

                bool alreadyExist = detailActualOutput.Any(t => t.MaterialRefId == matledger.MaterialRefId);
                if (alreadyExist == false)
                {
                    detailActualOutput.Add(matwithrate);
                }
                else
                {
                    var idx = detailActualOutput.FindIndex(t => t.MaterialRefId == matledger.MaterialRefId);

                    detailActualOutput[idx].UsageQty = detailActualOutput[idx].UsageQty + ConsumptionQty;
                    materialcostvalue = Math.Round(matrate.AvgRate * ConsumptionQty, roundDecimals, MidpointRounding.AwayFromZero);
                    detailActualOutput[idx].NetAmount = detailActualOutput[idx].NetAmount + materialcostvalue;
                    if (totalSalesWithoutTax == 0)
                    {
                        detailActualOutput[idx].CostPercentage = 0;
                    }
                    else
                    {
                        detailActualOutput[idx].CostPercentage = Math.Round(materialcostvalue / totalSalesWithoutTax * 100, roundDecimals, MidpointRounding.AwayFromZero);
                    }
                }

                var actDtoAlreadyExist = ActualMaterialUsageDetail.Any(t => t.MaterialRefId == matledger.MaterialRefId);
                if (actDtoAlreadyExist == false)
                {
                    MaterialLedgerDto actDto = new MaterialLedgerDto();
                    actDto.LedgerFromDate = input.StartDate;
                    actDto.LedgerToDate = input.EndDate;
                    actDto.MaterialRefId = matledger.MaterialRefId;
                    actDto.Issued = matledger.Issued;
                    actDto.Sales = matledger.Sales;
                    actDto.Damaged = matledger.Damaged;
                    actDto.Shortage = matledger.Shortage;
                    actDto.Return = matledger.Return;
                    actDto.TotalConsumption = ConsumptionQty;
                    ActualMaterialUsageDetail.Add(actDto);
                }
                else
                {
                    var idx = ActualMaterialUsageDetail.FindIndex(t => t.MaterialRefId == matledger.MaterialRefId);
                    ActualMaterialUsageDetail[idx].Issued = ActualMaterialUsageDetail[idx].Issued + matledger.Issued;
                    ActualMaterialUsageDetail[idx].Sales = ActualMaterialUsageDetail[idx].Sales + matledger.Sales;
                    ActualMaterialUsageDetail[idx].Damaged = ActualMaterialUsageDetail[idx].Damaged + matledger.Damaged;
                    ActualMaterialUsageDetail[idx].Shortage = ActualMaterialUsageDetail[idx].Shortage + matledger.Shortage;
                    ActualMaterialUsageDetail[idx].Return = ActualMaterialUsageDetail[idx].Return + matledger.Return;
                    ActualMaterialUsageDetail[idx].TotalConsumption = ActualMaterialUsageDetail[idx].TotalConsumption + ConsumptionQty;
                }

                openingCostValue = openingCostValue + Math.Round(matledger.OpenBalance * matrate.AvgRate, roundDecimals, MidpointRounding.AwayFromZero);
                closingCostValue = closingCostValue + Math.Round(matledger.ClBalance * matrate.AvgRate, roundDecimals, MidpointRounding.AwayFromZero);
                overAllIssuedValue = overAllIssuedValue + Math.Round(TotalIssued * matrate.AvgRate, 2);
                overAllReceivedValue = overAllReceivedValue + Math.Round(TotalReceived * matrate.AvgRate, roundDecimals, MidpointRounding.AwayFromZero);
            }

            string priceRemarksinSingleString = "";
            if (priceremarks.Count > 0)
            {
                var lstPriceRemarks = priceremarks.Distinct().ToList();

                priceRemarksinSingleString = L("MaterialRateAsZeroErr");

                foreach (var lst in lstPriceRemarks)
                {
                    priceRemarksinSingleString = priceRemarksinSingleString + "\r\n <BR>" + lst;
                }
            }

            output.OpeningCostValue = openingCostValue;
            output.ClosingValue = closingCostValue;
            output.OverAllIssueValue = overAllIssuedValue;
            output.OverAllReceivedValue = overAllReceivedValue;

            output.ActualFoodCostValue = decActualTotalFoodCostValue;
            if (decActualTotalFoodCostValue != 0)
                if (output.SalesValueWithoutTax == 0)
                {
                    output.ActualFoodCostPercentage = 0;
                }
                else
                {
                    output.ActualFoodCostPercentage = Math.Round(decActualTotalFoodCostValue / output.SalesValueWithoutTax * 100, roundDecimals, MidpointRounding.AwayFromZero);
                }
            else
                output.ActualFoodCostPercentage = 0;

            output.ActualMaterialWiseValueAndPercentage = detailActualOutput.OrderBy(t => t.MaterialRefName).ToList();

            output.PricingRemarks = priceRemarksinSingleString;
            output.MappingRemarks = materialmappingRemarks;

            if (output.SalesValueWithoutTax != 0 && output.FoodCostValue != 0)
            {
                output.VarianceValue = output.FoodCostValue - output.ActualFoodCostValue;

                output.VariancePercentage = Math.Round(output.VarianceValue / output.SalesValueWithoutTax * 100, roundDecimals, MidpointRounding.AwayFromZero);
            }

            if (output.SalesValueWithoutTax == 0)
                remarks = remarks + L("SalesNotHappendForTheGivenDate");

            if (output.SalesValueWithoutTax < output.FoodCostValue)
                remarks = remarks + L("FoodCostGreaterThanSalesValue");

            output.Remarks = remarks;

            #endregion Get_Actual_Cost

            //  Standard Vs Actual Comparision Report DTO Fillup
            List<MaterialStandardVsActualUsageWithPriceAndDateRange> diffStdvsAct = new List<MaterialStandardVsActualUsageWithPriceAndDateRange>();

            int[] standardMaterialList = detailStandardOutput.Select(a => a.MaterialRefId).ToArray();
            int[] actualMaterialList = detailActualOutput.Select(a => a.MaterialRefId).ToArray();
            var arrmaterialList = standardMaterialList.Concat(actualMaterialList).Distinct();

            foreach (var matid in arrmaterialList)
            {
                MaterialStandardVsActualUsageWithPriceAndDateRange newdet = new MaterialStandardVsActualUsageWithPriceAndDateRange();
                newdet.MaterialRefId = matid;
                if (matListtoVerify.Contains(matid))
                {
                    int adf = 1;
                    adf++;
                }
                var stdMat = detailStandardOutput.FirstOrDefault(t => t.MaterialRefId == matid);
                var actMat = detailActualOutput.FirstOrDefault(t => t.MaterialRefId == matid);

                var stdDetail = StandardMaterialUsageDetail.Where(t => t.MaterialRefId == matid).ToList();
                newdet.StandardMaterialUsageDetail = stdDetail;

                var actDetail = ActualMaterialUsageDetail.Where(t => t.MaterialRefId == matid).ToList();
                newdet.ActualMaterialUsageDetail = actDetail;

                if (stdMat != null)
                {
                    newdet.MaterialRefName = stdMat.MaterialRefName;
                    newdet.Uom = stdMat.Uom;
                    newdet.AvgRate = stdMat.AvgRate;
                    newdet.InvoiceDateRange = stdMat.InvoiceDateRange;

                    newdet.StandardQty = Math.Round(stdMat.UsageQty, 14, MidpointRounding.AwayFromZero);
                    newdet.StandardAmount = stdMat.NetAmount;
                    newdet.StandardCostPercentage = stdMat.CostPercentage;
                }

                if (actMat != null && stdMat == null)
                {
                    newdet.MaterialRefName = actMat.MaterialRefName;
                    newdet.Uom = actMat.Uom;
                    newdet.AvgRate = actMat.AvgRate;
                    newdet.InvoiceDateRange = actMat.InvoiceDateRange;
                }

                if (actMat != null)
                {
                    newdet.ActualQty = Math.Round(actMat.UsageQty, 14, MidpointRounding.AwayFromZero);
                    newdet.ActualAmount = actMat.NetAmount;
                    newdet.ActualCostPercentage = actMat.CostPercentage;
                }

                newdet.QuantityDifference = Math.Round(newdet.StandardQty - newdet.ActualQty, 14, MidpointRounding.AwayFromZero);
                newdet.CostDifference = Math.Round(newdet.StandardAmount - newdet.ActualAmount, 14, MidpointRounding.AwayFromZero);

                diffStdvsAct.Add(newdet);
            }

            output.StandardVsActual = diffStdvsAct;

            //  Get Group Category Wise Report

            List<MaterialGroupCategoryCosting> detailCategoryActualOutput = new List<MaterialGroupCategoryCosting>();

            var issueMaster = _issueRepo.GetAll();
            var issueDetail = _issuedetailRepo.GetAll();

            if (input.Locations != null && input.Locations.Any())
            {
                var allLocations = input.Locations.Select(a => a.Id).ToArray();
                issueMaster = issueMaster.Where(a => allLocations.Contains(a.LocationRefId));
            }

            //@@ ReportPending UC   @ 22 Jan

            List<MaterialRateViewListDto> issueMaterialUsage;

            issueMaterialUsage = await (from issmas in issueMaster
                            .Where(a => DbFunctions.TruncateTime(a.IssueTime) >= input.StartDate.Date
                                && DbFunctions.TruncateTime(a.IssueTime) <= input.EndDate.Date)
                                        join issdet in issueDetail
                                        on issmas.Id equals issdet.IssueRefId
                                        join matcat in _materialGroupCategoryRepo.GetAll()
                                        on issmas.MaterialGroupCategoryRefId equals matcat.Id
                                        join mat in _materialRepo.GetAll()
                                        on issdet.MaterialRefId equals mat.Id
                                        select new MaterialRateViewListDto
                                        {
                                            IssueGroupCategoryId = (int)issmas.MaterialGroupCategoryRefId,
                                            IssueGroupCategoryName = matcat.MaterialGroupCategoryName,
                                            MaterialRefId = issdet.MaterialRefId,
                                            BilledQty = issdet.IssueQty,
                                            UnitRefId = issdet.UnitRefId,
                                            DefaultUnitId = mat.DefaultUnitId
                                        }).ToListAsync();

            var actualMatusageUc = (from issmenu in issueMaterialUsage
                                    join uc in rsUc
                                    on issmenu.UnitRefId equals uc.BaseUnitId
                                    where uc.RefUnitId == issmenu.DefaultUnitId
                                    group issmenu by new
                                    {
                                        issmenu.IssueGroupCategoryId,
                                        issmenu.IssueGroupCategoryName,
                                        issmenu.MaterialRefId,
                                        issmenu.DefaultUnitId,
                                        issmenu.UnitRefId,
                                        issmenu.UnitRefName,
                                        issmenu.DefaultUnitName,
                                        uc.Conversion
                                    } into g
                                    select new MaterialRateViewListDto
                                    {
                                        IssueGroupCategoryId = g.Key.IssueGroupCategoryId,
                                        IssueGroupCategoryName = g.Key.IssueGroupCategoryName,
                                        MaterialRefId = g.Key.MaterialRefId,
                                        UnitRefId = g.Key.UnitRefId,
                                        UnitRefName = g.Key.UnitRefName,
                                        DefaultUnitId = g.Key.DefaultUnitId,
                                        DefaultUnitName = g.Key.DefaultUnitName,
                                        BilledQty = g.Sum(t => t.BilledQty * g.Key.Conversion),
                                    }).ToList().OrderBy(t => t.MaterialRefId);

            var actualMatusage = (from issmenu in actualMatusageUc
                                  group issmenu by new
                                  {
                                      issmenu.IssueGroupCategoryId,
                                      issmenu.IssueGroupCategoryName,
                                      issmenu.MaterialRefId,
                                      issmenu.DefaultUnitId,
                                      issmenu.DefaultUnitName
                                  } into g
                                  select new MaterialRateViewListDto
                                  {
                                      IssueGroupCategoryId = g.Key.IssueGroupCategoryId,
                                      IssueGroupCategoryName = g.Key.IssueGroupCategoryName,
                                      MaterialRefId = g.Key.MaterialRefId,
                                      DefaultUnitId = g.Key.DefaultUnitId,
                                      DefaultUnitName = g.Key.DefaultUnitName,
                                      BilledQty = g.Sum(t => t.BilledQty)
                                  }).ToList();

            //@@ ReportPending UC   @ 22 Jan
            List<MaterialRateViewListDto> groupCategoryActualUsage;
            groupCategoryActualUsage = (from cons in actualMatusage
                                        group cons by new
                                        {
                                            cons.IssueGroupCategoryId,
                                            cons.MaterialRefId,
                                            cons.IssueGroupCategoryName,
                                            cons.DefaultUnitId
                                        } into g
                                        select new MaterialRateViewListDto
                                        {
                                            IssueGroupCategoryId = g.Key.IssueGroupCategoryId,
                                            IssueGroupCategoryName = g.Key.IssueGroupCategoryName,
                                            MaterialRefId = g.Key.MaterialRefId,
                                            BilledQty = g.Sum(t => t.BilledQty),
                                            DefaultUnitId = g.Key.DefaultUnitId
                                        }).ToList();

            decimal decGroupActualTotalFoodCostValue = 0;
            foreach (var usage in groupCategoryActualUsage)
            {
                if (usage.BilledQty == 0)
                    continue;
                var matrate = materialRate.FirstOrDefault(t => t.MaterialRefId == usage.MaterialRefId);

                decimal materialcostvalue;

                if (matrate == null)
                {
                    throw new UserFriendlyException("NoRateAvaiableIssueErr", usage.MaterialName);
                }

                if (matrate.AvgRate == 0)
                {
                    //priceremarks = priceremarks + "," + matrate.MaterialName;
                }

                materialcostvalue = Math.Round(matrate.AvgRate * usage.BilledQty, roundDecimals, MidpointRounding.AwayFromZero);
                decGroupActualTotalFoodCostValue = decGroupActualTotalFoodCostValue + materialcostvalue;

                MaterialGroupCategoryCosting catWithrate = new MaterialGroupCategoryCosting();
                catWithrate.MaterialGroupCategoryRefId = usage.IssueGroupCategoryId;
                catWithrate.MaterialGroupCategoryRefName = usage.IssueGroupCategoryName;
                catWithrate.NetAmount = materialcostvalue;
                detailCategoryActualOutput.Add(catWithrate);
            }

            var resultcategoryoutput = (from cat in detailCategoryActualOutput
                                        group cat by new { cat.MaterialGroupCategoryRefId, cat.MaterialGroupCategoryRefName } into g
                                        select new MaterialGroupCategoryCosting
                                        {
                                            MaterialGroupCategoryRefId = g.Key.MaterialGroupCategoryRefId,
                                            MaterialGroupCategoryRefName = g.Key.MaterialGroupCategoryRefName,
                                            NetAmount = g.Sum(t => t.NetAmount)
                                        }
                                        ).ToList();

            //GetCategorySales
            //  Get Sales

            var CategorySales = await _categoryReportAppService.GetCategorySales(
                new GetItemInput
                {
                    StartDate = fromDt,
                    EndDate = toDt,
                    Locations = locinput.MapTo<List<SimpleLocationDto>>(),
                    //Gift = true,
                    //Void = true,
                    //Refund = true,
                });

            List<CategoryReportDto> categorySalesGroup = new List<CategoryReportDto>();

            if (CategorySales.CategoryList != null)
                categorySalesGroup = CategorySales.CategoryList.Items.ToList();

            //  Standard Vs Actual Category Wise Comparision Report DTO Fillup
            List<MaterialGroupCategorySalesVsIssueCosting> categorydiffStdvsAct = new List<MaterialGroupCategorySalesVsIssueCosting>();

            string[] issueCategoryList = resultcategoryoutput.Select(a => a.MaterialGroupCategoryRefName).ToArray();
            string[] salesCategoryList = categorySalesGroup.Select(a => a.CategoryName).ToArray();
            var arrCategoryList = issueCategoryList.Concat(salesCategoryList).Distinct();

            foreach (var categoryName in arrCategoryList)
            {
                MaterialGroupCategorySalesVsIssueCosting newdet = new MaterialGroupCategorySalesVsIssueCosting();
                newdet.MaterialGroupCategoryRefName = categoryName;

                var issCategory = resultcategoryoutput.FirstOrDefault(t => t.MaterialGroupCategoryRefName == categoryName);
                var salCategory = categorySalesGroup.FirstOrDefault(t => t.CategoryName == categoryName);

                if (salCategory != null)
                {
                    newdet.SalesAmount = salCategory.Total;
                }

                decimal catCostPercentage = 0;
                if (issCategory != null)
                {
                    newdet.CostAmount = issCategory.NetAmount;
                    if (newdet.SalesAmount > 0)
                        catCostPercentage = Math.Round(newdet.CostAmount / newdet.SalesAmount, roundDecimals, MidpointRounding.AwayFromZero);
                }
                if (catCostPercentage == 0)
                    newdet.CostPercentage = "N/A";
                else
                    newdet.CostPercentage = catCostPercentage.ToString();

                categorydiffStdvsAct.Add(newdet);
            }

            output.SalesVsIsueCategoryWiseCosting = categorydiffStdvsAct;

            output.ActualCategoryWiseCost = resultcategoryoutput;
            return output;
        }

        public async Task<RecipeCostingDtoConsolidated> GetRecipeCostingReport(GetCostReportInput input)
        {
            decimal totalSalesWithoutTax;
            string remarks = "";

            if (input.EndDate.Date == DateTime.Now.Date)
            {
                remarks = L("TodayMayNotShownCorrectReportBeforeDayClose");
            }

            if (input.Locations == null)
            {
                throw new UserFriendlyException("LocationErr");
            }

            List<LocationListDto> locinput = input.Locations;
            int[] locationList = locinput.Select(t => t.Id).ToArray();

            DateTime fromDt = input.StartDate.Date;
            DateTime toDt = input.EndDate.Date;

            //  Get Sales
            //var Sales = await _connectReportAppService.GetItemSales(
            //    new GetItemInput
            //    {
            //        StartDate = fromDt,
            //        EndDate = toDt,
            //        Locations = locinput,
            //        //Gift = true,
            //        //Void = true,
            //        //Refund = true,
            //    });

            //  Get Sales
            var allSales = await _connectReportAppService.GetAllItemSales(
                new GetItemInput
                {
                    StartDate = fromDt,
                    EndDate = toDt,
                    Locations = locinput.MapTo<List<SimpleLocationDto>>(),
                    Sales = true,
                    Comp = true,
                    Void = true,
                    Gift = true
                });

            totalSalesWithoutTax = allSales.Sales.Sum(t => t.Total);

            var itemSales = allSales.Sales;

            var recipeMaterials = await _materialRepo.GetAll().Where(t => t.MaterialTypeId != (int)MaterialType.RAW).ToListAsync();

            if (input.Recipes.Count > 0)
            {
                string[] arrstringreciperefs = input.Recipes.Select(t => t.Value).ToArray();
                int[] recipeRefIds = arrstringreciperefs.Select(int.Parse).ToArray();
                recipeMaterials = recipeMaterials.Where(t => recipeRefIds.Contains(t.Id)).ToList();
            }

            var unitList = _unitrepo.GetAll().ToList();

            RecipeCostingDtoConsolidated returnoutput = new RecipeCostingDtoConsolidated();

            List<RecipeCostingDto> outputRecipe = new List<RecipeCostingDto>();

            if (recipeMaterials.Count == 0)
            {
                returnoutput.RecipeWiseCosting = new List<RecipeCostingDto>();

                returnoutput.Remarks = "No More Records";

                return returnoutput;
            }
            //  Material Wise Price
            List<int> materialFilter = recipeMaterials.Select(t => t.Id).ToList();
            var matView = await GetMaterialRateView(new GetHouseReportMaterialRateInput
            {
                StartDate = fromDt,
                EndDate = toDt,
                Locations = locinput,
                MaterialRefIds = materialFilter,
                FunctionCalledBy = "Get Recipe Costing Report",
                DoesIngredientsDetailsRequired = true
            });

            List<MaterialRateViewListDto> materialRate = matView.MaterialRateView;

            foreach (var rm in recipeMaterials)
            {
                RecipeCostingDto newDto = new RecipeCostingDto();
                newDto.MaterialRefId = rm.Id;
                newDto.MaterialRefName = rm.MaterialName;
                newDto.Uom = unitList.First(t => t.Id == rm.DefaultUnitId).Name;

                var prodLst = await (from prdMas in _productionRepo.GetAll().Where(t => locationList.Contains(t.LocationRefId) &&
                                    t.ProductionTime >= input.StartDate.Date &&
                                    DbFunctions.TruncateTime(t.ProductionTime) <= input.EndDate.Date)
                                     join prddet in _productindetailRepo.GetAll().Where(p => p.MaterialRefId == rm.Id)
                                     on prdMas.Id equals prddet.ProductionRefId
                                     select new ProductionDetailViewDto
                                     {
                                         Sno = prddet.Sno,
                                         ProductionRefId = (int)prddet.ProductionRefId,
                                         MaterialRefId = prddet.MaterialRefId,
                                         MaterialRefName = rm.MaterialName,
                                         ProductionQty = prddet.ProductionQty,
                                         UnitRefId = prddet.UnitRefId,
                                         IssueRefId = prddet.IssueRefId,
                                         ProductionReferences = prdMas.ProductionSlipNumber,
                                         ProductionTime = prdMas.ProductionTime
                                     }).ToListAsync();

                newDto.ProductionDetails = prodLst;

                newDto.ProductionQty = Math.Round(prodLst.Sum(t => t.ProductionQty), 14, MidpointRounding.AwayFromZero);

                if (newDto.ProductionQty == 0)
                    continue;

                foreach (var prd in prodLst.Where(t => t.IssueRefId == null))
                {
                    remarks = remarks + rm.MaterialName + " " + L("Production") + " " + L("Ref") + prd.ProductionReferences + " ";
                }

                int?[] issueRefIdList = prodLst.Where(t => t.IssueRefId != null).Select(t => t.IssueRefId).ToArray();

                var issueMaterialDetails = await (from issDet in _issuedetailRepo.GetAll().
                                            Where(a => issueRefIdList.Contains(a.IssueRefId) && a.RecipeRefId == rm.Id)
                                                  join mat in _materialRepo.GetAll()
                                                  on issDet.MaterialRefId equals mat.Id
                                                  join un in _unitrepo.GetAll()
                                                  on mat.DefaultUnitId equals un.Id
                                                  select new IssueDetailViewDto
                                                  {
                                                      Id = issDet.Id,
                                                      IssueQty = issDet.IssueQty,
                                                      IssueRefId = issDet.IssueRefId,
                                                      MaterialRefId = issDet.MaterialRefId,
                                                      MaterialRefName = mat.MaterialName,
                                                      Sno = issDet.Sno,
                                                      DefaultUnitId = mat.DefaultUnitId,
                                                      DefaultUnitName = un.Name
                                                  }).ToListAsync();

                var matlistWithSumGroup = issueMaterialDetails.GroupBy(t => t.MaterialRefId).Select(g => g.ToList()).ToList();

                List<MaterialUsageWithPriceAndDateRange> groupMaterialDetails = new List<MaterialUsageWithPriceAndDateRange>();

                foreach (var g in matlistWithSumGroup)
                {
                    var matprice = materialRate.FirstOrDefault(t => t.MaterialRefId == g[0].MaterialRefId);

                    MaterialUsageWithPriceAndDateRange matDto = new MaterialUsageWithPriceAndDateRange
                    {
                        MaterialRefId = g[0].MaterialRefId,
                        MaterialRefName = g[0].MaterialRefName,
                        InvoiceDateRange = matprice.InvoiceDateRange,
                        AvgRate = matprice.AvgRate,
                        Uom = g[0].DefaultUnitName,
                        UsageQty = g.Sum(t => t.IssueQty),
                        NetAmount = Math.Round(g.Sum(t => t.IssueQty) * matprice.AvgRate, roundDecimals, MidpointRounding.AwayFromZero),
                        CostPercentage = 0
                    };
                    groupMaterialDetails.Add(matDto);
                }

                newDto.CostForProduction = Math.Round(groupMaterialDetails.Sum(t => t.NetAmount), roundDecimals, MidpointRounding.AwayFromZero);

                newDto.ActualMaterialWiseValueAndPercentage = groupMaterialDetails;

                if (newDto.ProductionQty > 0 && newDto.CostForProduction > 0)
                    newDto.CostPerUom = Math.Round(newDto.CostForProduction / newDto.ProductionQty, roundDecimals, MidpointRounding.AwayFromZero);

                List<MaterialMenuMappingDto> mapwithMenuList = new List<MaterialMenuMappingDto>();
                var outputlist = await _materialmenumappingRepo.GetAll().Where(t => t.MaterialRefId == rm.Id && t.LocationRefId == input.LocationRefId).ToListAsync();
                if (outputlist == null || outputlist.Count == 0)
                {
                    outputlist = await _materialmenumappingRepo.GetAll().Where(t => t.MaterialRefId == rm.Id && t.LocationRefId == null).ToListAsync();
                }
                newDto.SaleConvertedCost = 0;

                foreach (var a in outputlist)
                {
                    var QtySold = itemSales.Where(t => t.MenuItemPortionId == a.PosMenuPortionRefId).Sum(t => t.Quantity);
                    var TotalSales = itemSales.Where(t => t.MenuItemPortionId == a.PosMenuPortionRefId).Sum(t => t.Total);

                    var posName = itemSales.FirstOrDefault(t => t.MenuItemPortionId == a.PosMenuPortionRefId);

                    MaterialMenuMappingDto newmm = new MaterialMenuMappingDto();

                    newmm.PosMenuPortionRefId = a.PosMenuPortionRefId;
                    newmm.PosRefName = posName.MenuItemName + " - " + posName.MenuItemPortionName;
                    newmm.MenuItemName = posName.MenuItemName;
                    newmm.Portion = posName.MenuItemPortionName;
                    newmm.MenuQuantitySold = a.MenuQuantitySold;
                    newmm.MaterialRefId = a.MaterialRefId;
                    newmm.PortionQty = Math.Round(a.PortionQty / a.MenuQuantitySold * QtySold, 14, MidpointRounding.AwayFromZero);
                    newmm.AutoSalesDeduction = a.AutoSalesDeduction;
                    newmm.PortionUnitId = a.PortionUnitId;
                    newmm.TotalSaleQuantity = QtySold;
                    newmm.CostPerUnit = newDto.CostPerUom;
                    newmm.CostTotal = Math.Round(newmm.PortionQty * newDto.CostPerUom, roundDecimals, MidpointRounding.AwayFromZero);
                    newmm.TotalSaleValue = TotalSales;
                    newmm.UnitRefName = newDto.Uom;

                    mapwithMenuList.Add(newmm);
                }

                newDto.SaleConvertedCost = mapwithMenuList.Sum(t => t.CostTotal);

                newDto.TotalSales = mapwithMenuList.Sum(t => t.TotalSaleValue);
                newDto.RecipeSalesDetail = mapwithMenuList;

                var wastages = _materialLedgerRepo.GetAll().Where(t => locationList.Contains(t.LocationRefId) && t.MaterialRefId == rm.Id && t.LedgerDate >= fromDt && t.LedgerDate <= toDt).ToList();

                decimal WastageQtyUptoToday = wastages.Sum(t => t.Damaged) + wastages.Sum(t => t.Shortage);

                var currentdayLedger = _materialLedgerRepo.GetAll().Where(t => locationList.Contains(t.LocationRefId) && t.MaterialRefId == rm.Id && t.LedgerDate >= toDt && t.LedgerDate <= toDt).ToList();

                newDto.WastageQty = WastageQtyUptoToday;
                newDto.WastageCost = Math.Round(WastageQtyUptoToday * newDto.CostPerUom, roundDecimals, MidpointRounding.AwayFromZero);

                if (newDto.ProductionQty > 0)
                    outputRecipe.Add(newDto);
            }

            returnoutput.RecipeWiseCosting = outputRecipe;

            returnoutput.Remarks = remarks;

            return returnoutput;
        }

        public async Task<MenuItemCostingDtoConsolidated> GetMenuItemCostingReport(GetCostReportInput input)
        {
            decimal deptTotalSalesWithoutTax;
            string remarks = "";

            if (input.EndDate.Date == DateTime.Now.Date)
            {
                remarks = L("TodayMayNotShownCorrectReportBeforeDayClose") + " <br>";
            }

            if (input.Locations == null)
            {
                throw new UserFriendlyException("LocationErr");
            }

            List<LocationListDto> locinput = input.Locations;
            int[] locationList = locinput.Select(t => t.Id).ToArray();

            DateTime fromDt = input.StartDate.Date;
            DateTime toDt = input.EndDate.Date;

            var departmentSales = await _connectReportAppService.GetAllItemSales(
                new GetItemInput
                {
                    StartDate = fromDt,
                    EndDate = toDt,
                    Locations = locinput.MapTo<List<SimpleLocationDto>>(),
                    Gift = input.GiftIncludeFlag,
                    Void = false,
                    Comp = input.CompIncludeFlag,
                    Sales = true
                });

            if (departmentSales.Sales != null)
                deptTotalSalesWithoutTax = departmentSales.Sales.Sum(t => t.Total);

            var totalItems = new List<MenuListDto>();
            if (departmentSales.Sales != null)
            {
                totalItems.AddRange(departmentSales.Sales);
            }

            if (departmentSales.Gift != null && input.GiftIncludeFlag)
                totalItems.AddRange(departmentSales.Gift);

            if (departmentSales.Comp != null && input.CompIncludeFlag)
                totalItems.AddRange(departmentSales.Comp);

            totalItems = totalItems.OrderBy(t => t.MenuItemPortionId).ToList();

            var rsMenuList = await (from menuportion in _menuitemportionRepo.GetAll()
                                    join menu in _menuitemRepo.GetAll()
                                    on menuportion.MenuItemId equals menu.Id
                                    select new ProductListDto
                                    {
                                        PortionId = menuportion.Id,
                                        PortionName = string.Concat(menu.Name, " - ", menuportion.Name)
                                    }).ToListAsync();

            List<ProductListDto> menuSoldList = new List<ProductListDto>();
            menuSoldList = rsMenuList.OrderBy(t => t.PortionName).ToList();

            if (input.MenuItems.Count > 0)
            {
                string[] arrstringmenurefs = input.MenuItems.Select(t => t.Value).ToArray();
                int[] menuRefIds = arrstringmenurefs.Select(int.Parse).ToArray();
                menuSoldList = rsMenuList.Where(t => menuRefIds.Contains(t.PortionId)).OrderBy(t => t.PortionName).ToList();
                totalItems = totalItems.Where(t => menuRefIds.Contains(t.MenuItemPortionId)).ToList();
            }

            MenuItemCostingDtoConsolidated returnoutput = new MenuItemCostingDtoConsolidated();

            if (menuSoldList.Count == 0 || menuSoldList == null)
            {
                return returnoutput;
            }

            var gbyDeptDtos = from c in totalItems
                              group c by c.DepartmentName into g
                              select new { DepartmentName = g.Key, TotalItems = g };

            var unitList = await _unitrepo.GetAllListAsync();
            var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();

            var arrPortionIds = menuSoldList.Select(t => t.PortionId).ToList();
            var rsMenuMappingMaterials = await _materialmenumappingRepo.GetAllListAsync(t => arrPortionIds.Contains(t.PosMenuPortionRefId));

            var rsMenumappingDepartment = await _menumappingdepartmentRepo.GetAll().ToListAsync();

            var rsDeptList = await _departmentRepo.GetAll().ToListAsync();

            var rsMaterial = await _materialRepo.GetAllListAsync();

            List<MenuItemCostingDto> outputMenu = new List<MenuItemCostingDto>();
            List<int> materialFilter = rsMenuMappingMaterials.Select(t => t.MaterialRefId).ToList();
            var matView = await GetMaterialRateView(new GetHouseReportMaterialRateInput
            {
                StartDate = fromDt,
                EndDate = toDt,
                Locations = locinput,
                MaterialRefIds = materialFilter,
                FunctionCalledBy = "Menu Item Costing Report"
            });

            List<MaterialRateViewListDto> materialRate = matView.MaterialRateView;

            foreach (var menu in menuSoldList)
            {
                if (totalItems != null)
                {
                    foreach (var deptItems in gbyDeptDtos)
                    {
                        decimal menuItemSoldQty = deptItems.TotalItems.Where(t => t.MenuItemPortionId == menu.PortionId).ToList().Sum(t => t.Quantity);
                        if (menuItemSoldQty == 0)
                            continue;

                        MenuItemCostingDto newDto = new MenuItemCostingDto();
                        //newDto.DepartmentRefId = deptItems.Id;
                        newDto.DepartmentRefName = deptItems.DepartmentName;

                        newDto.MenuItemRefId = menu.PortionId;
                        newDto.MenuItemRefName = menu.PortionName;
                        newDto.QtySold = menuItemSoldQty;
                        newDto.SalesAmount = deptItems.TotalItems.Where(t => t.MenuItemPortionId == menu.PortionId).ToList().Sum(t => t.Quantity * t.Price);
                        newDto.UnitPrice = newDto.SalesAmount / newDto.QtySold;

                        var menuMapping = rsMenuMappingMaterials.Where(t => t.PosMenuPortionRefId == menu.PortionId && t.LocationRefId == input.LocationRefId).ToList();

                        if (menuMapping == null || menuMapping.Count == 0)
                        {
                            menuMapping = rsMenuMappingMaterials.Where(t => t.PosMenuPortionRefId == menu.PortionId && t.LocationRefId == null).ToList();
                        }

                        var department = rsDeptList.FirstOrDefault(t => t.Name.ToUpper().Equals(deptItems.DepartmentName.ToUpper()));

                        if (department == null)
                        {
                            throw new UserFriendlyException(L("Department") + " " + L("NotExist"), deptItems.DepartmentName);
                        }

                        int deptId = department.Id;

                        int[] menuMappingRefIds = menuMapping.Select(t => t.Id).ToArray();

                        var deptList = rsMenumappingDepartment.Where(t => menuMappingRefIds.Contains(t.MenuMappingRefId)).ToList();

                        var AllDepartmentFlag = !deptList.Any();

                        if (!menuMapping.Any())
                        {
                            remarks = remarks + L("MenuMappingNotDoneForMenuItem", menu.PortionName + " " + deptItems.DepartmentName) + " <br>";
                        }

                        List<MaterialUsageWithPriceAndDateRange> groupMaterialDetails = new List<MaterialUsageWithPriceAndDateRange>();

                        foreach (var material in menuMapping)
                        {
                            if (AllDepartmentFlag == false)
                            {
                                var deptLinkedMM = deptList.Where(t => t.MenuMappingRefId == material.Id);
                                if (deptLinkedMM.Any())
                                {
                                    deptLinkedMM = deptLinkedMM.Where(t => t.DepartmentRefId == deptId);
                                    if (!deptLinkedMM.Any())
                                    {
                                        continue;
                                    }
                                }
                            }

                            var matprice = materialRate.FirstOrDefault(t => t.MaterialRefId == material.MaterialRefId);

                            var selectedMaterial = rsMaterial.FirstOrDefault(t => t.Id == material.MaterialRefId);
                            if (selectedMaterial == null)
                                continue;

                            var portionUnitName = unitList.FirstOrDefault(t => t.Id == material.PortionUnitId);
                            if (portionUnitName == null)
                                throw new UserFriendlyException(L("Unit") + " " + L("NotExist") + " - " + matprice.MaterialName);

                            decimal usagePerUnitOfMenuitemSales = Math.Round(material.PortionQty / material.MenuQuantitySold, 14, MidpointRounding.AwayFromZero);
                            decimal usageQty = Math.Round(usagePerUnitOfMenuitemSales * newDto.QtySold, 14, MidpointRounding.AwayFromZero);
                            decimal unitPrice = 0;
                            if (material.PortionUnitId == matprice.DefaultUnitId)
                                unitPrice = matprice.AvgRate;
                            else
                            {
                                var unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == material.PortionUnitId && t.RefUnitId == matprice.DefaultUnitId && t.MaterialRefId == material.Id);
                                if (unitConversion == null)
                                    unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == material.PortionUnitId && t.RefUnitId == matprice.DefaultUnitId);
                                if (unitConversion == null)
                                    throw new UserFriendlyException(L("UnitConversion") + " " + L("NotExist") + " - " + matprice.MaterialName);
                                unitPrice = unitConversion.Conversion * matprice.AvgRate;
                            }

                            decimal netAmount = Math.Round(usageQty * matprice.AvgRate, roundDecimals, MidpointRounding.AwayFromZero);

                            MaterialUsageWithPriceAndDateRange matDto = new MaterialUsageWithPriceAndDateRange
                            {
                                MaterialRefId = material.MaterialRefId,
                                MaterialRefName = matprice.MaterialName,
                                InvoiceDateRange = matprice.InvoiceDateRange,
                                AvgRate = unitPrice,
                                UsagePerUnit = usagePerUnitOfMenuitemSales,
                                Uom = portionUnitName.Name,
                                UsageQty = usageQty,
                                NetAmount = netAmount,
                                CostPercentage = 0
                            };
                            groupMaterialDetails.Add(matDto);
                        }

                        newDto.CostPerUnit = Math.Round(groupMaterialDetails.Sum(t => t.UsagePerUnit * t.AvgRate), 14, MidpointRounding.AwayFromZero);
                        newDto.TotalCost = newDto.CostPerUnit * newDto.QtySold;
                        newDto.Profit = newDto.SalesAmount - newDto.TotalCost;
                        if (newDto.Profit == 0 || newDto.SalesAmount == 0)
                        {
                            newDto.ProfitMargin = 0;
                        }
                        else
                        {
                            newDto.ProfitMargin = Math.Round(newDto.Profit / newDto.SalesAmount * 100, roundDecimals, MidpointRounding.AwayFromZero);
                        }
                        newDto.StandardMaterialWiseValueAndPercentage = groupMaterialDetails;

                        outputMenu.Add(newDto);
                    }
                }
            }

            outputMenu = outputMenu.OrderBy(t => t.MenuItemRefName).OrderBy(t => t.DepartmentRefName).ToList();

            returnoutput.RecipeWiseCosting = outputMenu;
            returnoutput.TotalCost = outputMenu.Sum(t => t.TotalCost);
            returnoutput.TotalSales = outputMenu.Sum(t => t.SalesAmount);
            returnoutput.TotalProfit = outputMenu.Sum(t => t.Profit);
            if (returnoutput.TotalSales > 0 && returnoutput.TotalProfit > 0)
                returnoutput.TotalProfitMargin = Math.Round(returnoutput.TotalProfit / returnoutput.TotalSales * 100, roundDecimals, MidpointRounding.AwayFromZero);

            if (remarks.Length > 0)
            {
                remarks = @L("PointsToVerify") + "<BR>" + remarks;
                returnoutput.Remarks = remarks;
            }
            return returnoutput;
        }

        public async Task<ProductionUnitCostingDtoConsolidated> GetProductionUnitCostingReport(GetProductionUnitCostReportInput input)
        {
            string remarks = "";

            if (input.EndDate.Date == DateTime.Now.Date)
            {
                remarks = L("TodayMayNotShownCorrectReportBeforeDayClose");
            }

            if (input.Locations == null)
            {
                throw new UserFriendlyException("LocationErr");
            }

            if (input.ProductionUnits == null || input.ProductionUnits.Count == 0)
            {
                var locid = input.Locations[0].Id;

                input.ProductionUnits = _productionunitRepo.GetAll().Where(t => t.LocationRefId == locid).ToList().MapTo<List<ProductionUnitListDto>>();
                //throw new UserFriendlyException(L("ProductionUnit") + L("Error")) ;
            }

            List<LocationListDto> locinput = input.Locations;
            int[] locationList = locinput.Select(t => t.Id).ToArray();

            DateTime fromDt = input.StartDate.Date;
            DateTime toDt = input.EndDate.Date;

            //  Material Wise Price

            var productionUnits = await _productionunitRepo.GetAll().Where(t => locationList.Contains(t.LocationRefId)).ToListAsync();

            if (input.ProductionUnits.Count > 0)
            {
                int[] puRefIds = input.ProductionUnits.Select(t => t.Id).ToArray();
                //int[] puRefIds = arrstringpurefs.Select(int.Parse).ToArray();
                productionUnits = productionUnits.Where(t => puRefIds.Contains(t.Id)).ToList();
            }

            var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();

            var unitList = _unitrepo.GetAll().ToList();

            ProductionUnitCostingDtoConsolidated returnoutput = new ProductionUnitCostingDtoConsolidated();

            List<ProductionUnitCostingDto> outputRecipe = new List<ProductionUnitCostingDto>();

            List<Location> lstLocationSearch = _locationRepo.GetAll().ToList();

            foreach (var rm in productionUnits)
            {
                ProductionUnitCostingDto newDto = new ProductionUnitCostingDto();
                newDto.LocationRefId = rm.LocationRefId;
                newDto.LocationRefName = lstLocationSearch.FirstOrDefault(t => t.Id == rm.LocationRefId).Name;
                newDto.ProductionUnitRefId = rm.Id;
                newDto.ProductionUnitRefName = rm.Name;

                var prodLst = await (from prdMas in _productionRepo.GetAll().
                                      Where(t => t.LocationRefId == rm.LocationRefId && t.ProductionUnitRefId == rm.Id &&
                                    t.ProductionTime >= input.StartDate.Date &&
                                    DbFunctions.TruncateTime(t.ProductionTime) <= input.EndDate.Date)
                                     join prddet in _productindetailRepo.GetAll()
                                     on prdMas.Id equals prddet.ProductionRefId
                                     join mat in _materialRepo.GetAll() on prddet.MaterialRefId equals mat.Id
                                     join un in _unitrepo.GetAll() on mat.DefaultUnitId equals un.Id
                                     join uiss in _unitrepo.GetAll() on prddet.UnitRefId equals uiss.Id
                                     select new ProductionDetailViewDto
                                     {
                                         Sno = prddet.Sno,
                                         ProductionRefId = (int)prddet.ProductionRefId,
                                         MaterialRefId = prddet.MaterialRefId,
                                         MaterialRefName = mat.MaterialName,
                                         ProductionQty = prddet.ProductionQty,
                                         UnitRefId = prddet.UnitRefId,
                                         UnitRefName = uiss.Name,
                                         IssueRefId = prddet.IssueRefId,
                                         ProductionReferences = prdMas.ProductionSlipNumber,
                                         ProductionTime = prdMas.ProductionTime,
                                         DefaultUnitId = mat.DefaultUnitId,
                                         Uom = un.Name
                                     }).ToListAsync();

                var prodListUc = (from prdMas in prodLst
                                  join uc in rsUc
                                   on prdMas.UnitRefId equals uc.BaseUnitId
                                  where uc.RefUnitId == prdMas.DefaultUnitId
                                  group prdMas by new
                                  {
                                      prdMas.ProductionRefId,
                                      prdMas.MaterialRefId,
                                      prdMas.MaterialRefName,
                                      prdMas.DefaultUnitId,
                                      prdMas.UnitRefId,
                                      prdMas.IssueRefId,
                                      prdMas.UnitRefName,
                                      prdMas.Uom,
                                      uc.Conversion
                                  } into g
                                  select new ProductionDetailViewDto
                                  {
                                      IssueRefId = g.Key.IssueRefId,
                                      MaterialRefId = g.Key.MaterialRefId,
                                      MaterialRefName = g.Key.MaterialRefName,
                                      UnitRefId = g.Key.UnitRefId,
                                      UnitRefName = g.Key.UnitRefName,
                                      DefaultUnitId = g.Key.DefaultUnitId,
                                      Uom = g.Key.Uom,
                                      ProductionQty = g.Sum(t => t.ProductionQty * g.Key.Conversion),
                                  }).ToList().OrderBy(t => t.MaterialRefId);

                var yieldprodLst = await (from yield in _yieldRepo.GetAll().
                  Where(t => t.LocationRefId == rm.LocationRefId && t.ProductionUnitRefId == rm.Id &&
              t.CompletedTime >= input.StartDate.Date &&
              DbFunctions.TruncateTime(t.CompletedTime) <= input.EndDate.Date)
                                          join yieldoutput in _yieldoutputRepo.GetAll()
                                          on yield.Id equals yieldoutput.YieldRefId
                                          join mat in _materialRepo.GetAll() on yieldoutput.OutputMaterialRefId equals mat.Id
                                          join un in _unitrepo.GetAll() on mat.DefaultUnitId equals un.Id
                                          join uiss in _unitrepo.GetAll() on yieldoutput.UnitRefId equals uiss.Id
                                          select new ProductionDetailViewDto
                                          {
                                              Sno = yieldoutput.Sno,
                                              ProductionRefId = (int)yield.Id,
                                              MaterialRefId = mat.Id,
                                              MaterialRefName = mat.MaterialName,
                                              ProductionQty = yieldoutput.OutputQty,
                                              UnitRefId = yieldoutput.UnitRefId,
                                              UnitRefName = uiss.Name,
                                              IssueRefId = null,
                                              ProductionReferences = yield.RequestSlipNumber,
                                              ProductionTime = yield.CompletedTime.Value,
                                              DefaultUnitId = mat.DefaultUnitId,
                                              Uom = un.Name
                                          }).ToListAsync();

                var yieldprodLstUc = (from prdMas in yieldprodLst
                                      join uc in rsUc
                                       on prdMas.UnitRefId equals uc.BaseUnitId
                                      where uc.RefUnitId == prdMas.DefaultUnitId
                                      group prdMas by new
                                      {
                                          prdMas.ProductionRefId,
                                          prdMas.MaterialRefId,
                                          prdMas.MaterialRefName,
                                          prdMas.DefaultUnitId,
                                          prdMas.UnitRefId,
                                          prdMas.IssueRefId,
                                          prdMas.UnitRefName,
                                          prdMas.Uom,
                                          uc.Conversion
                                      } into g
                                      select new ProductionDetailViewDto
                                      {
                                          IssueRefId = g.Key.IssueRefId,
                                          MaterialRefId = g.Key.MaterialRefId,
                                          MaterialRefName = g.Key.MaterialRefName,
                                          UnitRefId = g.Key.UnitRefId,
                                          UnitRefName = g.Key.UnitRefName,
                                          DefaultUnitId = g.Key.DefaultUnitId,
                                          Uom = g.Key.Uom,
                                          ProductionQty = g.Sum(t => t.ProductionQty * g.Key.Conversion),
                                      }).ToList().OrderBy(t => t.MaterialRefId);

                newDto.ProductionDetails = prodListUc.Union(yieldprodLstUc).ToList();

                var issueMaterialDetails = await (from issMas in _issueRepo.GetAll()
                                                  .Where(t => t.LocationRefId == rm.LocationRefId && t.ProductionUnitRefId == rm.Id &&
                                                  t.IssueTime >= input.StartDate.Date && DbFunctions.TruncateTime(t.IssueTime) <= input.EndDate.Date)
                                                  join issDet in _issuedetailRepo.GetAll() on issMas.Id equals issDet.IssueRefId
                                                  join mat in _materialRepo.GetAll()
                                                  on issDet.MaterialRefId equals mat.Id
                                                  join un in _unitrepo.GetAll() on mat.DefaultUnitId equals un.Id
                                                  join uiss in _unitrepo.GetAll() on issDet.UnitRefId equals uiss.Id
                                                  select new IssueDetailViewDto
                                                  {
                                                      Id = issDet.Id,
                                                      IssueQty = issDet.IssueQty,
                                                      UnitRefId = issDet.UnitRefId,
                                                      UnitRefName = uiss.Name,
                                                      IssueRefId = issDet.IssueRefId,
                                                      MaterialRefId = issDet.MaterialRefId,
                                                      MaterialRefName = mat.MaterialName,
                                                      Sno = issDet.Sno,
                                                      DefaultUnitId = mat.DefaultUnitId,
                                                      DefaultUnitName = un.Name
                                                  }).ToListAsync();

                var issueMaterialUc = (from issmenu in issueMaterialDetails
                                       join uc in rsUc
                                        on issmenu.UnitRefId equals uc.BaseUnitId
                                       where uc.RefUnitId == issmenu.DefaultUnitId
                                       group issmenu by new
                                       {
                                           issmenu.IssueRefId,
                                           issmenu.MaterialRefId,
                                           issmenu.DefaultUnitId,
                                           issmenu.UnitRefId,
                                           issmenu.UnitRefName,
                                           issmenu.DefaultUnitName,
                                           uc.Conversion
                                       } into g
                                       select new IssueDetailViewDto
                                       {
                                           IssueRefId = g.Key.IssueRefId,
                                           MaterialRefId = g.Key.MaterialRefId,
                                           UnitRefId = g.Key.UnitRefId,
                                           UnitRefName = g.Key.UnitRefName,
                                           DefaultUnitId = g.Key.DefaultUnitId,
                                           DefaultUnitName = g.Key.DefaultUnitName,
                                           IssueQty = g.Sum(t => t.IssueQty * g.Key.Conversion),
                                       }).ToList().OrderBy(t => t.MaterialRefId);

                var yieldissueMaterialDetails = await (from yield in _yieldRepo.GetAll().Where(t => t.LocationRefId == rm.LocationRefId && t.ProductionUnitRefId == rm.Id &&
                   t.IssueTime >= input.StartDate.Date &&
              DbFunctions.TruncateTime(t.IssueTime) <= input.EndDate.Date)
                                                       join yieldinput in _yieldinputRepo.GetAll()
                                                       on yield.Id equals yieldinput.YieldRefId
                                                       join mat in _materialRepo.GetAll()
                                                       on yieldinput.InputMaterialRefId equals mat.Id
                                                       join un in _unitrepo.GetAll() on mat.DefaultUnitId equals un.Id
                                                       join uiss in _unitrepo.GetAll() on yieldinput.UnitRefId equals uiss.Id
                                                       select new IssueDetailViewDto
                                                       {
                                                           Id = yieldinput.Id,
                                                           IssueQty = yieldinput.InputQty,
                                                           IssueRefId = yieldinput.YieldRefId,
                                                           UnitRefId = yieldinput.UnitRefId,
                                                           UnitRefName = uiss.Name,
                                                           MaterialRefId = mat.Id,
                                                           MaterialRefName = mat.MaterialName,
                                                           Sno = yieldinput.Sno,
                                                           DefaultUnitId = mat.DefaultUnitId,
                                                           DefaultUnitName = un.Name
                                                       }).ToListAsync();

                var yieldissueMaterialDetailsUc = (from issmenu in yieldissueMaterialDetails
                                                   join uc in rsUc
                                                    on issmenu.UnitRefId equals uc.BaseUnitId
                                                   where uc.RefUnitId == issmenu.DefaultUnitId
                                                   group issmenu by new
                                                   {
                                                       issmenu.IssueRefId,
                                                       issmenu.MaterialRefId,
                                                       issmenu.DefaultUnitId,
                                                       issmenu.UnitRefId,
                                                       issmenu.UnitRefName,
                                                       issmenu.DefaultUnitName,
                                                       uc.Conversion
                                                   } into g
                                                   select new IssueDetailViewDto
                                                   {
                                                       IssueRefId = g.Key.IssueRefId,
                                                       MaterialRefId = g.Key.MaterialRefId,
                                                       UnitRefId = g.Key.UnitRefId,
                                                       UnitRefName = g.Key.UnitRefName,
                                                       DefaultUnitId = g.Key.DefaultUnitId,
                                                       DefaultUnitName = g.Key.DefaultUnitName,
                                                       IssueQty = g.Sum(t => t.IssueQty * g.Key.Conversion),
                                                   }).ToList().OrderBy(t => t.MaterialRefId);

                var returnMaterialDetails = await (from retMas in _returnRepo.GetAll().Where(t => t.LocationRefId == rm.LocationRefId && t.ProductionUnitRefId == rm.Id &&
              t.ReturnDate >= input.StartDate.Date &&
              DbFunctions.TruncateTime(t.ReturnDate) <= input.EndDate.Date)
                                                   join retDet in _returndetailRepo.GetAll() on retMas.Id equals retDet.ReturnRefId
                                                   join mat in _materialRepo.GetAll()
                                                   on retDet.MaterialRefId equals mat.Id
                                                   join un in _unitrepo.GetAll() on mat.DefaultUnitId equals un.Id
                                                   join uiss in _unitrepo.GetAll() on retDet.UnitRefId equals uiss.Id
                                                   select new IssueDetailViewDto
                                                   {
                                                       Id = retDet.Id,
                                                       IssueQty = retDet.ReturnQty,
                                                       IssueRefId = retDet.ReturnRefId,
                                                       UnitRefId = retDet.UnitRefId,
                                                       UnitRefName = uiss.Name,
                                                       MaterialRefId = retDet.MaterialRefId,
                                                       MaterialRefName = mat.MaterialName,
                                                       Sno = retDet.Sno,
                                                       DefaultUnitName = un.Name,
                                                       DefaultUnitId = mat.DefaultUnitId
                                                   }).ToListAsync();

                var returnMaterialDetailsUc = (from issmenu in returnMaterialDetails
                                               join uc in rsUc
                                                    on issmenu.UnitRefId equals uc.BaseUnitId
                                               where uc.RefUnitId == issmenu.DefaultUnitId
                                               group issmenu by new
                                               {
                                                   issmenu.IssueRefId,
                                                   issmenu.MaterialRefId,
                                                   issmenu.DefaultUnitId,
                                                   issmenu.UnitRefId,
                                                   issmenu.UnitRefName,
                                                   issmenu.DefaultUnitName,
                                                   uc.Conversion
                                               } into g
                                               select new IssueDetailViewDto
                                               {
                                                   IssueRefId = g.Key.IssueRefId,
                                                   MaterialRefId = g.Key.MaterialRefId,
                                                   UnitRefId = g.Key.UnitRefId,
                                                   UnitRefName = g.Key.UnitRefName,
                                                   DefaultUnitId = g.Key.DefaultUnitId,
                                                   DefaultUnitName = g.Key.DefaultUnitName,
                                                   IssueQty = g.Sum(t => t.IssueQty * g.Key.Conversion),
                                               }).ToList().OrderBy(t => t.MaterialRefId);

                var matlistWithSumGroup = issueMaterialUc.Union(returnMaterialDetailsUc).Union(yieldissueMaterialDetailsUc).ToList();

                var mergedmatList = matlistWithSumGroup.Select(t => t.MaterialRefId).Distinct().ToList();

                if (mergedmatList.Count > 0)
                {
                    var matView = await GetMaterialRateView(new GetHouseReportMaterialRateInput
                    {
                        StartDate = fromDt,
                        EndDate = toDt,
                        Locations = locinput,
                        MaterialRefIds = mergedmatList,
                        FunctionCalledBy = "Get Production Unit Costing Report"
                    });

                    List<MaterialRateViewListDto> materialRate = matView.MaterialRateView;

                    List<MaterialIssueAndReturnWithPriceAndDateRange> groupMaterialDetails = new List<MaterialIssueAndReturnWithPriceAndDateRange>();
                    var rsMaterial = await _materialRepo.GetAllListAsync(t => mergedmatList.Contains(t.Id));
                    foreach (var matid in mergedmatList)
                    {
                        var matprice = materialRate.FirstOrDefault(t => t.MaterialRefId == matid);

                        var material = rsMaterial.FirstOrDefault(t => t.Id == matid);

                        var matissuegroup = issueMaterialUc.Where(t => t.MaterialRefId == matid).Sum(t => t.IssueQty);

                        var matyieldgroup = yieldissueMaterialDetailsUc.Where(t => t.MaterialRefId == matid).Sum(t => t.IssueQty);

                        var matreturngroup = returnMaterialDetailsUc.Where(t => t.MaterialRefId == matid).Sum(t => t.IssueQty);
                        decimal materialPrice = 0;
                        if (material.DefaultUnitId == material.IssueUnitId)
                        {
                            materialPrice = matprice.AvgRate;
                        }
                        else
                        {
                            var unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == material.DefaultUnitId && t.RefUnitId == material.IssueUnitId && t.MaterialRefId == matid);
                            if (unitConversion == null)
                                unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == material.DefaultUnitId && t.RefUnitId == material.IssueUnitId);
                            if (unitConversion == null)
                                throw new UserFriendlyException(L("UnitConversion") + " " + L("NotExist") + " - " + matprice.MaterialName);
                            materialPrice = 1 / unitConversion.Conversion * matprice.AvgRate;
                            matissuegroup = matissuegroup * unitConversion.Conversion;
                            matyieldgroup = matyieldgroup * unitConversion.Conversion;
                            matreturngroup = matreturngroup * unitConversion.Conversion;
                        }

                        MaterialIssueAndReturnWithPriceAndDateRange matDto = new MaterialIssueAndReturnWithPriceAndDateRange
                        {
                            MaterialRefId = material.Id,
                            MaterialRefName = material.MaterialName,
                            InvoiceDateRange = matprice.InvoiceDateRange,
                            AvgRate = materialPrice,
                            Uom = unitList.First(t => t.Id == material.IssueUnitId).Name,
                            IssueQty = matissuegroup + matyieldgroup,
                            ReturnQty = matreturngroup,
                            UsageQty = matissuegroup + matyieldgroup - matreturngroup,
                            NetAmount = Math.Round((matissuegroup - matreturngroup) * materialPrice, roundDecimals, MidpointRounding.AwayFromZero),
                            CostPercentage = 0
                        };
                        groupMaterialDetails.Add(matDto);
                    }
                    newDto.MaterialUsage = groupMaterialDetails;
                    newDto.MaterialCost = groupMaterialDetails.Sum(t => t.NetAmount);
                }
                else
                {
                    newDto.MaterialUsage = new List<MaterialIssueAndReturnWithPriceAndDateRange>();
                    newDto.MaterialCost = 0;
                }

                outputRecipe.Add(newDto);
            }

            returnoutput.ProductionWiseCosting = outputRecipe;
            returnoutput.Remarks = remarks;

            return returnoutput;
        }

        public async Task<FileDto> GetProductionUnitCostingReportExcel(ProductionUnitCostingDtoConsolidated input)
        {
            var allList = input;
            FileDto fileExcel = _housereportExporter.ProductionUnitCostingReportExcel(allList);
            return fileExcel;
        }

        public async Task<List<MaterialIngredientRequiredDto>> GetRawMaterialRequiredForRecipe(GetRawMaterailBasedOnRecipeInput input)
        {
            List<MaterialIngredientRequiredDto> tempingrDetail = new List<MaterialIngredientRequiredDto>();
            List<MaterialIngredientRequiredDto> outputDetail = new List<MaterialIngredientRequiredDto>();

            MaterialRecipeTypes recipedetail = null;
            if (input.MaterialRecipeType != null)
                recipedetail = input.MaterialRecipeType;
            else
                recipedetail = await _materialRecipeTypeRepo.FirstOrDefaultAsync(t => t.MaterailRefId == input.RecipeRefId);

            if (recipedetail == null)
            {
                var mat = await _materialRepo.FirstOrDefaultAsync(t => t.Id == input.RecipeRefId);
                throw new UserFriendlyException(L("RequiredIngredientForRecipe", mat.MaterialName));
            }

            if (recipedetail.PrdBatchQty == 0)
            {
                recipedetail.PrdBatchQty = 1;
            }

            //var outputlist = await _materialingredientRepo.GetAll().Where(t => t.RecipeRefId == input.RecipeRefId).OrderBy(t => t.UserSerialNumber).ToListAsync();
            List<MaterialIngredient> outputlist = new List<MaterialIngredient>();
            if (input.MaterialIngredients != null && input.MaterialIngredients.Any())
            {
                outputlist = input.MaterialIngredients;
            }
            else
            {
                outputlist = await _materialingredientRepo.GetAll().Where(t => t.RecipeRefId == input.RecipeRefId).OrderBy(t => t.UserSerialNumber).ToListAsync();
            }

            //var rsMaterial = await _materialRepo.GetAllListAsync();
            List<Material> rsMaterials = new List<Material>();
            if (input.Materials != null && input.Materials.Any())
            {
                rsMaterials = input.Materials;
            }
            else
            {
                rsMaterials = await _materialRepo.GetAllListAsync();
            }

            foreach (var a in outputlist)
            {
                var mat = rsMaterials.FirstOrDefault(t => t.Id == a.MaterialRefId);

                if (mat == null)
                {
                    mat = await _materialRepo.FirstOrDefaultAsync(t => t.Id == a.MaterialRefId);
                    if (mat == null)
                        continue;
                    rsMaterials.Add(mat);
                    //throw new UserFriendlyException(L("Material") + " " + L("NotExist") + " " + L("Id") + " " + newmm.MaterialRefId);
                }

                MaterialIngredientRequiredDto newmm = new MaterialIngredientRequiredDto();

                newmm.RecipeRefId = a.RecipeRefId;
                newmm.MaterialRefId = a.MaterialRefId;
                newmm.MaterialUsedQty = Math.Round(a.MaterialUsedQty / recipedetail.PrdBatchQty * input.OutputQty, 14, MidpointRounding.AwayFromZero);
                //@@Pending
                //if (a.VariationflagForProduction == true)
                //    newmm.MaterialUsedQty = a.MaterialUsedQty / recipedetail.PrdBatchQty * input.OutputQty;
                //else
                //    newmm.MaterialUsedQty = a.MaterialUsedQty;

                newmm.VariationflagForProduction = a.VariationflagForProduction;
                newmm.UserSerialNumber = a.UserSerialNumber;
                newmm.UnitRefId = a.UnitRefId;

                newmm.MaterialRefName = mat.MaterialName;
                newmm.DefaultUnitId = mat.DefaultUnitId;
                newmm.UnitRefId = a.UnitRefId;
                tempingrDetail.Add(newmm);
            }

            List<UnitConversionListDto> rsUc = new List<UnitConversionListDto>();
            if (input.UnitConversionLists != null && input.UnitConversionLists.Any())
            {
                rsUc = input.UnitConversionLists;
            }
            else
            {
                rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();
            }

            //var arringrMaterialRefIds = tempingrDetail.Select(t => t.MaterialRefId).ToList();
            //var nonExistInMaterials = rsMaterials.Select(t => t.Id).Except(arringrMaterialRefIds).ToList();
            //if (nonExistInMaterials.Count > 0)
            //{
            //    var materialRevised = await _materialRepo.GetAllListAsync(t => nonExistInMaterials.Contains(t.Id));
            //    rsMaterials.AddRange(materialRevised);
            //}
            var nonrawListWithUc = (from salemenu in tempingrDetail
                                    join mat in rsMaterials.Where(t => t.MaterialTypeId != (int)MaterialType.RAW)
                                    on salemenu.MaterialRefId equals mat.Id
                                    join uc in rsUc
                                    on salemenu.UnitRefId equals uc.BaseUnitId
                                    where uc.RefUnitId == salemenu.DefaultUnitId
                                    group salemenu by new
                                    {
                                        salemenu.MaterialRefId,
                                        salemenu.MaterialRefName,
                                        salemenu.DefaultUnitId,
                                        salemenu.UnitRefId,
                                        uc.Conversion
                                    } into g
                                    select new MaterialWithQty
                                    {
                                        MaterialRefId = g.Key.MaterialRefId,
                                        UnitRefId = g.Key.UnitRefId,
                                        DefaultUnitId = g.Key.DefaultUnitId,
                                        UsedQty = g.Sum(t => t.MaterialUsedQty * g.Key.Conversion),
                                    }).ToList().OrderBy(t => t.MaterialRefId);

            var nonrawList = (from salemenu in nonrawListWithUc
                              group salemenu by new
                              {
                                  salemenu.MaterialRefId,
                                  salemenu.DefaultUnitId,
                                  salemenu.UnitRefId
                              } into g
                              select new MaterialWithQty
                              {
                                  MaterialRefId = g.Key.MaterialRefId,
                                  DefaultUnitId = g.Key.DefaultUnitId,
                                  UnitRefId = g.Key.UnitRefId,
                                  UsedQty = g.Sum(t => t.UsedQty)
                              }).ToList();

            var rawListWithUc = (from salemenu in tempingrDetail
                                 join mat in rsMaterials.Where(t => t.MaterialTypeId == (int)MaterialType.RAW)
                                 on salemenu.MaterialRefId equals mat.Id
                                 join uc in rsUc
                                 on salemenu.UnitRefId equals uc.BaseUnitId
                                 where uc.RefUnitId == salemenu.DefaultUnitId
                                 group salemenu by new
                                 {
                                     salemenu.RecipeRefId,
                                     salemenu.MaterialRefId,
                                     salemenu.MaterialRefName,
                                     salemenu.DefaultUnitId,
                                     salemenu.UnitRefId,
                                     salemenu.VariationflagForProduction,
                                     uc.Conversion,
                                     salemenu.UserSerialNumber,
                                 } into g
                                 select new MaterialIngredientRequiredDto
                                 {
                                     RecipeRefId = g.Key.RecipeRefId,
                                     MaterialRefId = g.Key.MaterialRefId,
                                     UnitRefId = g.Key.UnitRefId,
                                     DefaultUnitId = g.Key.DefaultUnitId,
                                     VariationflagForProduction = g.Key.VariationflagForProduction,
                                     UserSerialNumber = g.Key.UserSerialNumber,
                                     MaterialUsedQty = g.Sum(t => t.MaterialUsedQty * g.Key.Conversion),
                                 }).ToList().OrderBy(t => t.MaterialRefId);

            var rawlist = (from salemenu in rawListWithUc
                           group salemenu by new { salemenu.RecipeRefId, salemenu.MaterialRefId, salemenu.DefaultUnitId, salemenu.UserSerialNumber, salemenu.VariationflagForProduction } into g
                           select new MaterialIngredientRequiredDto
                           {
                               RecipeRefId = g.Key.RecipeRefId,
                               MaterialRefId = g.Key.MaterialRefId,
                               DefaultUnitId = g.Key.DefaultUnitId,
                               UserSerialNumber = g.Key.UserSerialNumber,
                               VariationflagForProduction = g.Key.VariationflagForProduction,
                               MaterialUsedQty = g.Sum(t => t.MaterialUsedQty)
                           }).ToList();

            outputDetail.AddRange(rawlist);

            //  If any of Ingredients itself as Recipe type, then we need to drill down to get the Material List

            if (nonrawList.Count > 0)
            {
                do
                {
                    tempingrDetail = new List<MaterialIngredientRequiredDto>();

                    foreach (var recipesubitem in nonrawList)
                    {
                        recipedetail = await _materialRecipeTypeRepo.FirstOrDefaultAsync(t => t.MaterailRefId == recipesubitem.MaterialRefId);
                        if (recipedetail == null)
                        {
                            var mat = rsMaterials.FirstOrDefault(t => t.Id == recipesubitem.MaterialRefId);

                            throw new UserFriendlyException(L("RequiredIngredientForRecipe", mat.MaterialName));
                        }

                        outputlist = await _materialingredientRepo.GetAll().Where(t => t.RecipeRefId == recipesubitem.MaterialRefId).OrderBy(t => t.UserSerialNumber).ToListAsync();

                        foreach (var a in outputlist)
                        {
                            MaterialIngredientRequiredDto newmm = new MaterialIngredientRequiredDto();

                            newmm.RecipeRefId = a.RecipeRefId;
                            newmm.MaterialRefId = a.MaterialRefId;
                            if (recipedetail.PrdBatchQty != 0)
                                newmm.MaterialUsedQty = a.MaterialUsedQty / recipedetail.PrdBatchQty * recipesubitem.UsedQty;
                            else
                                continue;

                            //@@Pending
                            //if (a.VariationflagForProduction == true)
                            //    newmm.MaterialUsedQty = a.MaterialUsedQty / recipedetail.PrdBatchQty * recipesubitem.UsedQty;
                            //else
                            //    newmm.MaterialUsedQty = a.MaterialUsedQty;

                            newmm.VariationflagForProduction = a.VariationflagForProduction;
                            newmm.UserSerialNumber = a.UserSerialNumber;

                            var mat = rsMaterials.FirstOrDefault(t => t.Id == a.MaterialRefId);

                            if (mat == null)
                            {
                                mat = await _materialRepo.FirstOrDefaultAsync(t => t.Id == a.MaterialRefId);
                                if (mat == null)
                                {
                                    continue;
                                }
                                rsMaterials.Add(mat);
                                //throw new UserFriendlyException(L("Material") + " " + L("NotExist") + " " + L("Id") + " " + newmm.MaterialRefId);
                            }
                            newmm.MaterialRefName = mat.MaterialName;
                            newmm.DefaultUnitId = mat.DefaultUnitId;
                            newmm.UnitRefId = a.UnitRefId;
                            tempingrDetail.Add(newmm);
                        }
                    }

                    nonrawListWithUc = (from salemenu in tempingrDetail
                                        join mat in rsMaterials.Where(t => t.MaterialTypeId != (int)MaterialType.RAW)
                                        on salemenu.MaterialRefId equals mat.Id
                                        join uc in rsUc
                                        on salemenu.UnitRefId equals uc.BaseUnitId
                                        where uc.RefUnitId == salemenu.DefaultUnitId
                                        group salemenu by new
                                        {
                                            salemenu.MaterialRefId,
                                            salemenu.MaterialRefName,
                                            salemenu.DefaultUnitId,
                                            salemenu.UnitRefId,
                                            uc.Conversion
                                        } into g
                                        select new MaterialWithQty
                                        {
                                            MaterialRefId = g.Key.MaterialRefId,
                                            UnitRefId = g.Key.UnitRefId,
                                            DefaultUnitId = g.Key.DefaultUnitId,
                                            UsedQty = g.Sum(t => t.MaterialUsedQty * g.Key.Conversion),
                                        }).ToList().OrderBy(t => t.MaterialRefId);

                    nonrawList = (from salemenu in nonrawListWithUc
                                  group salemenu by new { salemenu.MaterialRefId, salemenu.DefaultUnitId } into g
                                  select new MaterialWithQty
                                  {
                                      MaterialRefId = g.Key.MaterialRefId,
                                      DefaultUnitId = g.Key.DefaultUnitId,
                                      UsedQty = g.Sum(t => t.UsedQty)
                                  }).ToList();

                    rawListWithUc = (from salemenu in tempingrDetail
                                     join mat in rsMaterials.Where(t => t.MaterialTypeId == (int)MaterialType.RAW)
                                     on salemenu.MaterialRefId equals mat.Id
                                     join uc in rsUc
                                     on salemenu.UnitRefId equals uc.BaseUnitId
                                     where uc.RefUnitId == salemenu.DefaultUnitId
                                     group salemenu by new
                                     {
                                         salemenu.RecipeRefId,
                                         salemenu.MaterialRefId,
                                         salemenu.MaterialRefName,
                                         salemenu.DefaultUnitId,
                                         salemenu.UnitRefId,
                                         salemenu.VariationflagForProduction,
                                         uc.Conversion,
                                         salemenu.UserSerialNumber,
                                     } into g
                                     select new MaterialIngredientRequiredDto
                                     {
                                         RecipeRefId = g.Key.RecipeRefId,
                                         MaterialRefId = g.Key.MaterialRefId,
                                         UnitRefId = g.Key.UnitRefId,
                                         DefaultUnitId = g.Key.DefaultUnitId,
                                         VariationflagForProduction = g.Key.VariationflagForProduction,
                                         UserSerialNumber = g.Key.UserSerialNumber,
                                         MaterialUsedQty = g.Sum(t => t.MaterialUsedQty * g.Key.Conversion),
                                     }).ToList().OrderBy(t => t.MaterialRefId);

                    rawlist = (from salemenu in rawListWithUc
                               group salemenu by new { salemenu.RecipeRefId, salemenu.MaterialRefId, salemenu.DefaultUnitId, salemenu.UserSerialNumber, salemenu.VariationflagForProduction } into g
                               select new MaterialIngredientRequiredDto
                               {
                                   RecipeRefId = g.Key.RecipeRefId,
                                   MaterialRefId = g.Key.MaterialRefId,
                                   DefaultUnitId = g.Key.DefaultUnitId,
                                   UserSerialNumber = g.Key.UserSerialNumber,
                                   VariationflagForProduction = g.Key.VariationflagForProduction,
                                   MaterialUsedQty = g.Sum(t => t.MaterialUsedQty)
                               }).ToList();

                    outputDetail.AddRange(rawlist);
                } while (nonrawList.Count > 0);
            }

            var returnoutput = (from matLst in outputDetail
                                group matLst by new { matLst.MaterialRefId, matLst.DefaultUnitId } into g
                                select new MaterialIngredientRequiredDto
                                {
                                    MaterialRefId = g.Key.MaterialRefId,
                                    DefaultUnitId = g.Key.DefaultUnitId,
                                    UnitRefId = g.Key.DefaultUnitId,
                                    MaterialUsedQty = g.Sum(t => t.MaterialUsedQty)
                                }).ToList();

            foreach (var lst in returnoutput)
            {
                var mat = rsMaterials.FirstOrDefault(t => t.Id == lst.MaterialRefId);
                lst.MaterialRefName = mat.MaterialName;
            }

            return returnoutput;
        }

        public async Task<HouseDashBoardOutputDto> GetHouseDashboardReport(GetHouseDashboardInput input)
        {
            List<SalesAndExpensesDto> output = new List<SalesAndExpensesDto>();

            SalesAndExpensesDto allLocDto = new SalesAndExpensesDto();

            int drillDownLevel = input.DrillDownLevel <= 1 ? 2 : input.DrillDownLevel;

            TimeSpan difference = input.EndDate.Date - input.StartDate.Date;
            int diffInDays = (int)difference.TotalDays + 1;

            string DateRangeRemarks = "";
            if (diffInDays == 7)
                DateRangeRemarks = L("Weekly");
            else if (input.StartDate.Day == 1 && input.EndDate.Date == input.StartDate.Date.AddMonths(1).AddDays(-1))
            {
                DateRangeRemarks = L("Monthly");
            }
            else if (input.StartDate.Day == 1 && input.EndDate.Day == DateTime.Today.Day)
            {
                DateRangeRemarks = L("ThisMonth");
            }
            else
                DateRangeRemarks = diffInDays + L("Days");

            DateTime fromDate = input.StartDate;
            DateTime toDate = input.EndDate;

            for (int i = 1; i <= drillDownLevel; i++)
            {
                allLocDto = await GetPurchaseVsSalesReport(new GetHouseDashboardInput
                {
                    StartDate = fromDate,
                    EndDate = toDate,
                    Locations = input.Locations,
                    Suppliers = input.Suppliers,
                    AllLocationFlag = input.AllLocationFlag,
                    AllSupplierFlag = input.AllSupplierFlag,
                });
                if (DateRangeRemarks.Equals(L("Monthly")))
                {
                    DateTime tempDate = fromDate.AddMonths(-1);
                    fromDate = new DateTime(tempDate.Year, tempDate.Month, 1);
                    toDate = fromDate.AddMonths(1).AddDays(-1);
                }
                else if (DateRangeRemarks.Equals(L("ThisMonth")))
                {
                    DateTime tempDate = fromDate.AddMonths(-1);
                    fromDate = new DateTime(tempDate.Year, tempDate.Month, 1);
                    toDate = fromDate.AddDays(diffInDays - 1);
                }
                else
                {
                    fromDate = fromDate.AddDays(-1 * diffInDays);
                    toDate = toDate.AddDays(-1 * diffInDays);
                }

                output.Add(allLocDto);
            }

            int recCount = output.Count - 1;
            for (int i = recCount; i >= 0; i--)
            {
                if (i == recCount)
                    continue;

                decimal salesDifference = output[i].Sales - output[i + 1].Sales;
                decimal salesDiffPercentage = 0;
                if (output[i + 1].Sales > 0)
                    salesDiffPercentage = salesDifference / output[i + 1].Sales * 100;

                decimal purchaseDifference = output[i].Purchases - output[i + 1].Purchases;
                decimal purchaseDiffPercentage = 0;
                if (output[i + 1].Purchases > 0)
                    purchaseDiffPercentage = purchaseDifference / output[i + 1].Purchases * 100;

                output[i].SalesDifferent = salesDifference;
                output[i].DiffSalesPercentage = salesDiffPercentage;

                output[i].PurchasesDifferent = purchaseDifference;
                output[i].DiffPurchasesPercentage = purchaseDiffPercentage;

                decimal foodCostPercentage = 0;
                if (output[i].Purchases > 0 && output[i].Sales > 0)
                    foodCostPercentage = output[i].Purchases / output[i].Sales * 100;
                output[i].FoodCostPercentage = foodCostPercentage;
            }

            return new HouseDashBoardOutputDto
            {
                SalesAndExpenses = output,
                DateRemarks = DateRangeRemarks
            };
        }

        public async Task<SalesAndExpensesDto> GetPurchaseVsSalesReport(GetHouseDashboardInput input)
        {
            decimal totalSalesWithoutTax;

            if (input.Locations == null)
            {
                throw new UserFriendlyException("LocationErr");
            }
            List<LocationListDto> locinput = input.Locations;
            DateTime fromDt = input.StartDate.Date;
            DateTime toDt = input.EndDate.Date;

            //  Get Sales
            var salesOut = await _connectReportAppService.GetTickets(
                new GetTicketInput
                {
                    StartDate = fromDt,
                    EndDate = toDt,
                    Locations = locinput.MapTo<List<SimpleLocationDto>>()
                }, true);

            totalSalesWithoutTax = salesOut.DashBoardDto.TotalAmount;

            int[] locIds = input.Locations.Select(t => t.Id).ToArray();
            int[] supIds = input.Suppliers.Select(t => t.Id).ToArray();

            var rsInvoiceMaster = _invoiceRepo.GetAll().Where(t => t.InvoiceDate >= input.StartDate && t.InvoiceDate <= input.EndDate);
            rsInvoiceMaster = rsInvoiceMaster.WhereIf(input.AllLocationFlag == false, t => locIds.Contains(t.LocationRefId));
            rsInvoiceMaster = rsInvoiceMaster.WhereIf(input.AllSupplierFlag == false, t => supIds.Contains(t.SupplierRefId));

            var rsInvoiceDetail = _invoiceDetailRepo.GetAll();

            var supplierWiseSales = await (from invMas in rsInvoiceMaster
                                           join invDet in rsInvoiceDetail on invMas.Id equals invDet.InvoiceRefId
                                           join sup in _supplierRepo.GetAll() on invMas.SupplierRefId equals sup.Id
                                           group invMas by new { invMas.SupplierRefId, sup.SupplierName } into g
                                           select new SupplierWisePurchase
                                           {
                                               SupplierRefId = g.Key.SupplierRefId,
                                               SupplierRefName = g.Key.SupplierName,
                                               Purchase = g.Sum(t => t.InvoiceAmount)
                                           }).ToListAsync();

            var materialWiseSales = await (from invMas in rsInvoiceMaster
                                           join invDet in rsInvoiceDetail on invMas.Id equals invDet.InvoiceRefId
                                           join mat in _materialRepo.GetAll() on invDet.MaterialRefId equals mat.Id
                                           group invDet by new { invDet.MaterialRefId, mat.MaterialName } into g
                                           select new MaterialWisePurchase
                                           {
                                               MaterialRefId = g.Key.MaterialRefId,
                                               MaterialRefName = g.Key.MaterialName,
                                               Quantity = g.Sum(t => t.TotalQty),
                                               Purchase = g.Sum(t => t.NetAmount),
                                           }).ToListAsync();

            SalesAndExpensesDto output = new SalesAndExpensesDto();
            if (input.StartDate.Date == input.EndDate.Date)
                output.DateRange = input.StartDate.ToString("dd-MMM-yy");
            else
                output.DateRange = input.StartDate.ToString("dd-MMM") + " - " + input.EndDate.ToString("dd-MMM-yy");

            output.Sales = totalSalesWithoutTax;
            output.Purchases = supplierWiseSales.Sum(t => t.Purchase);
            output.SupplierWisePurchases = supplierWiseSales.OrderByDescending(t => t.Purchase).ToList();
            output.MaterialWisePurchases = materialWiseSales.OrderByDescending(t => t.Purchase).ToList();
            output.CurrentLevel = input.CurrentLevel;

            decimal foodCostPercentage = 0;
            if (output.Sales > 0 && output.Purchases > 0)
                foodCostPercentage = output.Purchases / output.Sales * 100;
            output.FoodCostPercentage = foodCostPercentage;

            return output;
        }

        public async Task<HouseDashBoardOutputDto> GetHouseDashboardArguments(GetHouseDashboardInput input)
        {
            List<SalesAndExpensesDto> output = new List<SalesAndExpensesDto>();

            SalesAndExpensesDto allLocDto = new SalesAndExpensesDto();

            int drillDownLevel = input.DrillDownLevel <= 1 ? 2 : input.DrillDownLevel;

            TimeSpan difference = input.EndDate.Date - input.StartDate.Date;
            int diffInDays = (int)difference.TotalDays + 1;

            string DateRangeRemarks = "";
            if (diffInDays == 7)
                DateRangeRemarks = L("Weekly");
            else if (input.StartDate.Day == 1 && input.EndDate.Date == input.StartDate.Date.AddMonths(1).AddDays(-1))
            {
                DateRangeRemarks = L("Monthly");
            }
            else if (input.StartDate.Day == 1 && input.EndDate.Day == DateTime.Today.Day)
            {
                DateRangeRemarks = L("ThisMonth");
            }
            else
                DateRangeRemarks = diffInDays + L("Days");

            DateTime fromDate = input.StartDate;
            DateTime toDate = input.EndDate;

            for (int i = 1; i <= drillDownLevel; i++)
            {
                allLocDto = await GetPurchaseVsSalesReport(new GetHouseDashboardInput
                {
                    StartDate = fromDate,
                    EndDate = toDate,
                    Locations = input.Locations,
                    Suppliers = input.Suppliers,
                    AllLocationFlag = input.AllLocationFlag,
                    AllSupplierFlag = input.AllSupplierFlag,
                });
                if (DateRangeRemarks.Equals(L("Monthly")))
                {
                    DateTime tempDate = fromDate.AddMonths(-1);
                    fromDate = new DateTime(tempDate.Year, tempDate.Month, 1);
                    toDate = fromDate.AddMonths(1).AddDays(-1);
                }
                else if (DateRangeRemarks.Equals(L("ThisMonth")))
                {
                    DateTime tempDate = fromDate.AddMonths(-1);
                    fromDate = new DateTime(tempDate.Year, tempDate.Month, 1);
                    toDate = fromDate.AddDays(diffInDays - 1);
                }
                else
                {
                    fromDate = fromDate.AddDays(-1 * diffInDays);
                    toDate = toDate.AddDays(-1 * diffInDays);
                }

                output.Add(allLocDto);
            }

            int recCount = output.Count - 1;
            for (int i = recCount; i >= 0; i--)
            {
                if (i == recCount)
                    continue;

                decimal salesDifference = output[i].Sales - output[i + 1].Sales;
                decimal salesDiffPercentage = 0;
                if (output[i + 1].Sales > 0)
                    salesDiffPercentage = salesDifference / output[i + 1].Sales * 100;

                decimal purchaseDifference = output[i].Purchases - output[i + 1].Purchases;
                decimal purchaseDiffPercentage = 0;
                if (output[i + 1].Purchases > 0)
                    purchaseDiffPercentage = purchaseDifference / output[i + 1].Purchases * 100;

                output[i].SalesDifferent = salesDifference;
                output[i].DiffSalesPercentage = salesDiffPercentage;

                output[i].PurchasesDifferent = purchaseDifference;
                output[i].DiffPurchasesPercentage = purchaseDiffPercentage;
            }

            return new HouseDashBoardOutputDto
            {
                SalesAndExpenses = output,
                DateRemarks = DateRangeRemarks
            };
        }

        public async Task<FileDto> GetExcelOfMenuCost(MenuItemCostingDtoConsolidated input)
        {
            FileDto fileExcel = _housereportExporter.MenuCostToExcelFile(input);
            return fileExcel;
        }

        public async Task<FileDto> GetExcelOfCostOfSales(CostOfSalesReport input)
        {
            FileDto fileExcel = _housereportExporter.CostOfSalesToExcelFile(input);
            return fileExcel;
        }

        public async Task<FileDto> GetExcelOfProductAnalysis(GetProductAnalysisViewDtoOutput input)
        {
            FileDto fileExcel = await _housereportExporter.ProductAnalysisToExcelFile(input);
            //FileDto d = fileExcel.MapTo<FileDto>();
            return fileExcel;
        }

        public async Task<List<VarianceReportDto>> GetVarianceReportAll(GetMaterialLedgerInput input)
        {
            string MaterialName = "";
            int? MaterialRefId = null;
            if (input.MaterialRefId != null)
            {
                var mat = await _materialRepo.FirstOrDefaultAsync(t => t.Id == input.MaterialRefId);
                MaterialName = mat.MaterialName;
                MaterialRefId = mat.Id;
            }

            GetHouseReportInput rdto = new GetHouseReportInput
            {
                StartDate = input.StartDate,
                EndDate = input.EndDate,
                LocationRefId = input.LocationRefId.Value,
                MaterialName = MaterialName,
                MaterialRefId = MaterialRefId
            };

            var matLedgerDtos = await GetStockSummary(rdto);

            List<MaterialVarianceReportViewDto> output = matLedgerDtos.StockSummary.MapTo<List<MaterialVarianceReportViewDto>>();

            var loc = await _locationRepo.GetAll().Where(t => t.Id == input.LocationRefId).ToListAsync();
            List<LocationListDto> locinput = loc.MapTo<List<LocationListDto>>();

            var rsAdj = _adjustmentRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId && DbFunctions.TruncateTime(t.ClosingStockDate.Value) == DbFunctions.TruncateTime(input.EndDate)).WhereIf(!input.Filter.IsNullOrEmpty(), t => t.TokenRefNumber.ToString().Contains(input.Filter));

            int[] arrAdjustmentRefIds = rsAdj.Select(t => t.Id).ToArray();
            var rsAdjustmentClosingStock = await _adjustmentDetailRepo.GetAll().Where(t => arrAdjustmentRefIds.Contains(t.AdjustmentRefIf)).ToListAsync();
            var rsMaterial = await _materialRepo.GetAllListAsync();
            var rsMaterialCategory = await _materialGroupCategoryRepo.GetAllListAsync();
            var rsMaterialStockCycle = await _materialStockCycleRepo.GetAllListAsync();
            var rsInventoryCycleTags = await _inventoryCycleTagRepo.GetAllListAsync();
            List<MaterialVarianceReportViewDto> outputDtos = new List<MaterialVarianceReportViewDto>();

            #region FilterBasedOnInventoryCycle

            DateTime lastDayOfMonth = input.EndDate.Date;
            DateTime temp = new DateTime(lastDayOfMonth.Year, lastDayOfMonth.Month, 1);
            lastDayOfMonth = temp.AddMonths(1).AddDays(-1);

            //	Filter based on Inventory Cycle
            foreach (var lst in output)
            {
                var mc = rsMaterialStockCycle.Where(t => t.MaterialRefId == lst.MaterialRefId).ToList();
                if (mc.Count == 0 || mc == null)
                {
                    outputDtos.Add(lst);
                }
                else
                {
                    foreach (var det in mc)
                    {
                        var sc = rsInventoryCycleTags.FirstOrDefault(t => t.Id == det.InventoryCycleTagRefId);
                        if (sc == null)
                        {
                            outputDtos.Add(lst);
                            break;
                        }
                        if (sc.InventoryCycleMode == (int)StockCycle.Daily)
                        {
                            outputDtos.Add(lst);
                            break;
                        }
                        else if (sc.InventoryCycleMode == (int)StockCycle.Weekly)
                        {
                            List<int> lstWeek = _xmlandjsonConvertorAppService.DeSerializeFromJSON<List<int>>(sc.InventoryModeSchedule);
                            int dow = (int)input.EndDate.DayOfWeek;
                            var exists = lstWeek.Exists(item => item == dow);
                            if (exists == true)
                            {
                                outputDtos.Add(lst);
                                break;
                            }
                        }
                        else if (sc.InventoryCycleMode == (int)StockCycle.Monthly)
                        {
                            List<int> lstWeek = _xmlandjsonConvertorAppService.DeSerializeFromJSON<List<int>>(sc.InventoryModeSchedule);
                            int dom = input.EndDate.Day;
                            var exists = lstWeek.Exists(item => item == dom);
                            if (exists == true)
                            {
                                outputDtos.Add(lst);
                                break;
                            }
                        }
                        else if (sc.InventoryCycleMode == (int)StockCycle.End_Of_Month)
                        {
                            if (lastDayOfMonth.Date == input.EndDate.Date)
                            {
                                outputDtos.Add(lst);
                                break;
                            }
                        }
                    }
                }
            }

            #endregion FilterBasedOnInventoryCycle

            if (outputDtos.Count == 0)
            {
                return new List<VarianceReportDto>();
            }

            List<int> materialFilter = outputDtos.Select(t => t.MaterialRefId).ToList();
            var matRateViewDtos = await GetMaterialRateView(new GetHouseReportMaterialRateInput
            {
                StartDate = input.StartDate.Date,
                EndDate = input.EndDate.Date,
                Locations = locinput,
                MaterialRefIds = materialFilter,
                FunctionCalledBy = "Get Variance Report All"
            });

            var rsUnits = await _unitrepo.GetAllListAsync();
            foreach (var lst in outputDtos)
            {
                var mat = rsMaterial.FirstOrDefault(t => t.Id == lst.MaterialRefId);
                lst.MaterialGroupCategoryRefId = mat.MaterialGroupCategoryRefId;
                lst.MaterialRefName = mat.MaterialName;
                lst.Uom = rsUnits.FirstOrDefault(t => t.Id == mat.DefaultUnitId).Name;

                var cat = rsMaterialCategory.FirstOrDefault(t => t.Id == lst.MaterialGroupCategoryRefId);
                lst.MaterialGroupCategoryRefName = cat.MaterialGroupCategoryName;

                var priceDto = matRateViewDtos.MaterialRateView.FirstOrDefault(t => t.MaterialRefId == lst.MaterialRefId);
                if (priceDto != null)
                {
                    lst.MaterialRateDto = priceDto;
                    lst.AvgRate = priceDto.AvgRate;
                }

                var closingStockDto = rsAdjustmentClosingStock.FirstOrDefault(t => t.MaterialRefId == lst.MaterialRefId);
                if (closingStockDto == null)
                {
                    lst.IsClosingStockEntered = false;
                    lst.ClosingStockEntered = 0;
                    lst.VarianceCost = 0;
                }
                else
                {
                    lst.IsClosingStockEntered = true;
                    lst.ClosingStockEntered = closingStockDto.StockEntry.Value;
                    lst.Variance = lst.ClBalance - lst.ClosingStockEntered;
                    lst.VarianceCost = lst.Variance * lst.AvgRate;
                }
            }

            var ro = (from r in outputDtos
                      group r by new { r.MaterialGroupCategoryRefId, r.MaterialGroupCategoryRefName } into g
                      select new VarianceReportDto
                      {
                          MaterialGroupCategoryRefId = g.Key.MaterialGroupCategoryRefId,
                          MaterialGroupCategoryRefName = g.Key.MaterialGroupCategoryRefName,
                          VarianceReport = g.ToList()
                      }).ToList();

            return ro;
        }

        public async Task<FileDto> GetVarianceAllToExcel(GetMaterialLedgerInput input)
        {
            var allList = await GetVarianceReportAll(input);
            string userName = "";
            var user = await UserManager.GetUserByIdAsync(AbpSession.UserId.Value);
            if (user != null)
                userName = user.Name;
            FileDto fileExcel = _housereportExporter.VarianceReportToExcelFile(allList, input.StartDate, input.EndDate, userName);
            return fileExcel;
        }

        public async Task<OverAllMaterialWastageReport> GetMaterailWastageAll(InputAdjustmentReport input)
        {
            var locs = await _locationRepo.GetAll().ToListAsync();
            if (input.ToLocations.Count > 0)
            {
                var givenLocsIds = input.ToLocations.Select(t => t.Id).ToArray();
                locs = locs.Where((t => givenLocsIds.Contains(t.Id))).ToList();
            }
            var arrLocationRefIds = locs.Select(t => t.Id).ToArray();

            var userList = UserManager.Users.ToList();

            List<LocationListDto> locinput = locs.MapTo<List<LocationListDto>>();
            input.ToLocations = locinput;
            input.LocationInfo = locinput.FirstOrDefault();
            input.PreparedUserId = AbpSession.UserId;
            var user = userList.FirstOrDefault(t => t.Id == AbpSession.UserId.Value);
            string userName = "";
            if (user != null)
                userName = user.Name;
            input.PreparedUserName = userName;
            input.PrintedTime = DateTime.Now;
            var customReport = await FeatureChecker.GetValueAsync(AppFeatures.Custom);
            bool customCrgReport = false;
            if (customReport.ToUpper() == "TRUE")
                customCrgReport = true;
            input.CustomReportFlag = customCrgReport;

            string DamageString = L("Damaged");
            string WastageString = L("Wastage");
            string ShortageString = L("Shortage");

            var output = await (from mas in _adjustmentRepo.GetAll()
                            .Where(t => DbFunctions.TruncateTime(t.AdjustmentDate) >= DbFunctions.TruncateTime(input.StartDate.Value) && DbFunctions.TruncateTime(t.AdjustmentDate) <= DbFunctions.TruncateTime(input.EndDate.Value) && arrLocationRefIds.Contains(t.LocationRefId))
                                join det in _adjustmentDetailRepo.GetAll().Where(t => t.AdjustmentMode.Equals(DamageString) || t.AdjustmentMode.Equals(ShortageString) || t.AdjustmentMode.Equals(WastageString)) on mas.Id equals det.AdjustmentRefIf
                                join loc in _locationRepo.GetAll() on mas.LocationRefId equals loc.Id
                                join mat in _materialRepo.GetAll() on det.MaterialRefId equals mat.Id
                                join unit in _unitrepo.GetAll() on det.UnitRefId equals unit.Id
                                select new MaterialWastageReportDto
                                {
                                    LocationRefId = mas.LocationRefId,
                                    LocationRefName = loc.Name,
                                    MaterialRefId = det.MaterialRefId,
                                    MaterialPetName = mat.MaterialPetName,
                                    MaterialRefName = mat.MaterialName,
                                    TotalQty = Math.Round(det.AdjustmentQty, 14),
                                    AdjustmentDate = mas.AdjustmentDate,
                                    UnitRefId = det.UnitRefId,
                                    UnitRefName = unit.Name,
                                    UserId = mas.CreatorUserId.Value,
                                    DefaultUnitName = unit.Name,
                                    TokenRefNumber = mas.TokenRefNumber,
                                    AdjustmentMode = det.AdjustmentMode,
                                    Remarks = det.AdjustmentApprovedRemarks
                                }
                                ).ToListAsync();

            var rsUnit = await _unitrepo.GetAllListAsync();
            var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();
            var rsMaterialCategory = await _materialGroupCategoryRepo.GetAllListAsync();
            var rsMaterial = await _materialRepo.GetAllListAsync();

            if (!output.Any())
                return null;
            var materialFilter = output.Select(t => t.MaterialRefId).ToList();
            var matRateViewDtos = await GetMaterialRateView(new GetHouseReportMaterialRateInput
            {
                StartDate = input.StartDate.Value.Date,
                EndDate = input.EndDate.Value.Date,
                Locations = locinput,
                MaterialRefIds = materialFilter,
                FunctionCalledBy = "Get Material Wastage Summary"
            });

            foreach (var lst in output)
            {
                var mat = rsMaterial.FirstOrDefault(t => t.Id == lst.MaterialRefId);
                lst.MaterialGroupCategoryRefId = mat.MaterialGroupCategoryRefId;

                var cat = rsMaterialCategory.FirstOrDefault(t => t.Id == lst.MaterialGroupCategoryRefId);
                lst.MaterialGroupCategoryRefName = cat.MaterialGroupCategoryName;
                lst.DefaultUnitId = mat.DefaultUnitId;
                lst.DefaultUnitName = rsUnit.FirstOrDefault(t => t.Id == mat.DefaultUnitId).Name;

                if (mat.DefaultUnitId == lst.UnitRefId)
                    lst.QuantityInPurchaseUnit = lst.TotalQty;          //  Purchase Unit
                else if (mat.IssueUnitId == lst.UnitRefId)
                    lst.QuantityInIssueUnit = lst.TotalQty;             // Recipe Unit
                else if (mat.StockAdjustmentUnitId == lst.UnitRefId)
                    lst.QuantityInStkAdjustedInventoryUnit = lst.TotalQty;       // Inventory Unit
                else
                    lst.QuantityInIssueUnit = lst.TotalQty;             //  Except Default and Stock adj unit all units are as Recipe Unit

                decimal conversion = 1;
                if (mat.DefaultUnitId == lst.UnitRefId)
                    conversion = 1;
                else
                {
                    var un = rsUc.FirstOrDefault(t => t.BaseUnitId == mat.DefaultUnitId && t.RefUnitId == lst.UnitRefId && t.MaterialRefId == lst.MaterialRefId);
                    if (un == null)
                        un = rsUc.FirstOrDefault(t => t.BaseUnitId == mat.DefaultUnitId && t.RefUnitId == lst.UnitRefId);
                    conversion = 1 / un.Conversion;
                }
                var priceDto = matRateViewDtos.MaterialRateView.FirstOrDefault(t => t.MaterialRefId == lst.MaterialRefId);
                if (priceDto != null)
                {
                    decimal unitPrice = Math.Round(priceDto.AvgRate * conversion, 14, MidpointRounding.AwayFromZero);
                    decimal totalAmount = unitPrice * lst.TotalQty;
                    lst.Price = Math.Round(unitPrice, 14);
                    lst.TotalAmount = Math.Round(totalAmount, 14, MidpointRounding.AwayFromZero);
                    lst.TotalAmount = Math.Round(lst.TotalAmount, 4);
                }

                user = userList.FirstOrDefault(t => t.Id == lst.UserId);
                if (user != null)
                    lst.UserName = user.Name;

                if (lst.TokenRefNumber == 444)
                    lst.MenuWastageMode = L("MenuWastage");
                else if (lst.TokenRefNumber == 555)
                    lst.MenuWastageMode = L("Comp");
                else if (lst.TokenRefNumber == 777)
                    lst.MenuWastageMode = L("StockTaken");
                else if (lst.TokenRefNumber == 888)
                    lst.MenuWastageMode = L("ClosingStockEntry");
                else if (lst.TokenRefNumber == 999)
                    lst.MenuWastageMode = L("DayClose");
                else
                    lst.MenuWastageMode = L("Manual");
            }

            if (input.EntryTypes.Count > 0)
            {
                string[] selected = input.EntryTypes.ToList().Select(t => t.DisplayText).ToArray();
                output = output.Where(t => selected.Contains(t.MenuWastageMode)).ToList();
            }

            OverAllMaterialWastageReport reportOutput = new OverAllMaterialWastageReport();
            reportOutput.WastageReport = output;
            reportOutput.OverAllTotalAmount = reportOutput.WastageReport.Sum(t => t.TotalAmount);

            //Adj_444 ,	//	Menu Wastage - Entry
            //Adj_555,	//	Wastage While Billing - Comp Wastages
            //Adj_666,	//	OnHand  Initial Setup - Through Import / Enter Manually
            //Adj_777,	//	Read From Excel Based Stock On Given Date
            //Adj_888,	//	Physical Stock Taken Differences, Closing Stock Entry - Specially made for FSB
            //Adj_999,	//	Day Close Wastage Excess , Wastage - POST AUTO ADJUSTMENT , WIPE OUT STOCK ON DAYCLOSE Materials differences

            var overAllConsolidated = (from salemenu in output
                                       join uc in rsUc
                                       on salemenu.UnitRefId equals uc.BaseUnitId
                                       where uc.RefUnitId == salemenu.DefaultUnitId
                                       group salemenu by new
                                       {
                                           salemenu.MaterialRefId,
                                           salemenu.MaterialRefName,
                                           salemenu.DefaultUnitId,
                                           salemenu.DefaultUnitName,
                                           uc.Conversion,
                                           salemenu.Price,
                                       } into g
                                       select new MaterialWastageReportDto
                                       {
                                           MaterialRefId = g.Key.MaterialRefId,
                                           MaterialRefName = g.Key.MaterialRefName,
                                           DefaultUnitId = g.Key.DefaultUnitId,
                                           DefaultUnitName = g.Key.DefaultUnitName,
                                           Price = g.Key.Price * 1 / g.Key.Conversion,
                                           TotalQty = g.Sum(t => t.TotalQty * g.Key.Conversion),
                                           TotalAmount = g.Sum(t => t.TotalAmount),
                                       }).ToList().OrderBy(t => t.MaterialRefId).ToList();

            reportOutput.OverAllWastageReport = overAllConsolidated.ToList();
            var ro = (from r in output
                      group r by new { r.LocationRefId, r.LocationRefName } into g
                      select new MaterialWastageConsolidatedReport
                      {
                          LocationRefId = g.Key.LocationRefId,
                          LocationRefName = g.Key.LocationRefName,
                          WastageDetailReport = g.ToList()
                      }).ToList();

            foreach (var lst in ro)
            {
                var consolidatedWastage = (from salemenu in lst.WastageDetailReport
                                           join uc in rsUc
                                           on salemenu.UnitRefId equals uc.BaseUnitId
                                           where uc.RefUnitId == salemenu.DefaultUnitId
                                           group salemenu by new
                                           {
                                               salemenu.MaterialRefId,
                                               salemenu.MaterialRefName,
                                               salemenu.DefaultUnitId,
                                               salemenu.DefaultUnitName,
                                               uc.Conversion,
                                               salemenu.Price,
                                           } into g
                                           select new MaterialWastageReportDto
                                           {
                                               MaterialRefId = g.Key.MaterialRefId,
                                               MaterialRefName = g.Key.MaterialRefName,
                                               DefaultUnitId = g.Key.DefaultUnitId,
                                               DefaultUnitName = g.Key.DefaultUnitName,
                                               Price = g.Key.Price * 1 / g.Key.Conversion,
                                               TotalQty = g.Sum(t => t.TotalQty * g.Key.Conversion),
                                               TotalAmount = g.Sum(t => t.TotalAmount),
                                           }).ToList().OrderBy(t => t.MaterialRefId).ToList();
                lst.ConsolidatedWastageReport = consolidatedWastage;
            }
            reportOutput.LocationWiseWastageList = ro;
            return reportOutput;
        }

        public async Task<List<FileDto>> GetMaterialWastageToExcel(InputAdjustmentReport input)
        {
            var allList = await GetMaterailWastageAll(input);
            if (allList == null)
                throw new UserFriendlyException("No More Records");
            string userName = "";
            var user = await UserManager.GetUserByIdAsync(AbpSession.UserId.Value);
            if (user != null)
                userName = user.Name;
            if (allList.OverAllWastageReport.Count > 0)
            {
                if (input.ExportType == 0)
                {
                    List<FileDto> fileExcel = await _housereportExporter.MaterialWastageReportToExcelFile(allList, input);
                    return fileExcel;
                }
                else
                {
                    List<FileDto> fileExcel = await _housereportExporter.MaterialWastageReportToExport(allList, input, userName);
                    return fileExcel;
                }
            }
            else
            {
                throw new UserFriendlyException(L("NoRecordsFound"));
            }
        }

        public async Task<List<ComboboxItemDto>> GetEntryTypes()
        {
            List<ComboboxItemDto> retList = new List<ComboboxItemDto>();
            retList.Add(new ComboboxItemDto { Value = L("1"), DisplayText = L("Manual") });
            retList.Add(new ComboboxItemDto { Value = L("444"), DisplayText = L("MenuWastage") });
            retList.Add(new ComboboxItemDto { Value = L("555"), DisplayText = L("Comp") });
            retList.Add(new ComboboxItemDto { Value = L("777"), DisplayText = L("StockTaken") });
            retList.Add(new ComboboxItemDto { Value = L("888"), DisplayText = L("ClosingStockEntry") });
            retList.Add(new ComboboxItemDto { Value = L("999"), DisplayText = L("DayClose") });
            return retList;
        }

        public async Task<MenuItemCostingDtoConsolidated> GetMenuCostingReportForGivenMenuPortionIds(GetCostReportInput input)
        {
            string remarks = "";
            MenuItemCostingDtoConsolidated returnoutput = new MenuItemCostingDtoConsolidated();
            if (input.Locations == null)
            {
                if (input.LocationRefId.HasValue)
                {
                    var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId.Value);
                    input.Locations = new List<LocationListDto>();
                    input.Locations.Add(loc.MapTo<LocationListDto>());
                }
                if (input.Locations == null || input.Locations.Count == 0)
                    throw new UserFriendlyException("LocationErr");
            }

            List<LocationListDto> locinput = input.Locations;
            int[] locationList = locinput.Select(t => t.Id).ToArray();

            DateTime fromDt = input.StartDate.Date;
            DateTime toDt = input.EndDate.Date;

            var rsMenuList = await (from menuportion in _menuitemportionRepo.GetAll()
                                    join menu in _menuitemRepo.GetAll()
                                    on menuportion.MenuItemId equals menu.Id
                                    select new ProductListDto
                                    {
                                        PortionId = menuportion.Id,
                                        PortionName = string.Concat(menu.Name, " - ", menuportion.Name)
                                    }).ToListAsync();

            List<ProductListDto> menuSoldList = new List<ProductListDto>();
            if (input.MenuItems != null && input.MenuItems.Count > 0)
            {
                string[] arrstringmenurefs = input.MenuItems.Select(t => t.Value).ToArray();
                int[] menuRefIds = arrstringmenurefs.Select(int.Parse).ToArray();
                menuSoldList = rsMenuList.Where(t => menuRefIds.Contains(t.PortionId)).OrderBy(t => t.PortionName).ToList();
            }
            else if (input.MenuPortionRefId.HasValue)
            {
                menuSoldList = rsMenuList.Where(t => t.PortionId == input.MenuPortionRefId.Value).OrderBy(t => t.PortionName).ToList();
            }

            if (menuSoldList.Count == 0)
            {
                returnoutput.RecipeWiseCosting = new List<MenuItemCostingDto>();
                return returnoutput;
            }

            var unitList = await _unitrepo.GetAllListAsync();
            var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();

            List<int> menuPortionIds = menuSoldList.Select(t => t.PortionId).ToList();

            var iqueryiableMenuMapping = _materialmenumappingRepo.GetAll();
            if (menuPortionIds.Any())
                iqueryiableMenuMapping = iqueryiableMenuMapping.Where(t => menuPortionIds.Contains(t.PosMenuPortionRefId));

            var rsMenuMappingMaterials = await iqueryiableMenuMapping.ToListAsync();

            var rsMenumappingDepartment = await _menumappingdepartmentRepo.GetAll().ToListAsync();

            var rsDeptList = await _departmentRepo.GetAll().ToListAsync();

            List<DepartmentListDto> AllDepartment = new List<DepartmentListDto>();
            AllDepartment.Add(new DepartmentListDto { Id = 0, Name = L("General") });
            AllDepartment.AddRange(rsDeptList.MapTo<List<DepartmentListDto>>());

            var rsMaterial = await _materialRepo.GetAllListAsync();

            List<MenuItemCostingDto> outputMenu = new List<MenuItemCostingDto>();

            //  Material Wise Price
            List<int> materialFilter = rsMenuMappingMaterials.Select(t => t.MaterialRefId).ToList();
            List<MaterialRateViewListDto> materialRate = new List<MaterialRateViewListDto>();
            if (input.MaterialRateView == null || input.MaterialRateView.Count == 0 || (materialRate.Count != materialFilter.Count))
            {
                var matView = await GetMaterialRateView(new GetHouseReportMaterialRateInput
                {
                    StartDate = fromDt,
                    EndDate = toDt,
                    Locations = locinput,
                    MaterialRefIds = materialFilter,
                    FunctionCalledBy = "Get Menu Costing Report ",
                    DoesIngredientsDetailsRequired = true
                });

                materialRate = matView.MaterialRateView;
            }
            else
            {
                materialRate = input.MaterialRateView;
            }

            foreach (var menu in menuSoldList)
            {
                {
                    foreach (var dept in AllDepartment)
                    {
                        decimal menuItemSoldQty = 1;

                        MenuItemCostingDto newDto = new MenuItemCostingDto();
                        newDto.DepartmentRefId = dept.Id;
                        newDto.DepartmentRefName = dept.Name;
                        newDto.MenuItemRefId = menu.PortionId;
                        newDto.MenuItemRefName = menu.PortionName;
                        newDto.QtySold = menuItemSoldQty;
                        newDto.SalesAmount = 0;

                        var menuMapping = rsMenuMappingMaterials.Where(t => t.PosMenuPortionRefId == menu.PortionId && t.LocationRefId == input.LocationRefId).ToList();
                        if (menuMapping == null || menuMapping.Count == 0)
                        {
                            menuMapping = rsMenuMappingMaterials.Where(t => t.PosMenuPortionRefId == menu.PortionId && t.LocationRefId == null).ToList();
                        }

                        int deptId = dept.Id;

                        int[] menuMappingRefIds = menuMapping.Select(t => t.Id).ToArray();

                        var deptListMapped = rsMenumappingDepartment.Where(t => menuMappingRefIds.Contains(t.MenuMappingRefId)).ToList();

                        bool AllDepartmentFlag;
                        if (deptListMapped.Count() == 0 || deptId == 0)
                        {
                            AllDepartmentFlag = true;
                            if (deptId != 0)
                            {
                                var existRec = outputMenu.Where(t => t.DepartmentRefId == 0 && t.MenuItemRefId == menu.PortionId).FirstOrDefault();
                                if (existRec != null)
                                {
                                    newDto.CostPerUnit = existRec.CostPerUnit;
                                    newDto.TotalCost = existRec.TotalCost;
                                    newDto.Profit = existRec.Profit;
                                    newDto.ProfitMargin = existRec.ProfitMargin;
                                    newDto.StandardMaterialWiseValueAndPercentage = existRec.StandardMaterialWiseValueAndPercentage;
                                    outputMenu.Add(newDto);
                                    continue;
                                }
                            }
                        }
                        else
                        {
                            AllDepartmentFlag = false;
                        }

                        List<MaterialUsageWithPriceAndDateRange> groupMaterialDetails = new List<MaterialUsageWithPriceAndDateRange>();

                        foreach (var material in menuMapping)
                        {
                            if (AllDepartmentFlag == false)
                            {
                                var deptLinkedMM = deptListMapped.Where(t => t.MenuMappingRefId == material.Id);
                                if (deptLinkedMM.Count() > 0)
                                {
                                    deptLinkedMM = deptLinkedMM.Where(t => t.DepartmentRefId == deptId);
                                    if (deptLinkedMM.Count() == 0)
                                    {
                                        continue;
                                    }
                                }
                            }

                            var matprice = materialRate.FirstOrDefault(t => t.MaterialRefId == material.MaterialRefId);
                            if (matprice == null)
                                continue;
                            var selectedMaterial = rsMaterial.FirstOrDefault(t => t.Id == material.MaterialRefId);
                            if (selectedMaterial == null)
                                continue;

                            var portionUnitName = unitList.FirstOrDefault(t => t.Id == material.PortionUnitId);
                            if (portionUnitName == null)
                                throw new UserFriendlyException(L("Unit") + " " + L("NotExist") + " - " + matprice.MaterialName);

                            decimal usagePerUnitOfMenuitemSales = Math.Round(material.PortionQty / material.MenuQuantitySold, 14, MidpointRounding.AwayFromZero);
                            decimal usageQty = Math.Round(usagePerUnitOfMenuitemSales * newDto.QtySold, 14, MidpointRounding.AwayFromZero);
                            decimal unitPrice = 0;
                            if (material.PortionUnitId == matprice.DefaultUnitId)
                                unitPrice = matprice.AvgRate;
                            else
                            {
                                var unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == material.PortionUnitId && t.RefUnitId == matprice.DefaultUnitId && t.MaterialRefId == material.Id);
                                if (unitConversion == null)
                                    unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == material.PortionUnitId && t.RefUnitId == matprice.DefaultUnitId);

                                if (unitConversion == null)
                                    throw new UserFriendlyException(L("UnitConversion") + " " + L("NotExist") + " - " + matprice.MaterialName);
                                unitPrice = unitConversion.Conversion * matprice.AvgRate;
                            }

                            decimal netAmount = Math.Round(usageQty * unitPrice, roundDecimals, MidpointRounding.AwayFromZero);

                            MaterialUsageWithPriceAndDateRange matDto = new MaterialUsageWithPriceAndDateRange
                            {
                                MaterialRefId = material.MaterialRefId,
                                MaterialRefName = matprice.MaterialName,
                                InvoiceDateRange = matprice.InvoiceDateRange,
                                AvgRate = unitPrice,
                                UsagePerUnit = usagePerUnitOfMenuitemSales,
                                Uom = portionUnitName.Name,
                                UsageQty = usageQty,
                                NetAmount = netAmount,
                                CostPercentage = 0
                            };
                            groupMaterialDetails.Add(matDto);
                        }

                        newDto.CostPerUnit = Math.Round(groupMaterialDetails.Sum(t => t.UsagePerUnit * t.AvgRate), roundDecimals, MidpointRounding.AwayFromZero);
                        newDto.TotalCost = newDto.CostPerUnit * newDto.QtySold;
                        newDto.Profit = newDto.SalesAmount - newDto.TotalCost;
                        if (newDto.Profit == 0 || newDto.SalesAmount == 0)
                        {
                            newDto.ProfitMargin = 0;
                        }
                        else
                        {
                            newDto.ProfitMargin = Math.Round(newDto.Profit / newDto.SalesAmount * 100, roundDecimals, MidpointRounding.AwayFromZero);
                        }
                        newDto.StandardMaterialWiseValueAndPercentage = groupMaterialDetails;

                        outputMenu.Add(newDto);
                    }
                }
            }

            outputMenu = outputMenu.Where(t => t.DepartmentRefId != 0).ToList();
            outputMenu = outputMenu.OrderBy(t => t.DepartmentRefId).ToList();

            returnoutput.RecipeWiseCosting = outputMenu;
            returnoutput.TotalCost = outputMenu.Sum(t => t.TotalCost);
            returnoutput.TotalSales = outputMenu.Sum(t => t.SalesAmount);
            returnoutput.TotalProfit = outputMenu.Sum(t => t.Profit);
            if (returnoutput.TotalSales > 0 && returnoutput.TotalProfit > 0)
                returnoutput.TotalProfitMargin = Math.Round(returnoutput.TotalProfit / returnoutput.TotalSales * 100, roundDecimals, MidpointRounding.AwayFromZero);

            if (remarks.Length > 0)
            {
                remarks = @L("PointsToVerify") + "<BR>" + remarks;
                returnoutput.Remarks = remarks;
            }
            returnoutput.MaterialRateView = materialRate;
            return returnoutput;
        }

        public async Task<AdjustmentDtosWithExcelFile> GetClosingStockVarianceReportExcel(LocationWithDate input)
        {
            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
            if (loc == null)
            {
                throw new UserFriendlyException(L("LocationError"));
            }

            string locationName = loc.Name.PadRight(10, '-');

            if (loc.HouseTransactionDate.HasValue)
            {
                if (loc.HouseTransactionDate.Value <= input.StockTakenDate)
                {
                    throw new UserFriendlyException(L("AccountDate") + ":" + loc.HouseTransactionDate.Value.ToString("yyyy-MMM-dd") + " " + L("StockTaken") + ":" + input.StockTakenDate.ToString("yyyy-MMM-dd"));
                }
            }
            else
            {
                throw new UserFriendlyException(L("AccountDate") + " ?");
            }

            var closingstocks = await _closingStockRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId
               && DbFunctions.TruncateTime(t.StockDate) == DbFunctions.TruncateTime(input.StockTakenDate)).ToListAsync();

            if (closingstocks == null || closingstocks.Count == 0)
            {
                throw new UserFriendlyException(L("NoMoreClosingStockTakenOnTheDate", input.StockTakenDate.ToString("dd-MMM-yyyy")));
            }

            int[] closingIds = closingstocks.Select(t => t.Id).ToArray();

            var closingStockDetailList = (from mat in _materialRepo.GetAll().Where(t => t.WipeOutStockOnClosingDay == false)
                                          join matGroup in _materialGroupCategoryRepo.GetAll()
                                          on mat.MaterialGroupCategoryRefId equals matGroup.Id
                                          join un in _unitrepo.GetAll()
                                          on mat.DefaultUnitId equals un.Id
                                          join cldetail in _closingStockDetailRepo.GetAll().Where(t => closingIds.Contains(t.ClosingStockRefId))
                                       on mat.Id equals cldetail.MaterialRefId
                                          select new MaterialViewDto
                                          {
                                              Id = mat.Id,
                                              MaterialName = mat.MaterialName,
                                              MaterialPetName = mat.MaterialPetName,
                                              MaterialGroupCategoryName = matGroup.MaterialGroupCategoryName,
                                              DefaultUnitId = mat.DefaultUnitId,
                                              DefaultUnitName = un.Name,
                                              OnHand = cldetail.OnHand,
                                              IsHighValueItem = mat.IsHighValueItem
                                          }).ToList();

            if (input.IsHighValueItemOnly == true)
                closingStockDetailList = closingStockDetailList.Where(t => t.IsHighValueItem == true).ToList();

            int[] materialRefIds = closingStockDetailList.Select(t => t.Id).ToArray();

            List<Material> requestMaterials = await _materialRepo.GetAllListAsync(t => materialRefIds.Contains(t.Id));

            GetStockSummaryDtoOutput outputClosingStock = await GetStockSummary(new GetHouseReportInput
            {
                StartDate = input.StockTakenDate,
                EndDate = input.StockTakenDate,
                LocationRefId = input.LocationRefId,
                ExcludeRateView = true,
                RequestedMaterialList = requestMaterials
            });

            List<MaterialLedgerDto> StockSummary = outputClosingStock.StockSummary;

            var closingStockConsolidated = (from det in closingStockDetailList
                                            join clstk in StockSummary
                                            on det.Id equals clstk.MaterialRefId
                                            group det by new
                                            {
                                                det.Id,
                                                det.MaterialName,
                                                det.MaterialGroupCategoryName,
                                                det.Uom,
                                                det.IsHighValueItem,
                                                clstk.ClBalance
                                            } into g
                                            select new MaterialViewDto
                                            {
                                                Id = g.Key.Id,
                                                MaterialName = g.Key.MaterialName,
                                                MaterialGroupCategoryName = g.Key.MaterialGroupCategoryName,
                                                Uom = g.Key.Uom,
                                                IsHighValueItem = g.Key.IsHighValueItem,
                                                OnHand = g.Sum(t => t.OnHand),
                                                ClBalance = g.Key.ClBalance
                                            }).ToList();

            var rsMaterialList = await _materialRepo.GetAll().ToListAsync();
            var rsUnit = await _unitrepo.GetAllListAsync();
            var dtos = new List<ClosingStockVarrianceDto>();

            foreach (var lst in closingStockConsolidated)
            {
                decimal diffStockvalue = Math.Round(lst.OnHand - lst.ClBalance, 14, MidpointRounding.AwayFromZero);

                lst.Difference = diffStockvalue;

                string diffStatus;
                if (diffStockvalue < 0)
                    diffStatus = L("Shortage");
                else if (diffStockvalue == 0)
                    diffStatus = L("Equal");
                else
                    diffStatus = L("Excess");

                lst.Status = diffStatus;

                int materialRefId = lst.Id;
                var mat = rsMaterialList.FirstOrDefault(t => t.Id == materialRefId);

                var unit = rsUnit.FirstOrDefault(t => t.Id == mat.DefaultUnitId);
                if (unit == null)
                {
                    throw new UserFriendlyException(L("Unit") + " " + L("Error") + " " + mat.MaterialName);
                }

                var stk = StockSummary.FirstOrDefault(t => t.MaterialRefId == lst.Id);

                var newDto = new ClosingStockVarrianceDto
                {
                    MaterialRefId = materialRefId,
                    MaterialName = mat.MaterialName,
                    MaterialPetName = mat.MaterialPetName,
                    OpenBalance = stk.OpenBalance,
                    ExcessReceived = stk.ExcessReceived,
                    Sales = stk.Sales,
                    Shortage = stk.Shortage,
                    SupplierReturn = stk.SupplierReturn,
                    LedgerDate = stk.LedgerDate,
                    Issued = stk.Issued,
                    Return = stk.Return,
                    TransferIn = stk.TransferIn,
                    TransferOut = stk.TransferOut,
                    Received = stk.Received,
                    TotalConsumption = stk.TotalConsumption,
                    AvgPrice = stk.AvgPrice,
                    PlusMinus = stk.PlusMinus,
                    StockValue = stk.StockValue,
                    Damaged = stk.Damaged,
                    ClBalance = lst.ClBalance,
                    CurrentStock = lst.OnHand,
                    DifferenceQuantity = Math.Abs(diffStockvalue),
                    ExcessShortageQty = diffStockvalue,
                    AdjustmentStatus = diffStatus,
                    DefaultUnitId = mat.DefaultUnitId,
                    Uom = unit.Name
                };

                dtos.Add(newDto);
            }

            bool materialCodeExists = false;
            if (PermissionChecker.IsGranted("Pages.Tenant.House.Master.Material.PrintMaterialCode"))
            {
                materialCodeExists = true;
            }

            FileDto fileExcel = _housereportExporter.ClosingStockVarrianceToExcelFile(dtos, locationName, input.StockTakenDate, materialCodeExists);

            AdjustmentDtosWithExcelFile output = new AdjustmentDtosWithExcelFile();
            output.ExcelFile = fileExcel;
            output.ClosingStockVarrianceDtos = dtos;

            return output;
        }

        public async Task<InterTransferReportDto> GetTransferOutLocationWiseDetailReport(InputInterTransferReport input)
        {
            if (input.ToLocations == null)
            {
                throw new UserFriendlyException("LocationErr");
            }

            int[] ToLocIds = input.ToLocations.Select(t => t.Id).ToArray();

            var rsMasterQb = _intertransferRepo.GetAll().Where(t => t.RequestLocationRefId == input.LocationRefId);

            rsMasterQb = rsMasterQb.WhereIf(ToLocIds.Count() > 0, t => ToLocIds.Contains(t.LocationRefId));

            if (input.ExactSearchFlag)
                rsMasterQb = rsMasterQb.WhereIf(!input.TransferNumber.IsNullOrEmpty(), t => t.Id.ToString().Equals(input.TransferNumber));
            else
                rsMasterQb = rsMasterQb.WhereIf(!input.TransferNumber.IsNullOrEmpty(), t => t.Id.ToString().Contains(input.TransferNumber));

            if (input.StartDate.Value != null && input.EndDate.Value != null)
            {
                rsMasterQb =
                    rsMasterQb.Where(
                        a => DbFunctions.TruncateTime(a.CompletedTime) >= DbFunctions.TruncateTime(input.StartDate)
                        && DbFunctions.TruncateTime(a.CompletedTime) <= DbFunctions.TruncateTime(input.EndDate));
            }

            var rsDetailQb = _intertransferDetailRepo.GetAll().Where(t => t.IssueQty > 0);
            var rsMaterialQb = _materialRepo.GetAll();

            bool MaterialExists = false;

            if (input.MaterialList != null && input.MaterialList.Count > 0)
            {
                MaterialExists = true;
                int[] materialListIds = input.MaterialList.Select(t => t.Id).ToArray();
                rsDetailQb = rsDetailQb.WhereIf(MaterialExists, t => materialListIds.Contains(t.MaterialRefId));
                rsMaterialQb = rsMaterialQb.WhereIf(MaterialExists, t => materialListIds.Contains(t.Id));
            }

            if (input.MaterialTypeList != null && input.MaterialTypeList.Count > 0)
            {
                var stridList = input.MaterialTypeList.Select(t => t.Value).ToArray();
                int[] materialTypeIdList = Array.ConvertAll(stridList, s => int.Parse(s));
                rsMaterialQb = rsMaterialQb.Where(t => materialTypeIdList.Contains(t.MaterialTypeId));
            }

            string rawstring = L("RAW");
            string semistring = L("SEMI");

            InterTransferReportDto output = new InterTransferReportDto();

            var allItems = await (from mas in rsMasterQb
                                  join det in rsDetailQb on mas.Id equals det.InterTransferRefId
                                  join mat in rsMaterialQb on det.MaterialRefId equals mat.Id
                                  join unit in _unitrepo.GetAll() on mat.DefaultUnitId equals unit.Id
                                  join uiss in _unitrepo.GetAll() on det.UnitRefId equals uiss.Id
                                  join loc in _locationRepo.GetAll() on mas.LocationRefId equals loc.Id
                                  select new InterTransferReportDetailOutPut
                                  {
                                      TransferRefId = mas.Id,
                                      LocationRefId = mas.LocationRefId,
                                      LocationRefName = loc.Name,
                                      TransferDate = mas.CompletedTime.Value,
                                      ReceivedTime = mas.ReceivedTime,
                                      MaterialRefId = det.MaterialRefId,
                                      MaterialTypeRefId = mat.MaterialTypeId,
                                      MaterialTypeRefName = mat.MaterialTypeId == 0 ? rawstring : semistring,
                                      MaterialPetName = mat.MaterialPetName,
                                      MaterialRefName = mat.MaterialName,
                                      TotalQty = Math.Round(det.IssueQty, 14),
                                      Price = det.Price,
                                      TotalAmount = det.IssueValue,
                                      CurrentStatus = det.CurrentStatus,
                                      DefaultUnitId = mat.DefaultUnitId,
                                      DefaultUnitName = unit.Name,
                                      Uom = unit.Name,
                                      UnitRefId = det.UnitRefId,
                                      UnitRefName = uiss.Name,
                                      VehicleNumber = mas.VehicleNumber,
                                      PersonInCharge = mas.PersonInCharge
                                  }).ToListAsync();

            output.DetailReport = allItems.OrderBy(t => t.MaterialRefId).ToList();

            var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();

            var groupLocationWiseItems = (from mas in allItems
                                          join uc in rsUc
                                          on mas.UnitRefId equals uc.BaseUnitId
                                          where uc.RefUnitId == mas.DefaultUnitId
                                          group mas by new
                                          {
                                              mas.LocationRefId,
                                              mas.LocationRefName,
                                              mas.MaterialTypeRefId,
                                              mas.MaterialTypeRefName,
                                              mas.MaterialRefId,
                                              mas.MaterialRefName,
                                              mas.Uom,
                                              mas.DefaultUnitName,
                                              uc.Conversion
                                          } into g
                                          select new InterTransferReportDetailOutPut
                                          {
                                              LocationRefId = g.Key.LocationRefId,
                                              LocationRefName = g.Key.LocationRefName,
                                              MaterialTypeRefId = g.Key.MaterialTypeRefId,
                                              MaterialTypeRefName = g.Key.MaterialTypeRefName,
                                              MaterialRefId = g.Key.MaterialRefId,
                                              MaterialRefName = g.Key.MaterialRefName,
                                              Uom = g.Key.DefaultUnitName,
                                              DefaultUnitName = g.Key.DefaultUnitName,
                                              TotalQty = Math.Round(g.Sum(t => t.TotalQty * g.Key.Conversion), 14),
                                              TotalAmount = g.Sum(t => t.TotalAmount * g.Key.Conversion),
                                              Price = g.Sum(t => t.TotalAmount) == 0 ? 0 : g.Sum(t => t.TotalAmount * g.Key.Conversion) / g.Sum(t => t.TotalQty * g.Key.Conversion)
                                          }).ToList();

            groupLocationWiseItems = (from mas in groupLocationWiseItems
                                      group mas by new
                                      {
                                          mas.LocationRefId,
                                          mas.LocationRefName,
                                          mas.MaterialTypeRefId,
                                          mas.MaterialTypeRefName,
                                          mas.MaterialRefId,
                                          mas.MaterialRefName,
                                          mas.Uom,
                                          mas.DefaultUnitName,
                                      } into g
                                      select new InterTransferReportDetailOutPut
                                      {
                                          LocationRefId = g.Key.LocationRefId,
                                          LocationRefName = g.Key.LocationRefName,
                                          MaterialTypeRefId = g.Key.MaterialTypeRefId,
                                          MaterialTypeRefName = g.Key.MaterialTypeRefName,
                                          MaterialRefId = g.Key.MaterialRefId,
                                          MaterialRefName = g.Key.MaterialRefName,
                                          Uom = g.Key.Uom,
                                          DefaultUnitName = g.Key.DefaultUnitName,
                                          TotalQty = Math.Round(g.Sum(t => t.TotalQty), 14),
                                          TotalAmount = g.Sum(t => t.TotalAmount),
                                          Price = Math.Round(g.Sum(t => t.TotalAmount) == 0 ? 0 : g.Sum(t => t.TotalAmount) / g.Sum(t => t.TotalQty), roundDecimals)
                                      }).ToList();

            groupLocationWiseItems = groupLocationWiseItems.OrderBy(t => t.MaterialRefId).ToList();

            output.LocationWiseConsolidatedTransferReport = groupLocationWiseItems;

            return output;
        }

        public async Task<InterTransferReportDto> GetTransferInLocationWiseDetailReport(InputInterTransferReport input)
        {
            if (input.ToLocations == null)
            {
                throw new UserFriendlyException("LocationErr");
            }

            int[] FromLocIds = input.ToLocations.Select(t => t.Id).ToArray();

            var rsMasterQb = _intertransferRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId);

            rsMasterQb = rsMasterQb.WhereIf(FromLocIds.Count() > 0, t => FromLocIds.Contains(t.RequestLocationRefId));

            if (input.ExactSearchFlag)
                rsMasterQb = rsMasterQb.WhereIf(!input.TransferNumber.IsNullOrEmpty(), t => t.Id.ToString().Equals(input.TransferNumber));
            else
                rsMasterQb = rsMasterQb.WhereIf(!input.TransferNumber.IsNullOrEmpty(), t => t.Id.ToString().Contains(input.TransferNumber));

            if (input.StartDate.Value != null && input.EndDate.Value != null)
            {
                rsMasterQb =
                    rsMasterQb.Where(
                        a => DbFunctions.TruncateTime(a.ReceivedTime) >= DbFunctions.TruncateTime(input.StartDate)
                        && DbFunctions.TruncateTime(a.ReceivedTime) <= DbFunctions.TruncateTime(input.EndDate));
            }

            var rsDetailQb = _intertransferReceivedRepo.GetAll().Where(t => t.ReceivedQty > 0);
            var rsMaterialQb = _materialRepo.GetAll();

            bool MaterialExists = false;

            if (input.MaterialList != null && input.MaterialList.Count > 0)
            {
                MaterialExists = true;
                int[] materialListIds = input.MaterialList.Select(t => t.Id).ToArray();
                rsDetailQb = rsDetailQb.WhereIf(MaterialExists, t => materialListIds.Contains(t.MaterialRefId));
                rsMaterialQb = rsMaterialQb.WhereIf(MaterialExists, t => materialListIds.Contains(t.Id));
            }

            if (input.MaterialTypeList != null && input.MaterialTypeList.Count > 0)
            {
                var stridList = input.MaterialTypeList.Select(t => t.Value).ToArray();
                int[] materialTypeIdList = Array.ConvertAll(stridList, s => int.Parse(s));
                rsMaterialQb = rsMaterialQb.Where(t => materialTypeIdList.Contains(t.MaterialTypeId));
            }

            string rawstring = L("RAW");
            string semistring = L("SEMI");

            InterTransferReportDto output = new InterTransferReportDto();

            var allItems = (from mas in rsMasterQb
                            join det in rsDetailQb on mas.Id equals det.InterTransferRefId
                            join mat in rsMaterialQb on det.MaterialRefId equals mat.Id
                            join unit in _unitrepo.GetAll() on mat.DefaultUnitId equals unit.Id
                            join uiss in _unitrepo.GetAll() on det.UnitRefId equals uiss.Id
                            join loc in _locationRepo.GetAll() on mas.RequestLocationRefId equals loc.Id
                            select new InterTransferReportDetailOutPut
                            {
                                TransferRefId = mas.Id,
                                LocationRefId = mas.RequestLocationRefId,
                                LocationRefName = loc.Name,
                                TransferDate = mas.CompletedTime.Value,
                                ReceivedTime = mas.ReceivedTime,
                                MaterialRefId = det.MaterialRefId,
                                MaterialTypeRefId = mat.MaterialTypeId,
                                MaterialTypeRefName = mat.MaterialTypeId == 0 ? rawstring : semistring,
                                MaterialPetName = mat.MaterialPetName,
                                MaterialRefName = mat.MaterialName,
                                TotalQty = Math.Round(det.ReceivedQty, 14),
                                Price = det.Price,
                                TotalAmount = det.ReceivedValue,
                                CurrentStatus = det.CurrentStatus,
                                DefaultUnitId = mat.DefaultUnitId,
                                DefaultUnitName = unit.Name,
                                Uom = unit.Name,
                                UnitRefId = det.UnitRefId,
                                UnitRefName = uiss.Name,
                                VehicleNumber = mas.VehicleNumber,
                                PersonInCharge = mas.PersonInCharge
                            }).ToList();

            output.DetailReport = allItems.OrderBy(t => t.MaterialRefId).ToList();

            var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();

            var groupLocationWiseItems = (from mas in allItems
                                          join uc in rsUc
                                          on mas.UnitRefId equals uc.BaseUnitId
                                          where uc.RefUnitId == mas.DefaultUnitId
                                          group mas by new
                                          {
                                              mas.LocationRefId,
                                              mas.LocationRefName,
                                              mas.MaterialTypeRefId,
                                              mas.MaterialTypeRefName,
                                              mas.MaterialRefId,
                                              mas.MaterialRefName,
                                              mas.Uom,
                                              mas.DefaultUnitName,
                                              uc.Conversion
                                          } into g
                                          select new InterTransferReportDetailOutPut
                                          {
                                              LocationRefId = g.Key.LocationRefId,
                                              LocationRefName = g.Key.LocationRefName,
                                              MaterialTypeRefId = g.Key.MaterialTypeRefId,
                                              MaterialTypeRefName = g.Key.MaterialTypeRefName,
                                              MaterialRefId = g.Key.MaterialRefId,
                                              MaterialRefName = g.Key.MaterialRefName,
                                              Uom = g.Key.DefaultUnitName,
                                              DefaultUnitName = g.Key.DefaultUnitName,
                                              TotalQty = Math.Round(g.Sum(t => t.TotalQty * g.Key.Conversion), 14),
                                              TotalAmount = g.Sum(t => t.TotalAmount * g.Key.Conversion),
                                              Price = g.Sum(t => t.TotalAmount) == 0 ? 0 : g.Sum(t => t.TotalAmount * g.Key.Conversion) / g.Sum(t => t.TotalQty * g.Key.Conversion)
                                          }).ToList();

            groupLocationWiseItems = (from mas in groupLocationWiseItems
                                      group mas by new
                                      {
                                          mas.LocationRefId,
                                          mas.LocationRefName,
                                          mas.MaterialTypeRefId,
                                          mas.MaterialTypeRefName,
                                          mas.MaterialRefId,
                                          mas.MaterialRefName,
                                          mas.Uom,
                                          mas.DefaultUnitName,
                                      } into g
                                      select new InterTransferReportDetailOutPut
                                      {
                                          LocationRefId = g.Key.LocationRefId,
                                          LocationRefName = g.Key.LocationRefName,
                                          MaterialTypeRefId = g.Key.MaterialTypeRefId,
                                          MaterialTypeRefName = g.Key.MaterialTypeRefName,
                                          MaterialRefId = g.Key.MaterialRefId,
                                          MaterialRefName = g.Key.MaterialRefName,
                                          Uom = g.Key.Uom,
                                          DefaultUnitName = g.Key.DefaultUnitName,
                                          TotalQty = Math.Round(g.Sum(t => t.TotalQty), 14),
                                          TotalAmount = g.Sum(t => t.TotalAmount),
                                          Price = Math.Round(g.Sum(t => t.TotalAmount) == 0 ? 0 : g.Sum(t => t.TotalAmount) / g.Sum(t => t.TotalQty), roundDecimals)
                                      }).ToList();

            groupLocationWiseItems = groupLocationWiseItems.OrderBy(t => t.MaterialRefId).ToList();

            output.LocationWiseConsolidatedTransferReport = groupLocationWiseItems;

            return output;
        }

        public async Task<FileDto> GetLocationWiseConsolidatedClosingStockVarianceReportExcel(ClosingStockConsolidatedDto input)
        {
            string reportName = input.StockTakenDate.ToString("ddd dd-MMM-yyyy");
            var locRefIds = input.Locations.Select(t => t.Id).ToList();

            var temp = await _locationRepo.GetAllListAsync(t => locRefIds.Contains(t.Id));
            List<LocationListDto> locations = temp.MapTo<List<LocationListDto>>();
            foreach (var location in locations)
            {
                if (location.HouseTransactionDate.HasValue)
                {
                    if (location.HouseTransactionDate.Value <= input.StockTakenDate)
                    {
                        throw new UserFriendlyException(location.Code + " " + L("AccountDate") + ":" + location.HouseTransactionDate.Value.ToString("yyyy-MMM-dd") + " " + L("StockTaken") + ":" + input.StockTakenDate.ToString("yyyy-MMM-dd"));
                    }
                }
                else
                {
                    throw new UserFriendlyException(location.Code + " " + L("AccountDate") + " ?");
                }
            }

            var locsRefIds = input.Locations.Select(t => t.Id).ToArray();

            var closingstocks = await _closingStockRepo.GetAll().Where(t => locsRefIds.Contains(t.LocationRefId)
                        && DbFunctions.TruncateTime(t.StockDate) == DbFunctions.TruncateTime(input.StockTakenDate)).ToListAsync();

            if (closingstocks == null || closingstocks.Count == 0)
            {
                throw new UserFriendlyException(L("NoMoreClosingStockTakenOnTheDate", input.StockTakenDate.ToString("dd-MMM-yyyy")));
            }

            var rsMaterial = _materialRepo.GetAll().Where(t => t.WipeOutStockOnClosingDay == false);
            if (input.IsHighValueItem)
                rsMaterial = rsMaterial.Where(t => t.IsHighValueItem == true);

            int[] closingIds = closingstocks.Select(t => t.Id).ToArray();

            var closingStockDetailList = await (from clmas in _closingStockRepo.GetAll().Where(t => closingIds.Contains(t.Id))
                                                join cldetail in _closingStockDetailRepo.GetAll().Where(t => closingIds.Contains(t.ClosingStockRefId))
                                               on clmas.Id equals cldetail.ClosingStockRefId
                                                join mat in rsMaterial
                                                on cldetail.MaterialRefId equals mat.Id
                                                join un in _unitrepo.GetAll()
                                                on mat.DefaultUnitId equals un.Id
                                                select new ClosingStockDetailViewDto
                                                {
                                                    LocationRefId = clmas.LocationRefId,
                                                    MaterialRefId = mat.Id,
                                                    MaterialRefName = mat.MaterialName,
                                                    MaterialPetName = mat.MaterialPetName,
                                                    UnitRefId = mat.DefaultUnitId,
                                                    UnitRefName = un.Name,
                                                    OnHand = cldetail.OnHand,
                                                }).ToListAsync();

            int[] materialRefIds = closingStockDetailList.Select(t => t.MaterialRefId).ToArray();

            List<Material> requestMaterials = await _materialRepo.GetAllListAsync(t => materialRefIds.Contains(t.Id));
            if (!input.IsAllMaterialFlag)
            {
                List<string> selMatRefIds = input.Materials.Select(t => t.Value).ToList();
                int n;
                List<int?> nullableInts = selMatRefIds.Select(s => Int32.TryParse(s, out n) ? n : (int?)null).ToList();
                List<int?> matIds = nullableInts.Where(t => t.HasValue).ToList();
                requestMaterials = requestMaterials.Where(t => matIds.Contains(t.Id)).ToList();
            }
            var rsMaterialList = await _materialRepo.GetAll().ToListAsync();
            var rsUnit = await _unitrepo.GetAllListAsync();
            var dtos = new List<ClosingStockVarrianceDto>();

            foreach (var location in locations)
            {
                GetStockSummaryDtoOutput outputClosingStock = await GetStockSummary(new GetHouseReportInput
                {
                    StartDate = input.StockTakenDate,
                    EndDate = input.StockTakenDate,
                    LocationRefId = location.Id,
                    ExcludeRateView = true,
                    RequestedMaterialList = requestMaterials
                });

                List<MaterialLedgerDto> StockSummary = outputClosingStock.StockSummary;

                var locWiseClosingDetail = closingStockDetailList.Where(t => t.LocationRefId == location.Id).ToList();

                var closingStockConsolidated = (from det in locWiseClosingDetail
                                                join clstk in StockSummary
                                                on det.MaterialRefId equals clstk.MaterialRefId
                                                group det by new
                                                {
                                                    det.LocationRefId,
                                                    det.MaterialRefId,
                                                    clstk.ClBalance
                                                } into g
                                                select new ClosingStockConsolidatedDetail
                                                {
                                                    LocationRefId = g.Key.LocationRefId,
                                                    MaterialRefId = g.Key.MaterialRefId,
                                                    OnHand = g.Sum(t => t.OnHand),
                                                    ClBalance = g.Key.ClBalance
                                                }).ToList();

                foreach (var lst in closingStockConsolidated)
                {
                    decimal diffStockvalue = Math.Round(lst.OnHand - lst.ClBalance, 14, MidpointRounding.AwayFromZero);

                    lst.Difference = diffStockvalue;

                    string diffStatus;
                    if (diffStockvalue < 0)
                        diffStatus = L("Shortage");
                    else if (diffStockvalue == 0)
                        diffStatus = L("Equal");
                    else
                        diffStatus = L("Excess");

                    lst.Status = diffStatus;

                    int materialRefId = lst.MaterialRefId;
                    var mat = rsMaterialList.FirstOrDefault(t => t.Id == materialRefId);

                    var unit = rsUnit.FirstOrDefault(t => t.Id == mat.DefaultUnitId);
                    if (unit == null)
                    {
                        throw new UserFriendlyException(L("Unit") + " " + L("Error") + " " + mat.MaterialName);
                    }

                    var stk = StockSummary.FirstOrDefault(t => t.MaterialRefId == lst.MaterialRefId);

                    var newDto = new ClosingStockVarrianceDto
                    {
                        LocationRefId = location.Id,
                        MaterialRefId = materialRefId,
                        MaterialName = mat.MaterialName,
                        MaterialPetName = mat.MaterialPetName,
                        OpenBalance = stk.OpenBalance,
                        ExcessReceived = stk.ExcessReceived,
                        Sales = stk.Sales,
                        Shortage = stk.Shortage,
                        SupplierReturn = stk.SupplierReturn,
                        LedgerDate = stk.LedgerDate,
                        Issued = stk.Issued,
                        Return = stk.Return,
                        TransferIn = stk.TransferIn,
                        TransferOut = stk.TransferOut,
                        Received = stk.Received,
                        TotalConsumption = stk.TotalConsumption,
                        AvgPrice = stk.AvgPrice,
                        PlusMinus = stk.PlusMinus,
                        StockValue = stk.StockValue,
                        Damaged = stk.Damaged,
                        ClBalance = lst.ClBalance,
                        CurrentStock = lst.OnHand,
                        DifferenceQuantity = Math.Abs(diffStockvalue),
                        ExcessShortageQty = diffStockvalue,
                        AdjustmentStatus = diffStatus,
                        DefaultUnitId = mat.DefaultUnitId,
                        Uom = unit.Name
                    };

                    dtos.Add(newDto);
                }
            }

            bool materialCodeExists = false;
            if (PermissionChecker.IsGranted("Pages.Tenant.House.Master.Material.PrintMaterialCode"))
            {
                materialCodeExists = true;
            }

            var rsUnits = await _unitrepo.GetAllListAsync();

            FileDto fileExcel = _housereportExporter.ConsolidatedLocationWiseClosingStockVarrianceToExcelFile(locations, requestMaterials, dtos, input.StockTakenDate, materialCodeExists, rsUnits);

            return fileExcel;
        }

        public async Task<ApiBatchMessageReturn> ApiPhysicalStockBatchFileForGivenDatePeriod(ApiPhysicalStockInputDto input)
        {
            // File Name "Physical_"
            ApiBatchMessageReturn output = new ApiBatchMessageReturn();
            var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
            if (location == null)
            {
                output.ErrorMessage = "Location Not Exists For Id :" + input.LocationRefId;
                return output;
            }
            var tenant = await GetCurrentTenantAsync();
            var tenantName = tenant.TenancyName;

            List<string> rows = new List<string>();
            StringBuilder masterFile = new StringBuilder();
            try
            {
                GetStockSummaryDtoOutput outputClosingStock = await GetStockSummary(new GetHouseReportInput
                {
                    StartDate = input.StockDate,
                    EndDate = input.StockDate,
                    LocationRefId = input.LocationRefId,
                    ExcludeRateView = true
                });
                var rsMaterials = await _materialRepo.GetAllListAsync();
                var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();
                var rsUnits = await _unitrepo.GetAllListAsync();

                var fp = new TextFileWriter();
                string locationCode = fp.PrintFormatRight(location.Code, 5);
                string stockDateFormated = input.StockDate.ToString("yyyy-MM-dd");
                if (outputClosingStock.StockSummary != null && outputClosingStock.StockSummary.Count > 0)
                {
                    var allItems = outputClosingStock.StockSummary;
                    //fp.FileOpen(fileName, "O");
                    string record = string.Empty;
                    foreach (var lst in allItems)
                    {
                        decimal clBalanceQty = lst.ClBalance;
                        string uom = lst.Uom;

                        var material = rsMaterials.FirstOrDefault(t => t.Id == lst.MaterialRefId);
                        if (material.DefaultUnitId != material.TransferUnitId)
                        {
                            decimal conversionFactor = 0;
                            var unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == material.DefaultUnitId && t.RefUnitId == material.TransferUnitId && t.MaterialRefId == material.Id);
                            if (unitConversion == null)
                                unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == material.DefaultUnitId && t.RefUnitId == material.TransferUnitId);
                            if (unitConversion != null)
                            {
                                conversionFactor = unitConversion.Conversion;
                            }
                            else
                            {
                                unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == material.TransferUnitId && t.RefUnitId == material.DefaultUnitId && t.MaterialRefId == material.Id);
                                if (unitConversion == null)
                                    unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == material.TransferUnitId && t.RefUnitId == material.DefaultUnitId);

                                if (unitConversion == null)
                                {
                                    var baseUnit = await _unitrepo.FirstOrDefaultAsync(t => t.Id == material.TransferUnitId);
                                    var refUnit = await _unitrepo.FirstOrDefaultAsync(t => t.Id == material.DefaultUnitId);
                                    throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
                                }
                                conversionFactor = 1 / unitConversion.Conversion;
                            }
                            clBalanceQty = clBalanceQty * conversionFactor;
                            var unit = rsUnits.FirstOrDefault(t => t.Id == material.TransferUnitId);
                            uom = unit.Name;
                        }

                        string closingQty = clBalanceQty.ToString("0.00");
                        //record = locationCode + "," + stockDateFormated + "," + fp.PrintFormatLeft(lst.MaterialPetName, 30) + "," + closingQty + "," + lst.Uom;
                        //fp.PrintTextLine(record);
                        record = location.Code + "," + stockDateFormated + "," + lst.MaterialPetName + "," + closingQty + "," + uom;
                        rows.Add(record);
                        masterFile.Append(record);
                        masterFile.AppendLine();
                    }
                }
            }
            catch (Exception ex)
            {
                output.ErrorMessage = ex.Message;
                return output;
            }
            output.Rows = rows;
            //output.FileOutput = masterFile;
            //output.OutputMessage = masterFile.ToString();
            return output;
        }

        public async Task<ApiBatchMessageReturn> ApiInventoryTransferBatchFileForGivenDatePeriod(ApiPhysicalStockInputDto input)
        {
            //GoodsReceivedReturned
            ApiBatchMessageReturn output = new ApiBatchMessageReturn();
            var rsLocation = await _locationRepo.GetAllListAsync();
            var location = rsLocation.FirstOrDefault(t => t.Id == input.LocationRefId);
            if (location == null)
            {
                output.ErrorMessage = "Location Not Exists For Id :" + input.LocationRefId;
                return output;
            }
            var tenant = await GetCurrentTenantAsync();
            var tenantName = tenant.TenancyName;
            var rsUnits = await _unitrepo.GetAllListAsync();

            StringBuilder detailFile = new StringBuilder();
            List<string> rows = new List<string>();
            try
            {
                List<int> materialRefIds = new List<int>();

                #region Get Data

                #region Transfer Out

                var tempOut = await _intertransferRepo.GetAllListAsync(t => t.RequestLocationRefId == input.LocationRefId && DbFunctions.TruncateTime(t.CompletedTime) == DbFunctions.TruncateTime(input.StockDate));
                List<InterTransferListDto> transferOutDtos = tempOut.MapTo<List<InterTransferListDto>>();

                List<int> transferOurRefIds = transferOutDtos.Select(t => t.Id).ToList();
                List<InterTransferDetailViewDto> interTransferOutDetail = new List<InterTransferDetailViewDto>();
                interTransferOutDetail = await (from request in _intertransferDetailRepo.GetAll().Where(a => transferOurRefIds.Contains(a.InterTransferRefId))
                                                join reqMaster in _intertransferRepo.GetAll().Where(t => transferOurRefIds.Contains(t.Id))
                                                    on request.InterTransferRefId equals reqMaster.Id
                                                join mat in _materialRepo.GetAll() on request.MaterialRefId equals mat.Id
                                                join un in _unitrepo.GetAll() on mat.DefaultUnitId equals un.Id
                                                join uiss in _unitrepo.GetAll() on request.UnitRefId equals uiss.Id
                                                select new InterTransferDetailViewDto
                                                {
                                                    Id = request.Id,
                                                    RequestQty = request.RequestQty,
                                                    Barcode = mat.Barcode == null ? mat.MaterialName : mat.Barcode,
                                                    MaterialRefId = request.MaterialRefId,
                                                    MaterialPetName = mat.MaterialPetName,
                                                    MaterialRefName = mat.MaterialName,
                                                    CurrentStatus = request.CurrentStatus,
                                                    InterTransferRefId = request.InterTransferRefId,
                                                    IssueQty = request.IssueQty,
                                                    Price = request.Price,
                                                    ApprovedRemarks = request.ApprovedRemarks,
                                                    RequestRemarks = request.RequestRemarks,
                                                    Sno = request.Sno,
                                                    MaterialTypeId = mat.MaterialTypeId,
                                                    Uom = un.Name,
                                                    UnitRefId = request.UnitRefId,
                                                    UnitRefName = uiss.Name,
                                                    DefaultUnitId = mat.DefaultUnitId,
                                                    DefaultUnitName = un.Name,
                                                    DocReferenceNumber = reqMaster.DocReferenceNumber
                                                }).ToListAsync();

                foreach (var lst in interTransferOutDetail)
                {
                    if (lst.CurrentStatus.Equals("Q"))
                        lst.CurrentStatus = L("RequestApprovalPending");
                    else if (lst.CurrentStatus.Equals("P"))
                        lst.CurrentStatus = L("Pending");
                    else if (lst.CurrentStatus.Equals("A"))
                        lst.CurrentStatus = L("Approved");
                    else if (lst.CurrentStatus.Equals("R"))
                        lst.CurrentStatus = L("Rejected");
                    else if (lst.CurrentStatus.Equals("D"))
                        lst.CurrentStatus = L("Drafted");
                    else if (lst.CurrentStatus.Equals("V"))
                        lst.CurrentStatus = L("Received");
                    else if (lst.CurrentStatus.Equals("I"))
                        lst.CurrentStatus = L("PartiallyReceived");
                    else if (lst.CurrentStatus.Equals("E"))
                        lst.CurrentStatus = L("Excess");
                    else if (lst.CurrentStatus.Equals("S"))
                        lst.CurrentStatus = L("Shortage");

                    string materialTypeName = lst.MaterialTypeId == (int)MaterialType.RAW ? L("RAW") : L("SEMI");
                    lst.MaterialTypeName = materialTypeName;
                }
                materialRefIds.AddRange(interTransferOutDetail.Select(t => t.MaterialRefId).ToList());

                #endregion Transfer Out

                var tempIn = await _intertransferRepo.GetAllListAsync(t => t.LocationRefId == input.LocationRefId && t.CompletedTime.HasValue && DbFunctions.TruncateTime(t.ReceivedTime) == DbFunctions.TruncateTime(input.StockDate));
                List<InterTransferListDto> transferInDtos = tempIn.MapTo<List<InterTransferListDto>>();

                List<int> transferInRefIds = transferInDtos.Select(t => t.Id).ToList();
                List<InterTransferReceivedEditDto> interTransferReceivedDetail = new List<InterTransferReceivedEditDto>();

                interTransferReceivedDetail = await (from request in _intertransferDetailRepo.GetAll()
                                      .Where(a => transferInRefIds.Contains(a.InterTransferRefId) && "AES".IndexOf(a.CurrentStatus) >= 0)
                                                     join reqMaster in _intertransferRepo.GetAll().Where(t => transferInRefIds.Contains(t.Id))
                                                                   on request.InterTransferRefId equals reqMaster.Id
                                                     join mat in _materialRepo.GetAll()
                                                     on request.MaterialRefId equals mat.Id
                                                     join un in _unitrepo.GetAll()
                                                          on mat.DefaultUnitId equals un.Id
                                                     join uiss in _unitrepo.GetAll()
                                                         on request.UnitRefId equals uiss.Id
                                                     select new InterTransferReceivedEditDto
                                                     {
                                                         Id = request.Id,
                                                         ReceivedQty = 0,
                                                         ReturnFlag = false,
                                                         ReturnQty = 0,
                                                         AdjustmentFlag = false,
                                                         AdjustmentMode = "",
                                                         AdjustmentQty = 0,
                                                         MaterialRefId = request.MaterialRefId,
                                                         MaterialPetName = mat.MaterialPetName,
                                                         MaterialRefName = mat.MaterialName,
                                                         CurrentStatus = request.CurrentStatus,
                                                         InterTransferRefId = request.InterTransferRefId,
                                                         Sno = request.Sno,
                                                         RequestQty = request.RequestQty,
                                                         IssueQty = request.IssueQty,
                                                         Price = request.Price,
                                                         ReceivedValue = Math.Round(request.IssueQty * request.Price, roundDecimals),
                                                         Uom = un.Name,
                                                         UnitRefId = request.UnitRefId,
                                                         UnitRefName = uiss.Name,
                                                         DefaultUnitId = mat.DefaultUnitId,
                                                         DefaultUnitName = un.Name,
                                                         DocReferenceNumber = reqMaster.DocReferenceNumber
                                                     }).ToListAsync();
                materialRefIds.AddRange(interTransferReceivedDetail.Select(t => t.MaterialRefId).ToList());

                //var rsUnitConversion = await _unitconversionRepo.GetAllListAsync();
                foreach (var item in interTransferReceivedDetail)
                {
                    if (item.CurrentStatus.Equals("Q"))
                        item.CurrentStatus = L("RequestApprovalPending");
                    else if (item.CurrentStatus.Equals("P"))
                        item.CurrentStatus = L("Pending");
                    else if (item.CurrentStatus.Equals("A"))
                        item.CurrentStatus = L("Approved");
                    else if (item.CurrentStatus.Equals("R"))
                        item.CurrentStatus = L("Rejected");
                    else if (item.CurrentStatus.Equals("D"))
                        item.CurrentStatus = L("Drafted");
                    else if (item.CurrentStatus.Equals("V"))
                        item.CurrentStatus = L("Received");
                    else if (item.CurrentStatus.Equals("I"))
                        item.CurrentStatus = L("PartiallyReceived");
                    else if (item.CurrentStatus.Equals("E"))
                        item.CurrentStatus = L("Excess");
                    else if (item.CurrentStatus.Equals("S"))
                        item.CurrentStatus = L("Shortage");

                    item.LineTotal = Math.Round(item.IssueQty * item.Price, roundDecimals, MidpointRounding.AwayFromZero);

                    var rsReceived = await _intertransferReceivedRepo.FirstOrDefaultAsync(t => t.InterTransferRefId == item.InterTransferRefId && t.MaterialRefId == item.MaterialRefId);
                    if (rsReceived != null)
                    {
                        item.ReceivedQty = rsReceived.ReceivedQty;
                        item.ReceivedValue = Math.Round(item.ReceivedQty * item.Price, roundDecimals, MidpointRounding.AwayFromZero);
                        item.LineTotal = item.ReceivedValue;
                    }
                }

                var rsMaterial = await _materialRepo.GetAllListAsync(t => materialRefIds.Contains(t.Id));

                #endregion Get Data

                var fpHeader = new TextFileWriter();
                var fpDetail = new TextFileWriter();

                var subPath = "~/Report/" + tenantName + "/";
                var exists = Directory.Exists(HttpContext.Current.Server.MapPath(subPath));

                if (!exists)
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(subPath));

                var fileNameHeader =
                    HttpContext.Current.Server.MapPath("~/Report/" + tenantName + "/InvCard_" + location.Code + input.StockDate.ToString("yyyyMMdd") + ".TXT");

                string locationCode = fpHeader.PrintFormatRight(location.Code, 5);
                string stockDateFormated = input.StockDate.ToString("yyyy-MM-dd");
                string masterrecord = string.Empty;

                var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();
                //var rsUnits = await _unitrepo.GetAllListAsync();

                #region Master Record

                foreach (var lst in transferOutDtos)
                {
                    var rsTranMaster = lst;
                    var details = interTransferOutDetail.Where(t => t.InterTransferRefId == lst.Id && t.IssueQty > 0);

                    stockDateFormated = rsTranMaster.CompletedTime.Value.ToString("yyyy-MM-dd");
                    var trnLocation = rsLocation.FirstOrDefault(t => t.Id == rsTranMaster.LocationRefId);
                    string trnLocationCode = fpHeader.PrintFormatRight(trnLocation.Code, 5);

                    string detailrecord = string.Empty;
                    foreach (var detail in details)
                    {
                        var material = rsMaterial.FirstOrDefault(t => t.Id == detail.MaterialRefId);
                        var unit = rsUnits.FirstOrDefault(t => t.Id == detail.UnitRefId);
                        //string qtyString = fpDetail.PrintFormatRight(detail.IssueQty.ToString("######0.00"), 10);

                        //detailrecord = locationCode + "," + fpDetail.PrintFormatLeft(material.MaterialPetName, 30) + ",OTRAN," + trnLocationCode + "," + stockDateFormated + "," + fpDetail.PrintFormatLeft(rsTranMaster.Id.ToString(), 20) + "," + qtyString + "," + unit.Name;
                        if (material.StockAdjustmentUnitId == detail.UnitRefId)
                        {
                        }
                        else
                        {
                            decimal conversionFactor = 0;
                            var unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == detail.UnitRefId && t.RefUnitId == material.StockAdjustmentUnitId && t.MaterialRefId == material.Id);
                            if (unitConversion == null)
                                unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == detail.UnitRefId && t.RefUnitId == material.StockAdjustmentUnitId);

                            if (unitConversion != null)
                            {
                                conversionFactor = unitConversion.Conversion;
                            }
                            else
                            {
                                unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == material.StockAdjustmentUnitId && t.RefUnitId == detail.UnitRefId && t.MaterialRefId == material.Id);
                                if (unitConversion == null)
                                    unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == material.StockAdjustmentUnitId && t.RefUnitId == detail.UnitRefId);
                                if (unitConversion == null)
                                {
                                    var baseUnit = await _unitrepo.FirstOrDefaultAsync(t => t.Id == material.StockAdjustmentUnitId);
                                    var refUnit = await _unitrepo.FirstOrDefaultAsync(t => t.Id == detail.UnitRefId);
                                    throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
                                }
                                conversionFactor = 1 / unitConversion.Conversion;
                            }
                            detail.IssueQty = detail.IssueQty * conversionFactor;
                            unit = rsUnits.FirstOrDefault(t => t.Id == material.StockAdjustmentUnitId);
                        }
                        detailrecord = locationCode + "," + material.MaterialPetName + ",OTRN," + trnLocationCode + "," + stockDateFormated + "," + rsTranMaster.DocReferenceNumber + "," + detail.IssueQty.ToString("0.00") + "," + unit.Name;
                        rows.Add(detailrecord);
                        detailFile.Append(detailrecord);
                        detailFile.AppendLine();
                    }
                }

                foreach (var lst in transferInDtos)
                {
                    var rsTranMaster = lst;
                    var details = interTransferReceivedDetail.Where(t => t.InterTransferRefId == lst.Id && t.ReceivedQty > 0);

                    stockDateFormated = rsTranMaster.ReceivedTime.Value.ToString("yyyy-MM-dd");
                    var trnLocation = rsLocation.FirstOrDefault(t => t.Id == rsTranMaster.RequestLocationRefId);
                    string trnLocationCode = fpHeader.PrintFormatRight(trnLocation.Code, 5);

                    string detailrecord = string.Empty;
                    foreach (var detail in details)
                    {
                        var material = rsMaterial.FirstOrDefault(t => t.Id == detail.MaterialRefId);
                        var unit = rsUnits.FirstOrDefault(t => t.Id == detail.UnitRefId);
                        //string qtyString = fpDetail.PrintFormatRight(detail.ReceivedQty.ToString("######0.00"), 10);

                        //detailrecord = locationCode + "," + fpDetail.PrintFormatLeft(material.MaterialPetName, 30) + ",ITRAN," + trnLocationCode + "," + stockDateFormated + "," + fpDetail.PrintFormatLeft(rsTranMaster.Id.ToString(), 20) + "," + qtyString + "," + unit.Name;
                        if (material.StockAdjustmentUnitId == detail.UnitRefId)
                        {
                        }
                        else
                        {
                            decimal conversionFactor = 0;
                            var unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == detail.UnitRefId && t.RefUnitId == material.StockAdjustmentUnitId && t.MaterialRefId == material.Id);

                            if (unitConversion == null)
                                unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == detail.UnitRefId && t.RefUnitId == material.StockAdjustmentUnitId);

                            if (unitConversion != null)
                            {
                                conversionFactor = unitConversion.Conversion;
                            }
                            else
                            {
                                unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == material.StockAdjustmentUnitId && t.RefUnitId == detail.UnitRefId && t.MaterialRefId == material.Id);
                                if (unitConversion == null)
                                    unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == material.StockAdjustmentUnitId && t.RefUnitId == detail.UnitRefId);

                                if (unitConversion == null)
                                {
                                    var baseUnit = await _unitrepo.FirstOrDefaultAsync(t => t.Id == material.StockAdjustmentUnitId);
                                    var refUnit = await _unitrepo.FirstOrDefaultAsync(t => t.Id == detail.UnitRefId);
                                    throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
                                }
                                conversionFactor = 1 / unitConversion.Conversion;
                            }
                            detail.ReceivedQty = detail.ReceivedQty * conversionFactor;
                            unit = rsUnits.FirstOrDefault(t => t.Id == material.StockAdjustmentUnitId);
                        }
                        detailrecord = locationCode + "," + material.MaterialPetName + ",ITRN," + trnLocationCode + "," + stockDateFormated + "," + rsTranMaster.DocReferenceNumber + "," + detail.ReceivedQty.ToString("0.00") + "," + unit.Name;
                        rows.Add(detailrecord);
                        detailFile.Append(detailrecord);
                        detailFile.AppendLine();
                    }
                }

                #endregion Master Record

                //try
                //{
                //    using (StreamWriter outfile = new StreamWriter(fileNameHeader, false))
                //    {
                //        outfile.Write(detailFile.ToString());
                //    }
                //}
                //catch (Exception ex)
                //{
                //    output.ErrorMessage = ex.Message;
                //    return output;
                //}
            }
            catch (Exception ex)
            {
                output.ErrorMessage = ex.Message;
                return output;
            }
            output.Rows = rows;
            return output;
        }

        public async Task<ApiBatchGoodsTextFile> ApiGoodsReceivedReturnedBatchFileForGivenDatePeriod(ApiPhysicalStockInputDto input)
        {
            //GoodsReceivedReturned
            ApiBatchGoodsTextFile output = new ApiBatchGoodsTextFile();
            var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
            if (location == null)
            {
                output.ErrorMessage = "Location Not Exists For Id :" + input.LocationRefId;
                return output;
            }
            var tenant = await GetCurrentTenantAsync();
            var tenantName = tenant.TenancyName;

            var rsUnits = await _unitrepo.GetAllListAsync();

            StringBuilder masterFile = new StringBuilder();
            StringBuilder detailFile = new StringBuilder();
            List<string> masterRows = new List<string>();
            List<string> detailRows = new List<string>();

            try
            {
                List<int> materialRefIds = new List<int>();

                #region Get Data

                var invMaster = await _invoiceRepo.GetAllListAsync(t => t.LocationRefId == input.LocationRefId && DbFunctions.TruncateTime(t.AccountDate) == DbFunctions.TruncateTime(input.StockDate));
                var invMasterListdto = invMaster.MapTo<List<InvoiceListDto>>();
                List<GetInvoiceForEditOutput> invoiceList = new List<GetInvoiceForEditOutput>();
                foreach (var lst in invMasterListdto)
                {
                    var tempInvoice = await _invoiceAppService.GetInvoiceForEdit(new NullableIdInput { Id = lst.Id });
                    invoiceList.Add(tempInvoice);
                    materialRefIds.AddRange(tempInvoice.InvoiceDetail.Select(t => t.MaterialRefId).ToList());
                }

                var purchaseReturnMaster = await _purchaseReturn.GetAllListAsync(t => t.LocationRefId == input.LocationRefId && DbFunctions.TruncateTime(t.ReturnDate) == DbFunctions.TruncateTime(input.StockDate));
                var purchaseReturnListdto = purchaseReturnMaster.MapTo<List<PurchaseReturnListDto>>();
                List<GetPurchaseReturnForEditOutput> purchaseReturnList = new List<GetPurchaseReturnForEditOutput>();
                foreach (var lst in purchaseReturnListdto)
                {
                    var tempPurchaseReturn = await _purchaseReturnAppService.GetPurchaseReturnForEdit(new NullableIdInput { Id = lst.Id });
                    purchaseReturnList.Add(tempPurchaseReturn);
                    materialRefIds.AddRange(tempPurchaseReturn.PurchaseReturnDetail.Select(t => t.MaterialRefId).ToList());
                }

                var rsMaterial = await _materialRepo.GetAllListAsync(t => materialRefIds.Contains(t.Id));

                #endregion Get Data

                var fpHeader = new TextFileWriter();
                var fpDetail = new TextFileWriter();

                var subPath = "~/Report/" + tenantName + "/";
                var exists = Directory.Exists(HttpContext.Current.Server.MapPath(subPath));

                if (!exists)
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(subPath));

                //var fileNameHeader =
                //    HttpContext.Current.Server.MapPath("~/Report/" + tenantName + "/Goods_Received_H_" + location.Code + input.StockDate.ToString("yyyyMMdd") + ".TXT");

                //var fileNameDetail =
                //    HttpContext.Current.Server.MapPath("~/Report/" + tenantName + "/Goods_Received_D_" + location.Code + input.StockDate.ToString("yyyyMMdd") + ".TXT");
                var fileNameHeader =
                    "Goods_Received_H_" + location.Code + input.StockDate.ToString("yyyyMMdd") + ".TXT";

                var fileNameDetail =
                    "Goods_Received_D_" + location.Code + input.StockDate.ToString("yyyyMMdd") + ".TXT";

                string locationCode = fpHeader.PrintFormatRight(location.Code, 5);
                string stockDateFormated = input.StockDate.ToString("yyyy-MM-dd");
                //fpHeader.FileOpen(fileNameHeader, "O");
                //fpDetail.FileOpen(fileNameDetail, "O");
                string masterrecord = string.Empty;
                var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();

                #region Master Record

                foreach (var lst in invoiceList)
                {
                    var invoice = lst.Invoice;
                    var invoiceDetail = lst.InvoiceDetail;

                    stockDateFormated = invoice.AccountDate.Value.ToString("yyyy-MM-dd");

                    //masterrecord = locationCode + "," + stockDateFormated + "," + fpHeader.PrintFormatRight(invoice.Id.ToString(), 7) + ",0," + fpHeader.PrintFormatLeft(invoice.SupplierRefId.ToString(), 30) + "," + fpHeader.PrintFormatRight(invoice.InvoiceNumber, 30) + "," + fpHeader.PrintFormatRight("", 30);
                    string idFormat = "0" + invoice.Id.ToString().Trim().PadLeft(6, '0');
                    string invRefNumber = invoice.InvoiceNumberReferenceFromOtherErp;
                    if (invRefNumber.Length > 30)
                        invRefNumber = invRefNumber.Left(30);
                    string poRefNumber = invoice.PurchaseOrderReferenceFromOtherErp;
                    if (poRefNumber.Length > 30)
                        poRefNumber = poRefNumber.Left(30);
                    masterrecord = locationCode + "," + stockDateFormated + "," + idFormat + ",0," + invoice.SupplierCode + "," + invRefNumber + "," + poRefNumber;
                    masterRows.Add(masterrecord);
                    masterFile.Append(masterrecord);
                    masterFile.AppendLine();

                    //fpHeader.PrintTextLine(masterrecord);

                    string detailrecord = string.Empty;
                    foreach (var invDetail in invoiceDetail)
                    {
                        var material = rsMaterial.FirstOrDefault(t => t.Id == invDetail.MaterialRefId);
                        var unit = rsUnits.FirstOrDefault(t => t.Id == invDetail.UnitRefId);
                        //string qtyString = fpDetail.PrintFormatRight(invDetail.TotalQty.ToString("######0.00"), 10);
                        //string unitPriceString = fpDetail.PrintFormatRight(invDetail.Price.ToString("######0.00"), 10);
                        //string taxAmountString = fpDetail.PrintFormatRight(invDetail.TaxAmount.ToString("######0.00"), 10);
                        //string totalAmountSring = fpDetail.PrintFormatRight(invDetail.TotalAmount.ToString("######0.00"), 10);

                        //detailrecord = locationCode + "," + stockDateFormated + "," + fpDetail.PrintFormatRight(invoice.InvoiceNumber, 30) + ",0," + fpDetail.PrintFormatRight(invDetail.Sno.ToString(), 11) + "," + fpDetail.PrintFormatLeft(material.MaterialPetName, 30) + "," + qtyString + "," + unit.Name + "," + unitPriceString + "," + taxAmountString + "," + totalAmountSring;

                        decimal totalQty = invDetail.TotalQty;
                        string uom = unit.Name;
                        decimal unitPrice = invDetail.Price;
                        decimal taxAmount = invDetail.TaxAmount;
                        decimal totalAmount = invDetail.TotalAmount;

                        if (material.DefaultUnitId != invDetail.UnitRefId)
                        {
                            var unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == invDetail.UnitRefId && t.RefUnitId == material.DefaultUnitId && t.MaterialRefId == material.Id);
                            if (unitConversion == null)
                                unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == invDetail.UnitRefId && t.RefUnitId == material.DefaultUnitId);
                            if (unitConversion == null)
                                throw new UserFriendlyException(L("UnitConversion") + " " + L("NotExist") + " - " + material.MaterialName);
                            unit = rsUnits.FirstOrDefault(t => t.Id == material.DefaultUnitId);
                            totalQty = unitConversion.Conversion * totalQty;
                            uom = unit.Name;
                            if (unitPrice != 0)
                                unitPrice = unitPrice * 1 / unitConversion.Conversion;
                            if (taxAmount != 0)
                                taxAmount = taxAmount * 1 / unitConversion.Conversion;
                            if (totalAmount != 0)
                                totalAmount = totalAmount * 1 / unitConversion.Conversion;
                        }

                        detailrecord = locationCode + "," + stockDateFormated + "," + invRefNumber + ",0," + invDetail.Sno.ToString() + "," + material.MaterialPetName + "," + totalQty.ToString("0.00") + "," + uom + "," + unitPrice.ToString("0.00") + "," + taxAmount.ToString("0.00") + "," + totalAmount.ToString("0.00");
                        detailRows.Add(detailrecord);
                        //fpDetail.PrintTextLine(detailrecord);
                        detailFile.Append(detailrecord);
                        detailFile.AppendLine();
                    }
                }

                foreach (var lst in purchaseReturnList)
                {
                    var returnMaster = lst.PurchaseReturn;
                    var returnDetail = lst.PurchaseReturnDetail;

                    stockDateFormated = returnMaster.ReturnDate.ToString("yyyy-MM-dd");

                    //masterrecord = locationCode + "," + stockDateFormated + "," + fpHeader.PrintFormatRight(returnMaster.Id.ToString(), 7) + ",1," + fpHeader.PrintFormatLeft(returnMaster.SupplierRefId.ToString(), 30) + "," + fpHeader.PrintFormatRight(returnMaster.ReferenceNumber, 30) + "," + fpHeader.PrintFormatRight("", 30);
                    string idFormat = "1" + returnMaster.Id.ToString().Trim().PadLeft(6, '0');
                    masterrecord = locationCode + "," + stockDateFormated + "," + idFormat + ",1," + returnMaster.SupplierCode + "," + returnMaster.InvoiceNumberReferenceFromOtherErp + "," + returnMaster.PurchaseOrderReferenceFromOtherErp;
                    masterRows.Add(masterrecord);
                    masterFile.Append(masterrecord);
                    masterFile.AppendLine();

                    string detailrecord = string.Empty;
                    foreach (var detail in returnDetail)
                    {
                        var material = rsMaterial.FirstOrDefault(t => t.Id == detail.MaterialRefId);
                        var unit = rsUnits.FirstOrDefault(t => t.Id == detail.UnitRefId);
                        //string qtyString = fpDetail.PrintFormatRight(detail.Quantity.ToString("######0.00"), 10);
                        //string unitPriceString = fpDetail.PrintFormatRight(detail.Price.ToString("######0.00"), 10);
                        //string taxAmountString = fpDetail.PrintFormatRight(detail.TaxAmount.ToString("######0.00"), 10);
                        //string totalAmountSring = fpDetail.PrintFormatRight(detail.TotalAmount.ToString("######0.00"), 10);

                        //detailrecord = locationCode + "," + stockDateFormated + "," + fpDetail.PrintFormatRight(returnMaster.ReferenceNumber, 30) + ",1," + fpDetail.PrintFormatRight(detail.Sno.ToString(), 11) + "," + fpDetail.PrintFormatLeft(material.MaterialPetName, 30) + "," + qtyString + "," + unit.Name + "," + unitPriceString + "," + taxAmountString + "," + totalAmountSring;
                        decimal totalQty = detail.Quantity;
                        string uom = unit.Name;
                        decimal unitPrice = detail.Price;
                        decimal taxAmount = detail.TaxAmount;
                        decimal totalAmount = detail.TotalAmount;

                        if (material.DefaultUnitId != detail.UnitRefId)
                        {
                            var unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == detail.UnitRefId && t.RefUnitId == material.DefaultUnitId && t.MaterialRefId == material.Id);
                            if (unitConversion == null)
                                unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == detail.UnitRefId && t.RefUnitId == material.DefaultUnitId);
                            if (unitConversion == null)
                                throw new UserFriendlyException(L("UnitConversion") + " " + L("NotExist") + " - " + material.MaterialName);
                            unit = rsUnits.FirstOrDefault(t => t.Id == material.DefaultUnitId);
                            totalQty = unitConversion.Conversion * totalQty;
                            uom = unit.Name;
                            if (unitPrice != 0)
                                unitPrice = unitPrice * 1 / unitConversion.Conversion;
                            if (taxAmount != 0)
                                taxAmount = taxAmount * 1 / unitConversion.Conversion;
                            if (totalAmount != 0)
                                totalAmount = totalAmount * 1 / unitConversion.Conversion;
                        }

                        detailrecord = locationCode + "," + stockDateFormated + "," + returnMaster.InvoiceNumberReferenceFromOtherErp + ",1," + detail.Sno.ToString() + "," + material.MaterialPetName + "," + totalQty.ToString("0.00") + "," + uom + "," + unitPrice.ToString("0.00") + "," + taxAmount.ToString("0.00") + "," + totalAmount.ToString("0.00");
                        detailRows.Add(detailrecord);
                        detailFile.Append(detailrecord);
                        detailFile.AppendLine();
                    }
                }

                #endregion Master Record

                #region File Output Print

                //try
                //{
                //    using (StreamWriter outfile = new StreamWriter(fileNameHeader, false))
                //    {
                //        outfile.Write(masterFile.ToString());
                //    }
                //}
                //catch (Exception ex)
                //{
                //    output.ErrorMessage = ex.Message;
                //    return output;
                //}

                //try
                //{
                //    using (StreamWriter outfile = new StreamWriter(fileNameDetail, false))
                //    {
                //        outfile.Write(detailFile.ToString());
                //    }
                //}
                //catch (Exception ex)
                //{
                //    output.ErrorMessage = ex.Message;
                //    return output;
                //}
                //output.MasterFileName = fileNameHeader;

                #endregion File Output Print

                output.MasterRows = masterRows;
                //output.MasterOutputMessage = masterFile.ToString();
                //output.MasterFileOutput = masterFile;

                //output.DetailFileName = fileNameDetail;
                output.DetailRows = detailRows;
                //output.DetailOutputMessage = detailFile.ToString();
                //output.DetailFileOutput = detailFile;
            }
            catch (Exception ex)
            {
                output.ErrorMessage = ex.Message;
                return output;
            }

            return output;
        }

        public async Task<FileDownLoadDto> GetClosingStockVarianceBeforeOrAfterDayCloseReportExcel(GetHouseReportInput input)
        {
            input.MaxResultCount = AppConsts.MaxPageSize;
            input.SkipCount = 0;
            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
            if (loc == null)
            {
                throw new UserFriendlyException(L("LocationError"));
            }
            string locationName = loc.Name.PadRight(10, '-');

            GetStockSummaryDtoOutput outputClosingStock = await GetStockSummary(input);

            List<MaterialLedgerDto> StockSummary = outputClosingStock.StockSummary;

            bool materialCodeExists = false;
            if (PermissionChecker.IsGranted("Pages.Tenant.House.Master.Material.PrintMaterialCode"))
            {
                materialCodeExists = true;
            }

            var fileExcel = await _housereportExporter.VarianceReportBeforeOrAfterDayClose(StockSummary, locationName, input.StartDate, input.EndDate, materialCodeExists, outputClosingStock.VarianceStatusBeforeOrAfterStatus, input.ExportType);

            FileDownLoadDto output = new FileDownLoadDto();
            output.ExcelFile = fileExcel;
            return output;
        }

        public async Task<FileDto> GetClosingStockReportGroupOrCategoryOrMaterialWiseForGivenInputToExcel(ClosingStockExportGroupCategoryMaterialDto input)
        {
            var result = await GetClosingStockReportGroupOrCategoryOrMaterialWiseForGivenInput(input);
            result.InputFilterDto = input;
            if (result.MaterialWisePurchaseDtos.Count == 0)
            {
                throw new UserFriendlyException(L("NoMoreRecordsFound"));
            }
            return _housereportExporter.ExportClosingStockGroupCategoryMaterials(result);
        }

        public async Task<LocationWiseClosingStockGroupCateogryMaterialDto> GetClosingStockReportGroupOrCategoryOrMaterialWiseForGivenInput(ClosingStockExportGroupCategoryMaterialDto input)
        {
            if (input.TenantId == 0)
            {
                if (AbpSession.TenantId.HasValue)
                    input.TenantId = (int)AbpSession.TenantId;
                else
                    throw new UserFriendlyException("Tenant Id is wrong");
            }

            #region Location Filter
            List<int> selectedLocationRefIds = new List<int>();
            if (input.Locations != null && input.Locations.Any())
            {
                selectedLocationRefIds = input.Locations.Select(a => a.Id).ToList();
            }
            else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
                     && !input.LocationGroup.Locations.Any())
            {
                selectedLocationRefIds = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
            }
            else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                selectedLocationRefIds = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
            }
            else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                selectedLocationRefIds = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true
                });
            }
            else if (input.LocationGroup == null)
            {
                if (input.UserId > 0)
                {
                    selectedLocationRefIds = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                    {
                        Locations = new List<SimpleLocationDto>(),
                        Group = false,
                        UserId = input.UserId
                    });
                }
            }

            #endregion

            #region Group Category Material Filter
            List<int> arrMateriaRefIds = new List<int>();
            if (input.MaterialList.Count > 0)
            {
                arrMateriaRefIds = input.MaterialList.Select(t => int.Parse(t.Value)).ToList();
                input.ExportMaterialAlso = true;
            }
            List<int> arrGroupRefIds = new List<int>();
            if (input.GroupList.Count > 0)
            {
                arrGroupRefIds = input.GroupList.Select(t => int.Parse(t.Value)).ToList();
            }
            List<int> arrCategoryRefIds = new List<int>();
            if (input.CategoryList.Count > 0)
            {
                arrCategoryRefIds = input.CategoryList.Select(t => int.Parse(t.Value)).ToList();
            }

            var materialQueryable = _materialRepo.GetAll();
            if (input.TenantId > 0 && input.RunInBackGround)
                materialQueryable = materialQueryable.Where(t => t.TenantId == input.TenantId);

            var allItems = await (from mat in materialQueryable
                                    .WhereIf(arrMateriaRefIds.Count > 0, t => arrMateriaRefIds.Contains(t.Id))
                                  join matCat in _materialGroupCategoryRepo.GetAll()
                                    .WhereIf(arrCategoryRefIds.Count > 0, t => arrCategoryRefIds.Contains(t.Id))
                                      on mat.MaterialGroupCategoryRefId equals matCat.Id
                                  join matGrp in _materialGroupRepo.GetAll().WhereIf(arrGroupRefIds.Count > 0, t => arrGroupRefIds.Contains(t.Id))
                                      on matCat.MaterialGroupRefId equals matGrp.Id
                                  select new InvoiceCategoryWisePurchaseOrderAnalysisDto
                                  {
                                      MaterialGroupRefId = matGrp.Id,
                                      MaterialGroupRefName = matGrp.MaterialGroupName,
                                      MaterialGroupCategoryRefId = matCat.Id,
                                      MaterialGroupCategoryRefName = matCat.MaterialGroupCategoryName,
                                      MaterialRefId = mat.Id,
                                      MaterialRefName = mat.MaterialName,
                                      MaterialPetName = mat.MaterialPetName,
                                  }).ToListAsync();
            arrMateriaRefIds = allItems.Select(t => t.MaterialRefId).ToList();

            #endregion

            List<GetStockSummaryDtoOutput> getStockSummaryDtoOutputs = new List<GetStockSummaryDtoOutput>();
            List<LocationWiseHeadWiseStock> outputDto = new List<LocationWiseHeadWiseStock>();
            var locationQueryable = _locationRepo.GetAll();
            if (input.RunInBackGround == true)
                locationQueryable = locationQueryable.Where(t => t.TenantId == input.TenantId);

            var rsLocations = await locationQueryable.Where(t => selectedLocationRefIds.Contains(t.Id)).ToListAsync();
            rsLocations = rsLocations.OrderBy(t => t.AvgPriceLocationRefId).ToList();
            int prevLocPriceId = 0;
            List<MaterialRateViewListDto> prevLocationRateView = new List<MaterialRateViewListDto>();
            foreach (var locId in selectedLocationRefIds)
            {
                var loc = rsLocations.FirstOrDefault(t => t.Id == locId);
                if (prevLocPriceId != loc.AvgPriceLocationRefId)
                {
                    prevLocationRateView = new List<MaterialRateViewListDto>();
                }
                var stockSummary = await GetStockSummary_Version1(new GetHouseReportInput
                {
                    LocationRefId = locId,
                    StartDate = input.EndDate,
                    EndDate = input.EndDate,
                    MaterialRefIds = arrMateriaRefIds,
                    MaterialRateView = prevLocationRateView,
                    LiveStock = false,
                    RunInBackGround = input.RunInBackGround,
                    TenantId = input.TenantId
                });
                getStockSummaryDtoOutputs.Add(stockSummary);
                prevLocationRateView = stockSummary.MaterialRateView;
                prevLocPriceId = loc.AvgPriceLocationRefId.HasValue ? loc.AvgPriceLocationRefId.Value : loc.Id;
            }

            foreach (var stockList in getStockSummaryDtoOutputs)
            {
                if (stockList.StockSummary.Count == 0)
                    continue;
                var gp = stockList.StockSummary;
                LocationWiseHeadWiseStock newLocationConsolidatedDto = new LocationWiseHeadWiseStock
                {
                    LocationRefId = gp.FirstOrDefault().LocationRefId,
                    LocationRefName = rsLocations.FirstOrDefault(t => t.Id == gp.FirstOrDefault().LocationRefId).Name
                };

                foreach (var sublst in gp.GroupBy(t => new { t.MaterialRefId, t.MaterialName }))
                {
                    MaterialWisePurchaseDto materialDto = new MaterialWisePurchaseDto
                    {
                        MaterialRefId = sublst.Key.MaterialRefId,
                        MaterialRefName = sublst.Key.MaterialName,
                        MaterialGroupRefId = gp.FirstOrDefault().MaterialGroupRefId,
                        MaterialGroupCategoryRefId = gp.FirstOrDefault().MaterialGroupCategoryRefId,
                        TotalAmount = sublst.Sum(t => t.StockValue)
                    };
                    newLocationConsolidatedDto.MaterialWisePurchaseDtos.Add(materialDto);
                }

                foreach (var sublst in gp.GroupBy(t => new { t.MaterialGroupCategoryRefId, t.MaterialGroupCategoryRefName }))
                {
                    MaterialGroupCategoryWisePurchaseDto groupCategoryDto = new MaterialGroupCategoryWisePurchaseDto
                    {
                        MaterialGroupCategoryRefId = sublst.Key.MaterialGroupCategoryRefId,
                        MaterialGroupCategoryRefName = sublst.Key.MaterialGroupCategoryRefName,
                        TotalAmount = sublst.Sum(t => t.StockValue)
                    };
                    newLocationConsolidatedDto.MaterialGroupCategoryWisePurchaseDtos.Add(groupCategoryDto);
                }

                foreach (var sublst in gp.GroupBy(t => new { t.MaterialGroupRefId, t.MaterialGroupRefName }))
                {
                    MaterialGroupWisePurchaseDto groupDto = new MaterialGroupWisePurchaseDto
                    {
                        MaterialGroupRefId = sublst.Key.MaterialGroupRefId,
                        MaterialGroupRefName = sublst.Key.MaterialGroupRefName,
                        TotalAmount = sublst.Sum(t => t.StockValue)
                    };
                    newLocationConsolidatedDto.MaterialGroupWisePurchaseDtos.Add(groupDto);
                }
                outputDto.Add(newLocationConsolidatedDto);
            }

            LocationWiseClosingStockGroupCateogryMaterialDto output = new LocationWiseClosingStockGroupCateogryMaterialDto();

            List<MaterialWisePurchaseDto> consolidatedMaterialWisePurchaseDtos = new List<MaterialWisePurchaseDto>();
            List<MaterialGroupCategoryWisePurchaseDto> consolidatedMaterialGroupCategoryWisePurchaseDtos = new List<MaterialGroupCategoryWisePurchaseDto>();
            List<MaterialGroupWisePurchaseDto> consolidatedMaterialGroupWisePurchaseDtos = new List<MaterialGroupWisePurchaseDto>();
            foreach (var lst in outputDto)
            {
                consolidatedMaterialWisePurchaseDtos.AddRange(lst.MaterialWisePurchaseDtos);
                consolidatedMaterialGroupCategoryWisePurchaseDtos.AddRange(lst.MaterialGroupCategoryWisePurchaseDtos);
                consolidatedMaterialGroupWisePurchaseDtos.AddRange(lst.MaterialGroupWisePurchaseDtos);
            }

            foreach (var sublst in consolidatedMaterialWisePurchaseDtos.GroupBy(t => new { t.MaterialRefId, t.MaterialRefName }))
            {
                MaterialWisePurchaseDto materialDto = new MaterialWisePurchaseDto
                {
                    MaterialRefId = sublst.Key.MaterialRefId,
                    MaterialRefName = sublst.Key.MaterialRefName,
                    TotalAmount = sublst.Sum(t => t.TotalAmount)
                };
                output.MaterialWisePurchaseDtos.Add(materialDto);
            }

            foreach (var sublst in consolidatedMaterialGroupCategoryWisePurchaseDtos.GroupBy(t => new { t.MaterialGroupCategoryRefId, t.MaterialGroupCategoryRefName }))
            {
                MaterialGroupCategoryWisePurchaseDto groupCategoryDto = new MaterialGroupCategoryWisePurchaseDto
                {
                    MaterialGroupCategoryRefId = sublst.Key.MaterialGroupCategoryRefId,
                    MaterialGroupCategoryRefName = sublst.Key.MaterialGroupCategoryRefName,
                    TotalAmount = sublst.Sum(t => t.TotalAmount)
                };
                output.MaterialGroupCategoryWisePurchaseDtos.Add(groupCategoryDto);
            }

            foreach (var sublst in consolidatedMaterialGroupWisePurchaseDtos.GroupBy(t => new { t.MaterialGroupRefId, t.MaterialGroupRefName }))
            {
                MaterialGroupWisePurchaseDto groupDto = new MaterialGroupWisePurchaseDto
                {
                    MaterialGroupRefId = sublst.Key.MaterialGroupRefId,
                    MaterialGroupRefName = sublst.Key.MaterialGroupRefName,
                    TotalAmount = sublst.Sum(t => t.TotalAmount)
                };
                output.MaterialGroupWisePurchaseDtos.Add(groupDto);
            }

            output.LocationWiseHeadWiseStockList = outputDto;
            return output;
        }


        public async Task<FileDto> GetInterTransferReceivedDetailReportMaterialWiseForGivenInputToExcel(ClosingStockExportGroupCategoryMaterialDto input)
        {
            var result = await GetInterTransferReceivedDetailReportForGivenMaterialWise(input);
            result.InputFilterDto = input;
            if (result.InterTransferReceivedReportDtos.Count == 0)
            {
                throw new UserFriendlyException(L("NoMoreRecordsFound"));
            }
            return _housereportExporter.ExportInterTransferReceivedDetailReportForGivenMaterialWise(result);
        }

        public async Task<InterTransferReceivedExportReportDto> GetInterTransferReceivedDetailReportForGivenMaterialWise(ClosingStockExportGroupCategoryMaterialDto input)
        {
            var intertransferMaster = _intertransferRepo.GetAll().Where(t => t.ReceivedTime.HasValue);

            #region Date & Supplier Filter
            if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
            {
                intertransferMaster =
                    intertransferMaster.Where(
                        a => DbFunctions.TruncateTime(a.ReceivedTime) >= DbFunctions.TruncateTime(input.StartDate)
                             && DbFunctions.TruncateTime(a.ReceivedTime) <= DbFunctions.TruncateTime(input.EndDate));
            }

            #endregion

            #region Location Filter
            List<int> selectedLocationRefIds = new List<int>();
            if (input.Locations != null && input.Locations.Any())
            {
                selectedLocationRefIds = input.Locations.Select(a => a.Id).ToList();
            }
            else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
                     && !input.LocationGroup.Locations.Any())
            {
                selectedLocationRefIds = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
            }
            else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                selectedLocationRefIds = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
            }
            else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                selectedLocationRefIds = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true
                });
            }
            else if (input.LocationGroup == null)
            {
                if (input.UserId > 0)
                {
                    selectedLocationRefIds = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                    {
                        Locations = new List<SimpleLocationDto>(),
                        Group = false,
                        UserId = input.UserId
                    });
                }
            }
            intertransferMaster = intertransferMaster.Where(a => selectedLocationRefIds.Contains(a.LocationRefId));

            #endregion

            var intertransferRecivedDetail = _intertransferReceivedRepo.GetAll();

            #region Group Category Material Filter
            List<int> arrGroupRefIds = new List<int>();
            if (input.GroupList.Count > 0)
            {
                arrGroupRefIds = input.GroupList.Select(t => int.Parse(t.Value)).ToList();
            }
            List<int> arrCategoryRefIds = new List<int>();
            if (input.CategoryList.Count > 0)
            {
                arrCategoryRefIds = input.CategoryList.Select(t => int.Parse(t.Value)).ToList();
            }
            List<int> arrMateriaRefIds = new List<int>();
            if (input.MaterialList.Count > 0)
            {
                arrMateriaRefIds = input.MaterialList.Select(t => int.Parse(t.Value)).ToList();
                input.ExportMaterialAlso = true;
            }
            #endregion

            var allItems = await (from interTrasnfer in intertransferMaster
                                  join recdDetail in intertransferRecivedDetail on interTrasnfer.Id equals recdDetail.InterTransferRefId
                                  join mat in _materialRepo.GetAll().WhereIf(arrMateriaRefIds.Count > 0, t => arrMateriaRefIds.Contains(t.Id))
                                      on recdDetail.MaterialRefId equals mat.Id
                                  join matCat in _materialGroupCategoryRepo.GetAll().WhereIf(arrCategoryRefIds.Count > 0, t => arrCategoryRefIds.Contains(t.Id))
                                      on mat.MaterialGroupCategoryRefId equals matCat.Id
                                  join matGrp in _materialGroupRepo.GetAll().WhereIf(arrGroupRefIds.Count > 0, t => arrGroupRefIds.Contains(t.Id))
                                      on matCat.MaterialGroupRefId equals matGrp.Id
                                  join loc in _locationRepo.GetAll() on interTrasnfer.LocationRefId equals loc.Id
                                  join reqLoc in _locationRepo.GetAll() on interTrasnfer.RequestLocationRefId equals reqLoc.Id
                                  join recdUnit in _unitrepo.GetAll() on recdDetail.UnitRefId equals recdUnit.Id
                                  join trfUnit in _unitrepo.GetAll() on mat.TransferUnitId equals trfUnit.Id
                                  select new InterTransferReceivedGroupCategoryMaterialWiseAnalysisDto
                                  {
                                      LocationRefId = interTrasnfer.LocationRefId,
                                      LocationRefCode = loc.Code,
                                      LocationRefName = loc.Name,
                                      ReceivedTime = interTrasnfer.ReceivedTime.Value,
                                      RequestLocationRefId = interTrasnfer.RequestLocationRefId,
                                      RequestLocationRefName = reqLoc.Code,
                                      InterTrasnferRefId = interTrasnfer.Id,
                                      MaterialGroupRefId = matGrp.Id,
                                      MaterialGroupRefName = matGrp.MaterialGroupName,
                                      MaterialGroupCategoryRefId = matCat.Id,
                                      MaterialGroupCategoryRefName = matCat.MaterialGroupCategoryName,
                                      MaterialRefId = mat.Id,
                                      MaterialRefName = mat.MaterialName,
                                      MaterialPetName = mat.MaterialPetName,
                                      ReceivedQty = recdDetail.ReceivedQty,
                                      UnitRefId = recdDetail.UnitRefId,
                                      UnitRefName = recdUnit.Name,
                                      Price = recdDetail.Price,
                                      DefaultUnitId = mat.DefaultUnitId,
                                      TransferUnitId = mat.TransferUnitId,
                                      TransferUnitName = trfUnit.Name,
                                      TotalAmount = Math.Round(recdDetail.Price * recdDetail.ReceivedQty, 2)
                                  }).ToListAsync();
            allItems = allItems.OrderBy(t => t.ReceivedTime).ToList();
            List<InterTransferReceivedReportDto> outputDto = new List<InterTransferReceivedReportDto>();
            var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();

            foreach (var gp in allItems.GroupBy(t => new { t.MaterialRefId, t.UnitRefId, t.TransferUnitId }))
            {
                if (gp.Key.UnitRefId == gp.Key.TransferUnitId)
                    continue;
                var unitConversion = await _materialAppService.GetConversion(new GetConversionBasedOnBaseUnitRefUnit
                {
                    BaseUnitId = gp.Key.UnitRefId,
                    RefUnitId = gp.Key.TransferUnitId,
                    ConversionUnitList = rsUc,
                    MaterialRefId = gp.Key.MaterialRefId,
                });
                if (unitConversion == null)
                {

                }
                else
                {
                    foreach (var lst in gp.ToList())
                    {
                        lst.Price = lst.Price * unitConversion.Conversion;
                        lst.ReceivedQty = lst.ReceivedQty * unitConversion.Conversion;
                        lst.TotalAmount = Math.Round(lst.Price * lst.ReceivedQty, 2);
                    }
                }
            }

            foreach (var gp in allItems.GroupBy(t => new { t.LocationRefId, t.MaterialRefId }))
            {
                InterTransferReceivedReportDto newDto = new InterTransferReceivedReportDto
                {
                    LocationRefId = gp.Key.LocationRefId,
                    LocationRefCode = gp.FirstOrDefault().LocationRefCode,
                    MaterialGroupRefId = gp.FirstOrDefault().MaterialGroupRefId,
                    MaterialGroupCategoryRefId = gp.FirstOrDefault().MaterialGroupCategoryRefId,
                    MaterialGroupRefName = gp.FirstOrDefault().MaterialGroupRefName,
                    MaterialGroupCategoryRefName = gp.FirstOrDefault().MaterialGroupCategoryRefName,
                    MaterailRefName = gp.FirstOrDefault().MaterialRefName,
                    MaterialRefId = gp.FirstOrDefault().MaterialRefId,
                    TotalRecdQty = gp.Sum(t => t.ReceivedQty),
                    TotalRecdValue = gp.Sum(t => t.ReceivedValue)
                };

                if (newDto.TotalRecdValue > 0)
                {
                    newDto.AvgPrice = Math.Round(newDto.TotalRecdValue / newDto.TotalRecdQty, 2);
                }
                newDto.ReceivedDetails = gp.OrderBy(t => t.ReceivedTime).ToList();
                outputDto.Add(newDto);
            }

            return new InterTransferReceivedExportReportDto { InterTransferReceivedReportDtos = outputDto };
        }

        public async Task<FileDto> GetMaterialTrackReceivedDetailReportMaterialWiseForGivenInputToExcel(ClosingStockExportGroupCategoryMaterialDto input)
        {
            var result = await GetMaterialTrackReceivedDetailReportForGivenMaterialWise(input);
            result.InputFilterDto = input;
            if (result.MaterialTrackWithValueReportDtos.Count == 0)
            {
                throw new UserFriendlyException(L("NoMoreRecordsFound"));
            }
            return _housereportExporter.ExportMaterialTrackReportForGivenMaterialWise(result);
        }

        public async Task<MaterialTrackExportReportDto> GetMaterialTrackReceivedDetailReportForGivenMaterialWise(ClosingStockExportGroupCategoryMaterialDto input)
        {
            if (input.TenantId == 0)
            {
                if (AbpSession.TenantId.HasValue)
                    input.TenantId = (int) AbpSession.TenantId;
            }

            List<MaterialTrackWithInvoiceAndInterTransferAnalysisDto> overAllItems = new List<MaterialTrackWithInvoiceAndInterTransferAnalysisDto>();

            #region Location Filter
            List<int> selectedLocationRefIds = new List<int>();
            if (input.Locations != null && input.Locations.Any())
            {
                selectedLocationRefIds = input.Locations.Select(a => a.Id).ToList();
            }
            else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
                     && !input.LocationGroup.Locations.Any())
            {
                selectedLocationRefIds = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
            }
            else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                selectedLocationRefIds = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
            }
            else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                selectedLocationRefIds = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true
                });
            }
            else if (input.LocationGroup == null)
            {
                if (input.UserId > 0)
                {
                    selectedLocationRefIds = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                    {
                        Locations = new List<SimpleLocationDto>(),
                        Group = false,
                        UserId = input.UserId
                    });
                }
            }
            #endregion

            #region Group Category Material Filter
            List<int> arrMateriaRefIds = new List<int>();
            if (input.MaterialList.Count > 0)
            {
                arrMateriaRefIds = input.MaterialList.Select(t => int.Parse(t.Value)).ToList();
                input.ExportMaterialAlso = true;
            }
            List<int> arrGroupRefIds = new List<int>();
            if (input.GroupList.Count > 0)
            {
                arrGroupRefIds = input.GroupList.Select(t => int.Parse(t.Value)).ToList();
            }
            List<int> arrCategoryRefIds = new List<int>();
            if (input.CategoryList.Count > 0)
            {
                arrCategoryRefIds = input.CategoryList.Select(t => int.Parse(t.Value)).ToList();
            }

            #endregion

            var materialQueryable = _materialRepo.GetAll();
            var locationQueryable = _locationRepo.GetAll();
            var supplierQueryable = _supplierRepo.GetAll();

            if (input.RunInBackGround)
            {
                materialQueryable = materialQueryable.Where(t => t.TenantId == input.TenantId);
                locationQueryable = locationQueryable.Where(t => t.TenantId == input.TenantId);
                supplierQueryable = supplierQueryable.Where(t => t.TenantId == input.TenantId);
            }

            #region Open Balance
            {
                var openBalanceDetails = await (from mat in materialQueryable
                                                .WhereIf(arrMateriaRefIds.Count > 0, t => arrMateriaRefIds.Contains(t.Id))
                                                join matCat in _materialGroupCategoryRepo.GetAll().WhereIf(arrCategoryRefIds.Count > 0, t => arrCategoryRefIds.Contains(t.Id))
                                                                on mat.MaterialGroupCategoryRefId equals matCat.Id
                                                join matGrp in _materialGroupRepo.GetAll().WhereIf(arrGroupRefIds.Count > 0, t => arrGroupRefIds.Contains(t.Id))
                                                    on matCat.MaterialGroupRefId equals matGrp.Id
                                                join matLoc in _materialLocationWiseStockRepo.GetAll() on mat.Id equals matLoc.MaterialRefId
                                                join loc in locationQueryable.Where(t => selectedLocationRefIds.Contains(t.Id)) on matLoc.LocationRefId equals loc.Id
                                                join recdUnit in _unitrepo.GetAll() on mat.DefaultUnitId equals recdUnit.Id
                                                join trfUnit in _unitrepo.GetAll() on mat.TransferUnitId equals trfUnit.Id
                                                select new MaterialTrackWithInvoiceAndInterTransferAnalysisDto
                                                {
                                                    TransactionTime = input.StartDate,
                                                    Action = "-",
                                                    LocationRefId = loc.Id,
                                                    LocationRefCode = loc.Code,
                                                    LocationRefName = loc.Name,
                                                    LinedEntityName = "Open Balance",
                                                    ReferenceNumber = "Open Balance",
                                                    MaterialGroupRefId = matGrp.Id,
                                                    MaterialGroupRefName = matGrp.MaterialGroupName,
                                                    MaterialGroupCategoryRefId = matCat.Id,
                                                    MaterialGroupCategoryRefName = matCat.MaterialGroupCategoryName,
                                                    MaterialRefId = mat.Id,
                                                    MaterialRefName = mat.MaterialName,
                                                    MaterialPetName = mat.MaterialPetName,
                                                    Qty = 0,
                                                    UnitRefId = mat.DefaultUnitId,
                                                    UnitRefName = recdUnit.Name,
                                                    Price = 0m,
                                                    DefaultUnitId = mat.DefaultUnitId,
                                                    TransferUnitId = mat.TransferUnitId,
                                                    TransferUnitName = trfUnit.Name,
                                                    TotalAmount = 0
                                                }).ToListAsync();

                foreach (var locGp in openBalanceDetails.GroupBy(t => t.LocationRefId))
                {
                    List<int> selectedMaterialRefIds = locGp.Select(t => t.MaterialRefId).ToList();
                    var outputClosingStock = await GetStockSummary(new GetHouseReportInput
                    {
                        StartDate = input.StartDate.AddDays(-1),
                        EndDate = input.StartDate.AddDays(-1),
                        LocationRefId = locGp.Key,
                        MaterialRefIds = selectedMaterialRefIds,
                        ExcludeRateView = false,
                        RunInBackGround = input.RunInBackGround,
                        TenantId = input.TenantId
                    });

                    var StockSummary = outputClosingStock.StockSummary;
                    foreach (var mat in locGp.ToList())
                    {
                        var stk = StockSummary.FirstOrDefault(t => t.LocationRefId == mat.LocationRefId && t.MaterialRefId == mat.MaterialRefId);
                        if (stk != null)
                        {
                            mat.TransactionTime = input.StartDate.AddDays(-1);
                            mat.Price = stk.AvgPrice;
                            mat.Qty = stk.ClBalance;
                            mat.TotalAmount = Math.Round(mat.Price * mat.Qty, 2);
                        }
                    }
                }
                overAllItems.AddRange(openBalanceDetails);
            }
            #endregion

            #region InterTransferIssued
            {
                var intertransferMaster = _intertransferRepo.GetAll().Where(t => t.CompletedTime.HasValue);
                if (input.RunInBackGround)
                    intertransferMaster = intertransferMaster.Where(t => t.TenantId == input.TenantId);

                #region Date & Supplier Filter
                if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
                {
                    intertransferMaster =
                        intertransferMaster.Where(
                            a => DbFunctions.TruncateTime(a.ReceivedTime) >= DbFunctions.TruncateTime(input.StartDate)
                                 && DbFunctions.TruncateTime(a.ReceivedTime) <= DbFunctions.TruncateTime(input.EndDate));
                }

                #endregion

                intertransferMaster = intertransferMaster.Where(a => selectedLocationRefIds.Contains(a.RequestLocationRefId));

                var intertransferIssueDetail = _intertransferDetailRepo.GetAll();
                string desc = L("InterTransfer") + " " + L("Issue");
                var itemsInterTransferIssueDetails = await (from interTrasnfer in intertransferMaster
                                                            join issueDetail in intertransferIssueDetail on interTrasnfer.Id equals issueDetail.InterTransferRefId
                                                            join mat in materialQueryable.WhereIf(arrMateriaRefIds.Count > 0, t => arrMateriaRefIds.Contains(t.Id))
                                                                on issueDetail.MaterialRefId equals mat.Id
                                                            join matCat in _materialGroupCategoryRepo.GetAll().WhereIf(arrCategoryRefIds.Count > 0, t => arrCategoryRefIds.Contains(t.Id))
                                                                on mat.MaterialGroupCategoryRefId equals matCat.Id
                                                            join matGrp in _materialGroupRepo.GetAll().WhereIf(arrGroupRefIds.Count > 0, t => arrGroupRefIds.Contains(t.Id))
                                                                on matCat.MaterialGroupRefId equals matGrp.Id
                                                            join loc in locationQueryable on interTrasnfer.LocationRefId equals loc.Id
                                                            join reqLoc in locationQueryable on interTrasnfer.RequestLocationRefId equals reqLoc.Id
                                                            join recdUnit in _unitrepo.GetAll() on issueDetail.UnitRefId equals recdUnit.Id
                                                            join trfUnit in _unitrepo.GetAll() on mat.TransferUnitId equals trfUnit.Id
                                                            select new MaterialTrackWithInvoiceAndInterTransferAnalysisDto
                                                            {
                                                                TransactionTime = interTrasnfer.ReceivedTime.Value,
                                                                Action = "-",
                                                                LocationRefId = interTrasnfer.RequestLocationRefId,
                                                                LocationRefCode = reqLoc.Code,
                                                                LocationRefName = reqLoc.Name,
                                                                LinedEntityName = loc.Code,
                                                                RefernceId = interTrasnfer.Id,
                                                                ReferenceNumber = desc + " " + interTrasnfer.DocReferenceNumber,
                                                                MaterialGroupRefId = matGrp.Id,
                                                                MaterialGroupRefName = matGrp.MaterialGroupName,
                                                                MaterialGroupCategoryRefId = matCat.Id,
                                                                MaterialGroupCategoryRefName = matCat.MaterialGroupCategoryName,
                                                                MaterialRefId = mat.Id,
                                                                MaterialRefName = mat.MaterialName,
                                                                MaterialPetName = mat.MaterialPetName,
                                                                Qty = issueDetail.IssueQty,
                                                                UnitRefId = issueDetail.UnitRefId,
                                                                UnitRefName = recdUnit.Name,
                                                                Price = issueDetail.Price,
                                                                DefaultUnitId = mat.DefaultUnitId,
                                                                TransferUnitId = mat.TransferUnitId,
                                                                TransferUnitName = trfUnit.Name,
                                                                TotalAmount = Math.Round(issueDetail.Price * issueDetail.IssueQty, 2)
                                                            }).ToListAsync();
                overAllItems.AddRange(itemsInterTransferIssueDetails);
            }
            #endregion

            #region InterTransferReceived
            {

                var intertransferMaster = _intertransferRepo.GetAll().Where(t => t.ReceivedTime.HasValue);

                #region Date & Supplier Filter
                if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
                {
                    intertransferMaster =
                        intertransferMaster.Where(
                            a => DbFunctions.TruncateTime(a.ReceivedTime) >= DbFunctions.TruncateTime(input.StartDate)
                                 && DbFunctions.TruncateTime(a.ReceivedTime) <= DbFunctions.TruncateTime(input.EndDate));
                }

                #endregion

                intertransferMaster = intertransferMaster.Where(a => selectedLocationRefIds.Contains(a.LocationRefId));

                var intertransferRecivedDetail = _intertransferReceivedRepo.GetAll();
                string desc = L("InterTransfer") + " " + L("Received");
                var itemsInterTransferReceivedDetails = await (from interTrasnfer in intertransferMaster
                                                               join recdDetail in intertransferRecivedDetail on interTrasnfer.Id equals recdDetail.InterTransferRefId
                                                               join mat in materialQueryable.WhereIf(arrMateriaRefIds.Count > 0, t => arrMateriaRefIds.Contains(t.Id))
                                                                   on recdDetail.MaterialRefId equals mat.Id
                                                               join matCat in _materialGroupCategoryRepo.GetAll().WhereIf(arrCategoryRefIds.Count > 0, t => arrCategoryRefIds.Contains(t.Id))
                                                                   on mat.MaterialGroupCategoryRefId equals matCat.Id
                                                               join matGrp in _materialGroupRepo.GetAll().WhereIf(arrGroupRefIds.Count > 0, t => arrGroupRefIds.Contains(t.Id))
                                                                   on matCat.MaterialGroupRefId equals matGrp.Id
                                                               join loc in locationQueryable on interTrasnfer.LocationRefId equals loc.Id
                                                               join reqLoc in locationQueryable on interTrasnfer.RequestLocationRefId equals reqLoc.Id
                                                               join recdUnit in _unitrepo.GetAll() on recdDetail.UnitRefId equals recdUnit.Id
                                                               join trfUnit in _unitrepo.GetAll() on mat.TransferUnitId equals trfUnit.Id
                                                               select new MaterialTrackWithInvoiceAndInterTransferAnalysisDto
                                                               {
                                                                   TransactionTime = interTrasnfer.ReceivedTime.Value,
                                                                   Action = "+",
                                                                   LocationRefId = interTrasnfer.LocationRefId,
                                                                   LocationRefCode = loc.Code,
                                                                   LocationRefName = loc.Name,
                                                                   LinedEntityName = reqLoc.Code,
                                                                   RefernceId = interTrasnfer.Id,
                                                                   ReferenceNumber = desc + " " + interTrasnfer.DocReferenceNumber,
                                                                   MaterialGroupRefId = matGrp.Id,
                                                                   MaterialGroupRefName = matGrp.MaterialGroupName,
                                                                   MaterialGroupCategoryRefId = matCat.Id,
                                                                   MaterialGroupCategoryRefName = matCat.MaterialGroupCategoryName,
                                                                   MaterialRefId = mat.Id,
                                                                   MaterialRefName = mat.MaterialName,
                                                                   MaterialPetName = mat.MaterialPetName,
                                                                   Qty = recdDetail.ReceivedQty,
                                                                   UnitRefId = recdDetail.UnitRefId,
                                                                   UnitRefName = recdUnit.Name,
                                                                   Price = recdDetail.Price,
                                                                   DefaultUnitId = mat.DefaultUnitId,
                                                                   TransferUnitId = mat.TransferUnitId,
                                                                   TransferUnitName = trfUnit.Name,
                                                                   TotalAmount = Math.Round(recdDetail.Price * recdDetail.ReceivedQty, 2)
                                                               }).ToListAsync();
                overAllItems.AddRange(itemsInterTransferReceivedDetails);
            }
            #endregion

            #region PurchaseReceived
            {
                var invoice = _invoiceRepo.GetAll();
             

                #region Date & Supplier Filter
                if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
                {
                    invoice =
                        invoice.Where(
                            a => DbFunctions.TruncateTime(a.AccountDate) >= DbFunctions.TruncateTime(input.StartDate)
                                 && DbFunctions.TruncateTime(a.AccountDate) <= DbFunctions.TruncateTime(input.EndDate));
                }

                #endregion

                invoice = invoice.Where(a => selectedLocationRefIds.Contains(a.LocationRefId));

                var invoiceDetail = _invoiceDetailRepo.GetAll();
                if (input.RunInBackGround)
                {
                    invoice = invoice.Where(t => t.TenantId == input.TenantId);
                    invoiceDetail = invoiceDetail.Where(t => t.TenantId == input.TenantId);
                }
                string desc = L("Invoice");
                var itemsInviceDetails = await (from inv in invoice
                                                join recdDetail in invoiceDetail on inv.Id equals recdDetail.InvoiceRefId
                                                join supplier in supplierQueryable on inv.SupplierRefId equals supplier.Id
                                                join mat in materialQueryable.WhereIf(arrMateriaRefIds.Count > 0, t => arrMateriaRefIds.Contains(t.Id))
                                                    on recdDetail.MaterialRefId equals mat.Id
                                                join matCat in _materialGroupCategoryRepo.GetAll().WhereIf(arrCategoryRefIds.Count > 0, t => arrCategoryRefIds.Contains(t.Id))
                                                    on mat.MaterialGroupCategoryRefId equals matCat.Id
                                                join matGrp in _materialGroupRepo.GetAll().WhereIf(arrGroupRefIds.Count > 0, t => arrGroupRefIds.Contains(t.Id))
                                                    on matCat.MaterialGroupRefId equals matGrp.Id
                                                join loc in locationQueryable on inv.LocationRefId equals loc.Id
                                                join recdUnit in _unitrepo.GetAll() on recdDetail.UnitRefId equals recdUnit.Id
                                                join trfUnit in _unitrepo.GetAll() on mat.TransferUnitId equals trfUnit.Id
                                                select new MaterialTrackWithInvoiceAndInterTransferAnalysisDto
                                                {
                                                    TransactionTime = inv.AccountDate,
                                                    Action = "+",
                                                    LocationRefId = inv.LocationRefId,
                                                    LocationRefCode = loc.Code,
                                                    LocationRefName = loc.Name,
                                                    LinedEntityName = supplier.SupplierName,
                                                    RefernceId = inv.Id,
                                                    ReferenceNumber = desc + " " + inv.InvoiceNumber,
                                                    MaterialGroupRefId = matGrp.Id,
                                                    MaterialGroupRefName = matGrp.MaterialGroupName,
                                                    MaterialGroupCategoryRefId = matCat.Id,
                                                    MaterialGroupCategoryRefName = matCat.MaterialGroupCategoryName,
                                                    MaterialRefId = mat.Id,
                                                    MaterialRefName = mat.MaterialName,
                                                    MaterialPetName = mat.MaterialPetName,
                                                    Qty = recdDetail.TotalQty,
                                                    UnitRefId = recdDetail.UnitRefId,
                                                    UnitRefName = recdUnit.Name,
                                                    Price = recdDetail.Price,
                                                    DefaultUnitId = mat.DefaultUnitId,
                                                    TransferUnitId = mat.TransferUnitId,
                                                    TransferUnitName = trfUnit.Name,
                                                    TotalAmount = Math.Round(recdDetail.Price * recdDetail.TotalQty, 2)
                                                }).ToListAsync();
                overAllItems.AddRange(itemsInviceDetails);
            }
            #endregion

            #region PurchaseReturn Details
            {
                var master = _purchaseReturn.GetAll();

                #region Date & Supplier Filter
                if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
                {
                    master =
                        master.Where(
                            a => DbFunctions.TruncateTime(a.AccountDate) >= DbFunctions.TruncateTime(input.StartDate)
                                 && DbFunctions.TruncateTime(a.AccountDate) <= DbFunctions.TruncateTime(input.EndDate));
                }

                #endregion
                master = master.Where(a => selectedLocationRefIds.Contains(a.LocationRefId));

                var detail = _purchaseReturnDetailRepo.GetAll();
                string desc = L("PurchaseReturn");
                if (input.RunInBackGround)
                {
                    master = master.Where(t => t.TenantId == input.TenantId);
                }
                var itemsPurchaseReturnDetails = await (from inv in master
                                                        join recdDetail in detail on inv.Id equals recdDetail.PurchaseReturnRefId
                                                        join supplier in supplierQueryable on inv.SupplierRefId equals supplier.Id
                                                        join mat in materialQueryable.WhereIf(arrMateriaRefIds.Count > 0, t => arrMateriaRefIds.Contains(t.Id))
                                                            on recdDetail.MaterialRefId equals mat.Id
                                                        join matCat in _materialGroupCategoryRepo.GetAll().WhereIf(arrCategoryRefIds.Count > 0, t => arrCategoryRefIds.Contains(t.Id))
                                                            on mat.MaterialGroupCategoryRefId equals matCat.Id
                                                        join matGrp in _materialGroupRepo.GetAll().WhereIf(arrGroupRefIds.Count > 0, t => arrGroupRefIds.Contains(t.Id))
                                                            on matCat.MaterialGroupRefId equals matGrp.Id
                                                        join loc in locationQueryable on inv.LocationRefId equals loc.Id
                                                        join recdUnit in _unitrepo.GetAll() on recdDetail.UnitRefId equals recdUnit.Id
                                                        join trfUnit in _unitrepo.GetAll() on mat.TransferUnitId equals trfUnit.Id
                                                        select new MaterialTrackWithInvoiceAndInterTransferAnalysisDto
                                                        {
                                                            TransactionTime = inv.AccountDate == null ? inv.ReturnDate : inv.AccountDate.Value,
                                                            Action = "-",
                                                            LocationRefId = inv.LocationRefId,
                                                            LocationRefCode = loc.Code,
                                                            LocationRefName = loc.Name,
                                                            LinedEntityName = supplier.SupplierName,
                                                            RefernceId = inv.Id,
                                                            ReferenceNumber = desc + " " + inv.ReferenceNumber,
                                                            MaterialGroupRefId = matGrp.Id,
                                                            MaterialGroupRefName = matGrp.MaterialGroupName,
                                                            MaterialGroupCategoryRefId = matCat.Id,
                                                            MaterialGroupCategoryRefName = matCat.MaterialGroupCategoryName,
                                                            MaterialRefId = mat.Id,
                                                            MaterialRefName = mat.MaterialName,
                                                            MaterialPetName = mat.MaterialPetName,
                                                            Qty = recdDetail.Quantity,
                                                            UnitRefId = recdDetail.UnitRefId,
                                                            UnitRefName = recdUnit.Name,
                                                            Price = recdDetail.Price,
                                                            DefaultUnitId = mat.DefaultUnitId,
                                                            TransferUnitId = mat.TransferUnitId,
                                                            TransferUnitName = trfUnit.Name,
                                                            TotalAmount = Math.Round(recdDetail.Price * recdDetail.Quantity, 2)
                                                        }).ToListAsync();
                overAllItems.AddRange(itemsPurchaseReturnDetails);
            }
            #endregion

            List<MaterialTrackWithValueReportDto> outputDto = new List<MaterialTrackWithValueReportDto>();
            var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();
            foreach (var gp in overAllItems.GroupBy(t => new { t.MaterialRefId, t.UnitRefId, t.TransferUnitId }))
            {
                if (gp.Key.UnitRefId == gp.Key.TransferUnitId)
                    continue;
                var unitConversion = await _materialAppService.GetConversion(new GetConversionBasedOnBaseUnitRefUnit
                {
                    BaseUnitId = gp.Key.UnitRefId,
                    RefUnitId = gp.Key.TransferUnitId,
                    ConversionUnitList = rsUc,
                    MaterialRefId = gp.Key.MaterialRefId,
                });
                if (unitConversion == null)
                {

                }
                else
                {
                    foreach (var lst in gp.ToList())
                    {
                        lst.Price = lst.Price * unitConversion.Conversion;
                        lst.Qty = lst.Qty * unitConversion.Conversion;
                        lst.TotalAmount = Math.Round(lst.Price * lst.Qty, 2);
                    }
                }
            }

            foreach (var gp in overAllItems.GroupBy(t => new { t.LocationRefId, t.MaterialRefId }))
            {
                MaterialTrackWithValueReportDto newDto = new MaterialTrackWithValueReportDto
                {
                    LocationRefId = gp.Key.LocationRefId,
                    LocationRefCode = gp.FirstOrDefault().LocationRefCode,
                    MaterialGroupRefId = gp.FirstOrDefault().MaterialGroupRefId,
                    MaterialGroupCategoryRefId = gp.FirstOrDefault().MaterialGroupCategoryRefId,
                    MaterialGroupRefName = gp.FirstOrDefault().MaterialGroupRefName,
                    MaterialGroupCategoryRefName = gp.FirstOrDefault().MaterialGroupCategoryRefName,
                    MaterialRefName = gp.FirstOrDefault().MaterialRefName,
                    MaterialRefId = gp.FirstOrDefault().MaterialRefId,
                    Qty = gp.Where(t => t.Action.Equals("+")).Sum(t => t.Qty) - gp.Where(t => t.Action.Equals("-")).Sum(t => t.Qty),
                    TotalAmount = Math.Round(gp.Where(t => t.Action.Equals("+")).Sum(t => t.Qty * t.Price) - gp.Where(t => t.Action.Equals("-")).Sum(t => t.Qty * t.Price), 2),
                };

                if (newDto.TotalAmount > 0 && newDto.Qty>0)
                {
                    newDto.AvgPrice = Math.Round(newDto.TotalAmount / newDto.Qty, 2);
                }
                newDto.ReceivedDetails = gp.OrderBy(t => t.TransactionTime).ToList();
                outputDto.Add(newDto);
            }

            return new MaterialTrackExportReportDto { MaterialTrackWithValueReportDtos = outputDto };
        }


        public async Task<FileDto> GetCloseStockGroupCategoryExcel(ClosingStockExportGroupCategoryMaterialDto input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackGround)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = HouseReportNames.CLOSINGSTOCK_GROUP_CATEGORY_MATERIAL_VALUE,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = "Closing Stock - Group / Category / Material"
                    });

                    if (backGroundId > 0)
                    {
                        input.LocationGroup.UserId = AbpSession.UserId.Value;

                        await _bgm.EnqueueAsync<HouseReportBackgroundJob, HouseReportBackgroundJobIdJobArgs>(new HouseReportBackgroundJobIdJobArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = HouseReportNames.CLOSINGSTOCK_GROUP_CATEGORY_MATERIAL_VALUE,
                            CloseReportInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });

                        return new FileDto { BackGroudId = backGroundId };
                    }
                }
                else
                {
                    return await GetClosingStockReportGroupOrCategoryOrMaterialWiseForGivenInputToExcel(input);
                }
            }
            return null;
        }

        public async Task<FileDto> GetBackGroundMaterialTrackReceivedDetailReportMaterialWiseForGivenInputToExcel(ClosingStockExportGroupCategoryMaterialDto input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackGround)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = HouseReportNames.MATERIAL_TRACK_INVOICE_INTERTRANSFER,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = "Received Detail Track"
                    });

                    if (backGroundId > 0)
                    {
                        input.LocationGroup.UserId = AbpSession.UserId.Value;

                        await _bgm.EnqueueAsync<HouseReportBackgroundJob, HouseReportBackgroundJobIdJobArgs>(new HouseReportBackgroundJobIdJobArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = HouseReportNames.MATERIAL_TRACK_INVOICE_INTERTRANSFER,
                            CloseReportInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });

                        return new FileDto { BackGroudId = backGroundId };
                    }
                }
                else
                {
                    return await GetMaterialTrackReceivedDetailReportMaterialWiseForGivenInputToExcel(input);
                }
            }
            return null;
        }


    }
}
