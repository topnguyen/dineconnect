﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using System;


namespace DinePlan.DineConnect.House.Master.Implementation
    {
    public class RecipeAppService : DineConnectAppServiceBase, IRecipeAppService
    {

        private readonly IRecipeListExcelExporter _recipeExporter;
        private readonly IRecipeManager _recipeManager;
        private readonly IRecipeIngredientManager _recipeIngredientManager;
        private readonly IRepository<Recipe> _recipeRepo;
        private readonly IRepository<Unit> _unit;
        private readonly IRepository<RecipeIngredient> _recipeIngredientRepo;
        private readonly IRepository<RecipeGroup> _recipeGroupRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<Invoice> _invoiceRepo;
        private readonly IRepository<InvoiceDetail> _invoiceDetailRepo;
        private readonly UnitAppService _unitAppService;

        public RecipeAppService(IRecipeManager recipeManager,
                IRepository<Recipe> recipeRepo,
                IRecipeListExcelExporter recipeExporter,
                IRepository<Unit> unit,
                IRepository<RecipeIngredient> recipeIngredientRepo,
                IRecipeIngredientManager recipeIngredientManager,
                IRepository<RecipeGroup> recipeGroup,
                IRepository<Material> materialRepo,
                IRepository<Invoice> invoiceRepo,
                IRepository<InvoiceDetail> invoiceDetailRepo,
                UnitAppService unitAppService
            )
        {
            _recipeManager = recipeManager;
            _recipeRepo = recipeRepo;
            _recipeExporter = recipeExporter;
            _unit = unit;
            _recipeIngredientRepo = recipeIngredientRepo;
            _recipeIngredientManager = recipeIngredientManager;
            _recipeGroupRepo = recipeGroup;
            _materialRepo = materialRepo;
            _invoiceRepo = invoiceRepo;
            _invoiceDetailRepo = invoiceDetailRepo;
            _unitAppService = unitAppService;
        }

        public async Task<PagedResultOutput<RecipeListDto>> GetAll(GetRecipeInput input)
        {
            var allItems = _recipeRepo.GetAll();
            if (!string.IsNullOrEmpty(input.Operation) && input.Operation.Equals("SEARCH"))
            {
                allItems = _recipeRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.RecipeName.Equals(input.Filter)
               );
            }
            else
            {
                allItems = _recipeRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.RecipeName.Contains(input.Filter)
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<RecipeListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<RecipeListDto>(
                allItemCount,
                allListDtos
                );
        }

        protected decimal GetMaterialAvgRate(int materialRefId,DateTime startDate,DateTime endDate )
        {

            decimal retVal = 0.0m;
            var matInvList = (from inv in _invoiceRepo.GetAll()
                              where (DbFunctions.TruncateTime(inv.AccountDate) >= startDate.Date && DbFunctions.TruncateTime(inv.AccountDate) <= endDate.Date)
                              join invDet in _invoiceDetailRepo.GetAll() on inv.Id equals invDet.InvoiceRefId
                              where invDet.MaterialRefId == materialRefId
                              group invDet by new { invDet.MaterialRefId } into g
                              select g.Sum(s => s.NetAmount) / g.Sum(s => s.TotalQty));

            if (matInvList.Count() != 0)
                retVal = matInvList.Single();
            else
                retVal=0;
            return retVal;
                                         
        }

        protected decimal GetBatchCost(int recipeId, DateTime startDate, DateTime endDate)
        {
            
            decimal totBatchCost;
            decimal tmpCost;
            decimal tmpMatRate;
            var ingrList = _recipeIngredientRepo.GetAllList().Where(x => x.RecipeRefId == recipeId);

            totBatchCost = 0.0m;
            tmpCost = 0.0m;
            tmpMatRate = 0.0m;
            foreach (var items in ingrList)
            {
                tmpMatRate = GetMaterialAvgRate(items.MaterialRefId, startDate, endDate);
                tmpCost = items.MaterialUsedQty * tmpMatRate;

                totBatchCost = totBatchCost + tmpCost;
            }

            return totBatchCost;
        }

        public async Task<PagedResultOutput<RecipeCostingReportDto>> GetRecipeCosting(GetRecipeInput input)
        {
            var allRecipeItems = await _recipeRepo.GetAll().OrderBy(r=>r.RecipeName).ToListAsync();
            List<RecipeCostingReportDto> mrec = new List<RecipeCostingReportDto>();
            foreach (var items in allRecipeItems)
            {
                RecipeCostingReportDto rec = new RecipeCostingReportDto();
                rec.RecipeName = items.RecipeName;
                rec.PrdBatchQty = items.PrdBatchQty;
                //rec.PrdUnitName = _unitAppService.GetUnitName(items.PrdUnitId);
                rec.PrdUnitName = "KG";
                rec.BatchCost=  GetBatchCost(items.Id,input.StartDate,input.EndDate);
                rec.PerUnitCost = rec.BatchCost / rec.PrdBatchQty;
                mrec.Add(rec);
            }

            var allListDtos = mrec.MapTo<List<RecipeCostingReportDto>>();

            var allItemCount = allRecipeItems.Count;
            return new PagedResultOutput<RecipeCostingReportDto>(
                allItemCount,
                allListDtos
                );

        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _recipeRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<RecipeListDto>>();
            return _recipeExporter.ExportToFile(allListDtos);
        }

        public async Task<GetRecipeForEditOutput> GetRecipeForEdit(NullableIdInput input)
        {
            RecipeEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _recipeRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<RecipeEditDto>();
            }
            else
            {
                editDto = new RecipeEditDto();
            }

            return new GetRecipeForEditOutput
            {
                Recipe = editDto
            };
        }

        public async Task<IdInput> CreateOrUpdateRecipe(CreateOrUpdateRecipeInput input)
        {
            if (input.Recipe.Id.HasValue)
            {
                return await UpdateRecipe(input);
            }
            else
            {
                return await CreateRecipe(input);
            }
        }

        public async Task DeleteRecipe(IdInput input)
        {
            await _recipeRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task<IdInput> UpdateRecipe(CreateOrUpdateRecipeInput input)
        {
            var item = await _recipeRepo.GetAsync(input.Recipe.Id.Value);
            var dto = input.Recipe;

            //TODO: SERVICE Recipe Update Individually
            item.RecipeName = dto.RecipeName;
            item.RecipeShortName = dto.RecipeShortName;
            item.RecipeGroupRefId = item.RecipeGroupRefId;
            item.PrdBatchQty = dto.PrdBatchQty;
            item.PrdUnitId = dto.PrdUnitId;
            item.SelUnitId = dto.SelUnitId;
            item.LifeTimeInMinutes = dto.LifeTimeInMinutes;
            item.AlarmTimeInMinutes = dto.AlarmTimeInMinutes;
            item.AlarmFlag = dto.AlarmFlag;
            item.FixedCost = dto.FixedCost;

            //@@Pending Edit and Save 
            //  Update Recipe Ingredient Details
            List<int> ingredientsToBeRetained = new List<int>();

            if (input.RecipeIngredientList != null && input.RecipeIngredientList.Count > 0)
            {
                foreach (var editLst in input.RecipeIngredientList)
                {
                    int materialrefid = editLst.MaterialRefId;
                    ingredientsToBeRetained.Add(materialrefid);

                    var detailId =  _recipeIngredientRepo.GetAllList(l => l.MaterialRefId == editLst.MaterialRefId && l.RecipeRefId == input.Recipe.Id.Value);

                    if ( detailId.Count == 0)           //  If it;
                    {
                        RecipeIngredient newRecIng = new RecipeIngredient();
                        newRecIng.RecipeRefId = input.Recipe.Id.Value;
                        newRecIng.MaterialRefId = editLst.MaterialRefId;
                        newRecIng.MaterialUsedQty = editLst.MaterialUsedQty;
                        newRecIng.UnitRefId = editLst.UnitRefId;
                        newRecIng.UserSerialNumber = editLst.UserSerialNumber;
                        newRecIng.VariationflagForProduction = editLst.VariationflagForProduction;

                        var retId2 = await _recipeIngredientRepo.InsertAndGetIdAsync(newRecIng);
                        CheckErrors(await _recipeIngredientManager.CreateSync(newRecIng));
                    }
                    else                              //  If it's Edit Existing 
                    {
                        var ingredientitem = await _recipeIngredientRepo.GetAsync(detailId[0].Id);

                        ingredientitem.MaterialUsedQty = editLst.MaterialUsedQty;
                        ingredientitem.UnitRefId = editLst.UnitRefId;
                        ingredientitem.VariationflagForProduction = editLst.VariationflagForProduction;
                        ingredientitem.UserSerialNumber = editLst.UserSerialNumber;

                        var retId2 = await _recipeIngredientRepo.UpdateAsync(ingredientitem);
                    }

                }
            }

            var delIngredientList = _recipeIngredientRepo.GetAll().Where(a => a.RecipeRefId == input.Recipe.Id.Value && !ingredientsToBeRetained.Contains(a.MaterialRefId)).ToList();

            foreach (var a in delIngredientList)
            {
                _recipeIngredientRepo.Delete(a.Id);
            }

            CheckErrors(await _recipeManager.CreateSync(item));

            return new IdInput
            {
                Id = input.Recipe.Id.Value
            };
        }

        protected virtual async Task<IdInput> CreateRecipe(CreateOrUpdateRecipeInput input)
        {
            var dto = input.Recipe.MapTo<Recipe>();
            var retRecipeId = await _recipeRepo.InsertOrUpdateAndGetIdAsync(dto);

            if (input.RecipeIngredientList != null)
            {

                foreach (RecipeIngredientViewDto item in input.RecipeIngredientList.ToList())
                {
                    RecipeIngredient newRecIng = new RecipeIngredient();
                    newRecIng.RecipeRefId = retRecipeId;
                    newRecIng.MaterialRefId = item.MaterialRefId;
                    newRecIng.MaterialUsedQty = item.MaterialUsedQty;
                    newRecIng.UnitRefId = item.UnitRefId;
                    newRecIng.UserSerialNumber = item.UserSerialNumber;
                    newRecIng.VariationflagForProduction = item.VariationflagForProduction;

                    var retId2 = await _recipeIngredientRepo.InsertAndGetIdAsync(newRecIng);
                    CheckErrors(await _recipeIngredientManager.CreateSync(newRecIng));
                }
            }
            CheckErrors(await _recipeManager.CreateSync(dto));

            return new IdInput
            {
                Id = retRecipeId
            };
        }

        public async Task<ListResultOutput<RecipeListDto>> GetRecipeNames()
        {
            var lstRecipe = await _recipeRepo.GetAll().ToListAsync();
            return new ListResultOutput<RecipeListDto>(lstRecipe.MapTo<List<RecipeListDto>>());
        }
        public async Task<PagedResultOutput<RecipeViewDto>> GetView(GetUnitConversionInput input)
        {
            var allItems = (from r in _recipeRepo.GetAll()
                            join rg in _recipeGroupRepo.GetAll() 
                            on r.RecipeGroupRefId equals rg.Id 
                            join u in _unit.GetAll()
                            on r.PrdUnitId equals u.Id
                            join ur in _unit.GetAll()
                            on r.SelUnitId equals ur.Id
                            select new RecipeViewDto()
                            {
                                Id = r.Id,
                                GroupName = rg.RecipeGroupName,
                                RecipeName = r.RecipeName,
                                RecipeShortName = r.RecipeShortName,
                                PrdBatchQty = r.PrdBatchQty,
                                PrdUnitName = u.Name,
                                SelUnitName = ur.Name,
                                FixedCost = r.FixedCost,
                                CreationTime = r.CreationTime
                            }).WhereIf(
                            !input.Filter.IsNullOrEmpty(),
                            p => p.RecipeName.Contains(input.Filter)
                        );

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.ToList();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<RecipeViewDto>(
                allItemCount,
                allListDtos
                );
        }


        public async Task<PagedResultOutput<RecipeIngredientViewDto>> GetIngredientDto()
        {
            var allItems = _recipeIngredientRepo.GetAll();
            //if (!string.IsNullOrEmpty(input.Operation) && input.Operation.Equals("SEARCH"))
            //{
            //    allItems = recipeIngredientRepo
            //   .GetAll()
            //   .WhereIf(
            //       !input.Filter.IsNullOrEmpty(),
            //       p => p.Id.Equals(input.Filter)
            //   );
            //}
            //else
            //{
            //    allItems = recipeIngredientRepo
            //   .GetAll();
            //}
            var sortMenuItems = await allItems
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<RecipeIngredientViewDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<RecipeIngredientViewDto>(
                allItemCount,
                allListDtos
                );
        }

    }
}
