﻿
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;


namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class MaterialUnitsLinkAppService : DineConnectAppServiceBase, IMaterialUnitsLinkAppService
    {

        private readonly IMaterialUnitsLinkListExcelExporter _materialunitslinkExporter;
        private readonly IMaterialUnitsLinkManager _materialunitslinkManager;
        private readonly IRepository<MaterialUnitsLink> _materialunitslinkRepo;
        private readonly IRepository<Material> _materialManager;
        private readonly IRepository<Unit> _unitManager;

       public MaterialUnitsLinkAppService(IMaterialUnitsLinkManager materialunitslinkManager,
            IRepository<MaterialUnitsLink> materialUnitsLinkRepo,
            IMaterialUnitsLinkListExcelExporter materialunitslinkExporter, IRepository<Material> materialManager,
            IRepository<Unit> unitManager)
        {
            _materialunitslinkManager = materialunitslinkManager;
            _materialunitslinkRepo = materialUnitsLinkRepo;
            _materialunitslinkExporter = materialunitslinkExporter;
            _materialManager = materialManager;
            _unitManager = unitManager;
        }

        public async Task<PagedResultOutput<MaterialUnitsLinkListDto>> GetAll(GetMaterialUnitsLinkInput input)
        {
            var allItems = _materialunitslinkRepo.GetAll();
            if (!string.IsNullOrEmpty(input.Operation) && input.Operation.Equals("SEARCH"))
            {
                allItems = _materialunitslinkRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.MaterialRefId.Equals(input.Filter)
               );
            }
            else
            {
                allItems = _materialunitslinkRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.MaterialRefId.ToString().Contains(input.Filter)
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<MaterialUnitsLinkListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<MaterialUnitsLinkListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _materialunitslinkRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<MaterialUnitsLinkListDto>>();
            return _materialunitslinkExporter.ExportToFile(allListDtos);
        }

        public async Task<GetMaterialUnitsLinkForEditOutput> GetMaterialUnitsLinkForEdit(NullableIdInput input)
        {
            MaterialUnitsLinkEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _materialunitslinkRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<MaterialUnitsLinkEditDto>();
            }
            else
            {
                editDto = new MaterialUnitsLinkEditDto();
            }

            return new GetMaterialUnitsLinkForEditOutput
            {
                MaterialUnitsLink = editDto
            };
        }

        public async Task CreateOrUpdateMaterialUnitsLink(CreateOrUpdateMaterialUnitsLinkInput input)
        {
            if (input.MaterialUnitsLink.Id.HasValue)
            {
                await UpdateMaterialUnitsLink(input);
            }
            else
            {
                await CreateMaterialUnitsLink(input);
            }
        }

        public async Task DeleteMaterialUnitsLink(IdInput input)
        {
            await _materialunitslinkRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateMaterialUnitsLink(CreateOrUpdateMaterialUnitsLinkInput input)
        {
            var item = await _materialunitslinkRepo.GetAsync(input.MaterialUnitsLink.Id.Value);
            var dto = input.MaterialUnitsLink;

            item.MaterialRefId = dto.MaterialRefId;
            item.UnitRefId = dto.UnitRefId;

            //TODO: SERVICE MaterialUnitsLink Update Individually

            CheckErrors(await _materialunitslinkManager.CreateSync(item));
        }

        protected virtual async Task CreateMaterialUnitsLink(CreateOrUpdateMaterialUnitsLinkInput input)
        {
            var dto = input.MaterialUnitsLink.MapTo<MaterialUnitsLink>();

            CheckErrors(await _materialunitslinkManager.CreateSync(dto));
        }

        public async Task<ListResultOutput<MaterialUnitsLinkListDto>> GetMaterialNames()
        {
            var lstMaterialUnitsLink = await _materialunitslinkRepo.GetAll().ToListAsync();
            return new ListResultOutput<MaterialUnitsLinkListDto>(lstMaterialUnitsLink.MapTo<List<MaterialUnitsLinkListDto>>());
        }

        public async Task<PagedResultOutput<MaterialUnitsLinkViewDto>> GetView(GetMaterialUnitsLinkInput input)
        {
            var allItems = (from mulm in _materialunitslinkRepo.GetAll()
                            join un in _unitManager.GetAll()
                            on mulm.UnitRefId equals un.Id
                            join material in _materialManager.GetAll()
                            on mulm.MaterialRefId equals material.Id
                            select new MaterialUnitsLinkViewDto()
                            {
                                Id = mulm.Id,
                                MaterialName = material.MaterialName,
                                UnitName = un.Name,
                                CreationTime = mulm.CreationTime,
                            }).WhereIf(
                               !input.Filter.IsNullOrEmpty(),
                               p => p.MaterialName.Contains(input.Filter)
               );

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<MaterialUnitsLinkViewDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<MaterialUnitsLinkViewDto>(
                allItemCount,
                allListDtos
                );
        }
    }
}
