﻿
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;


namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class MaterialBrandsLinkAppService : DineConnectAppServiceBase, IMaterialBrandsLinkAppService
    {

        private readonly IMaterialBrandsLinkListExcelExporter _materialbrandslinkExporter;
        private readonly IMaterialBrandsLinkManager _materialbrandslinkManager;
        private readonly IRepository<MaterialBrandsLink> _materialbrandslinkRepo;
        private readonly IRepository<Material> _material;
        private readonly IRepository<Brand> _brand;

        public MaterialBrandsLinkAppService(IMaterialBrandsLinkManager materialbrandslinkManager,
            IRepository<MaterialBrandsLink> materialBrandsLinkRepo,
            IMaterialBrandsLinkListExcelExporter materialbrandslinkExporter, IRepository<Material> material
            , IRepository<Brand> brand)
        {
            _materialbrandslinkManager = materialbrandslinkManager;
            _materialbrandslinkRepo = materialBrandsLinkRepo;
            _materialbrandslinkExporter = materialbrandslinkExporter;
            _material = material;
            _brand = brand;

        }

        public async Task<PagedResultOutput<MaterialBrandsLinkListDto>> GetAll(GetMaterialBrandsLinkInput input)
        {
            var allItems = _materialbrandslinkRepo.GetAll();
            if (!string.IsNullOrEmpty(input.Operation) && input.Operation.Equals("SEARCH"))
            {
                allItems = _materialbrandslinkRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Id.Equals(input.Filter)
               );
            }
            else
            {
                allItems = _materialbrandslinkRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Id.ToString().Contains(input.Filter)
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<MaterialBrandsLinkListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<MaterialBrandsLinkListDto>(
                allItemCount,
                allListDtos
                );
        }
        public async Task<PagedResultOutput<MaterialBrandsLinkViewDto>> GetView(GetMaterialBrandsLinkInput input)
        {
            var allItems = (from mwb in _materialbrandslinkRepo.GetAll()
                            join b in _brand.GetAll() on
                            mwb.BrandRefId equals b.Id
                            join mt in _material.GetAll() on
                            mwb.MaterialRefId equals mt.Id
                            select new MaterialBrandsLinkViewDto()
                            {
                                Id = mwb.Id,
                                MaterialRefName = mt.MaterialName,
                                BrandRefName = b.Name,
                                
                            }).WhereIf(
                                !input.Filter.IsNullOrEmpty(),
                                p => p.MaterialRefName.Contains(input.Filter)

               );


            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<MaterialBrandsLinkViewDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<MaterialBrandsLinkViewDto>(
                allItemCount,
                allListDtos
                );
        }
        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _materialbrandslinkRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<MaterialBrandsLinkListDto>>();
            return _materialbrandslinkExporter.ExportToFile(allListDtos);
        }

        public async Task<GetMaterialBrandsLinkForEditOutput> GetMaterialBrandsLinkForEdit(NullableIdInput input)
        {
            MaterialBrandsLinkEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _materialbrandslinkRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<MaterialBrandsLinkEditDto>();
            }
            else
            {
                editDto = new MaterialBrandsLinkEditDto();
            }

            return new GetMaterialBrandsLinkForEditOutput
            {
                MaterialBrandsLink = editDto
            };
        }

        public async Task CreateOrUpdateMaterialBrandsLink(CreateOrUpdateMaterialBrandsLinkInput input)
        {
            if (input.MaterialBrandsLink.Id.HasValue)
            {
                await UpdateMaterialBrandsLink(input);
            }
            else
            {
                await CreateMaterialBrandsLink(input);
            }
        }

        public async Task DeleteMaterialBrandsLink(IdInput input)
        {
            await _materialbrandslinkRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateMaterialBrandsLink(CreateOrUpdateMaterialBrandsLinkInput input)
        {
            var item = await _materialbrandslinkRepo.GetAsync(input.MaterialBrandsLink.Id.Value);
            var dto = input.MaterialBrandsLink;

            //TODO: SERVICE MaterialBrandsLink Update Individually
            item.MaterialRefId = dto.MaterialRefId;
            item.BrandRefId = dto.BrandRefId;
            CheckErrors(await _materialbrandslinkManager.CreateSync(item));
        }

        protected virtual async Task CreateMaterialBrandsLink(CreateOrUpdateMaterialBrandsLinkInput input)
        {
            var dto = input.MaterialBrandsLink.MapTo<MaterialBrandsLink>();

            CheckErrors(await _materialbrandslinkManager.CreateSync(dto));
        }

        public async Task<ListResultOutput<MaterialBrandsLinkListDto>> GetIds()
        {
            var lstMaterialBrandsLink = await _materialbrandslinkRepo.GetAll().ToListAsync();
            return new ListResultOutput<MaterialBrandsLinkListDto>(lstMaterialBrandsLink.MapTo<List<MaterialBrandsLinkListDto>>());
        }
        public async Task<ListResultOutput<MaterialBrandsLinkListDto>> GetMaterialForBrand(IdInput input)
        {
            var lstMaterialWiseBrand = await _materialbrandslinkRepo.GetAll().Where(a => a.BrandRefId == input.Id).ToListAsync();
            return new ListResultOutput<MaterialBrandsLinkListDto>(lstMaterialWiseBrand.MapTo<List<MaterialBrandsLinkListDto>>());
        }
        public async Task<ListResultOutput<MaterialBrandsLinkListDto>> GetBrandForMaterial(IdInput input)
        {
            var lstMaterialWiseBrand = await _materialbrandslinkRepo.GetAll().Where(a => a.MaterialRefId == input.Id).ToListAsync();
            return new ListResultOutput<MaterialBrandsLinkListDto>(lstMaterialWiseBrand.MapTo<List<MaterialBrandsLinkListDto>>());
        }
    }
}
