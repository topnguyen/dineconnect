﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using Abp.Domain.Uow;
using System;

namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class UnitConversionAppService : DineConnectAppServiceBase, IUnitConversionAppService
    {

        private readonly IUnitConversionListExcelExporter _unitconversionExporter;
        private readonly IUnitConversionManager _unitconversionManager;
        private readonly IRepository<UnitConversion> _unitConversionRepo;
        private readonly IRepository<Unit> _unitRepo;
        private readonly IRepository<MaterialUnitsLink> _materialUnitLinkRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<UnitConversionVsSupplier> _unitConversionVsSupplierRepo;
        private readonly IRepository<Supplier> _supplierRepo;
        private readonly IRepository<InterTransferDetail> _intertransferDetailRepo;

        public UnitConversionAppService(IUnitConversionManager unitconversionManager,
            IRepository<UnitConversion> unitConversionRepo,
            IUnitConversionListExcelExporter unitconversionExporter, IRepository<Unit> unitManager,
            IRepository<MaterialUnitsLink> materialUnitLinkRepo,
            IRepository<Material> materialRepo,
            IRepository<UnitConversionVsSupplier> unitConversionVsSupplierRepo,
            IRepository<Supplier> supplierRepo,
            IRepository<InterTransferDetail> intertransferDetailRepo)
        {
            _unitconversionManager = unitconversionManager;
            _unitConversionRepo = unitConversionRepo;
            _unitconversionExporter = unitconversionExporter;
            _unitRepo = unitManager;
            _materialUnitLinkRepo = materialUnitLinkRepo;
            _materialRepo = materialRepo;
            _unitConversionVsSupplierRepo = unitConversionVsSupplierRepo;
            _supplierRepo = supplierRepo;
            _intertransferDetailRepo = intertransferDetailRepo;
        }

        public async Task<PagedResultOutput<UnitConversionListDto>> GetAll(GetUnitConversionInput input)
        {
            var allItems = _unitConversionRepo.GetAll();
            if (!string.IsNullOrEmpty(input.Operation) && input.Operation.Equals("SEARCH"))
            {
                allItems = _unitConversionRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Id.Equals(input.Filter)
               );
            }
            else
            {
                allItems = _unitConversionRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Id.ToString().Contains(input.Filter)
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<UnitConversionListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<UnitConversionListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _unitConversionRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<UnitConversionListDto>>();
            return _unitconversionExporter.ExportToFile(allListDtos);
        }

        public async Task<GetUnitConversionForEditOutput> GetUnitConversionForEdit(NullableIdInput input)
        {
            UnitConversionEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _unitConversionRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<UnitConversionEditDto>();
            }
            else
            {
                editDto = new UnitConversionEditDto();
            }

            return new GetUnitConversionForEditOutput
            {
                UnitConversion = editDto
            };
        }


        [UnitOfWork]
        public async Task<List<IdInput>> CreateOrUpdateUnitConversion(CreateOrUpdateUnitConversionInput input)
        {
            List<IdInput> idInputs = new List<IdInput>();
            int? recursiveId = null;

            var conversion = 1 / input.UnitConversion.Conversion;
            if (conversion == 0)
            {
                throw new UserFriendlyException(L("Reverse") + " " + L("ConverZeroErr"));
            }

            if (input.SupplierRefId.HasValue)
            {
                var supplier = await _supplierRepo.FirstOrDefaultAsync(t => t.Id == input.SupplierRefId.Value);
                var alreadSupplierLinked = input.UnitConversion.LinkedSupplierList.FirstOrDefault(t => t.Id == input.SupplierRefId.Value);
                if (alreadSupplierLinked == null)
                {
                    input.UnitConversion.LinkedSupplierList.Add(new SimpleSupplierListDto { Id = input.SupplierRefId.Value, SupplierName = supplier.SupplierName });
                }
                if (input.UnitConversion.Id.HasValue == false)
                {
                    // Check whether already have the link
                    var supplierLinkedRecords = await _unitConversionVsSupplierRepo.GetAllListAsync(t => t.SupplierRefId == input.SupplierRefId);
                    List<int> ucIds = supplierLinkedRecords.Select(t => t.UnitConversionRefId).ToList();
                    var existLinkSupplierUc = await _unitConversionRepo.GetAllListAsync(t => t.MaterialRefId == input.UnitConversion.MaterialRefId && ucIds.Contains(t.Id) && t.BaseUnitId == input.UnitConversion.BaseUnitId && t.RefUnitId == input.UnitConversion.RefUnitId);
                    if (existLinkSupplierUc.Count > 0)
                        input.UnitConversion.Id = existLinkSupplierUc.FirstOrDefault().Id;
                }
            }

            if (input.UnitConversion.LinkedSupplierList != null && input.UnitConversion.LinkedSupplierList.Count > 0)
            {
                input.UnitConversion.LinkedSupplierList = input.UnitConversion.LinkedSupplierList.Distinct().ToList();
            }

            if (input.UnitConversion.MaterialRefId.HasValue && input.RecursiveFlag == false)
            {
                var chkDefaultUom = await CheckMaterialDefaultUOM(new IdInput { Id = input.UnitConversion.MaterialRefId.Value });
                if (chkDefaultUom.ErrorFlag)
                {
                    throw new UserFriendlyException(chkDefaultUom.ErrorMessage);
                }
            }

            UnitConversion exists = null;
            if (input.UnitConversion.Id.HasValue)
                exists = await _unitConversionRepo.FirstOrDefaultAsync(t => t.Id == input.UnitConversion.Id.Value);

            if (exists == null && input.UnitConversion.MaterialRefId.HasValue == false)
            {
                var existRecords = await _unitConversionRepo.GetAllListAsync(t => t.BaseUnitId == input.UnitConversion.BaseUnitId && t.RefUnitId == input.UnitConversion.RefUnitId && t.MaterialRefId == input.UnitConversion.MaterialRefId);
                if (existRecords.Count == 0)
                {
                    // No more records
                }
                else if (existRecords.Count == 1)
                {
                    exists = existRecords.FirstOrDefault();
                }
                else if (existRecords.Count > 1)
                {
                    exists = await _unitConversionRepo.FirstOrDefaultAsync(t => t.BaseUnitId == input.UnitConversion.BaseUnitId && t.RefUnitId == input.UnitConversion.RefUnitId && t.MaterialRefId == input.UnitConversion.MaterialRefId && t.Conversion == input.UnitConversion.Conversion);
                    if (exists == null)
                    {
                        //  Error message
                    }
                }
            }

            if (exists != null)
                input.UnitConversion.Id = exists.Id;

            #region Business Rule - Same Material /Same Base Unit Id / Same Ref Unit
            if (input.UnitConversion.MaterialRefId.HasValue)
            {
                if (input.UnitConversion.LinkedSupplierList == null || input.UnitConversion.LinkedSupplierList.Count == 0) // No more supplier linked
                {
                    var existSameMaterialConversionWithSuppliers = await _unitConversionRepo.GetAllListAsync(t => t.MaterialRefId == input.UnitConversion.MaterialRefId && t.BaseUnitId == input.UnitConversion.BaseUnitId && t.RefUnitId == input.UnitConversion.RefUnitId && t.Id != input.UnitConversion.Id);
                    if (existSameMaterialConversionWithSuppliers.Count > 0)
                    {
                        var ucIds = existSameMaterialConversionWithSuppliers.Select(t => t.Id).ToList();
                        var existSupplierLinked = await _unitConversionVsSupplierRepo.GetAllListAsync(t => ucIds.Contains(t.UnitConversionRefId));
                        if (existSupplierLinked.Count > 0)
                        {
                            var anySupplierLinedUCIds = existSupplierLinked.Select(t => t.UnitConversionRefId).ToList();
                            existSameMaterialConversionWithSuppliers = existSameMaterialConversionWithSuppliers.Where(t => !anySupplierLinedUCIds.Contains(t.Id)).ToList();
                        }
                        if (existSameMaterialConversionWithSuppliers.Count > 0)
                        {
                            var existSameMaterialConversion = existSameMaterialConversionWithSuppliers.FirstOrDefault();
                            var bu = await _unitRepo.FirstOrDefaultAsync(t => t.Id == existSameMaterialConversion.BaseUnitId);
                            var ru = await _unitRepo.FirstOrDefaultAsync(t => t.Id == existSameMaterialConversion.RefUnitId);
                            var mat = await _materialRepo.FirstOrDefaultAsync(t => t.Id == input.UnitConversion.MaterialRefId.Value);
                            throw new UserFriendlyException(L("SameLinkAlreadyExistWithDifferentFor_Material", bu.Name, ru.Name, mat.MaterialName, existSameMaterialConversion.Conversion));
                        }
                    }
                }
                else // Same Supplier linked
                {
                    var existSameMaterialConversionWithSuppliers = await _unitConversionRepo.GetAllListAsync(t => t.MaterialRefId == input.UnitConversion.MaterialRefId && t.BaseUnitId == input.UnitConversion.BaseUnitId && t.RefUnitId == input.UnitConversion.RefUnitId && t.Id != input.UnitConversion.Id);
                    if (existSameMaterialConversionWithSuppliers.Count > 0)
                    {
                        var ucIds = existSameMaterialConversionWithSuppliers.Select(t => t.Id).ToList();
                        var existSupplierLinked = await _unitConversionVsSupplierRepo.GetAllListAsync(t => ucIds.Contains(t.UnitConversionRefId));
                        if (existSupplierLinked.Count > 0)
                        {
                            var newSupplierRefIdsToBeAdded = input.UnitConversion.LinkedSupplierList.Select(t => t.Id).ToList();
                            var anySupplierIdsExists = existSupplierLinked.Where(t => newSupplierRefIdsToBeAdded.Contains(t.SupplierRefId)).ToList();
                            existSameMaterialConversionWithSuppliers = existSameMaterialConversionWithSuppliers.Where(t => anySupplierIdsExists.Select(a => a.UnitConversionRefId).ToList().Contains(t.Id)).ToList();
                            if (existSameMaterialConversionWithSuppliers.Count > 0)
                            {
                                var firstRec = existSameMaterialConversionWithSuppliers.FirstOrDefault();
                                var bu = await _unitRepo.FirstOrDefaultAsync(t => t.Id == firstRec.BaseUnitId);
                                var ru = await _unitRepo.FirstOrDefaultAsync(t => t.Id == firstRec.RefUnitId);
                                var mat = await _materialRepo.FirstOrDefaultAsync(t => t.Id == input.UnitConversion.MaterialRefId.Value);
                                var supId = anySupplierIdsExists.FirstOrDefault();
                                var sup = await _supplierRepo.FirstOrDefaultAsync(t => t.Id == supId.SupplierRefId);
                                throw new UserFriendlyException(L("SameLinkAlreadyExistWithDifferentFor_Material_Supplier", bu.Name, ru.Name, mat.MaterialName, existSameMaterialConversionWithSuppliers[0].Conversion, sup.SupplierName));
                            }
                        }
                    }
                }
            }
            #endregion

            if (input.UnitConversion.Id.HasValue)
            {
                var ret = await UpdateUnitConversion(input);
                idInputs.Add(ret);
                recursiveId = ret.Id;
            }
            else
            {
                var ret = await CreateUnitConversion(input);
                idInputs.Add(ret);
                recursiveId = ret.Id;
            }

            if (input.RecursiveFlag == false)
            {
                int? existId = null;
                if (input.UnitConversion.Id.HasValue)
                {
                    var alreadyLinkedUc = await _unitConversionRepo.FirstOrDefaultAsync(t => t.ReferenceRecursiveId == input.UnitConversion.Id.Value);
                    if (alreadyLinkedUc == null)
                    {
                        var existRecord = await _unitConversionRepo.FirstOrDefaultAsync(t => t.BaseUnitId == input.UnitConversion.RefUnitId && t.RefUnitId == input.UnitConversion.BaseUnitId && t.MaterialRefId == input.UnitConversion.MaterialRefId);
                        if (existRecord != null)
                        {
                            existId = existRecord.Id;
                        }
                    }
                    else
                    {
                        existId = alreadyLinkedUc.Id;
                    }
                }

                conversion = 1 / input.UnitConversion.Conversion;

                #region checkExistConversion
                var decimalPart = conversion - Math.Floor(conversion);
                if (decimalPart >= (decimal)0.9999999m)
                {
                    conversion = Math.Floor(conversion);
                }

                #endregion

                if (conversion == 0)
                {
                    throw new UserFriendlyException(L("ConverZeroErr"));
                }

                UnitConversionEditDto uc = new UnitConversionEditDto
                {
                    Id = existId,
                    BaseUnitId = input.UnitConversion.RefUnitId,
                    RefUnitId = input.UnitConversion.BaseUnitId,
                    Conversion = conversion,
                    DecimalPlaceRounding = 14,
                    MaterialRefId = input.UnitConversion.MaterialRefId,
                    LinkedSupplierList = input.UnitConversion.LinkedSupplierList,
                    CreatedBasedOnRecursive = true,
                    ReferenceRecursiveId = recursiveId
                };

                var retList = await CreateOrUpdateUnitConversion(new CreateOrUpdateUnitConversionInput
                {
                    RecursiveFlag = true,
                    UnitConversion = uc
                });

                idInputs.AddRange(retList);
            }
            return idInputs;
        }

        public async Task<bool> DeleteUnitConversionMaterial(CreateOrUpdateUnitConversionInput input)
        {
            if (input.UnitConversion.Id == null || input.UnitConversion.Id == 0)
                return false;
            if (input.UnitConversion.MaterialRefId == null || input.UnitConversion.MaterialRefId == 0)
                return false;
            var ucLinked = await _unitConversionRepo.FirstOrDefaultAsync(t => t.ReferenceRecursiveId == input.UnitConversion.Id.Value);
            if (ucLinked != null)
            {
                await DeleteUnitConversion(new IdInput { Id = ucLinked.Id });
            }
            return await DeleteUnitConversion(new IdInput { Id = input.UnitConversion.Id.Value });
        }

        public async Task<bool> DeleteUnitConversion(IdInput input)
        {
            var uc = await _unitConversionRepo.FirstOrDefaultAsync(t => t.Id == input.Id);

            if (uc.MaterialRefId.HasValue)
            {
                var existAnyPendingRequest = await _intertransferDetailRepo.FirstOrDefaultAsync(t => t.MaterialRefId == uc.MaterialRefId && (t.UnitRefId==uc.BaseUnitId || t.UnitRefId==uc.RefUnitId));
                if (existAnyPendingRequest != null)
                {
                    var material = await _materialRepo.FirstOrDefaultAsync(t => t.Id==uc.MaterialRefId);
                    throw new UserFriendlyException(L("TransferRequestUnitReferenceAvaiableSoCouldNotDelete", material.MaterialName) + " Request Id : " + existAnyPendingRequest.InterTransferRefId);
                }
                await _unitConversionVsSupplierRepo.DeleteAsync(t => t.UnitConversionRefId == input.Id);
                await _unitConversionRepo.DeleteAsync(input.Id);
            }
            else
            {
                var rsMaterialUnitLink = await _materialUnitLinkRepo.GetAll().Where(t => t.UnitRefId == uc.BaseUnitId).ToListAsync();
                int[] matRefIds = rsMaterialUnitLink.Select(t => t.MaterialRefId).ToArray();
                var rsMaterials = await _materialRepo.GetAllListAsync(t => matRefIds.Contains(t.Id));
                rsMaterialUnitLink = await _materialUnitLinkRepo.GetAll().Where(t => matRefIds.Contains(t.MaterialRefId)).ToListAsync();

                if (rsMaterialUnitLink == null)
                {
                    await _unitConversionVsSupplierRepo.DeleteAsync(t => t.UnitConversionRefId == input.Id);
                    await _unitConversionRepo.DeleteAsync(input.Id);
                }
                else
                {
                    string MaterialNames = "";

                    foreach (var mat in rsMaterials)
                    {
                        if (MaterialNames.Length > 100)
                            break;

                        if (rsMaterialUnitLink.Where(t => t.MaterialRefId == mat.Id && (t.UnitRefId == uc.BaseUnitId || t.UnitRefId == uc.RefUnitId)).Count() > 1)
                        {
                            MaterialNames = MaterialNames + mat.MaterialName + ",";
                            continue;
                        }

                        //var rsMul = rsMaterialUnitLink.Where(t => t.MaterialRefId == mat.Id).ToList();
                        //foreach (var ml in rsMul)
                        //{
                        //    if (mat.DefaultUnitId == mat.IssueUnitId)
                        //        continue;
                        //    if ((mat.DefaultUnitId == uc.BaseUnitId || mat.IssueUnitId == uc.BaseUnitId) || (mat.DefaultUnitId == uc.RefUnitId && mat.IssueUnitId == uc.RefUnitId))
                        //    {
                        //        if (ml.UnitRefId == uc.BaseUnitId || ml.UnitRefId == uc.RefUnitId)
                        //        {
                        //            MaterialNames = MaterialNames + mat.MaterialName + ",";
                        //        }
                        //    }
                        //}

                    }

                    if (MaterialNames.Length == 0)
                    {
                        await _unitConversionVsSupplierRepo.DeleteAsync(t => t.UnitConversionRefId == input.Id);
                        await _unitConversionRepo.DeleteAsync(input.Id);
                    }
                    else
                    {
                        MaterialNames = MaterialNames.Left(MaterialNames.Length - 1);
                        throw new UserFriendlyException(L("UnitReferenceAvaiableSoCouldNotDelete", MaterialNames));
                    }
                }
            }
            await _unitConversionVsSupplierRepo.DeleteAsync(t => t.UnitConversionRefId == input.Id);
            return true;
        }

        public async Task DeleteUnitConversionBasedOnMaterial(UnitConversionEditDto input)
        {
            var uc = await _unitConversionRepo.FirstOrDefaultAsync(t => t.Id == input.Id);

            var rsMaterialUnitLink = await _materialUnitLinkRepo.GetAll().Where(t => t.UnitRefId == uc.BaseUnitId).ToListAsync();
            int[] matRefIds = rsMaterialUnitLink.Select(t => t.MaterialRefId).ToArray();
            var rsMaterials = await _materialRepo.GetAllListAsync(t => matRefIds.Contains(t.Id));
            rsMaterialUnitLink = await _materialUnitLinkRepo.GetAll().Where(t => matRefIds.Contains(t.MaterialRefId)).ToListAsync();


            if (rsMaterialUnitLink == null)
            {
                await _unitConversionRepo.DeleteAsync(input.Id.Value);
            }
            else
            {
                string MaterialNames = "";

                foreach (var mat in rsMaterials)
                {
                    if (MaterialNames.Length > 100)
                        break;

                    if (rsMaterialUnitLink.Where(t => t.MaterialRefId == mat.Id && (t.UnitRefId == uc.BaseUnitId || t.UnitRefId == uc.RefUnitId)).Count() > 1)
                    {
                        MaterialNames = MaterialNames + mat.MaterialName + ",";
                        continue;
                    }

                    //var rsMul = rsMaterialUnitLink.Where(t => t.MaterialRefId == mat.Id).ToList();
                    //foreach (var ml in rsMul)
                    //{
                    //    if (mat.DefaultUnitId == mat.IssueUnitId)
                    //        continue;
                    //    if ((mat.DefaultUnitId == uc.BaseUnitId || mat.IssueUnitId == uc.BaseUnitId) || (mat.DefaultUnitId == uc.RefUnitId && mat.IssueUnitId == uc.RefUnitId))
                    //    {
                    //        if (ml.UnitRefId == uc.BaseUnitId || ml.UnitRefId == uc.RefUnitId)
                    //        {
                    //            MaterialNames = MaterialNames + mat.MaterialName + ",";
                    //        }
                    //    }
                    //}
                }

                if (MaterialNames.Length == 0)
                    await _unitConversionRepo.DeleteAsync(input.Id.Value);
                else
                {
                    MaterialNames = MaterialNames.Left(MaterialNames.Length - 1);
                    throw new UserFriendlyException(L("UnitReferenceAvaiableSoCouldNotDelete", MaterialNames));
                }
            }

            await _unitConversionVsSupplierRepo.DeleteAsync(t => t.UnitConversionRefId == input.Id);
        }

        protected virtual async Task<IdInput> UpdateUnitConversion(CreateOrUpdateUnitConversionInput input)
        {
            var item = await _unitConversionRepo.GetAsync(input.UnitConversion.Id.Value);
            var dto = input.UnitConversion;
            item.BaseUnitId = dto.BaseUnitId;
            item.RefUnitId = dto.RefUnitId;
            item.Conversion = dto.Conversion;
            item.DecimalPlaceRounding = dto.DecimalPlaceRounding;

            item.SyncLastModification = dto.SyncLastModification;
            item.SyncId = dto.SyncId;
            await _unitConversionRepo.UpdateAsync(item);
            //CheckErrors(await _unitconversionManager.CreateSync(item));

            List<int> idsToBeRetained = new List<int>();
            if (input.UnitConversion.LinkedSupplierList != null && input.UnitConversion.LinkedSupplierList.Count > 0)
            {
                foreach (var supplier in input.UnitConversion.LinkedSupplierList)
                {
                    var existSupplierLink = await _unitConversionVsSupplierRepo.FirstOrDefaultAsync(t => t.UnitConversionRefId == item.Id && t.SupplierRefId == supplier.Id);
                    if (existSupplierLink == null)
                    {
                        UnitConversionVsSupplier unitConversionVsSupplier = new UnitConversionVsSupplier
                        {
                            UnitConversionRefId = item.Id,
                            SupplierRefId = supplier.Id
                        };
                        await _unitConversionVsSupplierRepo.InsertOrUpdateAndGetIdAsync(unitConversionVsSupplier);
                        idsToBeRetained.Add(unitConversionVsSupplier.Id);
                    }
                    else
                    {
                        idsToBeRetained.Add(existSupplierLink.Id);
                    }
                }
            }
            var toBeDeleted = await _unitConversionVsSupplierRepo.GetAll().Where(t => t.UnitConversionRefId == item.Id && !idsToBeRetained.Contains(t.Id)).ToListAsync();
            foreach (var delRec in toBeDeleted)
            {
                await _unitConversionVsSupplierRepo.DeleteAsync(delRec);
            }

            return new IdInput { Id = item.Id };
        }

        protected virtual async Task<IdInput> CreateUnitConversion(CreateOrUpdateUnitConversionInput input)
        {
            var dto = input.UnitConversion.MapTo<UnitConversion>();
            var retId = await _unitConversionRepo.InsertAndGetIdAsync(dto);
            //CheckErrors(await _unitconversionManager.CreateSync(dto));

            if (input.UnitConversion.LinkedSupplierList != null && input.UnitConversion.LinkedSupplierList.Count > 0)
            {
                foreach (var supplier in input.UnitConversion.LinkedSupplierList)
                {
                    UnitConversionVsSupplier unitConversionVsSupplier = new UnitConversionVsSupplier
                    {
                        UnitConversionRefId = dto.Id,
                        SupplierRefId = supplier.Id
                    };
                    await _unitConversionVsSupplierRepo.InsertOrUpdateAndGetIdAsync(unitConversionVsSupplier);
                }
            }
            return new IdInput { Id = retId };
        }

        public async Task<List<UnitConversionEditDto>> GetLinkedUnitCoversionForGivenMaterial(IdInput input)
        {
            var editUnitConversionList = (from uc in _unitConversionRepo.GetAll().Where(t => t.MaterialRefId == input.Id)
                                          join baseUnit in _unitRepo.GetAll() on uc.BaseUnitId equals baseUnit.Id
                                          join refUnit in _unitRepo.GetAll() on uc.RefUnitId equals refUnit.Id
                                          select new UnitConversionEditDto
                                          {
                                              Id = uc.Id,
                                              BaseUnitId = uc.BaseUnitId,
                                              BaseUom = baseUnit.Name,
                                              RefUnitId = uc.RefUnitId,
                                              RefUom = refUnit.Name,
                                              Conversion = uc.Conversion,
                                              DecimalPlaceRounding = uc.DecimalPlaceRounding,
                                              MaterialRefId = uc.MaterialRefId,
                                              DoesUpdateRequired = false,
                                              CreatedBasedOnRecursive = uc.CreatedBasedOnRecursive,
                                          }).ToList();

            if (editUnitConversionList.Count > 0)
            {
                List<int> ucIds = editUnitConversionList.Select(t => (int)t.Id).ToList();
                var rsLinkedWithSupplier = await _unitConversionVsSupplierRepo.GetAll().Where(t => ucIds.Contains(t.UnitConversionRefId)).ToListAsync();
                List<int> supIds = rsLinkedWithSupplier.Select(t => t.SupplierRefId).ToList();
                var rsSuppliers = await _supplierRepo.GetAll().Where(t => supIds.Contains(t.Id)).ToListAsync();
                foreach (var uc in editUnitConversionList)
                {
                    var defaultUc = await _unitConversionRepo.FirstOrDefaultAsync(t => t.BaseUnitId == uc.BaseUnitId && t.RefUnitId == uc.RefUnitId && t.MaterialRefId == null);
                    if (defaultUc != null)
                    {
                        uc.DefaultConversion = defaultUc.Conversion;
                        uc.DefaultDecimalPlaceRounding = defaultUc.DecimalPlaceRounding;
                    }

                    var linkedWithSupplier = rsLinkedWithSupplier.Where(t => t.UnitConversionRefId == uc.Id).ToList();
                    if (linkedWithSupplier.Count > 0)
                    {
                        supIds = linkedWithSupplier.Select(t => t.SupplierRefId).ToList();
                        uc.LinkedSupplierList = rsSuppliers.Where(t => supIds.Contains(t.Id)).ToList().MapTo<List<SimpleSupplierListDto>>();
                    }
                }
            }
            return editUnitConversionList;
        }

        public async Task<DefaultUomErrorMessageDto> CheckMaterialDefaultUOM(IdInput input)
        {
            DefaultUomErrorMessageDto output = new DefaultUomErrorMessageDto();
            var material = await _materialRepo.FirstOrDefaultAsync(t => t.Id == input.Id);
            output.Material = material.MapTo<SimpleMaterialListDto>();

            var unitLinked = await GetLinkedUnitCoversionForGivenMaterial(new IdInput { Id = input.Id });
            // Check material based any
            if (unitLinked.Count == 0)
            {
                return output;
            }

            var unit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == output.Material.DefaultUnitId);
            output.Material.DefaultUnitName = unit.Name;

            if (unitLinked.Count > 0)
            {
                unitLinked = unitLinked.Where(t => t.Conversion > 1).ToList();
            }
            output.UnitConversionEditDtos = unitLinked;

            List<int> ucIdsCanNotBeDefaultUoms = unitLinked.Select(t => t.BaseUnitId).ToList();
            string unitNameCanBeUOM = "";
            List<int> ucIdsCanBeDefaultUomRefIds = unitLinked.Where(t => !ucIdsCanNotBeDefaultUoms.Contains(t.BaseUnitId)).Select(t => t.BaseUnitId).ToList();

            var correctUomExists = ucIdsCanBeDefaultUomRefIds.Exists(t => t == output.Material.DefaultUnitId);
            if (correctUomExists)
            {
                return output;
            }
            if (ucIdsCanBeDefaultUomRefIds.Count > 0)
            {
                var units = await _unitRepo.GetAllListAsync(t => ucIdsCanBeDefaultUomRefIds.Contains(t.Id));
                var unitNames = units.Select(t => t.Name).ToList();
                unitNameCanBeUOM = string.Join(",", unitNames);
            }

            foreach (var uc in unitLinked.Where(t => ucIdsCanNotBeDefaultUoms.Contains(t.BaseUnitId)))
            {
                uc.ErrorRemarks = L("DefaultUOMCanNotBe", uc.BaseUom);
            }

            var doesDefaultUnitIdErrorExists = ucIdsCanNotBeDefaultUoms.Exists(t => t == output.Material.DefaultUnitId);
            if (doesDefaultUnitIdErrorExists == true)
            {
                output.ErrorMessage = L("DefaultUOMCanNotBe", output.Material.DefaultUnitName) + System.Environment.NewLine + System.Environment.NewLine + " " + L("UntiConversionMaterialWiseAlert");
            }
            //else if  (unitNameCanBeUOM == "")
            //{
            //    output.ErrorMessage = L("DefaultUOMCanNotBe", output.Material.DefaultUnitName) + System.Environment.NewLine + System.Environment.NewLine + " " + L("UntiConversionMaterialWiseAlert");
            //}
            else if (unitNameCanBeUOM.Length > 0)
            {
                output.ErrorMessage = L("DefaultUOMErrorMessage", unitNameCanBeUOM) + System.Environment.NewLine + System.Environment.NewLine + " " + L("UntiConversionMaterialWiseAlert");
            }

            return output;
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetReferenceUnitForCombobox(IdInput input)
        {
            var lst = await (from uc in _unitConversionRepo.GetAll().Where(a => a.BaseUnitId == input.Id)
                             join un in _unitRepo.GetAll()
                                 on uc.RefUnitId equals un.Id
                             select new ComboboxItemDto()
                             {
                                 Value = uc.RefUnitId.ToString(),
                                 DisplayText = un.Name
                             }).ToListAsync();

            //return lst;

            return new ListResultOutput<ComboboxItemDto>(lst.MapTo<List<ComboboxItemDto>>());

            //var lst1 = lst.MapTo<List<UnitConversionListDto>>();

            //return
            //  new ListResultOutput<ComboboxItemDto>(
            //      lst.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());

            //return
            //    new ListResultOutput<ComboboxItemDto>(lst.MapTo<ListResultOutput<ComboboxItemDto>()); 
            //        //lst.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());

        }

        public async Task<ListResultOutput<UnitConversionListDto>> GetReferenceUnitIds(IdInput input)
        {
            var lstUnitConversion = await _unitConversionRepo.GetAll().Where(a => a.BaseUnitId == input.Id).ToListAsync();

            return new ListResultOutput<UnitConversionListDto>(lstUnitConversion.MapTo<List<UnitConversionListDto>>());
        }

        public async Task<PagedResultOutput<UnitConversionWithNames>> GetAllWithNames(GetUnitConversionInput input)
        {
            var allItems = (from uc in _unitConversionRepo.GetAll().Where(t => t.MaterialRefId == null)
                            join un in _unitRepo.GetAll()
                                on uc.BaseUnitId equals un.Id
                            join unr in _unitRepo.GetAll()
                                on uc.RefUnitId equals unr.Id
                            select new UnitConversionWithNames()
                            {
                                Id = uc.Id,
                                BaseUnitId = uc.BaseUnitId,
                                RefUnitId = uc.RefUnitId,
                                Conversion = uc.Conversion,
                                BaseUnitName = un.Name,
                                ConvertedUnitName = unr.Name,
                                CreationTime = uc.CreationTime,
                                DecimalPlaceRounding = uc.DecimalPlaceRounding,
                                CreatedBasedOnRecursive = uc.CreatedBasedOnRecursive
                            }).WhereIf(
                               !input.Filter.IsNullOrEmpty(),
                               p => p.BaseUnitId.ToString().Equals(input.Filter)
               );

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.ToList(); // sortMenuItems.MapTo<List<UnitConversionListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<UnitConversionWithNames>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<PagedResultOutput<UnitConversionListDto>> GetAllList(GetUnitConversionInput input)
        {
            var sortMenuItems = await _unitConversionRepo.GetAll().OrderBy(input.Sorting)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<UnitConversionListDto>>();

            var allItemCount = allListDtos.Count();

            return new PagedResultOutput<UnitConversionListDto>(
                allItemCount,
                allListDtos
                );
        }


        public async Task<List<UnitConversionListDto>> GetAllUnitConversionWithBaseIds()
        {
            var rsUnits = await _unitRepo.GetAllListAsync();
            int[] unitRefIds = rsUnits.Select(t => t.Id).ToArray();

            var allItems = await _unitConversionRepo.GetAll().Where(t => unitRefIds.Contains(t.BaseUnitId) || unitRefIds.Contains(t.RefUnitId)).OrderBy(t => t.BaseUnitId).ToListAsync();

            var allListDtos = allItems.MapTo<List<UnitConversionListDto>>();
            List<int> ucIds = allListDtos.Select(t => (int)t.Id).ToList();
            var rsLinkedWithSupplier = await _unitConversionVsSupplierRepo.GetAll().Where(t => ucIds.Contains(t.UnitConversionRefId)).ToListAsync();
            List<int> supIds = rsLinkedWithSupplier.Select(t => t.SupplierRefId).ToList();
            var rsSuppliers = await _supplierRepo.GetAll().Where(t => supIds.Contains(t.Id)).ToListAsync();

            List<UnitConversionListDto> missedConversionList = new List<UnitConversionListDto>();
            foreach (var lst in allListDtos)
            {
                var convUnit = allListDtos.Where(t => (t.BaseUnitId == lst.RefUnitId && t.RefUnitId == lst.BaseUnitId)).ToList();

                var linkedWithSupplier = rsLinkedWithSupplier.Where(t => t.UnitConversionRefId == lst.Id).ToList();
                if (linkedWithSupplier.Count > 0)
                {
                    supIds = linkedWithSupplier.Select(t => t.SupplierRefId).ToList();
                    lst.LinkedSupplierIds = supIds;
                    lst.LinkedSupplierList = rsSuppliers.Where(t => supIds.Contains(t.Id)).ToList().MapTo<List<SimpleSupplierListDto>>();
                }

                if (convUnit.Count == 0)
                {
                    if (lst.Conversion == 0)
                    {
                        var du = await _unitRepo.FirstOrDefaultAsync(t => t.Id == lst.RefUnitId);
                        var iu = await _unitRepo.FirstOrDefaultAsync(t => t.Id == lst.BaseUnitId);

                        throw new UserFriendlyException(L("ConversionFactorShouldNotBeZero", iu.Name, du.Name));
                    }
                    missedConversionList.Add(new UnitConversionListDto
                    {
                        MaterialRefId = lst.MaterialRefId,
                        BaseUnitId = lst.RefUnitId,
                        RefUnitId = lst.BaseUnitId,
                        Conversion = 1 / lst.Conversion,
                        DecimalPlaceRounding = 6,
                        LinkedSupplierIds = lst.LinkedSupplierIds,
                        LinkedSupplierList = lst.LinkedSupplierList
                    });
                }
            }

            allListDtos.AddRange(missedConversionList);

            foreach (var ui in rsUnits)
            {
                allListDtos.Add(new UnitConversionListDto
                {
                    BaseUnitId = ui.Id,
                    RefUnitId = ui.Id,
                    Conversion = 1,
                    DecimalPlaceRounding = 14
                });
            }

            return allListDtos;
        }

        public IQueryable<UnitConversion> GetAllUnitConversionWithBaseIdsQueryable()
        {
            IQueryable<UnitConversion> allitems;

            var a = (from uc in _unitConversionRepo.GetAll()
                     join un in _unitRepo.GetAll() on uc.BaseUnitId equals un.Id into joined
                     from un in joined.DefaultIfEmpty()
                     select un);


            allitems = (from un in _unitRepo.GetAll()
                        join uc in _unitConversionRepo.GetAll() on un.Id equals uc.BaseUnitId into joined
                        from uc in joined.DefaultIfEmpty()
                        select new UnitConversion
                        {
                            BaseUnitId = un.Id,
                            RefUnitId = uc == null ? un.Id : uc.RefUnitId,
                            Conversion = uc == null ? 1 : uc.Conversion
                        });
            return allitems;
        }

        public async Task<List<DefaultUomErrorMessageDto>> ErrorMaterialDefaultUOMReport()
        {
            List<DefaultUomErrorMessageDto> outputReturnList = new List<DefaultUomErrorMessageDto>();
            var tempMaterials = await _materialRepo.GetAllListAsync();
            var rsMaterials = tempMaterials.MapTo<List<SimpleMaterialListDto>>();
            var rsUnits = await _unitRepo.GetAllListAsync();
            var tempUc = await _unitConversionRepo.GetAll().Where(t => t.MaterialRefId.HasValue == true).ToListAsync();
            var rsUcList = tempUc.MapTo<List<UnitConversionEditDto>>();

            foreach (var lst in rsMaterials)
            {
                DefaultUomErrorMessageDto output = new DefaultUomErrorMessageDto();
                var material = rsMaterials.FirstOrDefault(t => t.Id == lst.Id);
                output.Material = material.MapTo<SimpleMaterialListDto>();

                //var unitLinked = await GetLinkedUnitCoversionForGivenMaterial(new IdInput { Id = input.Id });
                var unitLinked = rsUcList.Where(t => t.MaterialRefId == lst.Id).ToList();
                // Check material based any
                if (unitLinked.Count == 0)
                {
                    continue;
                }

                output.UnitConversionEditDtos = unitLinked;

                if (unitLinked.Count > 0)
                {
                    unitLinked = unitLinked.Where(t => t.Conversion > 1).ToList();
                }

                List<int> ucIdsCanNotBeDefaultUoms = unitLinked.Select(t => t.BaseUnitId).ToList();
                string unitNameCanBeUOM = "";
                List<int> ucIdsCanBeDefaultUomRefIds = unitLinked.Where(t => !ucIdsCanNotBeDefaultUoms.Contains(t.BaseUnitId)).Select(t => t.BaseUnitId).ToList();

                foreach (var uc in unitLinked.Where(t => ucIdsCanNotBeDefaultUoms.Contains(t.BaseUnitId)))
                {
                    uc.ErrorRemarks = L("DefaultUOMCanNotBe", uc.BaseUom);
                }

                var correctUomExists = ucIdsCanBeDefaultUomRefIds.Exists(t => t == output.Material.DefaultUnitId);
                if (correctUomExists)
                {
                    bool errorFlaginCoversion = false;
                    foreach (var uc in output.UnitConversionEditDtos)
                    {

                        var baseUc = rsUnits.FirstOrDefault(t => t.Id == uc.BaseUnitId);
                        uc.BaseUom = baseUc.Name;

                        var refUc = rsUnits.FirstOrDefault(t => t.Id == uc.RefUnitId);
                        uc.RefUom = refUc.Name;

                        uc.ErrorRemarks = "";
                        #region Check Conversion Error
                        if (uc.ReferenceRecursiveId.HasValue)
                        {
                            var reverseConversion = output.UnitConversionEditDtos.FirstOrDefault(t => t.Id == uc.ReferenceRecursiveId);
                            if (reverseConversion != null)
                            {
                                var conversionToBeCheck = 1 / uc.Conversion;
                                var diffConv = reverseConversion.Conversion - conversionToBeCheck;

                                if (diffConv > 0.00001m)
                                {
                                    uc.ErrorRemarks = "Conversion not set correctly, instead of " + conversionToBeCheck + " conversion " + reverseConversion.Conversion + " is stored";
                                    errorFlaginCoversion = true;
                                }
                                else
                                {
                                    uc.ErrorRemarks = "";
                                }
                            }
                            else
                            {
                                uc.ErrorRemarks = "";
                            }
                        }
                        #endregion
                    }
                    if (errorFlaginCoversion == true)
                    {

                    }
                    else
                    {
                        continue;
                    }
                }
                else
                {
                    if (ucIdsCanBeDefaultUomRefIds.Count > 0)
                    {
                        var units = rsUnits.Where(t => ucIdsCanBeDefaultUomRefIds.Contains(t.Id)).ToList();
                        var unitNames = units.Select(t => t.Name).ToList();
                        unitNameCanBeUOM = string.Join(",", unitNames);
                    }

                    var doesDefaultUnitIdErrorExists = ucIdsCanNotBeDefaultUoms.Exists(t => t == output.Material.DefaultUnitId);
                    if (doesDefaultUnitIdErrorExists == true)
                    {
                        output.ErrorMessage = L("DefaultUOMCanNotBe", output.Material.DefaultUnitName);
                    }
                    else if (unitNameCanBeUOM.Length > 0)
                    {
                        output.ErrorMessage = L("DefaultUOMErrorMessage", unitNameCanBeUOM) + System.Environment.NewLine + System.Environment.NewLine + " " + L("UntiConversionMaterialWiseAlert");
                    }
                    else
                    {
                        continue;
                    }
                    var unit = rsUnits.FirstOrDefault(t => t.Id == material.DefaultUnitId);
                    output.Material.DefaultUnitName = unit.Name;

                    foreach (var uc in output.UnitConversionEditDtos)
                    {
                        var baseUc = rsUnits.FirstOrDefault(t => t.Id == uc.BaseUnitId);
                        uc.BaseUom = baseUc.Name;

                        var refUc = rsUnits.FirstOrDefault(t => t.Id == uc.RefUnitId);
                        uc.RefUom = refUc.Name;

                        uc.ErrorRemarks = "";
                        #region Check Conversion Error
                        if (uc.ReferenceRecursiveId.HasValue)
                        {
                            var reverseConversion = output.UnitConversionEditDtos.FirstOrDefault(t => t.Id == uc.ReferenceRecursiveId);
                            if (reverseConversion != null)
                            {
                                var conversionToBeCheck = 1 / uc.Conversion;
                                var diffConv = reverseConversion.Conversion - conversionToBeCheck;

                                if (diffConv > 0.00001m)
                                {
                                    uc.ErrorRemarks = "Conversion not set correctly, instead of " + conversionToBeCheck + " conversion " + reverseConversion.Conversion + " is stored";
                                }
                                else
                                {
                                    uc.ErrorRemarks = "";
                                }
                            }
                            else
                            {
                                uc.ErrorRemarks = "";
                            }
                        }
                        #endregion
                    }
                    output.UnitConversionEditDtos = output.UnitConversionEditDtos.OrderByDescending(t => t.Conversion).ToList();
                    if (output.UnitConversionEditDtos.Count > 0)
                    {
                        var lastOne = output.UnitConversionEditDtos.LastOrDefault();
                        output.DefaultUnitMightBeRefId = lastOne.BaseUnitId;
                        output.DefaultUnitMightBeRefName = lastOne.BaseUom;
                    }
                }
                outputReturnList.Add(output);
            }

            return outputReturnList;
        }

    }
}
