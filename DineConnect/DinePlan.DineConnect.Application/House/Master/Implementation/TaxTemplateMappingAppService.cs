﻿
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;


namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class TaxTemplateMappingAppService : DineConnectAppServiceBase, ITaxTemplateMappingAppService
    {

        private readonly ITaxTemplateMappingListExcelExporter _taxtemplatemappingExporter;
        private readonly ITaxTemplateMappingManager _taxtemplatemappingManager;
        private readonly IRepository<TaxTemplateMapping> _taxtemplatemappingRepo;

        public TaxTemplateMappingAppService(ITaxTemplateMappingManager taxtemplatemappingManager,
            IRepository<TaxTemplateMapping> taxTemplateMappingRepo,
            ITaxTemplateMappingListExcelExporter taxtemplatemappingExporter)
        {
            _taxtemplatemappingManager = taxtemplatemappingManager;
            _taxtemplatemappingRepo = taxTemplateMappingRepo;
            _taxtemplatemappingExporter = taxtemplatemappingExporter;

        }

        public async Task<PagedResultOutput<TaxTemplateMappingListDto>> GetAll(GetTaxTemplateMappingInput input)
        {
            var allItems = _taxtemplatemappingRepo.GetAll();
            if (input.Operation == "SEARCH")
            {
                allItems = _taxtemplatemappingRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Id.Equals(input.Filter)
               );
            }
            else
            {
                allItems = _taxtemplatemappingRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Id.ToString().Contains(input.Filter)
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<TaxTemplateMappingListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<TaxTemplateMappingListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _taxtemplatemappingRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<TaxTemplateMappingListDto>>();
            return _taxtemplatemappingExporter.ExportToFile(allListDtos);
        }

        public async Task<GetTaxTemplateMappingForEditOutput> GetTaxTemplateMappingForEdit(NullableIdInput input)
        {
            TaxTemplateMappingEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _taxtemplatemappingRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<TaxTemplateMappingEditDto>();
            }
            else
            {
                editDto = new TaxTemplateMappingEditDto();
            }

            return new GetTaxTemplateMappingForEditOutput
            {
                TaxTemplateMapping = editDto
            };
        }

        public async Task CreateOrUpdateTaxTemplateMapping(CreateOrUpdateTaxTemplateMappingInput input)
        {
            if (input.TaxTemplateMapping.Id.HasValue)
            {
                await UpdateTaxTemplateMapping(input);
            }
            else
            {
                await CreateTaxTemplateMapping(input);
            }
        }

        public async Task DeleteTaxTemplateMapping(IdInput input)
        {
            await _taxtemplatemappingRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateTaxTemplateMapping(CreateOrUpdateTaxTemplateMappingInput input)
        {
            var item = await _taxtemplatemappingRepo.GetAsync(input.TaxTemplateMapping.Id.Value);
            var dto = input.TaxTemplateMapping;

            //TODO: SERVICE TaxTemplateMapping Update Individually

            CheckErrors(await _taxtemplatemappingManager.CreateSync(item));
        }

        protected virtual async Task CreateTaxTemplateMapping(CreateOrUpdateTaxTemplateMappingInput input)
        {
            var dto = input.TaxTemplateMapping.MapTo<TaxTemplateMapping>();

            CheckErrors(await _taxtemplatemappingManager.CreateSync(dto));
        }

        public async Task<ListResultOutput<TaxTemplateMappingListDto>> GetPosMenuPortionRefIds()
        {
            var lstTaxTemplateMapping = await _taxtemplatemappingRepo.GetAll().ToListAsync();
            return new ListResultOutput<TaxTemplateMappingListDto>(lstTaxTemplateMapping.MapTo<List<TaxTemplateMappingListDto>>());
        }
    }
}
