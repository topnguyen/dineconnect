﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Impl;
using System;
using DinePlan.DineConnect.Connect.Sync.Dtos;
using DinePlan.DineConnect.Features;
using Abp.Application.Features;
using Abp.BackgroundJobs;
using Abp.Domain.Uow;
using DinePlan.DineConnect.Job;
using DinePlan.DineConnect.Job.House;

namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class SupplierAppService : DineConnectAppServiceBase, ISupplierAppService
    {

        private readonly ISupplierListExcelExporter _supplierExporter;
        private readonly ISupplierManager _supplierManager;
        private readonly IRepository<Supplier> _supplierRepo;
        private readonly IRepository<SupplierGSTInformation> _suppliergstinformationrepo;
        private readonly IBackgroundJobManager _bgm;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public SupplierAppService(ISupplierManager supplierManager,
            IRepository<Supplier> supplierRepo,
            ISupplierListExcelExporter supplierExporter, IUnitOfWorkManager unitOfWorkManager,
            IRepository<SupplierGSTInformation> suppliergstinformationrepo, IBackgroundJobManager bgm)
        {
            _supplierManager = supplierManager;
            _supplierRepo = supplierRepo;
            _supplierExporter = supplierExporter;
            _suppliergstinformationrepo = suppliergstinformationrepo;
            _bgm = bgm;
            _unitOfWorkManager = unitOfWorkManager;


        }

        public async Task<PagedResultOutput<SupplierListDto>> GetAll(GetSupplierInput input)
        {
            IQueryable<Supplier> allItems;
            if (!string.IsNullOrEmpty(input.Operation) && input.Operation.Equals("SEARCH"))
            {
                allItems = _supplierRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.SupplierName.Equals(input.Filter)
               );
            }
            else
            {
                allItems = _supplierRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.SupplierName.Contains(input.Filter)
               );
            }

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<SupplierListDto>>();
            foreach(var gp in allListDtos.GroupBy(t=>t.DefaultPayModeEnumRefId))
            {
                string payMode = "";
                if (gp.Key == (int)InvoicePayModeEnum.Cash)
                    payMode = L("Cash");
                else if (gp.Key == (int) InvoicePayModeEnum.Credit)
                    payMode = L("Credit");
                foreach(var lst in gp.ToList())
                {
                    lst.DefaultPayModeEnumRefName = payMode;
                }
            }
            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<SupplierListDto>(
                allItemCount,
                allListDtos
                );
        }


        public IdInput GetSupplierIdByName(string suppliername)
        {
            var lstSupId = _supplierRepo.GetAllList(a => a.SupplierName.Equals(suppliername)).ToList().FirstOrDefault();
            return new IdInput
            {
                Id = lstSupId != null ? lstSupId.Id : 0
            };
        }

        public async Task<FileDto> GetAllToExcel(GetSupplierInput input)
        {
            input.MaxResultCount = AppConsts.MaxPageSize;

            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<SupplierListDto>>();
            return _supplierExporter.ExportToFile(allListDtos);
        }

        public async Task<GetSupplierForEditOutput> GetSupplierForEdit(NullableIdInput input)
        {
            SupplierEditDto editDto;
            SupplierGSTInformationEditDto editGstDto;

            if (input.Id.HasValue)
            {
                var hDto = await _supplierRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<SupplierEditDto>();

                var sg = await _suppliergstinformationrepo.FirstOrDefaultAsync(t => t.SupplierRefId == editDto.Id);
                if (sg == null)
                    editGstDto = new SupplierGSTInformationEditDto();
                else
                    editGstDto = sg.MapTo<SupplierGSTInformationEditDto>();
            }
            else
            {
                editDto = new SupplierEditDto();
                editGstDto = new SupplierGSTInformationEditDto();

            }

            return new GetSupplierForEditOutput
            {
                Supplier = editDto,
                Suppliergstinformation = editGstDto
            };
        }


        public async Task CreateOrUpdateSupplier(CreateOrUpdateSupplierInput input)
        {
            var supplierId = 0;
            if (input.Supplier.Id.HasValue)
            {
                supplierId = input.Supplier.Id.Value;
                await UpdateSupplier(input);
            }
            else
            {
                supplierId = await CreateSupplier(input);
            }
            await _unitOfWorkManager.Current.SaveChangesAsync();

            await _bgm.EnqueueAsync<HouseIdentificationJob, HouseIdJobArgs>(new HouseIdJobArgs
            {
                SupplierId = supplierId
            });
        }

        public async Task DeleteSupplier(IdInput input)
        {
            await _supplierRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateSupplier(CreateOrUpdateSupplierInput input)
        {
            var item = await _supplierRepo.GetAsync(input.Supplier.Id.Value);
            var dto = input.Supplier;

            item.SupplierName = dto.SupplierName;
            item.Address1 = dto.Address1;
            item.Address2 = dto.Address2;
            item.Address3 = dto.Address3;
            item.City = dto.City;
            item.State = dto.State;
            item.Country = dto.Country;
            item.ZipCode = dto.ZipCode;
            item.PhoneNumber1 = dto.PhoneNumber1;
            item.Email = dto.Email;
            item.FaxNumber = dto.FaxNumber;
            item.Website = dto.Website;
            item.DefaultCreditDays = dto.DefaultCreditDays;
            item.OrderPlacedThrough = dto.OrderPlacedThrough;
            item.SupplierCode = dto.SupplierCode;
            item.AddOn = dto.AddOn;
            item.SyncLastModification = dto.SyncLastModification;
            item.SyncId = dto.SyncId;
            item.TaxRegistrationNumber = dto.TaxRegistrationNumber;
            item.LoadOnlyLinkedMaterials = dto.LoadOnlyLinkedMaterials;
            item.TaxApplicable = dto.TaxApplicable;
            item.DefaultPayModeEnumRefId = dto.DefaultPayModeEnumRefId;
            CheckErrors(await _supplierManager.CreateSync(item));

            var countryName = FeatureChecker.GetValue(AppFeatures.ConnectCountry);

            if (!string.IsNullOrWhiteSpace(countryName) && countryName.Equals("IN") && input.Suppliergstinformation != null)
            {
                var gstdto = input.Suppliergstinformation;
                if (!gstdto.Gstin.IsNullOrEmpty())
                {
                    var gstsupplier = await _suppliergstinformationrepo.FirstOrDefaultAsync(t => t.SupplierRefId == item.Id);
                    SupplierGSTInformation gstindiadto;
                    if (gstsupplier == null)
                    {
                        gstindiadto = new SupplierGSTInformation();
                    }
                    else
                    {
                        gstindiadto = await _suppliergstinformationrepo.GetAsync(gstsupplier.Id);
                    }
                    gstindiadto.SupplierRefId = item.Id;
                    gstindiadto.SupplierLegalName = gstdto.SupplierLegalName;
                    gstindiadto.Gstin = gstdto.Gstin;
                    gstindiadto.GstStatus = gstdto.GstStatus;
                    gstindiadto.ConstitutionStatus = gstdto.ConstitutionStatus;
                    gstindiadto.PayGstApplicable = gstdto.PayGstApplicable;
                    gstindiadto.ReverseChargeApplicable = gstdto.ReverseChargeApplicable;
                    gstindiadto.GstCertificate = gstdto.GstCertificate;

                    await _suppliergstinformationrepo.InsertOrUpdateAndGetIdAsync(gstindiadto);
                }
            }

        }

        protected virtual async Task<int> CreateSupplier(CreateOrUpdateSupplierInput input)
        {
            var dto = input.Supplier.MapTo<Supplier>();

            CheckErrors(await _supplierManager.CreateSync(dto));

            var countryName = FeatureChecker.GetValue(AppFeatures.ConnectCountry);

            if (!string.IsNullOrWhiteSpace(countryName) && countryName.Equals("IN"))
            {
                if (input.Suppliergstinformation != null && !input.Suppliergstinformation.Gstin.IsNullOrEmpty())
                {
                    var gstindiadto = input.Suppliergstinformation.MapTo<SupplierGSTInformation>();
                    gstindiadto.SupplierRefId = dto.Id;
                    await _suppliergstinformationrepo.InsertAndGetIdAsync(gstindiadto);
                    return dto.Id;
                }
            }
            return dto.Id;
        }

        //public async Task<ListResultOutput<SupplierListDto>> GetSimpleSupplierList(GetSupplierInput input)
        //{
        //    var lstSupplier = await _supplierRepo.GetAllListAsync();
        //    return new ListResultOutput<SimpleSupplierListDto>(lstSupplier.MapTo<List<SimpleSupplierListDto>>());
        //}

        public async Task<ListResultOutput<SimpleSupplierListDto>> GetSimpleSupplierListDtos()
        {
            var lstSupplier = await _supplierRepo.GetAllListAsync();
            return new ListResultOutput<SimpleSupplierListDto>(lstSupplier.MapTo<List<SimpleSupplierListDto>>());
        }

        public async Task<ListResultOutput<SupplierListDto>> GetSupplierNames()
        {
            var lstSupplier = await _supplierRepo.GetAllListAsync();
            return new ListResultOutput<SupplierListDto>(lstSupplier.MapTo<List<SupplierListDto>>());
        }

        public async Task<ListResultOutput<SupplierListDto>> ApiGetSupplier(TenantInput input)
        {
            var lstSupplier = await _supplierRepo.GetAllListAsync(a => a.TenantId == input.TenantId);
            return new ListResultOutput<SupplierListDto>(lstSupplier.MapTo<List<SupplierListDto>>());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetSupplierForCombobox()
        {
            var lstSupplier = await _supplierRepo.GetAll().ToListAsync();
            return
                new ListResultOutput<ComboboxItemDto>(
                    lstSupplier.Select(e => new ComboboxItemDto(e.Id.ToString(), e.SupplierName)).ToList());
        }

        public async Task<string> GetSupplierNameForParticularCode(EntityDto input)
        {
            var lst = await _supplierRepo.FirstOrDefaultAsync(t => t.Id == input.Id);
            if (lst == null)
                return "";
            return lst.SupplierName;
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetDocumentTypeForCombobox(NullableIdInput ninput)
        {
            List<ComboboxItemDto> retList = new List<ComboboxItemDto>();

            string enumstring;
            Array EnumValues = System.Enum.GetValues(typeof(SupplierDocumentType));
            foreach (int EnumValue in EnumValues)
            {
                enumstring = Enum.GetName(typeof(SupplierDocumentType), EnumValue);
                retList.Add(new ComboboxItemDto { Value = EnumValue.ToString(), DisplayText = enumstring });
            }

            return
                   new ListResultOutput<ComboboxItemDto>(
                       retList.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetGstStatusForCombobox(NullableIdInput ninput)
        {
            List<ComboboxItemDto> retList = new List<ComboboxItemDto>();

            string enumstring;
            Array EnumValues = System.Enum.GetValues(typeof(Supplier_Gst_Status));
            foreach (int EnumValue in EnumValues)
            {
                enumstring = Enum.GetName(typeof(Supplier_Gst_Status), EnumValue);
                retList.Add(new ComboboxItemDto { Value = EnumValue.ToString(), DisplayText = enumstring });
            }

            return
                   new ListResultOutput<ComboboxItemDto>(
                       retList.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetConstitutionStatusForCombobox(NullableIdInput ninput)
        {
            List<ComboboxItemDto> retList = new List<ComboboxItemDto>();

            string enumstring;
            Array EnumValues = System.Enum.GetValues(typeof(Supplier_Constitution));
            foreach (int EnumValue in EnumValues)
            {
                enumstring = Enum.GetName(typeof(Supplier_Constitution), EnumValue);
                retList.Add(new ComboboxItemDto { Value = EnumValue.ToString(), DisplayText = enumstring });
            }

            return
                   new ListResultOutput<ComboboxItemDto>(
                       retList.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText)).ToList());
        }


        public async Task<PagedResultOutput<SimpleSupplierListDto>> GetAllSimpleSupplier(GetSupplierInput input)
        {
            IQueryable<Supplier> allItems;

            allItems = _supplierRepo
           .GetAll()
           .WhereIf(
               !input.Filter.IsNullOrEmpty(),
               p => p.SupplierName.Contains(input.Filter)
           );

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<SimpleSupplierListDto>>();
            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<SimpleSupplierListDto>(
                allItemCount,
                allListDtos
                );
        }
    }
}
