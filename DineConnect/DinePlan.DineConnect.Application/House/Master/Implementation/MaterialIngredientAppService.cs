﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;


namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class MaterialIngredientAppService : DineConnectAppServiceBase, IMaterialIngredientAppService
    {

        private readonly IMaterialIngredientListExcelExporter _materialingredientExporter;
        private readonly IMaterialIngredientManager _materialingredientManager;
        private readonly IRepository<MaterialIngredient> _materialingredientRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<Unit> _unitRepo;

        public MaterialIngredientAppService(IMaterialIngredientManager materialingredientManager,
            IRepository<MaterialIngredient> materialIngredientRepo,
            IMaterialIngredientListExcelExporter materialingredientExporter,
            IRepository<Material> material,
            IRepository<Unit> unit)
        {
            _materialingredientManager = materialingredientManager;
            _materialingredientRepo = materialIngredientRepo;
            _materialingredientExporter = materialingredientExporter;
            _materialRepo = material;
            _unitRepo = unit;
        }

        public async Task<PagedResultOutput<MaterialIngredientListDto>> GetAll(GetMaterialIngredientInput input)
        {
            var allItems = _materialingredientRepo.GetAll();
            if (input.Operation == "SEARCH")
            {
                allItems = _materialingredientRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.RecipeRefId.Equals(input.Filter)
               );
            }
            else
            {
                allItems = _materialingredientRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.RecipeRefId.ToString().Contains(input.Filter)
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<MaterialIngredientListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<MaterialIngredientListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _materialingredientRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<MaterialIngredientListDto>>();
            return _materialingredientExporter.ExportToFile(allListDtos);
        }

        public async Task<GetMaterialIngredientForEditOutput> GetMaterialIngredientForEdit(NullableIdInput input)
        {
            MaterialIngredientEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _materialingredientRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<MaterialIngredientEditDto>();
            }
            else
            {
                editDto = new MaterialIngredientEditDto();
            }

            return new GetMaterialIngredientForEditOutput
            {
                MaterialIngredient = editDto
            };
        }

        public async Task CreateOrUpdateMaterialIngredient(CreateOrUpdateMaterialIngredientInput input)
        {
            if (input.MaterialIngredient.Id.HasValue)
            {
                await UpdateMaterialIngredient(input);
            }
            else
            {
                await CreateMaterialIngredient(input);
            }
        }

        public async Task DeleteMaterialIngredient(IdInput input)
        {
            await _materialingredientRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateMaterialIngredient(CreateOrUpdateMaterialIngredientInput input)
        {
            var item = await _materialingredientRepo.GetAsync(input.MaterialIngredient.Id.Value);
            var dto = input.MaterialIngredient;

            //TODO: SERVICE MaterialIngredient Update Individually

            CheckErrors(await _materialingredientManager.CreateSync(item));
        }

        protected virtual async Task CreateMaterialIngredient(CreateOrUpdateMaterialIngredientInput input)
        {
            var dto = input.MaterialIngredient.MapTo<MaterialIngredient>();

            CheckErrors(await _materialingredientManager.CreateSync(dto));
        }

        public async Task<ListResultOutput<MaterialIngredientListDto>> GetIds()
        {
            var lstMaterialIngredient = await _materialingredientRepo.GetAll().ToListAsync();
            return new ListResultOutput<MaterialIngredientListDto>(lstMaterialIngredient.MapTo<List<MaterialIngredientListDto>>());
        }

        public async Task<PagedResultOutput<MaterialIngredientViewDto>> GetViewByRefRecipeId(IdInput input)
        {
            var allItems = (from r in _materialingredientRepo.GetAll()
                            join m in _materialRepo.GetAll()
                            on r.MaterialRefId equals m.Id
                            join rec in _materialRepo.GetAll() on
                            r.RecipeRefId equals rec.Id
                            join u in _unitRepo.GetAll()
                            on r.UnitRefId equals u.Id
                            select new MaterialIngredientViewDto()
                            {
                                Id = r.Id,
                                RecipeRefId = r.RecipeRefId,
                                RecipeRefName = rec.MaterialName,
                                MaterialRefId = r.MaterialRefId,
                                MaterialRefName = m.MaterialName,
                                MaterialUsedQty = r.MaterialUsedQty,
                                UserSerialNumber = r.UserSerialNumber,
                                CreationTime = r.CreationTime,
                                VariationflagForProduction = r.VariationflagForProduction,
                                UnitRefId = u.Id,
                                UnitRefName = u.Name,
                            }).Where(p => p.RecipeRefId == input.Id);

            var sortMenuItems = await allItems.ToListAsync();

            var allListDtos = sortMenuItems.ToList();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<MaterialIngredientViewDto>(
                allItemCount,
                allListDtos
                );
        }
    }
}

