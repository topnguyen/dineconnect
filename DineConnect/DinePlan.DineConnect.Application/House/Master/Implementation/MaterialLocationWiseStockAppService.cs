﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.House.Transaction;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Location;
using System.Diagnostics;

namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class MaterialLocationWiseStockAppService : DineConnectAppServiceBase, IMaterialLocationWiseStockAppService
    {

        private readonly IMaterialLocationWiseStockListExcelExporter _materiallocationwisestockExporter;
        private readonly IMaterialLocationWiseStockManager _materiallocationwisestockManager;
        private readonly IRepository<MaterialLocationWiseStock> _materiallocationwisestockRepo;
        private readonly IMaterialAppService _materialAppService;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<CustomerTagMaterialPrice> _customertagmaterialpriceRepo;
        private readonly IRepository<CustomerTagDefinition> _customertagdefinitionRepo;
        private readonly IRepository<Unit> _unitrepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IAdjustmentAppService _adjustmentAppService;
        private readonly IUnitConversionAppService _unitConversionAppService;
        private readonly ILocationAppService _locService;

        public MaterialLocationWiseStockAppService(IMaterialLocationWiseStockManager materiallocationwisestockManager,
            IRepository<MaterialLocationWiseStock> materialLocationWiseStockRepo,
            IMaterialLocationWiseStockListExcelExporter materiallocationwisestockExporter
            , IRepository<Material> materialRepo,
            IMaterialAppService materialAppService,
          IRepository<Connect.Master.Location> location,
          IRepository<Unit> unitrepo,
          IRepository<Location> locationRepo,
          IAdjustmentAppService adjustmentAppService,
            IRepository<CustomerTagMaterialPrice> customertagmaterialpriceRepo,
            IRepository<CustomerTagDefinition> customertagdefinitionRepo,
            IUnitConversionAppService unitConversionAppService,
            ILocationAppService locService
            )
        {
            _materiallocationwisestockManager = materiallocationwisestockManager;
            _materiallocationwisestockRepo = materialLocationWiseStockRepo;
            _materiallocationwisestockExporter = materiallocationwisestockExporter;
            _materialRepo = materialRepo;
            _materialAppService = materialAppService;
            _customertagmaterialpriceRepo = customertagmaterialpriceRepo;
            _customertagdefinitionRepo = customertagdefinitionRepo;
            _unitrepo = unitrepo;
            _locationRepo = locationRepo;
            _adjustmentAppService = adjustmentAppService;
            _unitConversionAppService = unitConversionAppService;
            _locService = locService;

        }



        public async Task<PagedResultOutput<MaterialLocationWiseStockListDto>> GetAll(GetMaterialLocationWiseStockInput input)
        {
            var allItems = _materiallocationwisestockRepo.GetAll();
            if (!string.IsNullOrEmpty(input.Operation) && input.Operation.Equals("SEARCH"))
            {
                allItems = _materiallocationwisestockRepo
               .GetAll()
               .WhereIf(
                   !input.Material.IsNullOrEmpty(),
                   p => p.Material.MaterialName.Equals(input.Material)
               );
            }
            else
            {
                allItems = _materiallocationwisestockRepo
               .GetAll()
               .WhereIf(
                   !input.Material.IsNullOrEmpty(),
                   p => p.Material.MaterialName.ToString().Contains(input.Material)
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<MaterialLocationWiseStockListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<MaterialLocationWiseStockListDto>(
                allItemCount,
                allListDtos
                );
        }


        public async Task<PagedResultOutput<MaterialLocationWiseStockViewDto>> GetView(GetMaterialLocationWiseStockInput input)
        {
            Debug.WriteLine("MaterialLocationWiseStockAppService GetView() Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));

            var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();

            var material = _materialRepo.GetAll();

            material = material.WhereIf(input.IsHighValueItemOnly, t => t.IsHighValueItem == true);

            material = material.WhereIf(input.MaterialTypeRefId != null, t => t.MaterialTypeId == input.MaterialTypeRefId);
            material = material.WhereIf(!input.Material.IsNullOrEmpty(), t => (t.MaterialName.Contains(input.Material) || t.Barcode.Contains(input.Material) || t.Barcodes.Select(a => a.Barcode).Contains(input.Material)));

            if (input.MaterialRefIds != null)
                material = material.WhereIf(input.MaterialRefIds != null && input.MaterialRefIds.Any(), t => input.MaterialRefIds.Contains(t.Id));
            if (!input.Material.IsNullOrEmpty() && !input.Material.IsNullOrWhiteSpace())
                material = material.Where(t => t.MaterialName.Contains(input.Material));

            var allItems = (from msis in _materiallocationwisestockRepo.GetAll()
                            join l in _locationRepo.GetAll() on
                            msis.LocationRefId equals l.Id
                            join mat in material on
                            msis.MaterialRefId equals mat.Id
                            join un in _unitrepo.GetAll()
                            on mat.DefaultUnitId equals un.Id
                            join uiss in _unitrepo.GetAll() on mat.IssueUnitId equals uiss.Id
                            join utrn in _unitrepo.GetAll() on mat.TransferUnitId equals utrn.Id
                            select new MaterialLocationWiseStockViewDto()
                            {
                                Id = msis.Id,
                                LocationRefId = msis.LocationRefId,
                                LocationRefName = l.Name,
                                MaterialRefId = mat.Id,
                                Barcode = mat.Barcode,
                                MaterialRefName = mat.MaterialName,
                                CurrentInHand = msis.CurrentInHand,
                                MinimumStock = msis.MinimumStock,
                                MaximumStock = msis.MaximumStock,
                                ReOrderLevel = msis.ReOrderLevel,
                                IsOrderPlaced = msis.IsOrderPlaced,
                                IsActiveInLocation = msis.IsActiveInLocation,
                                Sales = 0,
                                Uom = un.Name,
                                LiveStock = 0,
                                DefaultUnitRefId = mat.DefaultUnitId,
                                TransactionUnitRefId = mat.IssueUnitId,
                                TransactionUnitName = uiss.Name,
                                TransferUnitRefId = mat.TransferUnitId,
                                TransferUnitRefName = utrn.Name,
                                ConvertAsZeroStockWhenClosingStockNegative = msis.ConvertAsZeroStockWhenClosingStockNegative,

                            }).Where(p => p.LocationRefId == input.LocationRefId);

            //var sortMenuItems = await allItems.OrderBy(input.Sorting).ToListAsync();
            var sortMenuItems = await allItems.OrderBy(input.Sorting)
              .PageBy(input)
              .ToListAsync();

            var mapWithMenuListSumGroup = await _materialAppService.GetLiveStockSales(input);

            int loopIndex = 0;
            foreach (var lst in sortMenuItems)
            {
                var existRecord = mapWithMenuListSumGroup.FirstOrDefault(t => t.MaterialRefId == lst.MaterialRefId);
                if (existRecord != null)
                {
                    lst.Sales = existRecord.PortionQty;
                }
                lst.LiveStock = lst.CurrentInHand - lst.Sales;

                if (lst.TransactionUnitRefId == lst.DefaultUnitRefId)
                    lst.LiveStock_TransactionUnit = lst.LiveStock;
                else
                {
                    var unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == lst.DefaultUnitRefId && t.RefUnitId == lst.TransactionUnitRefId);
                    if (unitConversion == null)
                        throw new UserFriendlyException(L("ConversionFactorNotExist", lst.Uom, lst.TransactionUnitName));
                    lst.LiveStock_TransactionUnit = unitConversion.Conversion * lst.LiveStock;
                }
                if (lst.TransferUnitRefId == lst.DefaultUnitRefId)
                    lst.LiveStock_TransferUnit = lst.LiveStock;
                else
                {
                    var unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == lst.DefaultUnitRefId && t.RefUnitId == lst.TransferUnitRefId);
                    if (unitConversion == null)
                        throw new UserFriendlyException(L("ConversionFactorNotExist", lst.Uom, lst.TransferUnitRefName));
                    lst.LiveStock_TransferUnit = unitConversion.Conversion * lst.LiveStock;
                }


                loopIndex++;
            }

            sortMenuItems = sortMenuItems.OrderBy(input.Sorting).ToList();

            if (input.LessThanMinimumStock)
            {
                sortMenuItems = sortMenuItems.Where(t => t.LiveStock <= t.MinimumStock).ToList();
            }

            if (input.LessThanReorderLevel)
            {
                sortMenuItems = sortMenuItems.Where(t => t.LiveStock <= t.ReOrderLevel && t.ReOrderLevel > 0).ToList();
            }

            var allListDtos = sortMenuItems.MapTo<List<MaterialLocationWiseStockViewDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<MaterialLocationWiseStockViewDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetMaterialLocationWiseStockInput input)
        {
            input.MaxResultCount = AppConsts.MaxPageSize;

            var allList = await GetView(input);
            var allListDtos = allList.Items.MapTo<List<MaterialLocationWiseStockViewDto>>();
            return _materiallocationwisestockExporter.ExportToFile(allListDtos);
        }

        public async Task<GetMaterialLocationWiseStockForEditOutput> GetMaterialLocationWiseStockForEdit(NullableIdInput input)
        {
            Debug.WriteLine("MaterialLocationWiseStockAppService GetMaterialLocationWiseStockForEdit Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));

            MaterialLocationWiseStockEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _materiallocationwisestockRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<MaterialLocationWiseStockEditDto>();
            }
            else
            {
                editDto = new MaterialLocationWiseStockEditDto();
            }

            return new GetMaterialLocationWiseStockForEditOutput
            {
                MaterialLocationWiseStock = editDto
            };
        }

        public async Task CreateOrUpdateMaterialLocationWiseStock(CreateOrUpdateMaterialLocationWiseStockInput input)
        {

            if (input.MaterialLocationWiseStock.Id.HasValue)
            {
                await UpdateMaterialLocationWiseStock(input);
            }
            //else
            //{
            //    await CreateMaterialLocationWiseStock(input);
            //}
        }

        public async Task DeleteMaterialLocationWiseStock(IdInput input)
        {
            await _materiallocationwisestockRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateMaterialLocationWiseStock(CreateOrUpdateMaterialLocationWiseStockInput input)
        {

            var item = await _materiallocationwisestockRepo.GetAsync(input.MaterialLocationWiseStock.Id.Value);

            var dto = input.MaterialLocationWiseStock;

            bool firsttime = false;
            if (item.CurrentInHand == 0 && dto.CurrentInHand > 0)
            {
                firsttime = true;
                //item.CurrentInHand = dto.CurrentInHand;
            }
            item.LocationRefId = dto.LocationRefId;
            item.MaterialRefId = dto.MaterialRefId;
            item.MinimumStock = dto.MinimumStock;
            item.MaximumStock = dto.MaximumStock;
            item.IsOrderPlaced = dto.IsOrderPlaced;
            item.ReOrderLevel = dto.ReOrderLevel;
            item.IsActiveInLocation = dto.IsActiveInLocation;
            item.ConvertAsZeroStockWhenClosingStockNegative = dto.ConvertAsZeroStockWhenClosingStockNegative;

            CheckErrors(await _materiallocationwisestockManager.CreateSync(item));

            if (firsttime == true)
            {
                CreateOrUpdateAdjustmentInput newAdjDto = new CreateOrUpdateAdjustmentInput();

                AdjustmentEditDto masDto = new AdjustmentEditDto();
                List<AdjustmentDetailViewDto> detDtos = new List<AdjustmentDetailViewDto>();

                if (input.MaterialLocationWiseStock.AccountDate == null)
                {
                    //var dayClose = await _locationAppService.GetDayClose(new IdInput { Id = input.MaterialLocationWiseStock.LocationRefId });
                    Location loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.MaterialLocationWiseStock.LocationRefId);
                    if (loc == null)
                    {
                        throw new UserFriendlyException(L("LocationErr"));
                    }
                    if (loc.HouseTransactionDate == null)
                    {
                        throw new UserFriendlyException(L("DayClose") + " " + L("Required"));
                    }
                    masDto.AdjustmentDate = loc.HouseTransactionDate.Value;
                }
                else
                {
                    masDto.AdjustmentDate = input.MaterialLocationWiseStock.AccountDate.Value;
                }

                masDto.AdjustmentRemarks = L("OnHandInitialSetup");
                masDto.ApprovedPersonId = (int)AbpSession.UserId;
                masDto.LocationRefId = dto.LocationRefId;
                masDto.EnteredPersonId = (int)AbpSession.UserId;
                masDto.TokenRefNumber = 666;

                var mat = await _materialRepo.FirstOrDefaultAsync(t => t.Id == dto.MaterialRefId);

                AdjustmentDetailViewDto det = new AdjustmentDetailViewDto();

                det.MaterialRefId = dto.MaterialRefId;
                det.AdjustmentQty = dto.CurrentInHand;
                det.AdjustmentApprovedRemarks = L("Initital");
                det.AdjustmentMode = L("Excess");
                det.UnitRefId = mat.DefaultUnitId;
                det.Sno = 1;

                detDtos.Add(det);

                newAdjDto.Adjustment = masDto;
                newAdjDto.AdjustmentDetail = detDtos;

                await _adjustmentAppService.CreateOrUpdateAdjustment(newAdjDto);

            }


        }

        public async Task<FileDto> GetStockExcel(IdInput input)
        {
            string locationName = _locationRepo.FirstOrDefault(t => t.Id == input.Id).Code.PadRight(4, '-');

            var matList = await (from msis in _materiallocationwisestockRepo.GetAll().Where(p => p.LocationRefId == input.Id)
                                 join l in _locationRepo.GetAll() on
                                  msis.LocationRefId equals l.Id
                                 join mat in _materialRepo.GetAll() on
                                 msis.MaterialRefId equals mat.Id
                                 join un in _unitrepo.GetAll()
                                 on mat.DefaultUnitId equals un.Id
                                 select new MaterialLocationWiseStockViewDto()
                                 {
                                     Id = msis.Id,
                                     LocationRefId = msis.LocationRefId,
                                     LocationRefName = l.Name,
                                     MaterialRefId = mat.Id,
                                     Barcode = mat.Barcode,
                                     MaterialRefName = mat.MaterialName,
                                     CurrentInHand = msis.CurrentInHand,
                                     MinimumStock = msis.MinimumStock,
                                     MaximumStock = msis.MaximumStock,
                                     ReOrderLevel = msis.ReOrderLevel,
                                     IsOrderPlaced = msis.IsOrderPlaced,
                                     IsActiveInLocation = msis.IsActiveInLocation,
                                     Uom = un.Name,
                                     MaterialTypeRefId = mat.MaterialTypeId,
                                     MaterialTypeRefName = mat.MaterialTypeId == 0 ? "RAW" : "SEMI",
                                     ConvertAsZeroStockWhenClosingStockNegative = msis.ConvertAsZeroStockWhenClosingStockNegative,
                                 }).ToListAsync();

            DateTime currentDate = DateTime.Now;

            FileDto fileExcel = _materiallocationwisestockExporter.StockToExcelFile(matList, locationName, currentDate);
            return fileExcel;
        }

        public async Task<PagedResultOutput<MaterialAllLocationWiseStockViewDto>> GetAllLocationView(GetMaterialLocationWiseStockInput input)
        {
            if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
                     && !input.LocationGroup.Locations.Any())
            {
                var locationRefIds = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                var locations = await _locationRepo.GetAllListAsync(t => locationRefIds.Contains(t.Id));
                input.Locations = locations.MapTo<List<LocationListDto>>();
            }
            else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var locationRefIds = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                var locations = await _locationRepo.GetAllListAsync(t => locationRefIds.Contains(t.Id));
                input.Locations = locations.MapTo<List<LocationListDto>>();
            }
            else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                var locationRefIds = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true
                });
                var locations = await _locationRepo.GetAllListAsync(t => locationRefIds.Contains(t.Id));
                input.Locations = locations.MapTo<List<LocationListDto>>();
            }
            else if (input.LocationGroup == null)
            {
                var locationRefIds = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = new List<SimpleLocationDto>(),
                    Group = false,
                    UserId = input.UserId
                });
                if (input.UserId > 0)
                {
                    var locations = await _locationRepo.GetAllListAsync(t => locationRefIds.Contains(t.Id));
                    input.Locations = locations.MapTo<List<LocationListDto>>();
                }
            }

            if (input.Locations == null || input.Locations.Count == 0)
            {
                var locations = await _locationRepo.GetAllListAsync();
                input.Locations = locations.MapTo<List<LocationListDto>>();
            }


            List<MaterialLocationWiseStockViewDto> allLocationStock = new List<MaterialLocationWiseStockViewDto>();
            GetMaterialLocationWiseStockInput locWiseInput = new GetMaterialLocationWiseStockInput();
            locWiseInput = input;

            if (input.MaterialRefIds.Count == 0)
            {
                var material = _materialRepo.GetAll();

                material = material.WhereIf(input.IsHighValueItemOnly, t => t.IsHighValueItem == true);

                material = material.WhereIf(input.MaterialTypeRefId != null, t => t.MaterialTypeId == input.MaterialTypeRefId);
                material = material.WhereIf(!input.Material.IsNullOrEmpty(), t => (t.MaterialName.Contains(input.Material) || t.Barcode.Contains(input.Material) || t.Barcodes.Select(a => a.Barcode).Contains(input.Material)));

                if (input.MaterialRefIds.Count>0)
                    material = material.WhereIf(input.MaterialRefIds != null && input.MaterialRefIds.Any(), t => input.MaterialRefIds.Contains(t.Id));
                var materialItems = await material.OrderBy(t=>t.Id)
                                  .PageBy(input)
                                  .ToListAsync();
                if (materialItems.Count>0)
                    input.MaterialRefIds = materialItems.Select(t=>t.Id).ToList();
            }

            if (input.MaterialRefIds.Count > 0)
            {
                foreach (var loc in input.Locations)
                {
                    locWiseInput.LocationRefId = loc.Id;
                    //input.MaxResultCount = AppConsts.MaxPageSize;
                    var locationStock = await GetView(input);
                    allLocationStock.AddRange(locationStock.Items.MapTo<List<MaterialLocationWiseStockViewDto>>());
                }
            }

            List<MaterialAllLocationWiseStockViewDto> consolidatedList = new List<MaterialAllLocationWiseStockViewDto>();
            allLocationStock = allLocationStock.OrderBy(t => t.MaterialRefId).ThenByDescending(t => t.LocationRefId).ToList();
            foreach (var locGroupMaterialStock in allLocationStock.GroupBy(t => t.MaterialRefId))
            {
                var matList = locGroupMaterialStock.ToList();
                var first = matList.FirstOrDefault();
                MaterialAllLocationWiseStockViewDto newDto = first.MapTo<MaterialAllLocationWiseStockViewDto>();
                newDto.LocationWiseStock = matList;

                newDto.CurrentInHand = matList.Sum(t => t.CurrentInHand);
                newDto.MinimumStock = matList.Sum(t => t.MinimumStock);
                newDto.MaximumStock = matList.Sum(t => t.MaximumStock);
                newDto.ReOrderLevel = matList.Sum(t => t.ReOrderLevel);
                newDto.Sales = matList.Sum(t => t.Sales);
                newDto.LiveStock = matList.Sum(t => t.LiveStock);
                newDto.LiveStock_TransactionUnit = matList.Sum(t => t.LiveStock_TransactionUnit);

                consolidatedList.Add(newDto);
            }


            var allItemCount = consolidatedList.Count;
            consolidatedList = consolidatedList.OrderBy(input.Sorting).ToList();
            return new PagedResultOutput<MaterialAllLocationWiseStockViewDto>(
                allItemCount,
                consolidatedList
                );
        }



    }
}
