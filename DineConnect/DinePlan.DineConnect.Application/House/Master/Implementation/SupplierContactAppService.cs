﻿
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;


namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class SupplierContactAppService : DineConnectAppServiceBase, ISupplierContactAppService
    {

        private readonly ISupplierContactListExcelExporter _suppliercontactExporter;
        private readonly ISupplierContactManager _suppliercontactManager;
        private readonly IRepository<SupplierContact> _suppliercontactRepo;
        private readonly IRepository<Supplier> _supplier;

        public SupplierContactAppService(ISupplierContactManager suppliercontactManager,
            IRepository<SupplierContact> supplierContactRepo,
            ISupplierContactListExcelExporter suppliercontactExporter
            , IRepository<Supplier> supplier)
        {
            _suppliercontactManager = suppliercontactManager;
            _suppliercontactRepo = supplierContactRepo;
            _suppliercontactExporter = suppliercontactExporter;
            _supplier = supplier;

        }

        public async Task<PagedResultOutput<SupplierContactListDto>> GetAll(GetSupplierContactInput input)
        {
            var allItems = _suppliercontactRepo.GetAll();
            if (!string.IsNullOrEmpty(input.Operation) && input.Operation.Equals("SEARCH"))
            {
                allItems = _suppliercontactRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Id.Equals(input.Filter)
               );
            }
            else
            {
                allItems = _suppliercontactRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Id.Equals(input.Filter) 
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<SupplierContactListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<SupplierContactListDto>(
                allItemCount,
                allListDtos
                );
        }
        public async Task<PagedResultOutput<SupplierContactViewDto>> GetView(GetSupplierContactInput input)
        {

            var allItems = (from sc in _suppliercontactRepo.GetAll()
                            join s in _supplier.GetAll() on
                            sc.SupplierRefId equals s.Id
                            select new SupplierContactViewDto()
                            {
                                Id = sc.Id,
                                SupplierRefId = sc.SupplierRefId,
                                SupplierRefName = s.SupplierName,
                                ContactPersonName = sc.ContactPersonName,
                                Designation=sc.Designation,
                                CreationTime = sc.CreationTime,
                                PhoneNumber = sc.PhoneNumber,
                                Email = sc.Email
                             
                            }).WhereIf(
                              !input.Filter.IsNullOrEmpty(),
                              p => p.SupplierRefId.ToString().Equals(input.Filter)
             );

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<SupplierContactViewDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<SupplierContactViewDto>(
                allItemCount,
                allListDtos
                );
        }


        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _suppliercontactRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<SupplierContactListDto>>();
            return _suppliercontactExporter.ExportToFile(allListDtos);
        }

        public async Task<GetSupplierContactForEditOutput> GetSupplierContactForEdit(NullableIdInput input)
        {
            SupplierContactEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _suppliercontactRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<SupplierContactEditDto>();
            }
            else
            {
                editDto = new SupplierContactEditDto();
            }

            return new GetSupplierContactForEditOutput
            {
                SupplierContact = editDto
            };
        }

        public async Task CreateOrUpdateSupplierContact(CreateOrUpdateSupplierContactInput input)
        {
            if (input.SupplierContact.Id.HasValue)
            {
                await UpdateSupplierContact(input);
            }
            else
            {
                await CreateSupplierContact(input);
            }
        }

        public async Task DeleteSupplierContact(IdInput input)
        {
            await _suppliercontactRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateSupplierContact(CreateOrUpdateSupplierContactInput input)
        {
            var item = await _suppliercontactRepo.GetAsync(input.SupplierContact.Id.Value);
            var dto = input.SupplierContact;

            //TODO: SERVICE SupplierContact Update Individually
            item.SupplierRefId = dto.SupplierRefId;
            item.ContactPersonName = dto.ContactPersonName;
            item.PhoneNumber = dto.PhoneNumber;
            item.Designation = dto.Designation;
            item.Email = dto.Email;

            CheckErrors(await _suppliercontactManager.CreateSync(item));
        }

        protected virtual async Task CreateSupplierContact(CreateOrUpdateSupplierContactInput input)
        {
            var dto = input.SupplierContact.MapTo<SupplierContact>();

            CheckErrors(await _suppliercontactManager.CreateSync(dto));
        }

        public async Task<ListResultOutput<SupplierContactListDto>> GetIds()
        {
            var lstSupplierContact = await _suppliercontactRepo.GetAll().ToListAsync();
            return new ListResultOutput<SupplierContactListDto>(lstSupplierContact.MapTo<List<SupplierContactListDto>>());
        }
    }
}
