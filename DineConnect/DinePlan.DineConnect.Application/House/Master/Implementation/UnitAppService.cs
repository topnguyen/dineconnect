﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;


namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class UnitAppService : DineConnectAppServiceBase, IUnitAppService
    {

        private readonly IUnitListExcelExporter _unitExporter;
        private readonly IUnitManager _unitManager;
        private readonly IRepository<Unit> _unitRepo;
        private readonly IRepository<UnitConversion> _unitConversionRepo;
        private readonly IRepository<Material> _materialRepo;

        public UnitAppService(IUnitManager unitManager
            ,IRepository<Unit> unitRepo
            ,IUnitListExcelExporter unitExporter
            ,IRepository<UnitConversion> unitConversion,
            IRepository<Material> materialRepo)
        {
            _unitManager = unitManager;
            _unitRepo = unitRepo;
            _unitExporter = unitExporter;
            _unitConversionRepo = unitConversion;
            _materialRepo = materialRepo;
        }

        public async Task<PagedResultOutput<UnitListDto>> GetAll(GetUnitInput input)
        {
            var allItems = _unitRepo.GetAll();
            if (!string.IsNullOrEmpty(input.Operation) && input.Operation.Equals("SEARCH"))
            {
                allItems = _unitRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Name.Equals(input.Filter)
               );
            }
            else
            {
                allItems = _unitRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Name.Contains(input.Filter)
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<UnitListDto>>();

            var allItemCount = await allItems.CountAsync();

            List<int> arrUnitRefIds = allListDtos.Select(t => t.Id).ToList();
            var rsUnitConversion = await _unitConversionRepo.GetAllListAsync(t => arrUnitRefIds.Contains(t.BaseUnitId) && t.MaterialRefId==null);
            foreach(var lst in allListDtos)
            {
                var count = rsUnitConversion.Count(t => t.BaseUnitId == lst.Id);
                lst.NoOfConversions = count;
            }

            return new PagedResultOutput<UnitListDto>(
                allItemCount,
                allListDtos
                );
        }
        public async Task<FileDto> GetAllToExcel(GetUnitInput input)
        {
            input.MaxResultCount = AppConsts.MaxPageSize;

            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<UnitListDto>>();
            return _unitExporter.ExportToFile(allListDtos);
        }

        public async Task<GetUnitForEditOutput> GetUnitForEdit(NullableIdInput input)
        {
            UnitEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _unitRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<UnitEditDto>();
            }
            else
            {
                editDto = new UnitEditDto();
            }

            return new GetUnitForEditOutput
            {
                Unit = editDto
            };
        }

        public async Task CreateOrUpdateUnit(CreateOrUpdateUnitInput input)
        {
            if (input.Unit.Id.HasValue)
            {
                await UpdateUnit(input);
            }
            else
            {
                await CreateUnit(input);
            }
        }

        public async Task DeleteUnit(IdInput input)
        {
            var rsMaterial = await _materialRepo.GetAll().Where(t => t.DefaultUnitId == input.Id || t.IssueUnitId == input.Id || t.TransferUnitId==input.Id || t.ReceivingUnitId==input.Id).ToListAsync();

            if (rsMaterial.Count>0)
            {
                throw new UserFriendlyException(L("UnitReferenceAvailableSoCouldNotDelete"));
            }

            var rsUnitConversion = await _unitConversionRepo.GetAll().Where(t => t.BaseUnitId == input.Id || t.RefUnitId == input.Id).ToListAsync();

            if (rsUnitConversion.Count >0)
            {
                throw new UserFriendlyException(L("UnitReferenceAvaiableInConversion"));
            }

            await _unitRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateUnit(CreateOrUpdateUnitInput input)
        {
            var item = await _unitRepo.GetAsync(input.Unit.Id.Value);
            var dto = input.Unit;

            item.Name = dto.Name;
			item.SyncLastModification = dto.SyncLastModification;
			item.SyncId = dto.SyncId;

			CheckErrors(await _unitManager.CreateSync(item));
        }

        protected virtual async Task CreateUnit(CreateOrUpdateUnitInput input)
        {
            var dto = input.Unit.MapTo<Unit>();

            CheckErrors(await _unitManager.CreateSync(dto));
        }

        public async Task<ListResultOutput<UnitListDto>> GetNames()
        {
            var lstUnit = await _unitRepo.GetAll().ToListAsync();
            return new ListResultOutput<UnitListDto>(lstUnit.MapTo<List<UnitListDto>>());
        }

        public  IdInput GetUnitIdByName(string unitname)
        {
            var lstUnitId = _unitRepo.GetAllList(a => a.Name.Equals(unitname)).ToList().FirstOrDefault();
            return new IdInput
            {
                Id = lstUnitId.Id
            };
        }


        public string GetUnitName(int UnitId)
        {
            var lstUnitId = (from un in _unitRepo.GetAll()
                             where un.Id == UnitId
                             select new { un.Name }).ToList();


            //var allItems = _unitRepo
            //  .GetAll()
            //  .Where(p=>p.Id == UnitId).SingleOrDefault(p=>p.Name,true);

            return lstUnitId.ToString();
            
        }
        public async Task CreateUnitsForTenant(int id)
        {
            var kg = new Unit { Name = "KG", TenantId = id };
            int kgValue = await _unitRepo.InsertAndGetIdAsync(kg);

            var gram = new Unit { Name = "G", TenantId = id };
            int gramValue = await _unitRepo.InsertAndGetIdAsync(gram);

            var kgtogram = new UnitConversion(kgValue, gramValue, 1000);
            await _unitConversionRepo.InsertAsync(kgtogram);

            var gramtoKg = new UnitConversion(gramValue, kgValue, Convert.ToDecimal(0.001));
            await _unitConversionRepo.InsertAsync(gramtoKg);

            var unit = new Unit { Name = "MG", TenantId = id };
            await _unitRepo.InsertAsync(unit);

            unit = new Unit { Name = "MT", TenantId = id };
            
            await _unitRepo.InsertAsync(unit);

            unit = new Unit { Name = "L", TenantId = id };
            await _unitRepo.InsertAsync(unit);

            unit = new Unit { Name = "ML", TenantId = id };
            await _unitRepo.InsertAsync(unit);

            unit = new Unit { Name = "M", TenantId = id };
            await _unitRepo.InsertAsync(unit);

            unit = new Unit { Name = "CM", TenantId = id };
            await _unitRepo.InsertAsync(unit);

            unit = new Unit { Name = "MM", TenantId = id };
            await _unitRepo.InsertAsync(unit);

            unit = new Unit { Name = "DOZ", TenantId = id };
            await _unitRepo.InsertAsync(unit);

            unit = new Unit { Name = "NO", TenantId = id };
            await _unitRepo.InsertAsync(unit);

            unit = new Unit { Name = "PC", TenantId = id };
            await _unitRepo.InsertAsync(unit);

        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetRefernceUnitName(IdInput input)
        {
            var linkedConversion = await _unitConversionRepo.GetAll().Where(t => t.MaterialRefId == null && (t.BaseUnitId == input.Id || t.RefUnitId == input.Id)).ToListAsync();

            List<int> ucIds = new List<int>();
            ucIds.AddRange( linkedConversion.Select(t => t.BaseUnitId).ToList());
            ucIds.AddRange(linkedConversion.Select(t => t.RefUnitId).ToList());
            ucIds.Add(input.Id);
            ucIds = ucIds.Distinct().ToList();

            var refunitlist = await (from un in _unitRepo.GetAll().Where(t=> ucIds.Contains(t.Id))
                               select new ComboboxItemDto
                               {
                                   Value = un.Id.ToString(),
                                   DisplayText = un.Name
                               }).ToListAsync();

            return new ListResultOutput<ComboboxItemDto>(refunitlist);
        }
 
        public async Task<ListResultOutput<UnitListDto>> GetMaterialUnitNames(int argMaterialCode)
        {
            var lstUnit1 = await _unitRepo.GetAll().ToListAsync();

            //var lstUnit = await (from u in _unitRepo.GetAll()
            //                     join mu in _materialUnitsLink.GetAll().Where(a => a.MaterialRefCode == argMaterialCode)
            //                     on u.Id equals mu.UnitRefId
            //                     select u).ToListAsync();

            return new ListResultOutput<UnitListDto>(lstUnit1.MapTo<List<UnitListDto>>());
        }
    }
}