﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Master;
using Abp.Authorization.Users;

namespace DinePlan.DineConnect.House.Master.Implementation
{
    public class StockMovementAppService : DineConnectAppServiceBase, IStockMovementAppService
    {
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<MaterialRecipeTypes> _materialrecipetypeRepo;
        private readonly IRepository<MaterialLedger> _materialLedgerRepo;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IRepository<InwardDirectCredit> _inwarddirectcreditRepo;
        private readonly IRepository<InwardDirectCreditDetail> _inwarddirectcreditDetailRepo;
        private readonly IRepository<Invoice> _invoiceRepo;
        private readonly IRepository<InvoiceDetail> _invoicedetailRepo;
        private readonly IRepository<Issue> _issueRepo;
        private readonly IRepository<IssueDetail> _issueDetailRepo;
        private readonly IRepository<Yield> _yieldRepo;
        private readonly IRepository<YieldInput> _yieldinputRepo;
        private readonly IRepository<YieldOutput> _yieldoutputRepo;
        private readonly IRepository<InterTransfer> _intertransferRepo;
        private readonly IRepository<InterTransferDetail> _intertransferDetailRepo;
        private readonly IRepository<InterTransferReceivedDetail> _intertransferReceivedDetailRepo;
        private readonly IRepository<Production> _productionRepo;
        private readonly IRepository<ProductionDetail> _productiondetailRepo;
        private readonly IRepository<Return> _returnRepo;
        private readonly IRepository<ReturnDetail> _returnDetailRepo;
        private readonly IRepository<Adjustment> _adjustmentRepo;
        private readonly IRepository<AdjustmentDetail> _adjustmentDetailRepo;
        private readonly IRepository<MaterialGroupCategory> _materialgroupcategoryRepo;
        private readonly IRepository<Supplier> _supplierRepo;
        private readonly IRepository<Unit> _unitRepo;
        private readonly IUnitConversionAppService _unitConversionAppService;
        private readonly IRepository<MaterialLocationWiseStock> _materiallocationwiseStockRepo;
        private readonly IRepository<PurchaseReturn> _purchaseReturnRepo;
        private readonly IRepository<PurchaseReturnDetail> _purchaseReturnDetailRepo;
        private readonly IRepository<SalesInvoice> _salesInvoiceRepo;
        private readonly IRepository<SalesInvoiceDetail> _salesInvoiceDetailRepo;
        private readonly IRepository<SalesDeliveryOrder> _salesDeliveryRepo;
        private readonly IRepository<SalesDeliveryOrderDetail> _salesDeliveryDetailRepo;
        private readonly IRepository<Customer> _customerRepo;

        public StockMovementAppService(IMaterialManager materialManager,
              IRepository<Material> materialRepo,
              IRepository<Location> locationRepo,
              IRepository<MaterialRecipeTypes> materialrecipetypeRepo,
              IRepository<MaterialLedger> materialLedgerRepo,
              IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository,
              IRepository<InwardDirectCredit> inwardDirectCreditRepo,
              IRepository<InwardDirectCreditDetail> inwarddirectcreditDetailRepo,
              IRepository<Invoice> invoiceRepo,
              IRepository<InvoiceDetail> invoicedetailRepo,
              IRepository<Issue> issueRepo,
              IRepository<IssueDetail> issueDetailRepo,
              IRepository<Yield> yieldRepo,
              IRepository<YieldInput> yieldInputRepo,
              IRepository<YieldOutput> yieldOutputRepo,
              IRepository<InterTransfer> interTransferRepo,
              IRepository<InterTransferDetail> intertransferDetailRepo,
              IRepository<InterTransferReceivedDetail> intertransferreceivedDetailRepo,
              IRepository<Production> productionRepo,
              IRepository<ProductionDetail> productiondetailRepo,
              IRepository<Return> returnRepo,
              IRepository<ReturnDetail> returnDetailRepo,
              IRepository<Adjustment> adjustmentRepo,
              IRepository<Supplier> supplierRepo,
              IRepository<MaterialGroupCategory> materialgroupcategoryRepo,
              IRepository<Unit> unitRepo,
              IRepository<AdjustmentDetail> adjustmentDetailRepo,
                 IUnitConversionAppService unitConversionAppService,
                 IRepository<MaterialLocationWiseStock> materiallocationwiseStockRepo,
              IRepository<PurchaseReturn> purchaseReturnRepo,
              IRepository<PurchaseReturnDetail> purchaseReturnDetailRepo,
              IRepository<SalesInvoice> salesInvoiceRepo,
              IRepository<SalesInvoiceDetail> salesInvoiceDetailRepo,
              IRepository<SalesDeliveryOrder> salesDeliveryRepo,
              IRepository<SalesDeliveryOrderDetail> salesDeliveryDetailRepo,
              IRepository<Customer> customerRepo
           )
        {
            _materialRepo = materialRepo;
            _locationRepo = locationRepo;
            _materialrecipetypeRepo = materialrecipetypeRepo;
            _materialLedgerRepo = materialLedgerRepo;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _inwarddirectcreditRepo = inwardDirectCreditRepo;
            _inwarddirectcreditDetailRepo = inwarddirectcreditDetailRepo;
            _invoiceRepo = invoiceRepo;
            _invoicedetailRepo = invoicedetailRepo;
            _issueRepo = issueRepo;
            _issueDetailRepo = issueDetailRepo;
            _yieldRepo = yieldRepo;
            _yieldinputRepo = yieldInputRepo;
            _yieldoutputRepo = yieldOutputRepo;
            _intertransferRepo = interTransferRepo;
            _intertransferDetailRepo = intertransferDetailRepo;
            _intertransferReceivedDetailRepo = intertransferreceivedDetailRepo;
            _productionRepo = productionRepo;
            _productiondetailRepo = productiondetailRepo;
            _returnRepo = returnRepo;
            _returnDetailRepo = returnDetailRepo;
            _adjustmentRepo = adjustmentRepo;
            _adjustmentDetailRepo = adjustmentDetailRepo;
            _materialgroupcategoryRepo = materialgroupcategoryRepo;
            _supplierRepo = supplierRepo;
            _unitRepo = unitRepo;
            _unitConversionAppService = unitConversionAppService;
            _materiallocationwiseStockRepo = materiallocationwiseStockRepo;
            _purchaseReturnRepo = purchaseReturnRepo;
            _purchaseReturnDetailRepo = purchaseReturnDetailRepo;
            _salesInvoiceRepo = salesInvoiceRepo;
            _salesInvoiceDetailRepo = salesInvoiceDetailRepo;
            _salesDeliveryRepo = salesDeliveryRepo;
            _salesDeliveryDetailRepo = salesDeliveryDetailRepo;
            _customerRepo = customerRepo;
        }


        //public async Task<List<StockMovementErrorDto>> StockMovementErrorFinding(StockMovementInputDto input)
        //{
        //    List<StockMovementErrorDto> errList = new List<StockMovementErrorDto>();

        //    IQueryable<Location> iqueryLocation;

        //    if (input.AllLocationVerification == true)
        //    {
        //        iqueryLocation = _locationRepo.GetAll();
        //    }
        //    else
        //    {
        //        iqueryLocation = from loc in _locationRepo.GetAll()
        //                         join ou in _userOrganizationUnitRepository.GetAll().Where(a => a.UserId == input.UserId)
        //                             on loc.Id equals ou.OrganizationUnitId
        //                         select loc;
        //    }

        //    var sortMenuItems = await iqueryLocation.ToListAsync();

        //    var allLocations = sortMenuItems.MapTo<List<LocationListDto>>();

        //    DateTime fromDate, toDate;

        //    if (input.MovementDateFrom.HasValue)
        //    {
        //        fromDate = input.MovementDateFrom.Value;
        //    }
        //    else
        //    {
        //        fromDate = DateTime.Today.AddDays(-1);
        //    }

        //    if (input.MovementDateTo.HasValue)
        //    {
        //        toDate = input.MovementDateTo.Value;
        //    }
        //    else
        //    {
        //        toDate = DateTime.Today;
        //    }

        //    var materialList = await _materialRepo.GetAll().ToListAsync();

        //    foreach (var loc in allLocations)
        //    {
        //        DateTime verifyDate = fromDate;

        //        do
        //        {
        //            foreach (var material in materialList)
        //            {
        //                var ledger = _materialLedgerRepo.GetAll().Where(a => a.MaterialRefId == material.Id && a.LocationRefId == loc.Id && a.LedgerDate == verifyDate.Date).ToList();
        //                if (ledger.Count == 0)
        //                {
        //                    continue;
        //                }
        //                if (ledger.Count > 1)
        //                {
        //                    StockMovementErrorDto err = new StockMovementErrorDto();
        //                    err.LocationRefId = loc.Id;
        //                    err.MaterialRefId = material.Id;
        //                    err.ErrorDate = verifyDate;
        //                    err.ErrorDesciption = L("LedgerParticularMaterialParticularDateMoreThanOneErr");
        //                    errList.Add(err);
        //                    continue;
        //                }

        //                var ledgerDetail = ledger[0];

        //                //  Verify Open Balance 
        //                decimal newOpenBalance;
        //                var latestLedgerDate = (from led in _materialLedgerRepo.GetAll()
        //                                        orderby led.LedgerDate descending
        //                                        where led.MaterialRefId == material.Id && led.LocationRefId == loc.Id
        //                                        && DbFunctions.TruncateTime(led.LedgerDate) <= DbFunctions.TruncateTime(verifyDate.Date)
        //                                        select led.LedgerDate).ToList();

        //                if (latestLedgerDate.Count() == 0)
        //                {
        //                    newOpenBalance = 0;
        //                }
        //                else
        //                {
        //                    var maxLedgerDate = latestLedgerDate.Max();
        //                    var lastDaysRecord = _materialLedgerRepo.FirstOrDefault(l => (l.LedgerDate.Value.Year == maxLedgerDate.Value.Year && l.LedgerDate.Value.Month == maxLedgerDate.Value.Month && l.LedgerDate.Value.Day == maxLedgerDate.Value.Day)
        //                    && l.MaterialRefId == material.Id
        //                    && l.LocationRefId == loc.Id);
        //                    newOpenBalance = lastDaysRecord.ClBalance;
        //                }

        //                if (ledgerDetail.OpenBalance != newOpenBalance)
        //                {
        //                    StockMovementErrorDto err = new StockMovementErrorDto();
        //                    err.LocationRefId = loc.Id;
        //                    err.MaterialRefId = material.Id;
        //                    err.ErrorDate = verifyDate;
        //                    err.ErrorDesciption = L("OpenBalanceErr");
        //                    errList.Add(err);
        //                }

        //                //  Verify Received
        //                var newreceivedfromrecipt =  (from inwardmaster in _inwarddirectcreditRepo.GetAll().Where(t => t.LocationRefId == loc.Id && t.DcDate.Date == verifyDate.Date)
        //                                    join inwarddetail in _inwarddirectcreditDetailRepo.GetAll()
        //                                    on inwardmaster.Id equals inwarddetail.DcRefId
        //                                    where inwardmaster.Id == inwarddetail.DcRefId && inwarddetail.MaterialRefId == material.Id
        //                                    group inwarddetail by new { inwarddetail.MaterialRefId } into g
        //                                    select g.Sum(s => s.DcReceivedQty)).FirstOrDefault();

        //                var newreceivedfromdirectinvoice = (from invoicemaster in _invoiceRepo.GetAll().Where(t => t.LocationRefId == loc.Id && t.AccountDate.Date == verifyDate.Date && t.IsDirectInvoice==true)
        //                                             join invoicedetail in _invoicedetailRepo.GetAll()
        //                                             on invoicemaster.Id equals invoicedetail.InvoiceRefId
        //                                             where invoicemaster.Id == invoicedetail.InvoiceRefId && invoicedetail.MaterialRefId == material.Id
        //                                             group invoicedetail by new { invoicedetail.MaterialRefId } into g
        //                                             select g.Sum(s => s.TotalQty)).FirstOrDefault();

        //                var newyieldrecived = (from yieldmaster in _yieldRepo.GetAll().Where(t => t.LocationRefId == loc.Id && 
        //                                       DbFunctions.TruncateTime( t.CompletedTime) == verifyDate.Date)
        //                                      join yieldoutput in _yieldoutputRepo.GetAll()
        //                                      on yieldmaster.Id equals yieldoutput.YieldRefId
        //                                      where yieldmaster.Id == yieldoutput.YieldRefId && yieldoutput.OutputMaterialRefId== material.Id
        //                                      group yieldoutput by new { yieldoutput.OutputMaterialRefId} into g
        //                                      select g.Sum(s => s.OutputQty)).FirstOrDefault();

        //                var newproductionreceived = (from productionmaster in _productionRepo.GetAll().Where(t => t.LocationRefId == loc.Id &&
        //               DbFunctions.TruncateTime(t.ProductionTime) == verifyDate.Date)
        //                                       join productiondetail in _productiondetailRepo.GetAll()
        //                                       on productionmaster.Id equals productiondetail.ProductionRefId
        //                                       where productionmaster.Id == productiondetail.ProductionRefId && productiondetail.MaterialRefId == material.Id
        //                                       group productiondetail by new { productiondetail.MaterialRefId } into g
        //                                       select g.Sum(s => s.ProductionQty)).FirstOrDefault();

        //                if (ledgerDetail.Received!= (newreceivedfromrecipt+ newreceivedfromdirectinvoice+ newyieldrecived + newproductionreceived))
        //                {
        //                    StockMovementErrorDto err = new StockMovementErrorDto();
        //                    err.LocationRefId = loc.Id;
        //                    err.MaterialRefId = material.Id;
        //                    err.ErrorDate = verifyDate;
        //                    err.ErrorDesciption = L("ReceivedErr");
        //                    errList.Add(err);
        //                }

        //                //  Verify Issued
        //                var newissued = (from issuemaster in _issueRepo.GetAll().Where(t => t.LocationRefId == loc.Id && t.IssueTime.Date == verifyDate.Date)
        //                                             join issuedetail in _issueDetailRepo.GetAll()
        //                                             on issuemaster.Id equals issuedetail.IssueRefId
        //                                             where issuemaster.Id == issuedetail.IssueRefId && issuedetail.MaterialRefId == material.Id
        //                                             group issuedetail by new { issuedetail.MaterialRefId } into g
        //                                             select g.Sum(s => s.IssueQty)).FirstOrDefault();

        //                var newyieldissued = (from yieldmaster in _yieldRepo.GetAll().Where(t => t.LocationRefId == loc.Id && t.IssueTime.Date == verifyDate.Date)
        //                                 join yieldinput in _yieldinputRepo.GetAll()
        //                                 on yieldmaster.Id equals yieldinput.YieldRefId
        //                                 where yieldmaster.Id == yieldinput.YieldRefId && yieldinput.InputMaterialRefId == material.Id
        //                                 group yieldinput by new { yieldinput.InputMaterialRefId } into g
        //                                 select g.Sum(s => s.InputQty)).FirstOrDefault();

        //                //@@Pending Sales has not taken for verification

        //                if (ledgerDetail.Issued != (newissued + newyieldissued))
        //                {
        //                    StockMovementErrorDto err = new StockMovementErrorDto();
        //                    err.LocationRefId = loc.Id;
        //                    err.MaterialRefId = material.Id;
        //                    err.ErrorDate = verifyDate;
        //                    err.ErrorDesciption = L("IssuedErr");
        //                    errList.Add(err);
        //                }

        //                //Verify TransferIn
        //                var newtransferin = (from intermaster in _intertransferRepo.GetAll().Where(t => t.LocationRefId == loc.Id &&  DbFunctions.TruncateTime(t.ReceivedTime) == verifyDate.Date)
        //                                 join interreceived in _intertransferReceivedDetailRepo.GetAll()
        //                                 on intermaster.Id equals interreceived.InterTransferRefId
        //                                 where intermaster.Id == interreceived.InterTransferRefId && interreceived.MaterialRefId == material.Id
        //                                 group interreceived by new { interreceived.MaterialRefId } into g
        //                                 select g.Sum(s => s.ReceivedQty)).FirstOrDefault();

        //                if (ledgerDetail.TransferIn != newtransferin)
        //                {
        //                    StockMovementErrorDto err = new StockMovementErrorDto();
        //                    err.LocationRefId = loc.Id;
        //                    err.MaterialRefId = material.Id;
        //                    err.ErrorDate = verifyDate;
        //                    err.ErrorDesciption = L("TransferInErr");
        //                    errList.Add(err);
        //                }


        //                //Verify TransferOut
        //                var newtransferout = (from intermaster in _intertransferRepo.GetAll().Where(t => t.RequestLocationRefId == loc.Id && DbFunctions.TruncateTime(t.CompletedTime) == verifyDate.Date)
        //                                     join interdetail in _intertransferDetailRepo.GetAll()
        //                                     on intermaster.Id equals interdetail.InterTransferRefId
        //                                     where intermaster.Id == interdetail.InterTransferRefId && interdetail.MaterialRefId == material.Id
        //                                     group interdetail by new { interdetail.MaterialRefId } into g
        //                                     select g.Sum(s => s.IssueQty)).FirstOrDefault();

        //                if (ledgerDetail.TransferOut != newtransferout)
        //                {
        //                    StockMovementErrorDto err = new StockMovementErrorDto();
        //                    err.LocationRefId = loc.Id;
        //                    err.MaterialRefId = material.Id;
        //                    err.ErrorDate = verifyDate;
        //                    err.ErrorDesciption = L("TransferOutErr");
        //                    errList.Add(err);
        //                }

        //                //Verify Return
        //                var newreturn = (from returnmaster in _returnRepo.GetAll().Where(t => t.LocationRefId== loc.Id && t.ReturnDate == verifyDate.Date)
        //                                      join returndetail in _returnDetailRepo.GetAll()
        //                                      on returnmaster.Id equals returndetail.ReturnRefId
        //                                      where returnmaster.Id == returndetail.ReturnRefId && returndetail.MaterialRefId == material.Id
        //                                      group returndetail by new { returndetail.MaterialRefId } into g
        //                                      select g.Sum(s => s.ReturnQty)).FirstOrDefault();

        //                if (ledgerDetail.Return != newreturn)
        //                {
        //                    StockMovementErrorDto err = new StockMovementErrorDto();
        //                    err.LocationRefId = loc.Id;
        //                    err.MaterialRefId = material.Id;
        //                    err.ErrorDate = verifyDate;
        //                    err.ErrorDesciption = L("ReturnErr");
        //                    errList.Add(err);
        //                }

        //                //Verify Damaged
        //                var newdamaged = (from adjmaster in _adjustmentRepo.GetAll().Where(t => t.LocationRefId == loc.Id && t.AdjustmentDate== verifyDate.Date)
        //                                 join adjdetail in _adjustmentDetailRepo.GetAll()
        //                                 on adjmaster.Id equals adjdetail.AdjustmentRefIf
        //                                 where adjmaster.Id == adjdetail.AdjustmentRefIf && adjdetail.MaterialRefId == material.Id && adjdetail.AdjustmentMode.Equals(L("Damaged"))
        //                                 group adjdetail by new { adjdetail.MaterialRefId } into g
        //                                 select g.Sum(s => s.AdjustmentQty)).FirstOrDefault();

        //                if (ledgerDetail.Damaged != newdamaged)
        //                {
        //                    StockMovementErrorDto err = new StockMovementErrorDto();
        //                    err.LocationRefId = loc.Id;
        //                    err.MaterialRefId = material.Id;
        //                    err.ErrorDate = verifyDate;
        //                    err.ErrorDesciption = L("DamagedErr");
        //                    errList.Add(err);
        //                }

        //                //Verify Shortage
        //                var newshortage = (from adjmaster in _adjustmentRepo.GetAll().Where(t => t.LocationRefId == loc.Id && t.AdjustmentDate == verifyDate.Date)
        //                                  join adjdetail in _adjustmentDetailRepo.GetAll()
        //                                  on adjmaster.Id equals adjdetail.AdjustmentRefIf
        //                                  where adjmaster.Id == adjdetail.AdjustmentRefIf && adjdetail.MaterialRefId == material.Id && adjdetail.AdjustmentMode.Equals(L("Shortage"))
        //                                  group adjdetail by new { adjdetail.MaterialRefId } into g
        //                                  select g.Sum(s => s.AdjustmentQty)).FirstOrDefault();

        //                if (ledgerDetail.Damaged != newdamaged)
        //                {
        //                    StockMovementErrorDto err = new StockMovementErrorDto();
        //                    err.LocationRefId = loc.Id;
        //                    err.MaterialRefId = material.Id;
        //                    err.ErrorDate = verifyDate;
        //                    err.ErrorDesciption = L("ShortageErr");
        //                    errList.Add(err);
        //                }

        //                //Verify ExcessReceived
        //                var newexcess = (from adjmaster in _adjustmentRepo.GetAll().Where(t => t.LocationRefId == loc.Id && t.AdjustmentDate == verifyDate.Date)
        //                                  join adjdetail in _adjustmentDetailRepo.GetAll()
        //                                  on adjmaster.Id equals adjdetail.AdjustmentRefIf
        //                                  where adjmaster.Id == adjdetail.AdjustmentRefIf && adjdetail.MaterialRefId == material.Id && adjdetail.AdjustmentMode.Equals(L("Excess"))
        //                                  group adjdetail by new { adjdetail.MaterialRefId } into g
        //                                  select g.Sum(s => s.AdjustmentQty)).FirstOrDefault();

        //                if (ledgerDetail.ExcessReceived != newexcess)
        //                {
        //                    StockMovementErrorDto err = new StockMovementErrorDto();
        //                    err.LocationRefId = loc.Id;
        //                    err.MaterialRefId = material.Id;
        //                    err.ErrorDate = verifyDate;
        //                    err.ErrorDesciption = L("ExcessReceivedErr");
        //                    errList.Add(err);
        //                }
        //            }

        //            verifyDate = verifyDate.AddDays(1);
        //        } while (verifyDate <= toDate);
        //    }
        //    return errList;
        //}

        public async Task<List<MaterialTrackConsolidatedDto>> GetMaterialTrack(GetMaterialTrackDto input)
        {
            IQueryable<Material> materialList;
            int[] materialRefIdsList;

            if (input.Materials.Count > 0)
            {
                string[] arrstringreciperefs = input.Materials.Select(t => t.Value).ToArray();
                materialRefIdsList = arrstringreciperefs.Select(int.Parse).ToArray();
                materialList = _materialRepo.GetAll().Where(t => materialRefIdsList.Contains(t.Id));
            }
            else
            {
                if (input.MaterialRefId.HasValue)
                    materialList = _materialRepo.GetAll();
                else if (input.MaterialGroupCategoryRefId.HasValue)
                    materialList = _materialRepo.GetAll().Where(t => t.MaterialGroupCategoryRefId == input.MaterialGroupCategoryRefId);
                else if (input.MaterialGroupRefId.HasValue)
                {
                    int[] materialgroupcategoryids = _materialgroupcategoryRepo.GetAll().Where(t => t.MaterialGroupRefId == input.MaterialGroupRefId).Select(t => t.Id).ToArray();
                    materialList = _materialRepo.GetAll().Where(t => materialgroupcategoryids.Contains(t.MaterialGroupCategoryRefId));
                }
                else
                    materialList = _materialRepo.GetAll();

                materialRefIdsList = materialList.Select(t => t.Id).ToArray();
            }
            input.StartDate = input.StartDate.Date;


            //      Over All Received
            string stringTrackStatus = L("Receipt");
            var lstDcReceived = await (from recmas in _inwarddirectcreditRepo.GetAll()
                                       .Where(t => t.LocationRefId == input.LocationRefId
                      && DbFunctions.TruncateTime(t.DcDate) >= input.StartDate.Date && DbFunctions.TruncateTime(t.DcDate) <= input.EndDate.Date)
                                       join recdet in _inwarddirectcreditDetailRepo.GetAll().Where(t => materialRefIdsList.Contains(t.MaterialRefId))
                                       on recmas.Id equals recdet.DcRefId
                                       join sup in _supplierRepo.GetAll()
                                       on recmas.SupplierRefId equals sup.Id
                                       select new MaterialTrackDto
                                       {
                                           LocationRefId = input.LocationRefId,
                                           MaterialRefId = recdet.MaterialRefId,
                                           MaterialName = "",
                                           Issued = 0,
                                           Recived = recdet.DcReceivedQty,
                                           LedgerDate = recmas.AccountDate==null? recmas.DcDate : recmas.AccountDate.Value,
                                           ModifyTime = recdet.CreationTime,
                                           TrackStatus = stringTrackStatus,
                                           TrackDesc = sup.SupplierName + " / " + recmas.DcNumberGivenBySupplier,
                                           UnitRefId = recdet.UnitRefId,
                                           SortOrder = 10,
                                           ConversionWithDefaultUnit = recdet.ConversionWithDefaultUnit,
                                       }).ToListAsync();

            stringTrackStatus = L("DirectInvoice");
            string stringPrice = L("Price");
            var lstDirectinvoiceReceived = await (from invoicemaster in _invoiceRepo.GetAll().
                                                  Where(t => t.LocationRefId == input.LocationRefId
                                               && DbFunctions.TruncateTime(t.AccountDate) >= input.StartDate.Date &&
                                                DbFunctions.TruncateTime(t.AccountDate) <= input.EndDate.Date
                                               && t.IsDirectInvoice == true)
                                                  join invoicedetail in _invoicedetailRepo.GetAll()
                                                  .Where(t => materialRefIdsList.Contains(t.MaterialRefId))
                                                  on invoicemaster.Id equals invoicedetail.InvoiceRefId
                                                  join sup in _supplierRepo.GetAll()
                                                  on invoicemaster.SupplierRefId equals sup.Id
                                                  select new MaterialTrackDto
                                                  {
                                                      LocationRefId = input.LocationRefId,
                                                      MaterialRefId = invoicedetail.MaterialRefId,
                                                      MaterialName = "",
                                                      Issued = 0,
                                                      Recived = invoicedetail.TotalQty,
                                                      LedgerDate = invoicemaster.AccountDate,
                                                      ModifyTime = invoicemaster.CreationTime,
                                                      TrackStatus = stringTrackStatus,
                                                      TrackDesc = sup.SupplierName + " / " + invoicemaster.InvoiceNumber,
                                                      Remarks = stringPrice + " " + invoicedetail.Price,
                                                      UnitRefId = invoicedetail.UnitRefId,
                                                      SortOrder = 11,
                                                      ConversionWithDefaultUnit = invoicedetail.ConversionWithDefaultUnit
                                                  }).ToListAsync();

            stringTrackStatus = L("PurchaseReturn");
            var lstPurchaseReturn = await (from retmaster in _purchaseReturnRepo.GetAll().
                                                  Where(t => t.LocationRefId == input.LocationRefId
                                               && DbFunctions.TruncateTime(t.ReturnDate) >= input.StartDate.Date &&
                                                DbFunctions.TruncateTime(t.ReturnDate) <= input.EndDate.Date)
                                           join retdetail in _purchaseReturnDetailRepo.GetAll()
                                           .Where(t => materialRefIdsList.Contains(t.MaterialRefId))
                                           on retmaster.Id equals retdetail.PurchaseReturnRefId
                                           join sup in _supplierRepo.GetAll()
                                           on retmaster.SupplierRefId equals sup.Id
                                           select new MaterialTrackDto
                                           {
                                               LocationRefId = input.LocationRefId,
                                               MaterialRefId = retdetail.MaterialRefId,
                                               MaterialName = "",
                                               Issued = retdetail.Quantity,
                                               Recived = 0,
                                               LedgerDate = retmaster.AccountDate == null ? retmaster.ReturnDate : retmaster.AccountDate.Value,
                                               ModifyTime = retmaster.CreationTime,
                                               TrackStatus = stringTrackStatus,
                                               TrackDesc = sup.SupplierName + " / " + retmaster.ReferenceNumber,
                                               Remarks = stringPrice + " " + retdetail.Price,
                                               UnitRefId = retdetail.UnitRefId,
                                               ConversionWithDefaultUnit = retdetail.ConversionWithDefaultUnit,
                                               SortOrder = 30
                                           }).ToListAsync();


            stringTrackStatus = L("Delivery");
            var lstDeliveryIssued = await (from mas in _salesDeliveryRepo.GetAll()
                              .Where(t => t.LocationRefId == input.LocationRefId
                      && DbFunctions.TruncateTime(t.DeliveryDate) >= input.StartDate.Date && DbFunctions.TruncateTime(t.DeliveryDate) <= input.EndDate.Date)
                                           join det in _inwarddirectcreditDetailRepo.GetAll()
                                           .Where(t => materialRefIdsList.Contains(t.MaterialRefId))
                                           on mas.Id equals det.DcRefId
                                           join cus in _customerRepo.GetAll()
                                           on mas.CustomerRefId equals cus.Id
                                           select new MaterialTrackDto
                                           {
                                               LocationRefId = input.LocationRefId,
                                               MaterialRefId = det.MaterialRefId,
                                               MaterialName = "",
                                               Issued = det.DcReceivedQty,
                                               Recived = 0,
                                               LedgerDate = mas.AccountDate == null ? mas.DeliveryDate : mas.AccountDate.Value,
                                               ModifyTime = det.CreationTime,
                                               TrackStatus = stringTrackStatus,
                                               TrackDesc = cus.CustomerName + " / " + mas.SoReferenceCode,
                                               UnitRefId = det.UnitRefId,
                                               ConversionWithDefaultUnit = det.ConversionWithDefaultUnit,
                                               SortOrder = 21
                                           }).ToListAsync();

            stringTrackStatus = L("SalesInvoice");
            var lstSalesInvoiceIssued = await (from invoicemaster in _salesInvoiceRepo.GetAll().
                                                  Where(t => t.LocationRefId == input.LocationRefId
                                               && DbFunctions.TruncateTime(t.AccountDate) >= input.StartDate.Date &&
                                                DbFunctions.TruncateTime(t.AccountDate) <= input.EndDate.Date
                                               && t.IsDirectSalesInvoice == true)
                                               join invoicedetail in _salesInvoiceDetailRepo.GetAll()
                                               .Where(t => materialRefIdsList.Contains(t.MaterialRefId))
                                               on invoicemaster.Id equals invoicedetail.SalesInvoiceRefId
                                               join cus in _customerRepo.GetAll()
                                               on invoicemaster.CustomerRefId equals cus.Id
                                               select new MaterialTrackDto
                                               {
                                                   LocationRefId = input.LocationRefId,
                                                   MaterialRefId = invoicedetail.MaterialRefId,
                                                   MaterialName = "",
                                                   Issued = invoicedetail.TotalQty,
                                                   Recived = 0,
                                                   LedgerDate = invoicemaster.AccountDate,
                                                   ModifyTime = invoicemaster.CreationTime,
                                                   TrackStatus = stringTrackStatus,
                                                   TrackDesc = cus.CustomerName + " / " + invoicemaster.InvoiceNumber,
                                                   Remarks = stringPrice + " " + invoicedetail.Price,
                                                   UnitRefId = invoicedetail.UnitRefId,
                                                   ConversionWithDefaultUnit = invoicedetail.ConversionWithDefaultUnit,
                                                   SortOrder = 23
                                               }).ToListAsync();

            stringTrackStatus = L("Yield") + " " + L("Output");

            //var yieldMaster =  _yieldRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId &&
            //                       DbFunctions.TruncateTime(t.CompletedTime) >= input.StartDate.Date &&
            //                       DbFunctions.TruncateTime(t.CompletedTime) >= input.EndDate.Date);
            //var arrYieldIds = yieldMaster.Select(t => t.Id).ToList();

            //var yieldDetail = _yieldoutputRepo.GetAll().Where(t => materialRefIdsList.Contains(t.OutputMaterialRefId) && arrYieldIds.Contains(t.YieldRefId));
            //                       //on yieldmaster.Id equals yieldoutput.YieldRefId

            var lstYieldReceived = await (from yieldmaster in _yieldRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId &&
                                   DbFunctions.TruncateTime(t.CompletedTime) >= input.StartDate.Date &&
                                   DbFunctions.TruncateTime(t.CompletedTime) <= input.EndDate.Date)
                                          join yieldoutput in _yieldoutputRepo.GetAll().Where(t => materialRefIdsList.Contains(t.OutputMaterialRefId))
                                          on yieldmaster.Id equals yieldoutput.YieldRefId
                                          select new MaterialTrackDto
                                          {
                                              LocationRefId = input.LocationRefId,
                                              MaterialRefId = yieldoutput.OutputMaterialRefId,
                                              MaterialName = "",
                                              Issued = 0,
                                              Recived = yieldoutput.OutputQty,
                                              LedgerDate = yieldmaster.YieldOutputAccountDate == null ? yieldmaster.CompletedTime.Value : yieldmaster.YieldOutputAccountDate.Value,
                                              //LedgerDate = (DateTime)yieldmaster.CompletedTime,
                                              ModifyTime = (DateTime)yieldmaster.CompletedTime,
                                              TrackStatus = stringTrackStatus,
                                              TrackDesc = yieldmaster.RequestSlipNumber.ToString(),
                                              UnitRefId = yieldoutput.UnitRefId,
                                              ConversionWithDefaultUnit = yieldoutput.ConversionWithDefaultUnit,
                                              SortOrder = 11
                                          }).ToListAsync();

            stringTrackStatus = L("Production");
            var lstProductionReceived = await (from productionmaster in _productionRepo.GetAll().
                                         Where(t => t.LocationRefId == input.LocationRefId
                                         && DbFunctions.TruncateTime(t.ProductionTime) >= input.StartDate.Date
                                         && DbFunctions.TruncateTime(t.ProductionTime) <= input.EndDate.Date)
                                               join productiondetail in _productiondetailRepo.GetAll().Where(t => materialRefIdsList.Contains(t.MaterialRefId))
                                               on productionmaster.Id equals productiondetail.ProductionRefId
                                               select new MaterialTrackDto
                                               {
                                                   LocationRefId = input.LocationRefId,
                                                   MaterialRefId = productiondetail.MaterialRefId,
                                                   MaterialName = "",
                                                   Issued = 0,
                                                   Recived = productiondetail.ProductionQty,
                                                   LedgerDate = productionmaster.AccountDate == null ? productionmaster.ProductionTime : productionmaster.AccountDate.Value,
                                                  // LedgerDate = (DateTime)productionmaster.ProductionTime,
                                                   ModifyTime = (DateTime)productionmaster.ProductionTime,
                                                   TrackStatus = stringTrackStatus,
                                                   TrackDesc = productionmaster.ProductionSlipNumber,
                                                   UnitRefId = productiondetail.UnitRefId,
                                                   ConversionWithDefaultUnit = productiondetail.ConversionWithDefaultUnit,
                                                   SortOrder = 12
                                               }).ToListAsync();

            stringTrackStatus = L("TransferIn");
            var lstTransferInReceived = await (from intermaster in _intertransferRepo.GetAll().
                                               Where(t => t.LocationRefId == input.LocationRefId
                                && DbFunctions.TruncateTime(t.ReceivedTime) >= input.StartDate.Date
                                && DbFunctions.TruncateTime(t.ReceivedTime) <= input.EndDate.Date)
                                               join interreceived in _intertransferReceivedDetailRepo.GetAll().
                                                  Where(t => materialRefIdsList.Contains(t.MaterialRefId))
                                               on intermaster.Id equals interreceived.InterTransferRefId
                                               select new MaterialTrackDto
                                               {
                                                   LocationRefId = input.LocationRefId,
                                                   MaterialRefId = interreceived.MaterialRefId,
                                                   MaterialName = "",
                                                   Issued = 0,
                                                   Recived = interreceived.ReceivedQty,
                                                   LedgerDate = intermaster.InterTransferReceivedAccountDate == null ? intermaster.ReceivedTime.Value : intermaster.InterTransferReceivedAccountDate.Value,
                                                //   LedgerDate = (DateTime)intermaster.ReceivedTime,
                                                   ModifyTime = (DateTime)intermaster.ReceivedTime,
                                                   TrackStatus = stringTrackStatus,
                                                   TrackDesc = intermaster.Id.ToString(),
                                                   UnitRefId = interreceived.UnitRefId,
                                                   ConversionWithDefaultUnit = interreceived.ConversionWithDefaultUnit,
                                                   SortOrder = 14
                                               }).ToListAsync();

            stringTrackStatus = L("Adjustment");
            string[] strAdjarray = { L("Shortage"), L("Damaged"), L("Wastage") };

            var lstAdjustmentIssued = await (from adjMas in _adjustmentRepo.GetAll().
                                             Where(t => t.LocationRefId == input.LocationRefId
                                && DbFunctions.TruncateTime(t.AdjustmentDate) >= input.StartDate.Date
                                && DbFunctions.TruncateTime(t.AdjustmentDate) <= input.EndDate.Date)
                                             join adjDet in _adjustmentDetailRepo.GetAll().
                                               Where(t => materialRefIdsList.Contains(t.MaterialRefId)
                                             && strAdjarray.Contains(t.AdjustmentMode))
                                             on adjMas.Id equals adjDet.AdjustmentRefIf
                                             select new MaterialTrackDto
                                             {
                                                 LocationRefId = input.LocationRefId,
                                                 MaterialRefId = adjDet.MaterialRefId,
                                                 MaterialName = "",
                                                 Issued = adjDet.AdjustmentQty,
                                                 Recived = 0,
                                                 LedgerDate = adjMas.AccountDate == null ? adjMas.AdjustmentDate : adjMas.AccountDate.Value,
                                                // LedgerDate = (DateTime)adjMas.AdjustmentDate,
                                                 ModifyTime = adjMas.CreationTime,
                                                 TrackStatus = stringTrackStatus + "-" + adjDet.AdjustmentMode,
                                                 TrackDesc = adjMas.Id.ToString() + " " + adjMas.AdjustmentRemarks + " " + adjDet.AdjustmentApprovedRemarks,
                                                 UnitRefId = adjDet.UnitRefId,
                                                 ConversionWithDefaultUnit = adjDet.ConversionWithDefaultUnit,
                                                 SortOrder = 25
                                             }).ToListAsync();


            //      Over All Issued
            stringTrackStatus = L("Yield") + " " + L("Input");
            var lstYieldIssued = await (from yieldmaster in _yieldRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId &&
                       DbFunctions.TruncateTime(t.CompletedTime) >= input.StartDate.Date &&
                       DbFunctions.TruncateTime(t.CompletedTime) <= input.EndDate.Date)
                                        join yieldinput in _yieldinputRepo.GetAll().Where(t => materialRefIdsList.Contains(t.InputMaterialRefId))
                                        on yieldmaster.Id equals yieldinput.YieldRefId
                                        select new MaterialTrackDto
                                        {
                                            LocationRefId = input.LocationRefId,
                                            MaterialRefId = yieldinput.InputMaterialRefId,
                                            MaterialName = "",
                                            Issued = yieldinput.InputQty,
                                            Recived = 0,
                                            LedgerDate = yieldmaster.AccountDate == null ? yieldmaster.IssueTime : yieldmaster.AccountDate.Value,
                                          //  LedgerDate = (DateTime)yieldmaster.IssueTime,
                                            ModifyTime = (DateTime)yieldmaster.CreationTime,
                                            TrackStatus = stringTrackStatus,
                                            TrackDesc = yieldmaster.RequestSlipNumber.ToString(),
                                            UnitRefId = yieldinput.UnitRefId,
                                            ConversionWithDefaultUnit = yieldinput.ConversionWithDefaultUnit,
                                            SortOrder = 16
                                        }).ToListAsync();

            stringTrackStatus = L("Issue");
            var lstIssued = await (from issuemaster in _issueRepo.GetAll().
                             Where(t => t.LocationRefId == input.LocationRefId && t.IssueTime >= input.StartDate.Date
                             && DbFunctions.TruncateTime(t.IssueTime) <= input.EndDate.Date)
                                   join issuedetail in _issueDetailRepo.GetAll().Where(t => materialRefIdsList.Contains(t.MaterialRefId))
                                   on issuemaster.Id equals issuedetail.IssueRefId
                                   select new MaterialTrackDto
                                   {
                                       LocationRefId = input.LocationRefId,
                                       MaterialRefId = issuedetail.MaterialRefId,
                                       MaterialName = "",
                                       Issued = issuedetail.IssueQty,
                                       Recived = 0,
                                       LedgerDate = issuemaster.AccountDate == null ? issuemaster.IssueTime : issuemaster.AccountDate.Value,
                                       //LedgerDate = issuemaster.IssueTime,
                                       ModifyTime = issuemaster.CreationTime,
                                       TrackStatus = stringTrackStatus,
                                       TrackDesc = issuemaster.RequestSlipNumber,
                                       UnitRefId = issuedetail.UnitRefId,
                                       ConversionWithDefaultUnit = issuedetail.ConversionWithDefaultUnit,
                                       SortOrder = 27
                                   }).ToListAsync();

            stringTrackStatus = L("Sales");
            var lstSales = await (from matLedger in _materialLedgerRepo.GetAll().
                             Where(t => t.LocationRefId == input.LocationRefId && t.LedgerDate >= input.StartDate.Date
                             && DbFunctions.TruncateTime(t.LedgerDate) <= input.EndDate.Date && materialRefIdsList.Contains(t.MaterialRefId))
                                  join mat in _materialRepo.GetAll().Where(t => materialRefIdsList.Contains(t.Id))
                                  on matLedger.MaterialRefId equals mat.Id
                                  select new MaterialTrackDto
                                  {
                                      LocationRefId = input.LocationRefId,
                                      MaterialRefId = matLedger.MaterialRefId,
                                      MaterialName = "",
                                      Issued = matLedger.Sales,
                                      Recived = 0,
                                      LedgerDate = matLedger.LedgerDate.Value,
                                      ModifyTime = matLedger.LedgerDate.Value,
                                      TrackStatus = stringTrackStatus,
                                      TrackDesc = stringTrackStatus,
                                      UnitRefId = mat.DefaultUnitId,
                                      ConversionWithDefaultUnit = 1,
                                      SortOrder = 28
                                  }).ToListAsync();

            int loopCnt = 0;
            foreach (var sd in lstSales)
            {
                lstSales[loopCnt].ModifyTime = lstSales[loopCnt].LedgerDate.AddMinutes(1339);
                loopCnt++;
            }

            stringTrackStatus = L("TransferOut");
            var lstTransferOut = await (from intermaster in _intertransferRepo.GetAll().
                                  Where(t => t.RequestLocationRefId == input.LocationRefId
                                  && DbFunctions.TruncateTime(t.CompletedTime) >= input.StartDate.Date
                                  && DbFunctions.TruncateTime(t.CompletedTime) <= input.EndDate.Date)
                                        join interdetail in _intertransferDetailRepo.GetAll()
                                        on intermaster.Id equals interdetail.InterTransferRefId
                                        select new MaterialTrackDto
                                        {
                                            LocationRefId = input.LocationRefId,
                                            MaterialRefId = interdetail.MaterialRefId,
                                            MaterialName = "",
                                            Issued = interdetail.IssueQty,
                                            Recived = 0,
                                            LedgerDate = intermaster.AccountDate == null ? intermaster.CreationTime : intermaster.AccountDate.Value,
                                            //LedgerDate = (DateTime)intermaster.CompletedTime,
                                            ModifyTime = (DateTime)intermaster.CompletedTime,
                                            TrackStatus = stringTrackStatus,
                                            TrackDesc = intermaster.Id.ToString(),
                                            UnitRefId = interdetail.UnitRefId,
                                            ConversionWithDefaultUnit = interdetail.ConversionWithDefaultUnit,
                                            SortOrder = 15
                                        }).ToListAsync();

            stringTrackStatus = L("Adjustment") + "-" + L("ExcessReceived");
            string strToVerify = L("Excess");
            var lstAdjustmentReceived = await (from adjMas in _adjustmentRepo.GetAll().
                                             Where(t => t.LocationRefId == input.LocationRefId
                                && DbFunctions.TruncateTime(t.AdjustmentDate) >= input.StartDate.Date
                                && DbFunctions.TruncateTime(t.AdjustmentDate) <= input.EndDate.Date)
                                               join adjDet in _adjustmentDetailRepo.GetAll().
                                               Where(t => materialRefIdsList.Contains(t.MaterialRefId)
                                               && t.AdjustmentMode.Equals(strToVerify))
                                               on adjMas.Id equals adjDet.AdjustmentRefIf
                                               select new MaterialTrackDto
                                               {
                                                   LocationRefId = input.LocationRefId,
                                                   MaterialRefId = adjDet.MaterialRefId,
                                                   MaterialName = "",
                                                   Issued = 0,
                                                   Recived = adjDet.AdjustmentQty,
                                                   LedgerDate = adjMas.AccountDate == null ? adjMas.AdjustmentDate : adjMas.AccountDate.Value,
                                                   //LedgerDate = (DateTime)adjMas.AdjustmentDate,
                                                   ModifyTime = (DateTime)adjMas.CreationTime,
                                                   TrackStatus = stringTrackStatus,
                                                   TrackDesc = adjMas.Id.ToString() + " " + adjMas.AdjustmentRemarks + " " + adjDet.AdjustmentApprovedRemarks,
                                                   UnitRefId = adjDet.UnitRefId,
                                                   ConversionWithDefaultUnit = adjDet.ConversionWithDefaultUnit,
                                                   SortOrder = 29
                                               }).ToListAsync();
            loopCnt = 0;
            foreach (var sd in lstAdjustmentReceived)
            {
                if ((lstAdjustmentReceived[loopCnt].LedgerDate.Date) != (lstAdjustmentReceived[loopCnt].ModifyTime.Date))
                {
                    lstAdjustmentReceived[loopCnt].ModifyTime = lstAdjustmentReceived[loopCnt].LedgerDate.AddMinutes(1339);
                }
                loopCnt++;
            }

            loopCnt = 0;
            foreach (var sd in lstAdjustmentIssued)
            {
                if ((lstAdjustmentIssued[loopCnt].LedgerDate.Date) != (lstAdjustmentIssued[loopCnt].ModifyTime.Date))
                {
                    lstAdjustmentIssued[loopCnt].ModifyTime = lstAdjustmentIssued[loopCnt].LedgerDate.AddMinutes(1339);
                }
                loopCnt++;
            }

            var lstFinalReceived = lstDcReceived.Union(lstDirectinvoiceReceived).Union(lstYieldReceived).Union(lstProductionReceived).Union(lstTransferInReceived).Union(lstAdjustmentReceived);
            var lstFinalIssued = lstSalesInvoiceIssued.Union(lstDeliveryIssued).Union(lstYieldIssued).Union(lstIssued).Union(lstSales).Union(lstYieldIssued).Union(lstTransferOut).Union(lstAdjustmentIssued).Union(lstPurchaseReturn);

            var lstFinal = lstFinalIssued.Union(lstFinalReceived).OrderBy(t => t.MaterialRefId).OrderBy(t=>t.AccountDate).OrderBy(t => t.ModifyTime).ToList();
            var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();
            var rsUnits = await _unitRepo.GetAllListAsync();

            foreach (var lst in lstFinal)
            {
                lst.Issued_BasedOnActualUnit = lst.Issued;
                lst.Recived_BasedOnActualUnit = lst.Recived;

                var mat = await _materialRepo.FirstOrDefaultAsync(t => t.Id == lst.MaterialRefId);
                if (mat.DefaultUnitId == lst.UnitRefId)
                {

                }
                else
                {
                    decimal conversionFactor = lst.ConversionWithDefaultUnit;
                    if (conversionFactor == 0)
                    {
                        conversionFactor = rsUc.FirstOrDefault(t => t.BaseUnitId == lst.UnitRefId && t.RefUnitId == mat.DefaultUnitId).Conversion;
                        lst.ConversionWithDefaultUnit = conversionFactor;
                    }
                    int factor = 4;
                    var revisedUnit = rsUnits.FirstOrDefault(t => t.Id == mat.DefaultUnitId);
                    if (revisedUnit.Name.ToUpper().Equals("PC") || revisedUnit.Name.ToUpper().Equals("PCS") || revisedUnit.Name.ToUpper().Equals("NUMBER") || revisedUnit.Name.ToUpper().Equals("NUMBERS"))
                    {
                        factor = 0;
                        conversionFactor = Math.Round(conversionFactor, 0);
                    }
                    lst.OpenBalance = Math.Round(lst.OpenBalance * conversionFactor, factor);
                    lst.Issued = Math.Round(lst.Issued * conversionFactor, factor);
                    lst.Recived = Math.Round(lst.Recived * conversionFactor, factor);
                    lst.ClosingBalance = Math.Round(lst.ClosingBalance * conversionFactor, factor);
                }
            }

            List<MaterialTrackConsolidatedDto> output = new List<MaterialTrackConsolidatedDto>();

            var unitList = await _unitRepo.GetAll().ToListAsync();

            List<int> arrMaterialRefIds = await materialList.Select(t => t.Id).ToListAsync();
            List<MaterialLedger> ledgerCorrections = new List<MaterialLedger>();
            if (arrMaterialRefIds.Count < 20)
            {
                ledgerCorrections = await _materialLedgerRepo.GetAllListAsync(t => arrMaterialRefIds.Contains(t.MaterialRefId)
                    && t.LocationRefId == input.LocationRefId && DbFunctions.TruncateTime(t.LedgerDate) >= DbFunctions.TruncateTime(input.StartDate) && DbFunctions.TruncateTime(t.LedgerDate) <= DbFunctions.TruncateTime(input.EndDate));
            }
            foreach (var material in materialList.ToList())
            {
                MaterialTrackConsolidatedDto curDto = new MaterialTrackConsolidatedDto();

                curDto.LocationRefId = input.LocationRefId;
                curDto.MaterialRefId = material.Id;
                curDto.MaterialName = material.MaterialName;
                curDto.Uom = unitList.FirstOrDefault(t => t.Id == material.DefaultUnitId).Name;

                decimal OpStock = 0.0m;
                decimal ClStock = 0.0m;

                // Needs to get the Closing Balance of Previous day for Opening Balance
                var latestLedgerDate = (from led in _materialLedgerRepo.GetAll()
                                        orderby led.LedgerDate descending
                                        where led.MaterialRefId == material.Id && led.LocationRefId == input.LocationRefId
                                        && DbFunctions.TruncateTime(led.LedgerDate) < DbFunctions.TruncateTime(input.StartDate)
                                        select led.LedgerDate).ToList();


                if (latestLedgerDate.Count() == 0)
                {
                    var currDayRecord = _materialLedgerRepo.FirstOrDefault(l => (l.LedgerDate.Value.Year == input.StartDate.Year && l.LedgerDate.Value.Month == input.StartDate.Month && l.LedgerDate.Value.Day == input.StartDate.Day)
                        && l.MaterialRefId == material.Id && l.LocationRefId == input.LocationRefId);

                    OpStock = 0.0m;

                    if (currDayRecord != null)
                        OpStock = currDayRecord.OpenBalance;
                }
                else
                {
                    var maxLedgerDate = latestLedgerDate.Max();
                    var lastDaysRecord = _materialLedgerRepo.FirstOrDefault(l => (l.LedgerDate.Value.Year == maxLedgerDate.Value.Year && l.LedgerDate.Value.Month == maxLedgerDate.Value.Month && l.LedgerDate.Value.Day == maxLedgerDate.Value.Day)
                    && l.MaterialRefId == material.Id
                    && l.LocationRefId == input.LocationRefId);
                    OpStock = lastDaysRecord.ClBalance;
                }

                curDto.OpeningBalance = OpStock;

                List<MaterialTrackDto> curMaterialList = new List<MaterialTrackDto>();

                //  Adding Opening Stock
                MaterialTrackDto openingStkDto = new MaterialTrackDto
                {
                    SortOrder = 0,
                    TrackStatus = L("OpenBalance"),
                    OpenBalance = OpStock,
                    ClosingBalance = OpStock,
                    ModifyTime = input.StartDate.Date,
                    TrackDesc = "",
                    Issued = 0,
                    Recived = 0,
                };
                curMaterialList.Add(openingStkDto);

                var lstForCurrentMaterial = lstFinal.Where(t => t.MaterialRefId == material.Id).OrderBy(t=>t.AccountDate).OrderBy(t => t.ModifyTime).ThenBy(t => t.SortOrder);

                var currentBalance = OpStock;
                decimal totalReceived = 0;
                decimal totalIssued = 0;
                DateTime closingTime = input.EndDate.Date;

                foreach (var trkMat in lstForCurrentMaterial)
                {
                    MaterialTrackDto matStkDto = new MaterialTrackDto
                    {
                        SortOrder = 1,
                        TrackStatus = trkMat.TrackStatus,
                        OpenBalance = currentBalance,
                        Issued = trkMat.Issued,
                        Recived = trkMat.Recived,
                        ClosingBalance = currentBalance - trkMat.Issued + trkMat.Recived,
                        TrackDesc = trkMat.TrackDesc,
                        ModifyTime = trkMat.ModifyTime,
                        LedgerDate = trkMat.LedgerDate,
                        Remarks = trkMat.Remarks,

                    };
                    totalReceived = totalReceived + trkMat.Recived;
                    totalIssued = totalIssued + trkMat.Issued;

                    currentBalance = currentBalance - trkMat.Issued + trkMat.Recived;

                    if (trkMat.UnitRefId != material.DefaultUnitId)
                    {
                        var defaultUnit = rsUnits.FirstOrDefault(t => t.Id == material.DefaultUnitId);

                        var unit = rsUnits.FirstOrDefault(t => t.Id == trkMat.UnitRefId);
                        decimal actual = 0;
                        decimal calculation = 0;

                        if (trkMat.Issued_BasedOnActualUnit > 0)
                        {
                            actual = trkMat.Issued_BasedOnActualUnit;
                            calculation = trkMat.Issued;
                        }
                        if (trkMat.Recived_BasedOnActualUnit > 0)
                        {
                            actual = trkMat.Recived_BasedOnActualUnit;
                            calculation = trkMat.Recived;
                        }
                        matStkDto.Remarks = "( " + Math.Round(actual,4) + " " + unit.Name + " * " + Math.Round( trkMat.ConversionWithDefaultUnit,4) + " " + defaultUnit.Name + " = " + Math.Round( calculation,4) + " " + defaultUnit.Name + ")" + " " + matStkDto.Remarks ;
                    }

                    curMaterialList.Add(matStkDto);

                    closingTime = trkMat.ModifyTime;
                }

                //  Adding Closing Stock
                MaterialTrackDto closingStkDto = new MaterialTrackDto
                {
                    SortOrder = 100,
                    TrackStatus = L("ClBalance"),
                    OpenBalance = currentBalance,
                    ClosingBalance = currentBalance,
                    ModifyTime = closingTime,
                    TrackDesc = "",
                    Issued = 0,
                    Recived = 0,
                };

                curMaterialList.Add(closingStkDto);

                latestLedgerDate = (from led in _materialLedgerRepo.GetAll()
                                    orderby led.LedgerDate descending
                                    where led.MaterialRefId == material.Id && led.LocationRefId == input.LocationRefId
                                    && DbFunctions.TruncateTime(led.LedgerDate) <= DbFunctions.TruncateTime(closingTime)
                                    select led.LedgerDate).ToList();

                if (latestLedgerDate == null)
                    continue;

                var clMaxDate = latestLedgerDate.Max();

                var clBalDbRecord = _materialLedgerRepo.FirstOrDefault(l => (l.LedgerDate.Value.Year == clMaxDate.Value.Year && l.LedgerDate.Value.Month == clMaxDate.Value.Month && l.LedgerDate.Value.Day == clMaxDate.Value.Day)
                && l.MaterialRefId == material.Id
                && l.LocationRefId == input.LocationRefId);

                if (clBalDbRecord == null)
                {
                    throw new UserFriendlyException(L("NoDataFound"));
                }

                if (clBalDbRecord.ClBalance != currentBalance)
                {
                    decimal diff = clBalDbRecord.ClBalance - currentBalance;

                    if (Math.Abs(diff) > 0.50M)
                    {
                        decimal diffPercentage = Math.Abs(diff) / currentBalance;
                        if (diffPercentage > 0.01M)
                        {
                            //throw new UserFriendlyException(L("MaterialTrackClBalanceErr", clBalDbRecord.LedgerDate.Value.ToString("dd-MMM-yy"), material.MaterialName, currentBalance, clBalDbRecord.ClBalance));
                        }
                    }
                    //if (Math.Abs(diff) > 0.0001m && curDto.ErrorDate == null)
                    //{
                    //    curDto.ErrorDate = clBalDbRecord.LedgerDate.Value;
                    //    curDto.Remarks = clBalDbRecord.LedgerDate.Value.ToString("dd-MMM-yyyy") + " - Current Balance : " + currentBalance + " , But Cl Balance Recorded as :" + clBalDbRecord.ClBalance + ", Please click the Carry Over button to correct the error";
                    //    curDto.ErrorInClosingBalanceCarryOver = true;
                    //}
                }

                var ledgerCheck = ledgerCorrections.Where(t => t.MaterialRefId == curDto.MaterialRefId).OrderBy(t => t.LedgerDate).ToList();
                decimal ledgerPrevDayClose = 0.0m;
                bool firstFlag = true;
                foreach (var ledgerCheckDto in ledgerCheck)
                {
                    if (firstFlag)
                    {
                        ledgerPrevDayClose = ledgerCheckDto.ClBalance;
                        firstFlag = false;
                    }
                    else
                    {
                        if (ledgerPrevDayClose == ledgerCheckDto.OpenBalance)
                        {
                            ledgerPrevDayClose = ledgerCheckDto.ClBalance;
                            continue;
                        }
                        else
                        {
                            if (curDto.ErrorDate == null)
                            {
                                curDto.ErrorDate = ledgerCheckDto.LedgerDate.Value;
                                curDto.Remarks = ledgerCheckDto.LedgerDate.Value.ToString("dd-MMM-yyyy") + " - Open Balance : " + ledgerCheckDto.OpenBalance + " , But Prev Cl Balance Recorded as :" + ledgerPrevDayClose + ", Please click the 'Correct The Issue' for correction. ";
                                curDto.ErrorInClosingBalanceCarryOver = true;
                                break;
                            }
                            ledgerPrevDayClose = ledgerCheckDto.ClBalance;
                        }
                    }
                }


                curDto.TotalIssued = totalIssued;
                curDto.TotalReceived = totalReceived;
                curDto.ClosingBalance = currentBalance;

                curDto.TrackDetail = curMaterialList;

                curMaterialList = null;


                OpStock = 0.0m;
                ClStock = 0.0m;
                currentBalance = 0;
                totalReceived = 0;
                totalIssued = 0;


                output.Add(curDto);
            }

            return output;

        }


        public async Task<GetVerifyMaterialLedgerDto> VerifyAndRectifyClosingStock(VerifyMaterialLedgerClosingStockDto input)
        {

            List<string> OutputChangesList = new List<string>();

            #region VerifyConstraints
            if (input.LocationRefId == 0)
            {
                throw new UserFriendlyException(L("LocationIdDoesNotExist", input.LocationRefId));
            }
            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
            if (loc == null)
            {
                throw new UserFriendlyException(L("LocationIdDoesNotExist", input.LocationRefId));
            }
            #endregion

            int[] arrMaterialRefIds = input.MaterialsList.Select(t => int.Parse(t.Value)).ToArray();

            List<MaterialLedger> lstLedgers = new List<MaterialLedger>();

            if (input.MaterialsList == null || input.MaterialsList.Count == 0)
            {
                lstLedgers = await _materialLedgerRepo.GetAllListAsync(t => t.LocationRefId == input.LocationRefId && t.LedgerDate >= input.StartDate);
            }
            else
            {
                lstLedgers = await _materialLedgerRepo.GetAllListAsync(t => t.LocationRefId == input.LocationRefId && t.LedgerDate >= input.StartDate && arrMaterialRefIds.Contains(t.MaterialRefId));
            }

            if (lstLedgers.Count == 0)
            {
                return new GetVerifyMaterialLedgerDto();
            }

            lstLedgers = lstLedgers.OrderBy(t => t.MaterialRefId).ToList();

            #region ForLooop
            foreach (var grp in lstLedgers.GroupBy(t => t.MaterialRefId))
            {
                #region GetPreviousOpenBalance
                decimal newOpenBalance = 0.0m;
                DateTime lastClosingDate = DateTime.Today;

                var latestLedgerDate = await (from led in _materialLedgerRepo.GetAll()
                                              orderby led.LedgerDate descending
                                              where led.MaterialRefId == grp.Key &&
                                              led.LocationRefId == input.LocationRefId
                                              && led.LedgerDate.Value < input.StartDate
                                              select led.LedgerDate).ToListAsync();

                if (latestLedgerDate.Count() == 0)
                {
                    newOpenBalance = 0;
                }
                else
                {
                    var maxLedgerDate = latestLedgerDate.Max();
                    var lastDaysRecord = await _materialLedgerRepo.FirstOrDefaultAsync(l => DbFunctions.TruncateTime(l.LedgerDate) == DbFunctions.TruncateTime(maxLedgerDate.Value)
                    && l.MaterialRefId == grp.Key
                    && l.LocationRefId == input.LocationRefId);
                    newOpenBalance = lastDaysRecord.ClBalance;
                    lastClosingDate = maxLedgerDate.Value;
                }
                #endregion

                var mat = await _materialRepo.FirstOrDefaultAsync(t => t.Id == grp.Key);

                bool firstDayFlag = true;
                var particularMaterialLedgers = grp.OrderBy(t => t.LedgerDate).ToList();
                bool errorFlag = false;
                decimal closingBalance = 0;
                foreach (var lst in particularMaterialLedgers)
                {
                    #region VerifyFirstRecordOpenBalanceWithPreviousClosingbalance
                    if (firstDayFlag == true)
                    {
                        if (lst.OpenBalance != newOpenBalance)
                        {
                            throw new UserFriendlyException(L("OpeningBalanceError", mat.MaterialName, lst.LedgerDate.Value.ToString("dd-MMM-yyyy"), lst.OpenBalance, newOpenBalance, lastClosingDate.ToString("dd-MMM-yyyy")));
                        }
                    }
                    firstDayFlag = false;
                    #endregion

                    if (errorFlag == false)
                        newOpenBalance = lst.OpenBalance;


                    closingBalance = (newOpenBalance + lst.Received + lst.ExcessReceived + lst.Return + lst.TransferIn) - (lst.Issued + lst.Sales + lst.Damaged + lst.SupplierReturn + lst.Shortage + lst.TransferOut);

                    if (Math.Round(closingBalance, 3) != Math.Round(lst.ClBalance, 3))
                    {
                        errorFlag = true;
                        if (input.CanChangeDbValue == true)
                        {
                            var ledDto = await _materialLedgerRepo.GetAsync(lst.Id);
                            ledDto.OpenBalance = newOpenBalance;
                            ledDto.ClBalance = closingBalance;
                            await _materialLedgerRepo.UpdateAsync(ledDto);
                        }
                        newOpenBalance = closingBalance;
                        string errorMessage = L("ErrorMatLedgerClosingBalance", lst.LedgerDate.Value.ToString("dd-MMM-yyyy ddd"), mat.MaterialName, lst.ClBalance, closingBalance);
                        OutputChangesList.Add(errorMessage);
                    }
                }
                #region UpdateCurrentStock_MaterialLocation

                if (errorFlag == true && input.CanChangeDbValue == true)
                {
                    var matlocstock = await _materiallocationwiseStockRepo.FirstOrDefaultAsync(t => t.LocationRefId == input.LocationRefId && t.MaterialRefId == grp.Key);
                    if (matlocstock == null)
                    {
                        throw new UserFriendlyException(L("MaterialLocationWiseStockError"));
                    }
                    var matlocstockDto = await _materiallocationwiseStockRepo.GetAsync(matlocstock.Id);
                    matlocstockDto.CurrentInHand = closingBalance;
                    await _materiallocationwiseStockRepo.UpdateAsync(matlocstockDto);
                }
                #endregion
            }
            #endregion

            GetVerifyMaterialLedgerDto output = new GetVerifyMaterialLedgerDto();
            output.OutputChangesList = OutputChangesList;

            return output;
        }



    }
}
