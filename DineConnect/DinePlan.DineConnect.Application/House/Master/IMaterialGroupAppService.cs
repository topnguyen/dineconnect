﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master
{
    public interface IMaterialGroupAppService : IApplicationService
    {
        Task<PagedResultOutput<MaterialGroupListDto>> GetAll(GetMaterialGroupInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetMaterialGroupForEditOutput> GetMaterialGroupForEdit(NullableIdInput nullableIdInput);
        Task<IdInput> CreateOrUpdateMaterialGroup(CreateOrUpdateMaterialGroupInput input);
        Task DeleteMaterialGroup(IdInput input);

        Task<ListResultOutput<MaterialGroupListDto>> GetMaterialGroupNames();

        Task<MaterialGroupEditDto> GetOrCreateByName(string groupname);
    }
}
