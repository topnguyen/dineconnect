﻿
//using System.Threading.Tasks;
//using Abp.Application.Services;
//using Abp.Application.Services.Dto;
//using DinePlan.DineConnect.House.Master.Dtos;
//using DinePlan.DineConnect.Dto;

//namespace DinePlan.DineConnect.House.Master
//{
//    public interface IRecipeIngredientAppService : IApplicationService
//    {
//        Task<PagedResultOutput<RecipeIngredientListDto>> GetAll(GetRecipeIngredientInput inputDto);
//        Task<FileDto> GetAllToExcel();
//        Task<GetRecipeIngredientForEditOutput> GetRecipeIngredientForEdit(NullableIdInput nullableIdInput);
//        Task CreateOrUpdateRecipeIngredient(CreateOrUpdateRecipeIngredientInput input);
//        Task DeleteRecipeIngredient(IdInput input);

//        Task<ListResultOutput<RecipeIngredientListDto>> Gets();
//        Task<PagedResultOutput<RecipeIngredientViewDto>> GetView(GetUnitConversionInput input);

//    }
//}

using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master
{
    public interface IRecipeIngredientAppService : IApplicationService
    {
        Task<PagedResultOutput<RecipeIngredientListDto>> GetAll(GetRecipeIngredientInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetRecipeIngredientForEditOutput> GetRecipeIngredientForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateRecipeIngredient(CreateOrUpdateRecipeIngredientInput input);
        Task DeleteRecipeIngredient(IdInput input);

        Task<ListResultOutput<RecipeIngredientListDto>> GetIds();
        Task<PagedResultOutput<RecipeIngredientViewDto>> GetView(GetUnitConversionInput input);

        Task<PagedResultOutput<RecipeIngredientViewDto>> GetViewByRefRecipeId(IdInput input);

    }
}