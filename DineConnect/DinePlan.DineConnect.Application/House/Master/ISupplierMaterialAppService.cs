﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Common.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.House.Master
{
    public interface ISupplierMaterialAppService : IApplicationService
    {
        Task<PagedResultOutput<SupplierMaterialListDto>> GetAll(GetSupplierMaterialInput inputDto);
        Task<FileDto> GetAllToExcel(GetSupplierMaterialInput input);
        Task<GetSupplierMaterialForEditOutput> GetSupplierMaterialForEdit(SupplierWithLocation input);
        Task DeleteSupplierMaterial(SupplierWithLocation input);
        Task<PagedResultOutput<SupplierMaterialViewDto>> GetView(GetSupplierMaterialInput input);
        Task<PagedResultOutput<NameValueDto>> FindMaterials(FindUsersInput input);
        Task<bool> isInSupplierMaterial(CheckSupplierMaterial input);
        Task CreateOrUpdateSupplierMaterial(CreateOrUpdateSupplierMaterialInput input);

        Task<ListResultOutput<SupplierMaterialViewDto>> GetMaterialBasedOnSupplierWithPrice(SentLocationAndSupplier input);

        Task<List<MaterialPoProjectionViewDto>> GetPurchaseProjection(InputPurchaseProjection input);

        Task<ListResultOutput<ComboboxItemDto>> GetSupplierForCombobox();

        Task<List<ApplicableTaxesForMaterial>> GetTaxForMaterial();

        Task<ListResultOutput<CustomerMaterialViewDto>> GetMaterialBasedOnCustomerWithPrice(SentLocationAndCustomer input);

        Task<FileDto> PurchaseProjectionToExcel(InputPurchaseProjection input);

        Task DeleteSupplierMaterialList(DeleteSupplierWithLocationList input);

        Task<List<SupplierMaterialViewDto>> GetSupplierWithPriceBasedOnMaterial(GetSupplierWithLocationAndMaterialInput input);

        Task<List<ApplicableTaxesForMaterial>> GetSaleTaxForMaterial();

    }
}