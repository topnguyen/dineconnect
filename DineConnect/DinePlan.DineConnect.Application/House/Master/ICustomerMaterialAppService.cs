﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master
{
    public interface ICustomerMaterialAppService : IApplicationService
    {
        Task<PagedResultOutput<CustomerMaterialListDto>> GetAll(GetCustomerMaterialInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetCustomerMaterialForEditOutput> GetCustomerMaterialForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateCustomerMaterial(CreateOrUpdateCustomerMaterialInput input);
        Task DeleteCustomerMaterial(IdInput input);

        Task<ListResultOutput<CustomerMaterialListDto>> GetNames();
    }
}