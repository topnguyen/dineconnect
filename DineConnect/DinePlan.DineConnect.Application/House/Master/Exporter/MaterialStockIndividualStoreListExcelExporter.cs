﻿
//using System.Collections.Generic;
//using DinePlan.DineConnect.House.Master.Dtos;
//using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
//using DinePlan.DineConnect.Dto;

//namespace DinePlan.DineConnect.House.Master.Exporter
//{
//    public class MaterialStockIndividualStoreListExcelExporter : EpPlusExcelExporterBase, IMaterialStockIndividualStoreListExcelExporter
//    {
//        public FileDto ExportToFile(List<MaterialStockIndividualStoreListDto> dtos)
//        {
//            return CreateExcelPackage(
//                "MaterialStockIndividualStoreList.xlsx",
//                excelPackage =>
//                {
//                    var sheet = excelPackage.Workbook.Worksheets.Add(L("MaterialStockIndividualStore"));
//                    sheet.OutLineApplyStyle = true;

//                    AddHeader(
//                        sheet,
//                        L("Id")
//                        );

//                    AddObjects(
//                        sheet, 2, dtos,
//                        _ => _.Id
//                        );

//                    for (var i = 1; i <= 1; i++)
//                    {
//                        sheet.Column(i).AutoFit();
//                    }
//                });
//        }
//    }
//}
