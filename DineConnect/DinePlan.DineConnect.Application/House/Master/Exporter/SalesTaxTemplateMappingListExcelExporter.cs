﻿
using System.Collections.Generic;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master.Exporter
{
    public class SalesTaxTemplateMappingListExcelExporter : FileExporterBase, ISalesTaxTemplateMappingListExcelExporter
    {
        public FileDto ExportToFile(List<SalesTaxTemplateMappingListDto> dtos)
        {
            return CreateExcelPackage(
                "SalesTaxTemplateMappingList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("SalesTaxTemplateMapping"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}