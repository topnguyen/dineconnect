﻿using System.Collections.Generic;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master.Exporter
{
    public class SupplierListExcelExporter : FileExporterBase, ISupplierListExcelExporter
    {
        public FileDto ExportToFile(List<SupplierListDto> dtos)
        {
            return CreateExcelPackage(
                "SupplierList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Supplier"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Code"),
                        L("Name"),
                        L("Tax") + " " + L("Applicable"),
                        L("TaxRegistrationNumber"),
                        L("Address1"),
                        L("Address2"),
                        L("Address3"),
                        L("City"),
                        L("ZipCode")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.SupplierCode,
                        _ => _.SupplierName,
                        _ => _.TaxApplicable == true ? "Yes" : "No",
                        _ => _.TaxRegistrationNumber,
                        _ => _.Address1,
                        _ => _.Address2,
                        _ => _.Address3,
                        _ => _.City,
                        _ => _.ZipCode
                        );

                    for (var i = 1; i <= 15; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}