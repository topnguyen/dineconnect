﻿
using System.Collections.Generic;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master.Exporter
{
    public class SupplierMaterialListExcelExporter : FileExporterBase, ISupplierMaterialListExcelExporter
    {
        public FileDto ExportToFile(List<SupplierMaterialViewDto> dtos)
        {
            return CreateExcelPackage(
                "SupplierMaterialList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("SupplierMaterial"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Supplier"),
                        L("MaterialName"),
						L("SupplierMaterialAliasName"),
						L("Unit"),
                        L("Price"),
                        L("MOQ")
						);

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.SupplierRefName,
                        _ => _.MaterialRefName,
						_ => _.SupplierMaterialAliasName,
						_ => _.UnitRefName,
                        _ => _.MaterialPrice,
                        _ => _.MinimumOrderQuantity
						);

                    for (var i = 1; i <= 10; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }

		public FileDto PurchaseProjectionExportToFile(PurchaseProjectionDto input)
		{
			return CreateExcelPackage(
				input.FileName +".xlsx",
				excelPackage =>
				{
					var sheet = excelPackage.Workbook.Worksheets.Add(L("PurchaseProjection"));
					sheet.OutLineApplyStyle = true;

					AddHeader(
						sheet,
						L("Material"),
						L("PerDayConsumption"),
						L("OnHand"),
						L("ProjectedQty"),
						L("InQueue"),
						L("OrderQty"),
						L("UOM"),
						L("Cost"),
						L("Total"),
						L("Supplier")
						);

					AddObjects(
						sheet, 2, input.ProjecttionData,
						_ => _.MaterialName,
						_ => _.PerDayConsumption,
						_ => _.OnHand,
						_ => _.NextNDaysProjectedConsumption,
						_ => _.AlreadyOrderedQuantity,
						_ => _.NextNDaysRoundedProjection,
						_ => _.Uom,
						_ => _.Price,
						_ => _.Amount,
						_ => _.SupplierRefName
						);

					for (var i = 1; i <= 15; i++)
					{
						sheet.Column(i).AutoFit();
					}
				});
		}
		
	}
}