﻿
using System.Collections.Generic;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using System.Linq;
using System;

namespace DinePlan.DineConnect.House.Master.Exporter
{
    public class MaterialMenuMappingListExcelExporter : FileExporterBase, IMaterialMenuMappingListExcelExporter
    {
        public FileDto ExportToFile(List<MaterialMenuMappingListDto> dtos)
        {
            return CreateExcelPackage(
                "MaterialMenuMappingList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("MaterialMenuMapping"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }

        public FileDto ExportParticularMaterialMenuMappingToFile(List<MaterialMenuMappingListDto> materialMenuMappingListDtos)
        {
            string fileName = "";
            string materialName = "";
            if (materialMenuMappingListDtos.Count == 1)
            {
                fileName = "No More Mapping Menu";
                materialName = "No More Material";
            }
            else
            {
                materialName = materialMenuMappingListDtos.FirstOrDefault().MaterialRefName;
                fileName = materialName;
            }

            fileName = "Mapping Menu for " + fileName + " - " + DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss");
            var fileReturn = CreateExcelPackage(
                fileName + ".xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add("Mapped Menu List" + " " + materialName);
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        "Menu Id",
                        "Menu Item Name",
                        "Portion Name",
                        "Menu Qty",
                        "Material Code",
                        "Material Name",
                        "Portion Qty",
                        "Unit",
                        "Auto Sales Deduction",
                        "Department Linked",
                        "Revised Material Code",
                        "Revised Material Name",
                        "Revised Portion Qty",
                        "Revised Unit"
                        //"Revised Auto Sales Deduction",
                        //"Revised Department Linked"
                        );


                    AddObjects(
                        sheet, 2, materialMenuMappingListDtos,
                        r => r.PosMenuPortionRefId,
                        r => r.MenuItemName,
                        r => r.PortionName,
                        r => r.MenuQuantitySold,
                        r => r.MaterialRefCode,
                        r => r.MaterialRefName,
                        r => r.PortionQty,
                        r => r.UnitRefName,
                        r => r.AutoSalesDeduction,
                        r => r.DeptNames
                        );

                    sheet.Protection.IsProtected = true;
                    sheet.Protection.SetPassword(DateTime.Today.ToString("ddMMMyy").ToLower());
                 
                    sheet.Column(11).Style.Locked = false;
                    sheet.Column(12).Style.Locked = false;
                    sheet.Column(13).Style.Locked = false;
                    sheet.Column(14).Style.Locked = false;

                    int rowCount = 2 + materialMenuMappingListDtos.Count + 1;

                    //for (var i = 1; i <= 20; i++)
                    //{
                    //    sheet.Column(i).AutoFit();
                    //}
                    sheet.Cells.AutoFitColumns();
                    sheet.Column(5).Hidden = true;
                    sheet.Column(6).Hidden = true;
                    //var detailSheet = excelPackage.Workbook.Worksheets.Add(L("MenuCost") + L("Detail"));
                    //detailSheet.OutLineApplyStyle = true;

                    //rowCount = 1;

                    //foreach (var lst in masterDtos)
                    //{
                    //    AddHeader(detailSheet, rowCount, 1, lst.DepartmentRefName + " " + lst.MenuItemRefName);
                    //    rowCount++;
                    //    AddHeader(detailSheet, rowCount, 1, L("SoldQty"));
                    //    AddDetail(detailSheet, rowCount, 2, lst.QtySold, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                    //    rowCount++;
                    //    AddHeader(detailSheet, rowCount, 1, L("Sales"));
                    //    AddDetail(detailSheet, rowCount, 2, lst.SalesAmount, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                    //    rowCount++;
                    //    AddHeader(detailSheet, rowCount, 1, L("TotalCost"));
                    //    AddDetail(detailSheet, rowCount, 2, lst.TotalCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                    //    rowCount++;
                    //    AddHeader(detailSheet, rowCount, 1, L("Profit"));
                    //    AddDetail(detailSheet, rowCount, 2, lst.Profit, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                    //    rowCount++;
                    //    AddHeader(detailSheet, rowCount, 1, L("Profit") + " %");
                    //    AddDetail(detailSheet, rowCount, 2, lst.ProfitMargin, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                    //    rowCount++;
                    //    if (lst.TotalCost == 0)
                    //    {
                    //        rowCount++;
                    //        rowCount++;
                    //        continue;
                    //    }
                    //    AddHeader(
                    //        detailSheet,
                    //        rowCount,
                    //        @L("Material"),
                    //        @L("QuantityPerPortion"),
                    //        @L("UnitCost"),
                    //        @L("CostPerUom"),
                    //        @L("TotalUsage"),
                    //        @L("LineTotal"),
                    //        @L("FoodCostPercentage")
                    //        );
                    //    rowCount++;
                    //    AddObjects(
                    //        detailSheet, rowCount, lst.StandardMaterialWiseValueAndPercentage,
                    //        r => r.MaterialRefName,
                    //        r => r.UsagePerUnit,
                    //        r => r.AvgRate,
                    //        r => Math.Round(r.AvgRate * r.UsagePerUnit, roundDecimals, MidpointRounding.AwayFromZero),
                    //        r => r.UsageQty,
                    //        r => r.NetAmount,
                    //        r => Math.Round(r.NetAmount / lst.TotalCost * 100, roundDecimals, MidpointRounding.AwayFromZero)
                    //        );

                    //    rowCount = rowCount + lst.StandardMaterialWiseValueAndPercentage.Count;
                    //    rowCount++;

                    //    AddDetail(detailSheet, rowCount, 4, lst.CostPerUnit, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                    //    AddDetail(detailSheet, rowCount, 6, lst.TotalCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                    //    rowCount = rowCount + 3;
                    //}

                    //for (var i = 1; i <= 10; i++)
                    //{
                    //    detailSheet.Column(i).AutoFit();
                    //}
                });

            return fileReturn;
        }
        
    }
}
