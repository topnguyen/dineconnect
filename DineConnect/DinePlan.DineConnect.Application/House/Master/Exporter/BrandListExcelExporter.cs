﻿using System.Collections.Generic;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Master.Dtos;

namespace DinePlan.DineConnect.House.Master.Exporter
{
    public class BrandListExcelExporter : FileExporterBase, IBrandListExcelExporter
    {
        public FileDto ExportToFile(List<BrandListDto> dtos)
        {
            return CreateExcelPackage(
                "BrandList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Brand"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
