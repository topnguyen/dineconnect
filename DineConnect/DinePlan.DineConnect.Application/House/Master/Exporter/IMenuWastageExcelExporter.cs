﻿using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter.Util;
using DinePlan.DineConnect.House.Transaction;
using DinePlan.DineConnect.House.Transaction.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Exporter;

namespace DinePlan.DineConnect.House.Master.Exporter
{
	public interface IMenuWastageExcelExporter
	{
		FileDto ExportToFile(List<MenuItemWastageListDto> dtos);
		FileDto MenuWastageReportToExcelFile(List<MenuWastageConsolidatedReport> dtos, DateTime FromDate, DateTime ToDate, string UserName);

		Task<FileDto> MenuWastageReportExportAsync(IMenuItemWastageAppService menuwastageAppService, InputMenuWastageReport input, string UserName, ExportType exporttype);
        Task<FileDto> MenuWastageCustomReportExcelExportAsync(IMenuItemWastageAppService menuwastageAppService, InputMenuWastageReport input);

    }
}


