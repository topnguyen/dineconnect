﻿using System;
using System.Collections.Generic;
using System.Linq;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using System.Data.OleDb;
using System.Data;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.House.Transaction.Implementation;
using System.Threading.Tasks;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using System.Linq.Dynamic;
using DinePlan.DineConnect.Connect.Master;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Exporter.Util;
using Abp.Configuration;
using DinePlan.DineConnect.Configuration;
using System.Text;
using OfficeOpenXml.Style;
using System.Drawing;
using System.Reflection;
using FieldAttributes = DinePlan.DineConnect.Exporter.Util.FieldAttributes;
using DinePlan.DineConnect.House.Transaction;
using DinePlan.DineConnect.Connect.Master.Dtos;
using Abp.AutoMapper;
using System.IO;
using Abp.Extensions;

namespace DinePlan.DineConnect.House.Master.Exporter
{
    public class HouseReportListExcelExporter : FileExporterBase, IHouseReportListExcelExporter
    {
        private readonly IRepository<Location> _locationRepo;
        private SettingManager _settingManager;
        private int roundDecimals = 3;
        private string dateFormat = "dd-MM-yyyy";
        private IRepository<Unit> _unitRepo;

        public HouseReportListExcelExporter(IRepository<Location> locationRepo,
            SettingManager settingManager,
            IRepository<Unit> unitRepo)
        {
            _locationRepo = locationRepo;
            _settingManager = settingManager;
            roundDecimals = _settingManager.GetSettingValue<int>(AppSettings.ConnectSettings.Decimals);
            dateFormat = _settingManager.GetSettingValue(AppSettings.ConnectSettings.DateTimeFormat);
            _unitRepo = unitRepo;
        }

        public FileDto ExportToFile(List<HouseReportListDto> dtos)
        {
            return CreateExcelPackage(
                "HouseReportList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("HouseReport"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id
                        );

                    for (var i = 1; i <= 7; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }


        public FileDto VarianceReportToExcelFile(List<VarianceReportDto> dtos, DateTime FromDate, DateTime ToDate, string UserName)
        {
            return CreateExcelPackage(
            @L("InventoryClosingVariance") + ".xlsx",
            excelPackage =>
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("InventoryClosingVariance"));
                sheet.OutLineApplyStyle = true;
                int row = 1;
                AddHeaderWithMergeWithColumnCount(sheet, row, 1, 16, L("InventoryClosingVariance"));
                row++;
                AddHeaderWithMergeWithColumnCount(sheet, row, 1, 16, L("FromTimeToTime", FromDate.ToString("dd-MMM-yyyy"), ToDate.ToString("dd-MMM-yyyy")));
                row++;
                AddDetail(sheet, row, 1, L("PrintedOn"));
                AddDetail(sheet, row, 2, DateTime.Today.ToString("dd-MMM-yyyy"));
                row++;
                AddDetail(sheet, row, 1, L("PrintedBy"));
                AddDetail(sheet, row, 2, UserName);
                row++;
                row++;

                AddHeader(sheet, row, L("Material"), L("UOM"), L("Price"), L("OpenBalance"), L("Received"), L("Issued"), L("TransferOut"), L("TransferIn"), L("Wastage"), L("ExcessReceived"), L("Shortage"), L("Return"), L("PurchaseReturn"), L("Sales"), L("ClBalance"), L("EnteredClosingBalance"), L("Variance"), L("VarianceCost"));

                foreach (var mas in dtos)
                {
                    row++;
                    AddHeaderWithMergeWithColumnCount(sheet, row, 1, 5, "Category : " + mas.MaterialGroupCategoryRefName);
                    row++;
                    AddObjects(sheet, row, mas.VarianceReport,
                    t => t.MaterialRefName,
                    t => t.Uom,
                    t => t.AvgRate,
                    t => t.OpenBalance,
                    t => t.Received,
                    t => t.Issued,
                    t => t.TransferOut,
                    t => t.TransferIn,
                    t => t.Damaged,
                    t => t.ExcessReceived,
               t => t.Shortage,
                    t => t.Return,
                    t => t.SupplierReturn,
                    t => t.Sales,
                    t => t.ClBalance,
                    t => t.IsClosingStockEntered == true ? t.ClosingStockEntered.ToString() : "Not Entered",
                    t => t.IsClosingStockEntered == true ? t.Variance.ToString() : "",
                    t => t.IsClosingStockEntered == true ? t.VarianceCost.ToString() : ""
                    );
                    row = row + mas.VarianceReport.Count;
                    row++;
                }
                for (var i = 1; i <= 25; i++)
                {
                    sheet.Column(i).AutoFit();
                }
            });
        }

        public async Task<List<FileDto>> MaterialWastageReportToExcelFile(OverAllMaterialWastageReport output, InputAdjustmentReport input)
        {

            input.DateRange = "From: " + input.StartDate.Value.ToString("dd-MMM-yyyy") + " To: " + input.EndDate.Value.ToString("dd-MMM-yyyy");
            string fileName = L("Material") + " " + L("Wastage") + " " + input.DateRange;//+ " " + input.LocationInfo.Code + " " + input.LocationInfo.Name;
            if (input.ToLocations.Count == 1)
            {
                fileName = fileName + " " + input.LocationInfo.Code + " " + input.LocationInfo.Name;
            }

            List<FileDto> returnFileList = new List<FileDto>();
            FileDto a = CreateExcelPackage(fileName + ".xlsx",
            excelPackage =>
            {
                if (input.CustomReportFlag)
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("MaterialWastage"));
                    sheet.OutLineApplyStyle = true;
                    int row = 1;
                    AddHeaderWithMergeWithColumnCount(sheet, row, 1, 10, L("Material") + " " + L("Wastage"));
                    sheet.Row(row).Height = 30;
                    sheet.Cells[row, 1].Style.Font.Size = 15;
                    row++;
                    AddHeaderWithMergeWithColumnCountAndAlignment(sheet, row, 3, 2, L("Location") + " " + L("Code") + ":" + input.LocationInfo.Code, ExcelHorizontalAlignment.Right);
                    AddHeaderWithMergeWithColumnCountAndAlignment(sheet, row, 6, 2, input.LocationInfo.Name, ExcelHorizontalAlignment.Left);
                    row++;
                    AddHeaderWithMergeWithColumnCount(sheet, row, 1, 10, input.DateRange);
                    row++;
                    AddHeaderWithMergeWithColumnCount(sheet, row, 1, 1, L("PrintedOn"));
                    AddHeaderWithMergeWithColumnCount(sheet, row, 3, 1, input.PrintedTime.ToString("dd-MMM-yyyy"));
                    row++;
                    AddHeaderWithMergeWithColumnCount(sheet, row, 1, 1, L("PrintedBy"));
                    AddHeaderWithMergeWithColumnCount(sheet, row, 3, 1, input.PreparedUserName);
                    row++;

                    AddHeader(sheet, row, L("Date"), L("Price"), L("AdjustmentMode"), L("EntryType"), /*L("UOM"),*/ L("Purchase") + " " + L("Qty"), L("Inventory") + " " + L("Qty"), L("Recipe") + " " + L("Qty"), L("Total") + L("Cost"), L("User"), L("Remarks"), L("Approval") + " " + L("Ref"));

                    row++;

                    foreach (var gp in output.WastageReport.GroupBy(t => t.MaterialRefName))
                    {
                        int col = 1;
                        var firstRecord = gp.FirstOrDefault();
                        AddHeaderWithMergeWithColumnCountAndAlignment(sheet, row, 1, 3, firstRecord.MaterialPetName + " " + firstRecord.MaterialRefName, ExcelHorizontalAlignment.Left);
                        row++;
                        decimal subTotalAmount = 0;
                        foreach (var det in gp.ToList())
                        {
                            col = 1;
                            AddDetail(sheet, row, col++, det.AdjustmentDate.ToString("dd-MMM-yyyy"));
                            AddDetailWithNumberFormat(sheet, row, col++, det.Price, "#0.0000");
                            AddDetail(sheet, row, col++, det.AdjustmentMode);
                            AddDetail(sheet, row, col++, det.MenuWastageMode);
                            //AddDetail(sheet, row, col++, det.UnitRefName);
                            AddDetailWithNumberFormat(sheet, row, col++, det.QuantityInPurchaseUnit, "#0.0000");
                            AddDetailWithNumberFormat(sheet, row, col++, det.QuantityInStkAdjustedInventoryUnit, "#0.0000");
                            AddDetailWithNumberFormat(sheet, row, col++, det.QuantityInIssueUnit, "#0.0000");
                            AddDetailWithNumberFormat(sheet, row, col++, det.TotalAmount, "#0.0000");
                            AddDetail(sheet, row, col++, det.UserName);
                            AddDetail(sheet, row, col++, det.Remarks);
                            AddDetail(sheet, row, col++, det.TokenRefNumber.ToString());
                            row++;
                            subTotalAmount = subTotalAmount + det.TotalAmount;
                        }
                        row++;
                        AddDetail(sheet, row, 1, "Sub Total");
                        AddDetailWithNumberFormat(sheet, row, 8, subTotalAmount, "#0.0000");
                        sheet.Cells[row, 8].Style.Font.Bold = true;
                        row++;
                        row++;
                    }
                    AddDetail(sheet, row, 1, L("GrandTotal"));
                    sheet.Cells[row, 1].Style.Font.Bold = true;
                    AddDetailWithNumberFormat(sheet, row, 8, output.OverAllTotalAmount, "0.0000");
                    sheet.Cells[row, 8].Style.Font.Bold = true;
                    for (var i = 1; i <= 25; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                }

                string Locations = "";
                {
                    #region DetailSheet
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Wastage"));
                    sheet.OutLineApplyStyle = true;
                    int row = 1;
                    AddHeaderWithMergeWithColumnCount(sheet, row, 1, 7, L("MaterialWastageReport"));
                    row++;
                    AddHeaderWithMergeWithColumnCount(sheet, row, 1, 7, L("FromTimeToTime", input.StartDate.Value.ToString("dd-MMM-yyyy"), input.EndDate.Value.ToString("dd-MMM-yyyy")));
                    row++;
                    AddDetail(sheet, row, 1, L("PrintedOn"));
                    AddDetail(sheet, row, 2, input.PrintedTime.ToString("dd-MMM-yyyy"));
                    row++;
                    AddDetail(sheet, row, 1, L("PrintedBy"));
                    AddDetail(sheet, row, 2, input.PreparedUserName);
                    row++;
                    row++;

                    AddHeader(sheet, row, L("Material"), L("EntryType"), L("Quantity"), L("UOM"), L("Price"), L("TotalCost"), L("Date"), L("User"));
                    decimal OverAllCost = 0;
                    var dtos = output.LocationWiseWastageList;
                    foreach (var mas in dtos)
                    {
                        Locations = Locations + mas.LocationRefName + ", ";
                        row++;
                        AddHeaderWithMergeWithColumnCount(sheet, row, 1, 5, "Location : " + mas.LocationRefName);
                        row++;
                        AddObjects(sheet, row, mas.WastageDetailReport,
                                t => t.MaterialRefName,
                                t => t.MenuWastageMode,
                                t => t.TotalQty,
                                t => t.DefaultUnitName,
                                t => t.Price,
                                t => t.TotalAmount,
                                t => t.AdjustmentDate.ToString("dd-MMM-yyyy"),
                                t => t.UserName
                                );
                        row = row + mas.WastageDetailReport.Count;
                        row++;
                        AddDetail(sheet, row, 2, L("SubTotal"));
                        decimal totalCost = Math.Round(mas.ConsolidatedWastageReport.Sum(t => t.TotalAmount), 2, MidpointRounding.AwayFromZero);
                        OverAllCost = OverAllCost + totalCost;
                        AddDetail(sheet, row, 6, totalCost);
                        row++;
                        row++;
                    }
                    row++;
                    AddDetail(sheet, row, 2, L("Total"));
                    AddDetail(sheet, row, 6, OverAllCost);
                    row++;
                    for (var i = 1; i <= 20; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                    #endregion
                }

                {
                    var dtos = output.LocationWiseWastageList;
                    #region LocationWiseConsolidated
                    var locationWiseConsolidated = excelPackage.Workbook.Worksheets.Add(L("Consolidated") + "-" + L("Location"));
                    locationWiseConsolidated.OutLineApplyStyle = true;
                    int row = 1;
                    AddHeaderWithMergeWithColumnCount(locationWiseConsolidated, row, 1, 4, L("MaterialWastageReport") + " " + L("Consolidated") + " " + L("Location"));
                    row++;
                    AddHeaderWithMergeWithColumnCount(locationWiseConsolidated, row, 1, 4, L("FromTimeToTime", input.StartDate.Value.ToString("dd-MMM-yyyy"), input.EndDate.Value.ToString("dd-MMM-yyyy")));
                    row++;
                    AddDetail(locationWiseConsolidated, row, 1, L("PrintedOn"));
                    AddDetail(locationWiseConsolidated, row, 2, input.PrintedTime.ToString("dd-MMM-yyyy"));
                    row++;
                    AddDetail(locationWiseConsolidated, row, 1, L("PrintedBy"));
                    AddDetail(locationWiseConsolidated, row, 2, input.PreparedUserName);
                    row++;
                    row++;

                    AddHeader(locationWiseConsolidated, row, L("Material"), L("Quantity"), L("UOM"), L("Price"), L("TotalCost"));
                    decimal OverAllCost = 0;
                    foreach (var mas in dtos)
                    {
                        row++;
                        AddHeaderWithMergeWithColumnCount(locationWiseConsolidated, row, 1, 4, "Location : " + mas.LocationRefName);
                        row++;
                        AddObjects(locationWiseConsolidated, row, mas.ConsolidatedWastageReport,
                                t => t.MaterialRefName,
                                t => t.TotalQty,
                                t => t.DefaultUnitName,
                                t => t.Price,
                                t => t.TotalAmount
                                );
                        row = row + mas.ConsolidatedWastageReport.Count;
                        row++;
                        AddDetail(locationWiseConsolidated, row, 2, L("SubTotal"));
                        decimal totalCost = Math.Round(mas.ConsolidatedWastageReport.Sum(t => t.TotalAmount), 2, MidpointRounding.AwayFromZero);
                        OverAllCost = OverAllCost + totalCost;
                        AddDetail(locationWiseConsolidated, row, 5, totalCost);
                        row++;
                        row++;
                    }
                    row++;
                    AddDetail(locationWiseConsolidated, row, 2, L("Total"));
                    AddDetail(locationWiseConsolidated, row, 5, OverAllCost);
                    row++;
                    for (var i = 1; i <= 20; i++)
                    {
                        locationWiseConsolidated.Column(i).AutoFit();
                    }
                    #endregion
                }

                Locations = Locations.Substring(0, Locations.Length - 2);

                {
                    #region OverAllConsolidated
                    var overAllConsolidated = excelPackage.Workbook.Worksheets.Add(L("Consolidated"));
                    overAllConsolidated.OutLineApplyStyle = true;
                    int row = 1;
                    AddHeaderWithMergeWithColumnCount(overAllConsolidated, row, 1, 4, L("MaterialWastageReport") + " " + L("Consolidated") + " " + L("All"));
                    row++;
                    AddHeaderWithMergeWithColumnCount(overAllConsolidated, row, 1, 4, L("FromTimeToTime", input.StartDate.Value.ToString("dd-MMM-yyyy"), input.EndDate.Value.ToString("dd-MMM-yyyy")));
                    row++;
                    AddDetail(overAllConsolidated, row, 1, L("PrintedOn"));
                    AddDetail(overAllConsolidated, row, 2, input.PrintedTime.ToString("dd-MMM-yyyy"));
                    row++;
                    AddDetail(overAllConsolidated, row, 1, L("PrintedBy"));
                    AddDetail(overAllConsolidated, row, 2, input.PreparedUserName);
                    row++;
                    row++;

                    AddHeader(overAllConsolidated, row, L("Material"), L("Quantity"), L("UOM"), L("Price"), L("TotalCost"));
                    decimal OverAllCost = 0;
                    {
                        row++;
                        AddHeaderWithMergeWithColumnCount(overAllConsolidated, row, 1, 4, "Location : " + Locations);
                        row++;
                        AddObjects(overAllConsolidated, row, output.OverAllWastageReport,
                                t => t.MaterialRefName,
                                t => t.TotalQty,
                                t => t.DefaultUnitName,
                                t => t.Price,
                                t => t.TotalAmount
                                );
                        row = row + output.OverAllWastageReport.Count;
                        row++;
                        decimal totalCost = Math.Round(output.OverAllWastageReport.Sum(t => t.TotalAmount), 2, MidpointRounding.AwayFromZero);
                        OverAllCost = OverAllCost + totalCost;
                    }
                    row++;
                    AddDetail(overAllConsolidated, row, 2, L("Total"));
                    AddDetail(overAllConsolidated, row, 5, OverAllCost);
                    row++;
                    for (var i = 1; i <= 20; i++)
                    {
                        overAllConsolidated.Column(i).AutoFit();
                    }
                    #endregion
                }
            });

            returnFileList.Add(a);
            return returnFileList;
        }

        public async Task<List<FileDto>> MaterialWastageReportToExport(OverAllMaterialWastageReport dtos, InputAdjustmentReport input, string UserName)
        {
            List<FileDto> returnFileList = new List<FileDto>();

            #region Custom Report
            if (input.CustomReportFlag)
            {
                var customReport = MaterialWastageCustomReportToExport(dtos, input);
                returnFileList.Add(customReport);
                return returnFileList;
            }
            #endregion

            #region LocationWise_Detail
            {
                var eObject = new ExportInputObject
                {
                    ExportType = input.ExportType
                };

                eObject.Fields.Add("MaterialRefName", new FieldAttributes
                {
                    DisplayName = L("Material"),
                    Alignment = FieldAlignment.Left,
                    FieldType = ExportFiledType.String,
                });
                eObject.Fields.Add("MenuWastageMode", new DineConnect.Exporter.Util.FieldAttributes
                {
                    DisplayName = L("EntryType"),
                    Alignment = FieldAlignment.Left,
                    FieldType = ExportFiledType.String
                });
                eObject.Fields.Add("TotalQty", new FieldAttributes
                {
                    DisplayName = L("Quantity"),
                    Alignment = FieldAlignment.Right,
                    FieldType = ExportFiledType.Decimal,
                    Format = "N" + roundDecimals,
                });
                eObject.Fields.Add("DefaultUnitName", new FieldAttributes
                {
                    DisplayName = L("UOM"),
                    Alignment = FieldAlignment.Left,
                    FieldType = ExportFiledType.String,
                });
                eObject.Fields.Add("Price", new FieldAttributes
                {
                    DisplayName = L("Price"),
                    Alignment = FieldAlignment.Right,
                    FieldType = ExportFiledType.Decimal,
                    Format = "N" + roundDecimals,
                });
                eObject.Fields.Add("TotalAmount", new FieldAttributes
                {
                    DisplayName = L("TotalCost"),
                    Alignment = FieldAlignment.Right,
                    FieldType = ExportFiledType.Decimal,
                    Format = "N" + roundDecimals,
                    TotalRequired = true
                });
                eObject.Fields.Add("AdjustmentDate", new FieldAttributes
                {
                    DisplayName = L("Date"),
                    Alignment = FieldAlignment.Left,
                    FieldType = ExportFiledType.Date,
                    Format = dateFormat
                });
                eObject.Fields.Add("UserName", new FieldAttributes
                {
                    DisplayName = L("User"),
                    Alignment = FieldAlignment.Left,
                    FieldType = ExportFiledType.String
                });

                var items = dtos.LocationWiseWastageList;

                if (items.Any())
                {
                    foreach (var mas in items)
                    {
                        var pOutput = new List<ExportDataObject>();
                        pOutput.AddRange(mas.WastageDetailReport);

                        StringBuilder firstPageBuilder = new StringBuilder();
                        firstPageBuilder.Append(L("Location") + " : ");
                        firstPageBuilder.Append(mas.LocationRefName);
                        firstPageBuilder.Append("-");
                        firstPageBuilder.Append(L("MaterialWastageReport"));
                        firstPageBuilder.Append("{NL}");
                        firstPageBuilder.Append(string.Format(L("FromTimeToTime"), input.StartDate.Value.ToString(dateFormat), input.EndDate.Value.ToString(dateFormat)));
                        firstPageBuilder.Append("{NL}");

                        StringBuilder lPageBuilder = new StringBuilder();
                        lPageBuilder.Append("{NL}");
                        lPageBuilder.Append(L("PrintedOn") + " :" + DateTime.Now.ToString(dateFormat));

                        eObject.FirstPage = firstPageBuilder.ToString();
                        eObject.LastPage = lPageBuilder.ToString();
                        eObject.ExportObjects = pOutput;

                        StringBuilder fileNameBuilder = new StringBuilder();
                        fileNameBuilder.Append(mas.LocationRefName);
                        fileNameBuilder.Append(L("-"));
                        fileNameBuilder.Append(L("MaterialWastageReport"));
                        fileNameBuilder.Append(L("-"));
                        fileNameBuilder.Append(!input.StartDate.Value.Equals(DateTime.MinValue)
                            ? input.StartDate.Value.ToString("dd/MM/yyyy")
                            : DateTime.Now.ToString("dd/MM/yyyy"));
                        fileNameBuilder.Append(" - ");
                        fileNameBuilder.Append(!input.EndDate.Value.Equals(DateTime.MinValue)
                            ? input.EndDate.Value.ToString("dd/MM/yyyy")
                            : DateTime.Now.ToString("dd/MM/yyyy"));

                        eObject.FileName = fileNameBuilder.ToString();
                        var exporter = new DineConnectDocExporter(eObject);
                        FileDto a = exporter.ExportFile(AppFolders.TempFileDownloadFolder);
                        returnFileList.Add(a);
                    }
                    //    return returnFileList;
                }
            }
            #endregion

            string Locations = "";
            #region LocationWiseConsolidated
            {
                var eObject = new ExportInputObject
                {
                    ExportType = input.ExportType
                };

                //AddHeader(locationWiseConsolidated, row, L("Material"), L("EntryType"), L("Quantity"), L("UOM"), L("Price"), L("TotalCost"));
                eObject.Fields.Add("MaterialRefName", new FieldAttributes
                {
                    DisplayName = L("Material"),
                    Alignment = FieldAlignment.Left,
                    FieldType = ExportFiledType.String,
                });
                eObject.Fields.Add("TotalQty", new FieldAttributes
                {
                    DisplayName = L("Quantity"),
                    Alignment = FieldAlignment.Right,
                    FieldType = ExportFiledType.Decimal,
                    Format = "N" + roundDecimals,
                });
                eObject.Fields.Add("DefaultUnitName", new FieldAttributes
                {
                    DisplayName = L("UOM"),
                    Alignment = FieldAlignment.Left,
                    FieldType = ExportFiledType.String,
                });
                eObject.Fields.Add("Price", new FieldAttributes
                {
                    DisplayName = L("Price"),
                    Alignment = FieldAlignment.Right,
                    FieldType = ExportFiledType.Decimal,
                    Format = "N" + roundDecimals,
                });
                eObject.Fields.Add("TotalAmount", new FieldAttributes
                {
                    DisplayName = L("TotalCost"),
                    Alignment = FieldAlignment.Right,
                    FieldType = ExportFiledType.Decimal,
                    Format = "N" + roundDecimals,
                    TotalRequired = true
                });

                var items = dtos.LocationWiseWastageList;

                if (items.Any())
                {
                    foreach (var mas in items)
                    {
                        Locations = Locations + mas.LocationRefName + ", ";
                        var pOutput = new List<ExportDataObject>();
                        pOutput.AddRange(mas.ConsolidatedWastageReport);
                        StringBuilder firstPageBuilder = new StringBuilder();
                        firstPageBuilder.Append(L("Location") + " : ");
                        firstPageBuilder.Append(mas.LocationRefName);
                        firstPageBuilder.Append("-");
                        firstPageBuilder.Append(L("MaterialWastageReport"));
                        firstPageBuilder.Append(L("-"));
                        firstPageBuilder.Append(L("Consolidated"));
                        firstPageBuilder.Append("{NL}");
                        firstPageBuilder.Append(string.Format(L("FromTimeToTime"), input.StartDate.Value.ToString(dateFormat), input.EndDate.Value.ToString(dateFormat)));
                        firstPageBuilder.Append("{NL}");

                        StringBuilder lPageBuilder = new StringBuilder();
                        lPageBuilder.Append("{NL}");
                        lPageBuilder.Append(L("PrintedOn") + " :" + DateTime.Now.ToString(dateFormat));

                        eObject.FirstPage = firstPageBuilder.ToString();
                        eObject.LastPage = lPageBuilder.ToString();
                        eObject.ExportObjects = pOutput;

                        StringBuilder fileNameBuilder = new StringBuilder();
                        fileNameBuilder.Append(mas.LocationRefName);
                        fileNameBuilder.Append(L("-"));
                        fileNameBuilder.Append(L("MaterialWastageReport"));
                        fileNameBuilder.Append(L("-"));
                        fileNameBuilder.Append(L("Consolidated"));
                        fileNameBuilder.Append(L("-"));
                        fileNameBuilder.Append(!input.StartDate.Value.Equals(DateTime.MinValue)
                            ? input.StartDate.Value.ToString("dd/MM/yyyy")
                            : DateTime.Now.ToString("dd/MM/yyyy"));
                        fileNameBuilder.Append(" - ");
                        fileNameBuilder.Append(!input.EndDate.Value.Equals(DateTime.MinValue)
                            ? input.EndDate.Value.ToString("dd/MM/yyyy")
                            : DateTime.Now.ToString("dd/MM/yyyy"));

                        eObject.FileName = fileNameBuilder.ToString();
                        var exporter = new DineConnectDocExporter(eObject);
                        FileDto a = exporter.ExportFile(AppFolders.TempFileDownloadFolder);
                        returnFileList.Add(a);
                    }

                }
            }
            #endregion

            #region OverAllConsolidated
            {
                var eObject = new ExportInputObject
                {
                    ExportType = input.ExportType
                };

                eObject.Fields.Add("MaterialRefName", new FieldAttributes
                {
                    DisplayName = L("Material"),
                    Alignment = FieldAlignment.Left,
                    FieldType = ExportFiledType.String,
                });
                eObject.Fields.Add("TotalQty", new FieldAttributes
                {
                    DisplayName = L("Quantity"),
                    Alignment = FieldAlignment.Right,
                    FieldType = ExportFiledType.Decimal,
                    Format = "N" + roundDecimals,
                });
                eObject.Fields.Add("DefaultUnitName", new FieldAttributes
                {
                    DisplayName = L("UOM"),
                    Alignment = FieldAlignment.Left,
                    FieldType = ExportFiledType.String,
                });
                eObject.Fields.Add("Price", new FieldAttributes
                {
                    DisplayName = L("Price"),
                    Alignment = FieldAlignment.Right,
                    FieldType = ExportFiledType.Decimal,
                    Format = "N" + roundDecimals,
                });
                eObject.Fields.Add("TotalAmount", new FieldAttributes
                {
                    DisplayName = L("TotalCost"),
                    Alignment = FieldAlignment.Right,
                    FieldType = ExportFiledType.Decimal,
                    Format = "N" + roundDecimals,
                    TotalRequired = true
                });

                var items = dtos.OverAllWastageReport;

                if (items.Any())
                {
                    {
                        var pOutput = new List<ExportDataObject>();
                        pOutput.AddRange(items);
                        StringBuilder firstPageBuilder = new StringBuilder();
                        firstPageBuilder.Append(L("Location") + " : ");
                        firstPageBuilder.Append(Locations);
                        firstPageBuilder.Append("-");
                        firstPageBuilder.Append(L("MaterialWastageReport"));
                        firstPageBuilder.Append(L("-"));
                        firstPageBuilder.Append(L("Consolidated") + " " + L("All"));
                        firstPageBuilder.Append("{NL}");
                        firstPageBuilder.Append(string.Format(L("FromTimeToTime"), input.StartDate.Value.ToString(dateFormat), input.EndDate.Value.ToString(dateFormat)));
                        firstPageBuilder.Append("{NL}");

                        StringBuilder lPageBuilder = new StringBuilder();
                        lPageBuilder.Append("{NL}");
                        lPageBuilder.Append(L("PrintedOn") + " :" + DateTime.Now.ToString(dateFormat));

                        eObject.FirstPage = firstPageBuilder.ToString();
                        eObject.LastPage = lPageBuilder.ToString();
                        eObject.ExportObjects = pOutput;

                        StringBuilder fileNameBuilder = new StringBuilder();
                        fileNameBuilder.Append("OverAll");
                        fileNameBuilder.Append(L("-"));
                        fileNameBuilder.Append(L("MaterialWastageReport"));
                        fileNameBuilder.Append(L("-"));
                        fileNameBuilder.Append(L("Consolidated"));
                        fileNameBuilder.Append(L("-"));
                        fileNameBuilder.Append(!input.StartDate.Value.Equals(DateTime.MinValue)
                            ? input.StartDate.Value.ToString("dd/MM/yyyy")
                            : DateTime.Now.ToString("dd/MM/yyyy"));
                        fileNameBuilder.Append(" - ");
                        fileNameBuilder.Append(!input.EndDate.Value.Equals(DateTime.MinValue)
                            ? input.EndDate.Value.ToString("dd/MM/yyyy")
                            : DateTime.Now.ToString("dd/MM/yyyy"));

                        eObject.FileName = fileNameBuilder.ToString();
                        var exporter = new DineConnectDocExporter(eObject);
                        FileDto a = exporter.ExportFile(AppFolders.TempFileDownloadFolder);
                        returnFileList.Add(a);
                    }

                }
            }
            #endregion
            return returnFileList;
            //return null;
        }

        public FileDto MaterialWastageCustomReportToExport(OverAllMaterialWastageReport dtos, InputAdjustmentReport input)
        {
            input.DateRange = "From: " + input.StartDate.Value.ToString("dd-MMM-yyyy") + " To: " + input.EndDate.Value.ToString("dd-MMM-yyyy");
            string fileName = L("Material") + " " + L("Wastage") + " " + input.DateRange + " " + input.LocationInfo.Code + " " + input.LocationInfo.Name;

            return CreateExcelPackage(fileName + ".xlsx",
            excelPackage =>
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("MaterialWastage"));
                sheet.OutLineApplyStyle = true;
                int row = 1;
                AddHeaderWithMergeWithColumnCount(sheet, row, 1, 11, L("Material") + " " + L("Wastage"));
                sheet.Row(row).Height = 30;
                sheet.Cells[row, 1].Style.Font.Size = 15;
                row++;
                AddHeaderWithMergeWithColumnCountAndAlignment(sheet, row, 3, 2, L("Location") + " " + L("Code") + ":" + input.LocationInfo.Code, ExcelHorizontalAlignment.Right);
                AddHeaderWithMergeWithColumnCountAndAlignment(sheet, row, 6, 2, input.LocationInfo.Name, ExcelHorizontalAlignment.Left);
                row++;
                AddHeaderWithMergeWithColumnCount(sheet, row, 1, 11, input.DateRange);
                row++;
                AddHeaderWithMergeWithColumnCount(sheet, row, 1, 1, L("PrintedOn"));
                AddHeaderWithMergeWithColumnCount(sheet, row, 3, 1, input.PrintedTime.ToString("dd-MMM-yyyy"));
                row++;
                AddHeaderWithMergeWithColumnCount(sheet, row, 1, 1, L("PrintedBy"));
                AddHeaderWithMergeWithColumnCount(sheet, row, 3, 1, input.PreparedUserName);
                row++;

                AddHeader(sheet, row, L("Date"), L("Price"), L("AdjMode"), L("EntryType"), L("UOM"), L("Purchase") + " " + L("Qty"), L("Inventory") + " " + L("Qty"), L("Recipe") + " " + L("Qty"), L("Total") + L("Cost"), L("User"), L("Remark"), L("Approval") + L("Ref"));

                row++;

                foreach (var gp in dtos.OverAllWastageReport.GroupBy(t => t.MaterialRefName))
                {
                    int col = 1;
                    AddHeaderWithMergeWithColumnCountAndAlignment(sheet, row, 1, 3, gp.Key, ExcelHorizontalAlignment.Left);
                    row++;
                    decimal subTotalAmount = 0;
                    foreach (var det in gp.ToList())
                    {
                        AddDetail(sheet, row, col++, det.AdjustmentDate.ToString("dd-MMM-yyyy"));
                        AddDetailWithNumberFormat(sheet, row, col++, det.Price, "#0.0000");
                        AddDetail(sheet, row, col++, det.AdjustmentMode);
                        AddDetail(sheet, row, col++, det.MenuWastageMode);
                        AddDetail(sheet, row, col++, det.UnitRefName);
                        AddDetailWithNumberFormat(sheet, row, col++, det.QuantityInPurchaseUnit, "#0.0000");
                        AddDetailWithNumberFormat(sheet, row, col++, det.QuantityInStkAdjustedInventoryUnit, "#0.0000");
                        AddDetailWithNumberFormat(sheet, row, col++, det.QuantityInIssueUnit, "#0.0000");
                        AddDetailWithNumberFormat(sheet, row, col++, det.TotalAmount, "#0.0000");
                        AddDetail(sheet, row, col++, det.UserName);
                        AddDetail(sheet, row, col++, det.Remarks);
                        AddDetail(sheet, row, col++, det.TokenRefNumber);
                        row++;
                        subTotalAmount = subTotalAmount + det.TotalAmount;
                    }
                    row++;
                    AddDetail(sheet, row, 1, "Sub Total");
                    AddDetailWithNumberFormat(sheet, row, 9, subTotalAmount, "#0.0000");
                    row++;
                }
                row++;
                AddDetail(sheet, row, 7, L("GrandTotal"));
                AddDetailWithNumberFormat(sheet, row, 8, dtos.OverAllTotalAmount, "0.0000");

                for (var i = 1; i <= 25; i++)
                {
                    sheet.Column(i).AutoFit();
                }


            });
        }


        public FileDto CloseStockToExcelFile(List<MaterialLedgerDto> dtos)
        {
            return CreateExcelPackage(
                @L("CloseStock") + ".xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("CLOSESTOCK"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        "Code",
                        "Name",
                        "ClosingStock",
                        "OnHand"
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        r => r.MaterialRefId,
                        r => r.MaterialName,
                        r => r.ClBalance
                        );

                    for (var i = 1; i <= 5; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }

        public async Task<FileDto> RateViewToExcelFile(GetMaterialRateViewDtoOutput input)
        {
            List<MaterialRateViewListDto> dtos = input.MaterialRateView;

            var returnExcelFile = CreateExcelPackage(
                @L("RATEVIEW") + ".xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("RATEVIEW"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("MaterialName"),
                        L("NetQtyRecevied"),
                        L("NetAmount"),
                        L("AvgRate"),
                        L("DateRange")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        r => r.MaterialName,
                        r => r.BilledQty,
                        r => r.NetAmount,
                        r => r.AvgRate,
                        r => r.InvoiceDateRange
                        );

                    sheet.Column(1).Width = 27;
                    sheet.Column(2).Width = 14;
                    sheet.Column(3).Width = 12;
                    sheet.Column(4).Width = 11;
                    sheet.Column(5).Width = 23;

                    sheet.Row(1).Height = 36;
                    sheet.Row(1).Style.WrapText = true;
                    sheet.Row(1).Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                    sheet.Column(2).Style.Numberformat.Format = "###0.00";
                    sheet.Column(3).Style.Numberformat.Format = "###0.00";
                    sheet.Column(4).Style.Numberformat.Format = "###0.00";
                    sheet.PrinterSettings.ShowGridLines = true;
                    sheet.PrinterSettings.RepeatRows = sheet.Cells["1:2"];
                });


            if (input.ExportType == 2)
            {
                string templatePathBase = System.AppDomain.CurrentDomain.BaseDirectory;
                string templatePath = templatePathBase + @"Temp\Downloads\" + returnExcelFile.FileToken;

                //C:\DinePlan\DineConnect\DineConnect\DinePlan.DineConnect.Web\Temp\Downloads\

                #region FilePathDefinition

                var subPath = "\\TempFiles" + "\\Reports\\Excel";

                var phyPath = Path.GetFullPath(subPath);

                bool exists = Directory.Exists((phyPath));//System.IO.Server.MapPath

                if (!exists)
                {
                    System.IO.Directory.CreateDirectory(phyPath);
                }
                #endregion

                string templateFileName = templatePath;/* + "\\" + "KeppelTemplateMpi.xlsx"*/

                string timeStamp = DateTime.Now.ToFileTime().ToString(); // DateTime.
                string dbsaveFileName = @"AvgPrice_" + timeStamp;
                string excelLocation = phyPath + "\\" + dbsaveFileName + ".xlsx";
                string outputPdfLocation = templatePath + ".pdf";

                FileInfo fileSaveAs = new FileInfo(excelLocation);

                File.Copy(templatePath, excelLocation);

                await ExportWorkbookToPdf(excelLocation, outputPdfLocation);

                returnExcelFile.FileName = dbsaveFileName + ".PDF";
                returnExcelFile.FileToken = returnExcelFile.FileToken + ".pdf";
                returnExcelFile.FileType = MimeTypeNames.ApplicationPdf;
            }

            return returnExcelFile;
        }

        public FileDto SupplierRateViewToExcelFile(List<SupplierRateViewListDto> dtos)
        {
            return CreateExcelPackage(
                @L("SUPPLIERRATEVIEW") + ".xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("SUPPLIERRATEVIEW"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("MaterialName"),
                        L("Supplier"),
                        L("Rate")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        r => r.MaterialName,
                        r => r.SupplierName,
                        r => r.Rate
                        );

                    for (var i = 1; i <= 10; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }



        public FileDto ProductionUnitCostingReportExcel(ProductionUnitCostingDtoConsolidated dtos)
        {
            var allList = dtos.ProductionWiseCosting;

            return CreateExcelPackage(
            @L("ProductionUnitCosting") + ".xlsx",
            excelPackage =>
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("ProductionUnitCosting"));
                sheet.OutLineApplyStyle = true;
                int row = 1;

                AddHeader(
                    sheet,
                    @L("Material"),
                    @L("IssueQty"),
                    @L("Return") + " " + @L("Quantity"),
                    @L("Usage"),
                    @L("UOM"),
                    @L("CostPerUom"),
                    @L("Amount")
                    );

                row = 3;
                foreach (var lst in allList)
                {
                    AddHeader(sheet, row, 1, lst.LocationRefName + " - " + lst.ProductionUnitRefName);
                    row = row + 2;

                    AddObjects(
                        sheet, row, lst.MaterialUsage,
                        r => r.MaterialRefName,
                        r => r.IssueQty,
                        r => r.ReturnQty,
                        r => r.UsageQty,
                        r => r.Uom,
                        r => r.AvgRate,
                        r => Math.Round(r.UsageQty * r.AvgRate, 2, MidpointRounding.AwayFromZero)
                        );

                    row = row + lst.MaterialUsage.Count;
                    row = row + 1;

                    AddHeader(sheet, row, 1, L("Total"));
                    AddHeader(sheet, row, 7, lst.MaterialCost);
                    row = row + 2;

                }

                for (var i = 1; i <= 15; i++)
                {
                    sheet.Column(i).AutoFit();
                }
            });
        }

        public FileDto StockTakenToExcelFile(List<MaterialViewDto> dtos, string locationName, DateTime stockDate, bool multipleUomAllowed)
        {
            locationName = locationName.Replace("#", "");
            bool showAliasNameColumn = false;
            bool showSupplierList = false;

            List<MaterialViewDto> revisedDtos = new List<MaterialViewDto>();
            revisedDtos.AddRange(dtos.Where(t => t.SupplierMaterialEditDtos.Count == 0).ToList());
            foreach (var lst in dtos.Where(t=>t.SupplierMaterialEditDtos.Count>0))
            {
               
                foreach (var aliasDto in lst.SupplierMaterialEditDtos.GroupBy(t=>t.SupplierMaterialAliasName))
                {
                    MaterialViewDto newLst = new MaterialViewDto
                    {
                        MaterialTypeId = lst.MaterialTypeId,
                        MaterialPetName = lst.MaterialPetName,
                        MaterialName = lst.MaterialName,
                        MaterialGroupCategoryRefId = lst.MaterialGroupCategoryRefId,
                        MaterialGroupCategoryName = lst.MaterialGroupCategoryName,
                        Id = lst.Id,
                        UnitsLinkViewDtos = lst.UnitsLinkViewDtos,
                        SupplierList = "",
                        MaterialSupplierAliasName = "",
                        Uom = lst.Uom,
                        UnitRefId = lst.UnitRefId,
                        IssueUnitId = lst.UnitRefId
                    };

                    newLst.MaterialSupplierAliasName = aliasDto.Key;
                    if (aliasDto.Key.Length > 0)
                    {
                        showAliasNameColumn = true;
                    }
                    newLst.SupplierList = string.Join(" , ", aliasDto.Select(t => t.SupplierRefName).ToList());
                    revisedDtos.Add(newLst);
                }
            }
            dtos = revisedDtos;

            return CreateExcelPackage(
                locationName + " -" + L("CLOSESTOCK") + " -" + stockDate.ToString("dd-MMM-yy") + ".xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("CLOSESTOCK") + " " + stockDate.ToString("dd-MMM-yy"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Category"),
                        L("Code"),
                        L("Name"),
                        L("SupplierMaterialAliasName") ,
                        L("Supplier"),
                        L("OnHand") + "-1",
                        L("UOM") + "-1"
                        );
                    //AddHeaderWithMerge(sheet, 1, 4, L("UOM") + "-" + L("OnHand"));
                   
                    try
                    {
                        sheet.Protection.AllowFormatColumns = true;
                        sheet.Protection.AllowDeleteRows = true;
                        sheet.Protection.AllowAutoFilter = true;
                        sheet.Protection.AllowSelectLockedCells = true;
                        sheet.Protection.AllowFormatRows = true;
                        sheet.Protection.IsProtected = true;
                        sheet.Protection.SetPassword("dinehouse");
                        int fixedCol = 7;
                        int maxCol = fixedCol;
                        int row = 2;
                        foreach (var lst in dtos)
                        {
                            sheet.Row(row).Height = 30;
                            int col = 1;
                            AddDetail(sheet, row, col, lst.MaterialGroupCategoryName);
                            col++;
                            AddDetail(sheet, row, col, lst.Id);
                            col++;
                            AddDetail(sheet, row, col, lst.MaterialName);
                            sheet.Cells[row, col].Style.WrapText = true;
                            col++;
                            AddDetail(sheet, row, col, lst.MaterialSupplierAliasName);
                            sheet.Cells[row, col].Style.WrapText = true;
                            col++;
                            AddDetail(sheet, row, col, lst.SupplierList);
                            sheet.Cells[row, col].Style.WrapText = true;
                            col++;
                            sheet.Cells[row, col].Style.Locked = false;
                            col++;
                            AddDetail(sheet, row, col, lst.Uom);
                            if (lst.UnitsLinkViewDtos != null)
                            {
                                foreach (var ul in lst.UnitsLinkViewDtos)
                                {
                                    col++;
                                    sheet.Cells[row, col].Style.Locked = false;
                                    col++;
                                    AddDetail(sheet, row, col, ul.UnitName);
                                }
                            }
                            row++;
                            if (maxCol < col)
                                maxCol = col;
                        }

                        int totalRowCount = dtos.Count + 1;
                        #region Lock cell and Excel color setting
                        if (maxCol >= fixedCol)
                        {
                            int index = 1;
                            int coloridx = 0;

                            for (int i = fixedCol-1; i < maxCol; i = i + 2)
                            {
                                AddDetail(sheet, 1, i, L("OnHand") + "-" + index.ToString());
                                AddDetail(sheet, 1, i + 1, L("UOM") + "-" + index.ToString());
                                index++;
                                //sheet.Column(i).Style.Locked = false;
                                string startRange = Char.ConvertFromUtf32(64 + i);
                                string endRange = char.ConvertFromUtf32(64 + i + 1);
                                string rangeString = startRange.Trim() + "1:" + endRange.Trim() + totalRowCount;
                                ExcelRange rng = sheet.Cells[rangeString];
                                rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                if (coloridx == 0)
                                {
                                    ExcelBorderStyle borderstyle = ExcelBorderStyle.Double;
                                    rng.Style.Border.BorderAround(borderstyle); //  255,242,204
                                    rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 242, 204));
                                }
                                else if (coloridx == 1)
                                {
                                    ExcelBorderStyle borderstyle = ExcelBorderStyle.DashDot;
                                    rng.Style.Border.BorderAround(borderstyle); //  252,228,214
                                    rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(252, 228, 214));
                                }
                                else if (coloridx == 2)
                                {
                                    ExcelBorderStyle borderstyle = ExcelBorderStyle.DashDot;
                                    rng.Style.Border.BorderAround(borderstyle);//  226,239, 218
                                    rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(226, 239, 218));
                                }
                                else if (coloridx == 3)
                                {
                                    ExcelBorderStyle borderstyle = ExcelBorderStyle.MediumDashDot;
                                    rng.Style.Border.BorderAround(borderstyle); // 217,225,242
                                    rng.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(217, 225, 242));
                                }
                                coloridx++;
                                if (coloridx > 3)
                                    coloridx = 0;
                            }
                        }
                        for (var i = 1; i <= maxCol; i++)
                        {
                            sheet.Column(i).AutoFit();
                        }
                        sheet.Column(3).Width = 40;
                        if (showAliasNameColumn == true)
                        {
                            sheet.Column(4).Width = 40;
                            sheet.Column(5).Width = 40;
                        }
                        else
                        {
                            sheet.Column(4).Width = 5;
                            sheet.Column(5).Width = 5;
                        }
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        MethodBase m = MethodBase.GetCurrentMethod();
                        string innerExceptionMessage = ex.InnerException != null ? ex.InnerException.Message : "";
                        throw new UserFriendlyException("Method : " + m.ReflectedType.Name + " : " + m.Name + " " + ex.Message + " " + innerExceptionMessage);
                    }

                });
        }

        public FileDto ClosingStockTakenToExcelFile(List<MaterialViewDto> matDetailDtos,
                List<MaterialViewDto> matConsolidatedDtos, string locationName, DateTime stockDate, LocationWithDate input, List<ClosingStockDetailViewDto> closingCustomReportDetails)
        {

            string dateRange = "Stock Taken Date: " + input.StockTakenDate.ToString("dd-MMM-yyyy");
            string fileName = "Stock Take Listing" + " " + dateRange;//+ " " + input.LocationInfo.Code + " " + input.LocationInfo.Name;
            fileName = fileName + " " + input.LocationInfo.Code + " " + input.LocationInfo.Name;

            return CreateExcelPackage(
                fileName + ".xlsx",
                excelPackage =>
                {
                    if (input.CustomReportFlag)
                    {
                        var customSheet = excelPackage.Workbook.Worksheets.Add(L("Stock Take Listing"));
                        customSheet.OutLineApplyStyle = true;
                        int row = 1;
                        AddHeaderWithMergeWithColumnCount(customSheet, row, 1, 5, "Stock Take Listing" + " " + "(AllItems)");
                        customSheet.Row(row).Height = 30;
                        customSheet.Cells[row, 1].Style.Font.Size = 15;
                        row++;
                        AddHeaderWithMergeWithColumnCountAndAlignment(customSheet, row, 2, 1, L("Location") + " " + L("Code") + ":" + input.LocationInfo.Code + " " + input.LocationInfo.Name, ExcelHorizontalAlignment.Center);
                        row++;
                        AddHeaderWithMergeWithColumnCount(customSheet, row, 1, 5, dateRange);
                        row++;
                        AddHeader(customSheet, row, 1, L("PrintedOn"));
                        AddHeader(customSheet, row, 2, input.PrintedTime.ToString("dd-MMM-yyyy"));
                        row++;
                        AddHeader(customSheet, row, 1, L("PrintedBy"));
                        AddHeader(customSheet, row, 2, input.PreparedUserName);
                        row++;

                        AddHeader(customSheet, row, L("Category"), L("MaterialCode"), L("MaterialName"), L("Purchase") + " " + L("Qty"), L("Inventory") + " " + L("Qty"), L("Recipe") + " " + L("Qty"));

                        row++;

                        foreach (var det in closingCustomReportDetails)
                        {
                            int col = 1;
                            AddDetail(customSheet, row, col++, det.MaterialGroupCategoryRefName);
                            AddDetail(customSheet, row, col++, det.MaterialPetName);
                            AddDetail(customSheet, row, col++, det.MaterialRefName);
                            AddDetailWithNumberFormat(customSheet, row, col++, det.QuantityInPurchaseUnit, "#0.0000");
                            AddDetailWithNumberFormat(customSheet, row, col++, det.QuantityInStkAdjustedInventoryUnit, "#0.0000");
                            AddDetailWithNumberFormat(customSheet, row, col++, det.QuantityInIssueUnit, "#0.0000");
                            row++;
                        }
                        for (var i = 1; i <= 25; i++)
                        {
                            customSheet.Column(i).AutoFit();
                        }
                    }


                    var closeSheet = excelPackage.Workbook.Worksheets.Add(L("CLOSESTOCK")
                    + stockDate.ToString("dd-MMM-yy"));
                    closeSheet.OutLineApplyStyle = true;

                    AddHeader(
                        closeSheet,
                        L("Category"),
                        L("Code"),
                        L("Name"),
                        L("OnHand") + "-1",
                        L("UOM") + "-1"
                        );

                    AddObjects(
                        closeSheet, 2, matConsolidatedDtos,
                        r => r.MaterialGroupCategoryName,
                        r => r.Id,
                        r => r.MaterialName,
                        r => r.OnHand,
                        r => r.Uom
                        );

                    for (var i = 1; i <= 7; i++)
                    {
                        closeSheet.Column(i).AutoFit();
                    }

                    var consSheet = excelPackage.Workbook.Worksheets.Add(L("Consolidated") + " " + L("CLOSESTOCK")
                        + stockDate.ToString("dd-MMM-yy"));
                    consSheet.OutLineApplyStyle = true;

                    AddHeader(
                        consSheet,
                        L("Category"),
                        L("Code"),
                        L("Name"),
                        L("UOM"),
                        L("OnHand"),
                        L("ClBalanceOn") + " " + stockDate.ToString("dd-MMM-yy"),
                        L("Difference"),
                        L("Status")
                        );

                    AddObjects(
                        consSheet, 2, matConsolidatedDtos,
                        r => r.MaterialGroupCategoryName,
                        r => r.Id,
                        r => r.MaterialName,
                        r => r.Uom,
                        r => r.OnHand,
                        r => r.ClBalance,
                        r => r.Difference,
                        r => r.Status
                        );

                    for (var i = 1; i <= 7; i++)
                    {
                        consSheet.Column(i).AutoFit();
                    }

                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Detail") + " "
                        + L("CLOSESTOCK") + stockDate.ToString("dd-MMM-yy"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Category"),
                        L("Code"),
                        L("Name"),
                        L("UOM"),
                        L("OnHand")
                        );

                    AddObjects(
                        sheet, 2, matDetailDtos,
                        r => r.MaterialGroupCategoryName,
                        r => r.Id,
                        r => r.MaterialName,
                        r => r.Uom,
                        r => r.OnHand
                        );

                    for (var i = 1; i <= 7; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }



        public FileDto ClosingStockVarrianceToExcelFile(List<ClosingStockVarrianceDto> matDetailDtos,
             string locationName, DateTime stockDate, bool materialCodeExists)
        {
            return CreateExcelPackage(
               locationName + "-" + L("ManualCloseStockTaken") + " " + L("Variance") + "-" + stockDate.ToString("dd-MMM-yy") + ".xlsx",
               excelPackage =>
               {
                   var consSheet = excelPackage.Workbook.Worksheets.Add(stockDate.ToString("dd-MMM-yy") + " " + L("ManualCloseStockTaken"));
                   consSheet.OutLineApplyStyle = true;

                   AddHeader(
                   consSheet,
                   L("Code"),
                   L("Material"),
                   L("UOM"),
                   L("Opening"),
                   L("Purchase"),
                   L("TransferIn"),
                   L("PurchaseReturn"),
                   L("TransferOut"),
                   L("Adjustment"),
                   L("Consumption"),
                   L("ClBalanceOn") + " " + stockDate.ToString("dd-MMM-yy"),
                   L("StockTaken"),
                   L("Variance"),
                   L("Status")
                   );

                   AddObjects(
                   consSheet, 2, matDetailDtos,
                   r => r.MaterialPetName,
                   r => r.MaterialName,
                   r => r.Uom,
                   r => r.OpenBalance,
                   r => r.Received,
                   r => r.TransferIn,
                   r => r.SupplierReturn,
                   r => r.TransferOut,
                   r => r.ExcessReceived - r.Damaged - r.Shortage,
                   r => r.Sales + r.Issued - r.Return,
                   r => r.ClBalance,
                   r => r.CurrentStock,
                   r => r.DifferenceQuantity,
                   r => r.AdjustmentStatus
                   );

                   for (var i = 1; i <= 15; i++)
                   {
                       consSheet.Column(i).AutoFit();
                   }

                   if (materialCodeExists == false)
                   {
                       consSheet.Column(1).Width = 0;
                   }


               });
        }

        public async Task<FileDto> ExportToFilePurchaseSummary(InputInvoiceAnalysis input, InvoiceAppService appService)
        {
            var file =
                new FileDto(
                    "PurchaseSummary-" + input.StartDate.ToString("yy-MM-dd") + "-" + input.EndDate.ToString("yy-MM-dd") +
                    ".xlsx",
                    MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("PurchaseSummary"));
                sheet.OutLineApplyStyle = true;

                AddHeader(
                    sheet,
                    L("Supplier"),
                    L("InvoiceDate"),
                    L("InvoiceNumber"),
                    L("Material"),
                    L("Qty"),
                    L("Unit"),
                    L("Price"),
                    L("Amount"),
                    L("Price/Unit")
                    );

                int rowCount = 2;
                var outPut = await appService.GetPurchaseAnalysis(input);
                if (DynamicQueryable.Any(outPut.InvoiceList))
                {
                    AddObjects(
                        sheet, rowCount, outPut.InvoiceList,
                        _ => _.SupplierRefName,
                        _ => _.InvoiceDate.ToString("yyyy-MMM-dd"),
                        _ => _.InvoiceNumber,
                        _ => _.MaterialRefName,
                        _ => _.TotalQty,
                        _ => _.UnitRefName,
                        _ => _.Price,
                        _ => _.TotalAmount,
                        _ => _.PriceForDefaultUnitWithTaxUOM
                        );
                    //input.SkipCount = input.SkipCount + input.MaxResultCount;
                }

                decimal Total = outPut.InvoiceList.Sum(t => t.TotalAmount);
                rowCount = rowCount + outPut.InvoiceList.Count + 1;
                AddDetail(sheet, rowCount, 8, Total, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);

                for (var i = 1; i <= 10; i++)
                {
                    sheet.Column(i).AutoFit();
                }

                Save(excelPackage, file);
            }

            return file;
        }

        public async Task<FileDto> ExportToFilePurchaseConsolidatedSummary(InputInvoiceAnalysis input, InvoiceAppService appService)
        {
            var file =
                new FileDto(
                    "PurchaseSummary-" + input.StartDate.ToString("yy-MM-dd") + "-" + input.EndDate.ToString("yy-MM-dd") +
                    ".xlsx",
                    MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("PurchaseSummary"));
                sheet.OutLineApplyStyle = true;

                AddHeader(
                    sheet,
                    L("Material"),
                    L("Qty"),
                    L("Unit"),
                    L("Price"),
                    L("Amount")
                    );

                int rowCount = 2;
                var outPut = await appService.GetPurchaseAnalysisConsolidated(input);
                if (DynamicQueryable.Any(outPut.InvoiceList))
                {
                    AddObjects(
                        sheet, rowCount, outPut.InvoiceList,
                        _ => _.MaterialRefName,
                        _ => _.TotalQty,
                        _ => _.DefaultUnitName,
                        _ => _.Price,
                        _ => _.TotalAmount
                        );
                    //input.SkipCount = input.SkipCount + input.MaxResultCount;
                }

                decimal Total = outPut.InvoiceList.Sum(t => t.TotalAmount);
                rowCount = rowCount + outPut.InvoiceList.Count + 1;
                AddDetail(sheet, rowCount, 5, Total, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);

                for (var i = 1; i <= 10; i++)
                {
                    sheet.Column(i).AutoFit();
                }

                Save(excelPackage, file);
            }

            return file;
        }


        public List<MaterialAdjustmentStockDto> ReadFromExcelFile(string argfilename)
        {
            string fileLocation = "";

            fileLocation = argfilename == "" ? "D:\\CloseStock.xlsx" : argfilename;
            string excelConnectionString = string.Empty;
            string fileExtension = ".xlsx";

            excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation +
                                    ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
            //connection String for xls file format.
            if (fileExtension == ".xls")
            {
                excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileLocation +
                                        ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
            }
            //connection String for xlsx file format.
            else if (fileExtension == ".xlsx")
            {
                excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;OLE DB Services=-4;Data Source=" +
                                        fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
            }

            try
            {
                OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                excelConnection.Open();
                DataTable dt = new DataTable();


                dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                if (dt == null)
                {
                    return null;
                }

                String[] excelSheets = new String[dt.Rows.Count];
                int t = 0;
                //excel data saves in temp file here.
                foreach (DataRow row in dt.Rows)
                {
                    excelSheets[t] = row["TABLE_NAME"].ToString();
                    t++;
                }
                OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);
                DataSet ds = new DataSet();

                string query = string.Format("Select * from [{0}]", excelSheets[0]);
                using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                {
                    dataAdapter.Fill(ds);
                }


                List<MaterialAdjustmentStockDto> outputlist = new List<MaterialAdjustmentStockDto>();

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string errMessage;
                    string rownumber = (i + 1).ToString();

                    var matid = ds.Tables[0].Rows[i][0].ToString();
                    if (matid == null || matid.Length == 0)
                    {
                        errMessage = string.Concat(L("Row"), rownumber, L("MaterialIdNoExists"));
                        throw new UserFriendlyException(errMessage);
                    }

                    var matname = ds.Tables[0].Rows[i][1].ToString();
                    if (matname == null || matname.Length == 0)
                    {
                        errMessage = string.Concat(L("Row"), rownumber, L("MaterialNameNoExists", matname));
                        throw new UserFriendlyException(errMessage);
                    }

                    var closingstock = ds.Tables[0].Rows[i][2].ToString();
                    if (string.IsNullOrEmpty(closingstock))
                    {
                        errMessage = string.Concat(L("Row"), rownumber, matname, " ", L("ClosingStockNoExists"));
                        throw new UserFriendlyException(errMessage);
                    }

                    var currentstock = ds.Tables[0].Rows[i][3].ToString();
                    if (string.IsNullOrEmpty(currentstock))
                    {
                        errMessage = string.Concat(L("Row"), i.ToString(), matname, " ", L("CurrentStockNoExists"));
                        throw new UserFriendlyException(errMessage);
                    }

                    if (ds.Tables[0].Rows[i][3] == null)
                    {
                    }

                    decimal diffStockvalue = Math.Round(decimal.Parse(ds.Tables[0].Rows[i][3].ToString()), roundDecimals, MidpointRounding.AwayFromZero) -
                                             Math.Round(decimal.Parse(ds.Tables[0].Rows[i][2].ToString()), roundDecimals, MidpointRounding.AwayFromZero);
                    string diffStatus;
                    if (diffStockvalue < 0)
                        diffStatus = "Shortage";
                    else if (diffStockvalue == 0)
                        diffStatus = "Equal";
                    else
                        diffStatus = "Excess";

                    outputlist.Add(new MaterialAdjustmentStockDto
                    {
                        MaterialRefId = int.Parse(ds.Tables[0].Rows[i][0].ToString()),
                        MaterialName = ds.Tables[0].Rows[i][1].ToString(),
                        ClBalance = Math.Round(decimal.Parse(ds.Tables[0].Rows[i][2].ToString()), roundDecimals, MidpointRounding.AwayFromZero),
                        CurrentStock = Math.Round(decimal.Parse(ds.Tables[0].Rows[i][3].ToString()), roundDecimals, MidpointRounding.AwayFromZero),
                        DifferenceQuantity = Math.Abs(diffStockvalue),
                        AdjustmentStatus = diffStatus
                    });
                }
                return outputlist;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message + " " + ex.InnerException);
            }
        }

        public FileDto StockToExcelFile(List<MaterialLedgerDto> dtos, string location, string dateRange, decimal salesAmountWithOutTax,
            List<InterTransferReportDetailOutPut> locTransferOut, List<InterTransferReportDetailOutPut> locTrasferIn)
        {
            var fileReturn = CreateExcelPackage(location + "-" + dateRange + "-" +
                @L("Stock") + L("Cost") + ".xlsx",
                excelPackage =>
                {
                    {
                        var sheet = excelPackage.Workbook.Worksheets.Add(L("STOCK"));
                        sheet.OutLineApplyStyle = true;

                        int loopCnt = 1;
                        AddHeader(sheet, loopCnt, "#");
                        loopCnt++;
                        AddHeader(sheet, loopCnt, L("Group"));
                        loopCnt++;
                        AddHeader(sheet, loopCnt, L("Category"));
                        loopCnt++;

                        AddHeader(sheet, loopCnt, L("Material"));
                        loopCnt++;

                        AddHeader(sheet, loopCnt, L("UOM"));
                        loopCnt++;

                        AddHeader(sheet, loopCnt, L("AvgPrice"));
                        loopCnt++;

                        int mergeCnt = loopCnt;

                        AddHeaderWithMerge(sheet, 1, mergeCnt, "Opening");
                        mergeCnt++;
                        mergeCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Stock"));
                        loopCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;

                        AddHeaderWithMerge(sheet, 1, mergeCnt, L("Received"));
                        mergeCnt++;
                        mergeCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Qty"));
                        loopCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;


                        AddHeaderWithMerge(sheet, 1, mergeCnt, L("ExcessReceived"));
                        mergeCnt++;
                        mergeCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Qty"));
                        loopCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;

                        AddHeaderWithMerge(sheet, 1, mergeCnt, L("TransferIn"));
                        mergeCnt++;
                        mergeCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Qty"));
                        loopCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;

                        AddHeaderWithMerge(sheet, 1, mergeCnt, L("TransferOut"));
                        mergeCnt++;
                        mergeCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Qty"));
                        loopCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;


                        AddHeaderWithMerge(sheet, 1, mergeCnt, L("Issue"));
                        mergeCnt++;
                        mergeCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Qty"));
                        loopCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;

                        AddHeaderWithMerge(sheet, 1, mergeCnt, L("Sales"));
                        mergeCnt++;
                        mergeCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Qty"));
                        loopCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;

                        AddHeaderWithMerge(sheet, 1, mergeCnt, L("Damaged"));
                        mergeCnt++;
                        mergeCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Qty"));
                        loopCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;

                        AddHeaderWithMerge(sheet, 1, mergeCnt, L("Shortage"));
                        mergeCnt++;
                        mergeCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Qty"));
                        loopCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;

                        AddHeaderWithMerge(sheet, 1, mergeCnt, L("Return"));
                        mergeCnt++;
                        mergeCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Qty"));
                        loopCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;

                        AddHeaderWithMerge(sheet, 1, mergeCnt, L("Closing"));
                        mergeCnt++;
                        mergeCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Stock"));
                        loopCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;

                        AddHeaderWithMerge(sheet, 1, mergeCnt, L("Consumption"));
                        mergeCnt++;
                        mergeCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Qty"));
                        loopCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;

                        AddHeaderWithMerge(sheet, 1, mergeCnt, L("AvgConsumption"));
                        mergeCnt++;
                        mergeCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Qty"));
                        loopCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;


                        int TotalColumns = loopCnt;

                        int sno = 1;
                        int row = 4;
                        int col = 1;

                        decimal OverAllOpenStockCost = 0;
                        decimal OverAllReceivedCost = 0;
                        decimal OverAllExcessReceivedCost = 0;
                        decimal OverAllTransferInCost = 0;
                        decimal OverAllTransferOutCost = 0;
                        decimal OverAllIssueCost = 0;
                        decimal OverAllSalesCost = 0;
                        decimal OverAllDamageCost = 0;
                        decimal OverAllShortageCost = 0;
                        decimal OverAllReturnCost = 0;
                        decimal OverAllClBalanceCost = 0;
                        decimal OverAllAvgConsumptionCost = 0;
                        decimal OverAllTotalConsumptionCost = 0;

                        foreach (var mat in dtos)
                        {
                            AddDetail(sheet, row, col, sno, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            AddDetail(sheet, row, col, mat.MaterialGroupRefName);
                            col++;

                            AddDetail(sheet, row, col, mat.MaterialGroupCategoryRefName);
                            col++;

                            AddDetail(sheet, row, col, mat.MaterialName);
                            col++;

                            AddDetail(sheet, row, col, mat.Uom);
                            col++;

                            decimal avgPrice = Math.Round(mat.AvgPrice, roundDecimals, MidpointRounding.ToEven);

                            AddDetail(sheet, row, col, mat.AvgPrice, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            AddDetail(sheet, row, col, mat.OpenBalance, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal OpBalanceCost = Math.Round(mat.OpenBalance * avgPrice, roundDecimals);
                            OverAllOpenStockCost = OverAllOpenStockCost + OpBalanceCost;

                            AddDetail(sheet, row, col, OpBalanceCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            AddDetail(sheet, row, col, mat.Received, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal ReceivedCost = Math.Round(mat.Received * avgPrice, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllReceivedCost = OverAllReceivedCost + ReceivedCost;

                            AddDetail(sheet, row, col, ReceivedCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            AddDetail(sheet, row, col, mat.ExcessReceived, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal ExcessReceivedCost = Math.Round(mat.ExcessReceived * avgPrice, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllExcessReceivedCost = OverAllExcessReceivedCost + ExcessReceivedCost;

                            AddDetail(sheet, row, col, ExcessReceivedCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            AddDetail(sheet, row, col, mat.TransferIn, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal TransferInCost = Math.Round(mat.TransferIn * avgPrice, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllTransferInCost = OverAllTransferInCost + TransferInCost;

                            AddDetail(sheet, row, col, TransferInCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            AddDetail(sheet, row, col, mat.TransferOut, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal TransferOutCost = Math.Round(mat.TransferOut * avgPrice, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllTransferOutCost = OverAllTransferOutCost + TransferOutCost;

                            AddDetail(sheet, row, col, TransferOutCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            AddDetail(sheet, row, col, mat.Issued, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal IssueCost = Math.Round(mat.Issued * avgPrice, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllIssueCost = OverAllIssueCost + IssueCost;

                            AddDetail(sheet, row, col, IssueCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            AddDetail(sheet, row, col, mat.Sales, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal SalesCost = Math.Round(mat.Sales * avgPrice, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllSalesCost = OverAllIssueCost + SalesCost;

                            AddDetail(sheet, row, col, IssueCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            AddDetail(sheet, row, col, mat.Damaged, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal DamageCost = Math.Round(mat.Damaged * avgPrice, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllDamageCost = OverAllDamageCost + DamageCost;

                            AddDetail(sheet, row, col, DamageCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            AddDetail(sheet, row, col, mat.Shortage, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal ShortageCost = Math.Round(mat.Shortage * avgPrice, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllShortageCost = OverAllShortageCost + ShortageCost;

                            AddDetail(sheet, row, col, ShortageCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            AddDetail(sheet, row, col, mat.Return, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal ReturnCost = Math.Round(mat.Return * avgPrice, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllReturnCost = OverAllReturnCost + ReturnCost;

                            AddDetail(sheet, row, col, ReturnCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            AddDetail(sheet, row, col, mat.ClBalance, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal ClBalanceCost = Math.Round(mat.ClBalance * avgPrice, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllClBalanceCost = OverAllClBalanceCost + ClBalanceCost;

                            AddDetail(sheet, row, col, ClBalanceCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            AddDetail(sheet, row, col, mat.TotalConsumption, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal TotalConsumptionCost = Math.Round(mat.TotalConsumption * avgPrice, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllTotalConsumptionCost = OverAllTotalConsumptionCost + TotalConsumptionCost;

                            AddDetail(sheet, row, col, TotalConsumptionCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            AddDetail(sheet, row, col, mat.AvgConsumption, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal AvgConsumptionCost = Math.Round(mat.AvgConsumption * avgPrice, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllAvgConsumptionCost = OverAllAvgConsumptionCost + AvgConsumptionCost;

                            AddDetail(sheet, row, col, AvgConsumptionCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            col = 1;
                            row++;
                            sno++;
                        }
                        row++;


                        AddHeader(sheet, row, 2, L("Opening"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllOpenStockCost);
                        row++;

                        AddHeader(sheet, row, 2, L("Received"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllReceivedCost);
                        row++;

                        AddHeader(sheet, row, 2, L("ExcessReceived"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllExcessReceivedCost);
                        row++;

                        AddHeader(sheet, row, 2, L("TransferIn"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllTransferInCost);
                        row++;

                        AddHeader(sheet, row, 2, L("TransferOut"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllTransferOutCost);
                        row++;

                        AddHeader(sheet, row, 2, L("Issue"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllIssueCost);
                        row++;

                        AddHeader(sheet, row, 2, L("Sales"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllSalesCost);
                        row++;

                        AddHeader(sheet, row, 2, L("Damaged"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllDamageCost);
                        row++;

                        AddHeader(sheet, row, 2, L("Shortage"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllShortageCost);
                        row++;

                        AddHeader(sheet, row, 2, L("Return"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllReturnCost);
                        row++;

                        AddHeader(sheet, row, 2, L("ClBalance"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllClBalanceCost);
                        row++;

                        AddHeader(sheet, row, 2, L("Consumption"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllTotalConsumptionCost);
                        row++;

                        AddHeader(sheet, row, 2, L("AvgConsumption"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllAvgConsumptionCost);
                        row++;

                        AddHeader(sheet, row, 2, L("Sales"));
                        AddHeaderWithMerge(sheet, row, 4, salesAmountWithOutTax);
                        row++;

                        AddHeader(sheet, row, 2, L("Consumption") + "%");
                        if (salesAmountWithOutTax > 0)
                        {
                            AddHeaderWithMerge(sheet, row, 4, OverAllTotalConsumptionCost / salesAmountWithOutTax * 100);
                        }
                        else
                        {
                            AddHeaderWithMerge(sheet, row, 4, "N/A");
                        }

                        row++;


                        for (var i = 1; i <= TotalColumns; i++)
                        {
                            sheet.Column(i).AutoFit();
                        }
                    }

                    {
                        var tosheet = excelPackage.Workbook.Worksheets.Add(L("TransferIn"));
                        tosheet.OutLineApplyStyle = true;

                        List<int> LocationRefIds = locTrasferIn.OrderBy(t => t.LocationRefId).Select(t => t.LocationRefId).Distinct().ToList();
                        var locs = _locationRepo.GetAllList(t => LocationRefIds.Contains(t.Id));
                        int row = 1;
                        int col = 1;

                        AddHeader(tosheet, row, col++, L("#"));
                        AddHeader(tosheet, row, col++, L("Material"));
                        foreach (var lst in locs)
                        {
                            AddHeader(tosheet, row, col++, lst.Code);
                        }
                        AddHeader(tosheet, row, col++, L("Total"));

                        var sno = 1;
                        row++;
                        foreach (var mat in dtos)
                        {
                            col = 1;
                            AddDetail(tosheet, row, col++, sno++, ExcelHorizontalAlignment.Right);
                            //col++;
                            AddDetail(tosheet, row, col++, mat.MaterialName);
                            //col++;
                            foreach (var lst in locs)
                            {
                                var toQty = locTrasferIn.Where(t => t.LocationRefId == lst.Id && t.MaterialRefId == mat.MaterialRefId).Sum(t => t.TotalQty);
                                AddDetail(tosheet, row, col++, toQty, ExcelHorizontalAlignment.Right);
                            }
                            var totalQty = locTrasferIn.Where(t => t.MaterialRefId == mat.MaterialRefId).Sum(t => t.TotalQty);
                            AddDetail(tosheet, row, col++, totalQty, ExcelHorizontalAlignment.Right);
                            row++;
                        }

                        for (var i = 1; i <= col; i++)
                        {
                            tosheet.Column(i).AutoFit();
                        }
                    }

                    {
                        var tosheet = excelPackage.Workbook.Worksheets.Add(L("TransferOut"));
                        tosheet.OutLineApplyStyle = true;

                        List<int> LocationRefIds = locTransferOut.OrderBy(t => t.LocationRefId).Select(t => t.LocationRefId).Distinct().ToList();
                        var locs = _locationRepo.GetAllList(t => LocationRefIds.Contains(t.Id));
                        int row = 1;
                        int col = 1;

                        AddHeader(tosheet, row, col++, L("#"));
                        AddHeader(tosheet, row, col++, L("Material"));
                        foreach (var lst in locs)
                        {
                            AddHeader(tosheet, row, col++, lst.Code);
                        }
                        AddHeader(tosheet, row, col++, L("Total"));

                        var sno = 1;
                        row++;
                        foreach (var mat in dtos)
                        {
                            col = 1;
                            AddDetail(tosheet, row, col++, sno++, ExcelHorizontalAlignment.Right);
                            //col++;
                            AddDetail(tosheet, row, col++, mat.MaterialName);
                            //col++;
                            foreach (var lst in locs)
                            {
                                var toQty = locTransferOut.Where(t => t.LocationRefId == lst.Id && t.MaterialRefId == mat.MaterialRefId).Sum(t => t.TotalQty);
                                AddDetail(tosheet, row, col++, toQty, ExcelHorizontalAlignment.Right);
                            }
                            var totalQty = locTransferOut.Where(t => t.MaterialRefId == mat.MaterialRefId).Sum(t => t.TotalQty);
                            AddDetail(tosheet, row, col++, totalQty, ExcelHorizontalAlignment.Right);
                            row++;
                        }

                        for (var i = 1; i <= col; i++)
                        {
                            tosheet.Column(i).AutoFit();
                        }
                    }

                });

            return fileReturn;
        }

        public FileDto MenuCostToExcelFile(MenuItemCostingDtoConsolidated input)
        {
            var masterDtos = input.RecipeWiseCosting;
            var fileReturn = CreateExcelPackage(
                @L("Menu") + L("Cost") + ".xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("MenuCost"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        @L("Department"),
                        @L("Menu"),
                        L("UnitPrice"),
                        @L("SoldQty"),
                        @L("Sales"),
                        @L("UnitCost"),
                        @L("TotalCost"),
                        @L("Profit"),
                        @L("Margin") + "%"
                        );

                    AddObjects(
                        sheet, 2, masterDtos,
                        r => r.DepartmentRefName,
                        r => r.MenuItemRefName,
                        r => r.UnitPrice,
                        r => r.QtySold,
                        r => r.SalesAmount,
                        r => r.CostPerUnit,
                        r => r.TotalCost,
                        r => r.Profit,
                        r => r.ProfitMargin
                        );

                    int rowCount = 2 + masterDtos.Count + 1;
                    AddDetail(sheet, rowCount, 5, input.TotalSales, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                    AddDetail(sheet, rowCount, 7, input.TotalCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                    AddDetail(sheet, rowCount, 8, input.TotalProfit, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                    AddDetail(sheet, rowCount, 9, input.TotalProfitMargin, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);


                    for (var i = 1; i <= 10; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }

                    var detailSheet = excelPackage.Workbook.Worksheets.Add(L("MenuCost") + L("Detail"));
                    detailSheet.OutLineApplyStyle = true;

                    rowCount = 1;

                    foreach (var lst in masterDtos)
                    {
                        AddHeader(detailSheet, rowCount, 1, lst.DepartmentRefName + " " + lst.MenuItemRefName);
                        rowCount++;
                        AddHeader(detailSheet, rowCount, 1, L("SoldQty"));
                        AddDetail(detailSheet, rowCount, 2, lst.QtySold, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                        rowCount++;
                        AddHeader(detailSheet, rowCount, 1, L("Sales"));
                        AddDetail(detailSheet, rowCount, 2, lst.SalesAmount, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                        rowCount++;
                        AddHeader(detailSheet, rowCount, 1, L("TotalCost"));
                        AddDetail(detailSheet, rowCount, 2, lst.TotalCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                        rowCount++;
                        AddHeader(detailSheet, rowCount, 1, L("Profit"));
                        AddDetail(detailSheet, rowCount, 2, lst.Profit, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                        rowCount++;
                        AddHeader(detailSheet, rowCount, 1, L("Profit") + " %");
                        AddDetail(detailSheet, rowCount, 2, lst.ProfitMargin, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                        rowCount++;
                        if (lst.TotalCost == 0)
                        {
                            rowCount++;
                            rowCount++;
                            continue;
                        }
                        AddHeader(
                            detailSheet,
                            rowCount,
                            @L("Material"),
                            @L("QuantityPerPortion"),
                            @L("UnitCost"),
                            @L("CostPerUom"),
                            @L("TotalUsage"),
                            @L("LineTotal"),
                            @L("FoodCostPercentage")
                            );
                        rowCount++;
                        AddObjects(
                            detailSheet, rowCount, lst.StandardMaterialWiseValueAndPercentage,
                            r => r.MaterialRefName,
                            r => r.UsagePerUnit,
                            r => r.AvgRate,
                            r => Math.Round(r.AvgRate * r.UsagePerUnit, roundDecimals, MidpointRounding.AwayFromZero),
                            r => r.UsageQty,
                            r => r.NetAmount,
                            r => Math.Round(r.NetAmount / lst.TotalCost * 100, roundDecimals, MidpointRounding.AwayFromZero)
                            );

                        rowCount = rowCount + lst.StandardMaterialWiseValueAndPercentage.Count;
                        rowCount++;

                        AddDetail(detailSheet, rowCount, 4, lst.CostPerUnit, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                        AddDetail(detailSheet, rowCount, 6, lst.TotalCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                        rowCount = rowCount + 3;
                    }

                    for (var i = 1; i <= 10; i++)
                    {
                        detailSheet.Column(i).AutoFit();
                    }
                });

            return fileReturn;
        }

        public FileDto CostOfSalesToExcelFile(CostOfSalesReport input)
        {
            var masterDtos = input.CostOfSales[input.indexId];

            string location = "";
            string fileName = "";
            foreach (var loc in masterDtos.Locations)
            {
                location = location + loc.Code + " - " + loc.Name + " ,";
                fileName = fileName + loc.Name + " ";
            }
            location = location.Remove(location.Length - 1);
            if (fileName.Length > 32)
                fileName = fileName.PadLeft(32);

            var standardDtos = masterDtos.MaterialWiseValueAndPercentage;
            var actualDtos = masterDtos.ActualMaterialWiseValueAndPercentage;
            var standardVsActualDtos = masterDtos.StandardVsActual;
            var standardCategoryWiseCost = masterDtos.StandardCategoryWiseCost;

            DateTime FromDate = masterDtos.FromDate;
            DateTime ToDate = masterDtos.ToDate;

            var fileReturn = CreateExcelPackage(
                fileName + " " + @L("CostOfSales") + ".xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Standard") + " " + L("Category"));
                    int row = 1;
                    AddHeaderWithMergeWithColumnCount(sheet, row, 1, 2, L("Standard") + " " + L("Category"));
                    row++;
                    AddHeaderWithMergeWithColumnCount(sheet, row, 1, 2, location);
                    row++;
                    AddHeaderWithMergeWithColumnCount(sheet, row, 1, 2, L("FromTimeToTime", FromDate.ToString("dd-MMM-yyyy"), ToDate.ToString("dd-MMM-yyyy")));
                    row++;
                    AddDetail(sheet, row, 1, L("PrintedOn"));
                    AddDetail(sheet, row, 2, DateTime.Today.ToString("dd-MMM-yyyy"));
                    row++;
                    AddDetail(sheet, row, 1, L("PrintedBy"));
                    AddDetail(sheet, row, 2, masterDtos.PreparedUserName);
                    row++;
                    row++;
                    AddHeader(sheet, row, 1, L("CategoryName"));
                    AddHeader(sheet, row, 2, L("Usage") + " " + L("Cost"));
                    AddHeader(sheet, row, 3, "Cost Of Good Sold %");
                    row++;
                    row++;

                    foreach(var lst in standardCategoryWiseCost)
                    {
                        AddHeader(sheet, row, 1, lst.MaterialGroupCategoryRefName);
                        AddHeader(sheet, row, 2, lst.NetAmount.ToString("####0.0000"));
                        sheet.Cells[row, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        AddHeader(sheet, row, 3, lst.CostPercentage.ToString("####0.00"));
                        sheet.Cells[row, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        row++;
                    }

                    var subtotalValue = standardCategoryWiseCost.Sum(t => t.NetAmount);
                    var subtotalPercent = standardCategoryWiseCost.Sum(t => t.CostPercentage);
                    row++;
                    AddHeader(sheet, row, 1, L("SubTotal"));
                    AddHeader(sheet, row, 2, subtotalValue.ToString("####0.0000"));
                    sheet.Cells[row, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    AddHeader(sheet, row, 3, subtotalPercent.ToString("####0.00"));
                    sheet.Cells[row, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    row++;
                    row++;
                    AddHeader(sheet, row, 1, L("NetSales"));
                    AddHeader(sheet, row, 2, masterDtos.SalesValueWithoutTax);
                    sheet.Cells[row, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    row++;
                    row++;
                    var mappingRemarks = masterDtos.MappingRemarks;
                    var pricingRemarks = masterDtos.PricingRemarks;


                    AddHeader(sheet, row, 1, L("Gross") + " " + L("Profit") + " %");
                    if (mappingRemarks.IsNullOrEmpty() && mappingRemarks.IsNullOrEmpty())
                    {
                        AddHeader(sheet, row, 2, (100 - subtotalPercent).ToString("##0.00"));
                        sheet.Cells[row, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    }
                    else
                    {
                        AddHeader(sheet, row, 2, "N/A");
                        AddHeader(sheet, row, 3, "Check Remarks");
                    }

                    row++;
                    
                    if (mappingRemarks.Length>0)
                    {
                        row++;
                        row++;
                        mappingRemarks = mappingRemarks.Replace("<BR>",  "~");
                        mappingRemarks = mappingRemarks.Replace("<br>", "~");
                        mappingRemarks = mappingRemarks.Replace('~', Convert.ToChar(10));
                        AddHeaderWithMergeWithColumnCount(sheet, row, 1, 2, mappingRemarks);
                        sheet.Cells[row, 1].Style.WrapText = true;
                        sheet.Row(row).Height = 100;
                    }
                    
                    if (pricingRemarks.Length > 0)
                    {
                        row++;
                        row++;
                        pricingRemarks = pricingRemarks.Replace("<BR>", "~");
                        pricingRemarks = pricingRemarks.Replace("<br>", "~");
                        pricingRemarks = pricingRemarks.Replace('~', Convert.ToChar(10));
                        AddHeaderWithMergeWithColumnCount(sheet, row, 1, 2, pricingRemarks);
                        sheet.Cells[row, 1].Style.WrapText = true;
                        sheet.Row(row).Height = 150;                        
                    }

                    for (var i = 1; i <= 5; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }


                    var consSheet = excelPackage.Workbook.Worksheets.Add(L("Consolidated"));
                    consSheet.OutLineApplyStyle = true;

                    int rowCount = 1;
                    AddHeader(consSheet, rowCount, 1, L("Location(s)"));
                    AddHeaderWithMerge(consSheet, rowCount, 2, location);
                    rowCount++;
                    rowCount++;
                    AddHeader(consSheet, rowCount, 1, L("Description"));
                    AddHeader(consSheet, rowCount, 2, L("Standard"));
                    AddHeader(consSheet, rowCount, 3, L("Actual"));
                    rowCount++;

                    AddHeader(consSheet, rowCount, 1, L("TotalSalesWithoutTax"));
                    AddHeaderWithMerge(consSheet, rowCount, 2, masterDtos.SalesValueWithoutTax);

                    rowCount++;
                    AddHeader(consSheet, rowCount, 1, L("FoodCost"));
                    AddHeader(consSheet, rowCount, 2, masterDtos.FoodCostValue);
                    AddHeader(consSheet, rowCount, 3, masterDtos.ActualFoodCostValue);

                    rowCount++;
                    AddHeader(consSheet, rowCount, 1, L("FoodCostPercentage"));
                    AddHeader(consSheet, rowCount, 2, masterDtos.FoodCostPercentage);
                    AddHeader(consSheet, rowCount, 3, masterDtos.ActualFoodCostPercentage);

                    rowCount++;
                    rowCount++;
                    rowCount++;
                    AddHeader(consSheet, rowCount, 1, @L("Opening") + " " + @L("Value"));
                    AddHeaderWithMerge(consSheet, rowCount, 2, masterDtos.OpeningCostValue);

                    rowCount++;
                    AddHeader(consSheet, rowCount, 1, @L("Receipt") + " " + @L("Value"));
                    AddHeaderWithMerge(consSheet, rowCount, 2, masterDtos.OverAllReceivedValue);

                    rowCount++;
                    AddHeader(consSheet, rowCount, 1, @L("Issue") + " " + @L("Value"));
                    AddHeaderWithMerge(consSheet, rowCount, 2, masterDtos.OverAllIssueValue);

                    rowCount++;
                    AddHeader(consSheet, rowCount, 1, @L("Closing") + " " + @L("Value"));
                    AddHeaderWithMerge(consSheet, rowCount, 2, masterDtos.ClosingValue);

                    for (var i = 1; i <= 10; i++)
                    {
                        consSheet.Column(i).AutoFit();
                    }


                    var standardSheet = excelPackage.Workbook.Worksheets.Add(L("StandardUsage"));
                    standardSheet.OutLineApplyStyle = true;

                    AddHeader(
                        standardSheet,
                        @L("MaterialName"),
                        @L("StandaradUsageQty"),
                        @L("UOM"),
                        @L("AvgRate"),
                        @L("Amount"),
                        @L("Percentage"),
                        @L("DateRange")
                        );

                    AddObjects(
                        standardSheet, 2, standardDtos,
                        r => r.MaterialRefName,
                        r => r.UsageQty,
                        r => r.Uom,
                        r => r.AvgRate,
                        r => r.NetAmount,
                        r => r.CostPercentage,
                        r => r.InvoiceDateRange
                        );

                    rowCount = 2 + standardDtos.Count + 1;
                    AddDetail(standardSheet, rowCount, 5, standardDtos.Sum(t => t.NetAmount), OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);

                    for (var i = 1; i <= 10; i++)
                    {
                        standardSheet.Column(i).AutoFit();
                    }


                    var actualSheet = excelPackage.Workbook.Worksheets.Add(L("ActualUsage"));
                    actualSheet.OutLineApplyStyle = true;

                    AddHeader(
                        actualSheet,
                        @L("MaterialName"),
                        @L("StandaradUsageQty"),
                        @L("UOM"),
                        @L("AvgRate"),
                        @L("Amount"),
                        @L("Percentage"),
                        @L("DateRange")
                        );

                    AddObjects(
                        actualSheet, 2, actualDtos,
                        r => r.MaterialRefName,
                        r => r.UsageQty,
                        r => r.Uom,
                        r => r.AvgRate,
                        r => r.NetAmount,
                        r => r.CostPercentage,
                        r => r.InvoiceDateRange
                        );

                    rowCount = 2 + actualDtos.Count + 1;
                    AddDetail(actualSheet, rowCount, 5, actualDtos.Sum(t => t.NetAmount), OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);

                    for (var i = 1; i <= 10; i++)
                    {
                        actualSheet.Column(i).AutoFit();
                    }

                    var standardVsActualSheet = excelPackage.Workbook.Worksheets.Add(L("StandardVsActual") + L("Detail"));
                    standardVsActualSheet.OutLineApplyStyle = true;


                    AddHeader(
                        standardVsActualSheet,
                        @L("MaterialName"),
                        @L("UOM"),
                        @L("AvgRate"),
                        @L("StandardUsageQty"),
                        @L("StandardCost"),
                        @L("StandardPercentage"),
                        @L("ActualUsageQty"),
                        @L("ActualCost"),
                        @L("ActualPercentage"),
                        @L("StdVsActQty"),
                        @L("StdVsActCost")
                        );

                    AddObjects(
                        standardVsActualSheet, 2, standardVsActualDtos,
                        r => r.MaterialRefName,
                        r => r.Uom,
                        r => r.AvgRate,
                        r => r.StandardQty,
                        r => r.StandardAmount,
                        r => r.StandardCostPercentage,
                        r => r.ActualQty,
                        r => r.ActualAmount,
                        r => r.ActualCostPercentage,
                        r => r.QuantityDifference,
                        r => r.CostDifference
                        );

                    rowCount = 2 + standardVsActualDtos.Count + 1;
                    AddDetail(standardVsActualSheet, rowCount, 5, standardVsActualDtos.Sum(t => t.StandardAmount), OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                    AddDetail(standardVsActualSheet, rowCount, 8, standardVsActualDtos.Sum(t => t.ActualAmount), OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);

                    for (var i = 1; i <= 15; i++)
                    {
                        standardVsActualSheet.Column(i).AutoFit();
                    }

                });

            return fileReturn;
        }

        public async Task<FileDto> ProductAnalysisToExcelFile(GetProductAnalysisViewDtoOutput input)
        {
            var masterDtos = input.ProductAnalysis;

            string location = "";
            string fileName = "";
            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
            if (loc == null)
            {
                throw new UserFriendlyException(L("Location") + " " + L("Id") + input.LocationRefId + " " + L("DoesNotExist"));
            }
            location = loc.Name;
            fileName = location;
            if (fileName.Length > 32)
                fileName = fileName.PadLeft(32);

            var fileReturn = CreateExcelPackage(
                fileName + " " + @L("ProductAnalysis") + input.StartDate.ToString("ddMMMyyyy") + "-" + input.EndDate.ToString("ddMMMyyyy") + ".xlsx",
                excelPackage =>
                {
                    var standardSheet = excelPackage.Workbook.Worksheets.Add(L("ProductAnalysis"));
                    standardSheet.OutLineApplyStyle = true;

                    int rowCount;
                    AddHeader(
                        standardSheet,
                        @L("MaterialName"),
                        @L("Qty"),
                        @L("UOM"),
                        @L("AvgPrice"),
                        @L("Amount"),
                        @L("MinPrice"),
                        @L("MaxPrice"),
                        @L("OpenBalance"),
                        @L("Receipt"),
                        @L("Issue"),
                        @L("ClBalance")
                        );

                    AddObjects(
                        standardSheet, 2, masterDtos,
                        r => r.MaterialName,
                        r => r.TotalQty,
                        r => r.Uom,
                        r => r.AvgPrice,
                        r => r.TotalAmount,
                        r => r.MinPrice,
                        r => r.MaxPrice,
                        r => r.OpenBalance,
                        r => r.Receipt,
                        r => r.Issue,
                        r => r.ClBalance
                        );

                    //rowCount = 2 + masterDtos.Count + 1;
                    //AddDetail(standardSheet, rowCount, 5, standardDtos.Sum(t => t.NetAmount), OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);

                    for (var i = 1; i <= 13; i++)
                    {
                        standardSheet.Column(i).AutoFit();
                    }

                });

            return fileReturn;
        }

        public FileDto ConsolidatedLocationWiseClosingStockVarrianceToExcelFile(List<LocationListDto> locations, List<Material> materials,
            List<ClosingStockVarrianceDto> matDetailDtos, DateTime stockDate, bool materialCodeExists, List<Unit> rsUnits)
        {
            //var rsUnits = await _unitRepo.GetAllListAsync();

            return CreateExcelPackage(
                L("Consolidated") + "-" + L("ManualCloseStockTaken") + " " + L("Variance") + "-" + stockDate.ToString("dd-MMM-yy") + ".xlsx",
               excelPackage =>
               {
                   {
                       var consSheet = excelPackage.Workbook.Worksheets.Add(L("Consolidated") + " " + L("Quantity"));
                       consSheet.OutLineApplyStyle = true;

                       AddHeader(
                       consSheet,
                       L("#"),
                       L("Code"),
                       L("Material"),
                       L("UOM")
                       );

                       int row = 1;
                       int col = 5;
                       foreach (var loc in locations)
                       {
                           AddHeader(consSheet, row, col++, loc.Code);
                       }
                       AddDetail(consSheet, row, col++, "Total");

                       row++;
                       col = 1;
                       int sno = 1;

                       foreach (var mat in materials)
                       {
                           col = 1;
                           AddDetail(consSheet, row, col++, sno.ToString());
                           sno++;
                           AddDetail(consSheet, row, col++, mat.MaterialPetName);
                           AddDetail(consSheet, row, col++, mat.MaterialName);
                           var unit = rsUnits.FirstOrDefault(t => t.Id == mat.DefaultUnitId);
                           AddDetail(consSheet, row, col++, unit.Name);
                           foreach (var location in locations)
                           {
                               var exists = matDetailDtos.FirstOrDefault(t => t.MaterialRefId == mat.Id && t.LocationRefId == location.Id);
                               if (exists == null)
                                   AddDetail(consSheet, row, col++, "");
                               else
                               {
                                   AddDetail(consSheet, row, col++, exists.CurrentStock);
                               }
                           }
                           var totalDiff = matDetailDtos.Where(t => t.MaterialRefId == mat.Id).Sum(t => t.CurrentStock);
                           AddDetail(consSheet, row, col++, totalDiff);
                           row++;
                       }

                       var totalColumns = locations.Count + 6;
                       for (var i = 1; i <= totalColumns; i++)
                       {
                           consSheet.Column(i).AutoFit();
                       }

                       if (materialCodeExists == false)
                       {
                           consSheet.Column(2).Width = 0;
                       }
                   }
                   {
                       var consSheet = excelPackage.Workbook.Worksheets.Add(L("Consolidated") + L("Diff"));
                       consSheet.OutLineApplyStyle = true;

                       AddHeader(
                       consSheet,
                       L("#"),
                       L("Code"),
                       L("Material"),
                       L("UOM")
                       );

                       int row = 1;
                       int col = 5;
                       foreach (var loc in locations)
                       {
                           AddHeader(consSheet, row, col++, loc.Code);
                       }
                       AddDetail(consSheet, row, col++, "Total");

                       row++;
                       col = 1;
                       int sno = 1;

                       foreach (var mat in materials)
                       {
                           col = 1;
                           AddDetail(consSheet, row, col++, sno.ToString());
                           sno++;
                           AddDetail(consSheet, row, col++, mat.MaterialPetName);
                           AddDetail(consSheet, row, col++, mat.MaterialName);
                           var unit = rsUnits.FirstOrDefault(t => t.Id == mat.DefaultUnitId);
                           AddDetail(consSheet, row, col++, unit.Name);
                           foreach (var location in locations)
                           {
                               var exists = matDetailDtos.FirstOrDefault(t => t.MaterialRefId == mat.Id && t.LocationRefId == location.Id);
                               if (exists == null)
                                   AddDetail(consSheet, row, col++, "");
                               else
                               {
                                   AddDetail(consSheet, row, col++, exists.ExcessShortageQty);
                               }
                           }
                           var totalDiff = matDetailDtos.Where(t => t.MaterialRefId == mat.Id).Sum(t => t.ExcessShortageQty);
                           AddDetail(consSheet, row, col++, totalDiff);
                           row++;
                       }

                       var totalColumns = locations.Count + 6;
                       for (var i = 1; i <= totalColumns; i++)
                       {
                           consSheet.Column(i).AutoFit();
                       }

                       if (materialCodeExists == false)
                       {
                           consSheet.Column(2).Width = 0;
                       }
                   }


               });
        }

        public FileDto StockToExcelFile_MergedIntoSingleSheet(List<MaterialLedgerDto> dtos, string location, string dateRange, decimal salesAmountWithOutTax,
            List<InterTransferReportDetailOutPut> locTransferOut, List<InterTransferReportDetailOutPut> locTrasferIn)
        {
            var fileReturn = CreateExcelPackage(location + "-" + dateRange + "-" +
                @L("Stock") + L("Cost") + ".xlsx",
                excelPackage =>
                {
                    {
                        var sheet = excelPackage.Workbook.Worksheets.Add(L("STOCK"));
                        sheet.OutLineApplyStyle = true;

                        int loopCnt = 1;
                        AddHeader(sheet, loopCnt, "#");
                        loopCnt++;

                        AddHeader(sheet, loopCnt, "Group");
                        loopCnt++;

                        AddHeader(sheet, loopCnt, "Category");
                        loopCnt++;

                        AddHeader(sheet, loopCnt, L("Material"));
                        loopCnt++;

                        AddHeader(sheet, loopCnt, L("UOM"));
                        loopCnt++;

                        AddHeader(sheet, loopCnt, L("AvgPrice"));
                        loopCnt++;

                        int mergeCnt = loopCnt;

                        AddHeaderWithMerge(sheet, 1, mergeCnt, "Opening");
                        mergeCnt++;
                        mergeCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Stock"));
                        loopCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;

                        AddHeaderWithMerge(sheet, 1, mergeCnt, L("Received"));
                        mergeCnt++;
                        mergeCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Qty"));
                        loopCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;


                        AddHeaderWithMerge(sheet, 1, mergeCnt, L("ExcessReceived"));
                        mergeCnt++;
                        mergeCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Qty"));
                        loopCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;


                        List<int> LocationRefIds = locTrasferIn.OrderBy(t => t.LocationRefId).Select(t => t.LocationRefId).Distinct().ToList();
                        var locsIn = _locationRepo.GetAllList(t => LocationRefIds.Contains(t.Id));

                        // Transfer Out
                        foreach (var lst in locsIn)
                        {
                            AddHeader(sheet, 2, loopCnt++, lst.Code);
                        }


                        AddHeaderWithMergeWithColumnCount(sheet, 1, mergeCnt, locsIn.Count + 1, L("TransferIn"));
                        mergeCnt = mergeCnt + locsIn.Count;
                        mergeCnt++;
                        mergeCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Qty"));
                        loopCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;

                        LocationRefIds = locTransferOut.OrderBy(t => t.LocationRefId).Select(t => t.LocationRefId).Distinct().ToList();
                        var locsOut = _locationRepo.GetAllList(t => LocationRefIds.Contains(t.Id));

                        // Transfer Out
                        foreach (var lst in locsOut)
                        {
                            AddHeader(sheet, 2, loopCnt++, lst.Code);
                        }


                        AddHeaderWithMergeWithColumnCount(sheet, 1, mergeCnt, locsOut.Count + 1, L("TransferOut"));
                        mergeCnt = mergeCnt + locsOut.Count;
                        mergeCnt++;
                        mergeCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Qty"));
                        loopCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;


                        AddHeaderWithMerge(sheet, 1, mergeCnt, L("Issue"));
                        mergeCnt++;
                        mergeCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Qty"));
                        loopCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;

                        AddHeaderWithMerge(sheet, 1, mergeCnt, L("Sales"));
                        mergeCnt++;
                        mergeCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Qty"));
                        loopCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;

                        AddHeaderWithMerge(sheet, 1, mergeCnt, L("Damaged"));
                        mergeCnt++;
                        mergeCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Qty"));
                        loopCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;

                        AddHeaderWithMerge(sheet, 1, mergeCnt, L("Shortage"));
                        mergeCnt++;
                        mergeCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Qty"));
                        loopCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;

                        AddHeaderWithMerge(sheet, 1, mergeCnt, L("Return"));
                        mergeCnt++;
                        mergeCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Qty"));
                        loopCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;

                        AddHeaderWithMerge(sheet, 1, mergeCnt, L("Closing"));
                        mergeCnt++;
                        mergeCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Stock"));
                        loopCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;

                        AddHeaderWithMerge(sheet, 1, mergeCnt, L("Consumption"));
                        mergeCnt++;
                        mergeCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Qty"));
                        loopCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;

                        AddHeaderWithMerge(sheet, 1, mergeCnt, L("AvgConsumption"));
                        mergeCnt++;
                        mergeCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Qty"));
                        loopCnt++;

                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;

                        int TotalColumns = loopCnt;

                        int sno = 1;
                        int row = 4;
                        int col = 1;

                        decimal OverAllOpenStockCost = 0;
                        decimal OverAllReceivedCost = 0;
                        decimal OverAllExcessReceivedCost = 0;
                        decimal OverAllTransferInCost = 0;
                        decimal OverAllTransferOutCost = 0;
                        decimal OverAllIssueCost = 0;
                        decimal OverAllSalesCost = 0;
                        decimal OverAllDamageCost = 0;
                        decimal OverAllShortageCost = 0;
                        decimal OverAllReturnCost = 0;
                        decimal OverAllClBalanceCost = 0;
                        decimal OverAllAvgConsumptionCost = 0;
                        decimal OverAllTotalConsumptionCost = 0;

                        foreach (var mat in dtos)
                        {
                            AddDetail(sheet, row, col, sno, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            AddDetail(sheet, row, col, mat.MaterialGroupRefName);
                            col++;

                            AddDetail(sheet, row, col, mat.MaterialGroupCategoryRefName);
                            col++;

                            AddDetail(sheet, row, col, mat.MaterialName);
                            col++;

                            AddDetail(sheet, row, col, mat.Uom);
                            col++;

                            decimal avgPrice = Math.Round(mat.AvgPrice, roundDecimals, MidpointRounding.AwayFromZero);

                            AddDetail(sheet, row, col, mat.AvgPrice, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            AddDetail(sheet, row, col, mat.OpenBalance, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal OpBalanceCost = Math.Round(mat.OpenBalance * avgPrice, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllOpenStockCost = OverAllOpenStockCost + OpBalanceCost;

                            AddDetail(sheet, row, col, OpBalanceCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            AddDetail(sheet, row, col, mat.Received, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal ReceivedCost = Math.Round(mat.Received * avgPrice, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllReceivedCost = OverAllReceivedCost + ReceivedCost;

                            AddDetail(sheet, row, col, ReceivedCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            AddDetail(sheet, row, col, mat.ExcessReceived, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal ExcessReceivedCost = Math.Round(mat.ExcessReceived * avgPrice, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllExcessReceivedCost = OverAllExcessReceivedCost + ExcessReceivedCost;

                            AddDetail(sheet, row, col, ExcessReceivedCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            foreach (var lst in locsIn)
                            {
                                var fromQty = locTrasferIn.Where(t => t.LocationRefId == lst.Id
                                && t.MaterialRefId == mat.MaterialRefId).Sum(t => t.TotalQty);
                                AddDetail(sheet, row, col++, fromQty, ExcelHorizontalAlignment.Right);
                            }
                            var totalFromQty = locTrasferIn.Where(t => t.MaterialRefId == mat.MaterialRefId).Sum(t => t.TotalQty);
                            if (Math.Round(totalFromQty - mat.TransferIn, 2) > (decimal)0.05)
                            {
                                int i = 15;
                            }

                            AddDetail(sheet, row, col, mat.TransferIn, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal TransferInCost = Math.Round(mat.TransferIn * avgPrice, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllTransferInCost = OverAllTransferInCost + TransferInCost;

                            AddDetail(sheet, row, col, TransferInCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            foreach (var lst in locsOut)
                            {
                                var toQty = locTransferOut.Where(t => t.LocationRefId == lst.Id
                                && t.MaterialRefId == mat.MaterialRefId).Sum(t => t.TotalQty);
                                AddDetail(sheet, row, col++, toQty, ExcelHorizontalAlignment.Right);
                            }
                            var totalToQty = locTransferOut.Where(t => t.MaterialRefId == mat.MaterialRefId).Sum(t => t.TotalQty);

                            if (Math.Round(totalToQty - mat.TransferOut, 2) > (decimal)0.05)
                            {
                                int i = 15;
                            }
                            AddDetail(sheet, row, col, mat.TransferOut, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal TransferOutCost = Math.Round(mat.TransferOut * avgPrice, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllTransferOutCost = OverAllTransferOutCost + TransferOutCost;

                            AddDetail(sheet, row, col, TransferOutCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            AddDetail(sheet, row, col, mat.Issued, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal IssueCost = Math.Round(mat.Issued * avgPrice, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllIssueCost = OverAllIssueCost + IssueCost;

                            AddDetail(sheet, row, col, IssueCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            AddDetail(sheet, row, col, mat.Sales, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal SalesCost = Math.Round(mat.Sales * avgPrice, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllSalesCost = OverAllIssueCost + SalesCost;

                            AddDetail(sheet, row, col, SalesCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            AddDetail(sheet, row, col, mat.Damaged, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal DamageCost = Math.Round(mat.Damaged * avgPrice, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllDamageCost = OverAllDamageCost + DamageCost;

                            AddDetail(sheet, row, col, DamageCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            AddDetail(sheet, row, col, mat.Shortage, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal ShortageCost = Math.Round(mat.Shortage * avgPrice, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllShortageCost = OverAllShortageCost + ShortageCost;

                            AddDetail(sheet, row, col, ShortageCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            AddDetail(sheet, row, col, mat.Return, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal ReturnCost = Math.Round(mat.Return * avgPrice, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllReturnCost = OverAllReturnCost + ReturnCost;

                            AddDetail(sheet, row, col, ReturnCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            AddDetail(sheet, row, col, mat.ClBalance, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal ClBalanceCost = Math.Round(mat.ClBalance * avgPrice, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllClBalanceCost = OverAllClBalanceCost + ClBalanceCost;

                            AddDetail(sheet, row, col, ClBalanceCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            AddDetail(sheet, row, col, mat.TotalConsumption, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal TotalConsumptionCost = Math.Round(mat.TotalConsumption * avgPrice, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllTotalConsumptionCost = OverAllTotalConsumptionCost + TotalConsumptionCost;

                            AddDetail(sheet, row, col, TotalConsumptionCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            AddDetail(sheet, row, col, mat.AvgConsumption, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal AvgConsumptionCost = Math.Round(mat.AvgConsumption * avgPrice, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllAvgConsumptionCost = OverAllAvgConsumptionCost + AvgConsumptionCost;

                            AddDetail(sheet, row, col, AvgConsumptionCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            col = 1;
                            row++;
                            sno++;
                        }
                        row++;


                        AddHeader(sheet, row, 2, L("Opening"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllOpenStockCost);
                        row++;

                        AddHeader(sheet, row, 2, L("Received"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllReceivedCost);
                        row++;

                        AddHeader(sheet, row, 2, L("ExcessReceived"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllExcessReceivedCost);
                        row++;

                        AddHeader(sheet, row, 2, L("TransferIn"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllTransferInCost);
                        row++;

                        AddHeader(sheet, row, 2, L("TransferOut"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllTransferOutCost);
                        row++;

                        AddHeader(sheet, row, 2, L("Issue"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllIssueCost);
                        row++;

                        AddHeader(sheet, row, 2, L("Sales"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllSalesCost);
                        row++;

                        AddHeader(sheet, row, 2, L("Damaged"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllDamageCost);
                        row++;

                        AddHeader(sheet, row, 2, L("Shortage"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllShortageCost);
                        row++;

                        AddHeader(sheet, row, 2, L("Return"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllReturnCost);
                        row++;

                        AddHeader(sheet, row, 2, L("ClBalance"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllClBalanceCost);
                        row++;

                        AddHeader(sheet, row, 2, L("Consumption"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllTotalConsumptionCost);
                        row++;

                        AddHeader(sheet, row, 2, L("AvgConsumption"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllAvgConsumptionCost);
                        row++;

                        AddHeader(sheet, row, 2, L("Sales"));
                        AddHeaderWithMerge(sheet, row, 4, salesAmountWithOutTax);
                        row++;

                        AddHeader(sheet, row, 2, L("Consumption") + "%");
                        if (salesAmountWithOutTax > 0)
                        {
                            AddHeaderWithMerge(sheet, row, 4, OverAllTotalConsumptionCost / salesAmountWithOutTax * 100);
                        }
                        else
                        {
                            AddHeaderWithMerge(sheet, row, 4, "N/A");
                        }

                        row++;


                        for (var i = 1; i <= TotalColumns; i++)
                        {
                            sheet.Column(i).AutoFit();
                        }

                        // rows and columns indices
                        int startRowIndex = 1;
                        int startingColIndex = 1;
                        int totalColIndex = TotalColumns + 3;

                        // rowIndex holds the current running row index
                        int toRowIndex = dtos.Count + 20;

                        AutoFilterRangeGiven(sheet, startRowIndex, startingColIndex, toRowIndex, totalColIndex);
                    }

                    {
                        var tosheet = excelPackage.Workbook.Worksheets.Add(L("TransferIn"));
                        tosheet.OutLineApplyStyle = true;

                        List<int> LocationRefIds = locTrasferIn.OrderBy(t => t.LocationRefId).Select(t => t.LocationRefId).Distinct().ToList();
                        var locs = _locationRepo.GetAllList(t => LocationRefIds.Contains(t.Id));
                        int row = 1;
                        int col = 1;

                        AddHeader(tosheet, row, col++, L("#"));
                        AddHeader(tosheet, row, col++, L("Material"));
                        foreach (var lst in locs)
                        {
                            AddHeader(tosheet, row, col++, lst.Code);
                        }
                        AddHeader(tosheet, row, col++, L("Total"));

                        var sno = 1;
                        row++;
                        foreach (var mat in dtos)
                        {
                            col = 1;
                            AddDetail(tosheet, row, col++, sno++, ExcelHorizontalAlignment.Right);
                            //col++;
                            AddDetail(tosheet, row, col++, mat.MaterialName);
                            //col++;
                            foreach (var lst in locs)
                            {
                                var toQty = locTrasferIn.Where(t => t.LocationRefId == lst.Id && t.MaterialRefId == mat.MaterialRefId).Sum(t => t.TotalQty);
                                AddDetail(tosheet, row, col++, toQty, ExcelHorizontalAlignment.Right);
                            }
                            var totalQty = locTrasferIn.Where(t => t.MaterialRefId == mat.MaterialRefId).Sum(t => t.TotalQty);
                            AddDetail(tosheet, row, col++, totalQty, ExcelHorizontalAlignment.Right);
                            row++;
                        }

                        for (var i = 1; i <= col; i++)
                        {
                            tosheet.Column(i).AutoFit();
                        }
                    }

                    {
                        var tosheet = excelPackage.Workbook.Worksheets.Add(L("TransferOut"));
                        tosheet.OutLineApplyStyle = true;

                        List<int> LocationRefIds = locTransferOut.OrderBy(t => t.LocationRefId).Select(t => t.LocationRefId).Distinct().ToList();
                        var locs = _locationRepo.GetAllList(t => LocationRefIds.Contains(t.Id));
                        int row = 1;
                        int col = 1;

                        AddHeader(tosheet, row, col++, L("#"));
                        AddHeader(tosheet, row, col++, L("Material"));
                        foreach (var lst in locs)
                        {
                            AddHeader(tosheet, row, col++, lst.Code);
                        }
                        AddHeader(tosheet, row, col++, L("Total"));

                        var sno = 1;
                        row++;
                        foreach (var mat in dtos)
                        {
                            col = 1;
                            AddDetail(tosheet, row, col++, sno++, ExcelHorizontalAlignment.Right);
                            //col++;
                            AddDetail(tosheet, row, col++, mat.MaterialName);
                            //col++;
                            foreach (var lst in locs)
                            {
                                var toQty = locTransferOut.Where(t => t.LocationRefId == lst.Id && t.MaterialRefId == mat.MaterialRefId).Sum(t => t.TotalQty);
                                AddDetail(tosheet, row, col++, toQty, ExcelHorizontalAlignment.Right);
                            }
                            var totalQty = locTransferOut.Where(t => t.MaterialRefId == mat.MaterialRefId).Sum(t => t.TotalQty);
                            AddDetail(tosheet, row, col++, totalQty, ExcelHorizontalAlignment.Right);
                            row++;
                        }

                        for (var i = 1; i <= col; i++)
                        {
                            tosheet.Column(i).AutoFit();
                        }
                    }

                    {
                        var sheet = excelPackage.Workbook.Worksheets.Add(L("Category") + L("Stock"));
                        sheet.OutLineApplyStyle = true;

                        int loopCnt = 1;
                        AddHeader(sheet, loopCnt, "#");
                        loopCnt++;

                        AddHeader(sheet, loopCnt, L("Category"));
                        loopCnt++;

                        AddHeader(sheet, 1, loopCnt, "Opening");

                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;

                        AddHeader(sheet, 1, loopCnt, L("Received"));

                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;


                        AddHeader(sheet, 1, loopCnt, L("ExcessReceived"));

                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;

                        //List<int> LocationRefIds = locTrasferIn.OrderBy(t => t.LocationRefId).Select(t => t.LocationRefId).Distinct().ToList();
                        //var locsIn = _locationRepo.GetAllList(t => LocationRefIds.Contains(t.Id));
                        //var startCnt = loopCnt;
                        //// Transfer Out
                        //foreach (var lst in locsIn)
                        //{
                        //    AddHeader(sheet, 2, loopCnt++, lst.Code);
                        //}

                        //if (locsIn.Count > 0)
                        //{
                        //    AddHeaderWithMergeWithColumnCount(sheet, 1, startCnt, locsIn.Count, L("TransferIn"));
                        //    AddHeaderWithMergeWithColumnCount(sheet, 2, startCnt, locsIn.Count, L("Cost"));
                        //}
                        //else
                        //{
                        //    AddHeader(sheet, 1, startCnt, L("TransferIn"));
                        //    AddHeader(sheet, 2, startCnt, L("Cost"));
                        //}
                        //AddHeader(sheet, 2, loopCnt, L("Cost"));
                        AddHeader(sheet, 1, loopCnt, L("TransferIn"));
                        AddHeader(sheet, 2, loopCnt, L("Cost"));

                        loopCnt++;

                        //LocationRefIds = locTransferOut.OrderBy(t => t.LocationRefId).Select(t => t.LocationRefId).Distinct().ToList();
                        //var locsOut = _locationRepo.GetAllList(t => LocationRefIds.Contains(t.Id));
                        //startCnt = loopCnt;
                        //// Transfer Out
                        //foreach (var lst in locsOut)
                        //{
                        //    AddHeader(sheet, 2, loopCnt++, lst.Code);
                        //}

                        //if (locsOut.Count > 0)
                        //{
                        //    AddHeaderWithMergeWithColumnCount(sheet, 1, startCnt, locsOut.Count, L("TransferOut"));
                        //    AddHeaderWithMergeWithColumnCount(sheet, 2, startCnt, locsOut.Count, L("Cost"));
                        //}
                        //else
                        //{
                        //    AddHeader(sheet, 1, loopCnt,  L("TransferOut"));
                        //    AddHeader(sheet, 2, loopCnt,  L("Cost"));
                        //}
                        AddHeader(sheet, 1, loopCnt, L("TransferOut"));
                        AddHeader(sheet, 2, loopCnt, L("Cost"));

                        loopCnt++;


                        AddHeader(sheet, 1, loopCnt, L("Issue"));
                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;

                        AddHeader(sheet, 1, loopCnt, L("Sales"));
                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;

                        AddHeader(sheet, 1, loopCnt, L("Damaged"));
                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;

                        AddHeader(sheet, 1, loopCnt, L("Shortage"));
                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;

                        AddHeader(sheet, 1, loopCnt, L("Return"));
                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;

                        AddHeader(sheet, 1, loopCnt, L("Closing"));
                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;

                        AddHeader(sheet, 1, loopCnt, L("Consumption"));
                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;

                        AddHeader(sheet, 1, loopCnt, L("AvgConsumption"));
                        AddHeader(sheet, 2, loopCnt, L("Cost"));
                        loopCnt++;

                        int TotalColumns = loopCnt;

                        int sno = 1;
                        int row = 4;
                        int col = 1;

                        decimal OverAllOpenStockCost = 0;
                        decimal OverAllReceivedCost = 0;
                        decimal OverAllExcessReceivedCost = 0;
                        decimal OverAllTransferInCost = 0;
                        decimal OverAllTransferOutCost = 0;
                        decimal OverAllIssueCost = 0;
                        decimal OverAllSalesCost = 0;
                        decimal OverAllDamageCost = 0;
                        decimal OverAllShortageCost = 0;
                        decimal OverAllReturnCost = 0;
                        decimal OverAllClBalanceCost = 0;
                        decimal OverAllAvgConsumptionCost = 0;
                        decimal OverAllTotalConsumptionCost = 0;

                        foreach (var lst in dtos.GroupBy(t => t.MaterialGroupCategoryRefId))
                        {
                            var matGroupList = lst.ToList();
                            var mat = matGroupList.FirstOrDefault();

                            AddDetail(sheet, row, col, sno, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;
                            AddDetail(sheet, row, col, mat.MaterialGroupCategoryRefName);
                            col++;

                            decimal openBalanceGroup = Math.Round(matGroupList.Sum(t => t.OpenBalance * t.AvgPrice), roundDecimals, MidpointRounding.AwayFromZero);
                            decimal recivedGroup = matGroupList.Sum(t => t.Received * t.AvgPrice);
                            decimal excessReceivedGroup = matGroupList.Sum(t => t.ExcessReceived * t.AvgPrice);
                            decimal transferInGroup = matGroupList.Sum(t => t.TransferIn * t.AvgPrice);
                            decimal transferOutGroup = matGroupList.Sum(t => t.TransferOut * t.AvgPrice);
                            decimal issueGroup = matGroupList.Sum(t => t.Issued * t.AvgPrice);
                            decimal salesGroup = matGroupList.Sum(t => t.Sales * t.AvgPrice);
                            decimal damagedGroup = matGroupList.Sum(t => t.Damaged * t.AvgPrice);
                            decimal shortageGroup = matGroupList.Sum(t => t.Shortage * t.AvgPrice);
                            decimal returnGroup = matGroupList.Sum(t => t.Return * t.AvgPrice);
                            decimal closingGroup = matGroupList.Sum(t => t.ClBalance * t.AvgPrice);
                            decimal consumptionGroup = matGroupList.Sum(t => t.TotalConsumption * t.AvgPrice);
                            decimal avgConsumptionGroup = matGroupList.Sum(t => t.AvgConsumption * t.AvgPrice);

                            decimal OpBalanceCost = Math.Round(openBalanceGroup, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllOpenStockCost = OverAllOpenStockCost + OpBalanceCost;

                            AddDetail(sheet, row, col, OpBalanceCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal ReceivedCost = Math.Round(recivedGroup, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllReceivedCost = OverAllReceivedCost + ReceivedCost;

                            AddDetail(sheet, row, col, ReceivedCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal ExcessReceivedCost = Math.Round(excessReceivedGroup, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllExcessReceivedCost = OverAllExcessReceivedCost + ExcessReceivedCost;

                            AddDetail(sheet, row, col, ExcessReceivedCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            //foreach (var lstin in locsIn)
                            //{
                            //    var fromQty = locTrasferIn.Where(t => t.LocationRefId == lstin.Id
                            //    && t.MaterialGroupCategoryRefId == mat.MaterialGroupCategoryRefId).Sum(t => t.TotalQty);
                            //    AddDetail(sheet, row, col++, fromQty, ExcelHorizontalAlignment.Right);
                            //}

                            decimal TransferInCost = Math.Round(transferInGroup, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllTransferInCost = OverAllTransferInCost + TransferInCost;

                            AddDetail(sheet, row, col, TransferInCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            //foreach (var lstout in locsOut)
                            //{
                            //    var toQty = locTransferOut.Where(t => t.LocationRefId == lstout.Id
                            //    && t.MaterialGroupCategoryRefId == mat.MaterialGroupCategoryRefId).Sum(t => t.TotalQty);
                            //    AddDetail(sheet, row, col++, toQty, ExcelHorizontalAlignment.Right);
                            //}

                            decimal TransferOutCost = Math.Round(transferOutGroup, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllTransferOutCost = OverAllTransferOutCost + TransferOutCost;

                            AddDetail(sheet, row, col, TransferOutCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal IssueCost = Math.Round(issueGroup, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllIssueCost = OverAllIssueCost + IssueCost;

                            AddDetail(sheet, row, col, IssueCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal SalesCost = Math.Round(salesGroup, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllSalesCost = OverAllIssueCost + SalesCost;

                            AddDetail(sheet, row, col, SalesCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal DamageCost = Math.Round(damagedGroup, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllDamageCost = OverAllDamageCost + DamageCost;

                            AddDetail(sheet, row, col, DamageCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal ShortageCost = Math.Round(shortageGroup, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllShortageCost = OverAllShortageCost + ShortageCost;

                            AddDetail(sheet, row, col, ShortageCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal ReturnCost = Math.Round(returnGroup, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllReturnCost = OverAllReturnCost + ReturnCost;

                            AddDetail(sheet, row, col, ReturnCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal ClBalanceCost = Math.Round(closingGroup, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllClBalanceCost = OverAllClBalanceCost + ClBalanceCost;

                            AddDetail(sheet, row, col, ClBalanceCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal TotalConsumptionCost = Math.Round(consumptionGroup, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllTotalConsumptionCost = OverAllTotalConsumptionCost + TotalConsumptionCost;

                            AddDetail(sheet, row, col, TotalConsumptionCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            decimal AvgConsumptionCost = Math.Round(avgConsumptionGroup, roundDecimals, MidpointRounding.AwayFromZero);
                            OverAllAvgConsumptionCost = OverAllAvgConsumptionCost + AvgConsumptionCost;

                            AddDetail(sheet, row, col, AvgConsumptionCost, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            col = 1;
                            row++;
                            sno++;
                        }
                        row++;


                        AddHeader(sheet, row, 2, L("Opening"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllOpenStockCost);
                        row++;

                        AddHeader(sheet, row, 2, L("Received"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllReceivedCost);
                        row++;

                        AddHeader(sheet, row, 2, L("ExcessReceived"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllExcessReceivedCost);
                        row++;

                        AddHeader(sheet, row, 2, L("TransferIn"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllTransferInCost);
                        row++;

                        AddHeader(sheet, row, 2, L("TransferOut"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllTransferOutCost);
                        row++;

                        AddHeader(sheet, row, 2, L("Issue"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllIssueCost);
                        row++;

                        AddHeader(sheet, row, 2, L("Sales"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllSalesCost);
                        row++;

                        AddHeader(sheet, row, 2, L("Damaged"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllDamageCost);
                        row++;

                        AddHeader(sheet, row, 2, L("Shortage"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllShortageCost);
                        row++;

                        AddHeader(sheet, row, 2, L("Return"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllReturnCost);
                        row++;

                        AddHeader(sheet, row, 2, L("ClBalance"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllClBalanceCost);
                        row++;

                        AddHeader(sheet, row, 2, L("Consumption"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllTotalConsumptionCost);
                        row++;

                        AddHeader(sheet, row, 2, L("AvgConsumption"));
                        AddHeaderWithMerge(sheet, row, 4, OverAllAvgConsumptionCost);
                        row++;

                        AddHeader(sheet, row, 2, L("Sales"));
                        AddHeaderWithMerge(sheet, row, 4, salesAmountWithOutTax);
                        row++;

                        AddHeader(sheet, row, 2, L("Consumption") + "%");
                        if (salesAmountWithOutTax > 0)
                        {
                            AddHeaderWithMerge(sheet, row, 4, OverAllTotalConsumptionCost / salesAmountWithOutTax * 100);
                        }
                        else
                        {
                            AddHeaderWithMerge(sheet, row, 4, "N/A");
                        }

                        row++;


                        for (var i = 1; i <= 25; i++)
                        {
                            sheet.Column(i).AutoFit();
                        }
                    }

                });

            return fileReturn;
        }

        public async Task<string> ExportWorkbookToPdf(string workbookPath, string outputPath)
        {
            // If either required string is null or empty, stop and bail out
            if (string.IsNullOrEmpty(workbookPath) || string.IsNullOrEmpty(outputPath))
            {
                return null;
            }

            // Create COM Objects
            Microsoft.Office.Interop.Excel.Application excelApplication;
            Microsoft.Office.Interop.Excel.Workbook excelWorkbook;

            // Create new instance of Excel
            excelApplication = new Microsoft.Office.Interop.Excel.Application();

            // Make the process invisible to the user
            excelApplication.ScreenUpdating = false;

            // Make the process silent
            excelApplication.DisplayAlerts = false;

            // Open the workbook that you wish to export to PDF
            excelWorkbook = excelApplication.Workbooks.Open(workbookPath);

            // If the workbook failed to open, stop, clean up, and bail out
            if (excelWorkbook == null)
            {
                excelApplication.Quit();

                excelApplication = null;
                excelWorkbook = null;

                return null;
            }

            var exportSuccessful = true;
            try
            {
                // Call Excel's native export function (valid in Office 2007 and Office 2010, AFAIK)
                excelWorkbook.ExportAsFixedFormat(Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypePDF, outputPath);
            }
            catch (System.Exception ex)
            {
                // Mark the export as failed for the return value...
                exportSuccessful = false;

                // Do something with any exceptions here, if you wish...
                // MessageBox.Show...        
            }
            finally
            {
                // Close the workbook, quit the Excel, and clean up regardless of the results...
                excelWorkbook.Close();
                excelApplication.Quit();

                excelApplication = null;
                excelWorkbook = null;
            }

            // You can use the following method to automatically open the PDF after export if you wish
            // Make sure that the file actually exists first...
            if (System.IO.File.Exists(outputPath))
            {
                System.Diagnostics.Process.Start(outputPath);
            }

            return outputPath;

        }

        public async Task<FileDto> VarianceReportBeforeOrAfterDayClose(List<MaterialLedgerDto> matDetailDtos,
         string locationName, DateTime stockStartDate, DateTime stockEndDate, bool materialCodeExists, string beforeOrAfterStatus, int exportType)
        {
            var returnExcelFile = CreateExcelPackage(
               locationName + "-" + beforeOrAfterStatus + " " + L("Variance") + "-" + stockStartDate.ToString("dd-MMM") + " to " + stockEndDate.ToString("dd-MMM-yyyy") + ".xlsx",
               excelPackage =>
               {
                   var sheet = excelPackage.Workbook.Worksheets.Add(stockStartDate.ToString("dd-MMM") + " to " + stockEndDate.ToString("dd-MMM-yyyy"));
                   sheet.OutLineApplyStyle = true;

                   //       name: '+' + app.localize('Excess'),
                   //       name: '-' + app.localize('Issued'),
                   //       name: '-' + app.localize('Sales'),
                   //       name: '-' + app.localize('Damage'),
                   //       name: '-' + app.localize('Shortage'),
                   //       name: '-' + app.localize('Return'),
                   //       name: app.localize('Cl ShouldBe'),
                   //       name: app.localize('Value'),
                   //       name: app.localize('Theoretical') + ' ' + app.localize('Usage'),
                   //       name: app.localize('Theoretical') + ' ' + app.localize('Usage') + ' ' + app.localize('Cost'),
                   //       name: app.localize('Entered Cl Stk'),
                   //       name: app.localize('Actual') + ' ' + app.localize('Usage'),
                   //       name: app.localize('Variance'),

                   int row = 1;
                   //AddHeader(
                   //       sheet,
                   //       ("Code"),  ("Name"),  ("Cost"), ("UOM"),
                   //       ("Opening"), ("+Purchase"), ("-Return"), ("+TransferIn"), ("-TransferOut"),
                   //       ("ActualUsageQty"), ("ActualUsageCost"),
                   //       ("-Batch Receipe"), ("-Theo Usage"), ("-Theo Sales"),
                   //       ("-Raw Waste"), ("-Finished Waste"), ("-Theo Usage Cost"),
                   //       ("= Expected Closing Stock"), ("= Entered Closing Stock"),
                   //       ("= Variance Qty"), ("= Variance Cost")
                   //       );

                   AddHeader(sheet, row, 1, "Code");
                   AddHeader(sheet, row, 2, "Name");
                   AddHeader(sheet, row, 3, "Cost");
                   AddHeader(sheet, row, 4, "UOM");
                   AddHeader(sheet, row, 5, "Opening"); sheet.Column(5).Style.Numberformat.Format = "###0.00";
                   AddHeader(sheet, row, 6, "+Purc"); sheet.Column(6).Style.Numberformat.Format = "###0.00";
                   AddHeader(sheet, row, 7, "-Ret"); sheet.Column(7).Style.Numberformat.Format = "###0.00";
                   AddHeader(sheet, row, 8, "+Trs In"); sheet.Column(8).Style.Numberformat.Format = "###0.00";
                   AddHeader(sheet, row, 9, "-Trs Out"); sheet.Column(9).Style.Numberformat.Format = "###0.00";
                   AddHeaderWithMerge(sheet, row, 10, "Actual");
                   //AddHeader(sheet, row, 10, "Actual Usage Qty"); sheet.Column(10).Style.Numberformat.Format = "###0.00";
                   //AddHeader(sheet, row, 11, "Actual Usage Cost"); sheet.Column(11).Style.Numberformat.Format = "###0.00";
                   AddHeader(sheet, row, 12, "-Batch Receipe"); sheet.Column(12).Style.Numberformat.Format = "###0.00";
                   AddHeader(sheet, row, 13, "-Theo Usage"); sheet.Column(13).Style.Numberformat.Format = "###0.00";
                   AddHeader(sheet, row, 14, "-Theo Sales"); sheet.Column(14).Style.Numberformat.Format = "###0.00";
                   AddHeader(sheet, row, 15, "-Raw Waste"); sheet.Column(15).Style.Numberformat.Format = "###0.00";
                   AddHeader(sheet, row, 16, "-Finished Waste"); sheet.Column(16).Style.Numberformat.Format = "###0.00";
                   AddHeader(sheet, row, 17, "Theo Usage Cost"); sheet.Column(17).Style.Numberformat.Format = "###0.00";
                   AddHeader(sheet, row, 18, "= Expd Closing Stk"); sheet.Column(18).Style.Numberformat.Format = "###0.00";
                   AddHeader(sheet, row, 19, "= Entered Closing Stk"); sheet.Column(19).Style.Numberformat.Format = "###0.00";
                   AddHeaderWithMerge(sheet, row, 20, "Variance"); //sheet.Column(20).Style.Numberformat.Format = "###0.00";
                   //AddHeader(sheet, row, 21, "= Var Cost"); sheet.Column(21).Style.Numberformat.Format = "###0.00";

                   row++;
                   AddHeader(sheet, row, 10, "Qty"); sheet.Column(10).Style.Numberformat.Format = "###0.00";
                   AddHeader(sheet, row, 11, "Cost"); sheet.Column(11).Style.Numberformat.Format = "###0.00";
                   AddHeader(sheet, row, 20, "Qty"); sheet.Column(20).Style.Numberformat.Format = "###0.00";
                   AddHeader(sheet, row, 21, "Cost"); sheet.Column(21).Style.Numberformat.Format = "###0.00";

                   sheet.Row(1).Height = 50;
                   sheet.Row(1).Style.WrapText = true;
                   sheet.Row(1).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                   sheet.Row(2).Height = 20;
                   sheet.Row(2).Style.WrapText = true;
                   sheet.Row(2).Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                   List<int> cols = new List<int>();
                   sheet.Column(5).Style.Numberformat.Format = "###0.00";
                   sheet.Column(6).Style.Numberformat.Format = "###0.00";
                   sheet.Column(7).Style.Numberformat.Format = "###0.00";
                   sheet.PrinterSettings.ShowGridLines = true;
                   sheet.PrinterSettings.RepeatRows = sheet.Cells["1:2"];
                   row++;

                   foreach (var lst in matDetailDtos)
                   {
                       AddDetail(sheet, row, 1, lst.MaterialPetName);
                       AddDetail(sheet, row, 2, lst.MaterialName);
                       AddDetail(sheet, row, 3, lst.AvgPrice);
                       AddDetail(sheet, row, 4, lst.Uom);
                       AddDetail(sheet, row, 5, lst.OpenBalance);
                       AddDetail(sheet, row, 6, lst.Received + lst.ExcessReceived);
                       AddDetail(sheet, row, 7, lst.SupplierReturn);
                       AddDetail(sheet, row, 8, lst.TransferIn);
                       AddDetail(sheet, row, 9, lst.TransferOut);
                       AddDetail(sheet, row, 10, lst.ActualUsageDisplay, ExcelHorizontalAlignment.Right);
                       AddDetail(sheet, row, 11, lst.ActualUsageCost);
                       AddDetail(sheet, row, 12, lst.Issued - lst.Return);
                       AddDetail(sheet, row, 13, lst.TheoreticalUsage);
                       AddDetail(sheet, row, 14, lst.Sales);
                       AddDetail(sheet, row, 15, lst.Shortage);
                       AddDetail(sheet, row, 16, lst.Damaged);
                       AddDetail(sheet, row, 17, lst.TheoreticalUsageCost);
                       AddDetail(sheet, row, 18, lst.ClBalance);
                       AddDetail(sheet, row, 19, lst.EnteredClosingStockDisplay, ExcelHorizontalAlignment.Right);
                       AddDetail(sheet, row, 20, lst.VarianceStockDisplay, ExcelHorizontalAlignment.Right);
                       AddDetail(sheet, row, 21, lst.VarianceCost);
                       sheet.Row(row).Height = 21;
                       row++;
                   }
                   row++;
                   var totalTheoreticalUsageCost = matDetailDtos.Sum(t => t.TheoreticalUsageCost);
                   var totalVarianceCost = matDetailDtos.Sum(t => t.VarianceCost);
                   AddDetail(sheet, row, 2, L("Total"));
                   AddDetail(sheet, row, 17, totalTheoreticalUsageCost);
                   AddDetail(sheet, row, 21, totalVarianceCost);
                   //AddObjects(
                   //consSheet, 2, matDetailDtos, r => r.MaterialPetName, r => r.MaterialName, r=>r.AvgPrice, r => r.Uom,
                   //r => r.OpenBalance, r => r.Received + r.ExcessReceived, r => r.SupplierReturn, r => r.TransferIn, r => r.TransferOut,
                   //r => r.ActualUsageDisplay, r => r.ActualUsageCost,
                   //r => r.Issued - r.Return , r => r.TheoreticalUsage, r=>r.Sales,
                   //r => r.Shortage, r => r.Damaged, r => r.TheoreticalUsageCost,
                   //r => r.ClBalance, r => r.EnteredClosingStockDisplay,
                   //r => r.VarianceStockDisplay, r => r.VarianceCost
                   //);

                   for (var i = 1; i <= 30; i++)
                   {
                       sheet.Column(i).AutoFit();
                   }

                   if (materialCodeExists == false)
                   {
                       sheet.Column(1).Width = 0;
                   }
                   sheet.PrinterSettings.PaperSize = ePaperSize.A4;
                   sheet.PrinterSettings.Orientation = eOrientation.Landscape;
                   sheet.PrinterSettings.HorizontalCentered = true;
                   sheet.PrinterSettings.FitToPage = true;
                   sheet.PrinterSettings.FitToWidth = 1;
                   sheet.PrinterSettings.FitToHeight = 0;

                   var header = sheet.HeaderFooter.EvenHeader;
                   // &24: Font size
                   // &U: Underlined
                   // &"": Font name
                   string headerText;
                   //if (beforeOrAfterStatus.Equals("After"))
                   headerText = "Inventory Variance Report " + beforeOrAfterStatus + " Day Close " + stockStartDate.ToString("dd-MMM") + " to " + stockEndDate.ToString("dd-MMM-yyyy");
                   header.CenteredText = "&24&U&\"Arial,Regular Bold\" " + headerText;
                   header.RightAlignedText = ExcelHeaderFooter.PageNumber + "/" + ExcelHeaderFooter.NumberOfPages + Convert.ToChar(10) + " Printed On " + DateTime.Now.ToString("dd-MMM-yyyy HH:mm");
                   header.LeftAlignedText = locationName;

                   var header1 = sheet.HeaderFooter.OddHeader;
                   // &24: Font size
                   // &U: Underlined
                   // &"": Font name
                   header1.CenteredText = "&24&U&\"Arial,Regular Bold\" " + headerText;
                   header1.RightAlignedText = ExcelHeaderFooter.PageNumber + "/" + ExcelHeaderFooter.NumberOfPages + Convert.ToChar(10) + " Printed On " + DateTime.Now.ToString("dd-MMM-yyyy HH:mm");
                   header1.LeftAlignedText = locationName;
               });

            if (exportType == 2)
            {
                string templatePathBase = System.AppDomain.CurrentDomain.BaseDirectory;
                string templatePath = templatePathBase + @"Temp\Downloads\" + returnExcelFile.FileToken;

                //C:\DinePlan\DineConnect\DineConnect\DinePlan.DineConnect.Web\Temp\Downloads\

                #region FilePathDefinition

                var subPath = "\\TempFiles" + "\\Reports\\Excel";

                var phyPath = Path.GetFullPath(subPath);

                bool exists = Directory.Exists((phyPath));//System.IO.Server.MapPath

                if (!exists)
                {
                    System.IO.Directory.CreateDirectory(phyPath);
                }
                #endregion

                string templateFileName = templatePath;/* + "\\" + "KeppelTemplateMpi.xlsx"*/

                string timeStamp = DateTime.Now.ToFileTime().ToString(); // DateTime.
                string dbsaveFileName = @"AvgPrice_" + timeStamp;
                string excelLocation = phyPath + "\\" + dbsaveFileName + ".xlsx";
                string outputPdfLocation = templatePath + ".pdf";

                FileInfo fileSaveAs = new FileInfo(excelLocation);

                File.Copy(templatePath, excelLocation);

                await ExportWorkbookToPdf(excelLocation, outputPdfLocation);

                returnExcelFile.FileName = dbsaveFileName + ".PDF";
                returnExcelFile.FileToken = returnExcelFile.FileToken + ".pdf";
                returnExcelFile.FileType = MimeTypeNames.ApplicationPdf;
            }
            return returnExcelFile;
        }


        public FileDto ExportClosingStockGroupCategoryMaterials(LocationWiseClosingStockGroupCateogryMaterialDto input)
        {
            var masterDtos = input.LocationWiseHeadWiseStockList;

            return CreateExcelPackage(
                @L("ClosingStock") + " " + input.InputFilterDto.EndDate.ToString("dd-MMM-yyy") + " " + L("Location") + " " + L("Wise") + ".xlsx",
                excelPackage =>
                {
                    decimal totalAmount = 0;
                    {
                        #region Group Sheet
                        var consSheet = excelPackage.Workbook.Worksheets.Add(L("Group"));
                        consSheet.OutLineApplyStyle = true;
                        int rowIndex = 1;
                        int colIndex = 1;
                        AddHeader(consSheet, rowIndex, colIndex++, L("Location").ToUpper());
                        foreach (var lst in input.MaterialGroupWisePurchaseDtos)
                        {
                            AddHeader(consSheet, rowIndex, colIndex++, lst.MaterialGroupRefName);
                        }
                        AddHeader(consSheet, rowIndex, colIndex++, L("Total").ToUpper());
                        rowIndex++;

                        foreach (var master in masterDtos)
                        {
                            colIndex = 1;
                            consSheet.Row(rowIndex).Height = 21;
                            AddHeader(consSheet, rowIndex, colIndex++, master.LocationRefName);
                            foreach (var lst in input.MaterialGroupWisePurchaseDtos)
                            {
                                var existRecord = master.MaterialGroupWisePurchaseDtos.FirstOrDefault(t => t.MaterialGroupRefId == lst.MaterialGroupRefId);
                                if (existRecord != null)
                                {
                                    AddDetailWithNumberFormat(consSheet, rowIndex, colIndex++, existRecord.TotalAmount, "0.00");
                                }
                                else
                                {
                                    colIndex++;
                                }
                            }
                            totalAmount = master.MaterialGroupWisePurchaseDtos.Sum(t => t.TotalAmount);
                            AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, totalAmount, "0.00");
                            rowIndex++;
                        }
                        colIndex = 1;
                        AddDetail(consSheet, rowIndex, colIndex++, L("Total"), OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                        foreach (var lst in input.MaterialGroupWisePurchaseDtos)
                        {
                            AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, lst.TotalAmount, "0.00");
                        }
                        totalAmount = input.MaterialGroupWisePurchaseDtos.Sum(t => t.TotalAmount);
                        AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, totalAmount, "0.00");
                        rowIndex++;
                        for (var i = 1; i <= input.MaterialGroupWisePurchaseDtos.Count + 2; i++)
                        {
                            consSheet.Column(i).AutoFit();
                        }
                        #endregion
                    }

                    {
                        #region Category Sheet
                        var consSheet = excelPackage.Workbook.Worksheets.Add(L("Category"));
                        consSheet.OutLineApplyStyle = true;
                        int rowIndex = 1;
                        int colIndex = 1;
                        AddHeader(consSheet, rowIndex, colIndex++, L("Location").ToUpper());
                        foreach (var lst in input.MaterialGroupCategoryWisePurchaseDtos)
                        {
                            AddHeader(consSheet, rowIndex, colIndex++, lst.MaterialGroupCategoryRefName);
                        }
                        AddHeader(consSheet, rowIndex, colIndex++, L("Total").ToUpper());
                        rowIndex++;

                        foreach (var master in masterDtos)
                        {
                            colIndex = 1;
                            consSheet.Row(rowIndex).Height = 21;
                            AddHeader(consSheet, rowIndex, colIndex++, master.LocationRefName);
                            foreach (var lst in input.MaterialGroupCategoryWisePurchaseDtos)
                            {
                                var existRecord = master.MaterialGroupCategoryWisePurchaseDtos.FirstOrDefault(t => t.MaterialGroupCategoryRefId == lst.MaterialGroupCategoryRefId);
                                if (existRecord != null)
                                {
                                    AddDetailWithNumberFormat(consSheet, rowIndex, colIndex++, existRecord.TotalAmount, "0.00");
                                }
                                else
                                {
                                    colIndex++;
                                }
                            }
                            totalAmount = master.MaterialGroupCategoryWisePurchaseDtos.Sum(t => t.TotalAmount);
                            AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, totalAmount, "0.00");
                            rowIndex++;
                        }
                        colIndex = 1;
                        AddDetail(consSheet, rowIndex, colIndex++, L("Total"), OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                        foreach (var lst in input.MaterialGroupCategoryWisePurchaseDtos)
                        {
                            AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, lst.TotalAmount, "0.00");
                        }
                        totalAmount = input.MaterialGroupCategoryWisePurchaseDtos.Sum(t => t.TotalAmount);
                        AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, totalAmount, "0.00");
                        rowIndex++;
                        for (var i = 1; i <= input.MaterialGroupCategoryWisePurchaseDtos.Count + 2; i++)
                        {
                            consSheet.Column(i).AutoFit();
                        }
                        #endregion
                    }

                    if (input.InputFilterDto.ExportMaterialAlso == true)
                    {
                        #region Material Sheet
                        var consSheet = excelPackage.Workbook.Worksheets.Add(L("Material"));
                        consSheet.OutLineApplyStyle = true;
                        int rowIndex = 1;
                        int colIndex = 1;
                        AddHeader(consSheet, rowIndex, colIndex++, L("Location").ToUpper());
                        foreach (var lst in input.MaterialWisePurchaseDtos)
                        {
                            AddHeader(consSheet, rowIndex, colIndex++, lst.MaterialRefName);
                        }
                        AddHeader(consSheet, rowIndex, colIndex++, L("Total").ToUpper());
                        rowIndex++;

                        foreach (var master in masterDtos)
                        {
                            colIndex = 1;
                            consSheet.Row(rowIndex).Height = 21;
                            AddHeader(consSheet, rowIndex, colIndex++, master.LocationRefName);
                            foreach (var lst in input.MaterialWisePurchaseDtos)
                            {
                                var existRecord = master.MaterialWisePurchaseDtos.FirstOrDefault(t => t.MaterialRefId == lst.MaterialRefId);
                                if (existRecord != null)
                                {
                                    AddDetailWithNumberFormat(consSheet, rowIndex, colIndex++, existRecord.TotalAmount, "0.00");
                                }
                                else
                                {
                                    colIndex++;
                                }
                            }
                            totalAmount = master.MaterialWisePurchaseDtos.Sum(t => t.TotalAmount);
                            AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, totalAmount, "0.00");
                            rowIndex++;
                        }
                        colIndex = 1;
                        AddDetail(consSheet, rowIndex, colIndex++, L("Total"), OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                        foreach (var lst in input.MaterialWisePurchaseDtos)
                        {
                            AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, lst.TotalAmount, "0.00");
                        }
                        totalAmount = input.MaterialWisePurchaseDtos.Sum(t => t.TotalAmount);
                        AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, totalAmount, "0.00");
                        rowIndex++;
                        for (var i = 1; i <= input.MaterialWisePurchaseDtos.Count + 2; i++)
                        {
                            consSheet.Column(i).AutoFit();
                        }
                        #endregion
                    }
                });
        }

        public FileDto ExportInterTransferReceivedDetailReportForGivenMaterialWise(InterTransferReceivedExportReportDto input)
        {
            var masterDtos = input.InterTransferReceivedReportDtos;

            return CreateExcelPackage(
                @L("InterTransfer") + " " + @L("Received") + " " + @L("Detail") + " " + input.InputFilterDto.StartDate.ToString("dd-MMM-yyyy") + " " + input.InputFilterDto.EndDate.ToString("dd-MMM-yyy") + " " + L("Location") + " " + L("Wise") + ".xlsx",
                excelPackage =>
                {
                    {
                        #region Group Sheet
                        var consSheet = excelPackage.Workbook.Worksheets.Add(L("Material"));
                        consSheet.OutLineApplyStyle = true;
                        int rowIndex = 1;
                        int colIndex = 1;
                        string dateRange = input.InputFilterDto.StartDate.ToString("dd-MMM-yyyy") + " - " + input.InputFilterDto.EndDate.ToString("dd-MMM-yyyy");
                        foreach (var master in masterDtos)
                        {
                            colIndex = 1;
                            consSheet.Row(rowIndex).Height = 21;
                            AddHeader(consSheet, rowIndex, colIndex++, L("DateRange").ToUpper());
                            AddHeader(consSheet, rowIndex, colIndex++, dateRange);

                            rowIndex++;
                            consSheet.Row(rowIndex).Height = 21;
                            colIndex = 1;
                            AddHeader(consSheet, rowIndex, colIndex++, L("Location"));
                            AddHeader(consSheet, rowIndex, colIndex++, master.LocationRefCode);
                            rowIndex++;

                            consSheet.Row(rowIndex).Height = 21;
                            colIndex = 1;
                            AddHeader(consSheet, rowIndex, colIndex++, L("Category"));
                            AddHeader(consSheet, rowIndex, colIndex++, master.MaterialGroupCategoryRefName);
                            rowIndex++;

                            consSheet.Row(rowIndex).Height = 21;
                            colIndex = 1;
                            AddHeader(consSheet, rowIndex, colIndex++, L("Material"));
                            AddHeader(consSheet, rowIndex, colIndex++, master.MaterailRefName);
                            rowIndex++;
                            rowIndex++;

                            consSheet.Row(rowIndex).Height = 21;
                            colIndex = 1;
                            AddHeader(consSheet, rowIndex, colIndex++, L("Date"));
                            AddHeader(consSheet, rowIndex, colIndex++, L("Quantity"));
                            AddHeader(consSheet, rowIndex, colIndex++, L("UOM"));
                            AddHeader(consSheet, rowIndex, colIndex++, L("Price"));
                            AddHeader(consSheet, rowIndex, colIndex++, L("Total"));
                            AddHeader(consSheet, rowIndex, colIndex++, L("Movement"));
                            rowIndex++;

                            foreach (var lst in master.ReceivedDetails)
                            {
                                colIndex = 1;
                                consSheet.Row(rowIndex).Height = 21;
                                AddDetail(consSheet, rowIndex, colIndex++, lst.ReceivedTime.ToString("dd-MMM-yyyy"));
                                AddDetailWithNumberFormat(consSheet, rowIndex, colIndex++, lst.ReceivedQty, "0.00");
                                AddDetail(consSheet, rowIndex, colIndex++, lst.UnitRefName);
                                AddDetailWithNumberFormat(consSheet, rowIndex, colIndex++, lst.Price, "0.00");
                                AddDetailWithNumberFormat(consSheet, rowIndex, colIndex++, lst.TotalAmount, "0.00");
                                AddDetail(consSheet, rowIndex, colIndex++, lst.RequestLocationRefName);
                                rowIndex++;
                            }
                            colIndex = 1;
                            AddHeader(consSheet, rowIndex, colIndex++, "");
                            AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, master.TotalRecdQty, "0.00");
                            AddHeader(consSheet, rowIndex, colIndex++, "");
                            AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, master.AvgPrice, "0.00");
                            AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, master.TotalRecdValue, "0.00");
                            AddHeader(consSheet, rowIndex, colIndex++, "");
                            rowIndex++;
                            rowIndex++;
                            rowIndex++;
                            rowIndex++;
                        }

                        for (var i = 1; i <= 5 + 2; i++)
                        {
                            consSheet.Column(i).AutoFit();
                        }
                        consSheet.Column(1).Width = 15;
                        consSheet.Column(2).Width = 15;
                        #endregion
                    }
                });
        }


        public FileDto ExportMaterialTrackReportForGivenMaterialWise(MaterialTrackExportReportDto input)
        {
            var masterDtos = input.MaterialTrackWithValueReportDtos;

            return CreateExcelPackage(
                @L("Material") + " " + @L("Received") + " " + @L("Issue") + " " + input.InputFilterDto.StartDate.ToString("dd-MMM-yyyy") + " " + input.InputFilterDto.EndDate.ToString("dd-MMM-yyy") + " " + L("Location") + " " + L("Wise") + ".xlsx",
                excelPackage =>
                {
                    {
                        #region Group Sheet
                        var consSheet = excelPackage.Workbook.Worksheets.Add(L("Material"));
                        consSheet.OutLineApplyStyle = true;
                        int rowIndex = 1;
                        int colIndex = 1;
                        string dateRange = input.InputFilterDto.StartDate.ToString("dd-MMM-yyyy") + " - " + input.InputFilterDto.EndDate.ToString("dd-MMM-yyyy");
                        foreach (var master in masterDtos)
                        {
                            colIndex = 1;
                            consSheet.Row(rowIndex).Height = 21;
                            AddHeader(consSheet, rowIndex, colIndex++, L("DateRange").ToUpper());
                            AddHeader(consSheet, rowIndex, colIndex++, dateRange);

                            rowIndex++;
                            consSheet.Row(rowIndex).Height = 21;
                            colIndex = 1;
                            AddHeader(consSheet, rowIndex, colIndex++, L("Location"));
                            AddHeader(consSheet, rowIndex, colIndex++, master.LocationRefCode);
                            rowIndex++;

                            consSheet.Row(rowIndex).Height = 21;
                            colIndex = 1;
                            AddHeader(consSheet, rowIndex, colIndex++, L("Category"));
                            AddHeader(consSheet, rowIndex, colIndex++, master.MaterialGroupCategoryRefName);
                            rowIndex++;

                            consSheet.Row(rowIndex).Height = 21;
                            colIndex = 1;
                            AddHeader(consSheet, rowIndex, colIndex++, L("Material"));
                            AddHeader(consSheet, rowIndex, colIndex++, master.MaterialRefName);
                            rowIndex++;
                            rowIndex++;

                            consSheet.Row(rowIndex).Height = 21;
                            colIndex = 1;
                            AddHeader(consSheet, rowIndex, colIndex++, L("Date"));
                            AddHeader(consSheet, rowIndex, colIndex++, L("Quantity"));
                            AddHeader(consSheet, rowIndex, colIndex++, L("UOM"));
                            AddHeader(consSheet, rowIndex, colIndex++, L("Price"));
                            AddHeader(consSheet, rowIndex, colIndex++, L("Total"));
                            AddHeader(consSheet, rowIndex, colIndex++, L("Movement"));
                            AddHeader(consSheet, rowIndex, colIndex++, L("Reference"));
                            rowIndex++;

                            foreach (var lst in master.ReceivedDetails)
                            {
                                colIndex = 1;
                                consSheet.Row(rowIndex).Height = 21;
                                if (lst.Action.Equals("-"))
                                {
                                    lst.Qty = lst.Qty * -1;
                                    lst.TotalAmount = lst.TotalAmount * -1;
                                }
                                AddDetail(consSheet, rowIndex, colIndex++, lst.TransactionTime.ToString("dd-MMM-yyyy"));
                                AddDetailWithNumberFormat(consSheet, rowIndex, colIndex++,lst.Qty, "0.00");
                                AddDetail(consSheet, rowIndex, colIndex++, lst.UnitRefName);
                                AddDetailWithNumberFormat(consSheet, rowIndex, colIndex++, lst.Price, "0.00");
                                AddDetailWithNumberFormat(consSheet, rowIndex, colIndex++, lst.TotalAmount, "0.00");
                                AddDetail(consSheet, rowIndex, colIndex++, lst.LinedEntityName);
                                AddDetail(consSheet, rowIndex, colIndex++, lst.ReferenceNumber + " , " + L("Id") + " :" +lst.RefernceId);
                                rowIndex++;
                            }
                            colIndex = 1;
                            AddHeader(consSheet, rowIndex, colIndex++, "");
                            AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, master.Qty, "0.00");
                            AddHeader(consSheet, rowIndex, colIndex++, "");
                            AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, master.AvgPrice, "0.00");
                            AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, master.TotalAmount, "0.00");
                            AddHeader(consSheet, rowIndex, colIndex++, "");
                            rowIndex++;
                            rowIndex++;
                            rowIndex++;
                            rowIndex++;
                        }

                        for (var i = 1; i <= 5 + 2; i++)
                        {
                            consSheet.Column(i).AutoFit();
                        }
                        consSheet.Column(1).Width = 15;
                        consSheet.Column(2).Width = 15;
                        #endregion
                    }
                });
        }
        
    }
}
