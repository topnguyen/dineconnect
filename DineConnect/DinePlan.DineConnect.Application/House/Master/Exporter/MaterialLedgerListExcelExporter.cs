﻿
using System.Collections.Generic;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Transaction.Exporter
{
    public class MaterialLedgerListExcelExporter : FileExporterBase, IMaterialLedgerListExcelExporter
    {
        public FileDto ExportToFile(List<MaterialLedgerViewDto> dtos)
        {
            return CreateExcelPackage(
                "MaterialLedgerList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("MaterialLedger"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Location"),
                        L("Material"),
                        L("UOM"),
                        L("Date"),
                        L("Open"),
                        L("Received"),
                        L("Issued"),
                        L("Close")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.LocationRefName,
                        _ => _.MaterialRefName,
                        _ => _.Uom,
                        _ => _.LedgerDate,
                        _ => _.OpenBalance,
                        _ => _.OverAllReceived,
                        _ => _.OverAllIssued,
                        _ => _.ClBalance
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}