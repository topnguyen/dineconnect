﻿
using System.Collections.Generic;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using System;

namespace DinePlan.DineConnect.House.Master.Exporter
{
    public class MaterialLocationWiseStockListExcelExporter : FileExporterBase, IMaterialLocationWiseStockListExcelExporter
    {
        public FileDto ExportToFile(List<MaterialLocationWiseStockViewDto> dtos)
        {
            return CreateExcelPackage(
                "MaterialLocationWiseStockList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("MaterialLocationWiseStock"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Material"),
                        L("UOM"),
                        L("LedgerBalance"),
                        L("Sales"),
                        L("AvailableBalance"),
                        L("MinStock"),
                        L("MaxStock"),
                        L("Re-OrderLevel"),
                          L("ConvertAsZeroStockWhenClosingStockNegative")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.MaterialRefName,
                        _ => _.Uom,
                         _ => _.CurrentInHand,
                        _ => _.Sales,
                        _ => _.LiveStock,
                        _ => _.MinimumStock,
                         _ => _.MaximumStock,
                        _ => _.ReOrderLevel,
                           r => r.ConvertAsZeroStockWhenClosingStockNegative == true ? "YES" : "NO"
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }

        public FileDto StockToExcelFile(List<MaterialLocationWiseStockViewDto> dtos, string locationName, DateTime stockDate)
        {
            return CreateExcelPackage(
                locationName + "-" + stockDate.ToString("dd-MMM-yy") + ".xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Stock") + stockDate.ToString("dd-MMM-yy"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Location"),
                        L("MaterialType"),
                        L("Material"),
                        L("UOM"),
                        L("OnHand"),
                        L("MinimumStock"),
                        L("MaximumStock"),
                        L("Re-OrderLevel"),
                        L("ConvertAsZeroStockWhenClosingStockNegative"),
                        L("IsActiveInLocation")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        r => r.LocationRefName,
                        r => r.MaterialTypeRefName,
                        r => r.MaterialRefName,
                        r => r.Uom,
                        r => r.CurrentInHand,
                        r => r.MinimumStock,
                        r => r.MaximumStock,
                        r => r.ReOrderLevel,
                        r => r.ConvertAsZeroStockWhenClosingStockNegative==true? "YES" : "NO",
                        r => r.IsActiveInLocation == true ? "YES" : "NO"
                        );

                    for (var i = 1; i <= 12; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }


    }
}
