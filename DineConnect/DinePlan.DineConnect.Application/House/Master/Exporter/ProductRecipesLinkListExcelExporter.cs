﻿
using System.Collections.Generic;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master.Exporter
{
    public class ProductRecipesLinkListExcelExporter : FileExporterBase, IProductRecipesLinkListExcelExporter
    {
        public FileDto ExportToFile(List<ProductRecipesLinkListDto> dtos)
        {
            return CreateExcelPackage(
                "ProductRecipesLinkList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("ProductRecipesLink"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
