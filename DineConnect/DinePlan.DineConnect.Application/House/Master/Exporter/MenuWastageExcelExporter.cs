﻿
using System.Collections.Generic;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using System.Linq;
using DinePlan.DineConnect.House.Master.Exporter;
using System;
using DinePlan.DineConnect.Exporter.Util;
using Abp.Configuration;
using DinePlan.DineConnect.Configuration;
using System.Text;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.UI;
using DinePlan.DineConnect.Exporter;

namespace DinePlan.DineConnect.House.Transaction.Exporter
{
    public class MenuWastageExcelExporter : FileExporterBase, IMenuWastageExcelExporter
    {
        private SettingManager _settingManager;
        private int roundDecimals = 2;
        private string dateFormat = "dd-MM-yyyy HH:ss:mm";
        public MenuWastageExcelExporter(SettingManager settingManager)
        {
            _settingManager = settingManager;
            roundDecimals = _settingManager.GetSettingValue<int>(AppSettings.ConnectSettings.Decimals);
            dateFormat = _settingManager.GetSettingValue(AppSettings.ConnectSettings.DateTimeFormat);
        }

        public FileDto ExportToFile(List<MenuItemWastageListDto> dtos)
        {
            return CreateExcelPackage(
                "MenuWastage.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Adjustment"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("LocationName"),
                        L("SalesDate"),
                        L("TokenNumber"),
                        L("Remarks"),
                        L("CreationTime")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.LocationRefName,
                        _ => _.SalesDate.ToString("yyyy-MMM-dd"),
                        _ => _.TokenRefNumber,
                        _ => _.Remarks,
                        _ => _.CreationTime.ToString("yyyy-MMM-dd")
                        );

                    for (var i = 1; i <= 10; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }

        public FileDto MenuWastageReportToExcelFile(List<MenuWastageConsolidatedReport> dtos, DateTime FromDate, DateTime ToDate, string UserName)
        {
            return CreateExcelPackage(
            "MenuWastageReport.xlsx",
            excelPackage =>
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("MenuWastage"));
                sheet.OutLineApplyStyle = true;
                int row = 1;
                AddHeaderWithMergeWithColumnCount(sheet, row, 1, 7, L("MenuWastageReport"));
                row++;
                AddHeaderWithMergeWithColumnCount(sheet, row, 1, 7, L("FromTimeToTime", FromDate.ToString("dd-MMM-yyyy"), ToDate.ToString("dd-MMM-yyyy")));
                row++;
                AddDetail(sheet, row, 1, L("PrintedOn"));
                AddDetail(sheet, row, 2, DateTime.Today.ToString("dd-MMM-yyyy"));
                row++;
                AddDetail(sheet, row, 1, L("PrintedBy"));
                AddDetail(sheet, row, 2, UserName);
                row++;
                row++;

                AddHeader(sheet, row, L("Menu"), L("EntryType"), L("Quantity"), L("Price"), L("TotalCost"), L("Date"), L("User"));
                decimal OverAllCost = 0;
                foreach (var mas in dtos)
                {
                    row++;
                    AddHeaderWithMergeWithColumnCount(sheet, row, 1, 5, "Location : " + mas.LocationRefName);
                    row++;
                    AddObjects(sheet, row, mas.WastageReport,
                            t => t.PosMenuPortionRefName,
                            t => t.MenuWastageMode,
                            t => t.WastageQty,
                            t => t.CostPerUnit,
                            t => t.TotalAmount,
                            t => t.SalesDate.ToString("dd-MMM-yyyy"),
                            t => t.UserName
                            );
                    row = row + mas.WastageReport.Count;
                    row++;
                    AddDetail(sheet, row, 2, L("SubTotal"));
                    decimal totalCost = Math.Round(mas.WastageReport.Sum(t => t.TotalAmount), 2);
                    OverAllCost = OverAllCost + totalCost;
                    AddDetail(sheet, row, 5, totalCost);
                    row++;
                    row++;
                }
                row++;
                AddDetail(sheet, row, 2, L("Total"));
                AddDetail(sheet, row, 5, OverAllCost);
                row++;
                for (var i = 1; i <= 20; i++)
                {
                    sheet.Column(i).AutoFit();
                }
            });
        }

        public async Task<FileDto> MenuWastageReportExportAsync(IMenuItemWastageAppService menuwastageAppService, InputMenuWastageReport input, string UserName, ExportType exporttype)
        {
            var returnOutput = await menuwastageAppService.GetMenuWastageConsolidatedAll(input);
            if (returnOutput.TotalCount == 0)
            {
                throw new UserFriendlyException(L("NoRecordsFound"));
            }

            var eObject = new ExportInputObject
            {
                ExportType = exporttype
            };

            eObject.Fields.Add("LocationRefName", new FieldAttributes
            {
                DisplayName = L("Location"),
                Alignment = FieldAlignment.Left,
                FieldType = ExportFiledType.String,
            });
            eObject.Fields.Add("PosMenuPortionRefName", new FieldAttributes
            {
                DisplayName = L("Menu"),
                Alignment = FieldAlignment.Left,
                FieldType = ExportFiledType.String,
            });
            eObject.Fields.Add("MenuWastageMode", new FieldAttributes
            {
                DisplayName = L("EntryType"),
                Alignment = FieldAlignment.Left,
                FieldType = ExportFiledType.String
            });

            eObject.Fields.Add("WastageQty", new FieldAttributes
            {
                DisplayName = L("Quantity"),
                Alignment = FieldAlignment.Right,
                FieldType = ExportFiledType.Decimal,
                Format = "N" + roundDecimals,
            });

            eObject.Fields.Add("CostPerUnit", new FieldAttributes
            {
                DisplayName = L("Price"),
                Alignment = FieldAlignment.Right,
                FieldType = ExportFiledType.Decimal,
                Format = "N" + roundDecimals,
            });

            eObject.Fields.Add("TotalAmount", new FieldAttributes
            {
                DisplayName = L("TotalCost"),
                Alignment = FieldAlignment.Right,
                FieldType = ExportFiledType.Decimal,
                Format = "N" + roundDecimals,
            });
            eObject.Fields.Add("SalesDate", new FieldAttributes
            {
                DisplayName = L("Date"),
                Alignment = FieldAlignment.Left,
                FieldType = ExportFiledType.Date,
                Format = dateFormat
            });

            eObject.Fields.Add("UserName", new FieldAttributes
            {
                DisplayName = L("User"),
                Alignment = FieldAlignment.Left,
                FieldType = ExportFiledType.String
            });

            var items = returnOutput.Items;

            //var pOutput = returnOutput.Items.MapTo<List<ExportDataObject>>();
            var pOutput = new List<ExportDataObject>();
            pOutput.AddRange(items);

            if (pOutput.Any())
            {
                StringBuilder firstPageBuilder = new StringBuilder();
                firstPageBuilder.Append(L("MenuWastageReport"));
                firstPageBuilder.Append("{NL}");
                firstPageBuilder.Append(string.Format(L("FromTimeToTime"), input.StartDate.Value.ToString(dateFormat), input.EndDate.Value.ToString(dateFormat)));
                firstPageBuilder.Append("{NL}");
                firstPageBuilder.Append("{NL}");

                StringBuilder lPageBuilder = new StringBuilder();
                lPageBuilder.Append("{NL}");
                lPageBuilder.Append(L("PrintedOn") + " :" + DateTime.Now.ToString(dateFormat));

                eObject.FirstPage = firstPageBuilder.ToString();
                eObject.LastPage = lPageBuilder.ToString();
                eObject.ExportObjects = pOutput;

                StringBuilder fileNameBuilder = new StringBuilder();
                fileNameBuilder.Append(L("MenuWastageReport"));
                fileNameBuilder.Append(L("-"));
                fileNameBuilder.Append(!input.StartDate.Value.Equals(DateTime.MinValue)
                    ? input.StartDate.Value.ToString("dd/MM/yyyy")
                    : DateTime.Now.ToString("dd/MM/yyyy"));
                fileNameBuilder.Append(" - ");
                fileNameBuilder.Append(!input.EndDate.Value.Equals(DateTime.MinValue)
                    ? input.EndDate.Value.ToString("dd/MM/yyyy")
                    : DateTime.Now.ToString("dd/MM/yyyy"));

                eObject.FileName = fileNameBuilder.ToString();
                var exporter = new DineConnectDocExporter(eObject);
                return exporter.ExportFile(AppFolders.TempFileDownloadFolder);
            }

            return null;
            //return CreateExcelPackage(
            //"MenuWastageReport.xlsx",
            //excelPackage =>
            //{
            //	var sheet = excelPackage.Workbook.Worksheets.Add(L("MenuWastage"));
            //	sheet.OutLineApplyStyle = true;
            //	int row = 1;
            //	AddHeaderWithMergeWithColumnCount(sheet, row, 1, 7, L("MenuWastageReport"));
            //	row++;
            //	AddHeaderWithMergeWithColumnCount(sheet, row, 1, 7, L("FromTimeToTime", FromDate.ToString("dd-MMM-yyyy"), ToDate.ToString("dd-MMM-yyyy")));
            //	row++;
            //	AddDetail(sheet, row, 1, L("PrintedOn"));
            //	AddDetail(sheet, row, 2, DateTime.Today.ToString("dd-MMM-yyyy"));
            //	row++;
            //	AddDetail(sheet, row, 1, L("PrintedBy"));
            //	AddDetail(sheet, row, 2, UserName);
            //	row++;
            //	row++;

            //	AddHeader(sheet, row, L("Menu"), L("EntryType"), L("Quantity"), L("Price"), L("TotalCost"), L("Date"), L("User"));
            //	decimal OverAllCost = 0;
            //	foreach (var mas in dtos)
            //	{
            //		row++;
            //		AddHeaderWithMergeWithColumnCount(sheet, row, 1, 5, "Location : " + mas.LocationRefName);
            //		row++;
            //		AddObjects(sheet, row, mas.WastageReport,
            //				t => t.PosMenuPortionRefName,
            //				t => t.MenuWastageMode,
            //				t => t.WastageQty,
            //				t => t.CostPerUnit,
            //				t => t.TotalAmount,
            //				t => t.SalesDate.ToString("dd-MMM-yyyy"),
            //				t => t.UserName
            //				);
            //		row = row + mas.WastageReport.Count;
            //		row++;
            //		AddDetail(sheet, row, 2, L("SubTotal"));
            //		decimal totalCost = Math.Round(mas.WastageReport.Sum(t => t.TotalAmount), 2);
            //		OverAllCost = OverAllCost + totalCost;
            //		AddDetail(sheet, row, 5, totalCost);
            //		row++;
            //		row++;
            //	}
            //	row++;
            //	AddDetail(sheet, row, 2, L("Total"));
            //	AddDetail(sheet, row, 5, OverAllCost);
            //	row++;
            //	for (var i = 1; i <= 20; i++)
            //	{
            //		sheet.Column(i).AutoFit();
            //	}
            //});
        }

        public async Task<FileDto> MenuWastageCustomReportExcelExportAsync(IMenuItemWastageAppService menuwastageAppService, InputMenuWastageReport input)
        {
            var returnOutput = await menuwastageAppService.GetMenuWastageConsolidatedAll(input);
            if (returnOutput.TotalCount == 0)
            {
                throw new UserFriendlyException(L("NoRecordsFound"));
            }
            var items = returnOutput.Items;

            input.DateRange = "From: " + input.StartDate.Value.ToString("dd-MMM-yyyy") + " To: " + input.EndDate.Value.ToString("dd-MMM-yyyy");
            string fileName = L("MenuWastage") + " " + input.DateRange + " " + input.LocationInfo.Code + " " + input.LocationInfo.Name;

            return CreateExcelPackage(fileName + ".xlsx",
            excelPackage =>
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("MenuWastage"));
                sheet.OutLineApplyStyle = true;
                int row = 1;
                AddHeaderWithMergeWithColumnCount(sheet, row, 1, 6, L("MenuWastage"));
                sheet.Row(row).Height = 30;
                sheet.Cells[row, 1].Style.Font.Size = 15;
                row++;
                AddHeaderWithMergeWithColumnCount(sheet, row, 2, 1, L("Location") + " " + L("Code") + ":" + input.LocationInfo.Code);
                AddHeaderWithMergeWithColumnCountAndAlignment(sheet, row, 4, 1, input.LocationInfo.Name,OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                row++;
                AddHeaderWithMergeWithColumnCount(sheet, row, 1, 6, input.DateRange);
                row++;
                AddHeaderWithMergeWithColumnCount(sheet, row, 1, 1, L("PrintedOn"));
                AddHeaderWithMergeWithColumnCountAndAlignment(sheet, row, 3, 1, input.PrintedTime.ToString("dd-MMM-yyyy"), OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                row++;
                AddHeaderWithMergeWithColumnCount(sheet, row, 1, 1, L("PrintedBy"));
                AddHeaderWithMergeWithColumnCountAndAlignment(sheet, row, 3, 1, input.PreparedUserName, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                row++;

                AddHeader(sheet, row, L("Date"), L("EntryMode"), L("Wastage") + " " + L("Qty"), L("TotalAmount"), L("Time"), L("User"), L("Remarks"));
                row++;
                foreach (var gp in items.GroupBy(t=>t.PosMenuPortionRefName))
                {
                    var firstRecord = gp.FirstOrDefault();
                    AddHeaderWithMergeWithColumnCountAndAlignment(sheet, row, 1, 6, firstRecord.PosMenuPortionCode + " " + firstRecord.PosMenuPortionRefName,OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                    row++;
                    foreach (var det in gp.ToList())
                    {
                        int col = 1;
                        AddDetail(sheet, row, col++, det.SalesDate.ToString("dd-MMM-yyyy"));
                        AddDetail(sheet, row, col++, det.MenuWastageMode);
                        AddDetailWithNumberFormat(sheet, row, col++, det.WastageQty, "#0.0000");
                        AddDetailWithNumberFormat(sheet, row, col++, det.TotalAmount,"#0.0000");
                        AddDetail(sheet, row, col++, det.SalesDate.ToString("HH:mm:ss"));
                        AddDetail(sheet, row, col++, det.UserName);
                        AddDetail(sheet, row, col++, det.WastageRemarks);
                        row++;
                    }
                    row++;
                }
                row++;
                for (var i = 1; i <= 25; i++)
                {
                    sheet.Column(i).AutoFit();
                }


            });

        }
    }
}
