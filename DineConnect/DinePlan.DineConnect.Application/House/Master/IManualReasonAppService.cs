﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master
{
    public interface IManualReasonAppService : IApplicationService
    {
        Task<PagedResultOutput<ManualReasonListDto>> GetAll(GetManualReasonInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetManualReasonForEditOutput> GetManualReasonForEdit(NullableIdInput nullableIdInput);
        Task<ManualReason> CreateOrUpdateManualReason(CreateOrUpdateManualReasonInput input);
        Task DeleteManualReason(IdInput input);
        Task<ListResultOutput<ComboboxItemDto>> GetManualReasonCategoryForCombobox();
        Task<ListResultOutput<ManualReasonListDto>> GetManualReasonNames(FilterManualReasonDto input);
    }
}