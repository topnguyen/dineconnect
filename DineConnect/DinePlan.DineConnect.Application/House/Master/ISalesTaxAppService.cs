﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master
{
    public interface ISalesTaxAppService : IApplicationService
    {
        Task<PagedResultOutput<SalesTaxListDto>> GetAll(GetSalesTaxInput inputDto);
        Task<FileDto> GetAllToExcel(GetSalesTaxInput input);
        Task<GetSalesTaxForEditOutput> GetSalesTaxForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateSalesTax(CreateOrUpdateSalesTaxInput input);
        Task DeleteSalesTax(IdInput input);

        Task<ListResultOutput<SalesTaxListDto>> GetSalesTaxNames();
    }
}