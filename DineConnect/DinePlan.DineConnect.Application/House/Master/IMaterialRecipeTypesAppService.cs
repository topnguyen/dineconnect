﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master
{
    public interface IMaterialRecipeTypesAppService : IApplicationService
    {
        Task<PagedResultOutput<MaterialRecipeTypesListDto>> GetAll(GetMaterialRecipeTypesInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetMaterialRecipeTypesForEditOutput> GetMaterialRecipeTypesForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateMaterialRecipeTypes(CreateOrUpdateMaterialRecipeTypesInput input);
        Task DeleteMaterialRecipeTypes(IdInput input);

        Task<ListResultOutput<MaterialRecipeTypesListDto>> GetIds();
    }
}
