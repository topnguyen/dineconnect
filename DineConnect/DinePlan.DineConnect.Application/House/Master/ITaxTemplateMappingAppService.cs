﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master
{
    public interface ITaxTemplateMappingAppService : IApplicationService
    {
        Task<PagedResultOutput<TaxTemplateMappingListDto>> GetAll(GetTaxTemplateMappingInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetTaxTemplateMappingForEditOutput> GetTaxTemplateMappingForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateTaxTemplateMapping(CreateOrUpdateTaxTemplateMappingInput input);
        Task DeleteTaxTemplateMapping(IdInput input);

        Task<ListResultOutput<TaxTemplateMappingListDto>> GetPosMenuPortionRefIds();
    }
}
