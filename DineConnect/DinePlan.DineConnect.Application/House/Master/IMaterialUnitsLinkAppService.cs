﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master
{
    public interface IMaterialUnitsLinkAppService : IApplicationService
    {
        Task<PagedResultOutput<MaterialUnitsLinkListDto>> GetAll(GetMaterialUnitsLinkInput inputDto);
        Task<PagedResultOutput<MaterialUnitsLinkViewDto>> GetView(GetMaterialUnitsLinkInput input);

        Task<FileDto> GetAllToExcel();
        Task<GetMaterialUnitsLinkForEditOutput> GetMaterialUnitsLinkForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateMaterialUnitsLink(CreateOrUpdateMaterialUnitsLinkInput input);
        Task DeleteMaterialUnitsLink(IdInput input);

        Task<ListResultOutput<MaterialUnitsLinkListDto>> GetMaterialNames();
    }
}