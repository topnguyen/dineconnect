﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master
{
    public interface IUserDefaultOrganizationAppService : IApplicationService
    {
        Task<PagedResultOutput<UserDefaultOrganizationListDto>> GetAll(GetUserDefaultOrganizationInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetUserDefaultOrganizationForEditOutput> GetUserDefaultOrganizationForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateUserDefaultOrganization(CreateOrUpdateUserDefaultOrganizationInput input);
        Task DeleteUserDefaultOrganization(IdInput input);

        Task<ListResultOutput<UserDefaultOrganizationListDto>> GetIds();
		Task<UserDefaultOrganizationListDto> GetUserInfo(IdInput input);


	}
}