﻿
using System.Collections.Generic;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;


namespace DinePlan.DineConnect.House.Master
{
    public interface ISupplierMaterialListExcelExporter
    {
        FileDto ExportToFile(List<SupplierMaterialViewDto> dtos);
		FileDto PurchaseProjectionExportToFile(PurchaseProjectionDto input);
		
	}
}