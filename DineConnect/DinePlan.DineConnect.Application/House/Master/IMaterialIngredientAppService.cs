﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master
{
    public interface IMaterialIngredientAppService : IApplicationService
    {
        Task<PagedResultOutput<MaterialIngredientListDto>> GetAll(GetMaterialIngredientInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetMaterialIngredientForEditOutput> GetMaterialIngredientForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateMaterialIngredient(CreateOrUpdateMaterialIngredientInput input);
        Task DeleteMaterialIngredient(IdInput input);

        Task<ListResultOutput<MaterialIngredientListDto>> GetIds();
        Task<PagedResultOutput<MaterialIngredientViewDto>> GetViewByRefRecipeId(IdInput input);

    }
}