﻿using System.Collections.Generic;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.House.Transaction.Implementation;
using System;
using DinePlan.DineConnect.Connect.Master.Dtos;

namespace DinePlan.DineConnect.House.Master
{
    public interface IHouseReportListExcelExporter
    {
        FileDto ExportToFile(List<HouseReportListDto> dtos);
        FileDto CloseStockToExcelFile(List<MaterialLedgerDto> dtos);
		//FileDto RateViewToExcelFile(GetMaterialRateViewDtoOutput input);
        Task<FileDto> RateViewToExcelFile(GetMaterialRateViewDtoOutput input);

        FileDto SupplierRateViewToExcelFile(List<SupplierRateViewListDto> dtos);
		FileDto ProductionUnitCostingReportExcel(ProductionUnitCostingDtoConsolidated dtos);
        FileDto StockToExcelFile(List<MaterialLedgerDto> dtos, string location, string dateRange, decimal salesAmountWithOutTax,
            List<InterTransferReportDetailOutPut> locTransferOut, List<InterTransferReportDetailOutPut> locTrasferIn);
        FileDto MenuCostToExcelFile(MenuItemCostingDtoConsolidated dtos);
        FileDto CostOfSalesToExcelFile(CostOfSalesReport dtos);
		Task<FileDto> ProductAnalysisToExcelFile(GetProductAnalysisViewDtoOutput input);
		List<MaterialAdjustmentStockDto> ReadFromExcelFile(string filename);
        Task<FileDto> ExportToFilePurchaseSummary(InputInvoiceAnalysis input, InvoiceAppService appService);
		Task<FileDto> ExportToFilePurchaseConsolidatedSummary(InputInvoiceAnalysis input, InvoiceAppService appService);
		FileDto StockTakenToExcelFile(List<MaterialViewDto> dtos, string locationName, DateTime stockDate,bool multipleUomAllowed);
		FileDto ClosingStockTakenToExcelFile(List<MaterialViewDto> matDetailDtos, 
			List<MaterialViewDto> matConsolidatedDtos, string locationName, DateTime stockDate, LocationWithDate input, List<ClosingStockDetailViewDto> closingCustomReportDetails);
		FileDto VarianceReportToExcelFile(List<VarianceReportDto> dtos, DateTime FromDate, DateTime ToDate, string UserName);

        Task<List<FileDto>> MaterialWastageReportToExcelFile(OverAllMaterialWastageReport output, InputAdjustmentReport input);

        Task<List<FileDto>> MaterialWastageReportToExport(OverAllMaterialWastageReport dtos, InputAdjustmentReport input, string UserName);

        FileDto ClosingStockVarrianceToExcelFile(List<ClosingStockVarrianceDto> matDetailDtos,string locationName, DateTime stockDate, bool materialCodeExists);

        FileDto ConsolidatedLocationWiseClosingStockVarrianceToExcelFile(List<LocationListDto> locations, List<Material> materials,
            List<ClosingStockVarrianceDto> matDetailDtos, DateTime stockDate, bool materialCodeExists, List<Unit> rsUnits);

        FileDto StockToExcelFile_MergedIntoSingleSheet(List<MaterialLedgerDto> dtos, string location, string dateRange, decimal salesAmountWithOutTax,
            List<InterTransferReportDetailOutPut> locTransferOut, List<InterTransferReportDetailOutPut> locTrasferIn);

        Task<string> ExportWorkbookToPdf(string workbookPath, string outputPath);

        //FileDto VarianceReportBeforeOrAfterDayClose(List<MaterialLedgerDto> matDetailDtos,
        // string locationName, DateTime stockStartDate, DateTime stockEndDate, bool materialCodeExists, string beforeOrAfterStatus, int exportType);
        Task<FileDto> VarianceReportBeforeOrAfterDayClose(List<MaterialLedgerDto> matDetailDtos,
         string locationName, DateTime stockStartDate, DateTime stockEndDate, bool materialCodeExists, string beforeOrAfterStatus, int exportType);

        FileDto ExportClosingStockGroupCategoryMaterials(LocationWiseClosingStockGroupCateogryMaterialDto input);

        FileDto ExportInterTransferReceivedDetailReportForGivenMaterialWise(InterTransferReceivedExportReportDto input);

        FileDto ExportMaterialTrackReportForGivenMaterialWise(MaterialTrackExportReportDto input);

    }
}