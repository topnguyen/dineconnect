﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.House.Master
{
    public interface IMaterialLocationWiseStockAppService : IApplicationService
    {
        Task<PagedResultOutput<MaterialLocationWiseStockListDto>> GetAll(GetMaterialLocationWiseStockInput inputDto);
        Task<FileDto> GetAllToExcel(GetMaterialLocationWiseStockInput input);
        Task<GetMaterialLocationWiseStockForEditOutput> GetMaterialLocationWiseStockForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateMaterialLocationWiseStock(CreateOrUpdateMaterialLocationWiseStockInput input);
        Task DeleteMaterialLocationWiseStock(IdInput input);
        Task<PagedResultOutput<MaterialLocationWiseStockViewDto>> GetView(GetMaterialLocationWiseStockInput inputDto);
        Task<FileDto> GetStockExcel(IdInput input);
        Task<PagedResultOutput<MaterialAllLocationWiseStockViewDto>> GetAllLocationView(GetMaterialLocationWiseStockInput input);
    }
}