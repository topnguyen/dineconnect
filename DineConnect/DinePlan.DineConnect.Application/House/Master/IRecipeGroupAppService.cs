﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master
{
    public interface IRecipeGroupAppService : IApplicationService
    {
        Task<PagedResultOutput<RecipeGroupListDto>> GetAll(GetRecipeGroupInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetRecipeGroupForEditOutput> GetRecipeGroupForEdit(NullableIdInput nullableIdInput);
        Task<IdInput> CreateOrUpdateRecipeGroup(CreateOrUpdateRecipeGroupInput input);
        Task DeleteRecipeGroup(IdInput input);

        Task<ListResultOutput<RecipeGroupListDto>> GetRecipeGroupNames();
    }
}