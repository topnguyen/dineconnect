﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master
{
    public interface ISalesTaxTemplateMappingAppService : IApplicationService
    {
        Task<PagedResultOutput<SalesTaxTemplateMappingListDto>> GetAll(GetSalesTaxTemplateMappingInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetSalesTaxTemplateMappingForEditOutput> GetSalesTaxTemplateMappingForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateSalesTaxTemplateMapping(CreateOrUpdateSalesTaxTemplateMappingInput input);
        Task DeleteSalesTaxTemplateMapping(IdInput input);

        Task<ListResultOutput<SalesTaxTemplateMappingListDto>> GetSosMenuPortionRefIds();
    }
}
