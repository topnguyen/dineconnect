﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.House.Master
{
    public interface ICustomerTagMaterialPriceAppService : IApplicationService
    {
        Task<PagedResultOutput<CustomerTagMaterialPriceListDto>> GetAll(GetCustomerTagMaterialPriceInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetCustomerTagMaterialPriceForEditOutput> GetCustomerTagMaterialPriceForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateCustomerTagMaterialPrice(CreateOrUpdateCustomerTagMaterialPriceInput input);
        Task DeleteCustomerTagMaterialPrice(IdInput input);

        Task<ListResultOutput<CustomerTagMaterialPriceListDto>> GetIds();

        Task<List<CustomerTagMaterialPriceViewEditDto>> GetMaterialPriceForEdit(GetCustomerTagMaterialPriceInput input);

        Task<CustomerTagMaterialPriceArrayDto> GetMaterialPriceForEditInArray(GetCustomerTagMaterialPriceInput input);

        Task<GetTagMaterialDynamic> GetMaterialPriceDynamic(GetCustomerTagMaterialPriceInput input);
        Task UpdateCustomerMaterialPrice(GetTagMaterialDynamic input);
        Task<List<CustomerTagMaterialPriceEditDto>> GetMaterialPriceForGivenTag(GetCustomerTagMaterialPriceInput input);
        Task UpdateCustomerMaterialPriceForGivenTag(CreateOrUpdateCustomerTagPriceDefinitionInput inputDto);

    }
}
