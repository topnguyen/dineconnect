﻿using System.Collections.Generic;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Master.Dtos;

namespace DinePlan.DineConnect.House.Master
{
    public interface IBrandListExcelExporter
    {
        FileDto ExportToFile(List<BrandListDto> dtos);
    }
}
