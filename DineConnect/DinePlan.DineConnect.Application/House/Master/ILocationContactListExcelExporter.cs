﻿using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master.Exporter
{
    public interface ILocationContactListExcelExporter
    {
        FileDto ExportToFile(List<LocationContactListDto> dtos);
    }
}