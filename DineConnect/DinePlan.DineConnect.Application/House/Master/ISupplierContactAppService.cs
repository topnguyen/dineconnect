﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master
{
    public interface ISupplierContactAppService : IApplicationService
    {
        Task<PagedResultOutput<SupplierContactListDto>> GetAll(GetSupplierContactInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetSupplierContactForEditOutput> GetSupplierContactForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateSupplierContact(CreateOrUpdateSupplierContactInput input);
        Task DeleteSupplierContact(IdInput input);

        Task<ListResultOutput<SupplierContactListDto>> GetIds();
        Task<PagedResultOutput<SupplierContactViewDto>> GetView(GetSupplierContactInput input);
    }
}