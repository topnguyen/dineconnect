﻿
using System.Collections.Generic;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;


namespace DinePlan.DineConnect.House.Master
{
    public interface IMaterialMenuMappingListExcelExporter
    {
        FileDto ExportToFile(List<MaterialMenuMappingListDto> dtos);

        FileDto ExportParticularMaterialMenuMappingToFile(List<MaterialMenuMappingListDto> dtos);
    }
}
