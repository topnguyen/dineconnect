﻿
using System.Collections.Generic;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using System;

namespace DinePlan.DineConnect.House.Master
{
    public interface IMaterialLocationWiseStockListExcelExporter
    {
        FileDto ExportToFile(List<MaterialLocationWiseStockViewDto> dtos);
        FileDto StockToExcelFile(List<MaterialLocationWiseStockViewDto> dtos, string locationName, DateTime stockDate);
    }
}