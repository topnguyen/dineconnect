﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Master
{
    public interface ICustomerTagDefinitionAppService : IApplicationService
    {
        Task<PagedResultOutput<CustomerTagDefinitionListDto>> GetAll(GetCustomerTagDefinitionInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetCustomerTagDefinitionForEditOutput> GetCustomerTagDefinitionForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateCustomerTagDefinition(CreateOrUpdateCustomerTagDefinitionInput input);
        Task DeleteCustomerTagDefinition(IdInput input);

        Task<ListResultOutput<CustomerTagDefinitionListDto>> GetTagCodes();
        Task AddTagIntoAllMaterial();

        Task CreateCustomerTagForTenant(int tenantId);

        Task<ListResultOutput<ComboboxItemDto>> GetCustomerTagDefinitionForCombobox();

        Task<ListResultOutput<ComboboxItemDto>> GetCustomerTagForCombobox();

    }
}
