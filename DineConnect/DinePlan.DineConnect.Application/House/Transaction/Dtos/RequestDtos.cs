﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.House.Transaction.Dtos
{
    [AutoMapFrom(typeof(Request))]
    public class RequestListDto : FullAuditedEntityDto
    {
        public int LocationRefId { get; set; }
        public string RequestSlipNumber { get; set; }
        public DateTime RequestTime { get; set; }
        public int RequestPersonId { get; set; }
        //public int? RecipeRefId { get; set; }

        //public decimal? RecipeProductionQty { get; set; }
        public int MaterialGroupCategoryRefId { get; set; }
        public int? ProductionUnitRefId { get; set; }
        public string ProductionUnitRefName { get; set; }
        public bool CompletedStatus { get; set; }

        //public bool MultipleBatchProductionAllowed { get; set; }
        public int TenantId { get; set; }
    }
    [AutoMapTo(typeof(Request))]
    public class RequestEditDto
    {
        public int? Id { get; set; }
        public int LocationRefId { get; set; }
        public string RequestSlipNumber { get; set; }
        public DateTime RequestTime { get; set; }
        public int RequestPersonId { get; set; }
        //public int? RecipeRefId { get; set; }

        //public decimal? RecipeProductionQty { get; set; }
        public int MaterialGroupCategoryRefId { get; set; }

        public int? ProductionUnitRefId { get; set; }
        public bool CompletedStatus { get; set; }

        //public bool MultipleBatchProductionAllowed { get; set; }
        public int TenantId { get; set; }
    }

    [AutoMapFrom(typeof(Request))]
    public class RequestListWithNameDto : FullAuditedEntityDto
    {
        public int LocationRefId { get; set; }
        public string LocationName { get; set; }
        public int RequestSlipNumber { get; set; }
        public DateTime RequestTime { get; set; }
        public int RequestPersonId { get; set; }
        //public int? RecipeRefId { get; set; }
        //public decimal? RecipeProductionQty { get; set; }
        //public bool MultipleBatchProductionAllowed { get; set; }
        public int? ProductionUnitRefId { get; set; }
        public int TenantId { get; set; }
    }

    [AutoMapTo(typeof(RequestRecipeDetail))]
    public class RequestRecipeDetailEditDto
    {
        public int? Id { get; set; }
        public int RequestRefId { get; set; }
        public int Sno { get; set; }
        public int RecipeRefId { get; set; }
        public decimal RecipeProductionQty { get; set; }
        public int UnitRefId { get; set; }
        public bool MultipleBatchProductionAllowed { get; set; }
        public string Remarks { get; set; }
    }

    [AutoMapTo(typeof(RequestRecipeDetail))]
    public class RequestRecipeDetailListDto
    {
        public int Id { get; set; }
        public int RequestRefId { get; set; }
        public int Sno { get; set; }
        public int RecipeRefId { get; set; }
        public string RecipeRefName { get; set; }
        public decimal RecipeProductionQty { get; set; }
        public string Uom { get; set; }
        public bool MultipleBatchProductionAllowed { get; set; }
        public string Remarks { get; set; }
        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }
        public decimal CurrentInHand { get; set; }
        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }

    }


    [AutoMapTo(typeof(RequestDetail))]
    public class RequestDetailViewDto
    {
        public int? Id { get; set; }
        public int RequestRefId { get; set; }
        public int Sno { get; set; }
        //public int? RecipeRefId { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public decimal RequestQty { get; set; }
        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }
        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }
        public decimal CurrentInHand { get; set; }
    }


    public class GetRequestInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }
        public int DefaultLocationRefId { get; set; }

        public int? RequestSlipNumber { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }

    public class GetRequestForStandardRecipe : IOutputDto
    {
        public List<RequestDetailViewDto> RequestDetail { get; set; }
    }

    public class GetRequestForEditOutput : IOutputDto
    {
        public RequestEditDto Request { get; set; }
        public List<RequestRecipeDetailListDto> RequestRecipeDetail { get; set; }
        public List<RequestDetailViewDto> RequestDetail { get; set; }
    }
    public class CreateOrUpdateRequestInput : IInputDto
    {
        [Required]
        public RequestEditDto Request { get; set; }
        public List<RequestRecipeDetailListDto> RequestRecipeDetail { get; set; }
        public List<RequestDetailViewDto> RequestDetail { get; set; }
    }
}

