﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using DinePlan.DineConnect.House.Temp;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Location;

namespace DinePlan.DineConnect.House.Transaction.Dtos
{
    [AutoMapFrom(typeof(Invoice))]
    public class InvoiceListDto : FullAuditedEntityDto
    {
       
        public int LocationRefId { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string InvoiceMode { get; set; }
        public int InvoicePayMode { get; set; }
        public DateTime? AccountDate { get; set; }
        public int SupplierRefId { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierRefName { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime DcFromDate { get; set; }
        public DateTime DcToDate { get; set; }
        public decimal TotalDiscountAmount { get; set; }
        public decimal TotalShipmentCharges { get; set; }
        public decimal RoundedAmount { get; set; }
        public decimal InvoiceAmount { get; set; }
        public bool IsDirectInvoice { get; set; }
        public decimal AmountWithoutTax { get; set; }
        public decimal TaxAmount { get; set; }
		public int? PurchaseCategoryRefId { get; set; }
        public string PurchaseCategoryName { get; set; }
        public string SupplierAddOn { get; set; }
        public string LocationAddOn { get; set; }
        public string LocationCode { get; set; }
    }

    [AutoMapTo(typeof(Invoice))]
    public class InvoiceEditDto
    {
        public int? Id { get; set; }
        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string InvoiceMode { get; set; }
        public int InvoicePayMode { get; set; }
        public DateTime? AccountDate { get; set; }
        public DateTime? DcDate { get; set; }
        public int SupplierRefId { get; set; }
        public string SupplierRefName { get; set; }
        public bool SupplierTaxApplicableByDefault { get; set; }
        public string SupplierCode { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime DcFromDate { get; set; }
        public DateTime DcToDate { get; set; }
        public decimal SubTotalAmount { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal TotalDiscountAmount { get; set; }
        public decimal TotalShipmentCharges { get; set; }
        public decimal RoundedAmount { get; set; }
        public decimal InvoiceAmount { get; set; }
        public bool IsDirectInvoice { get; set; }
		public int? PurchaseCategoryRefId { get; set; }
        public string PurchaseOrderReferenceFromOtherErp { get; set; }
        public string InvoiceNumberReferenceFromOtherErp { get; set; }
        //public string PoReferenceCodes { get; set; }
        public int CreatorUserId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreationTime { get; set; }
    }

   

    [AutoMapFrom(typeof(InvoiceDetail))]
    public class InvoiceDetailViewDto : FullAuditedEntityDto
    {
        public int InvoiceRefId { get; set; }
        public Invoice Invoices { get; set; }
        public int Sno { get; set; }
        public int DcTranId { get; set; }
        public int? PoRefId { get; set; }
        public string Barcode { get; set; }
        public int MaterialRefId { get; set; }
        public int MaterialGroupRefId { get; set; }
        public int MaterialGroupCategoryRefId { get; set; }
        public string MaterialPetName { get; set; }
        public string MaterialRefName { get; set; }
        public string Hsncode { get; set; }
        public decimal TotalQty { get; set; }
        //public decimal ReceivedQtyInUnit { get; set; }
        public int DefaultUnitId { get; set; }
        public string Uom { get; set; }
        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }
        public decimal Price { get; set; }
        public decimal TotalAmount { get; set; }
        public string DiscountFlag { get; set; }
        public decimal DiscountPercentage { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal NetAmount { get; set; }
        public string Remarks { get; set; }
        public List<TaxForMaterial> TaxForMaterial { get; set; }
        public List<ApplicableTaxesForMaterial> ApplicableTaxes { get; set; }
        public decimal? YieldPercentage { get; set; }
        public string YieldMode { get; set; }
        public decimal? YieldExcessShortageQty { get; set; }
        public string InvoiceNumber { get; set; }
        public string PurchaseOrderReferenceFromOtherErp { get; set; }
        public string InvoiceNumberReferenceFromOtherErp { get; set; }

        public string SupplierMaterialAliasName { get; set; }
    }

    [AutoMapTo(typeof(InvoiceDetail))]
    public class InvoiceDetailEditDto 
    {
        public int? Id { get; set; }
        public int InvoiceRefId { get; set; }
        public Invoice Invoices { get; set; }
        public int Sno { get; set; }
        public int MaterialRefId { get; set; }
        public decimal TotalQty { get; set; }
        //public decimal ReceivedQtyInUnit { get; set; }
        public int UnitRefId { get; set; }
        public decimal Price { get; set; }
        public decimal TotalAmount { get; set; }
        public string DiscountFlag { get; set; }
        public decimal DiscountPercentage { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal NetAmount { get; set; }
        public string Remarks { get; set; }
        public List<TaxForMaterial> TaxForMaterial { get; set; }
        public List<ApplicableTaxesForMaterial> ApplicableTaxes { get; set; }
        public decimal? YieldPercentage { get; set; }
        public string YieldMode { get; set; }
        public decimal? YieldExcessShortageQty { get; set; }

    }

    [AutoMapFrom(typeof(InvoiceTaxDetail))]
    public class InvoiceTaxDetailViewDto : FullAuditedEntityDto
    {
        public int InvoiceRefId { get; set; }
        public int Sno { get; set; }
        public int MaterialRefId { get; set; }
        public int TaxRefId { get; set; }
        public decimal TaxRate { get; set; }
        public decimal TaxValue { get; set; }
    }

    [AutoMapFrom(typeof(InvoiceDirectCreditLink))]
    public class InvoiceDirectCreditLinkViewDto : FullAuditedEntityDto
    {
        public int InvoiceRefId { get; set; }
        public int Sno { get; set; }
        public int MaterialRefId { get; set; }
        public int InwardDirectCreditRefId { get; set; }
		public int UnitRefId { get; set; }
        public decimal MaterialConvertedQty { get; set; }
        public int? PoRefId { get; set; }
    }

    public class GetInvoiceInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public List<LocationListDto> Locations { get; set; }
        public int SupplierRefId { get; set; }
        public string SupplierName { get; set; }
        public string InvoiceNumber { get; set; }
        public int LocationRefId { get; set; }

        public bool TaxShowFlag { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetInvoiceForEditOutput : IOutputDto
    {
        public InvoiceEditDto Invoice { get; set; }
        public List<InvoiceDetailViewDto> InvoiceDetail { get; set; }
        public List<InvoiceDirectCreditLinkViewDto> InvoiceDirectCreditLink { get; set; }
    }

    //For Sample Invoices
    public class CreateOrUpdateSampleInvoiceInput : IInputDto
    {
        [Required]
        public InvoiceEditDto Invoice { get; set; }
    }


    public class CreateOrUpdateInvoiceInput : IInputDto
    {
        [Required]
        public InvoiceEditDto Invoice { get; set; }

        [Required]
        public List<InvoiceDetailViewDto> InvoiceDetail { get; set; }

        public List<InvoiceDirectCreditLinkViewDto> InvoiceDirectCreditLink { get; set; }

    }

	public class GetInvoiceDraftReference : IOutputDto
	{
		public int Id { get; set; }
		public string InvoiceDraftReference { get; set; }
	}

	public class GetDcMaterialInfo : IInputDto
    {
        public int SupplierRefId { get; set; }
        public int CustomerRefId { get; set; }
        public int DcRefId { get; set; }
        public int DeliveryRefId { get; set; }
        public int LocationRefId { get; set; }
    }

    public class GetPoMaterialInfo : IInputDto
    {
        public int SupplierRefId { get; set; }
        public int CustomerRefId { get; set; }
        public int PoRefId { get; set; }
        public int DeliveryRefId { get; set; }
        public int LocationRefId { get; set; }
    }

    public class GetDcMaterialDataDetail: IInputDto
    {
        public DateTime? DeliverDateRequested { get; set; }
        [Required]
        public List<InvoiceDetailViewDto> DcDetail { get; set; }
        [Required]
        public List<SalesInvoiceDetailViewDto> SalesDcDetail { get; set; }
    }

    public class InputInvoiceDetailExcel
    {
        public List<GetInvoiceForEditOutput> Dtos;
    }


    public class InvoiceWithPoAndCategoryOutputDto
    {
        public List<InvoiceCategoryWisePurchaseOrderAnalysisDto> InvoiceCategoryWisePurchaseOrderAnalysisDtoList { get; set; }
    }


    public class SupplierWisePurchaseGroupCateogryMaterialDto
    {
        public List<MaterialWisePurchaseDto> MaterialWisePurchaseDtos { get; set; }
        public List<MaterialGroupWisePurchaseDto> MaterialGroupWisePurchaseDtos { get; set; }
        public List<MaterialGroupCategoryWisePurchaseDto> MaterialGroupCategoryWisePurchaseDtos { get; set; }
        public List<SupplierWiseHeadWisePurchase> SupplierWiseHeadWisePurchaseList { get; set; }
        public SupplierWisePurchaseGroupCateogryMaterialDto()
        {
            MaterialWisePurchaseDtos = new List<MaterialWisePurchaseDto>();
            MaterialGroupWisePurchaseDtos = new List<MaterialGroupWisePurchaseDto>();
            MaterialGroupCategoryWisePurchaseDtos = new List<MaterialGroupCategoryWisePurchaseDto>();
        }
        public PurchaseAnalysisGroupCategoryMaterialDto InputFilterDto { get; set; }
    }

    public class LocationWiseClosingStockGroupCateogryMaterialDto
    {
        public List<MaterialWisePurchaseDto> MaterialWisePurchaseDtos { get; set; }
        public List<MaterialGroupWisePurchaseDto> MaterialGroupWisePurchaseDtos { get; set; }
        public List<MaterialGroupCategoryWisePurchaseDto> MaterialGroupCategoryWisePurchaseDtos { get; set; }
        public List<LocationWiseHeadWiseStock> LocationWiseHeadWiseStockList { get; set; }
        public LocationWiseClosingStockGroupCateogryMaterialDto()
        {
            MaterialWisePurchaseDtos = new List<MaterialWisePurchaseDto>();
            MaterialGroupWisePurchaseDtos = new List<MaterialGroupWisePurchaseDto>();
            MaterialGroupCategoryWisePurchaseDtos = new List<MaterialGroupCategoryWisePurchaseDto>();
        }
        public ClosingStockExportGroupCategoryMaterialDto InputFilterDto { get; set; }
    }

    public class SupplierWiseHeadWisePurchase
    {
        public int SupplierRefId { get; set; }
        public string SupplierName { get; set; }
        public List<MaterialWisePurchaseDto> MaterialWisePurchaseDtos { get; set; }
        public List<MaterialGroupWisePurchaseDto> MaterialGroupWisePurchaseDtos { get; set; }
        public List<MaterialGroupCategoryWisePurchaseDto> MaterialGroupCategoryWisePurchaseDtos { get; set; }

        public SupplierWiseHeadWisePurchase()
        {
            MaterialWisePurchaseDtos = new List<MaterialWisePurchaseDto>();
            MaterialGroupWisePurchaseDtos = new List<MaterialGroupWisePurchaseDto>();
            MaterialGroupCategoryWisePurchaseDtos = new List<MaterialGroupCategoryWisePurchaseDto>();
        }
    }

    public class LocationWiseHeadWiseStock
    {
        public int LocationRefId { get; set; }
        public string LocationRefCode { get; set; }
        public string LocationRefName { get; set; }
        public List<MaterialWisePurchaseDto> MaterialWisePurchaseDtos { get; set; }
        public List<MaterialGroupWisePurchaseDto> MaterialGroupWisePurchaseDtos { get; set; }
        public List<MaterialGroupCategoryWisePurchaseDto> MaterialGroupCategoryWisePurchaseDtos { get; set; }

        public LocationWiseHeadWiseStock()
        {
            MaterialWisePurchaseDtos = new List<MaterialWisePurchaseDto>();
            MaterialGroupWisePurchaseDtos = new List<MaterialGroupWisePurchaseDto>();
            MaterialGroupCategoryWisePurchaseDtos = new List<MaterialGroupCategoryWisePurchaseDto>();
        }
    }


    public class MaterialGroupWisePurchaseDto
    {
        public int MaterialGroupRefId { get; set; }
        public string MaterialGroupRefName { get; set; }
        public decimal TotalAmount { get; set; }
    }

    public class MaterialGroupCategoryWisePurchaseDto
    {
        public int MaterialGroupCategoryRefId { get; set; }
        public string MaterialGroupCategoryRefName { get; set; }
        public decimal TotalAmount { get; set; }
    }

    public class MaterialWisePurchaseDto
    {
        public int MaterialRefId { get; set; }
        public int MaterialGroupCategoryRefId { get; set; }
        public int MaterialGroupRefId { get; set; }
        public string MaterialRefName { get; set; }
        public decimal TotalAmount { get; set; }
    }


    public class PurchaseAnalysisOutputDto 
	{
		public List<InvoiceAnalysisDto> InvoiceList { get; set; }
	}

    public class InputInvoiceAnalysis
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int LocationRefId { get; set; }

        public int SupplierRefId { get; set; }

        public int MaterialRefId { get; set; }
        public string InvoiceNumber { get; set; }

        public bool ExactSearchFlag { get; set; }
        public int InvoiceId { get; set; }

        public bool ShowAlreadyExported { get; set; }

        public bool UpdateExportCompletedFlag { get; set; }

    }

    public class PurchaseAnalysisGroupCategoryMaterialDto : IInputDto
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int LocationRefId { get; set; }

        public PurchaseAnalysisGroupCategoryMaterialDto()
        {
            SupplierList = new List<ComboboxItemDto>();
            GroupList = new List<ComboboxItemDto>();
            CategoryList = new List<ComboboxItemDto>();
            MaterialList = new List<ComboboxItemDto>();
            Locations = new List<SimpleLocationDto>();
            LocationTags = new List<LocationTag>();
        }

        public List<ComboboxItemDto> SupplierList { get; set; }
        public List<ComboboxItemDto> GroupList { get; set; }
        public List<ComboboxItemDto> CategoryList { get; set; }
        public List<ComboboxItemDto> MaterialList { get; set; }
        public List<SimpleLocationDto> Locations { get; set; }

        public LocationGroupDto LocationGroup { get; set; }

        public List<LocationTag> LocationTags { get; set; }
        public int UserId { get; set; }
        public bool ExportMaterialAlso { get; set; }
    }

    public class ClosingStockExportGroupCategoryMaterialDto : IInputDto
    {
        public bool RunInBackGround { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int LocationRefId { get; set; }

        public ClosingStockExportGroupCategoryMaterialDto()
        {
            GroupList = new List<ComboboxItemDto>();
            CategoryList = new List<ComboboxItemDto>();
            MaterialList = new List<ComboboxItemDto>();
            Locations = new List<SimpleLocationDto>();
            LocationTags = new List<LocationTag>();
        }

        public List<ComboboxItemDto> GroupList { get; set; }
        public List<ComboboxItemDto> CategoryList { get; set; }
        public List<ComboboxItemDto> MaterialList { get; set; }
        public List<SimpleLocationDto> Locations { get; set; }

        public LocationGroupDto LocationGroup { get; set; }

        public List<LocationTag> LocationTags { get; set; }
        public long UserId { get; set; }
        public int TenantId { get; set; }
        public bool ExportMaterialAlso { get; set; }

    }

    public class PurchaseInvoiceTaxDetailInputDto
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int LocationRefId { get; set; }
        public int SupplierRefId { get; set; }
        public int MaterialRefId { get; set; }
        public string InvoiceNumber { get; set; }
        public bool ExactSearchFlag { get; set; }
        public int InvoiceId { get; set; }
        public bool DoesChangePurchaseQuantityInDefaultUnit { get; set; }
    }

    public class PurchaseInvoiceTaxDetailOutputDto
    {
        public LocationListDto LocationInfo { get; set; }
        public string DateRange { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime PrintedTime { get; set; }
        public long? PreparedUserId { get; set; }
        public string PreparedUserName { get; set; }
        public bool CustomReportFlag { get; set; }
        public decimal TotalShipmentCharges { get; set; }
        public decimal TotalRoundedAmount { get; set; }
        public decimal  TotalDiscountAmount { get; set; }
        public decimal TotalPurchaseAmount { get; set; }
        public decimal TotalTaxAmount { get; set; }

        public List<InvoiceAnalysisDto> InvoiceDetailViewList { get; set; }
    }

    public class InputInvoiceAnalysisForCategoryWise
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int LocationRefId { get; set; }

        public int SupplierRefId { get; set; }

        public int CategoryRefId { get; set; }

    }


    public class InvoiceAnalysisDto
    {
        public int InvoiceRefId { get; set; }
        public int SupplierRefId { get; set; }
        public string SupplierRefName { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime InvoiceDate { get; set; }
        public int PurchaseOrderRefId { get; set; }
        public string PoReferenceNumber { get; set; }
        public int MaterialGroupCategoryRefId { get; set; }
        public string MaterialGroupCategoryRefName { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialPetName { get; set; }
        public string MaterialRefName { get; set; }
        public decimal TotalQty { get; set; }
        public decimal QuantityInPurchaseUnit { get; set; }  //  Purchase Unit
        public string PurchaseUnitName { get; set; }
        public decimal Price { get; set; }
        public decimal PriceForDefaultUnitForCalculation { get; set; }
        public decimal TotalAmount { get; set; }
        public string DiscountFlag { get; set; }
        public decimal DiscountPercentage { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal NetAmount { get; set; }
        public string Remarks { get; set; }
        public string Uom { get; set; }

        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }
        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }
        public string PriceForDefaultUnitWithTaxUOM { get; set; }
        public string PurchaseOrderReferenceFromOtherErp { get; set; }
        public string InvoiceNumberReferenceFromOtherErp { get; set; }
        public List<InvoiceDirectCreditLinkViewDto> InvoiceDirectCreditLink { get; set; }
    }


    public class InvoiceCategoryWisePurchaseOrderAnalysisDto
    {
        public DateTime AccountDate { get; set; }
        public int LocationRefId { get; set; }
        public string LocationRefCode { get; set; }
        public string LocationRefName { get; set; }
        public int InvoiceRefId { get; set; }
        public int SupplierRefId { get; set; }
        public string SupplierRefName { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime InvoiceDate { get; set; }
        public int? PurchaseOrderRefId { get; set; }
        public string PoReferenceNumber { get; set; }
        public int MaterialGroupRefId { get; set; }
        public string MaterialGroupRefName { get; set; }
        public int MaterialGroupCategoryRefId { get; set; }
        public string MaterialGroupCategoryRefName { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialPetName { get; set; }
        public string MaterialRefName { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal NetAmount { get; set; }
        public string Remarks { get; set; }
        public List<InvoiceDirectCreditLinkViewDto> InvoiceDirectCreditLink { get; set; }
        public DateTime? ExportDateTimeForExternalImport { get; set; }
    }


    public class InvoiceCategoryAnalysisDto
    {
        public int CategoryRefId { get; set; }
        public string CategoryRefName { get; set; }
        public decimal TotalQty { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal NetAmount { get; set; }

    }
    public class InputInvoiceDto : IInputDto

    {
        List<InvoiceListDto> InvoiceList { get; set; }
    }

    public class InputMaterialPurchaseDto : IInputDto
    {
        public DateTime EffectiveDate { get; set; }
        public int MaterialRefId { get; set; }
        public int NumberOfLastPurchaseDetail { get; set; }
    }

    public class OutputMaterialPurchaseDto  : IOutputDto
    {
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public DateTime PurchaseDate { get; set; }
        public int SupplierRefId { get; set; }
        public string SupplierRefName { get; set; }
        public decimal QuantityPurchased { get; set; }
        public decimal Price { get; set; }
        public decimal Tax { get; set; }
        public decimal PriceWithTax { get; set; }

    }

    public class DocumentDto : IInputDto
    {
        public int Id { get; set; }
        public int LocationRefId { get; set; }
        public int DocumentRefId { get; set; }
        public int SupplierRefId { get; set; }
        public int CustomerRefId { get; set; }
        public string Data { get; set; }
    }

    public class InvoiceWithAdjustmentId : IOutputDto
    {
        public int Id { get; set; }
        public int AdjustmentId { get; set; }
    }

}

