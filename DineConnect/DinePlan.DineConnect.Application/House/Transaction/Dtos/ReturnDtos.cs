﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using DinePlan.DineConnect.House.Temp;

namespace DinePlan.DineConnect.House.Transaction.Dtos
{
    [AutoMapFrom(typeof(Return))]
    public class ReturnListDto : FullAuditedEntityDto
    {
        //TODO: DTO Return Properties Missing
        //YOU CAN REFER ATTRIBUTES IN 
        public int LocationRefId { get; set; }
        public DateTime? ReturnDate { get; set; }
        public long TokenRefNumber { get; set; }
        public int? MaterialGroupCategoryRefId { get; set; }
        public int? ProductionUnitRefId { get; set; }
        public int? IssueRefId { get; set; }
        public string ProductionUnitRefName { get; set;     }

    }
    [AutoMapTo(typeof(Return))]
    public class ReturnEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO Return Properties Missing
        public int LocationRefId { get; set; }
        public DateTime? ReturnDate { get; set; }
        public long TokenRefNumber { get; set; }
        public int? MaterialGroupCategoryRefId { get; set; }
        public int? ProductionUnitRefId { get; set; }
        public int? IssueRefId { get; set; }

    }

    [AutoMapTo(typeof(ReturnDetail))]
    public class ReturnDetailViewDto
    {
        public int? Id { get; set; }
        public int ReturnRefId { get; set; }
        public int Sno { get; set; }
        public int RecipeRefId { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public decimal ReturnQty { get; set; }
        public string Remarks { get; set; }
        public int DefaultUnitId { get; set; }
        public string DefaultUnit { get; set; }
        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }
    }

    public class GetReturnInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int DefaultLocationRefId { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetReturnForEditOutput : IOutputDto
    {
        public ReturnEditDto Return { get; set; }
        public List<ReturnDetailViewDto> ReturnDetail { get; set; }
    }
    public class CreateOrUpdateReturnInput : IInputDto
    {
        [Required]
        public ReturnEditDto Return { get; set; }

        [Required]
        public List<ReturnDetailViewDto> ReturnDetail { get; set; }
    }
}

