﻿

using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using DinePlan.DineConnect.House;

namespace DinePlan.DineConnect.House.Transaction.Dtos
{
    [AutoMapFrom(typeof(SupplierDocument))]
    public class SupplierDocumentListDto : FullAuditedEntityDto
    {
        //TODO: DTO SupplierDocument Properties Missing
        //YOU CAN REFER ATTRIBUTES IN 
        public int DocumentRefId { get; set; }
        public string DocumentRefName { get; set; }
        public int SupplierRefId { get; set; }
        public string SupplierRefName { get; set; }
        public string FileName { get; set; }
        public bool IsEntryFinished { get; set; }
        public DateTime? CompletedTime { get; set; }
    }
    [AutoMapTo(typeof(SupplierDocument))]
    public class SupplierDocumentEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO SupplierDocument Properties Missing
        public int DocumentRefId { get; set; }
        public int SupplierRefId { get; set; }
        public string FileName { get; set; }
        public bool IsEntryFinished { get; set; }
        public DateTime? CompletedTime { get; set; }
    }

    public class GetSupplierDocumentInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "SupplierRefId";
            }
        }
    }
    public class GetSupplierDocumentForEditOutput : IOutputDto
    {
        public SupplierDocumentEditDto SupplierDocument { get; set; }
    }
    public class CreateOrUpdateSupplierDocumentInput : IInputDto
    {
        [Required]
        public SupplierDocumentEditDto SupplierDocument { get; set; }
    }
    public class SupplierDocumentDto : IInputDto
    {
        public int Id { get; set; }
        public int DocumentRefId { get; set; }
        public int SupplierRefId { get; set; }
        public string Data { get; set; }
    }
}

