﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using DinePlan.DineConnect.House.Temp;
using DinePlan.DineConnect.House.Master.Dtos;

namespace DinePlan.DineConnect.House.Transaction.Dtos
{
    [AutoMapFrom(typeof(SalesDeliveryOrder))]
    public class SalesDeliveryOrderListDto : FullAuditedEntityDto
    {
        public DateTime DeliveryDate { get; set; }

        public int LocationRefId { get; set; }

        public int CustomerRefId { get; set; }

        public string DcNumberGivenByCustomer { get; set; }

        public bool IsDcCompleted { get; set; }

        public string SoReference { get; set; }

        public int? SoRefId { get; set; }
    }

    [AutoMapTo(typeof(SalesDeliveryOrder))]
    public class SalesDeliveryOrderEditDto
    {
        public int? Id { get; set; }

        public DateTime DeliveryDate { get; set; }

        public int LocationRefId { get; set; }

        public int CustomerRefId { get; set; }

        public string DcNumberGivenByCustomer { get; set; }

        public bool IsDcCompleted { get; set; }

        public string SoReference { get; set; }

        public int? SoRefId { get; set; }
        public long? CreatorUserId { get; set; }
    }

    [AutoMapTo(typeof(SalesDeliveryOrderDetail))]
    public class SalesDeliveryOrderDetailEditDto
    {
        public int? Id { get; set; }

        public int DeliveryRefId { get; set; }

        public int MaterialRefId { get; set; }

        public decimal DeliveryQty { get; set; }


        public int UnitRefId { get; set; }

        public string MaterialReceivedStatus { get; set; }
    }

    [AutoMapTo(typeof(SalesDeliveryOrderDetail))]
    public class SalesDeliveryOrderDetailViewDto
    {
        public int? Id { get; set; }

        public int DeliveryRefId { get; set; }

        public int MaterialRefId { get; set; }

        public string MaterialRefName { get; set; }

        public decimal DeliveryQty { get; set; }

        public decimal BillConvertedQty { get; set; }

        public int UnitRefId { get; set; }

        public string UnitRefName { get; set; }

        public string MaterialReceivedStatus { get; set; }

    }

    [AutoMapTo(typeof(SalesDeliveryOrderDetail))]
    public class SalesDeliveryOrderDetailForParticularDc
    {
        public int MaterialRefId { get; set; }

        public string MaterialRefName { get; set; }

        public decimal DeliveryQty { get; set; }

        public decimal BillConvertedQty { get; set; }

        public string MaterialReceivedStatus { get; set; }

        public decimal PendingQty { get; set; }
    }


    [AutoMapFrom(typeof(SalesDeliveryOrder))]
    public class SalesDeliveryOrderWithCustomerName
    {
        public long Id { get; set; }

        public DateTime DeliveryDate { get; set; }

        public int CustomerRefId { get; set; }
        public string DcNumberGivenByCustomer { get; set; }
        public long ReceivedPersonId { get; set; }
        public bool IsDcCompleted { get; set; }

        public string SoReference { get; set; }

        public int? SoRefId { get; set; }

        public string CustomerName { get; set; }

        public DateTime CreationTime { get; set; }
    }

    [AutoMapFrom(typeof(SalesDeliveryOrder))]
    public class SalesDeliveryOrderView
    {
        public long Id { get; set; }

        public DateTime DeliveryDate { get; set; }
        public int CustomerRefId { get; set; }
        public string DcNumberGivenByCustomer { get; set; }
        public long ReceivedPersonId { get; set; }
        public bool IsDcCompleted { get; set; }

        public string CustomerName { get; set; }
        public int MaterialCode { get; set; }
        public string MaterialName { get; set; }
        public decimal DeliveryQty { get; set; }
        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }

        public string MaterialReceivedStatus { get; set; }
        public DateTime CreationTime { get; set; }
    }

    public class GetSalesDeliveryOrderInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public string CustomerName { get; set; }
        public int SelectedId { get; set; }

        public int DefaultLocationRefId { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public string DcNumberGivenByCustomer { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime Desc";
            }
        }
    }
    public class GetSalesDeliveryOrderForEditOutput : IOutputDto
    {
        public SalesDeliveryOrderEditDto SalesDeliveryOrder { get; set; }

        public List<SalesDeliveryOrderDetailViewDto> SalesDeliveryOrderDetail { get; set; }
    }

    public class CreateOrUpdateSalesDeliveryOrderInput : IInputDto
    {
        [Required]
        public SalesDeliveryOrderEditDto SalesDeliveryOrder { get; set; }

        [Required]
        public List<SalesDeliveryOrderDetailViewDto> SalesDeliveryOrderDetail { get; set; }
		public TemplateSaveDto Templatesave { get; set; }
	}


    ////For Sample InwardDC
    //public class CreateSampleSalesDeliveryOrderInput : IInputDto
    //{
    //    [Required]
    //    public SalesDeliveryOrderEditDto SalesDeliveryOrder { get; set; }
    //}

    ////For Sample SalesDeliveryOrderDetail
    //public class CreateOrUpdateSalesDeliveryOrderDetailInput : IInputDto
    //{

    //    [Required]
    //    public SalesDeliveryOrderDetailEditDto SalesDeliveryOrderDetail { get; set; }

    //    [Required]

    //    public SalesDeliveryOrderListDto InwardDC { get; set; }

    //}

    [AutoMapFrom(typeof(SalesDeliveryOrderDetail))]
    public class SalesDeliveryOrderDetailListDto : FullAuditedEntityDto<long>
    {
        public long DeliveryRefId { get; set; }
        public int MaterialRefId { get; set; }
        public decimal DeliveryQty { get; set; }
        public int UnitRefId { get; set; }

        public string MaterialReceivedStatus { get; set; }
    }

    [AutoMapTo(typeof(SalesDeliveryOrderDetail))]
    public class GetSalesDeliveryOrderDetail
    {
        public long DeliveryRefId { get; set; }
        public int MaterialRefId { get; set; }
        public decimal DeliveryQty { get; set; }
        public Decimal BillConvertedQty { get; set; }
        public int UnitRefId { get; set; }

        public string MaterialReceivedStatus { get; set; }
    }


    public class GetSalesDeliveryOrderDetailInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }

    //public class PrintOutIn40Cols
    //{
    //    public string HeaderText { get; set; }
    //    public string BodyText { get; set; }
    //    public string FooterText { get; set; }

    //    public string BoldHeaderText { get; set; }
    //    public string BoldBodyText { get; set; }
    //    public string BoldFooterText { get; set; }
    //}

    //[AutoMapTo(typeof(TempSalesDeliveryOrderDetail))]
    //public class TempSalesDeliveryOrderDetailAddDto
    //{
    //    public int? Id { get; set; }
    //    public long DeliveryRefId { get; set; }
    //    public int MaterialRefId { get; set; }
    //    public decimal DeliveryQty { get; set; }
    //    public int UnitRefId { get; set; }

    //    public string MaterialReceivedStatus { get; set; }
    //    public int TenantId { get; set; }
    //}

    //public class CreateTempInwardDcDetailInput : IInputDto
    //{
    //    [Required]
    //    public TempSalesDeliveryOrderDetailAddDto TempInwardDcDetail { get; set; }
    //}

    //[AutoMapFrom(typeof(TempSalesDeliveryOrderDetail))]
    //public class TempSalesDeliveryOrderDetailListDto : FullAuditedEntityDto
    //{
    //    public long DeliveryRefId { get; set; }
    //    public int MaterialRefId { get; set; }
    //    public decimal DeliveryQty { get; set; }
    //    public int UnitRefId { get; set; }

    //    public string MaterialReceivedStatus { get; set; }
    //    public int TenantId { get; set; }
    //}
}