﻿
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Connect.Master.Dtos;
using System.Text;

namespace DinePlan.DineConnect.House.Transaction.Dtos
{
    [AutoMapFrom(typeof(ClosingStock))]
    public class ClosingStockListDto : FullAuditedEntityDto
    {
        //TODO: DTO ClosingStock Properties Missing
        //YOU CAN REFER ATTRIBUTES IN 
        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }
        public DateTime StockDate { get; set; }
        public int? ApprovedUserId { get; set; }
		public int? AdjustmentRefId { get; set; }
	}
    [AutoMapTo(typeof(ClosingStock))]
    public class ClosingStockEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO ClosingStock Properties Missing
        public int LocationRefId { get; set; }
        public DateTime StockDate { get; set; }
        public int? ApprovedUserId { get; set; }
		public int? AdjustmentRefId { get; set; }

	}

    [AutoMapTo(typeof(ClosingStockDetail))]
    public class ClosingStockDetailViewDto
    {
        public int? Id { get; set; }
        public int LocationRefId { get; set; }
        public int ClosingStockRefId { get; set; }
        public string ClosingStockRefName { get; set; }
        public int MaterialRefId { get; set; }
        public string Barcode { get; set; }
        public string MaterialRefName { get; set; }
        public string MaterialPetName {get;set;}
        public int MaterialGroupCategoryRefId { get; set; }
        public string MaterialGroupCategoryRefName { get; set; }

        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }
        public decimal OnHand { get; set; }

        public List<ClosingStockUnitWiseDetailViewDto> UnitWiseDetails { get; set; }
        public decimal QuantityInPurchaseUnit { get; set; }  //  Purchase Unit
        public decimal QuantityInStkAdjustedInventoryUnit { get; set; }   // Inventory Unit
        public decimal QuantityInIssueUnit { get; set; } // Recipe Unit  //  Except Default and Stock adj unit all units are as Recipe Unit

    }

    [AutoMapTo(typeof(ClosingStockUnitWiseDetail))]
    public class ClosingStockUnitWiseDetailViewDto
    {
        public int? Id { get; set; }
        public int MaterialRefId { get; set; }
        public int ClosingStockDetailRefId { get; set; }
        public decimal OnHand { get; set; }
        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }
    }

    public class GetScheduleMaterialList
    {
        public List<MaterialViewDto> MaterialStockList { get; set; }
        public string LocationName { get; set; }
        public string StockListRemarks { get; set; }
    }
    

    public class GetClosingStockInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }
        public int DefaultLocationRefId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "StockDate DESC";
            }
        }
    }
    public class GetClosingStockForEditOutput : IOutputDto
    {
        public ClosingStockEditDto ClosingStock { get; set; }
        public List<ClosingStockDetailViewDto> ClosingStockDetail { get; set; }
    }

	public class ClosingStockExists : IOutputDto
	{
		public int Count { get; set; }
		
	}

    public class CreateOrUpdateClosingStockInput : IInputDto
    {
        [Required]
        public ClosingStockEditDto ClosingStock { get; set; }
        public List<ClosingStockDetailViewDto> ClosingStockDetail { get; set; }
        public TemplateSaveDto Templatesave { get; set; }

    }



   [AutoMapFrom(typeof(MaterialLedger))]
   public class ClosingStockVarrianceDto
   {
      public int LocationRefId { get; set; }

      public DateTime LedgerDate { get; set; }

      public int MaterialRefId { get; set; }

      public string MaterialName { get; set; }
      public string MaterialPetName { get; set; }
      public int DefaultUnitId { get; set; }
      public string Uom { get; set; }

      public int MaterialTypeRefId { get; set; }

      public string MaterialTypeRefName { get; set; }

      public decimal OpenBalance { get; set; }

      public decimal Received { get; set; }

      public decimal Issued { get; set; }

      public decimal Sales { get; set; }

      public decimal TransferIn { get; set; }

      public decimal TransferOut { get; set; }

      public decimal Damaged { get; set; }

      public decimal SupplierReturn { get; set; }

      public decimal ExcessReceived { get; set; }
      public decimal Shortage { get; set; }
      public decimal Return { get; set; }

      public decimal ClBalance { get; set; }

      public string PlusMinus { get; set; }

      public decimal AvgPrice { get; set; }

      public decimal StockValue { get; set; }

      public decimal TotalConsumption { get; set; }

      public decimal CurrentStock { get; set; }
      public decimal DifferenceQuantity { get; set; }
      public decimal ExcessShortageQty { get; set; }
      public string AdjustmentStatus { get; set; }
   }

    public class ClosingStockConsolidatedDto
    {
        public DateTime StockTakenDate { get; set; }
        public List<LocationListDto> Locations { get; set; }
        public List<ComboboxItemDto> Materials { get; set; }
        public bool IsHighValueItem { get; set; }
        public bool IsAllMaterialFlag { get; set; }
    }

    public class ClosingStockConsolidatedReportLocationWise
    {
        public List<LocationListDto> Locations { get; set; }
        public List<MaterialListDto> Materials { get; set; }
        public List<ClosingStockConsolidatedDetail> ClosingStockConsolidatedDetails { get; set; }
    }

    public class ClosingStockConsolidatedDetail
    {
        public int LocationRefId { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public decimal OnHand { get; set; }
        public decimal ClBalance { get; set; }
        public decimal Difference { get; set; }
        public string Status { get; set; }

    }

    public class ApiPhysicalStockInputDto :IInputDto
    {
        public int LocationRefId { get; set; }
        public DateTime StockDate { get; set; }
    }

    public class ApiBatchMessageReturn : IOutputDto
    {
        public virtual string ErrorMessage { get; set; }
      
        public List<string> Rows { get; set; }
       
    }

    public class ApiBatchGoodsTextFile : IOutputDto
    {
        public virtual string ErrorMessage { get; set; }
        public List<string> MasterRows { get; set; }
        public List<string> DetailRows { get; set; }
        //public string MasterFileName { get; set; }
        //public string MasterOutputMessage { get; set; }

        //public StringBuilder MasterFileOutput { get; set; }
        //public string DetailFileName { get; set; }
        //public string DetailOutputMessage { get; set; }

        //public StringBuilder DetailFileOutput { get; set; }
    }


}
