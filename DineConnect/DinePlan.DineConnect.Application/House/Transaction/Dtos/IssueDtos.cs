﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using DinePlan.DineConnect.House.Temp;


namespace DinePlan.DineConnect.House.Transaction.Dtos
{
    [AutoMapFrom(typeof(Issue))]
    public class IssueListDto : FullAuditedEntityDto
    {
        public int LocationRefId { get; set; }
        public string RequestSlipNumber { get; set; }
        public int? RequestRefId { get; set; }
        public DateTime RequestTime { get; set; }
        public DateTime? IssueTime { get; set; }
        public int TokenNumber { get; set; }
        public int? RecipeRefId { get; set; }
        public decimal? RecipeProductionQty { get; set; }
        public int? MaterialGroupCategoryRefId { get; set; }

        public int? ProductionUnitRefId { get; set; }

        public string ProductionUnitRefName { get; set; }

        public bool CompletedStatus { get; set; }

        public bool MultipleBatchProductionAllowed { get; set; }
        public int TenantId { get; set; }
    }
    [AutoMapTo(typeof(Issue))]
    public class IssueEditDto
    {
        public int? Id { get; set; }
        public int LocationRefId { get; set; }
        public int? RequestRefId { get; set; }
        public string RequestSlipNumber { get; set; }
        public DateTime RequestTime { get; set; }
        public DateTime IssueTime { get; set; }
        public int TokenNumber { get; set; }
        //public int? RecipeRefId { get; set; }
        //public decimal? RecipeProductionQty { get; set; }
        public int MaterialGroupCategoryRefId { get; set; }
        public int? ProductionUnitRefId { get; set; }
        //public bool MultipleBatchProductionAllowed { get; set; }
    }

    [AutoMapTo(typeof(IssueRecipeDetail))]
    public class IssueRecipeDetailEditDto
    {
        public int? Id { get; set; }
        public int IssueRefId { get; set; }
        public int Sno { get; set; }
        public int RecipeRefId { get; set; }
        public decimal RecipeProductionQty { get; set; }
        public decimal CompletedQty { get; set; }
        public int UnitRefId { get; set; }
        public bool MultipleBatchProductionAllowed { get; set; }
        public bool CompletionFlag { get; set; }
        public string Remarks { get; set; }
    }

    [AutoMapTo(typeof(IssueRecipeDetail))]
    public class IssueRecipeDetailListDto
    {
        public int Id { get; set; }
        public int IssueRefId { get; set; }
        public int Sno { get; set; }
        public int RecipeRefId { get; set; }
        public string RecipeRefName { get; set; }
        public decimal RecipeProductionQty { get; set; }
        public decimal CompletedQty { get; set; }
        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }
        public int UnitRefId { get; set; }
        public string Uom { get; set; }
        public bool MultipleBatchProductionAllowed { get; set; }
        public bool CompletionFlag { get; set; }
        public string Remarks { get; set; }

        public decimal CurrentInHand { get; set; }
    }


    [AutoMapFrom(typeof(Issue))]
    public class IssueListWithNameDto : FullAuditedEntityDto
    {
        public int LocationRefId { get; set; }
        public string LocationName { get; set; }
        public int RequestSlipNumber { get; set; }
        public DateTime RequestTime { get; set; }
        public int? RequestRefId { get; set; }
        public DateTime? IssueTime { get; set; }
        public int TokenNumber { get; set; }
        public int RequestPersonId { get; set; }
        public int? RecipeRefId { get; set; }
        public decimal? RecipeProductionQty { get; set; }
        public bool MultipleBatchProductionAllowed { get; set; }
        public int TenantId { get; set; }
        public int? ProductionUnitRefId { get; set; }

    }


    [AutoMapTo(typeof(IssueDetail))]
    public class IssueDetailViewDto
    {
        public int? Id { get; set; }
        public int IssueRefId { get; set; }
        public decimal Sno { get; set; }
        public int? RecipeRefId { get; set; }
        public string Barcode { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public int MaterialTypeRefId { get; set; }
        public string MaterialTypeRefName { get; set; }
        public decimal RequestQty { get; set; }
        public decimal IssueQty { get; set; }
        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }
        public decimal CurrentInHand { get; set; }
        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }
        public int TransferUnitId { get; set; }
        public string TransferUnitRefName { get; set; }
    }


    [AutoMapFrom(typeof(Issue))]
    public class IssueWithRecipeListDto : FullAuditedEntityDto
    {
        public int LocationRefId { get; set; }
        public string RequestSlipNumber { get; set; }
        public int? RequestRefId { get; set; }
        public DateTime RequestTime { get; set; }
        public DateTime? IssueTime { get; set; }
        public int TokenNumber { get; set; }
     
        public int? MaterialGroupCategoryRefId { get; set; }

        public int? ProductionUnitRefId { get; set; }

        public string ProductionUnitRefName { get; set; }

        public bool CompletedStatus { get; set; }

        public List<IssueRecipeDetailListDto> IssueRecipeDetail { get; set; }

    }

    public class GetIssueInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }
        public int DefaultLocationRefId { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? RequestSlipNumber { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }



    public class GetStandardIngridient : IInputDto
    {
        public int LocationRefId { get; set; }
        public int recipeRefId { get; set; }
        public decimal requestQtyforProduction { get; set; }

        public List<GetRecipeDetail> RequestRecipeList {get;set;}
    }


    public class GetRecipeDetail : IInputDto
    {
        public int RecipeRefId { get; set; }
        public decimal RequestQtyforProduction { get; set; }
    }

    public class GetCategoryMaterial : IInputDto
    {
        public int LocationRefId { get; set; }
        public int MaterialGroupCategoryRefId { get; set; }
        public int MaterialGroupRefId { get; set; }
        public bool SemiFinishedOnly { get; set; }
    }

    public class GetIssueForStandardRecipe : IOutputDto
    {
        public List<IssueDetailViewDto> IssueDetail { get; set; }
    }


    public class GetIssueForEditOutput : IOutputDto
    {
        public IssueEditDto Issue { get; set; }

        public List<IssueRecipeDetailListDto> IssueRecipeDetail { get; set; }

        public List<IssueDetailViewDto> IssueDetail { get; set; }
        
        
    }
    public class CreateOrUpdateIssueInput : IInputDto
    {
        public IssueEditDto Issue { get; set; }

        public List<IssueRecipeDetailListDto> IssueRecipeDetail { get; set; }

        public List<IssueDetailViewDto> IssueDetail { get; set; }
    }

    public class InputLocation : IInputDto
    {
        public int LocationRefId { get; set; }

        public int? ProductionUnitRefId { get; set; }

        public string MaterialListFor { get; set; }
    }

	public class ImportIssueDto : IOutputDto
	{
		public int Sno { get; set; }
		public int LocationRefId { get; set; }
		public string LocationRefName { get; set; }
		public string Barcode { get; set; }
		public int MaterialRefId { get; set; }
		public string MaterialRefName { get; set; }
		public decimal IssueQty { get; set; }
		public decimal CurrentStatus { get; set; }
		public int Id { get; set; }
		public string MaterialTypeName { get; set; }
		public int UnitRefId { get; set; }
		public string Uom { get; set; }
		public int DefaultUnitId { get; set; }
		public string DefaultUnit { get; set; }
		public decimal OnHand { get; set; }
	}

}