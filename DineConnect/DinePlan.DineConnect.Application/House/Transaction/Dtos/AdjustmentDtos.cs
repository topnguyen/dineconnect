﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Exporter;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Exporter.Util;

namespace DinePlan.DineConnect.House.Transaction.Dtos
{
    [AutoMapFrom(typeof(Adjustment))]
    public class AdjustmentListDto : FullAuditedEntityDto
    {
        //TODO: DTO Adjustment Properties Missing
        //YOU CAN REFER ATTRIBUTES IN 
        public int LocationRefId { get; set; }
        public DateTime AdjustmentDate { get; set; }
        public long TokenRefNumber { get; set; }
        public int EnteredPersonId { get; set; }
        public int ApprovedPersonId { get; set; }
        public string AdjustmentRemarks { get; set; }
        public DateTime? ClosingStockDate { get; set; }

    }
    [AutoMap(typeof(Adjustment))]
    public class AdjustmentEditDto : FullAuditedEntityDto
    {
        public int? Id { get; set; }
        //TODO: DTO Adjustment Properties Missing
        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }
        public DateTime AdjustmentDate { get; set; }
        public long TokenRefNumber { get; set; }     //222 - Negative Stock As Zero, 333 - PurchaseYield Difference,  444 - Menu Wastage, 555 - CompWastage, 666 - OnHandInitialSetup, 777- AutoPostFromExcel, 888- StockTakenDifference, 999 - TransferReceivedDifference
        public int EnteredPersonId { get; set; }
        public int ApprovedPersonId { get; set; }
        public string AdjustmentRemarks { get; set; }
        public DateTime? ClosingStockDate { get; set; }
        public decimal TotalExcessCost { get; set; }
        public decimal TotalShortageCost { get; set; }
        public decimal TotalAdjustmentCost { get; set; }
        public decimal TotalClosingStockValue { get; set; }
        public decimal TotalStockEntryValue { get; set; }
        public int TenantId { get; set; }
    }
    [AutoMap(typeof(Adjustment))]
    public class AdjustmentViewDto
    {
        public int? Id { get; set; }
        //TODO: DTO Adjustment Properties Missing
        public string LocationRefName { get; set; }
        public DateTime AdjustmentDate { get; set; }
        public DateTime CreationTime { get; set; }
        public long TokenRefNumber { get; set; }
        public int EnteredPersonId { get; set; }
        public int ApprovedPersonId { get; set; }
        public string AdjustmentRemarks { get; set; }
        public DateTime? ClosingStockDate { get; set; }
    }
    public class AdjustmentDetailViewDto
    {
        public int? Id { get; set; }
        public int AdjustmentRefIf { get; set; }
        public int Sno { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialPetName { get; set; }
        public string MaterialGroupCategoryName { get; set; }
        public string MaterialRefName { get; set; }
        public decimal AdjustmentQty { get; set; }
        public decimal? StockEntry { get; set; }
        public decimal? StockEntryValue { get; set; }
        public decimal? ClosingStock { get; set; }
        public decimal? ClosingStockValue { get; set; }
        public string AdjustmentMode { get; set; }
        public string AdjustmentApprovedRemarks { get; set; }
        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }
        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }
        public long CreatorUserId { get; set; }
        public decimal? Price { get; set; }
        public decimal? AdjustmentCost { get; set; }
    }

    public class GetAdjustmentInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }
        public int? LocationRefId { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? TokenRefNumber { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetAdjustmentForEditOutput
    {
        public AdjustmentEditDto Adjustment { get; set; }

        public List<AdjustmentDetailViewDto> AdjustmentDetail { get; set; }

    }
    public class CreateOrUpdateAdjustmentInput
    {
        [Required]
        public AdjustmentEditDto Adjustment { get; set; }
        [Required]
        public List<AdjustmentDetailViewDto> AdjustmentDetail { get; set; }

        public bool ForceAdjustmentFlag { get; set; }

        public bool ClosingStockFlag { get; set; }
        public DateTime? ClosingStockDate { get; set; }
        public int SupplierRefId { get; set; }
    }

    public class PreviousAdjustment
    {
        public List<AdjustmentAutoViewDto> PreviousTransactions { get; set; }
        public DateTime LastTransactioDate { get; set; }
    }

    public class GetPreviousAdjustment
    {
        public int LocationRefId { get; set; }
        public bool StockTakenFlag { get; set; }
    }

    [AutoMapFrom(typeof(Adjustment))]
    public class AdjustmentAutoViewDto : FullAuditedEntityDto
    {
        public int AdjustmentId { get; set; }
        public int LocationRefId { get; set; }
        public DateTime AdjustmentDate { get; set; }
        public long TokenRefNumber { get; set; }
        public int EnteredPersonId { get; set; }
        public int ApprovedPersonId { get; set; }
        public string AdjustmentRemarks { get; set; }
        public DateTime? ClosingStockDate { get; set; }
        public int NumberOfMaterials { get; set; }

        public decimal TotalAdjustmentAmount { get; set; }

    }

    public class InputAdjustmentReport
    {
        public ExportType ExportType { get; set; }
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string DateRange { get; set; }

        public LocationListDto LocationInfo { get; set; }
        public DateTime PrintedTime { get; set; }
        public long? PreparedUserId { get; set; }
        public string PreparedUserName { get; set; }
        public bool CustomReportFlag { get; set; }

        public List<LocationListDto> ToLocations { get; set; }

        public int? MaterialTypeRefId { get; set; }
        public int? MaterialRefId { get; set; }

        public string AdjustmentMode { get; set; }

        public List<ComboboxItemDto> EntryTypes { get; set; }
    }


    public class AdjustmentReportConsolidatedDto
    {
        public int MaterialTypeRefId { get; set; }
        public string MaterialTypeRefName { get; set; }
        public int MaterialGroupCategoryRefId { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public string AdjustmentMode { get; set; }
        public decimal TotalQty { get; set; }
        public decimal ExcessQty { get; set; }
        public decimal ShortageQty { get; set; }
        public string Uom { get; set; }
        public decimal Price { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal ExcessAmount { get; set; }
        public decimal ShortageAmount { get; set; }
        public string Remarks { get; set; }
        public int PlusOrMinus { get; set; }

    }


    public class AdjustmentDtosWithExcelFile
    {
        public FileDto ExcelFile { get; set; }
        public List<MaterialAdjustmentStockDto> ClosingAdjustmentDtos { get; set; }

        public List<ClosingStockVarrianceDto> ClosingStockVarrianceDtos { get; set; }

        public int? AdjustmentRefId { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class OverAllMaterialWastageReport
    {
        public decimal OverAllTotalAmount { get; set; }
        public List<MaterialWastageReportDto> WastageReport { get; set; }
        public List<MaterialWastageReportDto> OverAllWastageReport { get; set; }
        public List<MaterialWastageConsolidatedReport> LocationWiseWastageList { get; set; }
    }

    public class MaterialWastageConsolidatedReport
    {
        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }
        public List<MaterialWastageReportDto> ConsolidatedWastageReport { get; set; }
        public List<MaterialWastageReportDto> WastageDetailReport { get; set; }
    }




    public class MaterialWastageReportDto : ExportDataObject
    {
        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }
        public DateTime AdjustmentDate { get; set; }
        public int MaterialTypeRefId { get; set; }
        public string MaterialTypeRefName { get; set; }
        public int MaterialGroupCategoryRefId { get; set; }
        public string MaterialGroupCategoryRefName { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialPetName { get; set; }
        public string MaterialRefName { get; set; }
        public string MenuWastageMode { get; set; }
        public string AdjustmentMode { get; set; }
        public decimal TotalQty { get; set; }
        public decimal QuantityInPurchaseUnit { get; set; }  //  Purchase Unit
        public decimal QuantityInStkAdjustedInventoryUnit { get; set; }   // Inventory Unit
        public decimal QuantityInIssueUnit { get; set; } // Recipe Unit  //  Except Default and Stock adj unit all units are as Recipe Unit
        public decimal ExcessQty { get; set; }
        public decimal ShortageQty { get; set; }
        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }
        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }
        public decimal Price { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal ExcessAmount { get; set; }
        public decimal ShortageAmount { get; set; }
        public string Remarks { get; set; }
        public int PlusOrMinus { get; set; }
        public long UserId { get; set; }
        public string UserName { get; set; }
        public long TokenRefNumber { get; set; }

    }


    public enum Adjustment_Reason
    {
        Adj_444,    //	Menu Wastage - Entry 
        Adj_555,    //	Wastage While Billing - Comp Wastages
        Adj_666,    //	OnHand  Initial Setup - Through Import / Enter Manually
        Adj_777,    //	Read From Excel Based Stock On Given Date
        Adj_888,    //	Physical Stock Taken Differences, Closing Stock Entry - Specially made for FSB
        Adj_999,    //	Day Close Wastage Excess , Wastage - POST AUTO ADJUSTMENT , WIPE OUT STOCK ON DAYCLOSE Materials differences
    };
}

