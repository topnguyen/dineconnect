﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using DinePlan.DineConnect.House.Temp;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Connect.Master.Dtos;

namespace DinePlan.DineConnect.House.Transaction.Dtos
{
    [AutoMapFrom(typeof(SalesInvoice))]
    public class SalesInvoiceListDto : FullAuditedEntityDto
    {
        //TODO: DTO SalesInvoice Properties Missing
        //YOU CAN REFER ATTRIBUTES IN 
        public int LocationRefId { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string InvoiceMode { get; set; }
        public DateTime? AccountDate { get; set; }
        public int CustomerRefId { get; set; }
        public string CustomerRefName { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime DcFromDate { get; set; }
        public DateTime DcToDate { get; set; }
        public decimal TotalDiscountAmount { get; set; }
        public decimal TotalShipmentCharges { get; set; }
        public decimal RoundedAmount { get; set; }
        public decimal InvoiceAmount { get; set; }
        public bool IsDirectSalesInvoice { get; set; }

    }
    [AutoMapTo(typeof(SalesInvoice))]
    public class SalesInvoiceEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO SalesInvoice Properties Missing
        public int LocationRefId { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string InvoiceMode { get; set; }
        public DateTime? AccountDate { get; set; }
        public int CustomerRefId { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime DcFromDate { get; set; }
        public DateTime DcToDate { get; set; }
        public decimal TotalDiscountAmount { get; set; }
        public decimal TotalShipmentCharges { get; set; }
        public decimal RoundedAmount { get; set; }
        public decimal InvoiceAmount { get; set; }
        public bool IsDirectSalesInvoice { get; set; }

    }



    [AutoMapFrom(typeof(SalesInvoiceDetail))]
    public class SalesInvoiceDetailViewDto : FullAuditedEntityDto
    {
        public int SalesInvoiceRefId { get; set; }
        public SalesInvoice Invoices { get; set; }
        public int Sno { get; set; }
        public int DcTranId { get; set; }
        public int MaterialRefId { get; set; }
        public int MaterialGroupRefId { get; set; }
        public int MaterialGroupCategoryRefId { get; set; }
        public string MaterialRefName { get; set; }
        public decimal TotalQty { get; set; }
        //public decimal ReceivedQtyInUnit { get; set; }
        public int DefaultUnitId { get; set; }
        public string Uom { get; set; }
        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }
        public decimal Price { get; set; }
        public decimal TotalAmount { get; set; }
        public string DiscountFlag { get; set; }
        public decimal DiscountPercentage { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal SalesTaxAmount { get; set; }
        public decimal NetAmount { get; set; }
        public string Remarks { get; set; }

        public List<TaxForMaterial> TaxForMaterial { get; set; }

        public List<ApplicableTaxesForMaterial> ApplicableTaxes { get; set; }

    }

    [AutoMapTo(typeof(SalesInvoiceDetail))]
    public class SalesInvoiceDetailEditDto
    {
        public int? Id { get; set; }
        public int SalesInvoiceRefId { get; set; }
        public int Sno { get; set; }
        public int MaterialRefId { get; set; }
        public decimal TotalQty { get; set; }
        //public decimal ReceivedQtyInUnit { get; set; }
        public int UnitRefId { get; set; }
        public decimal Price { get; set; }
        public decimal TotalAmount { get; set; }
        public string DiscountFlag { get; set; }
        public decimal DiscountPercentage { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal SalesTaxAmount { get; set; }
        public decimal NetAmount { get; set; }
        public string Remarks { get; set; }
        public List<TaxForMaterial> TaxForMaterial { get; set; }
        public List<ApplicableTaxesForMaterial> ApplicableTaxes { get; set; }
    }



    [AutoMapFrom(typeof(SalesInvoiceTaxDetail))]
    public class SalesInvoiceTaxDetailViewDto : FullAuditedEntityDto
    {
        public int SalesInvoiceRefId { get; set; }
        public int Sno { get; set; }
        public int MaterialRefId { get; set; }
        public int SalesTaxRefId { get; set; }
        public decimal SalesTaxRate { get; set; }
        public decimal SalesTaxValue { get; set; }
    }

    [AutoMapFrom(typeof(SalesInvoiceDeliveryOrderLink))]
    public class SalesInvoiceDeliveryOrderLinkViewDto : FullAuditedEntityDto
    {
        public int SalesInvoiceRefId { get; set; }
        public int Sno { get; set; }
        public int MaterialRefId { get; set; }
        public int DeliveryOrderRefId { get; set; }

        public decimal MaterialConvertedQty { get; set; }
    }

    public class GetSalesInvoiceInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public List<LocationListDto> Locations { get; set; }
        public int CustomerRefId { get; set; }
        public string CustomerName { get; set; }
        public string InvoiceNumber { get; set; }
        public int LocationRefId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetSalesInvoiceForEditOutput : IOutputDto
    {
        public SalesInvoiceEditDto SalesInvoice { get; set; }
        public List<SalesInvoiceDetailViewDto> SalesInvoiceDetail { get; set; }
        public List<SalesInvoiceDeliveryOrderLinkViewDto> SalesInvoiceDeliveryOrderLink { get; set; }
    }

    //For Sample Invoices
    public class CreateOrUpdateSampleSalesInvoiceInput : IInputDto
    {
        [Required]
        public SalesInvoiceEditDto SalesInvoice { get; set; }
    }

    [AutoMapTo(typeof(SalesDeliveryOrder))]
    public class SalesDeliveryOrderForParticularDc
    {
        public int MaterialRefId { get; set; }

        public string MaterialRefName { get; set; }

        public decimal DeliveryQty { get; set; }

        public decimal BillConvertedQty { get; set; }

        public string MaterialReceivedStatus { get; set; }

        public decimal PendingQty { get; set; }
    }

    public class CreateOrUpdateSalesInvoiceInput : IInputDto
    {
        [Required]
        public SalesInvoiceEditDto SalesInvoice { get; set; }

        [Required]
        public List<SalesInvoiceDetailViewDto> SalesInvoiceDetail { get; set; }

        public List<SalesInvoiceDeliveryOrderLinkViewDto> SalesInvoiceDeliveryOrderLink { get; set; }

    }

    public class GetDoMaterialInfo : IInputDto
    {
        public int CustomerRefId { get; set; }
        public int DcRefId { get; set; }
        public int LocationRefId { get; set; }
    }

    public class GetDoMaterialDataDetail : IInputDto
    {
        [Required]
        public List<SalesInvoiceDetailViewDto> DcDetail { get; set; }
    }

    public class InputSalesInvoiceAnalysis
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int LocationRefId { get; set; }

        public int CustomerRefId { get; set; }

        public int MaterialRefId { get; set; }
        public string InvoiceNumber { get; set; }

        public bool ExactSearchFlag { get; set; }

    }

    public class InputSalesInvoiceAnalysisForCategoryWise
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int LocationRefId { get; set; }

        public int CustomerRefId { get; set; }

        public int CategoryRefId { get; set; }

    }

    public class SalesInvoiceAnalysisDto
    {
        public int SalesInvoiceRefId { get; set; }
        public int CustomerRefId { get; set; }
        public string CustomerRefName { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime InvoiceDate { get; set; }
        public int MaterialGroupCategoryRefId { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public decimal TotalQty { get; set; }
        public decimal Price { get; set; }
        public decimal TotalAmount { get; set; }
        public string DiscountFlag { get; set; }
        public decimal DiscountPercentage { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal SalesTaxAmount { get; set; }
        public decimal NetAmount { get; set; }
        public string Remarks { get; set; }
        public string Uom { get; set; }

        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }
        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }
        public string PriceForDefaultUnitWithTaxUOM { get; set; }
    }

    public class SalesInvoiceCategoryAnalysisDto
    {
        public int CategoryRefId { get; set; }
        public string CategoryRefName { get; set; }
        public decimal TotalQty { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal SalesTaxAmount { get; set; }
        public decimal NetAmount { get; set; }

    }
    public class SalesInputInvoiceDto : IInputDto

    {
        List<SalesInvoiceListDto> SalesInvoiceList { get; set; }
    }

    public class InputMaterialSalesDto : IInputDto
    {
        public DateTime EffectiveDate { get; set; }
        public int MaterialRefId { get; set; }
        public int NumberOfLastSalesDetail { get; set; }
    }

    public class OutputMaterialSalesDto : IOutputDto
    {
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public DateTime SalesDate { get; set; }
        public int CustomerRefId { get; set; }
        public string CustomerRefName { get; set; }
        public decimal QuantityPurchased { get; set; }
        public decimal Price { get; set; }
        public decimal Tax { get; set; }
        public decimal PriceWithTax { get; set; }

    }

}

