﻿

using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using DinePlan.DineConnect.House.Master.Dtos;

namespace DinePlan.DineConnect.House.Transaction.Dtos
{
    [AutoMapFrom(typeof(Production))]
    public class ProductionListDto : FullAuditedEntityDto
    {
        public int LocationRefId { get; set; }

        public string LocationRefName { get; set; }

        public string ProductionSlipNumber { get; set; }

        public DateTime ProductionTime { get; set; }

        public string Status { get; set; }

        public int? ProductionUnitRefId { get; set; }

        public string ProductionUnitRefName { get; set; }
    }

    [AutoMapTo(typeof(Production))]
    public class ProductionEditDto
    {
        public int? Id { get; set; }
        public int LocationRefId { get; set; }

        public string ProductionSlipNumber { get; set; }

        public DateTime ProductionTime { get; set; }

        public string Status { get; set; }

        public int? ProductionUnitRefId { get; set; }

    }


    [AutoMapTo(typeof(ProductionDetail))]
    public class ProductionDetailViewDto 
    {
        public int? Id { get; set; }

        public int ProductionRefId { get; set; }
        public string ProductionReferences { get; set; }

        public DateTime ProductionTime { get; set; }

        [Required]
        public int Sno { get; set; }

        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }

        public decimal ProductionQty { get; set; }

        public int DefaultUnitId { get; set; }
         public string Uom { get; set; }

         public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }


        public int? IssueRefId { get; set; }

        public string IssueReference { get; set; }

        public decimal CurrentInHand { get; set; }

        public decimal AllowedMinQty { get; set; }

        public decimal AllowedMaxQty { get; set; }

        public bool MultipleBatchProductionAllowed { get; set; }

        public int? ProductionUnitRefId { get; set; }

        public string ProductionUnitRefName { get; set; }
        public decimal Price { get; set; }
        public decimal  LineTotal { get; set; }
    }


    public class GetProductionInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public int DefaultLocationRefId { get; set; }
        public int? ProductionSlipNumber { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetProductionForEditOutput : IOutputDto
    {
        public ProductionEditDto Production { get; set; }

        public List<ProductionDetailViewDto> ProductionDetail { get; set; }

        public TemplateSaveDto Templatesave { get; set; }
    }

    public class CreateOrUpdateProductionInput : IInputDto
    {
        [Required]
        public ProductionEditDto Production { get; set; }
        public List<ProductionDetailViewDto> ProductionDetail { get; set; }

        public string ProductionMethod { get; set; }
        public TemplateSaveDto Templatesave { get; set; }
        public List<MaterialRateViewListDto> MaterialRateView { get; set; }
    }
}

