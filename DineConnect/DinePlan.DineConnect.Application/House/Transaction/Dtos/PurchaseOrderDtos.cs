﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using DinePlan.DineConnect.House.Temp;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Cater.Quotation.Dtos;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Location;

namespace DinePlan.DineConnect.House.Transaction.Dtos
{
    [AutoMapFrom(typeof(PurchaseOrder))]
    public class PurchaseOrderListDto : FullAuditedEntityDto
    {
        public DateTime PoDate { get; set; }
        public string PoReferenceCode { get; set; }
        public int ShippingLocationId { get; set; }

        public string QuoteReference { get; set; }

        public int CreditDays { get; set; }

        public int SupplierRefId { get; set; }

        public string Remarks { get; set; }
        public bool PoStatus { get; set; }
        public decimal PoNetAmount { get; set; }
        public int LocationRefId { get; set; }
        public DateTime? ApprovedTime { get; set; }

        public string PoCurrentStatus { get; set; }
        public DateTime DeliveryDateExpected { get; set; }
        public bool PoBasedOnInterTransferRequest { get; set; }
        public bool PoBasedOnCater { get; set; }
        public int TenantId { get; set; }
    }

    [AutoMapFrom(typeof(PurchaseOrder))]
    public class PurchaseOrderListWithRefNameDto : FullAuditedEntityDto
    {
        public int Id { get; set; }
        public DateTime PoDate { get; set; }
        public string PoReferenceCode { get; set; }
        public int ShippingLocationId { get; set; }
        public string ShippingLocationName { get; set; }
        public string QuoteReference { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public int CreditDays { get; set; }
        public int SupplierRefId { get; set; }
        public string SupplierName { get; set; }
        public string Remarks { get; set; }
        public decimal PoNetAmount { get; set; }
        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }
        public int TenantId { get; set; }
        public DateTime? ApprovedTime { get; set; }
        public string PoCurrentStatus { get; set; }
        public bool PoBasedOnInterTransferRequest { get; set; }
        public bool PoBasedOnCater { get; set; }
        public List<SimpleQuotationHeaderListDto> CaterQuoteDetails { get; set; }

    }

    [AutoMapTo(typeof(PurchaseOrder))]
    public class PurchaseOrderEditDto 
    {
        public int? Id { get; set; }
        public DateTime PoDate { get; set; }
        public string PoReferenceCode { get; set; }
        public int ShippingLocationId { get; set; }
        public string ShippingLocationRefName { get; set; }
        public string QuoteReference { get; set; }

        public int CreditDays { get; set; }

        public int SupplierRefId { get; set; }
        public string SupplierRefName { get; set; }
        public bool SupplierTaxApplicableByDefault { get; set; }
        public string Remarks { get; set; }
        public bool PoStatus { get; set; }
        public decimal PoNetAmount { get; set; }
        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }
        public DateTime DeliveryDateExpected { get; set; }
        public DateTime? ApprovedTime { get; set; }
        public string PoCurrentStatus { get; set; }
        public string UserRefName { get; set; }
        public long? CreatorUserId { get; set; }
        public bool PoBasedOnInterTransferRequest { get; set; }
        public bool PoBasedOnCater { get; set; }
        public int TenantId { get; set; }
    }

    [AutoMapTo(typeof(PurchaseOrderDetail))]
    public class PurchaseOrderDetailViewDto : FullAuditedEntityDto
    {
        public int? Id { get; set; }
        public int SupplierRefId { get; set; }
        public string SupplierRefName { get; set; }
        public int PoRefId { get; set; }
        public string PoReferenceCode { get; set; }
        public string PoStatusRemarks {get;set;}
        public int MaterialRefId { get; set; }
        public string Barcode { get; set; }
        public string MaterialRefName { get; set; }
        public decimal QtyOrdered { get; set; }
        public decimal QtyReceived{get; set;}
        public decimal QtyPending { get; set; }
        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }
        public decimal Price { get; set; }
        public decimal TotalAmount { get; set; }
        public string DiscountOption { get; set; }
        public decimal DiscountValue { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal NetAmount { get; set; }
        public bool PoStatus { get; set; }
        public string Remarks { get; set; }
        public int DefaultUnitId { get; set; }
        public string Uom { get; set; }
        public decimal AlreadyOrderedQuantity { get; set; }
        
        public decimal CurrentInHand { get; set; }
		public decimal MinimumOrderQuantity { get; set; }
        public decimal MaximumOrderQuantity { get; set; }
        public int UnitRefIdForMinOrder { get; set; }
		public string UnitRefNameForMinOrder { get; set; }
		public List<TaxForMaterial> TaxForMaterial { get; set; }
        public List<ApplicableTaxesForMaterial> ApplicableTaxes { get; set; }
        public List<InterTransferDetailWithPORefIds> InterTransferDetailWithPORefIds { get; set; }
        public List<CaterHeaderRefIds> CaterHeaderRefIds { get; set; }
        public string PoCurrentStatus { get; set; }
       public string SupplierMaterialAliasName { get; set; }

    }

    public class InterTransferDetailWithPORefIds
    {
        public int InterTransferRefId { get; set; }
    }

    [AutoMapFrom(typeof(PurchaseOrderTaxDetail))]
    public class PurchaseOrderTaxDetailViewDto:FullAuditedEntityDto
    {
        public int PoRefId { get; set; }
        public int MaterialRefId { get; set; }
        public int TaxRefId { get; set; }
    }

    public class GetPurchaseOrderInput : PagedAndSortedInputDto, IShouldNormalize
    {

        public string Filter { get; set; }

        public string Operation { get; set; }

        public int PurchaseOrderId { get; set; }

        public List<int> PurchaseOrderIdList { get; set; }

        public int DefaultLocationRefId { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string PoReferenceCode { get; set; }
        public string ReferenceNumber { get; set; }
        public string SupplierName { get; set; }
        public string PoStatus { get; set; }
        public int? MaterialRefId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }

        public List<int> PendingPoIds { get; set; }
        public long UserId { get; set; }

        public List<SimpleLocationDto> Locations { get; set; }

        public LocationGroupDto LocationGroup { get; set; }

        public GetPurchaseOrderInput()
        {
            PendingPoIds = new List<int>();
            PurchaseOrderIdList = new List<int>();
        }
    }

    public class GetPOInput : PagedAndSortedInputDto
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string PoReferenceCode { get; set; }
    }

    public class GetPurchaseOrderForEditOutput : IOutputDto
    {
        public PurchaseOrderEditDto PurchaseOrder { get; set; }
        public List<PurchaseOrderDetailViewDto> PurchaseOrderDetail { get; set; }
        public List<SimpleQuotationHeaderListDto> CaterQuoteDetails { get; set; }
        public TemplateSaveDto Templatesave { get; set; }
        public string PrintFileName { get; set; }
    }

    public class SetPurchaseOrderDashBoardDto : IOutputDto
    {
        public int TotalPending { get; set; }

        public int TotalApproved { get; set; }

        public int TotalYetToReceived { get; set; }

        public int TotalDeclined { get; set; }
    }

    public class CreateOrUpdatePurchaseOrderInput : IInputDto
    {
        [Required]
        public PurchaseOrderEditDto PurchaseOrder { get; set; }
        [Required]
        public List<PurchaseOrderDetailViewDto> PurchaseOrderDetail { get; set; }

        public TemplateSaveDto Templatesave { get; set; }

        public string StatusToBeAssigned { get; set; }
    }

    public class GetPurchaseIdAndReference : IOutputDto
    {
        public int Id { get; set; }
        public string PoReferenceCode { get; set; }
    }

    public class PurchaseOrderStatus : IInputDto
    {
        public int PoId { get; set; }
        public string PoReferenceCode { get; set; }
        public string PoCurrentStatus { get; set; }
        public string PoRemarks { get; set; }
    }


    public class GetObjectFromString : IInputDto
    {
        public int LocationRefId { get; set; }
        public int TemplateType { get; set; }
        public int TemplateId { get; set; }
        public string ObjectString { get; set; }
    }

    public class SetPurchaseOrderPrint : IInputDto
    {
        public string PurchaseOrderHTML { get; set; }
        public int PurchaseOrderId { get; set; }

    }

    public class GetPoReferenceDto : IInputDto
    {
        public int LocationRefId { get; set; }
        public int SupplierRefId { get; set; }
    }

    public class AutoPurchaseOrderConsolidatedDto : IOutputDto
    {
        public int SupplierRefId { get; set; }
        public string SupplierRefName { get; set; }
        public int NoOfProducts { get; set; }
        public int PoAmount { get; set; }
        public string Remarks { get; set; }
        public CreateOrUpdatePurchaseOrderInput AutoPoDto { get; set; }
    }

    public class AutoPoDetail : IOutputDto
    {
        public int SupplierRefId { get; set; }
        public int MaterialRefId { get; set; }
        public string Barcode { get; set; }
        public string MaterialRefName { get; set; }
        public decimal OnHand { get; set; }
        public decimal Rol { get; set; }
        public decimal Price { get; set; }
        public decimal MaximumStock { get; set; }
        public decimal QuantityToBeOrdered { get; set; }
        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }
        public decimal AlreadyOrderedQuantity { get; set; }
		public decimal MinimumOrderQuantity { get; set; }
        public decimal MaximumOrderQuantity { get; set; }
		public int UnitRefIdForMinOrder { get; set; }
		public string UnitRefNameForMinOrder { get; set; }

		public bool SupplierExistFlag { get; set; }
        public List<InterTransferDetailWithPORefIds> InterTransferDetailWithPORefIds { get; set; }
        public List<CaterHeaderRefIds> CaterHeaderRefIds { get; set; }
        public bool OmitQueueInOrder { get; set; }
    }

	public class MaterialVsSupplierDtos : IOutputDto
    {
        public int SupplierRefId { get; set; }
        public string SupplierRefName { get; set; }
        public decimal Price { get; set; }
        public decimal MOQ { get; set; }
        public DateInput LastPurchaseDate { get; set; }
    }

    public class PoStatusWiseList
    {
        public string PoCurrentStatus { get; set; }
        public List<PurchaseOrderListDto> PurchaseOrderList { get; set; }
    }

    public class PoPendingDashBoardAlertDto
    {
        public PoPendingDashBoardAlertDto()
        {
            PendingPoListDtos = new List<PurchaseOrderListWithRefNameDto>();
        }

        public List<PurchaseOrderListWithRefNameDto> PendingPoListDtos { get; set; }
        public int PoPendingCount => PendingPoListDtos.Count;
    }


    public class BulkPoMessageOutput : IOutputDto
    {
        public bool SuccessFlag { get; set; }
        public string Message { get; set; }
        public string SuccessMessage { get; set; }
        public string HtmlMessage { get; set; }
        public string ErrorMessage { get; set; }
        public int Count { get; set; }
        public List<PurchaseOrderListWithRefNameDto> PurchaseOrderList { get; set; }
    }

    public class BulkPoPreparationDto
    {
        public int SupplierRefId { get; set; }
        public List<LocationListDto> Locations { get; set; }
        public List<BulkPoMaterialListDto> BulkPoMaterialListDtos { get; set; }
        public TemplateSaveDto Templatesave { get; set; }
        public DateTime DeliveryDateExpected { get; set; }
    }

    public class BulkPoMaterialListDto
    {
        public int MaterialRefId { get; set; }
        public string MaterialPetName { get; set; }
        public string MaterialName { get; set; }
        public string Uom { get; set; }
        public decimal TotalRequestQty { get; set; }
        public Material Material { get; set; }
        public List<string> LocationWiseRequestQty { get; set; }
    }
}
