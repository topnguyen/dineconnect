﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using DinePlan.DineConnect.House.Temp;

namespace DinePlan.DineConnect.House.Transaction.Dtos
{
    [AutoMapFrom(typeof(InwardDirectCredit))]
    public class InwardDirectCreditListDto : FullAuditedEntityDto
    {
        public DateTime DcDate { get; set; }

        public int LocationRefId { get; set; }

        public int SupplierRefId { get; set; }

        public string DcNumberGivenBySupplier { get; set; }

        public bool IsDcCompleted { get; set; }

        public string PoReference { get; set; }

        public int? PoRefId { get; set; }
        public string PurchaseOrderReferenceFromOtherErp { get; set; }
        public string InvoiceNumberReferenceFromOtherErp { get; set; }

    }

    [AutoMapTo(typeof(InwardDirectCredit))]
    public class InwardDirectCreditEditDto
    {
        public int? Id { get; set; }

        public DateTime DcDate { get; set; }
        public DateTime? AccountDate { get; set; }

        public int LocationRefId { get; set; }

        public int SupplierRefId { get; set; }

        public string DcNumberGivenBySupplier { get; set; }

        public bool IsDcCompleted { get; set; }

        public string PoReference { get; set; }

        public int? PoRefId { get; set; }
        public long? CreatorUserId { get; set; }
        public int? AdjustmentRefId { get; set; }

        public string PurchaseOrderReferenceFromOtherErp { get; set; }
        public string InvoiceNumberReferenceFromOtherErp { get; set; }

    }

    [AutoMapTo(typeof(InwardDirectCreditDetail))]
    public class InwardDirectCreditDetailEditDto
    {
        public int? Id { get; set; }

        public int DcRefId { get; set; }

        public int MaterialRefId { get; set; }

        public decimal DcReceivedQty { get; set; }


        public int UnitRefId { get; set; }

        public string MaterialReceivedStatus { get; set; }
    }

    [AutoMapTo(typeof(InwardDirectCreditDetail))]
    public class InwardDirectCreditDetailViewDto
    {
        public int? Id { get; set; }

        public int DcRefId { get; set; }

        public string Barcode { get; set; }

        public int MaterialRefId { get; set; }

        public string MaterialRefName { get; set; }
        public string SupplierMaterialAliasName { get; set; }

        public decimal DcReceivedQty { get; set; }

        public decimal DcConvertedQty { get; set; }

        public int UnitRefId { get; set; }

        public string UnitRefName { get; set; }

        public string MaterialReceivedStatus { get; set; }
        public decimal? YieldPercentage { get; set; }
        public string YieldMode { get; set; }
        public decimal? YieldExcessShortageQty { get; set; }
    }

    [AutoMapTo(typeof(InwardDirectCreditDetail))]
    public class InwardDirectCreditDetailForParticularDc
    {
        public int MaterialRefId { get; set; }

        public string MaterialRefName { get; set; }

        public decimal DcReceivedQty { get; set; }

        public decimal DcConvertedQty { get; set; }

        public string MaterialReceivedStatus { get; set; }

        public decimal PendingQty { get; set; }
    }


    [AutoMapFrom(typeof(InwardDirectCredit))]
    public class InwardDirectCreditWithSupplierName
    {
        public long Id { get; set; }

        public DateTime DcDate { get; set; }
        public DateTime? AccountDate { get; set; }

        public int SupplierRefId { get; set; }
        public string DcNumberGivenBySupplier { get; set; }
        public long ReceivedPersonId { get; set; }
        public bool IsDcCompleted { get; set; }

        public string PoReference { get; set; }

        public DateTime? InvoiceDate { get; set; }
        public DateTime? InvoiceAccountDate { get; set; }

        public string InvoiceMode { get; set; }

        public int InvoicePayMode { get; set; }

        public string InvoiceNumber { get; set; }

        public int? PoRefId { get; set; }

        public string SupplierName { get; set; }

        public DateTime CreationTime { get; set; }
        public string PurchaseOrderReferenceFromOtherErp { get; set; }
        public string InvoiceNumberReferenceFromOtherErp { get; set; }

    }

    [AutoMapFrom(typeof(InwardDirectCredit))]
    public class InwardDirectCreditView
    {
        public long Id { get; set; }

        public DateTime DcDate { get; set; }
        public int SupplierRefId { get; set; }
        public string DcNumberGivenBySupplier { get; set; }
        public long ReceivedPersonId { get; set; }
        public bool IsDcCompleted { get; set; }

        public string SupplierName { get; set; }
        public int MaterialCode { get; set; }
        public string MaterialName { get; set; }
        public decimal DcReceivedQty { get; set; }
        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }

        public string MaterialReceivedStatus { get; set; }
        public DateTime CreationTime { get; set; }
        public string PurchaseOrderReferenceFromOtherErp { get; set; }
        public string InvoiceNumberReferenceFromOtherErp { get; set; }

    }

    public class GetInwardDirectCreditInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public string SupplierName { get; set; }
        public int SelectedId { get; set; }

        public int DefaultLocationRefId { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public string DcNumberGivenBySupplier { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime Desc";
            }
        }
    }

    public class InputIdWithPrintFlag
    {
        public int? Id { get; set; }
        public bool PrintFlag { get; set; }
    }

    public class GetInwardDirectCreditForEditOutput : IOutputDto
    {
        public InwardDirectCreditEditDto InwardDirectCredit { get; set; }

        public List<InwardDirectCreditDetailViewDto> InwardDirectCreditDetail { get; set; }
    }

    public class CreateOrUpdateInwardDirectCreditInput : IInputDto
    {
        [Required]
        public InwardDirectCreditEditDto InwardDirectCredit { get; set; }

        [Required]
        public List<InwardDirectCreditDetailViewDto> InwardDirectCreditDetail { get; set; }
    }


    ////For Sample InwardDC
    //public class CreateSampleInwardDirectCreditInput : IInputDto
    //{
    //    [Required]
    //    public InwardDirectCreditEditDto InwardDirectCredit { get; set; }
    //}

    ////For Sample InwardDirectCreditDetail
    //public class CreateOrUpdateInwardDirectCreditDetailInput : IInputDto
    //{

    //    [Required]
    //    public InwardDirectCreditDetailEditDto InwardDirectCreditDetail { get; set; }

    //    [Required]

    //    public InwardDirectCreditListDto InwardDC { get; set; }

    //}

    [AutoMapFrom(typeof(InwardDirectCreditDetail))]
    public class InwardDirectCreditDetailListDto : FullAuditedEntityDto<long>
    {
        public long DcRefId { get; set; }
        public int MaterialRefId { get; set; }
        public decimal DcReceivedQty { get; set; }
        public int UnitRefId { get; set; }

        public string MaterialReceivedStatus { get; set; }
    }

    [AutoMapTo(typeof(InwardDirectCreditDetail))]
    public class GetInwardDirectCreditDetail
    {
        public long DcRefId { get; set; }
        public int MaterialRefId { get; set; }
        public decimal DcReceivedQty { get; set; }
        public int UnitRefId { get; set; }

        public string MaterialReceivedStatus { get; set; }
    }


    public class GetInwardDirectCreditDetailInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    public class PrintOutIn40Cols
    {
        public string HeaderText { get; set; }
        public string BodyText { get; set; }
        public string FooterText { get; set; }

        public string BoldHeaderText { get; set; }
        public string BoldBodyText { get; set; }
        public string BoldFooterText { get; set; }
    }

}