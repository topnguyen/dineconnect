﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using DinePlan.DineConnect.House.Temp;
using DinePlan.DineConnect.House.Master.Dtos;

namespace DinePlan.DineConnect.House.Transaction.Dtos
{
    [AutoMapFrom(typeof(SalesOrder))]
    public class SalesOrderListDto : FullAuditedEntityDto
    {
        public DateTime SoDate { get; set; }
        public string SoReferenceCode { get; set; }
        public int ShippingLocationId { get; set; }

        public string QuoteReference { get; set; }

        public int CreditDays { get; set; }

        public int CustomerRefId { get; set; }

        public string Remarks { get; set; }
        public bool SoStatus { get; set; }
        public decimal SoNetAmount { get; set; }
        public int LocationRefId { get; set; }
        public DateTime? ApprovedTime { get; set; }

        public string SoCurrentStatus { get; set; }
        public int TenantId { get; set; }
    }

    [AutoMapFrom(typeof(SalesOrder))]
    public class SalesOrderListWithRefNameDto : FullAuditedEntityDto
    {
        public int Id { get; set; }
        public DateTime SoDate { get; set; }
        public string SoReferenceCode { get; set; }
        public int ShippingLocationId { get; set; }
        public string ShippingLocationName { get; set; }
        public string QuoteReference { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public int CreditDays { get; set; }
        public int CustomerRefId { get; set; }
        public string CustomerName { get; set; }
        public string Remarks { get; set; }
        public decimal SoNetAmount { get; set; }
        public int LocationRefId { get; set; }
        public int TenantId { get; set; }
        public DateTime? ApprovedTime { get; set; }
        public string SoCurrentStatus { get; set; }
    }

    [AutoMapTo(typeof(SalesOrder))]
    public class SalesOrderEditDto
    {
        public int? Id { get; set; }
        public DateTime SoDate { get; set; }
        public string SoReferenceCode { get; set; }
        public int ShippingLocationId { get; set; }
        public string ShippingLocationRefName { get; set; }
        public string QuoteReference { get; set; }

        public int CreditDays { get; set; }

        public int CustomerRefId { get; set; }
        public string CustomerRefName { get; set; }
        public string Remarks { get; set; }
        public bool SoStatus { get; set; }
        public decimal SoNetAmount { get; set; }
        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }
        public DateTime DeliveryDateExpected { get; set; }
        public DateTime? ApprovedTime { get; set; }
        public string SoCurrentStatus { get; set; }
        public string UserRefName { get; set; }
        public long? CreatorUserId { get; set; }
        public int TenantId { get; set; }
    }

    [AutoMapTo(typeof(SalesOrderDetail))]
    public class SalesOrderDetailViewDto : FullAuditedEntityDto
    {
        public int? Id { get; set; }
        public int SoRefId { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public decimal QtyOrdered { get; set; }
        public decimal QtyReceived { get; set; }
        public decimal QtyPending { get; set; }
        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }
        public decimal Price { get; set; }
        public decimal TotalAmount { get; set; }
        public string DiscountOption { get; set; }
        public decimal DiscountValue { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal NetAmount { get; set; }
        public bool SoStatus { get; set; }
        public string Remarks { get; set; }
        public int DefaultUnitId { get; set; }
        public string Uom { get; set; }
        public decimal AlreadyOrderedQuantity { get; set; }

        public decimal CurrentInHand { get; set; }

        public List<TaxForMaterial> TaxForMaterial { get; set; }
        public List<ApplicableTaxesForMaterial> ApplicableTaxes { get; set; }
    }

    [AutoMapFrom(typeof(SalesOrderTaxDetail))]
    public class SalesOrderTaxDetailViewDto : FullAuditedEntityDto
    {
        public int SoRefId { get; set; }
        public int MaterialRefId { get; set; }
        public int SalesTaxRefId { get; set; }
    }

    public class GetSalesOrderInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public int SalesOrderId { get; set; }

        public int DefaultLocationRefId { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string SoReferenceCode { get; set; }
        public string ReferenceNumber { get; set; }
        public string CustomerName { get; set; }
        public string SoStatus { get; set; }
        public int? MaterialRefId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }

    public class GetSOInput : PagedAndSortedInputDto
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string SoReferenceCode { get; set; }
    }
    public class GetSalesOrderForEditOutput : IOutputDto
    {
        public SalesOrderEditDto SalesOrder { get; set; }
        public List<SalesOrderDetailViewDto> SalesOrderDetail { get; set; }
        public TemplateSaveDto Templatesave { get; set; }
    }

    public class SetSalesOrderDashBoardDto : IOutputDto
    {
        public int TotalPending { get; set; }

        public int TotalApproved { get; set; }

        public int TotalYetToReceived { get; set; }

        public int TotalDeclined { get; set; }


    }

    public class CreateOrUpdateSalesOrderInput : IInputDto
    {
        [Required]
        public SalesOrderEditDto SalesOrder { get; set; }
        [Required]
        public List<SalesOrderDetailViewDto> SalesOrderDetail { get; set; }

        public TemplateSaveDto Templatesave { get; set; }

        public string StatusToBeAssigned { get; set; }
    }

    public class GetSalesIdAndReference : IOutputDto
    {
        public int Id { get; set; }
        public string SoReferenceCode { get; set; }
    }

    public class SalesOrderStatus : IInputDto
    {
        public int SoId { get; set; }
        public string SoReferenceCode { get; set; }
        public string SoCurrentStatus { get; set; }
        public string SoRemarks { get; set; }
    }


    //public class GetObjectFromString : IInputDto
    //{
    //    public string ObjectString { get; set; }
    //}

    public class SetSalesOrderPrint : IInputDto
    {
        public string SalesOrderHTML { get; set; }
        public int SalesOrderId { get; set; }

    }

    public class GetSoReferenceDto : IInputDto
    {
        public int LocationRefId { get; set; }
        public int CustomerRefId { get; set; }
    }

    public class AutoSalesOrderConsolidatedDto : IOutputDto
    {
        public int CustomerRefId { get; set; }
        public string CustomerRefName { get; set; }
        public int NoOfProducts { get; set; }
        public int SoAmount { get; set; }
        public string Remarks { get; set; }
        public CreateOrUpdateSalesOrderInput AutoSoDto { get; set; }
    }

    public class AutoSoDetail : IOutputDto
    {
        public int CustomerRefId { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public decimal OnHand { get; set; }
        public decimal Rol { get; set; }
        public decimal Price { get; set; }
        public decimal MaximumStock { get; set; }
        public decimal QuantityToBeOrdered { get; set; }
        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }
        public decimal AlreadyOrderedQuantity { get; set; }
    }

    public class MaterialVsCustomerDtos : IOutputDto
    {
        public int CustomerRefId { get; set; }
        public string CustomerRefName { get; set; }
        public decimal Price { get; set; }
        public decimal MOQ { get; set; }
        public DateInput LastSalesDate { get; set; }
    }


}
