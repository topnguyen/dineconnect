﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Exporter;
using DinePlan.DineConnect.Exporter.Util;

namespace DinePlan.DineConnect.House.Transaction.Dtos
{
    [AutoMapFrom(typeof(MenuItemWastage))]
    public class MenuItemWastageListDto : FullAuditedEntityDto
    {
        //TODO: DTO MenuItemWastage Properties Missing
        //YOU CAN REFER ATTRIBUTES IN 

        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }
        public DateTime SalesDate { get; set; }

        public DateTime? AccountDate { get; set; }

        public int? AdjustmentRefId { get; set; }
        public string AdjustmentRefName { get; set; }
        public long TokenRefNumber { get; set; }

        public string Remarks { get; set; }

    }
    [AutoMapTo(typeof(MenuItemWastage))]
    public class MenuItemWastageEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO MenuItemWastage Properties Missing
        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }
        public DateTime SalesDate { get; set; }

        public DateTime? AccountDate { get; set; }

        public int? AdjustmentRefId { get; set; }
        public long TokenRefNumber { get; set; }

        public string Remarks { get; set; }

    }

	public class MenuWastageConsolidatedReport
	{
		public int LocationRefId { get; set; }
		public string LocationRefName { get; set; }
		public List<MenuItemWastageDetailEditDto> WastageReport { get; set; }
	}

	[AutoMapTo(typeof(MenuItemWastageDetail))]
    public class MenuItemWastageDetailEditDto :  ExportDataObject
	{
		public int LocationRefId { get; set; }
		public string LocationRefName { get; set; }
		public string MenuWastageMode { get; set; }

		public int MenuItemWastageRefId { get; set; }
        public int Sno { get; set; }
        public int PosMenuPortionRefId { get; set; }
        public string PosMenuPortionCode { get; set; }
        public string PosMenuPortionRefName { get; set; }
        public decimal WastageQty { get; set; }
        public string WastageRemarks { get; set; }
		public decimal CostPerUnit { get; set; }
		public DateTime SalesDate { get; set; }
		public long? UserId { get; set; }
		public string UserName { get; set; }

		public decimal TotalAmount => Math.Round(CostPerUnit * WastageQty, 2);
     }

	public class InputMenuWastageReport
	{
		public ExportType ExportType { get; set; }
		public DateTime? StartDate { get; set; }
		public DateTime? EndDate { get; set; }

        public string DateRange { get; set; }

        public LocationListDto LocationInfo { get; set; }
        public DateTime PrintedTime { get; set; }
        public long? PreparedUserId { get; set; }
        public string PreparedUserName { get; set; }
        public bool CustomReportFlag { get; set; }

        public decimal TotalPurchaseReturnAmount { get; set; }
        public List<LocationListDto> ToLocations { get; set; }
       
		public bool CompExistFlag { get; set; }
	}


	public class GetMenuItemWastageInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

		public int? LocationRefId { get; set; }

		public DateTime? StartDate { get; set; }
		public DateTime? EndDate { get; set; }
		public int? TokenRefNumber { get; set; }

		public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
    public class GetMenuItemWastageForEditOutput : IOutputDto
    {
        public MenuItemWastageEditDto MenuItemWastage { get; set; }
        public List<MenuItemWastageDetailEditDto> MenuItemWastageDetail { get; set; }

    }
    public class CreateOrUpdateMenuItemWastageInput : IInputDto
    {
        [Required]
        public MenuItemWastageEditDto MenuItemWastage { get; set; }

        public List<MenuItemWastageDetailEditDto> MenuItemWastageDetail { get; set; }
    }
}

