﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Exporter;
using DinePlan.DineConnect.Exporter.Util;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Location;

namespace DinePlan.DineConnect.House.Transaction.Dtos
{
    [AutoMapFrom(typeof(InterTransfer))]
    public class InterTransferListDto : FullAuditedEntityDto
    {
        public int LocationRefId { get; set; }

        public int RequestLocationRefId { get; set; }
        public string DocReferenceNumber { get; set; }
        public DateTime RequestDate { get; set; }
        public DateTime DeliveryDateRequested { get; set; }
        public string CurrentStatus { get; set; }

        public DateTime? CompletedTime { get; set; }

        public DateTime? ReceivedTime { get; set; }

        public int ApprovedPersonId { get; set; }

        public int ReceivedPersonId { get; set; }

        public decimal? TransferValue { get; set; }

        public decimal? ReceivedValue { get; set; }

        public int? AdjustmentRefId { get; set; }


        public int? InterTransferRefId { get; set; }

    }

    [AutoMapFrom(typeof(InterTransfer))]
    public class InterTransferViewDto : FullAuditedEntityDto
    {
        public int LocationRefId { get; set; }

        public string LocationRefName { get; set; }

        public int RequestLocationRefId { get; set; }

        public string RequestLocationRefName { get; set; }
        public string DocReferenceNumber { get; set; }
        public DateTime RequestDate { get; set; }
        public DateTime DeliveryDateRequested { get; set; }

        public string CurrentStatus { get; set; }

        public string PoStatusRemarks { get; set; }

        public DateTime? CompletedTime { get; set; }

        public DateTime? ReceivedTime { get; set; }

        public decimal? TransferValue { get; set; }

        public decimal? ReceivedValue { get; set; }

        public int? AdjustmentRefId { get; set; }

        public int? InterTransferRefId { get; set; }

        public string VehicleNumber { get; set; }
        public string PersonInCharge { get; set; }
        public bool DeliverDateAlertFlag { get; set; }

        public int NoOfMaterials { get; set; }
        public int NoOfPoDependent { get; set; }
        public int NoOfStockDependent { get; set; }
        public bool DoesSplitNeeded { get; set; }
        public bool DoesThisRequestToBeConvertedAsPO { get; set; }
        public List<PoStatusWiseList> PoStatusWiseLists { get; set; }

        public int? ParentRequestRefId { get; set; }
        public int Id { get; set; }

    }

    public class InterRequestDetailShowDto
    {
        public string RequestLocationRefName { get; set; }

    }

    [AutoMap(typeof(InterTransfer))]
    public class InterTransferEditDto
    {
        public int? Id { get; set; }
        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }
        public int RequestLocationRefId { get; set; }
        public string RequestLocationRefName { get; set; }
        public string DocReferenceNumber { get; set; }
        public DateTime RequestDate { get; set; }
        public DateTime DeliveryDateRequested { get; set; }
        public string CurrentStatus { get; set; }
        public DateTime? CompletedTime { get; set; }
        public DateTime? ReceivedTime { get; set; }
        public int ApprovedPersonId { get; set; }
        public int ReceivedPersonId { get; set; }
        public decimal? TransferValue { get; set; }
        public decimal? ReceivedValue { get; set; }
        public DateTime? CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public int? AdjustmentRefId { get; set; }
        public int? InterTransferRefId { get; set; }
        public string VehicleNumber { get; set; }
        public string PersonInCharge { get; set; }
        public string TransferValueInWords { get; set; }
        public int? ParentRequestRefId { get; set; }
        public string TimeElapsedAlert { get; set; }

    }

    [AutoMapFrom(typeof(InterTransferDetail))]
    public class InterTransferDetailListDto : FullAuditedEntityDto
    {
        public int InterTransferRefId { get; set; }

        public int Sno { get; set; }

        public int MaterialRefId { get; set; }
        public string MaterialPetName { get; set; }
        public string MaterialRefName { get; set; }

        public decimal RequestQty { get; set; }

        public decimal IssueQty { get; set; }

        public string CurrentStatus { get; set; }

        public string RequestRemarks { get; set; }

        public string ApprovedRemarks { get; set; }
        public int? PurchaseOrderRefId { get; set; }

    }


    [AutoMap(typeof(InterTransferDetail))]
    public class InterTransferDetailViewDto
    {
        public int? Id { get; set; }
        public int InterTransferRefId { get; set; }
        public int Sno { get; set; }
        public string Barcode { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialGroupRefName { get; set; }
        public string MaterialGroupCategoryRefName { get; set; }
        public string MaterialPetName { get; set; }
        public string MaterialRefName { get; set; }
        public int MaterialTypeId { get; set; }
        public string MaterialTypeName { get; set; }
        public decimal RequestQty { get; set; }

        public decimal IssueQty { get; set; }
        public decimal Price { get; set; }

        [Required, StringLength(1)]                          //  P - Pending  (Default ) , C - Completed, X - Cancel
        public string CurrentStatus { get; set; }

        public string RequestRemarks { get; set; }

        public string ApprovedRemarks { get; set; }
        public string DocReferenceNumber { get; set; }
        public string Uom { get; set; }

        public decimal OnHand { get; set; }
        public string OnHandWithUom { get; set; }
        public List<MaterialUsagePattern> UsagePatternList { get; set; }

        public decimal ToLocationOnHand { get; set; }
        public int UnitRefId { get; set; }

        public string UnitRefName { get; set; }

        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }

        public decimal HoldQty { get; set; }

        public bool HoldFlag { get; set; }

        public decimal LineTotal { get; set; }
        public decimal ReservedIssueQuantity { get; set; }
        public decimal ReservedYieldQuantity { get; set; }
        public decimal ReservedQuantity => ReservedIssueQuantity + ReservedYieldQuantity;
        public int? PurchaseOrderRefId { get; set; }
        public string PoCurrentStatus { get; set; }
    }

    [AutoMap(typeof(InterTransferReceivedDetail))]
    public class InterTransferReceivedEditDto
    {
        public int? Id { get; set; }

        public int InterTransferRefId { get; set; }


        public int Sno { get; set; }


        public int MaterialRefId { get; set; }

        public string MaterialPetName { get; set; }
        public string MaterialRefName { get; set; }
        public string DocReferenceNumber { get; set; }

        public decimal RequestQty { get; set; }

        public decimal IssueQty { get; set; }


        public decimal ReceivedQty { get; set; }

        [Required]
        public decimal ReturnQty { get; set; }

        [Required]
        public string AdjustmentMode { get; set; }

        [Required]
        public decimal AdjustmentQty { get; set; }

        public int UnitRefId { get; set; }

        public string UnitRefName { get; set; }
        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }

        public decimal Price { get; set; }

        public decimal ReceivedValue { get; set; }

        [Required, StringLength(1)]  //  P - Pending  (Default ) , C - Completed, X - Cancel
        public string CurrentStatus { get; set; }

        public string Uom { get; set; }

        public bool AdjustmentFlag { get; set; }
        public bool ReturnFlag { get; set; }

        public decimal LineTotal { get; set; }

    }

    [AutoMap(typeof(InterTransferReceivedDetail))]
    public class InterTransferReceivedViewDto
    {
        public int? Id { get; set; }
        public int InterTransferRefId { get; set; }
        public int Sno { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialPetName { get; set; }
        public string MaterialRefName { get; set; }
        public decimal RequestQty { get; set; }
        public decimal ReceivedQty { get; set; }
        public int UnitRefId { get; set; }
        public decimal Price { get; set; }
        public decimal ReceivedValue { get; set; }
        [Required, StringLength(1)]  //  P - Pending  (Default ) , C - Completed, X - Cancel
        public string CurrentStatus { get; set; }

    }

    public class ConsolidatedRequestDto
    {
        public List<InterTransferViewDto> RequestList { get; set; }
    }

    public class GetInterTransferInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }
        public long UserId { get; set; }

        public int DefaultLocationRefId { get; set; }

        public List<SimpleLocationDto> Locations { get; set; }

        public LocationGroupDto LocationGroup { get; set; }

        public List<LocationTag> LocationTags { get; set; }

        public string RequestStatus { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public DateTime? DeliveryStartDate { get; set; }
        public DateTime? DeliveryEndDate { get; set; }
        public string CurrentStatus { get; set; }
        public bool ShowOnlyDeliveryDateExpectedAsOnDate { get; set; }
        public List<InterTransferViewDto> RequestList { get; set; }
        public bool RequestBasedOnRequestList { get; set; }
        public bool OmitAlreadyPoLinkedRequest { get; set; }
        public bool OmitQueueInOrder { get; set; }
        public int MaterialGroupCategoryRefId { get; set; }
        public int MaterialGroupRefId { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }

        public List<int> TransferRequestIds { get; set; }

        public GetInterTransferInput()
        {
            TransferRequestIds = new List<int>();
        }

        public bool DoesPendingRequest { get; set; }
        public bool DoesApprovedRequest { get; set; }
        public bool DoesReceivedStatus { get; set; }
    }




    public class GetInterApprovalDataFromId
    {
        public int? Id { get; set; }
        public List<MaterialRateViewListDto> MaterialRateView { get; set; }

        public bool PrintFlag { get; set; }
    }

    public class GetInterTransferForEditOutput
    {
        public InterTransferEditDto InterTransfer { get; set; }

        public List<InterTransferDetailViewDto> InterTransferDetail { get; set; }

        public TemplateSaveDto Templatesave { get; set; }

    }

    public class CreateOrUpdateInterTransferInput
    {

        public InterTransferEditDto InterTransfer { get; set; }

        public List<InterTransferDetailViewDto> InterTransferDetail { get; set; }

        public TemplateSaveDto Templatesave { get; set; }

        public string SaveStatus { get; set; }
    }

    //  Inter Transfer - Received Details

    public class GetInterReceivedForEditOutput
    {
        public InterTransferEditDto InterTransfer { get; set; }

        public List<InterTransferReceivedEditDto> InterTransferReceivedDetail { get; set; }


        //public InterRequestDetailShowDto RequestDetail { get; set; }
    }

    public class CreateOrUpdateInterReceivedInput
    {

        public InterTransferEditDto InterTransfer { get; set; }

        public List<InterTransferReceivedEditDto> InterTransferReceivedDetail { get; set; }

        public string SaveStatus { get; set; }

    }

    public class InterTransferReceivedWithAdjustmentId
    {
        public int Id { get; set; }

        public int AdjustmentId { get; set; }
        public int ReturnTransferId { get; set; }
    }

    public class SetInterTransferDashBoardDto
    {
        public int TotalPending { get; set; }

        public int TotalApproved { get; set; }

        public int TotalReceived { get; set; }

        public int TotalRejected { get; set; }

    }

    public class InterTransferRequestDetailViewDto
    {
        public int InterTransferRefId { get; set; }

        public int LocationRefId { get; set; }

        public string LocationRefCode { get; set; }
        public string LocationRefName { get; set; }

        public int Sno { get; set; }

        public int MaterialRefId { get; set; }
        public string MaterialPetName { get; set; }
        public string MaterialRefName { get; set; }
        public string MaterialGroupRefName { get; set; }
        public string MaterialGroupCategoryRefName { get; set; }
        public int MaterialTypeId { get; set; }

        public string MaterialTypeName { get; set; }

        public decimal RequestQty { get; set; }

        public decimal IssueQty { get; set; }
        public int UnitRefId { get; set; }

        public string UnitRefName { get; set; }

        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }

        public int TransferUnitId { get; set; }
        public string TransferUnitName { get; set; }

        [Required, StringLength(1)]                          //  P - Pending  (Default ) , C - Completed, X - Cancel
        public string CurrentStatus { get; set; }

        public string RequestRemarks { get; set; }

        public string ApprovedRemarks { get; set; }

        public string Uom { get; set; }

        public decimal OnHand { get; set; }
        public decimal MinimumStock { get; set; }
        public decimal MaximumStock { get; set; }

        public decimal ReorderLevel { get; set; }

        public List<MaterialUsagePattern> UsagePatternList { get; set; }
    }

    public class InterTransferPOGenerationDto
    {
        public string Barcode { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialPetName { get; set; }
        public string MaterialRefName { get; set; }
        public int MaterialTypeId { get; set; }
        public string MaterialTypeName { get; set; }
        public decimal TotalRequestQty { get; set; }
        public int UnitRefId { get; set; }
        public string Uom { get; set; }
        public decimal AlreadyOrderedQuantity { get; set; }
        public decimal LiveStock { get; set; }
        public decimal OnHandAfterIssue { get; set; }
        public decimal MinimumStock { get; set; }
        public decimal MaximumStock { get; set; }
        public decimal ReOrderLevel { get; set; }
        public decimal PoOrderQuantityRecommended { get; set; }
        public decimal PoOrderQuantity { get; set; }
        public string Remarks { get; set; }
        public string FormulaForRecommended { get; set; }
        public List<InterTransferRequestConsolidatedViewDto> LocationWiseRequestDetails { get; set; }
        public SupplierMaterialViewDto DefaultSupplier { get; set; }
        public List<SupplierMaterialViewDto> SupplierMaterialViewDtos { get; set; }
        public string SupplierRemarks { get; set; }
        public List<InterTransferDetailWithPORefIds> InterTransferDetailWithPORefIds { get; set; }
        public bool OmitQueueInOrder { get; set; }
    }

    public class InterTransferRequestList
    {
        public List<InterTransferViewDto> InterTransferViewDtos { get; set; }
        public int RequestLocationRefId { get; set; }
        public bool DoesPendingRequest { get; set; }
        public bool DoesApprovedRequest { get; set; }
        public bool DoesReceivedStatus { get; set; }
    }

    public class ExportToExcelDataConsolidated
    {
        public List<InterTransferRequestConsolidatedViewDto> ExportData { get; set; }
        public string FileName { get; set; }
        public bool DoesPendingRequest { get; set; }
        public bool DoesApprovedRequest { get; set; }
        public bool DoesReceivedStatus { get; set; }
    }

    public class InterTransferRequestConsolidatedViewDto
    {
        public int LocationRefId { get; set; }
        public string LocationRefCode { get; set; }
        public string LocationRefName { get; set; }

        public int MaterialRefId { get; set; }
        public string MaterialPetName { get; set; }
        public string MaterialRefName { get; set; }
        public string MaterialGroupRefName { get; set; }
        public string MaterialGroupCategoryRefName { get; set; }
        public int MaterialTypeId { get; set; }

        public string MaterialTypeName { get; set; }

        public decimal RequestQty { get; set; }
        public int UnitRefId { get; set; }

        public string RequestRemarks { get; set; }

        public string Uom { get; set; }

        //public decimal OnHand { get; set; }
        public decimal LiveStock { get; set; }
        public decimal MinimumStock { get; set; }
        public decimal MaximumStock { get; set; }
        public decimal ReorderLevel { get; set; }
        public List<InterTransferDetailWithPORefIds> InterTransferDetailWithPORefIds { get; set; }
    }

    public class DirectTransferImportDto
    {
        public int InterTransferRefId { get; set; }
        public int Sno { get; set; }
        public int RequestLocationRefId { get; set; }
        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }

        public string RequestLocationName { get; set; }
        public string Barcode { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialPetName { get; set; }
        public string MaterialRefName { get; set; }
        public decimal RequestQty { get; set; }
        public decimal CurrentStatus { get; set; }
        public string RequestRemarks { get; set; }
        public string ApprovedRemarks { get; set; }
        public int Id { get; set; }
        public string MaterialTypeName { get; set; }
        public int UnitRefId { get; set; }
        public string Uom { get; set; }
        public decimal OnHand { get; set; }
        public List<MaterialUsagePattern> UsagePatternList { get; set; }
        public bool Currenteditstatus { get; set; }
    }

    public class TransferTemplateDto
    {
        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }
        public string MaterialGroupCategoryName { get; set; }
        public string Barcode { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialPetName { get; set; }
        public string MaterialRefName { get; set; }
        public decimal CurrentOnHand { get; set; }
        public string Uom { get; set; }
    }

    public class InputInterTransferReport
    {
        public ExportType ExportType { get; set; }
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string DateRange { get; set; }

        public int LocationRefId { get; set; }

        public List<LocationListDto> ToLocations { get; set; }
        public List<MaterialListDto> MaterialList { get; set; }

        public List<ComboboxItemDto> MaterialTypeList { get; set; }

        //public int? MaterialRefId { get; set; }

        public string TransferNumber { get; set; }

        public bool ExactSearchFlag { get; set; }

        public bool ConsolidatedFlag { get; set; }

    }

    public class InterTransferReportDto
    {
        public LocationListDto LocationInfo { get; set; }
        public decimal TotalTransferAmount { get; set; }
        public string TransferStatus { get; set; }
        public string DateRange { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime PrintedTime { get; set; }
        public long? PreparedUserId { get; set; }
        public string PreparedUserName { get; set; }
        public bool DoesCustomReportFormat { get; set; }

        public List<InterTransferReportDetailOutPut> DetailReport { get; set; }
        public List<InterTransferReportDetailOutPut> ConsolidatedReport { get; set; }
        public List<InterTransferReportDetailOutPut> LocationWiseConsolidatedTransferReport { get; set; }

    }

    public class InterTransferReportDetailOutPut : ExportDataObject
    {
        public int TransferRefId { get; set; }
        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }
        public DateTime TransferDate { get; set; }
        public DateTime? ReceivedTime { get; set; }
        public int MaterialGroupCategoryRefId { get; set; }
        public int MaterialTypeRefId { get; set; }
        public string MaterialTypeRefName { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialPetName { get; set; }
        public string MaterialRefName { get; set; }
        public decimal TotalQty { get; set; }
        public decimal QuantityInPurchaseUnit { get; set; }  //  Purchase Unit
        public decimal QuantityInStkAdjustedInventoryUnit { get; set; }   // Inventory Unit
        public decimal QuantityInIssueUnit { get; set; } // Recipe Unit  //  Except Default and Stock adj unit all units are as Recipe Unit

        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }
        public string Uom { get; set; }
        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }
        public decimal Price { get; set; }
        public decimal TotalAmount { get; set; }
        public string CurrentStatus { get; set; }
        public string Remarks { get; set; }
        public string VehicleNumber { get; set; }
        public string PersonInCharge { get; set; }
    }

    public class SetInterTransferPrint
    {
        public string InterTransferHTML { get; set; }
        public int? InterTransferId { get; set; }
    }

    public class LocationAndRequestLocationDto
    {
        public int LocationRefId { get; set; }
        public int RequestLocationRefId { get; set; }
    }

    public class LocationAndMaterialDto
    {
        public int LocationRefId { get; set; }
        public int MaterialRefId { get; set; }
        public DateTime TransactionDate { get; set; }
    }

    public class MultipleLocationAndMultipleMaterialDto
    {
        public int LocationRefId { get; set; }
        public int MaterialRefId { get; set; }
        public List<int> MaterialRefIdList { get; set; }
        public DateTime TransactionDate { get; set; }
        public MultipleLocationAndMultipleMaterialDto()
        {
            MaterialRefIdList = new List<int>();
        }
    }

    public class StatusConsolidatedDto
    {
        public int FromLocationRefId { get; set; }
        public string FromLocationRefName { get; set; }
        public int ToLocationRefId { get; set; }
        public string ToLocationRefName { get; set; }
        public int TransferRefId { get; set; }
        public DateTime TransferDate { get; set; }
        public List<InterTransferStatusReportDto> TransferList { get; set; }
    }

    public class InterTransferStatusReportDto
    {
        public int FromLocationRefId { get; set; }
        public string FromLocationRefName { get; set; }
        public int ToLocationRefId { get; set; }
        public string ToLocationRefName { get; set; }
        public int TransferRefId { get; set; }
        public DateTime TransferDate { get; set; }
        public int MaterialGroupCategoryRefId { get; set; }
        public int MaterialTypeRefId { get; set; }
        public string MaterialTypeRefName { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialPetName { get; set; }
        public string MaterialRefName { get; set; }
        public decimal IssuedQty { get; set; }
        public decimal ReceivedQty { get; set; }
        public decimal DiffQty { get; set; }
        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }
        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }
        public decimal Price { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal DiffAmount { get; set; }
        public string CurrentStatus { get; set; }
        public string TransferStatus { get; set; }
        public string Remarks { get; set; }
        public string VehicleNumber { get; set; }
        public string PersonInCharge { get; set; }
    }

    public class InterTransferDashBoardRequestAlertDto
    {
        public InterTransferDashBoardRequestAlertDto()
        {
            ApprovalPendingDtos = new List<InterTransferViewDto>();
            TransferPendingDtos = new List<InterTransferViewDto>();
            ReceivePendingDtos = new List<InterTransferViewDto>();

        }
        public List<InterTransferViewDto> ApprovalPendingDtos { get; set; }
        public List<InterTransferViewDto> TransferPendingDtos { get; set; }
        public List<InterTransferViewDto> ReceivePendingDtos { get; set; }


        public int ApprovalPendingCount => ApprovalPendingDtos.Count;
        public int TransferPendingCount => TransferPendingDtos.Count;
        public int ReceivePendingCount => ReceivePendingDtos.Count;

    }

    public class RequestTimeLockDtos
    {
        public string AlertMsg { get; set; }

        public bool AllowFlag { get; set; }
    }


    public class BulkApprovalInputDto
    {
        public List<IdInput> BulkApprovalInputs { get; set; }
    }

    public class InterTransferReceivedGroupCategoryMaterialWiseAnalysisDto
    {
        public DateTime TransactionTime { get; set; }
        public string Action { get; set; } // + Purchase / Recd , - Issue / Return
        public int LocationRefId { get; set; }
        public string LocationRefCode { get; set; }
        public string LocationRefName { get; set; }
        public int RequestLocationRefId { get; set; }
        public string RequestLocationRefName { get; set; }

        public int InterTrasnferRefId { get; set; }
        public string ReferenceNumber { get; set; }
        public DateTime ReceivedTime { get; set; }
        public int MaterialGroupRefId { get; set; }
        public string MaterialGroupRefName { get; set; }
        public int MaterialGroupCategoryRefId { get; set; }
        public string MaterialGroupCategoryRefName { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialPetName { get; set; }
        public string MaterialRefName { get; set; }
        public decimal ReceivedQty { get; set; }
        public int UnitRefId { get; set; }

        public string UnitRefName { get; set; }
        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }
        public int TransferUnitId { get; set; }
        public string TransferUnitName { get; set; }
        public decimal Price { get; set; }

        public decimal ReceivedValue { get; set; }

        public decimal TotalAmount { get; set; }
        public string Remarks { get; set; }
    }


    public class InterTransferReceivedReportDto
    {
        public int LocationRefId { get; set; }
        public string LocationRefCode { get; set; }
        public int MaterialGroupRefId { get; set; }
        public string MaterialGroupRefName { get; set; }
        public int MaterialGroupCategoryRefId { get; set; }
        public string MaterialGroupCategoryRefName { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterailRefName { get; set; }
        public decimal TotalRecdQty { get; set; }
        public decimal AvgPrice { get; set; }
        public decimal TotalRecdValue { get; set; }
        public List<InterTransferReceivedGroupCategoryMaterialWiseAnalysisDto> ReceivedDetails { get; set; }
    }

    public class InterTransferReceivedExportReportDto
    {
        public List<InterTransferReceivedReportDto> InterTransferReceivedReportDtos { get; set; }
        public ClosingStockExportGroupCategoryMaterialDto InputFilterDto { get; set; }
    }

    


}
