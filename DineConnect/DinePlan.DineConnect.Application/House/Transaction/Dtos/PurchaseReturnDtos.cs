﻿

using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Connect.Master;

namespace DinePlan.DineConnect.House.Transaction.Dtos
{
    [AutoMapFrom(typeof(PurchaseReturn))]
    public class PurchaseReturnListDto : FullAuditedEntityDto
    {
        public int LocationRefId { get; set; }

        public string LocationRefName { get; set; }

        public int SupplierRefId { get; set; }
        public string SupplierCode { get; set; }

        public string SupplierRefName { get; set; }

        public DateTime ReturnDate { get; set; }

        public string ReferenceNumber { get; set; }

        public int? DcRefId { get; set; }

        public int? InvoiceRefId { get; set; }
        public decimal PurchaseReturnAmount { get; set; }

        public string Remarks { get; set; }

        public virtual int TenantId { get; set; }

        public virtual string ReturnDateAsString { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal AmountWithoutTax { get; set; }
    }
    [AutoMapTo(typeof(PurchaseReturn))]
    public class PurchaseReturnEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO PurchaseReturn Properties Missing

        public int LocationRefId { get; set; }

        public int SupplierRefId { get; set; }

        public string SupplierCode { get; set; }

        public DateTime ReturnDate { get; set; }

        public string ReferenceNumber { get; set; }

        public int? DcRefId { get; set; }

        public int? InvoiceRefId { get; set; }

        public decimal PurchaseReturnAmount { get; set; }
        public string InvoiceReferenceNumber { get; set; }
        public string Remarks { get; set; }
        public string PurchaseOrderReferenceFromOtherErp { get; set; }
        public string InvoiceNumberReferenceFromOtherErp { get; set; }
        public virtual int TenantId { get; set; }
    }

    [AutoMapFrom(typeof(PurchaseReturnDetail))]
    public class PurchaseReturnDetailViewDto : FullAuditedEntityDto
    {
        public int PurchaseReturnRefId { get; set; }

        public int Sno { get; set; }

        public int MaterialGroupRefId { get; set; }

        public int MaterialGroupCategoryRefId { get; set; }

        public int MaterialRefId { get; set; }

        public string MaterialRefName { get; set; }


        public decimal Quantity { get; set; }

        public decimal Price { get; set; }

        public decimal TotalAmount { get; set; }

        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }
        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }
        public string Uom { get; set; }

        public decimal TaxAmount { get; set; }

        public decimal NetAmount { get; set; }

        public string Remarks { get; set; }

        public int SupplierRefId { get; set; }
        
        public string ReferenceNumber { get; set; }

        public List<TaxForMaterial> TaxForMaterial { get; set; }

        public List<ApplicableTaxesForMaterial> ApplicableTaxes { get; set; }

        public string SupplierMaterialAliasName { get; set; }


    }

    [AutoMapFrom(typeof(PurchaseReturnTaxDetail))]
    public class PurchaseReturnTaxDetailViewDto : FullAuditedEntityDto
    {
        public int PurchaseReturnRefId { get; set; }
        public int Sno { get; set; }
        public int MaterialRefId { get; set; }
        public int TaxRefId { get; set; }
        public decimal TaxRate { get; set; }
        public decimal TaxValue { get; set; }
    }

    public class GetPurchaseReturnInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string SupplierName { get; set; }
        public string ReferenceNumber { get; set; }
        public int LocationRefId { get; set; }
        public bool TaxShowFlag { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime Desc";
            }
        }
    }
    public class GetPurchaseReturnForEditOutput : IOutputDto
    {
        public PurchaseReturnEditDto PurchaseReturn { get; set; }

        public List<PurchaseReturnDetailViewDto> PurchaseReturnDetail { get; set; }
    }

    public class CreateOrUpdatePurchaseReturnInput : IInputDto
    {
        public PurchaseReturnEditDto PurchaseReturn { get; set; }

        public List<PurchaseReturnDetailViewDto> PurchaseReturnDetail { get; set; }

    }


    public class PurchaseReturnDetailInputDto
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int LocationRefId { get; set; }
        public int? SupplierRefId { get; set; }
        public int? MaterialRefId { get; set; }
        public string InvoiceNumber { get; set; }
        public bool ExactSearchFlag { get; set; }
        public int? InvoiceId { get; set; }
        public bool DoesChangePurchaseQuantityInDefaultUnit { get; set; }
    }

    public class PurchaseReturnDetailOutputDto
    {
        public LocationListDto LocationInfo { get; set; }
        public string DateRange { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime PrintedTime { get; set; }
        public long? PreparedUserId { get; set; }
        public string PreparedUserName { get; set; }
        
        public decimal TotalPurchaseReturnAmount { get; set; }
        public decimal TotalTaxAmount { get; set; }
        public decimal NetPurchaseReturnAmount { get; set; }
        public List<PurchaseReturnDetailReportDto> ReturnList { get; set; }
        public bool DoesCustomReportFormat { get; set; }
    }

    [AutoMapFrom(typeof(PurchaseReturnDetail))]
    public class PurchaseReturnDetailReportDto : FullAuditedEntityDto
    {
        public int PurchaseReturnRefId { get; set; }

        public int Sno { get; set; }

        public int MaterialGroupRefId { get; set; }

        public int MaterialGroupCategoryRefId { get; set; }

        public int MaterialRefId { get; set; }
        public string MaterialPetName { get; set; }

        public string MaterialRefName { get; set; }

        public decimal Quantity { get; set; }

        public decimal QuantityInPurchaseUnit { get; set; }
        public decimal QuantityInStkAdjustedUnit { get; set; }
        public decimal QuantityInIssueUnit { get; set; }
        public decimal Price { get; set; }

        public decimal TotalAmount { get; set; }

        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }
        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }
        public string Uom { get; set; }

        public decimal TaxAmount { get; set; }

        public decimal NetAmount { get; set; }

        public string Remarks { get; set; }

        public int SupplierRefId { get; set; }
        public DateTime ReturnDate { get; set; }
        public string SupplierRefName { get; set; }

        public string ReferenceNumber { get; set; }

        public List<TaxForMaterial> TaxForMaterial { get; set; }

        public List<ApplicableTaxesForMaterial> ApplicableTaxes { get; set; }

    }

}
