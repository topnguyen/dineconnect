﻿

using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using DinePlan.DineConnect.House.Master.Dtos;

namespace DinePlan.DineConnect.House.Transaction.Dtos
{
    [AutoMapFrom(typeof(Yield))]
    public class YieldListDto : FullAuditedEntityDto
    {
        public int LocationRefId { get; set; }

        public string RequestSlipNumber { get; set; }

        public DateTime IssueTime { get; set; }

        public DateTime? CompletedTime { get; set; }

        public int TokenNumber { get; set; }

        public string Status { get; set; }

        public int? RecipeRefId { get; set; }

        public decimal? RecipeProductionQty { get; set; }

        public int? ProductionUnitRefId { get; set; }
        public string ProductionUnitRefName { get; set; }


        public virtual int TenantId { get; set; }

    }
    [AutoMapTo(typeof(Yield))]
    public class YieldEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO Yield Properties Missing
        public int LocationRefId { get; set; }

        public string RequestSlipNumber { get; set; }

        public DateTime IssueTime { get; set; }

        public DateTime? CompletedTime { get; set; }

        public int? ProductionUnitRefId { get; set; }

        public int TokenNumber { get; set; }

        public string Status { get; set; }

        public int? RecipeRefId { get; set; }

		public string RecipeRefName { get; set; }

        public decimal? RecipeProductionQty { get; set; }

        public virtual int TenantId { get; set; }

    }

    [AutoMapTo(typeof(YieldInput))]
    public class YieldInputViewDto
    {
        public int? Id { get; set; }
        public int YieldRefId { get; set; }
        public int Sno { get; set; }

        public int InputMaterialRefId { get; set; }

        public string MaterialRefName { get; set; }

        public decimal InputQty { get; set; }

        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }

        public string Uom { get; set; }

        public decimal CurrentInHand { get; set; }

        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }
    }


    [AutoMapTo(typeof(YieldOutput))]
    public class YieldOutputViewDto
    {
        public int? Id { get; set; }
        public int YieldRefId { get; set; }
        public int Sno { get; set; }
        public int OutputMaterialRefId { get; set; }

        public string MaterialRefName { get; set; }

        public decimal OutputQty { get; set; }
		public decimal YieldPrice { get; set; }

		public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }

        public string Uom { get; set; }

        public decimal CurrentInHand { get; set; }

        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }
    }


    public class GetYieldInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public int DefaultLocationRefId { get; set; }
        public int? RequestSlipNumber { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetYieldForEditOutput : IOutputDto
    {
        public YieldEditDto Yield { get; set; }
        public List<YieldInputViewDto> YieldInputDetail { get; set; }
        public List<YieldOutputViewDto> YieldOutputDetail { get; set; }
        public TemplateSaveDto Templatesave { get; set; }
    }
    public class CreateOrUpdateYieldInput : IInputDto
    {
        [Required]
        public YieldEditDto Yield { get; set; }
        public List<YieldInputViewDto> YieldInputDetail { get; set; }
        public List<YieldOutputViewDto> YieldOutputDetail { get; set; }
        public TemplateSaveDto Templatesave { get; set; }
        public string StatusToBeAssigned { get; set; }
    }

    public class SetYieldDashBoardDto : IOutputDto
    {
        public int TotalPending { get; set; }

        public int TotalFinished { get; set; }

    }
}

