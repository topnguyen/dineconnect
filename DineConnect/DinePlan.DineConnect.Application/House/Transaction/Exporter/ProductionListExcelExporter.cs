﻿
using System.Collections.Generic;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Transaction.Exporter
{
    public class ProductionListExcelExporter : FileExporterBase, IProductionListExcelExporter
    {
        public FileDto ExportToFile(List<ProductionListDto> dtos)
        {
            return CreateExcelPackage(
                "ProductionList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Production"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("ProductionSlipNumber"),
                        L("ProductionTime")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.ProductionSlipNumber,
                        _ => _.ProductionTime.ToString("yyyy-MMM-dd")
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
