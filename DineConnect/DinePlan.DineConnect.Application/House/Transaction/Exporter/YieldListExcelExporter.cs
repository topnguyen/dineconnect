﻿using System.Collections.Generic;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Transaction.Exporter
{
    public class YieldListExcelExporter : FileExporterBase, IYieldListExcelExporter
    {
        public FileDto ExportToFile(List<YieldListDto> dtos)
        {
            return CreateExcelPackage(
                "YieldList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Yield"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("YieldReferece"),
                        L("IssueTime"),
                        L("Status"),
                        L("CompletedTime")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.RequestSlipNumber,
                        _ => _.IssueTime.ToString("yyyy-MMM-dd"),
                        _ => _.Status,
                        _ => _.CompletedTime == null?"":_.CompletedTime.Value.ToString("yyyy-MMM-dd")
                      );

                    for (var i = 1; i <= 7; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}