﻿

using System.Collections.Generic;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Transaction.Exporter
{
    public class SalesInvoiceListExcelExporter : FileExporterBase, ISalesInvoiceListExcelExporter
    {
        public FileDto ExportToFile(List<SalesInvoiceListDto> dtos)
        {
            return CreateExcelPackage(
                "SalesInvoiceList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("SalesInvoice"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                         L("CustomerName"),
                          L("InvoiceNumber"),
                        L("InvoiceDate"),
                        L("InvoiceAmount"),
                        L("AccountDate")
                       
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.CustomerRefName,
                        _ => _.InvoiceNumber,
                        _ => _.InvoiceDate.ToString("yyyy-MMM-dd"),
                        _ => _.InvoiceAmount,
                        _ => _.AccountDate.Value.ToString("yyyy-MMM-dd")
                        
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
