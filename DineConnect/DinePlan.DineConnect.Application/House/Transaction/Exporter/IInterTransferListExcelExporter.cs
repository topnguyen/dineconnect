﻿
using System.Collections.Generic;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using System;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.House.Transaction
{
    public interface IInterTransferListExcelExporter
    {
        FileDto ExportToFile(List<InterTransferViewDto> dtos);
        FileDto ExportConsolidated(ExportToExcelDataConsolidated input);
        FileDto CreateDirectTransferTemplate(List<TransferTemplateDto> dtos, string location, string dateRange);
        Task<List<FileDto>> ExportDetailToFile(InterTransferReportDto output, InputInterTransferReport input);
        Task <List<FileDto>> ExportDetailToPdfOrHtml(InterTransferReportDto dtos, InputInterTransferReport input, string reportName);

        FileDto TransferStatusExportToFile(List<StatusConsolidatedDto> dtos, DateTime FromDate, DateTime ToDate, string UserName);

        FileDto ExportInterTrasnferReceivedDetailConsolidatedGroupCategoryMaterials(LocationWiseClosingStockGroupCateogryMaterialDto input);

    
    }
}

