﻿using System.Collections.Generic;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Transaction.Exporter
{
    public class PurchaseOrderListExcelExporter : FileExporterBase, IPurchaseOrderListExcelExporter
    {
        public FileDto ExportToFile(List<PurchaseOrderListWithRefNameDto> dtos)
        {
            return CreateExcelPackage(
                "PurchaseOrderList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("PurchaseOrder"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Date"),
                        L("Reference"),
                        L("SupplierName"),
                        L("ShippingLocation"),
                        L("Status"),
                        L("NetAmount")

                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.PoDate.ToString("yyyy-MMM-dd"),
                        _ => _.PoReferenceCode,
                        _ => _.SupplierName,
                        _ => _.ShippingLocationName,
                        _ => _.PoCurrentStatus,
                        _ => _.PoNetAmount

                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}