﻿
using System.Collections.Generic;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Transaction.Exporter
{
    public class RequestListExcelExporter : FileExporterBase, IRequestListExcelExporter
    {
        public FileDto ExportToFile(List<RequestListDto> dtos)
        {
            return CreateExcelPackage(
                "RequestList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Return"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("RequestRef"),
                        L("RequestTime"),
                        L("CompletedStatus")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.RequestSlipNumber,
                        _ => _.RequestTime.ToString("yyyy-MMM-dd"),
                        _ => _.CompletedStatus
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
