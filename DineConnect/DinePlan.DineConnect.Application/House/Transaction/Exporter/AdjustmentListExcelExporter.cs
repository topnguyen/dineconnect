﻿
using System.Collections.Generic;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using System.Linq;

namespace DinePlan.DineConnect.House.Transaction.Exporter
{
    public class AdjustmentListExcelExporter : FileExporterBase, IAdjustmentListExcelExporter
    {
        public FileDto ExportToFile(List<AdjustmentViewDto> dtos)
        {
            return CreateExcelPackage(
                "AdjustmentList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Adjustment"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("LocationName"),
                        L("AdjustmentDate"),
                        L("TokenNumber"),
                        L("Remarks")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.LocationRefName,
                        _ => _.AdjustmentDate.ToString("yyyy-MMM-dd"),
                        _ => _.TokenRefNumber,
                        _ => _.AdjustmentRemarks

                        );

                    for (var i = 1; i <= 10; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }

        public FileDto ExportAdjustmentDetailToFile(List<AdjustmentReportConsolidatedDto> dtos, InputAdjustmentReport input)
        {
            if (input.AdjustmentMode == "")
            {
                return CreateExcelPackage(
                    "AdjustmentDetailReport.xlsx",
                    excelPackage =>
                    {
                        var sheet = excelPackage.Workbook.Worksheets.Add(L("Adjustment"));
                        sheet.OutLineApplyStyle = true;

                        AddHeader(
                            sheet,
                            L("Material"),
                            L("Mode"),
                            L("ExcessQty"),
                            L("ShortageQty"),
                            L("Quantity"),
                            L("UOM"),
                            L("Price"),
                            L("ExcessAmount"),
                            L("ShortageAmount"),
                            L("Amount")
                            );

                        AddObjects(
                            sheet, 2, dtos,
                            _ => _.MaterialRefName,
                            _ => _.AdjustmentMode,
                            _ => _.ExcessQty,
                            _ => _.ShortageQty,
                            _ => _.TotalQty,
                            _ => _.Uom,
                            _ => _.Price,
                            _ => _.ExcessAmount,
                            _ => _.ShortageAmount,
                            _ => _.TotalAmount
                            );

                        decimal excessAmount = dtos.Sum(t => t.ExcessAmount);
                        decimal wastageAmount = dtos.Sum(t => t.ShortageAmount);
                        decimal totalAmount = dtos.Sum(t => t.TotalAmount);
                        int rowCount = 2 + dtos.Count + 1;

                        AddDetail(sheet, rowCount, 8, excessAmount);
                        AddDetail(sheet, rowCount, 9, wastageAmount);
                        AddDetail(sheet, rowCount, 10, totalAmount);

                        for (var i = 1; i <= 15; i++)
                        {
                            sheet.Column(i).AutoFit();
                        }
                    });
            }

            if (input.AdjustmentMode.Equals(L("Excess")))
            {
                return CreateExcelPackage(
                    "ExcessAdjustmentDetailReport.xlsx",
                    excelPackage =>
                    {
                        var sheet = excelPackage.Workbook.Worksheets.Add(L("ExcessAdjustment"));
                        sheet.OutLineApplyStyle = true;

                        AddHeader(
                            sheet,
                            L("Material"),
                            L("Mode"),
                            L("ExcessQty"),
                            L("UOM"),
                            L("Price"),
                            L("ExcessAmount")
                            );

                        AddObjects(
                            sheet, 2, dtos,
                            _ => _.MaterialRefName,
                            _ => _.AdjustmentMode,
                            _ => _.ExcessQty,
                            _ => _.Uom,
                            _ => _.Price,
                            _ => _.ExcessAmount
                            );

                        decimal excessAmount = dtos.Sum(t => t.ExcessAmount);
                        int rowCount = 2 + dtos.Count + 1;
                        AddDetail(sheet, rowCount, 6, excessAmount);

                        for (var i = 1; i <= 15; i++)
                        {
                            sheet.Column(i).AutoFit();
                        }
                    });
            }

            if (!input.AdjustmentMode.Equals(L("Excess")))
            {
                return CreateExcelPackage(
                    "WastageAdjustmentDetailReport.xlsx",
                    excelPackage =>
                    {
                        var sheet = excelPackage.Workbook.Worksheets.Add(L("ExcessAdjustment"));
                        sheet.OutLineApplyStyle = true;

                        AddHeader(
                            sheet,
                            L("Material"),
                            L("Mode"),
                            L("WastageQty"),
                            L("UOM"),
                            L("Price"),
                            L("ShortageAmount")
                            );

                        AddObjects(
                            sheet, 2, dtos,
                            _ => _.MaterialRefName,
                            _ => _.AdjustmentMode,
                            _ => _.ShortageQty,
                            _ => _.Uom,
                            _ => _.Price,
                            _ => _.ShortageAmount
                            );

                        decimal shortageAmount = dtos.Sum(t => t.ShortageAmount);
                        int rowCount = 2 + dtos.Count + 1;
                        AddDetail(sheet, rowCount, 6, shortageAmount);

                        for (var i = 1; i <= 15; i++)
                        {
                            sheet.Column(i).AutoFit();
                        }
                    });
            }
            return new FileDto();

        }

        public FileDto ExportManualStockDetailToFile(GetAdjustmentForEditOutput input)
        {
            if (input.Adjustment.TokenRefNumber == 888)
            {
                return CreateExcelPackage(
                    "ManualStock" + " " + input.Adjustment.LocationRefName + " " + input.Adjustment.AdjustmentDate.ToString("dd-MMM-yy") + ".xlsx",
                    excelPackage =>
                    {
                        var sheet = excelPackage.Workbook.Worksheets.Add(L("ManualStock"));
                        sheet.OutLineApplyStyle = true;
                        int rowCount = 1;

                        AddHeader(sheet, rowCount, 1, L("Location"));
                        AddHeader(sheet, rowCount, 2, input.Adjustment.LocationRefName);
                        rowCount++;

                        int sno = 1;

                        AddHeader(sheet, rowCount, 1, L("Date"));
                        AddHeader(sheet, rowCount, 2, input.Adjustment.AdjustmentDate.ToString("dd-MMM-yy"));
                        rowCount++;

                        AddHeader(sheet, rowCount, 1, L("Id"));
                        AddHeader(sheet, rowCount, 2, input.Adjustment.Id.Value);
                        rowCount++;
                        rowCount++;

                        AddHeader(
                            sheet, rowCount,
                            L("Sno"),
                            L("Category"),
                            L("Material"),
							@L("UOM"),
							@L("Price"),
							@L("SystemStock"),
							@L("SystemStockValue"),
							@L("ManualStock"),
							@L("ManualStockValue"),
							@L("VarianceQty"),
                            @L("AdjustmentMode"),
                            @L("LineTotal"),
                            @L("Remarks")
                            );
                        rowCount++;

                        AddObjects(
                            sheet, rowCount, input.AdjustmentDetail,
                            _ => _.Sno,
                            _ => _.MaterialGroupCategoryName,
                            _ => _.MaterialRefName,
							_ => _.UnitRefName,
							_ => _.Price,
							_ => _.ClosingStock,
							_ => _.ClosingStockValue,
							_ => _.StockEntry,
							_ => _.StockEntryValue,
							_ => _.AdjustmentQty,
                            _ => _.AdjustmentMode,
                            _ => _.AdjustmentCost,
                            _ => _.AdjustmentApprovedRemarks
                            );

                        rowCount = rowCount + input.AdjustmentDetail.Count + 1;
						AddHeader(sheet, rowCount, 6, input.Adjustment.TotalClosingStockValue);
						AddHeader(sheet, rowCount, 8, input.Adjustment.TotalStockEntryValue);
						AddHeader(sheet, rowCount, 11, input.Adjustment.TotalAdjustmentCost);

                        rowCount++;
                        rowCount++;

                        AddHeader(sheet, rowCount, 2, L("Excess"));
                        AddHeader(sheet, rowCount, 3, input.Adjustment.TotalExcessCost);

                        rowCount++;
                        AddHeader(sheet, rowCount, 2, L("Shortage"));
                        AddHeader(sheet, rowCount, 3, input.Adjustment.TotalShortageCost);

                        rowCount++;
                        AddHeader(sheet, rowCount, 2, L("Total"));
                        AddHeader(sheet, rowCount, 3, input.Adjustment.TotalAdjustmentCost);

                        for (var i = 1; i <= 13; i++)
                        {
                            sheet.Column(i).AutoFit();
                        }
                    });
            }
            else
            {
                return CreateExcelPackage(
              "Adjustment" + " " + input.Adjustment.LocationRefName + " " + input.Adjustment.Id + " " +  input.Adjustment.AdjustmentDate.ToString("dd-MMM-yy") + ".xlsx",
              excelPackage =>
              {
                  var sheet = excelPackage.Workbook.Worksheets.Add(L("DayCloseAdjustment"));
                  sheet.OutLineApplyStyle = true;
                  int rowCount = 1;

                  AddHeader(sheet, rowCount, 1, L("Location"));
                  AddHeader(sheet, rowCount, 2, input.Adjustment.LocationRefName);
                  rowCount++;

                  AddHeader(sheet, rowCount, 1, L("Date"));
                  AddHeader(sheet, rowCount, 2, input.Adjustment.AdjustmentDate.ToString("dd-MMM-yy"));
                  rowCount++;

                  AddHeader(sheet, rowCount, 1, L("Id"));
                  AddHeader(sheet, rowCount, 2, input.Adjustment.Id.Value);
                  rowCount++;
                  rowCount++;

                  AddHeader(
                      sheet, rowCount,
                      L("Sno"),
                      L("Category"),
                      L("Material"),
                      @L("Qty"),
                      @L("UOM"),
                      @L("AdjustmentMode"),
                      @L("Price"),
                      @L("LineTotal"),
                      @L("Remarks")
                      );
                  rowCount++;

                  AddObjects(
                      sheet, rowCount, input.AdjustmentDetail,
                      _ => _.Sno,
                      _ => _.MaterialGroupCategoryName,
                      _ => _.MaterialRefName,
                      _ => _.AdjustmentQty,
                      _ => _.UnitRefName,
                      _ => _.AdjustmentMode,
                      _ => _.Price,
                      _ => _.AdjustmentCost,
                      _ => _.AdjustmentApprovedRemarks
                      );

                  rowCount = rowCount + input.AdjustmentDetail.Count + 1;

                  AddHeader(sheet, rowCount, 7, input.Adjustment.TotalAdjustmentCost);

                  rowCount++;
                  rowCount++;

                  AddHeader(sheet, rowCount, 2, L("Excess"));
                  AddHeader(sheet, rowCount, 3, input.Adjustment.TotalExcessCost);

                  rowCount++;
                  AddHeader(sheet, rowCount, 2, L("Shortage"));
                  AddHeader(sheet, rowCount, 3, input.Adjustment.TotalShortageCost);

                  rowCount++;
                  AddHeader(sheet, rowCount, 2, L("Total"));
                  AddHeader(sheet, rowCount, 3, input.Adjustment.TotalAdjustmentCost);
                  for (var i = 1; i <= 8; i++)
                  {
                      sheet.Column(i).AutoFit();
                  }
              });
            }
        }
    }
}
