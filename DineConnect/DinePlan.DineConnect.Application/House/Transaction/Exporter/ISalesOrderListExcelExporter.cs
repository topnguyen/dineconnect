﻿using System.Collections.Generic;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;


namespace DinePlan.DineConnect.House.Transaction
{
    public interface ISalesOrderListExcelExporter
    {
        FileDto ExportToFile(List<SalesOrderListWithRefNameDto> dtos);
    }
}
