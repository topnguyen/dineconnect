﻿
using System.Collections.Generic;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;


namespace DinePlan.DineConnect.House.Transaction
{
    public interface IInvoiceListExcelExporter
    {
        FileDto ExportToFile(List<InvoiceListDto> dtos);

        FileDto ExportInvoiceDetailsToFile(List<GetInvoiceForEditOutput> dtos);
		FileDto ExportCategoryPurchaseToFile(List<InvoiceCategoryAnalysisDto> dtos);
        FileDto ExportPurchaseTaxDetails(PurchaseInvoiceTaxDetailOutputDto input);
        FileDto ExportPurchaseCategoryWithPoToFile(InvoiceWithPoAndCategoryOutputDto input);
        FileDto ExportSupplierWisePurchaseGroupCategoryMaterials(SupplierWisePurchaseGroupCateogryMaterialDto input);

    }
}