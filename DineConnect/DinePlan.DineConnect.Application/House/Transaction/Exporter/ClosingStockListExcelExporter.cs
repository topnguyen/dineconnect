﻿using System.Collections.Generic;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Transaction.Exporter
{
    public class ClosingStockListExcelExporter : FileExporterBase, IClosingStockListExcelExporter
    {
        public FileDto ExportToFile(List<ClosingStockListDto> dtos)
        {
            return CreateExcelPackage(
                "ClosingStockList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("ClosingStock"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Location"),
                        L("StockDate"),
                        L("ApprovedUser")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.LocationRefName,
                        _ => _.StockDate.ToString("yyyy-MMM-dd"),
                        _ => _.ApprovedUserId
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}