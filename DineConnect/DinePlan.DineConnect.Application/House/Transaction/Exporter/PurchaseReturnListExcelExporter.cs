﻿using System.Collections.Generic;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Transaction.Exporter
{
    public class PurchaseReturnListExcelExporter : FileExporterBase, IPurchaseReturnListExcelExporter
    {
        public FileDto ExportToFile(List<PurchaseReturnListDto> dtos)
        {
            return CreateExcelPackage(
                "PurchaseReturnList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("PurchaseReturn"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("ReturnDate"),
                        L("ReferenceNumber"),
                        L("SupplierName"),
                        L("PurchaseReturnAmount")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.ReferenceNumber,
                        _ => _.ReturnDate.ToString("yyyy-MMM-dd"),
                        _ => _.SupplierRefName,
                        _ => _.PurchaseReturnAmount
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }


        public FileDto PurchaseReportExportToFile(PurchaseReturnDetailOutputDto input)
        {
            input.DateRange = "From: " + input.StartDate.ToString("dd-MMM-yyyy") + " To: " + input.EndDate.ToString("dd-MMM-yyyy");
            string fileName = L("PurchaseReturn") + " " + input.DateRange + " " + input.LocationInfo.Code + " " + input.LocationInfo.Name;

            return CreateExcelPackage(fileName + ".xlsx",
            excelPackage =>
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("PurchaseReturn"));
                sheet.OutLineApplyStyle = true;
                int row = 1;
                AddHeaderWithMergeWithColumnCount(sheet, row, 1, 11, L("PurchaseReturn"));
                sheet.Row(row).Height = 30;
                sheet.Cells[row, 1].Style.Font.Size = 15;
                row++;
                AddHeaderWithMergeWithColumnCount(sheet, row, 3, 2, L("Location") + " " + L("Code") + ":" + input.LocationInfo.Code);
                AddHeaderWithMergeWithColumnCount(sheet, row, 6, 2, input.LocationInfo.Name);
                row++;
                AddHeaderWithMergeWithColumnCount(sheet, row, 1, 11, input.DateRange);
                row++;
                AddHeaderWithMergeWithColumnCount(sheet, row, 1, 1, L("PrintedOn"));
                AddHeaderWithMergeWithColumnCount(sheet, row, 3, 1, input.PrintedTime.ToString("dd-MMM-yyyy"));
                row++;
                AddHeaderWithMergeWithColumnCount(sheet, row, 1, 1, L("PrintedBy"));
                AddHeaderWithMergeWithColumnCount(sheet, row, 3, 1, input.PreparedUserName);
                row++;

                if (input.DoesCustomReportFormat)
                {
                    AddHeader(sheet, row, L("#"), L("MaterialCode"), L("MaterialName"), L("Ref#"), L("ReturnDate"), L("Time"), L("Supplier"), L("Return") + " " + @L("Amount"), L("Purchase") + " " + L("Qty"), L("Inventory") + " " + L("Qty"), L("Recipe") + " " + L("Qty"));
                }
                else
                {
                    AddHeader(sheet, row, L("#"), L("MaterialCode"), L("MaterialName"), L("Ref#"), L("ReturnDate"), L("Time"), L("Supplier"), L("Return") + " " + @L("Amount"), L("Return") + " " + L("Quantity"), L("UOM"), L("Cost"), L("TotalAmount"), L("Tax"), L("NetAmount"));
                }
                row++;
                int sno = 1;

                foreach (var det in input.ReturnList)
                {
                    int col = 1;
                    AddDetail(sheet, row, col++, sno++);
                    AddDetail(sheet, row, col++, det.MaterialPetName);
                    AddDetail(sheet, row, col++, det.MaterialRefName);
                    AddDetail(sheet, row, col++, det.ReferenceNumber);
                    AddDetail(sheet, row, col++, det.ReturnDate.ToString("dd-MMM-yyyy"));
                    AddDetail(sheet, row, col++, det.ReturnDate.ToString("HH:mm:ss"));
                    AddDetail(sheet, row, col++, det.SupplierRefName);
                    if (input.DoesCustomReportFormat)
                    {
                        AddDetailWithNumberFormat(sheet, row, col++, det.NetAmount,"0.0000");
                        AddDetailWithNumberFormat(sheet, row, col++, det.QuantityInPurchaseUnit, "0.0000");
                        AddDetailWithNumberFormat(sheet, row, col++, det.QuantityInStkAdjustedUnit, "0.0000");
                        AddDetailWithNumberFormat(sheet, row, col++, det.QuantityInIssueUnit, "0.0000");
                    }
                    else
                    {
                        AddDetailWithNumberFormat(sheet, row, col++, det.Quantity, "0.0000");
                        AddDetail(sheet, row, col++, det.UnitRefName);
                        AddDetailWithNumberFormat(sheet, row, col++, det.Price, "0.0000");
                        AddDetailWithNumberFormat(sheet, row, col++, det.TotalAmount, "0.0000");
                        AddDetailWithNumberFormat(sheet, row, col++, det.TaxAmount, "0.0000");
                        AddDetailWithNumberFormat(sheet, row, col++, det.NetAmount, "0.0000");
                    }
                    row++;
                }
                row++;
                if (input.DoesCustomReportFormat)
                {
                    AddDetail(sheet, row, 7, L("Total"));
                    AddDetailWithNumberFormat(sheet, row, 8, input.NetPurchaseReturnAmount, "0.0000");
                }
                else
                {
                    AddDetail(sheet, row, 7, L("Total"));
                    AddDetailWithNumberFormat(sheet, row, 11, input.TotalPurchaseReturnAmount, "0.0000");
                    AddDetailWithNumberFormat(sheet, row, 12, input.TotalTaxAmount, "0.0000");
                    AddDetailWithNumberFormat(sheet, row, 13, input.NetPurchaseReturnAmount, "0.0000");
                }
                for (var i = 1; i <= 25; i++)
                {
                    sheet.Column(i).AutoFit();
                }

                
            });
        }
    }
}