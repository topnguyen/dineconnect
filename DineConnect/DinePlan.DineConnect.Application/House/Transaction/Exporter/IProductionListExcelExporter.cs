﻿
using System.Collections.Generic;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;


namespace DinePlan.DineConnect.House.Transaction
{
    public interface IProductionListExcelExporter
    {
        FileDto ExportToFile(List<ProductionListDto> dtos);
    }
}
