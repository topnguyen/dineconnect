﻿
using System.Collections.Generic;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Transaction.Exporter
{
    public class SalesDeliveryOrderListExcelExporter : FileExporterBase, ISalesDeliveryOrderListExcelExporter
    {
        public FileDto ExportToFile(List<SalesDeliveryOrderWithCustomerName> dtos)
        {
            return CreateExcelPackage(
                "SalesDeliveryOrderList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("SalesDeliveryOrder"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Date"),
                        L("CustomerName"),
                        L("DcNumberGivenBySupplier"),
                        L("BillConversion")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.DeliveryDate.ToString("yyyy-MMM-dd"),
                        _ => _.CustomerName,
                        _ => _.DcNumberGivenByCustomer,
                        _ => _.IsDcCompleted
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
