﻿
using System.Collections.Generic;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Transaction.Exporter
{
    public class ReturnListExcelExporter : FileExporterBase, IReturnListExcelExporter
    {
        public FileDto ExportToFile(List<ReturnListDto> dtos)
        {
            return CreateExcelPackage(
                "ReturnList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Return"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Reference"),
                        L("ReturnDate")

                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.TokenRefNumber,
                        _ => _.ReturnDate.Value.ToString("yyyy-MMM-dd")
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
