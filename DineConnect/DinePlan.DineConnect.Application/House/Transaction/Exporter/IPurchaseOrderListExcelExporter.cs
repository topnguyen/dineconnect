﻿using System.Collections.Generic;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Transaction.Dtos;

namespace DinePlan.DineConnect.House.Transaction.Exporter
{
    public interface IPurchaseOrderListExcelExporter
    {
        FileDto ExportToFile(List<PurchaseOrderListWithRefNameDto> dtos);
    }
}
