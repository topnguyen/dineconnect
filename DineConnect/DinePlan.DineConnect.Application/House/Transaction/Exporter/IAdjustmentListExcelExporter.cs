﻿
using System.Collections.Generic;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;


namespace DinePlan.DineConnect.House.Transaction
{
    public interface IAdjustmentListExcelExporter
    {
        FileDto ExportToFile(List<AdjustmentViewDto> dtos);

        FileDto ExportAdjustmentDetailToFile(List<AdjustmentReportConsolidatedDto> dtos, InputAdjustmentReport input);
        FileDto ExportManualStockDetailToFile(GetAdjustmentForEditOutput input);

    }
}