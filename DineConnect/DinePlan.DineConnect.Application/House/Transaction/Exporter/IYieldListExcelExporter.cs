﻿
using System.Collections.Generic;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;


namespace DinePlan.DineConnect.House.Transaction
{
    public interface IYieldListExcelExporter
    {
        FileDto ExportToFile(List<YieldListDto> dtos);
    }
}
