﻿

using System.Collections.Generic;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.House.Transaction.Exporter
{
    public class InvoiceListExcelExporter : FileExporterBase, IInvoiceListExcelExporter
    {
        public FileDto ExportToFile(List<InvoiceListDto> dtos)
        {
            return CreateExcelPackage(
                "InvoiceList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Invoice"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("SupplierName"),
                        L("InvoiceNumber"),
                        L("InvoiceDate"),
                        L("AmountWOTax"),
                        L("Tax"),
                        L("InvoiceAmount"),
                        L("AccountDate")

                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.SupplierRefName,
                        _ => _.InvoiceNumber,
                        _ => _.InvoiceDate.ToString("yyyy-MMM-dd"),
                        _ => _.AmountWithoutTax,
                        _ => _.TaxAmount,
                        _ => _.InvoiceAmount,
                        _ => _.AccountDate.Value.ToString("yyyy-MMM-dd")

                        );

                    decimal TotalAmountWithoutTax = dtos.Sum(t => t.AmountWithoutTax);
                    decimal TotalTax = dtos.Sum(t => t.TaxAmount);
                    decimal TotalInvoice = dtos.Sum(t => t.InvoiceAmount);
                    int rowCount = 2 + dtos.Count + 1;
                    AddDetail(sheet, rowCount, 5, TotalAmountWithoutTax, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                    AddDetail(sheet, rowCount, 6, TotalTax, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                    AddDetail(sheet, rowCount, 7, TotalInvoice, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);

                    for (var i = 1; i <= 11; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }

        public FileDto ExportCategoryPurchaseToFile(List<InvoiceCategoryAnalysisDto> dtos)
        {
            return CreateExcelPackage(
                "InvoiceReportsCategoryWise.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("InvoiceReportsCategoryWise"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Category"),
                        L("Total"),
                        L("Tax"),
                        L("NetAmount")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.CategoryRefName,
                        _ => _.TotalAmount,
                        _ => _.TaxAmount,
                        _ => _.NetAmount
                        );

                    decimal TotalAmountWithoutTax = dtos.Sum(t => t.TotalAmount);
                    decimal TotalTax = dtos.Sum(t => t.TaxAmount);
                    decimal TotalInvoice = dtos.Sum(t => t.NetAmount);
                    int rowCount = 2 + dtos.Count + 1;
                    AddDetail(sheet, rowCount, 2, TotalAmountWithoutTax, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                    AddDetail(sheet, rowCount, 3, TotalTax, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                    AddDetail(sheet, rowCount, 4, TotalInvoice, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);

                    for (var i = 1; i <= 5; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }

        public FileDto ExportPurchaseCategoryWithPoToFile(InvoiceWithPoAndCategoryOutputDto input)
        {
            return CreateExcelPackage(
                "Invoice Export With Category and PO Details.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("CategoryWise"));
                    sheet.OutLineApplyStyle = true;
                    var dtos = input.InvoiceCategoryWisePurchaseOrderAnalysisDtoList;

                    AddHeader(
                        sheet,
                        L("Supplier"),
                        L("PoReference"),
                        L("Location"),
                        L("Group"),
                        L("Category"),
                        L("InvoiceNumber"),
                        L("InvoiceDate"),
                        L("WithOutTax"),
                        L("Tax"),
                        L("InvoiceAmount"),
                        L("AccountDate")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.SupplierRefName,
                        _ => _.PoReferenceNumber,
                        _ => _.LocationRefCode,
                        _ => _.MaterialGroupRefName,
                        _ => _.MaterialGroupCategoryRefName,
                        _ => _.InvoiceNumber,
                        _ => _.InvoiceDate.ToString("dd-MMM-yyyy"),
                        _ => _.TotalAmount,
                        _ => _.TaxAmount,
                        _ => _.NetAmount,
                        _ => _.AccountDate.ToString("dd-MMM-yyyy")
                        );

                    //decimal TotalAmountWithoutTax = dtos.Sum(t => t.TotalAmount);
                    //decimal TotalTax = dtos.Sum(t => t.TaxAmount);
                    //decimal TotalInvoice = dtos.Sum(t => t.NetAmount);
                    //int rowCount = 2 + dtos.Count + 1;
                    //AddDetail(sheet, rowCount, 2, TotalAmountWithoutTax, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                    //AddDetail(sheet, rowCount, 3, TotalTax, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                    //AddDetail(sheet, rowCount, 4, TotalInvoice, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);

                    for (var i = 1; i <= 15; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }

        public FileDto ExportInvoiceDetailsToFile(List<GetInvoiceForEditOutput> dtos)
        {
            var masterDtos = dtos;

            return CreateExcelPackage(
                @L("InvoiceDetail") + ".xlsx",
                excelPackage =>
                {
                    var consSheet = excelPackage.Workbook.Worksheets.Add(L("Consolidated"));
                    consSheet.OutLineApplyStyle = true;
                    int rowCount = 1;
                    foreach (var master in masterDtos)
                    {

                        AddHeader(consSheet, rowCount, 2, L("Location"));
                        AddHeaderWithMergeWithColumnCount(consSheet, rowCount, 3, 3, master.Invoice.LocationRefName);
                        rowCount++;

                        AddHeader(consSheet, rowCount, 2, L("Supplier"));
                        AddHeaderWithMergeWithColumnCount(consSheet, rowCount, 3, 3, master.Invoice.SupplierRefName);

                        rowCount++;

                        AddHeader(consSheet, rowCount, 2, L("InvoiceDate"));
                        AddHeaderWithMergeWithColumnCount(consSheet, rowCount, 3, 3, master.Invoice.InvoiceDate.ToString("dd-MMM-yyyy"));

                        rowCount++;
                        AddHeader(consSheet, rowCount, 2, L("Inv#"));
                        AddHeaderWithMergeWithColumnCount(consSheet, rowCount, 3, 3, master.Invoice.InvoiceNumber);

                        rowCount++;
                        AddHeader(consSheet, rowCount, 2, L("InvoiceAmount"));
                        AddHeaderWithMergeWithColumnCount(consSheet, rowCount, 3, 3, master.Invoice.InvoiceAmount);
                        rowCount++;
                        rowCount++;


                        AddHeader(
                            consSheet, rowCount,
                            @L("#"),
                            @L("MaterialName"),
                            @L("Quantity"),
                            @L("UOM"),
                            @L("Cost"),
                            @L("TotalAmount"),
                            @L("TaxAmount"),
                            @L("LineTotal"),
                            @L("AccountCode")
                            );
                        rowCount++;
                        AddObjects(
                            consSheet, rowCount, master.InvoiceDetail,
                            r => r.Sno,
                            r => r.MaterialRefName,
                            r => r.TotalQty,
                            r => r.Uom,
                            r => r.Price,
                            r => r.TotalAmount,
                            r => r.TaxAmount,
                            r => r.NetAmount,
                            r => r.Hsncode
                            );

                        rowCount = rowCount + master.InvoiceDetail.Count + 1;

                        rowCount++;
                        AddHeader(consSheet, rowCount, 2, L("SubTotal"));
                        AddHeader(consSheet, rowCount, 6, master.Invoice.SubTotalAmount);
                        AddHeader(consSheet, rowCount, 7, master.Invoice.TaxAmount);
                        AddHeader(consSheet, rowCount, 8, master.Invoice.TaxAmount + master.Invoice.SubTotalAmount);
                        rowCount++;

                        if (master.Invoice.TotalDiscountAmount > 0)
                        {
                            AddHeader(consSheet, rowCount, 2, L("Discount"));
                            AddHeader(consSheet, rowCount, 8, master.Invoice.TotalDiscountAmount);
                            rowCount++;
                        }

                        if (master.Invoice.TotalShipmentCharges > 0)
                        {
                            AddHeader(consSheet, rowCount, 2, L("Extra"));
                            AddHeader(consSheet, rowCount, 8, master.Invoice.TotalShipmentCharges);
                            rowCount++;
                        }

                        AddHeader(consSheet, rowCount, 2, L("InvoiceAmount"));
                        AddHeader(consSheet, rowCount, 8, master.Invoice.InvoiceAmount);

                        rowCount++;
                        rowCount++;
                        rowCount++;

                        //AddDetail(consSheet, rowCount, 5, standardDtos.Sum(t => t.NetAmount), OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                    }
                    for (var i = 1; i <= 12; i++)
                    {
                        consSheet.Column(i).AutoFit();
                    }
                });
        }

        public FileDto ExportPurchaseTaxDetails(PurchaseInvoiceTaxDetailOutputDto input)
        {
            input.DateRange = "From: " + input.StartDate.ToString("dd-MMM-yyyy") + " To: " + input.EndDate.ToString("dd-MMM-yyyy");
            string fileName = L("Purchase") + " " + L("Tax") + " " + input.DateRange + " " + input.LocationInfo.Code + " " + input.LocationInfo.Name;

            return CreateExcelPackage(fileName + ".xlsx",
            excelPackage =>
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Purchase") + " " + L("Tax"));
                sheet.OutLineApplyStyle = true;
                int row = 1;
                AddHeaderWithMergeWithColumnCount(sheet, row, 1, 11, L("Purchase") + " " + L("Tax") + " " + L("Detail"));
                sheet.Row(row).Height = 30;
                sheet.Cells[row, 1].Style.Font.Size = 15;
                row++;
                AddHeaderWithMergeWithColumnCount(sheet, row, 1, 11, L("Location") + " " + L("Code") + ":" + input.LocationInfo.Code +  " " + input.LocationInfo.Name);
                //AddHeaderWithMergeWithColumnCountAndAlignment(sheet, row, 3, 2, L("Location") + " " + L("Code") + ":" + input.LocationInfo.Code,OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                //AddHeaderWithMergeWithColumnCountAndAlignment(sheet, row, 6, 2, input.LocationInfo.Name,OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                row++;
                AddHeaderWithMergeWithColumnCount(sheet, row, 1, 11, input.DateRange);
                row++;
                AddHeaderWithMergeWithColumnCount(sheet, row, 1, 1, L("PrintedOn"));
                AddHeaderWithMergeWithColumnCountAndAlignment(sheet, row, 3, 1, input.PrintedTime.ToString("dd-MMM-yyyy"),OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                row++;
                AddHeaderWithMergeWithColumnCount(sheet, row, 1, 1, L("PrintedBy"));
                AddHeaderWithMergeWithColumnCountAndAlignment(sheet, row, 3, 1, input.PreparedUserName,OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                row++;

                if (input.CustomReportFlag)
                {
                    AddHeader(sheet, row, L("Id"), L("Date"), L("Supplier"), L("Inv#"), L("PO") + " " + L("Ref") + " " + L("Manual"), L("Invoice") + " " + L("Ref") + "" + L("Manual"), L("MaterialCode"), L("MaterialName"),  L("Purchase") + " " + L("Qty"), L("UOM") , L("Purchase") + " " + L("Amount"), L("Tax"));
                }
                else
                {
                    AddHeader(sheet, row, L("Id"), L("Date"), L("Supplier"), L("Inv#"), L("PO") + " " + L("Ref") + " " + L("Manual"), L("Invoice") + " " + L("Ref") + "" + L("Manual"), L("MaterialCode"), L("MaterialName"), L("Purchase") + " " + L("Qty"), L("UOM"), L("Purchase") + " " + L("Amount"), L("Tax"));
                }
                row++;
                foreach (var det in input.InvoiceDetailViewList)
                {
                    int col = 1;
                    AddDetail(sheet, row, col++, det.InvoiceRefId);
                    AddDetail(sheet, row, col++, det.InvoiceDate.ToString("dd-MMM-yyyy"));
                    AddDetail(sheet, row, col++, det.SupplierRefName);
                    AddDetail(sheet, row, col++, det.InvoiceNumber);
                    AddDetail(sheet, row, col++, det.PurchaseOrderReferenceFromOtherErp);
                    AddDetail(sheet, row, col++, det.InvoiceNumberReferenceFromOtherErp);
                    AddDetail(sheet, row, col++, det.MaterialPetName);
                    AddDetail(sheet, row, col++, det.MaterialRefName);
                    if (input.CustomReportFlag)
                    {
                        AddDetailWithNumberFormat(sheet, row, col++, det.QuantityInPurchaseUnit, "0.0000");
                        AddDetail(sheet, row, col++, det.DefaultUnitName);
                    }
                    else
                    {
                        AddDetailWithNumberFormat(sheet, row, col++, det.TotalQty, "0.0000");
                        AddDetail(sheet, row, col++, det.UnitRefName);
                    }
                    AddDetailWithNumberFormat(sheet, row, col++, det.TotalAmount, "0.0000");
                    AddDetailWithNumberFormat(sheet, row, col++, det.TaxAmount, "0.0000");
                    row++;
                }
                row++;
                if (input.CustomReportFlag)
                {
                    AddDetail(sheet, row, 3, L("Total"));
                    sheet.Cells[row, 3].Style.Font.Bold = true;
                    AddDetailWithNumberFormat(sheet, row, 11, input.TotalPurchaseAmount, "0.0000");
                    sheet.Cells[row, 11].Style.Font.Bold = true;
                    AddDetailWithNumberFormat(sheet, row, 12, input.TotalTaxAmount, "0.0000");
                    sheet.Cells[row, 12].Style.Font.Bold = true;
                }
                else
                {
                    AddDetail(sheet, row, 3, L("Total"));
                    AddDetailWithNumberFormat(sheet, row, 11, input.TotalPurchaseAmount, "0.0000");
                    AddDetailWithNumberFormat(sheet, row, 12, input.TotalTaxAmount, "0.0000");
                    sheet.Cells[row, 3].Style.Font.Bold = true;
                    sheet.Cells[row, 11].Style.Font.Bold = true;
                    sheet.Cells[row, 12].Style.Font.Bold = true;
                }
                for (var i = 1; i <= 25; i++)
                {
                    sheet.Column(i).AutoFit();
                }
            });
        }

        public FileDto ExportSupplierWisePurchaseGroupCategoryMaterials(SupplierWisePurchaseGroupCateogryMaterialDto input)
        {
            var masterDtos = input.SupplierWiseHeadWisePurchaseList;

            return CreateExcelPackage(
                @L("Purchase") + " " + L("Supplier") +  " " + L("Wise")+ " " + input.InputFilterDto.StartDate.ToString("dd-MMM-yyyy") + " - " + input.InputFilterDto.EndDate.ToString("dd-MMM-yyyy") + ".xlsx",
                excelPackage =>
                {
                    decimal totalAmount = 0;
                    {
                        #region Group Sheet
                        var consSheet = excelPackage.Workbook.Worksheets.Add(L("Group"));
                        consSheet.OutLineApplyStyle = true;
                        int rowIndex = 1;
                        int colIndex = 1;
                        AddHeader(consSheet, rowIndex, colIndex++, L("Supplier").ToUpper());
                        foreach (var lst in input.MaterialGroupWisePurchaseDtos)
                        {
                            AddHeader(consSheet, rowIndex, colIndex++, lst.MaterialGroupRefName);
                        }
                        AddHeader(consSheet, rowIndex, colIndex++, L("Total").ToUpper());
                        rowIndex++;

                        foreach (var master in masterDtos)
                        {
                            colIndex = 1;
                            consSheet.Row(rowIndex).Height = 21;
                            AddHeader(consSheet, rowIndex, colIndex++, master.SupplierName);
                            foreach (var lst in input.MaterialGroupWisePurchaseDtos)
                            {
                                var existRecord = master.MaterialGroupWisePurchaseDtos.FirstOrDefault(t => t.MaterialGroupRefId == lst.MaterialGroupRefId);
                                if (existRecord != null)
                                {
                                    AddDetailWithNumberFormat(consSheet, rowIndex, colIndex++, existRecord.TotalAmount, "0.00");
                                }
                                else
                                {
                                    colIndex++;
                                }
                            }
                            totalAmount = master.MaterialGroupWisePurchaseDtos.Sum(t => t.TotalAmount);
                            AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, totalAmount, "0.00");
                            rowIndex++;
                        }
                        colIndex = 1;
                        AddDetail(consSheet, rowIndex, colIndex++, L("Total"), OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                        foreach (var lst in input.MaterialGroupWisePurchaseDtos)
                        {
                            AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, lst.TotalAmount, "0.00");
                        }
                        totalAmount = input.MaterialGroupWisePurchaseDtos.Sum(t => t.TotalAmount);
                        AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, totalAmount, "0.00");
                        rowIndex++;
                        for (var i = 1; i <= input.MaterialGroupWisePurchaseDtos.Count + 2; i++)
                        {
                            consSheet.Column(i).AutoFit();
                        }
                        #endregion
                    }

                    {
                        #region Category Sheet
                        var consSheet = excelPackage.Workbook.Worksheets.Add(L("Category"));
                        consSheet.OutLineApplyStyle = true;
                        int rowIndex = 1;
                        int colIndex = 1;
                        AddHeader(consSheet, rowIndex, colIndex++, L("Supplier").ToUpper());
                        foreach (var lst in input.MaterialGroupCategoryWisePurchaseDtos)
                        {
                            AddHeader(consSheet, rowIndex, colIndex++, lst.MaterialGroupCategoryRefName);
                        }
                        AddHeader(consSheet, rowIndex, colIndex++, L("Total").ToUpper());
                        rowIndex++;

                        foreach (var master in masterDtos)
                        {
                            colIndex = 1;
                            consSheet.Row(rowIndex).Height = 21;
                            AddHeader(consSheet, rowIndex, colIndex++, master.SupplierName);
                            foreach (var lst in input.MaterialGroupCategoryWisePurchaseDtos)
                            {
                                var existRecord = master.MaterialGroupCategoryWisePurchaseDtos.FirstOrDefault(t => t.MaterialGroupCategoryRefId == lst.MaterialGroupCategoryRefId);
                                if (existRecord != null)
                                {
                                    AddDetailWithNumberFormat(consSheet, rowIndex, colIndex++, existRecord.TotalAmount, "0.00");
                                }
                                else
                                {
                                    colIndex++;
                                }
                            }
                            totalAmount = master.MaterialGroupCategoryWisePurchaseDtos.Sum(t => t.TotalAmount);
                            AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, totalAmount, "0.00");
                            rowIndex++;
                        }
                        colIndex = 1;
                        AddDetail(consSheet, rowIndex, colIndex++, L("Total"), OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                        foreach (var lst in input.MaterialGroupCategoryWisePurchaseDtos)
                        {
                            AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, lst.TotalAmount, "0.00");
                        }
                        totalAmount = input.MaterialGroupCategoryWisePurchaseDtos.Sum(t => t.TotalAmount);
                        AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, totalAmount, "0.00");
                        rowIndex++;
                        for (var i = 1; i <= input.MaterialGroupCategoryWisePurchaseDtos.Count + 2; i++)
                        {
                            consSheet.Column(i).AutoFit();
                        }
                        #endregion
                    }

                    if (input.InputFilterDto.ExportMaterialAlso==true)
                    {
                        #region Material Sheet
                        var consSheet = excelPackage.Workbook.Worksheets.Add(L("Material"));
                        consSheet.OutLineApplyStyle = true;
                        int rowIndex = 1;
                        int colIndex = 1;
                        AddHeader(consSheet, rowIndex, colIndex++, L("Supplier").ToUpper());
                        foreach (var lst in input.MaterialWisePurchaseDtos)
                        {
                            AddHeader(consSheet, rowIndex, colIndex++, lst.MaterialRefName);
                        }
                        AddHeader(consSheet, rowIndex, colIndex++, L("Total").ToUpper());
                        rowIndex++;

                        foreach (var master in masterDtos)
                        {
                            colIndex = 1;
                            consSheet.Row(rowIndex).Height = 21;
                            AddHeader(consSheet, rowIndex, colIndex++, master.SupplierName);
                            foreach (var lst in input.MaterialWisePurchaseDtos)
                            {
                                var existRecord = master.MaterialWisePurchaseDtos.FirstOrDefault(t => t.MaterialRefId == lst.MaterialRefId);
                                if (existRecord != null)
                                {
                                    AddDetailWithNumberFormat(consSheet, rowIndex, colIndex++, existRecord.TotalAmount, "0.00");
                                }
                                else
                                {
                                    colIndex++;
                                }
                            }
                            totalAmount = master.MaterialWisePurchaseDtos.Sum(t => t.TotalAmount);
                            AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, totalAmount, "0.00");
                            rowIndex++;
                        }
                        colIndex = 1;
                        AddDetail(consSheet, rowIndex, colIndex++, L("Total"),OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                        foreach (var lst in input.MaterialWisePurchaseDtos)
                        {
                            AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, lst.TotalAmount, "0.00");
                        }
                        totalAmount = input.MaterialWisePurchaseDtos.Sum(t => t.TotalAmount);
                        AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, totalAmount, "0.00");
                        rowIndex++;
                        for (var i = 1; i <= input.MaterialWisePurchaseDtos.Count + 2; i++)
                        {
                            consSheet.Column(i).AutoFit();
                        }
                        #endregion
                    }
                });
        }
    }
}
