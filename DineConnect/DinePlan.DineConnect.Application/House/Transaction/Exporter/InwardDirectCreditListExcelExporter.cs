﻿
using System.Collections.Generic;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Transaction.Exporter
{
    public class InwardDirectCreditListExcelExporter : FileExporterBase, IInwardDirectCreditListExcelExporter
    {
        public FileDto ExportToFile(List<InwardDirectCreditWithSupplierName> dtos)
        {
            return CreateExcelPackage(
                "InwardDirectCreditList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("InwardDirectCredit"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Date"),
                        L("SupplierName"),
                        L("DcNumberGivenBySupplier"),
                        L("BillConversion")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.DcDate.ToString("yyyy-MMM-dd"),
                        _ => _.SupplierName,
                        _ => _.DcNumberGivenBySupplier,
                        _ => _.IsDcCompleted
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
