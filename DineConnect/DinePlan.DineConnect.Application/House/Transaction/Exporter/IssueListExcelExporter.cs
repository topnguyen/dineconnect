﻿using System.Collections.Generic;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Transaction.Exporter
{
    public class IssueListExcelExporter : FileExporterBase, IIssueListExcelExporter
    {
        public FileDto ExportToFile(List<IssueListDto> dtos)
        {
            return CreateExcelPackage(
                "IssueList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Issue"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("IssueReference"),
                        L("IssueTime"),
                        L("CompletedStatus")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.RequestSlipNumber,
                        _ => _.IssueTime.Value.ToString("yyyy-MMM-dd"),
                        _ => _.CompletedStatus
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }

		public FileDto CreateIssueTemplate(List<TransferTemplateDto> dtos, string location, string dateRange)
		{

			var fileReturn = CreateExcelPackage(L("Issue") + "-" + location + "-" + dateRange + ".xlsx",
				excelPackage =>
				{
					var sheet = excelPackage.Workbook.Worksheets.Add(L("Issue"));
					sheet.OutLineApplyStyle = true;

					int loopCnt = 1;
					AddHeader(sheet, loopCnt, "Location Id");
					loopCnt++;

					AddHeader(sheet, loopCnt, "Location");
					loopCnt++;

					AddHeader(sheet, loopCnt, "Category");
					loopCnt++;

					AddHeader(sheet, loopCnt, "Material Id");
					loopCnt++;

					AddHeader(sheet, loopCnt, "Material Name");
					loopCnt++;

					AddHeader(sheet, loopCnt, L("OnHand"));
					loopCnt++;

					AddHeader(sheet, loopCnt, L("Quantity"));
					loopCnt++;

					int row = 2;
					int col = 1;

					foreach (var mat in dtos)
					{
						AddDetail(sheet, row, col, mat.LocationRefId, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
						col++;
						AddDetail(sheet, row, col, mat.LocationRefName);
						col++;

						AddDetail(sheet, row, col, mat.MaterialGroupCategoryName);
						col++;

						AddDetail(sheet, row, col, mat.MaterialRefId, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
						col++;

						AddDetail(sheet, row, col, mat.MaterialRefName);
						col++;

						AddDetail(sheet, row, col, mat.CurrentOnHand, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
						col++;

						col = 1;
						row++;
					}
					row++;

					sheet.Protection.IsProtected = true;
					sheet.Column(7).Style.Locked = false;

					for (var i = 1; i <= 10; i++)
					{
						sheet.Column(i).AutoFit();
					}
				});

			return fileReturn;
		}
	}
}