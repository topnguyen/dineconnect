﻿using System.Collections.Generic;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;


namespace DinePlan.DineConnect.House.Transaction
{
    public interface IIssueListExcelExporter
    {
        FileDto ExportToFile(List<IssueListDto> dtos);
		FileDto CreateIssueTemplate(List<TransferTemplateDto> dtos, string location, string dateRange);

	}
}