﻿using System.Collections.Generic;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Transaction.Exporter
{
    public class SalesOrderListExcelExporter : FileExporterBase, ISalesOrderListExcelExporter
    {
        public FileDto ExportToFile(List<SalesOrderListWithRefNameDto> dtos)
        {
            return CreateExcelPackage(
                "SalesOrderList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("SalesOrder"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Date"),
                        L("Reference"),
                        L("CustomerName"),
                        L("ShippingLocation"),
                        L("Status")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.SoDate.ToString("yyyy-MMM-dd"),
                        _ => _.SoReferenceCode,
                        _ => _.CustomerName,
                        _ => _.ShippingLocationName,
                        _ => _.SoCurrentStatus
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}