﻿using System.Collections.Generic;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using System.Linq;
using System.Drawing;
using System;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Exporter.Util;
using Abp.Configuration;
using DinePlan.DineConnect.Configuration;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Connect.Master;
using System.Diagnostics;

namespace DinePlan.DineConnect.House.Transaction.Exporter
{
    public class InterTransferListExcelExporter : FileExporterBase, IInterTransferListExcelExporter
    {
        private SettingManager _settingManager;
        private int roundDecimals = 2;
        private string dateFormat = "dd-MM-yyyy";
        private readonly IRepository<Location> _locationRepo;

        public InterTransferListExcelExporter(
            SettingManager settingManager,
            IRepository<Location> locationRepo)
        {
            _settingManager = settingManager;
            roundDecimals = _settingManager.GetSettingValue<int>(AppSettings.ConnectSettings.Decimals);
            dateFormat = _settingManager.GetSettingValue(AppSettings.ConnectSettings.DateTimeFormat);
            _locationRepo = locationRepo;
        }

        public FileDto ExportToFile(List<InterTransferViewDto> dtos)
        {
            return CreateExcelPackage(
                "InterTransferList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("InterTransfer"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Location"),
                        L("RequestLocation"),
                        L("CurrentStatus"),
                        L("RequestDate"),
                        L("DeliveryDateRequested")
                         );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.LocationRefName,
                        _ => _.RequestLocationRefName,
                        _ => _.CurrentStatus,
                        _ => _.RequestDate.ToString("dd-MMM-yyyy"),
                        _ => _.DeliveryDateRequested.ToString("dd-MMM-yyyy")
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }


        public FileDto TransferStatusExportToFile(List<StatusConsolidatedDto> dtos, DateTime FromDate, DateTime ToDate, string UserName)
        {
            return CreateExcelPackage(
        "TransferStatus.xlsx",
        excelPackage =>
        {
            var sheet = excelPackage.Workbook.Worksheets.Add(L("TransferStatus"));
            sheet.OutLineApplyStyle = true;
            int row = 1;
            AddHeaderWithMergeWithColumnCount(sheet, row, 1, 9, L("TransferStatusReport"));
            row++;
            AddHeaderWithMergeWithColumnCount(sheet, row, 1, 9, L("FromTimeToTime", FromDate.ToString("dd-MMM-yyyy"), ToDate.ToString("dd-MMM-yyyy")));
            row++;
            AddDetail(sheet, row, 1, L("PrintedOn"));
            AddDetail(sheet, row, 2, DateTime.Today.ToString("dd-MMM-yyyy"));
            row++;
            AddDetail(sheet, row, 1, L("PrintedBy"));
            AddDetail(sheet, row, 2, UserName);
            row++;
            row++;

            AddHeader(sheet, row, L("Material"), L("IssueQty"), L("Status"), L("ReceivedQty"), L("UOM"), L("Price"), L("TotalCost"), L("DifferenceQty"), L("DifferenceCost"));
            decimal OverAllIssueCost = 0;
            decimal OverAllDiffAmount = 0;
            foreach (var mas in dtos)
            {
                row++;
                AddHeaderWithMergeWithColumnCount(sheet, row, 1, 3, "Location : " + mas.FromLocationRefName + " =>" + mas.ToLocationRefName);
                row++;
                AddHeaderWithMergeWithColumnCount(sheet, row, 1, 3, "Date : " + mas.TransferDate.ToString("dd-MMM-yyyy") + " , Ref Id " + mas.TransferRefId);
                row++;
                AddObjects(sheet, row, mas.TransferList,
                        t => t.MaterialRefName,
                        t => t.IssuedQty,
                        t => t.TransferStatus,
                        t => t.ReceivedQty,
                        t => t.UnitRefName,
                        t => t.Price,
                        t => t.TotalAmount,
                        t => t.DiffQty,
                        t => t.DiffAmount
                        );
                row = row + mas.TransferList.Count;
                row++;
                AddDetail(sheet, row, 2, L("SubTotal"));
                decimal totalIssueCost = Math.Round(mas.TransferList.Sum(t => t.TotalAmount), 2);
                AddDetail(sheet, row, 7, totalIssueCost);
                decimal totalDiffAmount = Math.Round(mas.TransferList.Sum(t => t.DiffAmount), 2);
                AddDetail(sheet, row, 9, totalDiffAmount);
                OverAllIssueCost = OverAllIssueCost + totalIssueCost;
                OverAllDiffAmount = OverAllDiffAmount + totalDiffAmount;

                row++;
                row++;
            }
            row++;
            AddDetail(sheet, row, 2, L("Total"));
            AddDetail(sheet, row, 7, OverAllIssueCost);
            AddDetail(sheet, row, 9, OverAllDiffAmount);
            row++;
            for (var i = 1; i <= 20; i++)
            {
                sheet.Column(i).AutoFit();
            }
        });
        }


        public FileDto ExportConsolidated(ExportToExcelDataConsolidated input)
        {
            var fileReturn = CreateExcelPackage(
                input.FileName + ".xlsx",
                excelPackage =>
                {
                    var dtos = input.ExportData;
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("InterTransfer"));
                    sheet.OutLineApplyStyle = true;

                    List<string> locationList = dtos.Select(t => t.LocationRefCode).Distinct().ToList();

                    int loopcnt = 1;
                    AddHeader(sheet, loopcnt, "#");
                    loopcnt++;
                    AddHeader(sheet, loopcnt, L("Group"));
                    loopcnt++;
                    AddHeader(sheet, loopcnt, L("Category"));
                    loopcnt++;
                    AddHeader(sheet, loopcnt, L("Material"));
                    loopcnt++;
                    foreach (var location in locationList)
                    {
                        AddHeader(sheet, loopcnt, location);
                        loopcnt++;
                    }
                    AddHeader(sheet, loopcnt, L("Total"));
                    loopcnt++;

                    if (input.DoesPendingRequest)
                    {
                        AddHeader(sheet, loopcnt, L("OnHand"));
                        loopcnt++;
                        AddHeader(sheet, loopcnt, L("Balance"));
                        loopcnt++;
                        AddHeader(sheet, loopcnt, L("Remarks"));
                        loopcnt++;

                        AddHeader(sheet, loopcnt, L("Re-OrderLevel"));
                        loopcnt++;
                    }
                    AddHeader(sheet, loopcnt, L("UOM"));
                    loopcnt++;

                    int TotalColumns = loopcnt-1;

                    List<int> materialList = dtos.Select(t => t.MaterialRefId).Distinct().ToList();
                    int sno = 1;
                    int row = 2;
                    int col = 1;
                    decimal totalRequestQty = 0;

                    foreach (var matid in materialList)
                    {
                        var material = dtos.FirstOrDefault(t => t.MaterialRefId == matid);

                        AddDetail(sheet, row, col, sno, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                        col++;
                        AddDetail(sheet, row, col, material.MaterialGroupRefName);
                        col++;
                        AddDetail(sheet, row, col, material.MaterialGroupCategoryRefName);
                        col++;
                        AddDetail(sheet, row, col, material.MaterialRefName);
                        col++;

                        foreach (var location in locationList)
                        {
                            var locdetail = dtos.FirstOrDefault(t => t.MaterialRefId == matid && t.LocationRefCode.Equals(location));
                            if (locdetail != null)
                            {
                                totalRequestQty = totalRequestQty + locdetail.RequestQty;

                                AddDetail(sheet, row, col, locdetail.RequestQty, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            }
                            col++;
                        }

                        AddDetail(sheet, row, col, totalRequestQty, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                        col++;

                        if (input.DoesPendingRequest)
                        {
                            AddDetail(sheet, row, col, material.LiveStock, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;

                            string remarks = "";
                            decimal balanceQty;
                            balanceQty = material.LiveStock - totalRequestQty;
                            AddDetail(sheet, row, col, balanceQty, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);

                            if (balanceQty < 0)
                            {
                                remarks = L("ShortageAlert");
                                sheet.Cells[row, col].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                sheet.Cells[row, col].Style.Fill.BackgroundColor.SetColor(Color.Red);

                            }
                            else if (balanceQty == 0)
                            {
                                if (material.MaterialTypeId == 1)
                                    remarks = L("NeedToRaisePurchaseOrder");
                                else
                                    remarks = L("ProductionNeeded");

                                sheet.Cells[row, col].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                sheet.Cells[row, col].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                            }
                            else
                            {
                                if (material.ReorderLevel > balanceQty)
                                    remarks = L("LessThanReorderLevel");
                            }

                            col++;

                            AddDetail(sheet, row, col, remarks);
                            col++;

                            AddDetail(sheet, row, col, material.ReorderLevel, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                            col++;
                        }
                        AddDetail(sheet, row, col, material.Uom);
                        col++;

                        col = 1;
                        row++;
                        sno++;
                        totalRequestQty = 0;
                    }

                    for (var i = 1; i <= locationList.Count + 6; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }

                    // rows and columns indices
                    int startRowIndex = 1;
                    int startingColIndex = 1;
                    int totalColIndex = TotalColumns ;

                    // rowIndex holds the current running row index
                    int toRowIndex = dtos.Count + 1;

                    AutoFilterRangeGiven(sheet, startRowIndex, startingColIndex, toRowIndex, totalColIndex);

                });
            Debug.WriteLine("ExportConsolidatedToExcel() Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
            return fileReturn;

        }

        public async Task<List<FileDto>> ExportDetailToFile(InterTransferReportDto output, InputInterTransferReport input)
        {

            bool TransferOutFlag = false;
            bool TransferInFlag = false;
            string transferStatusPrint = "";
            string transferColumnStatus = "";
            if (output.TransferStatus.Equals("Transfer Out"))
            {
                TransferOutFlag = true;
                transferStatusPrint = L("Transfer") + " " + L("Out");
                transferColumnStatus = L("Transfer") + " " + L("To");
            }
            else
            {
                TransferInFlag = true;
                transferStatusPrint = L("Transfer") + " " + L("In");
                transferColumnStatus = L("Transfer") + " " + L("From");
            }

            input.DateRange = "From: " + input.StartDate.Value.ToString("dd-MMM-yyyy") + " To: " + input.EndDate.Value.ToString("dd-MMM-yyyy");
            string fileName = transferStatusPrint + " " + input.DateRange + " " + output.LocationInfo.Code + " " + output.LocationInfo.Name;

            List<FileDto> returnFileList = new List<FileDto>();
            var a = CreateExcelPackage(
                fileName + ".xlsx",
                excelPackage =>
                {
                    if (output.DoesCustomReportFormat)
                    {
                        var sheet = excelPackage.Workbook.Worksheets.Add(transferStatusPrint + " " + output.LocationInfo.Code);
                        sheet.OutLineApplyStyle = true;
                        int row = 1;
                        AddHeaderWithMergeWithColumnCount(sheet, row, 1, 10, transferStatusPrint);
                        sheet.Row(row).Height = 30;
                        sheet.Cells[row, 1].Style.Font.Size = 15;
                        row++;
                        AddHeaderWithMergeWithColumnCount(sheet, row, 3, 2, L("Location") + " " + L("Code") + ":" + output.LocationInfo.Code);
                        AddHeaderWithMergeWithColumnCountAndAlignment(sheet, row, 6, 2, output.LocationInfo.Name,OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        row++;
                        AddHeaderWithMergeWithColumnCount(sheet, row, 1, 10, input.DateRange);
                        row++;
                        AddHeaderWithMergeWithColumnCount(sheet, row, 1, 1, L("PrintedOn"));
                        AddHeaderWithMergeWithColumnCountAndAlignment(sheet, row, 3, 1, output.PrintedTime.ToString("dd-MMM-yyyy"),OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        row++;
                        AddHeaderWithMergeWithColumnCount(sheet, row, 1, 1, L("PrintedBy"));
                        AddHeaderWithMergeWithColumnCountAndAlignment(sheet, row, 3, 1, output.PreparedUserName, OfficeOpenXml.Style.ExcelHorizontalAlignment.Left);
                        row++;

                        

                        AddHeader(sheet, row, L("#"), L("MaterialCode"), L("MaterialName"), transferColumnStatus, L("PUCost"), /*L("UOM"),*/ L("Purchase") + " " + L("Qty"), L("Inventory") + " " + L("Qty"), L("Recipe") + " " + L("Qty"), L("Total") + " " + L("Cost"),L("Date") , L("Remarks") );

                        row++;
                        int sno = 1;

                        foreach (var det in output.DetailReport)
                        {
                            int col = 1;
                            AddDetail(sheet, row, col++, sno++);
                            AddDetail(sheet, row, col++, det.MaterialPetName);
                            AddDetail(sheet, row, col++, det.MaterialRefName);
                            AddDetail(sheet, row, col++, det.LocationRefName);
                            AddDetailWithNumberFormat(sheet, row, col++, det.Price,"#0.0000");
                            //AddDetail(sheet, row, col++, det.Uom);
                            AddDetailWithNumberFormat(sheet, row, col++, det.QuantityInPurchaseUnit,"#0.0000");
                            AddDetailWithNumberFormat(sheet, row, col++, det.QuantityInStkAdjustedInventoryUnit, "#0.0000");
                            AddDetailWithNumberFormat(sheet, row, col++, det.QuantityInIssueUnit, "#0.0000");
                            AddDetailWithNumberFormat(sheet, row, col++, det.TotalAmount,"#0.0000");
                            AddDetail(sheet, row, col++, det.TransferDate.ToString("dd-MMM-yyyy"));
                            AddDetail(sheet, row, col++, det.Remarks);
                            row++;
                        }
                        row++;
                        {
                            AddDetail(sheet, row, 4, L("Total"));
                            AddDetailWithNumberFormat(sheet, row, 9, output.TotalTransferAmount, "0.0000");
                        }
                      
                        for (var i = 1; i <= 25; i++)
                        {
                            sheet.Column(i).AutoFit();
                        }
                    }

                    var consSheet = excelPackage.Workbook.Worksheets.Add(L("Consolidated"));
                    consSheet.OutLineApplyStyle = true;

                    AddHeader(
                        consSheet,
                        L("Material"),
                        L("Quantity"),
                        L("UOM"),
                        L("Price"),
                        L("TotalAmount")

                         );

                    AddObjects(
                        consSheet, 2, output.ConsolidatedReport,
                        _ => _.MaterialRefName,
                        _ => _.TotalQty,
                        _ => _.Uom,
                        _ => Math.Round(_.Price, 3),
                        _ => Math.Round(_.TotalAmount, 2)
                        );

                    decimal Total = output.ConsolidatedReport.Sum(t => t.TotalAmount);
                    int rowCount = 2 + output.ConsolidatedReport.Count + 1;
                    AddDetail(consSheet, rowCount, 5, Total, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);

                    for (var i = 1; i <= 10; i++)
                    {
                        consSheet.Column(i).AutoFit();
                    }

                    rowCount = 1;

                    var detailsheet = excelPackage.Workbook.Worksheets.Add(L("Detail"));
                    detailsheet.OutLineApplyStyle = true;

                    AddHeader(
                        detailsheet,
                        L("Location"),
                        L("Approved") + L("Date"),
                        L("ReceivedTime"),
                        L("MaterialType"),
                        L("Material"),
                        L("Quantity"),
                        L("UOM"),
                        L("Price"),
                        L("TotalAmount"),
                        L("Remarks")
                         );
                    rowCount = 2;
                    foreach (var gp in output.DetailReport.GroupBy(t => t.MaterialRefId))
                    {
                        AddObjects(
                            detailsheet, rowCount, gp.ToList(),
                            _ => _.LocationRefName,
                            _ => _.TransferDate.ToString("dd-MMM-yy"),
                     _ => _.ReceivedTime == null ? "" : _.ReceivedTime.Value.ToString("dd-MMM-yy"),
                     _ => _.MaterialTypeRefName,
                            _ => _.MaterialRefName,
                            _ => _.TotalQty,
                            _ => _.UnitRefName,
                            _ => Math.Round(_.Price, 3),
                            _ => Math.Round(_.TotalAmount, 2),
                            _ => _.Remarks
                            );


                        decimal subtotalQty = gp.Sum(t => t.TotalQty);
                        decimal subTotalamount = gp.Sum(t => t.TotalAmount);
                        rowCount = rowCount + gp.ToList().Count + 1;
                        AddDetail(detailsheet, rowCount, 4, L("SubTotal"));
                        AddDetail(detailsheet, rowCount, 5, subtotalQty, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                        AddDetail(detailsheet, rowCount, 8, subTotalamount, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                        rowCount = rowCount + 2;
                    }

                    decimal TotalDetail = output.DetailReport.Sum(t => t.TotalAmount);
                    AddDetail(detailsheet, rowCount, 4, L("Total"));
                    AddDetail(detailsheet, rowCount, 8, TotalDetail, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);

                    for (var i = 1; i <= 10; i++)
                    {
                        detailsheet.Column(i).AutoFit();
                    }
                });

            returnFileList.Add(a);
            return returnFileList;
        }

        public FileDto CreateDirectTransferTemplate(List<TransferTemplateDto> dtos, string location, string dateRange)
        {

            var fileReturn = CreateExcelPackage(L("Transfer") + "-" + location + "-" + dateRange + ".xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("TransferApproval"));
                    sheet.OutLineApplyStyle = true;

                    int loopCnt = 1;
                    AddHeader(sheet, loopCnt, "Location Id");
                    loopCnt++;

                    AddHeader(sheet, loopCnt, "Request Location");
                    loopCnt++;

                    AddHeader(sheet, loopCnt, "Category");
                    loopCnt++;

                    AddHeader(sheet, loopCnt, "Material Id");
                    loopCnt++;

                    AddHeader(sheet, loopCnt, "Material Name");
                    loopCnt++;

                    AddHeader(sheet, loopCnt, L("OnHand"));
                    loopCnt++;

                    AddHeader(sheet, loopCnt, "Request Quantity");
                    loopCnt++;

                    int row = 2;
                    int col = 1;

                    foreach (var mat in dtos)
                    {
                        AddDetail(sheet, row, col, mat.LocationRefId, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                        col++;
                        AddDetail(sheet, row, col, mat.LocationRefName);
                        col++;

                        AddDetail(sheet, row, col, mat.MaterialGroupCategoryName);
                        col++;

                        AddDetail(sheet, row, col, mat.MaterialRefId, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                        col++;

                        AddDetail(sheet, row, col, mat.MaterialRefName);
                        col++;

                        AddDetail(sheet, row, col, mat.CurrentOnHand, OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                        col++;

                        col = 1;
                        row++;
                    }
                    row++;

                    sheet.Protection.IsProtected = true;
                    sheet.Column(7).Style.Locked = false;

                    //var numvalidation = sheet.DataValidations.AddDecimalValidation("E:E");
                    //numvalidation.ShowErrorMessage = true;



                    for (var i = 1; i <= 7; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });

            return fileReturn;
        }

        public async Task<List<FileDto>> ExportDetailToPdfOrHtml(InterTransferReportDto dtos, InputInterTransferReport input, string reportName)
        {

            List<FileDto> returnFileList = new List<FileDto>();

            string fromLocation = "";
            string toLocations = "";

            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
            fromLocation = loc.Name;

            foreach (var locDto in input.ToLocations)
            {
                if (locDto.Id == loc.Id)
                    continue;
                toLocations = toLocations + locDto.Name + " ,";
            }
            toLocations = toLocations.Substring(0, toLocations.Length - 1);
            string reportHead;
            if (reportName.Equals("Issue"))
            {
                reportHead = L("TransferDetailReport");
            }
            else
            {
                reportHead = L("Transfer") + " " + L("Received") + " " + L("Report");
                string temp = fromLocation;
                fromLocation = toLocations;
                toLocations = temp;
            }

            #region TransferDetail
            {
                var eObject = new ExportInputObject
                {
                    ExportType = input.ExportType
                };

                eObject.Fields.Add("LocationRefName", new FieldAttributes
                {
                    DisplayName = L("Location"),
                    Alignment = FieldAlignment.Left,
                    FieldType = ExportFiledType.String,
                });
                eObject.Fields.Add("TransferDate", new FieldAttributes
                {
                    DisplayName = L("TransferDate"),
                    Alignment = FieldAlignment.Left,
                    FieldType = ExportFiledType.Date,
                    Format = dateFormat
                });

                eObject.Fields.Add("MaterialTypeRefName", new FieldAttributes
                {
                    DisplayName = L("MaterialType"),
                    Alignment = FieldAlignment.Left,
                    FieldType = ExportFiledType.String,
                });

                eObject.Fields.Add("MaterialRefName", new FieldAttributes
                {
                    DisplayName = L("Material"),
                    Alignment = FieldAlignment.Left,
                    FieldType = ExportFiledType.String,
                });
                eObject.Fields.Add("TotalQty", new FieldAttributes
                {
                    DisplayName = L("Quantity"),
                    Alignment = FieldAlignment.Right,
                    FieldType = ExportFiledType.Decimal,
                    Format = "N" + roundDecimals,
                });
                eObject.Fields.Add("UnitRefName", new FieldAttributes
                {
                    DisplayName = L("UOM"),
                    Alignment = FieldAlignment.Left,
                    FieldType = ExportFiledType.String,
                });
                eObject.Fields.Add("Price", new FieldAttributes
                {
                    DisplayName = L("Price"),
                    Alignment = FieldAlignment.Right,
                    FieldType = ExportFiledType.Decimal,
                    Format = "N" + roundDecimals,
                });
                eObject.Fields.Add("TotalAmount", new FieldAttributes
                {
                    DisplayName = L("TotalCost"),
                    Alignment = FieldAlignment.Right,
                    FieldType = ExportFiledType.Decimal,
                    Format = "N" + roundDecimals,
                    TotalRequired = true
                });

                eObject.Fields.Add("Remarks", new FieldAttributes
                {
                    DisplayName = L("Remarks"),
                    Alignment = FieldAlignment.Left,
                    FieldType = ExportFiledType.String,
                });

                var items = dtos.DetailReport;

                if (items.Any())
                {
                    //foreach(var mas in items)
                    {
                        var pOutput = new List<ExportDataObject>();
                        pOutput.AddRange(items);

                        StringBuilder firstPageBuilder = new StringBuilder();
                        firstPageBuilder.Append(reportHead);
                        firstPageBuilder.Append("{NL}");
                        firstPageBuilder.Append(string.Format(L("FromLocToLoc"), fromLocation, toLocations));
                        firstPageBuilder.Append("{NL}");
                        firstPageBuilder.Append(string.Format(L("FromTimeToTime"), input.StartDate.Value.ToString(dateFormat), input.EndDate.Value.ToString(dateFormat)));
                        firstPageBuilder.Append("{NL}");

                        StringBuilder lPageBuilder = new StringBuilder();
                        lPageBuilder.Append("{NL}");
                        lPageBuilder.Append(L("PrintedOn") + " :" + DateTime.Now.ToString(dateFormat));

                        eObject.FirstPage = firstPageBuilder.ToString();
                        eObject.LastPage = lPageBuilder.ToString();
                        eObject.ExportObjects = pOutput;

                        StringBuilder fileNameBuilder = new StringBuilder();
                        fileNameBuilder.Append(L("TransferDetail"));
                        fileNameBuilder.Append(L("-"));
                        fileNameBuilder.Append(!input.StartDate.Value.Equals(DateTime.MinValue)
                            ? input.StartDate.Value.ToString("dd/MM/yyyy")
                            : DateTime.Now.ToString("dd/MM/yyyy"));
                        fileNameBuilder.Append(" - ");
                        fileNameBuilder.Append(!input.EndDate.Value.Equals(DateTime.MinValue)
                            ? input.EndDate.Value.ToString("dd/MM/yyyy")
                            : DateTime.Now.ToString("dd/MM/yyyy"));

                        eObject.FileName = fileNameBuilder.ToString();
                        var exporter = new DineConnectDocExporter(eObject);
                        FileDto a = exporter.ExportFile(AppFolders.TempFileDownloadFolder);
                        returnFileList.Add(a);
                    }
                }
            }


            #endregion
            #region TransferConsolidated
            {
                var eObject = new ExportInputObject
                {
                    ExportType = input.ExportType
                };

                eObject.Fields.Add("MaterialRefName", new FieldAttributes
                {
                    DisplayName = L("Material"),
                    Alignment = FieldAlignment.Left,
                    FieldType = ExportFiledType.String,
                });
                eObject.Fields.Add("TotalQty", new FieldAttributes
                {
                    DisplayName = L("Quantity"),
                    Alignment = FieldAlignment.Right,
                    FieldType = ExportFiledType.Decimal,
                    Format = "N" + roundDecimals,
                });
                eObject.Fields.Add("Uom", new FieldAttributes
                {
                    DisplayName = L("UOM"),
                    Alignment = FieldAlignment.Left,
                    FieldType = ExportFiledType.String,
                });
                eObject.Fields.Add("Price", new FieldAttributes
                {
                    DisplayName = L("Price"),
                    Alignment = FieldAlignment.Right,
                    FieldType = ExportFiledType.Decimal,
                    Format = "N" + roundDecimals,
                });
                eObject.Fields.Add("TotalAmount", new FieldAttributes
                {
                    DisplayName = L("TotalCost"),
                    Alignment = FieldAlignment.Right,
                    FieldType = ExportFiledType.Decimal,
                    Format = "N" + roundDecimals,
                    TotalRequired = true
                });

                var items = dtos.ConsolidatedReport;

                if (items.Any())
                {
                    {
                        var pOutput = new List<ExportDataObject>();
                        pOutput.AddRange(items);

                        StringBuilder firstPageBuilder = new StringBuilder();
                        firstPageBuilder.Append(reportHead + " - " + L("Consolidated"));
                        firstPageBuilder.Append("{NL}");
                        firstPageBuilder.Append(string.Format(L("FromLocToLoc"), fromLocation, toLocations));
                        firstPageBuilder.Append("{NL}");
                        firstPageBuilder.Append(string.Format(L("FromTimeToTime"), input.StartDate.Value.ToString(dateFormat), input.EndDate.Value.ToString(dateFormat)));
                        firstPageBuilder.Append("{NL}");

                        StringBuilder lPageBuilder = new StringBuilder();
                        lPageBuilder.Append("{NL}");
                        lPageBuilder.Append(L("PrintedOn") + " :" + DateTime.Now.ToString(dateFormat));

                        eObject.FirstPage = firstPageBuilder.ToString();
                        eObject.LastPage = lPageBuilder.ToString();
                        eObject.ExportObjects = pOutput;

                        StringBuilder fileNameBuilder = new StringBuilder();
                        fileNameBuilder.Append(L("TransferConsolidated"));
                        fileNameBuilder.Append(L("-"));
                        fileNameBuilder.Append(!input.StartDate.Value.Equals(DateTime.MinValue)
                            ? input.StartDate.Value.ToString("dd/MM/yyyy")
                            : DateTime.Now.ToString("dd/MM/yyyy"));
                        fileNameBuilder.Append(" - ");
                        fileNameBuilder.Append(!input.EndDate.Value.Equals(DateTime.MinValue)
                            ? input.EndDate.Value.ToString("dd/MM/yyyy")
                            : DateTime.Now.ToString("dd/MM/yyyy"));

                        eObject.FileName = fileNameBuilder.ToString();
                        var exporter = new DineConnectDocExporter(eObject);
                        FileDto a = exporter.ExportFile(AppFolders.TempFileDownloadFolder);
                        returnFileList.Add(a);
                    }
                }
            }
            #endregion



            return returnFileList;

        }

        public FileDto ExportInterTrasnferReceivedDetailConsolidatedGroupCategoryMaterials(LocationWiseClosingStockGroupCateogryMaterialDto input)
        {
            var masterDtos = input.LocationWiseHeadWiseStockList;

            return CreateExcelPackage(
                @L("InterTransfer") + " " +  @L("ReceivedSummary") + " " + input.InputFilterDto.StartDate.ToString("dd-MMM-yyyy")+ " " + input.InputFilterDto.EndDate.ToString("dd-MMM-yyy") + " " + L("Location") + " " + L("Wise") + ".xlsx",
                excelPackage =>
                {
                    decimal totalAmount = 0;
                    {
                        #region Group Sheet
                        var consSheet = excelPackage.Workbook.Worksheets.Add(L("Group"));
                        consSheet.OutLineApplyStyle = true;
                        int rowIndex = 1;
                        int colIndex = 1;
                        AddHeader(consSheet, rowIndex, colIndex++, L("Location").ToUpper());
                        foreach (var lst in input.MaterialGroupWisePurchaseDtos)
                        {
                            AddHeader(consSheet, rowIndex, colIndex++, lst.MaterialGroupRefName);
                        }
                        AddHeader(consSheet, rowIndex, colIndex++, L("Total").ToUpper());
                        rowIndex++;

                        foreach (var master in masterDtos)
                        {
                            colIndex = 1;
                            consSheet.Row(rowIndex).Height = 21;
                            AddHeader(consSheet, rowIndex, colIndex++, master.LocationRefName);
                            foreach (var lst in input.MaterialGroupWisePurchaseDtos)
                            {
                                var existRecord = master.MaterialGroupWisePurchaseDtos.FirstOrDefault(t => t.MaterialGroupRefId == lst.MaterialGroupRefId);
                                if (existRecord != null)
                                {
                                    AddDetailWithNumberFormat(consSheet, rowIndex, colIndex++, existRecord.TotalAmount, "0.00");
                                }
                                else
                                {
                                    colIndex++;
                                }
                            }
                            totalAmount = master.MaterialGroupWisePurchaseDtos.Sum(t => t.TotalAmount);
                            AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, totalAmount, "0.00");
                            rowIndex++;
                        }
                        colIndex = 1;
                        AddDetail(consSheet, rowIndex, colIndex++, L("Total"), OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                        foreach (var lst in input.MaterialGroupWisePurchaseDtos)
                        {
                            AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, lst.TotalAmount, "0.00");
                        }
                        totalAmount = input.MaterialGroupWisePurchaseDtos.Sum(t => t.TotalAmount);
                        AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, totalAmount, "0.00");
                        rowIndex++;
                        for (var i = 1; i <= input.MaterialGroupWisePurchaseDtos.Count + 2; i++)
                        {
                            consSheet.Column(i).AutoFit();
                        }
                        #endregion
                    }

                    {
                        #region Category Sheet
                        var consSheet = excelPackage.Workbook.Worksheets.Add(L("Category"));
                        consSheet.OutLineApplyStyle = true;
                        int rowIndex = 1;
                        int colIndex = 1;
                        AddHeader(consSheet, rowIndex, colIndex++, L("Location").ToUpper());
                        foreach (var lst in input.MaterialGroupCategoryWisePurchaseDtos)
                        {
                            AddHeader(consSheet, rowIndex, colIndex++, lst.MaterialGroupCategoryRefName);
                        }
                        AddHeader(consSheet, rowIndex, colIndex++, L("Total").ToUpper());
                        rowIndex++;

                        foreach (var master in masterDtos)
                        {
                            colIndex = 1;
                            consSheet.Row(rowIndex).Height = 21;
                            AddHeader(consSheet, rowIndex, colIndex++, master.LocationRefName);
                            foreach (var lst in input.MaterialGroupCategoryWisePurchaseDtos)
                            {
                                var existRecord = master.MaterialGroupCategoryWisePurchaseDtos.FirstOrDefault(t => t.MaterialGroupCategoryRefId == lst.MaterialGroupCategoryRefId);
                                if (existRecord != null)
                                {
                                    AddDetailWithNumberFormat(consSheet, rowIndex, colIndex++, existRecord.TotalAmount, "0.00");
                                }
                                else
                                {
                                    colIndex++;
                                }
                            }
                            totalAmount = master.MaterialGroupCategoryWisePurchaseDtos.Sum(t => t.TotalAmount);
                            AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, totalAmount, "0.00");
                            rowIndex++;
                        }
                        colIndex = 1;
                        AddDetail(consSheet, rowIndex, colIndex++, L("Total"), OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                        foreach (var lst in input.MaterialGroupCategoryWisePurchaseDtos)
                        {
                            AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, lst.TotalAmount, "0.00");
                        }
                        totalAmount = input.MaterialGroupCategoryWisePurchaseDtos.Sum(t => t.TotalAmount);
                        AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, totalAmount, "0.00");
                        rowIndex++;
                        for (var i = 1; i <= input.MaterialGroupCategoryWisePurchaseDtos.Count + 2; i++)
                        {
                            consSheet.Column(i).AutoFit();
                        }
                        #endregion
                    }

                    if (input.InputFilterDto.ExportMaterialAlso == true)
                    {
                        #region Material Sheet
                        var consSheet = excelPackage.Workbook.Worksheets.Add(L("Material"));
                        consSheet.OutLineApplyStyle = true;
                        int rowIndex = 1;
                        int colIndex = 1;
                        AddHeader(consSheet, rowIndex, colIndex++, L("Location").ToUpper());
                        foreach (var lst in input.MaterialWisePurchaseDtos)
                        {
                            AddHeader(consSheet, rowIndex, colIndex++, lst.MaterialRefName);
                        }
                        AddHeader(consSheet, rowIndex, colIndex++, L("Total").ToUpper());
                        rowIndex++;

                        foreach (var master in masterDtos)
                        {
                            colIndex = 1;
                            consSheet.Row(rowIndex).Height = 21;
                            AddHeader(consSheet, rowIndex, colIndex++, master.LocationRefName);
                            foreach (var lst in input.MaterialWisePurchaseDtos)
                            {
                                var existRecord = master.MaterialWisePurchaseDtos.FirstOrDefault(t => t.MaterialRefId == lst.MaterialRefId);
                                if (existRecord != null)
                                {
                                    AddDetailWithNumberFormat(consSheet, rowIndex, colIndex++, existRecord.TotalAmount, "0.00");
                                }
                                else
                                {
                                    colIndex++;
                                }
                            }
                            totalAmount = master.MaterialWisePurchaseDtos.Sum(t => t.TotalAmount);
                            AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, totalAmount, "0.00");
                            rowIndex++;
                        }
                        colIndex = 1;
                        AddDetail(consSheet, rowIndex, colIndex++, L("Total"), OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                        foreach (var lst in input.MaterialWisePurchaseDtos)
                        {
                            AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, lst.TotalAmount, "0.00");
                        }
                        totalAmount = input.MaterialWisePurchaseDtos.Sum(t => t.TotalAmount);
                        AddDetailWithNumberFormatAsBoldText(consSheet, rowIndex, colIndex++, totalAmount, "0.00");
                        rowIndex++;
                        for (var i = 1; i <= input.MaterialWisePurchaseDtos.Count + 2; i++)
                        {
                            consSheet.Column(i).AutoFit();
                        }
                        #endregion
                    }
                });
        }


        
    }
}
