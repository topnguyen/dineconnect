﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using System;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.House.Master;
using DinePlan.DineConnect.Common.Dto;
using System.Web.Hosting;
using DinePlan.DineConnect.House;

namespace DinePlan.DineConnect.House.Transaction.Implementation
{
    public class SalesInvoiceAppService : DineConnectAppServiceBase, ISalesInvoiceAppService
    {

        private readonly ISalesInvoiceListExcelExporter _salesinvoiceExporter;
        private readonly ISalesInvoiceManager _salesinvoiceManager;
        private readonly IRepository<SalesInvoice> _salesinvoiceRepo;
        private readonly IRepository<SalesInvoiceDetail> _salesinvoicedetailRepo;
        private readonly IRepository<SalesInvoiceTaxDetail> _salesinvoicetaxdetailRepo;
        private readonly IRepository<SalesInvoiceDeliveryOrderLink> _salesinvoiceDeliveryOrderLinkRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<MaterialGroupCategory> _materialGroupCategoryRepo;
        private readonly IRepository<SalesDeliveryOrder> _salesdeliverorderRepo;
        private readonly IRepository<SalesDeliveryOrderDetail>_salesdeliverorderDetailRepo;
        private readonly IRepository<CustomerMaterial> _customermaterialRepo;
        private readonly IRepository<Unit> _unitRepo;
        private readonly IRepository<UnitConversion> _unitConversionRepo;
        private readonly IRepository<SalesTax> _salestaxRepo;
        private readonly IRepository<SalesTaxTemplateMapping> _salestaxtemplatemappingRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IMaterialAppService _materialAppService;
        private readonly IRepository<Customer> _customerRepo;
        private readonly ISalesDeliveryOrderAppService _salesdeliverorderAppService;
        private readonly IRepository<SalesOrder> _salesorderRepo;
        private readonly IRepository<SalesOrderDetail> _salesorderdetailRepo;
        private readonly IRepository<SalesOrderTaxDetail> _salesorderTaxRepo;
        private readonly IRepository<MaterialLocationWiseStock> _materiallocationwisestockRepo;
		private readonly IRepository<SalesInvoiceDeliveryOrderLink> _salesInvoiceDeliveryOrderLinksRepo;


		public SalesInvoiceAppService(ISalesInvoiceManager invoiceManager,
            IRepository<SalesInvoice> invoiceRepo,
            ISalesInvoiceListExcelExporter invoiceExporter,
            IRepository<SalesInvoiceDetail> invoicedetailRepo,
            IRepository<SalesInvoiceDeliveryOrderLink> invoiceDeliveryOrderLinkRepo,
            IRepository<Material> materialRepo,
            IRepository<SalesDeliveryOrder> inwarddirectcreditRepo,
            IRepository<SalesDeliveryOrderDetail> inwarddirectcreditDetailRepo,
            IRepository<CustomerMaterial> customermaterialRepo,
            IRepository<Unit> unitRepo,
            IRepository<SalesTax> taxRepo,
            IRepository<SalesTaxTemplateMapping> taxtemplatemappingRepo,
            IRepository<Location> locationRepo,
            IMaterialAppService materialAppService,
            IRepository<SalesInvoiceTaxDetail> invoicetaxdetailRepo,
            IRepository<Customer> customerRepo,
            ISalesDeliveryOrderAppService inwarddirectcreditAppService,
            IRepository<MaterialGroupCategory> materialGroupCategoryRepo,
            IRepository<SalesOrder> purchaseorderRepo,
            IRepository<SalesOrderDetail> purchaseorderdetailRepo,
            IRepository<SalesOrderTaxDetail> purchaseorderTaxRepo,
            IRepository<UnitConversion> unitConversionRepo,
            IRepository<MaterialLocationWiseStock> materiallocationwisestockRepo,
			IRepository<SalesInvoiceDeliveryOrderLink> salesInvoiceDeliveryOrderLinkRepo

			)
        {
            _salesinvoiceManager = invoiceManager;
            _salesinvoiceRepo = invoiceRepo;
            _salesinvoiceExporter = invoiceExporter;
            _salesinvoicedetailRepo = invoicedetailRepo;
            _salesinvoiceDeliveryOrderLinkRepo = invoiceDeliveryOrderLinkRepo;
            _materialRepo = materialRepo;
           _salesdeliverorderRepo = inwarddirectcreditRepo;
           _salesdeliverorderDetailRepo = inwarddirectcreditDetailRepo;
            _customermaterialRepo = customermaterialRepo;
            _unitRepo = unitRepo;
            _salestaxRepo = taxRepo;
            _salestaxtemplatemappingRepo = taxtemplatemappingRepo;
            _locationRepo = locationRepo;
            _salesinvoicetaxdetailRepo = invoicetaxdetailRepo;
            _materialAppService = materialAppService;
            _customerRepo = customerRepo;
           _salesdeliverorderAppService = inwarddirectcreditAppService;
            _materialGroupCategoryRepo = materialGroupCategoryRepo;
            _salesorderRepo = purchaseorderRepo;
            _salesorderdetailRepo = purchaseorderdetailRepo;
            _salesorderTaxRepo = purchaseorderTaxRepo;
            _unitConversionRepo = unitConversionRepo;
            _materiallocationwisestockRepo = materiallocationwisestockRepo;
			_salesInvoiceDeliveryOrderLinksRepo = salesInvoiceDeliveryOrderLinkRepo;

		}

        public async Task<PagedResultOutput<SalesInvoiceListDto>> GetAll(GetSalesInvoiceInput input)
        {
            var rsCustomer = _customerRepo.GetAll().WhereIf(!input.CustomerName.IsNullOrEmpty(), p => p.CustomerName.Contains(input.CustomerName));

            var rsSalesInvoice = _salesinvoiceRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId).WhereIf(!input.InvoiceNumber.IsNullOrEmpty(), t => t.InvoiceNumber.Contains(input.InvoiceNumber));

            if (input.StartDate != null && input.EndDate != null)
            {
                rsSalesInvoice = rsSalesInvoice.Where(t => t.InvoiceDate >= input.StartDate && t.InvoiceDate <= input.EndDate);
            }


            var allItems = (from inv in rsSalesInvoice
                            join sup in rsCustomer
                            on inv.CustomerRefId equals sup.Id
                            select new SalesInvoiceListDto
                            {
                                InvoiceAmount = inv.InvoiceAmount,
                                InvoiceDate = inv.InvoiceDate,
                                AccountDate = inv.AccountDate,
                                InvoiceNumber = inv.InvoiceNumber,
								
                                CustomerRefId = inv.CustomerRefId,
                                CustomerRefName = sup.CustomerName,
                                TotalShipmentCharges = inv.TotalShipmentCharges,
                                CreationTime = inv.CreationTime,
                                Id = inv.Id,
                                IsDirectSalesInvoice = inv.IsDirectSalesInvoice
                            });

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<SalesInvoiceListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<SalesInvoiceListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetSalesInvoiceInput input)
        {
            input.MaxResultCount = AppConsts.MaxPageSize;
            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<SalesInvoiceListDto>>();
            return _salesinvoiceExporter.ExportToFile(allListDtos);
        }



        public async Task<FileDto> GetSalesInvoiceSummaryAllToExcel(GetSalesInvoiceInput input)
        {
            input.MaxResultCount = AppConsts.MaxPageSize;
            var allList = await GetSalesInvoiceSearch(input);
            var allListDtos = allList.Items.MapTo<List<SalesInvoiceListDto>>();
            return _salesinvoiceExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDcMaterialDataDetail> GetMaterialsFromDcId(GetDcMaterialInfo input)
        {
            //SalesInvoiceEditDto editDto;
            //await SetDcCompleteStatus(input.CustomerRefId);

            List<SalesInvoiceDetailViewDto> lstMaterial;

            lstMaterial = await (from inw in _salesdeliverorderDetailRepo.GetAll()
                                    .Where(a => a.DeliveryRefId == input.DeliveryRefId)
                                 join mat in _materialRepo.GetAll()
                                 on inw.MaterialRefId equals mat.Id
                                 join un in _unitRepo.GetAll()
                                 on mat.DefaultUnitId equals un.Id
                                 join uiss in _unitRepo.GetAll()
                                 on inw.UnitRefId equals uiss.Id
                                 select new SalesInvoiceDetailViewDto
                                 {
                                     Sno = 0,
                                     Id = 0,
                                     DcTranId = inw.DeliveryRefId,
                                     MaterialRefId = inw.MaterialRefId,
                                     MaterialRefName = mat.MaterialName,
                                     TotalQty = (inw.DeliveryQty - inw.BillConvertedQty),
                                     SalesTaxAmount = 0,
                                     SalesInvoiceRefId = 0,
                                     DiscountFlag = "",
                                     DiscountPercentage = 0,
                                     DiscountAmount = 0,
                                     TotalAmount = 0,
                                     Remarks = "",
                                     DefaultUnitId = mat.DefaultUnitId,
                                     Uom = un.Name,
                                     UnitRefId = inw.UnitRefId,
                                     UnitRefName = uiss.Name,

                                 }).ToListAsync();

            int loopcnt = 0;

            int companyRefid = await _locationRepo.GetAll().Where(l => l.Id == input.LocationRefId).Select(l => l.CompanyRefId).FirstOrDefaultAsync();

            SalesOrder poMaster = new SalesOrder();
            List<SalesOrderDetail> poDetail = new List<SalesOrderDetail>();
            List<SalesOrderTaxDetail> poTaxDetail = new List<SalesOrderTaxDetail>();


            SalesDeliveryOrder inwardMaster;
            inwardMaster = await _salesdeliverorderRepo.FirstOrDefaultAsync(t => t.Id == input.DeliveryRefId);

            if (inwardMaster != null)
            {
                poMaster = await _salesorderRepo.FirstOrDefaultAsync(t => t.Id == inwardMaster.SoRefId);
                if (poMaster != null)
                {
                    poDetail = await _salesorderdetailRepo.GetAll().Where(t => t.SoRefId == poMaster.Id).ToListAsync();
                    poTaxDetail = await _salesorderTaxRepo.GetAll().Where(t => t.SoRefId == poMaster.Id).ToListAsync();
                }
            }

            var rsUnitConversion = await _unitConversionRepo.GetAll().ToListAsync();

            foreach (var mat in lstMaterial)
            {
                if (poDetail.Count == 0)
                {
                    var supmaterial = await _customermaterialRepo.FirstOrDefaultAsync(s => s.CustomerRefId == input.CustomerRefId && s.MaterialRefId == mat.MaterialRefId);

                    if (supmaterial != null)
                    {
                        decimal conversionFactor = 1;
                        if (mat.UnitRefId == mat.DefaultUnitId)
                            conversionFactor = 1;
                        else
                        {
                           var conv  =
                                rsUnitConversion.First(
                                    t => t.BaseUnitId == mat.DefaultUnitId && t.RefUnitId == mat.UnitRefId);

                            if (conv != null)
                            {
                                conversionFactor = conv.Conversion;
                            }
							else
							{
								var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == mat.DefaultUnitId);
								if (baseUnit == null)
								{
									throw new UserFriendlyException(L("UnitIdDoesNotExist", mat.DefaultUnitId));
								}
								var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == mat.UnitRefId);
								if (refUnit == null)
								{
									throw new UserFriendlyException(L("UnitIdDoesNotExist", mat.UnitRefId));
								}
								throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
							}

						}

						lstMaterial[loopcnt].Price = supmaterial.MaterialPrice * 1 / conversionFactor;
                        lstMaterial[loopcnt].TotalAmount = Math.Round(lstMaterial[loopcnt].TotalQty * lstMaterial[loopcnt].Price, 2);
                    }
                }
                else
                {
                    var poMaterial = poDetail.FirstOrDefault(t => t.MaterialRefId == mat.MaterialRefId);
                    if (poMaterial != null)
                    {
                        decimal conversionFactor = 1;
                        if (mat.UnitRefId == poMaterial.UnitRefId)
                            conversionFactor = 1;
                        else
                        {
                            var conv=
                                rsUnitConversion.First(
                                    t => t.BaseUnitId == mat.UnitRefId && t.RefUnitId == poMaterial.UnitRefId);
                            if (conv != null)
                            {
                                conversionFactor = conv.Conversion;
                            }
							else
							{
								var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == mat.UnitRefId);
								if (baseUnit == null)
								{
									throw new UserFriendlyException(L("UnitIdDoesNotExist", mat.UnitRefId));
								}
								var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == poMaterial.UnitRefId);
								if (refUnit == null)
								{
									throw new UserFriendlyException(L("UnitIdDoesNotExist", poMaterial.UnitRefId));
								}
								throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
							}

						}

						lstMaterial[loopcnt].Price = poMaterial.Price * 1 / conversionFactor;
                        lstMaterial[loopcnt].TotalAmount = Math.Round(lstMaterial[loopcnt].TotalQty * lstMaterial[loopcnt].Price, 2);
                    }

                }

                List<TaxForMaterial> taxtoAdded = new List<TaxForMaterial>();
                decimal matTaxAmount = 0;

                // taxtemplate;
                // Find any specific TAX For particular Material
                List<SalesTaxTemplateMapping> taxTemplates;
                taxTemplates = await _salestaxtemplatemappingRepo.GetAll().Where(t => t.MaterialRefId == mat.MaterialRefId).ToListAsync();

                if (taxTemplates.Count() == 0 || taxTemplates == null)
                {
                    // If not specific for mateial then need to  Find Any Sepcific Tax For Material Group Category id
                    taxTemplates = await _salestaxtemplatemappingRepo.GetAll().Where(t => t.MaterialGroupCategoryRefId == mat.MaterialGroupCategoryRefId).ToListAsync();
                }

                if (taxTemplates.Count() == 0 || taxTemplates == null)
                {
                    // If not specific for mateial Group Category then need to  Find Any Sepcific Tax For Material Group id
                    taxTemplates = await _salestaxtemplatemappingRepo.GetAll().Where(t => t.MaterialGroupRefId == mat.MaterialGroupRefId).ToListAsync();
                }

                if (taxTemplates.Count() == 0 || taxTemplates == null)
                {
                    // If not specific for mateial Group then need to  Find Any Sepcific Tax For Location
                    taxTemplates = await _salestaxtemplatemappingRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId).ToListAsync();
                }

                if (taxTemplates.Count() == 0 || taxTemplates == null)
                {
                    // If not specific for Location then need to  Find Any Sepcific Tax For Organization
                    taxTemplates = await _salestaxtemplatemappingRepo.GetAll().Where(t => t.CompanyRefId == companyRefid).ToListAsync();
                }

                if (taxTemplates.Count() == 0 || taxTemplates == null)
                {
                    // If not specific for all above need to find the default any
                    taxTemplates = await _salestaxtemplatemappingRepo.GetAll().Where(t => t.LocationRefId == null && t.CompanyRefId == null && t.MaterialGroupCategoryRefId == null && t.MaterialRefId == null).ToListAsync();
                }

                List<ApplicableTaxesForMaterial> applicabletaxes = new List<ApplicableTaxesForMaterial>();

                if (taxTemplates.Count() > 0)
                {
                    foreach (var taxtemplate in taxTemplates)
                    {
                        var tax = _salestaxRepo.Single(t => t.Id == taxtemplate.SalesTaxRefId);
                        taxtoAdded.Add(new TaxForMaterial
                        {
                            MaterialRefId = mat.MaterialRefId,
                            TaxName = tax.TaxName,
                            TaxRefId = taxtemplate.SalesTaxRefId,
                            TaxRate = tax.Percentage,
                            TaxValue = Math.Round((mat.TotalAmount * tax.Percentage / 100), 2),
                            SortOrder = tax.SortOrder,
                            Rounding = tax.Rounding,
                            TaxCalculationMethod = tax.TaxCalculationMethod,
                        }
                        );
                    }


                    List<SalesTax> taxesforMaterial = new List<SalesTax>();
                    if (poTaxDetail.Count > 0)
                    {
                        int[] taxids = poTaxDetail.Where(t => t.SoRefId == poMaster.Id && t.MaterialRefId == mat.MaterialRefId).Select(t => t.SalesTaxRefId).ToArray();
                        taxesforMaterial = await _salestaxRepo.GetAll().Where(t => taxids.Contains(t.Id)).ToListAsync();
                    }
                    else if (inwardMaster != null)
                    {
                        int[] taxids = taxtoAdded.Where(t => t.MaterialRefId == mat.MaterialRefId).Select(t => t.TaxRefId).ToArray();
                        taxesforMaterial = await _salestaxRepo.GetAll().Where(t => taxids.Contains(t.Id)).ToListAsync();
                    }

                    foreach (var tax in taxesforMaterial.OrderBy(t => t.SortOrder))
                    {
                        matTaxAmount = matTaxAmount + Math.Round((mat.TotalAmount * tax.Percentage / 100), 2);
                        applicabletaxes.Add(new ApplicableTaxesForMaterial
                        {
                            TaxName = tax.TaxName,
                            TaxRefId = tax.Id,
                            TaxRate = tax.Percentage,
                            SortOrder = tax.SortOrder,
                            Rounding = tax.Rounding,
                            TaxCalculationMethod = tax.TaxCalculationMethod,
                        });
                    }

                }

                lstMaterial[loopcnt].SalesTaxAmount = matTaxAmount;
                lstMaterial[loopcnt].TaxForMaterial = taxtoAdded.OrderBy(t => t.SortOrder).ToList();
                lstMaterial[loopcnt].ApplicableTaxes = applicabletaxes;

                lstMaterial[loopcnt].NetAmount = lstMaterial[loopcnt].TotalAmount + lstMaterial[loopcnt].SalesTaxAmount;
                taxtoAdded = null;
                matTaxAmount = 0;
                loopcnt++;
            }

            return new GetDcMaterialDataDetail
            {
                SalesDcDetail = lstMaterial,
            };
        }
        public async Task<GetSalesInvoiceForEditOutput> GetSalesInvoiceForEdit(NullableIdInput input)
        {
            SalesInvoiceEditDto editDto;
            List<SalesInvoiceDetailViewDto> editDetailDto;
            List<SalesInvoiceDeliveryOrderLinkViewDto> editSalesInvoiceDCLink;
            if (input.Id.HasValue)
            {
                var hDto = await _salesinvoiceRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<SalesInvoiceEditDto>();

                if (editDto.IsDirectSalesInvoice == false)
                {
					editDetailDto = await (from invDet in _salesinvoicedetailRepo.GetAll()
					   .Where(a => a.SalesInvoiceRefId== input.Id.Value)
										   join mat in _materialRepo.GetAll() on invDet.MaterialRefId equals mat.Id
										   join unit in _unitRepo.GetAll() on invDet.UnitRefId equals unit.Id
										   join invdclnk in _salesInvoiceDeliveryOrderLinksRepo.GetAll()
										   on new { a = invDet.SalesInvoiceRefId, b = invDet.Sno }
										   equals new { a = invdclnk.SalesInvoiceRefId, b = invdclnk.Sno }
										   where mat.Id == invdclnk.MaterialRefId
										   join uiss in _unitRepo.GetAll()
										   on invDet.UnitRefId equals uiss.Id
										   select new SalesInvoiceDetailViewDto
										   {
											   Id = invDet.Id,
											   DiscountAmount = invDet.DiscountAmount,
											   DcTranId = invdclnk.DeliveryOrderRefId,
											   MaterialRefId = invDet.MaterialRefId,
											   MaterialRefName = mat.MaterialName,
											   SalesInvoiceRefId = invDet.SalesInvoiceRefId,
											   DiscountFlag = invDet.DiscountFlag,
											   DiscountPercentage = invDet.DiscountPercentage,
											   NetAmount = invDet.NetAmount,
											   Price = invDet.Price,
											   SalesTaxAmount = invDet.SalesTaxAmount,
											   TotalAmount = invDet.TotalAmount,
											   TotalQty = invDet.TotalQty,
											   Remarks = invDet.Remarks,
											   Sno = invDet.Sno,
											   Uom = unit.Name,
											   UnitRefId = invDet.UnitRefId,
											   UnitRefName = uiss.Name
										   }).OrderBy(t => t.Sno).ToListAsync();
                }
                else
                {
                    editDetailDto = await (from invDet in _salesinvoicedetailRepo.GetAll()
                                           .Where(a => a.SalesInvoiceRefId == input.Id.Value)
                                           join mat in _materialRepo.GetAll() on invDet.MaterialRefId equals mat.Id
                                           join unit in _unitRepo.GetAll() on mat.DefaultUnitId equals unit.Id
                                           join uiss in _unitRepo.GetAll() on invDet.UnitRefId equals uiss.Id
                                           select new SalesInvoiceDetailViewDto
                                           {
                                               Id = invDet.Id,
                                               DiscountAmount = invDet.DiscountAmount,
                                               DcTranId = 0,
                                               MaterialRefId = invDet.MaterialRefId,
                                               MaterialRefName = mat.MaterialName,
                                               SalesInvoiceRefId = invDet.SalesInvoiceRefId,
                                               DiscountFlag = invDet.DiscountFlag,
                                               DiscountPercentage = invDet.DiscountPercentage,
                                               NetAmount = invDet.NetAmount,
                                               Price = invDet.Price,
                                               SalesTaxAmount = invDet.SalesTaxAmount,
                                               TotalAmount = invDet.TotalAmount,
                                               TotalQty = invDet.TotalQty,
                                               Remarks = invDet.Remarks,
                                               Sno = invDet.Sno,
                                               Uom = unit.Name,
                                               UnitRefId = invDet.UnitRefId,
                                               UnitRefName = uiss.Name
                                           }).ToListAsync();
                }

                int loopCnt = 0;
                foreach (var det in editDetailDto)
                {
                    var taxlist = await (from potax in _salesinvoicetaxdetailRepo.GetAll().Where(pot => pot.SalesInvoiceRefId == input.Id.Value && pot.MaterialRefId == det.MaterialRefId)
                                         join tax in _salestaxRepo.GetAll()
                                         on potax.SalesTaxRefId equals tax.Id
                                         select new TaxForMaterial
                                         {
                                             TaxRefId = potax.SalesTaxRefId,
                                             TaxName = tax.TaxName,
                                             TaxRate = potax.SalesTaxRate,
                                             TaxValue = potax.SalesTaxValue,
                                             SortOrder = tax.SortOrder,
                                             Rounding = tax.Rounding
                                         }).ToListAsync();

                    editDetailDto[loopCnt].TaxForMaterial = taxlist;

                    List<ApplicableTaxesForMaterial> applicabletaxes = new List<ApplicableTaxesForMaterial>();

                    foreach (var tax in taxlist.OrderBy(t => t.SortOrder))
                    {
                        applicabletaxes.Add(new ApplicableTaxesForMaterial
                        {
                            TaxName = tax.TaxName,
                            TaxRefId = tax.TaxRefId,
                            TaxRate = tax.TaxRate,
                            SortOrder = tax.SortOrder,
                            Rounding = tax.Rounding,
                            TaxCalculationMethod = tax.TaxCalculationMethod,
                        });
                    }

                    editDetailDto[loopCnt].ApplicableTaxes = applicabletaxes;

                    loopCnt++;
                }

                editSalesInvoiceDCLink = await (from invDC in _salesinvoiceDeliveryOrderLinkRepo.GetAll().Where(a => a.SalesInvoiceRefId == input.Id.Value)
                                           select new SalesInvoiceDeliveryOrderLinkViewDto
                                           {
                                               Id = invDC.Id,
                                               SalesInvoiceRefId = invDC.SalesInvoiceRefId,
                                               DeliveryOrderRefId = invDC.DeliveryOrderRefId,
                                               MaterialRefId = invDC.MaterialRefId
                                           }).ToListAsync();

            }
            else
            {
                editDto = new SalesInvoiceEditDto();
                editDto.InvoiceDate = DateTime.Now.Date;
                editDto.AccountDate = DateTime.Now.Date;
                editDetailDto = new List<SalesInvoiceDetailViewDto>();
                editSalesInvoiceDCLink = new List<SalesInvoiceDeliveryOrderLinkViewDto>();
            }

            return new GetSalesInvoiceForEditOutput
            {
                SalesInvoice = editDto,
                SalesInvoiceDetail = editDetailDto,
                SalesInvoiceDeliveryOrderLink = editSalesInvoiceDCLink
            };
        }

        public async Task<IdInput> CreateOrUpdateSalesInvoice(CreateOrUpdateSalesInvoiceInput input)
        {
            IdInput ret = new IdInput();

            if (input.SalesInvoice.Id.HasValue)
            {
                //ret =  await UpdateSalesInvoice(input);
            }
            else
            {
                ret = await CreateSalesInvoice(input);
                await SetDcCompleteStatus(new IdInput { Id = input.SalesInvoice.CustomerRefId });

            }
            return ret;

        }

        public async Task DeleteSalesInvoice(IdInput input)
        {
            var rsInvMas = await _salesinvoiceRepo.FirstOrDefaultAsync(t => t.Id == input.Id);
            if (rsInvMas == null)
            {
                throw new UserFriendlyException(L("SalesInvoice") + " " + L("NotExist"));
            }

            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == rsInvMas.LocationRefId);

            if (loc.HouseTransactionDate.Value.Date != rsInvMas.AccountDate.Date)
            {
                throw new UserFriendlyException(L("SalesInvoiceDeleteError", loc.HouseTransactionDate.Value.ToString("yyyy-MMM-dd"), rsInvMas.AccountDate.Date.ToString("yyyy-MMM-dd"), rsInvMas.Id, rsInvMas.InvoiceNumber));
            }

            if (rsInvMas.IsDirectSalesInvoice == false)
            {
                throw new UserFriendlyException("CanDeleteOnlyDirectSalesInvoice");
            }

            var invoiceDetail = await _salesinvoicedetailRepo.GetAll().Where(t => t.SalesInvoiceRefId == input.Id).ToListAsync();
            foreach (var det in invoiceDetail)
            {
                MaterialLedgerDto ml = new MaterialLedgerDto();
                ml.LocationRefId = rsInvMas.LocationRefId;
                ml.MaterialRefId = det.MaterialRefId;
                ml.LedgerDate = rsInvMas.AccountDate;
                ml.Sales = -1 * det.TotalQty;
                ml.UnitId = det.UnitRefId;

                var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);

                await _salesinvoicetaxdetailRepo.DeleteAsync(u => u.SalesInvoiceRefId == input.Id && u.MaterialRefId == det.MaterialRefId);

                await _salesinvoicedetailRepo.DeleteAsync(det.Id);
            }

            //await _salesinvoiceDeliveryOrderLinkRepo.DeleteAsync(u => u.SalesInvoiceRefId == input.Id);

            await _salesinvoiceRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task<IdInput> CreateSalesInvoice(CreateOrUpdateSalesInvoiceInput input)
        {
            var dto = input.SalesInvoice.MapTo<SalesInvoice>();

            if (input.SalesInvoiceDetail == null || input.SalesInvoiceDetail.Count() == 0)
            {
                throw new UserFriendlyException(L("MinimumOneDetail"));
            }

            var retId = await _salesinvoiceRepo.InsertAndGetIdAsync(dto);


            foreach (SalesInvoiceDetailViewDto items in input.SalesInvoiceDetail)
            {
                SalesInvoiceDetail invdet = new SalesInvoiceDetail();

                if (items.DiscountAmount > 0)
                {
                    items.DiscountFlag = "P";
                    items.DiscountPercentage = (items.DiscountAmount / items.TotalAmount) * 100;
                }

                invdet.SalesInvoiceRefId = (int)dto.Id;
                invdet.Sno = items.Sno;
                invdet.MaterialRefId = items.MaterialRefId;
                invdet.TotalQty = items.TotalQty;
                invdet.Price = items.Price;
                invdet.TotalAmount = items.TotalAmount;
                invdet.DiscountFlag = items.DiscountFlag;
                invdet.DiscountPercentage = items.DiscountPercentage;
                invdet.DiscountAmount = items.DiscountAmount;
                invdet.SalesTaxAmount = items.SalesTaxAmount;
                invdet.NetAmount = items.NetAmount;
                invdet.Remarks = items.Remarks;
                invdet.UnitRefId = items.UnitRefId;

                var retId2 = await _salesinvoicedetailRepo.InsertAndGetIdAsync(invdet);

                foreach (var tax in items.TaxForMaterial)
                {
                    SalesInvoiceTaxDetail taxForPoDetail = new SalesInvoiceTaxDetail
                    {
                        SalesInvoiceRefId = (int)dto.Id,
                        Sno = items.Sno,
                        MaterialRefId = items.MaterialRefId,
                        SalesTaxRefId = tax.TaxRefId,
                        SalesTaxRate = tax.TaxRate,
                        SalesTaxValue = tax.TaxValue,
                    };
                    var retId3 = await _salesinvoicetaxdetailRepo.InsertAndGetIdAsync(taxForPoDetail);

                }
                if (input.SalesInvoice.IsDirectSalesInvoice == true)
                {
                    MaterialLedgerDto ml = new MaterialLedgerDto();
                    ml.LocationRefId = dto.LocationRefId;
                    ml.MaterialRefId = invdet.MaterialRefId;
                    ml.LedgerDate = dto.AccountDate.Date;
                    ml.Sales = invdet.TotalQty;
                    ml.UnitId = items.UnitRefId;
                    var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                    invdet.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;
                }

            }

            if (input.SalesInvoice.IsDirectSalesInvoice == false)
            {
                int previousDirectCreditDcId = 0;
                foreach (SalesInvoiceDeliveryOrderLinkViewDto itemsdc in input.SalesInvoiceDeliveryOrderLink)
                {
                    if (itemsdc.DeliveryOrderRefId == 0)
                    {
                        continue;
                    }
                    previousDirectCreditDcId = itemsdc.DeliveryOrderRefId;

                    SalesInvoiceDeliveryOrderLink idl = new SalesInvoiceDeliveryOrderLink();
                    idl.SalesInvoiceRefId = (int)dto.Id;
                    idl.Sno = itemsdc.Sno;
                    idl.DeliveryOrderRefId = itemsdc.DeliveryOrderRefId;
                    idl.MaterialRefId = itemsdc.MaterialRefId;

                    var retId3 = await _salesinvoiceDeliveryOrderLinkRepo.InsertAndGetIdAsync(idl);

                    //  Updating Inward Direct Credit Detail Converted Qty and Status

                    var getDcDetoDetail =_salesdeliverorderDetailRepo.GetAll().Where(a => a.MaterialRefId == itemsdc.MaterialRefId && a.DeliveryRefId == itemsdc.DeliveryOrderRefId).ToList();
                    if (getDcDetoDetail == null || getDcDetoDetail.Count() == 0)
                    {
                        throw new UserFriendlyException(L("ProblemInInwardDCConversion"));
                    }

                    var editDCDetail = await _salesdeliverorderDetailRepo.GetAsync(getDcDetoDetail[0].Id);

                    if (editDCDetail == null)
                    {
                        throw new UserFriendlyException(L("ProblemInInwardDCConversion"));
                    }

                    editDCDetail.BillConvertedQty = editDCDetail.BillConvertedQty + itemsdc.MaterialConvertedQty;

                    if (editDCDetail.BillConvertedQty > editDCDetail.DeliveryQty)
                    {
                        //@@Pending     How to Rollback Here if 
                        throw new UserFriendlyException(L("ProblemInInwardDCConversion"));
                    }

                    await _salesdeliverorderDetailRepo.InsertOrUpdateAsync(editDCDetail);

                }


                //  Complete the Inward Direct Credit Dc Status
                foreach (SalesInvoiceDeliveryOrderLinkViewDto itemsdc in input.SalesInvoiceDeliveryOrderLink)
                {
                    if (itemsdc.DeliveryOrderRefId == 0)
                    {
                        continue;
                    }
                    previousDirectCreditDcId = itemsdc.DeliveryOrderRefId;

                    var CompletedDC = (from dcMas in _salesdeliverorderDetailRepo.GetAll().Where(a => a.DeliveryRefId == itemsdc.DeliveryOrderRefId && a.DeliveryQty != a.BillConvertedQty) select dcMas).ToList();

                    if (CompletedDC.Count == 0)
                    {
                        var editDCMaster = _salesdeliverorderRepo.GetAll().Where(a => a.Id == itemsdc.DeliveryOrderRefId).ToList();
                        if (editDCMaster != null)
                        {
                            var editInwMasterDto = await _salesdeliverorderRepo.GetAsync(editDCMaster[0].Id);

                            editInwMasterDto.IsDcCompleted = true;

                            await _salesdeliverorderRepo.InsertOrUpdateAsync(editInwMasterDto);
                        }
                    }

                }
            }

            CheckErrors(await _salesinvoiceManager.CreateSync(dto));

            return new IdInput
            {
                Id = retId
            };
        }

        public async Task<ListResultOutput<SalesInvoiceListDto>> GetIds()
        {
            var lstSalesInvoice = await _salesinvoiceRepo.GetAll().ToListAsync();
            return new ListResultOutput<SalesInvoiceListDto>(lstSalesInvoice.MapTo<List<SalesInvoiceListDto>>());
        }

        public virtual async Task SetDcCompleteStatus(IdInput input)
        {
            var lstCompleted = (from inwmas in _salesdeliverorderRepo.GetAll().Where(im => im.CustomerRefId == input.Id && im.IsDcCompleted == false)
                                join inwdet in _salesdeliverorderDetailRepo.GetAll().Where(id => id.BillConvertedQty != id.DeliveryQty)
                                on inwmas.Id equals inwdet.DeliveryRefId into g
                                where g.Count() == 0
                                select new
                                {
                                    DcId = inwmas.Id,
                                    PendingCount = g.Count()
                                }).ToList();

            foreach (var l in lstCompleted)
            {

                var editMas = await _salesdeliverorderRepo.GetAsync(l.DcId);
                editMas.IsDcCompleted = true;

                await _salesdeliverorderRepo.InsertOrUpdateAsync(editMas);
            }

        }

        public async Task<List<SalesInvoiceAnalysisDto>> GetSalesAnalysis(InputSalesInvoiceAnalysis input)
        {
            var invoiceMaster = _salesinvoiceRepo.GetAll();

            if (input.CustomerRefId != 0)
            {
                invoiceMaster = invoiceMaster.Where(t => t.CustomerRefId == input.CustomerRefId);
            }

            if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
            {
                invoiceMaster =
                    invoiceMaster.Where(
                        a => DbFunctions.TruncateTime(a.InvoiceDate) >= DbFunctions.TruncateTime(input.StartDate)
                             && DbFunctions.TruncateTime(a.InvoiceDate) <= DbFunctions.TruncateTime(input.EndDate));
            }

            if (input.LocationRefId != 0)
            {
                invoiceMaster = invoiceMaster.Where(t => t.LocationRefId == input.LocationRefId);
            }

            bool MaterialExists = false;

            if (input.MaterialRefId != 0)
            {
                MaterialExists = true;
            }

            if (input.InvoiceNumber != null)
            {
                if (input.ExactSearchFlag == false)
                    invoiceMaster = invoiceMaster.Where(t => t.InvoiceNumber.Contains(input.InvoiceNumber));
                else
                    invoiceMaster = invoiceMaster.Where(t => t.InvoiceNumber.Equals(input.InvoiceNumber));
            }

            var allItems = (from invmas in invoiceMaster
                            join invdet in _salesinvoicedetailRepo.GetAll().WhereIf(MaterialExists, p => p.MaterialRefId == input.MaterialRefId)
                            on invmas.Id equals invdet.SalesInvoiceRefId
                            join sup in _customerRepo.GetAll()
                            on invmas.CustomerRefId equals sup.Id
                            join mat in _materialRepo.GetAll()
                            on invdet.MaterialRefId equals mat.Id
                            join unit in _unitRepo.GetAll()
                            on mat.DefaultUnitId equals unit.Id
                            join uiss in _unitRepo.GetAll() on invdet.UnitRefId equals uiss.Id
                            select new SalesInvoiceAnalysisDto
                            {
                                InvoiceDate = invmas.InvoiceDate,
                                InvoiceNumber = invmas.InvoiceNumber,
                                SalesInvoiceRefId = invmas.Id,
                                CustomerRefId = invmas.CustomerRefId,
                                CustomerRefName = sup.CustomerName,
                                MaterialRefId = invdet.MaterialRefId,
                                MaterialRefName = mat.MaterialName,
                                TotalQty = invdet.TotalQty,
                                Price = invdet.Price,
                                DiscountAmount = invdet.DiscountAmount,
                                DiscountFlag = invdet.DiscountFlag,
                                DiscountPercentage = invdet.DiscountPercentage,
                                TotalAmount = invdet.TotalAmount,
                                SalesTaxAmount = invdet.SalesTaxAmount,
                                NetAmount = invdet.NetAmount,
                                Remarks = invdet.Remarks,
                                Uom = unit.Name,
                                DefaultUnitId = mat.DefaultUnitId,
                                DefaultUnitName = unit.Name,
                                UnitRefId = invdet.UnitRefId,
                                UnitRefName = uiss.Name,
                            }).ToList();

            List<SalesInvoiceAnalysisDto> sortMenuItems;

            sortMenuItems = allItems.ToList();

            var rsUnitConversion = await _unitConversionRepo.GetAllListAsync();

            foreach (var lst in sortMenuItems)
            {
                decimal conversionFactor;
                decimal PricePerUom;

                if (lst.DefaultUnitId == lst.UnitRefId)
                {
                    conversionFactor = 1;
                }
                else
                {
                    var unitConversion = rsUnitConversion.FirstOrDefault(t => t.BaseUnitId == lst.DefaultUnitId && t.RefUnitId == lst.UnitRefId);
                    if (unitConversion != null)
                    {
                        conversionFactor = unitConversion.Conversion;
                    }
                    else
                    {
                        unitConversion = rsUnitConversion.FirstOrDefault(t => t.BaseUnitId == lst.UnitRefId && t.RefUnitId == lst.DefaultUnitId);
                        if (unitConversion == null)
                        {
                            var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == lst.UnitRefId);
                            var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == lst.DefaultUnitId);

                            throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
                        }
                        conversionFactor = 1 / unitConversion.Conversion;
                    }
                }
                PricePerUom = lst.Price * conversionFactor;

                lst.PriceForDefaultUnitWithTaxUOM = PricePerUom.ToString("N") + " / " + lst.DefaultUnitName;

            }

            var allListDtos = sortMenuItems.MapTo<List<SalesInvoiceAnalysisDto>>();

            var allItemCount = allItems.Count();

            return allListDtos;
        }


        public async Task<List<SalesInvoiceCategoryAnalysisDto>> GetSalesAnalysisCategoryWise(InputSalesInvoiceAnalysisForCategoryWise input)
        {
            var invoiceMaster = _salesinvoiceRepo.GetAll();

            if (input.CustomerRefId != 0)
            {
                invoiceMaster = invoiceMaster.Where(t => t.CustomerRefId == input.CustomerRefId);
            }

            if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
            {
                invoiceMaster =
                    invoiceMaster.Where(
                        a => DbFunctions.TruncateTime(a.InvoiceDate) >= DbFunctions.TruncateTime(input.StartDate)
                             && DbFunctions.TruncateTime(a.InvoiceDate) <= DbFunctions.TruncateTime(input.EndDate));
            }

            if (input.LocationRefId != 0)
            {
                invoiceMaster = invoiceMaster.Where(t => t.LocationRefId == input.LocationRefId);
            }

            bool CategoryExist = false;

            if (input.CategoryRefId != 0)
            {
                CategoryExist = true;
            }

            var allItems = (from invmas in invoiceMaster
                            join invdet in _salesinvoicedetailRepo.GetAll()
                            on invmas.Id equals invdet.SalesInvoiceRefId
                            join sup in _customerRepo.GetAll()
                            on invmas.CustomerRefId equals sup.Id
                            join mat in _materialRepo.GetAll().WhereIf(CategoryExist, p => p.MaterialGroupCategoryRefId == input.CategoryRefId)
                            on invdet.MaterialRefId equals mat.Id
                            join unit in _unitRepo.GetAll()
                            on mat.DefaultUnitId equals unit.Id
                            select new SalesInvoiceAnalysisDto
                            {
                                InvoiceDate = invmas.InvoiceDate,
                                InvoiceNumber = invmas.InvoiceNumber,
                                SalesInvoiceRefId = invmas.Id,
                                CustomerRefId = invmas.CustomerRefId,
                                CustomerRefName = sup.CustomerName,
                                MaterialRefId = invdet.MaterialRefId,
                                MaterialRefName = mat.MaterialName,
                                MaterialGroupCategoryRefId = mat.MaterialGroupCategoryRefId,
                                TotalQty = invdet.TotalQty,
                                Price = invdet.Price,
                                DiscountAmount = invdet.DiscountAmount,
                                DiscountFlag = invdet.DiscountFlag,
                                DiscountPercentage = invdet.DiscountPercentage,
                                TotalAmount = invdet.TotalAmount,
                                SalesTaxAmount = invdet.SalesTaxAmount,
                                NetAmount = invdet.NetAmount,
                                Remarks = invdet.Remarks,
                                Uom = unit.Name
                            }).ToList();

            var allItemsCategory = (from matSales in allItems
                                    join cat in _materialGroupCategoryRepo.GetAll().WhereIf(CategoryExist, p => p.Id == input.CategoryRefId)
                                    on matSales.MaterialGroupCategoryRefId equals cat.Id
                                    group matSales by new { matSales.MaterialGroupCategoryRefId, cat.MaterialGroupCategoryName } into g
                                    select new SalesInvoiceCategoryAnalysisDto
                                    {
                                        CategoryRefId = g.Key.MaterialGroupCategoryRefId,
                                        CategoryRefName = g.Key.MaterialGroupCategoryName,
                                        TotalAmount = g.Sum(t => t.TotalAmount),
                                        SalesTaxAmount = g.Sum(t => t.SalesTaxAmount),
                                        TotalQty = g.Sum(t => t.TotalQty),
                                        NetAmount = g.Sum(t => t.NetAmount)
                                    });



            List<SalesInvoiceCategoryAnalysisDto> sortMenuItems;

            sortMenuItems = allItemsCategory.ToList();

            var allListDtos = sortMenuItems.MapTo<List<SalesInvoiceCategoryAnalysisDto>>();

            var allItemCount = allItems.Count();

            return allListDtos;
        }

        public async Task<List<SalesDeliveryOrderDetailForParticularDc>> GetMaterialForInwardDc(IdInput ninput)
        {
            var allItems = (from mat in _materialRepo.GetAll()
                            join inwDet in _salesdeliverorderDetailRepo.GetAll()
                            on mat.Id equals inwDet.MaterialRefId
                            where inwDet.DeliveryRefId.ToString().Contains(ninput.Id.ToString()) && inwDet.DeliveryQty != inwDet.BillConvertedQty
                            select new SalesDeliveryOrderDetailForParticularDc
                            {
                                MaterialRefId = inwDet.MaterialRefId,
                                MaterialRefName = mat.MaterialName,
                                DeliveryQty = inwDet.DeliveryQty,
                                BillConvertedQty = inwDet.BillConvertedQty,
                                MaterialReceivedStatus = inwDet.MaterialReceivedStatus,
                                PendingQty = inwDet.DeliveryQty - inwDet.BillConvertedQty
                            });

            var matRecipeList = await allItems.ToListAsync();

            //ListResultOutput<DeliveryOrderDetailForParticularDc> allDtos = matRecipeList.MapTo<ListResultOutput<DeliveryOrderDetailForParticularDc>>();

            return matRecipeList;

            //            new ListResultOutput<DeliveryOrderDetailForParticularDc>(
            //              matRecipeList.Select(e => new ComboboxItemDto(e.Id.ToString(), e.MaterialName)).ToList());


        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetDcTranId(InputDateRangeWithCustomerId input)
        {
            var allItems = await _salesdeliverorderAppService.GetSalesDoTranid(input);
            return
                new ListResultOutput<ComboboxItemDto>(
                    allItems.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.DcNumberGivenByCustomer.ToString())).ToList());
        }

        private void AlterDate(GetSalesInvoiceInput input)
        {
            if (input.StartDate.Equals(DateTime.MinValue))
            {
                input.StartDate = DateTime.Now;
            }
            if (input.EndDate.Equals(DateTime.MinValue))
            {
                input.EndDate = DateTime.Now;
            }

            var totalHours = input.EndDate.Value.Subtract(input.StartDate.Value).TotalHours;
            if (totalHours <= 24 && !input.EndDate.Value.Day.Equals(input.StartDate.Value.Day))
            {
                input.StartDate = input.StartDate;
                input.EndDate = input.StartDate;
            }
        }

        public async Task<PagedResultOutput<SalesInvoiceListDto>> GetSalesInvoiceSearch(GetSalesInvoiceInput input)
        {
            AlterDate(input);

            var allItems = (from inv in _salesinvoiceRepo.GetAll()
                            join sup in _customerRepo.GetAll().WhereIf(!input.Filter.IsNullOrEmpty(), p => p.CustomerName.Contains(input.Filter))
                            on inv.CustomerRefId equals sup.Id
                            select new SalesInvoiceListDto
                            {
                                InvoiceAmount = inv.InvoiceAmount,
                                InvoiceDate = inv.InvoiceDate,
                                AccountDate = inv.AccountDate,
                                InvoiceNumber = inv.InvoiceNumber,
                                CustomerRefId = inv.CustomerRefId,
                                CustomerRefName = sup.CustomerName,
                                TotalShipmentCharges = inv.TotalShipmentCharges,
                                Id = inv.Id,
                                IsDirectSalesInvoice = inv.IsDirectSalesInvoice,
                                CreationTime = inv.CreationTime
                            });

            if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
            {
                allItems =
                    allItems.Where(
                        a => DbFunctions.TruncateTime(a.InvoiceDate) >= DbFunctions.TruncateTime(input.StartDate)
                             && DbFunctions.TruncateTime(a.InvoiceDate) <= DbFunctions.TruncateTime(input.EndDate));
            }

            if (input.CustomerRefId > 0)
            {
                allItems = allItems.Where(a => a.CustomerRefId == input.CustomerRefId);
            }

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<SalesInvoiceListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<SalesInvoiceListDto>(
                allItemCount,
                allListDtos
                );

        }

        public async Task<List<OutputMaterialSalesDto>> GetMaterialSales(InputMaterialSalesDto input)
        {
            var editDetailDto = (from invMas in _salesinvoiceRepo.GetAll()  // .Where(t=>t.SalesInvoiceDate <= input.EffectiveDate)
                                 join invDet in _salesinvoicedetailRepo.GetAll().Where(t => t.MaterialRefId == input.MaterialRefId)
                                 on invMas.Id equals invDet.SalesInvoiceRefId
                                 join mat in _materialRepo.GetAll() on invDet.MaterialRefId equals mat.Id
                                 join unit in _unitRepo.GetAll() on mat.DefaultUnitId equals unit.Id
                                 join sup in _customerRepo.GetAll() on invMas.CustomerRefId equals sup.Id
                                 select new OutputMaterialSalesDto
                                 {
                                     MaterialRefId = invDet.MaterialRefId,
                                     MaterialRefName = mat.MaterialName,
                                     CustomerRefId = sup.Id,
                                     CustomerRefName = sup.CustomerName,
                                     SalesDate = invMas.InvoiceDate,
                                     QuantityPurchased = invDet.TotalQty,
                                     Price = invDet.Price,
                                     Tax = Math.Round(invDet.SalesTaxAmount / invDet.TotalQty, 2),
                                     PriceWithTax = invDet.Price + Math.Round(invDet.SalesTaxAmount / invDet.TotalQty, 2),
                                 });

            var output = await editDetailDto.OrderByDescending(t => t.SalesDate).Take(input.NumberOfLastSalesDetail).ToListAsync();

            return output;

        }



        public async Task<PrintOutIn40Cols> GetPrintDataPixel10(IdInput input)
        {
            PrintOutIn40Cols output = new PrintOutIn40Cols();

            SalesInvoiceEditDto editDto;
            List<SalesInvoiceDetailViewDto> editDetailDto;
            List<SalesInvoiceDeliveryOrderLinkViewDto> editSalesInvoiceDCLink;

            var hDto = await _salesinvoiceRepo.GetAsync(input.Id);
            editDto = hDto.MapTo<SalesInvoiceEditDto>();

            if (editDto.IsDirectSalesInvoice == false)
            {
                editDetailDto = await (from invDet in _salesinvoicedetailRepo.GetAll()
                                        .Where(a => a.SalesInvoiceRefId == input.Id)
                                       join mat in _materialRepo.GetAll() on invDet.MaterialRefId equals mat.Id
                                       join uiss in _unitRepo.GetAll() on invDet.UnitRefId equals uiss.Id
                                       join invdclnk in _salesinvoiceDeliveryOrderLinkRepo.GetAll()
                                       on new { a = invDet.SalesInvoiceRefId, b = invDet.Sno }
                                       equals new { a = invdclnk.SalesInvoiceRefId, b = invdclnk.Sno }
                                       where mat.Id == invdclnk.MaterialRefId
                                       select new SalesInvoiceDetailViewDto
                                       {
                                           Id = invDet.Id,
                                           DiscountAmount = invDet.DiscountAmount,
                                           DcTranId = invdclnk.DeliveryOrderRefId,
                                           MaterialRefId = invDet.MaterialRefId,
                                           MaterialRefName = mat.MaterialName,
                                           SalesInvoiceRefId = invDet.SalesInvoiceRefId,
                                           DiscountFlag = invDet.DiscountFlag,
                                           DiscountPercentage = invDet.DiscountPercentage,
                                           NetAmount = invDet.NetAmount,
                                           Price = invDet.Price,
                                           SalesTaxAmount = invDet.SalesTaxAmount,
                                           TotalAmount = invDet.TotalAmount,
                                           TotalQty = invDet.TotalQty,
                                           Remarks = invDet.Remarks,
                                           Sno = invDet.Sno,
                                           UnitRefName = uiss.Name,
                                           UnitRefId = invDet.UnitRefId
                                       }).ToListAsync();
            }
            else
            {
                editDetailDto = await (from invDet in _salesinvoicedetailRepo.GetAll()
                                        .Where(a => a.SalesInvoiceRefId == input.Id)
                                       join mat in _materialRepo.GetAll() on invDet.MaterialRefId equals mat.Id
                                       join uiss in _unitRepo.GetAll() on invDet.UnitRefId equals uiss.Id
                                       select new SalesInvoiceDetailViewDto
                                       {
                                           Id = invDet.Id,
                                           DiscountAmount = invDet.DiscountAmount,
                                           DcTranId = 0,
                                           MaterialRefId = invDet.MaterialRefId,
                                           MaterialRefName = mat.MaterialName,
                                           SalesInvoiceRefId = invDet.SalesInvoiceRefId,
                                           DiscountFlag = invDet.DiscountFlag,
                                           DiscountPercentage = invDet.DiscountPercentage,
                                           NetAmount = invDet.NetAmount,
                                           Price = invDet.Price,
                                           SalesTaxAmount = invDet.SalesTaxAmount,
                                           TotalAmount = invDet.TotalAmount,
                                           TotalQty = invDet.TotalQty,
                                           Remarks = invDet.Remarks,
                                           Sno = invDet.Sno,
                                           Uom = uiss.Name,
                                           UnitRefName = uiss.Name,
                                           UnitRefId = invDet.UnitRefId
                                       }).ToListAsync();
            }

            decimal SubTotal = Math.Round(editDetailDto.Sum(t => t.TotalAmount), 2);
            decimal TaxAmount = Math.Round(editDetailDto.Sum(t => t.SalesTaxAmount), 2);
            decimal NetAmount = Math.Round(hDto.InvoiceAmount, 2);
            hDto.RoundedAmount = Math.Round(hDto.RoundedAmount, 2);
            hDto.TotalShipmentCharges = Math.Round(hDto.TotalShipmentCharges, 2);


            int loopCnt = 0;
            foreach (var det in editDetailDto)
            {
                var taxlist = await (from potax in _salesinvoicetaxdetailRepo.GetAll().Where(pot => pot.SalesInvoiceRefId == input.Id && pot.MaterialRefId == det.MaterialRefId)
                                     join tax in _salestaxRepo.GetAll()
                                     on potax.SalesTaxRefId equals tax.Id
                                     select new TaxForMaterial
                                     {
                                         TaxRefId = potax.SalesTaxRefId,
                                         TaxName = tax.TaxName,
                                         TaxRate = potax.SalesTaxRate,
                                         TaxValue = potax.SalesTaxValue,
                                         SortOrder = tax.SortOrder,
                                         Rounding = tax.Rounding
                                     }).ToListAsync();

                editDetailDto[loopCnt].TaxForMaterial = taxlist;

                List<ApplicableTaxesForMaterial> applicabletaxes = new List<ApplicableTaxesForMaterial>();

                foreach (var tax in taxlist.OrderBy(t => t.SortOrder))
                {
                    applicabletaxes.Add(new ApplicableTaxesForMaterial
                    {
                        TaxName = tax.TaxName,
                        TaxRefId = tax.TaxRefId,
                        TaxRate = tax.TaxRate,
                        SortOrder = tax.SortOrder,
                        Rounding = tax.Rounding,
                        TaxCalculationMethod = tax.TaxCalculationMethod,
                    });
                }

                editDetailDto[loopCnt].ApplicableTaxes = applicabletaxes;

                loopCnt++;
            }

            editSalesInvoiceDCLink = await (from invDC in _salesinvoiceDeliveryOrderLinkRepo.GetAll().Where(a => a.SalesInvoiceRefId == input.Id)
                                       select new SalesInvoiceDeliveryOrderLinkViewDto
                                       {
                                           Id = invDC.Id,
                                           SalesInvoiceRefId = invDC.SalesInvoiceRefId,
                                           DeliveryOrderRefId = invDC.DeliveryOrderRefId,
                                           MaterialRefId = invDC.MaterialRefId
                                       }).ToListAsync();


            var customer = _customerRepo.FirstOrDefault(t => t.Id == hDto.CustomerRefId);
            var billingLocation = _locationRepo.FirstOrDefault(t => t.Id == hDto.LocationRefId);

            TextFileWriter fp = new TextFileWriter();
            string subPath = "~/Report/";
            bool exists = System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(subPath));


            if (!exists)
                System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(subPath));

            string fileName = System.Web.HttpContext.Current.Server.MapPath("~/Report/" + AbpSession.UserId + "_InvReport.txt");
            int PageLenth = 45;


            fp.FileOpen(fileName, "O");
            if (billingLocation.Name != null)
            {
                fp.PrintTextLine(fp.PrintFormatCenter(billingLocation.Name, PageLenth));
            }

            if (billingLocation.Address1 != null)
            {
                fp.PrintTextLine(fp.PrintFormatCenter(billingLocation.Address1, PageLenth));
            }

            if (billingLocation.Address2 != null)
            {
                fp.PrintTextLine(fp.PrintFormatCenter(billingLocation.Address2, PageLenth));
            }

            if (billingLocation.Address3 != null)
            {
                string addwithCity = billingLocation.Address3;
                if (billingLocation.City != null)
                    addwithCity = addwithCity + ", " + billingLocation.City;
                fp.PrintTextLine(fp.PrintFormatCenter(addwithCity, PageLenth));
            }

            fp.PrintTextLine(fp.PrintFormatCenter(L("SalesInvoice"), PageLenth));

            fp.PrintText(fp.PrintFormatLeft(L("Inv#") + ": " + hDto.InvoiceNumber, 10));
            fp.PrintText(" " + L("Dt") + ": " + hDto.InvoiceDate.ToString("dd/MM/yyyy"));
            //if (hDto.AccountDate!=null)
            //    fp.PrintText(" " + L("Acc Dt") + ": " + hDto.AccountDate.ToString("yy-MMM-dd"));
            fp.PrintTextLine("");

            fp.PrintSepLine('-', PageLenth);

            string retText = System.IO.File.ReadAllText(fileName);

            retText = retText.Replace("\r\n", "<br/>");

            retText = retText.Replace(" ", "&nbsp;");

            output.HeaderText = retText;
            output.BoldHeaderText = "<STRONG>" + retText + "</STRONG>";

            fp.FileOpen(fileName, "O");

            fp.PrintText(L("Customer") + " : ");
            if (customer.CustomerName.Length > 0)
                fp.PrintTextLine(customer.CustomerName);
            fp.PrintSepLine('-', PageLenth);
            fp.PrintText(fp.PrintFormatLeft(L("Material"), 24));
            fp.PrintText(fp.PrintFormatRight(L("Qty"), 8));
            fp.PrintText(fp.PrintFormatRight(L("UOM"), 4));
            fp.PrintText(fp.PrintFormatRight(L("Price"), 9));
            fp.PrintTextLine("");
            fp.PrintSepLine('-', PageLenth);
            int loopIndex = 1;

            foreach (var det in editDetailDto)
            {
                fp.PrintText(fp.PrintFormatLeft(det.MaterialRefName, 24));
                decimal qty = Math.Round(det.TotalQty, 2);
                string qtyinstring = string.Concat(new string(' ', 8 - qty.ToString().Length), qty.ToString());

                string unitRefName = det.UnitRefName;
                if (det.UnitRefName.Length > 2)
                    unitRefName = det.UnitRefName.Left(2);

                qtyinstring = qtyinstring + " " + unitRefName;

                fp.PrintText(fp.PrintFormatLeft(qtyinstring, 12));

                decimal price = Math.Round(det.TotalAmount, 2);
                string priceinstring = string.Concat(new string(' ', 8 - price.ToString().Length), price.ToString());
                fp.PrintText(fp.PrintFormatRight(priceinstring, 9));

                fp.PrintTextLine("");
                loopIndex++;
            }

            fp.PrintTextLine(" ");
            fp.PrintSepLine('-', PageLenth);

            fp.PrintText(fp.PrintFormatLeft(L("SubTotal"), 30));
            string subtotalinstring = string.Concat(new string(' ', 15 - SubTotal.ToString().Length), SubTotal.ToString());
            fp.PrintTextLine(fp.PrintFormatRight(subtotalinstring, 15));

            if (TaxAmount > 0)
            {
                fp.PrintText(fp.PrintFormatLeft(L("Tax"), 30));
                string taxamountinstring = string.Concat(new string(' ', 15 - TaxAmount.ToString().Length), TaxAmount.ToString());
                fp.PrintTextLine(fp.PrintFormatRight(taxamountinstring, 15));
            }

            if (hDto.TotalDiscountAmount > 0)
            {
                fp.PrintText(fp.PrintFormatLeft(L("Discount"), 30));
                string discountinstring = string.Concat(new string(' ', 15 - hDto.TotalDiscountAmount.ToString().Length), hDto.TotalDiscountAmount.ToString());
                fp.PrintTextLine(fp.PrintFormatRight(discountinstring, 15));
            }


            if (hDto.TotalShipmentCharges > 0)
            {
                fp.PrintText(fp.PrintFormatLeft(L("Extra"), 30));
                string shipmentinstring = string.Concat(new string(' ', 15 - hDto.TotalShipmentCharges.ToString().Length), hDto.TotalShipmentCharges.ToString());
                fp.PrintTextLine(fp.PrintFormatRight(shipmentinstring, 15));
            }

            if (hDto.RoundedAmount != 0)
            {
                fp.PrintText(fp.PrintFormatLeft(L("RoundedOff"), 30));
                string roundamtinstring = string.Concat(new string(' ', 15 - hDto.RoundedAmount.ToString().Length), hDto.RoundedAmount.ToString());
                fp.PrintTextLine(fp.PrintFormatRight(roundamtinstring, 15));
            }

            fp.PrintText(fp.PrintFormatLeft(L("Total"), 30));
            string totalamtinstring = string.Concat(new string(' ', 15 - NetAmount.ToString().Length), NetAmount.ToString());
            fp.PrintTextLine(fp.PrintFormatRight(totalamtinstring, 15));

            fp.PrintSepLine('-', PageLenth);
            //fp.PrintTextLine(fp.PrintFormatCenter("Print @ " + DateTime.Now.ToString("yy-MMM-dd HH:mm:ss"), PageLenth));
            //fp.PrintSepLine('-', PageLenth);
            //fp.PrintTextLine(" ");

            retText = System.IO.File.ReadAllText(fileName);

            retText = retText.Replace("\r\n", "<br/>");

            //retText = retText.Replace(" ", "&nbsp;");
            output.BodyText = retText;
            output.BoldBodyText = "<STRONG>" + retText + "</STRONG>";

            return output;

        }

        public async Task<PrintOutIn40Cols> GetPrintData(IdInput input)
        {
            PrintOutIn40Cols output = new PrintOutIn40Cols();

            SalesInvoiceEditDto editDto;
            List<SalesInvoiceDetailViewDto> editDetailDto;
            List<SalesInvoiceDeliveryOrderLinkViewDto> editSalesInvoiceDCLink;

            var hDto = await _salesinvoiceRepo.GetAsync(input.Id);
            editDto = hDto.MapTo<SalesInvoiceEditDto>();

            if (editDto.IsDirectSalesInvoice == false)
            {
                editDetailDto = await (from invDet in _salesinvoicedetailRepo.GetAll()
                                        .Where(a => a.SalesInvoiceRefId == input.Id)
                                       join mat in _materialRepo.GetAll() on invDet.MaterialRefId equals mat.Id
                                       join uiss in _unitRepo.GetAll() on invDet.UnitRefId equals uiss.Id
                                       join invdclnk in _salesinvoiceDeliveryOrderLinkRepo.GetAll()
                                       on new { a = invDet.SalesInvoiceRefId, b = invDet.Sno }
                                       equals new { a = invdclnk.SalesInvoiceRefId, b = invdclnk.Sno }
                                       where mat.Id == invdclnk.MaterialRefId
                                       select new SalesInvoiceDetailViewDto
                                       {
                                           Id = invDet.Id,
                                           DiscountAmount = invDet.DiscountAmount,
                                           DcTranId = invdclnk.DeliveryOrderRefId,
                                           MaterialRefId = invDet.MaterialRefId,
                                           MaterialRefName = mat.MaterialName,
                                           SalesInvoiceRefId = invDet.SalesInvoiceRefId,
                                           DiscountFlag = invDet.DiscountFlag,
                                           DiscountPercentage = invDet.DiscountPercentage,
                                           NetAmount = invDet.NetAmount,
                                           Price = invDet.Price,
                                           SalesTaxAmount = invDet.SalesTaxAmount,
                                           TotalAmount = invDet.TotalAmount,
                                           TotalQty = invDet.TotalQty,
                                           Remarks = invDet.Remarks,
                                           Sno = invDet.Sno,
                                           Uom = uiss.Name,
                                           UnitRefId = invDet.UnitRefId,
                                           UnitRefName = uiss.Name
                                       }).ToListAsync();
            }
            else
            {
                editDetailDto = await (from invDet in _salesinvoicedetailRepo.GetAll()
                                        .Where(a => a.SalesInvoiceRefId == input.Id)
                                       join mat in _materialRepo.GetAll() on invDet.MaterialRefId equals mat.Id
                                       join uiss in _unitRepo.GetAll() on invDet.UnitRefId equals uiss.Id
                                       select new SalesInvoiceDetailViewDto
                                       {
                                           Id = invDet.Id,
                                           DiscountAmount = invDet.DiscountAmount,
                                           DcTranId = 0,
                                           MaterialRefId = invDet.MaterialRefId,
                                           MaterialRefName = mat.MaterialName,
                                           SalesInvoiceRefId = invDet.SalesInvoiceRefId,
                                           DiscountFlag = invDet.DiscountFlag,
                                           DiscountPercentage = invDet.DiscountPercentage,
                                           NetAmount = invDet.NetAmount,
                                           Price = invDet.Price,
                                           SalesTaxAmount = invDet.SalesTaxAmount,
                                           TotalAmount = invDet.TotalAmount,
                                           TotalQty = invDet.TotalQty,
                                           Remarks = invDet.Remarks,
                                           Sno = invDet.Sno,
                                           Uom = uiss.Name,
                                           UnitRefId = invDet.UnitRefId,
                                           UnitRefName = uiss.Name
                                       }).ToListAsync();
            }

            decimal SubTotal = Math.Round(editDetailDto.Sum(t => t.TotalAmount), 2);
            decimal TaxAmount = Math.Round(editDetailDto.Sum(t => t.SalesTaxAmount), 2);
            decimal NetAmount = Math.Round(hDto.InvoiceAmount, 2);
            hDto.RoundedAmount = Math.Round(hDto.RoundedAmount, 2);
            hDto.TotalShipmentCharges = Math.Round(hDto.TotalShipmentCharges, 2);


            int loopCnt = 0;
            foreach (var det in editDetailDto)
            {
                var taxlist = await (from potax in _salesinvoicetaxdetailRepo.GetAll().Where(pot => pot.SalesInvoiceRefId == input.Id && pot.MaterialRefId == det.MaterialRefId)
                                     join tax in _salestaxRepo.GetAll()
                                     on potax.SalesTaxRefId equals tax.Id
                                     select new TaxForMaterial
                                     {
                                         TaxRefId = potax.SalesTaxRefId,
                                         TaxName = tax.TaxName,
                                         TaxRate = potax.SalesTaxRate,
                                         TaxValue = potax.SalesTaxValue,
                                         SortOrder = tax.SortOrder,
                                         Rounding = tax.Rounding
                                     }).ToListAsync();

                editDetailDto[loopCnt].TaxForMaterial = taxlist;

                List<ApplicableTaxesForMaterial> applicabletaxes = new List<ApplicableTaxesForMaterial>();

                foreach (var tax in taxlist.OrderBy(t => t.SortOrder))
                {
                    applicabletaxes.Add(new ApplicableTaxesForMaterial
                    {
                        TaxName = tax.TaxName,
                        TaxRefId = tax.TaxRefId,
                        TaxRate = tax.TaxRate,
                        SortOrder = tax.SortOrder,
                        Rounding = tax.Rounding,
                        TaxCalculationMethod = tax.TaxCalculationMethod,
                    });
                }

                editDetailDto[loopCnt].ApplicableTaxes = applicabletaxes;

                loopCnt++;
            }

            editSalesInvoiceDCLink = await (from invDC in _salesinvoiceDeliveryOrderLinkRepo.GetAll().Where(a => a.SalesInvoiceRefId == input.Id)
                                       select new SalesInvoiceDeliveryOrderLinkViewDto
                                       {
                                           Id = invDC.Id,
                                           SalesInvoiceRefId = invDC.SalesInvoiceRefId,
                                           DeliveryOrderRefId = invDC.DeliveryOrderRefId,
                                           MaterialRefId = invDC.MaterialRefId
                                       }).ToListAsync();


            var customer = _customerRepo.FirstOrDefault(t => t.Id == hDto.CustomerRefId);
            var billingLocation = _locationRepo.FirstOrDefault(t => t.Id == hDto.LocationRefId);

            TextFileWriter fp = new TextFileWriter();
            string subPath = "~/Report/";
            bool exists = System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(subPath));


            if (!exists)
                System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(subPath));

            string fileName = System.Web.HttpContext.Current.Server.MapPath("~/Report/" + AbpSession.UserId + "_InvReport.txt");
            int PageLenth = 35;


            fp.FileOpen(fileName, "O");
            if (billingLocation.Name != null)
            {
                fp.PrintTextLine(fp.PrintFormatCenter(billingLocation.Name, PageLenth));
            }

            if (billingLocation.Address1 != null)
            {
                fp.PrintTextLine(fp.PrintFormatCenter(billingLocation.Address1, PageLenth));
            }

            if (billingLocation.Address2 != null)
            {
                fp.PrintTextLine(fp.PrintFormatCenter(billingLocation.Address2, PageLenth));
            }

            if (billingLocation.Address3 != null)
            {
                string addwithCity = billingLocation.Address3;
                if (billingLocation.City != null)
                    addwithCity = addwithCity + ", " + billingLocation.City;
                fp.PrintTextLine(fp.PrintFormatCenter(addwithCity, PageLenth));
            }

            fp.PrintTextLine(fp.PrintFormatCenter(L("SalesInvoice"), PageLenth));

            fp.PrintText(fp.PrintFormatLeft(L("Inv#") + ": " + hDto.InvoiceNumber, 10));
            fp.PrintText(" " + L("Dt") + ": " + hDto.InvoiceDate.ToString("dd/MM/yyyy"));
            //if (hDto.AccountDate!=null)
            //    fp.PrintText(" " + L("Acc Dt") + ": " + hDto.AccountDate.ToString("yy-MMM-dd"));
            fp.PrintTextLine("");

            fp.PrintSepLine('-', PageLenth);

            string retText = System.IO.File.ReadAllText(fileName);

            retText = retText.Replace("\r\n", "<br/>");

            retText = retText.Replace(" ", "&nbsp;");

            output.HeaderText = retText;
            output.BoldHeaderText = "<STRONG>" + retText + "</STRONG>";

            fp.FileOpen(fileName, "O");

            fp.PrintText(L("Customer") + " : ");
            if (customer.CustomerName.Length > 0)
                fp.PrintTextLine(customer.CustomerName);

            //if (customer.TaxRegistrationNumber != null)
            //{
            //    fp.PrintText(L("Tax#"));
            //    fp.PrintTextLine(customer.TaxRegistrationNumber);
            //}
            fp.PrintSepLine('-', PageLenth);

            fp.PrintText(fp.PrintFormatLeft(L("Material"), 18));
            fp.PrintText(fp.PrintFormatRight(L("Qty"), 7));
            fp.PrintText(fp.PrintFormatRight(L("UOM"), 4));
            fp.PrintText(fp.PrintFormatRight(L("Price"), 6));


            fp.PrintTextLine("");
            fp.PrintSepLine('-', PageLenth);

            int loopIndex = 1;

            foreach (var det in editDetailDto)
            {
                string mname = det.MaterialRefName;
                if (mname.Length > 18)
                    mname = mname.Left(18);

                fp.PrintText(fp.PrintFormatLeft(mname, 18));
                decimal qty = Math.Round(det.TotalQty, 2);
                string qtyinstring = string.Concat(new string(' ', 8 - qty.ToString().Length), qty.ToString());

                string unitRefName = det.UnitRefName;
                if (det.UnitRefName.Length > 2)
                    unitRefName = det.UnitRefName.Left(2);

                qtyinstring = qtyinstring + " " + unitRefName;

                fp.PrintText(fp.PrintFormatLeft(qtyinstring, 11));

                decimal price = Math.Round(det.TotalAmount, 0);
                string priceinstring = string.Concat(new string(' ', 6 - price.ToString().Length), price.ToString());
                fp.PrintText(fp.PrintFormatRight(priceinstring, 6));

                fp.PrintTextLine("");
                loopIndex++;
            }

            fp.PrintTextLine(" ");
            fp.PrintSepLine('-', PageLenth);

            fp.PrintText(fp.PrintFormatLeft(L("SubTotal"), 20));
            string subtotalinstring = string.Concat(new string(' ', 15 - SubTotal.ToString().Length), SubTotal.ToString());
            fp.PrintTextLine(fp.PrintFormatRight(subtotalinstring, 15));

            if (TaxAmount > 0)
            {
                fp.PrintText(fp.PrintFormatLeft(L("Tax"), 20));
                string taxamountinstring = string.Concat(new string(' ', 15 - TaxAmount.ToString().Length), TaxAmount.ToString());
                fp.PrintTextLine(fp.PrintFormatRight(taxamountinstring, 15));
            }

            if (hDto.TotalDiscountAmount > 0)
            {
                fp.PrintText(fp.PrintFormatLeft(L("Discount"), 20));
                string discountinstring = string.Concat(new string(' ', 15 - hDto.TotalDiscountAmount.ToString().Length), hDto.TotalDiscountAmount.ToString());
                fp.PrintTextLine(fp.PrintFormatRight(discountinstring, 15));
            }


            if (hDto.TotalShipmentCharges > 0)
            {
                fp.PrintText(fp.PrintFormatLeft(L("Extra"), 20));
                string shipmentinstring = string.Concat(new string(' ', 15 - hDto.TotalShipmentCharges.ToString().Length), hDto.TotalShipmentCharges.ToString());
                fp.PrintTextLine(fp.PrintFormatRight(shipmentinstring, 15));
            }

            if (hDto.RoundedAmount != 0)
            {
                fp.PrintText(fp.PrintFormatLeft(L("RoundedOff"), 20));
                string roundamtinstring = string.Concat(new string(' ', 15 - hDto.RoundedAmount.ToString().Length), hDto.RoundedAmount.ToString());
                fp.PrintTextLine(fp.PrintFormatRight(roundamtinstring, 15));
            }

            fp.PrintText(fp.PrintFormatLeft(L("Total"), 20));
            string totalamtinstring = string.Concat(new string(' ', 15 - NetAmount.ToString().Length), NetAmount.ToString());
            fp.PrintTextLine(fp.PrintFormatRight(totalamtinstring, 15));

            fp.PrintSepLine('-', PageLenth);
            //fp.PrintTextLine(fp.PrintFormatCenter("Print @ " + DateTime.Now.ToString("yy-MMM-dd HH:mm:ss"), PageLenth));
            //fp.PrintSepLine('-', PageLenth);
            //fp.PrintTextLine(" ");

            retText = System.IO.File.ReadAllText(fileName);

            retText = retText.Replace("\r\n", "<br/>");

            //retText = retText.Replace(" ", "&nbsp;");
            output.BodyText = retText;
            output.BoldBodyText = "<STRONG>" + retText + "</STRONG>";

            return output;

        }

        public async Task<ListResultOutput<CustomerMaterialViewDto>> GetMaterialBasedOnCustomerAndSalesInvoice(SentLocationAndSupplier input)
        {
            List<CustomerMaterialViewDto> lstMaterial;

            lstMaterial = await (from mat in _materialRepo.GetAll()
                                 join supmat in _salesinvoicedetailRepo.GetAll().
                                     Where(sp => sp.SalesInvoiceRefId == input.SalesInvoiceRefId)
                                 on mat.Id equals supmat.MaterialRefId
                                 join matloc in _materiallocationwisestockRepo.GetAll().
                                     Where(mls => mls.LocationRefId == input.LocationRefId)
                                 on mat.Id equals matloc.MaterialRefId
                                 join un in _unitRepo.GetAll()
                                 on mat.DefaultUnitId equals un.Id
                                 select new CustomerMaterialViewDto
                                 {
                                     MaterialRefId = mat.Id,
                                     MaterialRefName = mat.MaterialName,
                                     MaterialGroupCategoryRefId = mat.MaterialGroupCategoryRefId,
                                     MaterialPrice = supmat.Price,
                                     MinimumOrderQuantity = 0,
                                     MaximumOrderQuantity = supmat.TotalQty,
                                     CurrentInHand = matloc.CurrentInHand,
                                     AlreadyOrderedQuantity = 0,
                                     Uom = un.Name,
                                     UnitRefId = mat.DefaultUnitId,
                                     IsQuoteNeededForSales = mat.IsQuoteNeededForPurchase,
                                     IsFractional = mat.IsFractional,
                                     IsBranded = mat.IsBranded
                                 }).ToListAsync();


            int loopcnt = 0;

            int companyRefid = await _locationRepo.GetAll().Where(l => l.Id == input.LocationRefId).Select(l => l.CompanyRefId).FirstOrDefaultAsync();

            var invTaxesList = await _salesinvoicetaxdetailRepo.GetAll().Where(pot => pot.SalesInvoiceRefId == input.SalesInvoiceRefId).ToListAsync();

            var rsTaxes = await _salestaxRepo.GetAllListAsync();

            foreach (var mat in lstMaterial)
            {
                List<TaxForMaterial> taxtoAdded = new List<TaxForMaterial>();
                List<TaxForMaterial> sortedTaxToAdded = new List<TaxForMaterial>();

                // taxtemplate;
                // Find any specific TAX For particular Material
                //List<int> taxTemplates;
                List<int> outputTaxTemplates = new List<int>();

                var taxTemplates = await _salestaxtemplatemappingRepo.GetAll().Where(t => t.MaterialRefId == mat.MaterialRefId).Select(t => t.SalesTaxRefId).ToArrayAsync();

                outputTaxTemplates.AddRange(taxTemplates);
                taxTemplates = null;

                taxTemplates = await _salestaxtemplatemappingRepo.GetAll().Where(t => t.MaterialGroupCategoryRefId == mat.MaterialGroupCategoryRefId && t.MaterialRefId == null).Select(t => t.SalesTaxRefId).ToArrayAsync();
                outputTaxTemplates.AddRange(taxTemplates);
                taxTemplates = null;

                taxTemplates = await _salestaxtemplatemappingRepo.GetAll().Where(t => t.MaterialGroupRefId == mat.MaterialGroupRefId && t.MaterialGroupCategoryRefId == null && t.MaterialRefId == null).Select(t => t.SalesTaxRefId).ToArrayAsync();
                outputTaxTemplates.AddRange(taxTemplates);
                taxTemplates = null;

                taxTemplates = await _salestaxtemplatemappingRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId && t.MaterialGroupRefId == null).Select(t => t.SalesTaxRefId).ToArrayAsync();
                outputTaxTemplates.AddRange(taxTemplates);
                taxTemplates = null;

                taxTemplates = await _salestaxtemplatemappingRepo.GetAll().Where(t => t.CompanyRefId == companyRefid && t.LocationRefId == input.LocationRefId).Select(t => t.SalesTaxRefId).ToArrayAsync();
                outputTaxTemplates.AddRange(taxTemplates);
                taxTemplates = null;

                taxTemplates = await _salestaxtemplatemappingRepo.GetAll().Where(t => t.LocationRefId == null && t.CompanyRefId == null && t.MaterialGroupRefId == null && t.MaterialGroupCategoryRefId == null && t.MaterialRefId == null).Select(t => t.SalesTaxRefId).ToArrayAsync();
                outputTaxTemplates.AddRange(taxTemplates);
                taxTemplates = null;

                outputTaxTemplates = outputTaxTemplates.Distinct().MapTo<List<int>>();

                List<ApplicableTaxesForMaterial> applicabletaxes = new List<ApplicableTaxesForMaterial>();

                if (outputTaxTemplates.Count() > 0)
                {
                    foreach (var taxtemplate in outputTaxTemplates)
                    {
                        var tax = _salestaxRepo.Single(t => t.Id == taxtemplate);
                        taxtoAdded.Add(new TaxForMaterial
                        {
                            MaterialRefId = mat.MaterialRefId,
                            TaxName = tax.TaxName,
                            TaxRefId = taxtemplate,
                            TaxRate = tax.Percentage,
                            SortOrder = tax.SortOrder,
                            Rounding = tax.Rounding,
                            TaxCalculationMethod = tax.TaxCalculationMethod,
                        }
                        );
                    }
                    sortedTaxToAdded = taxtoAdded.OrderBy(t => t.SortOrder).ToList();



                    var taxlist = (from potax in invTaxesList.Where(pot => pot.SalesInvoiceRefId == input.SalesInvoiceRefId && pot.MaterialRefId == mat.MaterialRefId)
                                   join tax in rsTaxes
                                   on potax.SalesTaxRefId equals tax.Id
                                   select new TaxForMaterial
                                   {
                                       TaxRefId = potax.SalesTaxRefId,
                                       TaxName = tax.TaxName,
                                       TaxRate = potax.SalesTaxRate,
                                       TaxValue = potax.SalesTaxValue,
                                       SortOrder = tax.SortOrder,
                                       Rounding = tax.Rounding
                                   }).ToList();

                    foreach (var tax in sortedTaxToAdded)
                    {
                        var taxExist = taxlist.FirstOrDefault(t => t.TaxRefId == tax.TaxRefId);
                        if (taxExist != null)
                        {
                            applicabletaxes.Add(new ApplicableTaxesForMaterial
                            {
                                TaxName = tax.TaxName,
                                TaxRefId = tax.TaxRefId,
                                TaxRate = tax.TaxRate,
                                SortOrder = tax.SortOrder,
                                Rounding = tax.Rounding,
                                TaxCalculationMethod = tax.TaxCalculationMethod,
                            });
                        }
                    }
                }
                //@@Pending Sort Based On SortOrder

                lstMaterial[loopcnt].TaxForMaterial = sortedTaxToAdded;
                lstMaterial[loopcnt].ApplicableTaxes = applicabletaxes;

                outputTaxTemplates = null;
                taxtoAdded = null;
                loopcnt++;
            }

            var retlst = new ListResultOutput<CustomerMaterialViewDto>(lstMaterial.MapTo<List<CustomerMaterialViewDto>>());

            return retlst;

        }


    }
}
