﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Transaction.Dtos;

namespace DinePlan.DineConnect.House.Transaction
{
    public interface ISupplierDocumentAppService : IApplicationService
    {
        Task<PagedResultOutput<SupplierDocumentListDto>> GetAll(GetSupplierDocumentInput inputDto);
        //Task<FileDto> GetAllToExcel();
        Task<GetSupplierDocumentForEditOutput> GetSupplierDocumentForEdit(NullableIdInput nullableIdInput);
        Task<IdInput> CreateOrUpdateSupplierDocument(CreateOrUpdateSupplierDocumentInput input);
        Task DeleteSupplierDocument(IdInput input);

        Task<ListResultOutput<SupplierDocumentListDto>> GetIds();
    }
}