﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.House.Master.Dtos;

namespace DinePlan.DineConnect.House.Transaction
{
    public interface ISalesInvoiceAppService : IApplicationService
    {
        Task<PagedResultOutput<SalesInvoiceListDto>> GetAll(GetSalesInvoiceInput inputDto);
        Task<FileDto> GetAllToExcel(GetSalesInvoiceInput input);
        Task<GetSalesInvoiceForEditOutput> GetSalesInvoiceForEdit(NullableIdInput nullableIdInput);
        Task<IdInput> CreateOrUpdateSalesInvoice(CreateOrUpdateSalesInvoiceInput input);
        Task DeleteSalesInvoice(IdInput input);
        Task<PagedResultOutput<SalesInvoiceListDto>> GetSalesInvoiceSearch(GetSalesInvoiceInput input);
        Task<ListResultOutput<SalesInvoiceListDto>> GetIds();

        Task<GetDcMaterialDataDetail> GetMaterialsFromDcId(GetDcMaterialInfo input);
        Task SetDcCompleteStatus(IdInput input);
        Task<List<SalesInvoiceAnalysisDto>> GetSalesAnalysis(InputSalesInvoiceAnalysis input);
        Task<List<SalesDeliveryOrderDetailForParticularDc>> GetMaterialForInwardDc(IdInput ninput);

        Task<ListResultOutput<ComboboxItemDto>> GetDcTranId(InputDateRangeWithCustomerId input);

        Task<List<SalesInvoiceCategoryAnalysisDto>> GetSalesAnalysisCategoryWise(InputSalesInvoiceAnalysisForCategoryWise input);
        Task<List<OutputMaterialSalesDto>> GetMaterialSales(InputMaterialSalesDto input);

        Task<PrintOutIn40Cols> GetPrintData(IdInput input);
        Task<FileDto> GetSalesInvoiceSummaryAllToExcel(GetSalesInvoiceInput input);
        Task<ListResultOutput<CustomerMaterialViewDto>> GetMaterialBasedOnCustomerAndSalesInvoice(SentLocationAndSupplier input);

    }
}