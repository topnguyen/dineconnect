﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Utility.Exporter;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.House.Master.Exporter;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using System;
using DinePlan.DineConnect.House.Master;

namespace DinePlan.DineConnect.House.Transaction.Implementation
{
    public class MenuItemWastageAppService : DineConnectAppServiceBase, IMenuItemWastageAppService
    {

        private readonly IMenuWastageExcelExporter _exporter;
        private readonly IMenuItemWastageManager _menuitemwastageManager;
        private readonly IRepository<MenuItemWastage> _menuitemwastageRepo;
        private readonly IRepository<MenuItemWastageDetail> _menuitemwastageDetailRepo;
        private readonly IRepository<MenuItemPortion> _menuitemportionRepo;
        private readonly IRepository<MenuItem> _menuitemRepo;
		private readonly IRepository<Location> _locationRepo;
		private readonly IConnectReportAppService _connectReportAppService;
		private readonly IHouseReportAppService _houseReportAppService;

		public MenuItemWastageAppService(IMenuItemWastageManager menuitemwastageManager,
            IRepository<MenuItemWastage> menuItemWastageRepo,
            IRepository<MenuItemWastageDetail> menuitemwastageDetailRepo,
            IRepository<MenuItemPortion> menuitemportionRepo,
            IRepository<MenuItem> menuitemRepo,
			IRepository<Location> locationRepo,
			IMenuWastageExcelExporter exporter,
			IConnectReportAppService connectReportAppService,
			IHouseReportAppService houseReportAppService
			)
        {
            _menuitemwastageManager = menuitemwastageManager;
            _menuitemwastageRepo = menuItemWastageRepo;
            _menuitemwastageDetailRepo = menuitemwastageDetailRepo;
            _menuitemportionRepo = menuitemportionRepo;
            _menuitemRepo = menuitemRepo;
			_locationRepo = locationRepo;
			_exporter = exporter;
			_connectReportAppService = connectReportAppService;
			_houseReportAppService = houseReportAppService;
		}

        public async Task<PagedResultOutput<MenuItemWastageListDto>> GetAll(GetMenuItemWastageInput input)
        {
            var allItems = _menuitemwastageRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.TokenRefNumber.ToString().Contains(input.Filter) || p.Remarks.Contains(input.Filter));

			if (input.LocationRefId!=null)
			{
				allItems = allItems.Where(t => t.LocationRefId == input.LocationRefId.Value);
			}

			if (input.StartDate != null)
			{
				allItems = allItems.Where(t => t.SalesDate>= input.StartDate.Value && t.SalesDate <= input.EndDate.Value);
			}


			var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<MenuItemWastageListDto>>();

            var allItemCount = await allItems.CountAsync();

			var rsLocs = await _locationRepo.GetAllListAsync();
			foreach(var lst in allListDtos)
			{
				var loc = rsLocs.FirstOrDefault(t => t.Id == lst.LocationRefId);
				lst.LocationRefName = loc.Name;
			}

            return new PagedResultOutput<MenuItemWastageListDto>(
                allItemCount,
                allListDtos
                );
        }

		public async Task<FileDto> GetAllToExcel(GetMenuItemWastageInput input)
		{
			input.MaxResultCount = AppConsts.MaxPageSize;
			var allList = await GetAll(input);
			var allListDtos = allList.Items.MapTo<List<MenuItemWastageListDto>>();
			
			return _exporter.ExportToFile(allListDtos);
		}

		public async Task<GetMenuItemWastageForEditOutput> GetMenuItemWastageForEdit(NullableIdInput input)
        {
            MenuItemWastageEditDto editDto;
            List<MenuItemWastageDetailEditDto> editDetailsDto;

            if (input.Id.HasValue)
            {
                var hDto = await _menuitemwastageRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<MenuItemWastageEditDto>();

                editDetailsDto = await (from menuportion in _menuitemportionRepo.GetAll()
                                   join menu in _menuitemRepo.GetAll().Where(a => a.ProductType == 1)
                                   on menuportion.MenuItemId equals menu.Id 
                                   join det in _menuitemwastageDetailRepo
                                        .GetAll().Where(t => t.MenuItemWastageRefId == editDto.Id)
                                    on menuportion.Id equals det.PosMenuPortionRefId
                                   select new MenuItemWastageDetailEditDto
                                   {
                                       MenuItemWastageRefId = det.MenuItemWastageRefId,
                                       Sno = det.Sno,
                                       PosMenuPortionRefId = det.PosMenuPortionRefId,
                                       PosMenuPortionCode = menu.AliasCode,
                                       PosMenuPortionRefName = string.Concat(menu.Name, " - ", menuportion.Name),
                                       WastageQty = det.WastageQty,
                                       WastageRemarks = det.WastageRemarks,
                                   }).ToListAsync();
            }
            else
            {
                editDto = new MenuItemWastageEditDto();
                editDetailsDto = new List<MenuItemWastageDetailEditDto>();
            }

            return new GetMenuItemWastageForEditOutput
            {
                MenuItemWastage = editDto,
                MenuItemWastageDetail = editDetailsDto
            };
        }

        public async Task CreateOrUpdateMenuItemWastage(CreateOrUpdateMenuItemWastageInput input)
        {
            if (input.MenuItemWastage.Id.HasValue)
            {
                await UpdateMenuItemWastage(input);
            }
            else
            {
                await CreateMenuItemWastage(input);
            }
        }

        public async Task DeleteMenuItemWastage(IdInput input)
        {
            var master = await _menuitemwastageRepo.FirstOrDefaultAsync(t => t.Id == input.Id);
            if (master != null)
            {
                var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == master.LocationRefId);
                if (loc.HouseTransactionDate.Value.Date > master.SalesDate.Date)
                {
                    throw new UserFriendlyException(L("MenuWastageDateNotMatchedWithHouseTransactionDate", master.SalesDate.Date.ToString("dd-MMM-yyyy"), loc.HouseTransactionDate.Value.Date.ToString("dd-MMM-yyyy")));
                }

                var details = await _menuitemwastageDetailRepo.GetAllListAsync(t => t.MenuItemWastageRefId == master.Id);
                foreach(var lst in details)
                {
                    await _menuitemwastageDetailRepo.DeleteAsync(lst.Id);
                }

                await _menuitemwastageRepo.DeleteAsync(input.Id);
            }
        }

        protected virtual async Task UpdateMenuItemWastage(CreateOrUpdateMenuItemWastageInput input)
        {
            var item = await _menuitemwastageRepo.GetAsync(input.MenuItemWastage.Id.Value);
            var dto = input.MenuItemWastage;

            item.LocationRefId = dto.LocationRefId;
            if (item.SalesDate.Date == dto.SalesDate.Date)
            {

            }
            else
            {
                item.SalesDate = dto.SalesDate;
            }
            item.Remarks = dto.Remarks;
            item.TokenRefNumber = dto.TokenRefNumber;
            CheckErrors(await _menuitemwastageManager.CreateSync(item));

			List<ComboboxItemDto> lstMenuItems = new List<ComboboxItemDto>();
			foreach (var items in input.MenuItemWastageDetail.ToList())
			{
				lstMenuItems.Add(new ComboboxItemDto { Value = items.PosMenuPortionRefId.ToString() });
			}

			var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == dto.LocationRefId);
			List<LocationListDto> locinput = new List<LocationListDto>();
			locinput.Add(loc.MapTo<LocationListDto>());

			var rsMenuCost = await _houseReportAppService.GetMenuCostingReportForGivenMenuPortionIds(new GetCostReportInput
			{
				StartDate = input.MenuItemWastage.SalesDate,
				EndDate = input.MenuItemWastage.SalesDate,
				MenuItems = lstMenuItems,
				Locations = locinput
			});

			List<int> posMenuToBeRetained = new List<int>();

            if (input.MenuItemWastageDetail != null && input.MenuItemWastageDetail.Count > 0)
            {
                foreach (var items in input.MenuItemWastageDetail)
                {
                    var matRefId = items.PosMenuPortionRefId;
                    posMenuToBeRetained.Add(matRefId);

					var menu = rsMenuCost.RecipeWiseCosting.FirstOrDefault(t => t.MenuItemRefId == items.PosMenuPortionRefId);
					
					var existingDetail = _menuitemwastageDetailRepo.GetAllList(u => u.MenuItemWastageRefId == dto.Id && u.PosMenuPortionRefId.Equals(matRefId));
                    if (existingDetail.Count == 0)  //Add new record
                    {
                        MenuItemWastageDetail det = new MenuItemWastageDetail();
                        det.MenuItemWastageRefId = (int)dto.Id;
                        det.PosMenuPortionRefId = items.PosMenuPortionRefId;
                        det.WastageQty = items.WastageQty;
                        det.WastageRemarks = items.WastageRemarks;
                        det.Sno = items.Sno;
						det.Cost = menu.CostPerUnit;

						var retId2 = await _menuitemwastageDetailRepo.InsertAndGetIdAsync(det);
                    }
                    else
                    {
                        var editDetailDto = await _menuitemwastageDetailRepo.GetAsync(existingDetail[0].Id);

                        editDetailDto.MenuItemWastageRefId = (int)dto.Id;
                        editDetailDto.PosMenuPortionRefId = items.PosMenuPortionRefId;
                        editDetailDto.WastageQty = items.WastageQty;
                        editDetailDto.WastageRemarks = items.WastageRemarks;
                        editDetailDto.Sno = items.Sno;
						editDetailDto.Cost = menu.CostPerUnit;

						var retId3 = await _menuitemwastageDetailRepo.InsertOrUpdateAndGetIdAsync(editDetailDto);

                    }
                }
            }

            var delDetailsList = _menuitemwastageDetailRepo.GetAll().Where(a => a.MenuItemWastageRefId == input.MenuItemWastage.Id.Value && !posMenuToBeRetained.Contains(a.PosMenuPortionRefId)).ToList();

            foreach (var a in delDetailsList)
            {
                await _menuitemwastageDetailRepo.DeleteAsync(a.Id);
            }


        }

        protected virtual async Task CreateMenuItemWastage(CreateOrUpdateMenuItemWastageInput input)
        {
            var dto = input.MenuItemWastage.MapTo<MenuItemWastage>();

            if (input.MenuItemWastageDetail == null || input.MenuItemWastageDetail.Count() == 0)
            {
                throw new UserFriendlyException(L("MinimumOneDetail"));
            }

            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == dto.LocationRefId);

            if (loc.HouseTransactionDate.Value.Date == dto.SalesDate.Date || DateTime.Now.Date == dto.SalesDate.Date)
            {
                dto.SalesDate = DateTime.Now;
            }

            CheckErrors(await _menuitemwastageManager.CreateSync(dto));

			List<ComboboxItemDto> lstMenuItems = new List<ComboboxItemDto>();
			foreach(var items in input.MenuItemWastageDetail.ToList())
			{
				lstMenuItems.Add(new ComboboxItemDto { Value = items.PosMenuPortionRefId.ToString() });
			}

			List<LocationListDto> locinput = new List<LocationListDto>();
			locinput.Add(loc.MapTo<LocationListDto>());

			var rsMenuCost = await _houseReportAppService.GetMenuCostingReportForGivenMenuPortionIds(new GetCostReportInput
			{
				StartDate = input.MenuItemWastage.SalesDate,
				EndDate = input.MenuItemWastage.SalesDate,
				MenuItems = lstMenuItems,
				Locations = locinput
			});

			foreach (var items in input.MenuItemWastageDetail.ToList())
            {
                MenuItemWastageDetail det = new MenuItemWastageDetail();
                det.MenuItemWastageRefId = (int)dto.Id;
                det.PosMenuPortionRefId = items.PosMenuPortionRefId;
                det.WastageQty = items.WastageQty;
                det.WastageRemarks = items.WastageRemarks;
                det.Sno = items.Sno;
				var menu = rsMenuCost.RecipeWiseCosting.FirstOrDefault(t => t.MenuItemRefId == det.PosMenuPortionRefId);
                if (menu != null)
                {
                    det.Cost = menu.CostPerUnit;
                }
				var retId2 = await _menuitemwastageDetailRepo.InsertAndGetIdAsync(det);
            }
            
        }

		public async Task<PagedResultOutput<MenuWastageConsolidatedReport>> GetMenuWastageAll(InputMenuWastageReport input)
		{
			var arrLocationRefIds = input.ToLocations.Select(t => t.Id).ToArray();

			var locs = await _locationRepo.GetAll().Where(t => arrLocationRefIds.Contains(t.Id)).ToListAsync();
			List<LocationListDto> locinput = locs.MapTo<List<LocationListDto>>();

			var menuWastageString = L("MenuWastage");
			var output = await (from mas in _menuitemwastageRepo.GetAll()
							.Where(t => DbFunctions.TruncateTime(t.SalesDate) >= DbFunctions.TruncateTime(input.StartDate.Value) && DbFunctions.TruncateTime(t.SalesDate) <= DbFunctions.TruncateTime(input.EndDate.Value) && arrLocationRefIds.Contains(t.LocationRefId))
								join det in _menuitemwastageDetailRepo.GetAll() on mas.Id equals det.MenuItemWastageRefId
								join loc in _locationRepo.GetAll() on mas.LocationRefId equals loc.Id
								join menuportion in _menuitemportionRepo.GetAll() 
									on det.PosMenuPortionRefId equals menuportion.Id 
								join menu in _menuitemRepo.GetAll().Where(a => a.ProductType == 1)
								   on menuportion.MenuItemId equals menu.Id
								select new MenuItemWastageDetailEditDto
								{
									LocationRefId = mas.LocationRefId,
									LocationRefName = loc.Name,
									MenuItemWastageRefId = mas.Id,
									PosMenuPortionRefId = det.PosMenuPortionRefId,
									PosMenuPortionRefName = string.Concat(menu.Name, " - ", menuportion.Name),
									WastageQty = det.WastageQty,
									WastageRemarks = det.WastageRemarks,
									UserId = mas.CreatorUserId.Value,
									MenuWastageMode = menuWastageString,
									SalesDate = mas.SalesDate,
									CostPerUnit = det.Cost
								}
								).ToListAsync();

			var userList = UserManager.Users.ToList();

			foreach (var lst in output)
			{
				if (lst.CostPerUnit == 0)
				{
					//var mat = rsMenu.FirstOrDefault(t => t.Id == lst.PosMenuPortionRefId);
					//var locPrice = mat.LocationPrices.Where(t => t.LocationId == lst.LocationRefId).FirstOrDefault();
					//if (locPrice != null)
					//	lst.Price = locPrice.Price;
				}
				var user = userList.FirstOrDefault(t => t.Id == lst.UserId);
				if (user != null)
					lst.UserName = user.Name;
			}

			if (input.CompExistFlag)
			{
				var rsLocations = await _locationRepo.GetAllListAsync();

				DateTime fromDate = input.StartDate.Value;
				foreach (var lstloc in locinput)
				{
					List<LocationListDto> newloclist = new List<LocationListDto>();
					newloclist.Add(lstloc);
					do
					{
						var daySales = await _connectReportAppService.GetAllItemSales(
							new GetItemInput
							{
								StartDate = fromDate,
								EndDate = fromDate,
								Locations = newloclist.MapTo<List<SimpleLocationDto>>(),
                                Comp = true,
                            });

						var itemComp = daySales.Comp.MapTo<List<MenuListDto>>();

						List<ComboboxItemDto> lstMenuItems = new List<ComboboxItemDto>();
						foreach (var items in itemComp)
						{
							lstMenuItems.Add(new ComboboxItemDto { Value = items.MenuItemPortionId.ToString() });
						}

						var rsMenuCost = await _houseReportAppService.GetMenuCostingReportForGivenMenuPortionIds(new GetCostReportInput
						{
							StartDate = fromDate,
							EndDate = fromDate,
							MenuItems = lstMenuItems,
							LocationRefId = lstloc.Id,
						});

						foreach (var item in itemComp)
						{
							MenuItemWastageDetailEditDto compDto = new MenuItemWastageDetailEditDto();
							compDto.SalesDate = fromDate;
							compDto.LocationRefName = lstloc.Name;
							compDto.LocationRefId = lstloc.Id;
							compDto.MenuWastageMode = L("Comp");
							compDto.PosMenuPortionRefId = item.MenuItemPortionId;
							compDto.PosMenuPortionRefName = string.Concat(item.MenuItemName, " - ", item.MenuItemPortionName);
							compDto.WastageQty = item.Quantity;
							var menu = rsMenuCost.RecipeWiseCosting.FirstOrDefault(t => t.MenuItemRefId == item.MenuItemPortionId);
							compDto.CostPerUnit = menu.CostPerUnit;
							output.Add(compDto);
						}
						fromDate = fromDate.AddDays(1);
					} while (fromDate <= input.EndDate.Value);
				}
			}

			output = output.OrderBy(t => t.SalesDate).ToList();
			var ro = (from r in output
				group r by new { r.LocationRefId, r.LocationRefName } into g
				select new MenuWastageConsolidatedReport
				{
					LocationRefId = g.Key.LocationRefId,
					LocationRefName = g.Key.LocationRefName,
					WastageReport = g.ToList()
				}).ToList();

			//return ro;
			return new PagedResultOutput<MenuWastageConsolidatedReport>(
			   ro.Count,
			   ro
			   );
		}

		public async Task<PagedResultOutput<MenuItemWastageDetailEditDto>> GetMenuWastageConsolidatedAll(InputMenuWastageReport input)
		{
			var arrLocationRefIds = input.ToLocations.Select(t => t.Id).ToArray();
			var locs = await _locationRepo.GetAll().Where(t => arrLocationRefIds.Contains(t.Id)).ToListAsync();
			List<LocationListDto> locinput = locs.MapTo<List<LocationListDto>>();
            input.LocationInfo = locinput.FirstOrDefault();
            input.PreparedUserId = AbpSession.UserId;
            var userList = UserManager.Users.ToList();
            string userName = "";
            var user = userList.FirstOrDefault(t => t.Id == AbpSession.UserId.Value);
            if (user != null)
                userName = user.Name;
            input.PreparedUserName = userName;
            input.PrintedTime = DateTime.Now;

            var menuWastageString = L("MenuWastage");
			var output = await (from mas in _menuitemwastageRepo.GetAll()
							.Where(t => DbFunctions.TruncateTime(t.SalesDate) >= DbFunctions.TruncateTime(input.StartDate.Value) 
                                && DbFunctions.TruncateTime(t.SalesDate) <= DbFunctions.TruncateTime(input.EndDate.Value) 
                                    && arrLocationRefIds.Contains(t.LocationRefId))
								join det in _menuitemwastageDetailRepo.GetAll() on mas.Id equals det.MenuItemWastageRefId
								join loc in _locationRepo.GetAll() on mas.LocationRefId equals loc.Id
								join menuportion in _menuitemportionRepo.GetAll()
									on det.PosMenuPortionRefId equals menuportion.Id
								join menu in _menuitemRepo.GetAll().Where(a => a.ProductType == 1)
								   on menuportion.MenuItemId equals menu.Id
								select new MenuItemWastageDetailEditDto
								{
									LocationRefId = mas.LocationRefId,
									LocationRefName = loc.Name,
									MenuItemWastageRefId = mas.Id,
									PosMenuPortionRefId = det.PosMenuPortionRefId,
                                    PosMenuPortionCode = menu.AliasCode,
                                    PosMenuPortionRefName = string.Concat(menu.Name, " - ", menuportion.Name),
									WastageQty = det.WastageQty,
									WastageRemarks = det.WastageRemarks,
									UserId = mas.CreatorUserId.Value,
									MenuWastageMode = menuWastageString,
									SalesDate = mas.SalesDate,
									CostPerUnit = det.Cost
								}
								).ToListAsync();

			

			foreach (var lst in output)
			{
				//if (lst.CostPerUnit == 0)
				//{
				//	//var mat = rsMenu.FirstOrDefault(t => t.Id == lst.PosMenuPortionRefId);
				//	//var locPrice = mat.LocationPrices.Where(t => t.LocationId == lst.LocationRefId).FirstOrDefault();
				//	//if (locPrice != null)
				//	//	lst.Price = locPrice.Price;
				//}
				user = userList.FirstOrDefault(t => t.Id == lst.UserId);
				if (user != null)
					lst.UserName = user.Name;
			}

			if (input.CompExistFlag)
			{
				var rsLocations = await _locationRepo.GetAllListAsync();

				DateTime fromDate = input.StartDate.Value;
				foreach (var lstloc in locinput)
				{
					List<LocationListDto> newloclist = new List<LocationListDto>();
					newloclist.Add(lstloc);
					do
					{
						var daySales = await _connectReportAppService.GetAllItemSales(
							new GetItemInput
							{
								StartDate = fromDate,
								EndDate = fromDate,
								Locations = newloclist.MapTo<List<SimpleLocationDto>>(),
								Comp = true,
							});

						var itemComp = daySales.Comp.MapTo<List<MenuListDto>>();

						List<ComboboxItemDto> lstMenuItems = new List<ComboboxItemDto>();
						foreach (var items in itemComp)
						{
							lstMenuItems.Add(new ComboboxItemDto { Value = items.MenuItemPortionId.ToString() });
						}

						var rsMenuCost = await _houseReportAppService.GetMenuCostingReportForGivenMenuPortionIds(new GetCostReportInput
						{
							StartDate = fromDate,
							EndDate = fromDate,
							MenuItems = lstMenuItems,
							LocationRefId = lstloc.Id,
						});

						foreach (var item in itemComp)
						{
							MenuItemWastageDetailEditDto compDto = new MenuItemWastageDetailEditDto();
							compDto.SalesDate = fromDate;
							compDto.LocationRefName = lstloc.Name;
							compDto.LocationRefId = lstloc.Id;
							compDto.MenuWastageMode = L("Comp");
							compDto.PosMenuPortionRefId = item.MenuItemPortionId;
                            compDto.PosMenuPortionCode = item.AliasCode;
							compDto.PosMenuPortionRefName = string.Concat(item.MenuItemName, " - ", item.MenuItemPortionName);
							compDto.WastageQty = item.Quantity;
							var menu = rsMenuCost.RecipeWiseCosting.FirstOrDefault(t => t.MenuItemRefId == item.MenuItemPortionId);
							compDto.CostPerUnit = menu.CostPerUnit;
							output.Add(compDto);
						}
						fromDate = fromDate.AddDays(1);
					} while (fromDate <= input.EndDate.Value);
				}
			}

			output = output.OrderBy(t => t.SalesDate).ToList();
			return new PagedResultOutput<MenuItemWastageDetailEditDto>(
			   output.Count,
			   output
			   );
		}

		public async Task<FileDto> GetMenuWastageToExcel(InputMenuWastageReport input)
		{
		//	var allList = await GetMenuWastageAll(input);
			string userName = "";
			var user = await UserManager.GetUserByIdAsync(AbpSession.UserId.Value);
			if (user != null)
				userName = user.Name;

			//if (allList!=null &&  allList.TotalCount > 0)
            if (input.CustomReportFlag)
            {
                FileDto file = await _exporter.MenuWastageCustomReportExcelExportAsync(this, input);
                return file;
            }
            else
			{
				//FileDto fileExcel = _exporter.MenuWastageReportToExcelFile(allList, input.StartDate.Value, input.EndDate.Value, userName);
				//return fileExcel;
				FileDto file = await _exporter.MenuWastageReportExportAsync(this, input, userName,input.ExportType);
				return file;
			}
			//else
			//{
			//	throw new UserFriendlyException(L("NoRecordsFound"));
			//}
		}

	}
}