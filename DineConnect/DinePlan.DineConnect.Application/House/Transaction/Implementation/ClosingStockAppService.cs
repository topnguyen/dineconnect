﻿
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Utility.Exporter;
using DinePlan.DineConnect.Utility;
using System;
using DinePlan.DineConnect.Common;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;

namespace DinePlan.DineConnect.House.Transaction.Implementation
{
    public class ClosingStockAppService : DineConnectAppServiceBase, IClosingStockAppService
    {
        private readonly IExcelExporter _exporter;
        private readonly IClosingStockManager _closingstockManager;
        private readonly IRepository<ClosingStock> _closingstockRepo;
        private readonly IRepository<ClosingStockDetail> _closingstockDetailRepo;
        private readonly IRepository<MaterialGroupCategory> _materialGroupCategoryRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<Unit> _unitRepo;
        private readonly IXmlAndJsonConvertor _xmlandjsonConvertorAppService;
        private readonly IRepository<Template> _templateRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<ClosingStockUnitWiseDetail> _closingStockUnitWiseDetailRepo;


        public ClosingStockAppService(IClosingStockManager closingstockManager,
            IRepository<ClosingStock> closingStockRepo,
            IExcelExporter exporter, IRepository<ClosingStockDetail> closingstockDetailRepo,
            IRepository<Material> materialRepo, IRepository<Unit> unitRepo,
            IXmlAndJsonConvertor xmlandjsonConvertorAppService,
            IRepository<Location> locationRepo,
            IRepository<Template> templateRepo,
            IRepository<ClosingStockUnitWiseDetail> closingStockUnitWiseDetailRepo,
            IRepository<MaterialGroupCategory> materialGroupCategoryRepo)
        {
            _closingstockManager = closingstockManager;
            _closingstockRepo = closingStockRepo;
            _closingStockUnitWiseDetailRepo = closingStockUnitWiseDetailRepo;
            _exporter = exporter;
            _closingstockDetailRepo = closingstockDetailRepo;
            _materialRepo = materialRepo;
            _unitRepo = unitRepo;
            _xmlandjsonConvertorAppService = xmlandjsonConvertorAppService;
            _templateRepo = templateRepo;
            _locationRepo = locationRepo;
            _materialGroupCategoryRepo = materialGroupCategoryRepo;

        }



        public async Task<PagedResultOutput<ClosingStockListDto>> GetAllConsolidatedDayWise(GetClosingStockInput input)
        {
            var closing = _closingstockRepo.GetAll().Where(t => t.LocationRefId == input.DefaultLocationRefId);

            if (input.StartDate != null && input.EndDate != null)
            {
                closing = closing.Where(a =>
                                 DbFunctions.TruncateTime(a.StockDate) >= DbFunctions.TruncateTime(input.StartDate)
                                 &&
                                 DbFunctions.TruncateTime(a.StockDate) <= DbFunctions.TruncateTime(input.EndDate));
            }

            var allItems = (from mas in closing
                            group mas by new { mas.StockDate } into g
                            select new ClosingStockListDto
                            {
                                StockDate = g.Key.StockDate
                            });

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<ClosingStockListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<ClosingStockListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<PagedResultOutput<ClosingStockListDto>> GetAll(GetClosingStockInput input)
        {
            var closing = _closingstockRepo.GetAll().Where(t => t.LocationRefId == input.DefaultLocationRefId);

            if (input.StartDate != null && input.EndDate != null)
            {
                closing = closing.Where(a =>
                                 DbFunctions.TruncateTime(a.StockDate) >= DbFunctions.TruncateTime(input.StartDate)
                                 &&
                                 DbFunctions.TruncateTime(a.StockDate) <= DbFunctions.TruncateTime(input.EndDate));
                // rsRequest.Where(t => t.RequestTime >= input.StartDate && t.RequestTime <= input.EndDate);
            }

            var sortMenuItems = await closing
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<ClosingStockListDto>>();

            var allItemCount = await closing.CountAsync();

            return new PagedResultOutput<ClosingStockListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _closingstockRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<ClosingStockListDto>>();
            var baseE = new BaseExportObject()
            {
                ExportObject = allListDtos,
                ColumnNames = new string[] { "Id" }
            };
            return _exporter.ExportToFile(baseE, L("ClosingStock"));

        }

        public async Task<GetClosingStockForEditOutput> GetClosingStockForEdit(NullableIdInput input)
        {
            ClosingStockEditDto editDto;
            List<ClosingStockDetailViewDto> editDetailDto;

            if (input.Id.HasValue)
            {
                var hDto = await _closingstockRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<ClosingStockEditDto>();
                editDetailDto = await (from issDet in _closingstockDetailRepo.GetAll().Where(a => a.ClosingStockRefId == input.Id.Value)
                                       join mat in _materialRepo.GetAll()
                                       on issDet.MaterialRefId equals mat.Id
                                       join un in _unitRepo.GetAll()
                                       on mat.DefaultUnitId equals un.Id
                                       join uiss in _unitRepo.GetAll()
                                       on issDet.UnitRefId equals uiss.Id
                                       join matGroupCategory in _materialGroupCategoryRepo.GetAll()
                                       on mat.MaterialGroupCategoryRefId equals matGroupCategory.Id
                                       select new ClosingStockDetailViewDto
                                       {
                                           Id = issDet.Id,
                                           Barcode = mat.Barcode,
                                           MaterialRefId = issDet.MaterialRefId,
                                           MaterialRefName = mat.MaterialName,
                                           MaterialPetName = mat.MaterialPetName,
                                           UnitRefId = issDet.UnitRefId,
                                           UnitRefName = uiss.Name,
                                           OnHand = issDet.OnHand,
                                           MaterialGroupCategoryRefId = mat.MaterialGroupCategoryRefId,
                                           MaterialGroupCategoryRefName = matGroupCategory.MaterialGroupCategoryName
                                       }).ToListAsync();

                var arrClosingStockDetailRefIds = editDetailDto.Select(t => t.Id).ToList();

                var rsClStkUnitWiseDetails = await (from issUnitWiseDet in _closingStockUnitWiseDetailRepo.GetAll()
                                                    .Where(t => arrClosingStockDetailRefIds.Contains(t.ClosingStockDetailRefId))
                                                    join uiss in _unitRepo.GetAll()
                                                    on issUnitWiseDet.UnitRefId equals uiss.Id
                                                    select new ClosingStockUnitWiseDetailViewDto
                                                    {
                                                        Id = issUnitWiseDet.Id,
                                                        ClosingStockDetailRefId = issUnitWiseDet.ClosingStockDetailRefId,
                                                        OnHand = issUnitWiseDet.OnHand,
                                                        UnitRefId = issUnitWiseDet.UnitRefId,
                                                        UnitRefName = uiss.Name,
                                                    }).ToListAsync();

                foreach (var lst in editDetailDto)
                {
                    var unitWiseDetails = rsClStkUnitWiseDetails.Where(t => t.ClosingStockDetailRefId == lst.Id).ToList();
                    lst.UnitWiseDetails = unitWiseDetails;
                }
            }
            else
            {
                editDto = new ClosingStockEditDto();
                editDetailDto = new List<ClosingStockDetailViewDto>();
            }

            return new GetClosingStockForEditOutput
            {
                ClosingStock = editDto,
                ClosingStockDetail = editDetailDto
            };
        }

        public async Task<MessageOutput> ExistSameMaterialAlreadyInClosingStock(CreateOrUpdateClosingStockInput input)
        {
            MessageOutput output = new MessageOutput();
            output.SuccessFlag = true;
            var existSameDayAlready = await _closingstockRepo.FirstOrDefaultAsync(t => DbFunctions.TruncateTime(t.StockDate) == DbFunctions.TruncateTime(input.ClosingStock.StockDate));
            if (existSameDayAlready == null)
            {
                return output;
            }
            foreach (var lst in input.ClosingStockDetail)
            {
                var exists = await _closingstockDetailRepo.FirstOrDefaultAsync(t => t.ClosingStockRefId == existSameDayAlready.Id && t.MaterialRefId == lst.MaterialRefId);
                if (exists != null)
                {
                    var mat = await _materialRepo.FirstOrDefaultAsync(t => t.Id == lst.MaterialRefId);
                    output.Materials.Add(mat.MapTo<SimpleMaterialListDto>());
                    output.SuccessFlag = false;
                }
            }

            return output;

        }

        public async Task<IdInput> CreateOrUpdateClosingStock(CreateOrUpdateClosingStockInput input)
        {
            if (input.ClosingStock.Id.HasValue)
            {
                return await UpdateClosingStock(input);
            }
            else
            {
                return await CreateClosingStock(input);
            }
        }

        public async Task DeleteClosingStock(IdInput input)
        {
            var lstDetails = await _closingstockDetailRepo.GetAllListAsync(t => t.ClosingStockRefId == input.Id);
            foreach (var lst in lstDetails)
            {
                await _closingStockUnitWiseDetailRepo.DeleteAsync(t => t.ClosingStockDetailRefId == lst.Id);
                await _closingstockDetailRepo.DeleteAsync(lst.Id);
            }
            await _closingstockRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task<IdInput> UpdateClosingStock(CreateOrUpdateClosingStockInput input)
        {
            var item = await _closingstockRepo.GetAsync(input.ClosingStock.Id.Value);
            if (input.ClosingStockDetail == null || input.ClosingStockDetail.Count() == 0)
            {
                throw new UserFriendlyException(L("MinimumOneDetail"));
            }

            List<int> materialsToBeRetained = new List<int>();
            var dto = input.ClosingStock;

            if (input.ClosingStockDetail != null && input.ClosingStockDetail.Count > 0)
            {
                foreach (var items in input.ClosingStockDetail)
                {
                    var matRefId = items.MaterialRefId;
                    materialsToBeRetained.Add(matRefId);
                    var existingClosingStockDetail = await _closingstockDetailRepo.FirstOrDefaultAsync(u => u.ClosingStockRefId == dto.Id && u.MaterialRefId.Equals(matRefId));
                    if (existingClosingStockDetail == null)  //Add new record
                    {
                        ClosingStockDetail id = new ClosingStockDetail();
                        id.ClosingStockRefId = (int)dto.Id;
                        id.OnHand = items.OnHand;
                        id.MaterialRefId = items.MaterialRefId;
                        id.UnitRefId = items.UnitRefId;
                        var retId2 = await _closingstockDetailRepo.InsertAndGetIdAsync(id);

                        foreach (var lst in items.UnitWiseDetails)
                        {
                            ClosingStockUnitWiseDetail closingStockUnitWiseDetail = new ClosingStockUnitWiseDetail
                            {
                                ClosingStockDetailRefId = retId2,
                                OnHand = lst.OnHand,
                                UnitRefId = lst.UnitRefId,
                            };
                            var clstkUnitWiseId = await _closingStockUnitWiseDetailRepo.InsertOrUpdateAndGetIdAsync(closingStockUnitWiseDetail);
                        }
                    }
                    else
                    {
                        var editDetailDto = await _closingstockDetailRepo.GetAsync(existingClosingStockDetail.Id);
                        editDetailDto.ClosingStockRefId = (int)dto.Id;
                        editDetailDto.OnHand = items.OnHand;
                        editDetailDto.MaterialRefId = items.MaterialRefId;
                        editDetailDto.UnitRefId = items.UnitRefId;
                        var retId3 = await _closingstockDetailRepo.InsertOrUpdateAndGetIdAsync(editDetailDto);

                        List<int> unitRefIdstoBeRetained = new List<int>();

                        foreach (var lst in items.UnitWiseDetails)
                        {
                            var unitRefIds = lst.UnitRefId;
                            unitRefIdstoBeRetained.Add(matRefId);

                            var existingUnitDetail = await _closingStockUnitWiseDetailRepo.FirstOrDefaultAsync(u => u.ClosingStockDetailRefId == editDetailDto.Id && u.UnitRefId == lst.UnitRefId);
                            if (existingUnitDetail == null)  //Add new record
                            {
                                ClosingStockUnitWiseDetail closingStockUnitWiseDetail = new ClosingStockUnitWiseDetail
                                {
                                    ClosingStockDetailRefId = editDetailDto.Id,
                                    OnHand = lst.OnHand,
                                    UnitRefId = lst.UnitRefId,
                                };
                                var clstkUnitWiseId = await _closingStockUnitWiseDetailRepo.InsertOrUpdateAndGetIdAsync(closingStockUnitWiseDetail);
                            }
                            else
                            {
                                if (existingUnitDetail.OnHand != lst.OnHand)
                                {
                                    existingUnitDetail.OnHand = lst.OnHand;
                                    var clstkUnitWiseId = await _closingStockUnitWiseDetailRepo.InsertOrUpdateAndGetIdAsync(existingUnitDetail);
                                }
                            }
                        }

                        var delUnitWiseList = await _closingStockUnitWiseDetailRepo.GetAllListAsync(t => t.ClosingStockDetailRefId == t.Id && !unitRefIdstoBeRetained.Contains(t.UnitRefId));
                        foreach (var lst in delUnitWiseList)
                        {
                            await _closingStockUnitWiseDetailRepo.DeleteAsync(lst.Id);
                        }
                    }
                }
            }

            var delDetailsList = await _closingstockDetailRepo.GetAll().Where(a => a.ClosingStockRefId == input.ClosingStock.Id.Value && !materialsToBeRetained.Contains(a.MaterialRefId)).ToListAsync();

            foreach (var a in delDetailsList)
            {
                var delUnitWiseList = await _closingStockUnitWiseDetailRepo.GetAllListAsync(t => t.ClosingStockDetailRefId == a.Id);
                foreach (var lst in delUnitWiseList)
                {
                    await _closingStockUnitWiseDetailRepo.DeleteAsync(lst.Id);
                }
                await _closingstockDetailRepo.DeleteAsync(a.Id);
            }

            CheckErrors(await _closingstockManager.CreateSync(item));

            return new IdInput
            {
                Id = input.ClosingStock.Id.Value
            };
        }

        protected virtual async Task<IdInput> CreateClosingStock(CreateOrUpdateClosingStockInput input)
        {
            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.ClosingStock.LocationRefId);
            if (loc == null)
            {
                throw new UserFriendlyException(L("LocationErr"));
            }

            var dto = input.ClosingStock.MapTo<ClosingStock>();
            dto.TenantId = loc.TenantId;
            var retId = await _closingstockRepo.InsertAndGetIdAsync(dto);

            if (input.ClosingStockDetail == null || input.ClosingStockDetail.Count() == 0)
            {
                throw new UserFriendlyException(L("MinimumOneDetail"));
            }

            foreach (ClosingStockDetailViewDto items in input.ClosingStockDetail.ToList())
            {
                ClosingStockDetail id = new ClosingStockDetail();
                id.ClosingStockRefId = (int)dto.Id;
                id.OnHand = items.OnHand;
                id.MaterialRefId = items.MaterialRefId;
                id.UnitRefId = items.UnitRefId;
                id.TenantId = loc.TenantId;
                var retId2 = await _closingstockDetailRepo.InsertAndGetIdAsync(id);

                foreach (var lst in items.UnitWiseDetails)
                {
                    ClosingStockUnitWiseDetail closingStockUnitWiseDetail = new ClosingStockUnitWiseDetail
                    {
                        ClosingStockDetailRefId = retId2,
                        OnHand = lst.OnHand,
                        UnitRefId = lst.UnitRefId,
                    };
                    var retId3 = await _closingStockUnitWiseDetailRepo.InsertOrUpdateAndGetIdAsync(closingStockUnitWiseDetail);
                }
            }

            //await SaveTemplateClosingStock(input);

            return new IdInput
            {
                Id = retId
            };
        }

        public async Task SaveTemplateClosingStock(CreateOrUpdateClosingStockInput input)
        {
            string templateJson = _xmlandjsonConvertorAppService.SerializeToJSON(input);

            Template lastTemplate;
            int? templateScope;
            if (input.Templatesave == null || input.Templatesave.Templatescope == true)
            {
                templateScope = null;
                return;
            }
            else
            {
                templateScope = input.ClosingStock.LocationRefId;
            }

            if (input.Templatesave.Templateflag == true)
            {
                string repName = input.Templatesave.Templatename;

                var templateExist = await _templateRepo.GetAll().Where(t => t.Name == repName && t.TemplateType == (int)TemplateType.ClosingStockEntry).ToListAsync();
                if (templateExist.Count == 0)
                {
                    lastTemplate = new Template
                    {
                        Name = input.Templatesave.Templatename,
                        TemplateType = (int)TemplateType.ClosingStockEntry,
                        TemplateData = templateJson,
                        LocationRefId = templateScope
                    };
                }
                else
                {
                    lastTemplate = templateExist[0];
                    lastTemplate.LocationRefId = templateScope;
                    lastTemplate.TemplateData = templateJson;
                }
            }
            else
            {
                string repName = L("LastClosingStock");
                var templateExist = _templateRepo.GetAll().Where(t => t.Name == repName && t.TemplateType == (int)TemplateType.ClosingStockEntry).ToList();
                if (templateExist.Count() == 0)
                {
                    lastTemplate = new Template
                    {
                        Name = L("LastClosingStock"),
                        TemplateType = (int)TemplateType.ClosingStockEntry,
                        TemplateData = templateJson,
                        LocationRefId = input.ClosingStock.LocationRefId
                    };
                }
                else
                {
                    lastTemplate = templateExist[0];
                    lastTemplate.LocationRefId = templateScope;
                    lastTemplate.TemplateData = templateJson;
                }
            }

            await _templateRepo.InsertOrUpdateAndGetIdAsync(lastTemplate);
        }

        public async Task<GetClosingStockForEditOutput> GetTemplateObjectForEdit(GetObjectFromString input)
        {
            GetClosingStockForEditOutput ReturnObject = _xmlandjsonConvertorAppService.DeSerializeFromJSON<GetClosingStockForEditOutput>(input.ObjectString);

            ClosingStockEditDto editDto = ReturnObject.ClosingStock;

            editDto.StockDate = DateTime.Today;
            editDto.Id = null;


            List<ClosingStockDetailViewDto> editDetailDto = ReturnObject.ClosingStockDetail;

            editDetailDto = (from det in editDetailDto
                             join mat in _materialRepo.GetAll()
                             on det.MaterialRefId equals mat.Id
                             join un in _unitRepo.GetAll()
                             on mat.DefaultUnitId equals un.Id
                             join uiss in _unitRepo.GetAll()
                             on det.UnitRefId equals uiss.Id
                             select new ClosingStockDetailViewDto
                             {
                                 Id = det.Id,
                                 MaterialRefId = det.MaterialRefId,
                                 MaterialRefName = mat.MaterialName,
                                 UnitRefId = det.UnitRefId,
                                 UnitRefName = uiss.Name,
                                 OnHand = 0,
                             }).ToList();


            return new GetClosingStockForEditOutput
            {
                ClosingStock = editDto,
                ClosingStockDetail = editDetailDto
            };
        }

        public async Task<ClosingStockExists> GetClosingExists(GetClosingStockExistInput input)
        {
            ClosingStockExists output = new ClosingStockExists();
            var rsMas = await _closingstockRepo.GetAllListAsync(t => t.LocationRefId == input.LocationRefId &&  DbFunctions.TruncateTime(t.StockDate) == DbFunctions.TruncateTime(input.Dt.Value));
            if (rsMas.Count == 0)
            {
                output.Count = 0;
            }
            else
            {
                var arrIds = rsMas.Select(t => t.Id).ToArray();
                var rsDet = await _closingstockDetailRepo.GetAllListAsync(t => arrIds.Contains(t.ClosingStockRefId));
                output.Count = rsDet.Count;
            }
            return output;
        }


        public async Task<ClosingStockExists> IsLocationClosingStockExists(LocationWithDate input)
        {
            ClosingStockExists output = new ClosingStockExists();
            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
            if (loc == null)
            {
                throw new UserFriendlyException(L("Location") + input.LocationRefId + L("NotExists"));
            }
            var rsMas = await _closingstockRepo.GetAllListAsync(t => t.LocationRefId == input.LocationRefId
                  && DbFunctions.TruncateTime(t.StockDate) == DbFunctions.TruncateTime(input.StockTakenDate));
            if (rsMas.Count == 0)
            {
                output.Count = 0;
            }
            else
            {
                var arrIds = rsMas.Select(t => t.Id).ToArray();
                var rsDet = await _closingstockDetailRepo.GetAllListAsync(t => arrIds.Contains(t.Id));
                output.Count = rsDet.Count;
            }
            return output;
        }

    }
}