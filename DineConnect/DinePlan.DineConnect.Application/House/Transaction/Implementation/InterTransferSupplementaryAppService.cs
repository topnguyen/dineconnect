﻿#region Include
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.House.Master;
using DinePlan.DineConnect.Common;
using DinePlan.DineConnect.Connect.Master.Dtos;
using System.Globalization;
using System.Threading;
using DinePlan.DineConnect.Connect.Location;
using Castle.Core.Logging;
using System.Diagnostics;
using DinePlan.DineConnect.House.Impl;
#endregion  

namespace DinePlan.DineConnect.House.Transaction.Implementation
{

    public class InterTransferSupplementaryAppService : DineConnectAppServiceBase, IInterTransferSupplementaryAppService
    {
        private readonly IRepository<InterTransfer> _intertransferRepo;
        private readonly IRepository<InterTransferDetail> _intertransferDetailRepo;
        private readonly IRepository<PurchaseOrder> _purchaseOrderRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<MaterialGroupCategory> _materialGroupCategoryRepo;
        private readonly ILocationAppService _locService;


        public InterTransferSupplementaryAppService(IInterTransferManager intertransferManager,
           IRepository<InterTransfer> interTransferRepo,
           IRepository<InterTransferDetail> intertransferDetailRepo,
           IRepository<Material> materialRepo,
           IRepository<Location> locationRepo,
           IRepository<MaterialGroupCategory> materialGroupCategoryRepo,
           ILocationAppService locService,
           IRepository<PurchaseOrder> purchaseOrderRepo
            )
        {
            _intertransferRepo = interTransferRepo;
            _intertransferDetailRepo = intertransferDetailRepo;
            _locationRepo = locationRepo;
            _materialRepo = materialRepo;
            _materialGroupCategoryRepo = materialGroupCategoryRepo;
            _locService = locService;
            _purchaseOrderRepo = purchaseOrderRepo;
        }

        public async Task<PagedResultOutput<InterTransferViewDto>> GetViewForApproved(GetInterTransferInput input)
        {
            Debug.WriteLine("InterTransfer Suppli GetViewForApproved Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
            var rsInterTrans = _intertransferRepo.GetAll(); //.WhereIf(input.DefaultLocationRefId > 0, t => t.RequestLocationRefId == input.DefaultLocationRefId);
            #region Location Filter
            if (input.DefaultLocationRefId > 0)
            {
                rsInterTrans = rsInterTrans.Where(a => a.RequestLocationRefId == input.DefaultLocationRefId);
            }

            if (input.OmitAlreadyPoLinkedRequest)
            {
                rsInterTrans = rsInterTrans.Where(t => t.DoesPOCreatedForThisRequest == false);
            }

            if (input.Locations != null && input.Locations.Any())
            {
                var locations = input.Locations.Select(a => a.Id).ToList();
                rsInterTrans = rsInterTrans.Where(a => locations.Contains(a.LocationRefId));
            }
            else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
                     && !input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                if (locations.Any())
                {
                    rsInterTrans = rsInterTrans.Where(a => locations.Contains(a.LocationRefId));
                }
            }
            else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                rsInterTrans = rsInterTrans.Where(a => locations.Contains(a.LocationRefId));
            }
            else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true
                });
                rsInterTrans = rsInterTrans.Where(a => locations.Contains(a.LocationRefId));
            }
            else if (input.LocationGroup == null)
            {
                if (input.UserId > 0)
                {
                    var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                    {
                        Locations = new List<SimpleLocationDto>(),
                        Group = false,
                        UserId = input.UserId
                    });
                    rsInterTrans = rsInterTrans.Where(a => locations.Contains(a.LocationRefId));
                }
            }
            #endregion

            if (input.StartDate != null && input.EndDate != null)
            {
                rsInterTrans = rsInterTrans.Where(a =>
                                 DbFunctions.TruncateTime(a.RequestDate) >= DbFunctions.TruncateTime(input.StartDate)
                                 &&
                                 DbFunctions.TruncateTime(a.RequestDate) <= DbFunctions.TruncateTime(input.EndDate));
            }

            if (input.DeliveryStartDate != null && input.DeliveryEndDate != null)
            {
                rsInterTrans = rsInterTrans.Where(a =>
                                 DbFunctions.TruncateTime(a.DeliveryDateRequested) >= DbFunctions.TruncateTime(input.DeliveryStartDate)
                                 &&
                                 DbFunctions.TruncateTime(a.DeliveryDateRequested) <= DbFunctions.TruncateTime(input.DeliveryEndDate));
            }

            if (input.ShowOnlyDeliveryDateExpectedAsOnDate)
            {
                rsInterTrans = rsInterTrans.Where(a => a.CompletedTime.HasValue == false &&
                                DbFunctions.TruncateTime(a.DeliveryDateRequested) <= DbFunctions.TruncateTime(DateTime.Today));
            }
            if (input.TransferRequestIds.Count > 0)
            {
                rsInterTrans = rsInterTrans.Where(t => input.TransferRequestIds.Contains(t.Id));
            }

            if (input.Id.HasValue)
                rsInterTrans = rsInterTrans.Where(t => t.Id == input.Id.Value);

            IQueryable<InterTransferViewDto> allItems;

            if (input.MaterialGroupRefId > 0 || input.MaterialGroupCategoryRefId > 0)
            {
                allItems = (from req in rsInterTrans
                            join loc in _locationRepo.GetAll().WhereIf(!input.Filter.IsNullOrEmpty(), t => t.Name.Contains(input.Filter))
                            on req.LocationRefId equals loc.Id
                            join locReq in _locationRepo.GetAll()
                            on req.RequestLocationRefId equals locReq.Id
                            join reqDetail in _intertransferDetailRepo.GetAll() on req.Id equals reqDetail.InterTransferRefId
                            join mat in _materialRepo.GetAll().WhereIf(input.MaterialGroupCategoryRefId > 0, t => t.MaterialGroupCategoryRefId == input.MaterialGroupCategoryRefId) on reqDetail.MaterialRefId equals mat.Id
                            join matGroupCategory in _materialGroupCategoryRepo.GetAll()
                                .WhereIf(input.MaterialGroupRefId > 0, t => t.MaterialGroupRefId == input.MaterialGroupRefId)
                                .WhereIf(input.MaterialGroupCategoryRefId > 0, t => t.Id == input.MaterialGroupCategoryRefId)
                            on mat.MaterialGroupCategoryRefId equals matGroupCategory.Id
                            select new InterTransferViewDto
                            {
                                Id = req.Id,
                                LocationRefId = req.LocationRefId,
                                LocationRefName = loc.Name,
                                RequestLocationRefId = req.RequestLocationRefId,
                                RequestLocationRefName = locReq.Name,
                                CurrentStatus = req.CurrentStatus,
                                RequestDate = req.RequestDate,
                                DeliveryDateRequested = req.DeliveryDateRequested,
                                CompletedTime = req.CompletedTime,
                                ReceivedTime = req.ReceivedTime,
                                TransferValue = req.TransferValue,
                                CreationTime = req.CreationTime,
                                PersonInCharge = req.PersonInCharge,
                                VehicleNumber = req.VehicleNumber,
                                DoesThisRequestToBeConvertedAsPO = req.DoesPOCreatedForThisRequest,
                                ParentRequestRefId = req.ParentRequestRefId
                            }).Distinct();
            }
            else
            {
                allItems = (from req in rsInterTrans
                            join loc in _locationRepo.GetAll().WhereIf(!input.Filter.IsNullOrEmpty(), t => t.Name.Contains(input.Filter))
                            on req.LocationRefId equals loc.Id
                            join locReq in _locationRepo.GetAll()
                            on req.RequestLocationRefId equals locReq.Id
                            select new InterTransferViewDto
                            {
                                Id = req.Id,
                                LocationRefId = req.LocationRefId,
                                LocationRefName = loc.Name,
                                RequestLocationRefId = req.RequestLocationRefId,
                                RequestLocationRefName = locReq.Name,
                                CurrentStatus = req.CurrentStatus,
                                RequestDate = req.RequestDate,
                                DeliveryDateRequested = req.DeliveryDateRequested,
                                CompletedTime = req.CompletedTime,
                                ReceivedTime = req.ReceivedTime,
                                TransferValue = req.TransferValue,
                                CreationTime = req.CreationTime,
                                PersonInCharge = req.PersonInCharge,
                                VehicleNumber = req.VehicleNumber,
                                DoesThisRequestToBeConvertedAsPO = req.DoesPOCreatedForThisRequest,
                                ParentRequestRefId = req.ParentRequestRefId
                            });
            }

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();


            var allListDtos = sortMenuItems.MapTo<List<InterTransferViewDto>>();

            List<InterTransferViewDto> lsttobeRemoved = new List<InterTransferViewDto>();
            List<int> arrRequestIds = allListDtos.Select(t => t.Id).ToList();
            var tempInterTransferDetails = await _intertransferDetailRepo.GetAllListAsync(t => arrRequestIds.Contains(t.InterTransferRefId));
            var rsDetails = tempInterTransferDetails.MapTo<List<InterTransferDetailListDto>>();

            var arrPoIds = rsDetails.Where(t => t.PurchaseOrderRefId.HasValue).Select(t => t.PurchaseOrderRefId.Value).ToList();
            var tempPoMaster = await _purchaseOrderRepo.GetAllListAsync(t => arrPoIds.Contains(t.Id));
            var rsPoMaster = tempPoMaster.MapTo<List<PurchaseOrderListDto>>();
            foreach (var item in allListDtos)
            {
                if (item.CurrentStatus.Equals("Q"))
                    item.CurrentStatus = L("RequestApprovalPending");
                else if (item.CurrentStatus.Equals("P"))
                {
                    item.CurrentStatus = L("Pending");
                    if (item.DeliveryDateRequested.Date <= DateTime.Today.Date)
                        item.DeliverDateAlertFlag = true;

                }
                else if (item.CurrentStatus.Equals("A"))
                    item.CurrentStatus = L("Approved");
                else if (item.CurrentStatus.Equals("R"))
                    item.CurrentStatus = L("Rejected");
                else if (item.CurrentStatus.Equals("D"))
                    item.CurrentStatus = L("Drafted");
                else if (item.CurrentStatus.Equals("V"))
                    item.CurrentStatus = L("Received");
                else if (item.CurrentStatus.Equals("I"))
                    item.CurrentStatus = L("PartiallyReceived");

                if (input.RequestStatus.Equals("A"))
                {
                    if (item.CurrentStatus != L("Approved"))
                        lsttobeRemoved.Add(item);
                }

                var details = rsDetails.Where(t => t.InterTransferRefId == item.Id).ToList();
                var countDetails = details.Count();
                item.NoOfMaterials = countDetails;
                var poDependentCount = details.Where(t => t.PurchaseOrderRefId.HasValue == true).Count();
                var stockDependentCount = details.Where(t => t.PurchaseOrderRefId.HasValue == false).Count();
                item.NoOfPoDependent = poDependentCount;
                item.NoOfStockDependent = stockDependentCount;

                if (item.NoOfStockDependent > 0 && item.NoOfPoDependent > 0)
                {
                    item.DoesSplitNeeded = true;
                }
                else if (item.NoOfStockDependent > 0 && item.NoOfPoDependent == 0)
                {
                    item.NoOfStockDependent = 0;
                }

                if (item.DoesThisRequestToBeConvertedAsPO)
                {
                    var poIds = details.Where(t => t.PurchaseOrderRefId.HasValue).Select(t => t.PurchaseOrderRefId.Value).ToList();
                    var poMaster = rsPoMaster.Where(t => poIds.Contains(t.Id)).ToList();
                    foreach (var lst in poMaster)
                    {
                        if (lst.PoCurrentStatus.Equals("P"))
                        {
                            lst.PoCurrentStatus = L("Pending");
                        }
                        else if (lst.PoCurrentStatus.Equals("A"))
                        {
                            lst.PoCurrentStatus = L("Approved");
                        }
                        else if (lst.PoCurrentStatus.Equals("X"))
                        {
                            lst.PoCurrentStatus = L("Declined");
                        }
                        else if (lst.PoCurrentStatus.Equals("C"))
                        {
                            lst.PoCurrentStatus = L("Completed");
                        }
                        else if (lst.PoCurrentStatus.Equals("H"))
                        {
                            lst.PoCurrentStatus = L("Partial");
                        }
                    }

                    var poStatusList = (from mas in poMaster
                                        group mas by new { mas.PoCurrentStatus } into g
                                        select new PoStatusWiseList
                                        {
                                            PoCurrentStatus = g.Key.PoCurrentStatus,
                                            PurchaseOrderList = g.ToList()
                                        }).ToList();
                    item.PoStatusWiseLists = poStatusList;
                    foreach (var lst in poStatusList)
                    {
                        item.PoStatusRemarks = item.PoStatusRemarks + lst.PoCurrentStatus + "( " + lst.PurchaseOrderList.Count() + " ) ";
                    }

                }
            }

            //  Remove the Non Approved only for ReceivedView
            foreach (var l in lsttobeRemoved)
            {
                allListDtos.Remove(l);
            }

            if (!input.CurrentStatus.IsNullOrEmpty())
            {
                //Get the culture property of the thread.
                CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                //Create TextInfo object.
                TextInfo textInfo = cultureInfo.TextInfo;

                input.CurrentStatus = input.CurrentStatus.ToLowerInvariant();

                var searchStatus = textInfo.ToTitleCase(input.CurrentStatus);

                allListDtos = allListDtos.Where(t => t.CurrentStatus.Contains(searchStatus)).ToList();
                //   allListDtos = allListDtos.FindAll(t => t.PoCurrentStatus.Contains(input.PoStatus));
            }

            var allItemCount = allItems.Count();
            Debug.WriteLine("InterTransfer GetViewForApproved End Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
            return new PagedResultOutput<InterTransferViewDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<SetInterTransferDashBoardDto> GetDashBoardTransferApproval(IdInput input)
        {
            var lstpending = await _intertransferRepo.GetAll().
                Where(p => p.RequestLocationRefId == input.Id && (p.CurrentStatus.Equals("Q") || p.CurrentStatus.Equals("D") || p.CurrentStatus.Equals("P"))).ToListAsync();
            int pendingCount = lstpending.Count();

            var lstApproved = await _intertransferRepo.GetAll().
                Where(p => p.RequestLocationRefId == input.Id && p.CurrentStatus.Equals("A")).ToListAsync();
            int approvedCount = lstApproved.Count();

            var lstReceived = await _intertransferRepo.GetAll().
                Where(p => p.RequestLocationRefId == input.Id && p.ReceivedTime > DateTime.Today && (p.CurrentStatus.Equals("I") || p.CurrentStatus.Equals("V"))).ToListAsync();
            int receivedCount = lstReceived.Count();

            var lstDeclined = await _intertransferRepo.GetAll().
                Where(p => p.RequestLocationRefId == input.Id && p.CompletedTime >= DateTime.Today && p.CurrentStatus.Equals("R")).ToListAsync();

            int declinedCount = lstDeclined.Count();

            return new SetInterTransferDashBoardDto
            {
                TotalApproved = approvedCount,
                TotalPending = pendingCount,
                TotalReceived = receivedCount,
                TotalRejected = declinedCount
            };
        }

    }
}
