﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace DinePlan.DineConnect.House.Transaction.Implementation
{
    public class  TextFileWriter
    {
        public int lineSize;                                // LineSize 80 or 132
        static int currentLineNo;                           //  Current Line No
        static int cursorPosition = 1;                      //  Current Position
        public static string FileName;

        static StringBuilder sb = new StringBuilder();

        public void FileOpen(string argFilename, string argFileOpenMode)
        {
            FileName = argFilename;
            if (argFileOpenMode.ToUpper() == "O")
                if (System.IO.File.Exists(argFilename))
                    System.IO.File.Delete(FileName);
        }

        private static void initialiseClass()
        {
            cursorPosition = 1;
            currentLineNo = 1;
        }

        public TextFileWriter()
        {
            initialiseClass();
        }

        public TextFileWriter(string argFileName)
        {
            initialiseClass();
            FileOpen(argFileName, "O");
        }

        

        public void Tab(int argTab)
        {
            if (cursorPosition < argTab)
            {
                int spaceRequired;
                spaceRequired = argTab - cursorPosition;
                sb.Append(new string(' ', spaceRequired));
                cursorPosition = cursorPosition + spaceRequired;
            }
            else if (cursorPosition == argTab)
            {

            }
            else
            {
                sb.AppendLine();
                currentLineNo++;
                cursorPosition = 1;
                int spaceRequired;
                spaceRequired = argTab - cursorPosition;
                sb.Append(new string(' ', spaceRequired));
                cursorPosition = cursorPosition + spaceRequired;
            }
        }

        public void PrintSepLine(char chartoPrint, int lineLength)
        {
            PrintTextLine(new string(chartoPrint, lineLength));
        }


        public  string PrintFormatRight(string src, int strlength)
        {
            if (strlength > src.Length)
            {
                return string.Concat(new string(' ', strlength - src.Length), src);
            }
            else
            {
                return src;
            }
        }

        public  string PrintFormatLeft(string src, int strlength)
        {
            if (strlength > src.Length)
            {
                return string.Concat(src, new string(' ', strlength - src.Length));
            }
            else
            {
                return src;
            }
        }

        public  string PrintFormatCenter(string src, int strlength)
        {
            int spaceHalf;
            spaceHalf = (strlength - src.Length) / 2;
            if (strlength > src.Length)
            {
                return string.Concat(new string(' ', spaceHalf), src, new string(' ', spaceHalf));
            }
            else
            {
                return src;
            }
        }

        public void PrintTextLine(string argToPrint)
        {
            PrintText(argToPrint);
            currentLineNo++;
            sb.AppendLine();
            PrintText("");
            cursorPosition = 1;
        }

        public void PrintText(string argToPrint)
        {
            sb.Append(argToPrint);
            cursorPosition = cursorPosition + argToPrint.Length;

            try
            {
                using (StreamWriter outfile = new StreamWriter(FileName, true))
                {
                    outfile.Write(sb.ToString());
                }
                sb = new StringBuilder();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }
    }
}
