﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using System;
using DinePlan.DineConnect.Common.Dto;

namespace DinePlan.DineConnect.House.Transaction
{
    public interface IInwardDirectCreditAppService : IApplicationService
    {
        Task<PagedResultOutput<InwardDirectCreditListDto>> GetAll(GetInwardDirectCreditInput inputDto);
        Task<PagedResultOutput<InwardDirectCreditWithSupplierName>> GetAllWithSupplierNames(GetInwardDirectCreditInput inputDto);

        Task<PagedResultOutput<InwardDirectCreditView>> GetAllView(GetInwardDirectCreditInput input);
        Task<PagedResultOutput<InwardDirectCreditDetailListDto>> GetAllInwardDcDetail(GetInwardDirectCreditDetailInput inputDto);
        //Task<PagedResultOutput<TempInwardDirectCreditDetailListDto>> GetAllTempInwardDcDetail(GetInwardDirectCreditDetailInput input);
        Task<FileDto> GetAllToExcel(GetInwardDirectCreditInput input);
        Task<GetInwardDirectCreditForEditOutput> GetInwardDirectCreditForEdit(InputIdWithPrintFlag nullableIdInput);
        Task<InvoiceWithAdjustmentId> CreateOrUpdateInwardDirectCredit(CreateOrUpdateInwardDirectCreditInput input);
        Task DeleteInwardDirectCredit(IdInput input);
        Task<ListResultOutput<InwardDirectCreditListDto>> GetInwardDirectCreditIds();

        Task<ListResultOutput<InwardDirectCreditListDto>> GetInwardDcTranid(InputDateRangeWithSupplierId input);
        Task<ListResultOutput<ComboboxItemDto>> GetInwardPoTranid(InputDateRangeWithSupplierId input);

        Task<PrintOutIn40Cols> GetPrintData(IdInput input);

    }
}

