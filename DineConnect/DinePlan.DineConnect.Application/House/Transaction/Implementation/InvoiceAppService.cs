﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using System;
using Abp.BackgroundJobs;
using Abp.Domain.Uow;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.House.Master;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Common;
using DinePlan.DineConnect.Job.House;
using Abp.Configuration;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Features;
using DinePlan.DineConnect.Connect.Location;
using System.Diagnostics;

namespace DinePlan.DineConnect.House.Transaction.Implementation
{
    public class InvoiceAppService : DineConnectAppServiceBase, IInvoiceAppService
    {
        private readonly IInvoiceListExcelExporter _invoiceExporter;
        private readonly IInvoiceManager _invoiceManager;
        private readonly IRepository<Invoice> _invoiceRepo;
        private readonly IRepository<InvoiceDetail> _invoicedetailRepo;
        private readonly IRepository<InvoiceTaxDetail> _invoicetaxdetailRepo;
        private readonly IRepository<InvoiceDirectCreditLink> _invoiceDirectCreditLinkRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<MaterialGroup> _materialGroupRepo;
        private readonly IRepository<MaterialGroupCategory> _materialGroupCategoryRepo;
        private readonly IRepository<InwardDirectCredit> _inwarddirectcreditRepo;
        private readonly IRepository<InwardDirectCreditDetail> _inwarddirectcreditDetailRepo;
        private readonly IRepository<SupplierMaterial> _suppliermaterialRepo;
        private readonly IRepository<Unit> _unitRepo;
        private readonly IRepository<UnitConversion> _unitConversionRepo;
        private readonly IRepository<Tax> _taxRepo;
        private readonly IRepository<TaxTemplateMapping> _taxtemplatemappingRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IMaterialAppService _materialAppService;
        private readonly IRepository<Supplier> _supplierRepo;
        private readonly IInwardDirectCreditAppService _inwarddirectcreditAppService;
        private readonly IRepository<PurchaseOrder> _purchaseorderRepo;
        private readonly IRepository<PurchaseOrderDetail> _purchaseorderdetailRepo;
        private readonly IRepository<PurchaseOrderTaxDetail> _purchaseorderTaxRepo;
        private readonly IRepository<MaterialLocationWiseStock> _materiallocationwisestockRepo;
        private readonly IRepository<PurchaseReturn> _purchaseReturnRepo;
        private readonly IRepository<PurchaseReturnDetail> _purchasereturnDetailRepo;
        private readonly IUnitConversionAppService _unitConversionAppService;
        private readonly IXmlAndJsonConvertor _xmlandjsonConvertorAppService;
        private readonly IRepository<Template> _templateRepo;
        private readonly IRepository<PurchaseCategory> _purchaseCategoryRepo;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Adjustment> _adjustmentRepo;
        private readonly IRepository<AdjustmentDetail> _adjustmentDetailRepo;
        private readonly ILocationAppService _locService;
        private readonly IBackgroundJobManager _bgm;

        public InvoiceAppService(IInvoiceManager invoiceManager,
            IRepository<Invoice> invoiceRepo,
            IInvoiceListExcelExporter invoiceExporter,
            IRepository<InvoiceDetail> invoicedetailRepo,
            IRepository<InvoiceDirectCreditLink> invoiceDirectCreditLinkRepo,
            IRepository<Material> materialRepo,
            IRepository<InwardDirectCredit> inwarddirectcreditRepo,
            IRepository<InwardDirectCreditDetail> inwarddirectcreditDetailRepo,
            IRepository<SupplierMaterial> suppliermaterialRepo,
            IRepository<Unit> unitRepo,
            IRepository<Tax> taxRepo,
            IRepository<TaxTemplateMapping> taxtemplatemappingRepo,
            IRepository<Location> locationRepo,
            IMaterialAppService materialAppService,
            IRepository<InvoiceTaxDetail> invoicetaxdetailRepo,
            IRepository<Supplier> supplierRepo,
            IInwardDirectCreditAppService inwarddirectcreditAppService,
            IRepository<MaterialGroupCategory> materialGroupCategoryRepo,
            IRepository<PurchaseOrder> purchaseorderRepo,
            IRepository<PurchaseOrderDetail> purchaseorderdetailRepo,
            IRepository<PurchaseOrderTaxDetail> purchaseorderTaxRepo,
            IRepository<UnitConversion> unitConversionRepo,
            IRepository<MaterialLocationWiseStock> materiallocationwisestockRepo,
            IRepository<PurchaseReturn> purchaseReturnRepo,
            IRepository<PurchaseReturnDetail> purchasereturnDetailRepo,
            IUnitConversionAppService unitConversionAppService,
            IXmlAndJsonConvertor xmlandjsonConvertorAppService,
            IRepository<Template> templateRepo,
            IRepository<PurchaseCategory> purchaseCategoryRepo, IUnitOfWorkManager unitOfWorkManager,
            //IAdjustmentAppService adjustmentAppService
            IRepository<Adjustment> adjustmentRepo,
            IRepository<AdjustmentDetail> adjustmentDetailRepo, IBackgroundJobManager bgm,
            IRepository<MaterialGroup> materialGroupRepo,
            ILocationAppService locService
            )
        {
            _invoiceManager = invoiceManager;
            _invoiceRepo = invoiceRepo;
            _invoiceExporter = invoiceExporter;
            _invoicedetailRepo = invoicedetailRepo;
            _invoiceDirectCreditLinkRepo = invoiceDirectCreditLinkRepo;
            _materialRepo = materialRepo;
            _inwarddirectcreditRepo = inwarddirectcreditRepo;
            _inwarddirectcreditDetailRepo = inwarddirectcreditDetailRepo;
            _suppliermaterialRepo = suppliermaterialRepo;
            _unitRepo = unitRepo;
            _taxRepo = taxRepo;
            _taxtemplatemappingRepo = taxtemplatemappingRepo;
            _locationRepo = locationRepo;
            _invoicetaxdetailRepo = invoicetaxdetailRepo;
            _materialAppService = materialAppService;
            _supplierRepo = supplierRepo;
            _inwarddirectcreditAppService = inwarddirectcreditAppService;
            _materialGroupCategoryRepo = materialGroupCategoryRepo;
            _purchaseorderRepo = purchaseorderRepo;
            _purchaseorderdetailRepo = purchaseorderdetailRepo;
            _purchaseorderTaxRepo = purchaseorderTaxRepo;
            _unitConversionRepo = unitConversionRepo;
            _materiallocationwisestockRepo = materiallocationwisestockRepo;
            _purchaseReturnRepo = purchaseReturnRepo;
            _purchasereturnDetailRepo = purchasereturnDetailRepo;
            _unitConversionAppService = unitConversionAppService;
            _xmlandjsonConvertorAppService = xmlandjsonConvertorAppService;
            _templateRepo = templateRepo;
            _purchaseCategoryRepo = purchaseCategoryRepo;
            _unitOfWorkManager = unitOfWorkManager;
            _adjustmentRepo = adjustmentRepo;
            _adjustmentDetailRepo = adjustmentDetailRepo;
            _bgm = bgm;
            _materialGroupRepo = materialGroupRepo;
            _locService = locService;
        }

        public async Task<PagedResultOutput<InvoiceListDto>> GetAll(GetInvoiceInput input)
        {
            Debug.WriteLine("InvoiceAppService GetAll() Start Time" + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
            var rsSupplier = _supplierRepo.GetAll().WhereIf(!input.SupplierName.IsNullOrEmpty(), p => p.SupplierName.Contains(input.SupplierName));

            var rsInvoice = _invoiceRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId).WhereIf(!input.InvoiceNumber.IsNullOrEmpty(), t => t.InvoiceNumber.Contains(input.InvoiceNumber));

            if (input.StartDate != null && input.EndDate != null)
            {
                rsInvoice = rsInvoice.Where(t => t.InvoiceDate >= input.StartDate && t.InvoiceDate <= input.EndDate);
            }


            var allItems = (from inv in rsInvoice.WhereIf(input.Id.HasValue, t => t.Id == input.Id)
                            join sup in rsSupplier
                            on inv.SupplierRefId equals sup.Id
                            select new InvoiceListDto
                            {
                                InvoiceAmount = inv.InvoiceAmount,
                                InvoiceDate = inv.InvoiceDate,
                                AccountDate = inv.AccountDate,
                                InvoiceNumber = inv.InvoiceNumber,
                                SupplierRefId = inv.SupplierRefId,
                                SupplierRefName = sup.SupplierName,
                                TotalShipmentCharges = inv.TotalShipmentCharges,
                                CreationTime = inv.CreationTime,
                                Id = inv.Id,
                                IsDirectInvoice = inv.IsDirectInvoice,
                                PurchaseCategoryRefId = inv.PurchaseCategoryRefId
                            });

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<InvoiceListDto>>();

            var allItemCount = await allItems.CountAsync();
            Debug.WriteLine("InvoiceAppService GetAll() End Time" + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
            return new PagedResultOutput<InvoiceListDto>(
                allItemCount,
                allListDtos
                );
        }


        public async Task<FileDto> GetInvoiceSummaryAllToExcel(GetInvoiceInput input)
        {
            input.MaxResultCount = 100000;
            var allList = await GetInvoiceSearch(input);
            var allListDtos = allList.Items.MapTo<List<InvoiceListDto>>();
            return _invoiceExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDcMaterialDataDetail> GetMaterialsFromDcId(GetDcMaterialInfo input)
        {
            List<InvoiceDetailViewDto> lstMaterial;

            lstMaterial = await (from inw in _inwarddirectcreditDetailRepo.GetAll()
                                    .Where(a => a.DcRefId == input.DcRefId)
                                 join mat in _materialRepo.GetAll()
                                 on inw.MaterialRefId equals mat.Id
                                 join un in _unitRepo.GetAll()
                                 on mat.DefaultUnitId equals un.Id
                                 join uiss in _unitRepo.GetAll()
                                 on inw.UnitRefId equals uiss.Id
                                 select new InvoiceDetailViewDto
                                 {
                                     Sno = 0,
                                     Id = 0,
                                     DcTranId = inw.DcRefId,
                                     MaterialRefId = inw.MaterialRefId,
                                     Barcode = mat.Barcode,
                                     MaterialRefName = mat.MaterialName,
                                     TotalQty = (inw.DcReceivedQty - inw.DcConvertedQty),
                                     TaxAmount = 0,
                                     InvoiceRefId = 0,
                                     DiscountFlag = "",
                                     DiscountPercentage = 0,
                                     DiscountAmount = 0,
                                     TotalAmount = 0,
                                     Remarks = "",
                                     DefaultUnitId = mat.DefaultUnitId,
                                     Uom = un.Name,
                                     UnitRefId = inw.UnitRefId,
                                     UnitRefName = uiss.Name,
                                     YieldPercentage = inw.YieldPercentage,
                                     YieldExcessShortageQty = inw.YieldExcessShortageQty,
                                     YieldMode = inw.YieldMode
                                 }).ToListAsync();

            lstMaterial = lstMaterial.Where(t => t.TotalQty > 0).ToList();
            //int loopcnt = 0;

            int companyRefid = await _locationRepo.GetAll().Where(l => l.Id == input.LocationRefId).Select(l => l.CompanyRefId).FirstOrDefaultAsync();

            PurchaseOrder poMaster = new PurchaseOrder();
            List<PurchaseOrderDetail> poDetail = new List<PurchaseOrderDetail>();
            List<PurchaseOrderTaxDetail> poTaxDetail = new List<PurchaseOrderTaxDetail>();


            InwardDirectCredit inwardMaster;
            inwardMaster = await _inwarddirectcreditRepo.FirstOrDefaultAsync(t => t.Id == input.DcRefId);

            if (inwardMaster != null)
            {
                poMaster = await _purchaseorderRepo.FirstOrDefaultAsync(t => t.Id == inwardMaster.PoRefId);
                if (poMaster != null)
                {
                    poDetail = await _purchaseorderdetailRepo.GetAll().Where(t => t.PoRefId == poMaster.Id).ToListAsync();
                    poTaxDetail = await _purchaseorderTaxRepo.GetAll().Where(t => t.PoRefId == poMaster.Id).ToListAsync();
                }
            }

            var rsUnitConversion = await _unitConversionRepo.GetAll().ToListAsync();

            foreach (var mat in lstMaterial)
            {
                if (poDetail.Count == 0)
                {
                    var supmaterial = await _suppliermaterialRepo.FirstOrDefaultAsync(s => s.SupplierRefId == input.SupplierRefId && s.MaterialRefId == mat.MaterialRefId && s.LocationRefId == input.LocationRefId);
                    if (supmaterial == null)
                    {
                        supmaterial = await _suppliermaterialRepo.FirstOrDefaultAsync(s => s.SupplierRefId == input.SupplierRefId && s.MaterialRefId == mat.MaterialRefId && s.LocationRefId == null);
                    }
                    if (supmaterial != null)
                    {
                        decimal conversionFactor = 1;
                        if (mat.UnitRefId == supmaterial.UnitRefId)
                            conversionFactor = 1;
                        else
                        {
                            var conv =
                                rsUnitConversion.First(
                                    t => t.BaseUnitId == supmaterial.UnitRefId && t.RefUnitId == mat.UnitRefId);
                            if (conv != null)
                            {
                                conversionFactor = conv.Conversion;
                            }
                            else
                            {
                                var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == supmaterial.UnitRefId);
                                if (baseUnit == null)
                                {
                                    throw new UserFriendlyException(L("UnitIdDoesNotExist", supmaterial.UnitRefId));
                                }
                                var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == mat.UnitRefId);
                                if (refUnit == null)
                                {
                                    throw new UserFriendlyException(L("UnitIdDoesNotExist", mat.UnitRefId));
                                }
                                throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
                            }

                        }

                        mat.Price = supmaterial.MaterialPrice * 1 / conversionFactor;
                        mat.TotalAmount = Math.Round(mat.TotalQty * mat.Price, 2);
                    }
                }
                else
                {
                    var poMaterial = poDetail.FirstOrDefault(t => t.MaterialRefId == mat.MaterialRefId);
                    if (poMaterial != null)
                    {
                        decimal conversionFactor = 1;
                        if (mat.UnitRefId == poMaterial.UnitRefId)
                            conversionFactor = 1;
                        else
                        {
                            var conv =
                                rsUnitConversion.First(
                                    t => t.BaseUnitId == mat.UnitRefId && t.RefUnitId == poMaterial.UnitRefId);
                            if (conv != null)
                                conversionFactor = conv.Conversion;
                        }

                        mat.Price = poMaterial.Price * 1 / conversionFactor;
                        mat.TotalAmount = Math.Round(mat.TotalQty * mat.Price, 2);
                    }

                }

                List<TaxForMaterial> taxtoAdded = new List<TaxForMaterial>();
                decimal matTaxAmount = 0;

                // taxtemplate;
                // Find any specific TAX For particular Material
                List<TaxTemplateMapping> taxTemplates;
                taxTemplates = await _taxtemplatemappingRepo.GetAll().Where(t => t.MaterialRefId == mat.MaterialRefId).ToListAsync();

                if (taxTemplates.Count() == 0 || taxTemplates == null)
                {
                    // If not specific for mateial then need to  Find Any Sepcific Tax For Material Group Category id
                    taxTemplates = await _taxtemplatemappingRepo.GetAll().Where(t => t.MaterialGroupCategoryRefId == mat.MaterialGroupCategoryRefId).ToListAsync();
                }

                if (taxTemplates.Count() == 0 || taxTemplates == null)
                {
                    // If not specific for mateial Group Category then need to  Find Any Sepcific Tax For Material Group id
                    taxTemplates = await _taxtemplatemappingRepo.GetAll().Where(t => t.MaterialGroupRefId == mat.MaterialGroupRefId).ToListAsync();
                }

                if (taxTemplates.Count() == 0 || taxTemplates == null)
                {
                    // If not specific for mateial Group then need to  Find Any Sepcific Tax For Location
                    taxTemplates = await _taxtemplatemappingRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId).ToListAsync();
                }

                if (taxTemplates.Count() == 0 || taxTemplates == null)
                {
                    // If not specific for Location then need to  Find Any Sepcific Tax For Organization
                    taxTemplates = await _taxtemplatemappingRepo.GetAll().Where(t => t.CompanyRefId == companyRefid).ToListAsync();
                }

                if (taxTemplates.Count() == 0 || taxTemplates == null)
                {
                    // If not specific for all above need to find the default any
                    taxTemplates = await _taxtemplatemappingRepo.GetAll().Where(t => t.LocationRefId == null && t.CompanyRefId == null && t.MaterialGroupCategoryRefId == null && t.MaterialRefId == null).ToListAsync();
                }

                List<ApplicableTaxesForMaterial> applicabletaxes = new List<ApplicableTaxesForMaterial>();

                if (taxTemplates.Count() > 0)
                {
                    foreach (var taxtemplate in taxTemplates)
                    {
                        var tax = _taxRepo.Single(t => t.Id == taxtemplate.TaxRefId);
                        taxtoAdded.Add(new TaxForMaterial
                        {
                            MaterialRefId = mat.MaterialRefId,
                            TaxName = tax.TaxName,
                            TaxRefId = taxtemplate.TaxRefId,
                            TaxRate = tax.Percentage,
                            TaxValue = Math.Round((mat.TotalAmount * tax.Percentage / 100), 2),
                            SortOrder = tax.SortOrder,
                            Rounding = tax.Rounding,
                            TaxCalculationMethod = tax.TaxCalculationMethod,
                        }
                        );
                    }


                    List<Tax> taxesforMaterial = new List<Tax>();
                    if (poTaxDetail.Count > 0)
                    {
                        int[] taxids = poTaxDetail.Where(t => t.PoRefId == poMaster.Id && t.MaterialRefId == mat.MaterialRefId).Select(t => t.TaxRefId).ToArray();
                        taxesforMaterial = await _taxRepo.GetAll().Where(t => taxids.Contains(t.Id)).ToListAsync();
                    }
                    else if (inwardMaster != null)
                    {
                        int[] taxids = taxtoAdded.Where(t => t.MaterialRefId == mat.MaterialRefId).Select(t => t.TaxRefId).ToArray();
                        taxesforMaterial = await _taxRepo.GetAll().Where(t => taxids.Contains(t.Id)).ToListAsync();
                    }

                    foreach (var tax in taxesforMaterial.OrderBy(t => t.SortOrder))
                    {
                        matTaxAmount = matTaxAmount + Math.Round((mat.TotalAmount * tax.Percentage / 100), 2);
                        applicabletaxes.Add(new ApplicableTaxesForMaterial
                        {
                            TaxName = tax.TaxName,
                            TaxRefId = tax.Id,
                            TaxRate = tax.Percentage,
                            SortOrder = tax.SortOrder,
                            Rounding = tax.Rounding,
                            TaxCalculationMethod = tax.TaxCalculationMethod,
                        });
                    }

                }

                mat.TaxAmount = matTaxAmount;
                mat.TaxForMaterial = taxtoAdded.OrderBy(t => t.SortOrder).ToList();
                mat.ApplicableTaxes = applicabletaxes;

                mat.NetAmount = mat.TotalAmount + mat.TaxAmount;
                taxtoAdded = null;
                matTaxAmount = 0;
            }
            
            return new GetDcMaterialDataDetail
            {
                DcDetail = lstMaterial,
            };
        }


        public async Task<GetDcMaterialDataDetail> GetMaterialsFromPoId(GetPoMaterialInfo input)
        {
            List<InvoiceDetailViewDto> lstMaterial;
            

            lstMaterial = await (from pod in _purchaseorderdetailRepo.GetAll()
                                    .Where(a => a.PoRefId == input.PoRefId)
                                 join mat in _materialRepo.GetAll()
                                 on pod.MaterialRefId equals mat.Id
                                 join un in _unitRepo.GetAll()
                                 on mat.DefaultUnitId equals un.Id
                                 join uiss in _unitRepo.GetAll()
                                 on pod.UnitRefId equals uiss.Id
                                 select new InvoiceDetailViewDto
                                 {
                                     Sno = 0,
                                     Id = 0,
                                     DcTranId = 0,
                                     PoRefId = pod.PoRefId,
                                     MaterialRefId = pod.MaterialRefId,
                                     Barcode = mat.Barcode,
                                     MaterialRefName = mat.MaterialName,
                                     //TotalQty = (pod.DcReceivedQty - pod.DcConvertedQty),
                                     TotalQty = pod.QtyOrdered - pod.QtyReceived,
                                     TaxAmount = 0,
                                     InvoiceRefId = 0,
                                     DiscountFlag = "",
                                     Price = pod.Price,
                                     DiscountPercentage = 0,
                                     DiscountAmount = 0,
                                     TotalAmount = 0,
                                     Remarks = "",
                                     DefaultUnitId = mat.DefaultUnitId,
                                     Uom = un.Name,
                                     UnitRefId = pod.UnitRefId,
                                     UnitRefName = uiss.Name,
                                     //YieldPercentage = pod.YieldPercentage,
                                     //YieldExcessShortageQty = inw.YieldExcessShortageQty,
                                     //YieldMode = inw.YieldMode
                                 }).ToListAsync();

            lstMaterial = lstMaterial.Where(t => t.TotalQty > 0).ToList();
            int loopcnt = 0;

            int companyRefid = await _locationRepo.GetAll().Where(l => l.Id == input.LocationRefId).Select(l => l.CompanyRefId).FirstOrDefaultAsync();

            PurchaseOrder poMaster = new PurchaseOrder();
            List<PurchaseOrderDetail> poDetail = new List<PurchaseOrderDetail>();
            List<PurchaseOrderTaxDetail> poTaxDetail = new List<PurchaseOrderTaxDetail>();


            InwardDirectCredit inwardMaster;
            inwardMaster = await _inwarddirectcreditRepo.FirstOrDefaultAsync(t => t.Id == input.PoRefId);

            //if (inwardMaster != null)
            {
                poMaster = await _purchaseorderRepo.FirstOrDefaultAsync(t => t.Id == input.PoRefId);
                if (poMaster != null)
                {
                    poDetail = await _purchaseorderdetailRepo.GetAll().Where(t => t.PoRefId == poMaster.Id).ToListAsync();
                    poTaxDetail = await _purchaseorderTaxRepo.GetAll().Where(t => t.PoRefId == poMaster.Id).ToListAsync();
                }
            }

            var rsUnitConversion = await _unitConversionRepo.GetAll().ToListAsync();

            foreach (var mat in lstMaterial)
            {
                if (poDetail.Count == 0)
                {
                    var supmaterial = await _suppliermaterialRepo.FirstOrDefaultAsync(s => s.SupplierRefId == input.SupplierRefId && s.MaterialRefId == mat.MaterialRefId && s.LocationRefId == input.LocationRefId);
                    if (supmaterial == null)
                    {
                        supmaterial = await _suppliermaterialRepo.FirstOrDefaultAsync(s => s.SupplierRefId == input.SupplierRefId && s.MaterialRefId == mat.MaterialRefId && s.LocationRefId == null);
                    }
                    if (supmaterial != null)
                    {
                        decimal conversionFactor = 1;
                        if (mat.UnitRefId == supmaterial.UnitRefId)
                            conversionFactor = 1;
                        else
                        {
                            var conv =
                                rsUnitConversion.First(
                                    t => t.BaseUnitId == supmaterial.UnitRefId && t.RefUnitId == mat.UnitRefId);
                            if (conv != null)
                            {
                                conversionFactor = conv.Conversion;
                            }
                            else
                            {
                                var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == supmaterial.UnitRefId);
                                if (baseUnit == null)
                                {
                                    throw new UserFriendlyException(L("UnitIdDoesNotExist", supmaterial.UnitRefId));
                                }
                                var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == mat.UnitRefId);
                                if (refUnit == null)
                                {
                                    throw new UserFriendlyException(L("UnitIdDoesNotExist", mat.UnitRefId));
                                }
                                throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
                            }

                        }

                        lstMaterial[loopcnt].Price = supmaterial.MaterialPrice * 1 / conversionFactor;
                        lstMaterial[loopcnt].TotalAmount = Math.Round(lstMaterial[loopcnt].TotalQty * lstMaterial[loopcnt].Price, 2);
                    }
                }
                else
                {
                    var poMaterial = poDetail.FirstOrDefault(t => t.MaterialRefId == mat.MaterialRefId);
                    if (poMaterial != null)
                    {
                        decimal conversionFactor = 1;
                        if (mat.UnitRefId == poMaterial.UnitRefId)
                            conversionFactor = 1;
                        else
                        {
                            var conv =
                                rsUnitConversion.First(
                                    t => t.BaseUnitId == mat.UnitRefId && t.RefUnitId == poMaterial.UnitRefId);
                            if (conv != null)
                                conversionFactor = conv.Conversion;
                        }

                        lstMaterial[loopcnt].Price = poMaterial.Price * 1 / conversionFactor;
                        lstMaterial[loopcnt].TotalAmount = Math.Round(lstMaterial[loopcnt].TotalQty * lstMaterial[loopcnt].Price, 2);
                    }

                }

                List<TaxForMaterial> taxtoAdded = new List<TaxForMaterial>();
                decimal matTaxAmount = 0;

                // taxtemplate;
                // Find any specific TAX For particular Material
                List<TaxTemplateMapping> taxTemplates;
                taxTemplates = await _taxtemplatemappingRepo.GetAll().Where(t => t.MaterialRefId == mat.MaterialRefId).ToListAsync();

                if (taxTemplates.Count() == 0 || taxTemplates == null)
                {
                    // If not specific for mateial then need to  Find Any Sepcific Tax For Material Group Category id
                    taxTemplates = await _taxtemplatemappingRepo.GetAll().Where(t => t.MaterialGroupCategoryRefId == mat.MaterialGroupCategoryRefId).ToListAsync();
                }

                if (taxTemplates.Count() == 0 || taxTemplates == null)
                {
                    // If not specific for mateial Group Category then need to  Find Any Sepcific Tax For Material Group id
                    taxTemplates = await _taxtemplatemappingRepo.GetAll().Where(t => t.MaterialGroupRefId == mat.MaterialGroupRefId).ToListAsync();
                }

                if (taxTemplates.Count() == 0 || taxTemplates == null)
                {
                    // If not specific for mateial Group then need to  Find Any Sepcific Tax For Location
                    taxTemplates = await _taxtemplatemappingRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId).ToListAsync();
                }

                if (taxTemplates.Count() == 0 || taxTemplates == null)
                {
                    // If not specific for Location then need to  Find Any Sepcific Tax For Organization
                    taxTemplates = await _taxtemplatemappingRepo.GetAll().Where(t => t.CompanyRefId == companyRefid).ToListAsync();
                }

                if (taxTemplates.Count() == 0 || taxTemplates == null)
                {
                    // If not specific for all above need to find the default any
                    taxTemplates = await _taxtemplatemappingRepo.GetAll().Where(t => t.LocationRefId == null && t.CompanyRefId == null && t.MaterialGroupCategoryRefId == null && t.MaterialRefId == null).ToListAsync();
                }

                List<ApplicableTaxesForMaterial> applicabletaxes = new List<ApplicableTaxesForMaterial>();

                if (taxTemplates.Count() > 0)
                {
                    foreach (var taxtemplate in taxTemplates)
                    {
                        var tax = _taxRepo.Single(t => t.Id == taxtemplate.TaxRefId);
                        taxtoAdded.Add(new TaxForMaterial
                        {
                            MaterialRefId = mat.MaterialRefId,
                            TaxName = tax.TaxName,
                            TaxRefId = taxtemplate.TaxRefId,
                            TaxRate = tax.Percentage,
                            TaxValue = Math.Round((mat.TotalAmount * tax.Percentage / 100), 2),
                            SortOrder = tax.SortOrder,
                            Rounding = tax.Rounding,
                            TaxCalculationMethod = tax.TaxCalculationMethod,
                        }
                        );
                    }


                    List<Tax> taxesforMaterial = new List<Tax>();
                    if (poTaxDetail.Count > 0)
                    {
                        int[] taxids = poTaxDetail.Where(t => t.PoRefId == poMaster.Id && t.MaterialRefId == mat.MaterialRefId).Select(t => t.TaxRefId).ToArray();
                        taxesforMaterial = await _taxRepo.GetAll().Where(t => taxids.Contains(t.Id)).ToListAsync();
                    }
                    else if (inwardMaster != null)
                    {
                        int[] taxids = taxtoAdded.Where(t => t.MaterialRefId == mat.MaterialRefId).Select(t => t.TaxRefId).ToArray();
                        taxesforMaterial = await _taxRepo.GetAll().Where(t => taxids.Contains(t.Id)).ToListAsync();
                    }

                    foreach (var tax in taxesforMaterial.OrderBy(t => t.SortOrder))
                    {
                        matTaxAmount = matTaxAmount + Math.Round((mat.TotalAmount * tax.Percentage / 100), 2);
                        applicabletaxes.Add(new ApplicableTaxesForMaterial
                        {
                            TaxName = tax.TaxName,
                            TaxRefId = tax.Id,
                            TaxRate = tax.Percentage,
                            SortOrder = tax.SortOrder,
                            Rounding = tax.Rounding,
                            TaxCalculationMethod = tax.TaxCalculationMethod,
                        });
                    }

                }

                lstMaterial[loopcnt].TaxAmount = matTaxAmount;
                lstMaterial[loopcnt].TaxForMaterial = taxtoAdded.OrderBy(t => t.SortOrder).ToList();
                lstMaterial[loopcnt].ApplicableTaxes = applicabletaxes;

                lstMaterial[loopcnt].NetAmount = lstMaterial[loopcnt].TotalAmount + lstMaterial[loopcnt].TaxAmount;
                taxtoAdded = null;
                matTaxAmount = 0;
                loopcnt++;
            }

            //poMaster = await _purchaseorderRepo.FirstOrDefaultAsync(t => t.Id == input.PoRefId);

            return new GetDcMaterialDataDetail
            {
                DeliverDateRequested = poMaster.DeliveryDateExpected,
                DcDetail = lstMaterial,
            };
        }


        public async Task<GetInvoiceForEditOutput> GetInvoiceForEdit(NullableIdInput input)
        {
            Debug.WriteLine("InvoiceAppService GetInvoiceForEdit() Start Time" + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
            InvoiceEditDto editDto;
            List<InvoiceDetailViewDto> editDetailDto;
            List<InvoiceDirectCreditLinkViewDto> editInvoiceDCLink;
            if (input.Id.HasValue)
            {
                var hDto = await _invoiceRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<InvoiceEditDto>();

                var user = await UserManager.GetUserByIdAsync(editDto.CreatorUserId);
                editDto.CreatedBy = user.Name;

                var rsSupplier = await _supplierRepo.FirstOrDefaultAsync(t => t.Id == editDto.SupplierRefId);
                if (rsSupplier != null)
                {
                    editDto.SupplierRefName = rsSupplier.SupplierName;
                    editDto.SupplierCode = rsSupplier.SupplierCode;
                    editDto.SupplierTaxApplicableByDefault = rsSupplier.TaxApplicable;
                }

                var rsLoc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == editDto.LocationRefId);
                if (rsLoc != null)
                {
                    editDto.LocationRefName = rsLoc.Name;
                }

                if (editDto.IsDirectInvoice == false)
                {
                    editDetailDto = await (from invDet in _invoicedetailRepo.GetAll()
                        .Where(a => a.InvoiceRefId == input.Id.Value)
                                           join mat in _materialRepo.GetAll() on invDet.MaterialRefId equals mat.Id
                                           join unit in _unitRepo.GetAll() on invDet.UnitRefId equals unit.Id
                                           join invdclnk in _invoiceDirectCreditLinkRepo.GetAll()
                                           on new { a = invDet.InvoiceRefId, b = invDet.Sno }
                                           equals new { a = invdclnk.InvoiceRefId, b = invdclnk.Sno }
                                           where mat.Id == invdclnk.MaterialRefId
                                           join uiss in _unitRepo.GetAll()
                                           on invDet.UnitRefId equals uiss.Id
                                           select new InvoiceDetailViewDto
                                           {
                                               Id = invDet.Id,
                                               DiscountAmount = invDet.DiscountAmount,
                                               DcTranId = invdclnk.InwardDirectCreditRefId,
                                               Barcode = mat.Barcode,
                                               MaterialRefId = invDet.MaterialRefId,
                                               MaterialPetName = mat.MaterialPetName,
                                               MaterialRefName = mat.MaterialName,
                                               Hsncode = mat.Hsncode,
                                               InvoiceRefId = invDet.InvoiceRefId,
                                               DiscountFlag = invDet.DiscountFlag,
                                               DiscountPercentage = invDet.DiscountPercentage,
                                               NetAmount = invDet.NetAmount,
                                               Price = invDet.Price,
                                               TaxAmount = invDet.TaxAmount,
                                               TotalAmount = invDet.TotalAmount,
                                               TotalQty = invDet.TotalQty,
                                               Remarks = invDet.Remarks,
                                               Sno = invDet.Sno,
                                               Uom = unit.Name,
                                               UnitRefId = invDet.UnitRefId,
                                               UnitRefName = uiss.Name,
                                               YieldPercentage = invDet.YieldPercentage,
                                               YieldMode = invDet.YieldMode,
                                               YieldExcessShortageQty = invDet.YieldExcessShortageQty
                                           }).OrderBy(t => t.Sno).ToListAsync();
                }
                else
                {
                    editDetailDto = await (from invDet in _invoicedetailRepo.GetAll()
                                           .Where(a => a.InvoiceRefId == input.Id.Value)
                                           join mat in _materialRepo.GetAll() on invDet.MaterialRefId equals mat.Id
                                           join unit in _unitRepo.GetAll() on mat.DefaultUnitId equals unit.Id
                                           join uiss in _unitRepo.GetAll() on invDet.UnitRefId equals uiss.Id
                                           select new InvoiceDetailViewDto
                                           {
                                               Id = invDet.Id,
                                               DiscountAmount = invDet.DiscountAmount,
                                               DcTranId = 0,
                                               Barcode = mat.Barcode,
                                               MaterialRefId = invDet.MaterialRefId,
                                               MaterialPetName = mat.MaterialPetName,
                                               MaterialRefName = mat.MaterialName,
                                               Hsncode = mat.Hsncode,
                                               InvoiceRefId = invDet.InvoiceRefId,
                                               DiscountFlag = invDet.DiscountFlag,
                                               DiscountPercentage = invDet.DiscountPercentage,
                                               NetAmount = invDet.NetAmount,
                                               Price = invDet.Price,
                                               TaxAmount = invDet.TaxAmount,
                                               TotalAmount = invDet.TotalAmount,
                                               TotalQty = invDet.TotalQty,
                                               Remarks = invDet.Remarks,
                                               Sno = invDet.Sno,
                                               Uom = unit.Name,
                                               UnitRefId = invDet.UnitRefId,
                                               UnitRefName = uiss.Name,
                                               YieldPercentage = invDet.YieldPercentage,
                                               YieldMode = invDet.YieldMode,
                                               YieldExcessShortageQty = invDet.YieldExcessShortageQty
                                           }).OrderBy(t => t.Sno).ToListAsync();
                }

                editDto.SubTotalAmount = editDetailDto.Sum(t => t.TotalAmount);
                editDto.TaxAmount = editDetailDto.Sum(t => t.TaxAmount);



                var materialRefIds = editDetailDto.Select(t => t.MaterialRefId).ToList();
                var rsSupplierMaterials = await _suppliermaterialRepo.GetAllListAsync(t => t.SupplierRefId == editDto.SupplierRefId && materialRefIds.Contains(t.MaterialRefId));

                foreach (var lst in editDetailDto)
                {
                    var supmat = rsSupplierMaterials.FirstOrDefault(t => t.LocationRefId == editDto.LocationRefId && t.MaterialRefId == lst.MaterialRefId);
                    if (supmat == null)
                    {
                        supmat = rsSupplierMaterials.FirstOrDefault(t => t.LocationRefId == null && t.MaterialRefId == lst.MaterialRefId);
                    }
                    if (supmat != null)
                    {
                        lst.SupplierMaterialAliasName = supmat.SupplierMaterialAliasName;
                    }
                }




                int loopCnt = 0;
                foreach (var det in editDetailDto)
                {
                    var taxlist = await (from potax in _invoicetaxdetailRepo.GetAll()
                                            .Where(pot => pot.InvoiceRefId == input.Id.Value && pot.MaterialRefId == det.MaterialRefId)
                                         join tax in _taxRepo.GetAll() on potax.TaxRefId equals tax.Id
                                         select new TaxForMaterial
                                         {
                                             TaxRefId = potax.TaxRefId,
                                             TaxName = tax.TaxName,
                                             TaxRate = potax.TaxRate,
                                             TaxValue = potax.TaxValue,
                                             SortOrder = tax.SortOrder,
                                             Rounding = tax.Rounding
                                         }).ToListAsync();

                    editDetailDto[loopCnt].TaxForMaterial = taxlist;

                    List<ApplicableTaxesForMaterial> applicabletaxes = new List<ApplicableTaxesForMaterial>();

                    foreach (var tax in taxlist.OrderBy(t => t.SortOrder))
                    {
                        applicabletaxes.Add(new ApplicableTaxesForMaterial
                        {
                            TaxName = tax.TaxName,
                            TaxRefId = tax.TaxRefId,
                            TaxRate = tax.TaxRate,
                            SortOrder = tax.SortOrder,
                            Rounding = tax.Rounding,
                            TaxCalculationMethod = tax.TaxCalculationMethod,
                        });
                    }
                    editDetailDto[loopCnt].ApplicableTaxes = applicabletaxes;
                    loopCnt++;
                }

                editInvoiceDCLink = await (from invDC in _invoiceDirectCreditLinkRepo.GetAll().Where(a => a.InvoiceRefId == input.Id.Value)
                                           select new InvoiceDirectCreditLinkViewDto
                                           {
                                               Id = invDC.Id,
                                               InvoiceRefId = invDC.InvoiceRefId,
                                               InwardDirectCreditRefId = invDC.InwardDirectCreditRefId,
                                               MaterialRefId = invDC.MaterialRefId
                                           }).ToListAsync();
                string strPoReferenceCodes = "";
                string strInvReferenceCodes = "";
                if (editInvoiceDCLink.Count > 0)
                {
                    var arrInwCreditRefIds = editInvoiceDCLink.Select(t => t.InwardDirectCreditRefId).ToList();
                    var inwardMasterList = await _inwarddirectcreditRepo.GetAll().Where(t => arrInwCreditRefIds.Contains(t.Id)).ToListAsync();
                    var arrPoRefIds = inwardMasterList.Where(t => t.PoRefId.HasValue).Select(t => t.PoRefId.Value).ToList();
                    //var poMaster = await _purchaseorderRepo.GetAllListAsync(t => arrPoRefIds.Contains(t.Id));
                    //if (poMaster.Count > 0)
                    //{
                    //    foreach(var poMas in poMaster)
                    //    {
                    //        strPoReferenceCodes = strPoReferenceCodes + poMas.PoReferenceCode + ",";
                    //    }
                    //    if (strPoReferenceCodes.Length > 0)
                    //        strPoReferenceCodes = strPoReferenceCodes.Left(strPoReferenceCodes.Length - 1);
                    //}
                    if (inwardMasterList.Count > 0)
                    {
                        foreach (var im in inwardMasterList)
                        {
                            if (!im.PurchaseOrderReferenceFromOtherErp.IsNullOrEmpty())
                                strPoReferenceCodes = strPoReferenceCodes + im.PurchaseOrderReferenceFromOtherErp + ",";

                            if (!im.InvoiceNumberReferenceFromOtherErp.IsNullOrEmpty())
                                strInvReferenceCodes = strInvReferenceCodes + im.InvoiceNumberReferenceFromOtherErp + ",";
                        }
                        if (strPoReferenceCodes.Length > 0)
                            strPoReferenceCodes = strPoReferenceCodes.Left(strPoReferenceCodes.Length - 1);
                        if (strInvReferenceCodes.Length > 0)
                            strInvReferenceCodes = strInvReferenceCodes.Left(strInvReferenceCodes.Length - 1);
                    }
                }
                editDto.InvoiceNumberReferenceFromOtherErp = strInvReferenceCodes;
                editDto.PurchaseOrderReferenceFromOtherErp = strPoReferenceCodes;
            }
            else
            {
                editDto = new InvoiceEditDto();
                editDto.InvoiceDate = DateTime.Now.Date;
                editDto.AccountDate = DateTime.Now.Date;
                editDetailDto = new List<InvoiceDetailViewDto>();
                editInvoiceDCLink = new List<InvoiceDirectCreditLinkViewDto>();
            }
            Debug.WriteLine("InvoiceAppService GetInvoiceForEdit() End Time" + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
            return new GetInvoiceForEditOutput
            {
                Invoice = editDto,
                InvoiceDetail = editDetailDto,
                InvoiceDirectCreditLink = editInvoiceDCLink
            };
        }

        public async Task<InvoiceWithAdjustmentId> CreateOrUpdateInvoice(CreateOrUpdateInvoiceInput input)
        {
            var existAlready = await InvoiceAlreadyExists(new GetInvoiceInput
            {
                StartDate = input.Invoice.InvoiceDate,
                LocationRefId = input.Invoice.LocationRefId,
                SupplierRefId = input.Invoice.SupplierRefId
            });

            if (existAlready.TotalCount > 0)
            {
                throw new UserFriendlyException(L("InvoiceAlreadyExists", existAlready.Items[0].InvoiceNumber));
            }

            InvoiceWithAdjustmentId ret = new InvoiceWithAdjustmentId();

            if (input.Invoice.Id.HasValue)
            {
                //ret =  await UpdateInvoice(input);
            }
            else
            {
                if (input.Invoice.IsDirectInvoice == false)
                {
                    if (input.InvoiceDetail.Where(t => t.PoRefId.HasValue && t.DcTranId == 0).Count() > 0)
                    {
                        List<int> arrPoRefIds = input.InvoiceDetail.Where(t => t.PoRefId.HasValue && t.DcTranId == 0).Select(t => t.PoRefId.Value).ToList();
                        var tempPo = await _purchaseorderRepo.GetAll().Where(t => arrPoRefIds.Contains(t.Id)).Select(t => new { t.Id, t.PoReferenceCode }).ToListAsync();

                        foreach (var gp in input.InvoiceDetail.Where(t => t.PoRefId.HasValue && t.DcTranId == 0).GroupBy(t => t.PoRefId))
                        {
                            var purchaseOrder = tempPo.FirstOrDefault(t => t.Id == gp.Key);
                            if (purchaseOrder == null)
                                continue;
                            DateTime dcDate = input.Invoice.InvoiceDate;
                            if (input.Invoice.DcDate.HasValue)
                            {
                                dcDate = input.Invoice.DcDate.Value;
                            }
                            if (input.Invoice.DcDate > input.Invoice.InvoiceDate)
                                input.Invoice.DcDate = input.Invoice.InvoiceDate;
                            InwardDirectCreditEditDto inwardDirectCredit = new InwardDirectCreditEditDto
                            {
                                SupplierRefId = input.Invoice.SupplierRefId,
                                LocationRefId = input.Invoice.LocationRefId,
                                AccountDate = input.Invoice.AccountDate.Value,
                                DcDate = dcDate,
                                DcNumberGivenBySupplier = L("PO") + " " + purchaseOrder.PoReferenceCode + " " + L("Invoice") + " " + input.Invoice.InvoiceNumber,
                                PoRefId = gp.Key,
                                PoReference = purchaseOrder.PoReferenceCode
                            };
                            List<InwardDirectCreditDetailViewDto> inwardDirectCreditDetails = new List<InwardDirectCreditDetailViewDto>();

                            foreach (var mat in gp.ToList())
                            {
                                InwardDirectCreditDetailViewDto newDetailDto = new InwardDirectCreditDetailViewDto
                                {
                                    MaterialRefId = mat.MaterialRefId,
                                    DcReceivedQty = mat.TotalQty,
                                    DcConvertedQty = 0,
                                    UnitRefId = mat.UnitRefId,
                                    MaterialReceivedStatus = "Direct PO"
                                };
                                inwardDirectCreditDetails.Add(newDetailDto);
                            }
                            CreateOrUpdateInwardDirectCreditInput inwardInputDto = new CreateOrUpdateInwardDirectCreditInput
                            {
                                InwardDirectCredit = inwardDirectCredit,
                                InwardDirectCreditDetail = inwardDirectCreditDetails
                            };
                            var newDc = await _inwarddirectcreditAppService.CreateOrUpdateInwardDirectCredit(inwardInputDto);
                            foreach (var mat in gp.ToList())
                            {
                                mat.DcTranId = newDc.Id;
                            }
                            foreach (var mat in input.InvoiceDirectCreditLink.Where(t => t.PoRefId == gp.Key))
                            {
                                mat.InwardDirectCreditRefId = newDc.Id;
                            }
                        }
                    }
                }
                ret = await CreateInvoice(input);
                await SetDcCompleteStatus(new IdInput { Id = input.Invoice.SupplierRefId });

                await _bgm.EnqueueAsync<HouseIdentificationJob, HouseIdJobArgs>(new HouseIdJobArgs
                {
                    InvoiceId = ret.Id
                });
            }
            return ret;

        }



        public async Task DeleteInvoice(IdInput input)
        {
            var rsInvMas = await _invoiceRepo.FirstOrDefaultAsync(t => t.Id == input.Id);
            if (rsInvMas == null)
            {
                throw new UserFriendlyException(L("Invoice") + " " + input.Id + " " + L("NotExist"));
            }

            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == rsInvMas.LocationRefId);

            if (loc.HouseTransactionDate.Value.Date != rsInvMas.AccountDate.Date)
            {
                throw new UserFriendlyException(L("InvoiceDeleteError", loc.HouseTransactionDate.Value.ToString("yyyy-MMM-dd"), rsInvMas.AccountDate.Date.ToString("yyyy-MMM-dd"), rsInvMas.Id, rsInvMas.InvoiceNumber));
            }

            if (rsInvMas.IsDirectInvoice == false)
            {
                throw new UserFriendlyException("CanDeleteOnlyDirectInvoice");
            }

            var invoiceDetail = await _invoicedetailRepo.GetAll().Where(t => t.InvoiceRefId == input.Id).ToListAsync();
            foreach (var det in invoiceDetail)
            {
                MaterialLedgerDto ml = new MaterialLedgerDto();
                ml.LocationRefId = rsInvMas.LocationRefId;
                ml.MaterialRefId = det.MaterialRefId;
                ml.LedgerDate = rsInvMas.AccountDate;
                ml.Received = -1 * det.TotalQty;
                ml.UnitId = det.UnitRefId;
                ml.SupplierRefId = rsInvMas.SupplierRefId;

                var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);

                await _invoicetaxdetailRepo.DeleteAsync(u => u.InvoiceRefId == input.Id && u.MaterialRefId == det.MaterialRefId);

                await _invoicedetailRepo.DeleteAsync(det.Id);
            }

            await _invoiceRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task<InvoiceWithAdjustmentId> CreateInvoice(CreateOrUpdateInvoiceInput input)
        {
            var dto = input.Invoice.MapTo<Invoice>();

            if (input.Invoice.IsDirectInvoice == true)
            {
                dto.DcFromDate = dto.InvoiceDate.Date;
                dto.DcToDate = dto.InvoiceDate.Date;
            }

            if (input.InvoiceDetail == null || input.InvoiceDetail.Count() == 0)
            {
                throw new UserFriendlyException(L("MinimumOneDetail"));
            }


            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == dto.LocationRefId);

            if (loc.HouseTransactionDate.Value.Date == dto.InvoiceDate.Date && dto.InvoiceDate.Date==DateTime.Now.Date)
            {
                dto.InvoiceDate = DateTime.Now;
            }

            if (dto.DcFromDate == DateTime.MinValue)
            {
                dto.DcFromDate = dto.InvoiceDate.Date;
                dto.DcToDate = dto.InvoiceDate.Date;
            }

            var retId = await _invoiceRepo.InsertAndGetIdAsync(dto);

            var rsUnits = await _unitRepo.GetAllListAsync();
            List<AdjustmentDetailViewDto> adjustmentDetailDtos = new List<AdjustmentDetailViewDto>();
            int adjSno = 1;
            foreach (InvoiceDetailViewDto items in input.InvoiceDetail)
            {
                InvoiceDetail invdet = new InvoiceDetail();

                if (items.DiscountAmount > 0)
                {
                    items.DiscountFlag = "P";
                    items.DiscountPercentage = (items.DiscountAmount / items.TotalAmount) * 100;
                }

                invdet.InvoiceRefId = (int)dto.Id;
                invdet.Sno = items.Sno;
                invdet.MaterialRefId = items.MaterialRefId;
                invdet.TotalQty = items.TotalQty;
                invdet.Price = items.Price;
                invdet.TotalAmount = items.TotalAmount;
                invdet.DiscountFlag = items.DiscountFlag;
                invdet.DiscountPercentage = items.DiscountPercentage;
                invdet.DiscountAmount = items.DiscountAmount;
                invdet.TaxAmount = items.TaxAmount;
                invdet.NetAmount = items.NetAmount;
                invdet.Remarks = items.Remarks;
                invdet.UnitRefId = items.UnitRefId;
                invdet.YieldPercentage = items.YieldPercentage;
                invdet.YieldMode = items.YieldMode;
                invdet.YieldExcessShortageQty = items.YieldExcessShortageQty;


                var retId2 = await _invoicedetailRepo.InsertAndGetIdAsync(invdet);

                #region Adjustment
                AdjustmentDetailViewDto det = new AdjustmentDetailViewDto();
                if (input.Invoice.IsDirectInvoice && items.YieldPercentage.HasValue
                        && items.YieldExcessShortageQty.HasValue && items.YieldMode.Length > 0)
                {
                    if (items.YieldExcessShortageQty != 0)
                    {
                        decimal adjustmentQty = items.YieldExcessShortageQty.Value;
                        string adjustmentMode = items.YieldMode;

                        if (adjustmentMode.Equals(L("Shortage")))
                        {
                            // Do Nothing
                        }
                        else if (adjustmentMode.Equals(L("Excess")))
                        {
                            // Nothing
                        }
                        else
                        {
                            throw new UserFriendlyException(L("ShouldBeExcessOrShortage"));
                        }

                        if (items.UnitRefId == 0)
                        {
                            throw new UserFriendlyException(items.MaterialRefName + L("UnitErr"));
                        }

                        var unit = rsUnits.FirstOrDefault(t => t.Id == items.UnitRefId);

                        string adjustmentApprovedRemarks = L("PurchaseYieldAdjustmemt", retId, adjustmentQty, unit.Name, adjustmentMode, items.TotalQty);

                        det.MaterialRefId = items.MaterialRefId;
                        det.AdjustmentQty = adjustmentQty;
                        det.AdjustmentApprovedRemarks = adjustmentApprovedRemarks;
                        det.AdjustmentMode = adjustmentMode;
                        det.UnitRefId = items.UnitRefId;
                        det.Sno = adjSno++;

                        adjustmentDetailDtos.Add(det);
                    }
                }
                #endregion

                foreach (var tax in items.TaxForMaterial)
                {
                    InvoiceTaxDetail taxForPoDetail = new InvoiceTaxDetail
                    {
                        InvoiceRefId = (int)dto.Id,
                        Sno = items.Sno,
                        MaterialRefId = items.MaterialRefId,
                        TaxRefId = tax.TaxRefId,
                        TaxRate = tax.TaxRate,
                        TaxValue = tax.TaxValue,
                    };
                    var retId3 = await _invoicetaxdetailRepo.InsertAndGetIdAsync(taxForPoDetail);

                }
                if (input.Invoice.IsDirectInvoice == true)
                {
                    MaterialLedgerDto ml = new MaterialLedgerDto();
                    ml.LocationRefId = dto.LocationRefId;
                    ml.MaterialRefId = invdet.MaterialRefId;
                    ml.LedgerDate = dto.AccountDate.Date;
                    ml.Received = invdet.TotalQty;
                    ml.UnitId = items.UnitRefId;
                    ml.SupplierRefId = input.Invoice.SupplierRefId;
                    var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                    invdet.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;
                }
            }

            #region PurchaseYield_Adjustment
            IdInput adjId = new IdInput { Id = 0 };
            CreateOrUpdateAdjustmentInput newAdjDto = new CreateOrUpdateAdjustmentInput();
            if (adjustmentDetailDtos.Count > 0)
            {
                AdjustmentEditDto masDto = new AdjustmentEditDto();

                if (input.Invoice.AccountDate == null)
                {
                    masDto.AdjustmentDate = loc.HouseTransactionDate.Value.Date;
                }
                else
                    masDto.AdjustmentDate = input.Invoice.AccountDate.Value.Date;

                masDto.AdjustmentRemarks = L("PurchaseYield") + " " + L("Difference");
                masDto.ApprovedPersonId = (int)AbpSession.UserId;
                masDto.LocationRefId = dto.LocationRefId;
                masDto.EnteredPersonId = (int)AbpSession.UserId;
                masDto.TokenRefNumber = 333;

                newAdjDto.Adjustment = masDto;
                newAdjDto.AdjustmentDetail = adjustmentDetailDtos;

                adjId = await CreatePurchaseYieldAdjustment(newAdjDto);
            }

            if (adjId.Id > 0)
                dto.AdjustmentRefId = adjId.Id;
            #endregion

            if (input.Invoice.IsDirectInvoice == false)
            {
                int previousDirectCreditDcId = 0;
                foreach (InvoiceDirectCreditLinkViewDto itemsdc in input.InvoiceDirectCreditLink)
                {
                    if (itemsdc.InwardDirectCreditRefId == 0)
                    {
                        continue;
                    }
                    previousDirectCreditDcId = itemsdc.InwardDirectCreditRefId;

                    InvoiceDirectCreditLink idl = new InvoiceDirectCreditLink();
                    idl.InvoiceRefId = (int)dto.Id;
                    idl.Sno = itemsdc.Sno;
                    idl.InwardDirectCreditRefId = itemsdc.InwardDirectCreditRefId;
                    idl.MaterialRefId = itemsdc.MaterialRefId;

                    var retId3 = await _invoiceDirectCreditLinkRepo.InsertAndGetIdAsync(idl);

                    //  Updating Inward Direct Credit Detail Converted Qty and Status

                    var getDcDetoDetail = _inwarddirectcreditDetailRepo.GetAll().Where(a => a.MaterialRefId == itemsdc.MaterialRefId && a.DcRefId == itemsdc.InwardDirectCreditRefId).ToList();
                    if (getDcDetoDetail == null || getDcDetoDetail.Count() == 0)
                    {
                        throw new UserFriendlyException(L("ProblemInInwardDCConversion"));
                    }

                    var editDCDetail = await _inwarddirectcreditDetailRepo.GetAsync(getDcDetoDetail[0].Id);

                    if (editDCDetail == null)
                    {
                        throw new UserFriendlyException(L("ProblemInInwardDCConversion"));
                    }

                    if (itemsdc.UnitRefId == editDCDetail.UnitRefId)
                        editDCDetail.DcConvertedQty = editDCDetail.DcConvertedQty + itemsdc.MaterialConvertedQty;
                    else
                    {
                        var unitConversion = await _unitConversionRepo.FirstOrDefaultAsync(t => t.BaseUnitId == itemsdc.UnitRefId && t.RefUnitId == editDCDetail.UnitRefId);
                        if (unitConversion == null)
                        {
                            unitConversion = await _unitConversionRepo.FirstOrDefaultAsync(t => t.BaseUnitId == editDCDetail.UnitRefId && t.RefUnitId == itemsdc.UnitRefId);
                        }
                        if (unitConversion == null)
                        {
                            var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == editDCDetail.UnitRefId);
                            var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == itemsdc.UnitRefId);

                            throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
                        }
                        decimal existValue = editDCDetail.DcConvertedQty;
                        editDCDetail.DcConvertedQty = editDCDetail.DcConvertedQty + Math.Round((itemsdc.MaterialConvertedQty * unitConversion.Conversion), unitConversion.DecimalPlaceRounding);
                        if (Math.Abs(editDCDetail.DcConvertedQty - editDCDetail.DcReceivedQty) < (decimal)0.000009)
                        {
                            editDCDetail.DcConvertedQty = editDCDetail.DcReceivedQty;
                        }
                    }

                    if (editDCDetail.DcConvertedQty > editDCDetail.DcReceivedQty)
                    {
                        //@@Pending     How to Rollback Here if 
                        throw new UserFriendlyException(L("ProblemInInwardDCConversion"));
                    }

                    await _inwarddirectcreditDetailRepo.InsertOrUpdateAsync(editDCDetail);

                }


                //  Complete the Inward Direct Credit Dc Status
                foreach (InvoiceDirectCreditLinkViewDto itemsdc in input.InvoiceDirectCreditLink)
                {
                    if (itemsdc.InwardDirectCreditRefId == 0)
                    {
                        continue;
                    }
                    previousDirectCreditDcId = itemsdc.InwardDirectCreditRefId;

                    var CompletedDC = (from dcMas in _inwarddirectcreditDetailRepo.GetAll().Where(a => a.DcRefId == itemsdc.InwardDirectCreditRefId && a.DcReceivedQty != a.DcConvertedQty) select dcMas).ToList();

                    if (CompletedDC.Count == 0)
                    {
                        var editDCMaster = _inwarddirectcreditRepo.GetAll().Where(a => a.Id == itemsdc.InwardDirectCreditRefId).ToList();
                        if (editDCMaster != null)
                        {
                            var editInwMasterDto = await _inwarddirectcreditRepo.GetAsync(editDCMaster[0].Id);

                            editInwMasterDto.IsDcCompleted = true;

                            await _inwarddirectcreditRepo.InsertOrUpdateAsync(editInwMasterDto);
                        }
                    }

                }
            }

            CheckErrors(await _invoiceManager.CreateSync(dto));

            return new InvoiceWithAdjustmentId
            {
                Id = retId,
                AdjustmentId = adjId.Id
            };
        }

        public async Task<ListResultOutput<InvoiceListDto>> GetIds()
        {
            var lstInvoice = await _invoiceRepo.GetAll().ToListAsync();
            return new ListResultOutput<InvoiceListDto>(lstInvoice.MapTo<List<InvoiceListDto>>());
        }

        public virtual async Task SetDcCompleteStatus(IdInput input)
        {
            var lstCompleted = (from inwmas in _inwarddirectcreditRepo.GetAll()
                                    .Where(im => im.SupplierRefId == input.Id && im.IsDcCompleted == false)
                                join inwdet in _inwarddirectcreditDetailRepo.GetAll().Where(id => id.DcConvertedQty != id.DcReceivedQty)
                                on inwmas.Id equals inwdet.DcRefId into g
                                where g.Count() == 0
                                select new
                                {
                                    DcId = inwmas.Id,
                                    PendingCount = g.Count()
                                }).ToList();

            foreach (var l in lstCompleted)
            {

                var editMas = await _inwarddirectcreditRepo.GetAsync(l.DcId);
                editMas.IsDcCompleted = true;

                await _inwarddirectcreditRepo.InsertOrUpdateAsync(editMas);
            }

        }

        public async Task<PurchaseAnalysisOutputDto> GetPurchaseAnalysis(InputInvoiceAnalysis input)
        {
            var invoiceMaster = _invoiceRepo.GetAll();

            var invoiceDetail = _invoicedetailRepo.GetAll();

            if (input.SupplierRefId != 0)
            {
                invoiceMaster = invoiceMaster.Where(t => t.SupplierRefId == input.SupplierRefId);
            }

            if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
            {
                invoiceMaster =
                    invoiceMaster.Where(
                        a => DbFunctions.TruncateTime(a.InvoiceDate) >= DbFunctions.TruncateTime(input.StartDate)
                             && DbFunctions.TruncateTime(a.InvoiceDate) <= DbFunctions.TruncateTime(input.EndDate));
            }

            if (input.LocationRefId != 0)
            {
                invoiceMaster = invoiceMaster.Where(t => t.LocationRefId == input.LocationRefId);
            }

            if (input.MaterialRefId != 0)
            {
                invoiceDetail = invoiceDetail.Where(t => t.MaterialRefId == input.MaterialRefId);
            }

            if (input.InvoiceNumber != null)
            {
                if (input.ExactSearchFlag == false)
                    invoiceMaster = invoiceMaster.Where(t => t.InvoiceNumber.Contains(input.InvoiceNumber));
                else
                    invoiceMaster = invoiceMaster.Where(t => t.InvoiceNumber.Equals(input.InvoiceNumber));
            }


            var allItems = (from invmas in invoiceMaster
                            join invdet in invoiceDetail
                            on invmas.Id equals invdet.InvoiceRefId
                            join sup in _supplierRepo.GetAll()
                            on invmas.SupplierRefId equals sup.Id
                            join mat in _materialRepo.GetAll()
                            on invdet.MaterialRefId equals mat.Id
                            join unit in _unitRepo.GetAll()
                            on mat.DefaultUnitId equals unit.Id
                            join uiss in _unitRepo.GetAll() on invdet.UnitRefId equals uiss.Id
                            select new InvoiceAnalysisDto
                            {
                                InvoiceDate = invmas.InvoiceDate,
                                InvoiceNumber = invmas.InvoiceNumber,
                                InvoiceRefId = invmas.Id,
                                SupplierRefId = invmas.SupplierRefId,
                                SupplierRefName = sup.SupplierName,
                                MaterialRefId = invdet.MaterialRefId,
                                MaterialRefName = mat.MaterialName,
                                TotalQty = invdet.TotalQty,
                                Price = invdet.Price,
                                DiscountAmount = invdet.DiscountAmount,
                                DiscountFlag = invdet.DiscountFlag,
                                DiscountPercentage = invdet.DiscountPercentage,
                                TotalAmount = invdet.TotalAmount,
                                TaxAmount = invdet.TaxAmount,
                                NetAmount = invdet.NetAmount,
                                Remarks = invdet.Remarks,
                                Uom = unit.Name,
                                DefaultUnitId = mat.DefaultUnitId,
                                DefaultUnitName = unit.Name,
                                UnitRefId = invdet.UnitRefId,
                                UnitRefName = uiss.Name,
                            }).ToList();

            List<InvoiceAnalysisDto> sortMenuItems;

            sortMenuItems = allItems.ToList();

            var rsUnitConversion = await _unitConversionRepo.GetAllListAsync();

            //int loopIndex = 0;
            foreach (var lst in sortMenuItems)
            {
                decimal conversionFactor;
                decimal PricePerUom;

                if (lst.DefaultUnitId == lst.UnitRefId)
                {
                    conversionFactor = 1;
                }
                else
                {
                    var unitConversion = rsUnitConversion.FirstOrDefault(t => t.BaseUnitId == lst.DefaultUnitId && t.RefUnitId == lst.UnitRefId);
                    if (unitConversion != null)
                    {
                        conversionFactor = unitConversion.Conversion;
                    }
                    else
                    {
                        unitConversion = rsUnitConversion.FirstOrDefault(t => t.BaseUnitId == lst.UnitRefId && t.RefUnitId == lst.DefaultUnitId);
                        if (unitConversion == null)
                        {
                            var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == lst.UnitRefId);
                            var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == lst.DefaultUnitId);

                            throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
                        }
                        conversionFactor = 1 / unitConversion.Conversion;
                    }
                }
                PricePerUom = (lst.Price + lst.TaxAmount) * conversionFactor;

                lst.PriceForDefaultUnitWithTaxUOM = PricePerUom.ToString("N") + " / " + lst.DefaultUnitName;
            }

            var allListDtos = sortMenuItems.MapTo<List<InvoiceAnalysisDto>>();

            var allItemCount = allItems.Count();

            return new PurchaseAnalysisOutputDto { InvoiceList = allListDtos };
        }

        public async Task<PurchaseAnalysisOutputDto> GetPurchaseAnalysisConsolidated(InputInvoiceAnalysis input)
        {
            var invoiceMaster = _invoiceRepo.GetAll();

            var invoiceDetail = _invoicedetailRepo.GetAll();

            if (input.SupplierRefId != 0)
            {
                invoiceMaster = invoiceMaster.Where(t => t.SupplierRefId == input.SupplierRefId);
            }

            if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
            {
                invoiceMaster =
                    invoiceMaster.Where(
                        a => DbFunctions.TruncateTime(a.InvoiceDate) >= DbFunctions.TruncateTime(input.StartDate)
                             && DbFunctions.TruncateTime(a.InvoiceDate) <= DbFunctions.TruncateTime(input.EndDate));
            }

            if (input.LocationRefId != 0)
            {
                invoiceMaster = invoiceMaster.Where(t => t.LocationRefId == input.LocationRefId);
            }

            if (input.MaterialRefId != 0)
            {
                invoiceDetail = invoiceDetail.Where(t => t.MaterialRefId == input.MaterialRefId);
            }

            if (input.InvoiceNumber != null)
            {
                if (input.ExactSearchFlag == false)
                    invoiceMaster = invoiceMaster.Where(t => t.InvoiceNumber.Contains(input.InvoiceNumber));
                else
                    invoiceMaster = invoiceMaster.Where(t => t.InvoiceNumber.Equals(input.InvoiceNumber));
            }


            var allItems = (from invmas in invoiceMaster
                            join invdet in invoiceDetail
                            on invmas.Id equals invdet.InvoiceRefId
                            join sup in _supplierRepo.GetAll()
                            on invmas.SupplierRefId equals sup.Id
                            join mat in _materialRepo.GetAll()
                            on invdet.MaterialRefId equals mat.Id
                            join unit in _unitRepo.GetAll()
                            on mat.DefaultUnitId equals unit.Id
                            join uiss in _unitRepo.GetAll() on invdet.UnitRefId equals uiss.Id
                            select new InvoiceAnalysisDto
                            {
                                InvoiceDate = invmas.InvoiceDate,
                                InvoiceNumber = invmas.InvoiceNumber,
                                InvoiceRefId = invmas.Id,
                                SupplierRefId = invmas.SupplierRefId,
                                SupplierRefName = sup.SupplierName,
                                MaterialRefId = invdet.MaterialRefId,
                                MaterialRefName = mat.MaterialName,
                                TotalQty = invdet.TotalQty,
                                Price = invdet.Price,
                                DiscountAmount = invdet.DiscountAmount,
                                DiscountFlag = invdet.DiscountFlag,
                                DiscountPercentage = invdet.DiscountPercentage,
                                TotalAmount = invdet.TotalAmount,
                                TaxAmount = invdet.TaxAmount,
                                NetAmount = invdet.NetAmount,
                                Remarks = invdet.Remarks,
                                Uom = unit.Name,
                                DefaultUnitId = mat.DefaultUnitId,
                                DefaultUnitName = unit.Name,
                                UnitRefId = invdet.UnitRefId,
                                UnitRefName = uiss.Name,
                            }).ToList();

            List<InvoiceAnalysisDto> sortMenuItems;

            sortMenuItems = allItems.ToList();

            var rsUnitConversion = await _unitConversionRepo.GetAllListAsync();

            //int loopIndex = 0;
            foreach (var lst in sortMenuItems)
            {
                decimal conversionFactor;
                decimal PricePerUom;

                if (lst.DefaultUnitId == lst.UnitRefId)
                {
                    conversionFactor = 1;
                }
                else
                {
                    var unitConversion = rsUnitConversion.FirstOrDefault(t => t.BaseUnitId == lst.DefaultUnitId && t.RefUnitId == lst.UnitRefId);
                    if (unitConversion != null)
                    {
                        conversionFactor = unitConversion.Conversion;
                    }
                    else
                    {
                        unitConversion = rsUnitConversion.FirstOrDefault(t => t.BaseUnitId == lst.UnitRefId && t.RefUnitId == lst.DefaultUnitId);
                        if (unitConversion == null)
                        {
                            var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == lst.UnitRefId);
                            var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == lst.DefaultUnitId);

                            throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
                        }
                        conversionFactor = 1 / unitConversion.Conversion;
                    }
                }
                PricePerUom = (lst.Price + lst.TaxAmount) * conversionFactor;

                lst.PriceForDefaultUnitWithTaxUOM = PricePerUom.ToString("N") + " / " + lst.DefaultUnitName;
            }

            var allListDtos = sortMenuItems.MapTo<List<InvoiceAnalysisDto>>();

            var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();

            var matInvListGroupWithUnitConversion = (from salemenu in allListDtos
                                                     join uc in rsUc on salemenu.UnitRefId equals uc.BaseUnitId
                                                     where uc.RefUnitId == salemenu.DefaultUnitId
                                                     group salemenu by new
                                                     {
                                                         salemenu.MaterialRefId,
                                                         salemenu.MaterialRefName,
                                                         salemenu.DefaultUnitId,
                                                         salemenu.DefaultUnitName,
                                                         uc.Conversion,
                                                         salemenu.Price,
                                                     } into g
                                                     select new InvoiceAnalysisDto
                                                     {
                                                         MaterialRefId = g.Key.MaterialRefId,
                                                         MaterialRefName = g.Key.MaterialRefName,
                                                         DefaultUnitId = g.Key.DefaultUnitId,
                                                         DefaultUnitName = g.Key.DefaultUnitName,
                                                         Price = g.Key.Price * 1 / g.Key.Conversion,
                                                         TotalQty = g.Sum(t => t.TotalQty * g.Key.Conversion),
                                                         TotalAmount = g.Sum(t => t.TotalAmount),
                                                     }).ToList().OrderBy(t => t.MaterialRefId).ToList();

            //var matInvList = (from salemenu in matInvListGroupWithUnitConversion
            //				  group salemenu by new { salemenu.MaterialRefId, salemenu.MaterialName, salemenu.DefaultUnitId, salemenu.DefaultUnitName } into g
            //				  select new ProductAnalysisViewDto
            //				  {
            //					  MaterialRefId = g.Key.MaterialRefId,
            //					  MaterialName = g.Key.MaterialName,
            //					  DefaultUnitName = g.Key.DefaultUnitName,
            //					  Uom = g.Key.DefaultUnitName,
            //					  DefaultUnitId = g.Key.DefaultUnitId,
            //					  TotalQty = g.Sum(t => t.TotalQty),
            //					  TotalAmount = g.Sum(t => t.TotalAmount),
            //					  MinPrice = g.Min(t => t.Price),
            //					  MaxPrice = g.Max(t => t.Price),
            //					  AvgPrice = Math.Round(g.Sum(s => s.TotalAmount) / g.Sum(s => s.TotalQty), 2),
            //					  AvgQty = Math.Round(g.Sum(s => s.TotalQty) / g.Count(), 2),
            //				  }).ToList();


            return new PurchaseAnalysisOutputDto { InvoiceList = matInvListGroupWithUnitConversion };
        }


        public async Task<List<InvoiceCategoryAnalysisDto>> GetPurchaseAnalysisCategoryWise(InputInvoiceAnalysisForCategoryWise input)
        {
            var invoiceMaster = _invoiceRepo.GetAll();

            if (input.SupplierRefId != 0)
            {
                invoiceMaster = invoiceMaster.Where(t => t.SupplierRefId == input.SupplierRefId);
            }

            if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
            {
                invoiceMaster =
                    invoiceMaster.Where(
                        a => DbFunctions.TruncateTime(a.InvoiceDate) >= DbFunctions.TruncateTime(input.StartDate)
                             && DbFunctions.TruncateTime(a.InvoiceDate) <= DbFunctions.TruncateTime(input.EndDate));
            }

            if (input.LocationRefId != 0)
            {
                invoiceMaster = invoiceMaster.Where(t => t.LocationRefId == input.LocationRefId);
            }

            bool CategoryExist = false;

            if (input.CategoryRefId != 0)
            {
                CategoryExist = true;
            }

            var allItems = (from invmas in invoiceMaster
                            join invdet in _invoicedetailRepo.GetAll()
                            on invmas.Id equals invdet.InvoiceRefId
                            join sup in _supplierRepo.GetAll()
                            on invmas.SupplierRefId equals sup.Id
                            join mat in _materialRepo.GetAll().WhereIf(CategoryExist, p => p.MaterialGroupCategoryRefId == input.CategoryRefId)
                            on invdet.MaterialRefId equals mat.Id
                            join unit in _unitRepo.GetAll()
                            on mat.DefaultUnitId equals unit.Id
                            select new InvoiceAnalysisDto
                            {
                                InvoiceDate = invmas.InvoiceDate,
                                InvoiceNumber = invmas.InvoiceNumber,
                                InvoiceRefId = invmas.Id,
                                SupplierRefId = invmas.SupplierRefId,
                                SupplierRefName = sup.SupplierName,
                                MaterialRefId = invdet.MaterialRefId,
                                MaterialRefName = mat.MaterialName,
                                MaterialGroupCategoryRefId = mat.MaterialGroupCategoryRefId,
                                TotalQty = invdet.TotalQty,
                                Price = invdet.Price,
                                DiscountAmount = invdet.DiscountAmount,
                                DiscountFlag = invdet.DiscountFlag,
                                DiscountPercentage = invdet.DiscountPercentage,
                                TotalAmount = invdet.TotalAmount,
                                TaxAmount = invdet.TaxAmount,
                                NetAmount = invdet.NetAmount,
                                Remarks = invdet.Remarks,
                                Uom = unit.Name
                            }).ToList();

            var allItemsCategory = (from matPurchase in allItems
                                    join cat in _materialGroupCategoryRepo.GetAll().WhereIf(CategoryExist, p => p.Id == input.CategoryRefId)
                                    on matPurchase.MaterialGroupCategoryRefId equals cat.Id
                                    group matPurchase by new { matPurchase.MaterialGroupCategoryRefId, cat.MaterialGroupCategoryName } into g
                                    select new InvoiceCategoryAnalysisDto
                                    {
                                        CategoryRefId = g.Key.MaterialGroupCategoryRefId,
                                        CategoryRefName = g.Key.MaterialGroupCategoryName,
                                        TotalAmount = g.Sum(t => t.TotalAmount),
                                        TaxAmount = g.Sum(t => t.TaxAmount),
                                        TotalQty = g.Sum(t => t.TotalQty),
                                        NetAmount = g.Sum(t => t.NetAmount)
                                    });



            List<InvoiceCategoryAnalysisDto> sortMenuItems;

            sortMenuItems = allItemsCategory.ToList();

            var allListDtos = sortMenuItems.MapTo<List<InvoiceCategoryAnalysisDto>>();

            var allItemCount = allItems.Count();

            return allListDtos;
        }

        public async Task<FileDto> GetPurchaseReportCategoryWiseWithPurchaseOrderAnalysisToExcel(InputInvoiceAnalysis input)
        {
            var result = await GetPurchaseReportCategoryWiseWithPurchaseOrderAnalysis(input);
            return _invoiceExporter.ExportPurchaseCategoryWithPoToFile(result);
        }

        public async Task<bool> ReverseExportCompletion(IdInput input)
        {
            var lstInvoice = await _invoiceRepo.FirstOrDefaultAsync(t => t.Id == input.Id);
            if (lstInvoice.ExportDateTimeForExternalImport.HasValue)
            {
                lstInvoice.ExportDateTimeForExternalImport = null;
                await _invoiceRepo.UpdateAsync(lstInvoice);
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<InvoiceWithPoAndCategoryOutputDto> GetPurchaseReportCategoryWiseWithPurchaseOrderAnalysis(InputInvoiceAnalysis input)
        {
            var invoiceMaster = _invoiceRepo.GetAll();
            if (input.ShowAlreadyExported)
            {
                //invoiceMaster = invoiceMaster.Where(t => t.ExportDateTimeForExternalImport.HasValue == true);
            }
            else
            {
                invoiceMaster = invoiceMaster.Where(t => t.ExportDateTimeForExternalImport.HasValue == false);
            }

            var invoiceDetail = _invoicedetailRepo.GetAll();
            if (input.SupplierRefId != 0)
            {
                invoiceMaster = invoiceMaster.Where(t => t.SupplierRefId == input.SupplierRefId);
            }

            if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
            {
                invoiceMaster =
                    invoiceMaster.Where(
                        a => DbFunctions.TruncateTime(a.InvoiceDate) >= DbFunctions.TruncateTime(input.StartDate)
                             && DbFunctions.TruncateTime(a.InvoiceDate) <= DbFunctions.TruncateTime(input.EndDate));
            }
            if (input.LocationRefId != 0)
                invoiceMaster = invoiceMaster.Where(t => t.LocationRefId == input.LocationRefId);
            if (input.MaterialRefId != 0)
                invoiceDetail = invoiceDetail.Where(t => t.MaterialRefId == input.MaterialRefId);

            if (input.InvoiceNumber != null)
            {
                if (input.ExactSearchFlag == false)
                    invoiceMaster = invoiceMaster.Where(t => t.InvoiceNumber.Contains(input.InvoiceNumber));
                else
                    invoiceMaster = invoiceMaster.Where(t => t.InvoiceNumber.Equals(input.InvoiceNumber));
            }

            var allItems = (from invmas in invoiceMaster
                            join invdet in invoiceDetail on invmas.Id equals invdet.InvoiceRefId
                            join sup in _supplierRepo.GetAll() on invmas.SupplierRefId equals sup.Id
                            join mat in _materialRepo.GetAll() on invdet.MaterialRefId equals mat.Id
                            join matCat in _materialGroupCategoryRepo.GetAll() on mat.MaterialGroupCategoryRefId equals matCat.Id
                            join matGrp in _materialGroupRepo.GetAll() on matCat.MaterialGroupRefId equals matGrp.Id
                            join loc in _locationRepo.GetAll() on invmas.LocationRefId equals loc.Id
                            select new InvoiceCategoryWisePurchaseOrderAnalysisDto
                            {
                                LocationRefId = invmas.LocationRefId,
                                LocationRefCode = loc.Code,
                                LocationRefName = loc.Name,
                                InvoiceDate = invmas.InvoiceDate,
                                InvoiceNumber = invmas.InvoiceNumber,
                                InvoiceRefId = invmas.Id,
                                SupplierRefId = invmas.SupplierRefId,
                                SupplierRefName = sup.SupplierName,
                                MaterialGroupRefId = matGrp.Id,
                                MaterialGroupRefName = matGrp.MaterialGroupName,
                                MaterialGroupCategoryRefId = matCat.Id,
                                MaterialGroupCategoryRefName = matCat.MaterialGroupCategoryName,
                                MaterialRefId = mat.Id,
                                MaterialRefName = mat.MaterialName,
                                MaterialPetName = mat.MaterialPetName,
                                TotalAmount = invdet.TotalAmount,
                                TaxAmount = invdet.TaxAmount,
                                NetAmount = invdet.NetAmount,
                                DiscountAmount = invdet.DiscountAmount,
                                AccountDate = invmas.AccountDate,
                                ExportDateTimeForExternalImport = invmas.ExportDateTimeForExternalImport
                            }).ToList();

            List<InvoiceCategoryWisePurchaseOrderAnalysisDto> sortMenuItems;

            sortMenuItems = allItems.ToList();


            //var inwardMasters = (from invDcLink in _invoiceDirectCreditLinkRepo.GetAll().Where(t=> arrInvCreditRefIds.Contains(t.InvoiceRefId))
            //                     join invDcDetail in _inwarddirectcreditDetailRepo.GetAll() on invDcLink.InwardDirectCreditRefId equals invDcDetail.Id
            //                     join invDcMaster in _inwarddirectcreditRepo.GetAll() on invDcDetail.DcRefId equals invDcMaster.Id
            //                     select 

            List<int> arrInvoiceRefIds = sortMenuItems.Select(t => t.InvoiceRefId).ToList();
            DateTime completionTime = DateTime.Now;
            if (input.UpdateExportCompletedFlag)
            {
                var rsInvoicesCompletionToBeUpdated = await _invoiceRepo.GetAllListAsync(t => arrInvoiceRefIds.Contains(t.Id));
                foreach (var lst in rsInvoicesCompletionToBeUpdated)
                {
                    lst.ExportDateTimeForExternalImport = completionTime;
                    await _invoiceRepo.UpdateAsync(lst);
                }
            }

            var rsInvoiceCreditLinks = await _invoiceDirectCreditLinkRepo.GetAll().Where(t => arrInvoiceRefIds.Contains(t.InvoiceRefId)).ToListAsync();
            List<int> arrInvCreditRefIds = rsInvoiceCreditLinks.Select(t => t.InwardDirectCreditRefId).ToList();
            var rsInwardMater = await _inwarddirectcreditRepo.GetAll().Where(t => arrInvCreditRefIds.Contains(t.Id)).ToListAsync();
            List<int> arrPoRefIds = rsInwardMater.Where(t => t.PoRefId.HasValue).Select(t => t.PoRefId.Value).ToList();
            var rsPurchaseOrders = await _purchaseorderRepo.GetAll().Where(t => arrPoRefIds.Contains(t.Id)).ToListAsync();
            List<InvoiceCategoryWisePurchaseOrderAnalysisDto> outputDto = new List<InvoiceCategoryWisePurchaseOrderAnalysisDto>();
            foreach (var gp in sortMenuItems.GroupBy(t => t.InvoiceRefId))
            {
                string poRefCode = "";
                int? poRefId = null;
                #region Get Po Reference
                var inwDcLink = rsInvoiceCreditLinks.FirstOrDefault(t => t.InvoiceRefId == gp.Key);
                if (inwDcLink != null)
                {
                    var inwMaster = rsInwardMater.FirstOrDefault(t => t.Id == inwDcLink.InwardDirectCreditRefId);
                    if (inwMaster != null)
                    {
                        var purOrder = rsPurchaseOrders.FirstOrDefault(t => t.Id == inwMaster.PoRefId);
                        if (purOrder != null)
                        {
                            poRefId = purOrder.Id;
                            poRefCode = purOrder.PoReferenceCode;
                        }
                    }
                }
                #endregion

                foreach (var sublst in gp.GroupBy(t => new { t.MaterialGroupRefId, t.MaterialGroupRefName, t.MaterialGroupCategoryRefId, t.MaterialGroupCategoryRefName }))
                {
                    InvoiceCategoryWisePurchaseOrderAnalysisDto newDto = new InvoiceCategoryWisePurchaseOrderAnalysisDto
                    {
                        InvoiceRefId = gp.Key,
                        InvoiceDate = gp.FirstOrDefault().InvoiceDate,
                        InvoiceNumber = gp.FirstOrDefault().InvoiceNumber,
                        SupplierRefId = gp.FirstOrDefault().SupplierRefId,
                        SupplierRefName = gp.FirstOrDefault().SupplierRefName,
                        LocationRefCode = gp.FirstOrDefault().LocationRefCode,
                        LocationRefName = gp.FirstOrDefault().LocationRefName,
                        AccountDate = gp.FirstOrDefault().AccountDate,
                        MaterialGroupRefId = sublst.Key.MaterialGroupRefId,
                        MaterialGroupRefName = sublst.Key.MaterialGroupRefName,
                        MaterialGroupCategoryRefId = sublst.Key.MaterialGroupCategoryRefId,
                        MaterialGroupCategoryRefName = sublst.Key.MaterialGroupCategoryRefName,
                        PoReferenceNumber = poRefCode,
                        PurchaseOrderRefId = poRefId,
                        DiscountAmount = sublst.Sum(t => t.DiscountAmount),
                        TotalAmount = sublst.Sum(t => t.TotalAmount),
                        TaxAmount = sublst.Sum(t => t.TaxAmount),
                        NetAmount = sublst.Sum(t => t.NetAmount),
                        ExportDateTimeForExternalImport = input.UpdateExportCompletedFlag == true ? completionTime : gp.FirstOrDefault().ExportDateTimeForExternalImport
                    };
                    outputDto.Add(newDto);
                }
            }
            return new InvoiceWithPoAndCategoryOutputDto { InvoiceCategoryWisePurchaseOrderAnalysisDtoList = outputDto };
        }


        public async Task<FileDto> GetPurchaseReportGroupOrCategoryOrMaterialWiseForGivenInputToExcel(PurchaseAnalysisGroupCategoryMaterialDto input)
        {
            var result = await GetPurchaseReportGroupOrCategoryOrMaterialWiseForGivenInput(input);
            result.InputFilterDto = input;
            if (result.MaterialWisePurchaseDtos.Count == 0)
            {
                throw new UserFriendlyException(L("NoMoreRecordsFound"));
            }
            return _invoiceExporter.ExportSupplierWisePurchaseGroupCategoryMaterials(result);
        }


        public async Task<SupplierWisePurchaseGroupCateogryMaterialDto> GetPurchaseReportGroupOrCategoryOrMaterialWiseForGivenInput(PurchaseAnalysisGroupCategoryMaterialDto input)
        {
            var invoiceMaster = _invoiceRepo.GetAll();

            #region Date & Supplier Filter
            if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
            {
                invoiceMaster =
                    invoiceMaster.Where(
                        a => DbFunctions.TruncateTime(a.InvoiceDate) >= DbFunctions.TruncateTime(input.StartDate)
                             && DbFunctions.TruncateTime(a.InvoiceDate) <= DbFunctions.TruncateTime(input.EndDate));
            }

            if (input.SupplierList != null && input.SupplierList.Count > 0)
            {
                List<int> arrSupplierRefIds = input.SupplierList.Select(t => int.Parse(t.Value)).ToList();
                invoiceMaster = invoiceMaster.Where(t => arrSupplierRefIds.Contains(t.SupplierRefId));
            }
            #endregion

            #region Location Filter
            List<int> selectedLocationRefIds = new List<int>();
            if (input.Locations != null && input.Locations.Any())
            {
                selectedLocationRefIds = input.Locations.Select(a => a.Id).ToList();
            }
            else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
                     && !input.LocationGroup.Locations.Any())
            {
                selectedLocationRefIds = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
            }
            else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                selectedLocationRefIds = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
            }
            else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                selectedLocationRefIds = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true
                });
            }
            else if (input.LocationGroup == null)
            {
                if (input.UserId > 0)
                {
                    selectedLocationRefIds = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                    {
                        Locations = new List<SimpleLocationDto>(),
                        Group = false,
                        UserId = input.UserId
                    });
                }
            }
            invoiceMaster = invoiceMaster.Where(a => selectedLocationRefIds.Contains(a.LocationRefId));

            #endregion

            var invoiceDetail = _invoicedetailRepo.GetAll();

            #region Group Category Material Filter
            List<int> arrGroupRefIds = new List<int>();
            if (input.GroupList.Count > 0)
            {
                arrGroupRefIds = input.GroupList.Select(t => int.Parse(t.Value)).ToList();
            }
            List<int> arrCategoryRefIds = new List<int>();
            if (input.CategoryList.Count > 0)
            {
                arrCategoryRefIds = input.CategoryList.Select(t => int.Parse(t.Value)).ToList();
            }
            List<int> arrMateriaRefIds = new List<int>();
            if (input.MaterialList.Count > 0)
            {
                arrMateriaRefIds = input.MaterialList.Select(t => int.Parse(t.Value)).ToList();
                input.ExportMaterialAlso = true;
            }
            #endregion

            var allItems = await (from invmas in invoiceMaster
                                  join invdet in invoiceDetail on invmas.Id equals invdet.InvoiceRefId
                                  join sup in _supplierRepo.GetAll() on invmas.SupplierRefId equals sup.Id
                                  join mat in _materialRepo.GetAll().WhereIf(arrMateriaRefIds.Count > 0, t => arrMateriaRefIds.Contains(t.Id))
                                      on invdet.MaterialRefId equals mat.Id
                                  join matCat in _materialGroupCategoryRepo.GetAll().WhereIf(arrCategoryRefIds.Count > 0, t => arrCategoryRefIds.Contains(t.Id))
                                      on mat.MaterialGroupCategoryRefId equals matCat.Id
                                  join matGrp in _materialGroupRepo.GetAll().WhereIf(arrGroupRefIds.Count > 0, t => arrGroupRefIds.Contains(t.Id))
                                      on matCat.MaterialGroupRefId equals matGrp.Id
                                  join loc in _locationRepo.GetAll() on invmas.LocationRefId equals loc.Id
                                  select new InvoiceCategoryWisePurchaseOrderAnalysisDto
                                  {
                                      LocationRefId = invmas.LocationRefId,
                                      LocationRefCode = loc.Code,
                                      LocationRefName = loc.Name,
                                      InvoiceDate = invmas.InvoiceDate,
                                      InvoiceNumber = invmas.InvoiceNumber,
                                      InvoiceRefId = invmas.Id,
                                      SupplierRefId = invmas.SupplierRefId,
                                      SupplierRefName = sup.SupplierName,
                                      MaterialGroupRefId = matGrp.Id,
                                      MaterialGroupRefName = matGrp.MaterialGroupName,
                                      MaterialGroupCategoryRefId = matCat.Id,
                                      MaterialGroupCategoryRefName = matCat.MaterialGroupCategoryName,
                                      MaterialRefId = mat.Id,
                                      MaterialRefName = mat.MaterialName,
                                      MaterialPetName = mat.MaterialPetName,
                                      TotalAmount = invdet.TotalAmount,
                                      TaxAmount = invdet.TaxAmount,
                                      NetAmount = invdet.NetAmount,
                                      DiscountAmount = invdet.DiscountAmount,
                                      AccountDate = invmas.AccountDate,
                                      ExportDateTimeForExternalImport = invmas.ExportDateTimeForExternalImport
                                  }).ToListAsync();

            List<InvoiceCategoryWisePurchaseOrderAnalysisDto> sortMenuItems;

            sortMenuItems = allItems.ToList();

            List<SupplierWiseHeadWisePurchase> outputDto = new List<SupplierWiseHeadWisePurchase>();
            foreach (var gp in sortMenuItems.GroupBy(t => t.SupplierRefId))
            {
                SupplierWiseHeadWisePurchase newSupplierDto = new SupplierWiseHeadWisePurchase
                {
                    SupplierRefId = gp.Key,
                    SupplierName = gp.FirstOrDefault().SupplierRefName
                };

                foreach (var sublst in gp.GroupBy(t => new { t.MaterialRefId, t.MaterialRefName }))
                {
                    MaterialWisePurchaseDto materialDto = new MaterialWisePurchaseDto
                    {
                        MaterialRefId = sublst.Key.MaterialRefId,
                        MaterialRefName = sublst.Key.MaterialRefName,
                        MaterialGroupRefId = gp.FirstOrDefault().MaterialGroupRefId,
                        MaterialGroupCategoryRefId = gp.FirstOrDefault().MaterialGroupCategoryRefId,
                        TotalAmount = sublst.Sum(t => t.TotalAmount)
                    };
                    newSupplierDto.MaterialWisePurchaseDtos.Add(materialDto);
                }

                foreach (var sublst in gp.GroupBy(t => new { t.MaterialGroupCategoryRefId, t.MaterialGroupCategoryRefName }))
                {
                    MaterialGroupCategoryWisePurchaseDto groupCategoryDto = new MaterialGroupCategoryWisePurchaseDto
                    {
                        MaterialGroupCategoryRefId = sublst.Key.MaterialGroupCategoryRefId,
                        MaterialGroupCategoryRefName = sublst.Key.MaterialGroupCategoryRefName,
                        TotalAmount = sublst.Sum(t => t.TotalAmount)
                    };
                    newSupplierDto.MaterialGroupCategoryWisePurchaseDtos.Add(groupCategoryDto);
                }

                foreach (var sublst in gp.GroupBy(t => new { t.MaterialGroupRefId, t.MaterialGroupRefName }))
                {
                    MaterialGroupWisePurchaseDto groupDto = new MaterialGroupWisePurchaseDto
                    {
                        MaterialGroupRefId = sublst.Key.MaterialGroupRefId,
                        MaterialGroupRefName = sublst.Key.MaterialGroupRefName,
                        TotalAmount = sublst.Sum(t => t.TotalAmount)
                    };
                    newSupplierDto.MaterialGroupWisePurchaseDtos.Add(groupDto);
                }
                outputDto.Add(newSupplierDto);
            }

            SupplierWisePurchaseGroupCateogryMaterialDto output = new SupplierWisePurchaseGroupCateogryMaterialDto();

            foreach (var sublst in sortMenuItems.GroupBy(t => new { t.MaterialRefId, t.MaterialRefName }))
            {
                MaterialWisePurchaseDto materialDto = new MaterialWisePurchaseDto
                {
                    MaterialRefId = sublst.Key.MaterialRefId,
                    MaterialRefName = sublst.Key.MaterialRefName,
                    TotalAmount = sublst.Sum(t => t.TotalAmount)
                };
                output.MaterialWisePurchaseDtos.Add(materialDto);
            }

            foreach (var sublst in sortMenuItems.GroupBy(t => new { t.MaterialGroupCategoryRefId, t.MaterialGroupCategoryRefName }))
            {
                MaterialGroupCategoryWisePurchaseDto groupCategoryDto = new MaterialGroupCategoryWisePurchaseDto
                {
                    MaterialGroupCategoryRefId = sublst.Key.MaterialGroupCategoryRefId,
                    MaterialGroupCategoryRefName = sublst.Key.MaterialGroupCategoryRefName,
                    TotalAmount = sublst.Sum(t => t.TotalAmount)
                };
                output.MaterialGroupCategoryWisePurchaseDtos.Add(groupCategoryDto);
            }

            foreach (var sublst in sortMenuItems.GroupBy(t => new { t.MaterialGroupRefId, t.MaterialGroupRefName }))
            {
                MaterialGroupWisePurchaseDto groupDto = new MaterialGroupWisePurchaseDto
                {
                    MaterialGroupRefId = sublst.Key.MaterialGroupRefId,
                    MaterialGroupRefName = sublst.Key.MaterialGroupRefName,
                    TotalAmount = sublst.Sum(t => t.TotalAmount)
                };
                output.MaterialGroupWisePurchaseDtos.Add(groupDto);
            }

            output.SupplierWiseHeadWisePurchaseList = outputDto;
            return output;
        }


        public async Task<FileDto> GetPurchaseAnalysisCategoryWiseToExcel(InputInvoiceAnalysisForCategoryWise input)
        {
            var result = await GetPurchaseAnalysisCategoryWise(input);
            return _invoiceExporter.ExportCategoryPurchaseToFile(result);
        }


        public async Task<List<InwardDirectCreditDetailForParticularDc>> GetMaterialForInwardDc(IdInput ninput)
        {
            var allItems = (from mat in _materialRepo.GetAll()
                            join inwDet in _inwarddirectcreditDetailRepo.GetAll()
                                .Where(t => t.DcRefId == ninput.Id && t.DcReceivedQty != t.DcConvertedQty)
                            on mat.Id equals inwDet.MaterialRefId
                            where inwDet.DcRefId == (ninput.Id) && inwDet.DcReceivedQty != inwDet.DcConvertedQty
                            select new InwardDirectCreditDetailForParticularDc
                            {
                                MaterialRefId = inwDet.MaterialRefId,
                                MaterialRefName = mat.MaterialName,
                                DcReceivedQty = inwDet.DcReceivedQty,
                                DcConvertedQty = inwDet.DcConvertedQty,
                                MaterialReceivedStatus = inwDet.MaterialReceivedStatus,
                                PendingQty = inwDet.DcReceivedQty - inwDet.DcConvertedQty
                            });

            var matRecipeList = await allItems.ToListAsync();

            //ListResultOutput<InwardDirectCreditDetailForParticularDc> allDtos = matRecipeList.MapTo<ListResultOutput<InwardDirectCreditDetailForParticularDc>>();

            return matRecipeList;

            //            new ListResultOutput<InwardDirectCreditDetailForParticularDc>(
            //              matRecipeList.Select(e => new ComboboxItemDto(e.Id.ToString(), e.MaterialName)).ToList());


        }

        public async Task<List<InwardDirectCreditDetailForParticularDc>> GetMaterialForPo(IdInput input)
        {
            var allItems = (from mat in _materialRepo.GetAll()
                            join poDet in _purchaseorderdetailRepo.GetAll().Where(t => t.PoRefId == input.Id && (t.QtyOrdered - t.QtyReceived) > 0)
                            on mat.Id equals poDet.MaterialRefId
                            select new InwardDirectCreditDetailForParticularDc
                            {
                                MaterialRefId = poDet.MaterialRefId,
                                MaterialRefName = mat.MaterialName,
                                DcReceivedQty = poDet.QtyReceived,
                                DcConvertedQty = poDet.QtyReceived,
                                MaterialReceivedStatus = "", // inwDet.MaterialReceivedStatus,
                                PendingQty = poDet.QtyOrdered - poDet.QtyReceived
                            });

            var matRecipeList = await allItems.ToListAsync();

            return matRecipeList;

        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetDcTranId(InputDateRangeWithSupplierId input)
        {
            var allItems = await _inwarddirectcreditAppService.GetInwardDcTranid(input);
            return
                new ListResultOutput<ComboboxItemDto>(
                    allItems.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.DcNumberGivenBySupplier.ToString())).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetDcPoTranId(InputDateRangeWithSupplierId input)
        {
            var allItems = await _inwarddirectcreditAppService.GetInwardPoTranid(input);
            return
                allItems;
        }

        private void AlterDate(GetInvoiceInput input)
        {
            if (input.StartDate.Equals(DateTime.MinValue))
            {
                input.StartDate = DateTime.Now;
            }
            if (input.EndDate.Equals(DateTime.MinValue))
            {
                input.EndDate = DateTime.Now;
            }

            var totalHours = input.EndDate.Value.Subtract(input.StartDate.Value).TotalHours;
            if (totalHours <= 24 && !input.EndDate.Value.Day.Equals(input.StartDate.Value.Day))
            {
                input.StartDate = input.StartDate;
                input.EndDate = input.StartDate;
            }
        }

        public async Task<PagedResultOutput<InvoiceListDto>> GetInvoiceSearch(GetInvoiceInput input)
        {
            AlterDate(input);

            int[] locationRefIds;
            if (input.Locations != null)
                locationRefIds = input.Locations.Select(t => t.Id).ToArray();
            else
            {
                locationRefIds = new int[2];
                locationRefIds[0] = input.LocationRefId;
            }

            var allItems = (from inv in _invoiceRepo.GetAll()
                                .Where(t => locationRefIds.Contains(t.LocationRefId))
                            join sup in _supplierRepo.GetAll().WhereIf(!input.Filter.IsNullOrEmpty(), p => p.SupplierName.Contains(input.Filter))
                            on inv.SupplierRefId equals sup.Id
                            select new InvoiceListDto
                            {
                                InvoiceAmount = inv.InvoiceAmount,
                                InvoiceDate = inv.InvoiceDate,
                                AccountDate = inv.AccountDate,
                                InvoiceNumber = inv.InvoiceNumber,
                                SupplierRefId = inv.SupplierRefId,
                                SupplierCode = sup.SupplierCode,
                                SupplierRefName = sup.SupplierName,
                                SupplierAddOn = sup.AddOn,
                                TotalShipmentCharges = inv.TotalShipmentCharges,
                                Id = inv.Id,
                                IsDirectInvoice = inv.IsDirectInvoice,
                                PurchaseCategoryRefId = inv.PurchaseCategoryRefId,
                                CreationTime = inv.CreationTime,
                                LocationRefId = inv.LocationRefId
                            });

            if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
            {
                allItems =
                    allItems.Where(
                        a => DbFunctions.TruncateTime(a.InvoiceDate) >= DbFunctions.TruncateTime(input.StartDate)
                             && DbFunctions.TruncateTime(a.InvoiceDate) <= DbFunctions.TruncateTime(input.EndDate));
            }

            if (input.SupplierRefId > 0)
            {
                allItems = allItems.Where(a => a.SupplierRefId == input.SupplierRefId);
            }



            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<InvoiceListDto>>();
            if (input.TaxShowFlag)
            {
                int[] invoiceIds = allItems.Select(t => t.Id).ToArray();

                var invoiceDetail = await _invoicedetailRepo.GetAll().Where(t => invoiceIds.Contains(t.InvoiceRefId)).ToListAsync();

                foreach (var lst in allListDtos)
                {
                    decimal taxAmount = invoiceDetail.Where(t => t.InvoiceRefId == lst.Id).ToList()
                        .Sum(t => t.TaxAmount);
                    lst.TaxAmount = taxAmount;
                    lst.AmountWithoutTax = lst.InvoiceAmount - lst.TaxAmount;

                    var myLoc = await _locationRepo.GetAsync(lst.LocationRefId);
                    if (myLoc != null)
                    {
                        lst.LocationAddOn = myLoc.AddOn;
                        lst.LocationCode = myLoc.Code;
                    }

                }
                using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                {
                    foreach (var lst in allListDtos)
                    {
                        if (lst.PurchaseCategoryRefId.HasValue)
                        {
                            var pcategory = _purchaseCategoryRepo.Get(lst.PurchaseCategoryRefId.Value);
                            lst.PurchaseCategoryName = pcategory.PurchaseCategoryName;
                        }
                    }
                }
            }

            var allItemCount = await allItems.CountAsync();
            return new PagedResultOutput<InvoiceListDto>(
                allItemCount,
                allListDtos
                );

        }

        public async Task<List<OutputMaterialPurchaseDto>> GetMaterialPurchase(InputMaterialPurchaseDto input)
        {
            var editDetailDto = (from invMas in _invoiceRepo.GetAll()  // .Where(t=>t.InvoiceDate <= input.EffectiveDate)
                                 join invDet in _invoicedetailRepo.GetAll().Where(t => t.MaterialRefId == input.MaterialRefId)
                                 on invMas.Id equals invDet.InvoiceRefId
                                 join mat in _materialRepo.GetAll() on invDet.MaterialRefId equals mat.Id
                                 join unit in _unitRepo.GetAll() on mat.DefaultUnitId equals unit.Id
                                 join sup in _supplierRepo.GetAll() on invMas.SupplierRefId equals sup.Id
                                 select new OutputMaterialPurchaseDto
                                 {
                                     MaterialRefId = invDet.MaterialRefId,
                                     MaterialRefName = mat.MaterialName,
                                     SupplierRefId = sup.Id,
                                     SupplierRefName = sup.SupplierName,
                                     PurchaseDate = invMas.InvoiceDate,
                                     QuantityPurchased = invDet.TotalQty,
                                     Price = invDet.Price,
                                     Tax = invDet.TaxAmount > 0 ? Math.Round(invDet.TaxAmount / invDet.TotalQty, 2) : 0,
                                     PriceWithTax = invDet.Price + invDet.TaxAmount > 0 ? Math.Round(invDet.Price + invDet.TaxAmount / invDet.TotalQty, 3) : 0M,
                                 });

            var output = await editDetailDto.OrderByDescending(t => t.PurchaseDate).Take(input.NumberOfLastPurchaseDetail).ToListAsync();

            return output;

        }



        public async Task<PrintOutIn40Cols> GetPrintDataPixel10(IdInput input)
        {
            PrintOutIn40Cols output = new PrintOutIn40Cols();

            InvoiceEditDto editDto;
            List<InvoiceDetailViewDto> editDetailDto;
            List<InvoiceDirectCreditLinkViewDto> editInvoiceDCLink;

            var hDto = await _invoiceRepo.GetAsync(input.Id);
            editDto = hDto.MapTo<InvoiceEditDto>();

            if (editDto.IsDirectInvoice == false)
            {
                editDetailDto = await (from invDet in _invoicedetailRepo.GetAll()
                                        .Where(a => a.InvoiceRefId == input.Id)
                                       join mat in _materialRepo.GetAll() on invDet.MaterialRefId equals mat.Id
                                       join uiss in _unitRepo.GetAll() on invDet.UnitRefId equals uiss.Id
                                       join invdclnk in _invoiceDirectCreditLinkRepo.GetAll()
                                       on new { a = invDet.InvoiceRefId, b = invDet.Sno }
                                       equals new { a = invdclnk.InvoiceRefId, b = invdclnk.Sno }
                                       where mat.Id == invdclnk.MaterialRefId
                                       select new InvoiceDetailViewDto
                                       {
                                           Id = invDet.Id,
                                           DiscountAmount = invDet.DiscountAmount,
                                           DcTranId = invdclnk.InwardDirectCreditRefId,
                                           Barcode = mat.Barcode,
                                           MaterialRefId = invDet.MaterialRefId,
                                           MaterialRefName = mat.MaterialName,
                                           InvoiceRefId = invDet.InvoiceRefId,
                                           DiscountFlag = invDet.DiscountFlag,
                                           DiscountPercentage = invDet.DiscountPercentage,
                                           NetAmount = invDet.NetAmount,
                                           Price = invDet.Price,
                                           TaxAmount = invDet.TaxAmount,
                                           TotalAmount = invDet.TotalAmount,
                                           TotalQty = invDet.TotalQty,
                                           Remarks = invDet.Remarks,
                                           Sno = invDet.Sno,
                                           UnitRefName = uiss.Name,
                                           UnitRefId = invDet.UnitRefId,
                                           YieldPercentage = invDet.YieldPercentage,
                                           YieldMode = invDet.YieldMode,
                                           YieldExcessShortageQty = invDet.YieldExcessShortageQty
                                       }).ToListAsync();
            }
            else
            {
                editDetailDto = await (from invDet in _invoicedetailRepo.GetAll()
                                        .Where(a => a.InvoiceRefId == input.Id)
                                       join mat in _materialRepo.GetAll() on invDet.MaterialRefId equals mat.Id
                                       join uiss in _unitRepo.GetAll() on invDet.UnitRefId equals uiss.Id
                                       select new InvoiceDetailViewDto
                                       {
                                           Id = invDet.Id,
                                           DiscountAmount = invDet.DiscountAmount,
                                           DcTranId = 0,
                                           Barcode = mat.Barcode,
                                           MaterialRefId = invDet.MaterialRefId,
                                           MaterialRefName = mat.MaterialName,
                                           InvoiceRefId = invDet.InvoiceRefId,
                                           DiscountFlag = invDet.DiscountFlag,
                                           DiscountPercentage = invDet.DiscountPercentage,
                                           NetAmount = invDet.NetAmount,
                                           Price = invDet.Price,
                                           TaxAmount = invDet.TaxAmount,
                                           TotalAmount = invDet.TotalAmount,
                                           TotalQty = invDet.TotalQty,
                                           Remarks = invDet.Remarks,
                                           Sno = invDet.Sno,
                                           Uom = uiss.Name,
                                           UnitRefName = uiss.Name,
                                           UnitRefId = invDet.UnitRefId,
                                           YieldPercentage = invDet.YieldPercentage,
                                           YieldMode = invDet.YieldMode,
                                           YieldExcessShortageQty = invDet.YieldExcessShortageQty
                                       }).ToListAsync();
            }

            decimal SubTotal = Math.Round(editDetailDto.Sum(t => t.TotalAmount), 2);
            decimal TaxAmount = Math.Round(editDetailDto.Sum(t => t.TaxAmount), 2);
            decimal NetAmount = Math.Round(hDto.InvoiceAmount, 2);
            hDto.RoundedAmount = Math.Round(hDto.RoundedAmount, 2);
            hDto.TotalShipmentCharges = Math.Round(hDto.TotalShipmentCharges, 2);


            int loopCnt = 0;
            foreach (var det in editDetailDto)
            {
                var taxlist = await (from potax in _invoicetaxdetailRepo.GetAll().Where(pot => pot.InvoiceRefId == input.Id && pot.MaterialRefId == det.MaterialRefId)
                                     join tax in _taxRepo.GetAll()
                                     on potax.TaxRefId equals tax.Id
                                     select new TaxForMaterial
                                     {
                                         TaxRefId = potax.TaxRefId,
                                         TaxName = tax.TaxName,
                                         TaxRate = potax.TaxRate,
                                         TaxValue = potax.TaxValue,
                                         SortOrder = tax.SortOrder,
                                         Rounding = tax.Rounding
                                     }).ToListAsync();

                editDetailDto[loopCnt].TaxForMaterial = taxlist;

                List<ApplicableTaxesForMaterial> applicabletaxes = new List<ApplicableTaxesForMaterial>();

                foreach (var tax in taxlist.OrderBy(t => t.SortOrder))
                {
                    applicabletaxes.Add(new ApplicableTaxesForMaterial
                    {
                        TaxName = tax.TaxName,
                        TaxRefId = tax.TaxRefId,
                        TaxRate = tax.TaxRate,
                        SortOrder = tax.SortOrder,
                        Rounding = tax.Rounding,
                        TaxCalculationMethod = tax.TaxCalculationMethod,
                    });
                }

                editDetailDto[loopCnt].ApplicableTaxes = applicabletaxes;

                loopCnt++;
            }

            editInvoiceDCLink = await (from invDC in _invoiceDirectCreditLinkRepo.GetAll().Where(a => a.InvoiceRefId == input.Id)
                                       select new InvoiceDirectCreditLinkViewDto
                                       {
                                           Id = invDC.Id,
                                           InvoiceRefId = invDC.InvoiceRefId,
                                           InwardDirectCreditRefId = invDC.InwardDirectCreditRefId,
                                           MaterialRefId = invDC.MaterialRefId
                                       }).ToListAsync();


            var supplier = _supplierRepo.FirstOrDefault(t => t.Id == hDto.SupplierRefId);
            var billingLocation = _locationRepo.FirstOrDefault(t => t.Id == hDto.LocationRefId);

            TextFileWriter fp = new TextFileWriter();
            string subPath = "~/Report/";
            bool exists = System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(subPath));


            if (!exists)
                System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(subPath));

            string fileName = System.Web.HttpContext.Current.Server.MapPath("~/Report/" + AbpSession.UserId + "_InvReport.txt");
            int PageLenth = 45;


            fp.FileOpen(fileName, "O");
            if (billingLocation.Name != null)
            {
                fp.PrintTextLine(fp.PrintFormatCenter(billingLocation.Name, PageLenth));
            }

            if (billingLocation.Address1 != null)
            {
                fp.PrintTextLine(fp.PrintFormatCenter(billingLocation.Address1, PageLenth));
            }

            if (billingLocation.Address2 != null)
            {
                fp.PrintTextLine(fp.PrintFormatCenter(billingLocation.Address2, PageLenth));
            }

            if (billingLocation.Address3 != null)
            {
                string addwithCity = billingLocation.Address3;
                if (billingLocation.City != null)
                    addwithCity = addwithCity + ", " + billingLocation.City;
                fp.PrintTextLine(fp.PrintFormatCenter(addwithCity, PageLenth));
            }

            fp.PrintTextLine(fp.PrintFormatCenter(L("Invoice"), PageLenth));

            fp.PrintText(fp.PrintFormatLeft(L("Inv#") + ": " + hDto.InvoiceNumber, 10));
            fp.PrintText(" " + L("Dt") + ": " + hDto.InvoiceDate.ToString("dd/MM/yyyy"));
            //if (hDto.AccountDate!=null)
            //    fp.PrintText(" " + L("Acc Dt") + ": " + hDto.AccountDate.ToString("yy-MMM-dd"));
            fp.PrintTextLine("");

            fp.PrintSepLine('-', PageLenth);

            string retText = System.IO.File.ReadAllText(fileName);

            retText = retText.Replace("\r\n", "<br/>");

            retText = retText.Replace(" ", "&nbsp;");

            output.HeaderText = retText;
            output.BoldHeaderText = "<STRONG>" + retText + "</STRONG>";

            fp.FileOpen(fileName, "O");

            fp.PrintText(L("Supplier") + " : ");
            if (supplier.SupplierName.Length > 0)
                fp.PrintTextLine(supplier.SupplierName);

            if (supplier.TaxRegistrationNumber != null)
            {
                fp.PrintText(L("Tax#"));
                fp.PrintTextLine(supplier.TaxRegistrationNumber);
            }
            fp.PrintSepLine('-', PageLenth);

            fp.PrintText(fp.PrintFormatLeft(L("Material"), 24));
            fp.PrintText(fp.PrintFormatRight(L("Qty"), 8));
            fp.PrintText(fp.PrintFormatRight(L("UOM"), 4));
            fp.PrintText(fp.PrintFormatRight(L("Price"), 9));


            fp.PrintTextLine("");
            fp.PrintSepLine('-', PageLenth);

            int loopIndex = 1;

            foreach (var det in editDetailDto)
            {
                fp.PrintText(fp.PrintFormatLeft(det.MaterialRefName, 24));
                decimal qty = Math.Round(det.TotalQty, 2);
                string qtyinstring = string.Concat(new string(' ', 8 - qty.ToString().Length), qty.ToString());

                string unitRefName = det.UnitRefName;
                if (det.UnitRefName.Length > 2)
                    unitRefName = det.UnitRefName.Left(2);

                qtyinstring = qtyinstring + " " + unitRefName;

                fp.PrintText(fp.PrintFormatLeft(qtyinstring, 12));

                decimal price = Math.Round(det.TotalAmount, 2);
                string priceinstring = string.Concat(new string(' ', 8 - price.ToString().Length), price.ToString());
                fp.PrintText(fp.PrintFormatRight(priceinstring, 9));

                fp.PrintTextLine("");
                loopIndex++;
            }

            fp.PrintTextLine(" ");
            fp.PrintSepLine('-', PageLenth);

            fp.PrintText(fp.PrintFormatLeft(L("SubTotal"), 30));
            string subtotalinstring = string.Concat(new string(' ', 15 - SubTotal.ToString().Length), SubTotal.ToString());
            fp.PrintTextLine(fp.PrintFormatRight(subtotalinstring, 15));

            if (TaxAmount > 0)
            {
                fp.PrintText(fp.PrintFormatLeft(L("Tax"), 30));
                string taxamountinstring = string.Concat(new string(' ', 15 - TaxAmount.ToString().Length), TaxAmount.ToString());
                fp.PrintTextLine(fp.PrintFormatRight(taxamountinstring, 15));
            }

            if (hDto.TotalDiscountAmount > 0)
            {
                fp.PrintText(fp.PrintFormatLeft(L("Discount"), 30));
                string discountinstring = string.Concat(new string(' ', 15 - hDto.TotalDiscountAmount.ToString().Length), hDto.TotalDiscountAmount.ToString());
                fp.PrintTextLine(fp.PrintFormatRight(discountinstring, 15));
            }


            if (hDto.TotalShipmentCharges > 0)
            {
                fp.PrintText(fp.PrintFormatLeft(L("Extra"), 30));
                string shipmentinstring = string.Concat(new string(' ', 15 - hDto.TotalShipmentCharges.ToString().Length), hDto.TotalShipmentCharges.ToString());
                fp.PrintTextLine(fp.PrintFormatRight(shipmentinstring, 15));
            }

            if (hDto.RoundedAmount != 0)
            {
                fp.PrintText(fp.PrintFormatLeft(L("RoundedOff"), 30));
                string roundamtinstring = string.Concat(new string(' ', 15 - hDto.RoundedAmount.ToString().Length), hDto.RoundedAmount.ToString());
                fp.PrintTextLine(fp.PrintFormatRight(roundamtinstring, 15));
            }

            fp.PrintText(fp.PrintFormatLeft(L("Total"), 30));
            string totalamtinstring = string.Concat(new string(' ', 15 - NetAmount.ToString().Length), NetAmount.ToString());
            fp.PrintTextLine(fp.PrintFormatRight(totalamtinstring, 15));

            fp.PrintSepLine('-', PageLenth);
            //fp.PrintTextLine(fp.PrintFormatCenter("Print @ " + DateTime.Now.ToString("yy-MMM-dd HH:mm:ss"), PageLenth));
            //fp.PrintSepLine('-', PageLenth);
            //fp.PrintTextLine(" ");

            retText = System.IO.File.ReadAllText(fileName);

            retText = retText.Replace("\r\n", "<br/>");

            //retText = retText.Replace(" ", "&nbsp;");
            output.BodyText = retText;
            output.BoldBodyText = "<STRONG>" + retText + "</STRONG>";

            return output;

        }

        public async Task<PrintOutIn40Cols> GetPrintDataOld(IdInput input)
        {
            PrintOutIn40Cols output = new PrintOutIn40Cols();

            InvoiceEditDto editDto;
            List<InvoiceDetailViewDto> editDetailDto;
            List<InvoiceDirectCreditLinkViewDto> editInvoiceDCLink;

            var hDto = await _invoiceRepo.GetAsync(input.Id);
            editDto = hDto.MapTo<InvoiceEditDto>();

            if (editDto.IsDirectInvoice == false)
            {
                editDetailDto = await (from invDet in _invoicedetailRepo.GetAll()
                                        .Where(a => a.InvoiceRefId == input.Id)
                                       join mat in _materialRepo.GetAll() on invDet.MaterialRefId equals mat.Id
                                       join uiss in _unitRepo.GetAll() on invDet.UnitRefId equals uiss.Id
                                       join invdclnk in _invoiceDirectCreditLinkRepo.GetAll()
                                       on new { a = invDet.InvoiceRefId, b = invDet.Sno }
                                       equals new { a = invdclnk.InvoiceRefId, b = invdclnk.Sno }
                                       where mat.Id == invdclnk.MaterialRefId
                                       select new InvoiceDetailViewDto
                                       {
                                           Id = invDet.Id,
                                           DiscountAmount = invDet.DiscountAmount,
                                           DcTranId = invdclnk.InwardDirectCreditRefId,
                                           Barcode = mat.Barcode,
                                           MaterialRefId = invDet.MaterialRefId,
                                           MaterialRefName = mat.MaterialName,
                                           InvoiceRefId = invDet.InvoiceRefId,
                                           DiscountFlag = invDet.DiscountFlag,
                                           DiscountPercentage = invDet.DiscountPercentage,
                                           NetAmount = invDet.NetAmount,
                                           Price = invDet.Price,
                                           TaxAmount = invDet.TaxAmount,
                                           TotalAmount = invDet.TotalAmount,
                                           TotalQty = invDet.TotalQty,
                                           Remarks = invDet.Remarks,
                                           Sno = invDet.Sno,
                                           Uom = uiss.Name,
                                           UnitRefId = invDet.UnitRefId,
                                           UnitRefName = uiss.Name
                                       }).ToListAsync();
            }
            else
            {
                editDetailDto = await (from invDet in _invoicedetailRepo.GetAll()
                                        .Where(a => a.InvoiceRefId == input.Id)
                                       join mat in _materialRepo.GetAll() on invDet.MaterialRefId equals mat.Id
                                       join uiss in _unitRepo.GetAll() on invDet.UnitRefId equals uiss.Id
                                       select new InvoiceDetailViewDto
                                       {
                                           Id = invDet.Id,
                                           DiscountAmount = invDet.DiscountAmount,
                                           DcTranId = 0,
                                           Barcode = mat.Barcode,
                                           MaterialRefId = invDet.MaterialRefId,
                                           MaterialRefName = mat.MaterialName,
                                           InvoiceRefId = invDet.InvoiceRefId,
                                           DiscountFlag = invDet.DiscountFlag,
                                           DiscountPercentage = invDet.DiscountPercentage,
                                           NetAmount = invDet.NetAmount,
                                           Price = invDet.Price,
                                           TaxAmount = invDet.TaxAmount,
                                           TotalAmount = invDet.TotalAmount,
                                           TotalQty = invDet.TotalQty,
                                           Remarks = invDet.Remarks,
                                           Sno = invDet.Sno,
                                           Uom = uiss.Name,
                                           UnitRefId = invDet.UnitRefId,
                                           UnitRefName = uiss.Name,
                                           YieldPercentage = invDet.YieldPercentage,
                                           YieldMode = invDet.YieldMode,
                                           YieldExcessShortageQty = invDet.YieldExcessShortageQty
                                       }).ToListAsync();
            }

            decimal SubTotal = Math.Round(editDetailDto.Sum(t => t.TotalAmount), 2);
            decimal TaxAmount = Math.Round(editDetailDto.Sum(t => t.TaxAmount), 2);
            decimal NetAmount = Math.Round(hDto.InvoiceAmount, 2);
            hDto.RoundedAmount = Math.Round(hDto.RoundedAmount, 2);
            hDto.TotalShipmentCharges = Math.Round(hDto.TotalShipmentCharges, 2);


            int loopCnt = 0;
            foreach (var det in editDetailDto)
            {
                var taxlist = await (from potax in _invoicetaxdetailRepo.GetAll().Where(pot => pot.InvoiceRefId == input.Id && pot.MaterialRefId == det.MaterialRefId)
                                     join tax in _taxRepo.GetAll()
                                     on potax.TaxRefId equals tax.Id
                                     select new TaxForMaterial
                                     {
                                         TaxRefId = potax.TaxRefId,
                                         TaxName = tax.TaxName,
                                         TaxRate = potax.TaxRate,
                                         TaxValue = potax.TaxValue,
                                         SortOrder = tax.SortOrder,
                                         Rounding = tax.Rounding
                                     }).ToListAsync();

                editDetailDto[loopCnt].TaxForMaterial = taxlist;

                List<ApplicableTaxesForMaterial> applicabletaxes = new List<ApplicableTaxesForMaterial>();

                foreach (var tax in taxlist.OrderBy(t => t.SortOrder))
                {
                    applicabletaxes.Add(new ApplicableTaxesForMaterial
                    {
                        TaxName = tax.TaxName,
                        TaxRefId = tax.TaxRefId,
                        TaxRate = tax.TaxRate,
                        SortOrder = tax.SortOrder,
                        Rounding = tax.Rounding,
                        TaxCalculationMethod = tax.TaxCalculationMethod,
                    });
                }

                editDetailDto[loopCnt].ApplicableTaxes = applicabletaxes;

                loopCnt++;
            }

            editInvoiceDCLink = await (from invDC in _invoiceDirectCreditLinkRepo.GetAll().Where(a => a.InvoiceRefId == input.Id)
                                       select new InvoiceDirectCreditLinkViewDto
                                       {
                                           Id = invDC.Id,
                                           InvoiceRefId = invDC.InvoiceRefId,
                                           InwardDirectCreditRefId = invDC.InwardDirectCreditRefId,
                                           MaterialRefId = invDC.MaterialRefId
                                       }).ToListAsync();


            var supplier = _supplierRepo.FirstOrDefault(t => t.Id == hDto.SupplierRefId);
            var billingLocation = _locationRepo.FirstOrDefault(t => t.Id == hDto.LocationRefId);

            TextFileWriter fp = new TextFileWriter();
            string subPath = "~/Report/";
            bool exists = System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(subPath));


            if (!exists)
                System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(subPath));

            string fileName = System.Web.HttpContext.Current.Server.MapPath("~/Report/" + AbpSession.UserId + "_InvReport.txt");
            int PageLenth = 35;


            fp.FileOpen(fileName, "O");
            if (billingLocation.Name != null)
            {
                fp.PrintTextLine(fp.PrintFormatCenter(billingLocation.Name, PageLenth));
            }

            if (billingLocation.Address1 != null)
            {
                fp.PrintTextLine(fp.PrintFormatCenter(billingLocation.Address1, PageLenth));
            }

            if (billingLocation.Address2 != null)
            {
                fp.PrintTextLine(fp.PrintFormatCenter(billingLocation.Address2, PageLenth));
            }

            if (billingLocation.Address3 != null)
            {
                string addwithCity = billingLocation.Address3;
                if (billingLocation.City != null)
                    addwithCity = addwithCity + ", " + billingLocation.City;
                fp.PrintTextLine(fp.PrintFormatCenter(addwithCity, PageLenth));
            }

            fp.PrintTextLine(fp.PrintFormatCenter(L("Invoice"), PageLenth));

            fp.PrintText(fp.PrintFormatLeft(L("Inv#") + ": " + hDto.InvoiceNumber, 10));
            fp.PrintText(" " + L("Dt") + ": " + hDto.InvoiceDate.ToString("dd/MM/yyyy"));
            //if (hDto.AccountDate!=null)
            //    fp.PrintText(" " + L("Acc Dt") + ": " + hDto.AccountDate.ToString("yy-MMM-dd"));
            fp.PrintTextLine("");

            fp.PrintSepLine('-', PageLenth);

            string retText = System.IO.File.ReadAllText(fileName);

            retText = retText.Replace("\r\n", "<br/>");

            retText = retText.Replace(" ", "&nbsp;");

            output.HeaderText = retText;
            output.BoldHeaderText = "<STRONG>" + retText + "</STRONG>";

            fp.FileOpen(fileName, "O");

            fp.PrintText(L("Sup") + ":");
            if (supplier.SupplierName.Length > 0)
            {
                string supname = supplier.SupplierName;
                if (supname.Length > 28)
                    supname = supname.Left(28);

                fp.PrintTextLine(supname);
            }


            if (supplier.TaxRegistrationNumber != null)
            {
                if (supplier.TaxRegistrationNumber.Length > 0)
                {
                    fp.PrintText(L("Tax#"));
                    fp.PrintTextLine(supplier.TaxRegistrationNumber);
                }
            }
            fp.PrintSepLine('-', PageLenth);

            fp.PrintText(fp.PrintFormatLeft(L("Material"), 18));
            fp.PrintText(fp.PrintFormatRight(L("Qty"), 7));
            fp.PrintText(fp.PrintFormatRight(L("UOM"), 4));
            fp.PrintText(fp.PrintFormatRight(L("Price"), 6));


            fp.PrintTextLine("");
            fp.PrintSepLine('-', PageLenth);

            int loopIndex = 1;

            foreach (var det in editDetailDto)
            {
                string mname = det.MaterialRefName;
                if (mname.Length > 18)
                    mname = mname.Left(18);

                fp.PrintText(fp.PrintFormatLeft(mname, 18));
                decimal qty = Math.Round(det.TotalQty, 2);
                string qtyinstring = string.Concat(new string(' ', 8 - qty.ToString().Length), qty.ToString());

                string unitRefName = det.UnitRefName;
                if (det.UnitRefName.Length > 2)
                    unitRefName = det.UnitRefName.Left(2);

                qtyinstring = qtyinstring + " " + unitRefName;

                fp.PrintText(fp.PrintFormatLeft(qtyinstring, 11));

                decimal price = Math.Round(det.TotalAmount, 0);
                string priceinstring = string.Concat(new string(' ', 6 - price.ToString().Length), price.ToString());
                fp.PrintText(fp.PrintFormatRight(priceinstring, 6));

                fp.PrintTextLine("");
                loopIndex++;
            }

            fp.PrintTextLine(" ");
            fp.PrintSepLine('-', PageLenth);

            fp.PrintText(fp.PrintFormatLeft(L("SubTotal"), 20));
            string subtotalinstring = string.Concat(new string(' ', 15 - SubTotal.ToString().Length), SubTotal.ToString());
            fp.PrintTextLine(fp.PrintFormatRight(subtotalinstring, 15));

            if (TaxAmount > 0)
            {
                fp.PrintText(fp.PrintFormatLeft(L("Tax"), 20));
                string taxamountinstring = string.Concat(new string(' ', 15 - TaxAmount.ToString().Length), TaxAmount.ToString());
                fp.PrintTextLine(fp.PrintFormatRight(taxamountinstring, 15));
            }

            if (hDto.TotalDiscountAmount > 0)
            {
                fp.PrintText(fp.PrintFormatLeft(L("Discount"), 20));
                string discountinstring = string.Concat(new string(' ', 15 - hDto.TotalDiscountAmount.ToString().Length), hDto.TotalDiscountAmount.ToString());
                fp.PrintTextLine(fp.PrintFormatRight(discountinstring, 15));
            }


            if (hDto.TotalShipmentCharges > 0)
            {
                fp.PrintText(fp.PrintFormatLeft(L("Extra"), 20));
                string shipmentinstring = string.Concat(new string(' ', 15 - hDto.TotalShipmentCharges.ToString().Length), hDto.TotalShipmentCharges.ToString());
                fp.PrintTextLine(fp.PrintFormatRight(shipmentinstring, 15));
            }

            if (hDto.RoundedAmount != 0)
            {
                fp.PrintText(fp.PrintFormatLeft(L("RoundedOff"), 20));
                string roundamtinstring = string.Concat(new string(' ', 15 - hDto.RoundedAmount.ToString().Length), hDto.RoundedAmount.ToString());
                fp.PrintTextLine(fp.PrintFormatRight(roundamtinstring, 15));
            }

            fp.PrintText(fp.PrintFormatLeft(L("Total"), 20));
            string totalamtinstring = string.Concat(new string(' ', 15 - NetAmount.ToString().Length), NetAmount.ToString());
            fp.PrintTextLine(fp.PrintFormatRight(totalamtinstring, 15));

            fp.PrintSepLine('-', PageLenth);
            //fp.PrintTextLine(fp.PrintFormatCenter("Print @ " + DateTime.Now.ToString("yy-MMM-dd HH:mm:ss"), PageLenth));
            //fp.PrintSepLine('-', PageLenth);
            //fp.PrintTextLine(" ");

            retText = System.IO.File.ReadAllText(fileName);

            retText = retText.Replace("\r\n", "<br/>");

            //retText = retText.Replace(" ", "&nbsp;");
            output.BodyText = retText;
            output.BoldBodyText = "<STRONG>" + retText + "</STRONG>";

            return output;

        }


        public async Task<PrintOutIn40Cols> GetPrintData(IdInput input)
        {
            PrintOutIn40Cols output = new PrintOutIn40Cols();

            InvoiceEditDto editDto;
            List<InvoiceDetailViewDto> editDetailDto;
            List<InvoiceDirectCreditLinkViewDto> editInvoiceDCLink;

            var hDto = await _invoiceRepo.GetAsync(input.Id);
            editDto = hDto.MapTo<InvoiceEditDto>();

            if (editDto.IsDirectInvoice == false)
            {
                editDetailDto = await (from invDet in _invoicedetailRepo.GetAll()
                                        .Where(a => a.InvoiceRefId == input.Id)
                                       join mat in _materialRepo.GetAll() on invDet.MaterialRefId equals mat.Id
                                       join uiss in _unitRepo.GetAll() on invDet.UnitRefId equals uiss.Id
                                       join invdclnk in _invoiceDirectCreditLinkRepo.GetAll()
                                       on new { a = invDet.InvoiceRefId, b = invDet.Sno }
                                       equals new { a = invdclnk.InvoiceRefId, b = invdclnk.Sno }
                                       where mat.Id == invdclnk.MaterialRefId
                                       select new InvoiceDetailViewDto
                                       {
                                           Id = invDet.Id,
                                           DiscountAmount = invDet.DiscountAmount,
                                           DcTranId = invdclnk.InwardDirectCreditRefId,
                                           Barcode = mat.Barcode,
                                           MaterialRefId = invDet.MaterialRefId,
                                           MaterialRefName = mat.MaterialName,
                                           InvoiceRefId = invDet.InvoiceRefId,
                                           DiscountFlag = invDet.DiscountFlag,
                                           DiscountPercentage = invDet.DiscountPercentage,
                                           NetAmount = invDet.NetAmount,
                                           Price = invDet.Price,
                                           TaxAmount = invDet.TaxAmount,
                                           TotalAmount = invDet.TotalAmount,
                                           TotalQty = invDet.TotalQty,
                                           Remarks = invDet.Remarks,
                                           Sno = invDet.Sno,
                                           Uom = uiss.Name,
                                           UnitRefId = invDet.UnitRefId,
                                           UnitRefName = uiss.Name,
                                           YieldPercentage = invDet.YieldPercentage,
                                           YieldMode = invDet.YieldMode,
                                           YieldExcessShortageQty = invDet.YieldExcessShortageQty
                                       }).ToListAsync();
            }
            else
            {
                editDetailDto = await (from invDet in _invoicedetailRepo.GetAll()
                                        .Where(a => a.InvoiceRefId == input.Id)
                                       join mat in _materialRepo.GetAll() on invDet.MaterialRefId equals mat.Id
                                       join uiss in _unitRepo.GetAll() on invDet.UnitRefId equals uiss.Id
                                       select new InvoiceDetailViewDto
                                       {
                                           Id = invDet.Id,
                                           DiscountAmount = invDet.DiscountAmount,
                                           DcTranId = 0,
                                           Barcode = mat.Barcode,
                                           MaterialRefId = invDet.MaterialRefId,
                                           MaterialRefName = mat.MaterialName,
                                           InvoiceRefId = invDet.InvoiceRefId,
                                           DiscountFlag = invDet.DiscountFlag,
                                           DiscountPercentage = invDet.DiscountPercentage,
                                           NetAmount = invDet.NetAmount,
                                           Price = invDet.Price,
                                           TaxAmount = invDet.TaxAmount,
                                           TotalAmount = invDet.TotalAmount,
                                           TotalQty = invDet.TotalQty,
                                           Remarks = invDet.Remarks,
                                           Sno = invDet.Sno,
                                           Uom = uiss.Name,
                                           UnitRefId = invDet.UnitRefId,
                                           UnitRefName = uiss.Name,
                                           YieldPercentage = invDet.YieldPercentage,
                                           YieldMode = invDet.YieldMode,
                                           YieldExcessShortageQty = invDet.YieldExcessShortageQty
                                       }).ToListAsync();
            }

            decimal SubTotal = Math.Round(editDetailDto.Sum(t => t.TotalAmount), 2);
            decimal TaxAmount = Math.Round(editDetailDto.Sum(t => t.TaxAmount), 2);
            decimal NetAmount = Math.Round(hDto.InvoiceAmount, 2);
            hDto.RoundedAmount = Math.Round(hDto.RoundedAmount, 2);
            hDto.TotalShipmentCharges = Math.Round(hDto.TotalShipmentCharges, 2);


            int loopCnt = 0;
            foreach (var det in editDetailDto)
            {
                var taxlist = await (from potax in _invoicetaxdetailRepo.GetAll().Where(pot => pot.InvoiceRefId == input.Id && pot.MaterialRefId == det.MaterialRefId)
                                     join tax in _taxRepo.GetAll()
                                     on potax.TaxRefId equals tax.Id
                                     select new TaxForMaterial
                                     {
                                         TaxRefId = potax.TaxRefId,
                                         TaxName = tax.TaxName,
                                         TaxRate = potax.TaxRate,
                                         TaxValue = potax.TaxValue,
                                         SortOrder = tax.SortOrder,
                                         Rounding = tax.Rounding
                                     }).ToListAsync();

                editDetailDto[loopCnt].TaxForMaterial = taxlist;

                List<ApplicableTaxesForMaterial> applicabletaxes = new List<ApplicableTaxesForMaterial>();

                foreach (var tax in taxlist.OrderBy(t => t.SortOrder))
                {
                    applicabletaxes.Add(new ApplicableTaxesForMaterial
                    {
                        TaxName = tax.TaxName,
                        TaxRefId = tax.TaxRefId,
                        TaxRate = tax.TaxRate,
                        SortOrder = tax.SortOrder,
                        Rounding = tax.Rounding,
                        TaxCalculationMethod = tax.TaxCalculationMethod,
                    });
                }

                editDetailDto[loopCnt].ApplicableTaxes = applicabletaxes;

                loopCnt++;
            }

            editInvoiceDCLink = await (from invDC in _invoiceDirectCreditLinkRepo.GetAll().Where(a => a.InvoiceRefId == input.Id)
                                       select new InvoiceDirectCreditLinkViewDto
                                       {
                                           Id = invDC.Id,
                                           InvoiceRefId = invDC.InvoiceRefId,
                                           InwardDirectCreditRefId = invDC.InwardDirectCreditRefId,
                                           MaterialRefId = invDC.MaterialRefId
                                       }).ToListAsync();


            var supplier = _supplierRepo.FirstOrDefault(t => t.Id == hDto.SupplierRefId);
            var billingLocation = _locationRepo.FirstOrDefault(t => t.Id == hDto.LocationRefId);

            TextFileWriter fp = new TextFileWriter();
            string subPath = "~/Report/";
            bool exists = System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(subPath));


            if (!exists)
                System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(subPath));

            string fileName = System.Web.HttpContext.Current.Server.MapPath("~/Report/" + AbpSession.UserId + "_InvReport.txt");
            int PageLenth = 35;


            fp.FileOpen(fileName, "O");
            if (billingLocation.Name != null)
            {
                fp.PrintTextLine(fp.PrintFormatCenter(billingLocation.Name, PageLenth));
            }

            if (billingLocation.Address1 != null)
            {
                fp.PrintTextLine(fp.PrintFormatCenter(billingLocation.Address1, PageLenth));
            }

            if (billingLocation.Address2 != null)
            {
                fp.PrintTextLine(fp.PrintFormatCenter(billingLocation.Address2, PageLenth));
            }

            if (billingLocation.Address3 != null)
            {
                string addwithCity = billingLocation.Address3;
                if (billingLocation.City != null)
                    addwithCity = addwithCity + ", " + billingLocation.City;
                fp.PrintTextLine(fp.PrintFormatCenter(addwithCity, PageLenth));
            }

            fp.PrintTextLine(fp.PrintFormatCenter(L("Invoice"), PageLenth));

            fp.PrintText(fp.PrintFormatLeft(L("Inv#") + ": " + hDto.InvoiceNumber, 10));
            fp.PrintText(" " + L("Dt") + ": " + hDto.InvoiceDate.ToString("dd/MM/yyyy"));
            //if (hDto.AccountDate!=null)
            //    fp.PrintText(" " + L("Acc Dt") + ": " + hDto.AccountDate.ToString("yy-MMM-dd"));
            fp.PrintTextLine("");

            fp.PrintSepLine('-', PageLenth);

            string retText = System.IO.File.ReadAllText(fileName);

            retText = retText.Replace("\r\n", "<br/>");

            retText = retText.Replace(" ", "&nbsp;");

            output.HeaderText = retText;
            output.BoldHeaderText = "<STRONG>" + retText + "</STRONG>";

            fp.FileOpen(fileName, "O");

            fp.PrintText(L("Sup") + ":");
            if (supplier.SupplierName.Length > 0)
            {
                string supname = supplier.SupplierName;
                if (supname.Length > 28)
                    supname = supname.Left(28);

                fp.PrintTextLine(supname);
            }


            if (supplier.TaxRegistrationNumber != null)
            {
                if (supplier.TaxRegistrationNumber.Length > 0)
                {
                    fp.PrintText(L("Tax#"));
                    fp.PrintTextLine(supplier.TaxRegistrationNumber);
                }
            }
            fp.PrintSepLine('-', PageLenth);

            fp.PrintText(fp.PrintFormatLeft(L("Material"), 16));
            fp.PrintText(fp.PrintFormatRight(L("Qty"), 7));
            fp.PrintText(fp.PrintFormatRight(L("UOM"), 6));
            fp.PrintText(fp.PrintFormatRight(L("Price"), 6));


            fp.PrintTextLine("");
            fp.PrintSepLine('-', PageLenth);

            int loopIndex = 1;

            foreach (var det in editDetailDto)
            {
                string mname = det.MaterialRefName;
                if (mname.Length > 16)
                    mname = mname.Left(16);

                fp.PrintText(fp.PrintFormatLeft(mname, 16));
                decimal qty = Math.Round(det.TotalQty, 2);
                string qtyinstring = string.Concat(new string(' ', 8 - qty.ToString().Length), qty.ToString());

                string unitRefName = det.UnitRefName;
                if (det.UnitRefName.Length > 4)
                    unitRefName = det.UnitRefName.Left(4);
                else
                    unitRefName = string.Concat(unitRefName, new string(' ', 4 - unitRefName.Length));

                qtyinstring = qtyinstring + " " + unitRefName;

                fp.PrintText(fp.PrintFormatLeft(qtyinstring, 11));

                decimal price = Math.Round(det.TotalAmount, 2);
                fp.PrintText(fp.PrintFormatRight(price.ToString(), 6));

                fp.PrintTextLine("");
                loopIndex++;
            }

            fp.PrintTextLine(" ");
            fp.PrintSepLine('-', PageLenth);

            fp.PrintText(fp.PrintFormatLeft(L("SubTotal"), 20));
            string subtotalinstring = string.Concat(new string(' ', 15 - SubTotal.ToString().Length), SubTotal.ToString());
            fp.PrintTextLine(fp.PrintFormatRight(subtotalinstring, 15));

            if (TaxAmount > 0)
            {
                fp.PrintText(fp.PrintFormatLeft(L("Tax"), 20));
                string taxamountinstring = string.Concat(new string(' ', 15 - TaxAmount.ToString().Length), TaxAmount.ToString());
                fp.PrintTextLine(fp.PrintFormatRight(taxamountinstring, 15));
            }

            if (hDto.TotalDiscountAmount > 0)
            {
                fp.PrintText(fp.PrintFormatLeft(L("Discount"), 20));
                string discountinstring = string.Concat(new string(' ', 15 - hDto.TotalDiscountAmount.ToString().Length), hDto.TotalDiscountAmount.ToString());
                fp.PrintTextLine(fp.PrintFormatRight(discountinstring, 15));
            }


            if (hDto.TotalShipmentCharges > 0)
            {
                fp.PrintText(fp.PrintFormatLeft(L("Extra"), 20));
                string shipmentinstring = string.Concat(new string(' ', 15 - hDto.TotalShipmentCharges.ToString().Length), hDto.TotalShipmentCharges.ToString());
                fp.PrintTextLine(fp.PrintFormatRight(shipmentinstring, 15));
            }

            if (hDto.RoundedAmount != 0)
            {
                fp.PrintText(fp.PrintFormatLeft(L("RoundedOff"), 20));
                string roundamtinstring = string.Concat(new string(' ', 15 - hDto.RoundedAmount.ToString().Length), hDto.RoundedAmount.ToString());
                fp.PrintTextLine(fp.PrintFormatRight(roundamtinstring, 15));
            }

            fp.PrintText(fp.PrintFormatLeft(L("Total"), 20));
            string totalamtinstring = string.Concat(new string(' ', 15 - NetAmount.ToString().Length), NetAmount.ToString());
            fp.PrintTextLine(fp.PrintFormatRight(totalamtinstring, 15));

            fp.PrintSepLine('-', PageLenth);
            //fp.PrintTextLine(fp.PrintFormatCenter("Print @ " + DateTime.Now.ToString("yy-MMM-dd HH:mm:ss"), PageLenth));
            //fp.PrintSepLine('-', PageLenth);
            //fp.PrintTextLine(" ");

            retText = System.IO.File.ReadAllText(fileName);

            retText = retText.Replace("\r\n", "<br/>");

            //retText = retText.Replace(" ", "&nbsp;");
            output.BodyText = retText;
            output.BoldBodyText = "<STRONG>" + retText + "</STRONG>";

            return output;

        }

        public async Task<ListResultOutput<SupplierMaterialViewDto>> GetMaterialBasedOnSupplierAndInvoice(SentLocationAndSupplier input)
        {
            List<SupplierMaterialViewDto> lstMaterial;

            lstMaterial = await (from mat in _materialRepo.GetAll()
                                 join invdet in _invoicedetailRepo.GetAll().
                                     Where(sp => sp.InvoiceRefId == input.InvoiceRefId)
                                 on mat.Id equals invdet.MaterialRefId
                                 join matloc in _materiallocationwisestockRepo.GetAll().
                                     Where(mls => mls.LocationRefId == input.LocationRefId)
                                 on mat.Id equals matloc.MaterialRefId
                                 join un in _unitRepo.GetAll()
                                 on mat.DefaultUnitId equals un.Id
                                 select new SupplierMaterialViewDto
                                 {
                                     Barcode = mat.Barcode,
                                     MaterialRefId = mat.Id,
                                     MaterialRefName = mat.MaterialName,
                                     MaterialGroupCategoryRefId = mat.MaterialGroupCategoryRefId,
                                     MaterialPrice = invdet.Price,
                                     MinimumOrderQuantity = 0,
                                     MaximumOrderQuantity = invdet.TotalQty,
                                     CurrentInHand = matloc.CurrentInHand,
                                     AlreadyOrderedQuantity = 0,
                                     Uom = un.Name,
                                     UnitRefId = mat.DefaultUnitId,
                                     IsQuoteNeededForPurchase = mat.IsQuoteNeededForPurchase,
                                     IsFractional = mat.IsFractional,
                                     IsBranded = mat.IsBranded
                                 }).ToListAsync();


            var materialRefIds = lstMaterial.Select(t => t.MaterialRefId).ToList();
            var rsSupplierMaterials = await _suppliermaterialRepo.GetAllListAsync(t => t.SupplierRefId == input.SupplierRefId && materialRefIds.Contains(t.MaterialRefId));

            foreach (var lst in lstMaterial)
            {
                var supmat = rsSupplierMaterials.FirstOrDefault(t => t.LocationRefId == lst.LocationRefId && t.MaterialRefId == lst.MaterialRefId);
                if (supmat == null)
                {
                    supmat = rsSupplierMaterials.FirstOrDefault(t => t.LocationRefId == null && t.MaterialRefId == lst.MaterialRefId);
                }
                if (supmat != null)
                {
                    lst.SupplierMaterialAliasName = supmat.SupplierMaterialAliasName;
                }
            }




            var rsPMasAlready = await _purchaseReturnRepo.GetAll().Where(t => t.InvoiceRefId == input.InvoiceRefId).ToListAsync();

            int[] rsMasIds = rsPMasAlready.Select(t => t.Id).ToArray();

            var lstAlreadyReturnedList = await (from mat in _materialRepo.GetAll()
                                                join retDet in _purchasereturnDetailRepo.GetAll().
                                                    Where(sp => rsMasIds.Contains(sp.PurchaseReturnRefId))
                                                on mat.Id equals retDet.MaterialRefId
                                                join un in _unitRepo.GetAll()
                                                on mat.DefaultUnitId equals un.Id
                                                join uiss in _unitRepo.GetAll()
                                             on retDet.UnitRefId equals uiss.Id
                                                select new SupplierReturnedMaterialViewDto
                                                {
                                                    MaterialRefId = mat.Id,
                                                    MaterialRefName = mat.MaterialName,
                                                    MaterialPrice = retDet.Price,
                                                    UnitRefId = retDet.UnitRefId,
                                                    UnitRefName = uiss.Name,
                                                    AlreadyReturnedQuantity = retDet.Quantity,
                                                    DefaultUnitId = mat.DefaultUnitId,
                                                    DefaultUnitName = un.Name,
                                                }).ToListAsync();
            var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();


            var lstGroupAlreadyReturnedList = (from salemenu in lstAlreadyReturnedList
                                               join uc in rsUc
                                               on salemenu.UnitRefId equals uc.BaseUnitId
                                               where uc.RefUnitId == salemenu.DefaultUnitId
                                               group salemenu by new
                                               {
                                                   salemenu.MaterialRefId,
                                                   salemenu.MaterialRefName,
                                                   salemenu.DefaultUnitId,
                                                   salemenu.DefaultUnitName,
                                                   uc.Conversion,
                                               } into g
                                               select new SupplierReturnedMaterialViewDto
                                               {
                                                   MaterialRefId = g.Key.MaterialRefId,
                                                   MaterialRefName = g.Key.MaterialRefName,
                                                   DefaultUnitId = g.Key.DefaultUnitId,
                                                   DefaultUnitName = g.Key.DefaultUnitName,
                                                   AlreadyReturnedQuantity = g.Sum(t => t.AlreadyReturnedQuantity * g.Key.Conversion),
                                               }).ToList().OrderBy(t => t.MaterialRefId);

            int loopcnt = 0;

            int companyRefid = await _locationRepo.GetAll().Where(l => l.Id == input.LocationRefId).Select(l => l.CompanyRefId).FirstOrDefaultAsync();

            var invTaxesList = await _invoicetaxdetailRepo.GetAll().Where(pot => pot.InvoiceRefId == input.InvoiceRefId).ToListAsync();

            var rsTaxes = await _taxRepo.GetAllListAsync();

            foreach (var mat in lstMaterial)
            {
                List<TaxForMaterial> taxtoAdded = new List<TaxForMaterial>();
                List<TaxForMaterial> sortedTaxToAdded = new List<TaxForMaterial>();

                // taxtemplate;
                // Find any specific TAX For particular Material
                //List<int> taxTemplates;
                List<int> outputTaxTemplates = new List<int>();

                var taxTemplates = await _taxtemplatemappingRepo.GetAll().Where(t => t.MaterialRefId == mat.MaterialRefId).Select(t => t.TaxRefId).ToArrayAsync();

                outputTaxTemplates.AddRange(taxTemplates);
                taxTemplates = null;

                taxTemplates = await _taxtemplatemappingRepo.GetAll().Where(t => t.MaterialGroupCategoryRefId == mat.MaterialGroupCategoryRefId && t.MaterialRefId == null).Select(t => t.TaxRefId).ToArrayAsync();
                outputTaxTemplates.AddRange(taxTemplates);
                taxTemplates = null;

                taxTemplates = await _taxtemplatemappingRepo.GetAll().Where(t => t.MaterialGroupRefId == mat.MaterialGroupRefId && t.MaterialGroupCategoryRefId == null && t.MaterialRefId == null).Select(t => t.TaxRefId).ToArrayAsync();
                outputTaxTemplates.AddRange(taxTemplates);
                taxTemplates = null;

                taxTemplates = await _taxtemplatemappingRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId && t.MaterialGroupRefId == null).Select(t => t.TaxRefId).ToArrayAsync();
                outputTaxTemplates.AddRange(taxTemplates);
                taxTemplates = null;

                taxTemplates = await _taxtemplatemappingRepo.GetAll().Where(t => t.CompanyRefId == companyRefid && t.LocationRefId == input.LocationRefId).Select(t => t.TaxRefId).ToArrayAsync();
                outputTaxTemplates.AddRange(taxTemplates);
                taxTemplates = null;

                taxTemplates = await _taxtemplatemappingRepo.GetAll().Where(t => t.LocationRefId == null && t.CompanyRefId == null && t.MaterialGroupRefId == null && t.MaterialGroupCategoryRefId == null && t.MaterialRefId == null).Select(t => t.TaxRefId).ToArrayAsync();
                outputTaxTemplates.AddRange(taxTemplates);
                taxTemplates = null;

                outputTaxTemplates = outputTaxTemplates.Distinct().MapTo<List<int>>();

                List<ApplicableTaxesForMaterial> applicabletaxes = new List<ApplicableTaxesForMaterial>();

                if (outputTaxTemplates.Count() > 0)
                {
                    foreach (var taxtemplate in outputTaxTemplates)
                    {
                        var tax = _taxRepo.Single(t => t.Id == taxtemplate);
                        taxtoAdded.Add(new TaxForMaterial
                        {
                            MaterialRefId = mat.MaterialRefId,
                            TaxName = tax.TaxName,
                            TaxRefId = taxtemplate,
                            TaxRate = tax.Percentage,
                            SortOrder = tax.SortOrder,
                            Rounding = tax.Rounding,
                            TaxCalculationMethod = tax.TaxCalculationMethod,
                        }
                        );
                    }
                    sortedTaxToAdded = taxtoAdded.OrderBy(t => t.SortOrder).ToList();



                    var taxlist = (from potax in invTaxesList.Where(pot => pot.InvoiceRefId == input.InvoiceRefId && pot.MaterialRefId == mat.MaterialRefId)
                                   join tax in rsTaxes
                                   on potax.TaxRefId equals tax.Id
                                   select new TaxForMaterial
                                   {
                                       TaxRefId = potax.TaxRefId,
                                       TaxName = tax.TaxName,
                                       TaxRate = potax.TaxRate,
                                       TaxValue = potax.TaxValue,
                                       SortOrder = tax.SortOrder,
                                       Rounding = tax.Rounding
                                   }).ToList();

                    foreach (var tax in sortedTaxToAdded)
                    {
                        var taxExist = taxlist.FirstOrDefault(t => t.TaxRefId == tax.TaxRefId);
                        if (taxExist != null)
                        {
                            applicabletaxes.Add(new ApplicableTaxesForMaterial
                            {
                                TaxName = tax.TaxName,
                                TaxRefId = tax.TaxRefId,
                                TaxRate = tax.TaxRate,
                                SortOrder = tax.SortOrder,
                                Rounding = tax.Rounding,
                                TaxCalculationMethod = tax.TaxCalculationMethod,
                            });
                        }
                    }
                }
                //@@Pending Sort Based On SortOrder

                lstMaterial[loopcnt].TaxForMaterial = sortedTaxToAdded;
                lstMaterial[loopcnt].ApplicableTaxes = applicabletaxes;


                var alreadyReturned = lstGroupAlreadyReturnedList.FirstOrDefault(t => t.MaterialRefId == mat.MaterialRefId);
                if (alreadyReturned == null)
                    lstMaterial[loopcnt].AlreadyReturnedQuantity = 0;
                else
                    lstMaterial[loopcnt].AlreadyReturnedQuantity = alreadyReturned.AlreadyReturnedQuantity;

                outputTaxTemplates = null;
                taxtoAdded = null;
                loopcnt++;
            }

            var retlst = new ListResultOutput<SupplierMaterialViewDto>(lstMaterial.MapTo<List<SupplierMaterialViewDto>>());

            return retlst;

        }

        public async Task<PagedResultOutput<InvoiceListDto>> InvoiceAlreadyExists(GetInvoiceInput input)
        {

            var allItems = await (from inv in _invoiceRepo.GetAll()
                                .Where(t => t.LocationRefId == input.LocationRefId && t.SupplierRefId == input.SupplierRefId && DbFunctions.TruncateTime(t.InvoiceDate) == DbFunctions.TruncateTime(input.StartDate) && t.InvoiceNumber == input.InvoiceNumber)
                                  select new InvoiceListDto
                                  {
                                      InvoiceAmount = inv.InvoiceAmount,
                                      InvoiceDate = inv.InvoiceDate,
                                      AccountDate = inv.AccountDate,
                                      InvoiceNumber = inv.InvoiceNumber,
                                      SupplierRefId = inv.SupplierRefId,
                                      TotalShipmentCharges = inv.TotalShipmentCharges,
                                      Id = inv.Id,
                                      IsDirectInvoice = inv.IsDirectInvoice,
                                      PurchaseCategoryRefId = inv.PurchaseCategoryRefId,
                                      CreationTime = inv.CreationTime
                                  }).ToListAsync();

            var allItemCount = allItems.Count();

            return new PagedResultOutput<InvoiceListDto>(
                allItemCount,
                allItems
                );
        }


        public async Task<List<GetInvoiceForEditOutput>> GetPurchaseInvoiceDetails(InputInvoiceAnalysis input)
        {
            List<GetInvoiceForEditOutput> output = new List<GetInvoiceForEditOutput>();

            var invoiceMaster = _invoiceRepo.GetAll();
            if (input.InvoiceId > 0)
            {
                var myInvoice = await _invoiceRepo.GetAsync(input.InvoiceId);
                GetInvoiceForEditOutput dto = await GetInvoiceForEdit(new NullableIdInput { Id = myInvoice.Id });
                output.Add(dto);
            }
            else
            {
                if (input.SupplierRefId != 0)
                {
                    invoiceMaster = invoiceMaster.Where(t => t.SupplierRefId == input.SupplierRefId);
                }

                if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
                {
                    invoiceMaster =
                        invoiceMaster.Where(
                            a => DbFunctions.TruncateTime(a.InvoiceDate) >= DbFunctions.TruncateTime(input.StartDate)
                                 && DbFunctions.TruncateTime(a.InvoiceDate) <= DbFunctions.TruncateTime(input.EndDate));
                }

                if (input.LocationRefId != 0)
                {
                    invoiceMaster = invoiceMaster.Where(t => t.LocationRefId == input.LocationRefId);
                }

                if (input.InvoiceNumber != null)
                {
                    if (input.ExactSearchFlag == false)
                        invoiceMaster = invoiceMaster.Where(t => t.InvoiceNumber.Contains(input.InvoiceNumber));
                    else
                        invoiceMaster = invoiceMaster.Where(t => t.InvoiceNumber.Equals(input.InvoiceNumber));
                }

                foreach (var inv in invoiceMaster)
                {
                    GetInvoiceForEditOutput dto = await GetInvoiceForEdit(new NullableIdInput { Id = inv.Id });
                    output.Add(dto);
                }
            }

            return output;
        }

        public async Task<FileDto> GetPurchaseInvoiceDetailsToExcel(InputInvoiceDetailExcel input)
        {
            var allListDtos = input.Dtos;
            return _invoiceExporter.ExportInvoiceDetailsToFile(allListDtos);
        }

        public async Task<FileDto> GetAllToExcel(GetInvoiceInput input)
        {
            input.MaxResultCount = AppConsts.MaxPageSize;
            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<InvoiceListDto>>();
            return _invoiceExporter.ExportToFile(allListDtos);
        }

        public async Task<GetInvoiceDraftReference> CreateOrUpdateInvoiceDraft(CreateOrUpdateInvoiceInput input)
        {
            var existAlready = await InvoiceAlreadyExists(new GetInvoiceInput
            {
                StartDate = input.Invoice.InvoiceDate,
                LocationRefId = input.Invoice.LocationRefId,
                SupplierRefId = input.Invoice.SupplierRefId
            });

            if (existAlready.TotalCount > 0)
            {
                throw new UserFriendlyException(L("InvoiceAlreadyExists", existAlready.Items[0].InvoiceNumber));
            }

            IdInput ret = new IdInput();

            input.Invoice.DcFromDate = input.Invoice.InvoiceDate;
            input.Invoice.DcToDate = input.Invoice.InvoiceDate;

            string templateJson = _xmlandjsonConvertorAppService.SerializeToJSON(input);

            Template lastTemplate;
            int templateScope;
            templateScope = input.Invoice.LocationRefId;

            var sup = await _supplierRepo.FirstOrDefaultAsync(t => t.Id == input.Invoice.SupplierRefId);
            if (sup == null)
            {
                throw new UserFriendlyException(L("Supplier" + " " + L
                    ("NotExist")));
            }
            string supName = sup.SupplierName;
            if (supName.Length > 15)
                supName = supName.Left(15);

            string repName = supName + "/" + input.Invoice.InvoiceDate.ToString("dd-MMM-yy") + "/" + input.Invoice.InvoiceNumber;

            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.Invoice.LocationRefId);

            var templateExist = _templateRepo.GetAll().Where(t => t.Name == repName && t.TemplateType == (int)TemplateType.Invoice).ToList();
            if (templateExist.Count() == 0)
            {
                lastTemplate = new Template
                {
                    Name = repName,
                    TemplateType = (int)TemplateType.Invoice,
                    TemplateData = templateJson,
                    LocationRefId = input.Invoice.LocationRefId
                };
            }
            else
            {
                lastTemplate = templateExist[0];
                lastTemplate.TemplateType = (int)TemplateType.Invoice;
                lastTemplate.LocationRefId = templateScope;
                lastTemplate.TemplateData = templateJson;
            }
            lastTemplate.TenantId = loc.TenantId;

            await _templateRepo.InsertOrUpdateAndGetIdAsync(lastTemplate);

            return new GetInvoiceDraftReference
            {
                InvoiceDraftReference = repName
            };

        }

        public async Task<GetInvoiceForEditOutput> GetTemplateObjectForEdit(GetObjectFromString input)
        {
            GetInvoiceForEditOutput ReturnObject = _xmlandjsonConvertorAppService.DeSerializeFromJSON<GetInvoiceForEditOutput>(input.ObjectString);

            InvoiceEditDto editDto = ReturnObject.Invoice;

            List<InvoiceDetailViewDto> editDetailDto = ReturnObject.InvoiceDetail;

            //foreach (var pd in editDetailDto)
            //{

            //}

            //var a = (from poDet in editDetailDto
            //		 join mat in _materialRepo.GetAll()
            //		 on poDet.MaterialRefId equals mat.Id
            //		 //join supmat in _suppliermaterialRepo.GetAll().Where(sp=>sp.SupplierRefId == editDto.SupplierRefId)
            //		 //on poDet.MaterialRefId equals supmat.MaterialRefId 
            //		 select new PurchaseOrderDetailViewDto
            //		 {
            //			 PoRefId = (int)poDet.PoRefId,
            //			 MaterialRefId = poDet.MaterialRefId,
            //			 Barcode = mat.Barcode,
            //			 MaterialRefName = mat.MaterialName,
            //			 QtyOrdered = poDet.QtyOrdered,
            //			 Price = poDet.Price,
            //			 DiscountOption = poDet.DiscountOption,
            //			 DiscountValue = poDet.DiscountValue,
            //			 TaxAmount = poDet.TaxAmount,
            //			 TotalAmount = poDet.TotalAmount,
            //			 UnitRefId = poDet.UnitRefId,
            //			 UnitRefName = poDet.UnitRefName,
            //			 Uom = poDet.Uom,
            //			 DefaultUnitId = mat.DefaultUnitId,
            //			 NetAmount = (poDet.QtyOrdered * poDet.Price),
            //			 PoStatus = poDet.PoStatus,
            //			 Remarks = poDet.Remarks,
            //			 TaxForMaterial = poDet.TaxForMaterial,
            //			 ApplicableTaxes = poDet.ApplicableTaxes
            //		 });

            //List<PurchaseOrderDetailViewDto> b = a.ToList();

            foreach (var lst in editDetailDto)

            {
                var rsMaterial = await _materialRepo.FirstOrDefaultAsync(t => t.Id == lst.MaterialRefId);
                lst.MaterialRefName = rsMaterial.MaterialName;
            }

            return new GetInvoiceForEditOutput
            {
                Invoice = editDto,
                InvoiceDetail = editDetailDto,
                InvoiceDirectCreditLink = new List<InvoiceDirectCreditLinkViewDto>()
            };
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetPurchaseCategoryForCombobox()
        {
            var lstPurchaseCategory = await _purchaseCategoryRepo.GetAll().ToListAsync();

            return
                new ListResultOutput<ComboboxItemDto>(
                    lstPurchaseCategory.Select(e => new ComboboxItemDto(e.Id.ToString(), e.PurchaseCategoryName)).ToList());
        }

        protected virtual async Task<IdInput> CreatePurchaseYieldAdjustment(CreateOrUpdateAdjustmentInput input)
        {
            var dto = input.Adjustment.MapTo<Adjustment>();

            var locationAdj = await _locationRepo.FirstOrDefaultAsync(t => t.Id == dto.LocationRefId);
            dto.AccountDate = locationAdj.HouseTransactionDate.Value;

            var retId = await _adjustmentRepo.InsertOrUpdateAndGetIdAsync(dto);

            foreach (AdjustmentDetailViewDto items in input.AdjustmentDetail.ToList())
            {
                AdjustmentDetail itdDto = new AdjustmentDetail
                {
                    AdjustmentRefIf = retId,
                    MaterialRefId = items.MaterialRefId,
                    AdjustmentQty = items.AdjustmentQty,
                    ClosingStock = items.ClosingStock,
                    StockEntry = items.StockEntry,
                    AdjustmentMode = items.AdjustmentMode,
                    AdjustmentApprovedRemarks = items.AdjustmentApprovedRemarks,
                    Sno = items.Sno,
                    UnitRefId = items.UnitRefId
                };


                MaterialLedgerDto ml = new MaterialLedgerDto
                {
                    LocationRefId = dto.LocationRefId,
                    MaterialRefId = itdDto.MaterialRefId,
                    LedgerDate = dto.AdjustmentDate
                };

                if (input.ForceAdjustmentFlag)
                {
                    ml.ForceAdjustmentFlag = true;
                }
                else
                {
                    ml.ForceAdjustmentFlag = false;
                }

                if (itdDto.AdjustmentMode.Equals(L("Excess")))
                    ml.ExcessReceived = itdDto.AdjustmentQty;
                else if (itdDto.AdjustmentMode.Equals(L("Shortage")))
                    ml.Shortage = itdDto.AdjustmentQty;
                else if (itdDto.AdjustmentMode.Equals(L("Wastage")))
                    ml.Damaged = itdDto.AdjustmentQty;
                else if (itdDto.AdjustmentMode.Equals(L("Damaged")))
                    ml.Damaged = itdDto.AdjustmentQty;
                else if (itdDto.AdjustmentMode.Equals(L("Equal")))
                    ml.ExcessReceived = itdDto.AdjustmentQty;
                else
                    throw new UserFriendlyException(L("AdjustmentMode") + " " + L("Error") + itdDto.AdjustmentMode);
                ml.UnitId = itdDto.UnitRefId;
                ml.SupplierRefId = input.SupplierRefId;

                var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                itdDto.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;

                await _adjustmentDetailRepo.InsertAndGetIdAsync(itdDto);
            }

            return new IdInput
            {
                Id = retId
            };
        }

        public async Task<PurchaseInvoiceTaxDetailOutputDto> GetPurchaseInvoiceTaxDetails(PurchaseInvoiceTaxDetailInputDto input)
        {
            PurchaseInvoiceTaxDetailOutputDto outputDto = new PurchaseInvoiceTaxDetailOutputDto();
            var invoiceMaster = _invoiceRepo.GetAll();

            var invoiceDetail = _invoicedetailRepo.GetAll();

            if (input.SupplierRefId > 0)
            {
                invoiceMaster = invoiceMaster.Where(t => t.SupplierRefId == input.SupplierRefId);
            }

            if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
            {
                invoiceMaster = invoiceMaster.Where(
                        a => DbFunctions.TruncateTime(a.InvoiceDate) >= DbFunctions.TruncateTime(input.StartDate)
                             && DbFunctions.TruncateTime(a.InvoiceDate) <= DbFunctions.TruncateTime(input.EndDate));
            }

            if (input.LocationRefId != 0)
            {
                invoiceMaster = invoiceMaster.Where(t => t.LocationRefId == input.LocationRefId);
            }

            if (input.InvoiceNumber != null)
            {
                if (input.ExactSearchFlag == false)
                    invoiceMaster = invoiceMaster.Where(t => t.InvoiceNumber.Contains(input.InvoiceNumber));
                else
                    invoiceMaster = invoiceMaster.Where(t => t.InvoiceNumber.Equals(input.InvoiceNumber));
            }

            if (input.MaterialRefId != 0)
            {
                invoiceDetail = invoiceDetail.Where(t => t.MaterialRefId == input.MaterialRefId);
            }
            else
            {
                if (invoiceMaster.Any())
                {
                    outputDto.TotalShipmentCharges = invoiceMaster.Sum(t => t.TotalShipmentCharges);
                    outputDto.TotalRoundedAmount = invoiceMaster.Sum(t => t.RoundedAmount);
                    outputDto.TotalDiscountAmount = invoiceMaster.Sum(t => t.TotalDiscountAmount);
                }
            }


            var allItems = (from invmas in invoiceMaster
                            join invdet in invoiceDetail on invmas.Id equals invdet.InvoiceRefId
                            join sup in _supplierRepo.GetAll() on invmas.SupplierRefId equals sup.Id
                            join mat in _materialRepo.GetAll() on invdet.MaterialRefId equals mat.Id
                            join unit in _unitRepo.GetAll() on mat.DefaultUnitId equals unit.Id
                            join uiss in _unitRepo.GetAll() on invdet.UnitRefId equals uiss.Id
                            select new InvoiceAnalysisDto
                            {
                                InvoiceDate = invmas.InvoiceDate,
                                InvoiceNumber = invmas.InvoiceNumber,
                                InvoiceRefId = invmas.Id,
                                SupplierRefId = invmas.SupplierRefId,
                                SupplierRefName = sup.SupplierName,
                                MaterialRefId = invdet.MaterialRefId,
                                MaterialPetName = mat.MaterialPetName,
                                MaterialRefName = mat.MaterialName,
                                TotalQty = invdet.TotalQty,
                                Price = invdet.Price,
                                DiscountAmount = invdet.DiscountAmount,
                                DiscountFlag = invdet.DiscountFlag,
                                DiscountPercentage = invdet.DiscountPercentage,
                                TotalAmount = invdet.TotalAmount,
                                TaxAmount = invdet.TaxAmount,
                                NetAmount = invdet.NetAmount,
                                Remarks = invdet.Remarks,
                                Uom = unit.Name,
                                DefaultUnitId = mat.DefaultUnitId,
                                DefaultUnitName = unit.Name,
                                UnitRefId = invdet.UnitRefId,
                                UnitRefName = uiss.Name,
                            }).ToList();

            List<InvoiceAnalysisDto> sortMenuItems;

            sortMenuItems = allItems.ToList();
            List<int> arrInvoiceRefIds = sortMenuItems.Select(t => t.InvoiceRefId).ToList();
            List<InvoiceDirectCreditLinkViewDto> editInvoiceDCLink = await (from invDC in _invoiceDirectCreditLinkRepo.GetAll()
                                      .Where(a => arrInvoiceRefIds.Contains(a.InvoiceRefId))
                                                                            select new InvoiceDirectCreditLinkViewDto
                                                                            {
                                                                                Id = invDC.Id,
                                                                                InvoiceRefId = invDC.InvoiceRefId,
                                                                                InwardDirectCreditRefId = invDC.InwardDirectCreditRefId,
                                                                                MaterialRefId = invDC.MaterialRefId
                                                                            }).ToListAsync();
            List<int> arrInwardDirectCreditRefIds = editInvoiceDCLink.Select(t => t.InwardDirectCreditRefId).ToList();
            List<InwardDirectCreditListDto> rsInwardDcMaster = new List<InwardDirectCreditListDto>();
            {
                var temp = await _inwarddirectcreditRepo.GetAllListAsync(t => arrInwardDirectCreditRefIds.Contains(t.Id));
                rsInwardDcMaster = temp.MapTo<List<InwardDirectCreditListDto>>();
            }
            var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();

            foreach (var lst in sortMenuItems)
            {
                var inwardDcExists = editInvoiceDCLink.FirstOrDefault(t => t.MaterialRefId == lst.MaterialRefId && t.InvoiceRefId == lst.InvoiceRefId);
                if (inwardDcExists != null)
                {
                    var inwardMaster = rsInwardDcMaster.FirstOrDefault(t => t.Id == inwardDcExists.InwardDirectCreditRefId);
                    if (inwardMaster != null)
                    {
                        lst.InvoiceNumberReferenceFromOtherErp = inwardMaster.InvoiceNumberReferenceFromOtherErp;
                        lst.PurchaseOrderReferenceFromOtherErp = inwardMaster.PurchaseOrderReferenceFromOtherErp;
                    }
                }
                decimal conversionFactor;
                decimal pricePerUom;
                decimal pricePerUomWithTax;
                if (lst.DefaultUnitId == lst.UnitRefId)
                {
                    conversionFactor = 1;
                    lst.QuantityInPurchaseUnit = lst.TotalQty;
                }
                else
                {
                    var unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == lst.DefaultUnitId && t.RefUnitId == lst.UnitRefId);
                    if (unitConversion != null)
                    {
                        conversionFactor = unitConversion.Conversion;
                    }
                    else
                    {
                        unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == lst.UnitRefId && t.RefUnitId == lst.DefaultUnitId);
                        if (unitConversion == null)
                        {
                            var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == lst.UnitRefId);
                            var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == lst.DefaultUnitId);
                            throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
                        }
                        conversionFactor = 1 / unitConversion.Conversion;
                    }
                    lst.QuantityInPurchaseUnit = lst.TotalQty * conversionFactor;
                }
                pricePerUom = (lst.Price * conversionFactor);
                lst.PriceForDefaultUnitForCalculation = pricePerUom;

                pricePerUomWithTax = (lst.Price + lst.TaxAmount) * conversionFactor;

                lst.PriceForDefaultUnitWithTaxUOM = pricePerUomWithTax.ToString("N") + " / " + lst.DefaultUnitName;
            }

            outputDto.InvoiceDetailViewList = sortMenuItems;
            outputDto.TotalTaxAmount = sortMenuItems.Sum(t => t.TaxAmount);
            outputDto.TotalPurchaseAmount = sortMenuItems.Sum(t => t.TotalAmount);

            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
            outputDto.LocationInfo = loc.MapTo<LocationListDto>();
            outputDto.StartDate = input.StartDate;
            outputDto.EndDate = input.EndDate;
            outputDto.PrintedTime = DateTime.Now;
            outputDto.PreparedUserId = AbpSession.UserId;
            var user = await UserManager.GetUserByIdAsync(AbpSession.UserId.Value);
            string userName = "";
            if (user != null)
                userName = user.Name;
            outputDto.PreparedUserName = userName;
            var customReport = await FeatureChecker.GetValueAsync(AppFeatures.Custom);
            bool customCrgReport = false;
            if (customReport.ToUpper() == "TRUE")
                customCrgReport = true;
            outputDto.CustomReportFlag = customCrgReport;
            return outputDto;
        }

        public async Task<FileDto> GetPurchaseInvoiceTaxDetailsToExcel(PurchaseInvoiceTaxDetailInputDto input)
        {
            var result = await GetPurchaseInvoiceTaxDetails(input);
            return _invoiceExporter.ExportPurchaseTaxDetails(result);
        }


    }
}
