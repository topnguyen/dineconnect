﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using DinePlan.DineConnect.House.Master.Dtos;

namespace DinePlan.DineConnect.House.Transaction
{
    public interface IInterTransferCrudAppService : IApplicationService
    {

        // ----------- Checked already

        //Task<GetInterTransferForEditOutput> GetInterTransferApprovalForEdit(GetInterApprovalDataFromId input);

        //      Task<PagedResultOutput<InterTransferListDto>> GetAll(GetInterTransferInput inputDto);
        //      Task<FileDto> GetAllToExcel(GetInterTransferInput input);

        //      Task<GetInterTransferForEditOutput> GetInterTransferForEdit(NullableIdInput nullableIdInput);

        //      Task<GetInterReceivedForEditOutput> GetInterReceivedForEdit(NullableIdInput nullableIdInput);

        Task<IdInput> CreateOrUpdateInterTransfer(CreateOrUpdateInterTransferInput input);
        Task<IdInput> CreateOrUpdateApproval(CreateOrUpdateInterTransferInput input);
        Task<MessageOutput> BulkAutoApproval(BulkApprovalInputDto input);

        Task<IdInput> TriggerAutoApprovalForGivenTransferRequestId(IdInput inputId);
        Task<IdInput> SplitRequestAsPOBasedDependentAndExistingStockDependent(IdInput input);

        Task<InterTransferReceivedWithAdjustmentId> CreateOrUpdateInterReceived(CreateOrUpdateInterReceivedInput input);

        Task<GetInterTransferForEditOutput> GetInterTransferApprovalForEdit(GetInterApprovalDataFromId input);
        //      Task DeleteInterTransfer(IdInput input);

        //      Task<ListResultOutput<InterTransferListDto>> GetIds();


        //      Task<PagedResultOutput<InterTransferViewDto>> GetViewForApproved(GetInterTransferInput input);
        //      Task<GetInterTransferForEditOutput> GetTemplateObjectForEdit(GetObjectFromString input);
        //      Task UpdateRequestStatusDraft(IdInput input);
        //      Task UpdateRequestStatusDraftToPending(IdInput input);
        //      Task<SetInterTransferDashBoardDto> GetDashBoardTransfer(IdInput input);


        //      Task<FileDto> ImportDirectTransferTemplate(LocationAndRequestLocationDto input);

        //Task<InterTransferReportDto> GetTransferDetailReport(InputInterTransferReport input);

        //Task<InterTransferReportDto> GetTransferReceivedDetailReport(InputInterTransferReport input);

        //      Task<List<FileDto>> GetTransferReportDetailExcel(InputInterTransferReport input);

        //      Task SendTransferRequestEmail(SetInterTransferPrint input);
        //      Task<List<MaterialPoProjectionViewDto>> GetAutoRequestProjection(InputPurchaseProjection input);
        //      Task<List<FileDto>> GetTransferReceivedReportDetailExcel(InputInterTransferReport input);

        //      Task<List<StatusConsolidatedDto>> GetTransferStatusDetailReport(InputInterTransferReport input);
        //Task<FileDto> GetTransferStatusDetailReportToExcel(InputInterTransferReport input);


        //      Task<FileDto> ExportConsolidatedToExcel(ExportToExcelDataConsolidated input);

        //      Task<List<InterTransferPOGenerationDto>> GetPoRequestBasedOnSelection(GetInterTransferInput input);



        //      Task<InterTransferDashBoardRequestAlertDto> getApprovalAndReceivePendingStaus(int userId);


        //      Task<FileDto> GetInterTransferReceivedConsolidatedReportGroupOrCategoryOrMaterialWiseForGivenInputToExcel(ClosingStockExportGroupCategoryMaterialDto input);

    }
}

