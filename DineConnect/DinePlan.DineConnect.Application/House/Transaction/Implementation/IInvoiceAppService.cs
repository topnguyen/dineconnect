﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.House.Transaction.Dtos;

namespace DinePlan.DineConnect.House.Transaction.Implementation
{
    public interface IInvoiceAppService : IApplicationService
    {
        Task<PagedResultOutput<InvoiceListDto>> GetAll(GetInvoiceInput inputDto);
        Task<FileDto> GetAllToExcel(GetInvoiceInput input);
        Task<GetInvoiceForEditOutput> GetInvoiceForEdit(NullableIdInput nullableIdInput);
        Task<InvoiceWithAdjustmentId> CreateOrUpdateInvoice(CreateOrUpdateInvoiceInput input);
        Task DeleteInvoice(IdInput input);
        Task<PagedResultOutput<InvoiceListDto>> GetInvoiceSearch(GetInvoiceInput input);
        Task<ListResultOutput<InvoiceListDto>> GetIds();

        Task<GetDcMaterialDataDetail> GetMaterialsFromDcId(GetDcMaterialInfo input);
		Task SetDcCompleteStatus(IdInput input);
        Task<PurchaseAnalysisOutputDto> GetPurchaseAnalysis(InputInvoiceAnalysis input);
        Task<List<InwardDirectCreditDetailForParticularDc>> GetMaterialForInwardDc(IdInput ninput);
        Task<List<InwardDirectCreditDetailForParticularDc>> GetMaterialForPo(IdInput input);

        Task<ListResultOutput<ComboboxItemDto>> GetDcTranId(InputDateRangeWithSupplierId input);

        Task<List<InvoiceCategoryAnalysisDto>> GetPurchaseAnalysisCategoryWise(InputInvoiceAnalysisForCategoryWise input);
        Task<List<OutputMaterialPurchaseDto>> GetMaterialPurchase(InputMaterialPurchaseDto input);

        Task<PrintOutIn40Cols> GetPrintData(IdInput input);

        Task<FileDto> GetInvoiceSummaryAllToExcel(GetInvoiceInput input);

        Task<ListResultOutput<SupplierMaterialViewDto>> GetMaterialBasedOnSupplierAndInvoice(SentLocationAndSupplier input);

        Task<PagedResultOutput<InvoiceListDto>> InvoiceAlreadyExists(GetInvoiceInput input);

        Task<List<GetInvoiceForEditOutput>> GetPurchaseInvoiceDetails(InputInvoiceAnalysis input);

        Task<FileDto> GetPurchaseInvoiceDetailsToExcel(InputInvoiceDetailExcel input);

		Task<GetInvoiceDraftReference> CreateOrUpdateInvoiceDraft(CreateOrUpdateInvoiceInput input);

		Task<GetInvoiceForEditOutput> GetTemplateObjectForEdit(GetObjectFromString input);

		Task<ListResultOutput<ComboboxItemDto>> GetPurchaseCategoryForCombobox();
		Task<FileDto> GetPurchaseAnalysisCategoryWiseToExcel(InputInvoiceAnalysisForCategoryWise input);

		Task<PurchaseAnalysisOutputDto> GetPurchaseAnalysisConsolidated(InputInvoiceAnalysis input);
        Task<PurchaseInvoiceTaxDetailOutputDto> GetPurchaseInvoiceTaxDetails(PurchaseInvoiceTaxDetailInputDto input);
        Task<FileDto> GetPurchaseInvoiceTaxDetailsToExcel(PurchaseInvoiceTaxDetailInputDto input);
        Task<InvoiceWithPoAndCategoryOutputDto> GetPurchaseReportCategoryWiseWithPurchaseOrderAnalysis(InputInvoiceAnalysis input);
        Task<FileDto> GetPurchaseReportCategoryWiseWithPurchaseOrderAnalysisToExcel(InputInvoiceAnalysis input);
        Task<bool> ReverseExportCompletion(IdInput input);
        Task<GetDcMaterialDataDetail> GetMaterialsFromPoId(GetPoMaterialInfo input);
        Task<ListResultOutput<ComboboxItemDto>> GetDcPoTranId(InputDateRangeWithSupplierId input);
        Task<SupplierWisePurchaseGroupCateogryMaterialDto> GetPurchaseReportGroupOrCategoryOrMaterialWiseForGivenInput(PurchaseAnalysisGroupCategoryMaterialDto input);
        Task<FileDto> GetPurchaseReportGroupOrCategoryOrMaterialWiseForGivenInputToExcel(PurchaseAnalysisGroupCategoryMaterialDto input);

    }
}