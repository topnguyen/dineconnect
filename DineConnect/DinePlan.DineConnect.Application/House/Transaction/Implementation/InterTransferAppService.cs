﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.House.Master;
using DinePlan.DineConnect.Common;
using Abp.Configuration;
using DinePlan.DineConnect.Connect.Master.Dtos;
using System.Text;
using Abp.Net.Mail;
using DinePlan.DineConnect.Connect.DayClose;
using DinePlan.DineConnect.Exporter;
using DinePlan.DineConnect.Connect.Location;
using Abp.Authorization;
using Castle.Core.Logging;
using System.Diagnostics;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Features;

namespace DinePlan.DineConnect.House.Transaction.Implementation
{
    public class InterTransferAppService : DineConnectAppServiceBase, IInterTransferAppService
    {

        private readonly IInterTransferListExcelExporter _intertransferExporter;
        private readonly IInterTransferManager _intertransferManager;
        private readonly IRepository<InterTransfer> _intertransferRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<InterTransferDetail> _intertransferDetailRepo;
        private readonly IRepository<InterTransferReceivedDetail> _intertransferReceivedDetailRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<MaterialGroupCategory> _materialGroupCategoryRepo;
        private readonly IRepository<MaterialGroup> _materialgroupRepo;
        private readonly IMaterialAppService _materialAppService;
        private readonly IInterTransferRequestAppService _interTransferRequestAppService;
        private readonly IInterTransferSupplementaryAppService _interTransferSupplementaryAppService;
        private readonly IRepository<Unit> _unitRepo;
        private readonly IRepository<UnitConversion> _unitconversionRepo;
        private readonly IRepository<MaterialLocationWiseStock> _materiallocationwisestockRepo;
        private readonly IRepository<Template> _templateRepo;
        private readonly IXmlAndJsonConvertor _xmlandjsonConvertorAppService;
        private readonly SettingManager _settingManager;
        private readonly IHouseReportAppService _housereportAppService;
        private readonly IDayCloseAppService _dayCloseAppService;
        private readonly IAdjustmentAppService _adjustmentAppService;
        private readonly IRepository<Company> _companyRepo;
        private readonly IEmailSender _emailSender;
        private readonly IRepository<Supplier> _supplierRepo;
        private readonly IRepository<MaterialLedger> _materialLedgerRepo;
        private readonly IMaterialLocationWiseStockAppService _materiallocationwisestockAppService;
        private readonly IUnitConversionAppService _unitConversionAppService;
        private readonly ILocationAppService _locService;
        private readonly IRepository<SupplierMaterial> _supplierMaterialRepo;
        private readonly ISupplierMaterialAppService _supplierMaterialAppService;
        private readonly IRepository<PurchaseOrder> _purchaseOrderRepo;
        private readonly IRepository<PurchaseOrderDetail> _purchaseOrderDetailRepo;
        private readonly ILogger _logger;

        public InterTransferAppService(IInterTransferManager intertransferManager,
            IRepository<InterTransfer> interTransferRepo,
            IInterTransferListExcelExporter intertransferExporter,
            IRepository<InterTransferDetail> intertransferDetailRepo,
            IRepository<Material> materialRepo,
            IRepository<InterTransferReceivedDetail> intertransferreceivedDetailRepo,
            IRepository<Location> locationRepo,
            IMaterialAppService materialAppService,
            IRepository<Unit> unitRepo,
            IRepository<Template> templateRepo,
            IRepository<MaterialLocationWiseStock> materiallocationwisestockRepo,
             SettingManager settingManager,
             IHouseReportAppService housereportAppService,
            IXmlAndJsonConvertor xmlandjsonConvertorAppService,
            IDayCloseAppService dayCloseAppService,
            IRepository<UnitConversion> unitconversionRepo,
            IAdjustmentAppService adjustmentAppService,
            IRepository<Company> companyRepo,
            IRepository<MaterialGroupCategory> materialGroupCategoryRepo,
            IEmailSender emailSender,
            IRepository<MaterialGroup> materialgroupRepo,
            IRepository<Supplier> supplierRepo,
            IRepository<MaterialLedger> materialLedgerRepo,
            IMaterialLocationWiseStockAppService materiallocationwisestockAppService,
            IUnitConversionAppService unitConversionAppService,
            ILocationAppService locService,
            IRepository<SupplierMaterial> supplierMaterialRepo,
            ISupplierMaterialAppService supplierMaterialAppService,
            IRepository<PurchaseOrder> purchaseOrderRepo,
            IRepository<PurchaseOrderDetail> purchaseOrderDetailRepo,
            ILogger logger,
            IInterTransferSupplementaryAppService interTransferSupplementaryAppService,
            IInterTransferRequestAppService interTransferRequestAppService)
        {
            _intertransferManager = intertransferManager;
            _intertransferRepo = interTransferRepo;
            _intertransferExporter = intertransferExporter;
            _intertransferDetailRepo = intertransferDetailRepo;
            _locationRepo = locationRepo;
            _materialRepo = materialRepo;
            _intertransferReceivedDetailRepo = intertransferreceivedDetailRepo;
            _materialAppService = materialAppService;
            _unitRepo = unitRepo;
            _materiallocationwisestockRepo = materiallocationwisestockRepo;
            _templateRepo = templateRepo;
            _xmlandjsonConvertorAppService = xmlandjsonConvertorAppService;
            _settingManager = settingManager;
            _housereportAppService = housereportAppService;
            _unitconversionRepo = unitconversionRepo;
            _dayCloseAppService = dayCloseAppService;
            _adjustmentAppService = adjustmentAppService;
            _companyRepo = companyRepo;
            _emailSender = emailSender;
            _materialGroupCategoryRepo = materialGroupCategoryRepo;
            _materialgroupRepo = materialgroupRepo;
            _supplierRepo = supplierRepo;
            _materialLedgerRepo = materialLedgerRepo;
            _materiallocationwisestockAppService = materiallocationwisestockAppService;
            _unitConversionAppService = unitConversionAppService;
            _locService = locService;
            _supplierMaterialRepo = supplierMaterialRepo;
            _supplierMaterialAppService = supplierMaterialAppService;
            _purchaseOrderRepo = purchaseOrderRepo;
            _purchaseOrderDetailRepo = purchaseOrderDetailRepo;
            _logger = logger;
            _interTransferSupplementaryAppService = interTransferSupplementaryAppService;
            _interTransferRequestAppService = interTransferRequestAppService;
        }

        public async Task<PagedResultOutput<InterTransferListDto>> GetAll(GetInterTransferInput input)
        {
            var allItems = _intertransferRepo.GetAll();
            if (!string.IsNullOrEmpty(input.Operation) && input.Operation.Equals("SEARCH"))
            {
                allItems = _intertransferRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Id.Equals(input.Filter)
               );
            }
            else
            {
                allItems = _intertransferRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Id.ToString().Contains(input.Filter)
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<InterTransferListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<InterTransferListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<InterTransferDashBoardRequestAlertDto> getApprovalAndReceivePendingStaus(int userId)
        {
            InterTransferDashBoardRequestAlertDto output = new InterTransferDashBoardRequestAlertDto();

            if (!PermissionChecker.IsGranted("Pages.Tenant.House.Transaction.InterTransfer.Approve"))
            {
                return output;
            }

            var locations = new List<int>();

            var allOrgani = await _locService.GetLocationBasedOnUser(new GetLocationInputBasedOnUser
            {
                UserId = userId
            });

            if (allOrgani.Items.Any()) locations = allOrgani.Items.Select(a => a.Id).ToList();

            DateTime checkingFrom = DateTime.Today.AddDays(-90);

            List<int> ids = new List<int>();
            var viewPendingDto = await _intertransferRepo.GetAll().Where(t => locations.Contains(t.LocationRefId) && t.CurrentStatus == "Q" && DbFunctions.TruncateTime(t.RequestDate) > checkingFrom).ToListAsync();
            ids = viewPendingDto.Select(t => t.Id).ToList();

            if (ids.Count > 0)
            {
                var pendingApprovalDto = await _interTransferRequestAppService.GetViewInterTransfer(new GetInterTransferInput
                {
                    Filter = "",
                    TransferRequestIds = ids,
                    MaxResultCount = AppConsts.MaxPageSize,
                    Sorting = "Id",
                    RequestStatus = ""
                });

                output.ApprovalPendingDtos = pendingApprovalDto.Items.MapTo<List<InterTransferViewDto>>();
            }

            viewPendingDto = await _intertransferRepo.GetAll().Where(t => locations.Contains(t.LocationRefId) && t.CurrentStatus == "P" && DbFunctions.TruncateTime(t.RequestDate) > checkingFrom).ToListAsync();
            ids = viewPendingDto.Select(t => t.Id).ToList();

            if (ids.Count > 0)
            {
                var PendingTransferDto = await _interTransferSupplementaryAppService.GetViewForApproved(new GetInterTransferInput
                {
                    Filter = "",
                    TransferRequestIds = ids,
                    MaxResultCount = AppConsts.MaxPageSize,
                    Sorting = "Id",
                    RequestStatus = ""
                });
                output.TransferPendingDtos = PendingTransferDto.Items.MapTo<List<InterTransferViewDto>>();
            }


            viewPendingDto = await _intertransferRepo.GetAll().Where(t => locations.Contains(t.LocationRefId) && t.CurrentStatus == "A" && DbFunctions.TruncateTime(t.RequestDate) > checkingFrom).ToListAsync();
            ids = viewPendingDto.Select(t => t.Id).ToList();

            if (ids.Count > 0)
            {
                var pendingReceiveDto = await _interTransferRequestAppService.GetViewInterTransfer(new GetInterTransferInput
                {
                    Filter = "",
                    TransferRequestIds = ids,
                    MaxResultCount = AppConsts.MaxPageSize,
                    Sorting = "Id",
                    RequestStatus = ""
                });

                output.ReceivePendingDtos = pendingReceiveDto.Items.MapTo<List<InterTransferViewDto>>();
            }

            return output;
        }

        //public async Task<PagedResultOutput<InterTransferViewDto>> GetView(GetInterTransferInput input)
        //{
        //    _logger.Info("InterTransfer Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
        //    Debug.WriteLine("InterTransfer Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
        //    var stopWatch = new Stopwatch();
        //    stopWatch.Start();
        //    var rsInterTrans = _intertransferRepo.GetAll().WhereIf(input.DefaultLocationRefId > 0, t => t.LocationRefId == input.DefaultLocationRefId);

        //    if (input.StartDate != null && input.EndDate != null)
        //    {
        //        rsInterTrans = rsInterTrans.Where(a =>
        //                         DbFunctions.TruncateTime(a.RequestDate) >= DbFunctions.TruncateTime(input.StartDate)
        //                         &&
        //                         DbFunctions.TruncateTime(a.RequestDate) <= DbFunctions.TruncateTime(input.EndDate));
        //    }

        //    if (input.DeliveryStartDate != null && input.DeliveryEndDate != null)
        //    {
        //        rsInterTrans = rsInterTrans.Where(a =>
        //                         DbFunctions.TruncateTime(a.DeliveryDateRequested) >= DbFunctions.TruncateTime(input.DeliveryStartDate)
        //                         &&
        //                         DbFunctions.TruncateTime(a.DeliveryDateRequested) <= DbFunctions.TruncateTime(input.DeliveryEndDate));
        //    }

        //    if (input.ShowOnlyDeliveryDateExpectedAsOnDate)
        //    {
        //        rsInterTrans = rsInterTrans.Where(a => a.CompletedTime.HasValue == false &&
        //                        DbFunctions.TruncateTime(a.DeliveryDateRequested) <= DbFunctions.TruncateTime(DateTime.Today));
        //    }

        //    if (input.TransferRequestIds.Count > 0)
        //    {
        //        rsInterTrans = rsInterTrans.Where(t => input.TransferRequestIds.Contains(t.Id));
        //    }

        //    if (input.Id.HasValue)
        //        rsInterTrans = rsInterTrans.Where(t => t.Id == input.Id.Value);

        //    IQueryable<InterTransferViewDto> allItems;
        //    if (input.MaterialGroupRefId > 0 || input.MaterialGroupCategoryRefId > 0)
        //    {
        //        allItems = (from req in rsInterTrans
        //                    join loc in _locationRepo.GetAll()
        //                    on req.LocationRefId equals loc.Id
        //                    join locReq in _locationRepo.GetAll()
        //                        .WhereIf(!input.Filter.IsNullOrEmpty(), p => p.Name.Contains(input.Filter))
        //                    on req.RequestLocationRefId equals locReq.Id
        //                    join reqDetail in _intertransferDetailRepo.GetAll() on req.Id equals reqDetail.InterTransferRefId
        //                    join mat in _materialRepo.GetAll().WhereIf(input.MaterialGroupCategoryRefId > 0, t => t.MaterialGroupCategoryRefId == input.MaterialGroupCategoryRefId) on reqDetail.MaterialRefId equals mat.Id
        //                    join matGroupCategory in _materialGroupCategoryRepo.GetAll()
        //                        .WhereIf(input.MaterialGroupRefId > 0, t => t.MaterialGroupRefId == input.MaterialGroupRefId)
        //                        .WhereIf(input.MaterialGroupCategoryRefId > 0, t => t.Id == input.MaterialGroupCategoryRefId)
        //                    on mat.MaterialGroupCategoryRefId equals matGroupCategory.Id
        //                    select new InterTransferViewDto
        //                    {
        //                        Id = req.Id,
        //                        LocationRefId = req.LocationRefId,
        //                        LocationRefName = loc.Name,
        //                        RequestLocationRefName = locReq.Name,
        //                        CurrentStatus = req.CurrentStatus,
        //                        RequestDate = req.RequestDate,
        //                        DeliveryDateRequested = req.DeliveryDateRequested,
        //                        CompletedTime = req.CompletedTime,
        //                        ReceivedTime = req.ReceivedTime,
        //                        TransferValue = req.TransferValue,
        //                        CreationTime = req.CreationTime,
        //                        VehicleNumber = req.VehicleNumber,
        //                        PersonInCharge = req.PersonInCharge,
        //                        DoesThisRequestToBeConvertedAsPO = req.DoesPOCreatedForThisRequest,
        //                        ParentRequestRefId = req.ParentRequestRefId
        //                    }).Distinct();
        //    }
        //    else
        //    {
        //        allItems = (from req in rsInterTrans
        //                    join loc in _locationRepo.GetAll()
        //                    on req.LocationRefId equals loc.Id
        //                    join locReq in _locationRepo.GetAll().WhereIf(
        //                       !input.Filter.IsNullOrEmpty(),
        //                       p => p.Name.Contains(input.Filter))
        //                    on req.RequestLocationRefId equals locReq.Id
        //                    select new InterTransferViewDto
        //                    {
        //                        Id = req.Id,
        //                        LocationRefId = req.LocationRefId,
        //                        LocationRefName = loc.Name,
        //                        RequestLocationRefName = locReq.Name,
        //                        CurrentStatus = req.CurrentStatus,
        //                        RequestDate = req.RequestDate,
        //                        DeliveryDateRequested = req.DeliveryDateRequested,
        //                        CompletedTime = req.CompletedTime,
        //                        ReceivedTime = req.ReceivedTime,
        //                        TransferValue = req.TransferValue,
        //                        CreationTime = req.CreationTime,
        //                        VehicleNumber = req.VehicleNumber,
        //                        PersonInCharge = req.PersonInCharge,
        //                        DoesThisRequestToBeConvertedAsPO = req.DoesPOCreatedForThisRequest,
        //                        ParentRequestRefId = req.ParentRequestRefId
        //                    });
        //    }

        //    var sortMenuItems = await allItems
        //        .OrderBy(input.Sorting)
        //        .PageBy(input)
        //        .ToListAsync();

        //    //List<InterTransferViewDto> lsttobeRemoved = new List<InterTransferViewDto>();
        //    List<int> arrRequestIds = sortMenuItems.Select(t => t.Id).ToList();
        //    var rsDetails = await _intertransferDetailRepo.GetAll().Where(t => arrRequestIds.Contains(t.InterTransferRefId)).Select(t=> new { t.Id, t.InterTransferRefId, t.PurchaseOrderRefId }).ToListAsync();
        //    //var rsDetails = tempInterTransferDetails.MapTo<List<InterTransferDetailListDto>>();

        //    var arrPoIds = rsDetails.Where(t => t.PurchaseOrderRefId.HasValue).Select(t => t.PurchaseOrderRefId.Value).ToList();
        //    var tempPoMaster = await _purchaseOrderRepo.GetAllListAsync(t => arrPoIds.Contains(t.Id));
        //    var rsPoMaster = tempPoMaster.MapTo<List<PurchaseOrderListDto>>();

        //    var interStopWatch = new Stopwatch();
        //    interStopWatch.Start();
        //    Parallel.ForEach(sortMenuItems, item =>
        //    {
        //        if (item.CurrentStatus.Equals("Q"))
        //            item.CurrentStatus = L("RequestApprovalPending");
        //        else if (item.CurrentStatus.Equals("P"))
        //            item.CurrentStatus = L("Pending");
        //        else if (item.CurrentStatus.Equals("A"))
        //            item.CurrentStatus = L("Approved");
        //        else if (item.CurrentStatus.Equals("R"))
        //            item.CurrentStatus = L("Rejected");
        //        else if (item.CurrentStatus.Equals("D"))
        //            item.CurrentStatus = L("Drafted");
        //        else if (item.CurrentStatus.Equals("V"))
        //            item.CurrentStatus = L("Received");
        //        else if (item.CurrentStatus.Equals("I"))
        //            item.CurrentStatus = L("PartiallyReceived");

        //        if (item.DeliveryDateRequested.Date <= DateTime.Today.Date)
        //            item.DeliverDateAlertFlag = true;

        //        var details = rsDetails.Where(t => t.InterTransferRefId == item.Id).ToList();
        //        var countDetails = details.Count();
        //        item.NoOfMaterials = countDetails;
        //        var poDependentCount = details.Count(t => t.PurchaseOrderRefId.HasValue == true);
        //        var stockDependentCount = details.Count(t => t.PurchaseOrderRefId.HasValue == false);
        //        item.NoOfPoDependent = poDependentCount;
        //        item.NoOfStockDependent = stockDependentCount;

        //        if (item.NoOfStockDependent > 0 && item.NoOfPoDependent > 0)
        //        {
        //            item.DoesSplitNeeded = true;
        //        }

        //        if (item.DoesThisRequestToBeConvertedAsPO)
        //        {
        //            var poIds = details.Where(t => t.PurchaseOrderRefId.HasValue).Select(t => t.PurchaseOrderRefId.Value).ToList();
        //            var poMaster = rsPoMaster.Where(t => poIds.Contains(t.Id)).ToList();
        //            foreach (var lst in poMaster)
        //            {
        //                if (lst.PoCurrentStatus.Equals("P"))
        //                {
        //                    lst.PoCurrentStatus = L("Pending");
        //                }
        //                else if (lst.PoCurrentStatus.Equals("A"))
        //                {
        //                    lst.PoCurrentStatus = L("Approved");
        //                }
        //                else if (lst.PoCurrentStatus.Equals("X"))
        //                {
        //                    lst.PoCurrentStatus = L("Declined");
        //                }
        //                else if (lst.PoCurrentStatus.Equals("C"))
        //                {
        //                    lst.PoCurrentStatus = L("Completed");
        //                }
        //                else if (lst.PoCurrentStatus.Equals("H"))
        //                {
        //                    lst.PoCurrentStatus = L("Partial");
        //                }
        //            }

        //            var poStatusList = (from mas in poMaster
        //                                group mas by new { mas.PoCurrentStatus } into g
        //                                select new PoStatusWiseList
        //                                {
        //                                    PoCurrentStatus = g.Key.PoCurrentStatus,
        //                                    PurchaseOrderList = g.ToList()
        //                                }).ToList();
        //            item.PoStatusWiseLists = poStatusList;
        //            foreach (var lst in poStatusList)
        //            {
        //                item.PoStatusRemarks = item.PoStatusRemarks + lst.PoCurrentStatus + "( " + lst.PurchaseOrderList.Count() + " ) ";
        //            }
        //        }
        //    });
        //    interStopWatch.Stop();
        //    _logger.Info("InterTransfer For Each Total Elapsed Time " + interStopWatch.ElapsedMilliseconds);
        //    ////  Remove the Non Approved only for ReceivedView
        //    //foreach (var l in lsttobeRemoved)
        //    //{
        //    //    sortMenuItems.Remove(l);
        //    //}

        //    var allListDtos = sortMenuItems.MapTo<List<InterTransferViewDto>>();

        //    if (!input.CurrentStatus.IsNullOrEmpty())
        //    {
        //        //Get the culture property of the thread.
        //        CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
        //        //Create TextInfo object.
        //        TextInfo textInfo = cultureInfo.TextInfo;

        //        input.CurrentStatus = input.CurrentStatus.ToLowerInvariant();

        //        var searchStatus = textInfo.ToTitleCase(input.CurrentStatus);

        //        allListDtos = allListDtos.Where(t => t.CurrentStatus.Contains(searchStatus)).ToList();
        //        //   allListDtos = allListDtos.FindAll(t => t.PoCurrentStatus.Contains(input.PoStatus));
        //    }

        //    var allItemCount = sortMenuItems.Count();
        //    stopWatch.Stop();
        //    _logger.Info("InterTransfer GetView Total Elapsed Time " + stopWatch.ElapsedMilliseconds);
        //    _logger.Info("InterTransfer Stop Time " + DateTime.Now);
        //    Debug.WriteLine("InterTransfer Stop Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
        //    return new PagedResultOutput<InterTransferViewDto>(
        //        allItemCount,
        //        allListDtos
        //        );
        //}



        public async Task<List<InterTransferPOGenerationDto>> GetPoRequestBasedOnSelection(GetInterTransferInput input)
        {
            Debug.WriteLine("InterTransfer GetPoRequestBasedOnSelection() Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
            if (input.DefaultLocationRefId == 0)
            {
                throw new UserFriendlyException("Loation Reference Not Exists");
            }

            List<InterTransferViewDto> resultFromPendingList = new List<InterTransferViewDto>();
            List<InterTransferRequestConsolidatedViewDto> dtos = new List<InterTransferRequestConsolidatedViewDto>();
            if (input.RequestBasedOnRequestList == true)
            {
                resultFromPendingList = input.RequestList;
                dtos = await GetConsolidatedPendingBasedGivenRequestList(new InterTransferRequestList
                {
                    InterTransferViewDtos = resultFromPendingList,
                    RequestLocationRefId = input.DefaultLocationRefId,
                    DoesPendingRequest = true
                });
            }
            else
            {
                input.SkipCount = 0;
                input.MaxResultCount = AppConsts.MaxPageSize;
                resultFromPendingList = await GetConsolidatedRequestPendingList(input);
                dtos = await GetConsolidatedPendingBasedGivenRequestList(new InterTransferRequestList
                {
                    InterTransferViewDtos = resultFromPendingList,
                    RequestLocationRefId = input.DefaultLocationRefId,
                    DoesPendingRequest = true
                });
            }
            List<InterTransferPOGenerationDto> output = new List<InterTransferPOGenerationDto>();

            List<int> materialList = dtos.Select(t => t.MaterialRefId).Distinct().ToList();

            var rsSupplierMaterial = await _supplierMaterialAppService.GetSupplierWithPriceBasedOnMaterial(
                new GetSupplierWithLocationAndMaterialInput
                {
                    LocationRefId = input.DefaultLocationRefId,
                    MaterialRefIdList = materialList
                });

            List<int> locationList = dtos.Select(t => t.LocationRefId).Distinct().ToList();


            var dt = await _materiallocationwisestockAppService.GetView(new GetMaterialLocationWiseStockInput
            {
                LocationRefId = input.DefaultLocationRefId,
                LiveStock = true,
                MaxResultCount = AppConsts.MaxPageSize,
                MaterialRefIds = materialList
            });

            List<MaterialLocationWiseStockViewDto> rsMaterialLocationStock = dt.Items.MapTo<List<MaterialLocationWiseStockViewDto>>();

            foreach (var matid in materialList)
            {
                var material = dtos.FirstOrDefault(t => t.MaterialRefId == matid);

                #region Select Supplier Based On Supplier Materials

                List<SupplierMaterialViewDto> supplierViewDto = new List<SupplierMaterialViewDto>();
                SupplierMaterialViewDto defaultSupplier = new SupplierMaterialViewDto();
                var tempSupplierMaterial = rsSupplierMaterial.Where(t => t.MaterialRefId == material.MaterialRefId).ToList();
                List<int> arrSupplierRefids = tempSupplierMaterial.Select(t => t.SupplierRefId).ToList();

                string supplierRemarks = "";
                decimal alreadyOrderedQuantity = 0;
                if (tempSupplierMaterial == null || tempSupplierMaterial.Count == 0)
                {
                    supplierRemarks = L("NoMoreSupplierLinkedWithThisMaterial");
                }
                else
                {
                    supplierViewDto = tempSupplierMaterial.MapTo<List<SupplierMaterialViewDto>>();
                    defaultSupplier = supplierViewDto.OrderBy(t => t.MaterialPrice).ToList().FirstOrDefault();
                    alreadyOrderedQuantity = defaultSupplier.AlreadyOrderedQuantity;
                }
                #endregion

                var requestMaterialList = dtos.Where(t => t.MaterialRefId == matid).ToList();

                decimal totalRequestQty = requestMaterialList.Sum(t => t.RequestQty);

                string remarks = "";
                string formula = "";
                decimal onHandAfterIssue;
                onHandAfterIssue = material.LiveStock - totalRequestQty;
                formula = " Balance Qty ";
                decimal poOrderedQuantity = 0;
                if (onHandAfterIssue <= 0)
                {
                    if (material.MaterialTypeId == 1)
                        remarks = remarks + L("NeedToRaisePurchaseOrder");
                    else
                        remarks = L("NeedToRaisePurchaseOrder") + " Or " + L("ProductionNeeded");
                    poOrderedQuantity = Math.Abs(onHandAfterIssue);
                    if (material.ReorderLevel > 0)
                    {
                        poOrderedQuantity = poOrderedQuantity + (material.ReorderLevel);
                        formula = formula + " + ROL";
                    }
                }
                else
                {
                    if (material.ReorderLevel > onHandAfterIssue)
                    {
                        remarks = L("LessThanReorderLevel");
                        poOrderedQuantity = (material.ReorderLevel - onHandAfterIssue);
                        formula = "ROL - " + formula;
                    }
                }
                poOrderedQuantity = Math.Ceiling(poOrderedQuantity);
                if (poOrderedQuantity > 0)
                    formula = "Round(" + formula + ")";
                else
                    formula = "";

                InterTransferPOGenerationDto det = new InterTransferPOGenerationDto
                {
                    MaterialRefId = material.MaterialRefId,
                    MaterialPetName = material.MaterialPetName,
                    MaterialRefName = material.MaterialRefName,
                    MaterialTypeId = material.MaterialTypeId,
                    MaterialTypeName = material.MaterialTypeName,
                    MinimumStock = material.MinimumStock,
                    MaximumStock = material.MaximumStock,
                    ReOrderLevel = material.ReorderLevel,
                    LiveStock = material.LiveStock,
                    UnitRefId = material.UnitRefId,
                    Uom = material.Uom,
                    TotalRequestQty = totalRequestQty,
                    LocationWiseRequestDetails = requestMaterialList,
                    PoOrderQuantity = poOrderedQuantity,
                    PoOrderQuantityRecommended = poOrderedQuantity,
                    OnHandAfterIssue = onHandAfterIssue,
                    Remarks = remarks,
                    FormulaForRecommended = formula,
                    AlreadyOrderedQuantity = alreadyOrderedQuantity,
                    SupplierMaterialViewDtos = supplierViewDto,
                    SupplierRemarks = supplierRemarks,
                    DefaultSupplier = defaultSupplier,
                    InterTransferDetailWithPORefIds = material.InterTransferDetailWithPORefIds
                };
                output.Add(det);
            }
            Debug.WriteLine("InterTransfer GetPoRequestBasedOnSelection() End Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));

            return output;
        }

        public async Task<FileDto> ImportDirectTransferTemplate(LocationAndRequestLocationDto input)
        {
            List<TransferTemplateDto> inputDtos = new List<TransferTemplateDto>();

            var requestLocation = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.RequestLocationRefId);

            if (requestLocation == null)
                throw new UserFriendlyException(L("LocationErr"));

            var allMaterial = await _materialRepo.GetAllListAsync();
            var allGroup = await _materialGroupCategoryRepo.GetAllListAsync();

            var dt = await _materiallocationwisestockAppService.GetView(new GetMaterialLocationWiseStockInput
            {
                LocationRefId = input.LocationRefId,
                LiveStock = true,
                MaxResultCount = AppConsts.MaxPageSize
            });

            List<MaterialLocationWiseStockViewDto> materialStock = dt.Items.MapTo<List<MaterialLocationWiseStockViewDto>>();

            //var materialStock = await _materiallocationwisestockRepo.GetAllListAsync(t => t.LocationRefId == input.Id);

            foreach (var mat in allMaterial)
            {
                TransferTemplateDto dto = new TransferTemplateDto();
                dto.LocationRefId = input.RequestLocationRefId;
                dto.LocationRefName = requestLocation.Name;
                dto.Barcode = mat.Barcode == null ? mat.MaterialName : mat.Barcode;
                dto.MaterialRefId = mat.Id;
                dto.MaterialRefName = mat.MaterialName;
                dto.CurrentOnHand = 0;
                var matGroupCategory = await _materialGroupCategoryRepo.FirstOrDefaultAsync(t => t.Id == mat.MaterialGroupCategoryRefId);
                if (matGroupCategory != null)
                    dto.MaterialGroupCategoryName = matGroupCategory.MaterialGroupCategoryName;

                var stk = materialStock.FirstOrDefault(t => t.LocationRefId == input.LocationRefId && t.MaterialRefId == mat.Id);
                if (stk != null)
                {
                    dto.CurrentOnHand = stk.LiveStock;
                }

                inputDtos.Add(dto);
            }

            var fileTemplate = _intertransferExporter.CreateDirectTransferTemplate(inputDtos, requestLocation.Name, DateTime.Today.ToString("dd-MMM-yy"));

            return fileTemplate;

        }

        public async Task<List<FileDto>> GetTransferReportDetailExcel(InputInterTransferReport input)
        {
            var returnDto = await GetTransferDetailReport(input);
            if (returnDto.ConsolidatedReport.Count == 0)
            {
                throw new UserFriendlyException(L("NoRecordsFound"));
            }
            if (input.ExportType == 0)
            {
                return await _intertransferExporter.ExportDetailToFile(returnDto, input);
            }
            else
            {
                return await _intertransferExporter.ExportDetailToPdfOrHtml(returnDto, input, "Issue");
            }

        }

        public async Task<List<FileDto>> GetTransferReceivedReportDetailExcel(InputInterTransferReport input)
        {
            var returnDto = await GetTransferReceivedDetailReport(input);

            if (returnDto.ConsolidatedReport.Count == 0)
            {
                throw new UserFriendlyException(L("NoRecordsFound"));
            }
            if (input.ExportType == ExportType.Excel)
            {
                return await _intertransferExporter.ExportDetailToFile(returnDto, input);
            }
            else
            {
                return await _intertransferExporter.ExportDetailToPdfOrHtml(returnDto, input, "Received");
            }

        }

        //public async Task<GetInterTransferForEditOutput> GetInterTransferForEdit(NullableIdInput input)
        //{
        //    InterTransferEditDto editDto;
        //    List<InterTransferDetailViewDto> editDetailDto;
        //    TemplateSaveDto editTemplateDto = new TemplateSaveDto();

        //    if (input.Id.HasValue)
        //    {
        //        var hDto = await _intertransferRepo.GetAsync(input.Id.Value);
        //        editDto = hDto.MapTo<InterTransferEditDto>();

        //        int locationid = editDto.LocationRefId;
        //        List<int> locationIds = new List<int>();
        //        locationIds.Add(editDto.LocationRefId);
        //        locationIds.Add(editDto.RequestLocationRefId);

        //        var rsLocation = await _locationRepo.GetAllListAsync(t => locationIds.Contains(t.Id));
        //        var loc = rsLocation.FirstOrDefault(t => t.Id == editDto.LocationRefId);
        //        editDto.LocationRefName = loc.Code + " - " + loc.Name;

        //        var reqLoc = rsLocation.FirstOrDefault(t => t.Id == editDto.RequestLocationRefId);
        //        editDto.RequestLocationRefName = reqLoc.Code + " - " + reqLoc.Name;

        //        editDetailDto = await (from request in _intertransferDetailRepo
        //                               .GetAll().Where(a => a.InterTransferRefId == input.Id.Value)
        //                               join mat in _materialRepo.GetAll()
        //                               on request.MaterialRefId equals mat.Id
        //                               join un in _unitRepo.GetAll() on mat.DefaultUnitId equals un.Id
        //                               join uiss in _unitRepo.GetAll() on request.UnitRefId equals uiss.Id
        //                               join matloc in _materiallocationwisestockRepo
        //                                .GetAll().Where(mls => mls.LocationRefId == locationid)
        //                               on mat.Id equals matloc.MaterialRefId
        //                               join matCat in _materialGroupCategoryRepo.GetAll()
        //                               on mat.MaterialGroupCategoryRefId equals matCat.Id
        //                               join matGrp in _materialgroupRepo.GetAll()
        //                               on matCat.MaterialGroupRefId equals matGrp.Id
        //                               select new InterTransferDetailViewDto
        //                               {
        //                                   Id = request.Id,
        //                                   RequestQty = request.RequestQty,
        //                                   Barcode = mat.Barcode == null ? mat.MaterialName : mat.Barcode,
        //                                   MaterialGroupRefName = matGrp.MaterialGroupName,
        //                                   MaterialGroupCategoryRefName = matCat.MaterialGroupCategoryName,
        //                                   MaterialRefId = request.MaterialRefId,
        //                                   MaterialPetName = mat.MaterialPetName,
        //                                   MaterialRefName = mat.MaterialName,
        //                                   CurrentStatus = request.CurrentStatus,
        //                                   InterTransferRefId = request.InterTransferRefId,
        //                                   IssueQty = request.IssueQty,
        //                                   Price = request.Price,
        //                                   ApprovedRemarks = request.ApprovedRemarks,
        //                                   RequestRemarks = request.RequestRemarks,
        //                                   Sno = request.Sno,
        //                                   MaterialTypeId = mat.MaterialTypeId,
        //                                   Uom = un.Name,
        //                                   OnHand = matloc.CurrentInHand,
        //                                   UnitRefId = request.UnitRefId,
        //                                   UnitRefName = uiss.Name,
        //                                   DefaultUnitId = mat.DefaultUnitId,
        //                                   DefaultUnitName = un.Name,
        //                                   OnHandWithUom = matloc.CurrentInHand + "/" + un.Name,
        //                                   PurchaseOrderRefId = request.PurchaseOrderRefId
        //                               }).ToListAsync();

        //        var arrPoIds = editDetailDto.Where(t => t.PurchaseOrderRefId.HasValue).Select(t => t.PurchaseOrderRefId.Value).ToList();
        //        var tempPoMaster = await _purchaseOrderRepo.GetAllListAsync(t => arrPoIds.Contains(t.Id));
        //        var rsPoMaster = tempPoMaster.MapTo<List<PurchaseOrderListDto>>();
        //        var tempPoDetail = await _purchaseOrderDetailRepo.GetAllListAsync(t => arrPoIds.Contains(t.PoRefId));
        //        var rsPoDetails = tempPoDetail.MapTo<List<PurchaseOrderDetailViewDto>>();
        //        List<int> arrMaterialRefIds = editDetailDto.Select(t => t.MaterialRefId).ToList();
        //        var rsUnits = await _unitRepo.GetAllListAsync();
        //        List<MaterialViewDto> reservedQuantityMaterials = new List<MaterialViewDto>();
        //        var resultReserved = await _materialAppService.GetReservedQuantityFromIssueAndYield(
        //                          new LocationWithMaterialList
        //                          {
        //                              LocationRefId = locationid,
        //                              CalculateIssueReservedQuantity = true,
        //                              CalculateYieldReservedQuantity = true,
        //                              MaterialRefIds = arrMaterialRefIds
        //                          });
        //        reservedQuantityMaterials = resultReserved.Items.MapTo<List<MaterialViewDto>>();

        //        foreach (var lst in editDetailDto)
        //        {
        //            var reserved = reservedQuantityMaterials.FirstOrDefault(t => t.Id == lst.Id);
        //            if (reserved != null)
        //            {
        //                lst.ReservedIssueQuantity = reserved.ReservedIssueQuantity;
        //                lst.ReservedYieldQuantity = reserved.ReservedYieldQuantity;
        //            }

        //            if (lst.CurrentStatus.Equals("Q"))
        //                lst.CurrentStatus = L("RequestApprovalPending");
        //            else if (lst.CurrentStatus.Equals("P"))
        //                lst.CurrentStatus = L("Pending");
        //            else if (lst.CurrentStatus.Equals("A"))
        //                lst.CurrentStatus = L("Approved");
        //            else if (lst.CurrentStatus.Equals("R"))
        //                lst.CurrentStatus = L("Rejected");
        //            else if (lst.CurrentStatus.Equals("D"))
        //                lst.CurrentStatus = L("Drafted");
        //            else if (lst.CurrentStatus.Equals("V"))
        //                lst.CurrentStatus = L("Received");
        //            else if (lst.CurrentStatus.Equals("I"))
        //                lst.CurrentStatus = L("PartiallyReceived");
        //            else if (lst.CurrentStatus.Equals("E"))
        //                lst.CurrentStatus = L("Excess");
        //            else if (lst.CurrentStatus.Equals("S"))
        //                lst.CurrentStatus = L("Shortage");

        //            string materialTypeName = lst.MaterialTypeId == (int)MaterialType.RAW ? L("RAW") : L("SEMI");
        //            lst.MaterialTypeName = materialTypeName;

        //            var poMaster = rsPoMaster.FirstOrDefault(t => t.Id == lst.PurchaseOrderRefId);
        //            var poDetail = rsPoDetails.FirstOrDefault(t => t.MaterialRefId == lst.MaterialRefId && t.PoRefId == lst.PurchaseOrderRefId);
        //            if (poDetail != null)
        //            {
        //                var unit = rsUnits.FirstOrDefault(t => t.Id == poDetail.UnitRefId);
        //                lst.PoCurrentStatus = L("PoDetailStatus", poDetail.QtyOrdered, poDetail.QtyReceived, poMaster.Id, poMaster.PoReferenceCode, unit.Name);
        //            }
        //        }
        //    }
        //    else
        //    {
        //        editDto = new InterTransferEditDto();
        //        editDetailDto = new List<InterTransferDetailViewDto>();

        //    }


        //    return new GetInterTransferForEditOutput
        //    {
        //        InterTransfer = editDto,
        //        InterTransferDetail = editDetailDto,
        //        Templatesave = editTemplateDto
        //    };
        //}



        //public async Task<IdInput> CreateOrUpdateApproval(CreateOrUpdateInterTransferInput input)
        //{
        //    return await UpdateApproval(input);
        //}

        public async Task UpdateRequestStatusDraft(IdInput input)
        {
            var item = await _intertransferRepo.GetAsync(input.Id);
            item.CurrentStatus = "D";
            CheckErrors(await _intertransferManager.CreateSync(item));
        }

        public async Task UpdateRequestStatusDraftToPending(IdInput input)
        {
            var countChanges = await _intertransferDetailRepo.GetAll().
                Where(t => t.InterTransferRefId == input.Id && t.IssueQty != 0).ToListAsync();

            if (countChanges.Count == 0)
            {
                var item = await _intertransferRepo.GetAsync(input.Id);
                if (item.CurrentStatus == "D")
                {
                    item.CurrentStatus = "P";
                    CheckErrors(await _intertransferManager.CreateSync(item));
                }
                else
                {

                }
            }
        }

        //protected virtual async Task<IdInput> UpdateApproval(CreateOrUpdateInterTransferInput input)
        //{
        //    Debug.WriteLine("InterTransfer UpdateApproval() Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
        //    var item = await _intertransferRepo.GetAsync(input.InterTransfer.Id.Value);
        //    var dto = input.InterTransfer;

        //    if (input.SaveStatus.Equals("SAVE"))
        //    {
        //        //dto.CompletedTime = DateTime.Now;
        //        dto.CurrentStatus = "A";
        //    }
        //    else if (input.SaveStatus.Equals("DRAFT"))
        //    {
        //        dto.CompletedTime = null;
        //        dto.CurrentStatus = "D";
        //    }
        //    else if (input.SaveStatus.Equals("REJECT"))
        //    {
        //        //dto.CompletedTime = DateTime.Now;
        //        dto.CurrentStatus = "R";
        //    }

        //    item.CurrentStatus = dto.CurrentStatus;
        //    item.ApprovedPersonId = dto.ApprovedPersonId;

        //    item.CompletedTime = dto.CompletedTime;
        //    item.PersonInCharge = dto.PersonInCharge;
        //    item.VehicleNumber = dto.VehicleNumber;
        //    item.DeliveryDateRequested = dto.DeliveryDateRequested;
        //    decimal TotalTransferValue = 0;

        //    bool CanChangeRequestQty = false;
        //    if (PermissionChecker.IsGranted("Pages.Tenant.House.Transaction.InterTransferApproval.CanChangeRequestQuantity"))
        //    {
        //        CanChangeRequestQty = true;
        //    }

        //    List<InterTransferDetailViewDto> intertransferDetailDtos = new List<InterTransferDetailViewDto>();

        //    int interSno = 1;
        //    if (input.InterTransferDetail != null && input.InterTransferDetail.Count > 0)
        //    {
        //        foreach (var items in input.InterTransferDetail)
        //        {
        //            int materialrefid = items.MaterialRefId;

        //            var existingDetailEntry = _intertransferDetailRepo.GetAllList(u => u.InterTransferRefId == dto.Id && u.MaterialRefId.Equals(materialrefid));

        //            var itdDto = await _intertransferDetailRepo.GetAsync(existingDetailEntry[0].Id);
        //            string currentStatus;

        //            //if (CanChangeRequestQty == true && items.RequestQty > 0)
        //            //    itdDto.RequestQty = items.RequestQty;

        //            itdDto.IssueQty = items.IssueQty;
        //            itdDto.Price = items.Price;
        //            itdDto.IssueValue = Math.Round(items.IssueQty * items.Price, 3);
        //            TotalTransferValue = TotalTransferValue + itdDto.IssueValue;

        //            currentStatus = "";
        //            if (items.CurrentStatus == L("Pending"))
        //                currentStatus = "P";
        //            else if (items.CurrentStatus == L("Approved"))
        //                currentStatus = "A";
        //            else if (items.CurrentStatus == L("Excess"))
        //                currentStatus = "E";
        //            else if (items.CurrentStatus == L("Rejected"))
        //                currentStatus = "R";
        //            else if (items.CurrentStatus == L("Shortage"))
        //                currentStatus = "S";

        //            itdDto.CurrentStatus = currentStatus;
        //            itdDto.RequestRemarks = items.RequestRemarks;
        //            itdDto.ApprovedRemarks = items.ApprovedRemarks;

        //            var retId2 = await _intertransferDetailRepo.InsertOrUpdateAndGetIdAsync(itdDto);

        //            if (input.SaveStatus.Equals("SAVE") && (itdDto.CurrentStatus.Equals("A") || itdDto.CurrentStatus.Equals("E") || itdDto.CurrentStatus.Equals("S")))
        //            {
        //                MaterialLedgerDto ml = new MaterialLedgerDto();
        //                ml.LocationRefId = item.RequestLocationRefId;
        //                ml.MaterialRefId = items.MaterialRefId;
        //                ml.LedgerDate = (DateTime)dto.CompletedTime;
        //                ml.TransferOut = items.IssueQty;
        //                ml.UnitId = items.UnitRefId;

        //                var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
        //                itdDto.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;
        //            }

        //            #region InterTransferReturn
        //            InterTransferDetailViewDto trndet = new InterTransferDetailViewDto();
        //            if (items.HoldFlag)
        //            {
        //                decimal requestQty = items.HoldQty;

        //                if (items.UnitRefName.Length == 0 || items.UnitRefId == 0)
        //                {
        //                    throw new UserFriendlyException("UnitErr");
        //                }

        //                string remarksInterTransfer = L("TransferHold", input.InterTransfer.Id, items.IssueQty, items.RequestQty, items.UnitRefName);

        //                trndet.Sno = interSno++;
        //                trndet.MaterialRefId = materialrefid;
        //                trndet.RequestQty = requestQty;
        //                trndet.IssueQty = 0;
        //                trndet.DefaultUnitId = items.DefaultUnitId;
        //                trndet.UnitRefId = items.UnitRefId;
        //                trndet.Price = items.Price;
        //                trndet.CurrentStatus = "P";
        //                trndet.ApprovedRemarks = remarksInterTransfer;
        //                trndet.RequestRemarks = remarksInterTransfer;
        //                intertransferDetailDtos.Add(trndet);
        //            }


        //            #endregion
        //        }
        //    }

        //    CreateOrUpdateInterTransferInput newReturnInterTransferDto = new CreateOrUpdateInterTransferInput();
        //    IdInput transferIdInput = new IdInput();
        //    var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.InterTransfer.RequestLocationRefId);

        //    if (intertransferDetailDtos.Count > 0)
        //    {
        //        InterTransferEditDto masInterDto = new InterTransferEditDto();

        //        masInterDto.RequestDate = loc.HouseTransactionDate.Value.Date;
        //        masInterDto.LocationRefId = input.InterTransfer.LocationRefId;
        //        masInterDto.RequestLocationRefId = input.InterTransfer.RequestLocationRefId;
        //        masInterDto.CurrentStatus = "D";
        //        masInterDto.ApprovedPersonId = (int)AbpSession.UserId;
        //        masInterDto.ReceivedPersonId = (int)AbpSession.UserId;

        //        newReturnInterTransferDto.InterTransfer = masInterDto;
        //        newReturnInterTransferDto.InterTransferDetail = intertransferDetailDtos;
        //        newReturnInterTransferDto.SaveStatus = "";

        //        transferIdInput = await CreateOrUpdateInterTransfer(newReturnInterTransferDto);
        //    }

        //    item.TransferValue = TotalTransferValue;

        //    Debug.WriteLine("InterTransfer UpdateApproval() End Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));

        //    return new IdInput
        //    {
        //        Id = input.InterTransfer.Id.Value
        //    };
        //}

     

        //        public async Task<IdInput> CreateOrUpdateInterTransfer(CreateOrUpdateInterTransferInput input)
        //        {
        //            Debug.WriteLine("InterTransfer CreateOrUpdateInterTransfer() Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));

        //            if (input.InterTransfer.DeliveryDateRequested == DateTime.MinValue)
        //                input.InterTransfer.DeliveryDateRequested = input.InterTransfer.RequestDate;
        //            if (input.InterTransfer.DeliveryDateRequested.Date <= DateTime.Today.AddDays(1).Date)
        //            {
        //                var elapsedTimeAlert = await _interTransferRequestAppService.GetRequestTimeStatus(input.InterTransfer.LocationRefId);
        //                if (elapsedTimeAlert.AllowFlag == false)
        //                {
        //                    throw new UserFriendlyException(elapsedTimeAlert.AlertMsg);
        //                }
        //            }
        //            if (input.InterTransfer.CreatorUserId.HasValue && input.InterTransfer.CreatorUserId>0 && input.InterTransfer.CreatorUserId != AbpSession.UserId)
        //            {
        //                var existUser = await UserManager.GetUserByIdAsync((long) input.InterTransfer.CreatorUserId);
        //                var currentUser = await GetCurrentUserAsync();
        //                throw new UserFriendlyException(L("UserIdChangedErrorAlert",existUser.UserName, currentUser.UserName));
        //            }

        //            if (input.InterTransfer.Id.HasValue)
        //            {
        //                return await UpdateInterTransfer(input);
        //            }
        //            else
        //            {
        //                #region Check Material Active Status in Request Destination Location
        //                List<int> arrMaterialRefIds = input.InterTransferDetail.Select(t => t.MaterialRefId).ToList();
        //                var destinationLocationMaterialStatus = await _materiallocationwisestockRepo.GetAll().Where(t => t.LocationRefId == input.InterTransfer.RequestLocationRefId && arrMaterialRefIds.Contains(t.MaterialRefId) && t.IsActiveInLocation == false).ToListAsync();
        //                if (destinationLocationMaterialStatus.Count > 0)
        //                {
        //                    var reqLocation = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.InterTransfer.RequestLocationRefId)
        //;
        //                    arrMaterialRefIds = destinationLocationMaterialStatus.Select(t => t.MaterialRefId).ToList();
        //                    var inactiveMaterilas = await _materialRepo.GetAll().Where(t=> arrMaterialRefIds.Contains(t.Id)).Select(t => t.MaterialName).ToListAsync();
        //                    string errorMessage = string.Join(",", inactiveMaterilas);
        //                    errorMessage = L("Material") + " " + L("Dorment") + " " + L("Status")+ " " + L("In") + " " + L("Location") + " : " + reqLocation.Name + System.Environment.NewLine + errorMessage;
        //                    throw new UserFriendlyException(errorMessage);
        //                }
        //                #endregion

        //                var ip = input;
        //                if (ip.InterTransfer.DeliveryDateRequested == DateTime.MinValue)
        //                    ip.InterTransfer.DeliveryDateRequested = ip.InterTransfer.RequestDate;

        //                await CreateTemplate(ip);
        //                DateTime requestTime = DateTime.Now;

        //                var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.InterTransfer.LocationRefId);
        //                var reqTransactionDate = loc.HouseTransactionDate;

        //                if (reqTransactionDate == null)
        //                {
        //                    var dayClose = _dayCloseAppService.GetDayClose(new IdInput { Id = input.InterTransfer.LocationRefId });
        //                    requestTime = dayClose.Result.TransactionDate;
        //                }
        //                else if (requestTime != null)
        //                {
        //                    if (requestTime.ToString("yyyy-MMM-dd") != reqTransactionDate.Value.ToString("yyyy-MMM-dd"))
        //                    {
        //                        requestTime = reqTransactionDate.Value;
        //                    }
        //                }

        //                input.InterTransfer.RequestDate = requestTime;

        //                IdInput interid = await CreateInterTransfer(input);
        //                string OperationToBeDone = input.SaveStatus;

        //                if (input.SaveStatus != null && (OperationToBeDone.Equals("DA") || OperationToBeDone.Equals("DR")))
        //                {
        //                    input.SaveStatus = "SAVE";
        //                    input.InterTransfer.Id = interid.Id;

        //                    DateTime completedTime = DateTime.Now;

        //                    var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.InterTransfer.RequestLocationRefId);
        //                    var transactionDate = location.HouseTransactionDate;

        //                    if (transactionDate == null)
        //                    {
        //                        var dayClose = _dayCloseAppService.GetDayClose(new IdInput { Id = input.InterTransfer.RequestLocationRefId });
        //                        completedTime = dayClose.Result.TransactionDate;
        //                    }
        //                    else if (transactionDate != null)
        //                    {
        //                        if (completedTime.ToString("yyyy-MMM-dd") != transactionDate.Value.ToString("yyyy-MMM-dd"))
        //                        {
        //                            completedTime = transactionDate.Value;
        //                        }
        //                    }

        //                    input.InterTransfer.CompletedTime = completedTime;

        //                    List<Location> l = await _locationRepo.GetAll().Where(t => t.Id == input.InterTransfer.RequestLocationRefId).ToListAsync();
        //                    List<LocationListDto> locinput = l.MapTo<List<LocationListDto>>();
        //                    List<int> materialRefIdsRateRequired = input.InterTransferDetail.Select(t => t.MaterialRefId).ToList();

        //                    var matView = await _housereportAppService.GetMaterialRateView(new GetHouseReportMaterialRateInput
        //                    {
        //                        StartDate = completedTime,
        //                        EndDate = completedTime,
        //                        Locations = locinput,
        //                        MaterialRefIds = materialRefIdsRateRequired,
        //                        FunctionCalledBy = "Inter Transfer Create"
        //                    });

        //                    var materialRate = matView.MaterialRateView;

        //                    //var rsUnitConversion = await _unitconversionRepo.GetAllListAsync();
        //                    var rsUnitConversion = await _unitconversionRepo.GetAll().Select(t => new { t.BaseUnitId, t.RefUnitId, t.Conversion, t.MaterialRefId }).ToListAsync();
        //                    foreach (var lst in input.InterTransferDetail)
        //                    {
        //                        var matprice = materialRate.FirstOrDefault(t => t.MaterialRefId == lst.MaterialRefId);

        //                        lst.CurrentStatus = L("Approved");
        //                        if (lst.DefaultUnitId == 0)
        //                        {
        //                            var mat = await _materialRepo.FirstOrDefaultAsync(t => t.Id == lst.MaterialRefId);
        //                            lst.DefaultUnitId = mat.DefaultUnitId;
        //                        }

        //                        decimal conversionFactor = 1;
        //                        if (lst.DefaultUnitId == lst.UnitRefId)
        //                            conversionFactor = 1;
        //                        else
        //                        {
        //                            var conv = rsUnitConversion.First(t => t.BaseUnitId == lst.DefaultUnitId && t.RefUnitId == lst.UnitRefId && t.MaterialRefId == lst.MaterialRefId);
        //                            if (conv == null)
        //                            {
        //                                conv = rsUnitConversion.First(t => t.BaseUnitId == lst.DefaultUnitId && t.RefUnitId == lst.UnitRefId);
        //                            }
        //                            if (conv != null)
        //                            {
        //                                conversionFactor = conv.Conversion;
        //                            }
        //                            else
        //                            {
        //                                var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == lst.DefaultUnitId);
        //                                if (baseUnit == null)
        //                                {
        //                                    throw new UserFriendlyException(L("UnitIdDoesNotExist", lst.DefaultUnitId));
        //                                }
        //                                var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == lst.UnitRefId);
        //                                if (refUnit == null)
        //                                {
        //                                    throw new UserFriendlyException(L("UnitIdDoesNotExist", lst.UnitRefId));
        //                                }
        //                                throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
        //                            }

        //                            lst.OnHand = lst.OnHand * conversionFactor;
        //                        }

        //                        var templst = lst;

        //                        if (lst.Price > 0 && OperationToBeDone.Equals("DR"))
        //                        {
        //                            //  Do Nothing , Same Rate Based Received Price
        //                        }
        //                        else
        //                        {
        //                            if (lst.DefaultUnitId == lst.UnitRefId)
        //                                lst.Price = matprice.AvgRate;
        //                            else
        //                            {
        //                                lst.Price = Math.Round(matprice.AvgRate * 1 / conversionFactor, 3);
        //                            }
        //                        }

        //                        lst.LineTotal = Math.Round(lst.IssueQty * lst.Price, 3);

        //                    }

        //                    IdInput approvedId = await CreateOrUpdateApproval(input);

        //                    if (OperationToBeDone.Equals("DR"))
        //                    {
        //                        CreateOrUpdateInterReceivedInput inputforReceived = new CreateOrUpdateInterReceivedInput();
        //                        inputforReceived.InterTransfer = input.InterTransfer;
        //                        inputforReceived.SaveStatus = "V";
        //                        inputforReceived.InterTransfer.ReceivedTime = input.InterTransfer.RequestDate;

        //                        List<InterTransferReceivedEditDto> reclist = new List<InterTransferReceivedEditDto>();

        //                        int sno = 1;
        //                        foreach (var approvedmat in input.InterTransferDetail)
        //                        {
        //                            InterTransferReceivedEditDto newApprovalitem = new InterTransferReceivedEditDto
        //                            {
        //                                InterTransferRefId = approvedId.Id,
        //                                Sno = sno,
        //                                MaterialRefId = approvedmat.MaterialRefId,
        //                                MaterialRefName = approvedmat.MaterialRefName,
        //                                RequestQty = approvedmat.RequestQty,
        //                                IssueQty = approvedmat.IssueQty,
        //                                ReceivedQty = approvedmat.IssueQty,
        //                                Price = approvedmat.Price,
        //                                ReceivedValue = Math.Round(approvedmat.IssueQty * approvedmat.Price, 3),
        //                                CurrentStatus = L("Received"),
        //                                Uom = approvedmat.Uom,
        //                                UnitRefId = approvedmat.UnitRefId,
        //                            };

        //                            reclist.Add(newApprovalitem);
        //                            sno++;
        //                        }

        //                        inputforReceived.InterTransferReceivedDetail = reclist;

        //                        InterTransferReceivedWithAdjustmentId recid = await CreateOrUpdateInterReceived(inputforReceived);
        //                    }
        //                }

        //                return interid;// await CreateInterTransfer(input);
        //            }

        //        }


        //protected virtual async Task<IdInput> UpdateInterTransfer(CreateOrUpdateInterTransferInput input)
        //{
        //    var item = await _intertransferRepo.GetAsync(input.InterTransfer.Id.Value);
        //    var dto = input.InterTransfer;

        //    item.LocationRefId = dto.LocationRefId;
        //    item.RequestLocationRefId = dto.RequestLocationRefId;
        //    item.CurrentStatus = dto.CurrentStatus;
        //    item.RequestDate = dto.RequestDate;
        //    item.DeliveryDateRequested = dto.DeliveryDateRequested;
        //    item.DocReferenceNumber = dto.DocReferenceNumber;

        //    //  Update Detail Link
        //    List<int> materialsToBeRetained = new List<int>();

        //    if (input.InterTransferDetail != null && input.InterTransferDetail.Count > 0)
        //    {
        //        foreach (var items in input.InterTransferDetail)
        //        {
        //            int materialrefid = items.MaterialRefId;
        //            materialsToBeRetained.Add(materialrefid);

        //            var existingDetailEntry = _intertransferDetailRepo.GetAllList(u => u.InterTransferRefId == dto.Id && u.MaterialRefId.Equals(materialrefid));

        //            if (existingDetailEntry.Count == 0)  //  Add New Material
        //            {
        //                InterTransferDetail itdDto = new InterTransferDetail();

        //                itdDto.InterTransferRefId = (int)dto.Id;
        //                itdDto.Sno = items.Sno;
        //                itdDto.MaterialRefId = items.MaterialRefId;
        //                itdDto.RequestQty = items.RequestQty;
        //                itdDto.IssueQty = items.IssueQty;
        //                itdDto.UnitRefId = items.UnitRefId;
        //                itdDto.Price = items.Price;
        //                itdDto.IssueValue = Math.Round(items.IssueQty * items.Price, 3);
        //                itdDto.CurrentStatus = "P";
        //                itdDto.RequestRemarks = items.RequestRemarks;
        //                itdDto.ApprovedRemarks = items.ApprovedRemarks;

        //                var retDetailId = await _intertransferDetailRepo.InsertAndGetIdAsync(itdDto);
        //            }
        //            else
        //            {
        //                var itdDto = await _intertransferDetailRepo.GetAsync(existingDetailEntry[0].Id);
        //                string currentStatus;

        //                itdDto.InterTransferRefId = (int)dto.Id;
        //                itdDto.Sno = items.Sno;
        //                itdDto.MaterialRefId = items.MaterialRefId;
        //                itdDto.RequestQty = items.RequestQty;
        //                itdDto.IssueQty = items.IssueQty;
        //                itdDto.UnitRefId = items.UnitRefId;
        //                itdDto.Price = items.Price;
        //                itdDto.IssueValue = Math.Round(items.IssueQty * items.Price, 3);

        //                currentStatus = "P";
        //                if (items.CurrentStatus == L("Pending"))
        //                    currentStatus = "P";
        //                else if (items.CurrentStatus == L("Approved"))
        //                    currentStatus = "A";
        //                else if (items.CurrentStatus == L("Excess"))
        //                    currentStatus = "E";
        //                else if (items.CurrentStatus == L("Rejected"))
        //                    currentStatus = "R";


        //                itdDto.CurrentStatus = currentStatus;
        //                itdDto.RequestRemarks = items.RequestRemarks;
        //                itdDto.ApprovedRemarks = items.ApprovedRemarks;

        //                var retId2 = await _intertransferDetailRepo.InsertOrUpdateAndGetIdAsync(itdDto);

        //            }
        //        }
        //    }

        //    var delBrandList = _intertransferDetailRepo.GetAll().Where(a => a.InterTransferRefId == input.InterTransfer.Id.Value && !materialsToBeRetained.Contains(a.MaterialRefId)).ToList();

        //    foreach (var a in delBrandList)
        //    {
        //        _intertransferDetailRepo.Delete(a.Id);
        //    }

        //    CheckErrors(await _intertransferManager.CreateSync(item));

        //    //await CreateTemplate(input);

        //    return new IdInput
        //    {
        //        Id = input.InterTransfer.Id.Value
        //    };

        //}

        //protected virtual async Task CreateTemplate(CreateOrUpdateInterTransferInput input)
        //{
        //    if (input.Templatesave == null)
        //        return;
        //    string templateJson = _xmlandjsonConvertorAppService.SerializeToJSON(input);
        //    Template lastTemplate;
        //    int? templateScope;
        //    if (input.Templatesave.Templatescope == true)
        //    {
        //        templateScope = null;
        //    }
        //    else
        //    {
        //        if (input.Templatesave.TemplateType == (int)TemplateType.DirectInterTransfer)
        //            templateScope = input.InterTransfer.RequestLocationRefId;
        //        else
        //            templateScope = input.InterTransfer.LocationRefId;
        //    }

        //    if (input.Templatesave.Templateflag == true)
        //    {
        //        string repName = input.Templatesave.Templatename;

        //        var templateExist = _templateRepo.GetAll().Where(t => t.Name == repName
        //            && t.TemplateType == input.Templatesave.TemplateType).ToList();
        //        if (templateExist.Count == 0)
        //        {
        //            lastTemplate = new Template
        //            {
        //                Name = input.Templatesave.Templatename,
        //                TemplateType = input.Templatesave.TemplateType,
        //                TemplateData = templateJson,
        //                LocationRefId = templateScope
        //            };
        //        }
        //        else
        //        {
        //            lastTemplate = templateExist[0];
        //            lastTemplate.LocationRefId = templateScope;
        //            lastTemplate.TemplateData = templateJson;
        //        }
        //    }
        //    else
        //    {
        //        string repName = L("LastRequestDraft");
        //        var templateExist = _templateRepo.GetAll().Where(t => t.Name == repName && t.TemplateType == (int)TemplateType.RequestInterLocation).ToList();
        //        if (templateExist.Count() == 0)
        //        {
        //            lastTemplate = new Template
        //            {
        //                Name = L("LastRequestDraft"),
        //                TemplateType = (int)TemplateType.RequestInterLocation,
        //                TemplateData = templateJson,
        //                LocationRefId = input.InterTransfer.LocationRefId
        //            };
        //        }
        //        else
        //        {
        //            lastTemplate = templateExist[0];
        //            lastTemplate.LocationRefId = templateScope;
        //            lastTemplate.TemplateData = templateJson;
        //        }
        //    }
        //    await _templateRepo.InsertOrUpdateAndGetIdAsync(lastTemplate);
        //}

        //protected virtual async Task<IdInput> CreateInterTransfer(CreateOrUpdateInterTransferInput input)
        //{
        //    var dto = input.InterTransfer.MapTo<InterTransfer>();
        //    if (dto.CurrentStatus != "Q")
        //        dto.CurrentStatus = "P";

        //    var retId = await _intertransferRepo.InsertOrUpdateAndGetIdAsync(dto);

        //    foreach (InterTransferDetailViewDto items in input.InterTransferDetail.ToList())
        //    {
        //        InterTransferDetail itdDto = new InterTransferDetail();

        //        itdDto.InterTransferRefId = retId;
        //        itdDto.Sno = items.Sno;
        //        itdDto.MaterialRefId = items.MaterialRefId;
        //        itdDto.RequestQty = items.RequestQty;
        //        itdDto.IssueQty = items.IssueQty;
        //        itdDto.UnitRefId = items.UnitRefId;
        //        itdDto.Price = items.Price;
        //        itdDto.CurrentStatus = dto.CurrentStatus;
        //        itdDto.RequestRemarks = items.RequestRemarks;
        //        itdDto.ApprovedRemarks = items.ApprovedRemarks;

        //        var retDetailId = await _intertransferDetailRepo.InsertAndGetIdAsync(itdDto);
        //    }

        //    CheckErrors(await _intertransferManager.CreateSync(dto));

        //    string templateJson = _xmlandjsonConvertorAppService.SerializeToJSON(input);

        //    if (input.SaveStatus != null)
        //    {
        //        if (input.SaveStatus.Equals("DA") || input.SaveStatus.Equals("DR"))
        //            input.Templatesave = null;
        //    }
        //    if (input.Templatesave != null)
        //    {
        //        Template lastTemplate;
        //        int? templateScope;
        //        if (input.Templatesave.Templatescope == true)
        //        {
        //            templateScope = null;
        //        }
        //        else
        //        {
        //            templateScope = input.InterTransfer.LocationRefId;
        //        }

        //        if (input.Templatesave.Templateflag == true)
        //        {
        //            string repName = input.Templatesave.Templatename;

        //            var templateExist = _templateRepo.GetAll().Where(t => t.Name == repName && t.TemplateType == (int)TemplateType.RequestInterLocation).ToList();
        //            if (templateExist.Count == 0)
        //            {
        //                lastTemplate = new Template
        //                {
        //                    Name = input.Templatesave.Templatename,
        //                    TemplateType = (int)TemplateType.RequestInterLocation,
        //                    TemplateData = templateJson,
        //                    LocationRefId = templateScope
        //                };
        //            }
        //            else
        //            {
        //                lastTemplate = templateExist[0];
        //                lastTemplate.LocationRefId = templateScope;
        //                lastTemplate.TemplateData = templateJson;
        //            }
        //        }
        //        else
        //        {
        //            string repName = L("LastRequest");
        //            var templateExist = _templateRepo.GetAll().Where(t => t.Name == repName && t.TemplateType == (int)TemplateType.RequestInterLocation).ToList();
        //            if (templateExist.Count() == 0)
        //            {
        //                lastTemplate = new Template
        //                {
        //                    Name = L("LastRequest"),
        //                    TemplateType = (int)TemplateType.RequestInterLocation,
        //                    TemplateData = templateJson,
        //                    LocationRefId = input.InterTransfer.LocationRefId
        //                };
        //            }
        //            else
        //            {
        //                lastTemplate = templateExist[0];
        //                lastTemplate.LocationRefId = templateScope;
        //                lastTemplate.TemplateData = templateJson;
        //            }
        //        }

        //        await _templateRepo.InsertOrUpdateAndGetIdAsync(lastTemplate);
        //    }


        //    return new IdInput
        //    {
        //        Id = retId
        //    };

        //}

        //  Inter Transfer - Received Details

        public async Task<GetInterReceivedForEditOutput> GetInterReceivedForEdit(NullableIdInput input)
        {
            Debug.WriteLine("InterTransfer GetInterReceivedForEdit() Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));

            InterTransferEditDto editDto;
            List<InterTransferReceivedEditDto> editDetailDto;

            if (input.Id.HasValue)
            {
                var hDto = await _intertransferRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<InterTransferEditDto>();

                editDetailDto = await (from request in _intertransferDetailRepo.GetAll()
                                       .Where(a => a.InterTransferRefId == input.Id.Value && "AES".IndexOf(a.CurrentStatus) >= 0)
                                       join mat in _materialRepo.GetAll()
                                       on request.MaterialRefId equals mat.Id
                                       join un in _unitRepo.GetAll()
                                            on mat.DefaultUnitId equals un.Id
                                       join uiss in _unitRepo.GetAll()
                                           on request.UnitRefId equals uiss.Id
                                       select new InterTransferReceivedEditDto
                                       {
                                           Id = request.Id,
                                           ReceivedQty = 0,
                                           ReturnFlag = false,
                                           ReturnQty = 0,
                                           AdjustmentFlag = false,
                                           AdjustmentMode = "",
                                           AdjustmentQty = 0,
                                           MaterialRefId = request.MaterialRefId,
                                           MaterialPetName = mat.MaterialPetName,
                                           MaterialRefName = mat.MaterialName,
                                           CurrentStatus = request.CurrentStatus,
                                           InterTransferRefId = request.InterTransferRefId,
                                           Sno = request.Sno,
                                           RequestQty = request.RequestQty,
                                           IssueQty = request.IssueQty,
                                           Price = request.Price,
                                           ReceivedValue = Math.Round(request.IssueQty * request.Price, 3),
                                           Uom = un.Name,
                                           UnitRefId = request.UnitRefId,
                                           UnitRefName = uiss.Name,
                                           DefaultUnitId = mat.DefaultUnitId,
                                           DefaultUnitName = un.Name,

                                       }).ToListAsync();

                //var rsUnitConversion = await _unitconversionRepo.GetAllListAsync();
                var rsUnitConversion = await _unitconversionRepo.GetAll().Select(t => new { t.BaseUnitId, t.RefUnitId, t.Conversion, t.MaterialRefId }).ToListAsync();

                foreach (var item in editDetailDto)
                {
                    if (item.CurrentStatus.Equals("Q"))
                        item.CurrentStatus = L("RequestApprovalPending");
                    if (item.CurrentStatus.Equals("P"))
                        item.CurrentStatus = L("Pending");
                    else if (item.CurrentStatus.Equals("A"))
                        item.CurrentStatus = L("Approved");
                    else if (item.CurrentStatus.Equals("R"))
                        item.CurrentStatus = L("Rejected");
                    else if (item.CurrentStatus.Equals("D"))
                        item.CurrentStatus = L("Drafted");
                    else if (item.CurrentStatus.Equals("V"))
                        item.CurrentStatus = L("Received");
                    else if (item.CurrentStatus.Equals("I"))
                        item.CurrentStatus = L("PartiallyReceived");
                    else if (item.CurrentStatus.Equals("E"))
                        item.CurrentStatus = L("Excess");
                    else if (item.CurrentStatus.Equals("S"))
                        item.CurrentStatus = L("Shortage");

                    item.LineTotal = Math.Round(item.IssueQty * item.Price, 3);

                    var rsReceived = await _intertransferReceivedDetailRepo.FirstOrDefaultAsync(t => t.InterTransferRefId == input.Id.Value && t.MaterialRefId == item.MaterialRefId);
                    if (rsReceived != null)
                    {
                        item.ReceivedQty = rsReceived.ReceivedQty;
                        item.ReceivedValue = Math.Round(item.ReceivedQty * item.Price, 3);
                        item.LineTotal = item.ReceivedValue;
                    }
                }
                editDto.ReceivedValue = Math.Round(editDetailDto.Sum(t => t.LineTotal), 3);
            }
            else
            {
                editDto = new InterTransferEditDto();
                editDetailDto = new List<InterTransferReceivedEditDto>();
            }
            Debug.WriteLine("InterTransfer GetInterReceivedForEdit() End Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));

            return new GetInterReceivedForEditOutput
            {
                InterTransfer = editDto,
                InterTransferReceivedDetail = editDetailDto
            };
        }


        //public async Task<InterTransferReceivedWithAdjustmentId> CreateOrUpdateInterReceived(CreateOrUpdateInterReceivedInput input)
        //{

        //    return await UpdateInterReceived(input);
        //}

        //protected virtual async Task<InterTransferReceivedWithAdjustmentId> UpdateInterReceived(CreateOrUpdateInterReceivedInput input)
        //{
        //    Debug.WriteLine("UpdateInterReceived() Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));

        //    var item = await _intertransferRepo.GetAsync(input.InterTransfer.Id.Value);
        //    var dto = input.InterTransfer;

        //    item.CurrentStatus = input.SaveStatus;
        //    item.ReceivedTime = dto.ReceivedTime; //  DateTime.Now;
        //    item.ReceivedPersonId = dto.ReceivedPersonId;

        //    List<AdjustmentDetailViewDto> adjustmentDetailDtos = new List<AdjustmentDetailViewDto>();
        //    List<InterTransferDetailViewDto> intertransferDetailDtos = new List<InterTransferDetailViewDto>();


        //    //  Update Detail Link
        //    //List<int> materialsToBeRetained = new List<int>();

        //    decimal TotalReceivedValue = 0;
        //    int adjSno = 1;
        //    int interSno = 1;

        //    if (input.InterTransferReceivedDetail != null && input.InterTransferReceivedDetail.Count > 0)
        //    {
        //        foreach (var items in input.InterTransferReceivedDetail)
        //        {
        //            int materialrefid = items.MaterialRefId;

        //            //materialsToBeRetained.Add(materialrefid);

        //            var existingDetailEntry = _intertransferReceivedDetailRepo.GetAllList(u => u.InterTransferRefId == dto.Id && u.MaterialRefId.Equals(materialrefid));

        //            if (existingDetailEntry.Count == 0)  //  Add New Material
        //            {

        //                InterTransferReceivedDetail itdDto = new InterTransferReceivedDetail();

        //                itdDto.InterTransferRefId = (int)dto.Id;
        //                itdDto.Sno = items.Sno;
        //                itdDto.MaterialRefId = items.MaterialRefId;
        //                itdDto.ReceivedQty = items.ReceivedQty;
        //                itdDto.AdjustmentMode = items.AdjustmentMode;
        //                itdDto.AdjustmentQty = items.AdjustmentQty;
        //                itdDto.ReturnQty = items.ReturnQty;
        //                itdDto.UnitRefId = items.UnitRefId;
        //                itdDto.Price = items.Price;
        //                itdDto.ReceivedValue = Math.Round(items.ReceivedQty * items.Price, 3);
        //                TotalReceivedValue = TotalReceivedValue + itdDto.ReceivedValue;
        //                string currentStatus = "";
        //                if (items.CurrentStatus == L("Pending"))
        //                    currentStatus = "P";
        //                else if (items.CurrentStatus == L("Excess"))
        //                    currentStatus = "E";
        //                else if (items.CurrentStatus == L("Shortage"))
        //                    currentStatus = "S";
        //                else if (items.CurrentStatus == L("Rejected"))
        //                    currentStatus = "R";
        //                else if (items.CurrentStatus == L("Received"))
        //                    currentStatus = "V";

        //                if (currentStatus == "" || currentStatus.Length == 0)
        //                {
        //                    if (items.ReceivedQty == 0)
        //                    {
        //                        continue;
        //                    }
        //                    throw new UserFriendlyException(L("EnterQtyWithMaterialName", items.MaterialRefName));
        //                }

        //                itdDto.CurrentStatus = currentStatus;

        //                var retDetailId = await _intertransferReceivedDetailRepo.InsertAndGetIdAsync(itdDto);

        //                if (currentStatus.Equals("V") || currentStatus.Equals("E") || currentStatus.Equals("S"))
        //                {
        //                    MaterialLedgerDto ml = new MaterialLedgerDto();
        //                    ml.LocationRefId = dto.LocationRefId;
        //                    ml.MaterialRefId = items.MaterialRefId;
        //                    ml.LedgerDate = (DateTime)item.ReceivedTime;
        //                    ml.TransferIn = items.ReceivedQty;
        //                    ml.UnitId = items.UnitRefId;

        //                    var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
        //                    itdDto.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;
        //                }

        //                #region Adjustment
        //                AdjustmentDetailViewDto det = new AdjustmentDetailViewDto();
        //                if (items.AdjustmentFlag)
        //                {
        //                    decimal adjustmentQty = items.AdjustmentQty;
        //                    string adjustmentMode = items.AdjustmentMode;

        //                    decimal receivedQty = 0;
        //                    if (adjustmentMode.Equals(L("Shortage")))
        //                        receivedQty = items.ReceivedQty - items.AdjustmentQty;
        //                    else if (adjustmentMode.Equals(L("Excess")))
        //                    {
        //                        receivedQty = items.ReceivedQty + items.AdjustmentQty;
        //                    }

        //                    if (items.UnitRefName.Length == 0 || items.UnitRefId == 0)
        //                    {
        //                        throw new UserFriendlyException("UnitErr");
        //                    }

        //                    string adjustmentApprovedRemarks = L("TransferAdjustmemt", input.InterTransfer.Id, items.IssueQty, receivedQty, items.UnitRefName);

        //                    det.MaterialRefId = materialrefid;
        //                    det.AdjustmentQty = adjustmentQty;
        //                    det.AdjustmentApprovedRemarks = adjustmentApprovedRemarks;
        //                    det.AdjustmentMode = adjustmentMode;
        //                    det.UnitRefId = items.UnitRefId;
        //                    det.Sno = adjSno++;

        //                    adjustmentDetailDtos.Add(det);
        //                }
        //                #endregion

        //                #region InterTransferReturn
        //                InterTransferDetailViewDto trndet = new InterTransferDetailViewDto();
        //                if (items.ReturnFlag)
        //                {
        //                    decimal requestQty = items.ReceivedQty - items.ReturnQty;

        //                    if (items.UnitRefName.Length == 0 || items.UnitRefId == 0)
        //                    {
        //                        throw new UserFriendlyException("UnitErr");
        //                    }

        //                    string remarksInterTransfer = L("TransferAdjustmemt", input.InterTransfer.Id, items.IssueQty, requestQty, items.UnitRefName);

        //                    trndet.Sno = interSno++;
        //                    trndet.MaterialRefId = materialrefid;
        //                    trndet.RequestQty = items.ReturnQty;
        //                    trndet.IssueQty = items.ReturnQty;
        //                    trndet.DefaultUnitId = items.DefaultUnitId;
        //                    trndet.UnitRefId = items.UnitRefId;
        //                    trndet.Price = items.Price;
        //                    trndet.CurrentStatus = "A";
        //                    trndet.ApprovedRemarks = remarksInterTransfer;
        //                    trndet.RequestRemarks = remarksInterTransfer;



        //                    intertransferDetailDtos.Add(trndet);
        //                }


        //                #endregion

        //            }
        //            else
        //            {
        //                var itdDto = await _intertransferReceivedDetailRepo.GetAsync(existingDetailEntry[0].Id);
        //                string currentStatus;

        //                decimal differcenceQty;
        //                differcenceQty = items.ReceivedQty - itdDto.ReceivedQty;

        //                itdDto.InterTransferRefId = (int)dto.Id;
        //                itdDto.Sno = items.Sno;
        //                itdDto.MaterialRefId = items.MaterialRefId;
        //                itdDto.ReceivedQty = items.ReceivedQty;
        //                itdDto.UnitRefId = items.UnitRefId;
        //                itdDto.Price = items.Price;
        //                itdDto.ReceivedValue = Math.Round(items.ReceivedQty * items.Price, 3);

        //                TotalReceivedValue = TotalReceivedValue + itdDto.ReceivedValue;

        //                currentStatus = "";
        //                if (items.CurrentStatus == L("Pending"))
        //                    currentStatus = "P";
        //                else if (items.CurrentStatus == L("Excess"))
        //                    currentStatus = "E";
        //                else if (items.CurrentStatus == L("Shortage"))
        //                    currentStatus = "S";
        //                else if (items.CurrentStatus == L("Rejected"))
        //                    currentStatus = "R";
        //                else if (items.CurrentStatus == L("Received"))
        //                    currentStatus = "V";


        //                itdDto.CurrentStatus = currentStatus;

        //                var retId2 = await _intertransferReceivedDetailRepo.InsertOrUpdateAndGetIdAsync(itdDto);

        //                if (currentStatus.Equals("V") || currentStatus.Equals("E") || currentStatus.Equals("S"))
        //                {
        //                    MaterialLedgerDto ml = new MaterialLedgerDto();
        //                    ml.LocationRefId = dto.RequestLocationRefId;
        //                    ml.MaterialRefId = items.MaterialRefId;
        //                    ml.LedgerDate = (DateTime)item.ReceivedTime;
        //                    ml.TransferIn = differcenceQty;
        //                    ml.UnitId = items.UnitRefId;

        //                    var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
        //                    itdDto.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;
        //                }

        //            }
        //        }
        //    }

        //    item.ReceivedValue = TotalReceivedValue;
        //    IdInput adjId = new IdInput { Id = 0 };

        //    var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.InterTransfer.LocationRefId);
        //    CreateOrUpdateAdjustmentInput newAdjDto = new CreateOrUpdateAdjustmentInput();
        //    if (adjustmentDetailDtos.Count > 0)
        //    {
        //        AdjustmentEditDto masDto = new AdjustmentEditDto();

        //        if (input.InterTransfer.ReceivedTime == null)
        //        {
        //            masDto.AdjustmentDate = loc.HouseTransactionDate.Value.Date;
        //        }
        //        else
        //            masDto.AdjustmentDate = input.InterTransfer.ReceivedTime.Value.Date;

        //        masDto.AdjustmentRemarks = L("TransferReceivedDifference");
        //        masDto.ApprovedPersonId = (int)AbpSession.UserId;
        //        masDto.LocationRefId = dto.LocationRefId;
        //        masDto.EnteredPersonId = (int)AbpSession.UserId;
        //        masDto.TokenRefNumber = 999;

        //        newAdjDto.Adjustment = masDto;
        //        newAdjDto.AdjustmentDetail = adjustmentDetailDtos;

        //        adjId = await _adjustmentAppService.CreateOrUpdateAdjustment(newAdjDto);
        //    }

        //    CreateOrUpdateInterTransferInput newReturnInterTransferDto = new CreateOrUpdateInterTransferInput();
        //    IdInput transferIdInput = new IdInput();

        //    if (intertransferDetailDtos.Count > 0)
        //    {
        //        InterTransferEditDto masInterDto = new InterTransferEditDto();

        //        var loc1 = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.InterTransfer.RequestLocationRefId);

        //        masInterDto.RequestDate = loc1.HouseTransactionDate.Value.Date;
        //        masInterDto.LocationRefId = input.InterTransfer.RequestLocationRefId;
        //        masInterDto.RequestLocationRefId = input.InterTransfer.LocationRefId;
        //        masInterDto.CurrentStatus = "A";
        //        masInterDto.ApprovedPersonId = (int)AbpSession.UserId;
        //        masInterDto.ReceivedPersonId = (int)AbpSession.UserId;

        //        newReturnInterTransferDto.InterTransfer = masInterDto;
        //        newReturnInterTransferDto.InterTransferDetail = intertransferDetailDtos;
        //        newReturnInterTransferDto.SaveStatus = "DR";

        //        transferIdInput = await CreateOrUpdateInterTransfer(newReturnInterTransferDto);
        //    }

        //    if (adjId.Id > 0)
        //        item.AdjustmentRefId = adjId.Id;

        //    if (transferIdInput.Id > 0)
        //        item.InterTransferRefId = transferIdInput.Id;

        //    var locReq = await _locationRepo.FirstOrDefaultAsync(t => t.Id == dto.RequestLocationRefId);
        //    item.InterTransferReceivedAccountDate = locReq.HouseTransactionDate.Value;

        //    Debug.WriteLine("UpdateInterReceived() End Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));

        //    return new InterTransferReceivedWithAdjustmentId
        //    {
        //        Id = input.InterTransfer.Id.Value,
        //        AdjustmentId = adjId.Id,
        //        ReturnTransferId = transferIdInput.Id
        //    };

        //}


        public async Task<GetInterTransferForEditOutput> GetTemplateObjectForEdit(GetObjectFromString input)
        {
            var lstTemplate = await _templateRepo.FirstOrDefaultAsync(t => t.TemplateType == input.TemplateType && t.Id == input.TemplateId);
            GetInterTransferForEditOutput ReturnObject = new GetInterTransferForEditOutput();

            if (lstTemplate == null)
                ReturnObject = _xmlandjsonConvertorAppService.DeSerializeFromJSON<GetInterTransferForEditOutput>(input.ObjectString);
            else
                ReturnObject = _xmlandjsonConvertorAppService.DeSerializeFromJSON<GetInterTransferForEditOutput>(lstTemplate.TemplateData);

            TemplateSaveDto editTemplateDto = new TemplateSaveDto();

            InterTransferEditDto editDto = ReturnObject.InterTransfer;

            editDto.Id = null;
            editDto.CurrentStatus = "P";
            editDto.ReceivedTime = null;
            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == editDto.LocationRefId);

            if (editDto.DeliveryDateRequested == DateTime.MinValue)
            {
                editDto.DeliveryDateRequested = DateTime.Now;
                editDto.RequestDate = loc.HouseTransactionDate.Value;
            }
            else
            {
                DateTime requestDateFromTemplate = editDto.RequestDate;
                DateTime deliverDateFromTemplate = editDto.DeliveryDateRequested;
                TimeSpan diffBetweenDays = deliverDateFromTemplate - requestDateFromTemplate;
                double numberofDays = diffBetweenDays.TotalDays;
                editDto.RequestDate = loc.HouseTransactionDate.Value;
                editDto.DeliveryDateRequested = editDto.RequestDate.AddDays(numberofDays);
            }


            List<InterTransferDetailViewDto> interDetail = ReturnObject.InterTransferDetail;

            int locationid = editDto.LocationRefId;

            var retDetail = (from request in interDetail
                             join mat in _materialRepo.GetAll() on request.MaterialRefId equals mat.Id
                             join un in _unitRepo.GetAll() on mat.DefaultUnitId equals un.Id
                             join uiss in _unitRepo.GetAll() on request.UnitRefId equals uiss.Id
                             join matloc in _materiallocationwisestockRepo.GetAll().Where(mls => mls.LocationRefId == locationid)
                             on mat.Id equals matloc.MaterialRefId
                             select new InterTransferDetailViewDto
                             {
                                 Id = request.Id,
                                 RequestQty = request.RequestQty,
                                 Barcode = mat.Barcode == null ? mat.MaterialName : mat.Barcode,
                                 MaterialRefId = request.MaterialRefId,
                                 MaterialPetName = mat.MaterialPetName,
                                 MaterialRefName = mat.MaterialName,
                                 CurrentStatus = "P",
                                 InterTransferRefId = request.InterTransferRefId,
                                 IssueQty = request.IssueQty,
                                 Price = request.Price,
                                 ApprovedRemarks = request.ApprovedRemarks,
                                 RequestRemarks = request.RequestRemarks,
                                 Sno = request.Sno,
                                 MaterialTypeId = mat.MaterialTypeId,
                                 Uom = un.Name,
                                 OnHand = matloc.CurrentInHand,
                                 DefaultUnitId = mat.DefaultUnitId,
                                 DefaultUnitName = un.Name,
                                 UnitRefId = request.UnitRefId,
                                 UnitRefName = uiss.Name,
                                 OnHandWithUom = matloc.CurrentInHand + "/" + un.Name
                             });

            List<InterTransferDetailViewDto> editDetailDto = retDetail.ToList();

            foreach (var lst in editDetailDto)
            {
                string materialTypeName = lst.MaterialTypeId == (int)MaterialType.RAW ? L("RAW") : L("SEMI");
                lst.MaterialTypeName = materialTypeName;

                //lst.UsagePatternList = await _materialAppService.MaterialUsagePattern(
                //   new InputLocationAndMaterialId
                //   {
                //       LocationRefId = locationid,
                //       MaterialRefId = lst.MaterialRefId
                //   }
                //   );
            }


            return new GetInterTransferForEditOutput
            {
                InterTransfer = editDto,
                InterTransferDetail = editDetailDto,
                Templatesave = editTemplateDto
            };
        }


        //public async Task<SetInterTransferDashBoardDto> GetDashBoardTransfer(IdInput input)
        //{
        //    _logger.Info("Dashboard Start Time " + DateTime.Now);
        //    Debug.WriteLine("Dashboard Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
        //    var lstpending = await _intertransferRepo.GetAll().
        //        Where(p => p.LocationRefId == input.Id && (p.CurrentStatus.Equals("Q") || p.CurrentStatus.Equals("D") || p.CurrentStatus.Equals("P"))).ToListAsync();
        //    int pendingCount = lstpending.Count();

        //    var lstApproved = await _intertransferRepo.GetAll().
        //        Where(p => p.LocationRefId == input.Id && p.CompletedTime >= DateTime.Today && p.CurrentStatus.Equals("A")).ToListAsync();
        //    int approvedCount = lstApproved.Count();

        //    var lstReceived = await _intertransferRepo.GetAll().
        //        Where(p => p.LocationRefId == input.Id && p.ReceivedTime > DateTime.Today && (p.CurrentStatus.Equals("I") || p.CurrentStatus.Equals("V"))).ToListAsync();
        //    int receivedCount = lstReceived.Count();

        //    var lstDeclined = await _intertransferRepo.GetAll().
        //        Where(p => p.LocationRefId == input.Id && p.CompletedTime >= DateTime.Today && p.CurrentStatus.Equals("R")).ToListAsync();

        //    int declinedCount = lstDeclined.Count();
        //    _logger.Info("Dashboard Stop Time " + DateTime.Now);
        //    Debug.WriteLine("Dashboard Stop Time " + DateTime.Now);
        //    return new SetInterTransferDashBoardDto
        //    {
        //        TotalApproved = approvedCount,
        //        TotalPending = pendingCount,
        //        TotalReceived = receivedCount,
        //        TotalRejected = declinedCount
        //    };
        //}

        public async Task<InterTransferReportDto> GetTransferDetailReport(InputInterTransferReport input)
        {
            if (input.ToLocations == null)
            {
                throw new UserFriendlyException("LocationErr");
            }

            int[] ToLocIds = input.ToLocations.Select(t => t.Id).ToArray();

            var rsMasterQb = _intertransferRepo.GetAll().Where(t => t.RequestLocationRefId == input.LocationRefId);

            rsMasterQb = rsMasterQb.WhereIf(ToLocIds.Count() > 0, t => ToLocIds.Contains(t.LocationRefId));

            if (input.ExactSearchFlag)
                rsMasterQb = rsMasterQb.WhereIf(!input.TransferNumber.IsNullOrEmpty(), t => t.Id.ToString().Equals(input.TransferNumber));
            else
                rsMasterQb = rsMasterQb.WhereIf(!input.TransferNumber.IsNullOrEmpty(), t => t.Id.ToString().Contains(input.TransferNumber));

            if (input.StartDate.Value != null && input.EndDate.Value != null)
            {
                rsMasterQb =
                    rsMasterQb.Where(
                        a => DbFunctions.TruncateTime(a.CompletedTime) >= DbFunctions.TruncateTime(input.StartDate)
                        && DbFunctions.TruncateTime(a.CompletedTime) <= DbFunctions.TruncateTime(input.EndDate));
            }

            var rsDetailQb = _intertransferDetailRepo.GetAll().Where(t => t.IssueQty > 0);
            var rsMaterialQb = _materialRepo.GetAll();

            bool MaterialExists = false;


            if (input.MaterialList != null && input.MaterialList.Count > 0)
            {
                MaterialExists = true;
                int[] materialListIds = input.MaterialList.Select(t => t.Id).ToArray();
                rsDetailQb = rsDetailQb.WhereIf(MaterialExists, t => materialListIds.Contains(t.MaterialRefId));
                rsMaterialQb = rsMaterialQb.WhereIf(MaterialExists, t => materialListIds.Contains(t.Id));
            }

            if (input.MaterialTypeList != null && input.MaterialTypeList.Count > 0)
            {
                var stridList = input.MaterialTypeList.Select(t => t.Value).ToArray();
                int[] materialTypeIdList = Array.ConvertAll(stridList, s => int.Parse(s));
                rsMaterialQb = rsMaterialQb.Where(t => materialTypeIdList.Contains(t.MaterialTypeId));
            }

            string rawstring = L("RAW");
            string semistring = L("SEMI");


            InterTransferReportDto output = new InterTransferReportDto();

            var allItems = (from mas in rsMasterQb
                            join det in rsDetailQb on mas.Id equals det.InterTransferRefId
                            join mat in rsMaterialQb on det.MaterialRefId equals mat.Id
                            join unit in _unitRepo.GetAll() on mat.DefaultUnitId equals unit.Id
                            join uiss in _unitRepo.GetAll() on det.UnitRefId equals uiss.Id
                            join loc in _locationRepo.GetAll() on mas.LocationRefId equals loc.Id
                            select new InterTransferReportDetailOutPut
                            {
                                TransferRefId = mas.Id,
                                LocationRefId = mas.RequestLocationRefId,
                                LocationRefName = loc.Name,
                                TransferDate = mas.CompletedTime.Value,
                                ReceivedTime = mas.ReceivedTime,
                                MaterialRefId = det.MaterialRefId,
                                MaterialTypeRefId = mat.MaterialTypeId,
                                MaterialTypeRefName = mat.MaterialTypeId == 0 ? rawstring : semistring,
                                MaterialPetName = mat.MaterialPetName,
                                MaterialRefName = mat.MaterialName,
                                TotalQty = det.IssueQty,
                                Price = det.Price,
                                TotalAmount = det.IssueValue,
                                CurrentStatus = det.CurrentStatus,
                                DefaultUnitId = mat.DefaultUnitId,
                                DefaultUnitName = unit.Name,
                                Uom = unit.Name,
                                UnitRefId = det.UnitRefId,
                                UnitRefName = uiss.Name,
                                VehicleNumber = mas.VehicleNumber,
                                PersonInCharge = mas.PersonInCharge,
                                Remarks = det.RequestRemarks + " " + det.ApprovedRemarks
                            }).ToList();

            List<int> arrMaterialRefIds = allItems.Select(t => t.MaterialRefId).ToList();
            var materials = await _materialRepo.GetAllListAsync(t => arrMaterialRefIds.Contains(t.Id));

            foreach (var det in allItems)
            {
                var mat = materials.FirstOrDefault(t => t.Id == det.MaterialRefId);
                if (mat.DefaultUnitId == det.UnitRefId)
                    det.QuantityInPurchaseUnit = det.TotalQty;          //  Purchase Unit
                else if (mat.IssueUnitId == det.UnitRefId)
                    det.QuantityInIssueUnit = det.TotalQty;             // Recipe Unit
                else if (mat.StockAdjustmentUnitId == det.UnitRefId)
                    det.QuantityInStkAdjustedInventoryUnit = det.TotalQty;       // Inventory Unit
                else
                    det.QuantityInIssueUnit = det.TotalQty;             //  Except Default and Stock adj unit all units are as Recipe Unit
                output.TotalTransferAmount = output.TotalTransferAmount + det.TotalAmount;
            }

            output.DetailReport = allItems.OrderBy(t => t.MaterialRefId).ToList();

            output.TransferStatus = "Transfer Out";
            output.StartDate = input.StartDate.Value;
            output.EndDate = input.EndDate.Value;
            output.PrintedTime = DateTime.Now;
            output.PreparedUserId = AbpSession.UserId;
            string userName = "";
            var user = await UserManager.GetUserByIdAsync(AbpSession.UserId.Value);
            if (user != null)
                userName = user.Name;
            output.PreparedUserName = userName;
            output.PrintedTime = DateTime.Now;
            var customReport = await FeatureChecker.GetValueAsync(AppFeatures.Custom);
            bool customCrgReport = false;
            if (customReport.ToUpper() == "TRUE")
                customCrgReport = true;
            output.DoesCustomReportFormat = customCrgReport;
            var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
            output.LocationInfo = location.MapTo<LocationListDto>();


            var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();

            var groupItems = (from mas in allItems
                              join uc in rsUc
                              on mas.UnitRefId equals uc.BaseUnitId
                              where uc.RefUnitId == mas.DefaultUnitId
                              group mas by new
                              {
                                  mas.MaterialTypeRefId,
                                  mas.MaterialTypeRefName,
                                  mas.MaterialRefId,
                                  mas.MaterialRefName,
                                  mas.Uom,
                                  mas.DefaultUnitName,
                                  uc.Conversion
                              } into g
                              select new InterTransferReportDetailOutPut
                              {
                                  MaterialTypeRefId = g.Key.MaterialTypeRefId,
                                  MaterialTypeRefName = g.Key.MaterialTypeRefName,
                                  MaterialRefId = g.Key.MaterialRefId,
                                  MaterialRefName = g.Key.MaterialRefName,
                                  Uom = g.Key.DefaultUnitName,
                                  DefaultUnitName = g.Key.DefaultUnitName,
                                  TotalQty = g.Sum(t => t.TotalQty * g.Key.Conversion),
                                  TotalAmount = g.Sum(t => t.TotalAmount * g.Key.Conversion),
                                  Price = g.Sum(t => t.TotalAmount) == 0 ? 0 : g.Sum(t => t.TotalAmount * g.Key.Conversion) / g.Sum(t => t.TotalQty * g.Key.Conversion)
                              }).ToList();

            groupItems = (from mas in groupItems
                          group mas by new
                          {
                              mas.MaterialTypeRefId,
                              mas.MaterialTypeRefName,
                              mas.MaterialRefId,
                              mas.MaterialRefName,
                              mas.Uom,
                              mas.DefaultUnitName,
                          } into g
                          select new InterTransferReportDetailOutPut
                          {
                              MaterialTypeRefId = g.Key.MaterialTypeRefId,
                              MaterialTypeRefName = g.Key.MaterialTypeRefName,
                              MaterialRefId = g.Key.MaterialRefId,
                              MaterialRefName = g.Key.MaterialRefName,
                              Uom = g.Key.Uom,
                              DefaultUnitName = g.Key.DefaultUnitName,
                              TotalQty = g.Sum(t => t.TotalQty),
                              TotalAmount = g.Sum(t => t.TotalAmount),
                              Price = Math.Round(g.Sum(t => t.TotalAmount) == 0 ? 0 : g.Sum(t => t.TotalAmount) / g.Sum(t => t.TotalQty), 3)
                          }).ToList();

            groupItems = groupItems.OrderBy(t => t.MaterialRefId).ToList();

            output.ConsolidatedReport = groupItems;

            return output;
        }

        public async Task<InterTransferReportDto> GetTransferReceivedDetailReport(InputInterTransferReport input)
        {
            if (input.ToLocations == null)
            {
                throw new UserFriendlyException("LocationErr");
            }

            int[] ToLocIds = input.ToLocations.Select(t => t.Id).ToArray();

            var rsMasterQb = _intertransferRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId);

            rsMasterQb = rsMasterQb.WhereIf(ToLocIds.Count() > 0, t => ToLocIds.Contains(t.RequestLocationRefId));

            if (input.ExactSearchFlag)
                rsMasterQb = rsMasterQb.WhereIf(!input.TransferNumber.IsNullOrEmpty(), t => t.Id.ToString().Equals(input.TransferNumber));
            else
                rsMasterQb = rsMasterQb.WhereIf(!input.TransferNumber.IsNullOrEmpty(), t => t.Id.ToString().Contains(input.TransferNumber));

            if (input.StartDate.Value != null && input.EndDate.Value != null)
            {
                rsMasterQb =
                    rsMasterQb.Where(
                        a => DbFunctions.TruncateTime(a.ReceivedTime) >= DbFunctions.TruncateTime(input.StartDate)
                        && DbFunctions.TruncateTime(a.ReceivedTime) <= DbFunctions.TruncateTime(input.EndDate));
            }

            var rsDetailQb = _intertransferReceivedDetailRepo.GetAll().Where(t => t.ReceivedQty > 0);
            var rsMaterialQb = _materialRepo.GetAll();

            bool MaterialExists = false;


            if (input.MaterialList != null && input.MaterialList.Count > 0)
            {
                MaterialExists = true;
                int[] materialListIds = input.MaterialList.Select(t => t.Id).ToArray();
                rsDetailQb = rsDetailQb.WhereIf(MaterialExists, t => materialListIds.Contains(t.MaterialRefId));
                rsMaterialQb = rsMaterialQb.WhereIf(MaterialExists, t => materialListIds.Contains(t.Id));
            }

            if (input.MaterialTypeList != null && input.MaterialTypeList.Count > 0)
            {
                var stridList = input.MaterialTypeList.Select(t => t.Value).ToArray();
                int[] materialTypeIdList = Array.ConvertAll(stridList, s => int.Parse(s));
                rsMaterialQb = rsMaterialQb.Where(t => materialTypeIdList.Contains(t.MaterialTypeId));
            }

            string rawstring = L("RAW");
            string semistring = L("SEMI");

            InterTransferReportDto output = new InterTransferReportDto();

            var allItems = (from mas in rsMasterQb
                            join det in rsDetailQb on mas.Id equals det.InterTransferRefId
                            join rdet in _intertransferDetailRepo.GetAll() on mas.Id equals rdet.InterTransferRefId
                            join mat in rsMaterialQb on det.MaterialRefId equals mat.Id
                            join unit in _unitRepo.GetAll() on mat.DefaultUnitId equals unit.Id
                            join uiss in _unitRepo.GetAll() on det.UnitRefId equals uiss.Id
                            join loc in _locationRepo.GetAll() on mas.RequestLocationRefId equals loc.Id
                            select new InterTransferReportDetailOutPut
                            {
                                TransferRefId = mas.Id,
                                LocationRefId = mas.RequestLocationRefId,
                                LocationRefName = loc.Name,
                                TransferDate = mas.CompletedTime.Value,
                                ReceivedTime = mas.ReceivedTime,
                                MaterialRefId = det.MaterialRefId,
                                MaterialTypeRefId = mat.MaterialTypeId,
                                MaterialTypeRefName = mat.MaterialTypeId == 0 ? rawstring : semistring,
                                MaterialPetName = mat.MaterialPetName,
                                MaterialRefName = mat.MaterialName,
                                TotalQty = det.ReceivedQty,
                                Price = det.Price,
                                TotalAmount = det.ReceivedValue,
                                CurrentStatus = det.CurrentStatus,
                                DefaultUnitId = mat.DefaultUnitId,
                                DefaultUnitName = unit.Name,
                                Uom = unit.Name,
                                UnitRefId = det.UnitRefId,
                                UnitRefName = uiss.Name,
                                VehicleNumber = mas.VehicleNumber,
                                PersonInCharge = mas.PersonInCharge,
                                Remarks = rdet.RequestRemarks + " " + rdet.ApprovedRemarks
                            }).ToList();

            List<int> arrMaterialRefIds = allItems.Select(t => t.MaterialRefId).ToList();
            var materials = await _materialRepo.GetAllListAsync(t => arrMaterialRefIds.Contains(t.Id));

            foreach (var det in allItems)
            {
                var mat = materials.FirstOrDefault(t => t.Id == det.MaterialRefId);
                if (mat.DefaultUnitId == det.UnitRefId)
                    det.QuantityInPurchaseUnit = det.TotalQty;          //  Purchase Unit
                else if (mat.IssueUnitId == det.UnitRefId)
                    det.QuantityInIssueUnit = det.TotalQty;             // Recipe Unit
                else if (mat.StockAdjustmentUnitId == det.UnitRefId)
                    det.QuantityInStkAdjustedInventoryUnit = det.TotalQty;       // Inventory Unit
                else
                    det.QuantityInIssueUnit = det.TotalQty;             //  Except Default and Stock adj unit all units are as Recipe Unit
                output.TotalTransferAmount = output.TotalTransferAmount + det.TotalAmount;
            }

            output.DetailReport = allItems.OrderBy(t => t.MaterialRefId).ToList();


            output.TransferStatus = "Transfer In";
            output.StartDate = input.StartDate.Value;
            output.EndDate = input.EndDate.Value;
            output.PrintedTime = DateTime.Now;
            output.PreparedUserId = AbpSession.UserId;
            string userName = "";
            var user = await UserManager.GetUserByIdAsync(AbpSession.UserId.Value);
            if (user != null)
                userName = user.Name;
            output.PreparedUserName = userName;
            output.PrintedTime = DateTime.Now;
            var customReport = await FeatureChecker.GetValueAsync(AppFeatures.Custom);
            bool customCrgReport = false;
            if (customReport.ToUpper() == "TRUE")
                customCrgReport = true;
            customCrgReport = true;
            output.DoesCustomReportFormat = customCrgReport;
            var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
            output.LocationInfo = location.MapTo<LocationListDto>();


            var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();

            var groupItems = (from mas in allItems
                              join uc in rsUc
                              on mas.UnitRefId equals uc.BaseUnitId
                              where uc.RefUnitId == mas.DefaultUnitId
                              group mas by new
                              {
                                  mas.MaterialTypeRefId,
                                  mas.MaterialTypeRefName,
                                  mas.MaterialRefId,
                                  mas.MaterialRefName,
                                  mas.Uom,
                                  mas.DefaultUnitName,
                                  uc.Conversion
                              } into g
                              select new InterTransferReportDetailOutPut
                              {
                                  MaterialTypeRefId = g.Key.MaterialTypeRefId,
                                  MaterialTypeRefName = g.Key.MaterialTypeRefName,
                                  MaterialRefId = g.Key.MaterialRefId,
                                  MaterialRefName = g.Key.MaterialRefName,
                                  Uom = g.Key.DefaultUnitName,
                                  DefaultUnitName = g.Key.DefaultUnitName,
                                  TotalQty = g.Sum(t => t.TotalQty * g.Key.Conversion),
                                  TotalAmount = g.Sum(t => t.TotalAmount * g.Key.Conversion),
                                  Price = g.Sum(t => t.TotalAmount) == 0 ? 0 : g.Sum(t => t.TotalAmount * g.Key.Conversion) / g.Sum(t => t.TotalQty * g.Key.Conversion)
                              }).ToList();

            groupItems = (from mas in groupItems
                          group mas by new
                          {
                              mas.MaterialTypeRefId,
                              mas.MaterialTypeRefName,
                              mas.MaterialRefId,
                              mas.MaterialRefName,
                              mas.Uom,
                              mas.DefaultUnitName,
                          } into g
                          select new InterTransferReportDetailOutPut
                          {
                              MaterialTypeRefId = g.Key.MaterialTypeRefId,
                              MaterialTypeRefName = g.Key.MaterialTypeRefName,
                              MaterialRefId = g.Key.MaterialRefId,
                              MaterialRefName = g.Key.MaterialRefName,
                              Uom = g.Key.Uom,
                              DefaultUnitName = g.Key.DefaultUnitName,
                              TotalQty = g.Sum(t => t.TotalQty),
                              TotalAmount = g.Sum(t => t.TotalAmount),
                              Price = Math.Round(g.Sum(t => t.TotalAmount) == 0 ? 0 : g.Sum(t => t.TotalAmount) / g.Sum(t => t.TotalQty), 3)
                          }).ToList();

            groupItems = groupItems.OrderBy(t => t.MaterialRefId).ToList();

            output.ConsolidatedReport = groupItems;

            return output;
        }


        public async Task SendTransferRequestEmail(SetInterTransferPrint input)
        {
            GetInterTransferForEditOutput output = await _interTransferRequestAppService.GetInterTransferForEdit(new NullableIdInput { Id = input.InterTransferId });

            InterTransferEditDto mas;

            if (output.InterTransfer.Id.HasValue)
            {
                mas = output.InterTransfer;
            }
            else
            {
                return;
            }

            var billingLocation = await _locationRepo.FirstOrDefaultAsync(t => t.Id == mas.LocationRefId);
            string billingLocationName = billingLocation.Name;
            string billingLocationAddress1 = billingLocation.Address1;
            string billingLocationAddress2 = billingLocation.Address2;
            string billingLocationAddress3 = billingLocation.Address3;
            string billingLocationCity = billingLocation.City;
            string billingLocationState = billingLocation.State;
            string billingLocationCountry = billingLocation.Country;

            var shippingLocation = await _locationRepo.FirstOrDefaultAsync(t => t.Id == mas.RequestLocationRefId);

            string shippingLocationName = "";
            string shippingLocationAddress1 = "";
            string shippingLocationAddress2 = "";
            string shippingLocationAddress3 = "";
            string shippingLocationCity = "";
            string shippingLocationState = "";
            string shippingLocationCountry = "";

            string emailToSend = shippingLocation.Email;

            if (emailToSend == null || emailToSend.Length == 0)
            {
                throw new UserFriendlyException(L("EmailNotExistForRequestLocation", shippingLocation.Name));
            }

            shippingLocationName = shippingLocation.Name;
            shippingLocationAddress1 = shippingLocation.Address1;
            shippingLocationAddress2 = shippingLocation.Address2;
            shippingLocationAddress3 = shippingLocation.Address3;
            shippingLocationCity = shippingLocation.City;
            shippingLocationState = shippingLocation.State;
            shippingLocationCountry = shippingLocation.Country;

            var companyInfo = await _companyRepo.FirstOrDefaultAsync(t => t.Id == billingLocation.CompanyRefId);
            string companyName = companyInfo.Name;
            string companyAddress1 = companyInfo.Address1;
            string companyAddress2 = companyInfo.Address2;
            string companyAddress3 = companyInfo.Address3;
            string companyCity = companyInfo.City;

            var mailMessage = new StringBuilder();

            string htmlLine;
            htmlLine = "<!DOCTYPE HTML PUBLIC \" -//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">";
            mailMessage.AppendLine(htmlLine);
            htmlLine = "<html style=\"-webkit-text-size-adjust: none;-ms-text-size-adjust: none;\">";
            mailMessage.AppendLine(htmlLine);
            htmlLine = "<head>";
            mailMessage.AppendLine(htmlLine);
            htmlLine = "<meta http-equiv=\"Content - Type\" content=\"text / html; charset = utf - 8\">";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<title>" + companyName + L("Request") + "</title>";
            mailMessage.AppendLine(htmlLine);
            htmlLine = "</head>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<body style=\"padding: 0px; margin: 0px; \">";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<div id=\"mailsub\" class=\"notification\">";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<center>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<h1>" + companyName + "</h1>";
            mailMessage.AppendLine(htmlLine);

            if (!companyAddress1.IsNullOrEmpty())
            {
                htmlLine = "<p>" + companyAddress1 + "</p>";
                mailMessage.AppendLine(htmlLine);
            }

            if (!companyAddress2.IsNullOrEmpty())
            {
                htmlLine = "<p>" + companyAddress2 + "</p>";
                mailMessage.AppendLine(htmlLine);
            }

            if (!companyAddress3.IsNullOrEmpty())
            {
                htmlLine = "<p>" + companyAddress3 + "</p>";
                mailMessage.AppendLine(htmlLine);
            }

            if (!companyCity.IsNullOrEmpty())
            {
                htmlLine = "<p>" + companyCity + "</p>";
                mailMessage.AppendLine(htmlLine);
            }

            htmlLine = "</center>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<hr />";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<h2 style=\"text-align: center;\">" + L("Request") + "</h2>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<table id=\"header\" style=\"width: 100%;\">";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<tbody>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<tr>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<td><strong><strong>" + L("Ref#") + ": " + " </strong></strong><strong>" + mas.Id.ToString() + "</strong></td>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<td style=\"text-align: right;\"><strong>" + L("Date") + ": " + mas.RequestDate.ToString("dd-MMM-yy") + "</strong></td>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "</tr>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "</tbody>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "</table>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<hr/>";
            mailMessage.AppendLine(htmlLine);


            //htmlLine = "<h3><span style=\"text-decoration: underline;\"><strong>" + L("Supplier") + "</strong></span></h3>";
            //mailMessage.AppendLine(htmlLine);

            //htmlLine = "<p>" + supplierName + "</p>";
            //mailMessage.AppendLine(htmlLine);

            //if (!supplierAddress1.IsNullOrEmpty())
            //{
            //    htmlLine = "<p>" + supplierAddress1 + "</p>";
            //    mailMessage.AppendLine(htmlLine);
            //}

            //if (!supplierAddress2.IsNullOrEmpty())
            //{
            //    htmlLine = "<p>" + supplierAddress2 + "</p>";
            //    mailMessage.AppendLine(htmlLine);
            //}

            //if (!supplierAddress3.IsNullOrEmpty())
            //{
            //    htmlLine = "<p>" + supplierAddress3 + "</p>";
            //    mailMessage.AppendLine(htmlLine);
            //}

            //if (!supplierCity.IsNullOrEmpty())
            //{
            //    htmlLine = "<p>" + supplierCity + "</p>";
            //    mailMessage.AppendLine(htmlLine);
            //}

            //if (!supplierState.IsNullOrEmpty())
            //{
            //    htmlLine = "<p>" + supplierState + "</p>";
            //    mailMessage.AppendLine(htmlLine);
            //}

            //if (!supplierCountry.IsNullOrEmpty())
            //{
            //    htmlLine = "<p>" + supplierCountry + "</p>";
            //    mailMessage.AppendLine(htmlLine);
            //}

            //htmlLine = "<hr />";
            //mailMessage.AppendLine(htmlLine);

            htmlLine = "<table id=\"address\" width=\"100%\">";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<tbody>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<tr>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<td><strong><u>" + L("Location") + "</u></strong></td>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<td><strong><u>" + L("RequestLocation") + "</u></strong></td>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "</tr>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<tr>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<td>" + billingLocationName + "</td>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<td>" + shippingLocationName + "</td>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "</tr>";
            mailMessage.AppendLine(htmlLine);

            if (billingLocationAddress1.IsNullOrEmpty() == false || shippingLocationAddress1.IsNullOrEmpty() == false)
            {
                htmlLine = "<tr>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td>" + billingLocationAddress1 + " </td>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td>" + shippingLocationAddress1 + "</td>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "</tr>";
                mailMessage.AppendLine(htmlLine);
            }


            if (billingLocationAddress2.IsNullOrEmpty() == false || shippingLocationAddress2.IsNullOrEmpty() == false)
            {
                htmlLine = "<tr>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td>" + billingLocationAddress2 + " </td>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td>" + shippingLocationAddress2 + "</td>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "</tr>";
                mailMessage.AppendLine(htmlLine);
            }


            if (billingLocationAddress3.IsNullOrEmpty() == false || shippingLocationAddress3.IsNullOrEmpty() == false)
            {
                htmlLine = "<tr>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td>" + billingLocationAddress3 + " </td>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td>" + shippingLocationAddress2 + "</td>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "</tr>";
                mailMessage.AppendLine(htmlLine);
            }


            if (billingLocationCity.IsNullOrEmpty() == false || shippingLocationCity.IsNullOrEmpty() == false)
            {
                htmlLine = "<tr>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td>" + billingLocationCity + " </td>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td>" + shippingLocationCity + "</td>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "</tr>";
                mailMessage.AppendLine(htmlLine);
            }


            if (billingLocationState.IsNullOrEmpty() == false || shippingLocationState.IsNullOrEmpty() == false)
            {
                htmlLine = "<tr>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td>" + billingLocationState + " </td>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td>" + shippingLocationState + "</td>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "</tr>";
                mailMessage.AppendLine(htmlLine);
            }



            htmlLine = "</tbody>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "</table>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<hr />";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<p>" + L("RequestHeader") + " </p>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<table id=\"detail\" cellpadding=\"7\" border=\"1\" width=\"100%\">";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<tbody>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<tr>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<td>#.Material</td>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<td style=\"text-align: right;\">Qty</td>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<td style=\"text-align: right;\">UOM</td>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<td style=\"text-align: right;\">Remarks</td>";
            mailMessage.AppendLine(htmlLine);



            htmlLine = "</tr>";
            mailMessage.AppendLine(htmlLine);

            int sno = 1;

            foreach (var poDet in output.InterTransferDetail)
            {
                htmlLine = "<tr>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td>" + sno.ToString().PadLeft(2) + "." + poDet.MaterialRefName + "</td>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td style=\"text-align: right;\">" + poDet.RequestQty + "</td>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td style=\"text-align: right;\">" + poDet.Uom + "</td>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td>" + poDet.RequestRemarks + "</td>";
                mailMessage.AppendLine(htmlLine);

                //htmlLine = "<td style=\"text-align: right;\">" + poDet.TotalAmount.ToString("###0.00") + "</td>";
                //mailMessage.AppendLine(htmlLine);

                //htmlLine = "<td style=\"text-align: right;\">" + poDet.TaxAmount.ToString("###0.00") + "</td>";
                //mailMessage.AppendLine(htmlLine);

                //htmlLine = "<td style=\"text-align: right;\">" + poDet.NetAmount.ToString("###0.00") + "</td>";
                //mailMessage.AppendLine(htmlLine);

                htmlLine = "</tr>";
                mailMessage.AppendLine(htmlLine);

                sno++;
            }

            //htmlLine = "<tr>";
            //mailMessage.AppendLine(htmlLine);

            //htmlLine = "</tr>";
            //mailMessage.AppendLine(htmlLine);

            htmlLine = "</tbody>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "</table>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<hr />";
            mailMessage.AppendLine(htmlLine);

            //htmlLine = "<h3><strong>" + L("TermsConditions") + "</strong></h3>";
            //mailMessage.AppendLine(htmlLine);

            //htmlLine = "<ul style=\"list - style - type: circle; \">";
            //mailMessage.AppendLine(htmlLine);

            //htmlLine = "<li> <b>" + L("TermDelivery", mas.DeliveryDateExpected.ToString("dd-MMM-yy")) + "</b></li>";
            //mailMessage.AppendLine(htmlLine);

            //htmlLine = "<li>" + L("Term1") + "</li>";
            //mailMessage.AppendLine(htmlLine);

            //htmlLine = "<li>" + L("Term2") + "</li>";
            //mailMessage.AppendLine(htmlLine);

            //htmlLine = "<li>" + L("Term3") + "</li>";
            //mailMessage.AppendLine(htmlLine);

            htmlLine = "</ul>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<hr />";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<p><span style=\"color: #0000ff;\">" + L("SignNotRequired") + "</span></p>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "</div>";    //mailsub div end
            mailMessage.AppendLine(htmlLine);
            htmlLine = "</body>";
            mailMessage.AppendLine(htmlLine);
            htmlLine = "</html>";
            mailMessage.AppendLine(htmlLine);

            try
            {
                await _emailSender.SendAsync(emailToSend, companyName + " - " + L("TransferRequest"), mailMessage.ToString());
            }
            catch (Exception ex)
            {

                throw new UserFriendlyException(ex.Message + " " + ex.InnerException);
            }
        }

        public async Task<List<MaterialPoProjectionViewDto>> GetAutoRequestProjection(InputPurchaseProjection input)
        {
            var allItems = (from matgroupCat in _materialGroupCategoryRepo.GetAll()
                            join matgroup in _materialgroupRepo.GetAll().Where(t => t.TenantId == AbpSession.TenantId)
                            on matgroupCat.MaterialGroupRefId equals matgroup.Id
                            join mat in _materialRepo.GetAll().Where(t => t.OwnPreparation == false)
                                .WhereIf(input.IsHighValueItemOnly, t => t.IsHighValueItem == true)
                            on matgroupCat.Id equals mat.MaterialGroupCategoryRefId
                            join unit in _unitRepo.GetAll()
                            on mat.DefaultUnitId equals unit.Id
                            join matloc in _materiallocationwisestockRepo.GetAll()
                                .Where(mls => mls.LocationRefId == input.LocationRefId) on mat.Id equals matloc.MaterialRefId
                            select new MaterialPoProjectionViewDto
                            {
                                Barcode = mat.Barcode == null ? mat.MaterialName : mat.Barcode,
                                MaterialRefId = mat.Id,
                                MaterialTypeId = mat.MaterialTypeId,
                                MaterialTypeName = "",
                                MaterialName = mat.MaterialName,
                                MaterialPetName = mat.MaterialPetName,
                                MaterialGroupCategoryName = matgroupCat.MaterialGroupCategoryName,
                                UserSerialNumber = mat.UserSerialNumber,
                                WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                                OnHand = matloc.CurrentInHand,
                                UnitRefId = unit.Id,
                                Uom = unit.Name,
                                AlreadyOrderedQuantity = 0,
                            });

            List<MaterialPoProjectionViewDto> sortMenuItems;

            sortMenuItems = await allItems.ToListAsync();

            //var rsSupplier = await _supplierRepo.GetAllListAsync();

            //var rsMaterialLocationStock = await _materiallocationwisestockRepo.GetAllListAsync(t => t.LocationRefId == input.LocationRefId);

            var dt = await _materiallocationwisestockAppService.GetView(new GetMaterialLocationWiseStockInput
            {
                LocationRefId = input.LocationRefId,
                LiveStock = true,
                MaxResultCount = AppConsts.MaxPageSize
            });

            List<MaterialLocationWiseStockViewDto> rsMaterialLocationStock = dt.Items.MapTo<List<MaterialLocationWiseStockViewDto>>();

            foreach (var lst in sortMenuItems)
            {
                lst.OmitQueueInOrder = input.OmitQueueInOrder;
                var matstock = rsMaterialLocationStock.FirstOrDefault(t => t.MaterialRefId == lst.MaterialRefId);

                if (matstock == null)
                {
                    throw new UserFriendlyException(L("MaterialNotExistInStockLedger", lst.MaterialName));
                }
                lst.OnHand = matstock.LiveStock;

                if (lst.OnHand < 0)
                {
                    int i = 10;
                }

                string materialTypeName = lst.MaterialTypeId == (int)MaterialType.RAW ? L("RAW") : L("SEMI");

                lst.MaterialTypeName = materialTypeName;

                DateTime fromDate = input.StartDate.Date;
                DateTime toDate = input.EndDate.Date;
                TimeSpan diffBetweenDays = toDate - fromDate;
                double numberofDays = diffBetweenDays.TotalDays;
                if (numberofDays == 0)
                    numberofDays = 1;

                var alreadyRequest = await (from requestDet in _intertransferDetailRepo
                                            .GetAll().Where(t => t.MaterialRefId == lst.MaterialRefId)
                                            join requestMas in _intertransferRepo.GetAll()
                                            .Where(t => t.LocationRefId == input.LocationRefId && t.CurrentStatus == "P" || t.CurrentStatus == "A" || t.CurrentStatus == "D")
                                            on requestDet.InterTransferRefId equals requestMas.Id
                                            select requestDet).ToListAsync();

                if (alreadyRequest == null || alreadyRequest.Count == 0)
                {
                    lst.AlreadyOrderedQuantity = 0;
                }
                else
                {
                    lst.AlreadyOrderedQuantity = Math.Round(alreadyRequest.Sum(t => t.RequestQty - t.IssueQty), 6);
                }

                var onHand = lst.OnHand;
                if (onHand <= 0)
                    onHand = 0;

                if (input.RequestQtyBasedOn.Equals("AVGCONSUMPTION"))
                {
                    var matLedger = await _materialLedgerRepo.GetAll().Where(a => a.MaterialRefId == lst.MaterialRefId && a.LocationRefId == input.LocationRefId && (DbFunctions.TruncateTime(a.LedgerDate) >= fromDate.Date && DbFunctions.TruncateTime(a.LedgerDate) <= toDate)).ToListAsync();

                    lst.LastNDaysConsumption = 0;
                    lst.PerDayConsumption = 0;

                    if (matLedger.Count > 0)
                    {
                        lst.LastNDaysConsumption = ((matLedger.Sum(a => a.Issued + a.Sales + a.TransferOut) - matLedger.Sum(a => a.Return + a.TransferIn)));
                        if (lst.LastNDaysConsumption < 0)
                        {
                            lst.LastNDaysConsumption = 0;
                            lst.PerDayConsumption = 0;
                        }
                        else if (lst.LastNDaysConsumption > 0)
                        {
                            lst.PerDayConsumption = Math.Round(((matLedger.Sum(a => a.Issued + a.Sales + a.TransferOut) - matLedger.Sum(a => a.Return + a.TransferIn)) / (decimal)numberofDays), 6);
                        }
                    }

                    if (lst.PerDayConsumption > 0)
                    {
                        lst.NextNDaysProjectedConsumption = Math.Ceiling(input.NextNDays * lst.PerDayConsumption);

                        lst.NextNDaysRoundedProjection = Math.Ceiling(lst.NextNDaysProjectedConsumption - (lst.OmitQueueInOrder == true ? 0 : lst.AlreadyOrderedQuantity) - (onHand));


                        if (lst.NextNDaysRoundedProjection < 0)
                            lst.NextNDaysRoundedProjection = 0;
                    }

                    if (lst.NextNDaysRoundedProjection <= 0)
                    {
                        if (matstock != null)
                        {
                            if (matstock.ReOrderLevel > 0 && matstock.ReOrderLevel > (onHand + (lst.OmitQueueInOrder == true ? 0 : lst.AlreadyOrderedQuantity)))
                            {
                                if (matstock.MaximumStock > 0)
                                    lst.NextNDaysRoundedProjection = matstock.MaximumStock - (onHand + (lst.OmitQueueInOrder == true ? 0 : lst.AlreadyOrderedQuantity));
                                else
                                    lst.NextNDaysRoundedProjection = matstock.ReOrderLevel - (onHand + (lst.OmitQueueInOrder == true ? 0 : lst.AlreadyOrderedQuantity));
                            }
                        }
                    }
                }
                else
                {
                    if (matstock != null)
                    {
                        if (matstock.ReOrderLevel > 0 && matstock.ReOrderLevel > (lst.OnHand + (lst.OmitQueueInOrder == true ? 0 : lst.AlreadyOrderedQuantity)))
                        {
                            if (matstock.MaximumStock > 0)
                                lst.NextNDaysRoundedProjection = matstock.MaximumStock - (onHand + (lst.OmitQueueInOrder == true ? 0 : lst.AlreadyOrderedQuantity));
                            else
                                lst.NextNDaysRoundedProjection = matstock.ReOrderLevel - (onHand + (lst.OmitQueueInOrder == true ? 0 : lst.AlreadyOrderedQuantity));
                        }
                    }
                }
            }

            var allListDtos = sortMenuItems.MapTo<List<MaterialPoProjectionViewDto>>();

            allListDtos = allListDtos.Where(t => t.NextNDaysRoundedProjection > 0).OrderByDescending(t => t.NextNDaysRoundedProjection).ToList();
            var allItemCount = await allItems.CountAsync();

            return allListDtos;
        }

        public async Task<List<StatusConsolidatedDto>> GetTransferStatusDetailReport(InputInterTransferReport input)
        {
            if (input.ToLocations == null)
            {
                throw new UserFriendlyException("LocationErr");
            }

            int[] ToLocIds = input.ToLocations.Select(t => t.Id).ToArray();

            var rsMasterQb = _intertransferRepo.GetAll().Where(t => t.RequestLocationRefId == input.LocationRefId);

            rsMasterQb = rsMasterQb.WhereIf(ToLocIds.Count() > 0, t => ToLocIds.Contains(t.LocationRefId));

            if (input.ExactSearchFlag)
                rsMasterQb = rsMasterQb.WhereIf(!input.TransferNumber.IsNullOrEmpty(), t => t.Id.ToString().Equals(input.TransferNumber));
            else
                rsMasterQb = rsMasterQb.WhereIf(!input.TransferNumber.IsNullOrEmpty(), t => t.Id.ToString().Contains(input.TransferNumber));

            if (input.StartDate.Value != null && input.EndDate.Value != null)
            {
                rsMasterQb =
                    rsMasterQb.Where(
                        a => DbFunctions.TruncateTime(a.CompletedTime) >= DbFunctions.TruncateTime(input.StartDate)
                        && DbFunctions.TruncateTime(a.CompletedTime) <= DbFunctions.TruncateTime(input.EndDate));
            }

            var rsDetailQb = _intertransferDetailRepo.GetAll().Where(t => t.IssueQty > 0);
            string[] arrStatusList = { "A", "V", "I", "E", "S" }; // ARPES
                                                                  //rsDetailQb = rsDetailQb.Where(t => arrStatusList.Contains(t.CurrentStatus));
            var rsMaterialQb = _materialRepo.GetAll();

            bool MaterialExists = false;


            if (input.MaterialList != null && input.MaterialList.Count > 0)
            {
                MaterialExists = true;
                int[] materialListIds = input.MaterialList.Select(t => t.Id).ToArray();
                rsDetailQb = rsDetailQb.WhereIf(MaterialExists, t => materialListIds.Contains(t.MaterialRefId));
                rsMaterialQb = rsMaterialQb.WhereIf(MaterialExists, t => materialListIds.Contains(t.Id));
            }

            if (input.MaterialTypeList != null && input.MaterialTypeList.Count > 0)
            {
                var stridList = input.MaterialTypeList.Select(t => t.Value).ToArray();
                int[] materialTypeIdList = Array.ConvertAll(stridList, s => int.Parse(s));
                rsMaterialQb = rsMaterialQb.Where(t => materialTypeIdList.Contains(t.MaterialTypeId));
            }

            string rawstring = L("RAW");
            string semistring = L("SEMI");

            List<InterTransferStatusReportDto> output = new List<InterTransferStatusReportDto>();

            var allItems = await (from mas in rsMasterQb
                                  join det in rsDetailQb on mas.Id equals det.InterTransferRefId
                                  join mat in rsMaterialQb on det.MaterialRefId equals mat.Id
                                  join unit in _unitRepo.GetAll() on mat.DefaultUnitId equals unit.Id
                                  join uiss in _unitRepo.GetAll() on det.UnitRefId equals uiss.Id
                                  join fromLoc in _locationRepo.GetAll() on mas.RequestLocationRefId equals fromLoc.Id
                                  join toLoc in _locationRepo.GetAll() on mas.LocationRefId equals toLoc.Id
                                  select new InterTransferStatusReportDto
                                  {
                                      TransferRefId = mas.Id,
                                      TransferDate = mas.CompletedTime.Value,
                                      MaterialRefId = det.MaterialRefId,
                                      MaterialTypeRefId = mat.MaterialTypeId,
                                      MaterialTypeRefName = mat.MaterialTypeId == 0 ? rawstring : semistring,
                                      MaterialPetName = mat.MaterialPetName,
                                      MaterialRefName = mat.MaterialName,
                                      IssuedQty = det.IssueQty,
                                      Price = det.Price,
                                      TotalAmount = det.IssueValue,
                                      CurrentStatus = det.CurrentStatus,
                                      DefaultUnitId = mat.DefaultUnitId,
                                      DefaultUnitName = unit.Name,
                                      UnitRefId = det.UnitRefId,
                                      UnitRefName = uiss.Name,
                                      VehicleNumber = mas.VehicleNumber,
                                      PersonInCharge = mas.PersonInCharge,
                                      FromLocationRefId = fromLoc.Id,
                                      FromLocationRefName = fromLoc.Name,
                                      ToLocationRefId = toLoc.Id,
                                      ToLocationRefName = toLoc.Name,
                                  }).ToListAsync();

            output = allItems.OrderBy(t => t.MaterialRefId).ToList();
            int[] arrInterTransferRefIds = output.Select(t => t.TransferRefId).ToArray();

            var rsInterReceived = await _intertransferReceivedDetailRepo.GetAll().Where(t => arrInterTransferRefIds.Contains(t.InterTransferRefId)).ToListAsync();

            var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();

            foreach (var lst in output)
            {
                var rcdDto = rsInterReceived.FirstOrDefault(t => t.InterTransferRefId == lst.TransferRefId && t.MaterialRefId == lst.MaterialRefId);
                if (rcdDto == null)
                {
                    lst.ReceivedQty = 0;
                }
                else
                {
                    lst.ReceivedQty = rcdDto.ReceivedQty;
                }
                lst.DiffQty = lst.ReceivedQty - lst.IssuedQty;
                lst.DiffAmount = lst.DiffQty * lst.Price;
                if (lst.ReceivedQty == 0)
                    lst.TransferStatus = L("NotReceived");
                else if (lst.ReceivedQty == lst.IssuedQty)
                    lst.TransferStatus = L("Completed");
                else if (lst.ReceivedQty > lst.ReceivedQty)
                    lst.TransferStatus = L("Excess");
                else if (lst.ReceivedQty < lst.ReceivedQty)
                    lst.TransferStatus = L("Shortage");
            }

            var ro = (from mas in output
                      group mas by new { mas.FromLocationRefId, mas.FromLocationRefName, mas.ToLocationRefId, mas.ToLocationRefName, mas.TransferRefId, mas.TransferDate } into g
                      select new StatusConsolidatedDto
                      {
                          FromLocationRefId = g.Key.FromLocationRefId,
                          FromLocationRefName = g.Key.FromLocationRefName,
                          ToLocationRefId = g.Key.ToLocationRefId,
                          ToLocationRefName = g.Key.ToLocationRefName,
                          TransferRefId = g.Key.TransferRefId,
                          TransferDate = g.Key.TransferDate,
                          TransferList = g.ToList()
                      }).ToList();

            return ro;
        }

        public async Task<FileDto> GetTransferStatusDetailReportToExcel(InputInterTransferReport input)
        {

            var allList = await GetTransferStatusDetailReport(input);
            string userName = "";
            var user = await UserManager.GetUserByIdAsync(AbpSession.UserId.Value);
            if (user != null)
                userName = user.Name;

            return _intertransferExporter.TransferStatusExportToFile(allList, input.StartDate.Value, input.EndDate.Value, userName);
        }



        public async Task<FileDto> GetInterTransferReceivedConsolidatedReportGroupOrCategoryOrMaterialWiseForGivenInputToExcel(ClosingStockExportGroupCategoryMaterialDto input)
        {
            var result = await GetInterTransferReceivedConsolidatedReportGroupOrCategoryOrMaterialWiseForGivenInput(input);
            result.InputFilterDto = input;
            if (result.MaterialWisePurchaseDtos.Count == 0)
            {
                throw new UserFriendlyException(L("NoMoreRecordsFound"));
            }
            return _intertransferExporter.ExportInterTrasnferReceivedDetailConsolidatedGroupCategoryMaterials(result);
        }


        public async Task<LocationWiseClosingStockGroupCateogryMaterialDto> GetInterTransferReceivedConsolidatedReportGroupOrCategoryOrMaterialWiseForGivenInput(ClosingStockExportGroupCategoryMaterialDto input)
        {
            Debug.WriteLine("InterTransfer GetInterTransferReceivedConsolidatedReportGroupOrCategoryOrMaterialWiseForGivenInput() Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));

            var intertransferMaster = _intertransferRepo.GetAll();

            #region Date & Supplier Filter
            if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
            {
                intertransferMaster =
                    intertransferMaster.Where(
                        a => DbFunctions.TruncateTime(a.CompletedTime) >= DbFunctions.TruncateTime(input.StartDate)
                             && DbFunctions.TruncateTime(a.CompletedTime) <= DbFunctions.TruncateTime(input.EndDate));
            }

            #endregion

            #region Location Filter
            List<int> selectedLocationRefIds = new List<int>();
            if (input.Locations != null && input.Locations.Any())
            {
                selectedLocationRefIds = input.Locations.Select(a => a.Id).ToList();
            }
            else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
                     && !input.LocationGroup.Locations.Any())
            {
                selectedLocationRefIds = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
            }
            else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                selectedLocationRefIds = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
            }
            else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                selectedLocationRefIds = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true
                });
            }
            else if (input.LocationGroup == null)
            {
                if (input.UserId > 0)
                {
                    selectedLocationRefIds = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                    {
                        Locations = new List<SimpleLocationDto>(),
                        Group = false,
                        UserId = input.UserId
                    });
                }
            }
            intertransferMaster = intertransferMaster.Where(a => selectedLocationRefIds.Contains(a.LocationRefId));

            #endregion

            var intertransferRecivedDetail = _intertransferReceivedDetailRepo.GetAll();

            #region Group Category Material Filter
            List<int> arrGroupRefIds = new List<int>();
            if (input.GroupList.Count > 0)
            {
                arrGroupRefIds = input.GroupList.Select(t => int.Parse(t.Value)).ToList();
            }
            List<int> arrCategoryRefIds = new List<int>();
            if (input.CategoryList.Count > 0)
            {
                arrCategoryRefIds = input.CategoryList.Select(t => int.Parse(t.Value)).ToList();
            }
            List<int> arrMateriaRefIds = new List<int>();
            if (input.MaterialList.Count > 0)
            {
                arrMateriaRefIds = input.MaterialList.Select(t => int.Parse(t.Value)).ToList();
                input.ExportMaterialAlso = true;
            }
            #endregion

            var allItems = await (from interTrasnfer in intertransferMaster
                                  join recdDetail in intertransferRecivedDetail on interTrasnfer.Id equals recdDetail.InterTransferRefId
                                  join mat in _materialRepo.GetAll().WhereIf(arrMateriaRefIds.Count > 0, t => arrMateriaRefIds.Contains(t.Id))
                                      on recdDetail.MaterialRefId equals mat.Id
                                  join matCat in _materialGroupCategoryRepo.GetAll().WhereIf(arrCategoryRefIds.Count > 0, t => arrCategoryRefIds.Contains(t.Id))
                                      on mat.MaterialGroupCategoryRefId equals matCat.Id
                                  join matGrp in _materialgroupRepo.GetAll().WhereIf(arrGroupRefIds.Count > 0, t => arrGroupRefIds.Contains(t.Id))
                                      on matCat.MaterialGroupRefId equals matGrp.Id
                                  join loc in _locationRepo.GetAll() on interTrasnfer.LocationRefId equals loc.Id
                                  select new InterTransferReceivedGroupCategoryMaterialWiseAnalysisDto
                                  {
                                      LocationRefId = interTrasnfer.LocationRefId,
                                      LocationRefCode = loc.Code,
                                      LocationRefName = loc.Name,
                                      InterTrasnferRefId = interTrasnfer.Id,
                                      MaterialGroupRefId = matGrp.Id,
                                      MaterialGroupRefName = matGrp.MaterialGroupName,
                                      MaterialGroupCategoryRefId = matCat.Id,
                                      MaterialGroupCategoryRefName = matCat.MaterialGroupCategoryName,
                                      MaterialRefId = mat.Id,
                                      MaterialRefName = mat.MaterialName,
                                      MaterialPetName = mat.MaterialPetName,
                                      ReceivedQty = recdDetail.ReceivedQty,
                                      UnitRefId = recdDetail.UnitRefId,
                                      Price = recdDetail.Price,
                                      DefaultUnitId = mat.DefaultUnitId,
                                      TotalAmount = recdDetail.ReceivedValue,
                                  }).ToListAsync();

            List<LocationWiseHeadWiseStock> outputDto = new List<LocationWiseHeadWiseStock>();
            foreach (var gp in allItems.GroupBy(t => t.LocationRefId))
            {
                LocationWiseHeadWiseStock newSupplierDto = new LocationWiseHeadWiseStock
                {
                    LocationRefId = gp.Key,
                    LocationRefCode = gp.FirstOrDefault().LocationRefCode,
                    LocationRefName = gp.FirstOrDefault().LocationRefName
                };

                foreach (var sublst in gp.GroupBy(t => new { t.MaterialRefId, t.MaterialRefName }))
                {
                    MaterialWisePurchaseDto materialDto = new MaterialWisePurchaseDto
                    {
                        MaterialRefId = sublst.Key.MaterialRefId,
                        MaterialRefName = sublst.Key.MaterialRefName,
                        MaterialGroupRefId = gp.FirstOrDefault().MaterialGroupRefId,
                        MaterialGroupCategoryRefId = gp.FirstOrDefault().MaterialGroupCategoryRefId,
                        TotalAmount = sublst.Sum(t => t.TotalAmount)
                    };
                    newSupplierDto.MaterialWisePurchaseDtos.Add(materialDto);
                }

                foreach (var sublst in gp.GroupBy(t => new { t.MaterialGroupCategoryRefId, t.MaterialGroupCategoryRefName }))
                {
                    MaterialGroupCategoryWisePurchaseDto groupCategoryDto = new MaterialGroupCategoryWisePurchaseDto
                    {
                        MaterialGroupCategoryRefId = sublst.Key.MaterialGroupCategoryRefId,
                        MaterialGroupCategoryRefName = sublst.Key.MaterialGroupCategoryRefName,
                        TotalAmount = sublst.Sum(t => t.TotalAmount)
                    };
                    newSupplierDto.MaterialGroupCategoryWisePurchaseDtos.Add(groupCategoryDto);
                }

                foreach (var sublst in gp.GroupBy(t => new { t.MaterialGroupRefId, t.MaterialGroupRefName }))
                {
                    MaterialGroupWisePurchaseDto groupDto = new MaterialGroupWisePurchaseDto
                    {
                        MaterialGroupRefId = sublst.Key.MaterialGroupRefId,
                        MaterialGroupRefName = sublst.Key.MaterialGroupRefName,
                        TotalAmount = sublst.Sum(t => t.TotalAmount)
                    };
                    newSupplierDto.MaterialGroupWisePurchaseDtos.Add(groupDto);
                }
                outputDto.Add(newSupplierDto);
            }

            LocationWiseClosingStockGroupCateogryMaterialDto output = new LocationWiseClosingStockGroupCateogryMaterialDto();
            output.LocationWiseHeadWiseStockList = outputDto;

            foreach (var sublst in allItems.GroupBy(t => new { t.MaterialRefId, t.MaterialRefName }))
            {
                MaterialWisePurchaseDto materialDto = new MaterialWisePurchaseDto
                {
                    MaterialRefId = sublst.Key.MaterialRefId,
                    MaterialRefName = sublst.Key.MaterialRefName,
                    TotalAmount = sublst.Sum(t => t.TotalAmount)
                };
                output.MaterialWisePurchaseDtos.Add(materialDto);
            }

            foreach (var sublst in allItems.GroupBy(t => new { t.MaterialGroupCategoryRefId, t.MaterialGroupCategoryRefName }))
            {
                MaterialGroupCategoryWisePurchaseDto groupCategoryDto = new MaterialGroupCategoryWisePurchaseDto
                {
                    MaterialGroupCategoryRefId = sublst.Key.MaterialGroupCategoryRefId,
                    MaterialGroupCategoryRefName = sublst.Key.MaterialGroupCategoryRefName,
                    TotalAmount = sublst.Sum(t => t.TotalAmount)
                };
                output.MaterialGroupCategoryWisePurchaseDtos.Add(groupCategoryDto);
            }

            foreach (var sublst in allItems.GroupBy(t => new { t.MaterialGroupRefId, t.MaterialGroupRefName }))
            {
                MaterialGroupWisePurchaseDto groupDto = new MaterialGroupWisePurchaseDto
                {
                    MaterialGroupRefId = sublst.Key.MaterialGroupRefId,
                    MaterialGroupRefName = sublst.Key.MaterialGroupRefName,
                    TotalAmount = sublst.Sum(t => t.TotalAmount)
                };
                output.MaterialGroupWisePurchaseDtos.Add(groupDto);
            }
            Debug.WriteLine("InterTransfer GetInterTransferReceivedConsolidatedReportGroupOrCategoryOrMaterialWiseForGivenInput() End Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));

            return output;
        }



        public async Task<List<InterTransferRequestConsolidatedViewDto>> GetConsolidatedPendingBasedGivenRequestList(InterTransferRequestList input)
        {
            Debug.WriteLine("InterTransfer GetConsolidatedPendingBasedGivenRequestList() Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));

            var dtos = input.InterTransferViewDtos;
            int requestLocationId = 0;
            if (dtos.Count() > 0)
                requestLocationId = dtos[0].RequestLocationRefId;
            else
                requestLocationId = input.RequestLocationRefId;

            List<InterTransferRequestDetailViewDto> editDetailDto;
            List<InterTransferDetailWithPORefIds> interTransferDetailWithPORefIds = new List<InterTransferDetailWithPORefIds>();

            List<int> allRequestList = new List<int>();
            foreach (var a in dtos)
            {
                if (input.DoesPendingRequest)
                {
                    if (a.CurrentStatus.Equals(L("Pending")) || a.CurrentStatus.Equals(L("Drafted")))
                    {
                        allRequestList.Add(a.Id);
                        interTransferDetailWithPORefIds.Add(new InterTransferDetailWithPORefIds { InterTransferRefId = a.Id });
                    }
                }
                if (input.DoesApprovedRequest)
                {
                    if (a.CurrentStatus.Equals(L("Approved")))
                    {
                        allRequestList.Add(a.Id);
                        interTransferDetailWithPORefIds.Add(new InterTransferDetailWithPORefIds { InterTransferRefId = a.Id });
                    }
                }
                if (input.DoesReceivedStatus)
                {
                    if (a.CurrentStatus.Equals(L("Received")) || a.CurrentStatus.Equals(L("PartiallyReceived")))
                    {
                        allRequestList.Add(a.Id);
                        interTransferDetailWithPORefIds.Add(new InterTransferDetailWithPORefIds { InterTransferRefId = a.Id });
                    }
                }
            }

            editDetailDto = await (from requestmaster in _intertransferRepo.
                                   GetAll().Where(a => allRequestList.Contains(a.Id))
                                   join request in _intertransferDetailRepo.
                                   GetAll().Where(a => allRequestList.Contains(a.InterTransferRefId))
                                   on requestmaster.Id equals request.InterTransferRefId
                                   join mat in _materialRepo.GetAll() on request.MaterialRefId equals mat.Id
                                   join un in _unitRepo.GetAll() on mat.DefaultUnitId equals un.Id
                                   join uiss in _unitRepo.GetAll() on request.UnitRefId equals uiss.Id
                                   join unittransfer in _unitRepo.GetAll() on mat.TransferUnitId equals unittransfer.Id
                                   join matloc in _materiallocationwisestockRepo.
                                         GetAll().Where(mls => mls.LocationRefId == requestLocationId)
                                   on mat.Id equals matloc.MaterialRefId
                                   join matCat in _materialGroupCategoryRepo.GetAll() on mat.MaterialGroupCategoryRefId equals matCat.Id
                                   join matGroup in _materialgroupRepo.GetAll() on matCat.MaterialGroupRefId equals matGroup.Id
                                   join loc in _locationRepo.GetAll() on requestmaster.LocationRefId equals loc.Id
                                   select new InterTransferRequestDetailViewDto
                                   {
                                       LocationRefId = requestmaster.LocationRefId,
                                       LocationRefCode = loc.Code,
                                       LocationRefName = loc.Name,
                                       RequestQty = request.RequestQty,
                                       MaterialRefId = request.MaterialRefId,
                                       MaterialPetName = mat.MaterialPetName,
                                       MaterialRefName = mat.MaterialName,
                                       MaterialGroupRefName = matGroup.MaterialGroupName,
                                       MaterialGroupCategoryRefName = matCat.MaterialGroupCategoryName,
                                       CurrentStatus = request.CurrentStatus,
                                       InterTransferRefId = request.InterTransferRefId,
                                       IssueQty = request.IssueQty,
                                       ApprovedRemarks = request.ApprovedRemarks,
                                       RequestRemarks = request.RequestRemarks,
                                       Sno = request.Sno,
                                       MaterialTypeId = mat.MaterialTypeId,
                                       Uom = un.Name,
                                       MinimumStock = matloc.MinimumStock,
                                       MaximumStock = matloc.MaximumStock,
                                       ReorderLevel = matloc.ReOrderLevel,
                                       UnitRefId = request.UnitRefId,
                                       UnitRefName = uiss.Name,
                                       DefaultUnitId = mat.DefaultUnitId,
                                       DefaultUnitName = un.Name,
                                       TransferUnitId = mat.TransferUnitId,
                                       TransferUnitName = unittransfer.Name
                                   }).ToListAsync();

            List<string> LocationList = editDetailDto.Select(t => t.LocationRefName).Distinct().ToList();

            List<int> materialList = editDetailDto.Select(t => t.MaterialRefId).Distinct().ToList();

            List<InterTransferRequestConsolidatedViewDto> output = new List<InterTransferRequestConsolidatedViewDto>();
            List<InterTransferRequestConsolidatedViewDto> returnOutput = new List<InterTransferRequestConsolidatedViewDto>();

            var matCurrentStock = await _materiallocationwisestockAppService.GetView(new GetMaterialLocationWiseStockInput
            {
                LocationRefId = requestLocationId,
                LiveStock = true,
                MaterialRefIds = materialList,
                MaxResultCount = AppConsts.MaxPageSize
            });
            List<MaterialLocationWiseStockViewDto> materialLocationStock = matCurrentStock.Items.MapTo<List<MaterialLocationWiseStockViewDto>>();
            //List<MaterialLocationWiseStockViewDto> materialLocationStock = new List<MaterialLocationWiseStockViewDto>();

            var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();

            foreach (var group in editDetailDto.GroupBy(t => t.LocationRefId))
            {
                foreach (var subgroup in group.GroupBy(t => new { t.MaterialRefId, t.UnitRefId }))
                {
                    foreach (var matid in materialList)
                    {
                        var mat = subgroup.FirstOrDefault(t => t.MaterialRefId == matid && t.UnitRefId == subgroup.Key.UnitRefId);
                        if (mat != null)
                        {
                            var matStock = materialLocationStock.FirstOrDefault(t => t.MaterialRefId == matid);
                            InterTransferRequestConsolidatedViewDto dto = new InterTransferRequestConsolidatedViewDto();
                            dto.MaterialRefId = subgroup.Key.MaterialRefId;
                            dto.MaterialRefName = mat.MaterialRefName;
                            dto.MaterialPetName = mat.MaterialPetName;
                            dto.MaterialGroupRefName = mat.MaterialGroupRefName;
                            dto.MaterialGroupCategoryRefName = mat.MaterialGroupCategoryRefName;
                            dto.LocationRefId = group.Key;
                            dto.LocationRefCode = mat.LocationRefCode;
                            dto.LocationRefName = mat.LocationRefName;
                            var requestQty = subgroup.Sum(t => t.RequestQty);
                            if (mat.UnitRefId == mat.TransferUnitId)
                            {

                            }
                            else
                            {
                                var unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == mat.UnitRefId && t.RefUnitId == mat.TransferUnitId);
                                if (unitConversion == null)
                                {
                                    var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == mat.UnitRefId);
                                    var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == mat.TransferUnitId);
                                    throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name) + " " + L("Material") + " : " + mat.MaterialRefName);
                                }
                                requestQty = unitConversion.Conversion * requestQty;
                            }

                            dto.RequestQty = requestQty;
                            dto.UnitRefId = mat.TransferUnitId;
                            dto.Uom = mat.TransferUnitName;
                            dto.MaterialTypeId = mat.MaterialTypeId;
                            dto.MaterialTypeName = mat.MaterialTypeId == (int)MaterialType.RAW ? L("RAW") : L("SEMI");
                            dto.RequestRemarks = mat.RequestRemarks;
                            dto.LiveStock = matStock.LiveStock;
                            dto.ReorderLevel = mat.ReorderLevel;
                            dto.MinimumStock = mat.MinimumStock;
                            dto.MaximumStock = mat.MaximumStock;
                            if (dto.UnitRefId == 0)
                            {
                                throw new UserFriendlyException("Unit Error " + dto.MaterialRefName);
                            }
                            dto.InterTransferDetailWithPORefIds = interTransferDetailWithPORefIds;
                            output.Add(dto);
                        }
                    }
                }
            }

            foreach (var group in output.GroupBy(t => t.LocationRefId))
            {
                foreach (var subgroup in group.GroupBy(t => new { t.MaterialRefId }))
                {
                    foreach (var matid in materialList)
                    {
                        var mat = subgroup.FirstOrDefault(t => t.MaterialRefId == matid);
                        if (mat != null)
                        {
                            var matStock = materialLocationStock.FirstOrDefault(t => t.MaterialRefId == matid);
                            InterTransferRequestConsolidatedViewDto dto = new InterTransferRequestConsolidatedViewDto();
                            dto.MaterialRefId = subgroup.Key.MaterialRefId;
                            dto.MaterialRefName = mat.MaterialRefName;
                            dto.MaterialPetName = mat.MaterialPetName;
                            dto.MaterialGroupRefName = mat.MaterialGroupRefName;
                            dto.MaterialGroupCategoryRefName = mat.MaterialGroupCategoryRefName;
                            dto.LocationRefId = group.Key;
                            dto.LocationRefCode = mat.LocationRefCode;
                            dto.LocationRefName = mat.LocationRefName;
                            dto.RequestQty = subgroup.Sum(t => t.RequestQty);
                            dto.UnitRefId = mat.UnitRefId;
                            dto.Uom = mat.Uom;
                            dto.MaterialTypeId = mat.MaterialTypeId;
                            dto.MaterialTypeName = mat.MaterialTypeId == (int)MaterialType.RAW ? L("RAW") : L("SEMI");
                            dto.RequestRemarks = mat.RequestRemarks;
                            dto.LiveStock = matStock.LiveStock;
                            dto.ReorderLevel = mat.ReorderLevel;
                            dto.MinimumStock = mat.MinimumStock;
                            dto.MaximumStock = mat.MaximumStock;
                            if (dto.UnitRefId == 0)
                            {
                                throw new UserFriendlyException("Unit Error " + dto.MaterialRefName);
                            }
                            dto.InterTransferDetailWithPORefIds = interTransferDetailWithPORefIds;
                            returnOutput.Add(dto);
                        }
                    }
                }
            }
            Debug.WriteLine("InterTransfer GetConsolidatedPendingBasedGivenRequestList() End Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));

            return returnOutput;
        }

        public async Task<List<InterTransferViewDto>> GetConsolidatedRequestPendingList(GetInterTransferInput input)
        {
            Debug.WriteLine("InterTransfer GetConsolidatedRequestPendingList() Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
            input.SkipCount = 0;
            input.MaxResultCount = AppConsts.MaxPageSize;
            input.OmitAlreadyPoLinkedRequest = true;
            var result = await _interTransferSupplementaryAppService.GetViewForApproved(input);

            var allRequestListDtos = result.Items.MapTo<List<InterTransferViewDto>>();
            Debug.WriteLine("InterTransfer GetConsolidatedRequestPendingList() Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));

            return allRequestListDtos;
        }

        public async Task<FileDto> GetConsolidatedToExcel(GetInterTransferInput input)
        {
            Debug.WriteLine("GetConsolidatedToExcel() Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
            List<InterTransferViewDto> resultFromPendingList = new List<InterTransferViewDto>();
            List<InterTransferRequestConsolidatedViewDto> dtos = new List<InterTransferRequestConsolidatedViewDto>();
            string fileName = "Request Consolidation ";
            if (input.StartDate.HasValue && input.EndDate.HasValue)
            {
                fileName += "Request (" + input.StartDate.Value.ToString("dd-MMM-yyyy ddd") + " - " + input.EndDate.Value.ToString("dd-MMM-yyyy ddd") + " ) ";
            }
            if (input.DeliveryStartDate.HasValue)
            {
                fileName += " Delivery (" + input.DeliveryStartDate.Value.ToString("dd-MMM-yyyy ddd") + " - " + input.DeliveryEndDate.Value.ToString("dd-MMM-yyyy ddd") + ") ";
            }

            if (input.RequestBasedOnRequestList == true)
            {
                resultFromPendingList = input.RequestList;
                if (resultFromPendingList.Count > 0)
                {
                    dtos = await GetConsolidatedPendingBasedGivenRequestList(new InterTransferRequestList
                    {
                        InterTransferViewDtos = resultFromPendingList,
                        RequestLocationRefId = input.DefaultLocationRefId,
                        DoesApprovedRequest = input.DoesApprovedRequest,
                        DoesPendingRequest = input.DoesPendingRequest,
                        DoesReceivedStatus = input.DoesReceivedStatus
                    });
                }
                else
                {
                    throw new UserFriendlyException(L("NoRecordsFound"));
                }
            }
            else
            {
                input.SkipCount = 0;
                input.MaxResultCount = AppConsts.MaxPageSize;

                resultFromPendingList = await GetConsolidatedRequestPendingList(input);
                if (resultFromPendingList.Count > 0)
                {
                    dtos = await GetConsolidatedPendingBasedGivenRequestList(new InterTransferRequestList
                    {
                        InterTransferViewDtos = resultFromPendingList,
                        RequestLocationRefId = input.DefaultLocationRefId,
                        DoesApprovedRequest = input.DoesApprovedRequest,
                        DoesPendingRequest = input.DoesPendingRequest,
                        DoesReceivedStatus = input.DoesReceivedStatus
                    });
                }
                else
                {
                    throw new UserFriendlyException(L("NoRecordsFound"));
                }
            }

            Debug.WriteLine("GetConsolidatedToExcel() End Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
            return await ExportConsolidatedToExcel(new ExportToExcelDataConsolidated
            {
                ExportData = dtos,
                FileName = fileName,
                DoesPendingRequest = input.DoesPendingRequest,
                DoesApprovedRequest = input.DoesApprovedRequest,
                DoesReceivedStatus = input.DoesReceivedStatus
            });
        }

        public async Task<FileDto> ExportConsolidatedToExcel(ExportToExcelDataConsolidated input)
        {
            Debug.WriteLine("ExportConsolidatedToExcel() Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
            return _intertransferExporter.ExportConsolidated(input);
        }


        //public async Task<GetInterTransferForEditOutput> GetInterTransferApprovalForEdit(GetInterApprovalDataFromId input)
        //{
        //    InterTransferEditDto editDto;
        //    List<InterTransferDetailViewDto> editDetailDto;
        //    TemplateSaveDto editTemplateDto = new TemplateSaveDto();

        //    if (input.Id.HasValue)
        //    {
        //        var hDto = await _intertransferRepo.GetAsync(input.Id.Value);
        //        if (hDto != null)
        //        {
        //            editDto = hDto.MapTo<InterTransferEditDto>();

        //            if (editDto.VehicleNumber.IsNullOrEmpty())
        //                editDto.VehicleNumber = "";

        //            if (editDto.PersonInCharge.IsNullOrEmpty())
        //                editDto.PersonInCharge = "";

        //            if (editDto.TransferValue != null)
        //                editDto.TransferValueInWords = _xmlandjsonConvertorAppService.GetAmountInWords(editDto.TransferValue.Value);

        //            int locationid = editDto.RequestLocationRefId;

        //            editDetailDto = await (from request in _intertransferDetailRepo.GetAll().
        //                Where(a => a.InterTransferRefId == input.Id.Value)
        //                                   join mat in _materialRepo.GetAll()
        //                                       on request.MaterialRefId equals mat.Id
        //                                   join un in _unitRepo.GetAll()
        //                                       on mat.DefaultUnitId equals un.Id
        //                                   join uiss in _unitRepo.GetAll()
        //                                        on request.UnitRefId equals uiss.Id
        //                                   join matloc in
        //                                       _materiallocationwisestockRepo.GetAll().
        //                                           Where(mls => mls.LocationRefId == locationid)
        //                                       on mat.Id equals matloc.MaterialRefId
        //                                   join tomatloc in _materiallocationwisestockRepo.GetAll().
        //                                           Where(mls => mls.LocationRefId == hDto.LocationRefId)
        //                                          on mat.Id equals tomatloc.MaterialRefId
        //                                   select new InterTransferDetailViewDto
        //                                   {
        //                                       Id = request.Id,
        //                                       RequestQty = request.RequestQty,
        //                                       MaterialRefId = request.MaterialRefId,
        //                                       MaterialPetName = mat.MaterialPetName,
        //                                       MaterialRefName = mat.MaterialName,
        //                                       CurrentStatus = request.CurrentStatus,
        //                                       InterTransferRefId = request.InterTransferRefId,
        //                                       IssueQty = request.IssueQty,
        //                                       Price = request.Price,
        //                                       ApprovedRemarks = request.ApprovedRemarks,
        //                                       RequestRemarks = request.RequestRemarks,
        //                                       Sno = request.Sno,
        //                                       MaterialTypeId = mat.MaterialTypeId,
        //                                       Uom = un.Name,
        //                                       OnHand = matloc.CurrentInHand,
        //                                       ToLocationOnHand = tomatloc.CurrentInHand,
        //                                       UnitRefId = request.UnitRefId,
        //                                       UnitRefName = uiss.Name,
        //                                       DefaultUnitId = mat.DefaultUnitId,
        //                                       DefaultUnitName = un.Name,
        //                                       LineTotal = Math.Round(request.IssueQty * request.Price, 3)

        //                                   }).ToListAsync();

        //            var interTransferValueShown = await SettingManager.GetSettingValueAsync<bool>(AppSettings.HouseSettings.InterTransferValueShown);

        //            var rsUnitConversion = await _unitconversionRepo.GetAll().Select(t => new { t.BaseUnitId, t.RefUnitId, t.Conversion, t.MaterialRefId }).ToListAsync();

        //            List<MaterialRateViewListDto> materialRate = new List<MaterialRateViewListDto>();
        //            bool rateDefaultRequired = editDetailDto.Exists(t => t.Price == 0);
        //            if (rateDefaultRequired)
        //            {
        //                if (input.MaterialRateView == null || input.MaterialRateView.Count == 0)
        //                {
        //                    List<Location> loc = await _locationRepo.GetAll().Where(t => t.Id == editDto.RequestLocationRefId).ToListAsync();
        //                    List<LocationListDto> locinput = loc.MapTo<List<LocationListDto>>();
        //                    List<int> materialRefIdsRateNeeded = editDetailDto.Where(t => t.Price == 0).Select(t => t.MaterialRefId).ToList();
        //                    var matView = await _housereportAppService.GetMaterialRateView(new GetHouseReportMaterialRateInput
        //                    {
        //                        StartDate = loc[0].HouseTransactionDate.Value,
        //                        EndDate = loc[0].HouseTransactionDate.Value,
        //                        Locations = locinput,
        //                        MaterialRefIds = materialRefIdsRateNeeded,
        //                        FunctionCalledBy = "Get Intertransfer Approval For Edit"
        //                    });
        //                    materialRate = matView.MaterialRateView;
        //                }
        //                else
        //                {
        //                    materialRate = input.MaterialRateView;
        //                }
        //            }


        //            foreach (var lst in editDetailDto)
        //            {
        //                decimal deconHand = lst.OnHand;
        //                lst.OnHandWithUom = deconHand + "/" + lst.DefaultUnitName;

        //                decimal conversionFactor = 1;
        //                if (lst.DefaultUnitId == lst.UnitRefId)
        //                    conversionFactor = 1;
        //                else
        //                {
        //                    var conv =
        //                             rsUnitConversion.FirstOrDefault(t => t.BaseUnitId == lst.DefaultUnitId && t.RefUnitId == lst.UnitRefId && t.MaterialRefId == lst.MaterialRefId);
        //                    if (conv == null)
        //                    {
        //                        conv =
        //                             rsUnitConversion.FirstOrDefault(t => t.BaseUnitId == lst.DefaultUnitId && t.RefUnitId == lst.UnitRefId);
        //                    }

        //                    if (conv != null)
        //                    {
        //                        conversionFactor = conv.Conversion;
        //                    }
        //                    else
        //                    {
        //                        var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == lst.DefaultUnitId);
        //                        if (baseUnit == null)
        //                        {
        //                            throw new UserFriendlyException(L("UnitIdDoesNotExist", lst.DefaultUnitId));
        //                        }
        //                        var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == lst.UnitRefId);
        //                        if (refUnit == null)
        //                        {
        //                            throw new UserFriendlyException(L("UnitIdDoesNotExist", lst.UnitRefId));
        //                        }
        //                        throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
        //                    }
        //                    lst.OnHand = lst.OnHand * conversionFactor;
        //                }

        //                //if (interTransferValueShown == true && input.PrintFlag == false)
        //                if (lst.Price == 0)
        //                {
        //                    var matprice = materialRate.FirstOrDefault(t => t.MaterialRefId == lst.MaterialRefId);
        //                    if (lst.DefaultUnitId == lst.UnitRefId)
        //                        lst.Price = matprice.AvgRate;
        //                    else
        //                    {
        //                        lst.Price = Math.Round(matprice.AvgRate * 1 / conversionFactor, 3);
        //                    }
        //                }

        //                lst.LineTotal = Math.Round(lst.IssueQty * lst.Price, 3);

        //                if (lst.CurrentStatus.Equals("Q"))
        //                    lst.CurrentStatus = L("RequestApprovalPending");
        //                else if (lst.CurrentStatus.Equals("P"))
        //                    lst.CurrentStatus = L("Pending");
        //                else if (lst.CurrentStatus.Equals("A"))
        //                    lst.CurrentStatus = L("Approved");
        //                else if (lst.CurrentStatus.Equals("R"))
        //                    lst.CurrentStatus = L("Rejected");
        //                else if (lst.CurrentStatus.Equals("D"))
        //                    lst.CurrentStatus = L("Drafted");
        //                else if (lst.CurrentStatus.Equals("V"))
        //                    lst.CurrentStatus = L("Received");
        //                else if (lst.CurrentStatus.Equals("I"))
        //                    lst.CurrentStatus = L("PartiallyReceived");
        //                else if (lst.CurrentStatus.Equals("E"))
        //                    lst.CurrentStatus = L("Excess");
        //                else if (lst.CurrentStatus.Equals("S"))
        //                    lst.CurrentStatus = L("Shortage");

        //                string materialTypeName = lst.MaterialTypeId == (int)MaterialType.RAW ? L("RAW") : L("SEMI");
        //                lst.MaterialTypeName = materialTypeName;

        //                //lst.UsagePatternList = await _materialAppService.MaterialUsagePattern(
        //                //    new InputLocationAndMaterialId
        //                //    {
        //                //        LocationRefId = locationid,
        //                //        MaterialRefId = lst.MaterialRefId
        //                //    }
        //                //    );
        //            }
        //            hDto.TransferValue = Math.Round(editDetailDto.Sum(t => t.LineTotal), 3);
        //        }
        //        else
        //        {
        //            editDto = new InterTransferEditDto();
        //            editDetailDto = new List<InterTransferDetailViewDto>();
        //        }
        //    }
        //    else
        //    {
        //        editDto = new InterTransferEditDto();
        //        editDetailDto = new List<InterTransferDetailViewDto>();
        //    }

        //    return new GetInterTransferForEditOutput
        //    {
        //        InterTransfer = editDto,
        //        InterTransferDetail = editDetailDto,
        //        Templatesave = editTemplateDto
        //    };
        //}



    }
}
