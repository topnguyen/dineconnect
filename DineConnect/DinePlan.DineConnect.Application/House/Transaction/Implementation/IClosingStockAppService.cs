﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Connect.Master.Dtos;

namespace DinePlan.DineConnect.House.Transaction
{
    public interface IClosingStockAppService : IApplicationService
    {
        Task<PagedResultOutput<ClosingStockListDto>> GetAll(GetClosingStockInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetClosingStockForEditOutput> GetClosingStockForEdit(NullableIdInput nullableIdInput);
        Task<IdInput> CreateOrUpdateClosingStock(CreateOrUpdateClosingStockInput input);
        Task DeleteClosingStock(IdInput input);

        Task<GetClosingStockForEditOutput> GetTemplateObjectForEdit(GetObjectFromString input);

        Task SaveTemplateClosingStock(CreateOrUpdateClosingStockInput input);

        Task<ClosingStockExists> GetClosingExists(GetClosingStockExistInput input);

        Task<PagedResultOutput<ClosingStockListDto>> GetAllConsolidatedDayWise(GetClosingStockInput input);

        Task<MessageOutput> ExistSameMaterialAlreadyInClosingStock(CreateOrUpdateClosingStockInput input);

        Task<ClosingStockExists> IsLocationClosingStockExists(LocationWithDate input);
    }
}