﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.House.Master;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using System;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Master;

namespace DinePlan.DineConnect.House.Transaction.Implementation
{
    public class IssueAppService : DineConnectAppServiceBase, IIssueAppService
    {

        private readonly IIssueListExcelExporter _issueExporter;
        private readonly IIssueManager _issueManager;
        private readonly IRepository<Issue> _issueRepo;
        private readonly IRepository<IssueRecipeDetail> _issuerecipedetailRepo;
        private readonly IRepository<Request> _requestRepo;
        private readonly IRepository<ProductionDetail> _productiondetailRepo;
        private readonly IRepository<IssueDetail> _issueDetailRepo;
		private readonly IRepository<Location> _locationRepo;
        private readonly IMaterialAppService _materialAppService;
        private readonly IRepository<MaterialIngredient> _materialrecipeIngredientRepo;
        private readonly IRepository<MaterialRecipeTypes> _materialrecipeRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<Unit> _unitRepo;
        private readonly IRepository<MaterialLocationWiseStock> _materiallocationwisestockRepo;
        private readonly IRepository<MaterialIngredient> _materialingredientRepo;
        private readonly IRepository<MaterialGroupCategory> _materialgroupcategoryRepo;
        private readonly IRepository<ProductionUnit> _productionunitRepo;
        private readonly IRepository<UnitConversion> _unitConversionRepo;
		private readonly IMaterialLocationWiseStockAppService _materiallocationwisestockAppService;

		public IssueAppService(IIssueManager issueManager,
            IRepository<Issue> issueRepo,
            IRepository<IssueRecipeDetail> issuerecipedetailRepo,
            IIssueListExcelExporter issueExporter, 
            IRepository<IssueDetail> issueDetailRepo, 
            IMaterialAppService materialAppService,
            IRepository<MaterialIngredient> recipeIngredientRepo,
            IRepository<MaterialRecipeTypes> reciperepo,
            IRepository<Material> materialRepo,
            IRepository<Unit> unitRepo,
            IRepository<MaterialLocationWiseStock> materiallocationwisestockRepo,
            IRepository<MaterialIngredient> materialingredientRepo,
            IRepository<ProductionDetail> productiondetailRepo,
            IRepository<Request> requestRepo,
            IRepository<MaterialGroupCategory> materialgroupcategoryRepo,
            IRepository<ProductionUnit> productionunitRepo,
            IRepository<UnitConversion> unitConversionRepo,
			IMaterialLocationWiseStockAppService materiallocationwisestockAppService,
			IRepository<Location> locationRepo
			)
        {
            _issueManager = issueManager;
            _issueRepo = issueRepo;
            _issueExporter = issueExporter;
            _issueDetailRepo = issueDetailRepo;
            _materialAppService = materialAppService;
            _materialrecipeIngredientRepo = recipeIngredientRepo;
            _materialrecipeRepo = reciperepo;
            _materialRepo = materialRepo;
            _unitRepo = unitRepo;
            _materiallocationwisestockRepo = materiallocationwisestockRepo;
            _productiondetailRepo = productiondetailRepo;
            _materialingredientRepo = materialingredientRepo;
            _materialgroupcategoryRepo = materialgroupcategoryRepo;
            _requestRepo = requestRepo;
            _productionunitRepo = productionunitRepo;
            _issuerecipedetailRepo = issuerecipedetailRepo;
            _unitConversionRepo = unitConversionRepo;
			_materiallocationwisestockAppService = materiallocationwisestockAppService;
			_locationRepo = locationRepo;
		}

        public async Task<PagedResultOutput<IssueListDto>> GetAll(GetIssueInput input)
        {
            var rsIssue = _issueRepo.GetAll().Where(t => t.LocationRefId == input.DefaultLocationRefId).WhereIf(!input.Filter.IsNullOrEmpty(), p => p.RequestSlipNumber.ToString().Contains(input.Filter));

            if (input.StartDate != null && input.EndDate != null)
            {
                rsIssue = rsIssue.Where(a =>
                                 DbFunctions.TruncateTime(a.IssueTime) >= DbFunctions.TruncateTime(input.StartDate)
                                 &&
                                 DbFunctions.TruncateTime(a.IssueTime) <= DbFunctions.TruncateTime(input.EndDate));
                // rsRequest.Where(t => t.RequestTime >= input.StartDate && t.RequestTime <= input.EndDate);
            }

            var allItems = from issue in rsIssue
                           join productionunit in _productionunitRepo.GetAll()
                           on issue.ProductionUnitRefId equals productionunit.Id into gj
                           from g in gj.DefaultIfEmpty()
                           select new IssueListDto
                           {
                           Id = issue.Id,
                           LocationRefId = issue.LocationRefId,
                           RequestRefId = issue.RequestRefId,
                           RequestTime = issue.RequestTime,
                           RequestSlipNumber = issue.RequestSlipNumber,
                           IssueTime = issue.IssueTime,
                           TokenNumber = issue.TokenNumber,
                           MaterialGroupCategoryRefId = issue.MaterialGroupCategoryRefId,
                           CompletedStatus = issue.CompletedStatus,
                           CreationTime = issue.CreationTime,
                            ProductionUnitRefId = issue.ProductionUnitRefId,
                            ProductionUnitRefName = g.Name,
                           };

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<IssueListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<IssueListDto>(
                allItemCount,
                allListDtos
                );
        }


        public async Task<FileDto> GetAllToExcel(GetIssueInput input)
        {
            input.MaxResultCount = AppConsts.MaxPageSize;

            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<IssueListDto>>();
            return _issueExporter.ExportToFile(allListDtos);
        }

        public async Task<GetIssueForEditOutput> GetIssueForEdit(NullableIdInput input)
        {
            IssueEditDto editDto;
            List<IssueRecipeDetailListDto> editRecipeDetailDtos;
            List<IssueDetailViewDto> editDetailDto;

            if (input.Id.HasValue)
            {
                var hDto = await _issueRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<IssueEditDto>();

                editRecipeDetailDtos = await (from issRecipeDet in _issuerecipedetailRepo.GetAll().Where(a => a.IssueRefId == input.Id.Value)
                                              join mat in _materialRepo.GetAll()
                                              on issRecipeDet.RecipeRefId equals mat.Id
                                              join un in _unitRepo.GetAll()
                                              on mat.DefaultUnitId equals un.Id
                                              join matloc in _materiallocationwisestockRepo.GetAll()
                                             .Where(mls => mls.LocationRefId == editDto.LocationRefId) on mat.Id equals matloc.MaterialRefId
                                              select new IssueRecipeDetailListDto
                                              {
                                                  Id = issRecipeDet.Id,
                                                  IssueRefId = issRecipeDet.IssueRefId,
                                                  RecipeRefId = issRecipeDet.RecipeRefId,
                                                  RecipeRefName = mat.MaterialName,
                                                  Sno = issRecipeDet.Sno,
                                                  RecipeProductionQty = issRecipeDet.RecipeProductionQty,
                                                  UnitRefId = issRecipeDet.UnitRefId,
                                                  Uom = un.Name,
                                                  CompletedQty = issRecipeDet.CompletedQty,
                                                  CompletionFlag = issRecipeDet.CompletionFlag,
                                                  MultipleBatchProductionAllowed = issRecipeDet.MultipleBatchProductionAllowed,
                                                  Remarks = issRecipeDet.Remarks,
                                                  CurrentInHand = matloc.CurrentInHand
                                              }).ToListAsync();

                editDetailDto = await (from issDet in _issueDetailRepo.GetAll().Where(a => a.IssueRefId == input.Id.Value)
                                       join mat in _materialRepo.GetAll() 
                                       on issDet.MaterialRefId equals mat.Id 
                                       join un in _unitRepo.GetAll()
                                       on mat.DefaultUnitId equals un.Id
                                       join uiss in _unitRepo.GetAll()
                                       on issDet.UnitRefId equals uiss.Id 
                                       join matloc in _materiallocationwisestockRepo.GetAll()
                                      .Where(mls => mls.LocationRefId == editDto.LocationRefId) on mat.Id equals matloc.MaterialRefId
                                       select new IssueDetailViewDto
                                       {
                                           Id=issDet.Id,
                                           IssueQty=issDet.IssueQty,
                                           IssueRefId=issDet.IssueRefId,
                                           MaterialRefId=issDet.MaterialRefId,
                                           MaterialRefName = mat.MaterialName,
                                           RecipeRefId=issDet.RecipeRefId,
                                           RequestQty=issDet.RequestQty,
                                           UnitRefId = issDet.UnitRefId,
                                           UnitRefName = uiss.Name,
                                           CurrentInHand = matloc.CurrentInHand,
                                           Sno=issDet.Sno,
                                           DefaultUnitName = un.Name,
                                           DefaultUnitId = mat.DefaultUnitId
                                       }).ToListAsync();
            }
            else
            {
                
                editDto = new IssueEditDto();
                var maxTokno = (from iss in _issueRepo.GetAll() 
                                .Where(a=>a.IssueTime.Year == DateTime.Now.Year
                                && a.IssueTime.Month == DateTime.Now.Month
                                && a.IssueTime.Day == DateTime.Now.Day)
                                select iss).ToList();

                editDto.TokenNumber =maxTokno.Count() + 1;
                editDto.IssueTime = DateTime.Now;
                editDetailDto = new List<IssueDetailViewDto>();
                editRecipeDetailDtos = new List<IssueRecipeDetailListDto>();
            }

            return new GetIssueForEditOutput
            {
                Issue = editDto,
                IssueRecipeDetail = editRecipeDetailDtos,
                IssueDetail=editDetailDto
            };
        }

        public async Task<GetIssueForStandardRecipe> GetIssueForRecipe(GetStandardIngridient input)
        {
            List<IssueDetailViewDto> editDetailDto;

            if (input.RequestRecipeList == null)
            {
                var mas = _materialrecipeRepo.GetAll().Where(a => a.MaterailRefId == input.recipeRefId).ToList();

                if (mas == null || mas.Count == 0)
                {
                    throw new UserFriendlyException(L("MaterialRecipeInformationMissing"));
                }
                decimal standardProductionQty = mas[0].PrdBatchQty;

                editDetailDto = await (from issDet in _materialrecipeIngredientRepo.GetAll().Where(a => a.RecipeRefId == input.recipeRefId)
                                       join mat in _materialRepo.GetAll()
                                       on issDet.MaterialRefId equals mat.Id
                                       orderby issDet.UserSerialNumber
                                       join un in _unitRepo.GetAll() on mat.DefaultUnitId equals un.Id
                                       join uiss in _unitRepo.GetAll() on issDet.UnitRefId equals uiss.Id 
                                       join matloc in _materiallocationwisestockRepo.GetAll()
                                            .Where(mls => mls.LocationRefId == input.LocationRefId) on mat.Id equals matloc.MaterialRefId
                                       select new IssueDetailViewDto
                                       {
                                           Id = issDet.Id,
                                           IssueQty = Math.Round(issDet.MaterialUsedQty / standardProductionQty * input.requestQtyforProduction, 4),
                                           IssueRefId = 0,
                                           MaterialRefId = issDet.MaterialRefId,
                                           MaterialRefName = mat.MaterialName,
                                           RequestQty = Math.Round(issDet.MaterialUsedQty / standardProductionQty * input.requestQtyforProduction, 4),
                                           Sno = issDet.UserSerialNumber,
                                           DefaultUnitId = mat.DefaultUnitId,
                                           DefaultUnitName = un.Name,
                                           CurrentInHand = matloc.CurrentInHand,
                                           UnitRefId = issDet.UnitRefId,
                                           UnitRefName = uiss.Name
                                       }).ToListAsync();
            }
            else
            {
                List<IssueDetailViewDto> allDetails = new List<IssueDetailViewDto>();

                foreach (var recDet in input.RequestRecipeList)
                {
                    var mas = await _materialrecipeRepo.GetAll().Where(a => a.MaterailRefId == recDet.RecipeRefId).ToListAsync();

                    if (mas == null || mas.Count == 0)
                    {
                        throw new UserFriendlyException(L("MaterialRecipeInformationMissing"));
                    }
                    decimal standardProductionQty = mas[0].PrdBatchQty;

                    var detaildto = await (from issDet in _materialrecipeIngredientRepo.GetAll()
                                           .Where(a => a.RecipeRefId == recDet.RecipeRefId)
                                           join mat in _materialRepo.GetAll()
                                           on issDet.MaterialRefId equals mat.Id
                                           orderby issDet.UserSerialNumber
                                           join un in _unitRepo.GetAll() on mat.DefaultUnitId equals un.Id
                                           join uiss in _unitRepo.GetAll() on issDet.UnitRefId equals uiss.Id
                                           join matloc in _materiallocationwisestockRepo.GetAll()
                                                .Where(mls => mls.LocationRefId == input.LocationRefId) on mat.Id equals matloc.MaterialRefId
                                           select new IssueDetailViewDto
                                           {
                                               Id = issDet.Id,
                                               IssueQty = Math.Round(issDet.MaterialUsedQty / standardProductionQty * recDet.RequestQtyforProduction, 2),
                                               IssueRefId = 0,
                                               MaterialRefId = issDet.MaterialRefId,
                                               MaterialRefName = mat.MaterialName,
                                               RequestQty = Math.Round(issDet.MaterialUsedQty / standardProductionQty * recDet.RequestQtyforProduction, 2),
                                               Sno = issDet.UserSerialNumber,
                                               DefaultUnitId = mat.DefaultUnitId,
                                               DefaultUnitName = un.Name,
                                               CurrentInHand = matloc.CurrentInHand,
                                               UnitRefId = issDet.UnitRefId,
                                               UnitRefName = uiss.Name
                                           }).ToListAsync();

                    allDetails.AddRange(detaildto);
                }

                editDetailDto = (from a in allDetails
                                 group a by new { a.MaterialRefId, a.MaterialRefName,a.DefaultUnitId, a.DefaultUnitName, a.UnitRefId, a.UnitRefName } into g
                                 select new IssueDetailViewDto
                                 {
                                     MaterialRefId = g.Key.MaterialRefId,
                                     MaterialRefName = g.Key.MaterialRefName,
                                     IssueQty = g.Sum(t => t.IssueQty),
                                     DefaultUnitName = g.Key.DefaultUnitName,
                                     DefaultUnitId = g.Key.DefaultUnitId,
                                     UnitRefId = g.Key.UnitRefId,
                                     UnitRefName = g.Key.UnitRefName,
                                     CurrentInHand = g.Average(t => t.CurrentInHand)
                                 }).ToList();

            }

            return new GetIssueForStandardRecipe
            {
                IssueDetail = editDetailDto
            };
        }


        public async Task<int> GetMaxTokenNumber(DateTime selDt)
        {
            DateTime givenDt =selDt;
            var maxTokno =await (from iss in _issueRepo.GetAll()
                                .Where(a => a.IssueTime.Year == givenDt.Year
                                && a.IssueTime.Month == givenDt.Month
                                && a.IssueTime.Day == givenDt.Day)
                            select iss).ToListAsync();

            var TokenNumber = maxTokno.Count() + 1;
            return TokenNumber;
        }

        public async Task<IdInput> CreateOrUpdateIssue(CreateOrUpdateIssueInput input)
        {
            if (input.Issue.Id.HasValue)
            {
                return await UpdateIssue(input);
            }
            else
            {
                return await CreateIssue(input);
            }
        }

        public async Task DeleteIssue(IdInput input)
        {
            var issue = await _issueRepo.FirstOrDefaultAsync(t => t.Id == input.Id);

            if (issue.CompletedStatus == true )
            {
                throw new UserFriendlyException(L("IssueProductionsAlready"));
            }

            var prodExists = await _productiondetailRepo.GetAll().Where(t => t.IssueRefId == issue.Id).ToListAsync();

            if (prodExists.Count>0)
            {
                throw new UserFriendlyException(L("IssueProductionsAlready"));
            }

            if (issue.IssueTime.Date != DateTime.Now.Date)
            {
                throw new UserFriendlyException(L("CurrentDateIssuedCanBeOnlyDeleted"));
            }

            var issDetail = await _issueDetailRepo.GetAll().Where(t => t.IssueRefId== input.Id).ToListAsync();
            foreach (var det in issDetail)
            {
                MaterialLedgerDto ml = new MaterialLedgerDto();
                ml.LocationRefId = issue.LocationRefId;
                ml.MaterialRefId = det.MaterialRefId;
                ml.LedgerDate = issue.IssueTime.Date;
                ml.Issued =  -1 * det.IssueQty;
                ml.UnitId = det.UnitRefId;

                var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                det.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;
                await _issueDetailRepo.DeleteAsync(det.Id);
            }

            await _issueRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task<IdInput> UpdateIssue(CreateOrUpdateIssueInput input)
        {
            var item = await _issueRepo.GetAsync(input.Issue.Id.Value);

            if (input.IssueDetail == null || input.IssueDetail.Count() == 0)
            {
                throw new UserFriendlyException(L("MinimumOneDetail"));
            }

            var dto = input.Issue;

            //@@Pending     Need to remove //
            if (input.IssueRecipeDetail != null && input.IssueRecipeDetail.Count>0)
            {
                if (input.IssueRecipeDetail.Count == 1)
                {
                    dto.RequestSlipNumber = dto.RequestSlipNumber.Substring(0, dto.RequestSlipNumber.IndexOf("/"));

                    int recId = input.IssueRecipeDetail[0].RecipeRefId;

                    var matname = _materialRepo.GetAll().FirstOrDefault(t => t.Id == recId);

                    int length = 25 - dto.RequestSlipNumber.Length;
                    if (matname.MaterialName.Length < length)
                        length = matname.MaterialName.Length;
                    dto.RequestSlipNumber = dto.RequestSlipNumber + "/" + matname.MaterialName.Substring(0, length);
                }
            }
            else if (input.Issue.MaterialGroupCategoryRefId !=0)
            {
                int indexposition = dto.RequestSlipNumber.IndexOf("/");
                dto.RequestSlipNumber = dto.RequestSlipNumber.Substring(0,dto.RequestSlipNumber.IndexOf("/"));
                int length = 25 - dto.RequestSlipNumber.Length;
                var catname = _materialgroupcategoryRepo.GetAll().FirstOrDefault(t => t.Id == input.Issue.MaterialGroupCategoryRefId).MaterialGroupCategoryName;

                if (catname.Length < length)
                    length = catname.Length;

                dto.RequestSlipNumber = dto.RequestSlipNumber + "/" + catname.Substring(0, length);
            }

            item.IssueTime = dto.IssueTime;
            item.LocationRefId = dto.LocationRefId;
            item.RequestSlipNumber = dto.RequestSlipNumber;
            item.RequestTime = dto.RequestTime;
            item.MaterialGroupCategoryRefId = dto.MaterialGroupCategoryRefId;

            foreach (IssueRecipeDetailListDto items in input.IssueRecipeDetail.ToList())
            {
             

            }

            List<int> recipeToBeRetained = new List<int>();

            if (input.IssueRecipeDetail != null && input.IssueRecipeDetail.Count > 0)
            {
                foreach (var items in input.IssueRecipeDetail)
                {
                    var recipeRefId = items.RecipeRefId;
                    recipeToBeRetained.Add(recipeRefId);

                    var existingIssueDetail = _issuerecipedetailRepo.GetAllList(u => u.IssueRefId == dto.Id && u.RecipeRefId.Equals(recipeRefId));
                    if (existingIssueDetail.Count == 0)  //Add new record
                    {
                        IssueRecipeDetail ird = new IssueRecipeDetail();
                        ird.IssueRefId = (int)dto.Id;
                        ird.Sno = items.Sno;
                        ird.RecipeRefId = items.RecipeRefId;
                        ird.RecipeProductionQty = items.RecipeProductionQty;
                        ird.CompletedQty = items.CompletedQty;
                        ird.UnitRefId = items.UnitRefId;
                        ird.MultipleBatchProductionAllowed = items.MultipleBatchProductionAllowed;
                        ird.CompletionFlag = items.CompletionFlag;
                        ird.Remarks = items.Remarks;

                        var retRecipeId = await _issuerecipedetailRepo.InsertAndGetIdAsync(ird);

                    }
                    else
                    {
                        var editDetailDto = await _issuerecipedetailRepo.GetAsync(existingIssueDetail[0].Id);

                        editDetailDto.IssueRefId = (int)dto.Id;
                        editDetailDto.Sno = items.Sno;
                        editDetailDto.RecipeRefId = items.RecipeRefId;
                        editDetailDto.RecipeProductionQty = items.RecipeProductionQty;
                        editDetailDto.CompletedQty = items.CompletedQty;
                        editDetailDto.UnitRefId = items.UnitRefId;
                        editDetailDto.MultipleBatchProductionAllowed = items.MultipleBatchProductionAllowed;
                        editDetailDto.CompletionFlag = items.CompletionFlag;
                        editDetailDto.Remarks = items.Remarks;

                        var retRecipeId = await _issuerecipedetailRepo.InsertOrUpdateAndGetIdAsync(editDetailDto);

                    }
                }
            }

            var delRecieDetailsList = _issuerecipedetailRepo.GetAll().Where(a => a.IssueRefId == input.Issue.Id.Value && !recipeToBeRetained.Contains(a.RecipeRefId)).ToList();

            foreach (var a in delRecieDetailsList)
            {
                _issuerecipedetailRepo.Delete(a.Id);
            }


            List<int> materialsToBeRetained = new List<int>();

            if (input.IssueDetail !=null && input.IssueDetail.Count>0)
            {
                foreach(var items in input.IssueDetail)
                {
                    var matRefId = items.MaterialRefId;
                    materialsToBeRetained.Add(matRefId);

                    var existingIssueDetail = _issueDetailRepo.GetAllList(u => u.IssueRefId == dto.Id && u.MaterialRefId.Equals(matRefId));
                    if(existingIssueDetail.Count==0)  //Add new record
                    {
                        IssueDetail id = new IssueDetail();
                        id.IssueRefId =(int) dto.Id;
                        id.IssueQty = items.IssueQty;
                        id.MaterialRefId = items.MaterialRefId;
                        id.RecipeRefId = items.RecipeRefId;
                        id.RequestQty = items.RequestQty;
                        id.UnitRefId = items.UnitRefId;
                        id.Sno = (int) items.Sno;

                        var retId2 = await _issueDetailRepo.InsertAndGetIdAsync(id);

                        MaterialLedgerDto ml = new MaterialLedgerDto();
                        ml.LocationRefId = dto.LocationRefId;
                        ml.MaterialRefId = id.MaterialRefId;
                        ml.LedgerDate = dto.IssueTime.Date;
                        ml.Issued = id.IssueQty;
                        ml.UnitId = id.UnitRefId;

                        var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                        id.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;
                    }
                    else
                    {
                        var editDetailDto = await _issueDetailRepo.GetAsync(existingIssueDetail[0].Id);

                        decimal differcenceQty;
                        differcenceQty = items.IssueQty - editDetailDto.IssueQty;

                        editDetailDto.IssueRefId = (int)dto.Id;
                        editDetailDto.IssueQty = items.IssueQty;
                        editDetailDto.MaterialRefId = items.MaterialRefId;
                        editDetailDto.RecipeRefId = items.RecipeRefId;
                        editDetailDto.RequestQty = items.RequestQty;
                        editDetailDto.IssueQty = items.IssueQty;
                        editDetailDto.UnitRefId = items.UnitRefId;
                        editDetailDto.Sno = (int) items.Sno;

                        var retId3 = await _issueDetailRepo.InsertOrUpdateAndGetIdAsync(editDetailDto);

                        MaterialLedgerDto ml = new MaterialLedgerDto();
                        ml.LocationRefId = dto.LocationRefId;
                        ml.MaterialRefId = items.MaterialRefId;
                        ml.LedgerDate = dto.IssueTime.Date;
                        ml.Issued = differcenceQty;
                        ml.UnitId = items.UnitRefId;
                        ml.PlusMinus = "U";
                        var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                        editDetailDto.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;
                    }
                }
            }

            var delMatList = _issueDetailRepo.GetAll().Where(a => a.IssueRefId == input.Issue.Id.Value && !materialsToBeRetained.Contains(a.MaterialRefId)).ToList();

            foreach (var a in delMatList)
            {
                MaterialLedgerDto ml = new MaterialLedgerDto();
                ml.LocationRefId = dto.LocationRefId;
                ml.MaterialRefId = a.MaterialRefId;
                ml.LedgerDate = dto.IssueTime.Date;
                ml.Issued = (-1 * a.IssueQty); 
                ml.PlusMinus = "U";
                ml.UnitId = a.UnitRefId;
                var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);

                _issueDetailRepo.Delete(a.Id);
            }

            CheckErrors(await _issueManager.CreateSync(item));

            return new IdInput
            {
                Id = input.Issue.Id.Value
            };
        }

        protected virtual async Task<IdInput> CreateIssue(CreateOrUpdateIssueInput input)
        {

            var dto = input.Issue.MapTo<Issue>();

            if (input.IssueDetail == null || input.IssueDetail.Count() == 0)
            {
                throw new UserFriendlyException(L("MinimumOneDetail"));
            }

            if (input.Issue.RequestRefId == null || input.Issue.RequestRefId == 0)
            {
                if (input.IssueRecipeDetail != null && input.IssueRecipeDetail.Count>0)
                {
                    if (input.IssueRecipeDetail.Count == 1)
                    {
                        int length = 25 - dto.RequestSlipNumber.Length;

                        int recId = input.IssueRecipeDetail[0].RecipeRefId;

                        var matname = _materialRepo.GetAll().FirstOrDefault(t => t.Id == recId);

                        if (matname.MaterialName.Length < length)
                            length = matname.MaterialName.Length;

                        dto.RequestSlipNumber = dto.RequestSlipNumber + "/" + matname.MaterialName.Substring(0, length);
                    }
                }
                else if (input.Issue.MaterialGroupCategoryRefId != 0)
                {
                    int length = 25 - dto.RequestSlipNumber.Length;
                    var catname = _materialgroupcategoryRepo.GetAll().FirstOrDefault(t => t.Id == input.Issue.MaterialGroupCategoryRefId).MaterialGroupCategoryName;

                    if (catname.Length < length)
                        length = catname.Length;

                    dto.RequestSlipNumber = dto.RequestSlipNumber + "/" + catname.Substring(0, length);
                }
            }

            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == dto.LocationRefId);
            dto.AccountDate = loc.HouseTransactionDate.Value;
            var retId = await _issueRepo.InsertAndGetIdAsync(dto);

            foreach(IssueRecipeDetailListDto items in input.IssueRecipeDetail.ToList() )
            {
                IssueRecipeDetail ird = new IssueRecipeDetail();
                ird.IssueRefId = (int)dto.Id;
                ird.Sno = items.Sno;
                ird.RecipeRefId = items.RecipeRefId;
                ird.RecipeProductionQty = items.RecipeProductionQty;
                ird.CompletedQty = items.CompletedQty;
                ird.UnitRefId = items.UnitRefId;
                ird.MultipleBatchProductionAllowed = items.MultipleBatchProductionAllowed;
                ird.CompletionFlag = items.CompletionFlag;
                ird.Remarks = items.Remarks;

                var retRecipeId = await _issuerecipedetailRepo.InsertAndGetIdAsync(ird);

            }

            foreach(IssueDetailViewDto items in input.IssueDetail.ToList())
            {
                IssueDetail id = new IssueDetail();
                id.IssueRefId = (int)dto.Id;
                id.IssueQty = items.IssueQty;
                id.MaterialRefId = items.MaterialRefId;
                id.RequestQty = items.IssueQty;
                id.Sno = (int) items.Sno;
                id.UnitRefId = items.UnitRefId;

                var retId2 = await _issueDetailRepo.InsertAndGetIdAsync(id);


                MaterialLedgerDto ml = new MaterialLedgerDto();
                ml.LocationRefId = dto.LocationRefId;
                ml.MaterialRefId = id.MaterialRefId;
                ml.LedgerDate = dto.IssueTime;
                ml.Issued = id.IssueQty;
                ml.UnitId = id.UnitRefId;

                var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                id.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;
            }

            var request = await _requestRepo.FirstOrDefaultAsync(t => t.Id == input.Issue.RequestRefId);
            if (request != null)
            {
                var requestDto = await _requestRepo.GetAsync(request.Id);
                requestDto.CompletedStatus = true;
            }

            CheckErrors(await _issueManager.CreateSync(dto));

            return new IdInput
            {
                Id = retId
            };
        }

        public async Task<ListResultOutput<IssueListDto>> GetIssueIds(DateWithProductionUnit input)
        {
            DateTime dateFrom = input.Dt.Value.AddDays(-1);
            DateTime dateTo = input.Dt.Value.AddDays(1);

            var lstIssue = await _issueRepo.GetAll().Where(t=>t.IssueTime>= dateFrom && t.IssueTime < dateTo && t.ProductionUnitRefId==input.ProductionUnitRefId).ToListAsync();
            return new ListResultOutput<IssueListDto>(lstIssue.MapTo<List<IssueListDto>>());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetRequestSlipNumber(IdInput input)
        {
            var lstRequest = await _requestRepo.GetAll().Where(t => t.LocationRefId == input.Id &&  t.CompletedStatus == false).ToListAsync();
            return new ListResultOutput<ComboboxItemDto>(
                    lstRequest.Select(e => new ComboboxItemDto(e.Id.ToString(), e.RequestSlipNumber)).ToList());
        }

        public async Task<List<IssueWithRecipeListDto>> GetPendingIssues(InputLocation input)
        {
            await SetCompletedIssues(input);

            var rsUnitConversion = await _unitConversionRepo.GetAll().ToListAsync();

            List<IssueRecipeDetailListDto> editRecipeDetailDtos;

            var allItems = await _issueRepo.GetAll().Where(t=>t.CompletedStatus==false && t.LocationRefId==input.LocationRefId && t.ProductionUnitRefId==input.ProductionUnitRefId).ToListAsync();

            var allListDtos = allItems.MapTo<List<IssueWithRecipeListDto>>();

            int loopIndex = 0;
            foreach (var item in allListDtos)
            {
                editRecipeDetailDtos = await (from issRecipeDet in _issuerecipedetailRepo.GetAll().Where(a => a.IssueRefId == item.Id)
                                              join mat in _materialRepo.GetAll()
                                              on issRecipeDet.RecipeRefId equals mat.Id
                                              join un in _unitRepo.GetAll()
                                              on mat.DefaultUnitId equals un.Id
                                              select new IssueRecipeDetailListDto
                                              {
                                                  Id = issRecipeDet.Id,
                                                  IssueRefId = issRecipeDet.IssueRefId,
                                                  RecipeRefId = issRecipeDet.RecipeRefId,
                                                  RecipeRefName = mat.MaterialName,
                                                  Sno = issRecipeDet.Sno,
                                                  RecipeProductionQty = issRecipeDet.RecipeProductionQty,
                                                  UnitRefId = issRecipeDet.UnitRefId,
                                                  Uom = un.Name,
                                                  CompletedQty = issRecipeDet.CompletedQty,
                                                  CompletionFlag = issRecipeDet.CompletionFlag,
                                                  MultipleBatchProductionAllowed = issRecipeDet.MultipleBatchProductionAllowed,
                                                  Remarks = issRecipeDet.Remarks,
                                              }).ToListAsync();

                

                if (editRecipeDetailDtos.Count>0)
                {
                    int recIndex = 0;
                    foreach (var det in editRecipeDetailDtos) {
                        var prodDet = await _productiondetailRepo.GetAll().Where(t => t.IssueRefId == item.Id && t.MaterialRefId == det.RecipeRefId).ToListAsync();
                        //var alreadyProduced = prodDet.Sum(t => t.ProductionQty);

                        decimal alreadyProduced = 0;
                        decimal conversionFactor = 1;
                        foreach (var gpd in prodDet.GroupBy(t => t.UnitRefId))
                        {
                            conversionFactor = 1;
                            if (det.UnitRefId == gpd.Key)
                                conversionFactor = 1;
                            else
                            {
                                var conv = rsUnitConversion.First(t => t.BaseUnitId == det.UnitRefId
                                                                               && t.RefUnitId == gpd.Key);
                                if (conv != null)
                                {
                                    conversionFactor = conv.Conversion;
                                }
								else
								{
									var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == det.UnitRefId);
									if (baseUnit == null)
									{
										throw new UserFriendlyException(L("UnitIdDoesNotExist", det.UnitRefId));
									}
									var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == gpd.Key);
									if (refUnit == null)
									{
										throw new UserFriendlyException(L("UnitIdDoesNotExist", gpd.Key));
									}
									throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
								}

							}
							alreadyProduced = alreadyProduced + gpd.Sum(t => t.ProductionQty) * 1/conversionFactor;
                        }


                        var pendingProductionQty = det.RecipeProductionQty - alreadyProduced;
                        editRecipeDetailDtos[recIndex].RecipeProductionQty = pendingProductionQty;
                        recIndex++;
                    }
                }
                allListDtos[loopIndex].IssueRecipeDetail = editRecipeDetailDtos;
                loopIndex++;
            }
            return allListDtos;
        }

        public async Task SetCompletedIssues(InputLocation input)
        {
            var allItems = await _issueRepo.GetAll().Where(t => t.CompletedStatus == false && t.LocationRefId == input.LocationRefId).ToListAsync();

            var allListDtos = allItems.MapTo<List<IssueWithRecipeListDto>>();

            foreach (var item in allListDtos)
            {
                var recipeExist = await _issuerecipedetailRepo.GetAll().Where(a => a.IssueRefId == item.Id).ToListAsync();
                if (recipeExist.Count > 0)
                {
                    var pendingCount = recipeExist.Where(t=>t.CompletionFlag == false).ToList();

                    if (pendingCount.Count == 0)
                    {
                        var issDto = await _issueRepo.GetAsync(item.Id);
                        issDto.CompletedStatus = true;
                        await _issueRepo.InsertOrUpdateAsync(issDto);
                    }
                }
            }
        }


        public async Task<List<MaterialInfoDto>> GetMaterialForGivenRecipeCombobox(InputLocationAndRecipe input)
        {
            IQueryable<MaterialInfoDto> allItems;

            if (input.RecipeRefId>0)
            {
                allItems = (from mat in _materialRepo.GetAll()
                            join recIng in _materialingredientRepo.GetAll()
                            on mat.Id equals recIng.MaterialRefId
                            where recIng.RecipeRefId == input.RecipeRefId 
                            join un in _unitRepo.GetAll()
                            on mat.DefaultUnitId equals un.Id
                            join issunit in _unitRepo.GetAll()
                            on mat.IssueUnitId equals issunit.Id
                            join stkunit in _unitRepo.GetAll()
                            on mat.StockAdjustmentUnitId equals stkunit.Id
                            join trnunit in _unitRepo.GetAll()
                           on mat.TransferUnitId equals trnunit.Id
                            join matloc in _materiallocationwisestockRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId && t.IsActiveInLocation == true)
                            on mat.Id equals matloc.MaterialRefId
                            select new MaterialInfoDto
                            {
                                MaterialRefId = mat.Id,
                                MaterialRefName = mat.MaterialName,
                                MaterialTypeId = mat.MaterialTypeId,
                                IsFractional = mat.IsFractional,
                                IsBranded = mat.IsFractional,
                                IsQuoteNeededForPurchase = mat.IsQuoteNeededForPurchase,
                                MaterialPetName = mat.MaterialPetName,
                                DefaultUnitId = mat.DefaultUnitId,
                                DefaultUnitName = un.Name,
                                IssueUnitId = mat.IssueUnitId,
                                IssueUnitName = issunit.Name,
                                IsNeedtoKeptinFreezer = mat.IsNeedtoKeptinFreezer,
                                IsMfgDateExists = mat.IsMfgDateExists,
                                WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                                IsHighValueItem = mat.IsHighValueItem,
                                CurrentInHand = matloc.CurrentInHand,
                                StockAdjustmentUnitId = mat.StockAdjustmentUnitId,
                                StockAdjustentUnitRefName = stkunit.Name,
                                TransferUnitId = mat.TransferUnitId,
                                TransferUnitRefName = trnunit.Name
                            });
            }
            else
            {
                allItems = (from mat in _materialRepo.GetAll()
                            join un in _unitRepo.GetAll()
                            on mat.DefaultUnitId equals un.Id
                            join issunit in _unitRepo.GetAll()
                            on mat.IssueUnitId equals issunit.Id
                            join stkunit in _unitRepo.GetAll()
                           on mat.StockAdjustmentUnitId equals stkunit.Id
                            join trnunit in _unitRepo.GetAll()
                           on mat.TransferUnitId equals trnunit.Id
                            join matloc in _materiallocationwisestockRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId && t.IsActiveInLocation == true)
                            on mat.Id equals matloc.MaterialRefId
                            select new MaterialInfoDto
                            {
                                MaterialRefId = mat.Id,
                                MaterialRefName = mat.MaterialName,
                                MaterialTypeId = mat.MaterialTypeId,
                                IsFractional = mat.IsFractional,
                                IsBranded = mat.IsFractional,
                                IsQuoteNeededForPurchase = mat.IsQuoteNeededForPurchase,
                                MaterialPetName = mat.MaterialPetName,
                                DefaultUnitId = mat.DefaultUnitId,
                                DefaultUnitName = un.Name,
                                IssueUnitId = mat.IssueUnitId,
                                IssueUnitName = issunit.Name,
                                IsNeedtoKeptinFreezer = mat.IsNeedtoKeptinFreezer,
                                IsMfgDateExists = mat.IsMfgDateExists,
                                WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                                CurrentInHand = matloc.CurrentInHand,
                                IsHighValueItem = mat.IsHighValueItem,
                                StockAdjustmentUnitId = mat.StockAdjustmentUnitId,
                                StockAdjustentUnitRefName = stkunit.Name,
                                TransferUnitId = mat.TransferUnitId,
                                TransferUnitRefName = trnunit.Name
                            });
            }
            var matRecipeList = await allItems.ToListAsync();

            return matRecipeList;


        }

		public async Task<FileDto> ImportIssueTemplate(InputLocation input)
		{
			List<TransferTemplateDto> inputDtos = new List<TransferTemplateDto>();
            
			var allMaterial = await _materialRepo.GetAllListAsync();
            if (input.MaterialListFor.IsNullOrEmpty())
            {
                input.MaterialListFor = "";
            }
            if (input.MaterialListFor.Equals("RAW"))
                allMaterial = allMaterial.Where(t => t.MaterialTypeId == (int)MaterialType.RAW).ToList();
            else if (input.MaterialListFor.Equals("SEMI"))
                allMaterial = allMaterial.Where(t => t.MaterialTypeId == (int)MaterialType.SEMI).ToList();

            var allGroup = await _materialgroupcategoryRepo.GetAllListAsync();

			var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);

			if (location == null)
				throw new UserFriendlyException(L("LocationErr"));

			var dt = await _materiallocationwisestockAppService.GetView(new GetMaterialLocationWiseStockInput
			{
				LocationRefId = input.LocationRefId,
				LiveStock = true,
                MaxResultCount = AppConsts.MaxPageSize
			});

			List<MaterialLocationWiseStockViewDto> materialStock = dt.Items.MapTo<List<MaterialLocationWiseStockViewDto>>();

			//var materialStock = await _materiallocationwisestockRepo.GetAllListAsync(t => t.LocationRefId == input.Id);

			foreach (var mat in allMaterial)
			{
				TransferTemplateDto dto = new TransferTemplateDto();
				dto.LocationRefId = input.LocationRefId;
				dto.LocationRefName = location.Name;
				dto.Barcode = mat.Barcode == null ? mat.MaterialName : mat.Barcode;
				dto.MaterialRefId = mat.Id;
				dto.MaterialRefName = mat.MaterialName;
				dto.CurrentOnHand = 0;
				var matGroupCategory = await _materialgroupcategoryRepo.FirstOrDefaultAsync(t => t.Id == mat.MaterialGroupCategoryRefId);
				if (matGroupCategory != null)
					dto.MaterialGroupCategoryName = matGroupCategory.MaterialGroupCategoryName;

				var stk = materialStock.FirstOrDefault(t => t.LocationRefId == input.LocationRefId && t.MaterialRefId == mat.Id);
				if (stk != null)
				{
					dto.CurrentOnHand = stk.LiveStock;
				}

				inputDtos.Add(dto);
			}

			var fileTemplate = _issueExporter.CreateIssueTemplate(inputDtos, location.Name, DateTime.Today.ToString("dd-MMM-yy"));

			return fileTemplate;

		}



		public async Task<IdInput> GetCategoryIdForRecipe(IdInput input)
        {
            var returnid = await _materialRepo.FirstOrDefaultAsync(t => t.Id == input.Id);
            return new IdInput { Id = returnid.MaterialGroupCategoryRefId };
        }
    }

}