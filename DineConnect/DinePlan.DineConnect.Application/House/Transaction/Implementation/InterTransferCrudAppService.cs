﻿#region Include
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.House.Transaction.Dtos;
using Abp.UI;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.House.Master;
using DinePlan.DineConnect.Common;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.DayClose;
using Abp.Authorization;
using System.Diagnostics;
using Abp.Extensions;
using DinePlan.DineConnect.Configuration;
using Abp.Configuration;

#endregion

namespace DinePlan.DineConnect.House.Transaction.Implementation
{
    public class InterTransferCrudAppService : DineConnectAppServiceBase, IInterTransferCrudAppService
    {
        private readonly IRepository<InterTransfer> _intertransferRepo;
        private readonly IRepository<InterTransferDetail> _intertransferDetailRepo;
        //private readonly IRepository<PurchaseOrder> _purchaseOrderRepo;
        //private readonly IRepository<PurchaseOrderDetail> _purchaseOrderDetailRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<Material> _materialRepo;
        //private readonly IRepository<MaterialGroupCategory> _materialGroupCategoryRepo;
        //private readonly IRepository<MaterialGroup> _materialgroupRepo;
        private readonly IRepository<Unit> _unitRepo;
        private readonly IRepository<MaterialLocationWiseStock> _materiallocationwisestockRepo;
        private readonly IMaterialAppService _materialAppService;
        //private readonly ILogger _logger;
        private readonly IInterTransferRequestAppService _interTransferRequestAppService;

        // Start Only for Create Or Update
        private readonly IDayCloseAppService _dayCloseAppService;
        private readonly IRepository<Template> _templateRepo;
        private readonly IRepository<UnitConversion> _unitconversionRepo;
        private readonly IHouseReportAppService _housereportAppService;
        private readonly IXmlAndJsonConvertor _xmlandjsonConvertorAppService;
        // Stop

        private readonly IInterTransferListExcelExporter _intertransferExporter;
        private readonly IRepository<InterTransferReceivedDetail> _intertransferReceivedDetailRepo;

        private readonly IAdjustmentAppService _adjustmentAppService;

        public InterTransferCrudAppService(
           IRepository<InterTransfer> interTransferRepo,
           IRepository<InterTransferDetail> intertransferDetailRepo,
           IRepository<Material> materialRepo,
           IRepository<Location> locationRepo,
           IMaterialAppService materialAppService,
           IRepository<Unit> unitRepo,
           IRepository<MaterialLocationWiseStock> materiallocationwisestockRepo,
           //IRepository<MaterialGroupCategory> materialGroupCategoryRepo,
           //IRepository<MaterialGroup> materialgroupRepo,
           //IRepository<PurchaseOrder> purchaseOrderRepo,
           //IRepository<PurchaseOrderDetail> purchaseOrderDetailRepo,
           //ILogger logger,

            //
            IDayCloseAppService dayCloseAppService,
            IRepository<Template> templateRepo,
            IRepository<UnitConversion> unitconversionRepo,
            IHouseReportAppService housereportAppService,
            IXmlAndJsonConvertor xmlandjsonConvertorAppService,
           //
            IInterTransferListExcelExporter intertransferExporter,
            IInterTransferRequestAppService interTransferRequestAppService,
            IRepository<InterTransferReceivedDetail> intertransferReceivedDetailRepo,
            IAdjustmentAppService adjustmentAppService

            )
        {
            _intertransferRepo = interTransferRepo;
            _intertransferDetailRepo = intertransferDetailRepo;
            _locationRepo = locationRepo;
            _materialRepo = materialRepo;
            _materialAppService = materialAppService;
            _unitRepo = unitRepo;
            _materiallocationwisestockRepo = materiallocationwisestockRepo;
            //_materialGroupCategoryRepo = materialGroupCategoryRepo;
            //_materialgroupRepo = materialgroupRepo;
            //_purchaseOrderRepo = purchaseOrderRepo;
            //_purchaseOrderDetailRepo = purchaseOrderDetailRepo;
            //_logger = logger;
            //
            _dayCloseAppService = dayCloseAppService;
            _templateRepo = templateRepo;
            _unitconversionRepo = unitconversionRepo;
            _housereportAppService = housereportAppService;
            _xmlandjsonConvertorAppService = xmlandjsonConvertorAppService;
            //
            _intertransferExporter = intertransferExporter;
            _interTransferRequestAppService = interTransferRequestAppService;
            _intertransferReceivedDetailRepo = intertransferReceivedDetailRepo;

            _adjustmentAppService = adjustmentAppService;
        }

        public async Task<IdInput> CreateOrUpdateInterTransfer(CreateOrUpdateInterTransferInput input)
        {
            Debug.WriteLine("InterTransferCrudAppService CreateOrUpdateInterTransfer() Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));

            if (input.InterTransfer.DeliveryDateRequested == DateTime.MinValue)
                input.InterTransfer.DeliveryDateRequested = input.InterTransfer.RequestDate;
            if (input.InterTransfer.DeliveryDateRequested.Date <= DateTime.Today.AddDays(1).Date)
            {
                var elapsedTimeAlert = await _interTransferRequestAppService.GetRequestTimeStatus(input.InterTransfer.LocationRefId);
                if (elapsedTimeAlert.AllowFlag == false)
                {
                    throw new UserFriendlyException(elapsedTimeAlert.AlertMsg);
                }
            }
            if (input.InterTransfer.CreatorUserId.HasValue && input.InterTransfer.CreatorUserId > 0 && input.InterTransfer.CreatorUserId != AbpSession.UserId)
            {
                var existUser = await UserManager.GetUserByIdAsync((long)input.InterTransfer.CreatorUserId);
                var currentUser = await GetCurrentUserAsync();
                throw new UserFriendlyException(L("UserIdChangedErrorAlert", existUser.UserName, currentUser.UserName));
            }

            if (input.InterTransfer.Id.HasValue)
            {
                return await UpdateInterTransfer(input);
            }
            else
            {
                #region Check Material Active Status in Request Destination Location
                List<int> arrMaterialRefIds = input.InterTransferDetail.Select(t => t.MaterialRefId).ToList();
                var destinationLocationMaterialStatus = await _materiallocationwisestockRepo.GetAll().Where(t => t.LocationRefId == input.InterTransfer.RequestLocationRefId && arrMaterialRefIds.Contains(t.MaterialRefId) && t.IsActiveInLocation == false).ToListAsync();
                if (destinationLocationMaterialStatus.Count > 0)
                {
                    var reqLocation = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.InterTransfer.RequestLocationRefId)
;
                    arrMaterialRefIds = destinationLocationMaterialStatus.Select(t => t.MaterialRefId).ToList();
                    var inactiveMaterilas = await _materialRepo.GetAll().Where(t => arrMaterialRefIds.Contains(t.Id)).Select(t => t.MaterialName).ToListAsync();
                    string errorMessage = string.Join(",", inactiveMaterilas);
                    errorMessage = L("Material") + " " + L("Dorment") + " " + L("Status") + " " + L("In") + " " + L("Location") + " : " + reqLocation.Name + System.Environment.NewLine + errorMessage;
                    throw new UserFriendlyException(errorMessage);
                }
                #endregion

                var ip = input;
                if (ip.InterTransfer.DeliveryDateRequested == DateTime.MinValue)
                    ip.InterTransfer.DeliveryDateRequested = ip.InterTransfer.RequestDate;

                await CreateTemplate(ip);
                DateTime requestTime = DateTime.Now;

                var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.InterTransfer.LocationRefId);
                var reqTransactionDate = loc.HouseTransactionDate;

                if (reqTransactionDate == null)
                {
                    var dayClose = _dayCloseAppService.GetDayClose(new IdInput { Id = input.InterTransfer.LocationRefId });
                    requestTime = dayClose.Result.TransactionDate;
                }
                else if (requestTime != null)
                {
                    if (requestTime.ToString("yyyy-MMM-dd") != reqTransactionDate.Value.ToString("yyyy-MMM-dd"))
                    {
                        requestTime = reqTransactionDate.Value;
                    }
                }

                input.InterTransfer.RequestDate = requestTime;

                IdInput interid = await CreateInterTransfer(input);
                string OperationToBeDone = input.SaveStatus;

                if (input.SaveStatus != null && (OperationToBeDone.Equals("DA") || OperationToBeDone.Equals("DR")))
                {
                    input.SaveStatus = "SAVE";
                    input.InterTransfer.Id = interid.Id;

                    DateTime completedTime = DateTime.Now;

                    var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.InterTransfer.RequestLocationRefId);
                    var transactionDate = location.HouseTransactionDate;

                    if (transactionDate == null)
                    {
                        var dayClose = _dayCloseAppService.GetDayClose(new IdInput { Id = input.InterTransfer.RequestLocationRefId });
                        completedTime = dayClose.Result.TransactionDate;
                    }
                    else if (transactionDate != null)
                    {
                        if (completedTime.ToString("yyyy-MMM-dd") != transactionDate.Value.ToString("yyyy-MMM-dd"))
                        {
                            completedTime = transactionDate.Value;
                        }
                    }

                    input.InterTransfer.CompletedTime = completedTime;

                    List<Location> l = await _locationRepo.GetAll().Where(t => t.Id == input.InterTransfer.RequestLocationRefId).ToListAsync();
                    List<LocationListDto> locinput = l.MapTo<List<LocationListDto>>();
                    List<int> materialRefIdsRateRequired = input.InterTransferDetail.Select(t => t.MaterialRefId).ToList();

                    var matView = await _housereportAppService.GetMaterialRateView(new GetHouseReportMaterialRateInput
                    {
                        StartDate = completedTime,
                        EndDate = completedTime,
                        Locations = locinput,
                        MaterialRefIds = materialRefIdsRateRequired,
                        FunctionCalledBy = "Inter Transfer Create"
                    });

                    var materialRate = matView.MaterialRateView;

                    //var rsUnitConversion = await _unitconversionRepo.GetAllListAsync();
                    var rsUnitConversion = await _unitconversionRepo.GetAll().Select(t => new { t.BaseUnitId, t.RefUnitId, t.Conversion, t.MaterialRefId }).ToListAsync();
                    foreach (var lst in input.InterTransferDetail)
                    {
                        var matprice = materialRate.FirstOrDefault(t => t.MaterialRefId == lst.MaterialRefId);

                        lst.CurrentStatus = L("Approved");
                        if (lst.DefaultUnitId == 0)
                        {
                            var mat = await _materialRepo.FirstOrDefaultAsync(t => t.Id == lst.MaterialRefId);
                            lst.DefaultUnitId = mat.DefaultUnitId;
                        }

                        decimal conversionFactor = 1;
                        if (lst.DefaultUnitId == lst.UnitRefId)
                            conversionFactor = 1;
                        else
                        {
                            var conv = rsUnitConversion.First(t => t.BaseUnitId == lst.DefaultUnitId && t.RefUnitId == lst.UnitRefId && t.MaterialRefId == lst.MaterialRefId);
                            if (conv == null)
                            {
                                conv = rsUnitConversion.First(t => t.BaseUnitId == lst.DefaultUnitId && t.RefUnitId == lst.UnitRefId);
                            }
                            if (conv != null)
                            {
                                conversionFactor = conv.Conversion;
                            }
                            else
                            {
                                var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == lst.DefaultUnitId);
                                if (baseUnit == null)
                                {
                                    throw new UserFriendlyException(L("UnitIdDoesNotExist", lst.DefaultUnitId));
                                }
                                var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == lst.UnitRefId);
                                if (refUnit == null)
                                {
                                    throw new UserFriendlyException(L("UnitIdDoesNotExist", lst.UnitRefId));
                                }
                                throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
                            }

                            lst.OnHand = lst.OnHand * conversionFactor;
                        }

                        var templst = lst;

                        if (lst.Price > 0 && OperationToBeDone.Equals("DR"))
                        {
                            //  Do Nothing , Same Rate Based Received Price
                        }
                        else
                        {
                            if (lst.DefaultUnitId == lst.UnitRefId)
                                lst.Price = matprice.AvgRate;
                            else
                            {
                                lst.Price = Math.Round(matprice.AvgRate * 1 / conversionFactor, 3);
                            }
                        }

                        lst.LineTotal = Math.Round(lst.IssueQty * lst.Price, 3);

                    }

                    IdInput approvedId = await CreateOrUpdateApproval(input);

                    if (OperationToBeDone.Equals("DR"))
                    {
                        CreateOrUpdateInterReceivedInput inputforReceived = new CreateOrUpdateInterReceivedInput();
                        inputforReceived.InterTransfer = input.InterTransfer;
                        inputforReceived.SaveStatus = "V";
                        inputforReceived.InterTransfer.ReceivedTime = input.InterTransfer.RequestDate;

                        List<InterTransferReceivedEditDto> reclist = new List<InterTransferReceivedEditDto>();

                        int sno = 1;
                        foreach (var approvedmat in input.InterTransferDetail)
                        {
                            InterTransferReceivedEditDto newApprovalitem = new InterTransferReceivedEditDto
                            {
                                InterTransferRefId = approvedId.Id,
                                Sno = sno,
                                MaterialRefId = approvedmat.MaterialRefId,
                                MaterialRefName = approvedmat.MaterialRefName,
                                RequestQty = approvedmat.RequestQty,
                                IssueQty = approvedmat.IssueQty,
                                ReceivedQty = approvedmat.IssueQty,
                                Price = approvedmat.Price,
                                ReceivedValue = Math.Round(approvedmat.IssueQty * approvedmat.Price, 3),
                                CurrentStatus = L("Received"),
                                Uom = approvedmat.Uom,
                                UnitRefId = approvedmat.UnitRefId,
                            };

                            reclist.Add(newApprovalitem);
                            sno++;
                        }

                        inputforReceived.InterTransferReceivedDetail = reclist;

                        InterTransferReceivedWithAdjustmentId recid = await CreateOrUpdateInterReceived(inputforReceived);
                    }
                }

                return interid;// await CreateInterTransfer(input);
            }

        }

        protected virtual async Task<IdInput> CreateInterTransfer(CreateOrUpdateInterTransferInput input)
        {
            var dto = input.InterTransfer.MapTo<InterTransfer>();
            if (dto.CurrentStatus != "Q")
                dto.CurrentStatus = "P";

            var retId = await _intertransferRepo.InsertOrUpdateAndGetIdAsync(dto);

            foreach (InterTransferDetailViewDto items in input.InterTransferDetail.ToList())
            {
                InterTransferDetail itdDto = new InterTransferDetail();

                itdDto.InterTransferRefId = retId;
                itdDto.Sno = items.Sno;
                itdDto.MaterialRefId = items.MaterialRefId;
                itdDto.RequestQty = items.RequestQty;
                itdDto.IssueQty = items.IssueQty;
                itdDto.UnitRefId = items.UnitRefId;
                itdDto.Price = items.Price;
                itdDto.CurrentStatus = dto.CurrentStatus;
                itdDto.RequestRemarks = items.RequestRemarks;
                itdDto.ApprovedRemarks = items.ApprovedRemarks;

                var retDetailId = await _intertransferDetailRepo.InsertAndGetIdAsync(itdDto);
            }

            string templateJson = _xmlandjsonConvertorAppService.SerializeToJSON(input);

            if (input.SaveStatus != null)
            {
                if (input.SaveStatus.Equals("DA") || input.SaveStatus.Equals("DR"))
                    input.Templatesave = null;
            }
            if (input.Templatesave != null)
            {
                Template lastTemplate;
                int? templateScope;
                if (input.Templatesave.Templatescope == true)
                {
                    templateScope = null;
                }
                else
                {
                    templateScope = input.InterTransfer.LocationRefId;
                }

                if (input.Templatesave.Templateflag == true)
                {
                    string repName = input.Templatesave.Templatename;

                    var templateExist = _templateRepo.GetAll().Where(t => t.Name == repName && t.TemplateType == (int)TemplateType.RequestInterLocation).ToList();
                    if (templateExist.Count == 0)
                    {
                        lastTemplate = new Template
                        {
                            Name = input.Templatesave.Templatename,
                            TemplateType = (int)TemplateType.RequestInterLocation,
                            TemplateData = templateJson,
                            LocationRefId = templateScope
                        };
                    }
                    else
                    {
                        lastTemplate = templateExist[0];
                        lastTemplate.LocationRefId = templateScope;
                        lastTemplate.TemplateData = templateJson;
                    }
                }
                else
                {
                    string repName = L("LastRequest");
                    var templateExist = _templateRepo.GetAll().Where(t => t.Name == repName && t.TemplateType == (int)TemplateType.RequestInterLocation).ToList();
                    if (templateExist.Count() == 0)
                    {
                        lastTemplate = new Template
                        {
                            Name = L("LastRequest"),
                            TemplateType = (int)TemplateType.RequestInterLocation,
                            TemplateData = templateJson,
                            LocationRefId = input.InterTransfer.LocationRefId
                        };
                    }
                    else
                    {
                        lastTemplate = templateExist[0];
                        lastTemplate.LocationRefId = templateScope;
                        lastTemplate.TemplateData = templateJson;
                    }
                }

                await _templateRepo.InsertOrUpdateAndGetIdAsync(lastTemplate);
            }


            return new IdInput
            {
                Id = retId
            };

        }


        protected virtual async Task<IdInput> UpdateInterTransfer(CreateOrUpdateInterTransferInput input)
        {
            var item = await _intertransferRepo.GetAsync(input.InterTransfer.Id.Value);
            var dto = input.InterTransfer;

            item.LocationRefId = dto.LocationRefId;
            item.RequestLocationRefId = dto.RequestLocationRefId;
            item.CurrentStatus = dto.CurrentStatus;
            item.RequestDate = dto.RequestDate;
            item.DeliveryDateRequested = dto.DeliveryDateRequested;
            item.DocReferenceNumber = dto.DocReferenceNumber;

            //  Update Detail Link
            List<int> materialsToBeRetained = new List<int>();

            if (input.InterTransferDetail != null && input.InterTransferDetail.Count > 0)
            {
                foreach (var items in input.InterTransferDetail)
                {
                    int materialrefid = items.MaterialRefId;
                    materialsToBeRetained.Add(materialrefid);

                    var existingDetailEntry = _intertransferDetailRepo.GetAllList(u => u.InterTransferRefId == dto.Id && u.MaterialRefId.Equals(materialrefid));

                    if (existingDetailEntry.Count == 0)  //  Add New Material
                    {
                        InterTransferDetail itdDto = new InterTransferDetail();

                        itdDto.InterTransferRefId = (int)dto.Id;
                        itdDto.Sno = items.Sno;
                        itdDto.MaterialRefId = items.MaterialRefId;
                        itdDto.RequestQty = items.RequestQty;
                        itdDto.IssueQty = items.IssueQty;
                        itdDto.UnitRefId = items.UnitRefId;
                        itdDto.Price = items.Price;
                        itdDto.IssueValue = Math.Round(items.IssueQty * items.Price, 3);
                        itdDto.CurrentStatus = "P";
                        itdDto.RequestRemarks = items.RequestRemarks;
                        itdDto.ApprovedRemarks = items.ApprovedRemarks;

                        var retDetailId = await _intertransferDetailRepo.InsertAndGetIdAsync(itdDto);
                    }
                    else
                    {
                        var itdDto = await _intertransferDetailRepo.GetAsync(existingDetailEntry[0].Id);
                        string currentStatus;

                        itdDto.InterTransferRefId = (int)dto.Id;
                        itdDto.Sno = items.Sno;
                        itdDto.MaterialRefId = items.MaterialRefId;
                        itdDto.RequestQty = items.RequestQty;
                        itdDto.IssueQty = items.IssueQty;
                        itdDto.UnitRefId = items.UnitRefId;
                        itdDto.Price = items.Price;
                        itdDto.IssueValue = Math.Round(items.IssueQty * items.Price, 3);

                        currentStatus = "P";
                        if (items.CurrentStatus == L("Pending"))
                            currentStatus = "P";
                        else if (items.CurrentStatus == L("Approved"))
                            currentStatus = "A";
                        else if (items.CurrentStatus == L("Excess"))
                            currentStatus = "E";
                        else if (items.CurrentStatus == L("Rejected"))
                            currentStatus = "R";


                        itdDto.CurrentStatus = currentStatus;
                        itdDto.RequestRemarks = items.RequestRemarks;
                        itdDto.ApprovedRemarks = items.ApprovedRemarks;

                        var retId2 = await _intertransferDetailRepo.InsertOrUpdateAndGetIdAsync(itdDto);

                    }
                }
            }

            var delBrandList = _intertransferDetailRepo.GetAll().Where(a => a.InterTransferRefId == input.InterTransfer.Id.Value && !materialsToBeRetained.Contains(a.MaterialRefId)).ToList();

            foreach (var a in delBrandList)
            {
                _intertransferDetailRepo.Delete(a.Id);
            }

            //await CreateTemplate(input);

            return new IdInput
            {
                Id = input.InterTransfer.Id.Value
            };

        }

        protected virtual async Task CreateTemplate(CreateOrUpdateInterTransferInput input)
        {
            if (input.Templatesave == null)
                return;
            string templateJson = _xmlandjsonConvertorAppService.SerializeToJSON(input);
            Template lastTemplate;
            int? templateScope;
            if (input.Templatesave.Templatescope == true)
            {
                templateScope = null;
            }
            else
            {
                if (input.Templatesave.TemplateType == (int)TemplateType.DirectInterTransfer)
                    templateScope = input.InterTransfer.RequestLocationRefId;
                else
                    templateScope = input.InterTransfer.LocationRefId;
            }

            if (input.Templatesave.Templateflag == true)
            {
                string repName = input.Templatesave.Templatename;

                var templateExist = _templateRepo.GetAll().Where(t => t.Name == repName
                    && t.TemplateType == input.Templatesave.TemplateType).ToList();
                if (templateExist.Count == 0)
                {
                    lastTemplate = new Template
                    {
                        Name = input.Templatesave.Templatename,
                        TemplateType = input.Templatesave.TemplateType,
                        TemplateData = templateJson,
                        LocationRefId = templateScope
                    };
                }
                else
                {
                    lastTemplate = templateExist[0];
                    lastTemplate.LocationRefId = templateScope;
                    lastTemplate.TemplateData = templateJson;
                }
            }
            else
            {
                string repName = L("LastRequestDraft");
                var templateExist = _templateRepo.GetAll().Where(t => t.Name == repName && t.TemplateType == (int)TemplateType.RequestInterLocation).ToList();
                if (templateExist.Count() == 0)
                {
                    lastTemplate = new Template
                    {
                        Name = L("LastRequestDraft"),
                        TemplateType = (int)TemplateType.RequestInterLocation,
                        TemplateData = templateJson,
                        LocationRefId = input.InterTransfer.LocationRefId
                    };
                }
                else
                {
                    lastTemplate = templateExist[0];
                    lastTemplate.LocationRefId = templateScope;
                    lastTemplate.TemplateData = templateJson;
                }
            }
            await _templateRepo.InsertOrUpdateAndGetIdAsync(lastTemplate);
        }

        public async Task<IdInput> CreateOrUpdateApproval(CreateOrUpdateInterTransferInput input)
        {
            return await UpdateApproval(input);
        }

        protected virtual async Task<IdInput> UpdateApproval(CreateOrUpdateInterTransferInput input)
        {
            Debug.WriteLine("InterTransferCrudAppService UpdateApproval() Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
            var item = await _intertransferRepo.GetAsync(input.InterTransfer.Id.Value);
            var dto = input.InterTransfer;

            if (input.SaveStatus.Equals("SAVE"))
            {
                dto.CurrentStatus = "A";
            }
            else if (input.SaveStatus.Equals("DRAFT"))
            {
                dto.CompletedTime = null;
                dto.CurrentStatus = "D";
            }
            else if (input.SaveStatus.Equals("REJECT"))
            {
                //dto.CompletedTime = DateTime.Now;
                dto.CurrentStatus = "R";
            }

            item.CurrentStatus = dto.CurrentStatus;
            item.ApprovedPersonId = dto.ApprovedPersonId;

            item.CompletedTime = dto.CompletedTime;
            item.PersonInCharge = dto.PersonInCharge;
            item.VehicleNumber = dto.VehicleNumber;
            item.DeliveryDateRequested = dto.DeliveryDateRequested;
            decimal TotalTransferValue = 0;

            bool CanChangeRequestQty = false;
            if (PermissionChecker.IsGranted("Pages.Tenant.House.Transaction.InterTransferApproval.CanChangeRequestQuantity"))
            {
                CanChangeRequestQty = true;
            }

            List<InterTransferDetailViewDto> intertransferDetailDtos = new List<InterTransferDetailViewDto>();

            int interSno = 1;
            if (input.InterTransferDetail != null && input.InterTransferDetail.Count > 0)
            {
                foreach (var items in input.InterTransferDetail)
                {
                    int materialrefid = items.MaterialRefId;

                    var existingDetailEntry = _intertransferDetailRepo.GetAllList(u => u.InterTransferRefId == dto.Id && u.MaterialRefId.Equals(materialrefid));

                    var itdDto = await _intertransferDetailRepo.GetAsync(existingDetailEntry[0].Id);
                    string currentStatus;

                    //if (CanChangeRequestQty == true && items.RequestQty > 0)
                    //    itdDto.RequestQty = items.RequestQty;

                    itdDto.IssueQty = items.IssueQty;
                    itdDto.Price = items.Price;
                    itdDto.IssueValue = Math.Round(items.IssueQty * items.Price, 3);
                    TotalTransferValue = TotalTransferValue + itdDto.IssueValue;

                    currentStatus = "";
                    if (items.CurrentStatus == L("Pending"))
                        currentStatus = "P";
                    else if (items.CurrentStatus == L("Approved"))
                        currentStatus = "A";
                    else if (items.CurrentStatus == L("Excess"))
                        currentStatus = "E";
                    else if (items.CurrentStatus == L("Rejected"))
                        currentStatus = "R";
                    else if (items.CurrentStatus == L("Shortage"))
                        currentStatus = "S";

                    itdDto.CurrentStatus = currentStatus;
                    itdDto.RequestRemarks = items.RequestRemarks;
                    itdDto.ApprovedRemarks = items.ApprovedRemarks;

                    var retId2 = await _intertransferDetailRepo.InsertOrUpdateAndGetIdAsync(itdDto);

                    if (input.SaveStatus.Equals("SAVE") && (itdDto.CurrentStatus.Equals("A") || itdDto.CurrentStatus.Equals("E") || itdDto.CurrentStatus.Equals("S")))
                    {
                        MaterialLedgerDto ml = new MaterialLedgerDto();
                        ml.LocationRefId = item.RequestLocationRefId;
                        ml.MaterialRefId = items.MaterialRefId;
                        ml.LedgerDate = (DateTime)dto.CompletedTime;
                        ml.TransferOut = items.IssueQty;
                        ml.UnitId = items.UnitRefId;

                        var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                        itdDto.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;
                    }

                    #region InterTransferReturn
                    InterTransferDetailViewDto trndet = new InterTransferDetailViewDto();
                    if (items.HoldFlag)
                    {
                        decimal requestQty = items.HoldQty;

                        if (items.UnitRefName.Length == 0 || items.UnitRefId == 0)
                        {
                            throw new UserFriendlyException("UnitErr");
                        }

                        string remarksInterTransfer = L("TransferHold", input.InterTransfer.Id, items.IssueQty, items.RequestQty, items.UnitRefName);

                        trndet.Sno = interSno++;
                        trndet.MaterialRefId = materialrefid;
                        trndet.RequestQty = requestQty;
                        trndet.IssueQty = 0;
                        trndet.DefaultUnitId = items.DefaultUnitId;
                        trndet.UnitRefId = items.UnitRefId;
                        trndet.Price = items.Price;
                        trndet.CurrentStatus = "P";
                        trndet.ApprovedRemarks = remarksInterTransfer;
                        trndet.RequestRemarks = remarksInterTransfer;
                        intertransferDetailDtos.Add(trndet);
                    }


                    #endregion
                }
            }

            CreateOrUpdateInterTransferInput newReturnInterTransferDto = new CreateOrUpdateInterTransferInput();
            IdInput transferIdInput = new IdInput();
            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.InterTransfer.RequestLocationRefId);

            if (intertransferDetailDtos.Count > 0)
            {
                InterTransferEditDto masInterDto = new InterTransferEditDto();

                masInterDto.RequestDate = loc.HouseTransactionDate.Value.Date;
                masInterDto.LocationRefId = input.InterTransfer.LocationRefId;
                masInterDto.RequestLocationRefId = input.InterTransfer.RequestLocationRefId;
                masInterDto.CurrentStatus = "D";
                masInterDto.ApprovedPersonId = (int)AbpSession.UserId;
                masInterDto.ReceivedPersonId = (int)AbpSession.UserId;

                newReturnInterTransferDto.InterTransfer = masInterDto;
                newReturnInterTransferDto.InterTransferDetail = intertransferDetailDtos;
                newReturnInterTransferDto.SaveStatus = "";

                transferIdInput = await CreateOrUpdateInterTransfer(newReturnInterTransferDto);
            }

            item.TransferValue = TotalTransferValue;

            Debug.WriteLine("InterTransferCrudAppService UpdateApproval() End Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
            return new IdInput
            {
                Id = input.InterTransfer.Id.Value
            };
        }


        public async Task<InterTransferReceivedWithAdjustmentId> CreateOrUpdateInterReceived(CreateOrUpdateInterReceivedInput input)
        {
            return await UpdateInterReceived(input);
        }

        protected virtual async Task<InterTransferReceivedWithAdjustmentId> UpdateInterReceived(CreateOrUpdateInterReceivedInput input)
        {
            var item = await _intertransferRepo.GetAsync(input.InterTransfer.Id.Value);
            var dto = input.InterTransfer;

            item.CurrentStatus = input.SaveStatus;
            item.ReceivedTime = dto.ReceivedTime; //  DateTime.Now;
            item.ReceivedPersonId = dto.ReceivedPersonId;

            List<AdjustmentDetailViewDto> adjustmentDetailDtos = new List<AdjustmentDetailViewDto>();
            List<InterTransferDetailViewDto> intertransferDetailDtos = new List<InterTransferDetailViewDto>();


            //  Update Detail Link
            //List<int> materialsToBeRetained = new List<int>();

            decimal TotalReceivedValue = 0;
            int adjSno = 1;
            int interSno = 1;

            if (input.InterTransferReceivedDetail != null && input.InterTransferReceivedDetail.Count > 0)
            {
                foreach (var items in input.InterTransferReceivedDetail)
                {
                    int materialrefid = items.MaterialRefId;

                    //materialsToBeRetained.Add(materialrefid);

                    var existingDetailEntry = _intertransferReceivedDetailRepo.GetAllList(u => u.InterTransferRefId == dto.Id && u.MaterialRefId.Equals(materialrefid));

                    if (existingDetailEntry.Count == 0)  //  Add New Material
                    {

                        InterTransferReceivedDetail itdDto = new InterTransferReceivedDetail();

                        itdDto.InterTransferRefId = (int)dto.Id;
                        itdDto.Sno = items.Sno;
                        itdDto.MaterialRefId = items.MaterialRefId;
                        itdDto.ReceivedQty = items.ReceivedQty;
                        itdDto.AdjustmentMode = items.AdjustmentMode;
                        itdDto.AdjustmentQty = items.AdjustmentQty;
                        itdDto.ReturnQty = items.ReturnQty;
                        itdDto.UnitRefId = items.UnitRefId;
                        itdDto.Price = items.Price;
                        itdDto.ReceivedValue = Math.Round(items.ReceivedQty * items.Price, 3);
                        TotalReceivedValue = TotalReceivedValue + itdDto.ReceivedValue;
                        string currentStatus = "";
                        if (items.CurrentStatus == L("Pending"))
                            currentStatus = "P";
                        else if (items.CurrentStatus == L("Excess"))
                            currentStatus = "E";
                        else if (items.CurrentStatus == L("Shortage"))
                            currentStatus = "S";
                        else if (items.CurrentStatus == L("Rejected"))
                            currentStatus = "R";
                        else if (items.CurrentStatus == L("Received"))
                            currentStatus = "V";

                        if (currentStatus == "" || currentStatus.Length == 0)
                        {
                            if (items.ReceivedQty == 0)
                            {
                                continue;
                            }
                            throw new UserFriendlyException(L("EnterQtyWithMaterialName", items.MaterialRefName));
                        }

                        itdDto.CurrentStatus = currentStatus;

                        var retDetailId = await _intertransferReceivedDetailRepo.InsertAndGetIdAsync(itdDto);

                        if (currentStatus.Equals("V") || currentStatus.Equals("E") || currentStatus.Equals("S"))
                        {
                            MaterialLedgerDto ml = new MaterialLedgerDto();
                            ml.LocationRefId = dto.LocationRefId;
                            ml.MaterialRefId = items.MaterialRefId;
                            ml.LedgerDate = (DateTime)item.ReceivedTime;
                            ml.TransferIn = items.ReceivedQty;
                            ml.UnitId = items.UnitRefId;

                            var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                            itdDto.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;
                        }

                        #region Adjustment
                        AdjustmentDetailViewDto det = new AdjustmentDetailViewDto();
                        if (items.AdjustmentFlag)
                        {
                            decimal adjustmentQty = items.AdjustmentQty;
                            string adjustmentMode = items.AdjustmentMode;

                            decimal receivedQty = 0;
                            if (adjustmentMode.Equals(L("Shortage")))
                                receivedQty = items.ReceivedQty - items.AdjustmentQty;
                            else if (adjustmentMode.Equals(L("Excess")))
                            {
                                receivedQty = items.ReceivedQty + items.AdjustmentQty;
                            }

                            if (items.UnitRefName.Length == 0 || items.UnitRefId == 0)
                            {
                                throw new UserFriendlyException("UnitErr");
                            }

                            string adjustmentApprovedRemarks = L("TransferAdjustmemt", input.InterTransfer.Id, items.IssueQty, receivedQty, items.UnitRefName);

                            det.MaterialRefId = materialrefid;
                            det.AdjustmentQty = adjustmentQty;
                            det.AdjustmentApprovedRemarks = adjustmentApprovedRemarks;
                            det.AdjustmentMode = adjustmentMode;
                            det.UnitRefId = items.UnitRefId;
                            det.Sno = adjSno++;

                            adjustmentDetailDtos.Add(det);
                        }
                        #endregion

                        #region InterTransferReturn
                        InterTransferDetailViewDto trndet = new InterTransferDetailViewDto();
                        if (items.ReturnFlag)
                        {
                            decimal requestQty = items.ReceivedQty - items.ReturnQty;

                            if (items.UnitRefName.Length == 0 || items.UnitRefId == 0)
                            {
                                throw new UserFriendlyException("UnitErr");
                            }

                            string remarksInterTransfer = L("TransferAdjustmemt", input.InterTransfer.Id, items.IssueQty, requestQty, items.UnitRefName);

                            trndet.Sno = interSno++;
                            trndet.MaterialRefId = materialrefid;
                            trndet.RequestQty = items.ReturnQty;
                            trndet.IssueQty = items.ReturnQty;
                            trndet.DefaultUnitId = items.DefaultUnitId;
                            trndet.UnitRefId = items.UnitRefId;
                            trndet.Price = items.Price;
                            trndet.CurrentStatus = "A";
                            trndet.ApprovedRemarks = remarksInterTransfer;
                            trndet.RequestRemarks = remarksInterTransfer;



                            intertransferDetailDtos.Add(trndet);
                        }


                        #endregion

                    }
                    else
                    {
                        var itdDto = await _intertransferReceivedDetailRepo.GetAsync(existingDetailEntry[0].Id);
                        string currentStatus;

                        decimal differcenceQty;
                        differcenceQty = items.ReceivedQty - itdDto.ReceivedQty;

                        itdDto.InterTransferRefId = (int)dto.Id;
                        itdDto.Sno = items.Sno;
                        itdDto.MaterialRefId = items.MaterialRefId;
                        itdDto.ReceivedQty = items.ReceivedQty;
                        itdDto.UnitRefId = items.UnitRefId;
                        itdDto.Price = items.Price;
                        itdDto.ReceivedValue = Math.Round(items.ReceivedQty * items.Price, 3);

                        TotalReceivedValue = TotalReceivedValue + itdDto.ReceivedValue;

                        currentStatus = "";
                        if (items.CurrentStatus == L("Pending"))
                            currentStatus = "P";
                        else if (items.CurrentStatus == L("Excess"))
                            currentStatus = "E";
                        else if (items.CurrentStatus == L("Shortage"))
                            currentStatus = "S";
                        else if (items.CurrentStatus == L("Rejected"))
                            currentStatus = "R";
                        else if (items.CurrentStatus == L("Received"))
                            currentStatus = "V";


                        itdDto.CurrentStatus = currentStatus;

                        var retId2 = await _intertransferReceivedDetailRepo.InsertOrUpdateAndGetIdAsync(itdDto);

                        if (currentStatus.Equals("V") || currentStatus.Equals("E") || currentStatus.Equals("S"))
                        {
                            MaterialLedgerDto ml = new MaterialLedgerDto();
                            ml.LocationRefId = dto.RequestLocationRefId;
                            ml.MaterialRefId = items.MaterialRefId;
                            ml.LedgerDate = (DateTime)item.ReceivedTime;
                            ml.TransferIn = differcenceQty;
                            ml.UnitId = items.UnitRefId;

                            var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                            itdDto.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;
                        }

                    }
                }
            }

            item.ReceivedValue = TotalReceivedValue;
            IdInput adjId = new IdInput { Id = 0 };

            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.InterTransfer.LocationRefId);
            CreateOrUpdateAdjustmentInput newAdjDto = new CreateOrUpdateAdjustmentInput();
            if (adjustmentDetailDtos.Count > 0)
            {
                AdjustmentEditDto masDto = new AdjustmentEditDto();

                if (input.InterTransfer.ReceivedTime == null)
                {
                    masDto.AdjustmentDate = loc.HouseTransactionDate.Value.Date;
                }
                else
                    masDto.AdjustmentDate = input.InterTransfer.ReceivedTime.Value.Date;

                masDto.AdjustmentRemarks = L("TransferReceivedDifference");
                masDto.ApprovedPersonId = (int)AbpSession.UserId;
                masDto.LocationRefId = dto.LocationRefId;
                masDto.EnteredPersonId = (int)AbpSession.UserId;
                masDto.TokenRefNumber = 999;

                newAdjDto.Adjustment = masDto;
                newAdjDto.AdjustmentDetail = adjustmentDetailDtos;

                adjId = await _adjustmentAppService.CreateOrUpdateAdjustment(newAdjDto);
            }

            CreateOrUpdateInterTransferInput newReturnInterTransferDto = new CreateOrUpdateInterTransferInput();
            IdInput transferIdInput = new IdInput();

            if (intertransferDetailDtos.Count > 0)
            {
                InterTransferEditDto masInterDto = new InterTransferEditDto();

                var loc1 = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.InterTransfer.RequestLocationRefId);

                masInterDto.RequestDate = loc1.HouseTransactionDate.Value.Date;
                masInterDto.LocationRefId = input.InterTransfer.RequestLocationRefId;
                masInterDto.RequestLocationRefId = input.InterTransfer.LocationRefId;
                masInterDto.CurrentStatus = "A";
                masInterDto.ApprovedPersonId = (int)AbpSession.UserId;
                masInterDto.ReceivedPersonId = (int)AbpSession.UserId;

                newReturnInterTransferDto.InterTransfer = masInterDto;
                newReturnInterTransferDto.InterTransferDetail = intertransferDetailDtos;
                newReturnInterTransferDto.SaveStatus = "DR";

                transferIdInput = await CreateOrUpdateInterTransfer(newReturnInterTransferDto);
            }

            if (adjId.Id > 0)
                item.AdjustmentRefId = adjId.Id;

            if (transferIdInput.Id > 0)
                item.InterTransferRefId = transferIdInput.Id;

            var locReq = await _locationRepo.FirstOrDefaultAsync(t => t.Id == dto.RequestLocationRefId);
            item.InterTransferReceivedAccountDate = locReq.HouseTransactionDate.Value;

            return new InterTransferReceivedWithAdjustmentId
            {
                Id = input.InterTransfer.Id.Value,
                AdjustmentId = adjId.Id,
                ReturnTransferId = transferIdInput.Id
            };

        }

        public async Task<MessageOutput> BulkAutoApproval(BulkApprovalInputDto input)
        {
            foreach (var lst in input.BulkApprovalInputs)
            {
                var returnId = await TriggerAutoApprovalForGivenTransferRequestId(new IdInput { Id = lst.Id });
            }
            return new MessageOutput { SuccessFlag = true };
        }

        public async Task<IdInput> TriggerAutoApprovalForGivenTransferRequestId(IdInput inputId)
        {
            var rsInterTransfer = await _interTransferRequestAppService.GetInterTransferForEdit(new NullableIdInput { Id = inputId.Id });
            InterTransferEditDto interTransfer = rsInterTransfer.InterTransfer;
            List<InterTransferDetailViewDto> details = rsInterTransfer.InterTransferDetail;
            foreach (var lst in details)
            {
                lst.IssueQty = lst.RequestQty;
                lst.CurrentStatus = L("Approved");
            }

            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == rsInterTransfer.InterTransfer.RequestLocationRefId);
            if (loc.HouseTransactionDate.Value.Date == DateTime.Today.Date)
            {
                interTransfer.CompletedTime = DateTime.Now;
            }
            else
            {
                interTransfer.CompletedTime = loc.HouseTransactionDate.Value;
            }
            CreateOrUpdateInterTransferInput input = new CreateOrUpdateInterTransferInput
            {
                InterTransfer = interTransfer,
                InterTransferDetail = details,
                SaveStatus = "SAVE"
            };
            IdInput approvedId = await CreateOrUpdateApproval(input);
            return approvedId;
        }

        public async Task<IdInput> SplitRequestAsPOBasedDependentAndExistingStockDependent(IdInput input)
        {
            GetInterTransferForEditOutput request = await _interTransferRequestAppService.GetInterTransferForEdit(new NullableIdInput { Id = input.Id });

            InterTransferEditDto master;

            if (request.InterTransfer.Id.HasValue)
            {
                master = request.InterTransfer;
                if (master.CurrentStatus != "P")
                {
                    throw new UserFriendlyException(L("SplitRequestError3"));
                }
                var alreadySplited = await _intertransferRepo.FirstOrDefaultAsync(t => t.ParentRequestRefId == input.Id);
                if (alreadySplited != null)
                {
                    throw new UserFriendlyException(L("SplitRequestError4", input.Id, alreadySplited.Id));
                }
            }
            else
            {
                throw new UserFriendlyException("Id Not Exists");
            }
            CreateOrUpdateInterTransferInput newReturnInterTransferDto = new CreateOrUpdateInterTransferInput();
            IdInput transferIdInput = new IdInput();
            List<InterTransferDetailViewDto> intertransferDetailDtos = new List<InterTransferDetailViewDto>();

            int interSno = 1;
            List<int> itemsToBeMovedOut = new List<int>();

            if (request.InterTransferDetail != null && request.InterTransferDetail.Count > 0)
            {
                var poDependentList = request.InterTransferDetail.Where(t => t.PurchaseOrderRefId.HasValue == true).ToList();
                if (poDependentList.Count == 0)
                {
                    throw new UserFriendlyException(L("SplitRequestError1"));
                }

                var existStockDependetList = request.InterTransferDetail.Where(t => t.PurchaseOrderRefId.HasValue == false).ToList();
                if (existStockDependetList.Count == 0)
                {
                    throw new UserFriendlyException(L("SplitRequestError2"));
                }
                itemsToBeMovedOut = existStockDependetList.Select(t => t.Id.Value).ToList();
                var currentStatus = "";
                foreach (var items in existStockDependetList)
                {
                    InterTransferDetailViewDto trndet = new InterTransferDetailViewDto();
                    {
                        if (items.UnitRefName.Length == 0 || items.UnitRefId == 0)
                        {
                            throw new UserFriendlyException("UnitErr");
                        }
                        if (items.CurrentStatus == L("Pending"))
                            currentStatus = "P";
                        else if (items.CurrentStatus == L("Approved"))
                            currentStatus = "A";
                        else if (items.CurrentStatus == L("Excess"))
                            currentStatus = "E";
                        else if (items.CurrentStatus == L("Rejected"))
                            currentStatus = "R";
                        else if (items.CurrentStatus == L("Shortage"))
                            currentStatus = "S";
                        trndet = items.MapTo<InterTransferDetailViewDto>();
                        trndet.Sno = interSno++;
                        trndet.CurrentStatus = currentStatus;
                        intertransferDetailDtos.Add(trndet);
                    }
                }
            }

            if (intertransferDetailDtos.Count > 0)
            {
                InterTransferEditDto masInterDto = new InterTransferEditDto();

                var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == master.RequestLocationRefId);

                masInterDto.RequestDate = loc.HouseTransactionDate.Value.Date;
                masInterDto.LocationRefId = master.LocationRefId;
                masInterDto.RequestLocationRefId = master.RequestLocationRefId;
                masInterDto.CurrentStatus = "D";
                masInterDto.ApprovedPersonId = (int)AbpSession.UserId;
                masInterDto.ReceivedPersonId = (int)AbpSession.UserId;
                masInterDto.ParentRequestRefId = input.Id;

                newReturnInterTransferDto.InterTransfer = masInterDto;
                newReturnInterTransferDto.InterTransferDetail = intertransferDetailDtos;
                newReturnInterTransferDto.SaveStatus = "";

                transferIdInput = await CreateOrUpdateInterTransfer(newReturnInterTransferDto);
            }

            var delList = await _intertransferDetailRepo.GetAllListAsync(t => t.InterTransferRefId == input.Id && itemsToBeMovedOut.Contains(t.Id));
            foreach (var lst in delList)
            {
                await _intertransferDetailRepo.DeleteAsync(lst.Id);
            }
            return transferIdInput;
        }


        public async Task<GetInterTransferForEditOutput> GetInterTransferApprovalForEdit(GetInterApprovalDataFromId input)
        {
            InterTransferEditDto editDto;
            List<InterTransferDetailViewDto> editDetailDto;
            TemplateSaveDto editTemplateDto = new TemplateSaveDto();

            if (input.Id.HasValue)
            {
                var hDto = await _intertransferRepo.GetAsync(input.Id.Value);
                if (hDto != null)
                {
                    editDto = hDto.MapTo<InterTransferEditDto>();

                    if (editDto.VehicleNumber.IsNullOrEmpty())
                        editDto.VehicleNumber = "";

                    if (editDto.PersonInCharge.IsNullOrEmpty())
                        editDto.PersonInCharge = "";

                    if (editDto.TransferValue != null)
                        editDto.TransferValueInWords = _xmlandjsonConvertorAppService.GetAmountInWords(editDto.TransferValue.Value);

                    int locationid = editDto.RequestLocationRefId;

                    editDetailDto = await (from request in _intertransferDetailRepo.GetAll().
                        Where(a => a.InterTransferRefId == input.Id.Value)
                                           join mat in _materialRepo.GetAll()
                                               on request.MaterialRefId equals mat.Id
                                           join un in _unitRepo.GetAll()
                                               on mat.DefaultUnitId equals un.Id
                                           join uiss in _unitRepo.GetAll()
                                                on request.UnitRefId equals uiss.Id
                                           join matloc in
                                               _materiallocationwisestockRepo.GetAll().
                                                   Where(mls => mls.LocationRefId == locationid)
                                               on mat.Id equals matloc.MaterialRefId
                                           join tomatloc in _materiallocationwisestockRepo.GetAll().
                                                   Where(mls => mls.LocationRefId == hDto.LocationRefId)
                                                  on mat.Id equals tomatloc.MaterialRefId
                                           select new InterTransferDetailViewDto
                                           {
                                               Id = request.Id,
                                               RequestQty = request.RequestQty,
                                               MaterialRefId = request.MaterialRefId,
                                               MaterialPetName = mat.MaterialPetName,
                                               MaterialRefName = mat.MaterialName,
                                               CurrentStatus = request.CurrentStatus,
                                               InterTransferRefId = request.InterTransferRefId,
                                               IssueQty = request.IssueQty,
                                               Price = request.Price,
                                               ApprovedRemarks = request.ApprovedRemarks,
                                               RequestRemarks = request.RequestRemarks,
                                               Sno = request.Sno,
                                               MaterialTypeId = mat.MaterialTypeId,
                                               Uom = un.Name,
                                               OnHand = matloc.CurrentInHand,
                                               ToLocationOnHand = tomatloc.CurrentInHand,
                                               UnitRefId = request.UnitRefId,
                                               UnitRefName = uiss.Name,
                                               DefaultUnitId = mat.DefaultUnitId,
                                               DefaultUnitName = un.Name,
                                               LineTotal = Math.Round(request.IssueQty * request.Price, 3)

                                           }).ToListAsync();

                    bool interTransferValueShown = await SettingManager.GetSettingValueAsync<bool>(AppSettings.HouseSettings.InterTransferValueShown);

                    var rsUnitConversion = await _unitconversionRepo.GetAll().Select(t => new { t.BaseUnitId, t.RefUnitId, t.Conversion, t.MaterialRefId }).ToListAsync();

                    List<MaterialRateViewListDto> materialRate = new List<MaterialRateViewListDto>();
                    bool rateDefaultRequired = editDetailDto.Exists(t => t.Price == 0);
                    if (rateDefaultRequired)
                    {
                        if (input.MaterialRateView == null || input.MaterialRateView.Count == 0)
                        {
                            List<Location> loc = await _locationRepo.GetAll().Where(t => t.Id == editDto.RequestLocationRefId).ToListAsync();
                            List<LocationListDto> locinput = loc.MapTo<List<LocationListDto>>();
                            List<int> materialRefIdsRateNeeded = editDetailDto.Where(t => t.Price == 0).Select(t => t.MaterialRefId).ToList();
                            var matView = await _housereportAppService.GetMaterialRateView(new GetHouseReportMaterialRateInput
                            {
                                StartDate = loc[0].HouseTransactionDate.Value,
                                EndDate = loc[0].HouseTransactionDate.Value,
                                Locations = locinput,
                                MaterialRefIds = materialRefIdsRateNeeded,
                                FunctionCalledBy = "Get Intertransfer Approval For Edit"
                            });
                            materialRate = matView.MaterialRateView;
                        }
                        else
                        {
                            materialRate = input.MaterialRateView;
                        }
                    }


                    foreach (var lst in editDetailDto)
                    {
                        decimal deconHand = lst.OnHand;
                        lst.OnHandWithUom = deconHand + "/" + lst.DefaultUnitName;

                        decimal conversionFactor = 1;
                        if (lst.DefaultUnitId == lst.UnitRefId)
                            conversionFactor = 1;
                        else
                        {
                            var conv =
                                     rsUnitConversion.FirstOrDefault(t => t.BaseUnitId == lst.DefaultUnitId && t.RefUnitId == lst.UnitRefId && t.MaterialRefId == lst.MaterialRefId);
                            if (conv == null)
                            {
                                conv =
                                     rsUnitConversion.FirstOrDefault(t => t.BaseUnitId == lst.DefaultUnitId && t.RefUnitId == lst.UnitRefId);
                            }

                            if (conv != null)
                            {
                                conversionFactor = conv.Conversion;
                            }
                            else
                            {
                                var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == lst.DefaultUnitId);
                                if (baseUnit == null)
                                {
                                    throw new UserFriendlyException(L("UnitIdDoesNotExist", lst.DefaultUnitId));
                                }
                                var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == lst.UnitRefId);
                                if (refUnit == null)
                                {
                                    throw new UserFriendlyException(L("UnitIdDoesNotExist", lst.UnitRefId));
                                }
                                throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
                            }
                            lst.OnHand = lst.OnHand * conversionFactor;
                        }

                        //if (interTransferValueShown == true && input.PrintFlag == false)
                        if (lst.Price == 0)
                        {
                            var matprice = materialRate.FirstOrDefault(t => t.MaterialRefId == lst.MaterialRefId);
                            if (lst.DefaultUnitId == lst.UnitRefId)
                                lst.Price = matprice.AvgRate;
                            else
                            {
                                lst.Price = Math.Round(matprice.AvgRate * 1 / conversionFactor, 3);
                            }
                        }

                        lst.LineTotal = Math.Round(lst.IssueQty * lst.Price, 3);

                        if (lst.CurrentStatus.Equals("Q"))
                            lst.CurrentStatus = L("RequestApprovalPending");
                        else if (lst.CurrentStatus.Equals("P"))
                            lst.CurrentStatus = L("Pending");
                        else if (lst.CurrentStatus.Equals("A"))
                            lst.CurrentStatus = L("Approved");
                        else if (lst.CurrentStatus.Equals("R"))
                            lst.CurrentStatus = L("Rejected");
                        else if (lst.CurrentStatus.Equals("D"))
                            lst.CurrentStatus = L("Drafted");
                        else if (lst.CurrentStatus.Equals("V"))
                            lst.CurrentStatus = L("Received");
                        else if (lst.CurrentStatus.Equals("I"))
                            lst.CurrentStatus = L("PartiallyReceived");
                        else if (lst.CurrentStatus.Equals("E"))
                            lst.CurrentStatus = L("Excess");
                        else if (lst.CurrentStatus.Equals("S"))
                            lst.CurrentStatus = L("Shortage");

                        string materialTypeName = lst.MaterialTypeId == (int)MaterialType.RAW ? L("RAW") : L("SEMI");
                        lst.MaterialTypeName = materialTypeName;

                        //lst.UsagePatternList = await _materialAppService.MaterialUsagePattern(
                        //    new InputLocationAndMaterialId
                        //    {
                        //        LocationRefId = locationid,
                        //        MaterialRefId = lst.MaterialRefId
                        //    }
                        //    );
                    }
                    hDto.TransferValue = Math.Round(editDetailDto.Sum(t => t.LineTotal), 3);
                }
                else
                {
                    editDto = new InterTransferEditDto();
                    editDetailDto = new List<InterTransferDetailViewDto>();
                }
            }
            else
            {
                editDto = new InterTransferEditDto();
                editDetailDto = new List<InterTransferDetailViewDto>();
            }

            return new GetInterTransferForEditOutput
            {
                InterTransfer = editDto,
                InterTransferDetail = editDetailDto,
                Templatesave = editTemplateDto
            };
        }

    }
}
