﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using DinePlan.DineConnect.House.Master.Dtos;

namespace DinePlan.DineConnect.House.Transaction
{
    public interface IRequestAppService : IApplicationService
    {

        Task<PagedResultOutput<RequestListDto>> GetAll(GetRequestInput inputDto);
        Task<FileDto> GetAllToExcel(GetRequestInput input);


        Task<GetRequestForEditOutput> GetRequestForEdit(NullableIdInput nullableIdInput);

        Task<IdInput> CreateOrUpdateRequest(CreateOrUpdateRequestInput input);

        Task DeleteRequest(IdInput input);

        Task<ListResultOutput<RequestListDto>> GetIds();

        Task<int> GetMaxTokenNumber(DateTime selDt);

        Task<GetRequestForStandardRecipe> GetRequestForRecipe(GetStandardIngridient input);
        
        Task<GetRequestForStandardRecipe> GetRequestForCategory(GetCategoryMaterial input);


       Task<List<RequestListDto>> GetPendingRequests(InputLocation input);

        Task<List<MaterialInfoDto>> GetMaterialForGivenRecipeCombobox(InputLocationAndRecipe input);

        Task<IdInput> GetCategoryIdForRecipe(IdInput input);

    }
}
