﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Transaction
{
    public interface IMenuItemWastageAppService : IApplicationService
    {
        Task<PagedResultOutput<MenuItemWastageListDto>> GetAll(GetMenuItemWastageInput inputDto);
		Task<FileDto> GetAllToExcel(GetMenuItemWastageInput inputDto);

		Task<GetMenuItemWastageForEditOutput> GetMenuItemWastageForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateMenuItemWastage(CreateOrUpdateMenuItemWastageInput input);
        Task DeleteMenuItemWastage(IdInput input);
		Task<FileDto> GetMenuWastageToExcel(InputMenuWastageReport input);
		Task<PagedResultOutput<MenuWastageConsolidatedReport>> GetMenuWastageAll(InputMenuWastageReport input);
		Task<PagedResultOutput<MenuItemWastageDetailEditDto>> GetMenuWastageConsolidatedAll(InputMenuWastageReport input);


	}
}