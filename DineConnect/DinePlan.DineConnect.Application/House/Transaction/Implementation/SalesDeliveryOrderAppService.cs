﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Master;
using DinePlan.DineConnect.House.Master.Dtos;
using Abp.Timing;
using System;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Common;

namespace DinePlan.DineConnect.House.Transaction.Implementation
{
    public class SalesDeliveryOrderAppService : DineConnectAppServiceBase, ISalesDeliveryOrderAppService
    {
        private readonly ISalesDeliveryOrderListExcelExporter _salesdeliveryorderExporter;
        private readonly ISalesDeliveryOrderManager _salesdeliveryorderManager;
        private readonly IRepository<SalesDeliveryOrder> _salesdeliveryorderRepo;
        private readonly IRepository<Customer> _customerRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<SalesDeliveryOrderDetail> _salesdeliveryorderDetailRepo;
        private readonly IRepository<MaterialLedger> _materialLedgerRepo;
        private readonly IRepository<Unit> _unitRepo;
        private readonly IRepository<SalesOrder> _salesorderRepo;
        private readonly IRepository<SalesOrderDetail> _salesorderDetailRepo;
        private readonly IMaterialAppService _materialAppService;
        private readonly IRepository<Location> _locationRepo;
		private readonly IRepository<Template> _templateRepo;
		private readonly IXmlAndJsonConvertor _xmlandjsonConvertorAppService;

		public SalesDeliveryOrderAppService(ISalesDeliveryOrderManager inwarddirectcreditManager,
            IRepository<SalesDeliveryOrder> inwardDirectCreditRepo,
            ISalesDeliveryOrderListExcelExporter inwarddirectcreditExporter,
            IRepository<Customer> customerRepo,
            IRepository<Material> materialRepo,
            IRepository<SalesDeliveryOrderDetail> inwarddirectcreditDetailRepo,
            IRepository<MaterialLedger> materialLedgerRepo,
            IRepository<Unit> unitRepo,
            IMaterialAppService materialAppService,
            IRepository<SalesOrder> salesorderRepo,
            IRepository<SalesOrderDetail> salesorderDetailRepo,
            IRepository<Location> locationRepo,
			IRepository<Template> templateRepo,
			IXmlAndJsonConvertor xmlandjsonConvertorAppService
			)
        {
            _salesdeliveryorderManager = inwarddirectcreditManager;
            _salesdeliveryorderRepo = inwardDirectCreditRepo;
            _salesdeliveryorderExporter = inwarddirectcreditExporter;
            _customerRepo = customerRepo;
            _materialRepo = materialRepo;
            _salesdeliveryorderDetailRepo = inwarddirectcreditDetailRepo;
            _materialLedgerRepo = materialLedgerRepo;
            _unitRepo = unitRepo;
            _materialAppService = materialAppService;
            _salesorderRepo = salesorderRepo;
            _salesorderDetailRepo = salesorderDetailRepo;
            _locationRepo = locationRepo;
			_templateRepo = templateRepo;
			_xmlandjsonConvertorAppService = xmlandjsonConvertorAppService;
		}

        public async Task<PagedResultOutput<SalesDeliveryOrderListDto>> GetAll(GetSalesDeliveryOrderInput input)
        {
            var allItems = _salesdeliveryorderRepo.GetAll();
            if (!string.IsNullOrEmpty(input.Operation) && input.Operation.Equals("SEARCH"))
            {
                allItems = _salesdeliveryorderRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Id.Equals(input.Filter)
               );
            }
            else
            {
                allItems = _salesdeliveryorderRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Id.ToString().Contains(input.Filter)
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<SalesDeliveryOrderListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<SalesDeliveryOrderListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetSalesDeliveryOrderInput input)
        {

            input.MaxResultCount = AppConsts.MaxPageSize;

            var allList = await GetAllWithCustomerNames(input);
            var allListDtos = allList.Items.MapTo<List<SalesDeliveryOrderWithCustomerName>>();
            return _salesdeliveryorderExporter.ExportToFile(allListDtos);
        }

        public async Task<GetSalesDeliveryOrderForEditOutput> GetSalesDeliveryOrderForEdit(NullableIdInput input)
        {
            SalesDeliveryOrderEditDto editDto;
            List<SalesDeliveryOrderDetailViewDto> editDetailDto;

            if (input.Id.HasValue)
            {

                var hDto = await _salesdeliveryorderRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<SalesDeliveryOrderEditDto>();

                editDetailDto = await (from inwDet in _salesdeliveryorderDetailRepo.GetAll().Where(a => a.DeliveryRefId == input.Id.Value)
                                       join mat in _materialRepo.GetAll()
                                       on inwDet.MaterialRefId equals mat.Id
                                       join un in _unitRepo.GetAll()
                                       on inwDet.UnitRefId equals un.Id
                                       select new SalesDeliveryOrderDetailViewDto
                                       {
                                           Id = inwDet.Id,
                                           DeliveryQty = inwDet.DeliveryQty,
                                           BillConvertedQty = inwDet.BillConvertedQty,
                                           DeliveryRefId = inwDet.DeliveryRefId,
                                           MaterialReceivedStatus = inwDet.MaterialReceivedStatus,
                                           MaterialRefId = inwDet.MaterialRefId,
                                           MaterialRefName = mat.MaterialName,
                                           UnitRefId = inwDet.UnitRefId,
                                           UnitRefName = un.Name
                                       }).ToListAsync();


                var existConverted = editDetailDto.Where(t => t.BillConvertedQty > 0).ToList();

                if (existConverted.Count > 0)
                {
                    throw new UserFriendlyException(L("PartialConversionAsBill"));
                }


            }
            else
            {
                editDto = new SalesDeliveryOrderEditDto();
                editDetailDto = new List<SalesDeliveryOrderDetailViewDto>();
            }

            return new GetSalesDeliveryOrderForEditOutput
            {
                SalesDeliveryOrder = editDto,
                SalesDeliveryOrderDetail = editDetailDto
            };
        }

        public async Task<IdInput> CreateOrUpdateSalesDeliveryOrder(CreateOrUpdateSalesDeliveryOrderInput input)
        {

            if (input.SalesDeliveryOrder.Id.HasValue)
            {
                return await UpdateSalesDeliveryOrder(input);
            }
            else
            {
                return await CreateSalesDeliveryOrder(input);
            }

        }

        public async Task DeleteSalesDeliveryOrder(IdInput input)
        {
            var inwardMas = await _salesdeliveryorderRepo.GetAsync(input.Id);

            if (inwardMas.DeliveryDate.Date == DateTime.Today)
            {
                if (inwardMas.IsDcCompleted == false)
                {
                    var inwardDet = await _salesdeliveryorderDetailRepo.GetAll().Where(t => t.DeliveryRefId == input.Id).ToListAsync();

                    foreach (var detDto in inwardDet)
                    {
                        MaterialLedgerDto ml = new MaterialLedgerDto();
                        ml.LocationRefId = inwardMas.LocationRefId;
                        ml.MaterialRefId = detDto.MaterialRefId;
                        ml.LedgerDate = inwardMas.DeliveryDate.Date;
                        ml.Sales = -1 * detDto.DeliveryQty;
                        ml.UnitId = detDto.UnitRefId;

                        var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                    }

                    if (inwardMas.SoRefId != null && inwardMas.SoRefId > 0)
                    {
                        var poitem = await _salesorderRepo.GetAsync(inwardMas.SoRefId.Value);
                        poitem.SoCurrentStatus = "A";// L("Approved");
                    }
                    await _salesdeliveryorderRepo.DeleteAsync(input.Id);
                }
                else
                {
                    throw new UserFriendlyException("SelectedReceiptsBillConvertedErr");
                }
            }
            else
            {
                throw new UserFriendlyException("ReceiptsCurrentDateCanOnlyDeleted");
            }

        }

        protected virtual async Task<IdInput> UpdateSalesDeliveryOrder(CreateOrUpdateSalesDeliveryOrderInput input)
        {
            var item = await _salesdeliveryorderRepo.GetAsync(input.SalesDeliveryOrder.Id.Value);

            if (input.SalesDeliveryOrderDetail == null || input.SalesDeliveryOrderDetail.Count() == 0)
            {
                throw new UserFriendlyException(L("MinimumOneDetail"));
            }

            var dto = input.SalesDeliveryOrder;


            //TODO: SERVICE SalesDeliveryOrder Update Individually
            item.DeliveryDate = dto.DeliveryDate;
            item.DcNumberGivenByCustomer = dto.DcNumberGivenByCustomer;
            item.CustomerRefId = dto.CustomerRefId;
            item.IsDcCompleted = dto.IsDcCompleted;

            //  Update Detail Link
            List<int> materialsToBeRetained = new List<int>();

            if (input.SalesDeliveryOrderDetail != null && input.SalesDeliveryOrderDetail.Count > 0)
            {
                foreach (var items in input.SalesDeliveryOrderDetail)
                {
                    int materialrefid = items.MaterialRefId;
                    materialsToBeRetained.Add(materialrefid);

                    var existingDetailEntry = _salesdeliveryorderDetailRepo.GetAllList(u => u.DeliveryRefId == dto.Id && u.MaterialRefId.Equals(materialrefid));

                    if (existingDetailEntry.Count == 0)  //  Add New Material
                    {
                        SalesDeliveryOrderDetail ab = new SalesDeliveryOrderDetail();

                        ab.DeliveryRefId = (int)dto.Id;
                        ab.DeliveryQty = items.DeliveryQty;
                        ab.BillConvertedQty = 0;
                        ab.MaterialRefId = items.MaterialRefId;
                        ab.MaterialReceivedStatus = items.MaterialReceivedStatus;
                        ab.UnitRefId = items.UnitRefId;

                        var retId2 = await _salesdeliveryorderDetailRepo.InsertAndGetIdAsync(ab);

                        MaterialLedgerDto ml = new MaterialLedgerDto();
                        ml.LocationRefId = dto.LocationRefId;
                        ml.MaterialRefId = ab.MaterialRefId;
                        ml.LedgerDate = dto.DeliveryDate.Date;
                        ml.Sales = ab.DeliveryQty;
                        ml.UnitId = items.UnitRefId;
                        var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                        ab.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;
                    }
                    else
                    {
                        var editDetailDto = await _salesdeliveryorderDetailRepo.GetAsync(existingDetailEntry[0].Id);

                        decimal differcenceQty;
                        differcenceQty = items.DeliveryQty - editDetailDto.DeliveryQty;

                        editDetailDto.DeliveryRefId = (int)dto.Id;
                        editDetailDto.DeliveryQty = items.DeliveryQty;
                        editDetailDto.MaterialRefId = items.MaterialRefId;
                        editDetailDto.MaterialReceivedStatus = items.MaterialReceivedStatus;
                        editDetailDto.UnitRefId = items.UnitRefId;

                        var retId2 = await _salesdeliveryorderDetailRepo.InsertOrUpdateAndGetIdAsync(editDetailDto);

                        if (differcenceQty != 0)
                        {
                            MaterialLedgerDto ml = new MaterialLedgerDto();
                            ml.LocationRefId = dto.LocationRefId;
                            ml.MaterialRefId = items.MaterialRefId;
                            ml.LedgerDate = dto.DeliveryDate.Date;
                            ml.Sales = differcenceQty;
                            ml.PlusMinus = "U";
                            ml.UnitId = items.UnitRefId;
                            var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                            editDetailDto.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;
                        }
                    }


                }
            }

            var delInwardDcDetailList = _salesdeliveryorderDetailRepo.GetAll().Where(a => a.DeliveryRefId == input.SalesDeliveryOrder.Id.Value && !materialsToBeRetained.Contains(a.MaterialRefId)).ToList();

            foreach (var a in delInwardDcDetailList)
            {
                MaterialLedgerDto ml = new MaterialLedgerDto();
                ml.LocationRefId = dto.LocationRefId;
                ml.MaterialRefId = a.MaterialRefId;
                ml.LedgerDate = dto.DeliveryDate.Date;
                ml.Sales = (-1 * a.DeliveryQty);
                ml.PlusMinus = "U";
                ml.UnitId = a.UnitRefId;
                var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);

                _salesdeliveryorderDetailRepo.Delete(a.Id);

                //if (input.SalesDeliveryOrder.PoRefId != null && input.SalesDeliveryOrder.PoRefId > 0)
                //{
                //    var poitem = await _salesorderRepo.GetAsync(input.SalesDeliveryOrder.PoRefId.Value);
                //    bool CompleteFlag = true;
                //    var podetail = await _salesorderDetailRepo.GetAll().Where(t => t.PoRefId == input.SalesDeliveryOrder.PoRefId.Value).ToListAsync();
                //    foreach (var det in podetail)
                //    {
                //        var inwmas = _salesdeliveryorderRepo.GetAll().Where(t => t.PoRefId == input.SalesDeliveryOrder.PoRefId.Value).ToList().Select(t => t.Id).ToArray();

                //        var rcdQtyinInwDet = _salesdeliveryorderDetailRepo.GetAll().Where(t => inwmas.Contains(t.DcRefId) && t.MaterialRefId == det.MaterialRefId).ToList().Sum(t => t.DcReceivedQty);

                //        if (rcdQtyinInwDet < det.QtyOrdered)
                //        {
                //            CompleteFlag = false;
                //        }

                //        var podetDto = await _salesorderDetailRepo.GetAsync(det.Id);
                //        podetDto.QtyReceived = rcdQtyinInwDet;
                //        if (podetDto.QtyReceived > podetDto.QtyOrdered)
                //            podetDto.PoStatus = true;

                //        await _salesorderDetailRepo.UpdateAsync(podetDto);
                //    }

                //    if (CompleteFlag)
                //        poitem.PoCurrentStatus = "C"; // L("Completed");
                //    else
                //        poitem.PoCurrentStatus = "H";
                //}

            }


            CheckErrors(await _salesdeliveryorderManager.CreateSync(item));

            return new IdInput
            {
                Id = input.SalesDeliveryOrder.Id.Value
            };
        }

        protected virtual async Task<IdInput> CreateSalesDeliveryOrder(CreateOrUpdateSalesDeliveryOrderInput input)
        {
            var dto = input.SalesDeliveryOrder.MapTo<SalesDeliveryOrder>();

            Clock.Provider = new UtcClockProvider();

            if (input.SalesDeliveryOrderDetail == null || input.SalesDeliveryOrderDetail.Count() == 0)
            {
                throw new UserFriendlyException(L("MinimumOneDetail"));
            }

            var retId = await _salesdeliveryorderRepo.InsertOrUpdateAndGetIdAsync(dto);

            foreach (SalesDeliveryOrderDetailViewDto items in input.SalesDeliveryOrderDetail.ToList())
            {
                SalesDeliveryOrderDetail ab = new SalesDeliveryOrderDetail();
                ab.DeliveryRefId = retId;
                ab.DeliveryQty = items.DeliveryQty;
                ab.BillConvertedQty = 0;
                ab.MaterialRefId = items.MaterialRefId;
                ab.MaterialReceivedStatus = items.MaterialReceivedStatus;
                ab.UnitRefId = items.UnitRefId;

                var retId2 = await _salesdeliveryorderDetailRepo.InsertAndGetIdAsync(ab);

                MaterialLedgerDto ml = new MaterialLedgerDto();
                ml.LocationRefId = dto.LocationRefId;
                ml.MaterialRefId = ab.MaterialRefId;
                ml.LedgerDate = dto.DeliveryDate.Date;
                ml.Sales = ab.DeliveryQty;
                ml.UnitId = items.UnitRefId;
                var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                ab.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;

            }

            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == dto.LocationRefId);
            dto.AccountDate = loc.HouseTransactionDate.Value;

            CheckErrors(await _salesdeliveryorderManager.CreateSync(dto));

            if (input.SalesDeliveryOrder.SoRefId != null && input.SalesDeliveryOrder.SoRefId > 0)
            {
                var poitem = await _salesorderRepo.GetAsync(input.SalesDeliveryOrder.SoRefId.Value);
                bool CompleteFlag = true;
                var podetail = await _salesorderDetailRepo.GetAll().Where(t => t.SoRefId == input.SalesDeliveryOrder.SoRefId.Value).ToListAsync();
                foreach (var det in podetail)
                {
                    var inwmas = _salesdeliveryorderRepo.GetAll().Where(t => t.SoRefId == input.SalesDeliveryOrder.SoRefId.Value).ToList().Select(t => t.Id).ToArray();

                    var rcdQtyinInwDet = _salesdeliveryorderDetailRepo.GetAll().Where(t => inwmas.Contains(t.DeliveryRefId) && t.MaterialRefId == det.MaterialRefId).ToList().Sum(t => t.DeliveryQty);

                    if (rcdQtyinInwDet < det.QtyOrdered)
                    {
                        CompleteFlag = false;
                    }

                    var podetDto = await _salesorderDetailRepo.GetAsync(det.Id);
                    podetDto.QtyReceived = rcdQtyinInwDet;
                    if (podetDto.QtyReceived > podetDto.QtyOrdered)
                        podetDto.SoStatus = true;

                    await _salesorderDetailRepo.UpdateAsync(podetDto);
                }

                if (CompleteFlag)
                    poitem.SoCurrentStatus = "C"; // L("Completed");
                else
                    poitem.SoCurrentStatus = "H";
            }

			//string templateJson = _xmlandjsonConvertorAppService.SerializeToJSON(input);

			//Template lastTemplate;
			//int? templateScope;
			//if (input.Templatesave.Templatescope == true)
			//{
			//	templateScope = null;
			//}
			//else
			//{
			//	templateScope = input.SalesDeliveryOrder.LocationRefId;
			//}

			//if (input.Templatesave.Templateflag == true)
			//{
			//	string repName = input.Templatesave.Templatename;

			//	var templateExist = _templateRepo.GetAll().Where(t => t.Name == repName && t.TemplateType == (int)TemplateType.SalesDeliveryOrder).ToList();
			//	if (templateExist.Count == 0)
			//	{
			//		lastTemplate = new Template
			//		{
			//			Name = input.Templatesave.Templatename,
			//			TemplateType = (int)TemplateType.SalesDeliveryOrder,
			//			TemplateData = templateJson,
			//			LocationRefId = templateScope
			//		};
			//	}
			//	else
			//	{
			//		lastTemplate = templateExist[0];
			//		lastTemplate.LocationRefId = templateScope;
			//		lastTemplate.TemplateData = templateJson;
			//	}
			//}
			//else
			//{
			//	string repName = L("LastSo");
			//	var templateExist = _templateRepo.GetAll().Where(t => t.Name == repName && t.TemplateType == (int)TemplateType.SalesDeliveryOrder).ToList();
			//	if (templateExist.Count() == 0)
			//	{
			//		lastTemplate = new Template
			//		{
			//			Name = L("LastSo"),
			//			TemplateType = (int)TemplateType.SupplierPurchaseOrder,
			//			TemplateData = templateJson,
			//			LocationRefId = input.SalesDeliveryOrder.LocationRefId
			//		};
			//	}
			//	else
			//	{
			//		lastTemplate = templateExist[0];
			//		lastTemplate.LocationRefId = templateScope;
			//		lastTemplate.TemplateData = templateJson;
			//	}
			//}

			//await _templateRepo.InsertOrUpdateAndGetIdAsync(lastTemplate);

			return new IdInput
            {
                Id = retId
            };
        }


        public async Task<ListResultOutput<SalesDeliveryOrderListDto>> GetSalesDeliveryOrderIds()
        {
            var lstSalesDeliveryOrder = await _salesdeliveryorderRepo.GetAll().ToListAsync();
            return new ListResultOutput<SalesDeliveryOrderListDto>(lstSalesDeliveryOrder.MapTo<List<SalesDeliveryOrderListDto>>());
        }

        //InwardDcWithCustomerName
        public async Task<PagedResultOutput<SalesDeliveryOrderWithCustomerName>> GetAllWithCustomerNames(GetSalesDeliveryOrderInput input)
        {

            var rsCustomer = _customerRepo.GetAll().WhereIf(!input.CustomerName.IsNullOrEmpty(), p => p.CustomerName.Contains(input.CustomerName));

            var rsInwarddirect = _salesdeliveryorderRepo.GetAll().Where(t => t.LocationRefId == input.DefaultLocationRefId).WhereIf(!input.DcNumberGivenByCustomer.IsNullOrEmpty(), t => t.DcNumberGivenByCustomer.Contains(input.DcNumberGivenByCustomer));

            if (input.StartDate != null && input.EndDate != null)
            {
                rsInwarddirect = rsInwarddirect.Where(t => t.DeliveryDate >= input.StartDate && t.DeliveryDate <= input.EndDate);
            }

            var allitems = (from inwdc in rsInwarddirect
                            join sup in rsCustomer on inwdc.CustomerRefId equals sup.Id
                            select new SalesDeliveryOrderWithCustomerName()
                            {
                                Id = inwdc.Id,
                                DeliveryDate = inwdc.DeliveryDate,
                                IsDcCompleted = inwdc.IsDcCompleted,
                                DcNumberGivenByCustomer = inwdc.DcNumberGivenByCustomer,
                                CustomerName = sup.CustomerName,
                                CustomerRefId = inwdc.CustomerRefId,
                                CreationTime = inwdc.CreationTime,
                                SoRefId = inwdc.SoRefId,
                                SoReference = inwdc.SoReferenceCode
                            }
               );

            var sortMenuItems = await allitems
               .OrderBy(input.Sorting)
               .PageBy(input)
               .ToListAsync();

            var allListDtos = sortMenuItems.ToList();
            var allItemCount = await allitems.CountAsync();
            return new PagedResultOutput<SalesDeliveryOrderWithCustomerName>(
               allItemCount,
               allListDtos
               );

        }


        public async Task<ListResultOutput<SalesDeliveryOrderListDto>> GetSalesDoTranid(InputDateRangeWithCustomerId input)
        {
            DateTime dcFromDt = input.StartDate;
            DateTime dcEndDt = input.EndDate;

            var allItems = await (from inwdc in _salesdeliveryorderRepo.GetAll()
                                  .Where(t => t.LocationRefId == input.LocationRefId && t.CustomerRefId == input.CustomerRefId && !t.IsDcCompleted)
                                  .WhereIf(input.AllTimeDcs == false, t => DbFunctions.TruncateTime(t.DeliveryDate) >= DbFunctions.TruncateTime(dcFromDt)
                                          && DbFunctions.TruncateTime(t.DeliveryDate) <= DbFunctions.TruncateTime(dcEndDt)
                                 )
                                  select inwdc).ToListAsync();

            return new ListResultOutput<SalesDeliveryOrderListDto>(allItems.MapTo<List<SalesDeliveryOrderListDto>>());
        }

        public async Task<PagedResultOutput<SalesDeliveryOrderView>> GetAllView(GetSalesDeliveryOrderInput input)
        {
            var allitems = (from inwdc in _salesdeliveryorderRepo.GetAll().Where(p => p.LocationRefId == input.DefaultLocationRefId)
                            join indet in _salesdeliveryorderDetailRepo.GetAll() on inwdc.Id equals indet.DeliveryRefId
                            where inwdc.Id == input.SelectedId
                            join mat in _materialRepo.GetAll() on indet.MaterialRefId equals mat.Id
                            join sup in _customerRepo.GetAll() on inwdc.CustomerRefId equals sup.Id
                            join uiss in _unitRepo.GetAll() on indet.UnitRefId equals uiss.Id
                            select new SalesDeliveryOrderView()
                            {
                                Id = inwdc.Id,
                                DeliveryDate = inwdc.DeliveryDate,
                                IsDcCompleted = inwdc.IsDcCompleted,
                                DcNumberGivenByCustomer = inwdc.DcNumberGivenByCustomer,
                                CustomerName = sup.CustomerName,
                                CustomerRefId = inwdc.CustomerRefId,
                                DeliveryQty = indet.DeliveryQty,
                                MaterialCode = indet.MaterialRefId,
                                MaterialName = mat.MaterialName,
                                MaterialReceivedStatus = indet.MaterialReceivedStatus,
                                CreationTime = inwdc.CreationTime,
                                UnitRefId = indet.UnitRefId,
                                UnitRefName = uiss.Name
                            });

            var sortMenuItems = await allitems
               .OrderBy(input.Sorting)
               .PageBy(input)
               .ToListAsync();

            var allListDtos = sortMenuItems.ToList();
            var allItemCount = await allitems.CountAsync();
            return new PagedResultOutput<SalesDeliveryOrderView>(
               allItemCount,
               allListDtos
               );

        }



        public async Task<PagedResultOutput<SalesDeliveryOrderDetailListDto>> GetAllSalesDoDetail(GetSalesDeliveryOrderDetailInput input)
        {
            var allItems = _salesdeliveryorderDetailRepo.GetAll();
            if (!string.IsNullOrEmpty(input.Operation) && input.Operation.Equals("SEARCH"))
            {
                allItems = _salesdeliveryorderDetailRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Id.Equals(input.Filter)
               );
            }
            else
            {
                allItems = _salesdeliveryorderDetailRepo
               .GetAll();
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<SalesDeliveryOrderDetailListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<SalesDeliveryOrderDetailListDto>(
                allItemCount,
                allListDtos
                );
        }


        public async Task<PrintOutIn40Cols> GetPrintData(IdInput input)
        {
            PrintOutIn40Cols output = new PrintOutIn40Cols();

            SalesDeliveryOrderEditDto editDto;
            List<SalesDeliveryOrderDetailViewDto> editDetailDto;

            var hDto = await _salesdeliveryorderRepo.GetAsync(input.Id);
            editDto = hDto.MapTo<SalesDeliveryOrderEditDto>();

            editDetailDto = await (from inwDet in _salesdeliveryorderDetailRepo.GetAll().Where(a => a.DeliveryRefId == input.Id)
                                   join mat in _materialRepo.GetAll()
                                   on inwDet.MaterialRefId equals mat.Id
                                   join un in _unitRepo.GetAll()
                                   on inwDet.UnitRefId equals un.Id
                                   select new SalesDeliveryOrderDetailViewDto
                                   {
                                       Id = inwDet.Id,
                                       DeliveryQty = inwDet.DeliveryRefId,
                                       BillConvertedQty = inwDet.BillConvertedQty,
                                       DeliveryRefId = inwDet.DeliveryRefId,
                                       MaterialReceivedStatus = inwDet.MaterialReceivedStatus,
                                       MaterialRefId = inwDet.MaterialRefId,
                                       MaterialRefName = mat.MaterialName,
                                       UnitRefId = inwDet.UnitRefId,
                                       UnitRefName = un.Name
                                   }).ToListAsync();

            var customer = _customerRepo.FirstOrDefault(t => t.Id == hDto.CustomerRefId);
            var billingLocation = _locationRepo.FirstOrDefault(t => t.Id == hDto.LocationRefId);

            TextFileWriter fp = new TextFileWriter();


            string subPath = "~/Report/";
            bool exists = System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(subPath));

            if (!exists)
                System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(subPath));



            string fileName = System.Web.HttpContext.Current.Server.MapPath("~/Report/" + AbpSession.UserId + "_SalesDoReport.txt");
            int PageLenth = 45;

            fp.FileOpen(fileName, "O");
            if (billingLocation.Name != null)
            {
                fp.PrintTextLine(fp.PrintFormatCenter(billingLocation.Name, PageLenth));
            }

            if (billingLocation.Address1 != null)
            {
                fp.PrintTextLine(fp.PrintFormatCenter(billingLocation.Address1, PageLenth));
            }

            if (billingLocation.Address2 != null)
            {
                fp.PrintTextLine(fp.PrintFormatCenter(billingLocation.Address2, PageLenth));
            }

            if (billingLocation.Address3 != null)
            {
                string addwithCity = billingLocation.Address3;
                if (billingLocation.City != null)
                    addwithCity = addwithCity + ", " + billingLocation.City;
                fp.PrintTextLine(fp.PrintFormatCenter(addwithCity, PageLenth));
            }

            fp.PrintTextLine(fp.PrintFormatCenter(L("Receipts"), PageLenth));

            fp.PrintText(fp.PrintFormatLeft(L("Ref#") + ": " + hDto.DcNumberGivenByCustomer, 27));
            fp.PrintTextLine(L("Date") + ": " + hDto.DeliveryDate.ToString("dd/MM/yyyy"));

            fp.PrintSepLine('-', PageLenth);

            string retText = System.IO.File.ReadAllText(fileName);

            retText = retText.Replace("\r\n", "<br/>");

            retText = retText.Replace(" ", "&nbsp;");

            output.HeaderText = retText;

            output.BoldHeaderText = "<STRONG>" + retText + "</STRONG>";

            fp.FileOpen(fileName, "O");

            fp.PrintText(L("Customer") + " : ");
            if (customer.CustomerName.Length > 0)
                fp.PrintTextLine(customer.CustomerName);

            // //if (customer..TaxRegistrationNumber != null)
            // //{
            // //    fp.PrintText(L("Tax#"));
            // //    fp.PrintTextLine(customer.TaxRegistrationNumber);
            // //}


            fp.PrintSepLine('-', PageLenth);

            fp.PrintText(fp.PrintFormatLeft(L("Material"), 30));
            fp.PrintText(fp.PrintFormatRight(L("Qty"), 8));
            fp.PrintText(fp.PrintFormatLeft(" ", 1));
            fp.PrintText(fp.PrintFormatLeft(L("UOM"), 6));

            fp.PrintTextLine("");
            fp.PrintSepLine('-', PageLenth);

            int loopIndex = 1;

            foreach (var det in editDetailDto)
            {
                fp.PrintText(fp.PrintFormatLeft(det.MaterialRefName, 30));
                decimal qty = Math.Round(det.DeliveryQty, 2);
                string qtyinstring = string.Concat(new string(' ', 9 - qty.ToString().Length), qty.ToString());

                qtyinstring = qtyinstring + " " + det.UnitRefName;

                fp.PrintText(fp.PrintFormatLeft(qtyinstring, 14));
                fp.PrintTextLine("");
                loopIndex++;

            }

            fp.PrintSepLine('-', PageLenth);

            fp.PrintTextLine(fp.PrintFormatCenter("Print @ " + DateTime.Now.ToString("yy-MMM-dd HH:mm:ss"), PageLenth));
            fp.PrintSepLine('-', PageLenth);
            fp.PrintTextLine(" ");

            retText = System.IO.File.ReadAllText(fileName);

            retText = retText.Replace("\r\n", "<br/>");

            output.BodyText = retText;

            output.BoldBodyText = "<STRONG>" + retText + "</STRONG>";

            return output;

        }


    }
}
