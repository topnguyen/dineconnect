﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using System;
using DinePlan.DineConnect.Common.Dto;

namespace DinePlan.DineConnect.House.Transaction
{
    public interface ISalesDeliveryOrderAppService : IApplicationService
    {
        Task<PagedResultOutput<SalesDeliveryOrderListDto>> GetAll(GetSalesDeliveryOrderInput inputDto);
        Task<PagedResultOutput<SalesDeliveryOrderWithCustomerName>> GetAllWithCustomerNames(GetSalesDeliveryOrderInput inputDto);

        Task<PagedResultOutput<SalesDeliveryOrderView>> GetAllView(GetSalesDeliveryOrderInput input);
        Task<PagedResultOutput<SalesDeliveryOrderDetailListDto>> GetAllSalesDoDetail(GetSalesDeliveryOrderDetailInput inputDto);
        //Task<PagedResultOutput<TempSalesDeliveryOrderDetailListDto>> GetAllTempSalesDoDetail(GetSalesDeliveryOrderDetailInput input);
        Task<FileDto> GetAllToExcel(GetSalesDeliveryOrderInput input);
        Task<GetSalesDeliveryOrderForEditOutput> GetSalesDeliveryOrderForEdit(NullableIdInput nullableIdInput);
        Task<IdInput> CreateOrUpdateSalesDeliveryOrder(CreateOrUpdateSalesDeliveryOrderInput input);
        Task DeleteSalesDeliveryOrder(IdInput input);
        Task<ListResultOutput<SalesDeliveryOrderListDto>> GetSalesDeliveryOrderIds();

        Task<ListResultOutput<SalesDeliveryOrderListDto>> GetSalesDoTranid(InputDateRangeWithCustomerId input);

        Task<PrintOutIn40Cols> GetPrintData(IdInput input);

    }
}

