﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.House.Master;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using System;
using DinePlan.DineConnect.Connect.Master;

namespace DinePlan.DineConnect.House.Transaction.Implementation
{
    public class ReturnAppService : DineConnectAppServiceBase, IReturnAppService
    {

        private readonly IReturnListExcelExporter _returnExporter;
        private readonly IReturnManager _returnManager;
        private readonly IRepository<Return> _returnRepo;
        private readonly IRepository<ReturnDetail> _returnDetailRepo;
        private readonly IMaterialAppService _materialAppService;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<Unit> _unitRepo;
        private readonly IRepository<ProductionUnit> _productionunitRepo;
        private readonly IRepository<Location> _locationRepo;

        public ReturnAppService(IReturnManager returnManager,
            IRepository<Return> returnRepo,
            IReturnListExcelExporter returnExporter, IRepository<ReturnDetail> returnDetailRepo, IMaterialAppService materialAppService,
            IRepository<Material> materialRepo,
            IRepository<Unit> unitRepo,
            IRepository<ProductionUnit> productionunitRepo,
            IRepository<Location> locationRepo)
        {
            _returnManager = returnManager;
            _returnRepo = returnRepo;
            _returnExporter = returnExporter;
            _returnDetailRepo = returnDetailRepo;
            _materialAppService = materialAppService;
            _materialRepo = materialRepo;
            _unitRepo = unitRepo;
            _productionunitRepo = productionunitRepo;
            _locationRepo = locationRepo;
        }

        public async Task<PagedResultOutput<ReturnListDto>> GetAll(GetReturnInput input)
        {
            var rsReturn = _returnRepo.GetAll().Where(t => t.LocationRefId == input.DefaultLocationRefId).WhereIf(!input.Filter.IsNullOrEmpty(), p => p.TokenRefNumber.ToString().Contains(input.Filter));

            if (input.StartDate != null && input.EndDate != null)
            {
                rsReturn = rsReturn.Where(a =>
                                 DbFunctions.TruncateTime(a.ReturnDate) >= DbFunctions.TruncateTime(input.StartDate)
                                 &&
                                 DbFunctions.TruncateTime(a.ReturnDate) <= DbFunctions.TruncateTime(input.EndDate));
                // rsRequest.Where(t => t.RequestTime >= input.StartDate && t.RequestTime <= input.EndDate);
            }

            var allItems = from ret in rsReturn
                            join productionunit in _productionunitRepo.GetAll()
                            on ret.ProductionUnitRefId equals productionunit.Id into gj
                            from g in gj.DefaultIfEmpty()
                            select new ReturnListDto
                            {
                                Id = ret.Id,
                                ReturnDate = ret.ReturnDate,
                                LocationRefId = ret.LocationRefId,
                                MaterialGroupCategoryRefId = ret.MaterialGroupCategoryRefId,
                                ProductionUnitRefId = ret.ProductionUnitRefId,
                                IssueRefId = ret.IssueRefId,
                                ProductionUnitRefName = g.Name,
                                TokenRefNumber = ret.TokenRefNumber,
                                CreationTime = ret.CreationTime
                            };


            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<ReturnListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<ReturnListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetReturnInput input)
        {
            input.MaxResultCount = AppConsts.MaxPageSize;

            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<ReturnListDto>>();
            return _returnExporter.ExportToFile(allListDtos);

        }

        public async Task<GetReturnForEditOutput> GetReturnForEdit(NullableIdInput input)
        {
            ReturnEditDto editDto;
            List<ReturnDetailViewDto> editDetailDto;

            if (input.Id.HasValue)
            {
                var hDto = await _returnRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<ReturnEditDto>();

                editDetailDto = await (from retDet in _returnDetailRepo.GetAll().Where(a => a.ReturnRefId == input.Id.Value)
                                       join mat in _materialRepo.GetAll() 
                                       on retDet.MaterialRefId equals mat.Id
                                       join un in _unitRepo.GetAll() 
                                       on mat.DefaultUnitId equals un.Id
                                       join uiss in _unitRepo.GetAll() 
                                       on retDet.UnitRefId equals uiss.Id 
                                       select new ReturnDetailViewDto
                                       {
                                           Id = retDet.Id,
                                           ReturnQty = retDet.ReturnQty,
                                           ReturnRefId = retDet.ReturnRefId,
                                           MaterialRefId = retDet.MaterialRefId,
                                           MaterialRefName = mat.MaterialName,
                                           RecipeRefId = retDet.RecipeRefId,
                                           Remarks = retDet.Remarks,
                                           Sno = retDet.Sno,
                                           DefaultUnitId = mat.DefaultUnitId,
                                           DefaultUnit = un.Name,
                                           UnitRefId = retDet.UnitRefId,
                                           UnitRefName = uiss.Name
                                       }).ToListAsync();
            }
            else
            {
                editDto = new ReturnEditDto();
                int maxTokno =await GetMaxTokenNumber(DateTime.Today);
                editDto.TokenRefNumber = maxTokno;
                editDto.ReturnDate = DateTime.Now;
                editDetailDto = new List<ReturnDetailViewDto>();
            }

            return new GetReturnForEditOutput
            {
                Return = editDto,
                ReturnDetail=editDetailDto
            };
        }

        public async Task<int> GetMaxTokenNumber(DateTime selDt)
        {
            DateTime givenDt = selDt;
            var maxTokno = await (from iss in _returnRepo.GetAll()
                                 .Where(a => a.ReturnDate.Year == givenDt.Year
                                 && a.ReturnDate.Month == givenDt.Month
                                 && a.ReturnDate.Day == givenDt.Day)
                                  select iss).ToListAsync();

            var TokenNumber = maxTokno.Count() + 1;
            return TokenNumber;
        }
        public async Task<IdInput> CreateOrUpdateReturn(CreateOrUpdateReturnInput input)
        {
            if (input.Return.Id.HasValue)
            {
                return await UpdateReturn(input);
            }
            else
            {
                return await CreateReturn(input);
            }
        }

        public async Task DeleteReturn(IdInput input)
        {
            await _returnRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task<IdInput> UpdateReturn(CreateOrUpdateReturnInput input)
        {
            var item = await _returnRepo.GetAsync(input.Return.Id.Value);
            var dto = input.Return;

            //TODO: SERVICE Return Update Individually
            item.LocationRefId = dto.LocationRefId;
            item.ReturnDate =(DateTime) dto.ReturnDate;
            item.TokenRefNumber = dto.TokenRefNumber;
            item.MaterialGroupCategoryRefId = dto.MaterialGroupCategoryRefId;
            item.ProductionUnitRefId = dto.ProductionUnitRefId;
            item.IssueRefId = dto.IssueRefId;


            List<int> materialsToBeRetained = new List<int>();

            if (input.ReturnDetail != null && input.ReturnDetail.Count > 0)
            {
                foreach(var items in input.ReturnDetail)
                {
                    var matRefId = items.MaterialRefId;
                    materialsToBeRetained.Add(matRefId);
                    var existingReturnDetail = _returnDetailRepo.GetAllList(u => u.ReturnRefId == dto.Id && u.MaterialRefId.Equals(matRefId));
                    if (existingReturnDetail.Count == 0)  //Add new record
                    {
                        ReturnDetail id = new ReturnDetail();
                        id.ReturnRefId = (int)dto.Id;
                        id.ReturnQty = items.ReturnQty;
                        id.MaterialRefId = items.MaterialRefId;
                        id.RecipeRefId = items.RecipeRefId;
                        id.Remarks = items.Remarks;
                        id.UnitRefId = items.UnitRefId;
                        id.Sno = items.Sno;

                        var retId2 = await _returnDetailRepo.InsertAndGetIdAsync(id);

                        MaterialLedgerDto ml = new MaterialLedgerDto();
                        ml.LocationRefId = dto.LocationRefId;
                        ml.MaterialRefId = id.MaterialRefId;
                        ml.LedgerDate = item.ReturnDate.Date;
                        ml.Return = id.ReturnQty;
                        ml.UnitId = id.UnitRefId;

                        var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                        id.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;
                    }
                    else
                    {
                        var editDetailDto = await _returnDetailRepo.GetAsync(existingReturnDetail[0].Id);

                        decimal differcenceQty;
                        differcenceQty = items.ReturnQty - editDetailDto.ReturnQty;

                        editDetailDto.ReturnRefId = (int)dto.Id;
                        editDetailDto.ReturnQty = items.ReturnQty;
                        editDetailDto.MaterialRefId = items.MaterialRefId;
                        editDetailDto.RecipeRefId = items.RecipeRefId;
                        editDetailDto.Remarks = items.Remarks;
                        editDetailDto.Sno = items.Sno;
                        editDetailDto.UnitRefId = items.UnitRefId;

                        var retId3 = await _returnDetailRepo.InsertOrUpdateAndGetIdAsync(editDetailDto);

                        if (differcenceQty != 0)
                        {
                            MaterialLedgerDto ml = new MaterialLedgerDto();
                            ml.LocationRefId = dto.LocationRefId;
                            ml.MaterialRefId = items.MaterialRefId;
                            ml.LedgerDate = item.ReturnDate.Date;
                            ml.Return = differcenceQty;
                            ml.UnitId = items.UnitRefId;
                            ml.PlusMinus = "U";
                            var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                            editDetailDto.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;
                        }
                    }
                }
            }

            var delMatList = _returnDetailRepo.GetAll().Where(a => a.ReturnRefId == input.Return.Id.Value && !materialsToBeRetained.Contains(a.MaterialRefId)).ToList();

            foreach (var a in delMatList)
            {
                MaterialLedgerDto ml = new MaterialLedgerDto();
                ml.LocationRefId = dto.LocationRefId;
                ml.MaterialRefId = a.MaterialRefId;
                ml.LedgerDate = item.ReturnDate.Date;
                ml.Return = ( -1 *  a.ReturnQty);
                ml.UnitId = a.UnitRefId;
                ml.PlusMinus = "U";
                var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);

                _returnDetailRepo.Delete(a.Id);
            }

            CheckErrors(await _returnManager.CreateSync(item));

            return new IdInput
            {
                Id = input.Return.Id.Value
            };
        }

        protected virtual async Task<IdInput> CreateReturn(CreateOrUpdateReturnInput input)
        {
            var dto = input.Return.MapTo<Return>();

            if(input.ReturnDetail == null || input.ReturnDetail.Count()==0)
            {
                throw new UserFriendlyException(L("MinimumOneDetail"));
            }
            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == dto.LocationRefId);
            dto.AccountDate = loc.HouseTransactionDate.Value;
            var retId = await _returnRepo.InsertAndGetIdAsync(dto);

            foreach (ReturnDetailViewDto items in input.ReturnDetail.ToList())
            {
                ReturnDetail idDto = new ReturnDetail();
                idDto.ReturnRefId = (int)dto.Id;
                idDto.MaterialRefId = items.MaterialRefId;
                idDto.RecipeRefId = items.RecipeRefId;
                idDto.ReturnQty = items.ReturnQty;
                idDto.Remarks = items.Remarks;
                idDto.Sno = items.Sno;
                idDto.UnitRefId = items.UnitRefId;

                var retId2 = await _returnDetailRepo.InsertAndGetIdAsync(idDto);


                MaterialLedgerDto ml = new MaterialLedgerDto();
                ml.LocationRefId = dto.LocationRefId;
                ml.MaterialRefId = idDto.MaterialRefId;
                ml.LedgerDate = dto.ReturnDate;
                ml.Return = idDto.ReturnQty;
                ml.UnitId = idDto.UnitRefId;

                var matLedgerId=await _materialAppService.UpdateMaterialLedger(ml);
                idDto.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;

            }

            CheckErrors(await _returnManager.CreateSync(dto));

            return new IdInput
            {
                Id = retId
            };
        }

        public async Task<ListResultOutput<ReturnListDto>> GetIds()
        {
            var lstReturn = await _returnRepo.GetAll().ToListAsync();
            return new ListResultOutput<ReturnListDto>(lstReturn.MapTo<List<ReturnListDto>>());
        }
    }
}