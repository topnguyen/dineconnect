﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Features;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Session;
using Abp.UI;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Features;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.House.Master;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Utility.Exporter;

namespace DinePlan.DineConnect.House.Transaction.Implementation
{
    public class PurchaseReturnAppService : DineConnectAppServiceBase, IPurchaseReturnAppService
    {
        private readonly IExcelExporter _exporter;
        private readonly IRepository<Invoice> _invoiceRepo;
        private readonly IInwardDirectCreditAppService _inwarddirectcreditAppService;
        private readonly IRepository<InwardDirectCreditDetail> _inwarddirectcreditDetailRepo;
        private readonly IRepository<InwardDirectCredit> _inwarddirectcreditRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IMaterialAppService _materialAppService;
        private readonly IRepository<MaterialGroupCategory> _materialGroupCategoryRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<PurchaseReturnDetail> _purchasereturndetailRepo;
        private readonly IPurchaseReturnListExcelExporter _purchaseReturnExporter;
        private readonly IPurchaseReturnManager _purchasereturnManager;
        private readonly IRepository<PurchaseReturn> _purchasereturnRepo;
        private readonly IRepository<PurchaseReturnTaxDetail> _purchasereturntaxdetailRepo;
        private readonly IRepository<Supplier> _supplierRepo;
        private readonly IRepository<SupplierMaterial> _supplierMaterialRepo;
        private readonly IRepository<Tax> _taxRepo;
        private readonly IRepository<TaxTemplateMapping> _taxtemplatemappingRepo;
        private readonly IRepository<Unit> _unitRepo;
        private readonly IRepository<InvoiceDirectCreditLink> _invoiceDirectCreditLinkRepo;
        //private readonly IRepository<PurchaseOrder> _purchaseorderRepo;
        //private readonly IRepository<PurchaseOrderDetail> _purchaseorderdetailRepo;
        //private readonly IRepository<PurchaseOrderTaxDetail> _pruchaseorderTaxRepo;

        public PurchaseReturnAppService(IPurchaseReturnManager purchasereturnManager,
            IRepository<PurchaseReturn> purchaseReturnRepo,
            IRepository<PurchaseReturnDetail> purchasereturndetailRepo,
            IRepository<PurchaseReturnTaxDetail> purchasereturntaxdetailRepo,
            IRepository<Material> materialRepo,
            IRepository<InwardDirectCredit> inwarddirectcreditRepo,
            IRepository<InwardDirectCreditDetail> inwarddirectcreditDetailRepo,
            IRepository<Unit> unitRepo,
            IRepository<Tax> taxRepo,
            IRepository<TaxTemplateMapping> taxtemplatemappingRepo,
            IRepository<Location> locationRepo,
            IMaterialAppService materialAppService,
            IRepository<InvoiceTaxDetail> invoicetaxdetailRepo,
            IRepository<Supplier> supplierRepo,
            IRepository<SupplierMaterial> supplierMaterialRepo,
            IInwardDirectCreditAppService inwarddirectcreditAppService,
            IRepository<MaterialGroupCategory> materialGroupCategoryRepo,
            IPurchaseReturnListExcelExporter purchaseReturnExporter,
            IRepository<Invoice> invoiceRepo,
            IExcelExporter exporter,
            IRepository<InvoiceDirectCreditLink> invoiceDirectCreditLinkRepo)
        {
            _purchasereturnManager = purchasereturnManager;
            _purchasereturnRepo = purchaseReturnRepo;
            _exporter = exporter;
            _purchasereturndetailRepo = purchasereturndetailRepo;
            _purchasereturntaxdetailRepo = purchasereturntaxdetailRepo;
            _materialRepo = materialRepo;
            _inwarddirectcreditRepo = inwarddirectcreditRepo;
            _inwarddirectcreditDetailRepo = inwarddirectcreditDetailRepo;
            _unitRepo = unitRepo;
            _taxRepo = taxRepo;
            _taxtemplatemappingRepo = taxtemplatemappingRepo;
            _locationRepo = locationRepo;
            _materialAppService = materialAppService;
            _supplierRepo = supplierRepo;
            _supplierMaterialRepo = supplierMaterialRepo;
            _inwarddirectcreditAppService = inwarddirectcreditAppService;
            _materialGroupCategoryRepo = materialGroupCategoryRepo;
            _purchaseReturnExporter = purchaseReturnExporter;
            _invoiceRepo = invoiceRepo;
            _invoiceDirectCreditLinkRepo = invoiceDirectCreditLinkRepo;
            //_purchaseorderRepo = purchaseorderRepo;
            //_purchaseorderdetailRepo = purchaseorderdetailRepo;
            //_pruchaseorderTaxRepo = pruchaseorderTaxRepo;
        }

        public async Task<PagedResultOutput<PurchaseReturnListDto>> GetAll(GetPurchaseReturnInput input)
        {
            var rsSupplier = _supplierRepo.GetAll()
                .WhereIf(!input.Filter.IsNullOrEmpty(), p => p.SupplierName.Contains(input.Filter));

            var rsPurchaseReturn =
                _purchasereturnRepo.GetAll()
                    .Where(t => t.LocationRefId == input.LocationRefId)
                    .WhereIf(!input.ReferenceNumber.IsNullOrEmpty(),
                        t => t.ReferenceNumber.Contains(input.ReferenceNumber));


            if (input.StartDate != null && input.EndDate != null)
            {
                rsPurchaseReturn =
                    rsPurchaseReturn.Where(t => t.ReturnDate >= input.StartDate && t.ReturnDate <= input.EndDate);
            }

            var allItems = (from pr in rsPurchaseReturn.WhereIf(input.Id.HasValue, t=>t.Id==input.Id)
                join sup in rsSupplier
                    on pr.SupplierRefId equals sup.Id

                select new PurchaseReturnListDto
                {
                    Id = pr.Id,
                    ReturnDate = pr.ReturnDate,
                    ReferenceNumber = pr.ReferenceNumber,
                    SupplierRefId = pr.SupplierRefId,
                    SupplierRefName = sup.SupplierName,
                    SupplierCode = sup.SupplierCode,
                    CreationTime = pr.CreationTime,
                    DcRefId = pr.DcRefId,
                    InvoiceRefId = pr.InvoiceRefId,
                    LocationRefId = pr.LocationRefId,
                    Remarks = pr.Remarks,
                    PurchaseReturnAmount = pr.PurchaseReturnAmount,
                    CreatorUserId = pr.CreatorUserId
                });


            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<PurchaseReturnListDto>>();
            if (input.TaxShowFlag)
            {
                var invoiceIds = allItems.Select(t => t.Id).ToArray();

                var invoiceDetail =
                    await
                        _purchasereturndetailRepo.GetAll()
                            .Where(t => invoiceIds.Contains(t.PurchaseReturnRefId))
                            .ToListAsync();

                foreach (var lst in allListDtos)
                {
                    var taxAmount =
                        invoiceDetail.Where(t => t.PurchaseReturnRefId == lst.Id).ToList().Sum(t => t.TaxAmount);
                    lst.TaxAmount = taxAmount;
                    lst.AmountWithoutTax = lst.PurchaseReturnAmount - lst.TaxAmount;
                }
            }

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<PurchaseReturnListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetPurchaseReturnInput input)
        {
            input.MaxResultCount = AppConsts.MaxPageSize;

            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<PurchaseReturnListDto>>();
            return _purchaseReturnExporter.ExportToFile(allListDtos);
        }

        public async Task<GetPurchaseReturnForEditOutput> GetPurchaseReturnForEdit(NullableIdInput input)
        {
            PurchaseReturnEditDto editDto;
            List<PurchaseReturnDetailViewDto> editDetailDto;

            if (input.Id.HasValue)
            {
                var hDto = await _purchasereturnRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<PurchaseReturnEditDto>();

                editDto.InvoiceReferenceNumber = "";

                var supplier = await _supplierRepo.FirstOrDefaultAsync(t => t.Id == editDto.SupplierRefId);
                if (supplier!=null)
                {
                    editDto.SupplierCode = supplier.SupplierCode;
                }

                if (editDto.InvoiceRefId != null)
                {
                    var rsInvoice = await _invoiceRepo.FirstOrDefaultAsync(t => t.Id == editDto.InvoiceRefId);
                    if (rsInvoice == null)
                        throw new UserFriendlyException(L("InvoiceNotFound", editDto.InvoiceRefId));

                    editDto.InvoiceReferenceNumber = L("Invoice") + " " + L("Ref#") + " " + L("Dated") + " " +
                                                     rsInvoice.InvoiceDate.ToString("dd-MMM-yy") + " , " + L("#") +
                                                     rsInvoice.InvoiceNumber;

                    List<InvoiceDirectCreditLinkViewDto> editInvoiceDCLink = await (from invDC in _invoiceDirectCreditLinkRepo.GetAll()
                                                                                        .Where(a => a.InvoiceRefId == editDto.InvoiceRefId.Value)
                                               select new InvoiceDirectCreditLinkViewDto
                                               {
                                                   Id = invDC.Id,
                                                   InvoiceRefId = invDC.InvoiceRefId,
                                                   InwardDirectCreditRefId = invDC.InwardDirectCreditRefId,
                                                   MaterialRefId = invDC.MaterialRefId
                                               }).ToListAsync();

                    string strPoReferenceCodes = "";
                    string strInvReferenceCodes = "";
                    if (editInvoiceDCLink.Count > 0)
                    {
                        var arrInwCreditRefIds = editInvoiceDCLink.Select(t => t.InwardDirectCreditRefId).ToList();
                        var inwardMasterList = await _inwarddirectcreditRepo.GetAll().Where(t => arrInwCreditRefIds.Contains(t.Id)).ToListAsync();
                        var arrPoRefIds = inwardMasterList.Where(t => t.PoRefId.HasValue).Select(t => t.PoRefId.Value).ToList();
                        if (inwardMasterList.Count > 0)
                        {
                            foreach (var im in inwardMasterList)
                            {
                                if (!im.PurchaseOrderReferenceFromOtherErp.IsNullOrEmpty())
                                strPoReferenceCodes = strPoReferenceCodes + im.PurchaseOrderReferenceFromOtherErp + ",";

                                if (!im.InvoiceNumberReferenceFromOtherErp.IsNullOrEmpty())
                                strInvReferenceCodes = strInvReferenceCodes + im.InvoiceNumberReferenceFromOtherErp + ",";
                            }
                            if (strPoReferenceCodes.Length > 0)
                                strPoReferenceCodes = strPoReferenceCodes.Left(strPoReferenceCodes.Length - 1);
                            if (strInvReferenceCodes.Length > 0)
                                strInvReferenceCodes = strInvReferenceCodes.Left(strPoReferenceCodes.Length - 1);
                        }
                    }
                    editDto.InvoiceNumberReferenceFromOtherErp = strInvReferenceCodes;
                    editDto.PurchaseOrderReferenceFromOtherErp = strPoReferenceCodes;
                }

                editDetailDto = await (from invDet in _purchasereturndetailRepo.GetAll()
                    .Where(a => a.PurchaseReturnRefId == input.Id.Value)
                    join mat in _materialRepo.GetAll() on invDet.MaterialRefId equals mat.Id
                    join unit in _unitRepo.GetAll() on mat.DefaultUnitId equals unit.Id
                    join uiss in _unitRepo.GetAll() on invDet.UnitRefId equals uiss.Id
                    select new PurchaseReturnDetailViewDto
                    {
                        Id = invDet.Id,
                        MaterialRefId = invDet.MaterialRefId,
                        MaterialRefName = mat.MaterialName,
                        PurchaseReturnRefId = invDet.PurchaseReturnRefId,
                        NetAmount = invDet.NetAmount,
                        Price = invDet.Price,
                        TaxAmount = invDet.TaxAmount,
                        TotalAmount = invDet.TotalAmount,
                        Quantity = invDet.Quantity,
                        Remarks = invDet.Remarks,
                        Sno = invDet.Sno,
                        Uom = unit.Name,
                        UnitRefId = invDet.UnitRefId,
                        UnitRefName = uiss.Name,
                        DefaultUnitId = mat.DefaultUnitId,
                        DefaultUnitName = unit.Name,
                        CreatorUserId = invDet.CreatorUserId
                    }).ToListAsync();


                var materialRefIds = editDetailDto.Select(t => t.MaterialRefId).ToList();
                var rsSupplierMaterials = await _supplierMaterialRepo.GetAllListAsync(t => t.SupplierRefId == editDto.SupplierRefId && materialRefIds.Contains(t.MaterialRefId));

                foreach (var lst in editDetailDto)
                {
                    var supmat = rsSupplierMaterials.FirstOrDefault(t => t.LocationRefId == editDto.LocationRefId && t.MaterialRefId == lst.MaterialRefId);
                    if (supmat == null)
                    {
                        supmat = rsSupplierMaterials.FirstOrDefault(t => t.LocationRefId == null && t.MaterialRefId == lst.MaterialRefId);
                    }
                    if (supmat != null)
                    {
                        lst.SupplierMaterialAliasName = supmat.SupplierMaterialAliasName;
                    }
                }



                foreach (var det in editDetailDto)
                {
                    var taxlist = await (from potax in _purchasereturntaxdetailRepo.GetAll().
                        Where(pot => pot.PurchaseReturnRefId == input.Id.Value && pot.MaterialRefId == det.MaterialRefId)
                        join tax in _taxRepo.GetAll()
                            on potax.TaxRefId equals tax.Id
                        select new TaxForMaterial
                        {
                            TaxRefId = potax.TaxRefId,
                            TaxName = tax.TaxName,
                            TaxRate = potax.TaxRate,
                            TaxValue = potax.TaxValue,
                            SortOrder = tax.SortOrder,
                            Rounding = tax.Rounding
                        }).ToListAsync();

                    det.TaxForMaterial = taxlist;

                    var applicabletaxes = new List<ApplicableTaxesForMaterial>();

                    foreach (var tax in taxlist.OrderBy(t => t.SortOrder))
                    {
                        applicabletaxes.Add(new ApplicableTaxesForMaterial
                        {
                            TaxName = tax.TaxName,
                            TaxRefId = tax.TaxRefId,
                            TaxRate = tax.TaxRate,
                            SortOrder = tax.SortOrder,
                            Rounding = tax.Rounding,
                            TaxCalculationMethod = tax.TaxCalculationMethod
                        });
                    }
                    det.ApplicableTaxes = applicabletaxes;
                }
            }
            else
            {
                editDto = new PurchaseReturnEditDto();
                editDetailDto = new List<PurchaseReturnDetailViewDto>();
            }

            return new GetPurchaseReturnForEditOutput
            {
                PurchaseReturn = editDto,
                PurchaseReturnDetail = editDetailDto
            };
        }

        public async Task<IdInput> CreateOrUpdatePurchaseReturn(CreateOrUpdatePurchaseReturnInput input)
        {
            return await CreatePurchaseReturn(input);
        }

        public async Task DeletePurchaseReturn(IdInput input)
        {
			//@@Pending Reverse the Stock, Can not delete the Date for past 
			var purchaseReturnMaster = await _purchasereturnRepo.FirstOrDefaultAsync(t => t.Id == input.Id);

			var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == purchaseReturnMaster.LocationRefId);

			if (loc.HouseTransactionDate.Value.Date != purchaseReturnMaster.ReturnDate.Date)
			{
				throw new UserFriendlyException(L("PurchaseReturnDeleteError", loc.HouseTransactionDate.Value.ToString("yyyy-MMM-dd"), purchaseReturnMaster.ReturnDate.Date.ToString("yyyy-MMM-dd"), purchaseReturnMaster.Id, purchaseReturnMaster.ReferenceNumber));
			}

			var purchaseReturnDetail = await _purchasereturndetailRepo.GetAllListAsync(t => t.PurchaseReturnRefId == input.Id);

			foreach (var items in purchaseReturnDetail)
			{
				var ml = new MaterialLedgerDto();
				ml.LocationRefId = purchaseReturnMaster.LocationRefId;
				ml.MaterialRefId = items.MaterialRefId;
				ml.LedgerDate = purchaseReturnMaster.ReturnDate;
				ml.SupplierReturn = -1 * items.Quantity;
				ml.UnitId = items.UnitRefId;
                ml.SupplierRefId = purchaseReturnMaster.SupplierRefId;
				var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
			}

			await _purchasereturntaxdetailRepo.DeleteAsync(u => u.PurchaseReturnRefId == input.Id);
			await _purchasereturndetailRepo.DeleteAsync(u => u.PurchaseReturnRefId == input.Id);
            await _purchasereturnRepo.DeleteAsync(input.Id);
        }

        //protected virtual async Task<IdInput> UpdatePurchaseReturn(CreateOrUpdatePurchaseReturnInput input)
        //{
        //    var item = await _purchasereturnRepo.GetAsync(input.PurchaseReturn.Id.Value);
        //    var dto = input.PurchaseReturn;

        //    //TODO: SERVICE PurchaseReturn Update Individually

        //    CheckErrors(await _purchasereturnManager.CreateSync(item));

        //    return new IdInput
        //    {
        //        Id = dto.Id.Value
        //    };
        //}


        protected virtual async Task<IdInput> CreatePurchaseReturn(CreateOrUpdatePurchaseReturnInput input)
        {
            var dto = input.PurchaseReturn.MapTo<PurchaseReturn>();

            if (input.PurchaseReturnDetail == null || input.PurchaseReturnDetail.Count() == 0)
            {
                throw new UserFriendlyException(L("MinimumOneDetail"));
            }
            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == dto.LocationRefId);

            if (loc.HouseTransactionDate.Value.Date == dto.ReturnDate.Date && dto.ReturnDate.Date==DateTime.Today)
            {
                dto.ReturnDate = DateTime.Now;
            }
            dto.AccountDate = loc.HouseTransactionDate.Value;

            var retId = await _purchasereturnRepo.InsertAndGetIdAsync(dto);


            foreach (var items in input.PurchaseReturnDetail)
            {
                var invdet = new PurchaseReturnDetail();

                invdet.PurchaseReturnRefId = dto.Id;
                invdet.Sno = items.Sno;
                invdet.MaterialRefId = items.MaterialRefId;
                invdet.Quantity = items.Quantity;
                invdet.Price = items.Price;
                invdet.TotalAmount = items.TotalAmount;
                invdet.TaxAmount = items.TaxAmount;
                invdet.NetAmount = items.NetAmount;
                invdet.Remarks = items.Remarks;
                invdet.UnitRefId = items.UnitRefId;

                var retId2 = await _purchasereturndetailRepo.InsertAndGetIdAsync(invdet);

                foreach (var tax in items.TaxForMaterial)
                {
                    var taxForPoDetail = new PurchaseReturnTaxDetail
                    {
                        PurchaseReturnRefId = dto.Id,
                        Sno = items.Sno,
                        MaterialRefId = items.MaterialRefId,
                        TaxRefId = tax.TaxRefId,
                        TaxRate = tax.TaxRate,
                        TaxValue = tax.TaxValue
                    };
                    var retId3 = await _purchasereturntaxdetailRepo.InsertAndGetIdAsync(taxForPoDetail);
                }

                var ml = new MaterialLedgerDto();
                ml.LocationRefId = dto.LocationRefId;
                ml.MaterialRefId = invdet.MaterialRefId;
                ml.LedgerDate = dto.ReturnDate;
                ml.SupplierReturn = invdet.Quantity;
                ml.UnitId = items.UnitRefId;
                ml.SupplierRefId = input.PurchaseReturn.SupplierRefId;
                
                var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                invdet.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;
            }

            CheckErrors(await _purchasereturnManager.CreateSync(dto));

            return new IdInput
            {
                Id = retId
            };
        }

        public virtual async Task<PurchaseReturnDetailOutputDto> GetPurchaseReturnReport(PurchaseReturnDetailInputDto input)
        {
            var iQueryablePurReturnMaster =  _purchasereturnRepo.GetAll().Where(t=> t.LocationRefId==input.LocationRefId &&  DbFunctions.TruncateTime(t.ReturnDate)>= DbFunctions.TruncateTime(input.StartDate) && DbFunctions.TruncateTime(t.ReturnDate) <= DbFunctions.TruncateTime(input.EndDate));

            var iqueryableReturnDetails = _purchasereturndetailRepo.GetAll();
            #region Filter
            if (input.SupplierRefId.HasValue)
                if (input.SupplierRefId.Value>0)
                iQueryablePurReturnMaster = iQueryablePurReturnMaster.Where(t => t.SupplierRefId == input.SupplierRefId.Value);

            if (input.MaterialRefId.HasValue)
                if (input.MaterialRefId.Value>0)
                iqueryableReturnDetails = iqueryableReturnDetails.Where(t => t.MaterialRefId == input.MaterialRefId.Value);
            #endregion

            var allItems = await (from master in iQueryablePurReturnMaster 
                                   join detail in iqueryableReturnDetails on master.Id equals detail.PurchaseReturnRefId
                                   join supplier in _supplierRepo.GetAll() on master.SupplierRefId equals supplier.Id
                                   join mat in _materialRepo.GetAll() on detail.MaterialRefId equals mat.Id
                                   join unit in _unitRepo.GetAll() on mat.DefaultUnitId equals unit.Id
                                   join uiss in _unitRepo.GetAll() on detail.UnitRefId equals uiss.Id
                                   select new PurchaseReturnDetailReportDto
                                   {
                                       Id = detail.Id,
                                       MaterialRefId = detail.MaterialRefId,
                                       MaterialPetName = mat.MaterialPetName,
                                       MaterialRefName = mat.MaterialName,
                                       PurchaseReturnRefId = detail.PurchaseReturnRefId,
                                       NetAmount = detail.NetAmount,
                                       Price = detail.Price,
                                       TaxAmount = detail.TaxAmount,
                                       TotalAmount = detail.TotalAmount,
                                       Quantity = detail.Quantity,
                                       Remarks = detail.Remarks,
                                       Sno = detail.Sno,
                                       Uom = unit.Name,
                                       UnitRefId = detail.UnitRefId,
                                       UnitRefName = uiss.Name,
                                       DefaultUnitId = mat.DefaultUnitId,
                                       DefaultUnitName = unit.Name,
                                       CreatorUserId = detail.CreatorUserId,
                                       ReturnDate = master.ReturnDate,
                                       ReferenceNumber = master.ReferenceNumber,
                                       SupplierRefId = master.SupplierRefId,
                                       SupplierRefName = supplier.SupplierName
                                   }).ToListAsync();
            List<int> arrMaterialRefIds = allItems.Select(t => t.MaterialRefId).ToList();
            var materials = await _materialRepo.GetAllListAsync(t=> arrMaterialRefIds.Contains(t.Id));

            PurchaseReturnDetailOutputDto outputDto = new PurchaseReturnDetailOutputDto();
            foreach (var det in allItems)
            {
                var mat = materials.FirstOrDefault(t => t.Id == det.MaterialRefId);
                if (mat.DefaultUnitId == det.UnitRefId)
                    det.QuantityInPurchaseUnit = det.Quantity;  //  Purchase Unit
                else if (mat.IssueUnitId == det.UnitRefId)
                    det.QuantityInIssueUnit = det.Quantity; // Recipe Unit
                else if (mat.StockAdjustmentUnitId == det.UnitRefId)
                    det.QuantityInStkAdjustedUnit = det.Quantity;   // Inventory Unit
                else
                    det.QuantityInIssueUnit = det.Quantity;         //  Except Default and Stock adj unit all units are as Recipe Unit
                outputDto.TotalTaxAmount = outputDto.TotalTaxAmount + det.TaxAmount;
                outputDto.TotalPurchaseReturnAmount = outputDto.TotalPurchaseReturnAmount + det.TotalAmount;
                outputDto.NetPurchaseReturnAmount = outputDto.NetPurchaseReturnAmount + det.NetAmount;
            }
            outputDto.ReturnList = allItems;
            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
            outputDto.LocationInfo = loc.MapTo<LocationListDto>();
            outputDto.StartDate = input.StartDate;
            outputDto.EndDate = input.EndDate;
            outputDto.PreparedUserId = AbpSession.UserId;
            string userName = "";
            var user = await UserManager.GetUserByIdAsync(AbpSession.UserId.Value);
            if (user != null)
                userName = user.Name;
            outputDto.PreparedUserName = userName;
            outputDto.PrintedTime = DateTime.Now;
            var customReport = await FeatureChecker.GetValueAsync(AppFeatures.Custom);
            bool customCrgReport = false;
            if (customReport.ToUpper() == "TRUE")
                customCrgReport = true;
            customCrgReport = true;
            outputDto.DoesCustomReportFormat = customCrgReport;
            return outputDto;
        }

        public async Task<FileDto> GetPurchaseReturnReportToExcel(PurchaseReturnDetailInputDto input)
        {
            var allList = await GetPurchaseReturnReport(input);
            return _purchaseReturnExporter.PurchaseReportExportToFile(allList);
        }
        
    }
}