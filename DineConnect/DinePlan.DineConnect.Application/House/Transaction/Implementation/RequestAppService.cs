﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.House.Master;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using System;


namespace DinePlan.DineConnect.House.Transaction.Implementation
{
    public class RequestAppService : DineConnectAppServiceBase, IRequestAppService
    {

        private readonly IRequestManager _requestManager;
        private readonly IRepository<Request> _requestRepo;
        private readonly IRepository<RequestRecipeDetail> _requestrecipedetailRepo;
        private readonly IRepository<RequestDetail> _requestDetailRepo;
        private readonly IMaterialAppService _materialAppService;
        private readonly IRepository<MaterialIngredient> _materialrecipeIngredientRepo;
        private readonly IRepository<MaterialRecipeTypes> _materialrecipeRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<Unit> _unitRepo;
        private readonly IRepository<MaterialLocationWiseStock> _materiallocationwisestockRepo;
        private readonly IRepository<MaterialIngredient> _materialingredientRepo;
        private readonly IRepository<MaterialGroupCategory> _materialgroupcategoryRepo;
        private readonly IRepository<ProductionUnit> _productionunitRepo;
        private readonly IRequestListExcelExporter _requestExporter;

        public RequestAppService(IRequestManager requestManager,
            IRepository<Request> requestRepo,
            IRepository<RequestRecipeDetail> requestrecipedetailRepo,
            IRepository<RequestDetail> requestDetailRepo,
            IMaterialAppService materialAppService,
            IRepository<MaterialIngredient> recipeIngredientRepo,
            IRepository<MaterialRecipeTypes> reciperepo,
            IRepository<Material> materialRepo,
            IRepository<Unit> unitRepo,
            IRepository<MaterialLocationWiseStock> materiallocationwisestockRepo,
            IRepository<MaterialIngredient> materialingredientRepo,
            IRepository<MaterialGroupCategory> materialgroupcategoryRepo,
            IRepository<ProductionUnit> productionunitRepo,
            IRequestListExcelExporter requestExporter
            )
        {
            _requestManager = requestManager;
            _requestRepo = requestRepo;
            _requestrecipedetailRepo = requestrecipedetailRepo;
            _requestDetailRepo = requestDetailRepo;
            _materialAppService = materialAppService;
            _materialrecipeIngredientRepo = recipeIngredientRepo;
            _materialrecipeRepo = reciperepo;
            _materialRepo = materialRepo;
            _unitRepo = unitRepo;
            _materiallocationwisestockRepo = materiallocationwisestockRepo;
            _materialingredientRepo = materialingredientRepo;
            _materialgroupcategoryRepo = materialgroupcategoryRepo;
            _productionunitRepo = productionunitRepo;
            _requestExporter = requestExporter;
        }

        public async Task<PagedResultOutput<RequestListDto>> GetAll(GetRequestInput input)
        {
            /// VArek 
            //    asdf
          

            var rsRequest = _requestRepo.GetAll().Where(t => t.LocationRefId == input.DefaultLocationRefId).WhereIf(!input.Filter.IsNullOrEmpty(), p => p.RequestSlipNumber.Contains(input.Filter));

            if (input.StartDate != null && input.EndDate != null)
            {
                rsRequest =rsRequest.Where(a =>
                                DbFunctions.TruncateTime(a.RequestTime) >= DbFunctions.TruncateTime(input.StartDate)
                                &&
                                DbFunctions.TruncateTime(a.RequestTime) <= DbFunctions.TruncateTime(input.EndDate));
               // rsRequest.Where(t => t.RequestTime >= input.StartDate && t.RequestTime <= input.EndDate);
            }

            var allItems = from request in rsRequest
                           join productionunit in _productionunitRepo.GetAll()
                           on request.ProductionUnitRefId equals productionunit.Id into gj
                           from g in gj.DefaultIfEmpty()
                           select new RequestListDto
                           {
                               Id = request.Id,
                               LocationRefId = request.LocationRefId,
                               RequestTime = request.RequestTime,
                               RequestSlipNumber = request.RequestSlipNumber,
                               MaterialGroupCategoryRefId = request.MaterialGroupCategoryRefId,
                               ProductionUnitRefId = request.ProductionUnitRefId,
                               ProductionUnitRefName = g.Name,
                               CompletedStatus = request.CompletedStatus,
                               CreationTime = request.CreationTime

                           };

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<RequestListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<RequestListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetRequestInput input)
        {
            input.MaxResultCount = AppConsts.MaxPageSize;

            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<RequestListDto>>();
            return _requestExporter.ExportToFile(allListDtos);
        }
        public async Task<GetRequestForEditOutput> GetRequestForEdit(NullableIdInput input)
        {
            RequestEditDto editDto;
            List<RequestRecipeDetailListDto> editRecipeDetailDtos;
            List<RequestDetailViewDto> editDetailDto;

            if (input.Id.HasValue)
            {
                var hDto = await _requestRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<RequestEditDto>();

                editRecipeDetailDtos = await (from reqRecipeDet in _requestrecipedetailRepo.GetAll().Where(a => a.RequestRefId == input.Id.Value)
                                              join mat in _materialRepo.GetAll()
                                              on reqRecipeDet.RecipeRefId equals mat.Id
                                              join un in _unitRepo.GetAll()
                                              on mat.DefaultUnitId equals un.Id
                                              join uiss in _unitRepo.GetAll()
                                              on reqRecipeDet.UnitRefId equals uiss.Id
                                              join matloc in _materiallocationwisestockRepo.GetAll()
                                             .Where(mls => mls.LocationRefId == editDto.LocationRefId) on mat.Id equals matloc.MaterialRefId
                                              select new RequestRecipeDetailListDto
                                              {
                                                  Id = reqRecipeDet.Id,
                                                  RequestRefId = reqRecipeDet.RequestRefId,
                                                  RecipeRefId = reqRecipeDet.RecipeRefId,
                                                  RecipeRefName = mat.MaterialName,
                                                  Sno = reqRecipeDet.Sno,
                                                  RecipeProductionQty = reqRecipeDet.RecipeProductionQty,
                                                  Uom = un.Name,
                                                  MultipleBatchProductionAllowed = reqRecipeDet.MultipleBatchProductionAllowed,
                                                  Remarks = reqRecipeDet.Remarks,
                                                  CurrentInHand = matloc.CurrentInHand,
                                                  UnitRefId = reqRecipeDet.UnitRefId,
                                                  UnitRefName = uiss.Name,
                                                  DefaultUnitId = mat.DefaultUnitId,
                                                  DefaultUnitName = un.Name
                                              }).ToListAsync();


                editDetailDto = await (from issDet in _requestDetailRepo.GetAll().
                                       Where(a => a.RequestRefId == input.Id.Value)
                                       join mat in _materialRepo.GetAll()
                                       on issDet.MaterialRefId equals mat.Id
                                       join un in _unitRepo.GetAll()
                                       on mat.DefaultUnitId equals un.Id
                                       join uiss in _unitRepo.GetAll()
                                       on issDet.UnitRefId equals uiss.Id 
                                       join matloc in _materiallocationwisestockRepo.GetAll().Where(t=>t.LocationRefId == editDto.LocationRefId && t.IsActiveInLocation==true)
                                       on mat.Id equals matloc.MaterialRefId 
                                       select new RequestDetailViewDto
                                       {
                                           Id = issDet.Id,
                                           RequestQty = issDet.RequestQty,
                                           RequestRefId = issDet.RequestRefId,
                                           MaterialRefId = issDet.MaterialRefId,
                                           MaterialRefName = mat.MaterialName,
                                           Sno = issDet.Sno,
                                           DefaultUnitId = mat.DefaultUnitId,
                                           DefaultUnitName = un.Name,
                                           CurrentInHand = matloc.CurrentInHand,
                                           UnitRefId = issDet.UnitRefId,
                                           UnitRefName = uiss.Name
                                       }).ToListAsync();
            }
            else
            {

                editDto = new RequestEditDto();
                editDto.RequestTime = DateTime.Now;
                editRecipeDetailDtos = new List<RequestRecipeDetailListDto>();
                editDetailDto = new List<RequestDetailViewDto>();
                
            }

            return new GetRequestForEditOutput
            {
                Request = editDto,
                RequestRecipeDetail = editRecipeDetailDtos,
                RequestDetail = editDetailDto
            };
        }

        public async Task<GetRequestForStandardRecipe> GetRequestForRecipe(GetStandardIngridient input)
        {
            List<RequestDetailViewDto> editDetailDto;

            var mas = _materialrecipeRepo.GetAll().Where(a => a.MaterailRefId == input.recipeRefId).ToList();

            if (mas == null || mas.Count == 0)
            {
                throw new UserFriendlyException(L("MaterialRecipeInformationMissing"));
            }
            decimal standardProductionQty = mas[0].PrdBatchQty;

            editDetailDto = await (from issDet in _materialrecipeIngredientRepo.GetAll().Where(a => a.RecipeRefId == input.recipeRefId)
                                   join mat in _materialRepo.GetAll()
                                   on issDet.MaterialRefId equals mat.Id
                                   orderby issDet.UserSerialNumber
                                   join un in _unitRepo.GetAll() on mat.DefaultUnitId equals un.Id
                                   join uiss in _unitRepo.GetAll() on issDet.UnitRefId equals uiss.Id 
                                   join matloc in _materiallocationwisestockRepo.GetAll()
                                        .Where(mls => mls.LocationRefId == input.LocationRefId && mls.IsActiveInLocation==true) on mat.Id equals matloc.MaterialRefId
                                   select new RequestDetailViewDto
                                   {
                                       Id = issDet.Id,
                                       RequestQty = Math.Round(issDet.MaterialUsedQty / standardProductionQty * input.requestQtyforProduction, 2),
                                       RequestRefId = 0,
                                       MaterialRefId = issDet.MaterialRefId,
                                       Sno = 0,
                                       DefaultUnitId = mat.DefaultUnitId,
                                       DefaultUnitName = un.Name,
                                       UnitRefId = issDet.UnitRefId,
                                       UnitRefName = uiss.Name,
                                       CurrentInHand = matloc.CurrentInHand
                                   }).ToListAsync();

            return new GetRequestForStandardRecipe
            {
                RequestDetail = editDetailDto
            };
        }


        public async Task<int> GetMaxTokenNumber(DateTime selDt)
        {
            DateTime givenDt = selDt;
            var maxTokno = await (from iss in _requestRepo.GetAll()
                                 .Where(a => a.RequestTime.Year == givenDt.Year
                                 && a.RequestTime.Month == givenDt.Month
                                 && a.RequestTime.Day == givenDt.Day)
                                  select iss).ToListAsync();

            var TokenNumber = maxTokno.Count() + 1;
            return TokenNumber;
        }

        public async Task<IdInput> CreateOrUpdateRequest(CreateOrUpdateRequestInput input)
        {
            if (input.Request.Id.HasValue)
            {
                return await UpdateRequest(input);
            }
            else
            {
                return await CreateRequest(input);
            }
        }

        public async Task DeleteRequest(IdInput input)
        {
            var request = await _requestRepo.FirstOrDefaultAsync(t => t.Id == input.Id);

            if (request.CompletedStatus == true)
            {
                throw new UserFriendlyException(L("RequestIssuedAlready"));
            }

            var reqDetail = await _requestDetailRepo.GetAll().Where(t => t.RequestRefId == input.Id).ToListAsync();
            foreach (var det in reqDetail)
            {
                await _requestDetailRepo.DeleteAsync(det.Id);
            }

            await _requestRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task<IdInput> UpdateRequest(CreateOrUpdateRequestInput input)
        {
            var item = await _requestRepo.GetAsync(input.Request.Id.Value);

            if (input.RequestDetail == null || input.RequestDetail.Count() == 0)
            {
                throw new UserFriendlyException(L("MinimumOneDetail"));
            }

            var dto = input.Request;

            //@@Pending     Need to remove //
            if (input.RequestRecipeDetail != null && input.RequestRecipeDetail.Count>0)
            {
                if (input.RequestRecipeDetail.Count == 1)
                {
                    dto.RequestSlipNumber = dto.RequestSlipNumber.Substring(0, dto.RequestSlipNumber.IndexOf("/"));

                    int recId = input.RequestRecipeDetail[0].RecipeRefId;

                    var matname = _materialRepo.GetAll().FirstOrDefault(t => t.Id == recId);

                    int length = 25 - dto.RequestSlipNumber.Length;
                    if (matname.MaterialName.Length < length)
                        length = matname.MaterialName.Length;
                    dto.RequestSlipNumber = dto.RequestSlipNumber + "/" + matname.MaterialName.Substring(0, length);
                }
            }
            else if (input.Request.MaterialGroupCategoryRefId != 0)
            {
                int indexposition = dto.RequestSlipNumber.IndexOf("/");
                dto.RequestSlipNumber = dto.RequestSlipNumber.Substring(0, dto.RequestSlipNumber.IndexOf("/"));
                int length = 25 - dto.RequestSlipNumber.Length;
                var catname = _materialgroupcategoryRepo.GetAll().FirstOrDefault(t => t.Id == input.Request.MaterialGroupCategoryRefId).MaterialGroupCategoryName;

                if (catname.Length < length)
                    length = catname.Length;

                dto.RequestSlipNumber = dto.RequestSlipNumber + "/" + catname.Substring(0, length);
            }

            item.RequestTime = dto.RequestTime;
            item.LocationRefId = dto.LocationRefId;
            item.RequestSlipNumber = dto.RequestSlipNumber;
            item.RequestTime = dto.RequestTime;
            item.MaterialGroupCategoryRefId = dto.MaterialGroupCategoryRefId;
            item.ProductionUnitRefId = dto.ProductionUnitRefId;

            List<int> recipeToBeRetained = new List<int>();

            if (input.RequestRecipeDetail != null && input.RequestRecipeDetail.Count > 0)
            {
                foreach (var items in input.RequestRecipeDetail)
                {
                    var recipeRefId = items.RecipeRefId;
                    recipeToBeRetained.Add(recipeRefId);

                    var existingIssueDetail = _requestrecipedetailRepo.GetAllList(u => u.RequestRefId == dto.Id && u.RecipeRefId.Equals(recipeRefId));
                    if (existingIssueDetail.Count == 0)  //Add new record
                    {
                        RequestRecipeDetail ird = new RequestRecipeDetail();
                        ird.RequestRefId = (int)dto.Id;
                        ird.Sno = items.Sno;
                        ird.RecipeRefId = items.RecipeRefId;
                        ird.RecipeProductionQty = items.RecipeProductionQty;
                        ird.UnitRefId = items.UnitRefId;
                        ird.MultipleBatchProductionAllowed = items.MultipleBatchProductionAllowed;
                        ird.Remarks = items.Remarks;

                        var retRecipeId = await _requestrecipedetailRepo.InsertAndGetIdAsync(ird);

                    }
                    else
                    {
                        var editDetailDto = await _requestrecipedetailRepo.GetAsync(existingIssueDetail[0].Id);

                        editDetailDto.RequestRefId = (int)dto.Id;
                        editDetailDto.Sno = items.Sno;
                        editDetailDto.RecipeRefId = items.RecipeRefId;
                        editDetailDto.RecipeProductionQty = items.RecipeProductionQty;
                        editDetailDto.UnitRefId = items.UnitRefId;
                        editDetailDto.MultipleBatchProductionAllowed = items.MultipleBatchProductionAllowed;
                        editDetailDto.Remarks = items.Remarks;

                        var retRecipeId = await _requestrecipedetailRepo.InsertOrUpdateAndGetIdAsync(editDetailDto);

                    }
                }
            }

            var delRecieDetailsList = _requestrecipedetailRepo.GetAll().Where(a => a.RequestRefId == input.Request.Id.Value && !recipeToBeRetained.Contains(a.RecipeRefId)).ToList();

            foreach (var a in delRecieDetailsList)
            {
                _requestrecipedetailRepo.Delete(a.Id);
            }



            List<int> materialsToBeRetained = new List<int>();

            if (input.RequestDetail != null && input.RequestDetail.Count > 0)
            {
                foreach (var items in input.RequestDetail)
                {
                    var matRefId = items.MaterialRefId;
                    materialsToBeRetained.Add(matRefId);

                    var existingRequestDetail = _requestDetailRepo.GetAllList(u => u.RequestRefId == dto.Id && u.MaterialRefId.Equals(matRefId));
                    if (existingRequestDetail.Count == 0)  //Add new record
                    {
                        RequestDetail id = new RequestDetail();
                        id.RequestRefId = (int)dto.Id;
                        id.Sno = items.Sno;
                        id.RequestQty = items.RequestQty;
                        id.MaterialRefId = items.MaterialRefId;
                        id.UnitRefId = items.UnitRefId;

                        var retId2 = await _requestDetailRepo.InsertAndGetIdAsync(id);
                    }
                    else
                    {
                        var editDetailDto = await _requestDetailRepo.GetAsync(existingRequestDetail[0].Id);

                        decimal differcenceQty;
                        differcenceQty = items.RequestQty - editDetailDto.RequestQty;

                        editDetailDto.RequestRefId = (int)dto.Id;
                        editDetailDto.Sno = items.Sno;
                        editDetailDto.MaterialRefId = items.MaterialRefId;
                        editDetailDto.RequestQty = items.RequestQty;
                        editDetailDto.UnitRefId = items.UnitRefId;
                        

                        var retId3 = await _requestDetailRepo.InsertOrUpdateAndGetIdAsync(editDetailDto);


                    }
                }
            }

            var delMatList = _requestDetailRepo.GetAll().Where(a => a.RequestRefId == input.Request.Id.Value && !materialsToBeRetained.Contains(a.MaterialRefId)).ToList();

            foreach (var a in delMatList)
            {

                _requestDetailRepo.Delete(a.Id);
            }

            CheckErrors(await _requestManager.CreateSync(item));

            return new IdInput
            {
                Id = input.Request.Id.Value
            };
        }

        protected virtual async Task<IdInput> CreateRequest(CreateOrUpdateRequestInput input)
        {

            var dto = input.Request.MapTo<Request>();

            if (input.RequestDetail == null || input.RequestDetail.Count() == 0)
            {
                throw new UserFriendlyException(L("MinimumOneDetail"));
            }


            if (input.RequestRecipeDetail != null && input.RequestRecipeDetail.Count > 0)
            {
                if (input.RequestRecipeDetail.Count == 1)
                {

                     int recId = input.RequestRecipeDetail[0].RecipeRefId;

                    var matname = _materialRepo.GetAll().FirstOrDefault(t => t.Id == recId);

                    int length = 25 - dto.RequestSlipNumber.Length;
                    if (matname.MaterialName.Length < length)
                        length = matname.MaterialName.Length;
                    dto.RequestSlipNumber = dto.RequestSlipNumber + "/" + matname.MaterialName.Substring(0, length);
                }
            }
            else if (input.Request.MaterialGroupCategoryRefId != 0)
            {
                int length = 25 - dto.RequestSlipNumber.Length;
                var catname = _materialgroupcategoryRepo.GetAll().FirstOrDefault(t => t.Id == input.Request.MaterialGroupCategoryRefId).MaterialGroupCategoryName;

                if (catname.Length < length)
                    length = catname.Length;

                dto.RequestSlipNumber = dto.RequestSlipNumber + "/" + catname.Substring(0, length);
            }

            var retId = await _requestRepo.InsertAndGetIdAsync(dto);

            foreach (RequestRecipeDetailListDto items in input.RequestRecipeDetail.ToList())
            {
                RequestRecipeDetail ird = new RequestRecipeDetail();
                ird.RequestRefId = dto.Id;
                ird.Sno = items.Sno;
                ird.RecipeRefId = items.RecipeRefId;
                ird.RecipeProductionQty = items.RecipeProductionQty;
                ird.UnitRefId = items.UnitRefId;
                ird.MultipleBatchProductionAllowed = items.MultipleBatchProductionAllowed;
                ird.Remarks = items.Remarks;

                var retRecipeId = await _requestrecipedetailRepo.InsertAndGetIdAsync(ird);
            }

            foreach (RequestDetailViewDto items in input.RequestDetail.ToList())
            {
                RequestDetail id = new RequestDetail();
                id.RequestRefId = dto.Id;
                id.Sno = items.Sno;
                id.MaterialRefId = items.MaterialRefId;
                id.RequestQty = items.RequestQty;
                id.UnitRefId = items.UnitRefId;

                var retId2 = await _requestDetailRepo.InsertAndGetIdAsync(id);


            }



            CheckErrors(await _requestManager.CreateSync(dto));

            return new IdInput
            {
                Id = retId
            };
        }

        public async Task<ListResultOutput<RequestListDto>> GetIds()
        {
            var lstRequest = await _requestRepo.GetAll().ToListAsync();
            return new ListResultOutput<RequestListDto>(lstRequest.MapTo<List<RequestListDto>>());
        }


        public async Task<List<RequestListDto>> GetPendingRequests(InputLocation input)
        {
            var allItems = await _requestRepo.GetAll().Where(t => t.CompletedStatus == false).ToListAsync();

            var allListDtos = allItems.MapTo<List<RequestListDto>>();

            //int loopIndex = 0;
            //foreach (var item in allListDtos)
            //{
            //    if (item.RecipeRefId != 0 && item.MultipleBatchProductionAllowed == true)
            //    {
            //        var issueDet = await _issuedetailRepo.GetAll().Where(t => t.IssueRefId == item.Id).ToListAsync();
            //        var alreadyProduced = prodDet.Sum(t => t.ProductionQty);

            //        var pendingProductionQty = allItems[loopIndex].RecipeProductionQty - alreadyProduced;
            //        allListDtos[loopIndex].RecipeProductionQty = pendingProductionQty;
            //    }
            //    loopIndex++;
            //}



            return allListDtos;
        }


        public async Task<List<MaterialInfoDto>> GetMaterialForGivenRecipeCombobox(InputLocationAndRecipe input)
        {
            IQueryable<MaterialInfoDto> allItems;

            if (input.RecipeRefId>0)
            {
                allItems = (from mat in _materialRepo.GetAll()
                            join recIng in _materialingredientRepo.GetAll()
                            on mat.Id equals recIng.MaterialRefId
                            where recIng.RecipeRefId == input.RecipeRefId
                            join un in _unitRepo.GetAll()
                            on mat.DefaultUnitId equals un.Id
                            join issunit in _unitRepo.GetAll()
                            on mat.IssueUnitId equals issunit.Id
                            join matloc in _materiallocationwisestockRepo.GetAll().Where(t=> t.LocationRefId == input.LocationRefId && t.IsActiveInLocation==true)
                            on mat.Id equals matloc.MaterialRefId
                            select new MaterialInfoDto
                            {
                                MaterialRefId = mat.Id,
                                MaterialRefName = mat.MaterialName,
                                MaterialTypeId = mat.MaterialTypeId,
                                IsFractional = mat.IsFractional,
                                IsBranded = mat.IsFractional,
                                IsQuoteNeededForPurchase = mat.IsQuoteNeededForPurchase,
                                MaterialPetName = mat.MaterialPetName,
                                DefaultUnitId = mat.DefaultUnitId,
                                DefaultUnitName = un.Name,
                                IssueUnitId = mat.IssueUnitId,
                                IssueUnitName = issunit.Name,
                                IsNeedtoKeptinFreezer = mat.IsNeedtoKeptinFreezer,
                                IsMfgDateExists = mat.IsMfgDateExists,
                                WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                                IsHighValueItem = mat.IsHighValueItem,
                                CurrentInHand = matloc.CurrentInHand
                            });
            }
            else
            {
                allItems = (from mat in _materialRepo.GetAll()
                            join un in _unitRepo.GetAll()
                            on mat.DefaultUnitId equals un.Id
                            join issunit in _unitRepo.GetAll()
                            on mat.IssueUnitId equals issunit.Id
                            join matloc in _materiallocationwisestockRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId && t.IsActiveInLocation==true)
                            on mat.Id equals matloc.MaterialRefId
                            select new MaterialInfoDto
                            {
                                MaterialRefId = mat.Id,
                                MaterialRefName = mat.MaterialName,
                                MaterialTypeId = mat.MaterialTypeId,
                                IsFractional = mat.IsFractional,
                                IsBranded = mat.IsFractional,
                                IsQuoteNeededForPurchase = mat.IsQuoteNeededForPurchase,
                                MaterialPetName = mat.MaterialPetName,
                                DefaultUnitId = mat.DefaultUnitId,
                                DefaultUnitName = un.Name,
                                IssueUnitId = mat.IssueUnitId,
                                IssueUnitName = issunit.Name,
                                IsNeedtoKeptinFreezer = mat.IsNeedtoKeptinFreezer,
                                IsMfgDateExists = mat.IsMfgDateExists,
                                WipeOutStockOnClosingDay = mat.WipeOutStockOnClosingDay,
                                CurrentInHand= matloc.CurrentInHand,
                                IsHighValueItem = mat.IsHighValueItem
                            });
            }
            var matRecipeList = await allItems.ToListAsync();

            return matRecipeList;


        }



        public async Task<GetRequestForStandardRecipe> GetRequestForCategory(GetCategoryMaterial input)
        {
            List<RequestDetailViewDto> editDetailDto;

            editDetailDto = await (from mat in _materialRepo.GetAll().Where(t => t.MaterialGroupCategoryRefId == input.MaterialGroupCategoryRefId)
                                   join un in _unitRepo.GetAll() on mat.DefaultUnitId equals un.Id
                                   join uiss in _unitRepo.GetAll() on mat.IssueUnitId equals uiss.Id 
                                   join matloc in _materiallocationwisestockRepo.GetAll()
                                        .Where(mls => mls.LocationRefId == input.LocationRefId && mls.IsActiveInLocation==true) on mat.Id equals matloc.MaterialRefId
                                   select new RequestDetailViewDto
                                   {
                                       MaterialRefId = mat.Id,
                                       RequestQty = 0,
                                       RequestRefId = 0,
                                       Sno = 0,
                                       DefaultUnitId = mat.DefaultUnitId,
                                       DefaultUnitName = un.Name,
                                       UnitRefId = mat.IssueUnitId,
                                       UnitRefName = uiss.Name,
                                       CurrentInHand = matloc.CurrentInHand
                                   }).ToListAsync();

            return new GetRequestForStandardRecipe
            {
                RequestDetail = editDetailDto
            };
        }

        public async Task<IdInput> GetCategoryIdForRecipe(IdInput input)
        {
            var returnid = await _materialRepo.FirstOrDefaultAsync(t => t.Id == input.Id);
            return new IdInput { Id = returnid.MaterialGroupCategoryRefId };
        }
    }

}