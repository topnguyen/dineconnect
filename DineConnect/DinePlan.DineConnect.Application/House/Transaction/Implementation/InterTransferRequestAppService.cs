﻿#region Include
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.House.Master;
using DinePlan.DineConnect.Common;
using Abp.Configuration;
using DinePlan.DineConnect.Connect.Master.Dtos;
using System.Globalization;
using System.Threading;
using System.Text;
using Abp.Net.Mail;
using DinePlan.DineConnect.Connect.DayClose;
using DinePlan.DineConnect.Exporter;
using DinePlan.DineConnect.Connect.Location;
using Abp.Authorization;
using Castle.Core.Logging;
using System.Diagnostics;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Features;
#endregion  

namespace DinePlan.DineConnect.House.Transaction.Implementation
{
    public class InterTransferRequestAppService : DineConnectAppServiceBase, IInterTransferRequestAppService
    {
        private readonly IRepository<InterTransfer> _intertransferRepo;
        private readonly IRepository<InterTransferDetail> _intertransferDetailRepo;
        private readonly IRepository<PurchaseOrder> _purchaseOrderRepo;
        private readonly IRepository<PurchaseOrderDetail> _purchaseOrderDetailRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<MaterialGroupCategory> _materialGroupCategoryRepo;
        private readonly IRepository<MaterialGroup> _materialgroupRepo;
        private readonly IRepository<Unit> _unitRepo;
        private readonly IRepository<MaterialLocationWiseStock> _materiallocationwisestockRepo;
        private readonly IMaterialAppService _materialAppService;
        private readonly ILogger _logger;
        // Start Only for Create Or Update
        //private readonly IDayCloseAppService _dayCloseAppService;
        //private readonly IRepository<Template> _templateRepo;
        //private readonly IRepository<UnitConversion> _unitconversionRepo;
        //private readonly IHouseReportAppService _housereportAppService;
        //private readonly IXmlAndJsonConvertor _xmlandjsonConvertorAppService;
        // Stop
        private readonly IInterTransferListExcelExporter _intertransferExporter;

        public InterTransferRequestAppService(
           IRepository<InterTransfer> interTransferRepo,
           IRepository<InterTransferDetail> intertransferDetailRepo,
           IRepository<Material> materialRepo,
           IRepository<Location> locationRepo,
           IMaterialAppService materialAppService,
           IRepository<Unit> unitRepo,
           IRepository<MaterialLocationWiseStock> materiallocationwisestockRepo,
           IRepository<MaterialGroupCategory> materialGroupCategoryRepo,
           IRepository<MaterialGroup> materialgroupRepo,
           IRepository<PurchaseOrder> purchaseOrderRepo,
           IRepository<PurchaseOrderDetail> purchaseOrderDetailRepo,
           ILogger logger,
           IInterTransferListExcelExporter intertransferExporter
            //
            //IDayCloseAppService dayCloseAppService,
            //IRepository<Template> templateRepo,
            //IRepository<UnitConversion> unitconversionRepo,
            //IHouseReportAppService housereportAppService,
            //IXmlAndJsonConvertor xmlandjsonConvertorAppService
            //
            )
        {
            _intertransferRepo = interTransferRepo;
            _intertransferDetailRepo = intertransferDetailRepo;
            _locationRepo = locationRepo;
            _materialRepo = materialRepo;
            _materialAppService = materialAppService;
            _unitRepo = unitRepo;
            _materiallocationwisestockRepo = materiallocationwisestockRepo;
            _materialGroupCategoryRepo = materialGroupCategoryRepo;
            _materialgroupRepo = materialgroupRepo;
            _purchaseOrderRepo = purchaseOrderRepo;
            _purchaseOrderDetailRepo = purchaseOrderDetailRepo;
            _logger = logger;
            //
            //_dayCloseAppService = dayCloseAppService;
            //_templateRepo = templateRepo;
            //_unitconversionRepo = unitconversionRepo;
            //_housereportAppService = housereportAppService;
            //_xmlandjsonConvertorAppService = xmlandjsonConvertorAppService;
            //
            _intertransferExporter = intertransferExporter;
        }

        public async Task<PagedResultOutput<InterTransferViewDto>> GetViewInterTransfer(GetInterTransferInput input)
        {
            Debug.WriteLine("InterTransferRequestAppService GetViewInterTransfer Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            var rsInterTrans = _intertransferRepo.GetAll().WhereIf(input.DefaultLocationRefId > 0, t => t.LocationRefId == input.DefaultLocationRefId);

            if (input.StartDate != null && input.EndDate != null)
            {
                rsInterTrans = rsInterTrans.Where(a =>
                                 DbFunctions.TruncateTime(a.RequestDate) >= DbFunctions.TruncateTime(input.StartDate)
                                 &&
                                 DbFunctions.TruncateTime(a.RequestDate) <= DbFunctions.TruncateTime(input.EndDate));
            }

            if (input.DeliveryStartDate != null && input.DeliveryEndDate != null)
            {
                rsInterTrans = rsInterTrans.Where(a =>
                                 DbFunctions.TruncateTime(a.DeliveryDateRequested) >= DbFunctions.TruncateTime(input.DeliveryStartDate)
                                 &&
                                 DbFunctions.TruncateTime(a.DeliveryDateRequested) <= DbFunctions.TruncateTime(input.DeliveryEndDate));
            }

            if (input.ShowOnlyDeliveryDateExpectedAsOnDate)
            {
                rsInterTrans = rsInterTrans.Where(a => a.CompletedTime.HasValue == false &&
                                DbFunctions.TruncateTime(a.DeliveryDateRequested) <= DbFunctions.TruncateTime(DateTime.Today));
            }

            if (input.TransferRequestIds.Count > 0)
            {
                rsInterTrans = rsInterTrans.Where(t => input.TransferRequestIds.Contains(t.Id));
            }

            if (input.Id.HasValue)
                rsInterTrans = rsInterTrans.Where(t => t.Id == input.Id.Value);

            IQueryable<InterTransferViewDto> allItems;
            if (input.MaterialGroupRefId > 0 || input.MaterialGroupCategoryRefId > 0)
            {
                allItems = (from req in rsInterTrans
                            join loc in _locationRepo.GetAll()
                            on req.LocationRefId equals loc.Id
                            join locReq in _locationRepo.GetAll()
                                .WhereIf(!input.Filter.IsNullOrEmpty(), p => p.Name.Contains(input.Filter))
                            on req.RequestLocationRefId equals locReq.Id
                            join reqDetail in _intertransferDetailRepo.GetAll() on req.Id equals reqDetail.InterTransferRefId
                            join mat in _materialRepo.GetAll().WhereIf(input.MaterialGroupCategoryRefId > 0, t => t.MaterialGroupCategoryRefId == input.MaterialGroupCategoryRefId) on reqDetail.MaterialRefId equals mat.Id
                            join matGroupCategory in _materialGroupCategoryRepo.GetAll()
                                .WhereIf(input.MaterialGroupRefId > 0, t => t.MaterialGroupRefId == input.MaterialGroupRefId)
                                .WhereIf(input.MaterialGroupCategoryRefId > 0, t => t.Id == input.MaterialGroupCategoryRefId)
                            on mat.MaterialGroupCategoryRefId equals matGroupCategory.Id
                            select new InterTransferViewDto
                            {
                                Id = req.Id,
                                LocationRefId = req.LocationRefId,
                                LocationRefName = loc.Name,
                                RequestLocationRefName = locReq.Name,
                                CurrentStatus = req.CurrentStatus,
                                RequestDate = req.RequestDate,
                                DeliveryDateRequested = req.DeliveryDateRequested,
                                CompletedTime = req.CompletedTime,
                                ReceivedTime = req.ReceivedTime,
                                TransferValue = req.TransferValue,
                                CreationTime = req.CreationTime,
                                VehicleNumber = req.VehicleNumber,
                                PersonInCharge = req.PersonInCharge,
                                DoesThisRequestToBeConvertedAsPO = req.DoesPOCreatedForThisRequest,
                                ParentRequestRefId = req.ParentRequestRefId
                            }).Distinct();
            }
            else
            {
                allItems = (from req in rsInterTrans
                            join loc in _locationRepo.GetAll()
                            on req.LocationRefId equals loc.Id
                            join locReq in _locationRepo.GetAll().WhereIf(
                               !input.Filter.IsNullOrEmpty(),
                               p => p.Name.Contains(input.Filter))
                            on req.RequestLocationRefId equals locReq.Id
                            select new InterTransferViewDto
                            {
                                Id = req.Id,
                                LocationRefId = req.LocationRefId,
                                LocationRefName = loc.Name,
                                RequestLocationRefName = locReq.Name,
                                CurrentStatus = req.CurrentStatus,
                                RequestDate = req.RequestDate,
                                DeliveryDateRequested = req.DeliveryDateRequested,
                                CompletedTime = req.CompletedTime,
                                ReceivedTime = req.ReceivedTime,
                                TransferValue = req.TransferValue,
                                CreationTime = req.CreationTime,
                                VehicleNumber = req.VehicleNumber,
                                PersonInCharge = req.PersonInCharge,
                                DoesThisRequestToBeConvertedAsPO = req.DoesPOCreatedForThisRequest,
                                ParentRequestRefId = req.ParentRequestRefId
                            });
            }

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            //List<InterTransferViewDto> lsttobeRemoved = new List<InterTransferViewDto>();
            List<int> arrRequestIds = sortMenuItems.Select(t => t.Id).ToList();
            var rsDetails = await _intertransferDetailRepo.GetAll().Where(t => arrRequestIds.Contains(t.InterTransferRefId)).Select(t => new { t.Id, t.InterTransferRefId, t.PurchaseOrderRefId }).ToListAsync();
            //var rsDetails = tempInterTransferDetails.MapTo<List<InterTransferDetailListDto>>();

            var arrPoIds = rsDetails.Where(t => t.PurchaseOrderRefId.HasValue).Select(t => t.PurchaseOrderRefId.Value).ToList();
            var tempPoMaster = await _purchaseOrderRepo.GetAllListAsync(t => arrPoIds.Contains(t.Id));
            var rsPoMaster = tempPoMaster.MapTo<List<PurchaseOrderListDto>>();

            var interStopWatch = new Stopwatch();
            interStopWatch.Start();
            Parallel.ForEach(sortMenuItems, item =>
            {
                if (item.CurrentStatus.Equals("Q"))
                    item.CurrentStatus = L("RequestApprovalPending");
                else if (item.CurrentStatus.Equals("P"))
                    item.CurrentStatus = L("Pending");
                else if (item.CurrentStatus.Equals("A"))
                    item.CurrentStatus = L("Approved");
                else if (item.CurrentStatus.Equals("R"))
                    item.CurrentStatus = L("Rejected");
                else if (item.CurrentStatus.Equals("D"))
                    item.CurrentStatus = L("Drafted");
                else if (item.CurrentStatus.Equals("V"))
                    item.CurrentStatus = L("Received");
                else if (item.CurrentStatus.Equals("I"))
                    item.CurrentStatus = L("PartiallyReceived");

                if (item.DeliveryDateRequested.Date <= DateTime.Today.Date)
                    item.DeliverDateAlertFlag = true;

                var details = rsDetails.Where(t => t.InterTransferRefId == item.Id).ToList();
                var countDetails = details.Count();
                item.NoOfMaterials = countDetails;
                var poDependentCount = details.Count(t => t.PurchaseOrderRefId.HasValue == true);
                var stockDependentCount = details.Count(t => t.PurchaseOrderRefId.HasValue == false);
                item.NoOfPoDependent = poDependentCount;
                item.NoOfStockDependent = stockDependentCount;

                if (item.NoOfStockDependent > 0 && item.NoOfPoDependent > 0)
                {
                    item.DoesSplitNeeded = true;
                }

                if (item.DoesThisRequestToBeConvertedAsPO)
                {
                    var poIds = details.Where(t => t.PurchaseOrderRefId.HasValue).Select(t => t.PurchaseOrderRefId.Value).ToList();
                    var poMaster = rsPoMaster.Where(t => poIds.Contains(t.Id)).ToList();
                    foreach (var lst in poMaster)
                    {
                        if (lst.PoCurrentStatus.Equals("P"))
                        {
                            lst.PoCurrentStatus = L("Pending");
                        }
                        else if (lst.PoCurrentStatus.Equals("A"))
                        {
                            lst.PoCurrentStatus = L("Approved");
                        }
                        else if (lst.PoCurrentStatus.Equals("X"))
                        {
                            lst.PoCurrentStatus = L("Declined");
                        }
                        else if (lst.PoCurrentStatus.Equals("C"))
                        {
                            lst.PoCurrentStatus = L("Completed");
                        }
                        else if (lst.PoCurrentStatus.Equals("H"))
                        {
                            lst.PoCurrentStatus = L("Partial");
                        }
                    }

                    var poStatusList = (from mas in poMaster
                                        group mas by new { mas.PoCurrentStatus } into g
                                        select new PoStatusWiseList
                                        {
                                            PoCurrentStatus = g.Key.PoCurrentStatus,
                                            PurchaseOrderList = g.ToList()
                                        }).ToList();
                    item.PoStatusWiseLists = poStatusList;
                    foreach (var lst in poStatusList)
                    {
                        item.PoStatusRemarks = item.PoStatusRemarks + lst.PoCurrentStatus + "( " + lst.PurchaseOrderList.Count() + " ) ";
                    }
                }
            });
            interStopWatch.Stop();
            _logger.Info("InterTransfer For Each Total Elapsed Time " + interStopWatch.ElapsedMilliseconds);
            ////  Remove the Non Approved only for ReceivedView
            //foreach (var l in lsttobeRemoved)
            //{
            //    sortMenuItems.Remove(l);
            //}

            var allListDtos = sortMenuItems.MapTo<List<InterTransferViewDto>>();

            if (!input.CurrentStatus.IsNullOrEmpty())
            {
                //Get the culture property of the thread.
                CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                //Create TextInfo object.
                TextInfo textInfo = cultureInfo.TextInfo;

                input.CurrentStatus = input.CurrentStatus.ToLowerInvariant();

                var searchStatus = textInfo.ToTitleCase(input.CurrentStatus);

                allListDtos = allListDtos.Where(t => t.CurrentStatus.Contains(searchStatus)).ToList();
                //   allListDtos = allListDtos.FindAll(t => t.PoCurrentStatus.Contains(input.PoStatus));
            }

            var allItemCount = sortMenuItems.Count();
            stopWatch.Stop();
            _logger.Info("InterTransfer GetViewInterTransfer Total Elapsed Time " + stopWatch.ElapsedMilliseconds);
            _logger.Info("InterTransfer GetViewInterTransfer Stop Time " + DateTime.Now);
            Debug.WriteLine("InterTransferRequestAppService GetViewInterTransfer Stop Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
            return new PagedResultOutput<InterTransferViewDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<SetInterTransferDashBoardDto> GetDashBoardTransfer(IdInput input)
        {
            _logger.Info("Dashboard Start Time " + DateTime.Now);
            Debug.WriteLine("InterTransferRequestAppService Dashboard Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
            var lstpending = await _intertransferRepo.GetAll().
                Where(p => p.LocationRefId == input.Id && (p.CurrentStatus.Equals("Q") || p.CurrentStatus.Equals("D") || p.CurrentStatus.Equals("P"))).ToListAsync();
            int pendingCount = lstpending.Count();

            var lstApproved = await _intertransferRepo.GetAll().
                Where(p => p.LocationRefId == input.Id && p.CompletedTime >= DateTime.Today && p.CurrentStatus.Equals("A")).ToListAsync();
            int approvedCount = lstApproved.Count();

            var lstReceived = await _intertransferRepo.GetAll().
                Where(p => p.LocationRefId == input.Id && p.ReceivedTime > DateTime.Today && (p.CurrentStatus.Equals("I") || p.CurrentStatus.Equals("V"))).ToListAsync();
            int receivedCount = lstReceived.Count();

            var lstDeclined = await _intertransferRepo.GetAll().
                Where(p => p.LocationRefId == input.Id && p.CompletedTime >= DateTime.Today && p.CurrentStatus.Equals("R")).ToListAsync();

            int declinedCount = lstDeclined.Count();
            _logger.Info("Dashboard Stop Time "  + DateTime.Now.ToString("yyyy.MM.dd HH: mm:ss: ffff"));
            Debug.WriteLine("Dashboard Stop Time "  + DateTime.Now.ToString("yyyy.MM.dd HH: mm:ss: ffff"));
            return new SetInterTransferDashBoardDto
            {
                TotalApproved = approvedCount,
                TotalPending = pendingCount,
                TotalReceived = receivedCount,
                TotalRejected = declinedCount
            };
        }

        public async Task<GetInterTransferForEditOutput> GetInterTransferForEdit(NullableIdInput input)
        {
            Debug.WriteLine("InterTransferRequestAppService GetInterTransferForEdit Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
            InterTransferEditDto editDto;
            List<InterTransferDetailViewDto> editDetailDto;
            TemplateSaveDto editTemplateDto = new TemplateSaveDto();

            if (input.Id.HasValue)
            {
                var hDto = await _intertransferRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<InterTransferEditDto>();

                int locationid = editDto.LocationRefId;
                List<int> locationIds = new List<int>();
                locationIds.Add(editDto.LocationRefId);
                locationIds.Add(editDto.RequestLocationRefId);

                var rsLocation = await _locationRepo.GetAllListAsync(t => locationIds.Contains(t.Id));
                var loc = rsLocation.FirstOrDefault(t => t.Id == editDto.LocationRefId);
                editDto.LocationRefName = loc.Code + " - " + loc.Name;

                var reqLoc = rsLocation.FirstOrDefault(t => t.Id == editDto.RequestLocationRefId);
                editDto.RequestLocationRefName = reqLoc.Code + " - " + reqLoc.Name;

                editDetailDto = await (from request in _intertransferDetailRepo
                                       .GetAll().Where(a => a.InterTransferRefId == input.Id.Value)
                                       join mat in _materialRepo.GetAll()
                                       on request.MaterialRefId equals mat.Id
                                       join un in _unitRepo.GetAll() on mat.DefaultUnitId equals un.Id
                                       join uiss in _unitRepo.GetAll() on request.UnitRefId equals uiss.Id
                                       join matloc in _materiallocationwisestockRepo
                                        .GetAll().Where(mls => mls.LocationRefId == locationid)
                                       on mat.Id equals matloc.MaterialRefId
                                       join matCat in _materialGroupCategoryRepo.GetAll()
                                       on mat.MaterialGroupCategoryRefId equals matCat.Id
                                       join matGrp in _materialgroupRepo.GetAll()
                                       on matCat.MaterialGroupRefId equals matGrp.Id
                                       select new InterTransferDetailViewDto
                                       {
                                           Id = request.Id,
                                           RequestQty = request.RequestQty,
                                           Barcode = mat.Barcode == null ? mat.MaterialName : mat.Barcode,
                                           MaterialGroupRefName = matGrp.MaterialGroupName,
                                           MaterialGroupCategoryRefName = matCat.MaterialGroupCategoryName,
                                           MaterialRefId = request.MaterialRefId,
                                           MaterialPetName = mat.MaterialPetName,
                                           MaterialRefName = mat.MaterialName,
                                           CurrentStatus = request.CurrentStatus,
                                           InterTransferRefId = request.InterTransferRefId,
                                           IssueQty = request.IssueQty,
                                           Price = request.Price,
                                           ApprovedRemarks = request.ApprovedRemarks,
                                           RequestRemarks = request.RequestRemarks,
                                           Sno = request.Sno,
                                           MaterialTypeId = mat.MaterialTypeId,
                                           Uom = un.Name,
                                           OnHand = matloc.CurrentInHand,
                                           UnitRefId = request.UnitRefId,
                                           UnitRefName = uiss.Name,
                                           DefaultUnitId = mat.DefaultUnitId,
                                           DefaultUnitName = un.Name,
                                           OnHandWithUom = matloc.CurrentInHand + "/" + un.Name,
                                           PurchaseOrderRefId = request.PurchaseOrderRefId
                                       }).ToListAsync();

                var arrPoIds = editDetailDto.Where(t => t.PurchaseOrderRefId.HasValue).Select(t => t.PurchaseOrderRefId.Value).ToList();
                var tempPoMaster = await _purchaseOrderRepo.GetAllListAsync(t => arrPoIds.Contains(t.Id));
                var rsPoMaster = tempPoMaster.MapTo<List<PurchaseOrderListDto>>();
                var tempPoDetail = await _purchaseOrderDetailRepo.GetAllListAsync(t => arrPoIds.Contains(t.PoRefId));
                var rsPoDetails = tempPoDetail.MapTo<List<PurchaseOrderDetailViewDto>>();
                List<int> arrMaterialRefIds = editDetailDto.Select(t => t.MaterialRefId).ToList();
                var rsUnits = await _unitRepo.GetAllListAsync();
                List<MaterialViewDto> reservedQuantityMaterials = new List<MaterialViewDto>();
                var resultReserved = await _materialAppService.GetReservedQuantityFromIssueAndYield(
                                  new LocationWithMaterialList
                                  {
                                      LocationRefId = locationid,
                                      CalculateIssueReservedQuantity = true,
                                      CalculateYieldReservedQuantity = true,
                                      MaterialRefIds = arrMaterialRefIds
                                  });
                reservedQuantityMaterials = resultReserved.Items.MapTo<List<MaterialViewDto>>();

                foreach (var lst in editDetailDto)
                {
                    var reserved = reservedQuantityMaterials.FirstOrDefault(t => t.Id == lst.Id);
                    if (reserved != null)
                    {
                        lst.ReservedIssueQuantity = reserved.ReservedIssueQuantity;
                        lst.ReservedYieldQuantity = reserved.ReservedYieldQuantity;
                    }

                    if (lst.CurrentStatus.Equals("Q"))
                        lst.CurrentStatus = L("RequestApprovalPending");
                    else if (lst.CurrentStatus.Equals("P"))
                        lst.CurrentStatus = L("Pending");
                    else if (lst.CurrentStatus.Equals("A"))
                        lst.CurrentStatus = L("Approved");
                    else if (lst.CurrentStatus.Equals("R"))
                        lst.CurrentStatus = L("Rejected");
                    else if (lst.CurrentStatus.Equals("D"))
                        lst.CurrentStatus = L("Drafted");
                    else if (lst.CurrentStatus.Equals("V"))
                        lst.CurrentStatus = L("Received");
                    else if (lst.CurrentStatus.Equals("I"))
                        lst.CurrentStatus = L("PartiallyReceived");
                    else if (lst.CurrentStatus.Equals("E"))
                        lst.CurrentStatus = L("Excess");
                    else if (lst.CurrentStatus.Equals("S"))
                        lst.CurrentStatus = L("Shortage");

                    string materialTypeName = lst.MaterialTypeId == (int)MaterialType.RAW ? L("RAW") : L("SEMI");
                    lst.MaterialTypeName = materialTypeName;

                    var poMaster = rsPoMaster.FirstOrDefault(t => t.Id == lst.PurchaseOrderRefId);
                    var poDetail = rsPoDetails.FirstOrDefault(t => t.MaterialRefId == lst.MaterialRefId && t.PoRefId == lst.PurchaseOrderRefId);
                    if (poDetail != null)
                    {
                        var unit = rsUnits.FirstOrDefault(t => t.Id == poDetail.UnitRefId);
                        lst.PoCurrentStatus = L("PoDetailStatus", poDetail.QtyOrdered, poDetail.QtyReceived, poMaster.Id, poMaster.PoReferenceCode, unit.Name);
                    }
                }
            }
            else
            {
                editDto = new InterTransferEditDto();
                editDetailDto = new List<InterTransferDetailViewDto>();

            }
            Debug.WriteLine("InterTransferRequestAppService GetInterTransferForEdit Stop Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));

            return new GetInterTransferForEditOutput
            {
                InterTransfer = editDto,
                InterTransferDetail = editDetailDto,
                Templatesave = editTemplateDto
            };
        }

        public async Task<RequestTimeLockDtos> GetRequestTimeStatus(int argLocationRefId)
        {

            bool allowFlag = true;

            var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == argLocationRefId);
            if (location == null)
            {
                return null;
            }
            var transferLockHours = 0M;
            string timeElapsedAlert = null;
            try
            {
                transferLockHours = location.TransferRequestLockHours;
            }
            catch (Exception)
            {
                transferLockHours = 0;
            }
            if (transferLockHours == 0)
            {
                transferLockHours = await SettingManager.GetSettingValueAsync<decimal>(AppSettings.HouseSettings.TransferRequestLockHours);
            }
            if (transferLockHours > 0)
            {
                DateTime maxTimeAllowed = DateTime.Today.Date.AddHours((double)transferLockHours);
                if (DateTime.Now >= maxTimeAllowed)
                {
                    if (PermissionChecker.IsGranted("Pages.Tenant.House.Transaction.InterTransfer.GracePeriodAllowed"))
                    {
                        var transferGraceHours = location.TransferRequestGraceHours;
                        if (transferGraceHours == 0)
                            transferGraceHours = await SettingManager.GetSettingValueAsync<decimal>(AppSettings.HouseSettings.TransferRequestGraceHours);

                        maxTimeAllowed = DateTime.Today.Date.AddHours((double)transferGraceHours);
                        if (DateTime.Now >= maxTimeAllowed)
                        {
                            timeElapsedAlert = L("InterTransferRequestGraceTimeDenied");
                            allowFlag = false;
                        }
                        else
                        {
                            timeElapsedAlert = L("InterTransferRequestGraceTimeMessage", maxTimeAllowed.ToString("dd-MMM-yyyy HH:mm:ss"));
                            allowFlag = true;

                        }
                    }
                    else
                    {
                        timeElapsedAlert = L("InterTransferRequestDenied", maxTimeAllowed.ToString("dd-MMM-yyyy HH:mm:ss"));
                        allowFlag = false;
                    }
                }
                else
                {
                    timeElapsedAlert = L("InterTransferRequestTimeMessagge", maxTimeAllowed.ToString("dd-MMM-yyyy HH:mm:ss"));
                    allowFlag = true;
                }
            }
            //return timeElapsedAlert;
            return new RequestTimeLockDtos
            {
                AlertMsg = timeElapsedAlert,
                AllowFlag = allowFlag
            };
        }

        public async Task DeleteInterTransfer(IdInput input)
        {
            var rec = await _intertransferRepo.FirstOrDefaultAsync(t => t.Id == input.Id);
            if (rec == null)
            {
                throw new UserFriendlyException(L("RecordsNotFound"));
            }

            if (rec.CurrentStatus != "P")
            {
                throw new UserFriendlyException(L("DeleteNotAllowedApproved"));
            }

            var det = await _intertransferDetailRepo.GetAllListAsync(t => t.InterTransferRefId == input.Id);
            foreach (var lst in det)
            {
                if (lst.CurrentStatus != "P")
                {
                    throw new UserFriendlyException(L("DeleteNotAllowedApproved"));
                }
                await _intertransferDetailRepo.DeleteAsync(lst.Id);
            }

            await _intertransferRepo.DeleteAsync(input.Id);
        }


        public async Task<FileDto> GetAllToExcel(GetInterTransferInput input)
        {
            input.MaxResultCount = AppConsts.MaxPageSize;

            var allList = await GetViewInterTransfer(input);
            var allListDtos = allList.Items.MapTo<List<InterTransferViewDto>>();
            return _intertransferExporter.ExportToFile(allListDtos);
        }


        //        public async Task<IdInput> CreateOrUpdateInterTransfer(CreateOrUpdateInterTransferInput input)
        //        {
        //            Debug.WriteLine("InterTransfer CreateOrUpdateInterTransfer() Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));

        //            if (input.InterTransfer.DeliveryDateRequested == DateTime.MinValue)
        //                input.InterTransfer.DeliveryDateRequested = input.InterTransfer.RequestDate;
        //            if (input.InterTransfer.DeliveryDateRequested.Date <= DateTime.Today.AddDays(1).Date)
        //            {
        //                var elapsedTimeAlert = await GetRequestTimeStatus(input.InterTransfer.LocationRefId);
        //                if (elapsedTimeAlert.AllowFlag == false)
        //                {
        //                    throw new UserFriendlyException(elapsedTimeAlert.AlertMsg);
        //                }
        //            }
        //            if (input.InterTransfer.CreatorUserId.HasValue && input.InterTransfer.CreatorUserId > 0 && input.InterTransfer.CreatorUserId != AbpSession.UserId)
        //            {
        //                var existUser = await UserManager.GetUserByIdAsync((long)input.InterTransfer.CreatorUserId);
        //                var currentUser = await GetCurrentUserAsync();
        //                throw new UserFriendlyException(L("UserIdChangedErrorAlert", existUser.UserName, currentUser.UserName));
        //            }

        //            if (input.InterTransfer.Id.HasValue)
        //            {
        //                return await UpdateInterTransfer(input);
        //            }
        //            else
        //            {
        //                #region Check Material Active Status in Request Destination Location
        //                List<int> arrMaterialRefIds = input.InterTransferDetail.Select(t => t.MaterialRefId).ToList();
        //                var destinationLocationMaterialStatus = await _materiallocationwisestockRepo.GetAll().Where(t => t.LocationRefId == input.InterTransfer.RequestLocationRefId && arrMaterialRefIds.Contains(t.MaterialRefId) && t.IsActiveInLocation == false).ToListAsync();
        //                if (destinationLocationMaterialStatus.Count > 0)
        //                {
        //                    var reqLocation = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.InterTransfer.RequestLocationRefId)
        //;
        //                    arrMaterialRefIds = destinationLocationMaterialStatus.Select(t => t.MaterialRefId).ToList();
        //                    var inactiveMaterilas = await _materialRepo.GetAll().Where(t => arrMaterialRefIds.Contains(t.Id)).Select(t => t.MaterialName).ToListAsync();
        //                    string errorMessage = string.Join(",", inactiveMaterilas);
        //                    errorMessage = L("Material") + " " + L("Dorment") + " " + L("Status") + " " + L("In") + " " + L("Location") + " : " + reqLocation.Name + System.Environment.NewLine + errorMessage;
        //                    throw new UserFriendlyException(errorMessage);
        //                }
        //                #endregion

        //                var ip = input;
        //                if (ip.InterTransfer.DeliveryDateRequested == DateTime.MinValue)
        //                    ip.InterTransfer.DeliveryDateRequested = ip.InterTransfer.RequestDate;

        //                await CreateTemplate(ip);
        //                DateTime requestTime = DateTime.Now;

        //                var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.InterTransfer.LocationRefId);
        //                var reqTransactionDate = loc.HouseTransactionDate;

        //                if (reqTransactionDate == null)
        //                {
        //                    var dayClose = _dayCloseAppService.GetDayClose(new IdInput { Id = input.InterTransfer.LocationRefId });
        //                    requestTime = dayClose.Result.TransactionDate;
        //                }
        //                else if (requestTime != null)
        //                {
        //                    if (requestTime.ToString("yyyy-MMM-dd") != reqTransactionDate.Value.ToString("yyyy-MMM-dd"))
        //                    {
        //                        requestTime = reqTransactionDate.Value;
        //                    }
        //                }

        //                input.InterTransfer.RequestDate = requestTime;

        //                IdInput interid = await CreateInterTransfer(input);
        //                string OperationToBeDone = input.SaveStatus;

        //                if (input.SaveStatus != null && (OperationToBeDone.Equals("DA") || OperationToBeDone.Equals("DR")))
        //                {
        //                    input.SaveStatus = "SAVE";
        //                    input.InterTransfer.Id = interid.Id;

        //                    DateTime completedTime = DateTime.Now;

        //                    var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.InterTransfer.RequestLocationRefId);
        //                    var transactionDate = location.HouseTransactionDate;

        //                    if (transactionDate == null)
        //                    {
        //                        var dayClose = _dayCloseAppService.GetDayClose(new IdInput { Id = input.InterTransfer.RequestLocationRefId });
        //                        completedTime = dayClose.Result.TransactionDate;
        //                    }
        //                    else if (transactionDate != null)
        //                    {
        //                        if (completedTime.ToString("yyyy-MMM-dd") != transactionDate.Value.ToString("yyyy-MMM-dd"))
        //                        {
        //                            completedTime = transactionDate.Value;
        //                        }
        //                    }

        //                    input.InterTransfer.CompletedTime = completedTime;

        //                    List<Location> l = await _locationRepo.GetAll().Where(t => t.Id == input.InterTransfer.RequestLocationRefId).ToListAsync();
        //                    List<LocationListDto> locinput = l.MapTo<List<LocationListDto>>();
        //                    List<int> materialRefIdsRateRequired = input.InterTransferDetail.Select(t => t.MaterialRefId).ToList();

        //                    var matView = await _housereportAppService.GetMaterialRateView(new GetHouseReportMaterialRateInput
        //                    {
        //                        StartDate = completedTime,
        //                        EndDate = completedTime,
        //                        Locations = locinput,
        //                        MaterialRefIds = materialRefIdsRateRequired,
        //                        FunctionCalledBy = "Inter Transfer Create"
        //                    });

        //                    var materialRate = matView.MaterialRateView;

        //                    //var rsUnitConversion = await _unitconversionRepo.GetAllListAsync();
        //                    var rsUnitConversion = await _unitconversionRepo.GetAll().Select(t => new { t.BaseUnitId, t.RefUnitId, t.Conversion, t.MaterialRefId }).ToListAsync();
        //                    foreach (var lst in input.InterTransferDetail)
        //                    {
        //                        var matprice = materialRate.FirstOrDefault(t => t.MaterialRefId == lst.MaterialRefId);

        //                        lst.CurrentStatus = L("Approved");
        //                        if (lst.DefaultUnitId == 0)
        //                        {
        //                            var mat = await _materialRepo.FirstOrDefaultAsync(t => t.Id == lst.MaterialRefId);
        //                            lst.DefaultUnitId = mat.DefaultUnitId;
        //                        }

        //                        decimal conversionFactor = 1;
        //                        if (lst.DefaultUnitId == lst.UnitRefId)
        //                            conversionFactor = 1;
        //                        else
        //                        {
        //                            var conv = rsUnitConversion.First(t => t.BaseUnitId == lst.DefaultUnitId && t.RefUnitId == lst.UnitRefId && t.MaterialRefId == lst.MaterialRefId);
        //                            if (conv == null)
        //                            {
        //                                conv = rsUnitConversion.First(t => t.BaseUnitId == lst.DefaultUnitId && t.RefUnitId == lst.UnitRefId);
        //                            }
        //                            if (conv != null)
        //                            {
        //                                conversionFactor = conv.Conversion;
        //                            }
        //                            else
        //                            {
        //                                var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == lst.DefaultUnitId);
        //                                if (baseUnit == null)
        //                                {
        //                                    throw new UserFriendlyException(L("UnitIdDoesNotExist", lst.DefaultUnitId));
        //                                }
        //                                var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == lst.UnitRefId);
        //                                if (refUnit == null)
        //                                {
        //                                    throw new UserFriendlyException(L("UnitIdDoesNotExist", lst.UnitRefId));
        //                                }
        //                                throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
        //                            }

        //                            lst.OnHand = lst.OnHand * conversionFactor;
        //                        }

        //                        var templst = lst;

        //                        if (lst.Price > 0 && OperationToBeDone.Equals("DR"))
        //                        {
        //                            //  Do Nothing , Same Rate Based Received Price
        //                        }
        //                        else
        //                        {
        //                            if (lst.DefaultUnitId == lst.UnitRefId)
        //                                lst.Price = matprice.AvgRate;
        //                            else
        //                            {
        //                                lst.Price = Math.Round(matprice.AvgRate * 1 / conversionFactor, 3);
        //                            }
        //                        }

        //                        lst.LineTotal = Math.Round(lst.IssueQty * lst.Price, 3);

        //                    }

        //                    IdInput approvedId = await CreateOrUpdateApproval(input);

        //                    if (OperationToBeDone.Equals("DR"))
        //                    {
        //                        CreateOrUpdateInterReceivedInput inputforReceived = new CreateOrUpdateInterReceivedInput();
        //                        inputforReceived.InterTransfer = input.InterTransfer;
        //                        inputforReceived.SaveStatus = "V";
        //                        inputforReceived.InterTransfer.ReceivedTime = input.InterTransfer.RequestDate;

        //                        List<InterTransferReceivedEditDto> reclist = new List<InterTransferReceivedEditDto>();

        //                        int sno = 1;
        //                        foreach (var approvedmat in input.InterTransferDetail)
        //                        {
        //                            InterTransferReceivedEditDto newApprovalitem = new InterTransferReceivedEditDto
        //                            {
        //                                InterTransferRefId = approvedId.Id,
        //                                Sno = sno,
        //                                MaterialRefId = approvedmat.MaterialRefId,
        //                                MaterialRefName = approvedmat.MaterialRefName,
        //                                RequestQty = approvedmat.RequestQty,
        //                                IssueQty = approvedmat.IssueQty,
        //                                ReceivedQty = approvedmat.IssueQty,
        //                                Price = approvedmat.Price,
        //                                ReceivedValue = Math.Round(approvedmat.IssueQty * approvedmat.Price, 3),
        //                                CurrentStatus = L("Received"),
        //                                Uom = approvedmat.Uom,
        //                                UnitRefId = approvedmat.UnitRefId,
        //                            };

        //                            reclist.Add(newApprovalitem);
        //                            sno++;
        //                        }

        //                        inputforReceived.InterTransferReceivedDetail = reclist;

        //                        InterTransferReceivedWithAdjustmentId recid = await CreateOrUpdateInterReceived(inputforReceived);
        //                    }
        //                }

        //                return interid;// await CreateInterTransfer(input);
        //            }

        //        }

        //protected virtual async Task<IdInput> CreateInterTransfer(CreateOrUpdateInterTransferInput input)
        //{
        //    var dto = input.InterTransfer.MapTo<InterTransfer>();
        //    if (dto.CurrentStatus != "Q")
        //        dto.CurrentStatus = "P";

        //    var retId = await _intertransferRepo.InsertOrUpdateAndGetIdAsync(dto);

        //    foreach (InterTransferDetailViewDto items in input.InterTransferDetail.ToList())
        //    {
        //        InterTransferDetail itdDto = new InterTransferDetail();

        //        itdDto.InterTransferRefId = retId;
        //        itdDto.Sno = items.Sno;
        //        itdDto.MaterialRefId = items.MaterialRefId;
        //        itdDto.RequestQty = items.RequestQty;
        //        itdDto.IssueQty = items.IssueQty;
        //        itdDto.UnitRefId = items.UnitRefId;
        //        itdDto.Price = items.Price;
        //        itdDto.CurrentStatus = dto.CurrentStatus;
        //        itdDto.RequestRemarks = items.RequestRemarks;
        //        itdDto.ApprovedRemarks = items.ApprovedRemarks;

        //        var retDetailId = await _intertransferDetailRepo.InsertAndGetIdAsync(itdDto);
        //    }

        //    string templateJson = _xmlandjsonConvertorAppService.SerializeToJSON(input);

        //    if (input.SaveStatus != null)
        //    {
        //        if (input.SaveStatus.Equals("DA") || input.SaveStatus.Equals("DR"))
        //            input.Templatesave = null;
        //    }
        //    if (input.Templatesave != null)
        //    {
        //        Template lastTemplate;
        //        int? templateScope;
        //        if (input.Templatesave.Templatescope == true)
        //        {
        //            templateScope = null;
        //        }
        //        else
        //        {
        //            templateScope = input.InterTransfer.LocationRefId;
        //        }

        //        if (input.Templatesave.Templateflag == true)
        //        {
        //            string repName = input.Templatesave.Templatename;

        //            var templateExist = _templateRepo.GetAll().Where(t => t.Name == repName && t.TemplateType == (int)TemplateType.RequestInterLocation).ToList();
        //            if (templateExist.Count == 0)
        //            {
        //                lastTemplate = new Template
        //                {
        //                    Name = input.Templatesave.Templatename,
        //                    TemplateType = (int)TemplateType.RequestInterLocation,
        //                    TemplateData = templateJson,
        //                    LocationRefId = templateScope
        //                };
        //            }
        //            else
        //            {
        //                lastTemplate = templateExist[0];
        //                lastTemplate.LocationRefId = templateScope;
        //                lastTemplate.TemplateData = templateJson;
        //            }
        //        }
        //        else
        //        {
        //            string repName = L("LastRequest");
        //            var templateExist = _templateRepo.GetAll().Where(t => t.Name == repName && t.TemplateType == (int)TemplateType.RequestInterLocation).ToList();
        //            if (templateExist.Count() == 0)
        //            {
        //                lastTemplate = new Template
        //                {
        //                    Name = L("LastRequest"),
        //                    TemplateType = (int)TemplateType.RequestInterLocation,
        //                    TemplateData = templateJson,
        //                    LocationRefId = input.InterTransfer.LocationRefId
        //                };
        //            }
        //            else
        //            {
        //                lastTemplate = templateExist[0];
        //                lastTemplate.LocationRefId = templateScope;
        //                lastTemplate.TemplateData = templateJson;
        //            }
        //        }

        //        await _templateRepo.InsertOrUpdateAndGetIdAsync(lastTemplate);
        //    }


        //    return new IdInput
        //    {
        //        Id = retId
        //    };

        //}


        //protected virtual async Task<IdInput> UpdateInterTransfer(CreateOrUpdateInterTransferInput input)
        //{
        //    var item = await _intertransferRepo.GetAsync(input.InterTransfer.Id.Value);
        //    var dto = input.InterTransfer;

        //    item.LocationRefId = dto.LocationRefId;
        //    item.RequestLocationRefId = dto.RequestLocationRefId;
        //    item.CurrentStatus = dto.CurrentStatus;
        //    item.RequestDate = dto.RequestDate;
        //    item.DeliveryDateRequested = dto.DeliveryDateRequested;
        //    item.DocReferenceNumber = dto.DocReferenceNumber;

        //    //  Update Detail Link
        //    List<int> materialsToBeRetained = new List<int>();

        //    if (input.InterTransferDetail != null && input.InterTransferDetail.Count > 0)
        //    {
        //        foreach (var items in input.InterTransferDetail)
        //        {
        //            int materialrefid = items.MaterialRefId;
        //            materialsToBeRetained.Add(materialrefid);

        //            var existingDetailEntry = _intertransferDetailRepo.GetAllList(u => u.InterTransferRefId == dto.Id && u.MaterialRefId.Equals(materialrefid));

        //            if (existingDetailEntry.Count == 0)  //  Add New Material
        //            {
        //                InterTransferDetail itdDto = new InterTransferDetail();

        //                itdDto.InterTransferRefId = (int)dto.Id;
        //                itdDto.Sno = items.Sno;
        //                itdDto.MaterialRefId = items.MaterialRefId;
        //                itdDto.RequestQty = items.RequestQty;
        //                itdDto.IssueQty = items.IssueQty;
        //                itdDto.UnitRefId = items.UnitRefId;
        //                itdDto.Price = items.Price;
        //                itdDto.IssueValue = Math.Round(items.IssueQty * items.Price, 3);
        //                itdDto.CurrentStatus = "P";
        //                itdDto.RequestRemarks = items.RequestRemarks;
        //                itdDto.ApprovedRemarks = items.ApprovedRemarks;

        //                var retDetailId = await _intertransferDetailRepo.InsertAndGetIdAsync(itdDto);
        //            }
        //            else
        //            {
        //                var itdDto = await _intertransferDetailRepo.GetAsync(existingDetailEntry[0].Id);
        //                string currentStatus;

        //                itdDto.InterTransferRefId = (int)dto.Id;
        //                itdDto.Sno = items.Sno;
        //                itdDto.MaterialRefId = items.MaterialRefId;
        //                itdDto.RequestQty = items.RequestQty;
        //                itdDto.IssueQty = items.IssueQty;
        //                itdDto.UnitRefId = items.UnitRefId;
        //                itdDto.Price = items.Price;
        //                itdDto.IssueValue = Math.Round(items.IssueQty * items.Price, 3);

        //                currentStatus = "P";
        //                if (items.CurrentStatus == L("Pending"))
        //                    currentStatus = "P";
        //                else if (items.CurrentStatus == L("Approved"))
        //                    currentStatus = "A";
        //                else if (items.CurrentStatus == L("Excess"))
        //                    currentStatus = "E";
        //                else if (items.CurrentStatus == L("Rejected"))
        //                    currentStatus = "R";


        //                itdDto.CurrentStatus = currentStatus;
        //                itdDto.RequestRemarks = items.RequestRemarks;
        //                itdDto.ApprovedRemarks = items.ApprovedRemarks;

        //                var retId2 = await _intertransferDetailRepo.InsertOrUpdateAndGetIdAsync(itdDto);

        //            }
        //        }
        //    }

        //    var delBrandList = _intertransferDetailRepo.GetAll().Where(a => a.InterTransferRefId == input.InterTransfer.Id.Value && !materialsToBeRetained.Contains(a.MaterialRefId)).ToList();

        //    foreach (var a in delBrandList)
        //    {
        //        _intertransferDetailRepo.Delete(a.Id);
        //    }

        //    //await CreateTemplate(input);

        //    return new IdInput
        //    {
        //        Id = input.InterTransfer.Id.Value
        //    };

        //}

        //protected virtual async Task CreateTemplate(CreateOrUpdateInterTransferInput input)
        //{
        //    if (input.Templatesave == null)
        //        return;
        //    string templateJson = _xmlandjsonConvertorAppService.SerializeToJSON(input);
        //    Template lastTemplate;
        //    int? templateScope;
        //    if (input.Templatesave.Templatescope == true)
        //    {
        //        templateScope = null;
        //    }
        //    else
        //    {
        //        if (input.Templatesave.TemplateType == (int)TemplateType.DirectInterTransfer)
        //            templateScope = input.InterTransfer.RequestLocationRefId;
        //        else
        //            templateScope = input.InterTransfer.LocationRefId;
        //    }

        //    if (input.Templatesave.Templateflag == true)
        //    {
        //        string repName = input.Templatesave.Templatename;

        //        var templateExist = _templateRepo.GetAll().Where(t => t.Name == repName
        //            && t.TemplateType == input.Templatesave.TemplateType).ToList();
        //        if (templateExist.Count == 0)
        //        {
        //            lastTemplate = new Template
        //            {
        //                Name = input.Templatesave.Templatename,
        //                TemplateType = input.Templatesave.TemplateType,
        //                TemplateData = templateJson,
        //                LocationRefId = templateScope
        //            };
        //        }
        //        else
        //        {
        //            lastTemplate = templateExist[0];
        //            lastTemplate.LocationRefId = templateScope;
        //            lastTemplate.TemplateData = templateJson;
        //        }
        //    }
        //    else
        //    {
        //        string repName = L("LastRequestDraft");
        //        var templateExist = _templateRepo.GetAll().Where(t => t.Name == repName && t.TemplateType == (int)TemplateType.RequestInterLocation).ToList();
        //        if (templateExist.Count() == 0)
        //        {
        //            lastTemplate = new Template
        //            {
        //                Name = L("LastRequestDraft"),
        //                TemplateType = (int)TemplateType.RequestInterLocation,
        //                TemplateData = templateJson,
        //                LocationRefId = input.InterTransfer.LocationRefId
        //            };
        //        }
        //        else
        //        {
        //            lastTemplate = templateExist[0];
        //            lastTemplate.LocationRefId = templateScope;
        //            lastTemplate.TemplateData = templateJson;
        //        }
        //    }
        //    await _templateRepo.InsertOrUpdateAndGetIdAsync(lastTemplate);
        //}

    }
}
