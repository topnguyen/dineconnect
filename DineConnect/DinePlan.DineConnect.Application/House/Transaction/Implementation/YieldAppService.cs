﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.House.Master.Dtos;
using System;
using DinePlan.DineConnect.House.Master;
using DinePlan.DineConnect.Common;
using DinePlan.DineConnect.Connect.Master.Dtos;

namespace DinePlan.DineConnect.House.Transaction.Implementation
{
    public class YieldAppService : DineConnectAppServiceBase, IYieldAppService
    {

        private readonly IYieldListExcelExporter _yieldExporter;
        private readonly IYieldManager _yieldManager;
        private readonly IRepository<Yield> _yieldRepo;
        private readonly IRepository<YieldInput> _inputRepo;
        private readonly IRepository<YieldOutput> _outputRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<Template> _templateRepo;
        private readonly IRepository<Unit> _unitRepo;
        private readonly IMaterialAppService _materialAppService;
		private readonly IHouseReportAppService _houseReportAppService;
        private readonly IXmlAndJsonConvertor _xmlandjsonConvertorAppService;
        private readonly IRepository<MaterialLocationWiseStock> _materiallocationwisestockRepo;
        private readonly IRepository<ProductionUnit> _productionunitRepo;

        public YieldAppService(IYieldManager yieldManager,
            IRepository<Yield> yieldRepo,
            IRepository<YieldInput> yieldInputRepo,
            IRepository<YieldOutput> yieldOutputRepo,
            IYieldListExcelExporter yieldExporter,
            IRepository<Material> materialRepo,
            IRepository<Location> locationRepo,
            IRepository<Template> templateRepo,
            IMaterialAppService materialAppService,
            IRepository<Unit> unitRepo,
            IRepository<MaterialLocationWiseStock> materiallocationwisestockRepo,
            IXmlAndJsonConvertor xmlandjsonConvertorAppService,
            IRepository<ProductionUnit> productionunitRepo,
			IHouseReportAppService houseReportAppService)
        {
            _yieldManager = yieldManager;
            _yieldRepo = yieldRepo;
            _inputRepo = yieldInputRepo;
            _outputRepo = yieldOutputRepo;
            _yieldExporter = yieldExporter;
            _materialRepo = materialRepo;
            _locationRepo = locationRepo;
            _templateRepo = templateRepo;
            _unitRepo = unitRepo;
            _materialAppService = materialAppService;
            _xmlandjsonConvertorAppService = xmlandjsonConvertorAppService;
            _materiallocationwisestockRepo = materiallocationwisestockRepo;
            _productionunitRepo = productionunitRepo;
			_houseReportAppService = houseReportAppService;
		}

        public async Task<PagedResultOutput<YieldListDto>> GetAll(GetYieldInput input)
        {

            var rsYield = _yieldRepo.GetAll().Where(t => t.LocationRefId == input.DefaultLocationRefId).WhereIf(!input.Filter.IsNullOrEmpty(), p => p.RequestSlipNumber.Contains(input.Filter));

            if (input.StartDate != null && input.EndDate != null)
            {
                rsYield = rsYield.Where(a =>
                                 DbFunctions.TruncateTime(a.IssueTime) >= DbFunctions.TruncateTime(input.StartDate)
                                 &&
                                 DbFunctions.TruncateTime(a.IssueTime) <= DbFunctions.TruncateTime(input.EndDate));
                // rsRequest.Where(t => t.RequestTime >= input.StartDate && t.RequestTime <= input.EndDate);
            }

            var allItems = from ret in rsYield
                           join productionunit in _productionunitRepo.GetAll()
                           on ret.ProductionUnitRefId equals productionunit.Id into gj
                           from g in gj.DefaultIfEmpty()
                           select new YieldListDto
                           {
                               Id = ret.Id,
                               LocationRefId = ret.LocationRefId,
                               ProductionUnitRefId = ret.ProductionUnitRefId,
                               ProductionUnitRefName = g.Name,
                               CreationTime = ret.CreationTime,
                               Status = ret.Status,
                               IssueTime = ret.IssueTime,
                               CompletedTime =ret.CompletedTime,
                               RecipeRefId = ret.RecipeRefId,
                               RecipeProductionQty = ret.RecipeProductionQty,
                               RequestSlipNumber = ret.RequestSlipNumber,
                               TokenNumber = ret.TokenNumber,
                           };

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<YieldListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<YieldListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetYieldInput input)
        {
            input.MaxResultCount = AppConsts.MaxPageSize;

            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<YieldListDto>>();
            return _yieldExporter.ExportToFile(allListDtos);
        }

        public async Task<GetYieldForEditOutput> GetYieldForEdit(NullableIdInput input)
        {
            YieldEditDto editDto;
            List<YieldInputViewDto> editInputDtos;
            List<YieldOutputViewDto> editOutputDtos;
            TemplateSaveDto editTemplateDto = new TemplateSaveDto();

            if (input.Id.HasValue)
            {
                var hDto = await _yieldRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<YieldEditDto>();

				if (editDto.RecipeRefId.HasValue)
				{
					var recipe = await _materialRepo.FirstOrDefaultAsync(t => t.Id == editDto.RecipeRefId.Value);
					if (recipe!=null)
					{
						editDto.RecipeRefName = recipe.MaterialName;
					}
				}

                editInputDtos   = await (from yieldinput in _inputRepo.GetAll()
                                       .Where(a => a.YieldRefId == input.Id.Value)
                                          join mat in _materialRepo.GetAll()
                                          on yieldinput.InputMaterialRefId equals mat.Id
                                          join locmat in _materiallocationwisestockRepo.GetAll().Where(l=>l.LocationRefId== editDto.LocationRefId) 
                                          on mat.Id equals locmat.MaterialRefId 
                                          join un in _unitRepo.GetAll()
                                          on mat.DefaultUnitId equals un.Id
                                         join uiss in _unitRepo.GetAll() on yieldinput.UnitRefId equals uiss.Id
                                         select new YieldInputViewDto
                                          {
                                            Sno = yieldinput.Sno,
                                            YieldRefId = (int)yieldinput.YieldRefId,
                                            InputMaterialRefId = yieldinput.InputMaterialRefId,
                                            MaterialRefName = mat.MaterialName,
                                            InputQty = yieldinput.InputQty,
                                            Uom = un.Name,
                                            CurrentInHand = locmat.CurrentInHand + yieldinput.InputQty,
                                            UnitRefId = yieldinput.UnitRefId,
                                            UnitRefName = uiss.Name,
                                            DefaultUnitId = mat.DefaultUnitId,
                                            DefaultUnitName = un.Name
                                          }).ToListAsync();

                editOutputDtos = await (from yieldoutput in _outputRepo.GetAll()
                                  .Where(a => a.YieldRefId == input.Id.Value)
                                       join mat in _materialRepo.GetAll()
                                       on yieldoutput.OutputMaterialRefId equals mat.Id
                                       join locmat in _materiallocationwisestockRepo.GetAll()
                                            .Where(l => l.LocationRefId == editDto.LocationRefId)
                                       on mat.Id equals locmat.MaterialRefId
                                       join un in _unitRepo.GetAll()
                                       on mat.DefaultUnitId equals un.Id
                                        join uiss in _unitRepo.GetAll() 
                                        on yieldoutput.UnitRefId equals uiss.Id
                                        select new YieldOutputViewDto
                                       {
                                           Sno = yieldoutput.Sno,
                                           YieldRefId = (int)yieldoutput.YieldRefId,
                                           OutputMaterialRefId = yieldoutput.OutputMaterialRefId,
                                           MaterialRefName = mat.MaterialName,
                                           OutputQty = yieldoutput.OutputQty,
										   YieldPrice = yieldoutput.YieldPrice,
                                           Uom = un.Name,
                                           CurrentInHand = locmat.CurrentInHand,
                                           UnitRefId = yieldoutput.UnitRefId,
                                           UnitRefName = uiss.Name,
                                           DefaultUnitId = mat.DefaultUnitId,
                                           DefaultUnitName = un.Name
                                       }).ToListAsync();
            }
            else
            {
                editDto = new YieldEditDto();
                editInputDtos= new List<YieldInputViewDto>() ;
                editOutputDtos = new List<YieldOutputViewDto>() ;
            }

            return new GetYieldForEditOutput
            {
                Yield = editDto,
                YieldInputDetail = editInputDtos,
                YieldOutputDetail = editOutputDtos,
                Templatesave = editTemplateDto
            };
        }

        public async Task CreateOrUpdateYield(CreateOrUpdateYieldInput input)
        {
            if (input.Yield.Id.HasValue)
            {
                await UpdateYield(input);
            }
            else
            {
                await CreateYield(input);
            }
        }

        public async Task DeleteYield(IdInput input)
        {
            var mas = await _yieldRepo.GetAsync(input.Id);

            Location loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == mas.LocationRefId);
            if (loc == null)
            {
                throw new UserFriendlyException(L("LocationErr"));
            }


            if (mas.IssueTime.Date == loc.HouseTransactionDate.Value)
            {
            
                    var inputdet = await _inputRepo.GetAll().Where(t => t.YieldRefId == input.Id).ToListAsync();

                    foreach (var detDto in inputdet)
                    {
                        MaterialLedgerDto ml = new MaterialLedgerDto();
                        ml.LocationRefId = mas.LocationRefId;
                        ml.MaterialRefId = detDto.InputMaterialRefId;
                        ml.LedgerDate = mas.IssueTime.Date;
                        ml.Issued = -1 * detDto.InputQty;
                        ml.UnitId = detDto.UnitRefId;


                        var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                    }

                    await _inputRepo.DeleteAsync(t => t.YieldRefId == input.Id);

                    var outputdet = await _outputRepo.GetAll().Where(t => t.YieldRefId == input.Id).ToListAsync();

                    foreach (var detDto in outputdet)
                    {
                        MaterialLedgerDto ml = new MaterialLedgerDto();
                        ml.LocationRefId = mas.LocationRefId;
                        ml.MaterialRefId = detDto.OutputMaterialRefId;
                        ml.LedgerDate = mas.IssueTime.Date;
                        ml.Received = -1 * detDto.OutputQty;
                        ml.UnitId = detDto.UnitRefId;

                        var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                    }
                    await _outputRepo.DeleteAsync(t => t.YieldRefId == input.Id);

                    await _yieldRepo.DeleteAsync(input.Id);
            
            }
            else
            {
                throw new UserFriendlyException("YieldsCurrentDateCanOnlyDeleted");
            }

          
        }

        protected virtual async Task UpdateYield(CreateOrUpdateYieldInput input)
        {
            var item = await _yieldRepo.GetAsync(input.Yield.Id.Value);

            if (input.YieldInputDetail == null || input.YieldInputDetail.Count() == 0)
            {
                throw new UserFriendlyException(L("MinimumOneDetail"));
            }

            if ((input.YieldOutputDetail == null || input.YieldOutputDetail.Count() == 0) && input.StatusToBeAssigned.Equals(L("Completed")) )
            {
                throw new UserFriendlyException(L("YieldOutputShouldNotBeEmpty"));
            }

            var dto = input.Yield;
            item.RequestSlipNumber = dto.RequestSlipNumber;
            item.TokenNumber = dto.TokenNumber;
            item.Status = input.StatusToBeAssigned;
            item.CompletedTime = input.Yield.CompletedTime;
            item.ProductionUnitRefId = input.Yield.ProductionUnitRefId;

            List<int> materialsToBeRetained = new List<int>();

            if (input.YieldInputDetail != null && input.YieldInputDetail.Count > 0)
            {
                foreach (var items in input.YieldInputDetail)
                {
                    int materialrefid = items.InputMaterialRefId;
                    materialsToBeRetained.Add(materialrefid);

                    var existingDetailEntry = _inputRepo.GetAllList(u => u.YieldRefId == dto.Id && u.InputMaterialRefId ==materialrefid);

                    if (existingDetailEntry.Count == 0)  //  Add New Material
                    {
                        YieldInput ab = new YieldInput();

                        ab.YieldRefId = (int)dto.Id;
                        ab.InputQty = items.InputQty;
                        ab.InputMaterialRefId = items.InputMaterialRefId;
                        ab.UnitRefId = items.UnitRefId;

                        var retId2 = await _inputRepo.InsertAndGetIdAsync(ab);

                        MaterialLedgerDto ml = new MaterialLedgerDto();
                        ml.LocationRefId = dto.LocationRefId;
                        ml.MaterialRefId = ab.InputMaterialRefId;
                        ml.LedgerDate = dto.IssueTime.Date;
                        ml.Issued = ab.InputQty;
                        ml.UnitId = ab.UnitRefId;

                        var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                        ab.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;
                    }
                    else
                    {
                        var editDetailDto = await _inputRepo.GetAsync(existingDetailEntry[0].Id);

                        decimal differcenceQty;
                        differcenceQty = items.InputQty - editDetailDto.InputQty;

                        editDetailDto.YieldRefId = (int)dto.Id;
                        editDetailDto.InputQty = items.InputQty;
                        editDetailDto.InputMaterialRefId = items.InputMaterialRefId;
                        editDetailDto.UnitRefId = items.UnitRefId;

                        var retId2 = await _inputRepo.InsertOrUpdateAndGetIdAsync(editDetailDto);

                        if (differcenceQty != 0)
                        {
                            MaterialLedgerDto ml = new MaterialLedgerDto();
                            ml.LocationRefId = dto.LocationRefId;
                            ml.MaterialRefId = items.InputMaterialRefId;
                            ml.LedgerDate = dto.IssueTime.Date;
                            ml.Issued = differcenceQty;
                            ml.PlusMinus = "U";
                            ml.UnitId = items.UnitRefId;

                            var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                            editDetailDto.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;
                        }
                    }
                }
            }

            var delDetailList = _inputRepo.GetAll().Where(a => a.YieldRefId == input.Yield.Id.Value && !materialsToBeRetained.Contains(a.InputMaterialRefId)).ToList();

            foreach (var a in delDetailList)
            {
                MaterialLedgerDto ml = new MaterialLedgerDto();
                ml.LocationRefId = dto.LocationRefId;
                ml.MaterialRefId = a.InputMaterialRefId;
                ml.LedgerDate = dto.IssueTime.Date;
                ml.Issued = (-1 * a.InputQty);
                ml.PlusMinus = "U";
                ml.UnitId = a.UnitRefId;

                var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);

                _inputRepo.Delete(a.Id);
            }

            //  //  Update Output Detail Link
            List<int> outputmaterialsToBeRetained = new List<int>();

            if (input.YieldOutputDetail != null && input.YieldOutputDetail.Count > 0)
            {
                var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == dto.LocationRefId);
                item.YieldOutputAccountDate = loc.HouseTransactionDate.Value;
                foreach (var items in input.YieldOutputDetail)
                {
                    int materialrefid = items.OutputMaterialRefId;
                    outputmaterialsToBeRetained.Add(materialrefid);

                    var existingDetailEntry = _outputRepo.GetAllList(u => u.YieldRefId == dto.Id && u.OutputMaterialRefId == materialrefid);

                    if (existingDetailEntry.Count == 0)  //  Add New Material
                    {
                        YieldOutput ab = new YieldOutput();

                        ab.YieldRefId = (int)dto.Id;
                        ab.OutputQty = items.OutputQty;
                        ab.OutputMaterialRefId = items.OutputMaterialRefId;
                        ab.UnitRefId = items.UnitRefId;

                        var retId2 = await _outputRepo.InsertAndGetIdAsync(ab);

                        MaterialLedgerDto ml = new MaterialLedgerDto();
                        ml.LocationRefId = dto.LocationRefId;
                        ml.MaterialRefId = ab.OutputMaterialRefId;
                        ml.LedgerDate = dto.CompletedTime.Value.Date;
                        ml.Received = ab.OutputQty;
                        ml.UnitId = ab.UnitRefId;

                        var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                        ab.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;
                    }
                    else
                    {
                        var editDetailDto = await _outputRepo.GetAsync(existingDetailEntry[0].Id);

                        decimal differcenceQty;
                        differcenceQty = items.OutputQty - editDetailDto.OutputQty;

                        editDetailDto.YieldRefId = (int)dto.Id;
                        editDetailDto.OutputQty = items.OutputQty;
                        editDetailDto.OutputMaterialRefId = items.OutputMaterialRefId;
                        editDetailDto.UnitRefId = items.UnitRefId;

                        var retId2 = await _outputRepo.InsertOrUpdateAndGetIdAsync(editDetailDto);

                        if (differcenceQty != 0)
                        {
                            MaterialLedgerDto ml = new MaterialLedgerDto();
                            ml.LocationRefId = dto.LocationRefId;
                            ml.MaterialRefId = items.OutputMaterialRefId;
                            ml.LedgerDate = dto.CompletedTime.Value.Date;
                            ml.Received = differcenceQty;
                            ml.PlusMinus = "U";
                            ml.UnitId = items.UnitRefId;

                            var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                            editDetailDto.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;
                        }
                    }
                }
            }

            var deloutputDetailList = _outputRepo.GetAll().Where(a => a.YieldRefId == input.Yield.Id.Value && !outputmaterialsToBeRetained.Contains(a.OutputMaterialRefId)).ToList();

            foreach (var a in deloutputDetailList)
            {
                MaterialLedgerDto ml = new MaterialLedgerDto();
                ml.LocationRefId = dto.LocationRefId;
                ml.MaterialRefId = a.OutputMaterialRefId;
                ml.LedgerDate = dto.CompletedTime.Value.Date;
                ml.Received = (-1 * a.OutputQty);
                ml.PlusMinus = "U";
                ml.UnitId = a.UnitRefId;

                var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);

                _outputRepo.Delete(a.Id);
            }

            CheckErrors(await _yieldManager.CreateSync(item));

			if (item.Status.Equals(L("Completed")))
			{
				await SetYieldPrice(new IdInput {Id = item.Id });
			}
        }

		protected virtual async Task SetYieldPrice(IdInput input)
		{
			var mas = await _yieldRepo.FirstOrDefaultAsync(t => t.Id == input.Id);
			if (mas==null)
			{
				return;
			}

			#region InputCost
			var rsYieldInput = await _inputRepo.GetAllListAsync(t => t.YieldRefId == input.Id);

			List<int> arrMaterialRefIds = rsYieldInput.Select(t => t.InputMaterialRefId).ToList();

			var loc = await _locationRepo.GetAll().Where(t => t.Id == mas.LocationRefId).ToListAsync();
			List<LocationListDto> locsInput = loc.MapTo<List<LocationListDto>>();

			GetHouseReportMaterialRateInput rateDto = new GetHouseReportMaterialRateInput
			{
				StartDate = mas.IssueTime.Date,
				EndDate = mas.IssueTime.Date,
				Locations = locsInput,
				MaterialRefIds = arrMaterialRefIds,
                FunctionCalledBy = "Yield Price Set"
            };

			decimal inputCostValue = 0;
			var MaterialRate = await _houseReportAppService.GetMaterialRateView(rateDto);
			foreach(var lst in rsYieldInput)
			{
				var matprice = MaterialRate.MaterialRateView.FirstOrDefault(t => t.MaterialRefId == lst.InputMaterialRefId);
				if (matprice!=null)
				{
					inputCostValue = inputCostValue + matprice.AvgRate * lst.InputQty;
				}
			}
			#endregion

			if (inputCostValue==0)
			{
				return;
			}

			var rsYieldOutput = await _outputRepo.GetAllListAsync(t => t.YieldRefId == input.Id);
			decimal totalOutputQty = rsYieldOutput.Sum(t => t.OutputQty);
			decimal perQtyPrice = Math.Round(inputCostValue / totalOutputQty,2);
			foreach(var lst in rsYieldOutput)
			{
				lst.YieldPrice = perQtyPrice;
				await _outputRepo.UpdateAsync(lst);
			}
		}

        protected virtual async Task CreateYield(CreateOrUpdateYieldInput input)
        {
            var dto = input.Yield.MapTo<Yield>();
            dto.Status = input.StatusToBeAssigned;


            if (input.YieldInputDetail == null || input.YieldInputDetail.Count() == 0)
            {
                throw new UserFriendlyException(L("MinimumOneDetail"));
            }
            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == dto.LocationRefId);
            dto.AccountDate = loc.HouseTransactionDate.Value;

            if (input.YieldOutputDetail != null && input.YieldOutputDetail.Count > 0)
            {
                dto.YieldOutputAccountDate = loc.HouseTransactionDate.Value;
            }

            var retId = await _yieldRepo.InsertAndGetIdAsync(dto);

			#region YieldInput
			foreach (YieldInputViewDto item in input.YieldInputDetail)
            {
                YieldInput idet = new YieldInput();
                idet.YieldRefId = (int)dto.Id;
                idet.InputMaterialRefId = item.InputMaterialRefId;
                idet.InputQty = item.InputQty;
                idet.UnitRefId = item.UnitRefId;

                var retId2 = await _inputRepo.InsertAndGetIdAsync(idet);

                MaterialLedgerDto ml = new MaterialLedgerDto();
                ml.LocationRefId = dto.LocationRefId;
                ml.MaterialRefId = item.InputMaterialRefId;
                ml.LedgerDate = dto.IssueTime.Date;
                ml.Issued = item.InputQty;
                ml.UnitId = item.UnitRefId;

                var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                idet.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;
            }
			#endregion

          
			foreach (YieldOutputViewDto item in input.YieldOutputDetail)
            {
                YieldOutput iout = new YieldOutput();
                iout.YieldRefId = (int)dto.Id;
                iout.OutputMaterialRefId = item.OutputMaterialRefId;
                iout.OutputQty = item.OutputQty;
                iout.UnitRefId = item.UnitRefId;
				
                var retId3 = await _outputRepo.InsertAndGetIdAsync(iout);

                MaterialLedgerDto ml = new MaterialLedgerDto();
                ml.LocationRefId = dto.LocationRefId;
                ml.MaterialRefId = iout.OutputMaterialRefId;
                ml.LedgerDate = dto.IssueTime.Date;
                ml.Received = iout.OutputQty;
                ml.UnitId = iout.UnitRefId;

                var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                iout.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;
            }

			if (dto.Status.Equals(L("Completed")))
			{
				await SetYieldPrice(new IdInput { Id = dto.Id });
			}

			string templateJson = _xmlandjsonConvertorAppService.SerializeToJSON(input);

            Template lastTemplate;
            int? templateScope;
            if (input.Templatesave.Templatescope == true)
            {
                templateScope = null;
            }
            else
            {
                templateScope = input.Yield.LocationRefId;
            }

            if (input.Templatesave.Templateflag == true)
            {
                string repName = input.Templatesave.Templatename;

                var templateExist = _templateRepo.GetAll().Where(t => t.Name == repName && t.TemplateType == (int)TemplateType.SupplierPurchaseOrder).ToList();
                if (templateExist.Count == 0)
                {
                    lastTemplate = new Template
                    {
                        Name = input.Templatesave.Templatename,
                        TemplateType = (int)TemplateType.Yield,
                        TemplateData = templateJson,
                        LocationRefId = templateScope
                    };
                }
                else
                {
                    lastTemplate = templateExist[0];
                    lastTemplate.LocationRefId = templateScope;
                    lastTemplate.TemplateData = templateJson;
                }
            }
            else
            {
                string repName = L("LastYield");
                var templateExist = _templateRepo.GetAll().Where(t => t.Name == repName && t.TemplateType == (int)TemplateType.Yield).ToList();
                if (templateExist.Count() == 0)
                {
                    lastTemplate = new Template
                    {
                        Name = L("LastYield"),
                        TemplateType = (int)TemplateType.Yield,
                        TemplateData = templateJson,
                        LocationRefId = input.Yield.LocationRefId
                    };
                }
                else
                {
                    lastTemplate = templateExist[0];
                    lastTemplate.LocationRefId = templateScope;
                    lastTemplate.TemplateData = templateJson;
                }
            }

            await _templateRepo.InsertOrUpdateAndGetIdAsync(lastTemplate);

        }

        public virtual async Task CreateDraft(CreateOrUpdateYieldInput input)
        {
            string templateJson = _xmlandjsonConvertorAppService.SerializeToJSON(input);

            Template lastTemplate;
            int? templateScope;

            templateScope = input.Yield.LocationRefId;


            if (input.Templatesave.Templateflag == true)
            {
                string repName = input.Templatesave.Templatename;

                var templateExist = _templateRepo.GetAll().Where(t => t.Name == repName && t.TemplateType == (int)TemplateType.SupplierPurchaseOrder).ToList();
                if (templateExist.Count == 0)
                {
                    lastTemplate = new Template
                    {
                        Name = input.Templatesave.Templatename,
                        TemplateType = (int)TemplateType.SupplierPurchaseOrder,
                        TemplateData = templateJson,
                        LocationRefId = templateScope
                    };
                }
                else
                {
                    lastTemplate = templateExist[0];
                    lastTemplate.LocationRefId = templateScope;
                    lastTemplate.TemplateData = templateJson;
                }
            }
            else
            {
                string repName = L("LastYieldDrafted");
                var templateExist = _templateRepo.GetAll().Where(t => t.Name == repName && t.TemplateType == (int)TemplateType.SupplierPurchaseOrder).ToList();
                if (templateExist.Count == 0)
                {
                    lastTemplate = new Template
                    {
                        Name = L("LastYieldDrafted"),
                        TemplateType = (int)TemplateType.SupplierPurchaseOrder,
                        TemplateData = templateJson,
                        LocationRefId = input.Yield.LocationRefId
                    };
                }
                else
                {
                    lastTemplate = templateExist[0];
                    lastTemplate.LocationRefId = templateScope;
                    lastTemplate.TemplateData = templateJson;
                }
            }

            await _templateRepo.InsertOrUpdateAndGetIdAsync(lastTemplate);
        }

        public async Task<GetYieldForEditOutput> GetTemplateObjectForEdit(GetObjectFromString input)
        {
            GetYieldForEditOutput ReturnObject = _xmlandjsonConvertorAppService.DeSerializeFromJSON<GetYieldForEditOutput>(input.ObjectString);

            YieldEditDto editDto = ReturnObject.Yield;

            editDto.IssueTime = DateTime.Today;
            editDto.TokenNumber = 0;
            editDto.RequestSlipNumber = "";

            List<YieldInputViewDto> editInputDetailDto = ReturnObject.YieldInputDetail;
            List<YieldOutputViewDto> editOutputDetailDto = ReturnObject.YieldOutputDetail;

            var inputdto = (from det in editInputDetailDto
                     join mat in _materialRepo.GetAll() on det.InputMaterialRefId equals mat.Id
                     join locmat in _materiallocationwisestockRepo.GetAll().Where(l=>l.LocationRefId==editDto.LocationRefId) 
                     on mat.Id equals locmat.MaterialRefId 
                     join un in _unitRepo.GetAll() on mat.DefaultUnitId equals un.Id
                     join uiss in _unitRepo.GetAll() on det.UnitRefId equals uiss.Id
                     select new YieldInputViewDto
                     {
                         YieldRefId = (int)det.YieldRefId,
                         InputMaterialRefId = det.InputMaterialRefId,
                         MaterialRefName = mat.MaterialName,
                         InputQty = det.InputQty,
                         Sno = det.Sno,
                         CurrentInHand = locmat.CurrentInHand,
                         UnitRefId = det.UnitRefId,
                         Uom = un.Name,
                         UnitRefName = uiss.Name,
                         DefaultUnitId = mat.DefaultUnitId,
                         DefaultUnitName = un.Name
                     });

            List<YieldInputViewDto> retInput = inputdto.ToList();

            var outputdto = (from det in editOutputDetailDto
                            join mat in _materialRepo.GetAll() on det.OutputMaterialRefId equals mat.Id
                            join locmat in _materiallocationwisestockRepo.GetAll().Where(l => l.LocationRefId == editDto.LocationRefId)
                            on mat.Id equals locmat.MaterialRefId
                            join un in _unitRepo.GetAll() on mat.DefaultUnitId equals un.Id
                            join uiss in _unitRepo.GetAll() on det.UnitRefId equals uiss.Id
                             select new YieldOutputViewDto
                            {
                                YieldRefId = (int)det.YieldRefId,
                                OutputMaterialRefId = det.OutputMaterialRefId,
                                MaterialRefName = mat.MaterialName,
                                OutputQty = det.OutputQty,
                                Sno = det.Sno,
                                CurrentInHand = locmat.CurrentInHand,
                                UnitRefId = det.UnitRefId,
                                Uom = un.Name,
                                UnitRefName = uiss.Name,
                                DefaultUnitId = mat.DefaultUnitId,
                                DefaultUnitName = un.Name

                             });

            List<YieldOutputViewDto> retoutput = outputdto.ToList();

            return new GetYieldForEditOutput
            {
                Yield = editDto,
                YieldInputDetail = retInput,
                YieldOutputDetail = retoutput
            };
        }

        public async Task<SetYieldDashBoardDto> GetDashBoardYield(IdInput input)
        {
            string poStatus = L("Pending");

            var lstpending = await _yieldRepo.GetAll().Where(p => p.LocationRefId == input.Id && p.Status.Equals(poStatus)).ToListAsync();
            int pendingCount = lstpending.Count();

            poStatus = L("Completed");
            var lstApproved = _yieldRepo.GetAll().Where(p => p.LocationRefId == input.Id && DbFunctions.TruncateTime(p.CompletedTime) > DateTime.Today && p.Status.Equals(poStatus)).ToList();
            int approvedCount = lstApproved.Count();


            return new SetYieldDashBoardDto
            {
                TotalFinished = approvedCount,
                TotalPending = pendingCount,
            };
        }

    }
}
