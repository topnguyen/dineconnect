﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using DinePlan.DineConnect.House.Master.Dtos;

namespace DinePlan.DineConnect.House.Transaction
{
    public interface ISalesOrderAppService : IApplicationService
    {
        Task<FileDto> GetAllToExcel(GetSalesOrderInput input);
        Task<GetSalesOrderForEditOutput> GetSalesOrderForEdit(NullableIdInput nullableIdInput);
        Task<GetSalesIdAndReference> CreateOrUpdateSalesOrder(CreateOrUpdateSalesOrderInput input);
        Task DeleteSalesOrder(IdInput input);

        Task<PagedResultOutput<SalesOrderListWithRefNameDto>> GetAllWithRefName(GetSalesOrderInput input);
        Task<GetSalesOrderForEditOutput> GetSoDetailBasedOnSoReferenceCode(NullableIdInput input);

        Task<GetSalesOrderForEditOutput> GetTemplateObjectForEdit(GetObjectFromString input);

        Task CreateDraft(CreateOrUpdateSalesOrderInput input);

        Task SetSalesOrderStatus(SalesOrderStatus input);
        Task<SetSalesOrderDashBoardDto> GetDashBoardSalesOrder(IdInput input);

        //Task<string> GetSalesOrderReportInNotePad(GetSalesOrderInput input);

        Task SendSalesOrderEmail(SetSalesOrderPrint input);

        Task<ListResultOutput<SalesOrderListDto>> GetSalesReferenceNumber(GetSoReferenceDto input);

        Task SetCompleteSalesOrder(SalesOrderStatus input);

        Task<List<MaterialVsCustomerDtos>> GetCustomerInfoForParticularMaterial(IdInput input);

    }
}