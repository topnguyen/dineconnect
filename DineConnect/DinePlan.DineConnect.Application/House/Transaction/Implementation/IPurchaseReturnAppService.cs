﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Transaction.Dtos;

namespace DinePlan.DineConnect.House.Transaction.Implementation
{
    public interface IPurchaseReturnAppService : IApplicationService
    {
        Task<PagedResultOutput<PurchaseReturnListDto>> GetAll(GetPurchaseReturnInput inputDto);
        Task<FileDto> GetAllToExcel(GetPurchaseReturnInput input);
        Task<GetPurchaseReturnForEditOutput> GetPurchaseReturnForEdit(NullableIdInput nullableIdInput);
        Task<IdInput> CreateOrUpdatePurchaseReturn(CreateOrUpdatePurchaseReturnInput input);
        Task DeletePurchaseReturn(IdInput input);
        Task<PurchaseReturnDetailOutputDto> GetPurchaseReturnReport(PurchaseReturnDetailInputDto input);
        Task<FileDto> GetPurchaseReturnReportToExcel(PurchaseReturnDetailInputDto input);
    }
}

