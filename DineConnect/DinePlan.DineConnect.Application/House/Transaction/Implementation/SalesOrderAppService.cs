﻿using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Master.Dtos;
using Abp.Authorization;
using System;
using System.IO;
using System.Xml.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using Abp.Net.Mail;
using Abp.Domain.Uow;
using System.Globalization;
using System.Threading;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Connect.Master;

namespace DinePlan.DineConnect.House.Transaction.Implementation
{
    public class SalesOrderAppService : DineConnectAppServiceBase, ISalesOrderAppService
    {

        private readonly ISalesOrderListExcelExporter _salesorderExporter;
        private readonly ISalesOrderManager _salesorderManager;
        private readonly IRepository<SalesOrderDetail> _salesorderdetailRepo;
        private readonly IRepository<SalesOrderTaxDetail> _salesordertaxdetailRepo;
        private readonly IRepository<Material> _materialRepo;
        
        private readonly IRepository<SalesOrder> _salesorderRepo;

        private readonly IRepository<Customer> _customerRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<Company> _companyRepo;
        private readonly IRepository<Template> _templateRepo;
        private readonly IRepository<CustomerMaterial> _customermaterialRepo;
        private readonly IRepository<Unit> _unitRepo;
        private readonly IRepository<TaxTemplateMapping> _taxtemplatemappingRepo;
        private readonly IRepository<SalesTax> _salestaxRepo;
        private readonly IEmailSender _emailSender;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<SalesInvoice> _salesinvoiceRepo;
        private readonly IRepository<SalesInvoiceDetail> _salesinvoicedetailRepo;

        public SalesOrderAppService(
            ISalesOrderManager salesorderManager,
            IRepository<SalesOrder> salesOrderRepo,
            ISalesOrderListExcelExporter salesorderExporter, 
			IRepository<SalesOrderDetail> salesorderdetailRepo,
            IRepository<SalesOrderTaxDetail> salesordertaxdetailRepo,
            IRepository<Material> materialRepo,
            IRepository<Customer> customerRepo,
            IRepository<Location> locationRepo
            , IRepository<Template> templateRepo,
            IRepository<CustomerMaterial> customermaterialRepo,
            IRepository<Unit> unitRepo,
            IRepository<TaxTemplateMapping> taxtemplatemappingRepo,
            IRepository<SalesTax> salestaxRepo,
            IEmailSender emailSender,
            IRepository<Company> companyRepo,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<SalesInvoice> salesinvoiceRepo,
            IRepository<SalesInvoiceDetail> salesinvoicedetailRepo
            )
        {
            _salesorderManager = salesorderManager;
            _salesorderRepo = salesOrderRepo;
            _salesorderExporter = salesorderExporter;
            _salesorderdetailRepo = salesorderdetailRepo;
            _salesordertaxdetailRepo = salesordertaxdetailRepo;
            _materialRepo = materialRepo;
            _customerRepo = customerRepo;
            _locationRepo = locationRepo;
            _templateRepo = templateRepo;
            _customermaterialRepo = customermaterialRepo;
            _unitRepo = unitRepo;
            _salestaxRepo = salestaxRepo;
            _taxtemplatemappingRepo = taxtemplatemappingRepo;
            _emailSender = emailSender;
            _companyRepo = companyRepo;
            _unitOfWorkManager = unitOfWorkManager;
            _salesinvoiceRepo = salesinvoiceRepo;
            _salesinvoicedetailRepo = salesinvoicedetailRepo;
        }

        public async Task<PagedResultOutput<SalesOrderListWithRefNameDto>> GetAllWithRefName(GetSalesOrderInput input)
        {
            //PagedResultOutput<SalesOrderListWithRefNameDto> output = new PagedResultOutput<SalesOrderListWithRefNameDto>();
            //return output;

            var rsCustomer = _customerRepo.GetAll().WhereIf(!input.Filter.IsNullOrEmpty(), p => p.CustomerName.Contains(input.Filter));

            var rsMaterial = _materialRepo.GetAll().Where(t => t.Id == input.MaterialRefId);

            var soOrder = _salesorderRepo.GetAll().Where(t => t.LocationRefId == input.DefaultLocationRefId)
                                                    .WhereIf(!input.SoReferenceCode.IsNullOrEmpty(), t => t.SoReferenceCode.Contains(input.SoReferenceCode));

            if (input.StartDate != null && input.EndDate != null)
            {
                soOrder = soOrder.Where(t => t.SoDate >= input.StartDate && t.SoDate <= input.EndDate);
            }

            if (input.MaterialRefId != null)
            {
                soOrder = (from soMas in soOrder
                           join soDet in _salesorderdetailRepo.GetAll().Where(t => t.MaterialRefId == input.MaterialRefId)
                           on soMas.Id equals soDet.SoRefId
                           select soMas);
            }



            var allItems = (from so in soOrder
                            join sup in rsCustomer
                            on so.CustomerRefId equals sup.Id
                            join loc in _locationRepo.GetAll() on so.ShippingLocationId equals loc.Id
                            //join mat in rsMaterial
                            //on input.MaterialRefId equals mat.Id
                            select new SalesOrderListWithRefNameDto()
                            {
                                Id = so.Id,
                                SoDate = so.SoDate,
                                SoReferenceCode = so.SoReferenceCode,
                                ShippingLocationId = so.ShippingLocationId,
                                ShippingLocationName = loc.Name,
                                QuoteReference = so.QuoteReference,
                                CreditDays = so.CreditDays,
                                CustomerRefId = so.CustomerRefId,
                                CustomerName = sup.CustomerName,
                                SoNetAmount = so.SoNetAmount,
                                TenantId = so.TenantId,
                                Remarks = so.Remarks,
                                LocationRefId = so.LocationRefId,
                                CreationTime = so.CreationTime,
                                SoCurrentStatus = so.SoCurrentStatus,
                                ApprovedTime = so.ApprovedTime,
                                CreatorUserId = so.CreatorUserId
                            });



            var sortMenuItems = await allItems
               .OrderBy(input.Sorting)
               .PageBy(input)
               .ToListAsync();


            var allListDtos = sortMenuItems.MapTo<List<SalesOrderListWithRefNameDto>>();

            int loopIndex = 0;
            foreach (var lst in allListDtos)
            {

                if (allListDtos[loopIndex].SoCurrentStatus.Equals("P"))
                {
                    allListDtos[loopIndex].SoCurrentStatus = L("Pending");
                }
                else if (allListDtos[loopIndex].SoCurrentStatus.Equals("A"))
                {
                    allListDtos[loopIndex].SoCurrentStatus = L("Approved");
                }
                else if (allListDtos[loopIndex].SoCurrentStatus.Equals("X"))
                {
                    allListDtos[loopIndex].SoCurrentStatus = L("Declined");
                }
                else if (allListDtos[loopIndex].SoCurrentStatus.Equals("C"))
                {
                    allListDtos[loopIndex].SoCurrentStatus = L("Completed");
                }
                else if (allListDtos[loopIndex].SoCurrentStatus.Equals("H"))
                {
                    allListDtos[loopIndex].SoCurrentStatus = L("Partial");
                }
                loopIndex++;

            }


            if (!input.SoStatus.IsNullOrEmpty())
            {
                //Get the culture property of the thread.
                CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                //Create TextInfo object.
                TextInfo textInfo = cultureInfo.TextInfo;

                input.SoStatus = input.SoStatus.ToLowerInvariant();

                var searchStatus = textInfo.ToTitleCase(input.SoStatus);

                allListDtos = allListDtos.Where(t => t.SoCurrentStatus.Contains(searchStatus)).ToList();
            }



            var allItemCount = allListDtos.Count();

            return new PagedResultOutput<SalesOrderListWithRefNameDto>(
                allItemCount,
                allListDtos
                );

        }

        public async Task<FileDto> GetAllToExcel(GetSalesOrderInput input)
        {
            input.MaxResultCount = AppConsts.MaxPageSize;

            var allList = await GetAllWithRefName(input);
            var allListDtos = allList.Items.MapTo<List<SalesOrderListWithRefNameDto>>();
            return _salesorderExporter.ExportToFile(allListDtos);
        }

        public async Task<GetSalesOrderForEditOutput> GetSalesOrderForEdit(NullableIdInput input)
        {
            SalesOrderEditDto editDto;
            List<SalesOrderDetailViewDto> editDetailDto;
            TemplateSaveDto editTemplateDto = new TemplateSaveDto();

            if (input.Id.HasValue)
            {
                var hDto = await _salesorderRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<SalesOrderEditDto>();

                editDto.CustomerRefName = _customerRepo.FirstOrDefault(t => t.Id == editDto.CustomerRefId).CustomerName;
                editDto.ShippingLocationRefName = _locationRepo.FirstOrDefault(t => t.Id == editDto.ShippingLocationId).Name;
                editDto.LocationRefName = _locationRepo.FirstOrDefault(t => t.Id == editDto.LocationRefId).Name;

                editDetailDto = await (from soDet in _salesorderdetailRepo.GetAll()
                                       .Where(a => a.SoRefId == input.Id.Value)
                                       join mat in _materialRepo.GetAll()
                                       on soDet.MaterialRefId equals mat.Id
                                       join un in _unitRepo.GetAll()
                                       on mat.DefaultUnitId equals un.Id
                                       join udet in _unitRepo.GetAll()
                                       on soDet.UnitRefId equals udet.Id
                                       select new SalesOrderDetailViewDto
                                       {
                                           SoRefId = (int)soDet.SoRefId,
                                           MaterialRefId = soDet.MaterialRefId,
                                           MaterialRefName = mat.MaterialName,
                                           QtyOrdered = soDet.QtyOrdered,
                                           QtyReceived = soDet.QtyReceived,
                                           QtyPending = (soDet.QtyOrdered - soDet.QtyReceived) > 0 ? (soDet.QtyOrdered - soDet.QtyReceived) : 0,
                                           UnitRefId = soDet.UnitRefId,
                                           UnitRefName = udet.Name,
                                           Price = soDet.Price,
                                           DiscountOption = soDet.DiscountOption,
                                           DiscountValue = soDet.DiscountValue,
                                           TotalAmount = soDet.TotalAmount,
                                           TaxAmount = soDet.TaxAmount,
                                           NetAmount = soDet.NetAmount,
                                           SoStatus = soDet.SoStatus,
                                           Remarks = soDet.Remarks,
                                           Uom = un.Name,
                                           DefaultUnitId = mat.DefaultUnitId
                                       }).ToListAsync();
                int loopCnt = 0;
                foreach (var det in editDetailDto)
                {
                    var taxlist = await (from sotax in _salesordertaxdetailRepo.GetAll().Where(pot => pot.SoRefId == input.Id.Value && pot.MaterialRefId == det.MaterialRefId)
                                         join tax in _salestaxRepo.GetAll()
                                         on sotax.SalesTaxRefId equals tax.Id
                                         select new TaxForMaterial
                                         {
                                             MaterialRefId = sotax.MaterialRefId,
                                             TaxRefId = sotax.SalesTaxRefId,
                                             TaxName = tax.TaxName,
                                             TaxRate = sotax.SalesTaxRate,
                                             TaxValue = sotax.SalesTaxValue,
                                             SortOrder = tax.SortOrder,
                                             Rounding = tax.Rounding,
                                             TaxCalculationMethod = tax.TaxCalculationMethod,
                                         }).ToListAsync();

                    editDetailDto[loopCnt].TaxForMaterial = taxlist.OrderBy(t => t.SortOrder).ToList();

                    List<ApplicableTaxesForMaterial> applicabletaxes = new List<ApplicableTaxesForMaterial>();

                    foreach (var tax in taxlist.OrderBy(t => t.SortOrder))
                    {
                        applicabletaxes.Add(new ApplicableTaxesForMaterial
                        {
                            TaxName = tax.TaxName,
                            TaxRefId = tax.TaxRefId,
                            TaxRate = tax.TaxRate,
                            SortOrder = tax.SortOrder,
                            Rounding = tax.Rounding,
                            TaxCalculationMethod = tax.TaxCalculationMethod,
                        });
                    }

                    editDetailDto[loopCnt].ApplicableTaxes = applicabletaxes;

                    loopCnt++;
                }

            }
            else
            {
                editDto = new SalesOrderEditDto();
                editDetailDto = new List<SalesOrderDetailViewDto>();
            }

            return new GetSalesOrderForEditOutput
            {
                SalesOrder = editDto,
                SalesOrderDetail = editDetailDto,
                Templatesave = editTemplateDto
            };
        }

        public async Task<GetSalesIdAndReference> CreateOrUpdateSalesOrder(CreateOrUpdateSalesOrderInput input)
        {
            if (input.SalesOrder.Id.HasValue)
            {
                return await UpdateSalesOrder(input);
            }
            else
            {
                return await CreateSalesOrder(input);
            }
        }

        public async Task DeleteSalesOrder(IdInput input)
        {
            var so = await _salesorderRepo.FirstOrDefaultAsync(t => t.Id == input.Id);

            if (so.SoCurrentStatus == "P" || so.SoCurrentStatus == "A")
            {
                await _salesordertaxdetailRepo.DeleteAsync(u => u.SoRefId == input.Id);

                await _salesorderdetailRepo.DeleteAsync(u => u.SoRefId == input.Id);

                await _salesorderRepo.DeleteAsync(input.Id);
            }
        }

        protected virtual async Task<GetSalesIdAndReference> UpdateSalesOrder(CreateOrUpdateSalesOrderInput input)
        {

            bool ApprovedAuthorization = false;
            if (PermissionChecker.IsGranted("Pages.Tenant.House.Transaction.SalesOrder.Approve"))
            {
                ApprovedAuthorization = true;
            }

            var item = await _salesorderRepo.GetAsync(input.SalesOrder.Id.Value);

            if (input.SalesOrderDetail == null || input.SalesOrderDetail.Count() == 0)
            {
                throw new UserFriendlyException(L("MinimumOneDetail"));
            }

            var dto = input.SalesOrder;

            item.SoDate = dto.SoDate;
            item.SoReferenceCode = dto.SoReferenceCode;
            item.ShippingLocationId = dto.ShippingLocationId;
            item.QuoteReference = dto.QuoteReference;
            item.CreditDays = dto.CreditDays;
            item.CustomerRefId = dto.CustomerRefId;
            item.Remarks = dto.Remarks;
            item.LocationRefId = dto.LocationRefId;
            item.DeliveryDateExpected = dto.DeliveryDateExpected;
            item.SoNetAmount = dto.SoNetAmount;


            if (ApprovedAuthorization)
            {
                item.SoCurrentStatus = input.StatusToBeAssigned;
                item.ApprovedTime = DateTime.Now;
            }
            else
            {
                item.SoCurrentStatus = input.StatusToBeAssigned;
            }

            List<int> materialsToBeRetained = new List<int>();
            if (input.SalesOrderDetail != null && input.SalesOrderDetail.Count > 0)
            {
                foreach (var items in input.SalesOrderDetail)
                {
                    var matRefId = items.MaterialRefId;
                    materialsToBeRetained.Add(matRefId);
                    var existingReturnDetail = _salesorderdetailRepo.GetAllList(u => u.SoRefId == dto.Id && u.MaterialRefId == matRefId);
                    if (existingReturnDetail.Count == 0)  //Add new record
                    {
                        SalesOrderDetail pd = new SalesOrderDetail();
                        pd.SoRefId = (int)dto.Id;
                        pd.MaterialRefId = items.MaterialRefId;
                        pd.QtyOrdered = items.QtyOrdered;
                        pd.QtyReceived = items.QtyReceived;
                        pd.UnitRefId = items.UnitRefId;
                        pd.Price = items.Price;
                        pd.DiscountOption = items.DiscountOption;
                        pd.DiscountValue = items.DiscountValue;
                        pd.TotalAmount = items.TotalAmount;
                        pd.TaxAmount = items.TaxAmount;
                        pd.NetAmount = items.NetAmount;
                        pd.SoStatus = items.SoStatus;
                        pd.Remarks = items.Remarks;

                        var retId2 = await _salesorderdetailRepo.InsertAndGetIdAsync(pd);

                        foreach (var tax in items.TaxForMaterial)
                        {
                            SalesOrderTaxDetail taxForSoDetail = new SalesOrderTaxDetail
                            {
                                SoRefId = (int)dto.Id,
                                MaterialRefId = items.MaterialRefId,
                                SalesTaxRefId = tax.TaxRefId,
                                SalesTaxRate = tax.TaxRate,
                                SalesTaxValue = tax.TaxValue,
                            };
                            var retsotax = await _salesordertaxdetailRepo.InsertAndGetIdAsync(taxForSoDetail);
                        }
                    }
                    else
                    {
                        var editDetailDto = await _salesorderdetailRepo.GetAsync(existingReturnDetail[0].Id);

                        editDetailDto.SoRefId = (int)dto.Id;
                        editDetailDto.MaterialRefId = items.MaterialRefId;
                        editDetailDto.QtyOrdered = items.QtyOrdered;
                        editDetailDto.UnitRefId = items.UnitRefId;
                        editDetailDto.Price = items.Price;
                        editDetailDto.DiscountOption = items.DiscountOption;
                        editDetailDto.DiscountValue = items.DiscountValue;
                        editDetailDto.TotalAmount = items.TotalAmount;
                        editDetailDto.TaxAmount = items.TaxAmount;
                        editDetailDto.NetAmount = items.NetAmount;
                        editDetailDto.SoStatus = items.SoStatus;
                        editDetailDto.Remarks = items.Remarks;

                        var retId3 = await _salesorderdetailRepo.InsertOrUpdateAndGetIdAsync(editDetailDto);


                        if (items.TaxForMaterial != null && items.TaxForMaterial.Count > 0)
                        {
                            List<int> taxrefidtoberetained = new List<int>();
                            foreach (var taxdet in items.TaxForMaterial)
                            {
                                int taxrefid = taxdet.TaxRefId;
                                taxrefidtoberetained.Add(taxrefid);

                                var existingSoTaxDetails = _salesordertaxdetailRepo.GetAllList(u => u.SoRefId == dto.Id &&
                                u.MaterialRefId == matRefId && u.SalesTaxRefId == taxdet.TaxRefId);
                                if (existingSoTaxDetails.Count == 0)
                                {
                                    SalesOrderTaxDetail taxForSoDetail = new SalesOrderTaxDetail
                                    {
                                        SoRefId = (int)dto.Id,
                                        MaterialRefId = items.MaterialRefId,
                                        SalesTaxRefId = taxdet.TaxRefId,
                                        SalesTaxRate = taxdet.TaxRate,
                                        SalesTaxValue = taxdet.TaxValue,
                                    };
                                    var retsotax = await _salesordertaxdetailRepo.InsertAndGetIdAsync(taxForSoDetail);
                                }
                                else
                                {
                                    var editSotaxitem = await _salesordertaxdetailRepo.GetAsync(existingSoTaxDetails[0].Id);
                                    editSotaxitem.SalesTaxRate = taxdet.TaxRate;
                                    editSotaxitem.SalesTaxValue = taxdet.TaxValue;
                                }
                            }

                            var delSoTaxList = _salesordertaxdetailRepo.GetAll().Where(a => a.SoRefId == input.SalesOrder.Id.Value && a.MaterialRefId == items.MaterialRefId && !taxrefidtoberetained.Contains(a.SalesTaxRefId)).ToList();

                            foreach (var a in delSoTaxList)
                            {
                                _salesordertaxdetailRepo.Delete(a.Id);
                            }
                        }
                        else
                        {
                            var sotaxdetailstobedeleted = _salesordertaxdetailRepo.GetAllList(t => t.SoRefId == dto.Id && t.MaterialRefId == items.MaterialRefId);
                            foreach (var lst in sotaxdetailstobedeleted)
                            {
                                _salesordertaxdetailRepo.Delete(lst.Id);
                            }
                        }
                    }
                }
            }

            var delMatList = _salesorderdetailRepo.GetAll().Where(a => a.SoRefId == input.SalesOrder.Id.Value && !materialsToBeRetained.Contains(a.MaterialRefId)).ToList();

            foreach (var a in delMatList)
            {
                _salesorderdetailRepo.Delete(a.Id);
            }

            CheckErrors(await _salesorderManager.CreateSync(item));

            return new GetSalesIdAndReference
            {
                Id = input.SalesOrder.Id.Value,
                SoReferenceCode = input.SalesOrder.SoReferenceCode
            };
        }

        protected virtual async Task<GetSalesIdAndReference> CreateSalesOrder(CreateOrUpdateSalesOrderInput input)
        {
            bool ApprovedAuthorization = false;
          //  if (PermissionChecker.IsGranted("Pages.Tenant.House.Transaction.SalesOrder.Approve"))
           // {
                ApprovedAuthorization = true;
           // }

            var salesorderno = _salesorderRepo.GetAll().Where(so => so.CustomerRefId == input.SalesOrder.CustomerRefId).ToList().Count + 1;
            var locationcode = await _locationRepo.GetAll().Where(loc => loc.Id == input.SalesOrder.LocationRefId).ToListAsync();
			var sup = await _customerRepo.FirstOrDefaultAsync(s => s.Id == input.SalesOrder.CustomerRefId);

            var dto = input.SalesOrder.MapTo<SalesOrder>();

            if (ApprovedAuthorization)
            {
                dto.SoCurrentStatus = input.StatusToBeAssigned;
                dto.ApprovedTime = DateTime.Now;
            }
            else
            {
                dto.SoCurrentStatus = input.StatusToBeAssigned;
            }
			string supName = sup.CustomerName.Length > 3 ? sup.CustomerName.Left(3) : sup.CustomerName;

			dto.SoReferenceCode = String.Concat(locationcode[0].Code, "/", DateTime.UtcNow.ToString("yyMMMdd"), "/", supName, "/", salesorderno.ToString());

            if (input.SalesOrderDetail == null || input.SalesOrderDetail.Count() == 0)
            {
                throw new UserFriendlyException(L("MinimumOneDetail"));
            }

            var retId = await _salesorderRepo.InsertAndGetIdAsync(dto);

            foreach (SalesOrderDetailViewDto item in input.SalesOrderDetail)
            {
                SalesOrderDetail pd = new SalesOrderDetail();
                pd.SoRefId = (int)dto.Id;
                pd.MaterialRefId = item.MaterialRefId;
                pd.QtyOrdered = item.QtyOrdered;
                pd.QtyReceived = item.QtyReceived;
                pd.UnitRefId = item.UnitRefId;
                pd.Price = item.Price;
                pd.DiscountOption = item.DiscountOption;
                pd.DiscountValue = item.DiscountValue;
                pd.TaxAmount = item.TaxAmount;
                pd.TotalAmount = item.TotalAmount;
                pd.NetAmount = item.NetAmount;
                pd.SoStatus = item.SoStatus;
                pd.Remarks = item.Remarks;
                pd.UnitRefId = item.UnitRefId;
                var retId2 = await _salesorderdetailRepo.InsertAndGetIdAsync(pd);

                if (item.TaxForMaterial != null)
                {
                    foreach (var tax in item.TaxForMaterial)
                    {
                        SalesOrderTaxDetail taxForSoDetail = new SalesOrderTaxDetail
                        {
                            SoRefId = (int)dto.Id,
                            MaterialRefId = item.MaterialRefId,
                            SalesTaxRefId = tax.TaxRefId,
                            SalesTaxRate = tax.TaxRate,
                            SalesTaxValue = tax.TaxRate,
                        };
                        var retId3 = await _salesordertaxdetailRepo.InsertAndGetIdAsync(taxForSoDetail);
                    }
                }
            }

            CheckErrors(await _salesorderManager.CreateSync(dto));

            string templateJson = SerializeToJSON(input);

            Template lastTemplate;
            int? templateScope;
            if (input.Templatesave.Templatescope == true)
            {
                templateScope = null;
            }
            else
            {
                templateScope = input.SalesOrder.LocationRefId;
            }

            if (input.Templatesave.Templateflag == true)
            {
                string repName = input.Templatesave.Templatename;

                var templateExist = _templateRepo.GetAll().Where(t => t.Name == repName && t.TemplateType == (int)TemplateType.CustomerSalesOrder).ToList();
                if (templateExist.Count == 0)
                {
                    lastTemplate = new Template
                    {
                        Name = input.Templatesave.Templatename,
                        TemplateType = (int)TemplateType.CustomerSalesOrder,
                        TemplateData = templateJson,
                        LocationRefId = templateScope
                    };
                }
                else
                {
                    lastTemplate = templateExist[0];
                    lastTemplate.LocationRefId = templateScope;
                    lastTemplate.TemplateData = templateJson;
                }
            }
            else
            {
                string repName = L("LastSo");
                var templateExist = _templateRepo.GetAll().Where(t => t.Name == repName && t.TemplateType == (int)TemplateType.CustomerSalesOrder).ToList();
                if (templateExist.Count() == 0)
                {
                    lastTemplate = new Template
                    {
                        Name = L("LastSo"),
                        TemplateType = (int)TemplateType.CustomerSalesOrder,
                        TemplateData = templateJson,
                        LocationRefId = input.SalesOrder.LocationRefId
                    };
                }
                else
                {
                    lastTemplate = templateExist[0];
                    lastTemplate.LocationRefId = templateScope;
                    lastTemplate.TemplateData = templateJson;
                }
            }

            await _templateRepo.InsertOrUpdateAndGetIdAsync(lastTemplate);

            return new GetSalesIdAndReference
            {
                Id = retId,
                SoReferenceCode = dto.SoReferenceCode
            };
        }

        public virtual async Task SetSalesOrderStatus(SalesOrderStatus input)
        {
            var item = await _salesorderRepo.GetAsync(input.SoId);
            bool ApprovedAuthorization = false;
            if (!PermissionChecker.IsGranted("Pages.Tenant.House.Transaction.SalesOrder.Approve"))
            {
                ApprovedAuthorization = true;
            }
            else
            {
                throw new UserFriendlyException(L("NotAuthorized"));
            }

            if (ApprovedAuthorization)
            {
                item.Remarks = input.SoCurrentStatus;
                item.SoCurrentStatus = "A"; // L("Approved");
                item.ApprovedTime = DateTime.Now;
            }
        }

        public virtual async Task SetCompleteSalesOrder(SalesOrderStatus input)
        {

            bool ApprovedAuthorization = true;
            if (!PermissionChecker.IsGranted("Pages.Tenant.House.Transaction.SalesOrder.Approve"))
            {
                ApprovedAuthorization = true;
            }
            else
            {
                throw new UserFriendlyException(L("NotAuthorized"));
            }

            var item = await _salesorderRepo.GetAsync(input.SoId);
            if (ApprovedAuthorization)
            {
                item.Remarks = input.SoRemarks;
                item.SoCurrentStatus = "C"; // L("Approved");
            }
        }



        public virtual async Task CreateDraft(CreateOrUpdateSalesOrderInput input)
        {
            string templateJson = SerializeToJSON(input);

            Template lastTemplate;
            int? templateScope;

            templateScope = input.SalesOrder.LocationRefId;


            if (input.Templatesave.Templateflag == true)
            {
                string repName = input.Templatesave.Templatename;

                var templateExist = _templateRepo.GetAll().Where(t => t.Name == repName && t.TemplateType == (int)TemplateType.CustomerSalesOrder).ToList();
                if (templateExist.Count == 0)
                {
                    lastTemplate = new Template
                    {
                        Name = input.Templatesave.Templatename,
                        TemplateType = (int)TemplateType.CustomerSalesOrder,
                        TemplateData = templateJson,
                        LocationRefId = templateScope
                    };
                }
                else
                {
                    lastTemplate = templateExist[0];
                    lastTemplate.LocationRefId = templateScope;
                    lastTemplate.TemplateData = templateJson;
                }
            }
            else
            {
                string repName = L("LastSoDrafted");
                var templateExist = _templateRepo.GetAll().Where(t => t.Name == repName && t.TemplateType == (int)TemplateType.CustomerSalesOrder).ToList();
                if (templateExist.Count == 0)
                {
                    lastTemplate = new Template
                    {
                        Name = L("LastSoDrafted"),
                        TemplateType = (int)TemplateType.CustomerSalesOrder,
                        TemplateData = templateJson,
                        LocationRefId = input.SalesOrder.LocationRefId
                    };
                }
                else
                {
                    lastTemplate = templateExist[0];
                    lastTemplate.LocationRefId = templateScope;
                    lastTemplate.TemplateData = templateJson;
                }
            }

            await _templateRepo.InsertOrUpdateAndGetIdAsync(lastTemplate);
        }

        public async Task<ListResultOutput<SalesOrderListDto>> GetSalesReferenceNumber(GetSoReferenceDto input)
        {
            string[] statusStr = { "A", "H" };// L("Approved");
            var lstSalesOrder = await _salesorderRepo.GetAll().Where(so => so.CustomerRefId == input.CustomerRefId && so.LocationRefId == input.LocationRefId && statusStr.Contains(so.SoCurrentStatus)).ToListAsync();
            return new ListResultOutput<SalesOrderListDto>(lstSalesOrder.MapTo<List<SalesOrderListDto>>());
        }

        public async Task<GetSalesOrderForEditOutput> GetSoDetailBasedOnSoReferenceCode(NullableIdInput input)
        {
            SalesOrderEditDto editDto;
            List<SalesOrderDetailViewDto> editDetailDto;

            if (input.Id.HasValue)
            {
                var hDto = await _salesorderRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<SalesOrderEditDto>();

                editDetailDto = await (from soDet in _salesorderdetailRepo.GetAll()
                                       .Where(a => a.SoRefId == input.Id.Value && a.SoStatus == false)
                                       join mat in _materialRepo.GetAll() on soDet.MaterialRefId equals mat.Id
                                       join un in _unitRepo.GetAll() on mat.DefaultUnitId equals un.Id
                                       join udet in _unitRepo.GetAll() on soDet.UnitRefId equals udet.Id
                                       select new SalesOrderDetailViewDto
                                       {
                                           SoRefId = (int)soDet.SoRefId,
                                           MaterialRefId = soDet.MaterialRefId,
                                           MaterialRefName = mat.MaterialName,
                                           QtyOrdered = soDet.QtyOrdered,
                                           QtyReceived = soDet.QtyReceived,
                                           Price = soDet.Price,
                                           DiscountOption = soDet.DiscountOption,
                                           DiscountValue = soDet.DiscountValue,
                                           NetAmount = soDet.NetAmount,
                                           SoStatus = soDet.SoStatus,
                                           Remarks = soDet.Remarks,
                                           UnitRefId = soDet.UnitRefId,
                                           UnitRefName = udet.Name,
                                           Uom = un.Name,
                                           DefaultUnitId = mat.DefaultUnitId
                                       }).ToListAsync();
            }
            else
            {
                editDto = new SalesOrderEditDto();
                editDetailDto = new List<SalesOrderDetailViewDto>();
            }

            return new GetSalesOrderForEditOutput
            {
                SalesOrder = editDto,
                SalesOrderDetail = editDetailDto
            };
        }

        public async Task<GetSalesOrderForEditOutput> GetTemplateObjectForEdit(GetObjectFromString input)
        {
            GetSalesOrderForEditOutput ReturnObject = DeSerializeFromJSON<GetSalesOrderForEditOutput>(input.ObjectString);

            SalesOrderEditDto editDto = ReturnObject.SalesOrder;

            editDto.SoDate = DateTime.Today;
            editDto.DeliveryDateExpected = DateTime.Today;
            editDto.Remarks = "";

            List<SalesOrderDetailViewDto> editDetailDto = ReturnObject.SalesOrderDetail;

            var a = (from soDet in editDetailDto
                     join mat in _materialRepo.GetAll() on soDet.MaterialRefId equals mat.Id
                     //join supmat in _customermaterialRepo.GetAll().Where(sp => sp.CustomerRefId == editDto.CustomerRefId)
                     //on soDet.MaterialRefId equals supmat.MaterialRefId
                     select new SalesOrderDetailViewDto
                     {
                         SoRefId = (int)soDet.SoRefId,
                         MaterialRefId = soDet.MaterialRefId,
                         MaterialRefName = mat.MaterialName,
                         QtyOrdered = soDet.QtyOrdered,
                         Price = soDet.Price,
                         DiscountOption = soDet.DiscountOption,
                         DiscountValue = soDet.DiscountValue,
                         TaxAmount = soDet.TaxAmount,
                         TotalAmount = soDet.TotalAmount,
                         UnitRefId = soDet.UnitRefId,
                         UnitRefName = soDet.UnitRefName,
                         Uom = soDet.Uom,
                         DefaultUnitId = mat.DefaultUnitId,
                         NetAmount = (soDet.QtyOrdered * soDet.Price),
                         SoStatus = soDet.SoStatus,
                         Remarks = soDet.Remarks,
                         TaxForMaterial = soDet.TaxForMaterial
                     });

            List<SalesOrderDetailViewDto> b = a.ToList();
			foreach (var pd in editDetailDto)
			{
				var cp = await _customermaterialRepo.FirstOrDefaultAsync(t => t.CustomerRefId == editDto.CustomerRefId && t.MaterialRefId == pd.MaterialRefId);
				if (cp!=null)
				{
					pd.Price = cp.MaterialPrice;
					pd.NetAmount = pd.QtyOrdered * pd.Price;
				}
			}


			return new GetSalesOrderForEditOutput
            {
                SalesOrder = editDto,
                SalesOrderDetail = b
            };
        }

        public async Task<List<MaterialVsCustomerDtos>> GetCustomerInfoForParticularMaterial(IdInput input)
        {
            List<MaterialVsCustomerDtos> output = new List<MaterialVsCustomerDtos>();

            return output;
        }

        public async Task<SetSalesOrderDashBoardDto> GetDashBoardSalesOrder(IdInput input)
        {
            string soStatus = "P";// L("Pending");

            var lstpending = _salesorderRepo.GetAll().Where(p => p.LocationRefId == input.Id && p.SoCurrentStatus.Equals(soStatus)).ToList();
            int pendingCount = lstpending.Count();

            soStatus = "A"; // L("Approved");
            var lstApproved = _salesorderRepo.GetAll().Where(p => p.LocationRefId == input.Id && p.ApprovedTime >= DateTime.Today
                    && (p.SoCurrentStatus.Equals(soStatus) || (p.SoCurrentStatus.Equals("H")))).ToList();
            int approvedCount = lstApproved.Count();


            var lstyetToReceived = _salesorderRepo.GetAll().Where(p => p.LocationRefId == input.Id && DbFunctions.TruncateTime(p.DeliveryDateExpected) < DateTime.Now && p.SoCurrentStatus.Equals(soStatus)).ToList();
            int yetToReceivedCount = lstyetToReceived.Count();

            soStatus = "X";// L("Declined");
            var lstDeclined = _salesorderRepo.GetAll().Where(p => p.LocationRefId == input.Id && p.ApprovedTime >= DateTime.Today && p.SoCurrentStatus.Equals(soStatus)).ToList();

            int declinedCount = lstDeclined.Count();


            return new SetSalesOrderDashBoardDto
            {
                TotalApproved = approvedCount,
                TotalPending = pendingCount,
                TotalYetToReceived = yetToReceivedCount,
                TotalDeclined = declinedCount
            };
        }


        public string Serialize(object dataToSerialize)
        {
            if (dataToSerialize == null) return null;

            using (StringWriter stringwriter = new System.IO.StringWriter())
            {
                var serializer = new XmlSerializer(dataToSerialize.GetType());
                serializer.Serialize(stringwriter, dataToSerialize);
                return stringwriter.ToString();
            }

        }

        public static string SerializeToJSON(object dataToSerialize)
        {
            if (dataToSerialize == null) return null;

            MemoryStream stream1 = new MemoryStream();
            DataContractJsonSerializer ser = new DataContractJsonSerializer(dataToSerialize.GetType());
            ser.WriteObject(stream1, dataToSerialize);

            stream1.Position = 0;
            StreamReader sr = new StreamReader(stream1);

            return sr.ReadToEnd();
        }

        public static T DeSerializeFromJSON<T>(string jsonText)
        {
            if (String.IsNullOrWhiteSpace(jsonText)) return default(T);

            //MemoryStream stream1 = new MemoryStream();
            MemoryStream stream1 = new MemoryStream(Encoding.UTF8.GetBytes(jsonText));

            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            //  ser.WriteObject(stream1, dataToSerialize);
            //stream1.WriteByte(jsonText);
            stream1.Position = 0;
            // ser.ReadObject(stream1);

            //StreamReader sr = new StreamReader(stream1);

            return (T)ser.ReadObject(stream1);
        }


        public static T Deserialize<T>(string xmlText)
        {
            if (String.IsNullOrWhiteSpace(xmlText)) return default(T);

            using (StringReader stringReader = new System.IO.StringReader(xmlText))
            {
                var serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(stringReader);
            }
        }



        //public async Task<string> GetSalesOrderReportInNotePad(GetSalesOrderInput input)
        //{

        //    var soMas = _salesorderRepo.FirstOrDefault(t => t.Id == input.SalesOrderId);
        //    if (soMas == null)
        //    {
        //        return "";
        //    }

        //    List<SalesOrderDetailViewDto> editDetailDto;

        //    editDetailDto = await (from soDet in _salesorderdetailRepo.GetAll()
        //                                .Where(a => a.SoRefId == input.SalesOrderId)
        //                           join mat in _materialRepo.GetAll()
        //                           on soDet.MaterialRefId equals mat.Id
        //                           join un in _unitRepo.GetAll()
        //                           on mat.DefaultUnitId equals un.Id
        //                           join udet in _unitRepo.GetAll()
        //                            on mat.DefaultUnitId equals udet.Id
        //                           select new SalesOrderDetailViewDto
        //                           {
        //                               SoRefId = (int)soDet.SoRefId,
        //                               MaterialRefId = soDet.MaterialRefId,
        //                               MaterialRefName = mat.MaterialName,
        //                               QtyOrdered = soDet.QtyOrdered,
        //                               QtyReceived = soDet.QtyReceived,
        //                               UnitRefId = soDet.UnitRefId,
        //                               UnitRefName = udet.Name,
        //                               Price = soDet.Price,
        //                               DiscountOption = soDet.DiscountOption,
        //                               DiscountValue = soDet.DiscountValue,
        //                               TotalAmount = soDet.TotalAmount,
        //                               TaxAmount = soDet.TaxAmount,
        //                               NetAmount = soDet.NetAmount,
        //                               SoStatus = soDet.SoStatus,
        //                               Remarks = soDet.Remarks,
        //                               Uom = un.Name,
        //                               DefaultUnitId = mat.DefaultUnitId,

        //                           }).ToListAsync();

        //    var customer = _customerRepo.FirstOrDefault(t => t.Id == soMas.CustomerRefId);

        //    var billingLocation = _locationRepo.FirstOrDefault(t => t.Id == soMas.LocationRefId);
        //    var shippingLocation = _locationRepo.FirstOrDefault(t => t.Id == soMas.ShippingLocationId);

        //    TextFileWriter fp = new TextFileWriter();

        //    string fileName = System.Web.HttpContext.Current.Server.MapPath("~/Report/" + soMas.SoReferenceCode.Replace("/", "") + "SoReport.txt");

        //    int PageLenth = 80;

        //    fp.FileOpen(fileName, "O");
        //    fp.PrintTextLine(fp.PrintFormatCenter(L("PURCHASEORDER"), PageLenth));
        //    fp.PrintText(fp.PrintFormatLeft(L("SoReference") + ": " + soMas.SoReferenceCode, 63));
        //    fp.PrintTextLine(L("Date") + ": " + soMas.SoDate.ToString("dd-MMM-yyyy"));
        //    fp.PrintTextLine("");

        //    fp.PrintSepLine('=', PageLenth);

        //    fp.PrintTextLine("");
        //    fp.PrintTextLine(L("Customer"));
        //    fp.PrintTextLine("~~~~~~~~~~");

        //    if (customer.CustomerName.Length > 0)
        //        fp.PrintTextLine(customer.CustomerName);
        //    if (customer.Address1 != null)
        //        fp.PrintTextLine(customer.Address1);
        //    if (customer.Address2 != null)
        //        fp.PrintTextLine(customer.Address2);
        //    if (customer.Address3 != null && customer.Address3.Length > 0)
        //        fp.PrintTextLine(customer.Address3);
        //    if (customer.City != null)
        //        fp.PrintTextLine(customer.City);
        //    if (customer.State != null)
        //        fp.PrintTextLine(customer.State);

        //    fp.PrintTextLine("");
        //    fp.PrintSepLine('=', PageLenth);

        //    fp.PrintText(fp.PrintFormatLeft(L("BillingAddress"), 40));
        //    fp.PrintTextLine(fp.PrintFormatLeft(L("ShippingAddress"), 40));
        //    if (billingLocation.Name != null)
        //    {
        //        fp.PrintText(fp.PrintFormatLeft(billingLocation.Name, 40));
        //    }
        //    if (shippingLocation.Name != null)
        //    {
        //        fp.PrintText(fp.PrintFormatLeft(shippingLocation.Name, 40));
        //    }
        //    fp.PrintTextLine("");

        //    if (billingLocation.Address1 != null)
        //    {
        //        fp.PrintText(fp.PrintFormatLeft(billingLocation.Address1, 40));
        //    }
        //    if (shippingLocation.Address1 != null)
        //    {
        //        fp.PrintText(fp.PrintFormatLeft(shippingLocation.Address1, 40));
        //    }
        //    fp.PrintTextLine("");

        //    if (billingLocation.Address2 != null)
        //    {
        //        fp.PrintText(fp.PrintFormatLeft(billingLocation.Address2, 40));
        //    }
        //    if (shippingLocation.Address2 != null)
        //    {
        //        fp.PrintText(fp.PrintFormatLeft(shippingLocation.Address2, 40));
        //    }
        //    fp.PrintTextLine("");

        //    if (billingLocation.Address3 != null)
        //    {
        //        fp.PrintText(fp.PrintFormatLeft(billingLocation.Address3, 40));
        //    }
        //    if (shippingLocation.Address3 != null)
        //    {
        //        fp.PrintText(fp.PrintFormatLeft(shippingLocation.Address3, 40));
        //    }
        //    fp.PrintTextLine("");

        //    if (billingLocation.City != null)
        //    {
        //        fp.PrintText(fp.PrintFormatLeft(billingLocation.City, 40));
        //    }
        //    if (shippingLocation.City != null)
        //    {
        //        fp.PrintText(fp.PrintFormatLeft(shippingLocation.City, 40));
        //    }
        //    fp.PrintTextLine("");

        //    fp.PrintTextLine(L("SoDetailHeader"));

        //    fp.PrintSepLine('-', PageLenth);
        //    fp.PrintText(fp.PrintFormatLeft("#", 3));
        //    fp.PrintText(fp.PrintFormatLeft(L("Material"), 25));
        //    fp.PrintText(fp.PrintFormatRight(L("Qty"), 8));
        //    fp.PrintText(fp.PrintFormatRight(L("Unit"), 5));
        //    fp.PrintText(fp.PrintFormatRight(L("Cost"), 9));
        //    fp.PrintText(fp.PrintFormatRight(L("Total"), 10));
        //    fp.PrintText(fp.PrintFormatRight(L("Tax"), 9));
        //    fp.PrintText(fp.PrintFormatRight(L("LineTotal"), 11));
        //    fp.PrintTextLine("");
        //    fp.PrintSepLine('-', PageLenth);

        //    int loopIndex = 1;
        //    decimal TotalTaxAmount = 0m;
        //    decimal TotalAmount = 0m;

        //    foreach (var det in editDetailDto)
        //    {
        //        fp.PrintText(fp.PrintFormatLeft(loopIndex.ToString(), 3));
        //        fp.PrintText(fp.PrintFormatLeft(det.MaterialRefName, 25));
        //        fp.PrintText(fp.PrintFormatRight(det.QtyOrdered.ToString("G"), 8));
        //        fp.PrintText(fp.PrintFormatRight(det.Uom, 5));
        //        fp.PrintText(fp.PrintFormatRight(det.Price.ToString("G"), 9));
        //        fp.PrintText(fp.PrintFormatRight(det.TotalAmount.ToString("G"), 10));
        //        fp.PrintText(fp.PrintFormatRight(det.TaxAmount.ToString("G"), 9));
        //        fp.PrintText(fp.PrintFormatRight(det.NetAmount.ToString("G"), 11));
        //        fp.PrintTextLine("");
        //        loopIndex++;
        //        TotalTaxAmount = TotalTaxAmount + det.TaxAmount;
        //        TotalAmount = TotalAmount + det.TotalAmount;
        //    }

        //    fp.PrintTextLine(" ");
        //    fp.PrintSepLine('-', PageLenth);
        //    fp.PrintText(fp.PrintFormatLeft(L("SubTotal"), 51));
        //    fp.PrintText(fp.PrintFormatRight(TotalAmount.ToString("G"), 9));
        //    fp.PrintText(fp.PrintFormatRight(TotalTaxAmount.ToString("G"), 9));
        //    fp.PrintText(fp.PrintFormatRight(soMas.SoNetAmount.ToString("G"), 11));
        //    fp.PrintTextLine("");
        //    fp.PrintSepLine('-', PageLenth);
        //    fp.PrintTextLine(" ");
        //    fp.PrintTextLine(L("TermsConditions"));
        //    fp.PrintTextLine("------------------------------");
        //    fp.PrintTextLine(L("Term1"));
        //    fp.PrintTextLine(L("Term2"));
        //    fp.PrintTextLine(L("Term3"));
        //    fp.PrintTextLine(L("Term4", soMas.DeliveryDateExpected.ToString("dd-MMM-yyyy")));
        //    fp.PrintTextLine(" "); fp.PrintTextLine(" ");
        //    fp.PrintSepLine('-', PageLenth);
        //    fp.PrintTextLine(fp.PrintFormatCenter("REPORT GENERATED AT " + DateTime.Now.ToString("f"), PageLenth));
        //    fp.PrintSepLine('-', PageLenth);
        //    fp.PrintTextLine(" ");

        //    //Process.Start("NOTEPAD.EXE ", fileName);

        //    //string retText = System.IO.File.ReadAllText(fileName);

        //    //return retText;
        //}

        public async Task SendSalesOrderEmail(SetSalesOrderPrint input)
        {
            GetSalesOrderForEditOutput output = await GetSalesOrderForEdit(new NullableIdInput { Id = input.SalesOrderId });

            SalesOrderEditDto soMas;

            if (output.SalesOrder.Id.HasValue)
            {
                soMas = output.SalesOrder;
            }
            else
            {
                return;
            }

            int customerRefId = soMas.CustomerRefId;

            var customer = _customerRepo.FirstOrDefault(t => t.Id == customerRefId);

            string customerEmail = customer.Email;

            if (customerEmail.IsNullOrEmpty())
            {
                throw new UserFriendlyException(L("CustomerEmailErr"));
            }

            string customerName = customer.CustomerName;
            string customerAddress1 = customer.Address1;
            string customerAddress2 = customer.Address2;
            string customerAddress3 = customer.Address3;
            string customerCity = customer.City;
            string customerState = customer.State;
            string customerCountry = customer.Country;




            var billingLocation = _locationRepo.FirstOrDefault(t => t.Id == soMas.LocationRefId);
            string billingLocationName = billingLocation.Name;
            string billingLocationAddress1 = billingLocation.Address1;
            string billingLocationAddress2 = billingLocation.Address2;
            string billingLocationAddress3 = billingLocation.Address3;
            string billingLocationCity = billingLocation.City;
            string billingLocationState = billingLocation.State;
            string billingLocationCountry = billingLocation.Country;

            var companyInfo = _companyRepo.FirstOrDefault(t => t.Id == billingLocation.CompanyRefId);
            string companyName = companyInfo.Name;
            string companyAddress1 = companyInfo.Address1;
            string companyAddress2 = companyInfo.Address2;
            string companyAddress3 = companyInfo.Address3;
            string companyCity = companyInfo.City;

            var shippingLocation = _locationRepo.FirstOrDefault(t => t.Id == soMas.ShippingLocationId);

            string shippingLocationName = "";
            string shippingLocationAddress1 = "";
            string shippingLocationAddress2 = "";
            string shippingLocationAddress3 = "";
            string shippingLocationCity = "";
            string shippingLocationState = "";
            string shippingLocationCountry = "";

            if (soMas.LocationRefId == soMas.ShippingLocationId)
            {
                shippingLocationName = L("SameAsBillingAddress");
            }
            else
            {
                shippingLocationName = shippingLocation.Name;
                shippingLocationAddress1 = shippingLocation.Address1;
                shippingLocationAddress2 = shippingLocation.Address2;
                shippingLocationAddress3 = shippingLocation.Address3;
                shippingLocationCity = shippingLocation.City;
                shippingLocationState = shippingLocation.State;
                shippingLocationCountry = shippingLocation.Country;
            }


            var mailMessage = new StringBuilder();

            string htmlLine;
            htmlLine = "<!DOCTYPE HTML PUBLIC \" -//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">";
            mailMessage.AppendLine(htmlLine);
            htmlLine = "<html style=\"-webkit-text-size-adjust: none;-ms-text-size-adjust: none;\">";
            mailMessage.AppendLine(htmlLine);
            htmlLine = "<head>";
            mailMessage.AppendLine(htmlLine);
            htmlLine = "<meta http-equiv=\"Content - Type\" content=\"text / html; charset = utf - 8\">";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<title>" + companyName + L("SalesOrder") + "</title>";
            mailMessage.AppendLine(htmlLine);
            htmlLine = "</head>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<body style=\"padding: 0px; margin: 0px; \">";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<div id=\"mailsub\" class=\"notification\">";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<center>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<h1>" + companyName + "</h1>";
            mailMessage.AppendLine(htmlLine);

            if (!companyAddress1.IsNullOrEmpty())
            {
                htmlLine = "<p>" + companyAddress1 + "</p>";
                mailMessage.AppendLine(htmlLine);
            }

            if (!companyAddress2.IsNullOrEmpty())
            {
                htmlLine = "<p>" + companyAddress2 + "</p>";
                mailMessage.AppendLine(htmlLine);
            }

            if (!companyAddress3.IsNullOrEmpty())
            {
                htmlLine = "<p>" + companyAddress3 + "</p>";
                mailMessage.AppendLine(htmlLine);
            }

            if (!companyCity.IsNullOrEmpty())
            {
                htmlLine = "<p>" + companyCity + "</p>";
                mailMessage.AppendLine(htmlLine);
            }

            htmlLine = "</center>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<hr />";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<h2 style=\"text-align: center;\">" + L("SalesOrder") + "</h2>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<table id=\"header\" style=\"width: 100%;\">";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<tbody>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<tr>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<td><strong><strong>" + L("SoReference") + ": " + " </strong></strong><strong>" + soMas.SoReferenceCode + "</strong></td>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<td style=\"text-align: right;\"><strong>" + L("Date") + ": " + soMas.SoDate.ToString("dd-MMM-yy") + "</strong></td>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "</tr>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "</tbody>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "</table>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<hr/>";
            mailMessage.AppendLine(htmlLine);


            htmlLine = "<h3><span style=\"text-decoration: underline;\"><strong>" + L("Customer") + "</strong></span></h3>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<p>" + customerName + "</p>";
            mailMessage.AppendLine(htmlLine);

            if (!customerAddress1.IsNullOrEmpty())
            {
                htmlLine = "<p>" + customerAddress1 + "</p>";
                mailMessage.AppendLine(htmlLine);
            }

            if (!customerAddress2.IsNullOrEmpty())
            {
                htmlLine = "<p>" + customerAddress2 + "</p>";
                mailMessage.AppendLine(htmlLine);
            }

            if (!customerAddress3.IsNullOrEmpty())
            {
                htmlLine = "<p>" + customerAddress3 + "</p>";
                mailMessage.AppendLine(htmlLine);
            }

            if (!customerCity.IsNullOrEmpty())
            {
                htmlLine = "<p>" + customerCity + "</p>";
                mailMessage.AppendLine(htmlLine);
            }

            if (!customerState.IsNullOrEmpty())
            {
                htmlLine = "<p>" + customerState + "</p>";
                mailMessage.AppendLine(htmlLine);
            }

            if (!customerCountry.IsNullOrEmpty())
            {
                htmlLine = "<p>" + customerCountry + "</p>";
                mailMessage.AppendLine(htmlLine);
            }

            htmlLine = "<hr />";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<table id=\"address\" width=\"100%\">";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<tbody>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<tr>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<td><strong><u>" + L("BillingAddress") + "</u></strong></td>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<td><strong><u>" + L("ShippingAddress") + "</u></strong></td>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "</tr>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<tr>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<td>" + billingLocationName + "</td>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<td>" + shippingLocationName + "</td>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "</tr>";
            mailMessage.AppendLine(htmlLine);

            if (billingLocationAddress1.IsNullOrEmpty() == false || shippingLocationAddress1.IsNullOrEmpty() == false)
            {
                htmlLine = "<tr>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td>" + billingLocationAddress1 + " </td>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td>" + shippingLocationAddress1 + "</td>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "</tr>";
                mailMessage.AppendLine(htmlLine);
            }


            if (billingLocationAddress2.IsNullOrEmpty() == false || shippingLocationAddress2.IsNullOrEmpty() == false)
            {
                htmlLine = "<tr>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td>" + billingLocationAddress2 + " </td>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td>" + shippingLocationAddress2 + "</td>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "</tr>";
                mailMessage.AppendLine(htmlLine);
            }


            if (billingLocationAddress3.IsNullOrEmpty() == false || shippingLocationAddress3.IsNullOrEmpty() == false)
            {
                htmlLine = "<tr>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td>" + billingLocationAddress3 + " </td>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td>" + shippingLocationAddress2 + "</td>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "</tr>";
                mailMessage.AppendLine(htmlLine);
            }


            if (billingLocationCity.IsNullOrEmpty() == false || shippingLocationCity.IsNullOrEmpty() == false)
            {
                htmlLine = "<tr>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td>" + billingLocationCity + " </td>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td>" + shippingLocationCity + "</td>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "</tr>";
                mailMessage.AppendLine(htmlLine);
            }


            if (billingLocationState.IsNullOrEmpty() == false || shippingLocationState.IsNullOrEmpty() == false)
            {
                htmlLine = "<tr>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td>" + billingLocationState + " </td>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td>" + shippingLocationState + "</td>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "</tr>";
                mailMessage.AppendLine(htmlLine);
            }



            htmlLine = "</tbody>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "</table>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<hr />";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<p>" + L("SoDetailHeader") + " </p>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<table id=\"detail\" cellpadding=\"7\" border=\"1\" width=\"100%\">";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<tbody>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<tr>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<td>#.Material</td>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<td style=\"text-align: right;\">Qty</td>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<td style=\"text-align: right;\">UOM</td>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<td style=\"text-align: right;\">Cost</td>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<td style=\"text-align: right;\">Total</td>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<td style=\"text-align: right;\">Tax</td>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<td style=\"text-align: right;\">Line Total</td>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "</tr>";
            mailMessage.AppendLine(htmlLine);

            int sno = 1;
            decimal subTotalAmount = 0;
            decimal subTaxAmount = 0;
            foreach (var soDet in output.SalesOrderDetail)
            {
                htmlLine = "<tr>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td>" + sno.ToString().PadLeft(2) + "." + soDet.MaterialRefName + "</td>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td style=\"text-align: right;\">" + soDet.QtyOrdered + "</td>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td style=\"text-align: right;\">" + soDet.Uom + "</td>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td style=\"text-align: right;\">" + soDet.Price.ToString("###0.00") + " </td>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td style=\"text-align: right;\">" + soDet.TotalAmount.ToString("###0.00") + "</td>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td style=\"text-align: right;\">" + soDet.TaxAmount.ToString("###0.00") + "</td>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<td style=\"text-align: right;\">" + soDet.NetAmount.ToString("###0.00") + "</td>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "</tr>";
                mailMessage.AppendLine(htmlLine);

                subTaxAmount = subTaxAmount + soDet.TaxAmount;
                subTotalAmount = subTotalAmount + soDet.TotalAmount;
                sno++;
            }

            htmlLine = "<tr>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<td style=\"text-align: right;\">" + L("SubTotal") + "</td>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<td></td>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<td></td>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<td></td>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<td style=\"text-align: right;\">" + subTotalAmount.ToString("####0.00") + "</td>";
            mailMessage.AppendLine(htmlLine);


            htmlLine = "<td style=\"text-align: right;\">" + subTaxAmount.ToString("####0.00") + "</td>";
            mailMessage.AppendLine(htmlLine);


            htmlLine = "<td style=\"text-align: right;\">" + soMas.SoNetAmount.ToString("####0.00") + "</td>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "</tr>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "</tbody>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "</table>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<hr />";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<h3><strong>" + L("TermsConditions") + "</strong></h3>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<ul style=\"list - style - type: circle; \">";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<li> <b>" + L("TermDelivery", soMas.DeliveryDateExpected.ToString("dd-MMM-yy")) + "</b></li>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<li>" + L("Term1") + "</li>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<li>" + L("Term2") + "</li>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<li>" + L("Term3") + "</li>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "</ul>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<hr />";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<p><span style=\"color: #0000ff;\">" + L("SignNotRequired") + "</span></p>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "</div>";    //mailsub div end
            mailMessage.AppendLine(htmlLine);
            htmlLine = "</body>";
            mailMessage.AppendLine(htmlLine);
            htmlLine = "</html>";
            mailMessage.AppendLine(htmlLine);

            try
            {
                await _emailSender.SendAsync(customerEmail, companyName + " - " + L("SalesOrder"), mailMessage.ToString());
            }
            catch (Exception ex)
            {

                throw new UserFriendlyException(ex.Message + " " + ex.InnerException);
            }


        }



    }





}