﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Transaction
{
    public interface IYieldAppService : IApplicationService
    {
        Task<PagedResultOutput<YieldListDto>> GetAll(GetYieldInput inputDto);
        Task<FileDto> GetAllToExcel(GetYieldInput inputDto);
        Task<GetYieldForEditOutput> GetYieldForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateYield(CreateOrUpdateYieldInput input);
        Task DeleteYield(IdInput input);
        Task<GetYieldForEditOutput> GetTemplateObjectForEdit(GetObjectFromString input);
        Task CreateDraft(CreateOrUpdateYieldInput input);
        Task<SetYieldDashBoardDto> GetDashBoardYield(IdInput input);
    }
}

