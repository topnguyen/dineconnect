﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.House.Master;
using System;
using DinePlan.DineConnect.Connect.DayClose.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using Abp.Configuration;
using DinePlan.DineConnect.Configuration;

namespace DinePlan.DineConnect.House.Transaction.Implementation
{
    public class AdjustmentAppService : DineConnectAppServiceBase, IAdjustmentAppService
    {

        private readonly IAdjustmentListExcelExporter _adjustmentExporter;
        private readonly IAdjustmentManager _adjustmentManager;
        private readonly IRepository<Adjustment> _adjustmentRepo;
        private readonly IRepository<AdjustmentDetail> _adjustmentDetailRepo;
        private readonly IRepository<MaterialGroupCategory> _materialGroupCategoryRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<Unit> _unitRepo;
        private readonly IMaterialAppService _materialAppService;
        private readonly IHouseReportAppService _housereportAppService;
        private readonly IRepository<InterTransfer> _intertransferRepo;
        private readonly IRepository<ClosingStock> _closingstockRepo;
        private readonly IUnitConversionAppService _unitConversionAppService;
        //private readonly ILocationAppService _locationAppService;
        private int roundDecimals = 2;
        private SettingManager _settingManager;

        public AdjustmentAppService(IAdjustmentManager adjustmentManager,
              IRepository<Adjustment> adjustmentRepo,
              IAdjustmentListExcelExporter adjustmentExporter,
              IRepository<AdjustmentDetail> adjustmentDetailRepo,
              IRepository<Material> materialRepo,
              IRepository<Location> locationRepo,
              IRepository<Unit> unitRepo,
              IMaterialAppService materialAppService,
              IHouseReportAppService housereportAppService,
              IRepository<InterTransfer> intertransferRepo,
              IRepository<ClosingStock> closingstockRepo,
           IUnitConversionAppService unitConversionAppService,
           IRepository<MaterialGroupCategory> materialGroupCategoryRepo,
           SettingManager settingManager
           //ILocationAppService locationAppService
           )
        {
            _settingManager = settingManager;
            _adjustmentManager = adjustmentManager;
            _adjustmentRepo = adjustmentRepo;
            _adjustmentDetailRepo = adjustmentDetailRepo;
            _adjustmentExporter = adjustmentExporter;
            _materialRepo = materialRepo;
            _locationRepo = locationRepo;
            _materialAppService = materialAppService;
            _unitRepo = unitRepo;
            _housereportAppService = housereportAppService;
            _intertransferRepo = intertransferRepo;
            _closingstockRepo = closingstockRepo;
            _unitConversionAppService = unitConversionAppService;
            _materialGroupCategoryRepo = materialGroupCategoryRepo;
            roundDecimals = _settingManager.GetSettingValue<int>(AppSettings.HouseSettings.Decimals);
            //_locationAppService = locationAppService;
        }

        public async Task<PagedResultOutput<AdjustmentListDto>> GetAll(GetAdjustmentInput input)
        {
            var viewItems = (from adjv in _adjustmentRepo.GetAll()
                             join l in _locationRepo.GetAll().Where(l => l.Id == input.LocationRefId) on
                             adjv.LocationRefId equals l.Id
                             select new AdjustmentViewDto()
                             {
                                 LocationRefName = l.Name,
                                 AdjustmentDate = adjv.AdjustmentDate,
                                 TokenRefNumber = adjv.TokenRefNumber,
                             });


            var allItems = _adjustmentRepo.GetAll();

            if (input.Operation == "SEARCH")
            {
                allItems = _adjustmentRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Id.Equals(input.Filter)
               );
            }
            else
            {
                allItems = _adjustmentRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Id.ToString().Contains(input.Filter)
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<AdjustmentListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<AdjustmentListDto>(
                allItemCount,
                allListDtos
                );
        }
        public async Task<PagedResultOutput<AdjustmentViewDto>> GetView(GetAdjustmentInput input)
        {

            var rsAdj = _adjustmentRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId).WhereIf(!input.Filter.IsNullOrEmpty(), t => t.TokenRefNumber.ToString().Contains(input.Filter));

            if (!input.Filter.IsNullOrEmpty())
            {
                if (input.Filter == "888" || input.Filter == "777" || input.Filter == "999" || input.Filter == "555" || input.Filter == "666")
                {
                    rsAdj = rsAdj.Where(t => t.TokenRefNumber.ToString() == input.Filter);
                }
            }


            if (input.StartDate != null && input.EndDate != null)
            {
                rsAdj = rsAdj.Where(t => t.AdjustmentDate >= input.StartDate && t.AdjustmentDate <= input.EndDate);
            }

            var allItems = (from adjv in rsAdj
                            join l in _locationRepo.GetAll() on
                             adjv.LocationRefId equals l.Id
                            select new AdjustmentViewDto()
                            {
                                Id = adjv.Id,
                                LocationRefName = l.Name,
                                AdjustmentDate = adjv.AdjustmentDate,
                                TokenRefNumber = adjv.TokenRefNumber,
                                CreationTime = adjv.CreationTime,
                                AdjustmentRemarks = adjv.AdjustmentRemarks,
                                ClosingStockDate = adjv.ClosingStockDate
                            }

             );


            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<AdjustmentViewDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<AdjustmentViewDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetAdjustmentInput input)
        {
            input.MaxResultCount = AppConsts.MaxPageSize;

            var allList = await GetView(input);
            var allListDtos = allList.Items.MapTo<List<AdjustmentViewDto>>();
            return _adjustmentExporter.ExportToFile(allListDtos);
        }

        public async Task<GetAdjustmentForEditOutput> GetAdjustmentForEdit(NullableIdInput input)
        {
            AdjustmentEditDto editDto;
            List<AdjustmentDetailViewDto> editDetailDto;

            if (input.Id.HasValue)
            {
                var hDto = await _adjustmentRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<AdjustmentEditDto>();

                editDetailDto = await (from adjDet in _adjustmentDetailRepo.GetAll()
                                       .Where(a => a.AdjustmentRefIf == input.Id.Value)
                                       join mat in _materialRepo.GetAll()
                                       on adjDet.MaterialRefId equals mat.Id
                                       join un in _unitRepo.GetAll() on mat.DefaultUnitId equals un.Id
                                       join uiss in _unitRepo.GetAll() on adjDet.UnitRefId equals uiss.Id
                                       join matcat in _materialGroupCategoryRepo.GetAll() on mat.MaterialGroupCategoryRefId equals matcat.Id
                                       select new AdjustmentDetailViewDto
                                       {
                                           Id = adjDet.Id,
                                           AdjustmentRefIf = adjDet.AdjustmentRefIf,
                                           MaterialRefId = adjDet.MaterialRefId,
                                           MaterialPetName = mat.MaterialPetName,
                                           MaterialRefName = mat.MaterialName,
                                           MaterialGroupCategoryName = matcat.MaterialGroupCategoryName,
                                           AdjustmentQty = adjDet.AdjustmentQty,
                                           ClosingStock = adjDet.ClosingStock,
                                           StockEntry = adjDet.StockEntry,
                                           AdjustmentMode = adjDet.AdjustmentMode,
                                           AdjustmentApprovedRemarks = adjDet.AdjustmentApprovedRemarks,
                                           Sno = adjDet.Sno,
                                           DefaultUnitId = mat.DefaultUnitId,
                                           DefaultUnitName = un.Name,
                                           UnitRefId = adjDet.UnitRefId,
                                           UnitRefName = uiss.Name,
                                           Price = 0,
                                           AdjustmentCost = 0
                                       }).ToListAsync();

                var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == editDto.LocationRefId);
                editDto.LocationRefName = loc.Name;

                //if (editDto.TokenRefNumber==888 || editDto.TokenRefNumber==999 )
                //{
                List<LocationListDto> locs = new List<LocationListDto>();
                locs.Add(loc.MapTo<LocationListDto>());

                //var locs = _locationRepo.GetAll().Where(t => t.Id == editDto.LocationRefId).ToList().MapTo<List<LocationListDto>>();
                List<int> arrMaterialRefIds = editDetailDto.Select(t => t.MaterialRefId).ToList();

                var matRateView = await _housereportAppService.GetMaterialRateView(new GetHouseReportMaterialRateInput
                {
                    StartDate = editDto.AdjustmentDate,
                    EndDate = editDto.AdjustmentDate,
                    Locations = locs,
                    MaterialRefIds = arrMaterialRefIds,
                    FunctionCalledBy = "Adjustment For Edit"
                });

                var rateList = matRateView.MaterialRateView;
                var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();

                foreach (var lst in editDetailDto)
                {
                    var mat = rateList.FirstOrDefault(t => t.MaterialRefId == lst.MaterialRefId);
                    if (mat == null)
                        lst.Price = 0;
                    else
                    {
                        if (lst.UnitRefId == mat.DefaultUnitId)
                            lst.Price = Math.Round(mat.AvgRate, roundDecimals);
                        else
                        {
                            var unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == lst.UnitRefId && t.RefUnitId == mat.DefaultUnitId);
                            if (unitConversion == null)
                                throw new UserFriendlyException(L("UnitConversion") + " " + L("NotExist") + " - " + mat.MaterialName);
                            lst.Price = unitConversion.Conversion * Math.Round(mat.AvgRate, roundDecimals);
                        }
                    }

                    lst.AdjustmentCost = Math.Round((decimal)lst.Price * lst.AdjustmentQty, roundDecimals);
                    if (lst.AdjustmentMode.Equals(L("Excess")))
                        lst.AdjustmentCost = -1 * lst.AdjustmentCost;
                    else if (lst.AdjustmentMode.Equals(L("Shortage")))
                        lst.AdjustmentCost = 1 * lst.AdjustmentCost;
                    else if (lst.AdjustmentMode.Equals(L("Wastage")))
                        lst.AdjustmentCost = 1 * lst.AdjustmentCost;
                    else if (lst.AdjustmentMode.Equals(L("Damaged")))
                        lst.AdjustmentCost = 1 * lst.AdjustmentCost;

                    lst.ClosingStockValue = lst.ClosingStock * lst.Price;
                    lst.StockEntryValue = lst.StockEntry * lst.Price;
                }
                editDto.TotalAdjustmentCost = (decimal)editDetailDto.Sum(t => t.AdjustmentCost);
                editDto.TotalExcessCost = (decimal)editDetailDto.Where(t => t.AdjustmentCost < 0).Sum(t => t.AdjustmentCost);
                editDto.TotalShortageCost = (decimal)editDetailDto.Where(t => t.AdjustmentCost > 0).Sum(t => t.AdjustmentCost);
                editDto.TotalClosingStockValue = (decimal)editDetailDto.Sum(t => t.ClosingStockValue);
                editDto.TotalStockEntryValue = (decimal)editDetailDto.Sum(t => t.StockEntryValue);
            }
            else
            {
                editDto = new AdjustmentEditDto();
                editDetailDto = new List<AdjustmentDetailViewDto>();
            }

            return new GetAdjustmentForEditOutput
            {
                Adjustment = editDto,
                AdjustmentDetail = editDetailDto
            };
        }

        public async Task<IdInput> CreateOrUpdateAdjustment(CreateOrUpdateAdjustmentInput input)
        {
            if (input.Adjustment.Id.HasValue)
            {
                return await UpdateAdjustment(input);
            }
            else
            {
                return await CreateAdjustment(input);
            }
        }

        public async Task DeleteAdjustment(IdInput input)
        {

            var isAutoAdjustmentForIntertransfer = await _intertransferRepo.FirstOrDefaultAsync(t => t.AdjustmentRefId == input.Id);
            if (isAutoAdjustmentForIntertransfer != null)
            {
                throw new UserFriendlyException(L("AutoAdjustmentDeleteErrorForInterTransfer", isAutoAdjustmentForIntertransfer.Id));
            }

            var rsAdjMas = await _adjustmentRepo.FirstOrDefaultAsync(t => t.Id == input.Id);
            if (rsAdjMas == null)
            {
                throw new UserFriendlyException(L("Adjustment") + " " + input.Id + " " + L("NotExist"));
            }

            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == rsAdjMas.LocationRefId);

            var houseTransactionDate = loc.HouseTransactionDate;

            if (houseTransactionDate == null)
            {
                throw new UserFriendlyException(L("DayClose") + " " + L("Pending"));
            }

            if (houseTransactionDate.Value.Date != rsAdjMas.AdjustmentDate.Date)
            {
                throw new UserFriendlyException(L("AdjustmentDeleteError", loc.HouseTransactionDate.Value.ToString("yyyy-MMM-dd"), rsAdjMas.AdjustmentDate.Date.ToString("yyyy-MMM-dd"), rsAdjMas.Id, rsAdjMas.TokenRefNumber));
            }

            var adjustmentDetail = await _adjustmentDetailRepo.GetAll().Where(t => t.AdjustmentRefIf == input.Id).ToListAsync();
            foreach (var det in adjustmentDetail)
            {
                MaterialLedgerDto ml = new MaterialLedgerDto();
                ml.LocationRefId = rsAdjMas.LocationRefId;
                ml.MaterialRefId = det.MaterialRefId;
                ml.LedgerDate = rsAdjMas.AdjustmentDate;

                if (det.AdjustmentMode.Equals(L("Excess")))
                    ml.ExcessReceived = -1 * det.AdjustmentQty;
                else if (det.AdjustmentMode.Equals(L("Shortage")))
                    ml.Shortage = -1 * det.AdjustmentQty;
                else if (det.AdjustmentMode.Equals(L("Wastage")))
                    ml.Shortage = -1 * det.AdjustmentQty;
                else if (det.AdjustmentMode.Equals(L("Damaged")))
                    ml.Damaged = -1 * det.AdjustmentQty;


                ml.UnitId = det.UnitRefId;

                var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                det.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;

                await _adjustmentDetailRepo.DeleteAsync(det.Id);
            }

            await _adjustmentRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task<IdInput> UpdateAdjustment(CreateOrUpdateAdjustmentInput input)
        {
            var item = await _adjustmentRepo.GetAsync(input.Adjustment.Id.Value);
            var dto = input.Adjustment;

            item.LocationRefId = dto.LocationRefId;
            item.AdjustmentDate = dto.AdjustmentDate;
            item.AdjustmentRemarks = dto.AdjustmentRemarks;
            item.ClosingStockDate = dto.ClosingStockDate;
            item.TokenRefNumber = dto.TokenRefNumber;

            //  Update Detail Link
            List<int> materialsToBeRetained = new List<int>();

            if (input.AdjustmentDetail != null && input.AdjustmentDetail.Count > 0)
            {
                foreach (var items in input.AdjustmentDetail)
                {
                    int materialrefid = items.MaterialRefId;
                    materialsToBeRetained.Add(materialrefid);

                    var existingDetailEntry = _adjustmentDetailRepo.GetAllList(u => u.AdjustmentRefIf == dto.Id && u.MaterialRefId.Equals(materialrefid));

                    if (existingDetailEntry.Count == 0)  //  Add New Material
                    {
                        AdjustmentDetail itdDto = new AdjustmentDetail();
                        if (dto.Id != null) itdDto.Id = (int)dto.Id;
                        itdDto.AdjustmentRefIf = items.AdjustmentRefIf;
                        itdDto.MaterialRefId = items.MaterialRefId;
                        itdDto.AdjustmentQty = items.AdjustmentQty;
                        itdDto.ClosingStock = items.ClosingStock;
                        itdDto.StockEntry = items.StockEntry;
                        itdDto.AdjustmentMode = items.AdjustmentMode;
                        itdDto.AdjustmentApprovedRemarks = items.AdjustmentApprovedRemarks;
                        itdDto.Sno = items.Sno;
                        itdDto.UnitRefId = items.UnitRefId;

                        var adjDetailId = await _adjustmentDetailRepo.InsertAndGetIdAsync(itdDto);
                    }
                    else
                    {
                        var itdDto = await _adjustmentDetailRepo.GetAsync(existingDetailEntry[0].Id);

                        if (dto.Id != null) itdDto.Id = (int)dto.Id;
                        itdDto.AdjustmentRefIf = items.AdjustmentRefIf;
                        itdDto.MaterialRefId = items.MaterialRefId;
                        itdDto.AdjustmentQty = items.AdjustmentQty;
                        itdDto.ClosingStock = items.ClosingStock;
                        itdDto.StockEntry = items.StockEntry;
                        itdDto.AdjustmentMode = items.AdjustmentMode;
                        itdDto.AdjustmentApprovedRemarks = items.AdjustmentApprovedRemarks;
                        itdDto.Sno = items.Sno;
                        itdDto.UnitRefId = items.UnitRefId;

                        var retId2 = await _adjustmentDetailRepo.InsertOrUpdateAndGetIdAsync(itdDto);

                    }
                }
            }
            var delBrandList = _adjustmentDetailRepo.GetAll().Where(a => a.AdjustmentRefIf == input.Adjustment.Id.Value && !materialsToBeRetained.Contains(a.MaterialRefId)).ToList();

            foreach (var a in delBrandList)
            {
                _adjustmentDetailRepo.Delete(a.Id);
            }

            CheckErrors(await _adjustmentManager.CreateSync(item));

            return new IdInput
            {
                Id = input.Adjustment.Id.Value
            };
        }

        protected virtual async Task<IdInput> CreateAdjustment(CreateOrUpdateAdjustmentInput input)
        {
            var dto = input.Adjustment.MapTo<Adjustment>();

            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == dto.LocationRefId);
            dto.AccountDate = loc.HouseTransactionDate.Value;

            var retId = await _adjustmentRepo.InsertOrUpdateAndGetIdAsync(dto);

            foreach (AdjustmentDetailViewDto items in input.AdjustmentDetail.ToList())
            {
                AdjustmentDetail itdDto = new AdjustmentDetail
                {
                    AdjustmentRefIf = retId,
                    MaterialRefId = items.MaterialRefId,
                    AdjustmentQty = items.AdjustmentQty,
                    ClosingStock = items.ClosingStock,
                    StockEntry = items.StockEntry,
                    AdjustmentMode = items.AdjustmentMode,
                    AdjustmentApprovedRemarks = items.AdjustmentApprovedRemarks,
                    Sno = items.Sno,
                    UnitRefId = items.UnitRefId
                };

               
                MaterialLedgerDto ml = new MaterialLedgerDto
                {
                    LocationRefId = dto.LocationRefId,
                    MaterialRefId = itdDto.MaterialRefId,
                    LedgerDate = dto.AdjustmentDate
                };

                if (input.ForceAdjustmentFlag)
                {
                    ml.ForceAdjustmentFlag = true;
                }
                else
                {
                    ml.ForceAdjustmentFlag = false;
                }

                if (itdDto.AdjustmentMode.Equals(L("Excess")))
                    ml.ExcessReceived = itdDto.AdjustmentQty;
                else if (itdDto.AdjustmentMode.Equals(L("Shortage")))
                    ml.Shortage = itdDto.AdjustmentQty;
                else if (itdDto.AdjustmentMode.Equals(L("Wastage")))
                    ml.Shortage = itdDto.AdjustmentQty;
                else if (itdDto.AdjustmentMode.Equals(L("Damaged")))
                    ml.Damaged = itdDto.AdjustmentQty;
                else if (itdDto.AdjustmentMode.Equals(L("Equal")))
                    ml.ExcessReceived = itdDto.AdjustmentQty;
                else
                    throw new UserFriendlyException(L("AdjustmentMode") + " " + L("Error") + itdDto.AdjustmentMode);
                ml.UnitId = itdDto.UnitRefId;

                var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                itdDto.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;

                await _adjustmentDetailRepo.InsertAndGetIdAsync(itdDto);
            }

            CheckErrors(await _adjustmentManager.CreateSync(dto));

            if (dto.TokenRefNumber == 888 && input.ClosingStockFlag == true)
            {
                var rsClstk = await _closingstockRepo.GetAllListAsync(t => t.StockDate == input.ClosingStockDate.Value);
                foreach (var det in rsClstk)
                {
                    var cldto = await _closingstockRepo.GetAsync(det.Id);
                    cldto.ApprovedUserId = AbpSession.UserId;
                    cldto.AdjustmentRefId = dto.Id;

                    await _closingstockRepo.InsertOrUpdateAndGetIdAsync(cldto);
                }
            }

            return new IdInput
            {
                Id = retId
            };
        }

        public async Task<ListResultOutput<AdjustmentListDto>> GetIds()
        {
            var lstAdjustment = await _adjustmentRepo.GetAll().ToListAsync();
            return new ListResultOutput<AdjustmentListDto>(lstAdjustment.MapTo<List<AdjustmentListDto>>());
        }


        public async Task<List<AdjustmentAutoViewDto>> GetLastAutoAdjustments(IdInput input)
        {

            var adjustmentMaster = _adjustmentRepo.GetAll().Where(t => t.LocationRefId == input.Id &&
                           (t.TokenRefNumber == 777 || t.TokenRefNumber == 888)).OrderByDescending(t => t.AdjustmentDate).Take(5);

            var allItems = from adjMas in adjustmentMaster
                           join adjDet in _adjustmentDetailRepo.GetAll()
                           on adjMas.Id equals adjDet.AdjustmentRefIf
                           group adjDet by new { adjMas.Id, adjMas.AdjustmentDate, adjDet.AdjustmentRefIf, adjMas.TokenRefNumber, adjMas.AdjustmentRemarks, adjMas.ClosingStockDate } into g
                           select new AdjustmentAutoViewDto
                           {
                               AdjustmentId = g.Key.Id,
                               AdjustmentDate = g.Key.AdjustmentDate,
                               AdjustmentRemarks = g.Key.AdjustmentRemarks,
                               TokenRefNumber = g.Key.TokenRefNumber,
                               ClosingStockDate = g.Key.ClosingStockDate,
                               NumberOfMaterials = g.Count()
                           };

            var sortMenuItems = await allItems
                .OrderByDescending(t => t.AdjustmentDate)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<AdjustmentAutoViewDto>>();

            return allListDtos;
        }

        public async Task<List<AdjustmentReportConsolidatedDto>> GetAdjustmentDetailReport(InputAdjustmentReport input)
        {
            if (input.ToLocations == null)
            {
                throw new UserFriendlyException("LocationErr");
            }

            int[] locIds = input.ToLocations.Select(t => t.Id).ToArray();

            var rsMaster = _adjustmentRepo.GetAll().Where(t => locIds.Contains(t.LocationRefId));

            if (input.StartDate.Value != null && input.EndDate.Value != null)
            {
                rsMaster =
                    rsMaster.Where(
                        a => DbFunctions.TruncateTime(a.AdjustmentDate) >= DbFunctions.TruncateTime(input.StartDate)
                             && DbFunctions.TruncateTime(a.AdjustmentDate) <= DbFunctions.TruncateTime(input.EndDate));
            }
            else
            {
                input.StartDate = DateTime.Today.AddDays(-31);
                input.EndDate = DateTime.Now;
            }

            bool MaterialExists = false;

            if (input.MaterialRefId != null)
            {
                MaterialExists = true;
            }

            bool MaterialTypeExists = false;
            if (input.MaterialTypeRefId != null)
                MaterialTypeExists = true;

            string excess = L("Excess");
            string wastage = L("Wastage");
            string shortage = L("Shortage");
            string damaged = L("Damaged");

            var rsDetail = _adjustmentDetailRepo.GetAll();

            if (MaterialExists == true)
                rsDetail = rsDetail.Where(p => p.MaterialRefId == input.MaterialRefId);

            if (input.AdjustmentMode != "" && input.AdjustmentMode != null)
            {
                if (input.AdjustmentMode.Equals(excess))
                    rsDetail = rsDetail.Where(t => t.AdjustmentMode.Equals(excess));
                else
                    rsDetail = rsDetail.Where(t => !t.AdjustmentMode.Equals(excess));
            }

            var allItems = (from mas in rsMaster
                            join det in rsDetail
                            on mas.Id equals det.AdjustmentRefIf
                            join mat in _materialRepo.GetAll().WhereIf(MaterialTypeExists, t => t.MaterialTypeId == input.MaterialTypeRefId)
                            on det.MaterialRefId equals mat.Id
                            join unit in _unitRepo.GetAll()
                            on mat.DefaultUnitId equals unit.Id
                            select new AdjustmentReportConsolidatedDto
                            {
                                MaterialTypeRefId = mat.MaterialTypeId,
                                MaterialRefId = det.MaterialRefId,
                                MaterialRefName = mat.MaterialName,
                                TotalQty = det.AdjustmentQty,
                                ExcessQty = det.AdjustmentMode == excess ? det.AdjustmentQty : 0,
                                ShortageQty = det.AdjustmentMode == excess ? 0 : det.AdjustmentQty,
                                PlusOrMinus = det.AdjustmentMode == excess ? 1 : -1,
                                Uom = unit.Name
                            }).ToList();

            var groupItems = (from mas in allItems
                              group mas by new { mas.MaterialRefId, mas.MaterialRefName, mas.Uom, mas.Price } into g
                              select new AdjustmentReportConsolidatedDto
                              {
                                  MaterialRefId = g.Key.MaterialRefId,
                                  MaterialRefName = g.Key.MaterialRefName,
                                  Uom = g.Key.Uom,
                                  Price = g.Key.Price,
                                  TotalQty = g.Sum(t => t.TotalQty * t.PlusOrMinus),
                                  ExcessQty = g.Sum(t => t.ExcessQty),
                                  ExcessAmount = g.Sum(t => (t.ExcessQty * t.PlusOrMinus) * t.Price),
                                  ShortageQty = g.Sum(t => t.ShortageQty),
                                  ShortageAmount = g.Sum(t => (t.ShortageQty * t.PlusOrMinus) * t.Price),
                                  TotalAmount = g.Sum(t => (t.TotalQty * t.PlusOrMinus) * t.Price),
                              }).ToList();

            List<AdjustmentReportConsolidatedDto> sortMenuItems;

            sortMenuItems = groupItems;

            var allListDtos = sortMenuItems.MapTo<List<AdjustmentReportConsolidatedDto>>();

            List<int> arrMaterialRefIdsTobeRateNeeded = allListDtos.Select(t => t.MaterialRefId).ToList();

            var matRateView = await _housereportAppService.GetMaterialRateView(new GetHouseReportMaterialRateInput
            {
                StartDate = input.StartDate.Value,
                EndDate = input.EndDate.Value,
                Locations = input.ToLocations,
                MaterialRefIds = arrMaterialRefIdsTobeRateNeeded,
                FunctionCalledBy = "Get Adjustment Detail Report"
            });

            var rateList = matRateView.MaterialRateView;

            foreach (var lst in allListDtos)
            {
                var mat = rateList.FirstOrDefault(t => t.MaterialRefId == lst.MaterialRefId);
                if (mat == null)
                    lst.Price = 0;
                else
                    lst.Price = mat.AvgRate;

                lst.TotalAmount = Math.Round(lst.TotalQty * lst.Price, roundDecimals);
                lst.ExcessAmount = Math.Round(lst.ExcessQty * lst.Price, roundDecimals);
                lst.ShortageAmount = Math.Round(lst.ShortageQty * lst.Price * -1, roundDecimals);

                if (lst.TotalQty > 0)
                {
                    lst.AdjustmentMode = L("Excess");
                }
                else if (lst.TotalQty < 0)
                {
                    lst.AdjustmentMode = L("Wastage");
                }
            }

            return allListDtos;
        }

        public async Task<FileDto> GetAdjustmentDetailReportToExcel(InputAdjustmentReport input)
        {
            List<AdjustmentReportConsolidatedDto> allList = await GetAdjustmentDetailReport(input);
            var allListDtos = allList.MapTo<List<AdjustmentReportConsolidatedDto>>();
            return _adjustmentExporter.ExportAdjustmentDetailToFile(allListDtos, input);
        }

        public async Task<FileDto> GetManualStockAdjustmentDetailReportToExcel(GetAdjustmentForEditOutput input)
        {
            return _adjustmentExporter.ExportManualStockDetailToFile(input);
        }

        public async Task<IdInput> CompAdjustment(DayCloseDto input)
        {

            #region AdjustmentForCompWastage
            List<AdjustmentDetailViewDto> adjustmentDetailDtos = new List<AdjustmentDetailViewDto>();

            DateTime transactionDate;
            if (input.TransactionDate == null)
            {
                var tloc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
                transactionDate = tloc.HouseTransactionDate.Value.Date;
            }
            else
                transactionDate = input.TransactionDate.Date;

            var alreadyExist = await _adjustmentRepo.FirstOrDefaultAsync(t => t.LocationRefId == input.LocationRefId && DbFunctions.TruncateTime(t.AdjustmentDate) == DbFunctions.TruncateTime(transactionDate) && t.TokenRefNumber == 555);

            if (alreadyExist != null)
                return new IdInput { Id = -1 };
            else
            {
                int adjSno = 1;
                foreach (var items in input.DayCloseWastageData)
                {
                    AdjustmentDetailViewDto det = new AdjustmentDetailViewDto();
                    decimal adjustmentQty = items.Damaged;
                    string adjustmentMode = L("Wastage");

                    if (items.DefaultUnitName.Length == 0 || items.DefaultUnitId == 0)
                    {
                        throw new UserFriendlyException("UnitErr");
                    }

                    det.MaterialRefId = items.MaterialRefId;
                    det.AdjustmentQty = adjustmentQty;
                    det.AdjustmentApprovedRemarks = "";
                    det.AdjustmentMode = adjustmentMode;
                    det.UnitRefId = items.DefaultUnitId;
                    det.Sno = adjSno++;

                    adjustmentDetailDtos.Add(det);
                }
            }

            CreateOrUpdateAdjustmentInput newAdjDto = new CreateOrUpdateAdjustmentInput();
            if (adjustmentDetailDtos.Count > 0)
            {
                AdjustmentEditDto masDto = new AdjustmentEditDto();

                if (input.TransactionDate == null)
                {
                    var tloc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
                    masDto.AdjustmentDate = tloc.HouseTransactionDate.Value.Date;
                }
                else
                    masDto.AdjustmentDate = input.TransactionDate.Date;

                masDto.AdjustmentRemarks = L("CompWastage");
                masDto.ApprovedPersonId = (int)AbpSession.UserId;
                masDto.LocationRefId = input.LocationRefId;
                masDto.EnteredPersonId = (int)AbpSession.UserId;
                masDto.TokenRefNumber = 555;

                newAdjDto.Adjustment = masDto;
                newAdjDto.AdjustmentDetail = adjustmentDetailDtos;

                var adjId = await CreateOrUpdateAdjustment(newAdjDto);
                return adjId;
            }
            else
            {
                return new IdInput { Id = -1 };
            }
            #endregion
        }

    }
}
