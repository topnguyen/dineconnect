﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.House.Transaction
{
    public interface IProductionAppService : IApplicationService
    {
        Task<PagedResultOutput<ProductionListDto>> GetAll(GetProductionInput inputDto);
        Task<FileDto> GetAllToExcel(GetProductionInput inputDto);
        Task<GetProductionForEditOutput> GetProductionForEdit(NullableIdInput nullableIdInput);
        Task DeleteProduction(IdInput input);

        Task<IdInput> CreateOrUpdateProduction(CreateOrUpdateProductionInput input);
    }
}

