﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.House.Temp;
using DinePlan.DineConnect.House.Master;
using DinePlan.DineConnect.House.Master.Dtos;
using Abp.Timing;
using System;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Master;
using Abp.Configuration;
using DinePlan.DineConnect.Configuration;

namespace DinePlan.DineConnect.House.Transaction.Implementation
{
    public class InwardDirectCreditAppService : DineConnectAppServiceBase, IInwardDirectCreditAppService
    {
        private readonly IInwardDirectCreditListExcelExporter _inwarddirectcreditExporter;
        private readonly IInwardDirectCreditManager _inwarddirectcreditManager;
        private readonly IRepository<InwardDirectCredit> _inwarddirectcreditRepo;
        private readonly IRepository<Supplier> _supplierRepo;
        private readonly IRepository<SupplierMaterial> _supplierMaterialRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<InwardDirectCreditDetail> _inwarddirectcreditDetailRepo;
        private readonly IRepository<TempInwardDirectCreditDetail> _tempInwardDirectCreditRepo;
        private readonly IRepository<MaterialLedger> _materialLedgerRepo;
        private readonly IRepository<Unit> _unitRepo;
        private readonly IRepository<PurchaseOrder> _purchaseorderRepo;
        private readonly IRepository<PurchaseOrderDetail> _purchaseorderDetailRepo;
        private readonly IInwardDirectCreditDetailManager _inwarddcDetailManager;
        private readonly ITempInwardDirectCreditDetailManager _tempInwardDcManager;
        private readonly IMaterialAppService _materialAppService;
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<Adjustment> _adjustmentRepo;
        private readonly IRepository<AdjustmentDetail> _adjustmentDetailRepo;
        private readonly IRepository<Invoice> _invoiceRepo;
        private readonly IRepository<InvoiceDirectCreditLink> _invoiceDirectCreditLinkRepo;
        private readonly SettingManager _settingManager;
        private readonly IUnitConversionAppService _unitConversionAppService;

        public InwardDirectCreditAppService(IInwardDirectCreditManager inwarddirectcreditManager,
            IRepository<InwardDirectCredit> inwardDirectCreditRepo,
            IInwardDirectCreditListExcelExporter inwarddirectcreditExporter,
            IRepository<Supplier> supplierRepo,
            IRepository<Material> materialRepo,
            IRepository<InwardDirectCreditDetail> inwarddirectcreditDetailRepo,
            IRepository<TempInwardDirectCreditDetail> tempInwardDirectCreditRepo,
            IInwardDirectCreditDetailManager inwarddcDetailManager,
            ITempInwardDirectCreditDetailManager tempInwardDcManager,
            IRepository<MaterialLedger> materialLedgerRepo,
            IRepository<Unit> unitRepo,
            IMaterialAppService materialAppService,
            IRepository<PurchaseOrder> purchaseorderRepo,
            IRepository<PurchaseOrderDetail> purchaseorderDetailRepo,
            IRepository<Location> locationRepo,
            IRepository<Adjustment> adjustmentRepo,
            IRepository<AdjustmentDetail> adjustmentDetailRepo,
              IRepository<Invoice> invoiceRepo,
            IRepository<InvoiceDirectCreditLink> invoiceDirectCreditLinkRepo,
            SettingManager settingManager,
            IUnitConversionAppService unitConversionAppService,
            IRepository<SupplierMaterial> supplierMaterialRepo
            )
        {
            _inwarddirectcreditManager = inwarddirectcreditManager;
            _inwarddirectcreditRepo = inwardDirectCreditRepo;
            _inwarddirectcreditExporter = inwarddirectcreditExporter;
            _supplierRepo = supplierRepo;
            _supplierMaterialRepo = supplierMaterialRepo;
            _materialRepo = materialRepo;
            _inwarddirectcreditDetailRepo = inwarddirectcreditDetailRepo;
            _tempInwardDirectCreditRepo = tempInwardDirectCreditRepo;
            _inwarddcDetailManager = inwarddcDetailManager;
            _tempInwardDcManager = tempInwardDcManager;
            _materialLedgerRepo = materialLedgerRepo;
            _unitRepo = unitRepo;
            _materialAppService = materialAppService;
            _purchaseorderRepo = purchaseorderRepo;
            _purchaseorderDetailRepo = purchaseorderDetailRepo;
            _locationRepo = locationRepo;
            _adjustmentRepo = adjustmentRepo;
            _adjustmentDetailRepo = adjustmentDetailRepo;
            _invoiceRepo = invoiceRepo;
            _invoiceDirectCreditLinkRepo = invoiceDirectCreditLinkRepo;
            _settingManager = settingManager;
            _unitConversionAppService = unitConversionAppService;
        }

        public async Task<PagedResultOutput<InwardDirectCreditListDto>> GetAll(GetInwardDirectCreditInput input)
        {
            var allItems = _inwarddirectcreditRepo.GetAll();
            if (!string.IsNullOrEmpty(input.Operation) && input.Operation.Equals("SEARCH"))
            {
                allItems = _inwarddirectcreditRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Id.Equals(input.Filter)
               );
            }
            else
            {
                allItems = _inwarddirectcreditRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Id.ToString().Contains(input.Filter)
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<InwardDirectCreditListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<InwardDirectCreditListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetInwardDirectCreditInput input)
        {

            input.MaxResultCount = AppConsts.MaxPageSize;

            var allList = await GetAllWithSupplierNames(input);
            var allListDtos = allList.Items.MapTo<List<InwardDirectCreditWithSupplierName>>();
            return _inwarddirectcreditExporter.ExportToFile(allListDtos);
        }

        public async Task<GetInwardDirectCreditForEditOutput> GetInwardDirectCreditForEdit(InputIdWithPrintFlag input)
        {
            InwardDirectCreditEditDto editDto;
            List<InwardDirectCreditDetailViewDto> editDetailDto;

            if (input.Id.HasValue)
            {

                var hDto = await _inwarddirectcreditRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<InwardDirectCreditEditDto>();

                editDetailDto = await (from inwDet in _inwarddirectcreditDetailRepo.GetAll().Where(a => a.DcRefId == input.Id.Value)
                                       join mat in _materialRepo.GetAll()
                                       on inwDet.MaterialRefId equals mat.Id
                                       join un in _unitRepo.GetAll()
                                       on inwDet.UnitRefId equals un.Id                                       
                                       select new InwardDirectCreditDetailViewDto
                                       {
                                           Id = inwDet.Id,
                                           DcReceivedQty = inwDet.DcReceivedQty,
                                           DcConvertedQty = inwDet.DcConvertedQty,
                                           DcRefId = inwDet.DcRefId,
                                           MaterialReceivedStatus = inwDet.MaterialReceivedStatus,
                                           MaterialRefId = inwDet.MaterialRefId,
                                           Barcode = mat.Barcode,
                                           MaterialRefName = mat.MaterialName,
                                           UnitRefId = inwDet.UnitRefId,
                                           UnitRefName = un.Name,
                                           YieldExcessShortageQty = inwDet.YieldExcessShortageQty,
                                           YieldMode = inwDet.YieldMode,
                                           YieldPercentage = inwDet.YieldPercentage
                                       }).ToListAsync();

                var materialRefIds = editDetailDto.Select(t => t.MaterialRefId).ToList();
                var rsSupplierMaterials = await _supplierMaterialRepo.GetAllListAsync(t => t.SupplierRefId == editDto.SupplierRefId && materialRefIds.Contains(t.MaterialRefId));

                foreach(var lst in editDetailDto)
                {
                    var supmat = rsSupplierMaterials.FirstOrDefault(t => t.LocationRefId==editDto.LocationRefId && t.MaterialRefId == lst.MaterialRefId );
                    if (supmat == null)
                    {
                        supmat = rsSupplierMaterials.FirstOrDefault(t => t.LocationRefId == null && t.MaterialRefId == lst.MaterialRefId);
                    }
                    if (supmat != null)
                    {
                        lst.SupplierMaterialAliasName = supmat.SupplierMaterialAliasName;
                    }
                }
                var existConverted = editDetailDto.Where(t => t.DcConvertedQty > 0).ToList();

                if (existConverted.Count > 0 && input.PrintFlag == false)
                {
                    throw new UserFriendlyException(L("PartialConversionAsBill"));
                }
            }
            else
            {
                editDto = new InwardDirectCreditEditDto();
                editDetailDto = new List<InwardDirectCreditDetailViewDto>();
            }

            return new GetInwardDirectCreditForEditOutput
            {
                InwardDirectCredit = editDto,
                InwardDirectCreditDetail = editDetailDto
            };
        }

        public async Task<InvoiceWithAdjustmentId> CreateOrUpdateInwardDirectCredit(CreateOrUpdateInwardDirectCreditInput input)
        {
            //var isInwardPurchaseOrderReferenceFromOtherErpRequired = await _settingManager.GetSettingValueAsync<bool>(AppSettings.HouseSettings.IsInwardPurchaseOrderReferenceFromOtherErpRequired);
            //if (isInwardPurchaseOrderReferenceFromOtherErpRequired == true)
            //{
            //    if (input.InwardDirectCredit.PurchaseOrderReferenceFromOtherErp.IsNullOrEmpty() == true)
            //    {
            //        throw new UserFriendlyException(L("IsInwardPurchaseOrderReferenceFromOtherErpRequired"));
            //    }
            //}

            //var isInwardInvoiceNumberReferenceFromOtherErpRequired = await _settingManager.GetSettingValueAsync<bool>(AppSettings.HouseSettings.IsInwardInvoiceNumberReferenceFromOtherErpRequired);
            //if (isInwardPurchaseOrderReferenceFromOtherErpRequired == true)
            //{
            //    if (input.InwardDirectCredit.InvoiceNumberReferenceFromOtherErp.IsNullOrEmpty() == true)
            //    {
            //        throw new UserFriendlyException(L("IsInwardInvoiceNumberReferenceFromOtherErpRequired"));
            //    }
            //}

            if (input.InwardDirectCredit.Id.HasValue)
            {
                return await UpdateInwardDirectCredit(input);
            }
            else
            {
                return await CreateInwardDirectCredit(input);
            }

        }

        public async Task DeleteInwardDirectCredit(IdInput input)
        {

            var inwardMas = await _inwarddirectcreditRepo.GetAsync(input.Id);
            Location loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == inwardMas.LocationRefId);
            if (loc == null)
            {
                throw new UserFriendlyException(L("LocationErr"));
            }

            var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();
            decimal conversionFactor = 0;

            if (inwardMas.AccountDate.Value.Date == loc.HouseTransactionDate.Value)
            {
                if (inwardMas.IsDcCompleted == false)
                {
                    var inwardDet = await _inwarddirectcreditDetailRepo.GetAll().Where(t => t.DcRefId == input.Id).ToListAsync();

                    foreach (var detDto in inwardDet)
                    {
                        MaterialLedgerDto ml = new MaterialLedgerDto();
                        ml.LocationRefId = inwardMas.LocationRefId;
                        ml.MaterialRefId = detDto.MaterialRefId;
                        ml.LedgerDate = inwardMas.AccountDate.Value;
                        ml.Received = -1 * detDto.DcReceivedQty;
                        ml.UnitId = detDto.UnitRefId;

                        var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);

                        if (inwardMas.PoRefId != null && inwardMas.PoRefId > 0)
                        {
                            var poitem = await _purchaseorderRepo.GetAsync(inwardMas.PoRefId.Value);
                            poitem.PoCurrentStatus = "H";// L("Approved");

                            var poDetail = await _purchaseorderDetailRepo.FirstOrDefaultAsync(t => t.PoRefId == poitem.Id && t.MaterialRefId == detDto.MaterialRefId);

                            if (poDetail.UnitRefId == detDto.UnitRefId)
                            {
                                poDetail.QtyReceived = poDetail.QtyReceived - detDto.DcReceivedQty;
                            }
                            else
                            {
                                var unitConversion = await _materialAppService.GetConversion(new GetConversionBasedOnBaseUnitRefUnit
                                {
                                    BaseUnitId = detDto.UnitRefId,
                                    RefUnitId = poDetail.UnitRefId,
                                    ConversionUnitList = rsUc,
                                    SupplierRefId = inwardMas.SupplierRefId,
                                    MaterialRefId = poDetail.MaterialRefId,
                                });
                                conversionFactor = unitConversion.Conversion;
                                //var unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == detDto.UnitRefId && t.RefUnitId == poDetail.UnitRefId);
                                //if (unitConversion != null)
                                //{
                                //    conversionFactor = unitConversion.Conversion;
                                //}
                                //else
                                //{
                                //    unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == poDetail.UnitRefId && t.RefUnitId == detDto.UnitRefId);
                                //    if (unitConversion == null)
                                //    {
                                //        var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == poDetail.UnitRefId);
                                //        var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == detDto.UnitRefId);

                                //        throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
                                //    }
                                //    conversionFactor = 1 / unitConversion.Conversion;
                                //}
                                poDetail.QtyReceived = poDetail.QtyReceived - (detDto.DcReceivedQty * conversionFactor);
                            }

                        }

                    }

                    await _inwarddirectcreditRepo.DeleteAsync(input.Id);
                }
                else
                {
                    throw new UserFriendlyException("SelectedReceiptsBillConvertedErr");
                }
            }
            else
            {
                throw new UserFriendlyException("ReceiptsCurrentDateCanOnlyDeleted");
            }

        }

        protected virtual async Task<InvoiceWithAdjustmentId> UpdateInwardDirectCredit(CreateOrUpdateInwardDirectCreditInput input)
        {
            var item = await _inwarddirectcreditRepo.GetAsync(input.InwardDirectCredit.Id.Value);

            if (input.InwardDirectCreditDetail == null || input.InwardDirectCreditDetail.Count() == 0)
            {
                throw new UserFriendlyException(L("MinimumOneDetail"));
            }

            var dto = input.InwardDirectCredit;

            item.DcDate = dto.DcDate;
            item.AccountDate = dto.AccountDate;
            item.DcNumberGivenBySupplier = dto.DcNumberGivenBySupplier;
            item.SupplierRefId = dto.SupplierRefId;
            item.IsDcCompleted = dto.IsDcCompleted;
            item.PurchaseOrderReferenceFromOtherErp = dto.PurchaseOrderReferenceFromOtherErp;
            item.InvoiceNumberReferenceFromOtherErp = dto.InvoiceNumberReferenceFromOtherErp;

            if (item.AdjustmentRefId.HasValue)
                await DeletePurchaseYieldAdjustment(new IdInput { Id = item.AdjustmentRefId.Value });

            var rsUnits = await _unitRepo.GetAllListAsync();
            List<AdjustmentDetailViewDto> adjustmentDetailDtos = new List<AdjustmentDetailViewDto>();
            int adjSno = 1;
            var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();
            decimal conversionFactor = 0;

            PurchaseOrder poitem = new PurchaseOrder();
            bool linkedWithPoFlag = false;
            if (input.InwardDirectCredit.PoRefId != null && input.InwardDirectCredit.PoRefId > 0)
            {
                poitem = await _purchaseorderRepo.GetAsync(input.InwardDirectCredit.PoRefId.Value);
                linkedWithPoFlag = true;
            }

            //  Update Detail Link
            List<int> materialsToBeRetained = new List<int>();

            if (input.InwardDirectCreditDetail != null && input.InwardDirectCreditDetail.Count > 0)
            {
                foreach (var items in input.InwardDirectCreditDetail)
                {
                    int materialrefid = items.MaterialRefId;
                    materialsToBeRetained.Add(materialrefid);

                    var existingDetailEntry = _inwarddirectcreditDetailRepo.GetAllList(u => u.DcRefId == dto.Id && u.MaterialRefId.Equals(materialrefid));

                    if (existingDetailEntry.Count == 0)  //  Add New Material
                    {
                        InwardDirectCreditDetail ab = new InwardDirectCreditDetail();

                        ab.DcRefId = (int)dto.Id;
                        ab.DcReceivedQty = items.DcReceivedQty;
                        ab.DcConvertedQty = 0;
                        ab.MaterialRefId = items.MaterialRefId;
                        ab.MaterialReceivedStatus = items.MaterialReceivedStatus;
                        ab.UnitRefId = items.UnitRefId;

                        MaterialLedgerDto ml = new MaterialLedgerDto();
                        ml.LocationRefId = dto.LocationRefId;
                        ml.MaterialRefId = ab.MaterialRefId;
                        ml.LedgerDate = dto.AccountDate.Value.Date;
                        ml.Received = ab.DcReceivedQty;
                        ml.UnitId = items.UnitRefId;
                        var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);

                        ab.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;
                        var retId2 = await _inwarddirectcreditDetailRepo.InsertAndGetIdAsync(ab);

                        #region Link With PO
                        if (linkedWithPoFlag)
                        {
                            var poDetail = await _purchaseorderDetailRepo.FirstOrDefaultAsync(t => t.PoRefId == poitem.Id && t.MaterialRefId == items.MaterialRefId);
                            if (poDetail != null)
                            {
                                decimal recdQtyFromReceipts = 0m;
                                if (poDetail.UnitRefId == items.UnitRefId)
                                {
                                    recdQtyFromReceipts = items.DcReceivedQty;
                                }
                                else
                                {
                                    var unitConversion = await _materialAppService.GetConversion(new GetConversionBasedOnBaseUnitRefUnit
                                    {
                                        BaseUnitId = items.UnitRefId,
                                        RefUnitId = poDetail.UnitRefId,
                                        ConversionUnitList = rsUc,
                                        SupplierRefId = input.InwardDirectCredit.SupplierRefId,
                                        MaterialRefId = poDetail.MaterialRefId,
                                    });

                                    //var unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == items.UnitRefId && t.RefUnitId == poDetail.UnitRefId);
                                    //if (unitConversion != null)
                                    //{
                                    //    conversionFactor = unitConversion.Conversion;
                                    //}
                                    //else
                                    //{
                                    //    unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == poDetail.UnitRefId && t.RefUnitId == items.UnitRefId);
                                    //    if (unitConversion == null)
                                    //    {
                                    //        var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == poDetail.UnitRefId);
                                    //        var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == items.UnitRefId);

                                    //        throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
                                    //    }
                                    //    conversionFactor = 1 / unitConversion.Conversion;
                                    //}
                                    recdQtyFromReceipts = items.DcReceivedQty * conversionFactor;
                                }
                                poDetail.QtyReceived = poDetail.QtyReceived + recdQtyFromReceipts;

                                if (poDetail.QtyReceived >= poDetail.QtyOrdered)
                                    poDetail.PoStatus = true;
                                await _purchaseorderDetailRepo.UpdateAsync(poDetail);
                                poitem.PoCurrentStatus = "H";
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        var editDetailDto = await _inwarddirectcreditDetailRepo.GetAsync(existingDetailEntry[0].Id);

                        string diffStatus = "";
                        decimal differenceQty;
                        differenceQty = items.DcReceivedQty - editDetailDto.DcReceivedQty;
                        if (items.DcReceivedQty == editDetailDto.DcReceivedQty)
                        {
                            diffStatus = "=";
                        }
                        else if (items.DcReceivedQty > editDetailDto.DcReceivedQty)
                        {
                            diffStatus = "+";
                        }
                        else
                        {
                            diffStatus = "-";
                        }

                        if (differenceQty != 0)
                        {
                            editDetailDto.DcRefId = (int)dto.Id;
                            editDetailDto.DcReceivedQty = items.DcReceivedQty;
                            editDetailDto.MaterialRefId = items.MaterialRefId;
                            editDetailDto.MaterialReceivedStatus = items.MaterialReceivedStatus;
                            editDetailDto.UnitRefId = items.UnitRefId;

                            MaterialLedgerDto ml = new MaterialLedgerDto();
                            ml.LocationRefId = dto.LocationRefId;
                            ml.MaterialRefId = items.MaterialRefId;
                            ml.LedgerDate = dto.AccountDate.Value.Date;
                            ml.Received = differenceQty;
                            ml.PlusMinus = "U";
                            ml.UnitId = items.UnitRefId;
                            var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                            editDetailDto.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;

                            var retId2 = await _inwarddirectcreditDetailRepo.InsertOrUpdateAndGetIdAsync(editDetailDto);


                            if (linkedWithPoFlag)
                            {
                                var poDetail = await _purchaseorderDetailRepo.FirstOrDefaultAsync(t => t.PoRefId == poitem.Id && t.MaterialRefId == items.MaterialRefId);
                                if (poDetail != null)
                                {
                                    decimal recdQtyFromReceipts = 0m;
                                    if (poDetail.UnitRefId == items.UnitRefId)
                                    {
                                        recdQtyFromReceipts = differenceQty;
                                    }
                                    else
                                    {
                                        var unitConversion = await _materialAppService.GetConversion(new GetConversionBasedOnBaseUnitRefUnit
                                        {
                                            BaseUnitId = items.UnitRefId,
                                            RefUnitId = poDetail.UnitRefId,
                                            ConversionUnitList = rsUc,
                                            SupplierRefId = input.InwardDirectCredit.SupplierRefId,
                                            MaterialRefId = poDetail.MaterialRefId,
                                        });

                                        //var unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == items.UnitRefId && t.RefUnitId == poDetail.UnitRefId);
                                        //if (unitConversion != null)
                                        //{
                                        //    conversionFactor = unitConversion.Conversion;
                                        //}
                                        //else
                                        //{
                                        //    unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == poDetail.UnitRefId && t.RefUnitId == items.UnitRefId);
                                        //    if (unitConversion == null)
                                        //    {
                                        //        var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == poDetail.UnitRefId);
                                        //        var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == items.UnitRefId);

                                        //        throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
                                        //    }
                                        //    conversionFactor = 1 / unitConversion.Conversion;
                                        //}
                                        recdQtyFromReceipts = differenceQty * conversionFactor;
                                    }

                                    if (diffStatus.Equals("="))
                                    {
                                    }
                                    else if (diffStatus.Equals("+"))
                                    {
                                        poDetail.QtyReceived = poDetail.QtyReceived + recdQtyFromReceipts;
                                    }
                                    else if (diffStatus.Equals("-"))
                                    {
                                        poDetail.QtyReceived = poDetail.QtyReceived + recdQtyFromReceipts;
                                    }

                                    poDetail.PoStatus = false;
                                    if (poDetail.QtyReceived < 0)
                                    {
                                        poDetail.QtyReceived = 0;
                                    }
                                    if (poDetail.QtyReceived >= poDetail.QtyOrdered)
                                        poDetail.PoStatus = true;
                                    await _purchaseorderDetailRepo.UpdateAsync(poDetail);
                                    poitem.PoCurrentStatus = "H";
                                }

                            }
                        }

                        
                    }

                    #region Adjustment
                    AdjustmentDetailViewDto det = new AdjustmentDetailViewDto();
                    if (items.YieldPercentage.HasValue
                            && items.YieldExcessShortageQty.HasValue && items.YieldMode.Length > 0)
                    {
                        if (items.YieldExcessShortageQty != 0)
                        {
                            decimal adjustmentQty = items.YieldExcessShortageQty.Value;
                            string adjustmentMode = items.YieldMode;

                            if (adjustmentMode.Equals(L("Shortage")))
                            {
                                // Do Nothing
                            }
                            else if (adjustmentMode.Equals(L("Excess")))
                            {
                                // Nothing
                            }
                            else
                            {
                                throw new UserFriendlyException(L("ShouldBeExcessOrShortage"));
                            }

                            if (items.UnitRefId == 0)
                            {
                                throw new UserFriendlyException(items.MaterialRefName + L("UnitErr"));
                            }

                            var unit = rsUnits.FirstOrDefault(t => t.Id == items.UnitRefId);

                            string adjustmentApprovedRemarks = L("DcPurchaseYieldAdjustmemt", item.Id, adjustmentQty, unit.Name, adjustmentMode, items.DcReceivedQty);

                            det.MaterialRefId = items.MaterialRefId;
                            det.AdjustmentQty = adjustmentQty;
                            det.AdjustmentApprovedRemarks = adjustmentApprovedRemarks;
                            det.AdjustmentMode = adjustmentMode;
                            det.UnitRefId = items.UnitRefId;
                            det.Sno = adjSno++;

                            adjustmentDetailDtos.Add(det);
                        }
                    }
                    #endregion
                }
            }

            item.AdjustmentRefId = null;
            if (adjustmentDetailDtos.Count > 0)
                await CreatePurchaseYieldAdjustment(input, adjustmentDetailDtos, item);

            var delInwardDcDetailList = _inwarddirectcreditDetailRepo.GetAll().Where(a => a.DcRefId == input.InwardDirectCredit.Id.Value && !materialsToBeRetained.Contains(a.MaterialRefId)).ToList();

            foreach (var items in delInwardDcDetailList)
            {
                MaterialLedgerDto ml = new MaterialLedgerDto();
                ml.LocationRefId = dto.LocationRefId;
                ml.MaterialRefId = items.MaterialRefId;
                ml.LedgerDate = dto.AccountDate.Value.Date;
                ml.Received = (-1 * items.DcReceivedQty);
                ml.PlusMinus = "U";
                ml.UnitId = items.UnitRefId;
                var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);

                items.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;

                await _inwarddirectcreditDetailRepo.DeleteAsync(items.Id);

                if (linkedWithPoFlag)
                {
                    bool CompleteFlag = true;
                    var poDetail = await _purchaseorderDetailRepo.FirstOrDefaultAsync(t => t.PoRefId == poitem.Id && t.MaterialRefId==items.MaterialRefId);
                    if (poDetail != null)
                    {
                        decimal recdQtyFromReceipts = 0m;
                        if (poDetail.UnitRefId == items.UnitRefId)
                        {
                            recdQtyFromReceipts = items.DcReceivedQty;
                        }
                        else
                        {
                            var unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == items.UnitRefId && t.RefUnitId == poDetail.UnitRefId);
                            if (unitConversion != null)
                            {
                                conversionFactor = unitConversion.Conversion;
                            }
                            else
                            {
                                unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == poDetail.UnitRefId && t.RefUnitId == items.UnitRefId);
                                if (unitConversion == null)
                                {
                                    var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == poDetail.UnitRefId);
                                    var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == items.UnitRefId);

                                    throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
                                }
                                conversionFactor = 1 / unitConversion.Conversion;
                            }
                            recdQtyFromReceipts = items.DcReceivedQty * conversionFactor;
                        }
                        poDetail.QtyReceived = poDetail.QtyReceived - recdQtyFromReceipts;
                        poDetail.PoStatus = false;
                        if (poDetail.QtyReceived < 0)
                        {
                            poDetail.QtyReceived = 0;
                        }
                        if (poDetail.QtyReceived >= poDetail.QtyOrdered)
                            poDetail.PoStatus = true;
                        await _purchaseorderDetailRepo.UpdateAsync(poDetail);
                        poitem.PoCurrentStatus = "H";
                    }

                    if (CompleteFlag)
                        poitem.PoCurrentStatus = "C"; // L("Completed");
                    else
                        poitem.PoCurrentStatus = "H";
                }

            }

            #region Set PO Status
            if (linkedWithPoFlag)
            {
                var poDetails = await _purchaseorderDetailRepo.GetAllListAsync(t => t.PoRefId == input.InwardDirectCredit.PoRefId.Value);
                bool completeFlag = true;
                foreach (var podetail in poDetails)
                {
                    if (podetail.QtyOrdered > podetail.QtyReceived)
                    {
                        completeFlag = false;
                        break;
                    }
                }
                if (completeFlag)
                    poitem.PoCurrentStatus = "C"; // L("Completed");
                else
                    poitem.PoCurrentStatus = "H";
            }
            #endregion

            CheckErrors(await _inwarddirectcreditManager.CreateSync(item));

            return new InvoiceWithAdjustmentId
            {
                Id = input.InwardDirectCredit.Id.Value,
                AdjustmentId = item.AdjustmentRefId.HasValue ? item.AdjustmentRefId.Value : 0
            };
        }

        protected virtual async Task<InvoiceWithAdjustmentId> CreateInwardDirectCredit(CreateOrUpdateInwardDirectCreditInput input)
        {
            var dto = input.InwardDirectCredit.MapTo<InwardDirectCredit>();

            Clock.Provider = new UtcClockProvider();

            if (input.InwardDirectCreditDetail == null || input.InwardDirectCreditDetail.Count() == 0)
            {
                throw new UserFriendlyException(L("MinimumOneDetail"));
            }
            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == dto.LocationRefId);
            dto.AccountDate = loc.HouseTransactionDate.Value;
            var retId = await _inwarddirectcreditRepo.InsertOrUpdateAndGetIdAsync(dto);

            var rsUnits = await _unitRepo.GetAllListAsync();
            List<AdjustmentDetailViewDto> adjustmentDetailDtos = new List<AdjustmentDetailViewDto>();
            int adjSno = 1;
            var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();
            decimal conversionFactor = 0;

            PurchaseOrder poitem = new PurchaseOrder();
            bool linkedWithPoFlag = false;
            if (input.InwardDirectCredit.PoRefId != null && input.InwardDirectCredit.PoRefId > 0)
            {
                poitem = await _purchaseorderRepo.GetAsync(input.InwardDirectCredit.PoRefId.Value);
                linkedWithPoFlag = true;
            }

            foreach (InwardDirectCreditDetailViewDto items in input.InwardDirectCreditDetail.ToList())
            {
                InwardDirectCreditDetail ab = new InwardDirectCreditDetail();
                ab.DcRefId = retId;
                ab.DcReceivedQty = items.DcReceivedQty;
                ab.DcConvertedQty = 0;
                ab.MaterialRefId = items.MaterialRefId;
                if (items.MaterialReceivedStatus.IsNullOrEmpty())
                    items.MaterialReceivedStatus = " ";
                ab.MaterialReceivedStatus = items.MaterialReceivedStatus;
                ab.UnitRefId = items.UnitRefId;
                ab.YieldPercentage = items.YieldPercentage;
                ab.YieldMode = items.YieldMode;
                ab.YieldExcessShortageQty = items.YieldExcessShortageQty;

                var retId2 = await _inwarddirectcreditDetailRepo.InsertAndGetIdAsync(ab);

                #region Link With PO
                if (linkedWithPoFlag)
                {
                    var poDetail = await _purchaseorderDetailRepo.FirstOrDefaultAsync(t => t.PoRefId == poitem.Id && t.MaterialRefId == items.MaterialRefId);
                    if (poDetail != null)
                    {
                        decimal recdQtyFromReceipts = 0m;
                        if (poDetail.UnitRefId==items.UnitRefId)
                        {
                            recdQtyFromReceipts = items.DcReceivedQty;
                        }
                        else
                        {
                            var unitConversion = await _materialAppService.GetConversion(new GetConversionBasedOnBaseUnitRefUnit
                            {
                                BaseUnitId = poDetail.UnitRefId,
                                RefUnitId = items.UnitRefId,
                                ConversionUnitList = rsUc,
                                SupplierRefId = input.InwardDirectCredit.SupplierRefId,
                                MaterialRefId = poDetail.MaterialRefId,
                            });
                            //conversionFactor = 1/ unitConversion.Conversion;
                            //var unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == items.UnitRefId && t.RefUnitId == poDetail.UnitRefId);
                            //if (unitConversion != null)
                            //{
                            //    conversionFactor = unitConversion.Conversion;
                            //}
                            //else
                            //{
                                //unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == poDetail.UnitRefId && t.RefUnitId == items.UnitRefId && t.MaterialRefId==poDetail.MaterialRefId);
                                //if (unitConversion==null)
                                //    unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == poDetail.UnitRefId && t.RefUnitId == items.UnitRefId);
                                //else
                                //{

                                //}
                                //if (unitConversion == null)
                                //{
                                //    var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == poDetail.UnitRefId);
                                //    var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == items.UnitRefId);

                                //    throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
                                //}
                                
                            //    conversionFactor = 1 / unitConversion.Conversion;
                            //}

                            recdQtyFromReceipts = items.DcReceivedQty * 1 / unitConversion.Conversion;
                        }
                        poDetail.QtyReceived = poDetail.QtyReceived + recdQtyFromReceipts;

                        if (poDetail.QtyReceived >= poDetail.QtyOrdered)
                            poDetail.PoStatus = true;
                        await _purchaseorderDetailRepo.UpdateAsync(poDetail);
                        poitem.PoCurrentStatus = "H";
                    }
                }
                #endregion

                #region Adjustment
                AdjustmentDetailViewDto det = new AdjustmentDetailViewDto();
                if (items.YieldPercentage.HasValue
                        && items.YieldExcessShortageQty.HasValue && items.YieldMode.Length > 0)
                {
                    if (items.YieldExcessShortageQty != 0)
                    {
                        decimal adjustmentQty = items.YieldExcessShortageQty.Value;
                        string adjustmentMode = items.YieldMode;

                        if (adjustmentMode.Equals(L("Shortage")))
                        {
                            // Do Nothing
                        }
                        else if (adjustmentMode.Equals(L("Excess")))
                        {
                            // Nothing
                        }
                        else
                        {
                            throw new UserFriendlyException(L("ShouldBeExcessOrShortage"));
                        }

                        if (items.UnitRefId == 0)
                        {
                            throw new UserFriendlyException(items.MaterialRefName + L("UnitErr"));
                        }

                        var unit = rsUnits.FirstOrDefault(t => t.Id == items.UnitRefId);

                        string adjustmentApprovedRemarks = L("DcPurchaseYieldAdjustmemt", retId, adjustmentQty, unit.Name, adjustmentMode, items.DcReceivedQty);

                        det.MaterialRefId = items.MaterialRefId;
                        det.AdjustmentQty = adjustmentQty;
                        det.AdjustmentApprovedRemarks = adjustmentApprovedRemarks;
                        det.AdjustmentMode = adjustmentMode;
                        det.UnitRefId = items.UnitRefId;
                        det.Sno = adjSno++;

                        adjustmentDetailDtos.Add(det);
                    }
                }
                #endregion

                MaterialLedgerDto ml = new MaterialLedgerDto();
                ml.LocationRefId = dto.LocationRefId;
                ml.MaterialRefId = ab.MaterialRefId;
                ml.LedgerDate = dto.AccountDate.Value.Date;
                ml.Received = ab.DcReceivedQty;
                ml.UnitId = items.UnitRefId;
                ml.SupplierRefId = input.InwardDirectCredit.SupplierRefId;
                var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                
                ab.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;
            }

            if (adjustmentDetailDtos.Count > 0)
                await CreatePurchaseYieldAdjustment(input, adjustmentDetailDtos, dto);

      

            CheckErrors(await _inwarddirectcreditManager.CreateSync(dto));

            #region Set PO Status
            if (linkedWithPoFlag)
            {
                var poDetails = await _purchaseorderDetailRepo.GetAllListAsync(t => t.PoRefId == input.InwardDirectCredit.PoRefId.Value);
                bool completeFlag = true;
                foreach(var podetail in poDetails)
                {
                    if (podetail.QtyOrdered>podetail.QtyReceived)
                    {
                        completeFlag = false;
                        break;
                    }
                }
                if (completeFlag)
                    poitem.PoCurrentStatus = "C"; // L("Completed");
                else
                    poitem.PoCurrentStatus = "H";
            }
            #endregion

            //if (input.InwardDirectCredit.PoRefId != null && input.InwardDirectCredit.PoRefId > 0)
            //{
            //    var poitem = await _purchaseorderRepo.GetAsync(input.InwardDirectCredit.PoRefId.Value);
            //    bool CompleteFlag = true;
            //    var podetail = await _purchaseorderDetailRepo.GetAll().Where(t => t.PoRefId == input.InwardDirectCredit.PoRefId.Value).ToListAsync();
            //    foreach (var det in podetail)
            //    {
            //        var inwmas = _inwarddirectcreditRepo.GetAll().Where(t => t.PoRefId == input.InwardDirectCredit.PoRefId.Value).ToList().Select(t => t.Id).ToArray();

            //        var rcdQtyinInwDet = _inwarddirectcreditDetailRepo.GetAll().Where(t => inwmas.Contains(t.DcRefId) && t.MaterialRefId == det.MaterialRefId).ToList().Sum(t => t.DcReceivedQty);

            //        var rcdQtyinInwDet = _inwarddirectcreditDetailRepo.GetAll().Where(t => inwmas.Contains(t.DcRefId) && t.MaterialRefId == det.MaterialRefId).ToList().Sum(t => t.DcReceivedQty);

            //        if (rcdQtyinInwDet < det.QtyOrdered)
            //        {
            //            CompleteFlag = false;
            //        }

            //        var podetDto = await _purchaseorderDetailRepo.GetAsync(det.Id);
            //        podetDto.QtyReceived = rcdQtyinInwDet;
            //        if (podetDto.QtyReceived >= podetDto.QtyOrdered)
            //            podetDto.PoStatus = true;

            //        await _purchaseorderDetailRepo.UpdateAsync(podetDto);
            //    }

            //    if (CompleteFlag)
            //        poitem.PoCurrentStatus = "C"; // L("Completed");
            //    else
            //        poitem.PoCurrentStatus = "H";
            //}

            return new InvoiceWithAdjustmentId
            {
                Id = retId,
                AdjustmentId = dto.AdjustmentRefId.HasValue ? dto.AdjustmentRefId.Value : 0
            };
        }

        protected async Task CreatePurchaseYieldAdjustment(CreateOrUpdateInwardDirectCreditInput input, List<AdjustmentDetailViewDto> adjustmentDetailDtos, InwardDirectCredit dto)
        {
            #region PurchaseYield_Adjustment
            IdInput adjId = new IdInput { Id = 0 };
            CreateOrUpdateAdjustmentInput newAdjDto = new CreateOrUpdateAdjustmentInput();
            if (adjustmentDetailDtos.Count > 0)
            {
                AdjustmentEditDto masDto = new AdjustmentEditDto();

                if (input.InwardDirectCredit.DcDate == null)
                {
                    var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.InwardDirectCredit.LocationRefId);
                    masDto.AdjustmentDate = loc.HouseTransactionDate.Value.Date;
                }
                else
                    masDto.AdjustmentDate = input.InwardDirectCredit.DcDate.Date;

                masDto.AdjustmentRemarks = L("DC") + " " + L("PurchaseYield") + " " + L("Difference");
                masDto.ApprovedPersonId = (int)AbpSession.UserId;
                masDto.LocationRefId = dto.LocationRefId;
                masDto.EnteredPersonId = (int)AbpSession.UserId;
                masDto.TokenRefNumber = 333;

                newAdjDto.Adjustment = masDto;
                newAdjDto.AdjustmentDetail = adjustmentDetailDtos;
                newAdjDto.SupplierRefId = input.InwardDirectCredit.SupplierRefId;

                adjId = await CreatePurchaseYieldAdjustment(newAdjDto);
            }

            if (adjId.Id > 0)
                dto.AdjustmentRefId = adjId.Id;
            #endregion
        }



        public async Task<ListResultOutput<InwardDirectCreditListDto>> GetInwardDirectCreditIds()
        {
            var lstInwardDirectCredit = await _inwarddirectcreditRepo.GetAll().ToListAsync();
            return new ListResultOutput<InwardDirectCreditListDto>(lstInwardDirectCredit.MapTo<List<InwardDirectCreditListDto>>());
        }

        //InwardDcWithSupplierName
        public async Task<PagedResultOutput<InwardDirectCreditWithSupplierName>> GetAllWithSupplierNames(GetInwardDirectCreditInput input)
        {

            var rsSupplier = _supplierRepo.GetAll().WhereIf(!input.SupplierName.IsNullOrEmpty(), p => p.SupplierName.Contains(input.SupplierName));

            var rsInwarddirect = _inwarddirectcreditRepo.GetAll().Where(t => t.LocationRefId == input.DefaultLocationRefId).WhereIf(!input.DcNumberGivenBySupplier.IsNullOrEmpty(), t => t.DcNumberGivenBySupplier.Contains(input.DcNumberGivenBySupplier));

            if (input.StartDate != null && input.EndDate != null)
            {
                rsInwarddirect = rsInwarddirect.Where(t => t.DcDate >= input.StartDate && t.DcDate <= input.EndDate);
            }

            var allitems = (from inwdc in rsInwarddirect.WhereIf(input.Id.HasValue,t=>t.Id==input.Id)
                            join sup in rsSupplier on inwdc.SupplierRefId equals sup.Id
                            select new InwardDirectCreditWithSupplierName()
                            {
                                Id = inwdc.Id,
                                AccountDate = inwdc.AccountDate,
                                DcDate = inwdc.DcDate,
                                IsDcCompleted = inwdc.IsDcCompleted,
                                DcNumberGivenBySupplier = inwdc.DcNumberGivenBySupplier,
                                SupplierName = sup.SupplierName,
                                SupplierRefId = inwdc.SupplierRefId,
                                CreationTime = inwdc.CreationTime,
                                PoRefId = inwdc.PoRefId,
                                PoReference = inwdc.PoReferenceCode,
                                InvoiceNumberReferenceFromOtherErp = inwdc.InvoiceNumberReferenceFromOtherErp,
                                PurchaseOrderReferenceFromOtherErp = inwdc.PurchaseOrderReferenceFromOtherErp
                            }
               );

            var sortMenuItems = await allitems
               .OrderBy(input.Sorting)
               .PageBy(input)
               .ToListAsync();

            var allListDtos = sortMenuItems.ToList();
            var allItemCount = await allitems.CountAsync();
            List<int> dcRefIds = allListDtos.Select(t => (int)t.Id).ToList();
            var rsInwDCs = await _invoiceDirectCreditLinkRepo.GetAllListAsync(t => dcRefIds.Contains(t.InwardDirectCreditRefId));
            List<int> invRefIds = rsInwDCs.Select(t => t.InvoiceRefId).ToList();
            var rsInvoices = await _invoiceRepo.GetAllListAsync(t => invRefIds.Contains(t.Id));

            List<ComboboxItemDto> invoicePayModeList = new List<ComboboxItemDto>();
            if (allListDtos.Count > 0)
            {
                var temp = await _materialAppService.GetInvoicePayModeEnumForCombobox();
                invoicePayModeList = temp.Items.MapTo<List<ComboboxItemDto>>();
            }
            foreach (var lst in allListDtos.Where(t => t.PoRefId.HasValue))
            {
                var inwDc = rsInwDCs.FirstOrDefault(t => t.InwardDirectCreditRefId == lst.Id);
                if (inwDc != null)
                {
                    var inv = rsInvoices.FirstOrDefault(t => t.Id == inwDc.InvoiceRefId);
                    lst.InvoiceDate = inv.InvoiceDate;
                    var payMode = invoicePayModeList.FirstOrDefault(t => t.Value == inv.InvoicePayMode.ToString());
                    if (payMode != null)
                    {
                        lst.InvoiceMode = payMode.DisplayText;
                    }
                    lst.InvoiceAccountDate = inv.AccountDate;
                    lst.InvoiceNumber = inv.InvoiceNumber;
                }
            }
            return new PagedResultOutput<InwardDirectCreditWithSupplierName>(
               allItemCount,
               allListDtos
               );
        }

     

        public async Task<ListResultOutput<InwardDirectCreditListDto>> GetInwardDcTranid(InputDateRangeWithSupplierId input)
        {
            DateTime dcFromDt = input.StartDate;
            DateTime dcEndDt = input.EndDate;

            var allItems = await (from inwdc in _inwarddirectcreditRepo.GetAll()
                                  .Where(t => t.LocationRefId == input.LocationRefId && t.SupplierRefId == input.SupplierRefId && !t.IsDcCompleted)
                                  .WhereIf(input.AllTimeDcs == false, t => DbFunctions.TruncateTime(t.AccountDate) >= DbFunctions.TruncateTime(dcFromDt)
                                          && DbFunctions.TruncateTime(t.AccountDate) <= DbFunctions.TruncateTime(dcEndDt)
                                 )
                                  select inwdc).ToListAsync();

            return new ListResultOutput<InwardDirectCreditListDto>(allItems.MapTo<List<InwardDirectCreditListDto>>());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetInwardPoTranid(InputDateRangeWithSupplierId input)
        {
            DateTime fromDt = input.StartDate;
            DateTime endDt = input.EndDate;

            var rsPurchaseOrder = _purchaseorderRepo.GetAll()
                                  .Where(t => t.LocationRefId == input.LocationRefId && t.SupplierRefId == input.SupplierRefId)
                                  .WhereIf(input.AllTimeDcs == false, t => DbFunctions.TruncateTime(t.PoDate) >= DbFunctions.TruncateTime(fromDt) && DbFunctions.TruncateTime(t.PoDate) <= DbFunctions.TruncateTime(endDt));

            var allItems = await (from poMas in rsPurchaseOrder
                                  join poDetail in _purchaseorderDetailRepo
                                      .GetAll().Where(t => (t.QtyOrdered - t.QtyReceived) > 0 && t.PoStatus == false)
                                  on poMas.Id equals poDetail.PoRefId
                                  select new ComboboxItemDto
                                  {
                                      Value = poMas.Id.ToString(),
                                      DisplayText = poMas.PoReferenceCode
                                  }).ToListAsync();

            return
                new ListResultOutput<ComboboxItemDto>(
                    allItems.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText.ToString())).ToList());
        }

        public async Task<PagedResultOutput<InwardDirectCreditView>> GetAllView(GetInwardDirectCreditInput input)
        {
            var allitems = (from inwdc in _inwarddirectcreditRepo.GetAll().Where(p => p.LocationRefId == input.DefaultLocationRefId)
                            join indet in _inwarddirectcreditDetailRepo.GetAll() on inwdc.Id equals indet.DcRefId
                            where inwdc.Id == input.SelectedId
                            join mat in _materialRepo.GetAll() on indet.MaterialRefId equals mat.Id
                            join sup in _supplierRepo.GetAll() on inwdc.SupplierRefId equals sup.Id
                            join uiss in _unitRepo.GetAll() on indet.UnitRefId equals uiss.Id
                            select new InwardDirectCreditView()
                            {
                                Id = inwdc.Id,
                                DcDate = inwdc.DcDate,
                                IsDcCompleted = inwdc.IsDcCompleted,
                                DcNumberGivenBySupplier = inwdc.DcNumberGivenBySupplier,
                                SupplierName = sup.SupplierName,
                                SupplierRefId = inwdc.SupplierRefId,
                                DcReceivedQty = indet.DcReceivedQty,
                                MaterialCode = indet.MaterialRefId,
                                MaterialName = mat.MaterialName,
                                MaterialReceivedStatus = indet.MaterialReceivedStatus,
                                CreationTime = inwdc.CreationTime,
                                UnitRefId = indet.UnitRefId,
                                UnitRefName = uiss.Name
                            });

            var sortMenuItems = await allitems
               .OrderBy(input.Sorting)
               .PageBy(input)
               .ToListAsync();

            var allListDtos = sortMenuItems.ToList();
            var allItemCount = await allitems.CountAsync();
            return new PagedResultOutput<InwardDirectCreditView>(
               allItemCount,
               allListDtos
               );

        }



        public async Task<PagedResultOutput<InwardDirectCreditDetailListDto>> GetAllInwardDcDetail(GetInwardDirectCreditDetailInput input)
        {
            var allItems = _inwarddirectcreditDetailRepo.GetAll();
            if (!string.IsNullOrEmpty(input.Operation) && input.Operation.Equals("SEARCH"))
            {
                allItems = _inwarddirectcreditDetailRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Id.Equals(input.Filter)
               );
            }
            else
            {
                allItems = _inwarddirectcreditDetailRepo
               .GetAll();
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<InwardDirectCreditDetailListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<InwardDirectCreditDetailListDto>(
                allItemCount,
                allListDtos
                );
        }


        public async Task<PrintOutIn40Cols> GetPrintData(IdInput input)
        {
            PrintOutIn40Cols output = new PrintOutIn40Cols();

            InwardDirectCreditEditDto editDto;
            List<InwardDirectCreditDetailViewDto> editDetailDto;

            var hDto = await _inwarddirectcreditRepo.GetAsync(input.Id);
            editDto = hDto.MapTo<InwardDirectCreditEditDto>();

            editDetailDto = await (from inwDet in _inwarddirectcreditDetailRepo.GetAll().Where(a => a.DcRefId == input.Id)
                                   join mat in _materialRepo.GetAll()
                                   on inwDet.MaterialRefId equals mat.Id
                                   join un in _unitRepo.GetAll()
                                   on inwDet.UnitRefId equals un.Id
                                   select new InwardDirectCreditDetailViewDto
                                   {
                                       Id = inwDet.Id,
                                       DcReceivedQty = inwDet.DcReceivedQty,
                                       DcConvertedQty = inwDet.DcConvertedQty,
                                       DcRefId = inwDet.DcRefId,
                                       MaterialReceivedStatus = inwDet.MaterialReceivedStatus,
                                       MaterialRefId = inwDet.MaterialRefId,
                                       Barcode = mat.Barcode,
                                       MaterialRefName = mat.MaterialName,
                                       UnitRefId = inwDet.UnitRefId,
                                       UnitRefName = un.Name
                                   }).ToListAsync();

            var supplier = _supplierRepo.FirstOrDefault(t => t.Id == hDto.SupplierRefId);
            var billingLocation = _locationRepo.FirstOrDefault(t => t.Id == hDto.LocationRefId);

            TextFileWriter fp = new TextFileWriter();


            string subPath = "~/Report/";
            bool exists = System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(subPath));

            if (!exists)
                System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(subPath));



            string fileName = System.Web.HttpContext.Current.Server.MapPath("~/Report/" + AbpSession.UserId + "_DCReport.txt");
            int PageLenth = 45;

            fp.FileOpen(fileName, "O");
            if (billingLocation.Name != null)
            {
                fp.PrintTextLine(fp.PrintFormatCenter(billingLocation.Name, PageLenth));
            }

            if (billingLocation.Address1 != null)
            {
                fp.PrintTextLine(fp.PrintFormatCenter(billingLocation.Address1, PageLenth));
            }

            if (billingLocation.Address2 != null)
            {
                fp.PrintTextLine(fp.PrintFormatCenter(billingLocation.Address2, PageLenth));
            }

            if (billingLocation.Address3 != null)
            {
                string addwithCity = billingLocation.Address3;
                if (billingLocation.City != null)
                    addwithCity = addwithCity + ", " + billingLocation.City;
                fp.PrintTextLine(fp.PrintFormatCenter(addwithCity, PageLenth));
            }

            fp.PrintTextLine(fp.PrintFormatCenter(L("Receipts"), PageLenth));

            fp.PrintText(fp.PrintFormatLeft(L("Ref#") + ": " + hDto.DcNumberGivenBySupplier, 27));
            fp.PrintTextLine(L("Date") + ": " + hDto.AccountDate.Value.ToString("dd/MM/yyyy"));

            fp.PrintSepLine('-', PageLenth);

            string retText = System.IO.File.ReadAllText(fileName);

            retText = retText.Replace("\r\n", "<br/>");

            retText = retText.Replace(" ", "&nbsp;");

            output.HeaderText = retText;

            output.BoldHeaderText = "<STRONG>" + retText + "</STRONG>";

            fp.FileOpen(fileName, "O");

            fp.PrintText(L("Supplier") + " : ");
            if (supplier.SupplierName.Length > 0)
                fp.PrintTextLine(supplier.SupplierName);

            if (supplier.TaxRegistrationNumber != null)
            {
                fp.PrintText(L("Tax#"));
                fp.PrintTextLine(supplier.TaxRegistrationNumber);
            }

            fp.PrintSepLine('-', PageLenth);

            fp.PrintText(fp.PrintFormatLeft(L("Material"), 30));
            fp.PrintText(fp.PrintFormatRight(L("Qty"), 8));
            fp.PrintText(fp.PrintFormatLeft(" ", 1));
            fp.PrintText(fp.PrintFormatLeft(L("UOM"), 6));

            fp.PrintTextLine("");
            fp.PrintSepLine('-', PageLenth);

            int loopIndex = 1;

            foreach (var det in editDetailDto)
            {
                fp.PrintText(fp.PrintFormatLeft(det.MaterialRefName, 30));
                decimal qty = Math.Round(det.DcReceivedQty, 2);
                string qtyinstring = string.Concat(new string(' ', 9 - qty.ToString().Length), qty.ToString());

                qtyinstring = qtyinstring + " " + det.UnitRefName;

                fp.PrintText(fp.PrintFormatLeft(qtyinstring, 14));
                fp.PrintTextLine("");
                loopIndex++;

            }

            fp.PrintSepLine('-', PageLenth);

            fp.PrintTextLine(fp.PrintFormatCenter("Print @ " + DateTime.Now.ToString("yy-MMM-dd HH:mm:ss"), PageLenth));
            fp.PrintSepLine('-', PageLenth);
            fp.PrintTextLine(" ");

            retText = System.IO.File.ReadAllText(fileName);

            retText = retText.Replace("\r\n", "<br/>");

            output.BodyText = retText;

            output.BoldBodyText = "<STRONG>" + retText + "</STRONG>";

            return output;

        }


        protected virtual async Task<IdInput> CreatePurchaseYieldAdjustment(CreateOrUpdateAdjustmentInput input)
        {
            var dto = input.Adjustment.MapTo<Adjustment>();

            var locationAdj = await _locationRepo.FirstOrDefaultAsync(t => t.Id == dto.LocationRefId);
            dto.AccountDate = locationAdj.HouseTransactionDate.Value;

            var retId = await _adjustmentRepo.InsertOrUpdateAndGetIdAsync(dto);

            foreach (AdjustmentDetailViewDto items in input.AdjustmentDetail.ToList())
            {
                AdjustmentDetail itdDto = new AdjustmentDetail
                {
                    AdjustmentRefIf = retId,
                    MaterialRefId = items.MaterialRefId,
                    AdjustmentQty = items.AdjustmentQty,
                    ClosingStock = items.ClosingStock,
                    StockEntry = items.StockEntry,
                    AdjustmentMode = items.AdjustmentMode,
                    AdjustmentApprovedRemarks = items.AdjustmentApprovedRemarks,
                    Sno = items.Sno,
                    UnitRefId = items.UnitRefId
                };

                await _adjustmentDetailRepo.InsertAndGetIdAsync(itdDto);
                MaterialLedgerDto ml = new MaterialLedgerDto
                {
                    LocationRefId = dto.LocationRefId,
                    MaterialRefId = itdDto.MaterialRefId,
                    LedgerDate = dto.AdjustmentDate
                };

                if (input.ForceAdjustmentFlag)
                {
                    ml.ForceAdjustmentFlag = true;
                }
                else
                {
                    ml.ForceAdjustmentFlag = false;
                }

                if (itdDto.AdjustmentMode.Equals(L("Excess")))
                    ml.ExcessReceived = itdDto.AdjustmentQty;
                else if (itdDto.AdjustmentMode.Equals(L("Shortage")))
                    ml.Shortage = itdDto.AdjustmentQty;
                else if (itdDto.AdjustmentMode.Equals(L("Wastage")))
                    ml.Damaged = itdDto.AdjustmentQty;
                else if (itdDto.AdjustmentMode.Equals(L("Damaged")))
                    ml.Damaged = itdDto.AdjustmentQty;
                else if (itdDto.AdjustmentMode.Equals(L("Equal")))
                    ml.ExcessReceived = itdDto.AdjustmentQty;
                else
                    throw new UserFriendlyException(L("AdjustmentMode") + " " + L("Error") + itdDto.AdjustmentMode);
                ml.UnitId = itdDto.UnitRefId;
                ml.SupplierRefId = input.SupplierRefId;
                var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                itdDto.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;
            }

            return new IdInput
            {
                Id = retId
            };
        }

        protected async Task DeletePurchaseYieldAdjustment(IdInput input)
        {

            var rsAdjMas = await _adjustmentRepo.FirstOrDefaultAsync(t => t.Id == input.Id);
            if (rsAdjMas == null)
            {
                throw new UserFriendlyException(L("Adjustment") + " " + input.Id + " " + L("NotExist"));
            }

            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == rsAdjMas.LocationRefId);

            var houseTransactionDate = loc.HouseTransactionDate;

            if (houseTransactionDate == null)
            {
                throw new UserFriendlyException(L("DayClose") + " " + L("Pending"));
            }

            if (houseTransactionDate.Value.Date != rsAdjMas.AdjustmentDate.Date)
            {
                throw new UserFriendlyException(L("AdjustmentDeleteError", loc.HouseTransactionDate.Value.ToString("yyyy-MMM-dd"), rsAdjMas.AdjustmentDate.Date.ToString("yyyy-MMM-dd"), rsAdjMas.Id, rsAdjMas.TokenRefNumber));
            }

            var adjustmentDetail = await _adjustmentDetailRepo.GetAll().Where(t => t.AdjustmentRefIf == input.Id).ToListAsync();
            foreach (var det in adjustmentDetail)
            {
                MaterialLedgerDto ml = new MaterialLedgerDto();
                ml.LocationRefId = rsAdjMas.LocationRefId;
                ml.MaterialRefId = det.MaterialRefId;
                ml.LedgerDate = rsAdjMas.AdjustmentDate;

                if (det.AdjustmentMode.Equals(L("Excess")))
                    ml.ExcessReceived = -1 * det.AdjustmentQty;
                else if (det.AdjustmentMode.Equals(L("Shortage")))
                    ml.Shortage = -1 * det.AdjustmentQty;
                else if (det.AdjustmentMode.Equals(L("Wastage")))
                    ml.Damaged = -1 * det.AdjustmentQty;
                else if (det.AdjustmentMode.Equals(L("Damaged")))
                    ml.Damaged = -1 * det.AdjustmentQty;


                ml.UnitId = det.UnitRefId;
                
                var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);

                await _adjustmentDetailRepo.DeleteAsync(det.Id);
            }

            await _adjustmentRepo.DeleteAsync(input.Id);
        }
    }
}
