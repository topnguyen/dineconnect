﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using DinePlan.DineConnect.House.Master.Dtos;

namespace DinePlan.DineConnect.House.Transaction
{
    public interface IIssueAppService : IApplicationService
    {
        Task<PagedResultOutput<IssueListDto>> GetAll(GetIssueInput inputDto);
        Task<FileDto> GetAllToExcel(GetIssueInput input);
        Task<GetIssueForEditOutput> GetIssueForEdit(NullableIdInput nullableIdInput);
        Task<IdInput> CreateOrUpdateIssue(CreateOrUpdateIssueInput input);
        Task DeleteIssue(IdInput input);
        Task<ListResultOutput<IssueListDto>> GetIssueIds(DateWithProductionUnit input);
        Task<int> GetMaxTokenNumber(DateTime selDt);
        Task<GetIssueForStandardRecipe> GetIssueForRecipe(GetStandardIngridient input);

      

        Task<List<IssueWithRecipeListDto>> GetPendingIssues(InputLocation input);
        Task<List<MaterialInfoDto>> GetMaterialForGivenRecipeCombobox(InputLocationAndRecipe input);
        Task<IdInput> GetCategoryIdForRecipe(IdInput input);

        Task<ListResultOutput<ComboboxItemDto>> GetRequestSlipNumber(IdInput input);
        Task SetCompletedIssues(InputLocation input);
		Task<FileDto> ImportIssueTemplate(InputLocation input);


	}
}
