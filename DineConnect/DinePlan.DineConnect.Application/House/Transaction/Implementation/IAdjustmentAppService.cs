﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using DinePlan.DineConnect.Connect.DayClose.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;

namespace DinePlan.DineConnect.House.Transaction
{
    public interface IAdjustmentAppService : IApplicationService
    {
        Task<PagedResultOutput<AdjustmentListDto>> GetAll(GetAdjustmentInput inputDto);
        Task<FileDto> GetAllToExcel(GetAdjustmentInput input);
        Task<GetAdjustmentForEditOutput> GetAdjustmentForEdit(NullableIdInput nullableIdInput);
        Task<IdInput> CreateOrUpdateAdjustment(CreateOrUpdateAdjustmentInput input);
        Task DeleteAdjustment(IdInput input);

        Task<ListResultOutput<AdjustmentListDto>> GetIds();
        Task<PagedResultOutput<AdjustmentViewDto>> GetView(GetAdjustmentInput input);
        Task<List<AdjustmentAutoViewDto>> GetLastAutoAdjustments(IdInput input);

        Task<List<AdjustmentReportConsolidatedDto>> GetAdjustmentDetailReport(InputAdjustmentReport input);

        Task<FileDto> GetAdjustmentDetailReportToExcel(InputAdjustmentReport input);

        Task<FileDto> GetManualStockAdjustmentDetailReportToExcel(GetAdjustmentForEditOutput input);
        Task<IdInput> CompAdjustment(DayCloseDto input);
    }
        
}