﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.House.Master;
using DinePlan.DineConnect.Common;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Master.Dtos;
using Abp.Configuration;
using System;

namespace DinePlan.DineConnect.House.Transaction.Implementation
{
    public class ProductionAppService : DineConnectAppServiceBase, IProductionAppService
    {

        private readonly IProductionListExcelExporter _productionExporter;
        private readonly IProductionManager _productionManager;
        private readonly IRepository<Production> _productionRepo;
        private readonly IRepository<ProductionDetail> _productiondetailRepo;
        private readonly IRepository<Issue> _issueRepo;
        private readonly IRepository<IssueRecipeDetail> _issuerecipedetailRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<Template> _templateRepo;
        private readonly IRepository<Unit> _unitRepo;
        private readonly IMaterialAppService _materialAppService;
        private readonly IXmlAndJsonConvertor _xmlandjsonConvertorAppService;
        private readonly IRepository<MaterialLocationWiseStock> _materiallocationwisestockRepo;
        private readonly IRepository<ProductionUnit> _productionunitRepo;
        private readonly IRepository<UnitConversion> _unitConversionRepo;
        private readonly SettingManager _settingManager;
        private readonly IHouseReportAppService _housereportAppService;

        public ProductionAppService(IProductionManager productionManager,
            IRepository<Production> productionRepo,
            IProductionListExcelExporter productionExporter,
            IRepository<ProductionDetail> productiondetailRepo,
            IRepository<Material> materialRepo,
            IRepository<Location> locationRepo,
            IRepository<Template> templateRepo,
               IMaterialAppService materialAppService,
            IRepository<Unit> unitRepo,
            IRepository<MaterialLocationWiseStock> materiallocationwisestockRepo,
             IXmlAndJsonConvertor xmlandjsonConvertorAppService,
             IRepository<Issue> issueRepo,
             IRepository<IssueRecipeDetail> issuerecipedetailRepo,
             IRepository<ProductionUnit> productionunitRepo,
             IRepository<UnitConversion> unitConversionRepo,
             SettingManager settingManager,
             IHouseReportAppService housereportAppService)
        {
            _productionManager = productionManager;
            _productionRepo = productionRepo;
            _productionExporter = productionExporter;
            _productiondetailRepo = productiondetailRepo;
            _materialRepo = materialRepo;
            _locationRepo = locationRepo;
            _templateRepo = templateRepo;
            _unitRepo = unitRepo;
            _materialAppService = materialAppService;
            _xmlandjsonConvertorAppService = xmlandjsonConvertorAppService;
            _materiallocationwisestockRepo = materiallocationwisestockRepo;
            _issueRepo = issueRepo;
            _productionunitRepo = productionunitRepo;
            _issuerecipedetailRepo = issuerecipedetailRepo;
            _unitConversionRepo = unitConversionRepo;
            _settingManager = settingManager;
            _housereportAppService = housereportAppService;
        }

        public async Task<PagedResultOutput<ProductionListDto>> GetAll(GetProductionInput input)
        {
            var rsProduction = _productionRepo.GetAll().Where(t => t.LocationRefId == input.DefaultLocationRefId).WhereIf(!input.Filter.IsNullOrEmpty(), p => p.ProductionSlipNumber.Contains(input.Filter));

            if (input.StartDate != null && input.EndDate != null)
            {
                rsProduction = rsProduction.Where(a =>
                                 DbFunctions.TruncateTime(a.ProductionTime) >= DbFunctions.TruncateTime(input.StartDate)
                                 &&
                                 DbFunctions.TruncateTime(a.ProductionTime) <= DbFunctions.TruncateTime(input.EndDate));
            }

            var allItems = from ret in rsProduction
                           join productionunit in _productionunitRepo.GetAll()
                           on ret.ProductionUnitRefId equals productionunit.Id into gj
                           from g in gj.DefaultIfEmpty()
                           select new ProductionListDto
                           {
                               Id = ret.Id,
                               LocationRefId = ret.LocationRefId,
                               ProductionUnitRefId = ret.ProductionUnitRefId,
                               ProductionUnitRefName = g.Name,
                               CreationTime = ret.CreationTime,
                               ProductionSlipNumber = ret.ProductionSlipNumber,
                               ProductionTime = ret.ProductionTime,
                               Status = ret.Status,
                           };

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<ProductionListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<ProductionListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetProductionInput input)
        {
            input.MaxResultCount = AppConsts.MaxPageSize;

            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<ProductionListDto>>();
            return _productionExporter.ExportToFile(allListDtos);
        }

        public async Task<GetProductionForEditOutput> GetProductionForEdit(NullableIdInput input)
        {
            ProductionEditDto editDto;
            List<ProductionDetailViewDto> editOutputDtos;
            TemplateSaveDto editTemplateDto = new TemplateSaveDto();

            if (input.Id.HasValue)
            {
                var hDto = await _productionRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<ProductionEditDto>();

                editOutputDtos = await (from prddet in _productiondetailRepo.GetAll()
                               .Where(a => a.ProductionRefId == input.Id.Value)
                                        join mat in _materialRepo.GetAll()
                                        on prddet.MaterialRefId equals mat.Id
                                        join locmat in _materiallocationwisestockRepo.GetAll().Where(l => l.LocationRefId == editDto.LocationRefId)
                                        on mat.Id equals locmat.MaterialRefId
                                        join un in _unitRepo.GetAll()
                                        on mat.DefaultUnitId equals un.Id
                                        join uiss in _unitRepo.GetAll()
                                        on prddet.UnitRefId equals uiss.Id
                                        select new ProductionDetailViewDto
                                        {
                                            Sno = prddet.Sno,
                                            ProductionRefId = (int)prddet.ProductionRefId,
                                            MaterialRefId = prddet.MaterialRefId,
                                            MaterialRefName = mat.MaterialName,
                                            ProductionQty = prddet.ProductionQty,
                                            UnitRefId = prddet.UnitRefId,
                                            UnitRefName = uiss.Name,
                                            Uom = un.Name,
                                            CurrentInHand = locmat.CurrentInHand,
                                            IssueRefId = prddet.IssueRefId,
                                            Price = prddet.Price,
                                            LineTotal = Math.Round(prddet.Price * prddet.ProductionQty,3)
                                        }).ToListAsync();
            }
            else
            {
                editDto = new ProductionEditDto();
                editOutputDtos = new List<ProductionDetailViewDto>();
            }

            return new GetProductionForEditOutput
            {
                Production = editDto,
                ProductionDetail = editOutputDtos,
                Templatesave = editTemplateDto
            };
        }

        public async Task<IdInput> CreateOrUpdateProduction(CreateOrUpdateProductionInput input)
        {
            if (input.Production.Id.HasValue)
            {
                return await UpdateProduction(input);
            }
            else
            {
                return await CreateProduction(input);
            }
        }

        public async Task DeleteProduction(IdInput input)
        {
            await _productionRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task<IdInput> UpdateProduction(CreateOrUpdateProductionInput input)
        {
            var item = await _productionRepo.GetAsync(input.Production.Id.Value);
            var dto = input.Production;

            item.ProductionSlipNumber = dto.ProductionSlipNumber;
            item.Status = dto.Status;
            item.ProductionUnitRefId = dto.ProductionUnitRefId;

            CheckErrors(await _productionManager.CreateSync(item));
            return new IdInput { Id = item.Id };
        }

        protected virtual async Task<IdInput> CreateProduction(CreateOrUpdateProductionInput input)
        {
            var rsUnitConversion = await _unitConversionRepo.GetAll().ToListAsync();

            var dto = input.Production.MapTo<Production>();
            dto.Status = "Completed";

            if (input.ProductionDetail.Count() == 1)
            {
                dto.ProductionSlipNumber = dto.ProductionSlipNumber + "/" + input.ProductionDetail[0].MaterialRefName.PadRight(10);
            }

            if (input.ProductionDetail.Count() == 2)
            {
                dto.ProductionSlipNumber = dto.ProductionSlipNumber + "/" + input.ProductionDetail[0].MaterialRefName.PadRight(5) + "/" + input.ProductionDetail[1].MaterialRefName.PadRight(5);
            }

            if (input.ProductionDetail == null || input.ProductionDetail.Count() == 0)
            {
                throw new UserFriendlyException(L("MinimumOneDetail"));
            }

            var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == dto.LocationRefId);
            dto.AccountDate = location.HouseTransactionDate.Value;
            var retId = await _productionRepo.InsertAndGetIdAsync(dto);

            //var interTransferValueShown = await _settingManager.GetSettingValueAsync<bool>(AppSettings.HouseSettings.InterTransferValueShown);

            List<MaterialRateViewListDto> materialRate = new List<MaterialRateViewListDto>();
            bool rateDefaultRequired = input.ProductionDetail.Exists(t => t.Price == 0);
            if (rateDefaultRequired)
            {
                if (input.MaterialRateView == null || input.MaterialRateView.Count == 0)
                {
                    List<Location> loc = await _locationRepo.GetAll().Where(t => t.Id == dto.LocationRefId).ToListAsync();
                    List<LocationListDto> locinput = loc.MapTo<List<LocationListDto>>();
                    List<int> materialRefIdsRateNeeded = input.ProductionDetail.Where(t => t.Price == 0).Select(t => t.MaterialRefId).ToList();
                    var matView = await _housereportAppService.GetMaterialRateView(new GetHouseReportMaterialRateInput
                    {
                        StartDate = loc[0].HouseTransactionDate.Value,
                        EndDate = loc[0].HouseTransactionDate.Value,
                        Locations = locinput,
                        MaterialRefIds = materialRefIdsRateNeeded,
                        FunctionCalledBy = "Production Addition"
                    });

                    materialRate = matView.MaterialRateView;
                }
                else
                {
                    materialRate = input.MaterialRateView;
                }
            }

            foreach (ProductionDetailViewDto item in input.ProductionDetail)
            {
                ProductionDetail pdet = new ProductionDetail();
                pdet.ProductionRefId = (int)dto.Id;
                pdet.Sno = item.Sno;
                pdet.MaterialRefId = item.MaterialRefId;
                pdet.ProductionQty = item.ProductionQty;
                pdet.UnitRefId = item.UnitRefId;
                if (item.IssueRefId != null)
                    pdet.IssueRefId = (int)item.IssueRefId;
                else
                    pdet.IssueRefId = null;
                var mat = await _materialRepo.FirstOrDefaultAsync(t => t.Id == item.MaterialRefId);
                item.DefaultUnitId = mat.DefaultUnitId;
                decimal conversionFactor = 1;
                if (item.DefaultUnitId == item.UnitRefId)
                    conversionFactor = 1;
                else
                {
                    var conv =
                             rsUnitConversion.FirstOrDefault(t => t.BaseUnitId == item.DefaultUnitId && t.RefUnitId == item.UnitRefId);

                    if (conv != null)
                    {
                        conversionFactor = conv.Conversion;
                    }
                    else
                    {
                        var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == item.DefaultUnitId);
                        if (baseUnit == null)
                        {
                            throw new UserFriendlyException(L("UnitIdDoesNotExist", item.DefaultUnitId));
                        }
                        var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == item.UnitRefId);
                        if (refUnit == null)
                        {
                            throw new UserFriendlyException(L("UnitIdDoesNotExist", item.UnitRefId));
                        }
                        throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
                    }
                }

                //if (interTransferValueShown == true && input.PrintFlag == false)
                if (item.Price == 0)
                {
                    var matprice = materialRate.FirstOrDefault(t => t.MaterialRefId == item.MaterialRefId);
                    if (item.DefaultUnitId == item.UnitRefId)
                        item.Price = matprice.AvgRate;
                    else
                    {
                        item.Price = Math.Round(matprice.AvgRate * 1 / conversionFactor, 3);
                    }
                }
                var retId3 = await _productiondetailRepo.InsertAndGetIdAsync(pdet);

                var issueRefDto = await _issueRepo.FirstOrDefaultAsync(t => t.Id == item.IssueRefId);
                if (issueRefDto != null)
                {
                    var issueRecipeDto = await _issuerecipedetailRepo.FirstOrDefaultAsync(t => t.IssueRefId == issueRefDto.Id && t.RecipeRefId == item.MaterialRefId);

                    if (issueRecipeDto != null && issueRecipeDto.RecipeRefId > 0)
                    {
                        if (issueRecipeDto.MultipleBatchProductionAllowed == false)
                        {
                            var irdto = await _issuerecipedetailRepo.GetAsync(issueRecipeDto.Id);
                            irdto.CompletionFlag = true;
                        }
                        else
                        {

                            var prodDet = await _productiondetailRepo.GetAll()
                                .Where(t => t.IssueRefId == issueRefDto.Id && t.MaterialRefId == item.MaterialRefId
                                && t.Id != pdet.Id).ToListAsync();

                            //var alreadyProduced = prodDet.Sum(t => t.ProductionQty);
                            decimal alreadyProduced = 0;
                            conversionFactor = 1;
                            foreach (var gpd in prodDet.GroupBy(t => t.UnitRefId))
                            {
                                conversionFactor = 1;
                                if (issueRecipeDto.UnitRefId == gpd.Key)
                                    conversionFactor = 1;
                                else
                                {
                                    var conv =
                                        rsUnitConversion.First(
                                            t => t.BaseUnitId == issueRecipeDto.UnitRefId && t.RefUnitId == gpd.Key);

                                    if (conv != null)
                                    {
                                        conversionFactor = conv.Conversion;
                                    }
                                    else
                                    {
                                        var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == issueRecipeDto.UnitRefId);
                                        if (baseUnit == null)
                                        {
                                            throw new UserFriendlyException(L("UnitIdDoesNotExist", issueRecipeDto.UnitRefId));
                                        }
                                        var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == gpd.Key);
                                        if (refUnit == null)
                                        {
                                            throw new UserFriendlyException(L("UnitIdDoesNotExist", gpd.Key));
                                        }
                                        throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
                                    }
                                }
                                alreadyProduced = alreadyProduced + gpd.Sum(t => t.ProductionQty) * 1 / conversionFactor;

                            }

                            decimal completedQty = 0;
                            conversionFactor = 1;
                            if (issueRecipeDto.UnitRefId == pdet.UnitRefId)
                                conversionFactor = 1;
                            else
                            {
                                var conv =
                                    rsUnitConversion.First(
                                        t => t.BaseUnitId == issueRecipeDto.UnitRefId && t.RefUnitId == pdet.UnitRefId);
                                conversionFactor = conv.Conversion;
                            }

                            decimal currentProductionQty = item.ProductionQty * 1 / conversionFactor;
                            completedQty = (alreadyProduced + currentProductionQty);


                            var irdto = await _issuerecipedetailRepo.GetAsync(issueRecipeDto.Id);
                            irdto.CompletedQty = completedQty;

                            decimal recipeProductionQty = issueRecipeDto.RecipeProductionQty;

                            if (completedQty >= recipeProductionQty)
                            {
                                irdto.CompletionFlag = true;
                            }
                        }
                    }
                }
                else
                {
                    var addedData = await _productiondetailRepo.GetAsync(pdet.Id);
                    addedData.Price = item.Price;
                    await _productiondetailRepo.UpdateAsync(addedData);
                }

                MaterialLedgerDto ml = new MaterialLedgerDto();
                ml.LocationRefId = dto.LocationRefId;
                ml.MaterialRefId = pdet.MaterialRefId;
                ml.LedgerDate = dto.ProductionTime.Date;
                ml.Received = pdet.ProductionQty;
                ml.UnitId = pdet.UnitRefId;

                var matLedgerId = await _materialAppService.UpdateMaterialLedger(ml);
                pdet.ConversionWithDefaultUnit = matLedgerId.ConversionWithDefaultUnit;
            }

            CheckErrors(await _productionManager.CreateSync(dto));
            return new IdInput { Id = dto.Id };
        }

    }
}
