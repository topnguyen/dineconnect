﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using System;

namespace DinePlan.DineConnect.House.Transaction
{
    public interface IReturnAppService : IApplicationService
    {
        Task<PagedResultOutput<ReturnListDto>> GetAll(GetReturnInput inputDto);
        Task<FileDto> GetAllToExcel(GetReturnInput input);
        Task<GetReturnForEditOutput> GetReturnForEdit(NullableIdInput nullableIdInput);
        Task<IdInput> CreateOrUpdateReturn(CreateOrUpdateReturnInput input);
        Task DeleteReturn(IdInput input);

        Task<ListResultOutput<ReturnListDto>> GetIds();
        Task<int> GetMaxTokenNumber(DateTime selDt);
    }
}