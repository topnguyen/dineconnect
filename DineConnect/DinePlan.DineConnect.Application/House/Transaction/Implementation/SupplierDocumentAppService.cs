﻿
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.House.Master.Dtos;
using System;

namespace DinePlan.DineConnect.House.Transaction.Implementation
{
    public class SupplierDocumentAppService : DineConnectAppServiceBase, ISupplierDocumentAppService
    {

        //private readonly IExcelExporter _exporter;
        private readonly ISupplierDocumentManager _supplierdocumentManager;
        private readonly IRepository<SupplierDocument> _supplierdocumentRepo;
        private readonly IRepository<Supplier> _supplierRepo;


        public SupplierDocumentAppService(ISupplierDocumentManager supplierdocumentManager,
            IRepository<SupplierDocument> supplierDocumentRepo, IRepository<Supplier> supplierRepo)
          //  , IExcelExporter exporter)
        {
            _supplierdocumentManager = supplierdocumentManager;
            _supplierdocumentRepo = supplierDocumentRepo;
            _supplierRepo = supplierRepo;
            //_exporter = exporter;
        }

        public async Task<PagedResultOutput<SupplierDocumentListDto>> GetAll(GetSupplierDocumentInput input)
        {
            var allItems = (from sd in _supplierdocumentRepo.GetAll()
                             join s in _supplierRepo.GetAll()
                             on sd.SupplierRefId equals s.Id
                             select new SupplierDocumentListDto()
                             {
                                 Id=sd.Id,
                                SupplierRefId=sd.SupplierRefId,
                                SupplierRefName=s.SupplierName,
                                DocumentRefId=sd.DocumentRefId,
                               // DocumentRefName = Enum.GetName(typeof(DocumentType),sd.DocumentRefId),
                                FileName =sd.FileName,
                                IsEntryFinished=sd.IsEntryFinished
                                
                             }).WhereIf(!input.Filter.IsNullOrEmpty(),
                                p => p.SupplierRefId.Equals(input.Filter)
                             );

            List<SupplierDocumentListDto> sortMenuItems;

            sortMenuItems = await allItems.ToListAsync();

            int loopcnt = 0;
            foreach (var lst in sortMenuItems)
            {
                string documentTypeName = lst.DocumentRefId == (int)SupplierDocumentType.PO ? L("PO") : lst.DocumentRefId == (int)SupplierDocumentType.DC ? L("IDC") : L("Invoice");
                sortMenuItems[loopcnt].DocumentRefName = documentTypeName;
                loopcnt++;
            }

            var allListDtos = sortMenuItems.MapTo<List<SupplierDocumentListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<SupplierDocumentListDto>(
                allItemCount,
                allListDtos

                );
        }

        //public async Task<FileDto> GetAllToExcel()
        //{


        //    var allList = await _supplierdocumentRepo.GetAll().ToListAsync();
        //    var allListDtos = allList.MapTo<List<SupplierDocumentListDto>>();
        //    var baseE = new BaseExportObject()
        //    {
        //        ExportObject = allListDtos,
        //        ColumnNames = new string[] { "Id" }
        //    };
        //    return _exporter.ExportToFile(baseE, L("SupplierDocument"));

        //}

        public async Task<GetSupplierDocumentForEditOutput> GetSupplierDocumentForEdit(NullableIdInput input)
        {
            SupplierDocumentEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _supplierdocumentRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<SupplierDocumentEditDto>();
            }
            else
            {
                editDto = new SupplierDocumentEditDto();
            }

            return new GetSupplierDocumentForEditOutput
            {
                SupplierDocument = editDto
            };
        }

        public async Task<IdInput> CreateOrUpdateSupplierDocument(CreateOrUpdateSupplierDocumentInput input)
        {
            if (input.SupplierDocument.Id.HasValue)
            {
                return await UpdateSupplierDocument(input);
            }
            else
            {
                return await CreateSupplierDocument(input);
            }
        }

        public async Task DeleteSupplierDocument(IdInput input)
        {
            await _supplierdocumentRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task<IdInput> UpdateSupplierDocument(CreateOrUpdateSupplierDocumentInput input)
        {
            var item = await _supplierdocumentRepo.GetAsync(input.SupplierDocument.Id.Value);
            var dto = input.SupplierDocument;

            //TODO: SERVICE SupplierDocument Update Individually
            item.SupplierRefId = dto.SupplierRefId;
            item.DocumentRefId = dto.DocumentRefId;
            item.FileName = dto.FileName;
            item.IsEntryFinished = dto.IsEntryFinished;
            item.CompletedTime = dto.CompletedTime;

            CheckErrors(await _supplierdocumentManager.CreateSync(item));

            return new IdInput { Id = item.Id };
        }

        protected virtual async Task<IdInput> CreateSupplierDocument(CreateOrUpdateSupplierDocumentInput input)
        {
            var dto = input.SupplierDocument.MapTo<SupplierDocument>();

            dto.FileName = "Temporary";

            CheckErrors(await _supplierdocumentManager.CreateSync(dto));

            return new IdInput { Id = dto.Id };
        }

        public async Task<ListResultOutput<SupplierDocumentListDto>> GetIds()
        {
            var lstSupplierDocument = await _supplierdocumentRepo.GetAll().ToListAsync();
            return new ListResultOutput<SupplierDocumentListDto>(lstSupplierDocument.MapTo<List<SupplierDocumentListDto>>());
        }
    }
}