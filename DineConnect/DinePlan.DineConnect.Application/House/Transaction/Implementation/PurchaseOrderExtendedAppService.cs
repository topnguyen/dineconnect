﻿using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.House.Transaction.Dtos;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.House.Master.Dtos;
using System;
using Abp.Net.Mail;
using DinePlan.DineConnect.Emailing;
using Abp.Domain.Uow;
using DinePlan.DineConnect.House.Master;
using DinePlan.DineConnect.Cater.Quotation.Dtos;
using DinePlan.DineConnect.Cater.Quotation;
using DinePlan.DineConnect.House.Transaction.Exporter;

namespace DinePlan.DineConnect.House.Transaction.Implementation
{
    public class PurchaseOrderExtendedAppService : DineConnectAppServiceBase, IPurchaseOrderExtendedAppService
    {

        private readonly IPurchaseOrderListExcelExporter _purchaseorderExporter;
        private readonly IPurchaseOrderManager _purchaseorderManager;
        private readonly IRepository<PurchaseOrder> _purchaseorderRepo;
        private readonly IRepository<PurchaseOrderDetail> _purchaseorderdetailRepo;
        private readonly IRepository<PurchaseOrderTaxDetail> _purchaseordertaxdetailRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<Supplier> _supplierRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<Company> _companyRepo;
        private readonly IRepository<Template> _templateRepo;
        private readonly IRepository<SupplierMaterial> _suppliermaterialRepo;
        private readonly IRepository<Unit> _unitRepo;
        private readonly IRepository<UnitConversion> _unitConversionRepo;
        private readonly IRepository<TaxTemplateMapping> _taxtemplatemappingRepo;
        private readonly IRepository<Tax> _taxRepo;
        private readonly IEmailSender _emailSender;
        private readonly IEmailTemplateProvider _emailTemplateProvider;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IMaterialLocationWiseStockAppService _materiallocationwisestockappservice;
        private readonly IRepository<Invoice> _invoiceRepo;
        private readonly IRepository<InvoiceDetail> _invoicedetailRepo;
        private readonly ISupplierMaterialAppService _suppliermaterialAppService;
        private readonly IRepository<InterTransferDetail> _intertransferDetailRepo;
        private readonly IRepository<InterTransfer> _intertransferRepo;
        private readonly IRepository<CaterQuotationHeader> _caterQuotationHeaderRepository;
        private readonly IRepository<CaterQuoteRequiredMaterial> _caterQuoteRequiredMaterialRepo;
        private readonly IRepository<CaterQuoteRequiredMaterialVsPurchaseOrderLink> _caterQuoteRequiredMaterialVsPoLinkRepo;
        private readonly IRepository<CaterQuoteRequiredMaterialVsMenuLink> _caterQuoteRequiredMaterialVsMenuLinkRepo;
        private readonly ILocationAppService _locService;

        public PurchaseOrderExtendedAppService(IPurchaseOrderManager purchaseorderManager,
            IRepository<PurchaseOrder> purchaseOrderRepo,
            IPurchaseOrderListExcelExporter purchaseorderExporter, IRepository<PurchaseOrderDetail> purchaseorderdetailRepo,
            IRepository<PurchaseOrderTaxDetail> purchaseordertaxdetailRepo,
            IRepository<Material> materialRepo,
            IRepository<Supplier> supplierRepo,
            IRepository<Location> locationRepo,
            IRepository<Template> templateRepo,
            IRepository<SupplierMaterial> suppliermaterialRepo,
            IRepository<Unit> unitRepo,
            IRepository<TaxTemplateMapping> taxtemplatemappingRepo,
            IRepository<Tax> taxRepo,
            IEmailTemplateProvider emailTemplateProvider,
            IEmailSender emailSender,
            IRepository<Company> companyRepo,
            IUnitOfWorkManager unitOfWorkManager,
            IMaterialLocationWiseStockAppService materiallocationwisestockappservice,
            IRepository<Invoice> invoiceRepo,
            IRepository<InvoiceDetail> invoicedetailRepo,
            ISupplierMaterialAppService suppliermaterialAppService,
            IRepository<UnitConversion> unitConversionRepo,
            IRepository<InterTransferDetail> intertransferDetailRepo,
            IRepository<InterTransfer> intertransferRepo,
            IRepository<CaterQuotationHeader> caterQuotationHeaderRepository,
            IRepository<CaterQuoteRequiredMaterial> caterQuoteRequiredMaterialRepo,
            IRepository<CaterQuoteRequiredMaterialVsPurchaseOrderLink> caterQuoteRequiredMaterialVsPoLinkRepo,
            IRepository<CaterQuoteRequiredMaterialVsMenuLink> caterQuoteRequiredMaterialVsMenuLinkRepo,
             ILocationAppService locService
            )
        {
            _purchaseorderManager = purchaseorderManager;
            _purchaseorderRepo = purchaseOrderRepo;
            _purchaseorderExporter = purchaseorderExporter;
            _purchaseorderdetailRepo = purchaseorderdetailRepo;
            _purchaseordertaxdetailRepo = purchaseordertaxdetailRepo;
            _materialRepo = materialRepo;
            _supplierRepo = supplierRepo;
            _locationRepo = locationRepo;
            _templateRepo = templateRepo;
            _suppliermaterialRepo = suppliermaterialRepo;
            _unitRepo = unitRepo;
            _taxRepo = taxRepo;
            _taxtemplatemappingRepo = taxtemplatemappingRepo;
            _emailSender = emailSender;
            _emailTemplateProvider = emailTemplateProvider;
            _companyRepo = companyRepo;
            _unitOfWorkManager = unitOfWorkManager;
            _materiallocationwisestockappservice = materiallocationwisestockappservice;
            _invoiceRepo = invoiceRepo;
            _invoicedetailRepo = invoicedetailRepo;
            _suppliermaterialAppService = suppliermaterialAppService;
            _unitConversionRepo = unitConversionRepo;
            _intertransferRepo = intertransferRepo;
            _intertransferDetailRepo = intertransferDetailRepo;
            _caterQuotationHeaderRepository = caterQuotationHeaderRepository;
            _caterQuoteRequiredMaterialRepo = caterQuoteRequiredMaterialRepo;
            _caterQuoteRequiredMaterialVsPoLinkRepo = caterQuoteRequiredMaterialVsPoLinkRepo;
            _caterQuoteRequiredMaterialVsMenuLinkRepo = caterQuoteRequiredMaterialVsMenuLinkRepo;
            _locService = locService;
        }

        public async Task<List<AutoPurchaseOrderConsolidatedDto>> GetAutoPoDetails(LocationInputDto input)
        {
            var matCurrentStock = await _materiallocationwisestockappservice.GetView(new GetMaterialLocationWiseStockInput
            {
                LocationRefId = input.LocationRefId,
                LessThanReorderLevel = true,
                LiveStock = true,
                MaxResultCount = AppConsts.MaxPageSize
            });

            List<MaterialLocationWiseStockViewDto> lstMaterialsToBeOrdered = matCurrentStock.Items.MapTo<List<MaterialLocationWiseStockViewDto>>();

            lstMaterialsToBeOrdered = lstMaterialsToBeOrdered.Where(t => t.ReOrderLevel > 0 && t.MaximumStock > 0 && t.MinimumStock > 0).ToList();

            List<int> arrMaterialRefIds = lstMaterialsToBeOrdered.Select(t => t.MaterialRefId).ToList();
            var rsInvoiceDetails = await _invoicedetailRepo.GetAll().Where(t => arrMaterialRefIds.Contains(t.MaterialRefId)).ToListAsync();
            List<int> arrInvoiceRefIds = rsInvoiceDetails.Select(t => t.InvoiceRefId).ToList();
            var rsInvoices = await _invoiceRepo.GetAllListAsync(t => arrInvoiceRefIds.Contains(t.Id));
            var rsMaterials = await _materialRepo.GetAllListAsync();
            var rsUnits = await _unitRepo.GetAllListAsync();
            var rsUnitConversion = await _unitConversionRepo.GetAll().ToListAsync();
            var rsSuppliers = await _supplierRepo.GetAllListAsync();

            List<AutoPoDetail> consolidatedMaterialsToOrdered = new List<AutoPoDetail>();

            var rsSupMaterial = await _suppliermaterialRepo.GetAllListAsync();

            foreach (var lst in lstMaterialsToBeOrdered)
            {
                AutoPoDetail newDto = new AutoPoDetail();
                newDto.MaterialRefId = lst.MaterialRefId;
                newDto.Barcode = lst.Barcode;
                newDto.MaterialRefName = lst.MaterialRefName;
                newDto.OnHand = lst.LiveStock;
                newDto.Rol = lst.ReOrderLevel;
                newDto.MaximumStock = lst.MaximumStock;

                var material = rsMaterials.FirstOrDefault(t => t.Id == lst.MaterialRefId);

                var unit = rsUnits.FirstOrDefault(t => t.Id == material.DefaultUnitId);

                newDto.UnitRefId = material.DefaultUnitId;
                newDto.UnitRefName = unit.Name;

                var invoiceDetailRecords = rsInvoiceDetails.Where(t => t.MaterialRefId == lst.MaterialRefId);

                int lastInvoiceRefId = 0;
                if (invoiceDetailRecords.Count() > 0)
                {
                    lastInvoiceRefId = invoiceDetailRecords.Where(t => t.Price > 0).Max(t => t == null ? 0 : t.InvoiceRefId);
                }

                if (lastInvoiceRefId > 0)
                {
                    var lastInvoice = rsInvoices.FirstOrDefault(t => t.Id == lastInvoiceRefId);

                    var lastInvoiceDetail = invoiceDetailRecords.FirstOrDefault(t => t.InvoiceRefId == lastInvoiceRefId && t.MaterialRefId == lst.MaterialRefId && t.Price > 0);

                    if (lastInvoice != null && lastInvoiceDetail != null)
                    {
                        newDto.Price = lastInvoiceDetail.Price;
                        newDto.SupplierRefId = lastInvoice.SupplierRefId;
                    }
                    var sm = rsSupMaterial.FirstOrDefault(t => t.MaterialRefId == lst.MaterialRefId && t.SupplierRefId == lastInvoice.SupplierRefId && t.LocationRefId == input.LocationRefId);
                    if (sm == null)
                    {
                        sm = rsSupMaterial.FirstOrDefault(t => t.MaterialRefId == lst.MaterialRefId && t.SupplierRefId == lastInvoice.SupplierRefId && t.LocationRefId == null);
                    }

                    if (sm != null)
                    {
                        newDto.SupplierRefId = sm.SupplierRefId;
                        newDto.MinimumOrderQuantity = sm.MinimumOrderQuantity;
                        newDto.MaximumOrderQuantity = sm.MaximumOrderQuantity;
                        newDto.UnitRefIdForMinOrder = sm.UnitRefId;
                        newDto.UnitRefNameForMinOrder = rsUnits.FirstOrDefault(t => t.Id == sm.UnitRefId).Name;
                        if (newDto.Price == 0)
                        {
                            #region SupMatPrice
                            if (newDto.UnitRefId == sm.UnitRefId)
                                newDto.Price = sm.MaterialPrice;
                            else
                            {
                                decimal conversionFactor = 1;
                                if (newDto.UnitRefId == sm.UnitRefId)
                                    conversionFactor = 1;
                                else
                                {
                                    var conv =
                                        rsUnitConversion.First(
                                            t => t.BaseUnitId == sm.UnitRefId && t.RefUnitId == newDto.UnitRefId);
                                    if (conv != null)
                                    {
                                        conversionFactor = conv.Conversion;
                                    }
                                    else
                                    {
                                        var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == sm.UnitRefId);
                                        if (baseUnit == null)
                                        {
                                            throw new UserFriendlyException(L("UnitIdDoesNotExist", sm.UnitRefId));
                                        }
                                        var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == newDto.UnitRefId);
                                        if (refUnit == null)
                                        {
                                            throw new UserFriendlyException(L("UnitIdDoesNotExist", newDto.UnitRefId));
                                        }
                                        throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
                                    }
                                }
                                newDto.Price = sm.MaterialPrice * 1 / conversionFactor;
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        newDto.MinimumOrderQuantity = 0;
                        newDto.MaximumOrderQuantity = 0;
                        newDto.UnitRefIdForMinOrder = newDto.UnitRefId;
                        newDto.UnitRefNameForMinOrder = newDto.UnitRefName;
                    }
                }
                else
                {
                    var sm = rsSupMaterial.Where(t => t.MaterialRefId == lst.MaterialRefId && t.LocationRefId == input.LocationRefId).OrderBy(t => t.MaterialPrice).FirstOrDefault();
                    if (sm == null)
                    {
                        sm = rsSupMaterial.Where(t => t.MaterialRefId == lst.MaterialRefId && t.LocationRefId == null).OrderBy(t => t.MaterialPrice).FirstOrDefault();
                    }
                    if (sm != null)
                    {
                        newDto.SupplierRefId = sm.SupplierRefId;
                        newDto.MinimumOrderQuantity = sm.MinimumOrderQuantity;
                        newDto.MaximumOrderQuantity = sm.MaximumOrderQuantity;
                        newDto.UnitRefIdForMinOrder = sm.UnitRefId;
                        newDto.UnitRefNameForMinOrder = rsUnits.FirstOrDefault(t => t.Id == sm.UnitRefId).Name;
                        #region SupMatPrice
                        if (newDto.UnitRefId == sm.UnitRefId)
                            newDto.Price = sm.MaterialPrice;
                        else
                        {
                            decimal conversionFactor = 1;
                            if (newDto.UnitRefId == sm.UnitRefId)
                                conversionFactor = 1;
                            else
                            {
                                var conv =
                                    rsUnitConversion.First(
                                        t => t.BaseUnitId == sm.UnitRefId && t.RefUnitId == newDto.UnitRefId);
                                if (conv != null)
                                {
                                    conversionFactor = conv.Conversion;
                                }
                                else
                                {
                                    var baseUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == sm.UnitRefId);
                                    if (baseUnit == null)
                                    {
                                        throw new UserFriendlyException(L("UnitIdDoesNotExist", sm.UnitRefId));
                                    }
                                    var refUnit = await _unitRepo.FirstOrDefaultAsync(t => t.Id == newDto.UnitRefId);
                                    if (refUnit == null)
                                    {
                                        throw new UserFriendlyException(L("UnitIdDoesNotExist", newDto.UnitRefId));
                                    }
                                    throw new UserFriendlyException(L("ConversionFactorNotExist", baseUnit.Name, refUnit.Name));
                                }
                            }
                            newDto.Price = sm.MaterialPrice * 1 / conversionFactor;
                        }
                        #endregion
                    }
                    else
                    {
                        if (rsSuppliers.Count > 0)
                        {
                            var sup = rsSuppliers.FirstOrDefault();
                            newDto.SupplierRefId = sup.Id;
                            newDto.SupplierExistFlag = true;
                        }
                        else
                        {
                            newDto.SupplierRefId = -1;
                            newDto.SupplierExistFlag = false;
                        }
                        newDto.Price = 0;
                        newDto.MinimumOrderQuantity = 0;
                        newDto.MaximumOrderQuantity = 0;
                        newDto.UnitRefIdForMinOrder = newDto.UnitRefId;
                        newDto.UnitRefNameForMinOrder = newDto.UnitRefName;
                    }
                }

                newDto.QuantityToBeOrdered = Math.Round(newDto.MaximumStock - newDto.OnHand, 0);

                var supMaterial = rsSupMaterial.FirstOrDefault(t => t.MaterialRefId == lst.MaterialRefId && t.SupplierRefId == newDto.SupplierRefId && t.LocationRefId == input.LocationRefId);
                if (supMaterial == null)
                {
                    supMaterial = rsSupMaterial.FirstOrDefault(t => t.MaterialRefId == lst.MaterialRefId && t.SupplierRefId == newDto.SupplierRefId && t.LocationRefId == null);
                }

                if (supMaterial != null)
                {
                    if (supMaterial.MinimumOrderQuantity > (newDto.MaximumStock - newDto.OnHand))
                    {
                        newDto.QuantityToBeOrdered = supMaterial.MinimumOrderQuantity;
                    }
                    else
                    {
                        newDto.QuantityToBeOrdered = Math.Round(newDto.MaximumStock - newDto.OnHand, 0);
                    }
                    if (supMaterial.MaximumOrderQuantity < newDto.QuantityToBeOrdered && supMaterial.MaximumOrderQuantity > 0)
                        newDto.QuantityToBeOrdered = supMaterial.MaximumOrderQuantity;
                }
                consolidatedMaterialsToOrdered.Add(newDto);
            }


            List<AutoPurchaseOrderConsolidatedDto> output = new List<AutoPurchaseOrderConsolidatedDto>();

            consolidatedMaterialsToOrdered = consolidatedMaterialsToOrdered.OrderBy(t => t.SupplierRefId).ToList();

            var rsLocation = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);

            foreach (var group in consolidatedMaterialsToOrdered.GroupBy(t => t.SupplierRefId))
            {

                var supplier = rsSuppliers.FirstOrDefault(t => t.Id == group.Key);

                if (supplier == null)
                    continue;

                PurchaseOrderEditDto poMas = new PurchaseOrderEditDto();

                if (rsLocation.HouseTransactionDate != null)
                    poMas.PoDate = rsLocation.HouseTransactionDate.Value;
                else
                    poMas.PoDate = DateTime.Today;
                poMas.ShippingLocationId = input.LocationRefId;
                poMas.ShippingLocationRefName = rsLocation.Name;
                poMas.SupplierRefId = group.Key;
                poMas.SupplierRefName = supplier.SupplierName;
                poMas.QuoteReference = "";
                poMas.LocationRefId = rsLocation.Id;
                poMas.LocationRefName = rsLocation.Name;
                poMas.DeliveryDateExpected = poMas.PoDate.AddDays(1);

                List<PurchaseOrderDetailViewDto> poDetailList = new List<PurchaseOrderDetailViewDto>();

                var materialWithSupplierDtos = await _suppliermaterialAppService.GetMaterialBasedOnSupplierWithPrice(new SentLocationAndSupplier { LocationRefId = input.LocationRefId, SupplierRefId = group.Key });

                var materialListForSupplier = materialWithSupplierDtos.Items;

                foreach (var mattobeorder in group)
                {
                    var material = materialListForSupplier.FirstOrDefault(t => t.MaterialRefId == mattobeorder.MaterialRefId);

                    if (material == null)
                        continue;

                    decimal QtytoOrdered = mattobeorder.QuantityToBeOrdered - material.AlreadyOrderedQuantity;

                    if (QtytoOrdered <= 0)
                        continue;

                    PurchaseOrderDetailViewDto poDet = new PurchaseOrderDetailViewDto();
                    poDet.MaterialRefId = mattobeorder.MaterialRefId;
                    poDet.Barcode = mattobeorder.Barcode;
                    poDet.MaterialRefName = mattobeorder.MaterialRefName;
                    poDet.QtyOrdered = QtytoOrdered;
                    poDet.UnitRefId = mattobeorder.UnitRefId;
                    poDet.UnitRefName = mattobeorder.UnitRefName;
                    poDet.DefaultUnitId = mattobeorder.UnitRefId;
                    poDet.Uom = mattobeorder.UnitRefName;
                    poDet.Price = mattobeorder.Price;
                    poDet.DiscountValue = 0;
                    poDet.DiscountOption = "";
                    poDet.AlreadyOrderedQuantity = material.AlreadyOrderedQuantity;
                    poDet.CurrentInHand = material.CurrentInHand;
                    poDet.TaxForMaterial = material.TaxForMaterial;
                    poDet.ApplicableTaxes = material.ApplicableTaxes;
                    poDet.TotalAmount = poDet.Price * QtytoOrdered;
                    poDet.UnitRefIdForMinOrder = mattobeorder.UnitRefIdForMinOrder;
                    poDet.UnitRefNameForMinOrder = mattobeorder.UnitRefNameForMinOrder;
                    poDet.MinimumOrderQuantity = mattobeorder.MinimumOrderQuantity;
                    poDet.MaximumOrderQuantity = mattobeorder.MaximumOrderQuantity;
                    poDetailList.Add(poDet);
                }

                CreateOrUpdatePurchaseOrderInput poDto = new CreateOrUpdatePurchaseOrderInput();
                if (poDetailList.Count > 0)
                {
                    poDto.PurchaseOrder = poMas;
                    poDto.PurchaseOrderDetail = poDetailList;

                    AutoPurchaseOrderConsolidatedDto detailDto = new AutoPurchaseOrderConsolidatedDto();
                    detailDto.SupplierRefId = group.Key;
                    detailDto.SupplierRefName = poMas.SupplierRefName;
                    detailDto.NoOfProducts = poDetailList.Count;
                    detailDto.AutoPoDto = poDto;
                    detailDto.Remarks = "";
                    if (group.Where(t => t.SupplierExistFlag == true).ToList().Count > 0)
                    {
                        detailDto.Remarks = L("AutoPoAlert1");
                    }
                    output.Add(detailDto);
                }

            }

            return output;
        }


        public async Task<List<AutoPurchaseOrderConsolidatedDto>> GetAutoPoBasedOnCaterRequest(GetCaterRequestMaterialAsPO input)
        {
            var lstMaterialsToBeOrdered = input.RequestMaterialsToBeOrdered;

            List<int> arrMaterialRefIds = lstMaterialsToBeOrdered.Select(t => t.MaterialRefId).ToList();
            var rsInvoiceDetails = await _invoicedetailRepo.GetAll().Where(t => arrMaterialRefIds.Contains(t.MaterialRefId)).ToListAsync();
            List<int> arrInvoiceRefIds = rsInvoiceDetails.Select(t => t.InvoiceRefId).ToList();
            var rsInvoices = await _invoiceRepo.GetAllListAsync(t => arrInvoiceRefIds.Contains(t.Id));
            var rsMaterials = await _materialRepo.GetAllListAsync();
            var rsUnits = await _unitRepo.GetAllListAsync();
            var rsUnitConversion = await _unitConversionRepo.GetAll().ToListAsync();
            var rsSuppliers = await _supplierRepo.GetAllListAsync();

            List<AutoPoDetail> consolidatedMaterialsToOrdered = new List<AutoPoDetail>();

            var rsSupMaterial = await _suppliermaterialRepo.GetAllListAsync();

            foreach (var lst in lstMaterialsToBeOrdered)
            {
                AutoPoDetail newDto = new AutoPoDetail
                {
                    SupplierRefId = lst.DefaultSupplier.SupplierRefId,
                    MinimumOrderQuantity = lst.DefaultSupplier.MinimumOrderQuantity,
                    MaximumOrderQuantity = lst.DefaultSupplier.MaximumOrderQuantity,
                    Price = lst.DefaultSupplier.MaterialPrice,
                    MaterialRefId = lst.MaterialRefId,
                    Barcode = lst.Barcode,
                    MaterialRefName = lst.MaterialRefName,
                    OnHand = lst.LiveStock,
                    Rol = lst.ReOrderLevel,
                    QuantityToBeOrdered = lst.PoOrderQuantity,
                    UnitRefId = lst.UnitRefId,
                    UnitRefName = lst.Uom,
                    AlreadyOrderedQuantity = 0,
                    UnitRefIdForMinOrder = lst.DefaultSupplier.UnitRefIdForMinOrder,
                    UnitRefNameForMinOrder = lst.DefaultSupplier.UnitRefNameForMinOrder,
                    SupplierExistFlag = lst.DefaultSupplier.SupplierRefId == 0 ? false : true,
                    CaterHeaderRefIds = lst.CaterHeaderRefIds,
                    OmitQueueInOrder = lst.OmitQueueInOrder
                };
                consolidatedMaterialsToOrdered.Add(newDto);
            }

            List<AutoPurchaseOrderConsolidatedDto> output = new List<AutoPurchaseOrderConsolidatedDto>();

            consolidatedMaterialsToOrdered = consolidatedMaterialsToOrdered.OrderBy(t => t.SupplierRefId).ToList();

            var rsLocation = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
            var rsShippingLocation = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.ShippingLocationId);

            foreach (var group in consolidatedMaterialsToOrdered.GroupBy(t => t.SupplierRefId))
            {
                var supplier = rsSuppliers.FirstOrDefault(t => t.Id == group.Key);

                if (supplier == null)
                    continue;

                PurchaseOrderEditDto poMas = new PurchaseOrderEditDto();

                if (rsLocation.HouseTransactionDate != null)
                    poMas.PoDate = rsLocation.HouseTransactionDate.Value;
                else
                    poMas.PoDate = DateTime.Today;
                poMas.ShippingLocationId = input.ShippingLocationId;
                poMas.ShippingLocationRefName = rsShippingLocation.Name;
                poMas.SupplierRefId = group.Key;
                poMas.SupplierRefName = supplier.SupplierName;
                poMas.QuoteReference = "";
                poMas.LocationRefId = rsLocation.Id;
                poMas.LocationRefName = rsLocation.Name;
                poMas.DeliveryDateExpected = input.DeliveryDateExpected;

                List<PurchaseOrderDetailViewDto> poDetailList = new List<PurchaseOrderDetailViewDto>();
                List<int> materialRefIds = group.Select(t => t.MaterialRefId).ToList();
                var materialWithSupplierDtos = await _suppliermaterialAppService.GetMaterialBasedOnSupplierWithPrice(new SentLocationAndSupplier { LocationRefId = input.LocationRefId, SupplierRefId = group.Key, MaterialRefIds = materialRefIds, LoadOnlyLinkedMaterials = true });

                var materialListForSupplier = materialWithSupplierDtos.Items;

                foreach (var mattobeorder in group)
                {
                    var material = materialListForSupplier.FirstOrDefault(t => t.MaterialRefId == mattobeorder.MaterialRefId);

                    if (material == null)
                        continue;

                    decimal QtytoOrdered = mattobeorder.QuantityToBeOrdered - (mattobeorder.OmitQueueInOrder == true ? 0 : material.AlreadyOrderedQuantity);

                    if (QtytoOrdered <= 0)
                        continue;

                    PurchaseOrderDetailViewDto poDet = new PurchaseOrderDetailViewDto();
                    poDet.MaterialRefId = mattobeorder.MaterialRefId;
                    poDet.Barcode = mattobeorder.Barcode;
                    poDet.MaterialRefName = mattobeorder.MaterialRefName;
                    poDet.QtyOrdered = QtytoOrdered;
                    poDet.UnitRefId = mattobeorder.UnitRefId;
                    poDet.UnitRefName = mattobeorder.UnitRefName;
                    poDet.DefaultUnitId = mattobeorder.UnitRefId;
                    poDet.Uom = mattobeorder.UnitRefName;
                    poDet.Price = mattobeorder.Price;
                    poDet.DiscountValue = 0;
                    poDet.DiscountOption = "";
                    poDet.AlreadyOrderedQuantity = material.AlreadyOrderedQuantity;
                    poDet.CurrentInHand = material.CurrentInHand;
                    poDet.TaxForMaterial = material.TaxForMaterial;
                    poDet.ApplicableTaxes = material.ApplicableTaxes;
                    poDet.TotalAmount = poDet.Price * QtytoOrdered;
                    poDet.UnitRefIdForMinOrder = mattobeorder.UnitRefIdForMinOrder;
                    poDet.UnitRefNameForMinOrder = mattobeorder.UnitRefNameForMinOrder;
                    poDet.MinimumOrderQuantity = mattobeorder.MinimumOrderQuantity;
                    poDet.MaximumOrderQuantity = mattobeorder.MaximumOrderQuantity;
                    poDet.InterTransferDetailWithPORefIds = new List<InterTransferDetailWithPORefIds>();
                    poDet.CaterHeaderRefIds = mattobeorder.CaterHeaderRefIds;
                    poDetailList.Add(poDet);
                }

                CreateOrUpdatePurchaseOrderInput poDto = new CreateOrUpdatePurchaseOrderInput();
                if (poDetailList.Count > 0)
                {
                    poDto.PurchaseOrder = poMas;
                    poDto.PurchaseOrderDetail = poDetailList;

                    AutoPurchaseOrderConsolidatedDto detailDto = new AutoPurchaseOrderConsolidatedDto();
                    detailDto.SupplierRefId = group.Key;
                    detailDto.SupplierRefName = poMas.SupplierRefName;
                    detailDto.NoOfProducts = poDetailList.Count;
                    detailDto.AutoPoDto = poDto;
                    detailDto.Remarks = "";
                    if (group.Where(t => t.SupplierExistFlag == true).ToList().Count > 0)
                    {
                        detailDto.Remarks = L("AutoPoAlert1");
                    }
                    output.Add(detailDto);
                }

            }

            return output;
        }


        public async Task<List<AutoPurchaseOrderConsolidatedDto>> GetAutoPoBasedOnTransferRequest(GetRequestAsPO input)
        {
            var lstMaterialsToBeOrdered = input.RequestMaterialsToBeOrdered;

            List<int> arrMaterialRefIds = lstMaterialsToBeOrdered.Select(t => t.MaterialRefId).ToList();
            var rsInvoiceDetails = await _invoicedetailRepo.GetAll().Where(t => arrMaterialRefIds.Contains(t.MaterialRefId)).ToListAsync();
            List<int> arrInvoiceRefIds = rsInvoiceDetails.Select(t => t.InvoiceRefId).ToList();
            var rsInvoices = await _invoiceRepo.GetAllListAsync(t => arrInvoiceRefIds.Contains(t.Id));
            var rsMaterials = await _materialRepo.GetAllListAsync();
            var rsUnits = await _unitRepo.GetAllListAsync();
            var rsUnitConversion = await _unitConversionRepo.GetAll().ToListAsync();
            var rsSuppliers = await _supplierRepo.GetAllListAsync();

            List<AutoPoDetail> consolidatedMaterialsToOrdered = new List<AutoPoDetail>();

            var rsSupMaterial = await _suppliermaterialRepo.GetAllListAsync();

            foreach (var lst in lstMaterialsToBeOrdered)
            {
                AutoPoDetail newDto = new AutoPoDetail
                {
                    SupplierRefId = lst.DefaultSupplier.SupplierRefId,
                    MinimumOrderQuantity = lst.DefaultSupplier.MinimumOrderQuantity,
                    MaximumOrderQuantity = lst.DefaultSupplier.MaximumOrderQuantity,
                    Price = lst.DefaultSupplier.MaterialPrice,
                    MaterialRefId = lst.MaterialRefId,
                    Barcode = lst.Barcode,
                    MaterialRefName = lst.MaterialRefName,
                    OnHand = lst.LiveStock,
                    Rol = lst.ReOrderLevel,
                    QuantityToBeOrdered = lst.PoOrderQuantity,
                    UnitRefId = lst.UnitRefId,
                    UnitRefName = lst.Uom,
                    AlreadyOrderedQuantity = 0,
                    UnitRefIdForMinOrder = lst.DefaultSupplier.UnitRefIdForMinOrder,
                    UnitRefNameForMinOrder = lst.DefaultSupplier.UnitRefNameForMinOrder,
                    SupplierExistFlag = lst.DefaultSupplier.SupplierRefId == 0 ? false : true,
                    InterTransferDetailWithPORefIds = lst.InterTransferDetailWithPORefIds,
                    OmitQueueInOrder = lst.OmitQueueInOrder
                };
                consolidatedMaterialsToOrdered.Add(newDto);
            }

            List<AutoPurchaseOrderConsolidatedDto> output = new List<AutoPurchaseOrderConsolidatedDto>();

            consolidatedMaterialsToOrdered = consolidatedMaterialsToOrdered.OrderBy(t => t.SupplierRefId).ToList();

            var rsLocation = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
            var rsShippingLocation = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.ShippingLocationId);

            foreach (var group in consolidatedMaterialsToOrdered.GroupBy(t => t.SupplierRefId))
            {
                var supplier = rsSuppliers.FirstOrDefault(t => t.Id == group.Key);

                if (supplier == null)
                    continue;

                PurchaseOrderEditDto poMas = new PurchaseOrderEditDto();

                if (rsLocation.HouseTransactionDate != null)
                    poMas.PoDate = rsLocation.HouseTransactionDate.Value;
                else
                    poMas.PoDate = DateTime.Today;
                poMas.ShippingLocationId = input.ShippingLocationId;
                poMas.ShippingLocationRefName = rsShippingLocation.Name;
                poMas.SupplierRefId = group.Key;
                poMas.SupplierRefName = supplier.SupplierName;
                poMas.QuoteReference = "";
                poMas.LocationRefId = rsLocation.Id;
                poMas.LocationRefName = rsLocation.Name;
                poMas.DeliveryDateExpected = input.DeliveryDateExpected;

                List<PurchaseOrderDetailViewDto> poDetailList = new List<PurchaseOrderDetailViewDto>();
                List<int> materialRefIds = group.Select(t => t.MaterialRefId).ToList();
                var materialWithSupplierDtos = await _suppliermaterialAppService.GetMaterialBasedOnSupplierWithPrice(new SentLocationAndSupplier { LocationRefId = input.LocationRefId, SupplierRefId = group.Key, MaterialRefIds = materialRefIds, LoadOnlyLinkedMaterials = true });

                var materialListForSupplier = materialWithSupplierDtos.Items;

                foreach (var mattobeorder in group)
                {
                    var material = materialListForSupplier.FirstOrDefault(t => t.MaterialRefId == mattobeorder.MaterialRefId);

                    if (material == null)
                        continue;

                    decimal QtytoOrdered = mattobeorder.QuantityToBeOrdered - (mattobeorder.OmitQueueInOrder == true ? 0 : material.AlreadyOrderedQuantity);

                    if (QtytoOrdered <= 0)
                        continue;

                    PurchaseOrderDetailViewDto poDet = new PurchaseOrderDetailViewDto();
                    poDet.MaterialRefId = mattobeorder.MaterialRefId;
                    poDet.Barcode = mattobeorder.Barcode;
                    poDet.MaterialRefName = mattobeorder.MaterialRefName;
                    poDet.QtyOrdered = QtytoOrdered;
                    poDet.UnitRefId = mattobeorder.UnitRefId;
                    poDet.UnitRefName = mattobeorder.UnitRefName;
                    poDet.DefaultUnitId = mattobeorder.UnitRefId;
                    poDet.Uom = mattobeorder.UnitRefName;
                    poDet.Price = mattobeorder.Price;
                    poDet.DiscountValue = 0;
                    poDet.DiscountOption = "";
                    poDet.AlreadyOrderedQuantity = material.AlreadyOrderedQuantity;
                    poDet.CurrentInHand = material.CurrentInHand;
                    poDet.TaxForMaterial = material.TaxForMaterial;
                    poDet.ApplicableTaxes = material.ApplicableTaxes;
                    poDet.TotalAmount = poDet.Price * QtytoOrdered;
                    poDet.UnitRefIdForMinOrder = mattobeorder.UnitRefIdForMinOrder;
                    poDet.UnitRefNameForMinOrder = mattobeorder.UnitRefNameForMinOrder;
                    poDet.MinimumOrderQuantity = mattobeorder.MinimumOrderQuantity;
                    poDet.MaximumOrderQuantity = mattobeorder.MaximumOrderQuantity;
                    poDet.InterTransferDetailWithPORefIds = mattobeorder.InterTransferDetailWithPORefIds;
                    poDet.CaterHeaderRefIds = new List<CaterHeaderRefIds>();
                    poDetailList.Add(poDet);
                }

                CreateOrUpdatePurchaseOrderInput poDto = new CreateOrUpdatePurchaseOrderInput();
                if (poDetailList.Count > 0)
                {
                    poDto.PurchaseOrder = poMas;
                    poDto.PurchaseOrderDetail = poDetailList;

                    AutoPurchaseOrderConsolidatedDto detailDto = new AutoPurchaseOrderConsolidatedDto();
                    detailDto.SupplierRefId = group.Key;
                    detailDto.SupplierRefName = poMas.SupplierRefName;
                    detailDto.NoOfProducts = poDetailList.Count;
                    detailDto.AutoPoDto = poDto;
                    detailDto.Remarks = "";
                    if (group.Where(t => t.SupplierExistFlag == true).ToList().Count > 0)
                    {
                        detailDto.Remarks = L("AutoPoAlert1");
                    }
                    output.Add(detailDto);
                }
            }
            return output;
        }


        //public async Task<PagedResultOutput<PurchaseOrderListWithRefNameDto>> GetAllWithRefName(GetPurchaseOrderInput input)
        //{
        //    Debug.WriteLine("PO Basic GetAllWithRefName Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));

        //    var rsSupplier = _supplierRepo.GetAll().WhereIf(!input.Filter.IsNullOrEmpty(), p => p.SupplierName.Contains(input.Filter));

        //    var rsMaterial = _materialRepo.GetAll().Where(t => t.Id == input.MaterialRefId);

        //    var poOrder = _purchaseorderRepo.GetAll();

        //    if (input.PurchaseOrderIdList!=null && input.PurchaseOrderIdList.Count > 0)
        //    {
        //        input.PendingPoIds = input.PurchaseOrderIdList;
        //    }

        //    if (input.PendingPoIds.Count > 0)
        //    {
        //        poOrder = poOrder.Where(t => input.PendingPoIds.Contains(t.Id));
        //    }
        //    else
        //    {
        //        List<int> selectedLocationRefIds = new List<int>();
              
        //        #region Location Filter
        //        if (input.Locations != null && input.Locations.Any())
        //        {
        //            selectedLocationRefIds = input.Locations.Select(a => a.Id).ToList();
        //        }
        //        else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
        //                 && !input.LocationGroup.Locations.Any())
        //        {
        //            selectedLocationRefIds = _locService.GetLocationForUserAndGroup(new LocationGroupDto
        //            {
        //                Locations = input.LocationGroup.Locations,
        //                Group = false
        //            });
        //        }
        //        else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
        //        {
        //            selectedLocationRefIds = _locService.GetLocationForUserAndGroup(new LocationGroupDto
        //            {
        //                Locations = input.LocationGroup.Locations,
        //                Group = false
        //            });
        //        }
        //        else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
        //        {
        //            selectedLocationRefIds = _locService.GetLocationForUserAndGroup(new LocationGroupDto
        //            {
        //                Groups = input.LocationGroup.Groups,
        //                Group = true
        //            });
        //        }
        //        else if (input.LocationGroup == null)
        //        {
        //            if (input.UserId > 0)
        //            {
        //                selectedLocationRefIds = _locService.GetLocationForUserAndGroup(new LocationGroupDto
        //                {
        //                    Locations = new List<SimpleLocationDto>(),
        //                    Group = false,
        //                    UserId = input.UserId
        //                });
        //            }
        //        }
        //        #endregion

        //        if (input.DefaultLocationRefId > 0)
        //            selectedLocationRefIds.Add(input.DefaultLocationRefId);

        //        poOrder = poOrder.Where(t => selectedLocationRefIds.Contains( t.LocationRefId))
        //                                               .WhereIf(!input.PoReferenceCode.IsNullOrEmpty(), t => t.PoReferenceCode.Contains(input.PoReferenceCode));

        //        if (input.StartDate != null && input.EndDate != null)
        //        {
        //            poOrder = poOrder.Where(t => t.PoDate >= input.StartDate && t.PoDate <= input.EndDate);
        //        }
        //    }

        //    if (input.MaterialRefId != null)
        //    {
        //        poOrder = (from poMas in poOrder
        //                   join podet in _purchaseorderdetailRepo.GetAll().Where(t => t.MaterialRefId == input.MaterialRefId)
        //                   on poMas.Id equals podet.PoRefId
        //                   select poMas);
        //    }



        //    var allItems = (from po in poOrder.WhereIf(input.Id.HasValue, t=>t.Id==input.Id)
        //                    join sup in rsSupplier
        //                    on po.SupplierRefId equals sup.Id
        //                    join loc in _locationRepo.GetAll() on po.ShippingLocationId equals loc.Id
        //                    join poLoc in _locationRepo.GetAll() on po.LocationRefId equals poLoc.Id
        //                    select new PurchaseOrderListWithRefNameDto()
        //                    {
        //                        Id = po.Id,
        //                        PoDate = po.PoDate,
        //                        PoReferenceCode = po.PoReferenceCode,
        //                        ShippingLocationId = po.ShippingLocationId,
        //                        ShippingLocationName = loc.Name,
        //                        QuoteReference = po.QuoteReference,
        //                        CreditDays = po.CreditDays,
        //                        SupplierRefId = po.SupplierRefId,
        //                        SupplierName = sup.SupplierName,
        //                        PoNetAmount = po.PoNetAmount,
        //                        TenantId = po.TenantId,
        //                        Remarks = po.Remarks,
        //                        LocationRefId = po.LocationRefId,
        //                        LocationRefName = poLoc.Code,
        //                        CreationTime = po.CreationTime,
        //                        PoCurrentStatus = po.PoCurrentStatus,
        //                        ApprovedTime = po.ApprovedTime,
        //                        CreatorUserId = po.CreatorUserId,
        //                        PoBasedOnCater = po.PoBasedOnCater,
        //                        PoBasedOnInterTransferRequest = po.PoBasedOnInterTransferRequest
        //                    });

        //    var sortMenuItems = await allItems
        //       .OrderBy(input.Sorting)
        //       .PageBy(input)
        //       .ToListAsync();

        //    var allListDtos = sortMenuItems.MapTo<List<PurchaseOrderListWithRefNameDto>>();

        //    var anyCaterExists = allListDtos.Exists(t => t.PoBasedOnCater == true);

        //    List<CaterQuoteRequiredMaterialVsPurchaseOrderLinkListDto> rsCaterPurchaseOrder = new List<CaterQuoteRequiredMaterialVsPurchaseOrderLinkListDto>();
        //    List<QuotationHeaderListDto> allCaterHeaderListDtos = new List<QuotationHeaderListDto>();
        //    List<CaterQuoteRequiredMaterialListDto> caterQuoteRequiredMaterialListDtos = new List<CaterQuoteRequiredMaterialListDto>();
        //    List<int> arrHeaderRefIds = new List<int>();
        //    List<int> arrCaterMaterialDataRefIds = new List<int>();
        //    if (anyCaterExists)
        //    {
        //        List<int> arrPoRefIds = allListDtos.Select(t => t.Id).ToList();
        //        var tempCaterPoLink = await _caterQuoteRequiredMaterialVsPoLinkRepo.GetAllListAsync(t => arrPoRefIds.Contains(t.PurchaseOrderRefId));
        //        rsCaterPurchaseOrder = tempCaterPoLink.MapTo<List<CaterQuoteRequiredMaterialVsPurchaseOrderLinkListDto>>();

        //        arrCaterMaterialDataRefIds = rsCaterPurchaseOrder.Select(t => t.CaterQuoteRequiredMaterialDataRefId).ToList();

        //        var tempCaterRequired = await _caterQuoteRequiredMaterialRepo.GetAll().Where(t => arrCaterMaterialDataRefIds.Contains(t.Id)).ToListAsync();
        //        caterQuoteRequiredMaterialListDtos = tempCaterRequired.MapTo<List<CaterQuoteRequiredMaterialListDto>>();
        //        arrHeaderRefIds = caterQuoteRequiredMaterialListDtos.Select(t => t.CaterQuoteHeaderId).ToList();
        //        arrHeaderRefIds = arrHeaderRefIds.Distinct().ToList();
        //        var iqCaterHeader = _caterQuotationHeaderRepository.GetAll().Where(t => arrHeaderRefIds.Contains(t.Id));

        //        var allCaterHeaderItems = await iqCaterHeader
        //           .Include(e => e.CaterCustomer)
        //           .Include(e => e.EventAddress)
        //           .Include(e => e.Location)
        //           .Where(c => c.TenantId == AbpSession.TenantId).ToListAsync(); ;

        //        allCaterHeaderListDtos = allCaterHeaderItems.Select(e =>
        //        {
        //            var dto = e.MapTo<QuotationHeaderListDto>();
        //            dto.CaterCustomerCustomerName = e.CaterCustomer.CustomerName;
        //            dto.LocationRefName = e.Location.Code + " - " + e.Location.Name;
        //            return dto;
        //        }).ToList();
        //    }

        //    foreach (var lst in allListDtos)
        //    {
        //        if (lst.PoCurrentStatus.Equals("P"))
        //        {
        //            lst.PoCurrentStatus = L("Pending");
        //        }
        //        else if (lst.PoCurrentStatus.Equals("A"))
        //        {
        //            lst.PoCurrentStatus = L("Approved");
        //        }
        //        else if (lst.PoCurrentStatus.Equals("X"))
        //        {
        //            lst.PoCurrentStatus = L("Declined");
        //        }
        //        else if (lst.PoCurrentStatus.Equals("C"))
        //        {
        //            lst.PoCurrentStatus = L("Completed");
        //        }
        //        else if (lst.PoCurrentStatus.Equals("H"))
        //        {
        //            lst.PoCurrentStatus = L("Partial");
        //        }

        //        if (lst.PoBasedOnCater == true)
        //        {
        //            var caterPurchaseOrder = rsCaterPurchaseOrder.Where(t => t.PurchaseOrderRefId == lst.Id);
        //            arrCaterMaterialDataRefIds = caterPurchaseOrder.Select(t => t.CaterQuoteRequiredMaterialDataRefId).ToList();

        //            var linkedCaterQuote = caterQuoteRequiredMaterialListDtos.Where(t => arrCaterMaterialDataRefIds.Contains(t.Id));
        //            var arrHeaderRefIdsforthisPo = linkedCaterQuote.Select(t => t.CaterQuoteHeaderId).ToList();
        //            arrHeaderRefIdsforthisPo = arrHeaderRefIdsforthisPo.Distinct().ToList();

        //            lst.CaterQuoteDetails = allCaterHeaderListDtos.Where(t => arrHeaderRefIdsforthisPo.Contains(t.Id)).ToList().MapTo<List<SimpleQuotationHeaderListDto>>();
        //        }

        //    }


        //    if (!input.PoStatus.IsNullOrEmpty())
        //    {
        //        //Get the culture property of the thread.
        //        CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
        //        //Create TextInfo object.
        //        TextInfo textInfo = cultureInfo.TextInfo;

        //        input.PoStatus = input.PoStatus.ToLowerInvariant();

        //        var searchStatus = textInfo.ToTitleCase(input.PoStatus);

        //        allListDtos = allListDtos.Where(t => t.PoCurrentStatus.Contains(searchStatus)).ToList();
        //    }



        //    var allItemCount = allListDtos.Count();
        //    Debug.WriteLine("PO Basic GetAllWithRefName End Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
        //    return new PagedResultOutput<PurchaseOrderListWithRefNameDto>(
        //        allItemCount,
        //        allListDtos
        //        );

        //}

        //public async Task<PoPendingDashBoardAlertDto> getPoPendingStatus(int userId, int inputLocationRefId)

        //{
        //    PoPendingDashBoardAlertDto output = new PoPendingDashBoardAlertDto();

        //    if (!PermissionChecker.IsGranted("Pages.Tenant.House.Transaction.PurchaseOrder.Approve"))
        //    {
        //        return output;
        //    }

        //    var locations = new List<int>();

        //    var allOrgani = await _locService.GetLocationBasedOnUser(new GetLocationInputBasedOnUser
        //    {
        //        UserId = userId
        //    });

        //    if (allOrgani.Items.Any()) locations = allOrgani.Items.Select(a => a.Id).ToList();
        //    DateTime checkingFrom = DateTime.Today.AddDays(-90);
        //    List<int> ids = new List<int>();
        //    var viewPendingDto = await _purchaseorderRepo.GetAll().Where(t => locations.Contains(t.LocationRefId) && t.PoCurrentStatus == "P" && DbFunctions.TruncateTime(t.PoDate) > checkingFrom).ToListAsync();

        //    ids = viewPendingDto.Select(t => t.Id).ToList();
        //    if (ids.Count > 0)
        //    {

        //        var pendingOrderDto = await GetAllWithRefName(new GetPurchaseOrderInput
        //        {
        //            Filter = "",
        //            MaxResultCount = AppConsts.MaxPageSize,
        //            Sorting = "Id",
        //            PendingPoIds = ids,
        //            DefaultLocationRefId = inputLocationRefId
        //        });

        //        output.PendingPoListDtos = pendingOrderDto.Items.MapTo<List<PurchaseOrderListWithRefNameDto>>();
        //        return output;
        //    }
        //    return output;
        //}


        //public async Task<FileDto> GetAllToExcel(GetPurchaseOrderInput input)
        //{
        //    input.MaxResultCount = AppConsts.MaxPageSize;

        //    var allList = await GetAllWithRefName(input);
        //    var allListDtos = allList.Items.MapTo<List<PurchaseOrderListWithRefNameDto>>();
        //    return _purchaseorderExporter.ExportToFile(allListDtos);
        //}

        //public async Task<GetPurchaseOrderForEditOutput> GetPurchaseOrderForEdit(NullableIdInput input)
        //{
        //    Debug.WriteLine("PO Basic GetPurchaseOrderForEdit Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));

        //    PurchaseOrderEditDto editDto;
        //    List<PurchaseOrderDetailViewDto> editDetailDto;
        //    List<SimpleQuotationHeaderListDto> editCaterQuoteDetails = new List<SimpleQuotationHeaderListDto>();
        //    TemplateSaveDto editTemplateDto = new TemplateSaveDto();

        //    if (input.Id.HasValue)
        //    {
        //        var hDto = await _purchaseorderRepo.GetAsync(input.Id.Value);

        //        editDto = hDto.MapTo<PurchaseOrderEditDto>();
        //        var supplier = await _supplierRepo.FirstOrDefaultAsync(t => t.Id == editDto.SupplierRefId);
        //        editDto.SupplierRefName = supplier.SupplierName;
        //        editDto.SupplierTaxApplicableByDefault = supplier.TaxApplicable;
        //        editDto.ShippingLocationRefName = _locationRepo.FirstOrDefault(t => t.Id == editDto.ShippingLocationId).Name;
        //        editDto.LocationRefName = _locationRepo.FirstOrDefault(t => t.Id == editDto.LocationRefId).Name;

        //        editDetailDto = await (from poDet in _purchaseorderdetailRepo.GetAll()
        //                               .Where(a => a.PoRefId == input.Id.Value)
        //                               join mat in _materialRepo.GetAll()
        //                               on poDet.MaterialRefId equals mat.Id
        //                               join un in _unitRepo.GetAll()
        //                               on mat.DefaultUnitId equals un.Id
        //                               join udet in _unitRepo.GetAll()
        //                               on poDet.UnitRefId equals udet.Id
        //                               select new PurchaseOrderDetailViewDto
        //                               {
        //                                   PoRefId = (int)poDet.PoRefId,
        //                                   MaterialRefId = poDet.MaterialRefId,
        //                                   Barcode = mat.Barcode,
        //                                   MaterialRefName = mat.MaterialName,
        //                                   QtyOrdered = poDet.QtyOrdered,
        //                                   QtyReceived = poDet.QtyReceived,
        //                                   QtyPending = (poDet.QtyOrdered - poDet.QtyReceived) > 0 ? (poDet.QtyOrdered - poDet.QtyReceived) : 0,
        //                                   UnitRefId = poDet.UnitRefId,
        //                                   UnitRefName = udet.Name,
        //                                   Price = poDet.Price,
        //                                   DiscountOption = poDet.DiscountOption,
        //                                   DiscountValue = poDet.DiscountValue,
        //                                   TotalAmount = poDet.TotalAmount,
        //                                   TaxAmount = poDet.TaxAmount,
        //                                   NetAmount = poDet.NetAmount,
        //                                   PoStatus = poDet.PoStatus,
        //                                   Remarks = poDet.Remarks,
        //                                   Uom = un.Name,
        //                                   DefaultUnitId = mat.DefaultUnitId,
        //                                   MinimumOrderQuantity = 0,
        //                                   MaximumOrderQuantity = 0
        //                               }).ToListAsync();

        //        var rsSupplierMaterial = await _suppliermaterialRepo.GetAll().Where(t => t.SupplierRefId == editDto.SupplierRefId).ToListAsync();
        //        var rsUnits = await _unitRepo.GetAllListAsync();
        //        List<InterTransferDetailListDto> rsInterTransferDetails = new List<InterTransferDetailListDto>();
        //        if (editDto.PoBasedOnInterTransferRequest == true)
        //        {
        //            var tempTrnDetails = await _intertransferDetailRepo.GetAllListAsync(t => t.PurchaseOrderRefId.HasValue && t.PurchaseOrderRefId.Value == editDto.Id);
        //            rsInterTransferDetails = tempTrnDetails.MapTo<List<InterTransferDetailListDto>>();
        //        }

        //        List<CaterQuoteRequiredMaterialVsPurchaseOrderLinkListDto> rsCaterPurchaseOrder = new List<CaterQuoteRequiredMaterialVsPurchaseOrderLinkListDto>();
        //        List<CaterQuoteRequiredMaterialListDto> rsCaterRequiredMaterialListDtos = new List<CaterQuoteRequiredMaterialListDto>();
        //        List<QuotationHeaderListDto> allCaterHeaderListDtos = new List<QuotationHeaderListDto>();
        //        if (editDto.PoBasedOnCater == true)
        //        {
        //            var tempCaterPoLink = await _caterQuoteRequiredMaterialVsPoLinkRepo.GetAllListAsync(t => t.PurchaseOrderRefId == editDto.Id);
        //            rsCaterPurchaseOrder = tempCaterPoLink.MapTo<List<CaterQuoteRequiredMaterialVsPurchaseOrderLinkListDto>>();

        //            List<int> arrCaterMaterialDataRefIds = rsCaterPurchaseOrder.Select(t => t.CaterQuoteRequiredMaterialDataRefId).ToList();
        //            var tempCaterMaterials = await _caterQuoteRequiredMaterialRepo.GetAllListAsync(t => arrCaterMaterialDataRefIds.Contains(t.Id));
        //            rsCaterRequiredMaterialListDtos = tempCaterMaterials.MapTo<List<CaterQuoteRequiredMaterialListDto>>();

        //            var arrHeaderRefIdsforthisPo = rsCaterRequiredMaterialListDtos.Select(t => t.CaterQuoteHeaderId).ToList();
        //            arrHeaderRefIdsforthisPo = arrHeaderRefIdsforthisPo.Distinct().ToList();
        //            var iqCaterHeader = _caterQuotationHeaderRepository.GetAll().Where(t => arrHeaderRefIdsforthisPo.Contains(t.Id));

        //            var allCaterHeaderItems = await iqCaterHeader
        //               .Include(e => e.CaterCustomer)
        //               .Include(e => e.EventAddress)
        //               .Include(e => e.Location)
        //               .Where(c => c.TenantId == AbpSession.TenantId).ToListAsync(); ;

        //            allCaterHeaderListDtos = allCaterHeaderItems.Select(e =>
        //            {
        //                var dto = e.MapTo<QuotationHeaderListDto>();
        //                dto.CaterCustomerCustomerName = e.CaterCustomer.CustomerName;
        //                dto.LocationRefName = e.Location.Code + " - " + e.Location.Name;
        //                return dto;
        //            }).ToList();
        //            editCaterQuoteDetails = allCaterHeaderListDtos.Where(t => arrHeaderRefIdsforthisPo.Contains(t.Id)).ToList().MapTo<List<SimpleQuotationHeaderListDto>>();
        //        }

        //        int loopCnt = 0;
        //        foreach (var det in editDetailDto)
        //        {
        //            if (rsCaterRequiredMaterialListDtos.Count > 0)
        //            {
        //                var arrCaterHeaderRefIds = rsCaterRequiredMaterialListDtos.Where(t => t.MaterialRefId == det.MaterialRefId).Select(t => t.CaterQuoteHeaderId).ToList();
        //                List<CaterHeaderRefIds> caterHeaderRefIds = new List<CaterHeaderRefIds>();
        //                if (arrCaterHeaderRefIds.Count > 0)
        //                {
        //                    foreach (var headerRefId in arrCaterHeaderRefIds)
        //                    {
        //                        CaterHeaderRefIds newDto = new CaterHeaderRefIds { CaterHeaderRefId = headerRefId };
        //                        caterHeaderRefIds.Add(newDto);
        //                    }
        //                }
        //                det.CaterHeaderRefIds = caterHeaderRefIds;
        //            }

        //            if (rsInterTransferDetails.Count > 0)
        //            {
        //                var arrInterTransferRefIds = rsInterTransferDetails.Where(t => t.MaterialRefId == det.MaterialRefId).Select(t => t.InterTransferRefId).ToList();
        //                List<InterTransferDetailWithPORefIds> interTransferDetailWithPORefIds = new List<InterTransferDetailWithPORefIds>();
        //                if (arrInterTransferRefIds.Count > 0)
        //                {
        //                    foreach (var intrnRefId in arrInterTransferRefIds)
        //                    {
        //                        InterTransferDetailWithPORefIds newDto = new InterTransferDetailWithPORefIds { InterTransferRefId = intrnRefId };
        //                        interTransferDetailWithPORefIds.Add(newDto);
        //                    }
        //                }
        //                det.InterTransferDetailWithPORefIds = interTransferDetailWithPORefIds;
        //            }

        //            var supmat = rsSupplierMaterial.FirstOrDefault(t => t.MaterialRefId == det.MaterialRefId && t.LocationRefId == editDto.LocationRefId);
        //            if (supmat == null)
        //            {
        //                supmat = rsSupplierMaterial.FirstOrDefault(t => t.MaterialRefId == det.MaterialRefId && t.LocationRefId == null);
        //            }
        //            if (supmat == null)
        //            {
        //                det.UnitRefIdForMinOrder = det.UnitRefId;
        //                det.UnitRefNameForMinOrder = det.UnitRefName;
        //                det.MinimumOrderQuantity = 0;
        //                det.MaximumOrderQuantity = 0;
        //            }
        //            else
        //            {
        //                det.UnitRefIdForMinOrder = supmat.UnitRefId;
        //                var unit = rsUnits.FirstOrDefault(t => t.Id == supmat.UnitRefId);
        //                det.UnitRefNameForMinOrder = unit.Name;
        //                det.MinimumOrderQuantity = supmat.MinimumOrderQuantity;
        //                det.MaximumOrderQuantity = supmat.MaximumOrderQuantity;
        //                det.SupplierMaterialAliasName = supmat.SupplierMaterialAliasName;
        //            }
        //            var taxlist = await (from potax in _purchaseordertaxdetailRepo.GetAll().Where(pot => pot.PoRefId == input.Id.Value && pot.MaterialRefId == det.MaterialRefId)
        //                                 join tax in _taxRepo.GetAll()
        //                                 on potax.TaxRefId equals tax.Id
        //                                 select new TaxForMaterial
        //                                 {
        //                                     MaterialRefId = potax.MaterialRefId,
        //                                     TaxRefId = potax.TaxRefId,
        //                                     TaxName = tax.TaxName,
        //                                     TaxRate = potax.TaxRate,
        //                                     TaxValue = potax.TaxValue,
        //                                     SortOrder = tax.SortOrder,
        //                                     Rounding = tax.Rounding,
        //                                     TaxCalculationMethod = tax.TaxCalculationMethod,
        //                                 }).ToListAsync();

        //            editDetailDto[loopCnt].TaxForMaterial = taxlist.OrderBy(t => t.SortOrder).ToList();

        //            List<ApplicableTaxesForMaterial> applicabletaxes = new List<ApplicableTaxesForMaterial>();

        //            foreach (var tax in taxlist.OrderBy(t => t.SortOrder))
        //            {
        //                applicabletaxes.Add(new ApplicableTaxesForMaterial
        //                {
        //                    TaxName = tax.TaxName,
        //                    TaxRefId = tax.TaxRefId,
        //                    TaxRate = tax.TaxRate,
        //                    SortOrder = tax.SortOrder,
        //                    Rounding = tax.Rounding,
        //                    TaxCalculationMethod = tax.TaxCalculationMethod,
        //                });
        //            }

        //            editDetailDto[loopCnt].ApplicableTaxes = applicabletaxes;

        //            loopCnt++;
        //        }

        //    }
        //    else
        //    {
        //        editDto = new PurchaseOrderEditDto();
        //        editDetailDto = new List<PurchaseOrderDetailViewDto>();
        //    }
        //    Debug.WriteLine("PO Basic GetPurchaseOrderForEdit End Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
        //    return new GetPurchaseOrderForEditOutput
        //    {
        //        PurchaseOrder = editDto,
        //        PurchaseOrderDetail = editDetailDto,
        //        CaterQuoteDetails = editCaterQuoteDetails,
        //        Templatesave = editTemplateDto
        //    };
        //}

        //public async Task<BulkPoMessageOutput> CreateBulkPurchaseOrder(BulkPoPreparationDto input)
        //{
        //    var supplier = await _supplierRepo.FirstOrDefaultAsync(t => t.Id == input.SupplierRefId);
        //    if (supplier == null)
        //    {
        //        return new BulkPoMessageOutput { SuccessFlag = false, ErrorMessage = L("NotExistForId", L("Supplier"))};
        //    }

        //    List<int> materiaRefIds = input.BulkPoMaterialListDtos.Select(t => t.MaterialRefId).ToList();
        //    if (materiaRefIds.Count == 0)
        //    {
        //        return new BulkPoMessageOutput { SuccessFlag = false ,ErrorMessage = L("NoMoreRecordsFound") };
        //    }
        //    var rsMaterials = await _materialRepo.GetAllListAsync(t => materiaRefIds.Contains(t.Id));
        //    if (rsMaterials.Count == 0)
        //    {
        //        return new BulkPoMessageOutput { SuccessFlag = false, ErrorMessage = L("NoMoreRecordsFound") };
        //    }
        //    #region Generate Bulk Pos
        //    int locIndex = 0;
        //    List<GetPurchaseIdAndReference> returnPurchaseOrderList = new List<GetPurchaseIdAndReference>();
        //    foreach (var loc in input.Locations)
        //    {
        //        PurchaseOrderEditDto purchaseOrder = new PurchaseOrderEditDto 
        //        {
        //            LocationRefId = loc.Id,
        //            LocationRefName = loc.Name,
        //            SupplierRefId = input.SupplierRefId,
        //            SupplierRefName = supplier.SupplierName,
        //            ShippingLocationId = loc.Id,
        //            ShippingLocationRefName = loc.Name,
        //            DeliveryDateExpected = input.DeliveryDateExpected,
        //            PoDate = DateTime.Now,
        //        };
        //        var supplierMaterials = await _suppliermaterialRepo.GetAllListAsync(t => t.SupplierRefId == input.SupplierRefId);
        //        List<PurchaseOrderDetailViewDto> purchaseOrderDetails = new List<PurchaseOrderDetailViewDto>();
        //        foreach(var detail in input.BulkPoMaterialListDtos)
        //        {
        //            var mat = rsMaterials.FirstOrDefault(t => t.Id == detail.MaterialRefId);
        //            var supMaterial = supplierMaterials.FirstOrDefault(t => t.MaterialRefId == detail.MaterialRefId);
        //            if (supMaterial == null)
        //            {
        //                continue;
        //            }
        //            var tempQty= detail.LocationWiseRequestQty[locIndex];
        //            decimal requestQty = 0m;
        //            if (tempQty.Length > 0)
        //                requestQty = decimal.Parse(tempQty);
        //            if (requestQty > 0)
        //            {
        //                PurchaseOrderDetailViewDto newPoDetail = new PurchaseOrderDetailViewDto
        //                {
        //                    SupplierRefId = input.SupplierRefId,
        //                    SupplierRefName = supplier.SupplierName,
        //                    DefaultUnitId = mat.DefaultUnitId,
        //                    MaterialRefId = mat.Id,
        //                    MaterialRefName = mat.MaterialName,
        //                    QtyOrdered = requestQty,
        //                    UnitRefId = mat.DefaultUnitId,
        //                    Price = supMaterial.MaterialPrice,
        //                    TotalAmount = Math.Round( requestQty * supMaterial.MaterialPrice,2),
        //                    DiscountOption = "F",
        //                    TaxAmount = 0,
        //                    DiscountValue = 0,
        //                    NetAmount = Math.Round(requestQty * supMaterial.MaterialPrice, 2),
        //                    //TaxForMaterial = mat.tax
        //                };
        //                purchaseOrderDetails.Add(newPoDetail);
        //            }
        //        }

        //        if (purchaseOrderDetails.Count > 0)
        //        {
        //            purchaseOrder.PoNetAmount = purchaseOrderDetails.Sum(t => t.TotalAmount);

        //            TemplateSaveDto templatesave = new TemplateSaveDto
        //            {
        //                Templatename = "Sample",
        //                Templatescope = false,
        //                Templateflag = false
        //            };

        //            string statusToBeAssigned = "A";

        //            CreateOrUpdatePurchaseOrderInput purchaseInput = new CreateOrUpdatePurchaseOrderInput
        //            {
        //                PurchaseOrder = purchaseOrder,
        //                PurchaseOrderDetail = purchaseOrderDetails,
        //                Templatesave = templatesave,
        //                StatusToBeAssigned = statusToBeAssigned
        //            };
        //            var newPo = await CreateOrUpdatePurchaseOrder(purchaseInput);
        //            returnPurchaseOrderList.Add(newPo);
        //        }
        //        locIndex++;
        //    }
        //    #endregion

        //    #region Template Save

        //    string templateJson = SerializeToJSON(input);

        //    Template lastTemplate;
        //    int? templateScope = null;

        //    if (input.Templatesave!=null && input.Templatesave.Templateflag == true)
        //    {
        //        string repName = input.Templatesave.Templatename;

        //        var templateExist = _templateRepo.GetAll().Where(t => t.Name == repName && t.TemplateType == (int)TemplateType.BulkPurchaseOrder).ToList();
        //        if (templateExist.Count == 0)
        //        {
        //            lastTemplate = new Template
        //            {
        //                Name = input.Templatesave.Templatename,
        //                TemplateType = (int)TemplateType.SupplierPurchaseOrder,
        //                TemplateData = templateJson,
        //                LocationRefId = templateScope
        //            };
        //        }
        //        else
        //        {
        //            lastTemplate = templateExist[0];
        //            lastTemplate.LocationRefId = templateScope;
        //            lastTemplate.TemplateData = templateJson;
        //        }
        //    }
        //    else
        //    {
        //        string repName = L("Last") + " " + L("BulkPurchaseOrder");
        //        var templateExist = _templateRepo.GetAll().Where(t => t.Name == repName && t.TemplateType == (int)TemplateType.BulkPurchaseOrder).ToList();
        //        if (templateExist.Count() == 0)
        //        {
        //            lastTemplate = new Template
        //            {
        //                Name = L("LastPo"),
        //                TemplateType = (int)TemplateType.SupplierPurchaseOrder,
        //                TemplateData = templateJson,
        //                LocationRefId = templateScope
        //            };
        //        }
        //        else
        //        {
        //            lastTemplate = templateExist[0];
        //            lastTemplate.LocationRefId = templateScope;
        //            lastTemplate.TemplateData = templateJson;
        //        }
        //    }

        //    await _templateRepo.InsertOrUpdateAndGetIdAsync(lastTemplate);

        //    #endregion

        //    List<int> poRefIds = returnPurchaseOrderList.Select(t => t.Id).ToList();

        //    var resultPoList = await GetAllWithRefName(new GetPurchaseOrderInput
        //    {
        //        Filter = "",
        //        MaxResultCount = AppConsts.MaxPageSize,
        //        Sorting = "Id",
        //        PurchaseOrderIdList = poRefIds 
        //    });

        //    return new BulkPoMessageOutput { 
        //        SuccessFlag = true,
        //        Message = resultPoList.Items.Count + " Pos Created",
        //        PurchaseOrderList = resultPoList.Items.MapTo<List< PurchaseOrderListWithRefNameDto>>() 
        //    };
        //}

        //public async Task<GetPurchaseIdAndReference> CreateOrUpdatePurchaseOrder(CreateOrUpdatePurchaseOrderInput input)
        //{
        //    if (input.PurchaseOrder.Id.HasValue)
        //    {
        //        return await UpdatePurchaseOrder(input);
        //    }
        //    else
        //    {
        //        return await CreatePurchaseOrder(input);
        //    }
        //}

        //public async Task DeletePurchaseOrder(IdInput input)
        //{
        //    var po = await _purchaseorderRepo.FirstOrDefaultAsync(t => t.Id == input.Id);

        //    if (po.PoCurrentStatus == "P" || po.PoCurrentStatus == "A")
        //    {
        //        #region Transfer PO Ref Id
        //        var lstTransferDetail = await _intertransferDetailRepo.GetAllListAsync(t => t.PurchaseOrderRefId.HasValue && t.PurchaseOrderRefId.Value == input.Id);
        //        if (lstTransferDetail.Count > 0)
        //        {
        //            foreach (var detail in lstTransferDetail)
        //            {
        //                detail.PurchaseOrderRefId = null;
        //            }
        //            List<int> transferIds = lstTransferDetail.Select(t => t.InterTransferRefId).ToList();
        //            var lstTransfers = await _intertransferRepo.GetAllListAsync(t => transferIds.Contains(t.Id));
        //            foreach (var trn in lstTransfers)
        //            {
        //                bool anyOtherPoRefMaterialExists = lstTransferDetail.Where(t => t.InterTransferRefId == trn.Id && t.PurchaseOrderRefId.HasValue && t.PurchaseOrderRefId.Value != input.Id).Count() == 0 ? false : true;
        //                if (anyOtherPoRefMaterialExists == false)
        //                {
        //                    trn.DoesPOCreatedForThisRequest = false;
        //                }
        //            }
        //        }
        //        #endregion

        //        await _purchaseordertaxdetailRepo.DeleteAsync(u => u.PoRefId == input.Id);

        //        await _purchaseorderdetailRepo.DeleteAsync(u => u.PoRefId == input.Id);

        //        await _purchaseorderRepo.DeleteAsync(input.Id);
        //    }
        //}

        //protected virtual async Task<GetPurchaseIdAndReference> UpdatePurchaseOrder(CreateOrUpdatePurchaseOrderInput input)
        //{

        //    bool ApprovedAuthorization = false;
        //    if (PermissionChecker.IsGranted("Pages.Tenant.House.Transaction.PurchaseOrder.Approve"))
        //    {
        //        ApprovedAuthorization = true;
        //    }

        //    var item = await _purchaseorderRepo.GetAsync(input.PurchaseOrder.Id.Value);

        //    if (input.PurchaseOrderDetail == null || input.PurchaseOrderDetail.Count() == 0)
        //    {
        //        throw new UserFriendlyException(L("MinimumOneDetail"));
        //    }

        //    var dto = input.PurchaseOrder;

        //    item.PoDate = dto.PoDate;
        //    item.PoReferenceCode = dto.PoReferenceCode;
        //    item.ShippingLocationId = dto.ShippingLocationId;
        //    item.QuoteReference = dto.QuoteReference;
        //    item.CreditDays = dto.CreditDays;
        //    item.SupplierRefId = dto.SupplierRefId;
        //    item.Remarks = dto.Remarks;
        //    item.LocationRefId = dto.LocationRefId;
        //    item.DeliveryDateExpected = dto.DeliveryDateExpected;
        //    item.PoNetAmount = dto.PoNetAmount;


        //    if (ApprovedAuthorization)
        //    {
        //        item.PoCurrentStatus = input.StatusToBeAssigned;
        //        item.ApprovedTime = DateTime.Now;
        //    }
        //    else
        //    {
        //        item.PoCurrentStatus = input.StatusToBeAssigned;
        //    }

        //    List<int> materialsToBeRetained = new List<int>();
        //    if (input.PurchaseOrderDetail != null && input.PurchaseOrderDetail.Count > 0)
        //    {
        //        foreach (var items in input.PurchaseOrderDetail)
        //        {
        //            var matRefId = items.MaterialRefId;
        //            materialsToBeRetained.Add(matRefId);
        //            var existingReturnDetail = _purchaseorderdetailRepo.GetAllList(u => u.PoRefId == dto.Id && u.MaterialRefId == matRefId);
        //            if (existingReturnDetail.Count == 0)  //Add new record
        //            {
        //                PurchaseOrderDetail pd = new PurchaseOrderDetail();
        //                pd.PoRefId = (int)dto.Id;
        //                pd.MaterialRefId = items.MaterialRefId;
        //                pd.QtyOrdered = items.QtyOrdered;
        //                pd.QtyReceived = items.QtyReceived;
        //                pd.UnitRefId = items.UnitRefId;
        //                pd.Price = items.Price;
        //                pd.DiscountOption = items.DiscountOption;
        //                pd.DiscountValue = items.DiscountValue;
        //                pd.TotalAmount = items.TotalAmount;
        //                pd.TaxAmount = items.TaxAmount;
        //                if (items.NetAmount == 0)
        //                {
        //                    pd.NetAmount = items.TotalAmount - items.DiscountValue + items.TaxAmount;
        //                }
        //                else
        //                {
        //                    pd.NetAmount = items.NetAmount;
        //                }
                        
        //                pd.PoStatus = items.PoStatus;
        //                pd.Remarks = items.Remarks;

        //                var retId2 = await _purchaseorderdetailRepo.InsertAndGetIdAsync(pd);

        //                foreach (var tax in items.TaxForMaterial)
        //                {
        //                    PurchaseOrderTaxDetail taxForPoDetail = new PurchaseOrderTaxDetail
        //                    {
        //                        PoRefId = (int)dto.Id,
        //                        MaterialRefId = items.MaterialRefId,
        //                        TaxRefId = tax.TaxRefId,
        //                        TaxRate = tax.TaxRate,
        //                        TaxValue = tax.TaxValue,
        //                    };
        //                    var retpotax = await _purchaseordertaxdetailRepo.InsertAndGetIdAsync(taxForPoDetail);
        //                }
        //            }
        //            else
        //            {
        //                var editDetailDto = await _purchaseorderdetailRepo.GetAsync(existingReturnDetail[0].Id);

        //                editDetailDto.PoRefId = (int)dto.Id;
        //                editDetailDto.MaterialRefId = items.MaterialRefId;
        //                editDetailDto.QtyOrdered = items.QtyOrdered;
        //                editDetailDto.UnitRefId = items.UnitRefId;
        //                editDetailDto.Price = items.Price;
        //                editDetailDto.DiscountOption = items.DiscountOption;
        //                editDetailDto.DiscountValue = items.DiscountValue;
        //                editDetailDto.TotalAmount = items.TotalAmount;
        //                editDetailDto.TaxAmount = items.TaxAmount;
        //                editDetailDto.NetAmount = items.NetAmount;
        //                editDetailDto.PoStatus = items.PoStatus;
        //                editDetailDto.Remarks = items.Remarks;

        //                var retId3 = await _purchaseorderdetailRepo.InsertOrUpdateAndGetIdAsync(editDetailDto);

        //                //
        //                if (items.TaxForMaterial != null && items.TaxForMaterial.Count > 0)
        //                {
        //                    List<int> taxrefidtoberetained = new List<int>();
        //                    foreach (var taxdet in items.TaxForMaterial)
        //                    {
        //                        int taxrefid = taxdet.TaxRefId;
        //                        taxrefidtoberetained.Add(taxrefid);

        //                        var existingPoTaxDetails = _purchaseordertaxdetailRepo.GetAllList(u => u.PoRefId == dto.Id &&
        //                        u.MaterialRefId == matRefId && u.TaxRefId == taxdet.TaxRefId);
        //                        if (existingPoTaxDetails.Count == 0)
        //                        {
        //                            PurchaseOrderTaxDetail taxForPoDetail = new PurchaseOrderTaxDetail
        //                            {
        //                                PoRefId = (int)dto.Id,
        //                                MaterialRefId = items.MaterialRefId,
        //                                TaxRefId = taxdet.TaxRefId,
        //                                TaxRate = taxdet.TaxRate,
        //                                TaxValue = taxdet.TaxValue,
        //                            };
        //                            var retpotax = await _purchaseordertaxdetailRepo.InsertAndGetIdAsync(taxForPoDetail);
        //                        }
        //                        else
        //                        {
        //                            var editPotaxitem = await _purchaseordertaxdetailRepo.GetAsync(existingPoTaxDetails[0].Id);
        //                            editPotaxitem.TaxRate = taxdet.TaxRate;
        //                            editPotaxitem.TaxValue = taxdet.TaxValue;
        //                        }
        //                    }

        //                    var delPoTaxList = _purchaseordertaxdetailRepo.GetAll().Where(a => a.PoRefId == input.PurchaseOrder.Id.Value && a.MaterialRefId == items.MaterialRefId && !taxrefidtoberetained.Contains(a.TaxRefId)).ToList();

        //                    foreach (var a in delPoTaxList)
        //                    {
        //                        await _purchaseordertaxdetailRepo.DeleteAsync(a.Id);
        //                    }
        //                }
        //                else
        //                {
        //                    var potaxdetailstobedeleted = _purchaseordertaxdetailRepo.GetAllList(t => t.PoRefId == dto.Id && t.MaterialRefId == items.MaterialRefId);
        //                    foreach (var lst in potaxdetailstobedeleted)
        //                    {
        //                        await _purchaseordertaxdetailRepo.DeleteAsync(lst.Id);
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    var delMatList = _purchaseorderdetailRepo.GetAll().Where(a => a.PoRefId == input.PurchaseOrder.Id.Value && !materialsToBeRetained.Contains(a.MaterialRefId)).ToList();

        //    foreach (var a in delMatList)
        //    {
        //        await _purchaseorderdetailRepo.DeleteAsync(a.Id);
        //    }

        //    //if (caterHeaderIds.Count > 0)
        //    //{
        //    //    var lstCaterHeaders = await _caterQuotationHeaderRepository.GetAllListAsync(t => caterHeaderIds.Contains(t.Id));
        //    //    foreach (var lst in lstCaterHeaders)
        //    //    {
        //    //        lst.DoesPOCreatedForThisCateringOrder = true;
        //    //        await _caterQuotationHeaderRepository.UpdateAsync(lst);
        //    //    }
        //    //    var lstCaterRequiredMaterials = await _caterQuoteRequiredMaterialRepo.GetAllListAsync(t => caterRequestMaterialDataRefIds.Contains(t.Id));
        //    //    foreach (var caterMaterialDetail in lstCaterRequiredMaterials)
        //    //    {
        //    //        var existPoForCaterMaterial = await _caterQuoteRequiredMaterialVsPoLinkRepo.FirstOrDefaultAsync(t => t.CaterQuoteRequiredMaterialDataRefId == caterMaterialDetail.Id && t.PurchaseOrderRefId == dto.Id);
        //    //        if (existPoForCaterMaterial == null)
        //    //        {
        //    //            existPoForCaterMaterial = new CaterQuoteRequiredMaterialVsPurchaseOrderLink
        //    //            {
        //    //                PurchaseOrderRefId = dto.Id,
        //    //                CaterQuoteRequiredMaterialDataRefId = caterMaterialDetail.Id
        //    //            };
        //    //            await _caterQuoteRequiredMaterialVsPoLinkRepo.InsertOrUpdateAndGetIdAsync(existPoForCaterMaterial);
        //    //        }
        //    //        else
        //    //        {

        //    //        }
        //    //    }
        //    //    item.PoBasedOnCater = true;
        //    //}

        //    CheckErrors(await _purchaseorderManager.CreateSync(item));

        //    return new GetPurchaseIdAndReference
        //    {
        //        Id = input.PurchaseOrder.Id.Value,
        //        PoReferenceCode = input.PurchaseOrder.PoReferenceCode
        //    };
        //}

        //protected virtual async Task<GetPurchaseIdAndReference> CreatePurchaseOrder(CreateOrUpdatePurchaseOrderInput input)
        //{
        //    bool ApprovedAuthorization = false;
        //    if (PermissionChecker.IsGranted("Pages.Tenant.House.Transaction.PurchaseOrder.Approve"))
        //    {
        //        ApprovedAuthorization = true;
        //    }

        //    var purchaseorderno = _purchaseorderRepo.GetAll().Where(po => po.SupplierRefId == input.PurchaseOrder.SupplierRefId).ToList().Count + 1;
        //    var locationcode = await _locationRepo.FirstOrDefaultAsync(loc => loc.Id == input.PurchaseOrder.LocationRefId);
        //    var sup = await _supplierRepo.FirstOrDefaultAsync(s => s.Id == input.PurchaseOrder.SupplierRefId);

        //    var dto = input.PurchaseOrder.MapTo<PurchaseOrder>();

        //    if (ApprovedAuthorization)
        //    {
        //        dto.PoCurrentStatus = input.StatusToBeAssigned;
        //        dto.ApprovedTime = DateTime.Now;
        //    }
        //    else
        //    {
        //        dto.PoCurrentStatus = input.StatusToBeAssigned;
        //    }

        //    string supplierName;
        //    supplierName = sup.SupplierName;
        //    if (supplierName.Length < 3)
        //        supplierName = supplierName + supplierName + supplierName;

        //    dto.PoReferenceCode = String.Concat(locationcode.Code, "/", DateTime.UtcNow.ToString("yyMMMdd"), "/", supplierName.Left(3), "/", purchaseorderno.ToString());

        //    if (input.PurchaseOrderDetail == null || input.PurchaseOrderDetail.Count() == 0)
        //    {
        //        throw new UserFriendlyException(L("MinimumOneDetail"));
        //    }

        //    var retId = await _purchaseorderRepo.InsertAndGetIdAsync(dto);
        //    List<int> transferIds = new List<int>();
        //    List<int> transferDetailsIds = new List<int>();

        //    List<int> caterHeaderIds = new List<int>();
        //    List<int> caterRequestMaterialDataRefIds = new List<int>();

        //    foreach (PurchaseOrderDetailViewDto item in input.PurchaseOrderDetail)
        //    {
        //        PurchaseOrderDetail pd = new PurchaseOrderDetail();
        //        pd.PoRefId = (int)dto.Id;
        //        pd.MaterialRefId = item.MaterialRefId;
        //        pd.QtyOrdered = item.QtyOrdered;
        //        pd.QtyReceived = item.QtyReceived;
        //        pd.UnitRefId = item.UnitRefId;
        //        pd.Price = item.Price;
        //        pd.DiscountOption = item.DiscountOption;
        //        pd.DiscountValue = item.DiscountValue;
        //        pd.TaxAmount = item.TaxAmount;
        //        pd.TotalAmount = item.TotalAmount;
        //        if (item.NetAmount == 0)
        //        {
        //            pd.NetAmount = pd.TotalAmount - pd.DiscountValue + pd.TaxAmount;
        //        }
        //        else
        //        {
        //            pd.NetAmount = item.NetAmount;
        //        }
        //        pd.PoStatus = item.PoStatus;
        //        pd.Remarks = item.Remarks;
        //        pd.UnitRefId = item.UnitRefId;
        //        var retId2 = await _purchaseorderdetailRepo.InsertAndGetIdAsync(pd);

        //        if (item.TaxForMaterial != null)
        //        {
        //            foreach (var tax in item.TaxForMaterial)
        //            {
        //                PurchaseOrderTaxDetail taxForPoDetail = new PurchaseOrderTaxDetail
        //                {
        //                    PoRefId = (int)dto.Id,
        //                    MaterialRefId = item.MaterialRefId,
        //                    TaxRefId = tax.TaxRefId,
        //                    TaxRate = tax.TaxRate,
        //                    TaxValue = tax.TaxValue,
        //                };
        //                var retId3 = await _purchaseordertaxdetailRepo.InsertAndGetIdAsync(taxForPoDetail);
        //            }
        //        }
        //        if (item.InterTransferDetailWithPORefIds != null)
        //        {
        //            var materialInterTransferMasterIds = (item.InterTransferDetailWithPORefIds.Select(t => t.InterTransferRefId).ToList());
        //            transferIds.AddRange(materialInterTransferMasterIds);
        //            var lstTransferDetails = await _intertransferDetailRepo.GetAllListAsync(t => t.MaterialRefId == item.MaterialRefId && materialInterTransferMasterIds.Contains(t.InterTransferRefId));
        //            var materialInterTransferDetailIds = lstTransferDetails.Select(t => t.Id).ToList();
        //            transferDetailsIds.AddRange(materialInterTransferDetailIds);

        //        }
        //        if (item.CaterHeaderRefIds != null)
        //        {
        //            var tempcaterHeadIds = (item.CaterHeaderRefIds.Select(t => t.CaterHeaderRefId).ToList());
        //            caterHeaderIds.AddRange(tempcaterHeadIds);
        //            var lstCaterRequiredMaterialDetails = await _caterQuoteRequiredMaterialRepo.GetAllListAsync(t => t.MaterialRefId == item.MaterialRefId && tempcaterHeadIds.Contains(t.CaterQuoteHeaderId));
        //            var tempcaterRequestMaterialDataRefIds = lstCaterRequiredMaterialDetails.Select(t => t.Id).ToList();
        //            caterRequestMaterialDataRefIds.AddRange(tempcaterRequestMaterialDataRefIds);
        //        }
        //    }

        //    if (transferIds.Count > 0)
        //    {
        //        var lstTransfers = await _intertransferRepo.GetAllListAsync(t => transferIds.Contains(t.Id));
        //        foreach (var lst in lstTransfers)
        //        {
        //            lst.DoesPOCreatedForThisRequest = true;
        //            await _intertransferRepo.UpdateAsync(lst);
        //        }
        //        var lstTransferDetails = await _intertransferDetailRepo.GetAllListAsync(t => transferDetailsIds.Contains(t.Id));
        //        foreach (var trnDetail in lstTransferDetails)
        //        {
        //            trnDetail.PurchaseOrderRefId = dto.Id;
        //            await _intertransferDetailRepo.UpdateAsync(trnDetail);
        //        }
        //        dto.PoBasedOnInterTransferRequest = true;
        //    }

        //    if (caterHeaderIds.Count > 0)
        //    {
        //        var lstCaterHeaders = await _caterQuotationHeaderRepository.GetAllListAsync(t => caterHeaderIds.Contains(t.Id));
        //        foreach (var lst in lstCaterHeaders)
        //        {
        //            lst.DoesPOCreatedForThisCateringOrder = true;
        //            await _caterQuotationHeaderRepository.UpdateAsync(lst);
        //        }
        //        var lstCaterRequiredMaterials = await _caterQuoteRequiredMaterialRepo.GetAllListAsync(t => caterRequestMaterialDataRefIds.Contains(t.Id));
        //        foreach (var caterMaterialDetail in lstCaterRequiredMaterials)
        //        {
        //            var existPoForCaterMaterial = await _caterQuoteRequiredMaterialVsPoLinkRepo.FirstOrDefaultAsync(t => t.CaterQuoteRequiredMaterialDataRefId == caterMaterialDetail.Id && t.PurchaseOrderRefId == dto.Id);
        //            if (existPoForCaterMaterial == null)
        //            {
        //                existPoForCaterMaterial = new CaterQuoteRequiredMaterialVsPurchaseOrderLink
        //                {
        //                    PurchaseOrderRefId = dto.Id,
        //                    CaterQuoteRequiredMaterialDataRefId = caterMaterialDetail.Id
        //                };
        //                await _caterQuoteRequiredMaterialVsPoLinkRepo.InsertOrUpdateAndGetIdAsync(existPoForCaterMaterial);
        //            }
        //            else
        //            {

        //            }
        //        }
        //        dto.PoBasedOnCater = true;
        //    }
        //    CheckErrors(await _purchaseorderManager.CreateSync(dto));

        //    string templateJson = SerializeToJSON(input);

        //    Template lastTemplate;
        //    int? templateScope;
        //    if (input.Templatesave.Templatescope == true)
        //    {
        //        templateScope = null;
        //    }
        //    else
        //    {
        //        templateScope = input.PurchaseOrder.LocationRefId;
        //    }

        //    if (input.Templatesave.Templateflag == true)
        //    {
        //        string repName = input.Templatesave.Templatename;

        //        var templateExist = _templateRepo.GetAll().Where(t => t.Name == repName && t.TemplateType == (int)TemplateType.SupplierPurchaseOrder).ToList();
        //        if (templateExist.Count == 0)
        //        {
        //            lastTemplate = new Template
        //            {
        //                Name = input.Templatesave.Templatename,
        //                TemplateType = (int)TemplateType.SupplierPurchaseOrder,
        //                TemplateData = templateJson,
        //                LocationRefId = templateScope
        //            };
        //        }
        //        else
        //        {
        //            lastTemplate = templateExist[0];
        //            lastTemplate.LocationRefId = templateScope;
        //            lastTemplate.TemplateData = templateJson;
        //        }
        //    }
        //    else
        //    {
        //        string repName = L("LastPo");
        //        var templateExist = _templateRepo.GetAll().Where(t => t.Name == repName && t.TemplateType == (int)TemplateType.SupplierPurchaseOrder).ToList();
        //        if (templateExist.Count() == 0)
        //        {
        //            lastTemplate = new Template
        //            {
        //                Name = L("LastPo"),
        //                TemplateType = (int)TemplateType.SupplierPurchaseOrder,
        //                TemplateData = templateJson,
        //                LocationRefId = input.PurchaseOrder.LocationRefId
        //            };
        //        }
        //        else
        //        {
        //            lastTemplate = templateExist[0];
        //            lastTemplate.LocationRefId = templateScope;
        //            lastTemplate.TemplateData = templateJson;
        //        }
        //    }

        //    await _templateRepo.InsertOrUpdateAndGetIdAsync(lastTemplate);

        //    return new GetPurchaseIdAndReference
        //    {
        //        Id = retId,
        //        PoReferenceCode = dto.PoReferenceCode
        //    };
        //}

        //public virtual async Task SetPurchaseOrderStatus(PurchaseOrderStatus input)
        //{
        //    var item = await _purchaseorderRepo.GetAsync(input.PoId);
        //    bool ApprovedAuthorization = false;
        //    if (!PermissionChecker.IsGranted("Pages.Tenant.House.Transaction.PurchaseOrder.Approve"))
        //    {
        //        ApprovedAuthorization = true;
        //    }
        //    else
        //    {
        //        throw new UserFriendlyException(L("NotAuthorized"));
        //    }

        //    if (ApprovedAuthorization)
        //    {
        //        item.Remarks = input.PoCurrentStatus;
        //        item.PoCurrentStatus = "A"; // L("Approved");
        //        item.ApprovedTime = DateTime.Now;
        //    }
        //}

        //public virtual async Task SetCompletePurchaseOrder(PurchaseOrderStatus input)
        //{

        //    bool ApprovedAuthorization = true;
        //    //if (!PermissionChecker.IsGranted("Pages.Tenant.House.Transaction.PurchaseOrder.Approve"))
        //    //{
        //    //    ApprovedAuthorization = true;
        //    //}
        //    //else
        //    //{
        //    //    throw new UserFriendlyException(L("NotAuthorized"));
        //    //}

        //    var item = await _purchaseorderRepo.GetAsync(input.PoId);
        //    if (ApprovedAuthorization)
        //    {
        //        item.Remarks = input.PoRemarks;
        //        item.PoCurrentStatus = "C"; // L("Approved");
        //    }
        //}



        //public virtual async Task CreateDraft(CreateOrUpdatePurchaseOrderInput input)
        //{
        //    string templateJson = SerializeToJSON(input);

        //    Template lastTemplate;
        //    int? templateScope;

        //    templateScope = input.PurchaseOrder.LocationRefId;


        //    if (input.Templatesave.Templateflag == true)
        //    {
        //        string repName = input.Templatesave.Templatename;

        //        var templateExist = _templateRepo.GetAll().Where(t => t.Name == repName && t.TemplateType == (int)TemplateType.SupplierPurchaseOrder).ToList();
        //        if (templateExist.Count == 0)
        //        {
        //            lastTemplate = new Template
        //            {
        //                Name = input.Templatesave.Templatename,
        //                TemplateType = (int)TemplateType.SupplierPurchaseOrder,
        //                TemplateData = templateJson,
        //                LocationRefId = templateScope
        //            };
        //        }
        //        else
        //        {
        //            lastTemplate = templateExist[0];
        //            lastTemplate.LocationRefId = templateScope;
        //            lastTemplate.TemplateData = templateJson;
        //        }
        //    }
        //    else
        //    {
        //        string repName = L("LastPoDrafted");
        //        var templateExist = _templateRepo.GetAll().Where(t => t.Name == repName && t.TemplateType == (int)TemplateType.SupplierPurchaseOrder).ToList();
        //        if (templateExist.Count == 0)
        //        {
        //            lastTemplate = new Template
        //            {
        //                Name = L("LastPoDrafted"),
        //                TemplateType = (int)TemplateType.SupplierPurchaseOrder,
        //                TemplateData = templateJson,
        //                LocationRefId = input.PurchaseOrder.LocationRefId
        //            };
        //        }
        //        else
        //        {
        //            lastTemplate = templateExist[0];
        //            lastTemplate.LocationRefId = templateScope;
        //            lastTemplate.TemplateData = templateJson;
        //        }
        //    }

        //    await _templateRepo.InsertOrUpdateAndGetIdAsync(lastTemplate);
        //}

        //public async Task<ListResultOutput<PurchaseOrderListDto>> GetPurchaseReferenceNumber(GetPoReferenceDto input)
        //{
        //    string[] statusStr = { "A", "H" };// L("Approved");
        //    List<PurchaseOrder> lstPurchaseOrder = new List<PurchaseOrder>();
        //    if (input.SupplierRefId == 0)
        //    {
        //        lstPurchaseOrder = await _purchaseorderRepo.GetAll().Where(po => po.LocationRefId == input.LocationRefId && statusStr.Contains(po.PoCurrentStatus)).ToListAsync();
        //    }
        //    else
        //    {
        //        lstPurchaseOrder = await _purchaseorderRepo.GetAll().Where(po => po.SupplierRefId == input.SupplierRefId && po.LocationRefId == input.LocationRefId && statusStr.Contains(po.PoCurrentStatus)).ToListAsync();
        //    }
        //    return new ListResultOutput<PurchaseOrderListDto>(lstPurchaseOrder.MapTo<List<PurchaseOrderListDto>>());
        //}

        //public async Task<GetPurchaseOrderForEditOutput> GetPoDetailBasedOnPoReferenceCode(NullableIdInput input)
        //{
        //    PurchaseOrderEditDto editDto;
        //    List<PurchaseOrderDetailViewDto> editDetailDto;

        //    if (input.Id.HasValue)
        //    {
        //        var hDto = await _purchaseorderRepo.GetAsync(input.Id.Value);
        //        editDto = hDto.MapTo<PurchaseOrderEditDto>();

        //        editDetailDto = await (from poDet in _purchaseorderdetailRepo.GetAll()
        //                               .Where(a => a.PoRefId == input.Id.Value && a.PoStatus == false && a.QtyReceived < a.QtyOrdered)
        //                               join mat in _materialRepo.GetAll() on poDet.MaterialRefId equals mat.Id
        //                               join un in _unitRepo.GetAll() on mat.DefaultUnitId equals un.Id
        //                               join udet in _unitRepo.GetAll() on poDet.UnitRefId equals udet.Id
        //                               select new PurchaseOrderDetailViewDto
        //                               {
        //                                   PoRefId = (int)poDet.PoRefId,
        //                                   MaterialRefId = poDet.MaterialRefId,
        //                                   Barcode = mat.Barcode,
        //                                   MaterialRefName = mat.MaterialName,
        //                                   QtyOrdered = poDet.QtyOrdered,
        //                                   QtyReceived = poDet.QtyReceived,
        //                                   Price = poDet.Price,
        //                                   DiscountOption = poDet.DiscountOption,
        //                                   DiscountValue = poDet.DiscountValue,
        //                                   NetAmount = poDet.NetAmount,
        //                                   PoStatus = poDet.PoStatus,
        //                                   Remarks = poDet.Remarks,
        //                                   UnitRefId = poDet.UnitRefId,
        //                                   UnitRefName = udet.Name,
        //                                   Uom = un.Name,
        //                                   DefaultUnitId = mat.DefaultUnitId
        //                               }).ToListAsync();
        //    }
        //    else
        //    {
        //        editDto = new PurchaseOrderEditDto();
        //        editDetailDto = new List<PurchaseOrderDetailViewDto>();
        //    }

        //    return new GetPurchaseOrderForEditOutput
        //    {
        //        PurchaseOrder = editDto,
        //        PurchaseOrderDetail = editDetailDto
        //    };
        //}

        //public async Task<GetPurchaseOrderForEditOutput> GetTemplateObjectForEdit(GetObjectFromString input)
        //{
        //    GetPurchaseOrderForEditOutput ReturnObject = DeSerializeFromJSON<GetPurchaseOrderForEditOutput>(input.ObjectString);

        //    PurchaseOrderEditDto editDto = ReturnObject.PurchaseOrder;

        //    editDto.PoDate = DateTime.Today;
        //    editDto.DeliveryDateExpected = DateTime.Today;
        //    editDto.Remarks = "";

        //    List<PurchaseOrderDetailViewDto> editDetailDto = ReturnObject.PurchaseOrderDetail;

        //    foreach (var pd in editDetailDto)
        //    {

        //    }

        //    var a = (from poDet in editDetailDto
        //             join mat in _materialRepo.GetAll()
        //             on poDet.MaterialRefId equals mat.Id
        //             select new PurchaseOrderDetailViewDto
        //             {
        //                 PoRefId = (int)poDet.PoRefId,
        //                 MaterialRefId = poDet.MaterialRefId,
        //                 Barcode = mat.Barcode,
        //                 MaterialRefName = mat.MaterialName,
        //                 QtyOrdered = poDet.QtyOrdered,
        //                 Price = poDet.Price,
        //                 DiscountOption = poDet.DiscountOption,
        //                 DiscountValue = poDet.DiscountValue,
        //                 TaxAmount = poDet.TaxAmount,
        //                 TotalAmount = poDet.TotalAmount,
        //                 UnitRefId = poDet.UnitRefId,
        //                 UnitRefName = poDet.UnitRefName,
        //                 Uom = poDet.Uom,
        //                 DefaultUnitId = mat.DefaultUnitId,
        //                 NetAmount = (poDet.QtyOrdered * poDet.Price),
        //                 PoStatus = poDet.PoStatus,
        //                 Remarks = poDet.Remarks,
        //                 TaxForMaterial = poDet.TaxForMaterial,
        //                 ApplicableTaxes = poDet.ApplicableTaxes,
        //                 UnitRefIdForMinOrder = poDet.UnitRefIdForMinOrder,
        //                 UnitRefNameForMinOrder = poDet.UnitRefNameForMinOrder,
        //                 MinimumOrderQuantity = poDet.MinimumOrderQuantity,
        //                 MaximumOrderQuantity = poDet.MaximumOrderQuantity
        //             });

        //    List<PurchaseOrderDetailViewDto> b = a.ToList();

        //    foreach (var lst in b)
        //    {
        //        var sm = await _suppliermaterialRepo.FirstOrDefaultAsync(t => t.MaterialRefId == lst.MaterialRefId && t.SupplierRefId == editDto.SupplierRefId && t.LocationRefId == editDto.LocationRefId);
        //        if (sm == null)
        //        {
        //            sm = await _suppliermaterialRepo.FirstOrDefaultAsync(t => t.MaterialRefId == lst.MaterialRefId && t.SupplierRefId == editDto.SupplierRefId && t.LocationRefId == null);
        //        }
        //        if (sm == null)
        //        {
        //            lst.UnitRefIdForMinOrder = lst.UnitRefId;
        //            lst.UnitRefNameForMinOrder = lst.UnitRefName;
        //            lst.MinimumOrderQuantity = 0;
        //            lst.MaximumOrderQuantity = 0;
        //        }
        //        else
        //        {
        //            lst.UnitRefIdForMinOrder = sm.UnitRefId;
        //            lst.UnitRefNameForMinOrder = _unitRepo.FirstOrDefault(t => t.Id == sm.UnitRefId).Name;
        //            lst.MinimumOrderQuantity = sm.MinimumOrderQuantity;
        //            lst.MaximumOrderQuantity = sm.MaximumOrderQuantity;
        //        }
        //    }
        //    return new GetPurchaseOrderForEditOutput
        //    {
        //        PurchaseOrder = editDto,
        //        PurchaseOrderDetail = b
        //    };
        //}

        //public async Task<List<MaterialVsSupplierDtos>> GetSupplierInfoForParticularMaterial(IdInput input)
        //{
        //    List<MaterialVsSupplierDtos> output = new List<MaterialVsSupplierDtos>();

        //    return output;
        //}

        


        //public async Task<SetPurchaseOrderDashBoardDto> GetDashBoardPurchaseOrder(IdInput input)
        //{
        //    Debug.WriteLine("PO Basic GetDashBoardPurchaseOrder Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));

        //    string poStatus = "P";// L("Pending");

        //    var lstpending = _purchaseorderRepo.GetAll().Where(p => p.LocationRefId == input.Id && p.PoCurrentStatus.Equals(poStatus)).ToList();
        //    int pendingCount = lstpending.Count();

        //    poStatus = "A"; // L("Approved");
        //    var lstApproved = _purchaseorderRepo.GetAll().Where(p => p.LocationRefId == input.Id && p.ApprovedTime >= DateTime.Today
        //            && (p.PoCurrentStatus.Equals(poStatus) || (p.PoCurrentStatus.Equals("H")))).ToList();
        //    int approvedCount = lstApproved.Count();


        //    var lstyetToReceived = _purchaseorderRepo.GetAll().Where(p => p.LocationRefId == input.Id && DbFunctions.TruncateTime(p.DeliveryDateExpected) < DateTime.Now && p.PoCurrentStatus.Equals(poStatus)).ToList();
        //    int yetToReceivedCount = lstyetToReceived.Count();

        //    poStatus = "X";// L("Declined");
        //    var lstDeclined = _purchaseorderRepo.GetAll().Where(p => p.LocationRefId == input.Id && p.ApprovedTime >= DateTime.Today && p.PoCurrentStatus.Equals(poStatus)).ToList();

        //    int declinedCount = lstDeclined.Count();

        //    Debug.WriteLine("PO Basic GetDashBoardPurchaseOrder End Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));

        //    return new SetPurchaseOrderDashBoardDto
        //    {
        //        TotalApproved = approvedCount,
        //        TotalPending = pendingCount,
        //        TotalYetToReceived = yetToReceivedCount,
        //        TotalDeclined = declinedCount
        //    };
        //}


        //public string Serialize(object dataToSerialize)
        //{
        //    if (dataToSerialize == null) return null;

        //    using (StringWriter stringwriter = new System.IO.StringWriter())
        //    {
        //        var serializer = new XmlSerializer(dataToSerialize.GetType());
        //        serializer.Serialize(stringwriter, dataToSerialize);
        //        return stringwriter.ToString();
        //    }

        //}

        //public static string SerializeToJSON(object dataToSerialize)
        //{
        //    if (dataToSerialize == null) return null;

        //    MemoryStream stream1 = new MemoryStream();
        //    DataContractJsonSerializer ser = new DataContractJsonSerializer(dataToSerialize.GetType());
        //    ser.WriteObject(stream1, dataToSerialize);

        //    stream1.Position = 0;
        //    StreamReader sr = new StreamReader(stream1);

        //    return sr.ReadToEnd();
        //}

        //public static T DeSerializeFromJSON<T>(string jsonText)
        //{
        //    if (String.IsNullOrWhiteSpace(jsonText)) return default(T);

        //    //MemoryStream stream1 = new MemoryStream();
        //    MemoryStream stream1 = new MemoryStream(Encoding.UTF8.GetBytes(jsonText));

        //    DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
        //    //  ser.WriteObject(stream1, dataToSerialize);
        //    //stream1.WriteByte(jsonText);
        //    stream1.Position = 0;
        //    // ser.ReadObject(stream1);

        //    //StreamReader sr = new StreamReader(stream1);

        //    return (T)ser.ReadObject(stream1);
        //}


        //public static T Deserialize<T>(string xmlText)
        //{
        //    if (String.IsNullOrWhiteSpace(xmlText)) return default(T);

        //    using (StringReader stringReader = new System.IO.StringReader(xmlText))
        //    {
        //        var serializer = new XmlSerializer(typeof(T));
        //        return (T)serializer.Deserialize(stringReader);
        //    }
        //}



        //public async Task<string> GetPurchaseOrderReportInNotePad(GetPurchaseOrderInput input)
        //{

        //    var poMas = _purchaseorderRepo.FirstOrDefault(t => t.Id == input.PurchaseOrderId);
        //    if (poMas == null)
        //    {
        //        return "";
        //    }

        //    List<PurchaseOrderDetailViewDto> editDetailDto;

        //    editDetailDto = await (from poDet in _purchaseorderdetailRepo.GetAll()
        //                                .Where(a => a.PoRefId == input.PurchaseOrderId)
        //                           join mat in _materialRepo.GetAll()
        //                           on poDet.MaterialRefId equals mat.Id
        //                           join un in _unitRepo.GetAll()
        //                           on mat.DefaultUnitId equals un.Id
        //                           join udet in _unitRepo.GetAll()
        //                            on mat.DefaultUnitId equals udet.Id
        //                           select new PurchaseOrderDetailViewDto
        //                           {
        //                               PoRefId = (int)poDet.PoRefId,
        //                               MaterialRefId = poDet.MaterialRefId,
        //                               MaterialRefName = mat.MaterialName,
        //                               QtyOrdered = poDet.QtyOrdered,
        //                               QtyReceived = poDet.QtyReceived,
        //                               UnitRefId = poDet.UnitRefId,
        //                               UnitRefName = udet.Name,
        //                               Price = poDet.Price,
        //                               DiscountOption = poDet.DiscountOption,
        //                               DiscountValue = poDet.DiscountValue,
        //                               TotalAmount = poDet.TotalAmount,
        //                               TaxAmount = poDet.TaxAmount,
        //                               NetAmount = poDet.NetAmount,
        //                               PoStatus = poDet.PoStatus,
        //                               Remarks = poDet.Remarks,
        //                               Uom = un.Name,
        //                               DefaultUnitId = mat.DefaultUnitId,

        //                           }).ToListAsync();

        //    var supplier = _supplierRepo.FirstOrDefault(t => t.Id == poMas.SupplierRefId);

        //    var billingLocation = _locationRepo.FirstOrDefault(t => t.Id == poMas.LocationRefId);
        //    var shippingLocation = _locationRepo.FirstOrDefault(t => t.Id == poMas.ShippingLocationId);

        //    TextFileWriter fp = new TextFileWriter();

        //    string fileName = System.Web.HttpContext.Current.Server.MapPath("~/Report/" + poMas.PoReferenceCode.Replace("/", "") + "PoReport.txt");

        //    int PageLenth = 80;

        //    fp.FileOpen(fileName, "O");
        //    fp.PrintTextLine(fp.PrintFormatCenter(L("PURCHASEORDER"), PageLenth));
        //    fp.PrintText(fp.PrintFormatLeft(L("PoReference") + ": " + poMas.PoReferenceCode, 63));
        //    fp.PrintTextLine(L("Date") + ": " + poMas.PoDate.ToString("dd-MMM-yyyy"));
        //    fp.PrintTextLine("");

        //    fp.PrintSepLine('=', PageLenth);

        //    fp.PrintTextLine("");
        //    fp.PrintTextLine(L("Supplier"));
        //    fp.PrintTextLine("~~~~~~~~~~");

        //    if (supplier.SupplierName.Length > 0)
        //        fp.PrintTextLine(supplier.SupplierName);
        //    if (supplier.Address1 != null)
        //        fp.PrintTextLine(supplier.Address1);
        //    if (supplier.Address2 != null)
        //        fp.PrintTextLine(supplier.Address2);
        //    if (supplier.Address3 != null && supplier.Address3.Length > 0)
        //        fp.PrintTextLine(supplier.Address3);
        //    if (supplier.City != null)
        //        fp.PrintTextLine(supplier.City);
        //    if (supplier.State != null)
        //        fp.PrintTextLine(supplier.State);

        //    fp.PrintTextLine("");
        //    fp.PrintSepLine('=', PageLenth);

        //    fp.PrintText(fp.PrintFormatLeft(L("BillingAddress"), 40));
        //    fp.PrintTextLine(fp.PrintFormatLeft(L("ShippingAddress"), 40));
        //    if (billingLocation.Name != null)
        //    {
        //        fp.PrintText(fp.PrintFormatLeft(billingLocation.Name, 40));
        //    }
        //    if (shippingLocation.Name != null)
        //    {
        //        fp.PrintText(fp.PrintFormatLeft(shippingLocation.Name, 40));
        //    }
        //    fp.PrintTextLine("");

        //    if (billingLocation.Address1 != null)
        //    {
        //        fp.PrintText(fp.PrintFormatLeft(billingLocation.Address1, 40));
        //    }
        //    if (shippingLocation.Address1 != null)
        //    {
        //        fp.PrintText(fp.PrintFormatLeft(shippingLocation.Address1, 40));
        //    }
        //    fp.PrintTextLine("");

        //    if (billingLocation.Address2 != null)
        //    {
        //        fp.PrintText(fp.PrintFormatLeft(billingLocation.Address2, 40));
        //    }
        //    if (shippingLocation.Address2 != null)
        //    {
        //        fp.PrintText(fp.PrintFormatLeft(shippingLocation.Address2, 40));
        //    }
        //    fp.PrintTextLine("");

        //    if (billingLocation.Address3 != null)
        //    {
        //        fp.PrintText(fp.PrintFormatLeft(billingLocation.Address3, 40));
        //    }
        //    if (shippingLocation.Address3 != null)
        //    {
        //        fp.PrintText(fp.PrintFormatLeft(shippingLocation.Address3, 40));
        //    }
        //    fp.PrintTextLine("");

        //    if (billingLocation.City != null)
        //    {
        //        fp.PrintText(fp.PrintFormatLeft(billingLocation.City, 40));
        //    }
        //    if (shippingLocation.City != null)
        //    {
        //        fp.PrintText(fp.PrintFormatLeft(shippingLocation.City, 40));
        //    }
        //    fp.PrintTextLine("");

        //    fp.PrintTextLine(L("PoDetailHeader"));

        //    fp.PrintSepLine('-', PageLenth);
        //    fp.PrintText(fp.PrintFormatLeft("#", 3));
        //    fp.PrintText(fp.PrintFormatLeft(L("Material"), 25));
        //    fp.PrintText(fp.PrintFormatRight(L("Qty"), 8));
        //    fp.PrintText(fp.PrintFormatRight(L("Unit"), 5));
        //    fp.PrintText(fp.PrintFormatRight(L("Cost"), 9));
        //    fp.PrintText(fp.PrintFormatRight(L("Total"), 10));
        //    fp.PrintText(fp.PrintFormatRight(L("Tax"), 9));
        //    fp.PrintText(fp.PrintFormatRight(L("LineTotal"), 11));
        //    fp.PrintTextLine("");
        //    fp.PrintSepLine('-', PageLenth);

        //    int loopIndex = 1;
        //    decimal TotalTaxAmount = 0m;
        //    decimal TotalAmount = 0m;

        //    foreach (var det in editDetailDto)
        //    {
        //        fp.PrintText(fp.PrintFormatLeft(loopIndex.ToString(), 3));
        //        fp.PrintText(fp.PrintFormatLeft(det.MaterialRefName, 25));
        //        fp.PrintText(fp.PrintFormatRight(det.QtyOrdered.ToString("G"), 8));
        //        fp.PrintText(fp.PrintFormatRight(det.Uom, 5));
        //        fp.PrintText(fp.PrintFormatRight(det.Price.ToString("G"), 9));
        //        fp.PrintText(fp.PrintFormatRight(det.TotalAmount.ToString("G"), 10));
        //        fp.PrintText(fp.PrintFormatRight(det.TaxAmount.ToString("G"), 9));
        //        fp.PrintText(fp.PrintFormatRight(det.NetAmount.ToString("G"), 11));
        //        fp.PrintTextLine("");
        //        loopIndex++;
        //        TotalTaxAmount = TotalTaxAmount + det.TaxAmount;
        //        TotalAmount = TotalAmount + det.TotalAmount;
        //    }

        //    fp.PrintTextLine(" ");
        //    fp.PrintSepLine('-', PageLenth);
        //    fp.PrintText(fp.PrintFormatLeft(L("SubTotal"), 51));
        //    fp.PrintText(fp.PrintFormatRight(TotalAmount.ToString("G"), 9));
        //    fp.PrintText(fp.PrintFormatRight(TotalTaxAmount.ToString("G"), 9));
        //    fp.PrintText(fp.PrintFormatRight(poMas.PoNetAmount.ToString("G"), 11));
        //    fp.PrintTextLine("");
        //    fp.PrintSepLine('-', PageLenth);
        //    fp.PrintTextLine(" ");
        //    fp.PrintTextLine(L("TermsConditions"));
        //    fp.PrintTextLine("------------------------------");
        //    fp.PrintTextLine(L("Term1"));
        //    fp.PrintTextLine(L("Term2"));
        //    fp.PrintTextLine(L("Term3"));
        //    fp.PrintTextLine(L("Term4", poMas.DeliveryDateExpected.ToString("dd-MMM-yyyy")));
        //    fp.PrintTextLine(" "); fp.PrintTextLine(" ");
        //    fp.PrintSepLine('-', PageLenth);
        //    fp.PrintTextLine(fp.PrintFormatCenter("REPORT GENERATED AT " + DateTime.Now.ToString("f"), PageLenth));
        //    fp.PrintSepLine('-', PageLenth);
        //    fp.PrintTextLine(" ");

        //    //Process.Start("NOTEPAD.EXE ", fileName);

        //    string retText = System.IO.File.ReadAllText(fileName);

        //    return retText;
        //}

        //public async Task SendPurchaseOrderEmail(SetPurchaseOrderPrint input)
        //{
        //    GetPurchaseOrderForEditOutput output = await GetPurchaseOrderForEdit(new NullableIdInput { Id = input.PurchaseOrderId });

        //    PurchaseOrderEditDto pomas;

        //    if (output.PurchaseOrder.Id.HasValue)
        //    {
        //        pomas = output.PurchaseOrder;
        //    }
        //    else
        //    {
        //        return;
        //    }

        //    int supplierRefId = pomas.SupplierRefId;

        //    var supplier = _supplierRepo.FirstOrDefault(t => t.Id == supplierRefId);

        //    string supplierEmail = supplier.Email;

        //    if (supplierEmail.IsNullOrEmpty())
        //    {
        //        throw new UserFriendlyException(L("SupplierEmailErr"));
        //    }

        //    string supplierName = supplier.SupplierName;
        //    string supplierAddress1 = supplier.Address1;
        //    string supplierAddress2 = supplier.Address2;
        //    string supplierAddress3 = supplier.Address3;
        //    string supplierCity = supplier.City;
        //    string supplierState = supplier.State;
        //    string supplierCountry = supplier.Country;

        //    var billingLocation = _locationRepo.FirstOrDefault(t => t.Id == pomas.LocationRefId);
        //    string billingLocationName = billingLocation.Name;
        //    string billingLocationAddress1 = billingLocation.Address1;
        //    string billingLocationAddress2 = billingLocation.Address2;
        //    string billingLocationAddress3 = billingLocation.Address3;
        //    string billingLocationCity = billingLocation.City;
        //    string billingLocationState = billingLocation.State;
        //    string billingLocationCountry = billingLocation.Country;

        //    var companyInfo = _companyRepo.FirstOrDefault(t => t.Id == billingLocation.CompanyRefId);
        //    string companyName = companyInfo.Name;
        //    string companyAddress1 = companyInfo.Address1;
        //    string companyAddress2 = companyInfo.Address2;
        //    string companyAddress3 = companyInfo.Address3;
        //    string companyCity = companyInfo.City;

        //    var shippingLocation = _locationRepo.FirstOrDefault(t => t.Id == pomas.ShippingLocationId);

        //    string shippingLocationName = "";
        //    string shippingLocationAddress1 = "";
        //    string shippingLocationAddress2 = "";
        //    string shippingLocationAddress3 = "";
        //    string shippingLocationCity = "";
        //    string shippingLocationState = "";
        //    string shippingLocationCountry = "";

        //    if (pomas.LocationRefId == pomas.ShippingLocationId)
        //    {
        //        shippingLocationName = L("SameAsBillingAddress");
        //    }
        //    else
        //    {
        //        shippingLocationName = shippingLocation.Name;
        //        shippingLocationAddress1 = shippingLocation.Address1;
        //        shippingLocationAddress2 = shippingLocation.Address2;
        //        shippingLocationAddress3 = shippingLocation.Address3;
        //        shippingLocationCity = shippingLocation.City;
        //        shippingLocationState = shippingLocation.State;
        //        shippingLocationCountry = shippingLocation.Country;
        //    }


        //    var mailMessage = new StringBuilder();

        //    string htmlLine;
        //    htmlLine = "<!DOCTYPE HTML PUBLIC \" -//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">";
        //    mailMessage.AppendLine(htmlLine);
        //    htmlLine = "<html style=\"-webkit-text-size-adjust: none;-ms-text-size-adjust: none;\">";
        //    mailMessage.AppendLine(htmlLine);
        //    htmlLine = "<head>";
        //    mailMessage.AppendLine(htmlLine);
        //    htmlLine = "<meta http-equiv=\"Content - Type\" content=\"text / html; charset = utf - 8\">";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<title>" + companyName + L("PurchaseOrder") + "</title>";
        //    mailMessage.AppendLine(htmlLine);
        //    htmlLine = "</head>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<body style=\"padding: 0px; margin: 0px; \">";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<div id=\"mailsub\" class=\"notification\">";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<center>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<h1>" + companyName + "</h1>";
        //    mailMessage.AppendLine(htmlLine);

        //    if (!companyAddress1.IsNullOrEmpty())
        //    {
        //        htmlLine = "<p>" + companyAddress1 + "</p>";
        //        mailMessage.AppendLine(htmlLine);
        //    }

        //    if (!companyAddress2.IsNullOrEmpty())
        //    {
        //        htmlLine = "<p>" + companyAddress2 + "</p>";
        //        mailMessage.AppendLine(htmlLine);
        //    }

        //    if (!companyAddress3.IsNullOrEmpty())
        //    {
        //        htmlLine = "<p>" + companyAddress3 + "</p>";
        //        mailMessage.AppendLine(htmlLine);
        //    }

        //    if (!companyCity.IsNullOrEmpty())
        //    {
        //        htmlLine = "<p>" + companyCity + "</p>";
        //        mailMessage.AppendLine(htmlLine);
        //    }

        //    htmlLine = "</center>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<hr />";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<h2 style=\"text-align: center;\">" + L("PurchaseOrder") + "</h2>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<table id=\"header\" style=\"width: 100%;\">";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<tbody>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<tr>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<td><strong><strong>" + L("PoReference") + ": " + " </strong></strong><strong>" + pomas.PoReferenceCode + "</strong></td>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<td style=\"text-align: right;\"><strong>" + L("Date") + ": " + pomas.PoDate.ToString("dd-MMM-yy") + "</strong></td>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "</tr>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "</tbody>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "</table>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<hr/>";
        //    mailMessage.AppendLine(htmlLine);


        //    htmlLine = "<h3><span style=\"text-decoration: underline;\"><strong>" + L("Supplier") + "</strong></span></h3>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<p>" + supplierName + "</p>";
        //    mailMessage.AppendLine(htmlLine);

        //    if (!supplierAddress1.IsNullOrEmpty())
        //    {
        //        htmlLine = "<p>" + supplierAddress1 + "</p>";
        //        mailMessage.AppendLine(htmlLine);
        //    }

        //    if (!supplierAddress2.IsNullOrEmpty())
        //    {
        //        htmlLine = "<p>" + supplierAddress2 + "</p>";
        //        mailMessage.AppendLine(htmlLine);
        //    }

        //    if (!supplierAddress3.IsNullOrEmpty())
        //    {
        //        htmlLine = "<p>" + supplierAddress3 + "</p>";
        //        mailMessage.AppendLine(htmlLine);
        //    }

        //    if (!supplierCity.IsNullOrEmpty())
        //    {
        //        htmlLine = "<p>" + supplierCity + "</p>";
        //        mailMessage.AppendLine(htmlLine);
        //    }

        //    if (!supplierState.IsNullOrEmpty())
        //    {
        //        htmlLine = "<p>" + supplierState + "</p>";
        //        mailMessage.AppendLine(htmlLine);
        //    }

        //    if (!supplierCountry.IsNullOrEmpty())
        //    {
        //        htmlLine = "<p>" + supplierCountry + "</p>";
        //        mailMessage.AppendLine(htmlLine);
        //    }

        //    htmlLine = "<hr />";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<table id=\"address\" width=\"100%\">";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<tbody>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<tr>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<td><strong><u>" + L("BillingAddress") + "</u></strong></td>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<td><strong><u>" + L("ShippingAddress") + "</u></strong></td>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "</tr>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<tr>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<td>" + billingLocationName + "</td>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<td>" + shippingLocationName + "</td>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "</tr>";
        //    mailMessage.AppendLine(htmlLine);

        //    if (billingLocationAddress1.IsNullOrEmpty() == false || shippingLocationAddress1.IsNullOrEmpty() == false)
        //    {
        //        htmlLine = "<tr>";
        //        mailMessage.AppendLine(htmlLine);

        //        htmlLine = "<td>" + billingLocationAddress1 + " </td>";
        //        mailMessage.AppendLine(htmlLine);

        //        htmlLine = "<td>" + shippingLocationAddress1 + "</td>";
        //        mailMessage.AppendLine(htmlLine);

        //        htmlLine = "</tr>";
        //        mailMessage.AppendLine(htmlLine);
        //    }


        //    if (billingLocationAddress2.IsNullOrEmpty() == false || shippingLocationAddress2.IsNullOrEmpty() == false)
        //    {
        //        htmlLine = "<tr>";
        //        mailMessage.AppendLine(htmlLine);

        //        htmlLine = "<td>" + billingLocationAddress2 + " </td>";
        //        mailMessage.AppendLine(htmlLine);

        //        htmlLine = "<td>" + shippingLocationAddress2 + "</td>";
        //        mailMessage.AppendLine(htmlLine);

        //        htmlLine = "</tr>";
        //        mailMessage.AppendLine(htmlLine);
        //    }


        //    if (billingLocationAddress3.IsNullOrEmpty() == false || shippingLocationAddress3.IsNullOrEmpty() == false)
        //    {
        //        htmlLine = "<tr>";
        //        mailMessage.AppendLine(htmlLine);

        //        htmlLine = "<td>" + billingLocationAddress3 + " </td>";
        //        mailMessage.AppendLine(htmlLine);

        //        htmlLine = "<td>" + shippingLocationAddress2 + "</td>";
        //        mailMessage.AppendLine(htmlLine);

        //        htmlLine = "</tr>";
        //        mailMessage.AppendLine(htmlLine);
        //    }


        //    if (billingLocationCity.IsNullOrEmpty() == false || shippingLocationCity.IsNullOrEmpty() == false)
        //    {
        //        htmlLine = "<tr>";
        //        mailMessage.AppendLine(htmlLine);

        //        htmlLine = "<td>" + billingLocationCity + " </td>";
        //        mailMessage.AppendLine(htmlLine);

        //        htmlLine = "<td>" + shippingLocationCity + "</td>";
        //        mailMessage.AppendLine(htmlLine);

        //        htmlLine = "</tr>";
        //        mailMessage.AppendLine(htmlLine);
        //    }


        //    if (billingLocationState.IsNullOrEmpty() == false || shippingLocationState.IsNullOrEmpty() == false)
        //    {
        //        htmlLine = "<tr>";
        //        mailMessage.AppendLine(htmlLine);

        //        htmlLine = "<td>" + billingLocationState + " </td>";
        //        mailMessage.AppendLine(htmlLine);

        //        htmlLine = "<td>" + shippingLocationState + "</td>";
        //        mailMessage.AppendLine(htmlLine);

        //        htmlLine = "</tr>";
        //        mailMessage.AppendLine(htmlLine);
        //    }



        //    htmlLine = "</tbody>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "</table>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<hr />";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<p>" + L("PoDetailHeader") + " </p>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<table id=\"detail\" cellpadding=\"7\" border=\"1\" width=\"100%\">";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<tbody>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<tr>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<td>#.Material</td>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<td style=\"text-align: right;\">Qty</td>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<td style=\"text-align: right;\">UOM</td>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<td style=\"text-align: right;\">Cost</td>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<td style=\"text-align: right;\">Total</td>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<td style=\"text-align: right;\">Tax</td>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<td style=\"text-align: right;\">Line Total</td>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "</tr>";
        //    mailMessage.AppendLine(htmlLine);

        //    int sno = 1;
        //    decimal subTotalAmount = 0;
        //    decimal subTaxAmount = 0;

        //    foreach (var poDet in output.PurchaseOrderDetail)
        //    {
        //        htmlLine = "<tr>";
        //        mailMessage.AppendLine(htmlLine);
        //        string aliasName = "";

        //        if (poDet.SupplierMaterialAliasName != null)
        //        {
        //            aliasName = " " + L("Alias") + " :" + poDet.SupplierMaterialAliasName;
        //        }

        //        htmlLine = "<td>" + sno.ToString().PadLeft(2) + "." + poDet.MaterialRefName + aliasName + "</td>";
        //        mailMessage.AppendLine(htmlLine);

        //        htmlLine = "<td style=\"text-align: right;\">" + poDet.QtyOrdered.ToString("###0.000") + " </td>";
        //        mailMessage.AppendLine(htmlLine);

        //        htmlLine = "<td style=\"text-align: right;\">" + poDet.Uom + "</td>";
        //        mailMessage.AppendLine(htmlLine);

        //        htmlLine = "<td style=\"text-align: right;\">" + poDet.Price.ToString("###0.00") + " </td>";
        //        mailMessage.AppendLine(htmlLine);

        //        htmlLine = "<td style=\"text-align: right;\">" + poDet.TotalAmount.ToString("###0.00") + "</td>";
        //        mailMessage.AppendLine(htmlLine);

        //        htmlLine = "<td style=\"text-align: right;\">" + poDet.TaxAmount.ToString("###0.00") + "</td>";
        //        mailMessage.AppendLine(htmlLine);

        //        htmlLine = "<td style=\"text-align: right;\">" + poDet.NetAmount.ToString("###0.00") + "</td>";
        //        mailMessage.AppendLine(htmlLine);

        //        htmlLine = "</tr>";
        //        mailMessage.AppendLine(htmlLine);

        //        subTaxAmount = subTaxAmount + poDet.TaxAmount;
        //        subTotalAmount = subTotalAmount + poDet.TotalAmount;
        //        sno++;
        //    }

        //    htmlLine = "<tr>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<td style=\"text-align: right;\">" + L("SubTotal") + "</td>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<td></td>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<td></td>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<td></td>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<td style=\"text-align: right;\">" + subTotalAmount.ToString("####0.00") + "</td>";
        //    mailMessage.AppendLine(htmlLine);


        //    htmlLine = "<td style=\"text-align: right;\">" + subTaxAmount.ToString("####0.00") + "</td>";
        //    mailMessage.AppendLine(htmlLine);


        //    htmlLine = "<td style=\"text-align: right;\">" + pomas.PoNetAmount.ToString("####0.00") + "</td>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "</tr>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "</tbody>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "</table>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<hr />";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<h3><strong>" + L("TermsConditions") + "</strong></h3>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<ul style=\"list - style - type: circle; \">";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<li> <b>" + L("TermDelivery", pomas.DeliveryDateExpected.ToString("dd-MMM-yy")) + "</b></li>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<li>" + L("Term1") + "</li>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<li>" + L("Term2") + "</li>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<li>" + L("Term3") + "</li>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "</ul>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<hr />";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "<p><span style=\"color: #0000ff;\">" + L("SignNotRequired") + "</span></p>";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "</div>";    //mailsub div end
        //    mailMessage.AppendLine(htmlLine);
        //    htmlLine = "</body>";
        //    mailMessage.AppendLine(htmlLine);
        //    htmlLine = "</html>";
        //    mailMessage.AppendLine(htmlLine);

        //    //try
        //    //{
        //    //    await _emailSender.SendAsync(supplierEmail, "Sub", mailMessage.ToString(), true);

        //    //    MailMessage msg = new MailMessage();

        //    //    #region Adding Email Address to Mail
        //    //    //msg.From = new MailAddress("hr@company");
        //    //    msg.To.Add(new MailAddress(supplierEmail));

        //    //    List<string> emailList = new List<string>();
        //    //    var ccEmailString = SettingManager.GetSettingValue("App.House.PurchaseOrderCCMailList");
        //    //    string[] companyEmailList = ccEmailString.Split(";");

        //    //    foreach (var ccEmail in companyEmailList)
        //    //    {
        //    //        msg.CC.Add(new MailAddress(ccEmail));
        //    //    }
        //    //    #endregion

        //    //    msg.Subject = L("PurchaseOrder") + " " + output.PurchaseOrder.PoReferenceCode + " - " + companyName + " - " + output.PurchaseOrder.LocationRefName;
        //    //    msg.IsBodyHtml = true;
        //    //    msg.Body = mailMessage.ToString();

        //    //    await _emailSender.SendAsync(msg, true);


        //    //}
        //    //catch (Exception ex)
        //    //{

        //    //    throw new UserFriendlyException(ex.Message + " " + ex.InnerException);
        //    //}

        //    MailMessage msg = new MailMessage();

        //    SmtpClient client = new SmtpClient();
        //    client.DeliveryMethod = SmtpDeliveryMethod.Network;
        //    client.EnableSsl = true;
        //    client.Host = SettingManager.GetSettingValue("Abp.Net.Mail.Smtp.Host");
        //    client.Port = SettingManager.GetSettingValue<int>("Abp.Net.Mail.Smtp.Port"); //587;

        //    string un = SettingManager.GetSettingValue("Abp.Net.Mail.Smtp.UserName");
        //    string pw = SettingManager.GetSettingValue("Abp.Net.Mail.Smtp.Password");

        //    System.Net.NetworkCredential credentials =
        //      new System.Net.NetworkCredential(un,pw);
        //    client.UseDefaultCredentials = false;
        //    client.Credentials = credentials;

        //    string fromAddress = SettingManager.GetSettingValue("Abp.Net.Mail.DefaultFromAddress");

        //    msg.From = new MailAddress(fromAddress);
        //    msg.To.Add(supplierEmail);

        //    List<string> emailList = new List<string>();
        //    var ccEmailString = SettingManager.GetSettingValue("App.House.PurchaseOrderCCMailList");
        //    string[] companyEmailList = ccEmailString.Split(";");

        //    foreach (var ccEmail in companyEmailList)
        //    {
        //        msg.CC.Add(new MailAddress(ccEmail));
        //    }

        //    msg.Subject = L("PurchaseOrder") + " " + output.PurchaseOrder.PoReferenceCode + " - " + companyName + " - " + output.PurchaseOrder.LocationRefName;
        //    msg.IsBodyHtml = true;
        //    msg.Body = mailMessage.ToString();
        //    try
        //    {
        //        if (msg.To.Count > 0)
        //            client.Send(msg);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new UserFriendlyException(L("NotAbleToSendMailError", ex.Message + ex.InnerException));
        //    }
        //}


        //public async Task<MessageOutput> CheckCanEditThePurchaseOrder(IdInput input)
        //{
        //    MessageOutput output = new MessageOutput();
        //    var poDetails = await _purchaseorderdetailRepo.GetAllListAsync(t => t.PoRefId == input.Id);
        //    var anyReceiptsAlreadyDone = poDetails.Where(t => t.QtyReceived > 0).Count();
        //    if (anyReceiptsAlreadyDone > 0)
        //    {
        //        output.Count = anyReceiptsAlreadyDone;
        //        output.ErrorMessage = L("ReferenceExists",L("Receipt"),L("PurchaseOrder") + " : " + input.Id);
        //        output.SuccessFlag = false;
        //    }
        //    else
        //    {
        //        output.SuccessFlag = true;
        //    }
        //    return output;
        //}

    }





}