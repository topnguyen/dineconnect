﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Cater.Quotation.Dtos;

namespace DinePlan.DineConnect.House.Transaction
{
    public interface IPurchaseOrderExtendedAppService : IApplicationService
    {
        Task<List<AutoPurchaseOrderConsolidatedDto>> GetAutoPoDetails(LocationInputDto input);
        Task<List<AutoPurchaseOrderConsolidatedDto>> GetAutoPoBasedOnTransferRequest(GetRequestAsPO input);
        Task<List<AutoPurchaseOrderConsolidatedDto>> GetAutoPoBasedOnCaterRequest(GetCaterRequestMaterialAsPO input);

        //Task<FileDto> GetAllToExcel(GetPurchaseOrderInput input);
        //Task<GetPurchaseOrderForEditOutput> GetPurchaseOrderForEdit(NullableIdInput nullableIdInput);
        //Task<GetPurchaseIdAndReference> CreateOrUpdatePurchaseOrder(CreateOrUpdatePurchaseOrderInput input);
        //Task DeletePurchaseOrder(IdInput input);

        //Task<PagedResultOutput<PurchaseOrderListWithRefNameDto>> GetAllWithRefName(GetPurchaseOrderInput input);
        //Task<GetPurchaseOrderForEditOutput> GetPoDetailBasedOnPoReferenceCode(NullableIdInput input);

        //Task<GetPurchaseOrderForEditOutput> GetTemplateObjectForEdit(GetObjectFromString input);

        //Task CreateDraft(CreateOrUpdatePurchaseOrderInput input);

        //Task SetPurchaseOrderStatus(PurchaseOrderStatus input);
        //Task<SetPurchaseOrderDashBoardDto> GetDashBoardPurchaseOrder(IdInput input);

        //Task<string> GetPurchaseOrderReportInNotePad(GetPurchaseOrderInput input);

        //Task SendPurchaseOrderEmail(SetPurchaseOrderPrint input);

        //Task<ListResultOutput<PurchaseOrderListDto>> GetPurchaseReferenceNumber(GetPoReferenceDto input);

        //Task SetCompletePurchaseOrder(PurchaseOrderStatus input);



        //Task<List<MaterialVsSupplierDtos>> GetSupplierInfoForParticularMaterial(IdInput input);

        //Task<PoPendingDashBoardAlertDto> getPoPendingStatus(int userId, int inputLocationRefId);
        //Task<BulkPoMessageOutput> CreateBulkPurchaseOrder(BulkPoPreparationDto input);
        //Task<MessageOutput> CheckCanEditThePurchaseOrder(IdInput input);


    }
}