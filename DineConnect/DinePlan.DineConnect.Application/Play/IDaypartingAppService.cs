﻿
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Play.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Play
{
    public interface IDaypartAppService : IApplicationService
    {
        Task<IdInput> CreateOrUpdateDaypart(CreateOrUpdateDaypartInput input);
        Task<PagedResultOutput<DaypartListDto>> GetAll(GetDaypartListInputDto input);
        Task<GetDaypartForEditOutput> GetDaypartForEdit(NullableIdInput input);
        Task DeleteDaypart(NullableIdInput input);
    }
}
