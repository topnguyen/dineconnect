﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Play.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Play
{
    public interface IDisplayAppService: IApplicationService
    {
        Task<IdInput> CreateOrUpdateDisplay(CreateOrUpdateDisplayInput input);
        Task<PagedResultOutput<DisplayListDto>> GetAll(GetDisplayListInputDto input);
        Task<GetDisplayForEditOutput> GetDisplayForEdit(NullableIdInput input);
        Task<DisplayDeleteOutput> DeleteDisplay(NullableIdInput input);
        Task<object> RegisterDisplay(ConnectDisplayInput input);
        Task<object> CheckDisplayRegistration(CheckDisplayRegistrationInput input);
        Task<object> Unregister(UnregisterDisplayInput input);
    }
}
