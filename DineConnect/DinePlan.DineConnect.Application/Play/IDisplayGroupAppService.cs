﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Play.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Play
{
    public interface IDisplayGroupAppService: IApplicationService
    {
        Task<IdInput> CreateOrUpdateDisplayGroup(CreateOrUpdateDisplayGroupInput input);
        Task<PagedResultOutput<DisplayGroupListDto>> GetAll(GetDisplayGroupListInputDto input);
        Task<PagedResultOutput<DisplayListDto>> GetDisplaysOfGroup(GetDisplaysOfGroupListInputDto input);
        Task<GetDisplayGroupForEditOutput> GetDisplayGroupForEdit(NullableIdInput input);
        Task DeleteDisplayGroup(NullableIdInput input);
    }
}
