﻿
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Play.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Play
{
    public interface IScheduledEventAppService : IApplicationService
    {
        Task<IdInput> CreateOrUpdateScheduledEvent(CreateOrUpdateScheduledEventInput input);
        Task<PagedResultOutput<ScheduledEventListDto>> GetAll(GetScheduledEventInput input);
        Task<GetScheduledEventOutput> GetScheduledEventForUpdate(NullableIdInput input);
        Task DeleteScheduledEvent(DeleteScheduledEventInput input);
    }
}
