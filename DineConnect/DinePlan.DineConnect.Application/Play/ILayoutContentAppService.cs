﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Play.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Play
{
    public interface ILayoutContentAppService: IApplicationService
    {
        Task<List<UpdateLayoutContentOutput>> CreateOrUpdateLayoutContent(
            CreateOrUpdateLayoutContentInput input);
        Task<bool> InitializeContents(InitializeLayoutContentsInput input);
        Task<List<LayoutContentListDto>> GetLayoutContents(IdInput input);
    }
}
