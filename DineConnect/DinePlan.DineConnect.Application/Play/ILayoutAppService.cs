﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Play.Dto;
using DinePlan.DineConnect.Play.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Play
{
    public interface ILayoutAppService: IApplicationService
    {
        Task <int> CreateOrUpdateLayout(CreateOrUpdateLayoutInput input);
        Task<DeleteLayoutOutput> DeleteLayout(IdInput input);
        Task<PagedResultOutput<LayoutListDto>> GetAll(GetLayoutListInputDto inputDto);
        Task<GetLayoutForEditOutput> GetLayoutForEdit(NullableIdInput input);
        Task<LayoutContentTemplate> GetLayoutContentTemplate(int id);
        Task<List<LayoutContentListDto>> GetContents(IdInput input);
        Task<List<UpdateLayoutContentOutput>> CreateOrUpdateLayoutContent(
            CreateOrUpdateLayoutContentInput input);
        Task<LayoutListDto> GetLayout(NullableIdInput input);
        Task<LayoutListDto> GetLayoutForDisplay(GetLayoutForDisplayInput displayId);
        List<string> GetLayoutTemplates();
    }
}
