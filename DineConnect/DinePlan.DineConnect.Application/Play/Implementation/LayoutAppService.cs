﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Play.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Play.Implementation
{
    public class LayoutAppService : DineConnectAppServiceBase, ILayoutAppService
    {
        private readonly ISyncAppService _syncAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Layout> _layoutRepository;
        private readonly LayoutContentAppService _layoutContentService;
        private readonly IRepository<ScheduledEvent> _scheduledEventRepository;
        private readonly IRepository<Daypart> _daypartRepository;
        private readonly IRepository<Display> _DisplayRepository;

        public LayoutAppService(IUnitOfWorkManager unitOfWorkManager,
            IRepository<Layout> layoutRepository,
            IRepository<ScheduledEvent> scheduledEventRepository,
            IRepository<Daypart> daypartRepository,
            IRepository<Display> displayRepository,
            ISyncAppService syncAppService,
            LayoutContentAppService layoutContentService)
        {
            _syncAppService = syncAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _layoutRepository = layoutRepository;
            _layoutContentService = layoutContentService;
            _scheduledEventRepository = scheduledEventRepository;
            _daypartRepository = daypartRepository;
            _DisplayRepository = displayRepository;
        }
        public async Task<int> CreateOrUpdateLayout(CreateOrUpdateLayoutInput input)
        {
            var output = -1;
            if (input.Layout.Id.HasValue &&
                input.Layout.Id > 0)
            {
                var updateResult = await UpdateLayout(input);
                output = updateResult.Id;
            }
            else {
                output = await CreateLayout(input);
            }

            return output;
        }

        public async Task<PagedResultOutput<LayoutListDto>> GetAll(
            GetLayoutListInputDto input) {
            var allItems = _layoutRepository.GetAll()
                .Include(l => l.Resolution)
                .Where(l => l.IsDeleted == input.IsDeleted);

            allItems = allItems.WhereIf(!input.Filter.IsNullOrEmpty(),
                l => l.Name.Contains(input.Filter)||l.Description.Contains(input.Filter));

            var output = new PagedResultOutput<LayoutListDto>();

            var sortedItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            output.Items = sortedItems.MapTo<List<LayoutListDto>>();
            output.TotalCount = output.Items.Count;

            LayoutContentTemplates templates = new LayoutContentTemplates();
            foreach (var item in output.Items)
            {
                item.Template = templates.GetLayoutContentTemplate(item.TemplateId);
            }

            return output;
        }

        public async Task<LayoutListDto> GetLayout(NullableIdInput input) 
        {
            LayoutListDto output = null;
            if (input.Id.HasValue) {
                var item = await _layoutRepository.GetAll()
                    .Include(l => l.Resolution)
                    .Where(l => l.Id == input.Id).FirstOrDefaultAsync();
                output = item.MapTo<LayoutListDto>();
            }   
            return output;
        }

        public async Task<GetLayoutForEditOutput> GetLayoutForEdit(NullableIdInput input) {
            GetLayoutForEditOutput output = new GetLayoutForEditOutput();

            if (input.Id.HasValue)
            {
                var layout = await _layoutRepository.GetAsync(input.Id.Value);
                output.Layout = layout.MapTo<LayoutEditDto>();
            }
            else {
                output.Layout = new LayoutEditDto();
            }

            return output;
        }

        protected virtual async Task<int> CreateLayout(CreateOrUpdateLayoutInput input)
        {
            var layout = input.Layout.MapTo<Layout>();
            var output = -1;
            output = await _layoutRepository.InsertAndGetIdAsync(layout);
            var LayoutContentTemplates = new LayoutContentTemplates();
            var lcInput = new InitializeLayoutContentsInput()
            {
                LayoutId = output,
                RegionCount = LayoutContentTemplates
                    .GetLayoutContentTemplate(input.Layout.TemplateId).ContentCount
            };
            await _layoutContentService.InitializeContents(lcInput);

            return output;
        }

        protected virtual async Task<UpdateLayoutOutput> UpdateLayout(
            CreateOrUpdateLayoutInput input)
        {
            UpdateLayoutOutput output = null;
            var layout = input.Layout.MapTo<Layout>();
            var updateResult = await _layoutRepository.UpdateAsync(layout);
            output = updateResult.MapTo<UpdateLayoutOutput>();
            return output;
        }

        public async Task<DeleteLayoutOutput> DeleteLayout(IdInput input)
        {
            var deletion = new DeleteLayoutOutput();
            var sharedEventNames = LayoutUsedBy(input.Id);

            if (sharedEventNames != null && sharedEventNames.Length > 0)
            {
                deletion.Success = false;
                deletion.UsedByNames = sharedEventNames;
            }
            else {
                await _layoutRepository.DeleteAsync(input.Id);
                deletion.Success = true;
                deletion.UsedByNames = null;
            }
            
            return deletion;
        }

        private string[] LayoutUsedBy(int? layoutId) {

            var scheduledEvents = _scheduledEventRepository.GetAll()
                .AsNoTracking()
                .Where(e => e.LayoutId == layoutId)
                .Select(e => e.Name).ToList();
            return scheduledEvents.ToArray();
        }

        public async Task<LayoutContentTemplate> GetLayoutContentTemplate(int id) {
            var layout = await GetLayoutForEdit(new NullableIdInput() { Id = id });
            LayoutContentTemplates templates = new LayoutContentTemplates();
            var template = templates.GetLayoutContentTemplate(layout.Layout.TemplateId);
            return template;
        }

        public async Task<List<LayoutContentListDto>> GetContents(IdInput input)
        {
            return await _layoutContentService.GetLayoutContents(input);
        }

        public async Task<List<UpdateLayoutContentOutput>> CreateOrUpdateLayoutContent(
            CreateOrUpdateLayoutContentInput input)
        {
            return await _layoutContentService.CreateOrUpdateLayoutContent(input);
        }

        public async Task<LayoutListDto> GetLayoutForDisplay(GetLayoutForDisplayInput input) {

            if (!DisplayIsEnabled(input.DisplayId)) return null;

            var scheduledEvents = GetScheduledEvents(input);

            var layout = scheduledEvents?.FirstOrDefault()?.Layout;

            if (layout != null) { 
                 layout.Contents = await _layoutContentService.GetLayoutContents(new IdInput() { Id = layout.Id??0});
                var path = "/FileUpload/DownloadFile";

                if (layout.Contents != null && layout.Contents.Count > 0) {
                    foreach (var content in layout.Contents)
                    {
                        content.Path = $"{path}?fileName={content.SystemFileName}&url={content.SystemFileName}";
                    }    
                }

                LayoutContentTemplates templates = new LayoutContentTemplates();
                layout.Template = templates.GetLayoutContentTemplate(layout.TemplateId);
                return layout;
            }

            return null;
            
        }

        private bool DisplayIsEnabled(int displayId) {
            var getResult = _DisplayRepository.GetAll()
                .AsNoTracking()
                .Where(d => d.Id == displayId).FirstOrDefault();
            if (getResult != null)
            {
                var enabled = getResult.Active;
                return enabled;
            }
            else {
                return false;
            }
        }

        private List<ScheduledEventListDto> GetScheduledEvents(GetLayoutForDisplayInput input) {
            System.Diagnostics.Debug.Print(SimpleJson.SimpleJson.SerializeObject(input));

            var items = _scheduledEventRepository.GetAll()
                .Where(e =>
                    e.DisplayId == input.DisplayId
                ).ToList();

            var scheduledEvents = items.MapTo<List<ScheduledEventListDto>>();

            var displayGroupIds = _DisplayRepository.GetAll()
                .Include(d=>d.DisplayGroups)
                .Where(d => d.Id == input.DisplayId).FirstOrDefault()
                .DisplayGroups.Select(g=>g.Id).ToArray();
                

            items = _scheduledEventRepository.GetAll()
                .Where(e =>
                    displayGroupIds.Contains(e.DisplayGroupId ?? 0)
                ).ToList();

            var tmpItems = items.MapTo<List<ScheduledEventListDto>>();

            scheduledEvents.AddRange(tmpItems);

            ApplyDaypartTime(scheduledEvents, input.DisplayTime);

            scheduledEvents = scheduledEvents.Where(e=>
                input.DisplayTime >= e.StartTime &&
                input.DisplayTime <= e.EndTime
            ).OrderByDescending(e=>e.Priority).ToList();

            return scheduledEvents;
        }

        /*
        private static DateTime GetDaypartStartTime(ScheduledEvent scheduledEvent, IRepository<Daypart> daypartRepository) {
            var daypartId = scheduledEvent.DaypartId ?? 0;
            var daypart = daypartRepository.GetAll()
                .Where(e=>e.Id == daypartId).FirstOrDefault();
            DateTime date;
            if (daypart != null)
            {
                date = new DateTime(
                    scheduledEvent.StartTime.Year,
                    scheduledEvent.StartTime.Month,
                    scheduledEvent.StartTime.Day,
                    daypart.StartHour,
                    daypart.StartMinute,
                    0
                );
            }
            else {
                date = scheduledEvent.StartTime;
            }

            return date;
        }

        private static DateTime GetDaypartEndTime(ScheduledEvent scheduledEvent, IRepository<Daypart> daypartRepository)
        {
            var daypartId = scheduledEvent.DaypartId ?? 0;
            var daypart = daypartRepository.GetAll()
                .Where(e=>e.Id == daypartId).FirstOrDefault();
            DateTime date;

            if (daypart != null)
            {
                date = new DateTime(
                    scheduledEvent.StartTime.Year,
                    scheduledEvent.StartTime.Month,
                    scheduledEvent.StartTime.Day,
                    daypart.EndHour,
                    daypart.EndMinute,
                    59
                );
            }
            else
            {
                date = scheduledEvent.EndTime ?? DateTime.MaxValue;
            }

            return date;
        }
        */
        private void ApplyDaypartTime(List<ScheduledEventListDto> scheduledEvents,DateTime displayTime) {
            foreach (var scheduledEvent in scheduledEvents)
            {
                var daypartId = scheduledEvent.Daypart?.Id ?? 0;
                var daypart = _daypartRepository.GetAll()
                    .Include(d=>d.Exceptions)
                    .Where(e => e.Id == daypartId).FirstOrDefault();

                var compareTime = new Func<DateTime, int, int, int, int, bool>((dd,sh,sm,eh,em) => {
                    var isBetween = dd >= new DateTime(dd.Year, dd.Month, dd.Day, sh, sm, 0) &&
                        dd <= new DateTime(dd.Year, dd.Month, dd.Day, eh, em, 59);
                    return isBetween;
                });

                if (daypart != null) {

                    var tExceptions = daypart.Exceptions?.ToList();

                    bool inExceptions = false;

                    if (tExceptions != null) {
                        inExceptions = tExceptions.Any(e =>
                             displayTime.DayOfWeek.ToString() == e.Day &&
                             compareTime(displayTime, e.StartHour, e.StartMinute, e.EndHour, e.EndMinute)
                        );
                    }

                    scheduledEvent.StartTime = new DateTime(
                        scheduledEvent.StartTime.Year,
                        scheduledEvent.StartTime.Month,
                        scheduledEvent.StartTime.Day,
                        daypart.StartHour,
                        daypart.StartMinute,
                        0
                    );
                    scheduledEvent.EndTime = new DateTime(
                        scheduledEvent.StartTime.Year,
                        scheduledEvent.StartTime.Month,
                        scheduledEvent.StartTime.Day,
                        (inExceptions? daypart.StartHour: daypart.EndHour),
                        (inExceptions? daypart.StartMinute: daypart.EndMinute),
                        (inExceptions? 0:59)
                    );
                }
            }
            
        }

        public List<string> GetLayoutTemplates() {
            LayoutContentTemplates layoutTemplates = new LayoutContentTemplates();

            var templates = layoutTemplates.GetLayoutContentTemplates();
            List<string> templatesStr = new List<string>();
            foreach (var template in templates)
            {
                string templateStr = template.Html;
                templateStr += "|" + template.RegionPositions;
                templatesStr.Add(templateStr);
            }

            return templatesStr;
        }
    }
}
