﻿
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Sync;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

using DinePlan.DineConnect.Play.Dto;
using System;

namespace DinePlan.DineConnect.Play.Implementation
{
    public class DaypartAppService : DineConnectAppServiceBase, IDaypartAppService
    {
        private readonly ISyncAppService _syncAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Daypart> _daypartRepository;
        private readonly IRepository<DaypartException> _daypartExceptionRepository;
        public DaypartAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<Daypart> daypartRepository,
            IRepository<DaypartException> daypartExceptionRepository,
            ISyncAppService syncAppService)
        {
            _syncAppService = syncAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _daypartRepository = daypartRepository;
            _daypartExceptionRepository = daypartExceptionRepository;
        }

        public async Task<IdInput> CreateOrUpdateDaypart(CreateOrUpdateDaypartInput input)
        {
            if (input.Daypart.Id.HasValue && input.Daypart.Id > 0)
            {
                return await UpdateDaypart(input);
            }
            else
            {
                return await CreateDaypart(input);
            }
        }

        protected async Task<IdInput> CreateDaypart(CreateOrUpdateDaypartInput input)
        {
                var daypart = input.Daypart.MapTo<Daypart>();
                var createResult = await _daypartRepository.InsertAndGetIdAsync(daypart);
                return new IdInput() { Id = createResult };
        }

        protected async Task<IdInput> UpdateDaypart(CreateOrUpdateDaypartInput input)
        {
            try
            {
                var daypart = input.Daypart.MapTo<Daypart>();

                var exceptionsToUpdate = input.Daypart.Exceptions.ToList().MapTo<List<DaypartException>>();
                daypart.Exceptions.Clear();
                var updateResutl = await _daypartRepository.UpdateAsync(daypart);

                var dbDaypart = _daypartRepository.GetAll()
                    .Include(d => d.Exceptions)
                    .AsNoTracking()
                    .Where(d => d.Id == updateResutl.Id).FirstOrDefault();

                var nwExcpIds = input.Daypart.Exceptions.Select(e => e.Id).ToList();
                var dbExcpIds = dbDaypart.Exceptions.Select(e => e.Id).ToList();
                var toDelExcpIds = dbExcpIds.Where(e => !nwExcpIds.Contains(e)).ToList();

                foreach (var dpExcp in exceptionsToUpdate)
                {
                    if (dpExcp.Id == 0)
                    {
                        _daypartExceptionRepository.Insert(dpExcp);
                    }
                    else if (!toDelExcpIds.Contains(dpExcp.Id))
                    {
                        _daypartExceptionRepository.Update(dpExcp);
                    }
                }

                foreach (var id in toDelExcpIds)
                {
                    if (id > 0)
                    {
                        _daypartExceptionRepository.Delete(id);
                    }
                }

                _unitOfWorkManager.Current.SaveChanges();
                return new IdInput() { Id = updateResutl.Id };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<PagedResultOutput<DaypartListDto>> GetAll(GetDaypartListInputDto input)
        {
            var allItems = _daypartRepository.GetAll()
                .Where(g => g.IsDeleted == input.IsDeleted);

            allItems = allItems.WhereIf(!input.Filter.IsNullOrEmpty(),
                g => g.Name.Contains(input.Filter));

            var output = new PagedResultOutput<DaypartListDto>();

            var sortedItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            List<DaypartListDto> daypartList = new List<DaypartListDto>();

            if (input.IncludeCustom) {
                daypartList.Add(new DaypartListDto()
                {
                    Id = 0,
                    Name = "Custom"
                });
            }

            daypartList.AddRange(sortedItems.MapTo<List<DaypartListDto>>());
            output.Items = daypartList;
            output.TotalCount = output.Items.Count;

            return output;
        }

        public async Task<GetDaypartForEditOutput> GetDaypartForEdit(NullableIdInput input) {
            var dbDaypart = await _daypartRepository.GetAll()
                .Include(d => d.Exceptions)
                .Where(d => d.Id == input.Id).FirstOrDefaultAsync();
            var output = new GetDaypartForEditOutput();
            output.Daypart = dbDaypart.MapTo<DaypartEditDto>();
            return output;
        }

        public async Task DeleteDaypart(NullableIdInput input) {
            var dbDaypartExceptions = _daypartExceptionRepository.GetAll()
                .Where(e => e.DaypartId == input.Id).ToList();
            foreach (var excp in dbDaypartExceptions)
            {
                _daypartExceptionRepository.Delete(excp);
            }
            await _daypartRepository.DeleteAsync(input.Id ?? 0);
        }
    }


}
