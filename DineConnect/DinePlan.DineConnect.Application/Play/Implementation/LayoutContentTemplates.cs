﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Play.Implementation
{
    public class LayoutContentTemplates
    {
        private static int _TemplateCount = 17;
        private readonly string[] _Templates;
        private readonly string[] _RegionPositions;
        private readonly List<double[,]> _ClientRegionPositions;

        public LayoutContentTemplates()
        {
            _Templates = new string[_TemplateCount];
            _RegionPositions = new string[_TemplateCount];
            _ClientRegionPositions = new List<double[,]>();
            InitTemplates();
            CreateTemplateList();
        }

        private void InitTemplates() {
            _Templates[0] = @"
                <div class=""row region-height-100"">
                      <div class=""col-xs-12 region-height-100 region-padding-0"">
                        <div class=""template-region region-height-100"">
                            <<t1r1>>
                        </div>
                    </div>
                </div>
            ";
            _Templates[1] = @"
                <div class=""row region-height-50"">
                      <div class=""col-xs-12 region-height-100 region-padding-0"">
                        <div class=""template-region region-height-100"">
                            <<t2r1>>
                        </div>
                    </div>
                </div>
                <div class=""row region-height-50"">
                    <div class=""col-xs-12 region-height-100 region-padding-0"">
                        <div class=""template-region region-height-100"">
                            <<t2r2>>
                        </div>
                    </div>
                </div>
            ";
            _Templates[2] = @"
                <div class=""row region-height-100"">
                      <div class=""col-xs-6 region-height-100 region-padding-0"">
                        <div class=""template-region region-height-100"">
                            <<t3r1>>
                        </div>
                    </div>
                    <div class=""col-xs-6 region-height-100 region-padding-0"">
                        <div class=""template-region region-height-100"">
                            <<t3r2>>
                        </div>
                    </div>
                </div>
            ";
            _Templates[3] = @"
                <div class=""row region-height-33"">
                      <div class=""col-xs-12 region-height-100 region-padding-0"">
                        <div id=""_t4r1"" class=""template-region region-height-100"">
                            <<t4r1>>
                        </div>
                    </div>
                </div>
                <div class=""row region-height-33"">
                    <div class=""col-xs-12 region-height-100 region-padding-0"">
                        <div class=""template-region region-height-100"">
                            <<t4r2>>
                        </div>
                    </div>
                </div>
                <div class=""row region-height-33"">
                    <div class=""col-xs-12 region-height-100 region-padding-0"">
                        <div class=""template-region region-height-100"">
                            <<t4r3>>
                        </div>
                    </div>
                </div>
            ";
            _Templates[4] = @"
                <div class=""row region-height-50"">
                      <div class=""col-xs-6 region-height-100 region-padding-0"">
                        <div class=""template-region region-height-100"">
                            <<t5r1>>
                        </div>
                    </div>
                    <div class=""col-xs-6 region-height-100 region-padding-0"">
                        <div class=""template-region region-height-100"">
                            <<t5r2>>
                        </div>
                    </div>
                </div>
                <div class=""row region-height-50"">
                    <div class=""col-xs-12 region-height-100 region-padding-0"">
                        <div class=""template-region region-height-100"">
                            <<t5r3>>
                        </div>
                    </div>
                </div>
            ";
            _Templates[5] = @"
                <div class=""row region-height-50"">
                      <div class=""col-xs-12 region-height-100 region-padding-0"">
                        <div class=""template-region region-height-100"">
                            <<t6r1>>
                        </div>
                    </div>
                </div>
                <div class=""row region-height-50"">
                    <div class=""col-xs-6 region-height-100 region-padding-0"">
                        <div class=""template-region region-height-100"">
                            <<t6r2>>
                        </div>
                    </div>
                    <div class=""col-xs-6 region-height-100 region-padding-0"">
                        <div class=""template-region region-height-100"">
                            <<t6r3>>
                        </div>
                    </div>
                </div>
            ";
            _Templates[6] = @"
                <div class=""row region-height-100"">
                      <div class=""col-xs-4 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
			                <<t7r1>>
			            </div>
                    </div>
                    <div class=""col-xs-4 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
			                <<t7r2>>
			            </div>
                    </div>
                    <div class=""col-xs-4 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
			                <<t7r3>>
			            </div>
                    </div>
                </div>
            ";
            _Templates[7] = @"
                <div class=""row region-height-100"">
                      <div class=""col-xs-9 region-height-100 region-padding-0"">
                        <div class=""row region-height-50"">
                            <div class=""col-xs-12 region-height-100 region-padding-0"">
			                    <div class=""template-region region-height-100"">
			                        <<t8r1>>
			                    </div>
                            </div>
                        </div>
                        <div class=""row region-height-50"">
                            <div class=""col-xs-12 region-height-100 region-padding-0"">
			                    <div class=""template-region region-height-100"">
			                        <<t8r2>>
			                    </div>
                            </div>
                        </div>
                    </div>
                    <div class=""col-xs-3 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
			                <<t8r3>>
			            </div>
                    </div>
                </div>
            ";
            _Templates[8] = @"
                <div class=""row region-height-100"">
                      <div class=""col-xs-3 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
			                <<t9r1>>
			            </div>
                    </div>
                    <div class=""col-xs-9 region-height-100 region-padding-0"">
                        <div class=""row region-height-50"">
                            <div class=""col-xs-12 region-height-100 region-padding-0"">
			                    <div class=""template-region region-height-100"">
			                        <<t9r2>>
			                    </div>
                            </div>
                        </div>
                        <div class=""row region-height-50"">
                            <div class=""col-xs-12 region-height-100 region-padding-0"">
			                    <div class=""template-region region-height-100"">
			                        <<t9r3>>
			                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            ";
            _Templates[9] = @"
                <div class=""row region-height-50"">
                      <div class=""col-xs-6 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
			                <<t10r1>>
			            </div>
                    </div>
                    <div class=""col-xs-6 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
			                <<t10r2>>
			            </div>
                    </div>
                </div>
                <div class=""row region-height-50"">
                    <div class=""col-xs-6 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
			                <<t10r3>>
			            </div>
                    </div>
                    <div class=""col-xs-6 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
			                <<t10r4>>
			            </div>
                    </div>
                </div>
            ";
            _Templates[10] = @"
                <div class=""row region-height-25"">
                      <div class=""col-xs-12 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
			                <<t11r1>>
			            </div>
                    </div>
                </div>
                <div class=""row region-height-25"">
                    <div class=""col-xs-12 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
			                <<t11r2>>
			            </div>
                    </div>
                </div>
                <div class=""row region-height-25"">
                    <div class=""col-xs-12 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
			                <<t11r3>>
			            </div>
                    </div>
                </div>
                <div class=""row region-height-25"">
                    <div class=""col-xs-12 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
			                <<t11r4>>
			            </div>
                    </div>
                </div>
            ";
            _Templates[11] = @"
                <div class=""row region-height-100"">
                      <div class=""col-xs-3 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
    			            <<t12r1>>
			            </div>
                    </div>
                    <div class=""col-xs-3 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
    			            <<t12r2>>
			            </div>
                    </div>
                    <div class=""col-xs-3 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
    			            <<t12r3>>
			            </div>
                    </div>
                    <div class=""col-xs-3 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
    			            <<t12r4>>
			            </div>
                    </div>
                </div>
            ";
            _Templates[12] = @"
                <div class=""row region-height-33"">
                      <div class=""col-xs-6 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
			                <<t13r1>>
			            </div>
                    </div>
                    <div class=""col-xs-6 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
			                <<t13r2>>
			            </div>
                    </div>
                </div>   
                <div class=""row region-height-33"">
                    <div class=""col-xs-6 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
			                <<t13r3>>
			            </div>
                    </div>
                    <div class=""col-xs-6 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
			                <<t13r4>>
			            </div>
                    </div>
                </div> 
                <div class=""row region-height-33"">
                    <div class=""col-xs-12 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
			                <<t13r5>>
			            </div>
                    </div>
                </div> 
            ";
            _Templates[13] = @"
                <div class=""row region-height-33"">
                      <div class=""col-xs-6 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
			                <<t14r1>>
			            </div>
                    </div>
                    <div class=""col-xs-6 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
			                <<t14r2>>
			            </div>
                    </div>
                </div>
                <div class=""row region-height-33"">
                    <div class=""col-xs-6 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
			                <<t14r3>>
			            </div>
                    </div>
                    <div class=""col-xs-6 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
			                <<t14r4>>
			            </div>
                    </div>
                </div>
                <div class=""row region-height-33"">
                    <div class=""col-xs-6 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
			                <<t14r5>>
			            </div>
                    </div>
                    <div class=""col-xs-6 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
			                <<t14r6>>
			            </div>
                    </div>
                </div>
            ";
            _Templates[14] = @"
                <div class=""row region-height-33"">
                      <div class=""col-xs-12 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
			                <<t15r1>>
			            </div>
                    </div>
                </div>
                <div class=""row region-height-33"">
                    <div class=""col-xs-12 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
			                <<t15r2>>
			            </div>
                    </div>
                </div>
                <div class=""row region-height-33"">
                    <div class=""col-xs-4 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
			                <<t15r3>>
			            </div>
                    </div>
                    <div class=""col-xs-4 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
			                <<t15r4>>
			            </div>
                    </div>
                    <div class=""col-xs-4 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
			                <<t15r5>>
			            </div>
                    </div>
                </div>
            ";
            _Templates[15] = @"
                <div class=""row region-height-33"">
                      <div class=""col-xs-12 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
    			            <<t16r1>>
			            </div>
                    </div>
                </div>
                <div class=""row region-height-33"">
                    <div class=""col-xs-6 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
    			            <<t16r2>>
			            </div>
                    </div>
                    <div class=""col-xs-6 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
    			            <<t16r3>>
			            </div>
                    </div>
                </div>
                <div class=""row region-height-33"">
                    <div class=""col-xs-6 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
    			            <<t16r4>>
			            </div>
                    </div>
                    <div class=""col-xs-6 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
    			            <<t16r5>>
			            </div>
                    </div>
                </div>
            ";
            _Templates[16] = @"
                <div class=""row region-height-33"">
                      <div class=""col-xs-4 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
    			            <<t17r1>>
			            </div>
                    </div>
                    <div class=""col-xs-4 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
    			            <<t17r2>>
			            </div>
                    </div>
                    <div class=""col-xs-4 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
    			            <<t17r3>>
			            </div>
                    </div>
                </div>
                <div class=""row region-height-33"">
                    <div class=""col-xs-12 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
    			            <<t17r4>>
			            </div>
                    </div>
                </div>
                <div class=""row region-height-33"">
                    <div class=""col-xs-12 region-height-100 region-padding-0"">
			            <div class=""template-region region-height-100"">
    			            <<t17r5>>
			            </div>
                    </div>
                </div>
            ";

            _RegionPositions[0] = "[[1,1]]";
            _RegionPositions[1] = "[[1,0.5],[1,0.5]]";
            _RegionPositions[2] = "[[0.5,1],[0.5,1]]";
            _RegionPositions[3] = "[[1,0.3333333333333333],[1,0.3333333333333333],[1,0.3333333333333333]]";
            _RegionPositions[4] = "[[0.5,0.5],[0.5,0.5],[1,0.5]]";
            _RegionPositions[5] = "[[1,0.5],[0.5,0.5],[0.5,0.5]]";
            _RegionPositions[6] = "[[0.3333333333333333,1],[0.3333333333333333,1],[0.3333333333333333,1]]";
            _RegionPositions[7] = "[[0.75,0.5],[0.75,0.5],[0.25,1]]";
            _RegionPositions[8] = "[[0.25,1],[0.75,0.5],[0.75,0.5]]";
            _RegionPositions[9] = "[[0.5,0.5],[0.5,0.5],[0.5,0.5],[0.5,0.5]]";
            _RegionPositions[10] = "[[1,0.25],[1,0.25],[1,0.25],[1,0.25]]";
            _RegionPositions[11] = "[[0.25,1],[0.25,1],[0.25,1],[0.25,1]]";
            _RegionPositions[12] = "[[0.5,0.333333333333333],[0.5,0.333333333333333],[0.5,0.333333333333333],[0.5,0.333333333333333],[1,0.333333333333333]]";
            _RegionPositions[13] = "[[0.5,0.333333333333333],[0.5,0.333333333333333],[0.5,0.333333333333333],[0.5,0.333333333333333],[0.5,0.333333333333333],[0.5,0.333333333333333]]";
            _RegionPositions[14] = "[[1,0.333333333333333],[1,0.333333333333333],[0.333333333333333,0.333333333333333],[0.333333333333333,0.333333333333333],[0.333333333333333,0.333333333333333]]";
            _RegionPositions[15] = "[[1,0.333333333333333],[0.5,0.333333333333333],[0.5,0.333333333333333],[0.5,0.333333333333333],[0.5,0.333333333333333]]";
            _RegionPositions[16] = "[[0.333333333333333,0.333333333333333],[0.333333333333333,0.333333333333333],[0.333333333333333,0.333333333333333],[1,0.333333333333333],[1,0.333333333333333]]";

            _ClientRegionPositions.Add(new double[,] { { 0.0, 0.0, 1.0, 1.0 } });
            _ClientRegionPositions.Add(new double[,] { { 0.0, 0.0, 1.0, 0.5 }, { 0.5, 0.0, 1.0, 0.5 } });
            _ClientRegionPositions.Add(new double[,] { { 0.0, 0.0, 0.5, 1 }, { 0.0, 0.5, 0.5, 1 } });
            _ClientRegionPositions.Add(new double[,] { { 0.0, 0.0, 1, 0.33333 }, { 0.33333, 0.0, 1, 0.33333 }, { 0.66666, 0.0, 1, 0.33333 } });
            _ClientRegionPositions.Add(new double[,] { { 0.0, 0.0, 0.5, 0.5 }, { 0.0, 0.5, 0.5, 0.5 }, { 0.5, 0, 1, 0.5 } });
            _ClientRegionPositions.Add(new double[,] { { 0.0, 0.0, 1, 0.5 }, { 0.5, 0, 0.5, 0.5 }, { 0.5, 0.5, 0.5, 0.5 } });
            _ClientRegionPositions.Add(new double[,] { { 0, 0, 0.33333, 1 }, { 0, 0.33333, 0.33333, 1 }, { 0, 0.66666, 0.33333, 1 } });
            _ClientRegionPositions.Add(new double[,] { { 0.0, 0.0, 0.75, 0.5 }, { 0.5, 0, 0.75, 0.5 }, { 0.0, 0.75, 0.33333, 1 } });
            _ClientRegionPositions.Add(new double[,] { { 0, 0, 0.25, 1 }, { 0.0, 0.25, 0.75, 0.5 }, { 0.5, 0.25, 0.75, 0.5 } });
            _ClientRegionPositions.Add(new double[,] { { 0.0, 0.0, 0.5, 0.5 }, { 0.0, 0.5, 0.5, 0.5 }, { 0.5, 0.0, 0.5, 0.5 }, { 0.5, 0.5, 0.5, 0.5 } });
            _ClientRegionPositions.Add(new double[,] { { 0.0, 0.0, 1.0, 0.25 }, { 0.25, 0.0, 1.0, 0.25 }, { 0.5, 0.0, 1.0, 0.25 }, { 0.75, 0.0, 1.0, 0.25 } });
            _ClientRegionPositions.Add(new double[,] { { 0.0, 0.0, 0.25, 1.0 }, { 0.0, 0.25, 0.25, 1.0 }, { 0.0, 0.50, 0.25, 1.0 }, { 0.0, 0.75, 0.25, 1.0 } });
            _ClientRegionPositions.Add(new double[,] { { 0.0, 0.0, 0.5, 0.33333 }, { 0.0, 0.5, 0.5, 0.33333 }, { 0.33333, 0.0, 0.5, 0.33333 }, { 0.33333, 0.5, 0.5, 0.33333 }, { 0.66666, 0.0, 1, 0.33333 } });
            _ClientRegionPositions.Add(new double[,] { { 0.0, 0.0, 0.5, 0.33333 }, { 0.0, 0.5, 0.5, 0.33333 }, { 0.33333, 0.0, 0.5, 0.33333 }, { 0.33333, 0.5, 0.5, 0.33333 }, { 0.66666, 0.0, 0.5, 0.33333 }, { 0.66666, 0.5, 0.5, 0.33333 } });
            _ClientRegionPositions.Add(new double[,] { { 0.0, 0.0, 1.0, 0.33333 }, { 0.33333, 0.0, 1.0, 0.33333 }, { 0.66666, 0.0, 0.33333, 0.33333 }, { 0.66666, 0.33333, 0.33333, 0.33333 }, { 0.66666, 0.66666, 0.33333, 0.33333 } });
            _ClientRegionPositions.Add(new double[,] { { 0.0, 0.0, 1, 0.33333 }, { 0.33333, 0.0, 0.5, 0.33333 }, { 0.33333, 0.5, 0.5, 0.33333 }, { 0.66666, 0.0, 0.5, 0.33333 }, { 0.66666, 0.5, 0.5, 0.33333 } });
            _ClientRegionPositions.Add(new double[,] { { 0.0, 0.0, 0.33333, 0.33333 }, { 0.0, 0.33333, 0.33333, 0.33333 }, { 0.0, 0.66666, 0.33333, 0.33333 }, { 0.33333, 0.0, 1, 0.33333 }, { 0.66666, 0.0, 1, 0.33333 } });
        }

        private void CreateTemplateList() {
            this.Templates = new LayoutContentTemplate[_TemplateCount];
            int[] counts = new int[] { 1, 2, 2, 3, 3, 3, 3, 3, 3, 4, 4, 4, 5, 6, 5, 5, 5};
            for (int i = 0; i < _TemplateCount; i++)
            {
                this.Templates[i] = new LayoutContentTemplate() {
                    Id=i+1,
                    Name="Template " + i+1,
                    Html= _Templates[i],
                    ContentCount=counts[i],
                    RegionPositions = _RegionPositions[i],
                    ClientRegionPostions = _ClientRegionPositions[i]
                };
            }
            
        }

        public LayoutContentTemplate[] Templates { get; private set; }
        public LayoutContentTemplate GetLayoutContentTemplate(int id) {
            var template = this.Templates.Where(t => t.Id == id).FirstOrDefault();
            return template;
        }

        public LayoutContentTemplate[] GetLayoutContentTemplates() {
            return this.Templates;
        }
    }

    public class LayoutContentTemplate {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Html { get; set; }
        public int ContentCount { get; set; }
        public string RegionPositions { get; set; }
        public double[,] ClientRegionPostions { get; set; }
    }
}
