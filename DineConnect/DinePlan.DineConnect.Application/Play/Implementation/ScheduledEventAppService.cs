﻿
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Sync;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

using DinePlan.DineConnect.Play.Dto;
using System;

namespace DinePlan.DineConnect.Play.Implementation
{
    public class ScheduledEventAppService : DineConnectAppServiceBase, IScheduledEventAppService
    {
        private readonly ISyncAppService _syncAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<ScheduledEvent> _scheduledEventRepository;
        public ScheduledEventAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<ScheduledEvent> scheduledEventRepository,
            ISyncAppService syncAppService)
        {
            _syncAppService = syncAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _scheduledEventRepository = scheduledEventRepository;
        }

        public async Task<IdInput> CreateOrUpdateScheduledEvent(CreateOrUpdateScheduledEventInput input)
        {
            System.Diagnostics.Debug.Print(SimpleJson.SimpleJson.SerializeObject(input));
            input = PrepareInput(input);

            if (input.ScheduledEvent.Id.HasValue && input.ScheduledEvent.Id > 0)
            {
                return await UpdateScheduledEvent(input);
            }
            else
            {
                return await CreateScheduledEvent(input);
            }
        }

        protected CreateOrUpdateScheduledEventInput PrepareInput(CreateOrUpdateScheduledEventInput input) {

            if (input.ScheduledEvent.DisplayOption == DisplayOption.DSP)
            {
                input.ScheduledEvent.DisplayGroupId = null;
            }
            else if (input.ScheduledEvent.DisplayOption == DisplayOption.GRP) {
                input.ScheduledEvent.DisplayId = null;
            }

            if (input.ScheduledEvent.DaypartId == 0)
            {
                input.ScheduledEvent.DaypartId = null;
            }
            else {
                input.ScheduledEvent.EndTime = null;
            }

            if (input.ScheduledEvent.RepeatType == RepeatType.none.ToString()) {
                input.ScheduledEvent.RepeatInterval = null;
                input.ScheduledEvent.RepeatTimeUnit = null;
                input.ScheduledEvent.RepeatWeekdays = null;
                input.ScheduledEvent.RepeatUntil = null;
            }

            return input;
        }

        protected async Task<IdInput> CreateScheduledEvent(CreateOrUpdateScheduledEventInput input)
        {
            try
            {
                var scheduledEvent = input.ScheduledEvent.MapTo<ScheduledEvent>();
                var createResult = await _scheduledEventRepository.InsertAndGetIdAsync(scheduledEvent);
                input.ScheduledEvent.Id = createResult;
                List<ChildScheduledEventEditDto> childEvents = GenerateChildEvents(input.ScheduledEvent);

                foreach (var childEvent in childEvents)
                {
                    var scheduledChildEvent = childEvent.MapTo<ScheduledEvent>();
                    var createChildResult = await _scheduledEventRepository.InsertAndGetIdAsync(scheduledChildEvent);
                }

                return new IdInput() { Id = createResult };
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }

        protected async Task<IdInput> UpdateScheduledEvent(CreateOrUpdateScheduledEventInput input)
        {
            var scheduledEvent = input.ScheduledEvent.MapTo<ScheduledEvent>();
            var timeRelatedchangesFound = CheckTimeRelatedChanges(scheduledEvent);

            scheduledEvent.ParentId = _scheduledEventRepository.GetAll()
                .AsNoTracking()
                .Where(e => e.Id == input.ScheduledEvent.Id).Select(e => e.ParentId).FirstOrDefault();

            var updateResutl = await _scheduledEventRepository.UpdateAsync(scheduledEvent);

            if (input.ScheduledEvent.HasChildren|| updateResutl.ParentId == null)
            {
                if (input.UpdateAllChildren == false)
                {
                    ReleaseChildren(scheduledEvent.Id);
                    List<ChildScheduledEventEditDto> childEvents = GenerateChildEvents(input.ScheduledEvent);

                    foreach (var childEvent in childEvents)
                    {
                        var scheduledChildEvent = childEvent.MapTo<ScheduledEvent>();
                        await _scheduledEventRepository.InsertAndGetIdAsync(scheduledChildEvent);
                    }
                }
                else {
                    if (timeRelatedchangesFound)
                    {
                        DeleteChildren(scheduledEvent.Id);
                        List<ChildScheduledEventEditDto> childEvents = GenerateChildEvents(input.ScheduledEvent);

                        foreach (var childEvent in childEvents)
                        {
                            var scheduledChildEvent = childEvent.MapTo<ScheduledEvent>();
                            await _scheduledEventRepository.InsertAndGetIdAsync(scheduledChildEvent);
                        }
                    }
                    else {
                        UpdateChildrenFromParent(scheduledEvent);
                    }
                }
            }
            else {
                if (updateResutl.ParentId != null) {
                    if (input.UpdateAllChildren == false)
                    {
                        scheduledEvent.ParentId = null;
                        scheduledEvent.RepeatType = "none";
                        scheduledEvent.RepeatInterval = null;
                        scheduledEvent.RepeatTimeUnit = null;
                        scheduledEvent.RepeatWeekdays = null;
                        scheduledEvent.RepeatUntil = null;
                        updateResutl = await _scheduledEventRepository.UpdateAsync(scheduledEvent);
                    }
                    else {
                        UpdateParentFromChild(scheduledEvent);
                        UpdateChildrenFromChild(scheduledEvent);
                    }
                }
            }
            return new IdInput() { Id = updateResutl.Id };
        }

        private void DeleteChildren(int? eventId) {
            var getAllResult = _scheduledEventRepository.GetAll()
                .AsNoTracking()
                .Where(e => e.ParentId == eventId).ToList();
            // var children = getAllResult.MapTo<List<ChildScheduledEventEditDto>>();
            foreach (var child in getAllResult)
            {
                _scheduledEventRepository.Delete(child.Id);
            }
        }

        private bool CheckTimeRelatedChanges(ScheduledEvent newlyUpdatedScheduledEvent) {
            var nue = newlyUpdatedScheduledEvent;
            var getResult = _scheduledEventRepository.GetAll()
                .AsNoTracking()
                .Where(e=>e.Id==nue.Id).FirstOrDefault();

            bool notChanged = getResult.StartTime == nue.StartTime &&
                    getResult.EndTime == nue.EndTime &&
                    getResult.RepeatType == nue.RepeatType &&
                    getResult.RepeatInterval == nue.RepeatInterval &&
                    getResult.RepeatTimeUnit == nue.RepeatTimeUnit &&
                    getResult.RepeatWeekdays == nue.RepeatWeekdays &&
                    getResult.RepeatUntil == nue.RepeatUntil;
            return !notChanged;
        }

        private void ReleaseChildren(int parentId) {
            var getAllResult = _scheduledEventRepository.GetAll()
                .AsNoTracking()
                .Where(e => e.ParentId == parentId).ToList();
            var children = getAllResult.MapTo<List<ChildScheduledEventEditDto>>();

            foreach (var child in children)
            {
                child.ParentId = child.ParentId * -1;
                var itemToUpdate = child.MapTo<ScheduledEvent>();
                _scheduledEventRepository.Update(itemToUpdate);
            }
        }

        private void UpdateChildrenFromChild(ScheduledEvent child) {
            UpdateChildren(child, child.ParentId??0);
        }

        private void UpdateChildrenFromParent(ScheduledEvent parent)
        {
            UpdateChildren(parent, parent.Id);
        }

        private void UpdateParentFromChild(ScheduledEvent child) {

            int parentId = child.ParentId ?? 0;

            var getResult = _scheduledEventRepository.GetAll()
                .AsNoTracking()
                .Where(e=>e.Id == parentId).FirstOrDefault();

            if (getResult != null) {
                var parent = child.MapTo<ChildScheduledEventEditDto>();

                parent.Id = getResult.Id;
                parent.ParentId = null;
                parent.StartTime = getResult.StartTime;
                parent.EndTime = getResult.EndTime;

                var itemToUpdate = parent.MapTo<ScheduledEvent>();
                try
                {
                    _scheduledEventRepository.Update(itemToUpdate);
                    // _unitOfWorkManager.Current.SaveChanges();
    
            }
                catch (Exception ex)
                {

                    throw ex;
                }
            }
        }
        private void UpdateChildren(ScheduledEvent scheduledEvent, int id) {
            int parentId = id;

            var getAllResult = _scheduledEventRepository.GetAll()
                .AsNoTracking()
                .Where(e => e.ParentId == parentId && e.Id != scheduledEvent.Id).ToList();

            var children = getAllResult.MapTo<List<ChildScheduledEventEditDto>>();

            try
            {
                foreach (var child in children)
                {
                    var updatedChild = scheduledEvent.MapTo<ChildScheduledEventEditDto>();
                    updatedChild.Id = child.Id;
                    updatedChild.ParentId = child.ParentId;
                    updatedChild.StartTime = child.StartTime;
                    updatedChild.EndTime = child.EndTime;
                    var itemToUpdate = updatedChild.MapTo<ScheduledEvent>();
                    _scheduledEventRepository.Update(itemToUpdate);
                    _unitOfWorkManager.Current.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<PagedResultOutput<ScheduledEventListDto>> GetAll(GetScheduledEventInput input)
        {
            var allItems = _scheduledEventRepository.GetAll()
                .Where(g => g.IsDeleted == input.IsDeleted);

            if (input.FilteringOption == DisplayOption.DSP)
            {
                allItems = allItems.WhereIf(!input.Filter.IsNullOrEmpty(),
                    e => e.Display.Name.Contains(input.Filter));
            }
            else if (input.FilteringOption == DisplayOption.GRP) {
                allItems = allItems.WhereIf(!input.Filter.IsNullOrEmpty(),
                    e => e.DisplayGroup.Name.Contains(input.Filter));
            }

            allItems = allItems.WhereIf(input.StartDateFrom!=null&& input.StartDateTo !=null,
                e => e.StartTime >= input.StartDateFrom && e.StartTime <= input.StartDateTo);

            var output = new PagedResultOutput<ScheduledEventListDto>();

            var sortedItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            output.Items = sortedItems.MapTo<List<ScheduledEventListDto>>();
            output.TotalCount = output.Items.Count;

            return output;
        }

        public async Task<GetScheduledEventOutput> GetScheduledEventForUpdate(NullableIdInput input) {
            var dbDaypart = await _scheduledEventRepository.GetAsync(input.Id??0);
            var output = new GetScheduledEventOutput();
            if (dbDaypart.ParentId != null)
            {
                output.ScheduledEvent = dbDaypart.MapTo<ChildScheduledEventEditDto>();
            }
            else {
                output.ScheduledEvent = dbDaypart.MapTo<ScheduledEventEditDto>();
            }
            

            var hasChildren = _scheduledEventRepository.GetAll()
                .Where(e => e.ParentId == input.Id).ToList().Count>0;

            output.ScheduledEvent.HasChildren = hasChildren;

            return output;
        }

        public async Task DeleteScheduledEvent(DeleteScheduledEventInput input) {
            var getResult = _scheduledEventRepository.GetAll()
                .AsNoTracking()
                .Where(e => e.Id == input.Id).FirstOrDefault();

            int? eventId = null;

            if (getResult.ParentId != null)
            {
                eventId = getResult.ParentId;
            }
            else {
                eventId = getResult.Id;
            }

            if (input.DeleteAllChildren) {
                DeleteChildren(eventId);
            }

            await _scheduledEventRepository.DeleteAsync(eventId??0);
        }

        private List<ChildScheduledEventEditDto> GenerateChildEvents(ScheduledEventEditDto scheduledEvent) {
            List<DateTime?[]> eventDates;

            if (scheduledEvent.RepeatType != RepeatType.weekly.ToString())
            {
                eventDates = GetNextEventDates(scheduledEvent);
            }
            else {
                eventDates = GetNextWeeklyEventDates(scheduledEvent);
            }
            
            List<ChildScheduledEventEditDto> childEvents = 
                new List<ChildScheduledEventEditDto>();

            foreach (var eventDate in eventDates)
            {
                ChildScheduledEventEditDto childEvent = 
                    scheduledEvent.MapTo<ChildScheduledEventEditDto>();
                childEvent.StartTime = eventDate[0]??DateTime.Now;
                childEvent.EndTime = eventDate[1];
                childEvent.ParentId = scheduledEvent.Id;
                childEvents.Add(childEvent);
            }

            return childEvents;
        }

        private List<DateTime?[]>GetNextEventDates(ScheduledEventEditDto scheduledEvent) {
            List<DateTime?[]> eventDates = new List<DateTime?[]>();
            DateTime? tmpStartDate = scheduledEvent.StartTime;
            DateTime? tmpEndDate = scheduledEvent.EndTime;
            string scheduleEventRepeatType = scheduledEvent.RepeatType;

            RepeatType repeatType = (RepeatType)Enum.Parse(typeof(RepeatType), scheduleEventRepeatType);
            int intervalCount = scheduledEvent.RepeatInterval ?? 1;

            while (tmpStartDate <= scheduledEvent.RepeatUntil){
                DateTime?[] startEndDates = new DateTime?[2];

                if (repeatType == RepeatType.perminute)
                {
                    startEndDates[0] = tmpStartDate?.AddMinutes(intervalCount);
                    startEndDates[1] = tmpEndDate?.AddMinutes(intervalCount);
                }
                else if (repeatType == RepeatType.hourly)
                {
                    startEndDates[0] = tmpStartDate?.AddHours(intervalCount);
                    startEndDates[1] = tmpEndDate?.AddHours(intervalCount);
                }
                else if (repeatType == RepeatType.daily)
                {
                    startEndDates[0] = tmpStartDate?.AddDays(intervalCount);
                    startEndDates[1] = tmpEndDate?.AddDays(intervalCount);
                }
                else if (repeatType == RepeatType.monthly)
                {
                    startEndDates[0] = tmpStartDate?.AddMonths(intervalCount);
                    startEndDates[1] = tmpEndDate?.AddMonths(intervalCount);
                }
                else if (repeatType == RepeatType.yearly)
                {
                    startEndDates[0] = tmpStartDate?.AddYears(intervalCount);
                    startEndDates[1] = tmpEndDate?.AddYears(intervalCount);
                }

                tmpStartDate = startEndDates[0];
                tmpEndDate = startEndDates[1];

                if (tmpStartDate <= scheduledEvent.RepeatUntil) {
                    eventDates.Add(startEndDates);
                }
            }

            return eventDates;
        }

        private List<DateTime?[]> GetNextWeeklyEventDates(ScheduledEventEditDto scheduledEvent) {
            DateTime monday = scheduledEvent.StartTime.AddDays((scheduledEvent.StartTime.DayOfWeek - DayOfWeek.Sunday)*-1);
            DateTime saturday = monday.AddDays(6);

            DateTime tmpStartDate = monday;
            DateTime? tmpEndDate = new DateTime(monday.Year,monday.Month,monday.Day, 
                scheduledEvent.EndTime?.Hour??23, scheduledEvent.EndTime?.Minute??59,59);
            List<DateTime[]> eventDates = new List<DateTime[]>();

            var weekdays = "Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday".Split(',');

            var selWeekDays = scheduledEvent.RepeatWeekdays.Split(',').Select( d=>
                    (DayOfWeek)Enum.Parse(typeof(DayOfWeek),
                        weekdays.Where(w=> w.ToUpper().IndexOf(d.ToUpper())==0).FirstOrDefault())
                ).ToArray();

            //for (int i = 0; i < 7; i++)
            //{
            //    DateTime[] tDates = new DateTime[2];
            //    tDates[0]= monday.AddDays(i);
            //    tDates[1] = new DateTime(tDates[0].Year, tDates[0].Month, tDates[0].Day,
            //        tmpEndDate?.Hour??23, tmpEndDate?.Minute??59, 59) ;

            //    eventDates.Add(tDates);
            //}

            while (tmpStartDate <= scheduledEvent.RepeatUntil) {
                for (int i = 0; i < 7; i++)
                {
                    DateTime[] tDates = new DateTime[2];

                    tDates[0] = tmpStartDate.AddDays(i);
                    tDates[1] = new DateTime(tDates[0].Year, tDates[0].Month, tDates[0].Day,
                        tmpEndDate?.Hour ?? 23, tmpEndDate?.Minute ?? 59, 59);
                    eventDates.Add(tDates);
                }
                tmpStartDate = tmpStartDate.AddDays(7 * scheduledEvent.RepeatInterval ?? 0);
            }

            var filteredDates = eventDates
                .Where(d => d[0] >= scheduledEvent.StartTime && d[0] <= scheduledEvent.RepeatUntil &&
                selWeekDays.Contains(d[0].DayOfWeek)
            ).ToList();

            List<DateTime?[]> result = filteredDates
                .Select(d => new DateTime?[] { (DateTime?)d[0], (DateTime?)d[1] }).ToList();

            return result;
        }

        private bool EventValidForRepeatType(ScheduledEventEditDto scheduledEvent) {
            DateTime nextStartTime = scheduledEvent.StartTime;

            RepeatType repeatType = (RepeatType)Enum.Parse(typeof(RepeatType), scheduledEvent.RepeatType);
            int intervalCount = scheduledEvent.RepeatInterval ?? 1;

            if (repeatType == RepeatType.perminute)
            {
                nextStartTime = nextStartTime.AddMinutes(intervalCount);
            }
            else if (repeatType == RepeatType.hourly)
            {
                nextStartTime = nextStartTime.AddHours(intervalCount);
            }
            else if (repeatType == RepeatType.daily) {
                nextStartTime = nextStartTime.AddDays(intervalCount);
            }
            else if (repeatType == RepeatType.monthly)
            {
                nextStartTime = nextStartTime.AddMonths(intervalCount);
            }
            else if (repeatType == RepeatType.yearly)
            {
                nextStartTime = nextStartTime.AddYears(intervalCount);
            }

            return nextStartTime > scheduledEvent.EndTime;
        }

        private void UpdateChildEvents(ScheduledEventEditDto scheduledEvent) {
             var getResult = _scheduledEventRepository.GetAll()
                .Where(e=>e.ParentId == scheduledEvent.Id && e.IsDeleted == false).ToList();

            // List<ChildScheduledEventEditDto> childEvents = getResult.MapTo<List<ChildScheduledEventEditDto>>();

            List<DateTime?[]> eventDates = GetNextEventDates(scheduledEvent);
            foreach (var item in eventDates)
            {

            }

        }
    }
}
