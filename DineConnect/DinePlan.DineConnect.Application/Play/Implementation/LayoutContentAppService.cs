﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Play.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Play.Implementation
{

    public class LayoutContentAppService: DineConnectAppServiceBase, 
        ILayoutContentAppService
    {
        private readonly ISyncAppService _syncAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<LayoutContent> _layoutContentRepository;

        public LayoutContentAppService(IUnitOfWorkManager unitOfWorkManager,
            IRepository<LayoutContent> layoutContentRepository, ISyncAppService syncAppService)
        {
            _syncAppService = syncAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _layoutContentRepository = layoutContentRepository;
        }

        public async Task<List<UpdateLayoutContentOutput>> CreateOrUpdateLayoutContent(
            CreateOrUpdateLayoutContentInput input) {
            var output = new List<UpdateLayoutContentOutput>() ;
            foreach (var layoutContent in input.LayoutContents)
            {
                UpdateLayoutContentOutput updateResult;
                if (layoutContent.Id.HasValue &&
                    layoutContent.Id > 0)
                {
                    updateResult = await UpdateLayoutContent(layoutContent);
                }
                else
                {
                    updateResult = await CreateLayoutContent(layoutContent);
                }
                output.Add(updateResult);
            }

            // ?
            // await _syncAppService.UpdateSync(SyncConsts.)

            return output;
        }

        protected virtual async Task<UpdateLayoutContentOutput> CreateLayoutContent(
            LayoutContentEditDto input) {
            var layoutContent = input.MapTo<LayoutContent>();
            var output = layoutContent.MapTo<UpdateLayoutContentOutput>();
            try
            {
                var id = await _layoutContentRepository.InsertAndGetIdAsync(layoutContent);
                output.Id = id;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            

            return output;
        }

        protected virtual async Task<UpdateLayoutContentOutput> UpdateLayoutContent(
            LayoutContentEditDto input)
        {
            UpdateLayoutContentOutput output = null;
            var layoutContent = input.MapTo<LayoutContent>();
            var updateResult = await _layoutContentRepository.UpdateAsync(layoutContent);
            output = updateResult.MapTo<UpdateLayoutContentOutput>();
            return output;
        }

        public async Task <bool>DeleteLayoutContents(IdInput[] inputs) {
            var result = false;
            foreach (var input in inputs)
            {
                await _layoutContentRepository.DeleteAsync(input.Id);
            }
            result = true;

            return result;
        }

        public async Task<bool> InitializeContents(InitializeLayoutContentsInput input)
        {
            var contents = new LayoutContentEditDto[input.RegionCount];

            var createInput = new CreateOrUpdateLayoutContentInput();
            createInput.LayoutContents = new LayoutContentEditDto[input.RegionCount];

            for (var i = 0; i < contents.Length; i++)
            {
                createInput.LayoutContents[i] = new LayoutContentEditDto()
                {
                    LayoutId = input.LayoutId,
                    HasFile = false,
                    RegionNo = i + 1,
                    Stretched = false,
                    DisplayLength = 0,
                    SystemFileName = "-"
                };
            }

            await CreateOrUpdateLayoutContent(createInput);

            return true;
        }

        public async Task<List<LayoutContentListDto>> GetLayoutContents(IdInput input) {
            var allItems = _layoutContentRepository.GetAll()
                .Where(l => l.IsDeleted == false);

            allItems = allItems.Where(c => c.LayoutId == input.Id);

            var output = new List<LayoutContentListDto>();

            var sortedItems = await allItems
            .OrderBy("RegionNo")
            .ToListAsync();
                output = sortedItems.MapTo<List<LayoutContentListDto>>();

            return output;
        }

        
    }
}
