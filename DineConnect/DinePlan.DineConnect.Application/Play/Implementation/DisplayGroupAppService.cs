﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Sync;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

using DinePlan.DineConnect.Play.Dto;

namespace DinePlan.DineConnect.Play.Implementation
{
    public class DisplayGroupAppService : DineConnectAppServiceBase, IDisplayGroupAppService
    {
        private readonly ISyncAppService _syncAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<DisplayGroup> _displayGroupRepository;
        private readonly IRepository<Display> _displayRepository;
        public DisplayGroupAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<DisplayGroup> displayGroupRepository,
            IRepository<Display> displayRepository,
            ISyncAppService syncAppService)
        {
            _syncAppService = syncAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _displayGroupRepository = displayGroupRepository;
            _displayRepository = displayRepository;
        }

        public async Task<IdInput> CreateOrUpdateDisplayGroup(CreateOrUpdateDisplayGroupInput input) {
            if (input.DisplayGroup.Id.HasValue && input.DisplayGroup.Id > 0)
            {
                return await UpdateDisplayGroup(input);
            }
            else
            {
                return await CreateDisplayGroup(input);
            }
        }

        protected async Task<IdInput> CreateDisplayGroup(CreateOrUpdateDisplayGroupInput input) {
            var displayGroup = input.DisplayGroup.MapTo<DisplayGroup>();
            var ids = displayGroup.Displays.Select(g => g.Id).ToArray();
            displayGroup.Displays.Clear();
            var createResult = await _displayGroupRepository.InsertAndGetIdAsync(displayGroup);

            var displays = _displayRepository.GetAll()
                .Where(d => ids.Contains(d.Id)).ToArray();
            var dbDisplayGroup = _displayGroupRepository.GetAll()
                .Include(g => g.Displays)
                .Where(g => g.Id == createResult).FirstOrDefault();

            dbDisplayGroup.Displays.Clear();
            foreach (var display in displays)
            {
                dbDisplayGroup.Displays.Add(display);
            }
            _unitOfWorkManager.Current.SaveChanges();
            return new IdInput() { Id = createResult };
        }

        protected async Task<IdInput> UpdateDisplayGroup(CreateOrUpdateDisplayGroupInput input) {
            var displayGroup = input.DisplayGroup.MapTo<DisplayGroup>();
            
            var ids = displayGroup.Displays.Select(d => d.Id).ToArray();

            displayGroup.Displays.Clear();
            var updateResutl = await _displayGroupRepository.UpdateAsync(displayGroup);

            var displays = _displayRepository.GetAll()
                .Where(d => ids.Contains(d.Id)).ToArray();

            var dbDisplayGroup = _displayGroupRepository.GetAll()
                .Include(g => g.Displays)
                .Where(g => g.Id == displayGroup.Id).FirstOrDefault();
            dbDisplayGroup.Displays.Clear();
            foreach (var display in displays)
            {
                dbDisplayGroup.Displays.Add(display);
            }

            _unitOfWorkManager.Current.SaveChanges();

            return new IdInput() { Id = updateResutl.Id };
        }

        public async Task DeleteDisplayGroup(NullableIdInput input) {
            var dbDisplayGroup = _displayGroupRepository.GetAll()
                .Include(g => g.Displays)
                .Where(g => g.Id == input.Id).FirstOrDefault();

            dbDisplayGroup.Displays.Clear();
            await _displayGroupRepository.DeleteAsync(input.Id ?? 0);
            _unitOfWorkManager.Current.SaveChanges();
        }

        public async Task<PagedResultOutput<DisplayGroupListDto>> GetAll(GetDisplayGroupListInputDto input)
        {
            if (input.Exclusions == null) input.Exclusions = new int[] {};
            var allItems = _displayGroupRepository.GetAll()
                .Where(g => g.IsDeleted == input.IsDeleted && !input.Exclusions.Any(n=>g.Id==n));

            allItems = allItems.WhereIf(!input.Filter.IsNullOrEmpty(), 
                g => g.Name.Contains(input.Filter) || 
                g.Code.Contains(input.Filter));

            var output = new PagedResultOutput<DisplayGroupListDto>();

            var sortedItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            output.Items = sortedItems.MapTo<List<DisplayGroupListDto>>();
            output.TotalCount = output.Items.Count;

            return output;
        }

        public async Task<PagedResultOutput<DisplayListDto>> GetDisplaysOfGroup(GetDisplaysOfGroupListInputDto input) {
            var items = await _displayGroupRepository.GetAll()
                .Include(g=>g.Displays)
                .Where(g => g.Id == input.DispalyGroupId).FirstOrDefaultAsync();

            var displays = items.Displays.MapTo<List<DisplayListDto>>();

            if (!input.Filter.IsNullOrEmpty()) {
                displays = displays.Where(d => d.Name.Contains(input.Filter)).ToList();
            }

            var output = new PagedResultOutput<DisplayListDto>();
            output.Items = displays.ToList();
            output.TotalCount = displays.Count;
            return output;
        }
        public async Task<GetDisplayGroupForEditOutput> GetDisplayGroupForEdit(NullableIdInput input) {
            var displayGroup = await _displayGroupRepository.GetAsync(input.Id ?? 0);
            var output = new GetDisplayGroupForEditOutput();
            output.DisplayGroup = displayGroup.MapTo<DisplayGroupListDto>();
            return output;
        }
    }
}
