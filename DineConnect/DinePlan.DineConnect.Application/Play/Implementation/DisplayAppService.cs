﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.EntityFramework.Extensions;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Play.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Play.Implementation
{
    public class DisplayAppService : DineConnectAppServiceBase, IDisplayAppService
    {
        private const string SALT = "DINEPLAYREGISKEY_3d83bdea50e242489a18f5585847abbe";
        private readonly ISyncAppService _syncAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Display> _displayRepository;
        private readonly IRepository<DisplayGroup> _displayGroupRepository;
        private readonly IRepository<ScheduledEvent> _scheduledEventRepository;
        public DisplayAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<Display> displayRepository,
            IRepository<DisplayGroup> displayGroupRepository,
            IRepository<ScheduledEvent> scheduledEventRepository,
            ISyncAppService syncAppService)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _syncAppService = syncAppService;
            _displayRepository = displayRepository;
            _displayGroupRepository = displayGroupRepository;
            _scheduledEventRepository = scheduledEventRepository;
        }

        public async Task<IdInput> CreateOrUpdateDisplay(CreateOrUpdateDisplayInput input) {
            if (input.Display.Id.HasValue && input.Display.Id > 0)
            {
                return await UpdateDisplay(input);
            }
            else {
                return await CreateDisplay(input);
            }
        }

        protected async Task<IdInput> CreateDisplay(CreateOrUpdateDisplayInput input) {
            var ids = input.Display.DisplayGroups.Select(d => d.Id).ToArray();
            input.Display.DisplayGroups.Clear();
            // input.Display.Resolution = null;
            // input.Display.Location = null;
            input.Display.RegistrationKey = GenRegistrationKey(input.Display);
            var display = input.Display.MapTo<Display>();
            try
            {
                var createResult = await _displayRepository.InsertAndGetIdAsync(display);
                
                var dbDisplay = _displayRepository.GetAll()
                    .Include(d => d.DisplayGroups)
                    .Where(d => d.Id == createResult).FirstOrDefault();
                
                var displayGroups = _displayGroupRepository.GetAll()
                    .Where(g => ids.Contains(g.Id)).ToArray();

                foreach (var displayGroup in displayGroups)
                {
                    dbDisplay.DisplayGroups.Add(displayGroup);
                }
                _unitOfWorkManager.Current.SaveChanges();
                return new IdInput() { Id = createResult };
            }
            catch (Exception ex)
            {
                throw;
            }
            
        }

        protected async Task<IdInput> UpdateDisplay(CreateOrUpdateDisplayInput input) {

            try
            {
                input.Display.RegistrationKey = GenRegistrationKey(input.Display);
                // input.Display.Resolution = null;
                // input.Display.Location = null;

                var display = input.Display.MapTo<Display>();
                var groupIds = input.Display.DisplayGroups.Select(g => g.Id).ToArray();

                display.DisplayGroups.Clear();
                var updateResult = await _displayRepository.UpdateAsync(display);

                var groups = _displayGroupRepository.GetAll();
                var newGroups = groups.Where(g => groupIds.Contains(g.Id)).ToArray();
                var dbDisplay = _displayRepository.GetAll()
                    .Include(d => d.DisplayGroups)
                    .Where(d => d.Id == input.Display.Id).FirstOrDefault();

                dbDisplay.DisplayGroups.Clear();

                foreach (var newGroup in newGroups)
                {
                    dbDisplay.DisplayGroups.Add(newGroup);
                }
                _unitOfWorkManager.Current.SaveChanges();

                return new IdInput() { Id = updateResult.Id };
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<PagedResultOutput<DisplayListDto>> GetAll(GetDisplayListInputDto input)
        {
            if (input.Exclusions == null) input.Exclusions = new int[] { };
            var allItems = _displayRepository.GetAll()
                .Where(g => g.IsDeleted == input.IsDeleted && !input.Exclusions.Any(n => g.Id == n));

            allItems = allItems.WhereIf(!input.Filter.IsNullOrEmpty(),
                g => g.Name.Contains(input.Filter));

            allItems = allItems.WhereIf(input.LocationId > 0, d => d.LocationId == input.LocationId);

            var output = new PagedResultOutput<DisplayListDto>();

            var sortedItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            output.Items = sortedItems.MapTo<List<DisplayListDto>>();
            output.TotalCount = output.Items.Count;

            return output;
        }

        public async Task<GetDisplayForEditOutput> GetDisplayForEdit(NullableIdInput input)
        {
            GetDisplayForEditOutput output = new GetDisplayForEditOutput();

            if (input.Id.HasValue)
            {
                var display =await _displayRepository.GetAll()
                    .Include(d=>d.Resolution)
                    .Include(d=>d.Location)
                    .Include(d=>d.DisplayGroups)
                    .Where(d=>d.Id == input.Id).FirstOrDefaultAsync();

                output.Display = display.MapTo<DisplayEditDto>();
            }
            else
            {
                output.Display = new DisplayEditDto();
            }

            return output;
        }

        public async Task<DisplayDeleteOutput> DeleteDisplay(NullableIdInput input) {
            var deletion = new DisplayDeleteOutput();

            var ScheduledEventNames = DisplayUsedBy(input.Id);

            if (ScheduledEventNames != null && ScheduledEventNames.Length > 0)
            {
                deletion.Success = false;
                deletion.UsedByNames = ScheduledEventNames;
            }
            else {
                var display = _displayRepository.GetAll()
                    .Include(d => d.DisplayGroups)
                    .Where(d => d.Id == input.Id).FirstOrDefault();

                display.DisplayGroups.Clear();
                _unitOfWorkManager.Current.SaveChanges();
                await _displayRepository.DeleteAsync(input.Id ?? 0);

                deletion.Success = true;
                deletion.UsedByNames = null;
            }

            return deletion;
        }

        private string[] DisplayUsedBy(int? displayId) {
            var displayGroupIds = _displayGroupRepository.GetAll()
                .AsNoTracking()
                .Include(g => g.Displays)
                .Where(g => g.Displays.Select(d => d.Id).Contains(displayId??0))
                .Select(g => g.Id).ToList();

            var getDpResult = _scheduledEventRepository.GetAll()
                .AsNoTracking()
                .Where(e => e.ParentId ==null &&
                    (e.DisplayId == displayId || displayGroupIds.Contains(e.DisplayGroupId??0)))
                .Select(e=>e.Name).ToList();
            return getDpResult.ToArray();
        }

        private string GenRegistrationKey(DisplayEditDto display) {
            string text = (display.Id??0).ToString("00000");
            text += display.Name;
            text += SALT;
            string hashedText = ComputeSha256Hash(text);
            return hashedText;
        }

        static string ComputeSha256Hash(string rawData)
        {
            using (SHA256 sha256Hash = SHA256.Create())
            {
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        public async Task<object> RegisterDisplay(ConnectDisplayInput input) {

            var display = _displayRepository.GetAll()
                .Where(d => d.RegistrationKey == input.RegistrationKey)?.FirstOrDefault();
            if (display != null) {
                if (string.IsNullOrEmpty(display.MacAddress))
                {
                    display.MacAddress = input.MacAddress;
                    await _displayRepository.UpdateAsync(display);
                    return new { Succeeded = true, Message = "Display registered.", DisplayId = display.Id };
                }
                else if (display.MacAddress != input.MacAddress)
                {
                    return new { Succeeded = false, Message = "This key is already in use.", DisplayId = 0 };
                }
                else if (display.MacAddress == input.MacAddress) {
                    return new { Succeeded = true, Message = "Display was already registered.", DisplayId = display.Id };
                }
            }
            return new { Succeeded= false, Message = "Display is unregistered.", DisplayId = 0};
        }

        public async Task<object> CheckDisplayRegistration(CheckDisplayRegistrationInput input) {
            var display = await _displayRepository.GetAsync(input.DisplayId);

            if (display != null) {
                if (display.MacAddress == input.MacAddress && display.RegistrationKey == input.RegistrationKey)
                {
                    return new { IsValidRegistration = true };
                }
            }

            return new { IsValidRegistration = false };
        }

        public async Task<object> Unregister(UnregisterDisplayInput input) {
            var display = _displayRepository.GetAll()
                .Where(d=> d.Id == input.DisplayId && 
                d.MacAddress == input.MacAddress && 
                d.RegistrationKey == input.RegistrationKey).FirstOrDefault();

            if (display != null) {
                display.MacAddress = null;
                await _displayRepository.UpdateAsync(display);
                return new { Unregistered = true, Message = "Display was unregistered." };
            }

            return new { Unregistered = false, Message = "The display was not found." };
        }

    }
}
