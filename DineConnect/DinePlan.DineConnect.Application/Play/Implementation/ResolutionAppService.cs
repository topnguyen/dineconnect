﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Play.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Play.Implementation
{
    public class ResolutionAppService : DineConnectAppServiceBase, IResolutionAppService
    {
        private readonly ISyncAppService _syncAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Resolution> _resolutionRepository;
        private readonly IRepository<Display> _displayRepository;
        private readonly IRepository<Layout> _layoutRepository;
        public ResolutionAppService(IUnitOfWorkManager unitOfWorkManager,
            IRepository<Resolution> resolutionRepository, 
            IRepository<Display> displayRepository,
            IRepository<Layout> layoutRepository,
            ISyncAppService syncAppService)
        {
            _syncAppService = syncAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _resolutionRepository = resolutionRepository;
            _displayRepository = displayRepository;
            _layoutRepository = layoutRepository;
        }

        public async Task<PagedResultOutput<ResolutionListDto>> GetAll(
            GetResolutionListInputDto input) {
            var output = new PagedResultOutput<ResolutionListDto> ();

            var allItems = _resolutionRepository.GetAll()
                .Where(r => r.IsDeleted == input.IsDeleted);

            allItems = allItems.WhereIf(!input.Filter.IsNullOrEmpty(),
                r => r.Name.Contains(input.Filter));

            var sortedItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            output.Items = sortedItems.MapTo<List<ResolutionListDto>>();
            output.TotalCount = output.Items.Count;
            return output;
        }

        public async Task<int> CreateOrUpdateResolution(CreateOrUpdateResolutionInput input)
        {
            var output = -1;
            if (!input.Resolution.Id.HasValue || input.Resolution.Id <= 0)
            {
                output = await CreateResolution(input);
            }
            else {
                var updateResult = await UpdateResolution(input);
                output = updateResult.Id??-1;
            }
            return output;
        }

        protected virtual async Task<int> CreateResolution(CreateOrUpdateResolutionInput input) {
            var resolution = input.Resolution.MapTo<Resolution>();
            var output = -1;
            output = await _resolutionRepository.InsertAndGetIdAsync(resolution);
            return output;
        }

        protected virtual async Task<ResolutionEditDto> UpdateResolution(
            CreateOrUpdateResolutionInput input)
        {
            var resolution = input.Resolution.MapTo<Resolution>();
            var updateResult = await _resolutionRepository.UpdateAsync(resolution);
            var output = updateResult.MapTo<ResolutionEditDto>();
            return output;
        }

        public async Task<GetResolutionForEditOutput> GetResolutionForEdit(NullableIdInput input) {
            var output = new GetResolutionForEditOutput();

            if (input.Id.HasValue)
            {
                var resolution = await _resolutionRepository.GetAsync(input.Id.Value);
                output.Resolution = resolution.MapTo<ResolutionEditDto>();
            }

            return output;
        }

        public async Task<ResolutionDeleteOutput> DeleteResolution(IdInput input) {
            var deletion = new ResolutionDeleteOutput();
            var usedByNames = ResolutionUsedBy(input.Id);

            if (usedByNames != null && usedByNames.Length > 0)
            {
                deletion.Success = false;
                deletion.UsedByNames = usedByNames;
            }
            else {
                await _resolutionRepository.DeleteAsync(input.Id);

                deletion.Success = true;
                deletion.UsedByNames = null;
            }

            return deletion;
        }

        private string[] ResolutionUsedBy(int? resolutionId) {
            try
            {
                var getDpResult = _displayRepository.GetAll()
                .AsNoTracking()
                .Where(d => d.ResolutionId == resolutionId)
                .Select(d => "Display: " + d.Name).ToList();

                var getLytResult = _layoutRepository.GetAll()
                    .AsNoTracking()
                    .Where(l => l.ResolutionId == resolutionId)
                    .Select(l => "Layout: " + l.Name).ToList();

                var resultList = new List<string>();
                resultList.AddRange(getDpResult);
                resultList.AddRange(getLytResult);

                return resultList.ToArray();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
