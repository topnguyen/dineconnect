﻿
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Play.Dto
{
    [AutoMapFrom(typeof(ScheduledEvent))]
    public class ScheduledEventListDto : FullAuditedEntityDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public DisplayListDto Display { get; set; }
        public DisplayGroupListDto DisplayGroup { get; set; }
        public DaypartListDto Daypart { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public LayoutListDto Layout { get; set; }
        public int Priority { get; set; }
        public string RepeatType { get; set; }
        public int? RepeatInterval { get; set; }
        public string RepeatTimeUnit { get; set; }
        public string RepeatWeekdays { get; set; }
        public DateTime? RepeatUntil { get; set; }
    }

    public class CreateOrUpdateScheduledEventInput : IInputDto
    {
        public ScheduledEventEditDto ScheduledEvent { get; set; }
        public bool UpdateAllChildren { get; set; }
    }

    public class DeleteScheduledEventInput : IInputDto
    {
        public int Id { get; set; }
        public bool DeleteAllChildren { get; set; }
    }

    public class GetScheduledEventInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public DisplayOption FilteringOption { get; set; }
        public string Operation { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? StartDateFrom { get; set; }
        public DateTime? StartDateTo { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    [AutoMapTo(typeof(ScheduledEvent))]
    public class ScheduledEventEditDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public DisplayOption DisplayOption { get; set; }
        public int? DisplayId { get; set; }
        public int? DisplayGroupId { get; set; }
        public int? DaypartId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int LayoutId { get; set; }
        public int Priority { get; set; }
        public string RepeatType { get; set; }
        public int? RepeatInterval { get; set; }
        public string RepeatTimeUnit { get; set; }
        public string RepeatWeekdays { get; set; }
        public DateTime? RepeatUntil { get; set; }
        public bool HasChildren { get; set; }
    }

    [AutoMapFrom(typeof(ScheduledEventEditDto))]
    [AutoMapTo(typeof(ScheduledEvent))]
    public class ChildScheduledEventEditDto: ScheduledEventEditDto
    {
        public int? ParentId { get; set; }
    }

    public class GetScheduledEventOutput : IOutputDto
    {
        public ScheduledEventEditDto ScheduledEvent { get; set; }
    }

    public enum DisplayOption { 
        ALL, DSP,GRP
    }

    public enum RepeatType { 
        none,perminute,hourly,daily,weekly,monthly,yearly
    }
}
