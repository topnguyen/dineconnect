﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Play.Dto
{
    public class DisplayDto: FullAuditedEntityDto
    {
        public string Name { get; set; }
        public Resolution Resolution { get; set; }
        public int ResolutionId { get; set; }
        public string MacAddress { get; set; }
        public bool Active { get; set; }
        public bool Approved { get; set; }
    }

    public class CreateOrUpdateDisplayInput : IInputDto {
        public DisplayEditDto Display { get; set; }
    }

    [AutoMapTo(typeof(Display))]
    public class DisplayEditDto {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int ResolutionId { get; set; }
        // public virtual ResolutionEditDto Resolution { get; set; }
        public int LocationId { get; set; }
        //public virtual LocationEditDto Location { get; set; }
        public string MacAddress { get; set; }
        public bool Active { get; set; }
        public bool Approved { get; set; }
        //public List<DisplayGroupListDto> DisplayGroups { get; set; }
        public List<DisplayGroupOfDisplayEditDto> DisplayGroups { get; set; }
        public string RegistrationKey { get; set; }
    }

    [AutoMapFrom(typeof(Display))]
    [AutoMapTo(typeof(Display))]
    public class DisplayListDto {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int ResolutionId { get; set; }
        public virtual ResolutionListDto Resolution { get; set; }
        public int LocationId { get; set; }
        public string MacAddress { get; set; }
        public bool Active { get; set; }
        public bool Approved { get; set; }
        public string RegistrationKey { get; set; }
    }

    public class GetDisplayListInputDto : PagedAndSortedInputDto,IShouldNormalize
    {
        public string Filter { get; set; }
        public string Operation { get; set; }
        public bool IsDeleted { get; set; }
        public int[] Exclusions { get; set; }
        public int LocationId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    public class GetDisplayForEditOutput : IOutputDto
    {
        public DisplayEditDto Display { get; set; }
    }

    public class ConnectDisplayInput {
        public string RegistrationKey { get; set; }
        public string MacAddress { get; set; }
    }

    public class CheckDisplayRegistrationInput {
        public string RegistrationKey { get; set; }
        public string MacAddress { get; set; }
        public int DisplayId { get; set; }

    }

    public class UnregisterDisplayInput {
        public string RegistrationKey { get; set; }
        public string MacAddress { get; set; }
        public int DisplayId { get; set; }
    }

    public class DisplayDeleteOutput {
        public bool Success { get; set; }
        public string[] UsedByNames { get; set; }
    }
}
