﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Play.Dto
{
    [AutoMapTo(typeof(LayoutContent))]
    public class LayoutContentDto : FullAuditedEntityDto
    {
        public int TenantId { get; set; }
        public int LayoutId { get; set; }
        public Layout Layout { get; set; }
        public int RegionNo { get; set; }
        public string FileName { get; set; }
        public string SystemFileName { get; set; }
        public string FileType { get; set; }
        public int DisplayLength { get; set; }
        public bool Stretched { get; set; }
    }

    public class CreateOrUpdateLayoutContentInput : IInputDto {
        public LayoutContentEditDto[] LayoutContents { get; set; }
    }

    [AutoMapFrom(typeof(LayoutContent))]
    public class UpdateLayoutContentOutput
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string SystemFileName { get; set; }
        public int LayoutId { get; set; }
        public Layout Layout { get; set; }
        public int RegionNo { get; set; }
        public string FileType { get; set; }
        public int DisplayLength { get; set; }
        public bool Stretched { get; set; }
        public bool HasFile { get; set; }
    }

    [AutoMapTo(typeof(LayoutContent))]
    public class LayoutContentEditDto {
        public int? Id { get; set; }
        public string FileName { get; set; }
        public string SystemFileName { get; set; }
        public int LayoutId { get; set; }
        public int RegionNo { get; set; }
        public string FileType { get; set; }
        public int DisplayLength { get; set; }
        public bool Stretched { get; set; }
        public bool HasFile { get; set; }
    }

    public class InitializeLayoutContentsInput {
        public int LayoutId { get; set; }
        public int RegionCount { get; set; }
    }

    [AutoMapTo(typeof(LayoutContent))]
    public class LayoutContentListDto {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string SystemFileName { get; set; }
        public string Path { get; set; }
        public int LayoutId { get; set; }
        public int RegionNo { get; set; }
        public string FileType { get; set; }
        public int DisplayLength { get; set; }
        public bool Stretched { get; set; }
        public bool HasFile { get; set; }
    }

    public class LayoutContentFileUpdateCheckInput
    {

    }
    public class LayoutContentFileUpdateCheckOutput
    {

    }
}
