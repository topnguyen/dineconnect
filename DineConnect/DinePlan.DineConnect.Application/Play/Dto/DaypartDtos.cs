﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Play.Dto
{
    public class CreateOrUpdateDaypartInput : IInputDto
    {
        public DaypartEditDto Daypart { get; set; }
    }

    [AutoMapTo(typeof(Daypart))]
    public class DaypartEditDto: FullAuditedEntityDto
    {
        public DaypartEditDto()
        {
            Exceptions = new List<DaypartExceptionEditDto>();
        }
        public int? Id { get; set; }
        public string Name { get; set; }
        public int StartHour { get; set; }
        public int StartMinute { get; set; }
        public int EndHour { get; set; }
        public int EndMinute { get; set; }
        public List<DaypartExceptionEditDto> Exceptions { get; set; }
    }

    public class GetDaypartListInputDto: PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public string Operation { get; set; }
        public bool IsDeleted { get; set; }

        public bool IncludeCustom { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    [AutoMapFrom(typeof(Daypart))]
    public class DaypartListDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int StartHour { get; set; }
        public int StartMinute { get; set; }
        public int EndHour { get; set; }
        public int EndMinute { get; set; }
        public List<DaypartExceptionListDto> Exceptions { get; set; }
    }

    [AutoMapFrom(typeof(Daypart))]
    public class GetDaypartForEditOutput: IOutputDto
    {
        public DaypartEditDto Daypart { get; set; }
    }

    [AutoMapFrom(typeof(DaypartException))]
    [AutoMapTo(typeof(DaypartException))]
    public class DaypartExceptionEditDto: FullAuditedEntityDto
    {
        public int? Id { get; set; }
        public string Day { get; set; }
        public int StartHour { get; set; }
        public int StartMinute { get; set; }
        public int EndHour { get; set; }
        public int EndMinute { get; set; }
        public int DaypartId { get; set; }
    }

    [AutoMapFrom(typeof(DaypartException))]
    public class DaypartExceptionListDto
    {
        public int? Id { get; set; }
        public string Day { get; set; }
        public int StartHour { get; set; }
        public int StartMinute { get; set; }
        public int EndHour { get; set; }
        public int EndMinute { get; set; }
        public int DaypartId { get; set; }
    }
}

