﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Play.Dto
{
    [AutoMapTo(typeof(DisplayGroup))]
    public class DisplayGroupDto: FullAuditedEntityDto
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public List<Display> Displays { get; set; }
    }

    public class CreateOrUpdateDisplayGroupInput : IInputDto {
        public DisplayGroupEditDto DisplayGroup { get; set; }
    }

    [AutoMapTo(typeof(DisplayGroup))]
    public class DisplayGroupEditDto {
        public int? Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public List<Display> Displays { get; set; }
    }

    [AutoMapTo(typeof(DisplayGroup))]
    public class DisplayGroupOfDisplayEditDto
    {
        public int? Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }

    public class GetDisplayGroupListInputDto : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public int[] Exclusions { get; set; }
        public string Operation { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    public class GetDisplaysOfGroupListInputDto : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public int DispalyGroupId { get; set; }
        public int[] Exclusions { get; set; }
        public string Operation { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    [AutoMapFrom(typeof(DisplayGroup))]
    [AutoMapTo(typeof(DisplayGroup))]
    public class DisplayGroupListDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public List<DisplayListDto> Displays { get; set; }
    }

    public class GetDisplayGroupForEditOutput : IOutputDto {
        public DisplayGroupListDto DisplayGroup { get; set; }
    }
}
