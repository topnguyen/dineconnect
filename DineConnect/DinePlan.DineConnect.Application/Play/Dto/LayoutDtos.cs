﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Play.Implementation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Play.Dto
{
    [AutoMapTo(typeof(Layout))]
    public class LayoutDto: FullAuditedEntityDto
    {
        public string Name { get; set; }
        public Resolution Resolution { get; set; }
        public int ResolutionId { get; set; }
        public string Description { get; set; }
        public int TemplateId { get; set; }
        public Collection<LayoutContent> Contents { get; set; }
    }

    public class CreateOrUpdateLayoutInput : IInputDto
    {
        public LayoutEditDto Layout { get; set; }
    }

    [AutoMapTo(typeof(Layout))]
    public class LayoutEditDto {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int ResolutionId { get; set; }
        public string Description { get; set; }
        public int TemplateId { get; set; }
    }

    [AutoMapFrom(typeof(Layout))]
    public class LayoutListDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public Resolution Resolution { get; set; }
        public int ResolutionId { get; set; }
        public string Description { get; set; }
        public int TemplateId { get; set; }
        public LayoutContentTemplate Template { get; set; }
        public List<LayoutContentListDto> Contents { get; set; }
    }
    public class GetLayoutListInputDto: PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public string Operation { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    [AutoMapFrom(typeof(Layout))]
    public class UpdateLayoutOutput {
        public int Id { get; set; }
        public string Name { get; set; }
        public Resolution Resolution { get; set; }
        public string Description { get; set; }
        public int TemplateId { get; set; }
    }

    public class DeleteLayoutOutput {
        public bool Success { get; set; }
        public string[] UsedByNames { get; set; }
    }

    public class GetLayoutForEditOutput: IOutputDto
    {
        public LayoutEditDto Layout { get; set; }
    }

    public class GetLayoutForDisplayInput {
        public int DisplayId { get; set; }
        public string MacAddress { get; set; }
        public string RegistrationKey { get; set; }
        public DateTime DisplayTime { get; set; }
    }
}
