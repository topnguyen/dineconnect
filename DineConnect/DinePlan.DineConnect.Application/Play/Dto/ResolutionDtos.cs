﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Play.Dto
{
    [AutoMapTo(typeof(Resolution))]
    public class ResolutionDtos: FullAuditedEntityDto
    {
        public string Name { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public bool Enabled { get; set; }
    }

    public class CreateOrUpdateResolutionInput : IInputDto
    {
        public ResolutionEditDto Resolution { get; set; }
    }

    [AutoMapTo(typeof(Resolution))]
    public class ResolutionEditDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public bool Enabled { get; set; }
    }

    public class GetResolutionListInputDto: PagedAndSortedInputDto,
        IShouldNormalize
    {
        public string Filter { get; set; }
        public string Operation { get; set; }
        public bool IsDeleted { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    [AutoMapFrom(typeof(Resolution))]
    public class ResolutionListDto {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public bool Enabled { get; set; }
    }

    public class GetResolutionForEditOutput: IOutputDto {
        public ResolutionEditDto Resolution { get; set; }
    }

    public class ResolutionDeleteOutput
    {
        public bool Success { get; set; }
        public string[] UsedByNames { get; set; }
    }
}
