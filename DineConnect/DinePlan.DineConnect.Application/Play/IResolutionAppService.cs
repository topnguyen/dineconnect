﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Play.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Play
{
    public interface IResolutionAppService: IApplicationService
    {
        Task<PagedResultOutput<ResolutionListDto>> GetAll(GetResolutionListInputDto input);
        Task<int> CreateOrUpdateResolution(CreateOrUpdateResolutionInput input);
        Task<GetResolutionForEditOutput> GetResolutionForEdit(NullableIdInput input);
        Task<ResolutionDeleteOutput> DeleteResolution(IdInput input);
    }
}
