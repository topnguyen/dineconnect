﻿using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.CaterModel;
using DinePlan.DineConnect.House;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.House.Transaction.Dtos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Cater.Quotation.Dtos
{
    [AutoMapFrom(typeof(CaterQuotationHeader), typeof(QuotationHeaderListDto))]
    public class SimpleQuotationHeaderListDto 
    {
        public int Id { get; set; }
        public int CaterCustomerId { get; set; }
        public string CaterCustomerCustomerName { get; set; }
        public DateTime EventDateTime { get; set; }
        public string EventDateTimeString => EventDateTime.ToString("dd-MMM-yyyy");
        public int NoOfPax { get; set; }
        public bool DoesCostingRequired { get; set; }
        public string CurrentStatus { get; set; }
    }


    [AutoMapFrom(typeof(CaterQuotationHeader))]
    public class QuotationHeaderListDto : CreationAuditedEntity
    {
        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }
        public int CaterCustomerId { get; set; }
        public string CaterCustomerCustomerName { get; set; }
        public int EventAddressId { get; set; }
        public DateTime EventDateTime { get; set; }
        public int NoOfPax { get; set; }
        public int OrderChannel { get; set; }
        public string OrderChannelString => ((OrderChannels)OrderChannel).ToString();
        public decimal TotalAmount { get; set; }
        public string Note { get; set; }
        public bool DoesCostingRequired { get; set; }
        public string CurrentStatus { get; set; }
        public bool DoesPOCreatedForThisCateringOrder { get; set; }
        public List<PoStatusWithCount> PoStatusCountList { get; set; }
        public string PoStatusRemarks { get; set; }
        public int NoOfMaterials { get; set; }
        public int NoOfPoDependent { get; set; }
        public int NoOfStockDependent { get; set; }
        public bool DoesSplitNeeded { get; set; }
        public List<PoStatusWiseList> PoStatusWiseLists { get; set; }
        public DateTime DeliveryDateRequested { get; set; }
        public List<CaterQuoteRequiredMaterialListDto> CaterRequiredMaterialListDtos { get; set; }
    }

    public class PoStatusWithCount
    {
        public string PoStatus { get; set; }
        public int Count { get; set; }
    }

    [AutoMapFrom(typeof(CaterQuoteRequiredMaterial))]
    public class CaterQuoteRequiredMaterialListDto : FullAuditedEntity
    {
        public int CaterQuoteHeaderId { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public decimal TotalQty { get; set; }
        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }
        public int? StatusRefId { get; set; }
        public string StatusRefName { get; set; }
        public string Remarks { get; set; }
        public List<CaterMaterialPurchaseOrderDetailViewDto> PoDetails { get; set; }
        //public List<MaterialMenuMappingWithWipeOut> MenuMappedSubList { get; set; }
        //public List<ConsolidatedMaterialRequiredWithLinkedMenuDto> ConsolidatedSubDetails { get; set; }  
        //[ForeignKey("PurchaseOrderRefId")]
        //public int? PurchaseOrderRefId { get; set; }
        //public PurchaseOrder PurchaseOrders { get; set; }
    }

    [AutoMapFrom(typeof(PurchaseOrderDetail), typeof(PurchaseOrderDetailViewDto))]
    public class CaterMaterialPurchaseOrderDetailViewDto 
    {
        public int? Id { get; set; }
        public int PoRefId { get; set; }
        public string PoReferenceCode { get; set; }
        public string PoStatusRemarks { get; set; }
        public int SupplierRefId { get; set; }
        public string SupplierRefName { get; set; }
        public int MaterialRefId { get; set; }
        public string Barcode { get; set; }
        public string MaterialRefName { get; set; }
        public decimal QtyOrdered { get; set; }
        public decimal QtyReceived { get; set; }
        public decimal QtyPending => Math.Round( QtyOrdered - QtyReceived,6);
        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }
        public int DefaultUnitId { get; set; }
        public string Uom { get; set; }
        public string PoCurrentStatus { get; set; }
    }


    [AutoMapFrom(typeof(CaterQuoteRequiredMaterialVsPurchaseOrderLink))]
    public class CaterQuoteRequiredMaterialVsPurchaseOrderLinkListDto : FullAuditedEntity
    {
        public int CaterQuoteRequiredMaterialDataRefId { get; set; }

        public int PurchaseOrderRefId { get; set; }

        public string PoCurrentStatus { get; set; }
    }


    public class CaterRequestMaterialDetailViewDto
    {
        public int CaterQuoteHeaderId { get; set; }
        public QuotationHeaderListDto CaterHeader { get; set; }
        public int LocationRefId { get; set; }

        public string LocationRefName { get; set; }

        public int Sno { get; set; }

        public int MaterialRefId { get; set; }
        public string MaterialPetName { get; set; }
        public string MaterialRefName { get; set; }

        public int MaterialTypeId { get; set; }

        public string MaterialTypeName { get; set; }

        public decimal RequestQty { get; set; }

        public decimal IssueQty { get; set; }
        public int UnitRefId { get; set; }

        public string UnitRefName { get; set; }

        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }

        public int? StatusRefId { get; set; }

        [Required, StringLength(1)]                          //  P - Pending  (Default ) , C - Completed, X - Cancel
        public string CurrentStatus { get; set; }
        
        public string Uom { get; set; }

        public decimal OnHand { get; set; }
        public decimal MinimumStock { get; set; }
        public decimal MaximumStock { get; set; }

        public decimal ReorderLevel { get; set; }

        public List<MaterialUsagePattern> UsagePatternList { get; set; }
    }

    public class CaterRequestMaterialPOGenerationDto
    {
        public string Barcode { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialPetName { get; set; }
        public string MaterialRefName { get; set; }
        public int MaterialTypeId { get; set; }
        public string MaterialTypeName { get; set; }
        public decimal TotalRequiredQty { get; set; }
        
        public decimal TotalRequestQty { get; set; }
        public int UnitRefId { get; set; }
        public string Uom { get; set; }
        public decimal AlreadyOrderedQuantity { get; set; }
        public decimal LiveStock { get; set; }
        public decimal OnHandAfterIssue { get; set; }
        public decimal MinimumStock { get; set; }
        public decimal MaximumStock { get; set; }
        public decimal ReOrderLevel { get; set; }
        public decimal PoOrderQuantityRecommended { get; set; }
        public decimal PoOrderQuantity { get; set; }
        public string Remarks { get; set; }
        public string FormulaForRecommended { get; set; }
        public List<CaterRequestMaterialConsolidatedViewDto> LocationWiseRequestDetails { get; set; }
        public SupplierMaterialViewDto DefaultSupplier { get; set; }
        public List<SupplierMaterialViewDto> SupplierMaterialViewDtos { get; set; }
        public string SupplierRemarks { get; set; }
        //public List<InterTransferDetailWithPORefIds> InterTransferDetailWithPORefIds { get; set; }
        public List<CaterHeaderRefIds> CaterHeaderRefIds { get; set; }
        public List<QuotationHeaderListDto> CaterHeaders { get; set; }
        public List<CaterRequestMaterialDetailViewDto> SubDetails { get; set; }
        public int? StatusRefId { get; set; }
        public string StatusRefName { get; set; }
        public List<CaterMaterialPurchaseOrderDetailViewDto> PoDetails { get; set; }
        public bool OmitQueueInOrder { get; set; }
    }


    public class CaterRequestMaterialConsolidatedViewDto
    {
        public int LocationRefId { get; set; }

        public string LocationRefName { get; set; }

        public int MaterialRefId { get; set; }
        public string MaterialPetName { get; set; }
        public string MaterialRefName { get; set; }

        public int MaterialTypeId { get; set; }

        public string MaterialTypeName { get; set; }
        
        public decimal RequiredQty { get; set; }
        public decimal OrderedQty { get; set; }
        public decimal RequestQty { get; set; }

        public int UnitRefId { get; set; }

        public string Uom { get; set; }
        //public decimal OnHand { get; set; }
        public decimal LiveStock { get; set; }
        public decimal MinimumStock { get; set; }
        public decimal MaximumStock { get; set; }
        public decimal ReorderLevel { get; set; }
        public List<CaterHeaderRefIds> CaterHeaderRefIds { get; set; }
        public List<QuotationHeaderListDto> CaterHeaders { get; set; }
        public int? StatusRefId { get; set; }
        public string StatusRefName { get; set; }
        public List<CaterMaterialPurchaseOrderDetailViewDto> PoDetails { get; set; }
        public List<CaterRequestMaterialDetailViewDto> SubDetails { get; set; }
    }

    public class CaterHeaderRefIds
    {
        public int CaterHeaderRefId { get; set; }

    }

    public class GetCaterRequestMaterialAsPO
    {
        public int LocationRefId { get; set; }
        public int ShippingLocationId { get; set; }
        public DateTime DeliveryDateExpected { get; set; }
        public List<CaterRequestMaterialPOGenerationDto> RequestMaterialsToBeOrdered { get; set; }

    }
}