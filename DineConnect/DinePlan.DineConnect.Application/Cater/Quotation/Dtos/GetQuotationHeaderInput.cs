﻿using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Cater.Quotation.Dtos
{
    public class GetQuotationHeaderInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? CaterCustomerId { get; set; }
        public bool PoStatusRequired { get; set; }
        public string CurrentStatus { get; set; }
        public int DefaultLocationRefId { get; set; }

        public List<SimpleLocationDto> Locations { get; set; }

        public LocationGroupDto LocationGroup { get; set; }

        public List<LocationTag> LocationTags { get; set; }
        public bool OmitAlreadyPoLinkedRequest { get; set; }
        public List<QuotationHeaderListDto> RequestList { get; set; }
        public List<int> CaterHeaderRefIdList { get; set; }
        public bool RequestBasedOnRequestList { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }

    public class CaterRequestList
    {
        public List<QuotationHeaderListDto> QuoteList { get; set; }
    }
}