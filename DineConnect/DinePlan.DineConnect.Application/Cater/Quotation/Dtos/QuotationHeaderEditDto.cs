﻿using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using System;

namespace DinePlan.DineConnect.Cater.Quotation.Dtos
{
    [AutoMapTo(typeof(CaterQuotationHeader))]
    public class QuotationHeaderEditDto : CreationAuditedEntity<int?>
    {
        public int? TenantId { get; set; }
        public int CaterCustomerId { get; set; }
        public string CaterCustomerName { get; set; }

        public int LocationRefId { get; set; }

        public int EventAddressId { get; set; }

        public DateTime EventDateTime { get; set; }
        public DateTime EventTime { get; set; }
        public int NoOfPax { get; set; }

        public int OrderChannel { get; set; }

        public decimal TotalAmount { get; set; }

        public string Note { get; set; }

        public decimal QuoteTotalAmount { get; set; }

        public bool DoesPOCreatedForThisCateringOrder { get; set; }
    }
}