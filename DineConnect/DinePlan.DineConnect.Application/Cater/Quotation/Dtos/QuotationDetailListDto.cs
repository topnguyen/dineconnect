﻿using Abp.AutoMapper;
using DinePlan.DineConnect.House;
using DinePlan.DineConnect.House.Master.Dtos;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Cater.Quotation.Dtos
{
    [AutoMapFrom(typeof(CaterQuotationDetail))]
    public class QuotationDetailListDto
    {
        public int Id { get; set; }
        public int HeaderId { get; set; }
        public string GroupName { get; set; }
        public string ProductName { get; set; }

        public int MenuItemPortionId { get; set; }
        public string MenuItemName { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public string Taxes { get; set; }
        public string Note { get; set; }
        public string Tags { get; set; }
        public decimal Amount => Price * Quantity;
        public string AliasCode { get; set; }

        //public  string AliasName { get; set; }
        public List<QuotationDetailListDto> SubDetails { get; set; }

        public QuotationDetailListDto()
        {
            SubDetails = new List<QuotationDetailListDto>();
        }
    }

    public class QuotationWiseCosting
    {
        public QuotationHeaderEditDto QuotationInfo { get; set; }
        public List<QuotationDetailListDto> QuotationDetails { get; set; }
        public List<CateringMaterialMenuMapping> MenuVsMaterialAllDetails { get; set; }
        public List<QuoteRequiredMaterialWithCostDto> QuoteRequiredMaterialWithCostDtoList { get; set; }
        public decimal TotalMaterialCostRequired { get; set; }
        public decimal FoodCostPercentage => TotalCateringBillValue == 0.0m ? 0.0m : Math.Round(TotalMaterialCostRequired / TotalCateringBillValue * 100, 2);
        public decimal TotalCateringBillValue { get; set; }
        public bool OverAllFlag { get; set; }
    }

    [AutoMapFrom(typeof(MaterialMenuMapping))]
    public class MaterialMenuMappingForQuotationDetail
    {
        public int? LocationRefId { get; set; }
        public string DepartmentName { get; set; }
        public int PosMenuPortionRefId { get; set; }
        
        public string PosRefName { get; set; }
        public int MenuQuantitySold { get; set; }

        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public decimal PortionPerUnitOfSales { get; set; }
        public decimal PortionQty { get; set; }
        public int PortionUnitId { get; set; }
        public string UnitRefName { get; set; }
        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }
        public decimal WastageExpected { get; set; }
        public bool AutoSalesDeduction { get; set; }
        public decimal TotalSaleQuantity { get; set; }
        public string QuoteDetail { get; set; }
        public string Remarks { get; set; }
        
    }

    public class QuoteRequiredMaterialWithCostDto
    {
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public decimal TotalQty { get; set; }
        public int UnitRefId { get; set; }
        public string Uom { get; set; }
        public decimal Price { get; set; }
        public decimal TotalCost { get; set; }
        public decimal CostPercentage { get; set; }
        public List<CateringMaterialMenuMapping> MenuMappedSubList { get; set; }
        public List<ConsolidatedMaterialRequiredWithLinkedMenuDto> ConsolidatedSubDetails { get; set; }
        public string Remarks { get; set; }
    }
        

    public class GetQuotationWiseMaterialRequiredInputDto
    {
        public List<CaterQuotationHeader> CaterQuotations { get; set; }
        public List<int> QuotationRefIdList { get; set; }
        public int? QuotationRefId { get; set; }
        public List<int> MaterialRefIds { get; set; }
        public int? LocationRefId { get; set; }
    }

    public class CateringMaterialConsolidatedProjectionDto
    {
        public QuotationWiseCosting OverAllCosting { get; set; }
        public List<QuotationWiseCosting> QuotationWiseCostingList { get; set; }
        public string RateRemarks { get; set; }
    }



    [AutoMapFrom(typeof(MaterialMenuMapping))]
    public class ConsolidatedMaterialRequiredWithLinkedMenuDto
    {
        public int? LocationRefId { get; set; }
        public string DepartmentName { get; set; }
        public int PosMenuPortionRefId { get; set; }

        public string PosRefName { get; set; }
        public string MenuItemName { get; set; }
        public string Portion { get; set; }

        public int MenuQuantitySold { get; set; }

        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public decimal PortionQty { get; set; }
        public int PortionUnitId { get; set; }
        public string UnitRefName { get; set; }
        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }
        public decimal WastageExpected { get; set; }
        public bool AutoSalesDeduction { get; set; }
        public decimal TotalSaleQuantity { get; set; }
        public decimal TotalSaleValue { get; set; }
        public decimal CostPerUnit { get; set; }
        public decimal CostTotal { get; set; }

        public List<CateringMaterialMenuMapping> SubDetails { get; set; }
        

    }

    [AutoMapFrom(typeof(MaterialMenuMapping))]
    public class CateringMaterialMenuMapping
    {
        public int? LocationRefId { get; set; }
        public string DepartmentName { get; set; }
        public int PosMenuPortionRefId { get; set; }
        public string PosRefName { get; set; }
        public int MenuQuantitySold { get; set; }
        public int MaterialRefId { get; set; }
        public string MaterialRefName { get; set; }
        public decimal PortionPerUnitOfSales { get; set; }
        public decimal PortionQty { get; set; }
        public int PortionUnitId { get; set; }
        public string UnitRefName { get; set; }
        public int DefaultUnitId { get; set; }
        public string DefaultUnitName { get; set; }
        public decimal WastageExpected { get; set; }
        public bool AutoSalesDeduction { get; set; }
        public int NoOfPax { get; set; }
        public decimal TotalSaleQuantity { get; set; }
    }



}