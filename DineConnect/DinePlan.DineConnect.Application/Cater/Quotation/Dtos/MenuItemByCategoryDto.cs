﻿using System.Collections.Generic;

namespace DinePlan.DineConnect.Cater.Quotation.Dtos
{
    public class MenuItemByCategoryDto
    {
        public string CategoryName { get; set; }

        public List<MenuItemComboListDto> MenuItems { get; set; }
    }
}