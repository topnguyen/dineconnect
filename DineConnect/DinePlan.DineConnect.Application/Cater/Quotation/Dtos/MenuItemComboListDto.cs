﻿using DinePlan.DineConnect.Connect.Menu.Dtos;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Cater.Quotation.Dtos
{
    public class MenuItemComboListDto
    {
        public MenuItemComboListDto()
        {
            MenuItem = new MenuItemListDto();
            ProductComboGroups = new List<ProductComboGroupEditDto>();
        }

        public int? QuoteId { get; set; }
        public bool IsSelected { get; set; }
        public bool IsOrdered { get; set; }

        public bool IsRemoved { get; set; }
        public MenuItemListDto MenuItem { get; set; }

        public List<ProductComboGroupEditDto> ProductComboGroups { get; set; }
    }
}