﻿using DinePlan.DineConnect.Cater.Quotation.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Quotation.Exporting
{
    public interface IQuotationHeaderExporter
    {
        FileDto ExportToFile(List<QuotationHeaderListDto> dtos);
    }
}