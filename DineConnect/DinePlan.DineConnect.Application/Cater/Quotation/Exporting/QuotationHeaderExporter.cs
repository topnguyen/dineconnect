﻿using DinePlan.DineConnect.Cater.Quotation.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Quotation.Exporting
{
    public class QuotationHeaderExporter : FileExporterBase, IQuotationHeaderExporter
    {
        public FileDto ExportToFile(List<QuotationHeaderListDto> dtos)
        {
            return CreateExcelPackage(
                "CaterQuotationHeaderList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("CaterQuotationHeader"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("CustomerName"),
                        L("EventDate"),
                        L("EventTime"),
                        L("CreationTime")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.CaterCustomerCustomerName,
                        _ => _.EventDateTime.ToString("yyyy-MM-dd"),
                        _ => _.EventDateTime.ToString("hh:mm"),
                        _ => _.CreationTime.ToString("yyyy-MM-dd")

                        );

                    for (var i = 1; i <= 5; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}