﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Cater.Quotation.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Cater.Quotation
{
    public interface IQuotationHeaderAppService : IApplicationService
    {
        Task AddOrUpdateQuoteDetails(List<MenuItemComboListDto> input);

        Task<int> CreateOrUpdateQuotationHeader(QuotationHeaderEditDto input);

        Task DeleteQuotationHeader(NullableIdInput input);

        Task<FileDto> GetAllToExcel();

        Task<List<MenuItemByCategoryDto>> GetMenuItemCombos(int quoteId, string filterText);

        Task<List<QuotationDetailListDto>> GetQuotationDetails(int quoteId);

        Task<PagedResultOutput<QuotationHeaderListDto>> GetQuotationHeader(GetQuotationHeaderInput input);

        Task<QuotationHeaderEditDto> GetQuotationHeaderForEdit(NullableIdInput input);
        Task<CateringMaterialConsolidatedProjectionDto> GetMaterialsRequiredForGivenQuotation(GetQuotationWiseMaterialRequiredInputDto input);
        Task AppendQuoteMaterialRequiredData(QuotationWiseCosting quote);
        Task<List<CaterRequestMaterialPOGenerationDto>> GetPoRequestBasedOnQuoteSelection(GetQuotationHeaderInput input);
        Task<List<QuotationHeaderListDto>> GetConsolidatedRequestPendingList(GetQuotationHeaderInput input);
        Task<List<CaterRequestMaterialConsolidatedViewDto>> GetConsolidatedPendingBasedGivenCaterList(GetQuotationHeaderInput input);

    }
}