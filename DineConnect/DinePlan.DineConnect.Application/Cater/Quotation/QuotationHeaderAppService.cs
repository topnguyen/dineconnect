﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Cater.Quotation.Dtos;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House;
using DinePlan.DineConnect.House.Master;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Quotation.Exporting;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Linq.Dynamic;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Cater.Quotation
{
    public class QuotationHeaderAppService : DineConnectAppServiceBase, IQuotationHeaderAppService
    {
        private readonly IRepository<CaterQuotationHeader> _caterQuotationHeaderRepository;
        private readonly IQuotationHeaderExporter _quotationHeaderExporter;
        private readonly IRepository<MenuItem> _menuitemRepository;
        private readonly IRepository<ProductCombo> _productComboRepository;
        private readonly IRepository<CaterQuotationDetail> _caterQuotationDetailRepository;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<MaterialMenuMapping> _materialmenumappingRepo;
        private readonly IRepository<MenuMappingDepartment> _menumappingDepartmentRepo;
        private readonly IRepository<Department> _departmentRepo;

        //private readonly IRepository<MenuItemPortion> _menuItemPortionRepo;
        //private readonly IRepository<MenuItem> _menuItemRepo;
        private readonly IUnitConversionAppService _unitConversionAppService;

        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IHouseReportAppService _housereportAppService;
        private readonly IRepository<Unit> _unitRepo;
        private readonly IRepository<CaterQuoteRequiredMaterial> _caterQuoteRequiredMaterialRepo;
        private readonly IRepository<CaterQuoteRequiredMaterialVsPurchaseOrderLink> _caterQuoteRequiredMaterialVsPoLinkRepo;
        private readonly IRepository<CaterQuoteRequiredMaterialVsMenuLink> _caterQuoteRequiredMaterialVsMenuLinkRepo;
        private readonly IRepository<PurchaseOrder> _purchaseOrderRepo;
        private readonly IRepository<PurchaseOrderDetail> _purchaseOrderDetailRepo;
        private readonly ISupplierMaterialAppService _supplierMaterialAppService;
        private readonly IMaterialLocationWiseStockAppService _materiallocationwisestockAppService;
        private readonly IRepository<MaterialLocationWiseStock> _materiallocationwisestockRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<Supplier> _supplierRepo;

        public QuotationHeaderAppService(
           IRepository<CaterQuotationHeader> caterQuotationHeaderRepository
            , IQuotationHeaderExporter quotationHeaderExporter
            , IRepository<MenuItem> menuitemRepository
            , IRepository<ProductCombo> productComboRepository
            , IRepository<CaterQuotationDetail> caterQuotationDetailRepository,
             IRepository<Material> materialRepo,
            IRepository<MaterialMenuMapping> materialmenumappingRepo,
            IRepository<MenuMappingDepartment> menumappingDepartmentRepo,
            IRepository<Department> departmentRepo,
            IUnitConversionAppService unitConversionAppService
            , IUnitOfWorkManager unitOfWorkManager,
            IHouseReportAppService housereportAppService,
            IRepository<Unit> unitRepo,
            IRepository<CaterQuoteRequiredMaterial> caterQuoteRequiredMaterialRepo,
        IRepository<CaterQuoteRequiredMaterialVsPurchaseOrderLink> caterQuoteRequiredMaterialVsPoLinkRepo,
        IRepository<CaterQuoteRequiredMaterialVsMenuLink> caterQuoteRequiredMaterialVsMenuLinkRepo,
        IRepository<PurchaseOrder> purchaseOrderRepo,
        IRepository<PurchaseOrderDetail> purchaseOrderDetailRepo,
        ISupplierMaterialAppService supplierMaterialAppService,
        IMaterialLocationWiseStockAppService materiallocationwisestockAppService,
        IRepository<MaterialLocationWiseStock> materiallocationwisestockRepo,
        IRepository<Location> locationRepo,
        IRepository<Supplier> supplierRepo
            )
        {
            _caterQuotationHeaderRepository = caterQuotationHeaderRepository;
            _quotationHeaderExporter = quotationHeaderExporter;
            _menuitemRepository = menuitemRepository;
            _productComboRepository = productComboRepository;
            _caterQuotationDetailRepository = caterQuotationDetailRepository;
            _materialRepo = materialRepo;
            _materialmenumappingRepo = materialmenumappingRepo;
            _menumappingDepartmentRepo = menumappingDepartmentRepo;
            _departmentRepo = departmentRepo;
            _unitConversionAppService = unitConversionAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _housereportAppService = housereportAppService;
            _unitRepo = unitRepo;
            _caterQuoteRequiredMaterialRepo = caterQuoteRequiredMaterialRepo;
            _caterQuoteRequiredMaterialVsPoLinkRepo = caterQuoteRequiredMaterialVsPoLinkRepo;
            _caterQuoteRequiredMaterialVsMenuLinkRepo = caterQuoteRequiredMaterialVsMenuLinkRepo;
            _purchaseOrderRepo = purchaseOrderRepo;
            _purchaseOrderDetailRepo = purchaseOrderDetailRepo;
            _supplierMaterialAppService = supplierMaterialAppService;
            _materiallocationwisestockAppService = materiallocationwisestockAppService;
            _materiallocationwisestockRepo = materiallocationwisestockRepo;
            _locationRepo = locationRepo;
            _supplierRepo = supplierRepo;
        }

        public async Task<PagedResultOutput<QuotationHeaderListDto>> GetQuotationHeader(GetQuotationHeaderInput input)
        {
            if (!input.Filter.IsNullOrEmpty())
            {
                input.Filter = Regex.Replace(input.Filter.Trim(), @"\s+", " ");
            }

            var iqCaterHeader = _caterQuotationHeaderRepository.GetAll();
            if (input.StartDate.HasValue)
                iqCaterHeader = iqCaterHeader.Where(t => DbFunctions.TruncateTime(t.EventDateTime) >= DbFunctions.TruncateTime(input.StartDate.Value));
            if (input.EndDate.HasValue)
                iqCaterHeader = iqCaterHeader.Where(t => DbFunctions.TruncateTime(t.EventDateTime) <= DbFunctions.TruncateTime(input.EndDate.Value));
            if (input.CaterCustomerId.HasValue)
                iqCaterHeader = iqCaterHeader.Where(t => t.CaterCustomerId == input.CaterCustomerId.Value);
            if (input.CaterHeaderRefIdList != null && input.CaterHeaderRefIdList.Count > 0)
            {
                iqCaterHeader = iqCaterHeader.Where(t => input.CaterHeaderRefIdList.Contains(t.Id));
            }

            var allItems = iqCaterHeader
               .Include(e => e.CaterCustomer)
               .Include(e => e.EventAddress)
               .Include(e => e.Location)
               .Where(c => c.TenantId == AbpSession.TenantId)
               .WhereIf(!input.Filter.IsNullOrEmpty(), p => p.CaterCustomer.CustomerName.Contains(input.Filter));

            if (input.Sorting == null)
                input.Sorting = "Id";

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.Select(e =>
            {
                var dto = e.MapTo<QuotationHeaderListDto>();
                dto.CaterCustomerCustomerName = e.CaterCustomer.CustomerName;
                dto.LocationRefName = e.Location.Code + " - " + e.Location.Name;
                return dto;
            }).ToList();
            var allItemCount = await allItems.CountAsync();

            if (input.PoStatusRequired)
            {
                List<int> arrHeaderIds = allListDtos.Select(t => t.Id).ToList();
                var tempQuoteRequestMaterials = await _caterQuoteRequiredMaterialRepo.GetAllListAsync(t => arrHeaderIds.Contains(t.CaterQuoteHeaderId));
                var rsDetails = tempQuoteRequestMaterials.MapTo<List<CaterQuoteRequiredMaterialListDto>>();
                List<int> arrMaterialRefIds = rsDetails.Select(t => t.MaterialRefId).ToList();
                var tempMaterials = await _materialRepo.GetAllListAsync(t => arrMaterialRefIds.Contains(t.Id));
                var rsMaterials = tempMaterials.MapTo<List<MaterialViewDto>>();
                var rsUnits = await _unitRepo.GetAllListAsync();
                var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();

                List<int> arrCaterQuoteRequiredMaterialDataRefId = rsDetails.Select(t => t.Id).ToList();
                var tempQuoteRequestMaterialsVsPo = await _caterQuoteRequiredMaterialVsPoLinkRepo.GetAllListAsync(t => arrCaterQuoteRequiredMaterialDataRefId.Contains(t.CaterQuoteRequiredMaterialDataRefId));
                var rsQuotePoLinkDtos = tempQuoteRequestMaterialsVsPo.MapTo<List<CaterQuoteRequiredMaterialVsPurchaseOrderLinkListDto>>();

                var arrPoIds = tempQuoteRequestMaterialsVsPo.Where(t => t.PurchaseOrderRefId > 0).Select(t => t.PurchaseOrderRefId).ToList();
                var tempPoMaster = await _purchaseOrderRepo.GetAllListAsync(t => arrPoIds.Contains(t.Id));
                var rsPoMaster = tempPoMaster.MapTo<List<PurchaseOrderListDto>>();
                var arrSupplierRefIds = rsPoMaster.Select(t => t.SupplierRefId).ToList();
                foreach (var lst in rsPoMaster)
                {
                    if (lst.PoCurrentStatus.Equals("P"))
                    {
                        lst.PoCurrentStatus = L("PendingForPOApproval");
                    }
                    else if (lst.PoCurrentStatus.Equals("A"))
                    {
                        lst.PoCurrentStatus = L("Approved");
                    }
                    else if (lst.PoCurrentStatus.Equals("X"))
                    {
                        lst.PoCurrentStatus = L("Declined");
                    }
                    else if (lst.PoCurrentStatus.Equals("C"))
                    {
                        lst.PoCurrentStatus = L("Completed");
                    }
                    else if (lst.PoCurrentStatus.Equals("H"))
                    {
                        lst.PoCurrentStatus = L("Partial");
                    }
                }

                var rsSuppliers = await _supplierRepo.GetAllListAsync(t => arrSupplierRefIds.Contains(t.Id));

                var tempPoDetail = await _purchaseOrderDetailRepo.GetAllListAsync(t => arrPoIds.Contains(t.PoRefId));
                var rsPoDetails = tempPoDetail.MapTo<List<PurchaseOrderDetailViewDto>>();
                foreach (var poDetail in rsPoDetails)
                {
                    var currentPoMaster = rsPoMaster.FirstOrDefault(t => t.Id == poDetail.PoRefId);
                    poDetail.PoReferenceCode = currentPoMaster.PoReferenceCode;
                    poDetail.PoStatusRemarks = currentPoMaster.PoCurrentStatus;
                    var sup = rsSuppliers.FirstOrDefault(t => t.Id == currentPoMaster.SupplierRefId);
                    poDetail.SupplierRefId = currentPoMaster.SupplierRefId;
                    poDetail.SupplierRefName = sup.SupplierName;
                    var unit = rsUnits.FirstOrDefault(t => t.Id == poDetail.UnitRefId);
                    poDetail.UnitRefName = unit.Name;
                    poDetail.PoCurrentStatus = L("PoDetailStatus", poDetail.QtyOrdered, poDetail.QtyReceived, currentPoMaster.Id, currentPoMaster.PoReferenceCode, unit.Name);
                }

                foreach (var item in allListDtos)
                {
                    var deliveryDateRequested = DateTime.Today;
                    if (item.EventDateTime > DateTime.Today.AddDays(3))
                        deliveryDateRequested = item.EventDateTime.AddDays(-3);
                    else if (item.EventDateTime > DateTime.Today.AddDays(2))
                        deliveryDateRequested = item.EventDateTime.AddDays(-2);
                    else if (item.EventDateTime > DateTime.Today.AddDays(1))
                        deliveryDateRequested = item.EventDateTime.AddDays(-1);
                    else
                        deliveryDateRequested = DateTime.Today;

                    item.DeliveryDateRequested = deliveryDateRequested;
                    var details = rsDetails.Where(t => t.CaterQuoteHeaderId == item.Id).ToList();
                    var countDetails = details.Count();
                    item.NoOfMaterials = countDetails;

                    arrCaterQuoteRequiredMaterialDataRefId = details.Select(t => t.Id).ToList();
                    var quoteLinkedPos = rsQuotePoLinkDtos.Where(t => arrCaterQuoteRequiredMaterialDataRefId.Contains(t.CaterQuoteRequiredMaterialDataRefId)).ToList();
                    var poIds = quoteLinkedPos.Select(t => t.PurchaseOrderRefId).ToList();

                    List<int> arrPoLinkedDataRefIds = quoteLinkedPos.Select(t => t.CaterQuoteRequiredMaterialDataRefId).ToList();
                    var poDependentCount = details.Where(t => arrPoLinkedDataRefIds.Contains(t.Id)).Count();
                    var stockDependentCount = details.Where(t => !arrPoLinkedDataRefIds.Contains(t.Id)).Count();
                    item.NoOfPoDependent = poDependentCount;
                    item.NoOfStockDependent = stockDependentCount;

                    if (item.NoOfStockDependent > 0 && item.NoOfPoDependent > 0)
                    {
                        item.DoesSplitNeeded = true;
                    }
                    else if (item.NoOfStockDependent > 0 && item.NoOfPoDependent == 0)
                    {
                        item.NoOfStockDependent = 0;
                    }

                    var caterRequiredMaterials = rsDetails.Where(t => t.CaterQuoteHeaderId == item.Id).OrderByDescending(t => t.TotalQty).ToList();

                    foreach (var catmat in caterRequiredMaterials)
                    {
                        var mat = rsMaterials.FirstOrDefault(t => t.Id == catmat.MaterialRefId);
                        catmat.MaterialRefName = mat.MaterialName;
                        var unit = rsUnits.FirstOrDefault(t => t.Id == catmat.UnitRefId);
                        catmat.UnitRefName = unit.Name;
                        List<int> arrPurchaseOrderRefIdsforThis = rsQuotePoLinkDtos.Where(t => t.CaterQuoteRequiredMaterialDataRefId == catmat.Id).Select(t => t.PurchaseOrderRefId).ToList();
                        var poDetails = rsPoDetails.Where(t => t.MaterialRefId == catmat.MaterialRefId && arrPurchaseOrderRefIdsforThis.Contains(t.PoRefId)).ToList();
                        decimal totalQtyPlaced = 0.0m;
                        decimal totalQtyReceived = 0.0m;
                        foreach (var poDetail in poDetails)
                        {
                            poDetail.MaterialRefName = mat.MaterialName;
                            if (catmat.UnitRefId == poDetail.UnitRefId)
                            {
                                totalQtyPlaced = totalQtyPlaced + poDetail.QtyOrdered;
                                totalQtyReceived = totalQtyReceived + poDetail.QtyReceived;
                            }
                            else
                            {
                                var unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == catmat.UnitRefId && t.RefUnitId == poDetail.UnitRefId);
                                if (unitConversion == null)
                                    throw new UserFriendlyException(L("UnitConversion") + " " + L("NotExist") + " - " + catmat.MaterialRefName);

                                var conversionFactor = 1 / unitConversion.Conversion;
                                totalQtyPlaced = totalQtyPlaced + (poDetail.QtyOrdered * conversionFactor);
                                totalQtyReceived = totalQtyReceived + (poDetail.QtyReceived * conversionFactor);
                            }
                        }
                        catmat.PoDetails = poDetails.MapTo<List<CaterMaterialPurchaseOrderDetailViewDto>>();
                        if (poDetails.Count == 0)
                        {
                            catmat.StatusRefId = (int)CaterQuotationRequiredMaterialStatus.Po_Not_Generated;
                            catmat.StatusRefName = "PO Not Generated.";
                        }
                        else
                        {
                            catmat.StatusRefId = (int)CaterQuotationRequiredMaterialStatus.Po_Generated;
                            catmat.StatusRefName = "PO Generated.";
                            if (catmat.TotalQty == totalQtyPlaced)
                            {
                                catmat.StatusRefId = (int)CaterQuotationRequiredMaterialStatus.Po_Generated;
                                catmat.StatusRefName = "PO Generated.";
                            }
                            else if (catmat.TotalQty > totalQtyPlaced && catmat.TotalQty > 0)
                            {
                                catmat.StatusRefId = (int)CaterQuotationRequiredMaterialStatus.Po_Partially_Generated;
                                catmat.StatusRefName = "PO Partially Generated, " + Math.Round(catmat.TotalQty - totalQtyPlaced, 2) + " Quantity Due for PO.";
                            }
                            else if (totalQtyPlaced > catmat.TotalQty && catmat.TotalQty > 0)
                            {
                                catmat.StatusRefId = (int)CaterQuotationRequiredMaterialStatus.Po_Partially_Generated;
                                catmat.StatusRefName = "PO Generated, " + Math.Round(totalQtyPlaced, 2) + " Quantity";
                            }

                            if (totalQtyReceived > 0 && totalQtyPlaced > 0 && totalQtyReceived == totalQtyPlaced)
                            {
                                catmat.StatusRefName = catmat.StatusRefName + " " + Math.Round(totalQtyReceived, 2) + " Quantity Received.";
                            }
                            else if (totalQtyReceived > 0)
                            {
                                catmat.StatusRefName = catmat.StatusRefName + " " + Math.Round(totalQtyReceived, 2) + " Quantity Received.";
                            }
                        }
                    }
                    item.CaterRequiredMaterialListDtos = caterRequiredMaterials.OrderByDescending(t => t.PoDetails.Count()).ToList();

                    if (item.DoesPOCreatedForThisCateringOrder)
                    {
                        var poMaster = rsPoMaster.Where(t => poIds.Contains(t.Id)).ToList();
                        var poStatusList = (from mas in poMaster
                                            group mas by new { mas.PoCurrentStatus } into g
                                            select new PoStatusWiseList
                                            {
                                                PoCurrentStatus = g.Key.PoCurrentStatus,
                                                PurchaseOrderList = g.ToList()
                                            }).ToList();
                        item.PoStatusWiseLists = poStatusList;
                        List<PoStatusWithCount> poStatusCountList = new List<PoStatusWithCount>();
                        foreach (var lst in poStatusList)
                        {
                            List<int> arrPoRefIds = lst.PurchaseOrderList.Select(t => t.Id).ToList();
                            var linkedPoDetails = rsPoDetails.Where(t => arrPoRefIds.Contains(t.PoRefId)).Select(t => t.MaterialRefId).Distinct().ToList();
                            item.PoStatusRemarks = item.PoStatusRemarks + lst.PoCurrentStatus + "( " + linkedPoDetails.Count() + " ) ";
                            poStatusCountList.Add(new PoStatusWithCount { PoStatus = lst.PoCurrentStatus, Count = linkedPoDetails.Count() });
                        }
                        var totalStatusCount = poStatusCountList.Sum(t => t.Count);
                        var materialCountPendingForPo = countDetails - totalStatusCount;
                        if (materialCountPendingForPo > 0 || item.PoStatusRemarks.IsNullOrEmpty())
                        {
                            item.PoStatusRemarks = "Pending for PO";
                            poStatusCountList.Add(new PoStatusWithCount { PoStatus = "Pending for PO", Count = materialCountPendingForPo });
                        }
                        item.PoStatusCountList = poStatusCountList;
                        item.CurrentStatus = "Pending";
                    }
                    else
                    {
                        item.CurrentStatus = "Pending";
                        item.PoStatusRemarks = "Pending";
                    }
                }

                if (!input.CurrentStatus.IsNullOrEmpty())
                {
                    //Get the culture property of the thread.
                    CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                    //Create TextInfo object.
                    TextInfo textInfo = cultureInfo.TextInfo;

                    input.CurrentStatus = input.CurrentStatus.ToLowerInvariant();

                    var searchStatus = textInfo.ToTitleCase(input.CurrentStatus);

                    allListDtos = allListDtos.Where(t => !t.CurrentStatus.IsNullOrEmpty() && t.CurrentStatus.Contains(searchStatus)).ToList();
                    //   allListDtos = allListDtos.FindAll(t => t.PoCurrentStatus.Contains(input.PoStatus));
                }
            }
            return new PagedResultOutput<QuotationHeaderListDto>(allItemCount, allListDtos);
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _caterQuotationHeaderRepository.GetAll()
               .Include(e => e.CaterCustomer)
               .Include(e => e.EventAddress)
               .Where(c => c.TenantId == AbpSession.TenantId)
               .ToListAsync();

            var allListDtos = allList.Select(e =>
            {
                var dto = e.MapTo<QuotationHeaderListDto>();
                dto.CaterCustomerCustomerName = e.CaterCustomer.CustomerName;
                return dto;
            }).ToList();

            return _quotationHeaderExporter.ExportToFile(allListDtos);
        }

        public async Task<QuotationHeaderEditDto> GetQuotationHeaderForEdit(NullableIdInput input)
        {
            if (input.Id.HasValue)
            {
                var quotation = await _caterQuotationHeaderRepository.GetAll().Include(e => e.CaterCustomer).FirstOrDefaultAsync(e => e.Id == input.Id.Value);
                var dto = quotation.MapTo<QuotationHeaderEditDto>();
                dto.CaterCustomerName = quotation.CaterCustomer.CustomerName;
                dto.EventTime = quotation.EventDateTime;
                return dto;
            }
            else
            {
                return new QuotationHeaderEditDto();
            }
        }

        public async Task<int> CreateOrUpdateQuotationHeader(QuotationHeaderEditDto input)
        {
            input.EventDateTime = new DateTime(input.EventDateTime.Year, input.EventDateTime.Month, input.EventDateTime.Day, input.EventTime.Hour, input.EventTime.Minute, 0);
            input.TenantId = AbpSession.TenantId;

            if (input.Id.HasValue)
            {
                await UpdateQuotationHeader(input);
                return input.Id.Value;
            }
            else
            {
                return await CreateQuotationHeader(input);
            }
        }

        public async Task DeleteQuotationHeader(NullableIdInput input)
        {
            if (input.Id.HasValue)
                await _caterQuotationHeaderRepository.DeleteAsync(input.Id.Value);
            //Pending for cancel all the PO
        }

        private async Task<int> CreateQuotationHeader(QuotationHeaderEditDto input)
        {
            var quotationHeader = input.MapTo<CaterQuotationHeader>();

            var retId = await _caterQuotationHeaderRepository.InsertAndGetIdAsync(quotationHeader);
            await GetMaterialsRequiredForGivenQuotation(new GetQuotationWiseMaterialRequiredInputDto { QuotationRefId = retId });
            return retId;
        }

        private async Task UpdateQuotationHeader(QuotationHeaderEditDto input)
        {
            var quotationHeader = await _caterQuotationHeaderRepository.GetAsync(input.Id.Value);

            input.MapTo(quotationHeader);

            await _caterQuotationHeaderRepository.UpdateAsync(quotationHeader);
            await GetMaterialsRequiredForGivenQuotation(new GetQuotationWiseMaterialRequiredInputDto { QuotationRefId = input.Id.Value });
        }

        public async Task<List<MenuItemByCategoryDto>> GetMenuItemCombos(int quoteId, string filterText)
        {
            var comboItems = await _menuitemRepository.GetAll()
                .WhereIf(!filterText.IsNullOrEmpty(), e => e.Name.Contains(filterText) || e.Category.Name.Contains(filterText) || e.AliasName.Contains(filterText) || e.Category.Code.Contains(filterText))
                .ToListAsync();

            var combos = comboItems.Select(e =>
            {
                var dto = new MenuItemComboListDto
                {
                    MenuItem = e.MapTo<MenuItemListDto>(),
                };
                var portionIds = e.Portions.Select(p => p.Id);
                var quat = _caterQuotationDetailRepository
                .FirstOrDefault(i => i.HeaderId == quoteId && portionIds.Contains(i.MenuItemPortionId) && i.GroupName == dto.MenuItem.Name)?.Quantity;

                dto.QuoteId = quoteId;
                dto.IsSelected = quat.HasValue;
                dto.IsOrdered = quat.HasValue;
                dto.MenuItem.Quantity = quat.HasValue ? quat.Value : 1;

                var combo = _productComboRepository.FirstOrDefault(a => a.MenuItemId == e.Id);
                if (combo != null)
                {
                    dto.ProductComboGroups = combo.ComboGroups.Select(ci =>
                    {
                        var comboGroup = ci.MapTo<ProductComboGroupEditDto>();
                        var list = comboGroup.ComboItems.Select(i =>
                         {
                             var menuItemPortionId = _menuitemRepository.FirstOrDefault(i.MenuItemId).Portions.FirstOrDefault().Id;

                             var comboExist = _caterQuotationDetailRepository.GetAll()
                             .Where(d => d.MenuItemPortionId == menuItemPortionId && d.HeaderId == quoteId)
                             .Where(d => d.ProductName == ci.Name && d.GroupName == e.Name);

                             i.IsSelected = comboExist.Any();
                             i.IsOrdered = comboExist.Any();
                             i.Count = (int)(comboExist.FirstOrDefault() != null ? comboExist.FirstOrDefault().Quantity : 0);
                             return i;
                         }).ToList();
                        comboGroup.ComboItems = new List<ProductComboItemEditDto>(list);

                        return comboGroup;
                    }).ToList();
                }
                return dto;
            }).ToList();

            var result = combos.GroupBy(e => e.MenuItem.CategoryName)
                .Select(g => new MenuItemByCategoryDto
                {
                    CategoryName = g.Key,
                    MenuItems = g.ToList()
                }).ToList();

            return result;
        }

        public async Task AddOrUpdateQuoteDetails(List<MenuItemComboListDto> input)
        {
            if (!input.Any()) { return; }

            var quotation = await _caterQuotationHeaderRepository.FirstOrDefaultAsync(input.FirstOrDefault().QuoteId.Value);

            var listAdd = new List<CaterQuotationDetail>();
            var listRemove = new List<CaterQuotationDetail>();
            var allQuoDetail = _caterQuotationDetailRepository.GetAll();
            listAdd = AddItemForEdit(input, quotation, false, true);
            listRemove = AddItemForEdit(input, quotation, true, false);
            var updateDetailIds = new List<int>();
            listAdd.ForEach(ent =>
            {
                var comboExist = allQuoDetail
                .Where(e => e.MenuItemPortionId == ent.MenuItemPortionId && e.HeaderId == ent.HeaderId)
                .Where(e => e.ProductName == ent.ProductName && e.GroupName == ent.GroupName);

                if (comboExist.Any())
                {
                    var detail = comboExist.FirstOrDefault();
                    updateDetailIds.Add(detail.Id);

                    detail.Quantity = ent.Quantity;
                    _caterQuotationDetailRepository.InsertOrUpdateAndGetId(detail);
                }
                else

                    _caterQuotationDetailRepository.InsertOrUpdateAndGetId(ent);
            });
            //remove combodetail
            //var resultDelCombo = new List<CaterQuotationDetail>();
            //foreach (var item in input)
            //{
            //    foreach (var product in item.ProductComboGroups)
            //    {
            //        var comboItems = product.ComboItems.Where(s => !s.IsSelected && s.IsRemoved);
            //        foreach (var combo in comboItems)
            //        {
            //            var menuItemPortion = _menuitemRepository.FirstOrDefault(combo.MenuItemId).Portions.FirstOrDefault().Id;
            //            var itemDel = allQuoDetail.FirstOrDefault(s => s.HeaderId == quotation.Id && s.MenuItemPortionId == menuItemPortion && s.ProductName == product.Name);
            //            if (itemDel != null)
            //                _caterQuotationDetailRepository.Delete(itemDel);
            //        }
            //    }
            //}
            listRemove.ForEach(ent =>
            {
                var itemDel = allQuoDetail.FirstOrDefault(s => s.HeaderId == quotation.Id && s.MenuItemPortionId == ent.MenuItemPortionId && s.ProductName == ent.ProductName);
                if (itemDel != null)
                    _caterQuotationDetailRepository.DeleteAsync(itemDel);
            });

            //var removeDetails = await _caterQuotationDetailRepository.GetAll().Where (e => e.HeaderId == quotation.Id && !updateDetailIds.Contains(e.Id)).ToListAsync();
            //removeDetails.ForEach(e => { _caterQuotationDetailRepository.Delete(e); });

            //quotation.TotalAmount = Math.Round( listAdd.Sum(e => e.Price * e.Quantity),2,MidpointRounding.AwayFromZero);
            var quotationDetails = await _caterQuotationDetailRepository.GetAll().Where(t => t.HeaderId == quotation.Id).ToListAsync();

            quotation.TotalAmount = quotationDetails.Sum(t => t.Price * t.Quantity);
            quotation.QuoteTotalAmount = Math.Round(quotation.TotalAmount * quotation.NoOfPax, 2, MidpointRounding.AwayFromZero);

            await _caterQuotationHeaderRepository.UpdateAsync(quotation);
            await GetMaterialsRequiredForGivenQuotation(new GetQuotationWiseMaterialRequiredInputDto { QuotationRefId = quotation.Id });
        }
        public List<CaterQuotationDetail> AddItemForEdit(List<MenuItemComboListDto> input, CaterQuotationHeader quotation, bool isRemoved, bool isSelected)
        {
            var result = new List<CaterQuotationDetail>();
            foreach (var item in input.Where(s => s.IsRemoved == isRemoved && s.IsSelected == isSelected))
            {
                var comboDetail = new CaterQuotationDetail
                {
                    HeaderId = quotation.Id,
                    GroupName = item.MenuItem.Name,
                    MenuItemPortionId = item.MenuItem.Portions.FirstOrDefault().Id.Value,
                    Price = item.MenuItem.Portions.FirstOrDefault().Price,
                    Quantity = item.MenuItem.Quantity,
                    TenantId = AbpSession.TenantId.Value
                };
                if (comboDetail.Quantity == 0)
                {
                    continue;
                }
                result.Add(comboDetail);
                if (item.IsRemoved == true && item.IsSelected == false && item.ProductComboGroups.Count > 0)
                {
                    foreach (var combo in item.ProductComboGroups)
                    {
                        var comboItems = combo.ComboItems.Select(ci => new CaterQuotationDetail
                        {
                            HeaderId = quotation.Id,
                            GroupName = item.MenuItem.Name,
                            ProductName = combo.Name,
                            Quantity = ci.Count,
                            Price = ci.Price,
                            MenuItemPortionId = _menuitemRepository.FirstOrDefault(ci.MenuItemId).Portions.FirstOrDefault().Id
                        });
                        result.AddRange(comboItems);
                    }
                }
                if (item.IsRemoved == false && item.IsSelected == true)
                {
                    item.ProductComboGroups.ForEach(i =>
                    {
                        var comboItems = i.ComboItems.Where(s => s.IsRemoved == isRemoved && s.IsSelected == isSelected).Select(ci => new CaterQuotationDetail
                        {
                            HeaderId = quotation.Id,
                            GroupName = item.MenuItem.Name,
                            ProductName = i.Name,
                            Quantity = ci.Count,
                            Price = ci.Price,
                            MenuItemPortionId = _menuitemRepository.FirstOrDefault(ci.MenuItemId).Portions.FirstOrDefault().Id
                        });
                        result.AddRange(comboItems);
                    });
                }
            }
            return result;
        }
        //public List<CaterQuotationDetail> ProductComboItemForEdit(List<MenuItemComboListDto> input, CaterQuotationHeader quotation, bool isRemoved, bool isSelected)
        //{
        //    var result = new List<CaterQuotationDetail>();
        //    foreach (var item in input)
        //    {
        //        if(item.IsRemoved == false && item.IsSelected == true)
        //        {
        //            item.ProductComboGroups.ForEach(i =>
        //            {
        //                var comboItems = i.ComboItems.Where(s => s.IsRemoved == isRemoved && s.IsSelected == isSelected).Select(ci => new CaterQuotationDetail
        //                {
        //                    HeaderId = quotation.Id,
        //                    GroupName = item.MenuItem.Name,
        //                    ProductName = i.Name,
        //                    Quantity = ci.Count,
        //                    Price = ci.Price,
        //                    MenuItemPortionId = _menuitemRepository.FirstOrDefault(ci.MenuItemId).Portions.FirstOrDefault().Id
        //                });
        //                result.AddRange(comboItems);
        //            });
        //        }              
        //    }

        //    return result;
        //}
        public async Task<List<QuotationDetailListDto>> GetQuotationDetails(int quoteId)
        {
            var details = await _caterQuotationDetailRepository.GetAll().Include(e => e.MenuItemPortion).Where(e => e.HeaderId == quoteId).ToListAsync();

            var result = details.GroupBy(e => e.GroupName).Select(d =>
              {
                  var dto = d.Where(e => e.ProductName.IsNullOrEmpty()).FirstOrDefault().MapTo<QuotationDetailListDto>();
                  dto.MenuItemName = d.Where(e => e.ProductName.IsNullOrEmpty()).FirstOrDefault()?.MenuItemPortion?.MenuItem?.Name;
                  dto.AliasCode = d.Where(e => e.ProductName.IsNullOrEmpty()).FirstOrDefault()?.MenuItemPortion?.MenuItem?.AliasCode;

                  dto.SubDetails = d.Where(e => !e.ProductName.IsNullOrEmpty()).Select(e =>
                  {
                      var dt = e.MapTo<QuotationDetailListDto>();
                      dt.MenuItemName = e?.MenuItemPortion?.MenuItem?.Name;
                      dt.AliasCode = e?.MenuItemPortion?.MenuItem?.AliasCode;
                      return dt;
                  }).ToList();
                  return dto;
              }).ToList();

            return result;
        }

        public async Task<CateringMaterialConsolidatedProjectionDto> GetMaterialsRequiredForGivenQuotation(GetQuotationWiseMaterialRequiredInputDto input)
        {
            List<int> arrQuotationRefIds = new List<int>();
            if (input.QuotationRefId.HasValue)
            {
                arrQuotationRefIds.Add(input.QuotationRefId.Value);
            }
            if (input.QuotationRefIdList != null && input.QuotationRefIdList.Count > 0)
            {
                arrQuotationRefIds.AddRange(input.QuotationRefIdList);
            }
            if (input.CaterQuotations != null && input.CaterQuotations.Count > 0)
            {
                arrQuotationRefIds.AddRange(input.CaterQuotations.Select(t => t.Id).ToList());
            }

            var rsMaterials = await _materialRepo.GetAll().Select(t => new { t.Id, t.MaterialGroupCategoryRefId, t.MaterialName, t.MaterialPetName, t.DefaultUnitId }).ToListAsync();
            //var rsMaterials = await _materialRepo.GetAllListAsync();
            var rsMaterialMappings = await _materialmenumappingRepo.GetAllListAsync();
            var rsMenumappingDepartment = await _menumappingDepartmentRepo.GetAllListAsync();
            var rsDeptList = await _departmentRepo.GetAll().Select(t => new { t.Id, t.Name }).ToListAsync();
            if (rsDeptList.Count == 0)
            {
                throw new UserFriendlyException("Atleast one Department required for process costing");
            }
            var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();
            var rsUnits = await _unitRepo.GetAllListAsync();

            var fromDt = DateTime.Today.AddDays(-1);
            var toDt = DateTime.Today;

            List<int> arrmaterialRefIds = new List<int>();
            List<QuotationWiseCosting> output = new List<QuotationWiseCosting>();
            List<QuotationDetailListDto> overAllQuotationDetails = new List<QuotationDetailListDto>();
            List<CateringMaterialMenuMapping> overAllMenuVsMaterialAllDetails = new List<CateringMaterialMenuMapping>();
            List<QuoteRequiredMaterialWithCostDto> overAllQuoteRequiredMaterialWithCostDtoList = new List<QuoteRequiredMaterialWithCostDto>();

            foreach (var quoteId in arrQuotationRefIds)
            {
                QuotationWiseCosting newDto = new QuotationWiseCosting();
                var quote = await this.GetQuotationHeaderForEdit(new NullableIdInput { Id = quoteId });
                newDto.QuotationInfo = quote;
                if (input.LocationRefId.HasValue == false)
                {
                    input.LocationRefId = quote.LocationRefId;
                }
                var quoteDetails = await this.GetQuotationDetails(quoteId);
                newDto.QuotationDetails = quoteDetails.MapTo<List<QuotationDetailListDto>>();
                overAllQuotationDetails.AddRange(newDto.QuotationDetails);

                var mapwithMenuList = new List<CateringMaterialMenuMapping>();
                foreach (var item in newDto.QuotationDetails)
                {
                    var menuMapping = rsMaterialMappings.Where(t => t.PosMenuPortionRefId == item.MenuItemPortionId
                                                /*&& t.LocationRefId == input.LocationRefId*/).ToList();
                    if (menuMapping == null || menuMapping.Count == 0)
                    {
                        menuMapping = rsMaterialMappings.Where(t => t.PosMenuPortionRefId == item.MenuItemPortionId
                                                && t.LocationRefId == null).ToList();
                    }

                    //var department = rsDeptList.FirstOrDefault(t => t.Name.ToUpper().Equals(item.DepartmentName.ToUpper()));

                    //if (department == null)
                    //{
                    //    throw new UserFriendlyException(L("Department") + " " + L("NotExist"), item.DepartmentName);
                    //}

                    //var deptId = department.Id;
                    var deptId = rsDeptList.FirstOrDefault().Id;
                    var menuMappingRefIds = menuMapping.Select(t => t.Id).ToArray();

                    var deptList =
                        rsMenumappingDepartment.Where(t => menuMappingRefIds.Contains(t.MenuMappingRefId)).ToList();

                    bool AllDepartmentFlag;
                    if (deptList.Count() == 0)
                    {
                        AllDepartmentFlag = true;
                    }
                    else
                    {
                        AllDepartmentFlag = false;
                    }

                    foreach (var MmMaterial in menuMapping)
                    {
                        if (AllDepartmentFlag == false)
                        {
                            var deptLinkedMM = deptList.Where(t => t.MenuMappingRefId == MmMaterial.Id);
                            if (deptLinkedMM.Count() > 0)
                            {
                                deptLinkedMM = deptLinkedMM.Where(t => t.DepartmentRefId == deptId);
                                if (deptLinkedMM.Count() == 0)
                                {
                                    continue;
                                }
                            }
                        }

                        var newmm = new CateringMaterialMenuMapping();
                        var mat = rsMaterials.FirstOrDefault(t => t.Id == MmMaterial.MaterialRefId);

                        if (mat == null)
                        {
                            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                            {
                                var deletedMaterial = await _materialRepo.FirstOrDefaultAsync(t => t.Id == MmMaterial.MaterialRefId);
                                var deletedMaterialName = deletedMaterial == null ? "" : deletedMaterial.MaterialName;
                                throw new UserFriendlyException(L("SomeOfTheLinkedMaterialsInMenuMappingDeleted") + " " + L("MATERIALNAMEISWRONG", deletedMaterialName));
                            }
                            throw new UserFriendlyException(L("SomeOfTheLinkedMaterialsInMenuMappingDeleted"));
                        }

                        newmm.PosMenuPortionRefId = MmMaterial.PosMenuPortionRefId;

                        var menu = newDto.QuotationDetails.FirstOrDefault(t => t.MenuItemPortionId == MmMaterial.PosMenuPortionRefId);
                        if (menu != null)
                            newmm.PosRefName = menu.MenuItemName;

                        newmm.MenuQuantitySold = MmMaterial.MenuQuantitySold;
                        newmm.MaterialRefId = MmMaterial.MaterialRefId;
                        newmm.TotalSaleQuantity = Math.Round(item.Quantity * quote.NoOfPax, 6);
                        newmm.PortionPerUnitOfSales = Math.Round(MmMaterial.PortionQty / MmMaterial.MenuQuantitySold, 14);
                        newmm.PortionQty = Math.Round(MmMaterial.PortionQty / MmMaterial.MenuQuantitySold * newmm.TotalSaleQuantity, 14);
                        newmm.PortionUnitId = MmMaterial.PortionUnitId;
                        var portionUnit = rsUnits.FirstOrDefault(t => t.Id == MmMaterial.PortionUnitId);
                        newmm.UnitRefName = portionUnit.Name;
                        newmm.NoOfPax = quote.NoOfPax;
                        newmm.AutoSalesDeduction = MmMaterial.AutoSalesDeduction;
                        newmm.DefaultUnitId = mat.DefaultUnitId;
                        var defUnit = rsUnits.FirstOrDefault(t => t.Id == mat.DefaultUnitId);
                        newmm.DefaultUnitName = defUnit.Name;
                        mapwithMenuList.Add(newmm);
                    }
                }

                newDto.MenuVsMaterialAllDetails = mapwithMenuList;
                overAllMenuVsMaterialAllDetails.AddRange(mapwithMenuList);

                var mapwithMenuListGroupWithUC = (from salemenu in mapwithMenuList
                                                  join uc in rsUc
                                                      on salemenu.PortionUnitId equals uc.BaseUnitId
                                                  where uc.RefUnitId == salemenu.DefaultUnitId
                                                  group salemenu by new
                                                  {
                                                      salemenu.MaterialRefId,
                                                      salemenu.MaterialRefName,
                                                      salemenu.DefaultUnitId,
                                                      salemenu.PortionUnitId,
                                                      salemenu.UnitRefName,
                                                      salemenu.DefaultUnitName,
                                                      uc.Conversion
                                                  }
                                                  into g
                                                  select new ConsolidatedMaterialRequiredWithLinkedMenuDto
                                                  {
                                                      MaterialRefId = g.Key.MaterialRefId,
                                                      MaterialRefName = g.Key.MaterialRefName,
                                                      PortionUnitId = g.Key.PortionUnitId,
                                                      UnitRefName = g.Key.UnitRefName,
                                                      DefaultUnitId = g.Key.DefaultUnitId,
                                                      DefaultUnitName = g.Key.DefaultUnitName,
                                                      PortionQty = Math.Round(g.Sum(t => t.PortionQty * g.Key.Conversion), 14),
                                                      SubDetails = g.ToList()
                                                  }).ToList().OrderBy(t => t.MaterialRefId);

                var mapWithMenuListSumGroup = (from salemenu in mapwithMenuListGroupWithUC
                                               group salemenu by new { salemenu.MaterialRefId, salemenu.MaterialRefName }
                                           into g
                                               select new QuoteRequiredMaterialWithCostDto
                                               {
                                                   MaterialRefId = g.Key.MaterialRefId,
                                                   MaterialRefName = g.Key.MaterialRefName,
                                                   TotalQty = g.Sum(t => t.PortionQty),
                                                   ConsolidatedSubDetails = g.ToList()
                                               }).ToList();
                newDto.QuoteRequiredMaterialWithCostDtoList = mapWithMenuListSumGroup;
                overAllQuoteRequiredMaterialWithCostDtoList.AddRange(mapWithMenuListSumGroup);
                arrmaterialRefIds.AddRange(mapWithMenuListSumGroup.Select(t => t.MaterialRefId).ToList());
                output.Add(newDto);
            }

            DateTime rateRequiredDate = DateTime.Today.AddDays(-7);
            var matRateViewDto = await _housereportAppService.GetMaterialRateView(new GetHouseReportMaterialRateInput
            {
                StartDate = rateRequiredDate,
                EndDate = DateTime.Today,
                LocationRefId = input.LocationRefId,
                MaterialRefIds = arrmaterialRefIds,
                FunctionCalledBy = "Quote Material"
            });

            int[] matListtoVerify = { 81/*, 82, 130, 132*/ };

            List<int> rateZeroMaterialRefIds = new List<int>();
            string overAllRemarks = "";

            #region Setting Prices

            foreach (var quote in output)
            {
                foreach (var detail in quote.QuoteRequiredMaterialWithCostDtoList)
                {
                    var matrate = matRateViewDto.MaterialRateView.FirstOrDefault(t => t.MaterialRefId == detail.MaterialRefId);

                    if (matListtoVerify.Contains(detail.MaterialRefId))
                    {
                        int adf = 1;
                        adf++;
                    }
                    if (matrate == null)
                    {
                        overAllRemarks = overAllRemarks + "Material Price Data Missing for " + detail.MaterialRefId + "-" + detail.MaterialRefName + " <BR>";
                    }

                    if (matrate.AvgRate == 0)
                    {
                        detail.Remarks = "Rate Zero";
                    }
                    detail.MaterialRefName = matrate.MaterialName;
                    detail.UnitRefId = matrate.UnitRefId;
                    detail.Uom = matrate.Uom;
                    detail.Price = matrate.AvgRate;
                    detail.TotalCost = Math.Round(detail.Price * detail.TotalQty);

                    var mappedMenuList = quote.MenuVsMaterialAllDetails.Where(t => t.MaterialRefId == detail.MaterialRefId).ToList();
                    detail.MenuMappedSubList = mappedMenuList;
                }
                decimal totalMaterialCostRequired = quote.QuoteRequiredMaterialWithCostDtoList.Sum(t => t.TotalCost);
                if (totalMaterialCostRequired > 0)
                {
                    foreach (var detail in quote.QuoteRequiredMaterialWithCostDtoList)
                    {
                        detail.CostPercentage = Math.Round(detail.TotalCost / totalMaterialCostRequired * 100, 2, MidpointRounding.AwayFromZero);
                    }
                }
                quote.TotalMaterialCostRequired = totalMaterialCostRequired;
                quote.TotalCateringBillValue = quote.QuotationInfo.TotalAmount * quote.QuotationInfo.NoOfPax;
                await AppendQuoteMaterialRequiredData(quote);
            }

            #endregion Setting Prices

            CateringMaterialConsolidatedProjectionDto resultOutput = new CateringMaterialConsolidatedProjectionDto();
            resultOutput.QuotationWiseCostingList = output;

            QuotationWiseCosting overAllCateringMaterialRequired = new QuotationWiseCosting { OverAllFlag = true };
            overAllCateringMaterialRequired.TotalCateringBillValue = output.Sum(t => t.TotalCateringBillValue);
            overAllCateringMaterialRequired.TotalMaterialCostRequired = output.Sum(t => t.TotalMaterialCostRequired);
            if (output.Count == 1)
            {
                overAllCateringMaterialRequired = output.FirstOrDefault();
            }
            else
            {
                overAllCateringMaterialRequired.TotalCateringBillValue = output.Sum(t => t.TotalCateringBillValue);
                //overAllCateringMaterialRequired.QuotationInfo.CaterCustomerName = "Over All";
            }
            resultOutput.OverAllCosting = overAllCateringMaterialRequired;
            resultOutput.RateRemarks = overAllRemarks;
            return resultOutput;
        }

        public async Task AppendQuoteMaterialRequiredData(QuotationWiseCosting quote)
        {
            var existingRecords = await _caterQuoteRequiredMaterialRepo.GetAllListAsync(t => t.CaterQuoteHeaderId == quote.QuotationInfo.Id.Value);
            var existingMenuRecords = await _caterQuoteRequiredMaterialVsMenuLinkRepo.GetAllListAsync(t => t.CaterQuoteHeaderId == quote.QuotationInfo.Id.Value);

            #region Quote Required Material

            foreach (var detail in quote.QuoteRequiredMaterialWithCostDtoList)
            {
                var exist = existingRecords.FirstOrDefault(t => t.MaterialRefId == detail.MaterialRefId);
                int? caterQuoteRequiredMaterialDataRefId = null;
                if (exist != null)
                {
                    if (exist.TotalQty == detail.TotalQty && exist.UnitRefId == detail.UnitRefId)
                        continue;
                    else
                    {
                        exist.TotalQty = detail.TotalQty;
                        exist.UnitRefId = detail.UnitRefId;
                        if (exist.TotalQty < detail.TotalQty)
                            exist.StatusRefId = (int)CaterQuotationRequiredMaterialStatus.Quantity_Reduced;
                        else
                            exist.StatusRefId = (int)CaterQuotationRequiredMaterialStatus.Quantity_Increased;
                    }
                    caterQuoteRequiredMaterialDataRefId = await _caterQuoteRequiredMaterialRepo.InsertOrUpdateAndGetIdAsync(exist);
                }
                else
                {
                    exist = new CaterQuoteRequiredMaterial
                    {
                        CaterQuoteHeaderId = quote.QuotationInfo.Id.Value,
                        MaterialRefId = detail.MaterialRefId,
                        TotalQty = detail.TotalQty,
                        UnitRefId = detail.UnitRefId,
                        StatusRefId = (int)CaterQuotationRequiredMaterialStatus.Po_Not_Generated
                    };
                    caterQuoteRequiredMaterialDataRefId = await _caterQuoteRequiredMaterialRepo.InsertOrUpdateAndGetIdAsync(exist);
                }

                //var mappedMenuList = quote.MenuVsMaterialAllDetails.Where(t => t.MaterialRefId == detail.MaterialRefId).ToList();
                //detail.MenuMappedSubList = mappedMenuList;
                foreach (var menuMaterial in detail.MenuMappedSubList.Where(t => t.MaterialRefId == detail.MaterialRefId))
                {
                    var existMenu = existingMenuRecords.FirstOrDefault(t => t.PosMenuPortionRefId == menuMaterial.PosMenuPortionRefId && t.MaterialRefId == menuMaterial.MaterialRefId);
                    if (existMenu != null)
                    {
                        if (existMenu.PortionQty == menuMaterial.PortionQty && existMenu.PortionUnitId == menuMaterial.PortionUnitId && existMenu.NoOfPax == menuMaterial.NoOfPax && existMenu.MenuQuantitySold == menuMaterial.MenuQuantitySold && existMenu.TotalSaleQuantity == menuMaterial.TotalSaleQuantity)
                            continue;
                        else
                        {
                            existMenu.MenuQuantitySold = menuMaterial.MenuQuantitySold;
                            existMenu.PortionQty = menuMaterial.PortionQty;
                            existMenu.PortionUnitId = menuMaterial.PortionUnitId;
                            existMenu.NoOfPax = menuMaterial.NoOfPax;
                            existMenu.TotalSaleQuantity = menuMaterial.TotalSaleQuantity;
                        }
                        var menumapRefId = await _caterQuoteRequiredMaterialVsMenuLinkRepo.InsertOrUpdateAndGetIdAsync(existMenu);
                    }
                    else
                    {
                        existMenu = new CaterQuoteRequiredMaterialVsMenuLink
                        {
                            CaterQuoteHeaderId = quote.QuotationInfo.Id.Value,
                            CaterQuoteRequiredMaterialDataRefId = caterQuoteRequiredMaterialDataRefId.Value,
                            PosMenuPortionRefId = menuMaterial.PosMenuPortionRefId,
                            MenuQuantitySold = menuMaterial.MenuQuantitySold,
                            MaterialRefId = menuMaterial.MaterialRefId,
                            PortionPerUnitOfSales = menuMaterial.PortionPerUnitOfSales,
                            PortionQty = menuMaterial.PortionQty,
                            PortionUnitId = menuMaterial.PortionUnitId,
                            TotalSaleQuantity = menuMaterial.TotalSaleQuantity
                        };
                        var menumapRefId = await _caterQuoteRequiredMaterialVsMenuLinkRepo.InsertOrUpdateAndGetIdAsync(existMenu);
                    }
                }
            }

            #endregion Quote Required Material
        }

        public async Task<List<QuotationHeaderListDto>> GetConsolidatedRequestPendingList(GetQuotationHeaderInput input)
        {
            input.SkipCount = 0;
            input.MaxResultCount = AppConsts.MaxPageSize;
            input.Sorting = "Id";
            input.OmitAlreadyPoLinkedRequest = true;
            var result = await GetQuotationHeader(input);

            var allRequestListDtos = result.Items.MapTo<List<QuotationHeaderListDto>>();
            return allRequestListDtos;
        }

        public async Task<List<CaterRequestMaterialPOGenerationDto>> GetPoRequestBasedOnQuoteSelection(GetQuotationHeaderInput input)
        {
            if (input.DefaultLocationRefId == 0)
            {
                throw new UserFriendlyException("Loation Reference Not Exists");
            }

            List<QuotationHeaderListDto> resultFromPendingList = new List<QuotationHeaderListDto>();
            List<CaterRequestMaterialConsolidatedViewDto> dtos = new List<CaterRequestMaterialConsolidatedViewDto>();
            if (input.RequestBasedOnRequestList == true)
            {
                resultFromPendingList = input.RequestList;
                dtos = await GetConsolidatedPendingBasedGivenCaterList(new GetQuotationHeaderInput
                {
                    RequestList = resultFromPendingList
                });
            }
            else
            {
                input.SkipCount = 0;
                input.MaxResultCount = AppConsts.MaxPageSize;
                resultFromPendingList = await GetConsolidatedRequestPendingList(input);
                dtos = await GetConsolidatedPendingBasedGivenCaterList(new GetQuotationHeaderInput
                {
                    RequestList = resultFromPendingList
                });
            }
            List<CaterRequestMaterialPOGenerationDto> output = new List<CaterRequestMaterialPOGenerationDto>();

            List<int> materialList = dtos.Select(t => t.MaterialRefId).Distinct().ToList();

            var rsSupplierMaterial = await _supplierMaterialAppService.GetSupplierWithPriceBasedOnMaterial(
                new GetSupplierWithLocationAndMaterialInput
                {
                    LocationRefId = input.DefaultLocationRefId,
                    MaterialRefIdList = materialList
                });

            List<int> locationList = dtos.Select(t => t.LocationRefId).Distinct().ToList();

            var dt = await _materiallocationwisestockAppService.GetView(new GetMaterialLocationWiseStockInput
            {
                LocationRefId = input.DefaultLocationRefId,
                LiveStock = true,
                MaxResultCount = AppConsts.MaxPageSize,
                MaterialRefIds = materialList
            });

            List<MaterialLocationWiseStockViewDto> rsMaterialLocationStock = dt.Items.MapTo<List<MaterialLocationWiseStockViewDto>>();

            //List<int> arrHeaderIds = resultFromPendingList.Select(t => t.Id).ToList();
            //var tempQuoteRequestMaterials = await _caterQuoteRequiredMaterialRepo.GetAllListAsync(t => arrHeaderIds.Contains(t.CaterQuoteHeaderId));
            //var rsDetails = tempQuoteRequestMaterials.MapTo<List<CaterQuoteRequiredMaterialListDto>>();
            //List<int> arrMaterialRefIds = rsDetails.Select(t => t.MaterialRefId).ToList();
            //var tempMaterials = await _materialRepo.GetAllListAsync(t => arrMaterialRefIds.Contains(t.Id));
            //var rsMaterials = tempMaterials.MapTo<List<MaterialViewDto>>();

            //List<int> arrCaterQuoteRequiredMaterialDataRefId = rsDetails.Select(t => t.Id).ToList();
            //var tempQuoteRequestMaterialsVsPo = await _caterQuoteRequiredMaterialVsPoLinkRepo.GetAllListAsync(t => arrCaterQuoteRequiredMaterialDataRefId.Contains(t.CaterQuoteRequiredMaterialDataRefId));
            //var rsQuotePoLinkDtos = tempQuoteRequestMaterialsVsPo.MapTo<List<CaterQuoteRequiredMaterialVsPurchaseOrderLinkListDto>>();

            //var arrPoIds = tempQuoteRequestMaterialsVsPo.Where(t => t.PurchaseOrderRefId > 0).Select(t => t.PurchaseOrderRefId).ToList();
            //var tempPoMaster = await _purchaseOrderRepo.GetAllListAsync(t => arrPoIds.Contains(t.Id));
            //var rsPoMaster = tempPoMaster.MapTo<List<PurchaseOrderListDto>>();
            //var arrSupplierRefIds = rsPoMaster.Select(t => t.SupplierRefId).ToList();
            //var rsSuppliers = await _supplierRepo.GetAllListAsync(t => arrSupplierRefIds.Contains(t.Id));

            //var tempPoDetail = await _purchaseOrderDetailRepo.GetAllListAsync(t => arrPoIds.Contains(t.PoRefId));
            //var rsPoDetails = tempPoDetail.MapTo<List<PurchaseOrderDetailViewDto>>();
            //var rsUnits = await _unitRepo.GetAllListAsync();
            //var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();

            foreach (var matid in materialList)
            {
                var material = dtos.FirstOrDefault(t => t.MaterialRefId == matid);

                #region Select Supplier Based On Supplier Materials

                List<SupplierMaterialViewDto> supplierViewDto = new List<SupplierMaterialViewDto>();
                SupplierMaterialViewDto defaultSupplier = new SupplierMaterialViewDto();
                var tempSupplierMaterial = rsSupplierMaterial.Where(t => t.MaterialRefId == material.MaterialRefId).ToList();
                List<int> arrSupplierRefids = tempSupplierMaterial.Select(t => t.SupplierRefId).ToList();

                string supplierRemarks = "";
                decimal alreadyOrderedQuantity = 0;
                if (tempSupplierMaterial == null || tempSupplierMaterial.Count == 0)
                {
                    supplierRemarks = L("NoMoreSupplierLinkedWithThisMaterial");
                }
                else
                {
                    supplierViewDto = tempSupplierMaterial.MapTo<List<SupplierMaterialViewDto>>();
                    defaultSupplier = supplierViewDto.OrderBy(t => t.MaterialPrice).ToList().FirstOrDefault();
                    alreadyOrderedQuantity = defaultSupplier.AlreadyOrderedQuantity;
                }

                #endregion Select Supplier Based On Supplier Materials

                var requestMaterialList = dtos.Where(t => t.MaterialRefId == matid).ToList();
                decimal totalRequiredQty = requestMaterialList.Sum(t => t.RequiredQty);
                decimal totalRequestQty = requestMaterialList.Sum(t => t.RequestQty);

                string remarks = "";
                string formula = "";
                decimal onHandAfterIssue;
                onHandAfterIssue = material.LiveStock - totalRequestQty;
                formula = " Balance Qty ";
                decimal poOrderedQuantity = 0;
                if (onHandAfterIssue <= 0)
                {
                    if (material.MaterialTypeId == 1)
                        remarks = remarks + L("NeedToRaisePurchaseOrder");
                    else
                        remarks = L("NeedToRaisePurchaseOrder") + " Or " + L("ProductionNeeded");
                    poOrderedQuantity = Math.Abs(onHandAfterIssue);
                    if (material.ReorderLevel > 0)
                    {
                        poOrderedQuantity = poOrderedQuantity + (material.ReorderLevel);
                        formula = formula + " + ROL";
                    }
                }
                else
                {
                    if (material.ReorderLevel > onHandAfterIssue)
                    {
                        remarks = L("LessThanReorderLevel");
                        poOrderedQuantity = (material.ReorderLevel - onHandAfterIssue);
                        formula = "ROL - " + formula;
                    }
                }
                poOrderedQuantity = Math.Ceiling(poOrderedQuantity);
                if (poOrderedQuantity > 0)
                    formula = "Round(" + formula + ")";
                else
                    formula = "";

                CaterRequestMaterialPOGenerationDto det = new CaterRequestMaterialPOGenerationDto
                {
                    MaterialRefId = material.MaterialRefId,
                    MaterialPetName = material.MaterialPetName,
                    MaterialRefName = material.MaterialRefName,
                    MaterialTypeId = material.MaterialTypeId,
                    MaterialTypeName = material.MaterialTypeName,
                    MinimumStock = material.MinimumStock,
                    MaximumStock = material.MaximumStock,
                    ReOrderLevel = material.ReorderLevel,
                    LiveStock = material.LiveStock,
                    UnitRefId = material.UnitRefId,
                    Uom = material.Uom,
                    TotalRequiredQty = totalRequiredQty,
                    TotalRequestQty = totalRequestQty,
                    LocationWiseRequestDetails = requestMaterialList,
                    PoOrderQuantity = poOrderedQuantity,
                    PoOrderQuantityRecommended = poOrderedQuantity,
                    OnHandAfterIssue = onHandAfterIssue,
                    Remarks = remarks,
                    FormulaForRecommended = formula,
                    AlreadyOrderedQuantity = alreadyOrderedQuantity,
                    SupplierMaterialViewDtos = supplierViewDto,
                    SupplierRemarks = supplierRemarks,
                    DefaultSupplier = defaultSupplier,
                    CaterHeaderRefIds = material.CaterHeaderRefIds,
                    CaterHeaders = material.CaterHeaders,
                    SubDetails = material.SubDetails,
                    StatusRefId = material.StatusRefId,
                    StatusRefName = material.StatusRefName,
                    PoDetails = material.PoDetails
                };
                output.Add(det);
            }
            output = output.OrderByDescending(t => t.PoOrderQuantityRecommended).ToList();
            return output;
        }

        public async Task<List<CaterRequestMaterialConsolidatedViewDto>> GetConsolidatedPendingBasedGivenCaterList(GetQuotationHeaderInput input)
        {
            var dtos = input.RequestList;
            int requestLocationId = 0;
            if (dtos.Count() > 0)
                requestLocationId = dtos[0].LocationRefId;

            List<CaterRequestMaterialDetailViewDto> editDetailDto;
            List<CaterHeaderRefIds> overAllCaterHeaderRefIds = new List<CaterHeaderRefIds>();

            List<int> allRequestList = new List<int>();
            foreach (var a in dtos)
            {
                if (a.CurrentStatus.Equals(L("Pending")) || a.CurrentStatus.Equals(L("Drafted")))
                {
                    allRequestList.Add(a.Id);
                    overAllCaterHeaderRefIds.Add(new CaterHeaderRefIds { CaterHeaderRefId = a.Id });
                }
            }

            List<int> CaterHeaderRefIdList = overAllCaterHeaderRefIds.Select(t => t.CaterHeaderRefId).ToList();
            var getCaterHeaders = await GetQuotationHeader(new GetQuotationHeaderInput { MaxResultCount = AppConsts.MaxPageSize, SkipCount = 0, Sorting = "Id", CaterHeaderRefIdList = CaterHeaderRefIdList });

            List<QuotationHeaderListDto> overAllCaterQuotationHeaders = getCaterHeaders.Items.MapTo<List<QuotationHeaderListDto>>();

            editDetailDto = await (from requestmaster in _caterQuotationHeaderRepository.GetAll().Where(a => allRequestList.Contains(a.Id))
                                   join request in _caterQuoteRequiredMaterialRepo.GetAll().Where(a => allRequestList.Contains(a.CaterQuoteHeaderId)) on requestmaster.Id equals request.CaterQuoteHeaderId
                                   join mat in _materialRepo.GetAll()
                                   on request.MaterialRefId equals mat.Id
                                   join un in _unitRepo.GetAll()
                                   on mat.DefaultUnitId equals un.Id
                                   join uiss in _unitRepo.GetAll()
                                   on request.UnitRefId equals uiss.Id
                                   join matloc in _materiallocationwisestockRepo.
                                         GetAll().Where(mls => mls.LocationRefId == requestLocationId)
                                   on mat.Id equals matloc.MaterialRefId
                                   join loc in _locationRepo.GetAll()
                                   on requestmaster.LocationRefId equals loc.Id
                                   select new CaterRequestMaterialDetailViewDto
                                   {
                                       LocationRefId = requestmaster.LocationRefId,
                                       LocationRefName = loc.Name,
                                       RequestQty = request.TotalQty,
                                       MaterialRefId = request.MaterialRefId,
                                       MaterialPetName = mat.MaterialPetName,
                                       MaterialRefName = mat.MaterialName,
                                       StatusRefId = request.StatusRefId,
                                       //CurrentStatus = request.CurrentStatus,
                                       CaterQuoteHeaderId = request.CaterQuoteHeaderId,
                                       IssueQty = 0,
                                       //Sno = request.Sno,
                                       MaterialTypeId = mat.MaterialTypeId,
                                       Uom = un.Name,
                                       MinimumStock = matloc.MinimumStock,
                                       MaximumStock = matloc.MaximumStock,
                                       ReorderLevel = matloc.ReOrderLevel,
                                       UnitRefId = request.UnitRefId,
                                       UnitRefName = uiss.Name,
                                       DefaultUnitId = mat.DefaultUnitId,
                                       DefaultUnitName = un.Name
                                   }).ToListAsync();

            foreach (var lst in editDetailDto)
            {
                var caterHeader = overAllCaterQuotationHeaders.FirstOrDefault(t => t.Id == lst.CaterQuoteHeaderId);
                lst.CaterHeader = caterHeader;
            }

            List<string> LocationList = editDetailDto.Select(t => t.LocationRefName).Distinct().ToList();

            List<int> materialList = editDetailDto.Select(t => t.MaterialRefId).Distinct().ToList();

            List<CaterRequestMaterialConsolidatedViewDto> output = new List<CaterRequestMaterialConsolidatedViewDto>();

            var matCurrentStock = await _materiallocationwisestockAppService.GetView(new GetMaterialLocationWiseStockInput
            {
                LocationRefId = requestLocationId,
                LiveStock = true,
                MaterialRefIds = materialList,
                MaxResultCount = AppConsts.MaxPageSize
            });
            List<MaterialLocationWiseStockViewDto> materialLocationStock = matCurrentStock.Items.MapTo<List<MaterialLocationWiseStockViewDto>>();

            List<int> arrHeaderIds = dtos.Select(t => t.Id).ToList();
            var tempQuoteRequestMaterials = await _caterQuoteRequiredMaterialRepo.GetAllListAsync(t => arrHeaderIds.Contains(t.CaterQuoteHeaderId));
            var rsDetails = tempQuoteRequestMaterials.MapTo<List<CaterQuoteRequiredMaterialListDto>>();
            List<int> arrMaterialRefIds = rsDetails.Select(t => t.MaterialRefId).ToList();
            var tempMaterials = await _materialRepo.GetAllListAsync(t => arrMaterialRefIds.Contains(t.Id));
            var rsMaterials = tempMaterials.MapTo<List<MaterialViewDto>>();
            var rsUnits = await _unitRepo.GetAllListAsync();
            var rsUc = await _unitConversionAppService.GetAllUnitConversionWithBaseIds();

            List<int> arrCaterQuoteRequiredMaterialDataRefId = rsDetails.Select(t => t.Id).ToList();
            var tempQuoteRequestMaterialsVsPo = await _caterQuoteRequiredMaterialVsPoLinkRepo.GetAllListAsync(t => arrCaterQuoteRequiredMaterialDataRefId.Contains(t.CaterQuoteRequiredMaterialDataRefId));
            var rsQuotePoLinkDtos = tempQuoteRequestMaterialsVsPo.MapTo<List<CaterQuoteRequiredMaterialVsPurchaseOrderLinkListDto>>();

            var arrPoIds = tempQuoteRequestMaterialsVsPo.Where(t => t.PurchaseOrderRefId > 0).Select(t => t.PurchaseOrderRefId).ToList();
            var tempPoMaster = await _purchaseOrderRepo.GetAllListAsync(t => arrPoIds.Contains(t.Id));
            var rsPoMaster = tempPoMaster.MapTo<List<PurchaseOrderListDto>>();
            var arrSupplierRefIds = rsPoMaster.Select(t => t.SupplierRefId).ToList();
            var rsSuppliers = await _supplierRepo.GetAllListAsync(t => arrSupplierRefIds.Contains(t.Id));

            var tempPoDetail = await _purchaseOrderDetailRepo.GetAllListAsync(t => arrPoIds.Contains(t.PoRefId));
            var rsPoDetails = tempPoDetail.MapTo<List<PurchaseOrderDetailViewDto>>();

            foreach (var group in editDetailDto.GroupBy(t => t.LocationRefId))
            {
                foreach (var subgroup in group.GroupBy(t => new { t.MaterialRefId, t.UnitRefId }))
                {
                    List<int> tempArrCaterHeaderRefIds = subgroup.ToList().Select(t => t.CaterQuoteHeaderId).ToList();
                    var caterRequiredMaterials = rsDetails.Where(t => t.MaterialRefId == subgroup.Key.MaterialRefId && tempArrCaterHeaderRefIds.Contains(t.CaterQuoteHeaderId)).ToList();

                    List<int> tempArrCateringMaterialDataRefIds = caterRequiredMaterials.Select(t => t.Id).ToList();

                    List<int> arrPurchaseOrderRefIdsforThis = rsQuotePoLinkDtos.Where(t => tempArrCateringMaterialDataRefIds.Contains(t.CaterQuoteRequiredMaterialDataRefId)).Select(t => t.PurchaseOrderRefId).ToList();

                    var poDetails = rsPoDetails.Where(t => t.MaterialRefId == subgroup.Key.MaterialRefId && arrPurchaseOrderRefIdsforThis.Contains(t.PoRefId)).ToList();
                    var mat = rsMaterials.FirstOrDefault(t => t.Id == subgroup.Key.MaterialRefId);

                    var catmat = subgroup.FirstOrDefault(t => t.MaterialRefId == subgroup.Key.MaterialRefId);
                    if (catmat != null)
                    {
                        var matStock = materialLocationStock.FirstOrDefault(t => t.MaterialRefId == subgroup.Key.MaterialRefId);

                        CaterRequestMaterialConsolidatedViewDto dto = new CaterRequestMaterialConsolidatedViewDto();
                        dto.MaterialRefId = subgroup.Key.MaterialRefId;
                        dto.MaterialRefName = catmat.MaterialRefName;
                        dto.MaterialPetName = catmat.MaterialPetName;
                        dto.LocationRefId = group.Key;
                        dto.LocationRefName = catmat.LocationRefName;
                        dto.RequiredQty = subgroup.Sum(t => t.RequestQty);
                        dto.OrderedQty = poDetails.Sum(t => t.QtyOrdered);
                        dto.RequestQty = dto.RequiredQty - dto.OrderedQty;
                        dto.UnitRefId = catmat.UnitRefId;
                        dto.Uom = catmat.Uom;
                        dto.MaterialTypeId = catmat.MaterialTypeId;
                        dto.MaterialTypeName = catmat.MaterialTypeId == (int)MaterialType.RAW ? L("RAW") : L("SEMI");
                        dto.LiveStock = matStock.LiveStock;
                        dto.ReorderLevel = catmat.ReorderLevel;
                        dto.MinimumStock = catmat.MinimumStock;
                        dto.MaximumStock = catmat.MaximumStock;
                        dto.SubDetails = subgroup.ToList();
                        if (dto.UnitRefId == 0)
                        {
                            throw new UserFriendlyException("Unit Error " + dto.MaterialRefName);
                        }
                        var caterHeaderRefIds = subgroup.Select(t => t.CaterQuoteHeaderId).ToList();
                        var caterQuotationHeaders = subgroup.Select(t => t.CaterHeader).ToList();
                        dto.CaterHeaderRefIds = overAllCaterHeaderRefIds.Where(t => caterHeaderRefIds.Contains(t.CaterHeaderRefId)).ToList();
                        dto.CaterHeaders = caterQuotationHeaders;
                        decimal totalQtyPlaced = 0.0m;
                        decimal totalQtyReceived = 0.0m;
                        foreach (var poDetail in poDetails)
                        {
                            var currentPoMaster = rsPoMaster.FirstOrDefault(t => t.Id == poDetail.PoRefId);
                            poDetail.PoReferenceCode = currentPoMaster.PoReferenceCode;
                            var unit = rsUnits.FirstOrDefault(t => t.Id == poDetail.UnitRefId);
                            poDetail.UnitRefName = unit.Name;
                            poDetail.MaterialRefName = mat.MaterialName;
                            poDetail.PoCurrentStatus = L("PoDetailStatus", poDetail.QtyOrdered, poDetail.QtyReceived, currentPoMaster.Id, currentPoMaster.PoReferenceCode, unit.Name);
                            if (subgroup.Key.UnitRefId == poDetail.UnitRefId)
                            {
                                totalQtyPlaced = totalQtyPlaced + poDetail.QtyOrdered;
                                totalQtyReceived = totalQtyReceived + poDetail.QtyReceived;
                            }
                            else
                            {
                                var unitConversion = rsUc.FirstOrDefault(t => t.BaseUnitId == subgroup.Key.UnitRefId && t.RefUnitId == poDetail.UnitRefId);
                                if (unitConversion == null)
                                    throw new UserFriendlyException(L("UnitConversion") + " " + L("NotExist") + " - " + dto.MaterialRefName);

                                var conversionFactor = 1 / unitConversion.Conversion;
                                totalQtyPlaced = totalQtyPlaced + (poDetail.QtyOrdered * conversionFactor);
                                totalQtyReceived = totalQtyReceived + (poDetail.QtyReceived * conversionFactor);
                            }

                            var sup = rsSuppliers.FirstOrDefault(t => t.Id == currentPoMaster.SupplierRefId);
                            poDetail.SupplierRefId = currentPoMaster.SupplierRefId;
                            poDetail.SupplierRefName = sup.SupplierName;
                        }
                        dto.PoDetails = poDetails.MapTo<List<CaterMaterialPurchaseOrderDetailViewDto>>();
                        if (poDetails.Count == 0)
                        {
                            dto.StatusRefId = (int)CaterQuotationRequiredMaterialStatus.Po_Not_Generated;
                            dto.StatusRefName = "PO Not Generated.";
                        }
                        else
                        {
                            dto.StatusRefId = (int)CaterQuotationRequiredMaterialStatus.Po_Generated;
                            dto.StatusRefName = "PO Generated.";
                            if (dto.RequiredQty == totalQtyPlaced)
                            {
                                dto.StatusRefId = (int)CaterQuotationRequiredMaterialStatus.Po_Generated;
                                dto.StatusRefName = "PO Generated.";
                            }
                            else if (dto.RequiredQty > totalQtyPlaced && dto.RequiredQty > 0)
                            {
                                dto.StatusRefId = (int)CaterQuotationRequiredMaterialStatus.Po_Partially_Generated;
                                dto.StatusRefName = "PO Partially Generated, " + (dto.RequiredQty - totalQtyPlaced) + " Quantity Due for PO.";
                            }

                            if (totalQtyReceived > 0 && totalQtyPlaced > 0 && totalQtyReceived == totalQtyPlaced)
                            {
                                dto.StatusRefName = dto.StatusRefName + " " + totalQtyReceived + " Quantity Received.";
                            }
                            else if (totalQtyReceived > 0)
                            {
                                dto.StatusRefName = dto.StatusRefName + " " + totalQtyReceived + " Quantity Received.";
                            }
                        }
                        output.Add(dto);
                    }
                }
            }
            return output;
        }
    }
}