﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.CaterCustomers.Dto;
using DinePlan.DineConnect.CaterCustomers.Exporting;
using DinePlan.DineConnect.CaterModel;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.CaterCustomers
{
    public class CaterCustomerAppService : DineConnectAppServiceBase, ICaterCustomerAppService
    {
        private readonly IRepository<CaterCustomer> _caterCustomerRepository;
        private readonly IRepository<CaterCustomerAddressDetail> _caterCustomerAddressDetailRepository;
        private readonly ICaterCustomerExporter _caterCustomerExporter;

        public CaterCustomerAppService(
             IRepository<CaterCustomer> caterCustomerRepository
            , ICaterCustomerExporter caterCustomerExporter
            , IRepository<CaterCustomerAddressDetail> caterCustomerAddressDetailRepository)
        {
            _caterCustomerRepository = caterCustomerRepository;
            _caterCustomerExporter = caterCustomerExporter;
            _caterCustomerAddressDetailRepository = caterCustomerAddressDetailRepository;
        }

        public async Task<List<CaterCustomerListDto>> GetAll()
        {
            var allItems = await _caterCustomerRepository.GetAllListAsync();
            return allItems.MapTo<List<CaterCustomerListDto>>();
        }

        public async Task<PagedResultDto<CaterCustomerListDto>> GetAllCaterCustomer(GetCaterCustomersInput input)
        {
            if (!input.Filter.IsNullOrEmpty())
            {
                input.Filter = Regex.Replace(input.Filter.Trim(), @"\s+", " ");
            }

            var allItems = _caterCustomerRepository
               .GetAll()
               .Where(c => c.TenantId == AbpSession.TenantId)
               .WhereIf(!input.Filter.IsNullOrEmpty(), p => p.CustomerName.Contains(input.Filter) || p.LegalEntityName.Contains(input.Filter) || p.PhoneNumber.Contains(input.Filter) || p.TaxRegistrationNumber.Contains(input.Filter));

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<CaterCustomerListDto>>();
            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<CaterCustomerListDto>(allItemCount, allListDtos);
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _caterCustomerRepository.GetAllListAsync(c => c.TenantId == AbpSession.TenantId);
            var allListDtos = allList.MapTo<List<CaterCustomerListDto>>();
            return _caterCustomerExporter.ExportToFile(allListDtos);
        }

        public ListResultDto<ComboboxItemDto> GetOrderChannels()
        {
            var result = new List<ComboboxItemDto>();
            var i = 0;
            foreach (var item in Enum.GetNames(typeof(OrderChannels)))
            {
                result.Add(new ComboboxItemDto(i.ToString(), item));
                i++;
            }

            return new ListResultDto<ComboboxItemDto>(result);
        }

        public async Task<CaterCustomerEditDto> GetCaterCustomerForEdit(NullableIdInput input)
        {
            var editDto = new CaterCustomerEditDto();

            if (input.Id.HasValue)
            {
                var hDto = await _caterCustomerRepository.GetAll()
                    .Include(e => e.CaterCustomerAddresses)
                    .FirstOrDefaultAsync(e => e.Id == input.Id.Value);
                editDto = hDto.MapTo<CaterCustomerEditDto>();
            }
            else
            {
                editDto = new CaterCustomerEditDto();
            }

            return editDto;
        }

        public async Task CreateOrUpdateCaterCustomer(CaterCustomerEditDto input)
        {
            ValidateForCreateSync(input);
            input.TenantId = AbpSession.TenantId;

            if (input.Id.HasValue)
            {
                await UpdateCaterCustomer(input);
            }
            else
            {
                await CreateCaterCustomer(input);
            }
        }

        public async Task DeleteCaterCustomer(NullableIdInput input)
        {
            if (input.Id.HasValue)
                await _caterCustomerRepository.DeleteAsync(input.Id.Value);
        }

        private void ValidateForCreateSync(CaterCustomerEditDto caterCustomer)
        {
            var exists = _caterCustomerRepository.GetAll()
                .Where(c => c.TenantId == AbpSession.TenantId)
                .Where(a => a.CustomerName.ToUpper().Equals(caterCustomer.CustomerName.ToUpper()))
                .WhereIf(caterCustomer.Id != 0, a => a.Id != caterCustomer.Id);

            if (exists.Any())
            {
                throw new UserFriendlyException(L("CustomerNameAlreadyExists", caterCustomer.CustomerName));
            }
        }

        private async Task CreateCaterCustomer(CaterCustomerEditDto input)
        {
            var caterCustomer = input.MapTo<CaterCustomer>();

            await _caterCustomerRepository.InsertAsync(caterCustomer);
        }

        private async Task UpdateCaterCustomer(CaterCustomerEditDto input)
        {
            var caterCustomer = await _caterCustomerRepository.GetAsync(input.Id.Value);

            var existsId = input.CaterCustomerAddresses.Select(s => s.Id).ToList();
            var addressesNotinCustomer = await _caterCustomerAddressDetailRepository.GetAll()
                .Where(e => e.CaterCustomerId == input.Id && !existsId.Contains(e.Id))
                .ToListAsync();

            addressesNotinCustomer.ForEach(e => _caterCustomerAddressDetailRepository.DeleteAsync(e));

            input.MapTo(caterCustomer);

            caterCustomer.CaterCustomerAddresses = new HashSet<CaterCustomerAddressDetail>();
            foreach (var address in input.CaterCustomerAddresses)
            {
                if (!address.Id.HasValue || address.Id == 0)
                    caterCustomer.CaterCustomerAddresses.Add(address.MapTo<CaterCustomerAddressDetail>());
                else
                {
                    var oldAdd = await _caterCustomerAddressDetailRepository.GetAsync(address.Id.Value);
                    address.MapTo(oldAdd);
                    await _caterCustomerAddressDetailRepository.UpdateAsync(oldAdd);
                }
            }

            await _caterCustomerRepository.UpdateAsync(caterCustomer);
        }
    }
}