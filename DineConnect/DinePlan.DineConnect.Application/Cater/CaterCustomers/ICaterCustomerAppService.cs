﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.CaterCustomers.Dto;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.CaterCustomers
{
    public interface ICaterCustomerAppService : IApplicationService

    {
        Task CreateOrUpdateCaterCustomer(CaterCustomerEditDto input);

        Task DeleteCaterCustomer(NullableIdInput input);

        Task<List<CaterCustomerListDto>> GetAll();

        Task<PagedResultDto<CaterCustomerListDto>> GetAllCaterCustomer(GetCaterCustomersInput input);

        Task<FileDto> GetAllToExcel();

        Task<CaterCustomerEditDto> GetCaterCustomerForEdit(NullableIdInput input);

        ListResultDto<ComboboxItemDto> GetOrderChannels();
    }
}