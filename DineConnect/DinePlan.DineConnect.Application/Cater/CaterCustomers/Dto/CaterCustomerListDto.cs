﻿using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.CaterModel;

namespace DinePlan.DineConnect.CaterCustomers.Dto
{
    [AutoMapFrom(typeof(CaterCustomer))]
    public class CaterCustomerListDto : CreationAuditedEntity<int?>
    {
        public string CustomerName { get; set; }
        public string Name => CustomerName;

        public string LegalEntityName { get; set; }

        public string TaxRegistrationNumber { get; set; }

        public string CreditTerm { get; set; }

        public int OrderChannel { get; set; }
        public string OrderChannelName => ((OrderChannels)OrderChannel).ToString();

        public string Email { get; set; }

        public string FaxNumber { get; set; }

        public string Website { get; set; }

        public string AddOn { get; set; }
    }
}