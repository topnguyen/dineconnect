﻿using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.CaterModel;
using System.Collections.ObjectModel;

namespace DinePlan.DineConnect.CaterCustomers.Dto
{
    [AutoMapTo(typeof(CaterCustomer))]
    public class CaterCustomerEditDto : CreationAuditedEntity<int?>
    {
        public CaterCustomerEditDto()
        {
            CaterCustomerAddresses = new Collection<CustomerAddressEditDto>();
        }

        public string CustomerName { get; set; }

        public string LegalEntityName { get; set; }

        public string TaxRegistrationNumber { get; set; }

        public string CreditTerm { get; set; }

        public int OrderChannel { get; set; }

        public string Email { get; set; }

        public string FaxNumber { get; set; }

        public string Website { get; set; }

        public string AddOn { get; set; }
        public int? TenantId { get; set; }

        public Collection<CustomerAddressEditDto> CaterCustomerAddresses { get; set; }
    }
}