﻿using Abp.AutoMapper;
using DinePlan.DineConnect.CaterModel;

namespace DinePlan.DineConnect.CaterCustomers.Dto
{
    [AutoMap(typeof(CaterCustomerAddressDetail))]
    public class CustomerAddressEditDto
    {
        public int? Id { get; set; }
        public int CaterCustomerId { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Country { get; set; }

        public string ZipCode { get; set; }

        public string PhoneNumber { get; set; }
    }
}