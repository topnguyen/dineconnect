﻿using DinePlan.DineConnect.CaterCustomers.Dto;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.CaterCustomers.Exporting
{
    public interface ICaterCustomerExporter
    {
        FileDto ExportToFile(List<CaterCustomerListDto> dtos);
    }
}