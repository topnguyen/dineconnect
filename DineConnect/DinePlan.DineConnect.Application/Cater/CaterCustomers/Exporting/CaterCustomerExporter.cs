﻿using DinePlan.DineConnect.CaterCustomers.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.CaterCustomers.Exporting
{
    public class CaterCustomerExporter : FileExporterBase, ICaterCustomerExporter
    {
        public FileDto ExportToFile(List<CaterCustomerListDto> dtos)
        {
            return CreateExcelPackage(
                "CaterCustomerList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("CaterCustomer"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("CustomerName"),
                        L("LegalEntityName"),
                        L("TaxRegistrationNumber"),
                        L("CreationTime")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.CustomerName,
                        _ => _.LegalEntityName,
                        _ => _.TaxRegistrationNumber,
                        _ => _.CreationTime

                        );

                    var creationTimeColumn = sheet.Column(5);
                    creationTimeColumn.Style.Numberformat.Format = "yyyy-mm-dd hh:mm";

                    for (var i = 1; i <= 5; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}