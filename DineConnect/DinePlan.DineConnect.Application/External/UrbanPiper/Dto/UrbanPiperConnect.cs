﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.External.UrbanPiper.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    namespace DinePlan.DineConnect.External.Zomato.Dto
    {
        public class UrbanPiperConnect
        {
            public bool Connected { get; set; }
            public string ApiKey { get; set; }
            public int TagId { get; set; }

        }

        public class UrbanPiperSettingDto
        {
            public string ApiKey { get; set; }
            public int TagId { get; set; }

        }

    }

}
