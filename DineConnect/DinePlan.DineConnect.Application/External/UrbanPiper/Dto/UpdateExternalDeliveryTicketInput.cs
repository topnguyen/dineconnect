﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Ticket.Dto;

namespace DinePlan.DineConnect.External.UrbanPiper.Dto
{
    public class UpdateExternalDeliveryTicketInput
    {
        public IExternalDeliveryInput Input{get;set;}
        public int LocationId {get;set;}
        public string TicketNumber {get;set;}
        public int Tenant {get;set;}
        public string ContentType {get;set;}


    }

    public interface IExternalDeliveryInput
    {

    }

    
    public class FlyTicket : IExternalDeliveryInput
    {
        public int Id { get; set; }
        public int EntityId { get; set; }
        public int DepartmentId { get; set; }
        public int TerminalId { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime LastOrderDate { get; set; }
        public DateTime LastPaymentDate { get; set; }
        public decimal TotalAmount { get; set; }
        public string Note { get; set; }
        public FlyEntityStatus FlyEntityStatus { get; set; }
        public FlyTicketStatus FlyTicketStatus { get; set; }
        public List<FlyOrder> Orders { get; set; }

        public bool IsPaid { get; set; }
        public string TicketTags { get; set; }
        public string TicketNumber { get; set; }
        public string ClosingTag { get; set; }
        public bool TaxIncluded { get; set; }

        public List<FlyTransaction> Transactions { get; set; }
        public List<FlyTicketEntity> Entities { get; set; }
        public List<FlyCalculation> Calculations { get; set; }
        public List<FlyPayDetails> Payments { get; set; }

        public FlyTicket()
        {

        }
    }
    public class FlyPayDetails
    {
        public int TicketId { get; set; }
        public int PaymentTypeId { get; set; }
        public decimal Amount { get; set; }
        public string PaymentTag { get; set; }
        public PaymentFrame PaymentFrame { get; set; }

    }
    public class FlyCalculation
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
    }

    public class FlyTicketEntity
    {
        public int Id { get; set; }
        public int EntityTypeId { get; set; }
        public string EntityName { get; set; }
        public string EntityTypeName { get; set; }
        public string CustomData { get; set; }
    }

    public class FlyCustomField
    {
        public string Name { get; set; }
        public int FieldType { get; set; }
        public string EditingFormat { get; set; }
        public string ValueSource { get; set; }
        public bool IsPrimary { get; set; }
    }
    public class FlyOrder
    {
        public int OrderNumber { get; set; }
        public int MenuItemId { get; set; }
        public string MenuItemName { get; set; }
        public string PortionName { get; set; }
        public decimal Price { get; set; }
        public decimal Tax { get; set; }
        public decimal Quantity { get; set; }
        public string Note { get; set; }
        public int MenuItemType { get; set; }
        public int OrderRef { get; set; }
        public string FlyOrderStatus { get; set; }
        public List<FlyOrderStatus> OrderStatus { get; set; }
        public List<FlyOrderTagItems> OrderTagItems { get; set; }
        public string UserName { get; set; }
        public int RefId { get; set; }
        public string CustomStatus { get; set; }
    }

    public class FlyTransaction
    {
        public TrasactionType TrasactionType { get; set; }
        public decimal Amount { get; set; }
    }

    public enum TrasactionType
    {
        Sales,
        ServiceCharge,
        Tax,
        Discount,
        Round
    }

    public enum FlyEntityStatus
    {
        [Description("New Orders")] NewOrders,

        [Description("Bill Requested")] BillRequested,

        [Description("Available")] Available
    }

    public enum FlyTicketStatus
    {
        [Description("New Orders")] NewOrders,

        [Description("Locked")] Locked,

        [Description("Unpaid")] Unpaid,

        [Description("Paid")] Paid
    }

    public enum FlyOrderStatus
    {
        [Description("New")] New,
        [Description("Submitted")] Submitted,
        [Description("Serve Now")] ServeNow,
        [Description("Serve Later")] ServeLater,
        [Description("Void")] Void,
        [Description("Gift")] Gift,
        [Description("Urgent")] Urgent
    }

    public class FlyOrderTagItems
    {
        public int OrderTagGroupId { get; set; }
        public int OrderTagId { get; set; }
        public string TagName { get; set; }
        public string TagValue { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
    }
}
