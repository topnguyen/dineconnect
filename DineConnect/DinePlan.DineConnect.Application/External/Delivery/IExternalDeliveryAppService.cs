﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.External.Delivery.Dto;
using DinePlan.DineConnect.External.UrbanPiper.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.External.Delivery
{
    public interface IExternalDeliveryAppService : IApplicationService
    {
        Task Push(string orderContent);

        Task<StringObject> PushOrders(StringObject order);

        Task<ExternalTicketOutput> PushTicket(ExternalTicketInput ticket);


    }
}