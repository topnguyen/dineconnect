﻿/* 
 Licensed under the Apache License, Version 2.0

 http://www.apache.org/licenses/LICENSE-2.0
 */
using System;
using System.Xml.Serialization;
using System.Collections.Generic;
namespace Xml2CSharp
{
    [XmlRoot(ElementName = "header", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
    public class SoapOrderHeader
    {
        [XmlElement(ElementName = "referenceNumber", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string ReferenceNumber { get; set; }
        [XmlElement(ElementName = "orderedTimestamp", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string OrderedTimestamp { get; set; }
        [XmlElement(ElementName = "deliveryTimestamp", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string DeliveryTimestamp { get; set; }
        [XmlElement(ElementName = "locked", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string Locked { get; set; }
        [XmlElement(ElementName = "lockOwner", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string LockOwner { get; set; }
        [XmlElement(ElementName = "printPos", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string PrintPos { get; set; }
        [XmlElement(ElementName = "mode", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string Mode { get; set; }
        [XmlElement(ElementName = "revenueCenter", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string RevenueCenter { get; set; }
        [XmlElement(ElementName = "function", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string Function { get; set; }
        [XmlElement(ElementName = "pickUp", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string PickUp { get; set; }
        [XmlElement(ElementName = "orderStatus", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string OrderStatus { get; set; }
    }

    [XmlRoot(ElementName = "address", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
    public class SoapOrderAddress
    {
        [XmlElement(ElementName = "number", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string Number { get; set; }
        [XmlElement(ElementName = "street", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string Street { get; set; }
        [XmlElement(ElementName = "apartNumber", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string ApartNumber { get; set; }
        [XmlElement(ElementName = "near", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string Near { get; set; }
        [XmlElement(ElementName = "cityState", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string CityState { get; set; }
        [XmlElement(ElementName = "zipCode", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string ZipCode { get; set; }
        [XmlElement(ElementName = "zoneReferenceNumber", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string ZoneReferenceNumber { get; set; }
        [XmlElement(ElementName = "zoneDescription", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string ZoneDescription { get; set; }
    }

    [XmlRoot(ElementName = "remarks", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
    public class SoapOrderRemarks
    {
        [XmlElement(ElementName = "normal1", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string Normal1 { get; set; }
        [XmlElement(ElementName = "normal2", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string Normal2 { get; set; }
        [XmlElement(ElementName = "normal3", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string Normal3 { get; set; }
        [XmlElement(ElementName = "special1", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string Special1 { get; set; }
        [XmlElement(ElementName = "special2", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string Special2 { get; set; }
    }

    [XmlRoot(ElementName = "customer", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
    public class SoapOrderCustomer
    {
        [XmlElement(ElementName = "phone", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string Phone { get; set; }
        [XmlElement(ElementName = "extension", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string Extension { get; set; }
        [XmlElement(ElementName = "areaCode", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string AreaCode { get; set; }
        [XmlElement(ElementName = "name", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string Name { get; set; }
        [XmlElement(ElementName = "company", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string Company { get; set; }
        [XmlElement(ElementName = "address", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public SoapOrderAddress Address { get; set; }
        [XmlElement(ElementName = "remarks", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public SoapOrderRemarks Remarks { get; set; }
    }

    [XmlRoot(ElementName = "Item", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
    public class SoapOrderItem
    {
        [XmlElement(ElementName = "itemId", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string ItemId { get; set; }
        [XmlElement(ElementName = "description", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string Description { get; set; }
        [XmlElement(ElementName = "quantity", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string Quantity { get; set; }
        [XmlElement(ElementName = "price", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string Price { get; set; }
        [XmlElement(ElementName = "operation", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string Operation { get; set; }
        [XmlElement(ElementName="modifierArray", Namespace="http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public ModifierArray ModifierArray { get; set; }
    }

    public class SoapModifer
    {
        public string ItemId { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
    }
    [XmlRoot(ElementName="Modifier", Namespace="http://www.maitredpos.com/webservices/MDMealZone/201202")]
    public class Modifier {
        [XmlElement(ElementName="itemId", Namespace="http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string ItemId { get; set; }
        [XmlElement(ElementName="description", Namespace="http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string Description { get; set; }
        [XmlElement(ElementName="price", Namespace="http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string Price { get; set; }
        [XmlElement(ElementName="operation", Namespace="http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string Operation { get; set; }
    }
 
    [XmlRoot(ElementName="modifierArray", Namespace="http://www.maitredpos.com/webservices/MDMealZone/201202")]
    public class ModifierArray {
        [XmlElement(ElementName="Modifier", Namespace="http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public List<Modifier> Modifier { get; set; }
    }
    [XmlRoot(ElementName = "itemArray", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
    public class SoapOrderItemArray
    {
        [XmlElement(ElementName = "Item", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public List<SoapOrderItem> Item { get; set; }
    }

    [XmlRoot(ElementName = "mediaArray", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
    public class SoapMediaArray
    {
        [XmlElement(ElementName = "mediaId", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string MediaId { get; set; }
        [XmlElement(ElementName = "amount", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string Amount { get; set; }
    }
    [XmlRoot(ElementName = "Order", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
    public class SoapOrderOrder
    {
        [XmlElement(ElementName = "Operation", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string Operation { get; set; }
        [XmlElement(ElementName = "client", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string Client { get; set; }
        [XmlElement(ElementName = "voidReason", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string VoidReason { get; set; }
        [XmlElement(ElementName = "checkNumber", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string CheckNumber { get; set; }
        [XmlElement(ElementName = "tableNumber", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public string TableNumber { get; set; }
        [XmlElement(ElementName = "itemArray", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public SoapOrderItemArray ItemArray { get; set; }

        [XmlElement(ElementName = "mediaArray", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public SoapMediaArray MediaArray { get; set; }
    }

    [XmlRoot(ElementName = "orderArray", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
    public class SoapOrderArray
    {
        [XmlElement(ElementName = "Order", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public SoapOrderOrder Order { get; set; }
    }

    [XmlRoot(ElementName = "order", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
    public class SoapOrder
    {
        [XmlElement(ElementName = "header", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public SoapOrderHeader Header { get; set; }
        [XmlElement(ElementName = "customer", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public SoapOrderCustomer Customer { get; set; }
        [XmlElement(ElementName = "orderArray", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public SoapOrderArray OrderArray { get; set; }
    }

    [XmlRoot(ElementName = "AddOrder", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
    public class SoapOrderAddOrder
    {
        [XmlElement(ElementName = "order", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public SoapOrder Order { get; set; }
    }

    [XmlRoot(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class SoapOrderBody
    {
        [XmlElement(ElementName = "AddOrder", Namespace = "http://www.maitredpos.com/webservices/MDMealZone/201202")]
        public SoapOrderAddOrder AddOrder { get; set; }
    }

    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class SoapOrderEnvelope
    {
        [XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public SoapOrderBody Body { get; set; }
        [XmlAttribute(AttributeName = "soapenv", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Soapenv { get; set; }
        [XmlAttribute(AttributeName = "ns", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Ns { get; set; }
    }

}
