﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.External.UrbanPiper.Dto;

namespace DinePlan.DineConnect.External.Delivery.Dto
{
    public class StringObject
    {
        public string Content { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorDescription { get; set; }

    }

    public class IntegerObject
    {
        public int WorkPeriodId { get; set; }

    }

    public class ExternalTicketOutput
    {
        public int TicketId { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class ExternalTicketInput
    {
        public int TenantId { get; set; }
        public int DeviceType { get; set; }
        public decimal TotalAmount { get; set; }

        public int LocationId { get; set; }
        public string TicketNumber { get; set; }
        public FlyTicket DeliveryInput { get; set; }

        public ExternalTicketInput()
        {
            DeliveryInput = new FlyTicket();
        }

    }
}
