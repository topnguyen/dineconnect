﻿namespace DinePlan.DineConnect.External.Delivery.Dto
{
    public class UpdateOrderStatusInput
    {
        public UpdateOrderStatusInput()
        {
            OrderStatus = new OrderStatusInput();
        }

        public int TicketId { get; set; }
        public string Status { get; set; }
        public int OrderId { get; set; }
        public OrderStatusInput OrderStatus { get; set; }
    }

    public class OrderStatusInput
    {
        public string new_status { get; set; }
        public string message { get; set; }
    }
}