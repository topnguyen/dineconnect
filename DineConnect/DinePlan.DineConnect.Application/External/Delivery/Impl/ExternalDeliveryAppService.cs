﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using Castle.Core.Logging;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Delivery;
using DinePlan.DineConnect.Connect.DeliveryTicket;
using DinePlan.DineConnect.Connect.DeliveryTicket.Dto;
using DinePlan.DineConnect.Connect.External;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.UrbanPiperIntegrate;
using DinePlan.DineConnect.Connect.UrbanPiperIntegrate.Dto;
using DinePlan.DineConnect.External.Delivery.Dto;
using DinePlan.DineConnect.External.Delivery.Soap;
using DinePlan.DineConnect.External.UrbanPiper.Dto;
using DinePlan.DineConnect.SerialHelpers;
using DinePlan.DineConnect.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Common;
using FluentValidation;
using FluentValidation.Results;
using Xml2CSharp;

namespace DinePlan.DineConnect.External.Delivery.Impl
{
    public class ExternalDeliveryAppService : DineConnectAppServiceBase, IExternalDeliveryAppService
    {
        public static string TenantId = ConfigurationManager.AppSettings["DeliveryTenantId"];
        public static string LocationId = ConfigurationManager.AppSettings["DeliveryLocationId"];

        private readonly IDeliveryAppService _deliveryAppService;
        private readonly ILocationAppService _locationAppService;
        private readonly ILogger _logger;
        private readonly IRepository<MenuItem> _miManager;
        private IRepository<Location> _lRepo;
        private ISettingManager _settingManager;
        private readonly IRepository<ExternalDeliveryTicket> _edtRepo;

        public ExternalDeliveryAppService(IRepository<MenuItem> miManager, ILogger logger,
            ISettingManager settingManager, IRepository<Location> location, IDeliveryAppService delAppService,
            ILocationAppService locationAppService,
            IRepository<ExternalDeliveryTicket> edtRepo)
        {
            _miManager = miManager;
            _lRepo = location;
            _settingManager = settingManager;
            _logger = logger;
            _deliveryAppService = delAppService;
            _locationAppService = locationAppService;
            _edtRepo = edtRepo;
        }

        public async Task Push(string orderContent)
        {
            if (!string.IsNullOrEmpty(orderContent))
            {
                _logger.Fatal(orderContent);
            }
        }

        public async Task<StringObject> PushOrders(StringObject order)
        {
            try
            {
                _logger.Fatal("Request : " + JsonConvert.SerializeObject(order));
             
                var locationId = 1;
                var tenantId = 1;

                if (!string.IsNullOrEmpty(TenantId))
                {
                    tenantId = Convert.ToInt32(TenantId);
                }
                if (!string.IsNullOrEmpty(LocationId))
                {
                    locationId = Convert.ToInt32(LocationId);
                }
                _logger.Fatal("Location " + locationId + " and Tenant  : " + tenantId);

                var orgId = await _locationAppService.GetOrgnizationIdForLocationId(locationId);
                if (orgId > 0)
                {
                    CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);
                }
                var myGuid = Guid.NewGuid();
                var returnObj = new StringObject { Content = myGuid.ToString() };

                if (!string.IsNullOrEmpty(order?.Content))
                {
                    var myOrderContent = ReplaceContents(order.Content);

                    var mySer = new Serializer();
                    var myOrder = mySer.Deserialize<SoapOrderEnvelope>(myOrderContent);

                    if (myOrder == null)
                    {
                        throw new UserFriendlyException(string.Format(ExternalErrorDescription.Error_102_f, "Order"));
                    }

                    if (myOrder.Body == null)
                    {
                        throw new UserFriendlyException(string.Format(ExternalErrorDescription.Error_102_f, "Body"));
                    }

                    if (myOrder.Body.AddOrder == null)
                    {
                        throw new UserFriendlyException(string.Format(ExternalErrorDescription.Error_102_f, "AddOrder"));
                    }

                    if (myOrder.Body.AddOrder.Order == null)
                    {
                        throw new UserFriendlyException(string.Format(ExternalErrorDescription.Error_102_f, "Order"));
                    }

                    if (myOrder.Body.AddOrder.Order.Customer == null)
                    {
                        throw new UserFriendlyException(string.Format(ExternalErrorDescription.Error_102_f, "Customer"));
                    }
                    if (myOrder.Body.AddOrder.Order.OrderArray == null)
                    {
                        throw new UserFriendlyException(string.Format(ExternalErrorDescription.Error_102_f,
                            "Order Array"));
                    }
                    if (myOrder.Body.AddOrder.Order.OrderArray.Order == null)
                    {
                        throw new UserFriendlyException(string.Format(ExternalErrorDescription.Error_102_f,
                            "Order Array Order"));
                    }
                    if (myOrder.Body.AddOrder.Order.OrderArray.Order.ItemArray == null)
                    {
                        throw new UserFriendlyException(string.Format(ExternalErrorDescription.Error_102_f, "Item Array"));
                    }

                    if (myOrder.Body.AddOrder.Order.OrderArray.Order.ItemArray.Item == null)
                    {
                        throw new UserFriendlyException(string.Format(ExternalErrorDescription.Error_102_f,
                            "Item Array Items"));
                    }
                    if (!myOrder.Body.AddOrder.Order.OrderArray.Order.ItemArray.Item.Any())

                        throw new UserFriendlyException(string.Format(ExternalErrorDescription.Error_102_f,
                            "Item Array Items Empty"));

                    if (DineConnectUtil.IsInteger(myOrder.Body.AddOrder.Order.Header.RevenueCenter))
                    {
                        locationId = Convert.ToInt32(myOrder.Body.AddOrder.Order.Header.RevenueCenter);
                    }

                    var totalAmount = 0M;
                    if (DineConnectUtil.IsDecimal(myOrder.Body.AddOrder.Order?.OrderArray?.Order?.MediaArray?.Amount))
                    {
                        totalAmount = Convert.ToDecimal(myOrder.Body.AddOrder.Order?.OrderArray?.Order?.MediaArray?.Amount);
                    }

                    var builder = new StringBuilder();

                    if (!string.IsNullOrEmpty(myOrder?.Body?.AddOrder?.Order?.Customer?.Remarks?.Normal1))
                    {
                        builder.Append(myOrder.Body.AddOrder.Order.Customer.Remarks.Normal1);
                    }
                    if (!string.IsNullOrEmpty(myOrder?.Body?.AddOrder?.Order?.Customer?.Remarks?.Normal2))
                    {
                        builder.Append(myOrder.Body.AddOrder.Order.Customer.Remarks.Normal2);
                    }

                    if (!string.IsNullOrEmpty(myOrder?.Body?.AddOrder?.Order?.Customer?.Remarks?.Normal3))
                    {
                        builder.Append(myOrder.Body.AddOrder.Order.Customer.Remarks.Normal3);
                    }
                    if (!string.IsNullOrEmpty(myOrder?.Body?.AddOrder?.Order?.Customer?.Remarks?.Special1))
                    {
                        builder.Append(myOrder.Body.AddOrder.Order.Customer.Remarks.Special1);
                    }
                    if (!string.IsNullOrEmpty(myOrder?.Body?.AddOrder?.Order?.Customer?.Remarks?.Special2))
                    {
                        builder.Append(myOrder.Body.AddOrder.Order.Customer.Remarks.Special2);
                    }
                    var myPickB = false;
                    var myPickup = myOrder.Body.AddOrder.Order.Header.PickUp;
                    if (!string.IsNullOrEmpty(myPickup))
                    {
                        myPickB = Convert.ToBoolean(myPickup);
                    }
                    var newTi = new CreateOrUpdateDeliveryTicketInput
                    {
                        Ticket = new DeliveryTicketDto
                        {
                            LocationId = locationId,
                            TenantId = tenantId,
                            DeliveryStatus = DeliveryTicketStatus.NotAssigned,
                            DeliveryTicketType = myPickB ? DeliveryTicketType.PickUp : DeliveryTicketType.Delivery,
                            MemberCode = myOrder.Body.AddOrder.Order.Customer.Phone,
                            Name = myOrder.Body.AddOrder.Order.Customer.Name,
                            Address =
                                myOrder.Body.AddOrder.Order.Customer.Address.Number + "  " +
                                myOrder.Body.AddOrder.Order.Customer.Address.Street,
                            Orders = new List<DeliveryOrderDto>(),
                            TicketNumber = myOrder.Body.AddOrder.Order.Header.ReferenceNumber,
                            LastModificationTime = DateTime.Now,
                            Note = builder.ToString(),
                            TotalAmount = totalAmount
                        }
                    };

                    foreach (var allMyOrder in myOrder.Body.AddOrder.Order.OrderArray.Order.ItemArray.Item)
                    {
                        var myItem =
                            await
                                _miManager.FirstOrDefaultAsync(
                                    a => a.TenantId == tenantId && a.AliasCode.Equals(allMyOrder.ItemId.Trim())) ??
                            await
                                _miManager.FirstOrDefaultAsync(
                                    a =>
                                        a.TenantId == tenantId &&
                                        a.Name.ToUpper().Equals(allMyOrder.Description.ToUpper()));

                        if (myItem == null)
                        {
                            returnObj.ErrorCode = "104";
                            returnObj.ErrorCode = "Item Not Available " + allMyOrder.Description;
                            return returnObj;
                        }

                        var deliOrder = new DeliveryOrderDto
                        {
                            Name = allMyOrder.Description,
                            Quantity = Convert.ToDecimal(allMyOrder.Quantity),
                            Price = Convert.ToDecimal(allMyOrder.Price),
                            MenuItemPortionId = myItem.Portions.First().Id
                        };

                        if (allMyOrder.ModifierArray?.Modifier != null && allMyOrder.ModifierArray.Modifier.Any())
                        {
                            List<SoapModifer> modifiers = new List<SoapModifer>();
                            foreach (var modifier in allMyOrder.ModifierArray.Modifier)
                            {
                                SoapModifer singleM = new SoapModifer()
                                {
                                    Description = modifier.Description,
                                    ItemId = modifier.ItemId,
                                    Price = Convert.ToDecimal(modifier.Price)
                                };

                                modifiers.Add(singleM);
                            }

                            if (modifiers.Any())
                            {
                                deliOrder.OrderTags = JsonConvert.SerializeObject(modifiers);
                            }
                        }

                        newTi.Ticket.Orders.Add(deliOrder);
                    }
                    await _deliveryAppService.CreateOrUpdateTicket(newTi);
                }
                else
                {
                    throw new UserFriendlyException(ExternalErrorDescription.Erorr_101);
                }

                _logger.Fatal("Response : " + returnObj);

                return returnObj;
            }
            catch (Exception exception)
            {
                _logger.Fatal("Response : " + exception);
                throw new UserFriendlyException(exception.Message);
            }
        }

        public async Task<ExternalTicketOutput> PushTicket(ExternalTicketInput input)
        {
            try
            {
             
                ApiExternalDeliveryValidator validator = new ApiExternalDeliveryValidator();
                ValidationResult result = validator.Validate(input);

                if (!result.IsValid)
                {
                    return new ExternalTicketOutput()
                    {
                        ErrorMessage = FluentValidatoryExtension.ValidationResult(result.Errors)
                    };
                }

                var orgId = await _locationAppService.GetOrgnizationIdForLocationId(input.LocationId);
                if (orgId > 0)
                {
                    CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);
                }
                var myGuid = Guid.NewGuid();
                var returnObj = new ExternalTicketOutput { ErrorMessage = ""};

                var count = await
                    _edtRepo.LongCountAsync(
                        a =>
                            a.TicketNumber != null && a.TicketNumber.Equals(input.TicketNumber) &&
                            a.LocationId.Equals(input.LocationId));

                var updateTicket = count > 0;
                var createTicket = true;
                var ticketId = 0;
                if (updateTicket)
                {
                    var ticket =
                        await _edtRepo.FirstOrDefaultAsync(
                            a =>
                                a.TicketNumber.Equals(input.TicketNumber) &&
                                a.LocationId.Equals(input.LocationId));

                    if (ticket != null)
                    {
                        createTicket = false;
                        ticketId = ticket.Id;

                    }
                }

                if (createTicket)
                {
                    ticketId = await CreateTicket(new UpdateExternalDeliveryTicketInput()
                    {
                        ContentType = "PushOrder",
                        LocationId = input.LocationId,
                        TicketNumber = input.TicketNumber,
                        Tenant = input.TenantId,
                        Input = input.DeliveryInput

                    });
                }

                returnObj.TicketId = ticketId;
                return returnObj;
            }
            catch (Exception exception)
            {
                _logger.Fatal("Response : " + exception);
                throw new UserFriendlyException(exception.Message);
            }
        }
      
        private async Task<int> CreateTicket(UpdateExternalDeliveryTicketInput input)
        {
            var extT = new ExternalDeliveryTicket
            {
                TicketNumber = input.TicketNumber,
                LocationId = input.LocationId,
                Claimed = false,
                TenantId = input.Tenant,
                Status = "IN"
            };

            extT.AddDeliveryContentValue(new DeliveryContentValue
            {
                Contents = JsonConvert.SerializeObject(input),
                TypeOfContent = input.ContentType
            });

            var orderer = await _edtRepo.InsertAndGetIdAsync(extT);
            return orderer;
        }
      

        private string ReplaceContents(string content)
        {
            return content.Replace("\"", "'");
        }


    }


    public class ApiExternalDeliveryValidator : AbstractValidator<ExternalTicketInput>
    {
        public ApiExternalDeliveryValidator()
        {
            RuleFor(x => x.TenantId).NotEqual(0);
            RuleFor(x => x.LocationId).NotEqual(0);
            RuleFor(x => x.TicketNumber).NotNull().NotEmpty();
            RuleFor(x => x.DeliveryInput).NotNull();
            RuleFor(x => x.DeliveryInput.DepartmentId).NotNull().NotEmpty();

        }



    }
}