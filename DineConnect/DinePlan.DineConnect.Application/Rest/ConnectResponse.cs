﻿namespace DinePlan.DineConnect.Rest
{
    public class ConnectException
    {
        public int code { get; set; }
        public string message { get; set; }
        public string details { get; set; }
        public object validationErrors { get; set; }
    }

    public class ConnectResponse
    {
        public bool success { get; set; }
        public object result { get; set; }
        public ConnectException error { get; set; }
        public bool unAuthorizedRequest { get; set; }
    }
}