﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.MultiTenancy;
using DinePlan.DineConnect.Rest.DinePlan.Common.Model.Sync;
using Newtonsoft.Json;
using RestSharp;

namespace DinePlan.DineConnect.Rest
{
    public class RestUtils
    {
        public static string Url = "";
        public static string Tenant = "";
        public static string User = "";
        public static string Password = "";
        public static string _token = "";

        public static object GetResponse(string method, object input)
        {
            try
            {
                if (!string.IsNullOrEmpty(Url) && !string.IsNullOrEmpty(Tenant) && !string.IsNullOrEmpty(User) &&
                    !string.IsNullOrEmpty(Password) && !string.IsNullOrEmpty(_token))
                {
                    var client = new RestClient(Url);
                    var request = new RestRequest(method, Method.POST);
                    request.AddHeader("Authorization", $"Bearer {_token}");
                    request.AddHeader("Content-Type", "application/json");
                    if (input != null)
                    {
                        var body = JsonConvert.SerializeObject(input);
                        request.AddParameter("application/json", body, ParameterType.RequestBody);
                    }
                    var response = (RestResponse)client.Execute(request);
                    if (ValidateRestResponse(response))
                    {
                        var connectResponse = JsonConvert.DeserializeObject<ConnectResponse>(response.Content);

                        if (ValidateConnectResponse(connectResponse))
                        {
                            return connectResponse.result;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                //Put the Error
            }
            return null;
        }

        public static string GetToken(bool getNew)
        {
            if (!getNew && !string.IsNullOrEmpty(_token))
            {
                return _token;
            }

            string status = null;

            if (!string.IsNullOrEmpty(Url) && !string.IsNullOrEmpty(Tenant) && !string.IsNullOrEmpty(User) &&
                !string.IsNullOrEmpty(Password))
            {
                var client = new RestClient(Url);
                var request = CreateRequest("api/Account/Authenticate", Method.POST);
                request.RequestFormat = DataFormat.Json;

                var authRequest = new AuthRequest
                {
                    tenancyName = Tenant,
                    usernameOrEmailAddress = User,
                    password = Password
                };

                var body = JsonConvert.SerializeObject(authRequest);
                request.AddParameter("application/json", body, ParameterType.RequestBody);
                var response = (RestResponse)client.Execute(request);

                if (ValidateRestResponse(response))
                {
                    try
                    {
                        var authResponse = JsonConvert.DeserializeObject<ConnectResponse>(response.Content);
                        if (ValidateConnectResponse(authResponse))
                        {
                            status = authResponse.result.ToString();
                            _token = status;
                        }
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }
            return status;
        }

        public static RestRequest CreateRequest(string resource, Method method)
        {
            var request = new RestRequest(resource, method);
            request.AddHeader("Content-Type", "application/json");
            return request;
        }

        public static bool ValidateConnectResponse(ConnectResponse response)
        {
            if (response.success)
            {
                return true;
            }
            return false;
        }

        public static bool ValidateRestResponse(RestResponse response)
        {
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }
            return false;
        }
    }
}
