﻿namespace DinePlan.DineConnect.Rest
{
    public class ConnectRequests
    {
        
		public static string GetUserSerialNumberMax = "api/services/app/material/GetUserSerialNumberMax";

		public static string GETSUPPLIERS = "api/services/app/supplier/GetAll";
		
		public static string GETUNIT = "api/services/app/unit/GetAll";

		public static string GETUNITCONVERSION = "api/services/app/unitconversion/GetAll";

		


		public static string GETMATERIALGROUP = "api/services/app/materialgroup/GetAll";

		public static string GETMATERIALGROUPCATEGORY = "api/services/app/materialgroupcategory/GetView";

		public static string GETMATERIALS = "api/services/app/material/GetAll";

		public static string GETMATERIALFOREDIT = "api/services/app/material/GetMaterialForEdit";

		public static string GETTAX = "api/services/app/tax/GetAll";

		public static string GETTAXFOREDIT = "api/services/app/tax/GetTaxForEdit";

	}
}
