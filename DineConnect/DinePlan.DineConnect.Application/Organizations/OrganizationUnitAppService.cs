﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Organizations;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Organizations.Dto;
using DinePlan.DineConnect.House;
using DinePlan.DineConnect.Connect.Master.Dtos;
using System.Collections.Generic;
using System.Web;
using DinePlan.DineConnect.Session;

namespace DinePlan.DineConnect.Organizations
{
    public class OrganizationUnitAppService : DineConnectAppServiceBase, IOrganizationUnitAppService
    {
        private readonly OrganizationUnitManager _organizationUnitManager;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IRepository<UserDefaultOrganization> _userDefaultOrganizationRepo;

        public ConnectSession ConnectSession { get; set; }

        public OrganizationUnitAppService(
            OrganizationUnitManager organizationUnitManager,
            IRepository<OrganizationUnit, long> organizationUnitRepository,
            IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository,
            IRepository<UserDefaultOrganization> userDefaultOrganizationRepo)
        {
            _organizationUnitManager = organizationUnitManager;
            _organizationUnitRepository = organizationUnitRepository;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _userDefaultOrganizationRepo = userDefaultOrganizationRepo;
        }

        public async Task<ListResultOutput<OrganizationUnitDto>> GetOrganizationUnits()
        {
            var query =
                from ou in _organizationUnitRepository.GetAll()
                join uou in _userOrganizationUnitRepository.GetAll() on ou.Id equals uou.OrganizationUnitId into g
                select new { ou, memberCount = g.Count() };

            var items = await query.ToListAsync();
            
            return new ListResultOutput<OrganizationUnitDto>(
                items.Select(item =>
                {
                    var dto = item.ou.MapTo<OrganizationUnitDto>();
                    dto.MemberCount = item.memberCount;
                    return dto;
                }).ToList());
        }

        public async Task<PagedResultOutput<OrganizationUnitUserListDto>> GetOrganizationUnitUsers(GetOrganizationUnitUsersInput input)
        {
            var query = from uou in _userOrganizationUnitRepository.GetAll()
                join ou in _organizationUnitRepository.GetAll() on uou.OrganizationUnitId equals ou.Id
                join user in UserManager.Users on uou.UserId equals user.Id
                where uou.OrganizationUnitId == input.Id
                orderby input.Sorting
                select new {uou, user};

            var totalCount = await query.CountAsync();
            var items = await query.PageBy(input).ToListAsync();

            return new PagedResultOutput<OrganizationUnitUserListDto>(
                totalCount,
                items.Select(item =>
                {
                    var dto = item.user.MapTo<OrganizationUnitUserListDto>();
                    dto.AddedTime = item.uou.CreationTime;
                    return dto;
                }).ToList());
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree)]
        public async Task<OrganizationUnitDto> CreateOrganizationUnit(CreateOrganizationUnitInput input)
        {
            var organizationUnit = new OrganizationUnit(AbpSession.TenantId, input.DisplayName, input.ParentId);
            if(input.ParentId.HasValue)
            {
                var orgDto = new OrganizationUnit
                {
                    DisplayName = input.DisplayName,
                    TenantId = AbpSession.TenantId,
                    ParentId=input.ParentId
                };
                await _organizationUnitManager.CreateAsync(organizationUnit);
                await CurrentUnitOfWork.SaveChangesAsync();
            }
            else
            {
                try
                {
                    organizationUnit = new OrganizationUnit
                    {
                        DisplayName = input.DisplayName,
                        Code=input.DisplayName,
                        TenantId = AbpSession.TenantId
                    };
                    var oum = _organizationUnitRepository.InsertAndGetId(organizationUnit);
                }
                catch (Exception exception)
                {
                    throw exception;
                }
            }

            return organizationUnit.MapTo<OrganizationUnitDto>();
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree)]
        public async Task<OrganizationUnitDto> UpdateOrganizationUnit(UpdateOrganizationUnitInput input)
        {
            var organizationUnit = await _organizationUnitRepository.GetAsync(input.Id);
            
            organizationUnit.DisplayName = input.DisplayName;

            await _organizationUnitManager.UpdateAsync(organizationUnit);

            return await CreateOrganizationUnitDto(organizationUnit);
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree)]
        public async Task<OrganizationUnitDto> MoveOrganizationUnit(MoveOrganizationUnitInput input)
        {
            await _organizationUnitManager.MoveAsync(input.Id, input.NewParentId);
            
            return await CreateOrganizationUnitDto(
                await _organizationUnitRepository.GetAsync(input.Id)
                );
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree)]
        public async Task DeleteOrganizationUnit(IdInput<long> input)
        {
            await _organizationUnitManager.DeleteAsync(input.Id);
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_OrganizationUnits_ManageMembers)]
        public async Task AddUserToOrganizationUnit(UserToOrganizationUnitInput input)
        {
            await UserManager.AddToOrganizationUnitAsync(input.UserId, input.OrganizationUnitId);

            await AppendDefaultUserOrganization(input);
        }

        public async Task AppendDefaultUserOrganization(UserToOrganizationUnitInput input)
        {
            var existList = _userDefaultOrganizationRepo.FirstOrDefault(a => a.UserId == input.UserId);
            if (existList==null)
            {
                UserDefaultOrganization newDefaultUser = new UserDefaultOrganization
                {
                    UserId = input.UserId,
                    OrganizationUnitId = input.OrganizationUnitId
                };

                await _userDefaultOrganizationRepo.InsertOrUpdateAndGetIdAsync(newDefaultUser);
            }
            else
            {
                var item = await _userDefaultOrganizationRepo.GetAsync(existList.Id);
                item.OrganizationUnitId = input.OrganizationUnitId;
                await _userDefaultOrganizationRepo.InsertOrUpdateAndGetIdAsync(item);
            }
            if (input.OrganizationId > 0 && ConnectSession!=null)
            {
                ConnectSession.OrgId = Convert.ToInt32(input.OrganizationId);
            }
        }

		public async Task ChangeDefaultUserOrganization(UserToOrganizationUnitInput input)
		{
			var existList = _userDefaultOrganizationRepo.FirstOrDefault(a => a.UserId == input.UserId && a.OrganizationUnitId==input.OrganizationUnitId);
			if (existList != null)
			{
				var otherUserOrganisation = await _userOrganizationUnitRepository.GetAll().Where(t => t.UserId == input.UserId && t.OrganizationUnitId != input.OrganizationUnitId).ToListAsync();
				if (otherUserOrganisation.Count > 0)
				{
					long changedOrganisationId = otherUserOrganisation[0].OrganizationUnitId;

					var item = await _userDefaultOrganizationRepo.GetAsync(existList.Id);
					item.OrganizationUnitId = changedOrganisationId;
					await _userDefaultOrganizationRepo.InsertOrUpdateAndGetIdAsync(item);
				}
				else
				{
					await _userDefaultOrganizationRepo.DeleteAsync(t=>t.Id==existList.Id);
				}

			}
			if (input.OrganizationId > 0 && ConnectSession != null)
			{
				ConnectSession.OrgId = Convert.ToInt32(input.OrganizationId);
			}
		}



		[AbpAuthorize(AppPermissions.Pages_Administration_OrganizationUnits_ManageMembers)]
        public async Task RemoveUserFromOrganizationUnit(UserToOrganizationUnitInput input)
        {
            await UserManager.RemoveFromOrganizationUnitAsync(input.UserId, input.OrganizationUnitId);
			await ChangeDefaultUserOrganization(input);
		}

        [AbpAuthorize(AppPermissions.Pages_Administration_OrganizationUnits_ManageMembers)]
        public async Task<bool> IsInOrganizationUnit(UserToOrganizationUnitInput input)
        {
            return await UserManager.IsInOrganizationUnitAsync(input.UserId, input.OrganizationUnitId);
        }

        private async Task<OrganizationUnitDto> CreateOrganizationUnitDto(OrganizationUnit organizationUnit)
        {
            var dto = organizationUnit.MapTo<OrganizationUnitDto>();
            dto.MemberCount = await _userOrganizationUnitRepository.CountAsync(uou => uou.OrganizationUnitId == organizationUnit.Id);
            return dto;
        }

		public async Task AddLocationToUser(UserLocationDto input)
		{
			List<long> locationIdstoberetained = new List<long>();

			if (input.Locations!=null && input.Locations.Count > 0)
			{
				foreach (var loc in input.Locations)
				{
					UserToOrganizationUnitInput idto = new UserToOrganizationUnitInput
					{
						OrganizationUnitId = loc.Id,
						UserId = input.UserId
					};
					await AddUserToOrganizationUnit(idto);
					locationIdstoberetained.Add(loc.Id);
				}

				if (input.DefaultLocationRefId != null && input.DefaultLocationRefId > 0)
				{
					UserToOrganizationUnitInput defdto = new UserToOrganizationUnitInput { OrganizationUnitId = input.DefaultLocationRefId.Value, UserId = input.UserId };

					await AppendDefaultUserOrganization(defdto);
				}
			}

			var delList = await _userOrganizationUnitRepository.GetAll().Where(a => a.UserId == input.UserId && !locationIdstoberetained.Contains(a.OrganizationUnitId)).ToListAsync();

			foreach (var a in delList)
			{
				await _userOrganizationUnitRepository.DeleteAsync(a.Id);
			}


		}
	}
}