﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using DinePlan.DineConnect.Engage.MemberLoyaltyProgram.Dtos;
using System.Collections.Generic;
using DinePlan.DineConnect.Engage.Member.Dtos.DinePlan.DineConnect.Engage.Dtos;

namespace DinePlan.DineConnect.Engage.MemberLoyaltyProgram
{
    public interface IPointCampaignAppService : IApplicationService
    {
        Task<IdInput> CreateOrUpdatePointCampaign(CreateOrUpdatePointCampaignInput input);
        Task<PagedResultOutput<PointCampaignListDto>> GetAll(GetPointCampaignInput input);
        Task<GetPointCampaignOutput> GetPointCampaignForUpdate(NullableIdInput input);
        Task DeletePointCampaign(NullableIdInput input);
        Task<List<PointCampaignModifierTypeListDto>> GetModifierTypes();
        Task<decimal> AddPoints(AddPointInput input);
        Task<RedeemPointsAsVouchersOutput> RedeemPointsAsVouchers(RedeemPointsAsVouchersInput input);
    }
}

