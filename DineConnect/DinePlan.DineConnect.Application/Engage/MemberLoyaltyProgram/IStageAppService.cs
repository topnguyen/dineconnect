﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Engage.MemberLoyaltyProgram.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Engage.MemberLoyaltyProgram
{
    public interface IStageAppService : IApplicationService
    {
        Task<PagedResultOutput<StageListDto>> GetAll(GetStageInput input);
        Task<IdInput> CreateOrUpdateStage(CreateOrUpdateStageInput input);
        Task<CreateOrUpdateStageInput> GetStage(IdInput input);
        Task<ListResultOutput<ComboboxItemDto>> GetForCombobox(ProgramAndStageId ids);
    }
}
