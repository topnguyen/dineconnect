﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using AutoMapper;
using DinePlan.DineConnect.Engage.MemberLoyaltyProgram.Dtos;

namespace DinePlan.DineConnect.Engage.MemberLoyaltyProgram.Implementation
{
    public class LoyaltyProgramAppService : DineConnectAppServiceBase, ILoyaltyProgramAppService
    {
        private readonly IRepository<LoyaltyProgram> _programRepo;
        private readonly ILoyaltyProgramManager _programManager;
        private readonly IRepository<ProgramStage> _stageRepo;
        private readonly IRepository<PointAccumulation> _pointAccRepo;


        public LoyaltyProgramAppService(ILoyaltyProgramManager programManager, IRepository<LoyaltyProgram> programRepo, IRepository<ProgramStage> stageRepo, IRepository<PointAccumulation> pointAccRepo)
        {
            _programManager = programManager;
            _programRepo = programRepo;
            _stageRepo = stageRepo;
            _pointAccRepo = pointAccRepo;
        }

        public async Task<IdInput> CreateOrUpdateProgram(CreateOrUpdateProgramInput input)
        {
            try
            {
                if (input.Id.HasValue)
                {
                    input.ExpiryDays = input.Expiry ? input.ExpiryDays : 0;
                }
                var dto = input.MapTo<LoyaltyProgram>();
                CheckErrors(await _programManager.CreateOrUpdateSync(dto));
                return new IdInput { Id = dto.Id };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.InnerException + ex.Message);
            }
        }

        public async Task<CreateOrUpdateProgramInput> GetProgram(IdInput input)
        {
            var hDto = await _programRepo.GetAsync(input.Id);
            var pDto = hDto.MapTo<CreateOrUpdateProgramInput>();
            return pDto;
        }

        public async Task<PagedResultOutput<LoyaltyProgramListDto>> GetAll(GetLoyaltyProgramInput input)
        {
            try
            {
                var allItems = _programRepo.GetAll()
                        .WhereIf(
                            !input.Filter.IsNullOrWhiteSpace(),
                            p => p != null && (p.Name.Contains(input.Filter))
                        );

                var sortMenuItems = await allItems
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var allListDtos = sortMenuItems.MapTo<List<LoyaltyProgramListDto>>();

                var allItemCount = await allItems.CountAsync();

                return new PagedResultOutput<LoyaltyProgramListDto>(
                    allItemCount,
                    allListDtos
                    );
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetForCombobox()
        {
            List<ComboboxItemDto> returnList = new List<ComboboxItemDto>();
            var programs = _programRepo.GetAll();            
            foreach (var program in programs)
            {
                returnList.Add(new ComboboxItemDto()
                {
                    DisplayText = program.Name,
                    Value = program.Id.ToString()
                });
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }
    }
}
