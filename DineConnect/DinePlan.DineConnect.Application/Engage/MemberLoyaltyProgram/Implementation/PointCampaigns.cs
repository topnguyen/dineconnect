﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Engage.Member;
using DinePlan.DineConnect.Engage.MemberLoyaltyProgram.Dtos;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Engage.MemberLoyaltyProgram.Implementation
{
    public class PointCampaigns
    {
        IMemberAppService _memberAppService;
        IRepository<PointCampaign> _pointCampaignRepository;
        AddPointInput _input;
        public PointCampaigns(
            AddPointInput input,
            IRepository<PointCampaign> pointCampaignRepository,
            IMemberAppService memberAppService)
        {
            _pointCampaignRepository = pointCampaignRepository;
            _memberAppService = memberAppService;
            _input = input;
        }
        public async Task<decimal> GetPointsAsync() {
            
            List<decimal> pointGroup = new List<decimal>();

            var member = await _memberAppService.GetMemberForInfo(new NullableIdInput { Id = _input.MemberId });

            if (member == null) throw new System.Exception("Member not found.");

            var getResult = _pointCampaignRepository.GetAll()
                .Include(c => c.Modifiers.Select(m=>m.ModifierType));

            var pointCampaigns = getResult.MapTo<List<PointCampaignListDto>>();

            foreach (var pointCampaign in pointCampaigns)
            {
                decimal points = 0;
                points = _input.TicketTotalAmount / pointCampaign.RequiredAmount * pointCampaign.OfferedPoints;
                foreach (var mod in pointCampaign.Modifiers)
                {
                    if (mod.ModifierType.Code == PointCampaignModifierTypeEnum.BDMTH.ToString() &&
                        member.Member.BirthDay.HasValue)
                    {
                        int tMonth = DateTime.Today.Month;
                        int bMonth = member.Member.BirthDay.Value.Month;
                        if (tMonth == bMonth)
                        {
                            points = points * mod.ModifyingFactor;
                        }
                    }
                    else if (mod.ModifierType.Code == PointCampaignModifierTypeEnum.TMPRD.ToString())
                    {
                        DateTime tDate = DateTime.Today;
                        if (tDate >= mod.StartDate && tDate <= mod.EndDate)
                        {
                            points = points * mod.ModifyingFactor;
                        }
                    }
                    else if (mod.ModifierType.Code == PointCampaignModifierTypeEnum.MTIER.ToString())
                    {
                        if (member.Member.MembershipTierId == mod.MembershipTierId) {
                            points = points * mod.ModifyingFactor;
                        }
                    }
                }
                pointGroup.Add(points);
            }

            return pointGroup.Max();
        }
    }
}
