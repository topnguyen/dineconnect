﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using AutoMapper;
using DinePlan.DineConnect.Engage.MemberLoyaltyProgram.Dtos;

namespace DinePlan.DineConnect.Engage.MemberLoyaltyProgram.Implementation
{
    public class StageAppService : DineConnectAppServiceBase, IStageAppService
    {
        private readonly IRepository<ProgramStage> _stageRepo;
        private readonly IRepository<PointAccumulation> _pointAccRepo;
        private readonly IStageManager _stageManager;

        public StageAppService(IRepository<ProgramStage> stageRepo, IRepository<PointAccumulation> pointAccRepo, IStageManager stageManager)
        {
            _stageRepo = stageRepo;
            _pointAccRepo = pointAccRepo;
            _stageManager = stageManager;
        }

        public async Task<IdInput> CreateOrUpdateStage(CreateOrUpdateStageInput input)
        {
            if (input.Id.HasValue)
            {
                return await UpdateStage(input);
            }

            return await CreateStage(input);
        }

        private void UpdateOrder(int previousId, int currId)
        {
            var previousStage = _stageRepo.Get(previousId);
            var currStage = _stageRepo.Get(currId);
            var currPrevious = _stageRepo.GetAll().FirstOrDefault(x => x.NextStageId == currId);
            if (currPrevious != null)
            {
                currPrevious.NextStageId = currStage.NextStageId;
            }
            currStage.NextStageId = previousStage.NextStageId;
            previousStage.NextStageId = currId;
        }

        protected virtual async Task<IdInput> UpdateStage(CreateOrUpdateStageInput input)
        {
            try
            {
                var stage = await _stageRepo.GetAsync(input.Id.Value);

                stage.Condition = input.Condition;
                stage.ConditionDayValue = input.ConditionDayValue;
                stage.MinRedemptionPoints = input.MinRedemptionPoints;
                stage.Name = input.Name;
                stage.PointsExpiryAllowed = input.PointsExpiryAllowed;
                stage.PointExpiryDays = input.PointExpiryDays;
                stage.RedemptionValue = input.RedemptionValue;

                if (input?.PointAccumulations != null && input.PointAccumulations.Any())
                {
                    var oids = new List<int>();
                    foreach (var pa in input.PointAccumulations)
                    {
                        if (pa.Id == null)
                        {
                            stage.PointAccumulations.Add(Mapper.Map<PointAccumulationEditDto, PointAccumulation>(pa));
                        }
                        else
                        {
                            oids.Add(pa.Id.Value);
                            var fromUpdation = stage.PointAccumulations.SingleOrDefault(a => a.Id.Equals(pa.Id));
                            UpdatePointAccumulation(fromUpdation, pa);
                        }
                    }
                    var tobeRemoved =
                        stage.PointAccumulations.Where(a => a.Id != null && !a.Id.Equals(0) && !oids.Contains(a.Id)).ToList();
                    foreach (var tagItem in tobeRemoved)
                    {
                        await _pointAccRepo.DeleteAsync(tagItem.Id);
                    }
                }
                else
                {
                    foreach (var tagItem in stage.PointAccumulations.ToList())
                    {
                        await _pointAccRepo.DeleteAsync(tagItem.Id);
                    }
                }
                if (input.PreviousStageId.HasValue)
                {
                    UpdateOrder(input.PreviousStageId.Value, stage.Id);
                }
                return new IdInput { Id = stage.Id };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.InnerException + ex.Message);
            }
        }

        private void UpdatePointAccumulation(PointAccumulation to, PointAccumulationEditDto from)
        {
            to.PAccTypeId = from.PAccTypeId;
            to.RoundOff = from.RoundOff;
            to.Point = from.Point;
        }

        protected virtual async Task<IdInput> CreateStage(CreateOrUpdateStageInput input)
        {
            try
            {
                var dto = input.MapTo<ProgramStage>();
                CheckErrors(await _stageManager.CreateOrUpdateSync(dto));

                if (input.PreviousStageId.HasValue)
                {
                    UpdateOrder(input.PreviousStageId.Value, dto.Id);
                }

                return new IdInput { Id = dto.Id };
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<PagedResultOutput<StageListDto>> GetAll(GetStageInput input)
        {
            try
            {
                var allItems = _stageRepo.GetAll()
                        .WhereIf(
                            !input.Filter.IsNullOrWhiteSpace(),
                            p => p != null && (p.Name.Contains(input.Filter))
                        );

                var sortMenuItems = await allItems
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var allListDtos = sortMenuItems.MapTo<List<StageListDto>>();

                var allItemCount = await allItems.CountAsync();

                return new PagedResultOutput<StageListDto>(
                    allItemCount,
                    allListDtos
                    );
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<CreateOrUpdateStageInput> GetStage(IdInput input)
        {
            var hDto = await _stageRepo.GetAsync(input.Id);
            var pDto = hDto.MapTo<CreateOrUpdateStageInput>();
            var previous = _stageRepo.GetAll().FirstOrDefault(s => s.NextStageId == hDto.Id);
            pDto.PreviousStageId = previous?.Id;
            return pDto;
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetForCombobox(ProgramAndStageId ids)
        {
            List<ComboboxItemDto> returnList = new List<ComboboxItemDto>();
            var stages = _stageRepo.GetAll().Where(s => s.LoyaltyProgramId == ids.LoyaltyProgramId && (!ids.StageId.HasValue || s.Id != ids.StageId));
            foreach (var stage in stages)
            {
                returnList.Add(new ComboboxItemDto()
                {
                    DisplayText = stage.Name,
                    Value = stage.Id.ToString()
                });
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }
    }
}
