﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Sync;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

using DinePlan.DineConnect.Engage.MemberLoyaltyProgram.Dtos;
using DinePlan.DineConnect.Engage.Member;
using System;
using DinePlan.DineConnect.Engage.Member.Dtos;
using DinePlan.DineConnect.Engage.Member.Dtos.DinePlan.DineConnect.Engage.Dtos;
using DinePlan.DineConnect.Engage.Gift.DinePlan.DineConnect.Engage;
using DinePlan.DineConnect.Engage.Gift;

namespace DinePlan.DineConnect.Engage.MemberLoyaltyProgram.Implementation
{
    public class PointCampaignAppService : DineConnectAppServiceBase, IPointCampaignAppService
    {
        private readonly ISyncAppService _syncAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<PointCampaign> _pointCampaignRepository;
        private readonly IRepository<PointCampaignModifierType> _pointCampaignModifierTypeRepository;
        private readonly IRepository<PointCampaignModifier> _pointCampaignModifierRepository;
        private readonly IRepository<ConnectMember> _memberRepository;
        private readonly IRepository<MemberAccountType> _memberAccountTypeRepository;
        private readonly IRepository<GiftVoucher> _giftVoucherRepository;

        private readonly IMemberAppService _memberAppService;
        private readonly IMemberAccountAppService _memberAccountAppService;
        private readonly IGiftVoucherTypeAppService _giftVoucherTypeAppService;
        public PointCampaignAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<PointCampaign> pointCampaignRepository,
            IRepository<PointCampaignModifierType> pointCampaignModifierTypeRepository,
            IRepository<PointCampaignModifier> pointCampaignModifierRepository,
            IRepository<ConnectMember> memberRepository,
            IRepository<GiftVoucher> giftVoucherRepository,
            IRepository<MemberAccountType> memberAccountTypeRepository,
            ISyncAppService syncAppService,
            IMemberAppService memberAppService,
            IMemberAccountAppService memberAccountAppService,
            IGiftVoucherTypeAppService giftVoucherTypeAppService)
        {
            _syncAppService = syncAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _pointCampaignRepository = pointCampaignRepository;
            _pointCampaignModifierTypeRepository = pointCampaignModifierTypeRepository;
            _pointCampaignModifierRepository = pointCampaignModifierRepository;
            _memberRepository = memberRepository;
            _giftVoucherRepository = giftVoucherRepository;
            _memberAccountTypeRepository = memberAccountTypeRepository;
            _memberAppService = memberAppService;
            _memberAccountAppService = memberAccountAppService;
            _giftVoucherTypeAppService = giftVoucherTypeAppService;
        }

        public async Task<IdInput> CreateOrUpdatePointCampaign(CreateOrUpdatePointCampaignInput input)
        {
            if (input.PointCampaign.Id.HasValue && input.PointCampaign.Id > 0)
            {
                return await UpdatePointCampaign(input);
            }
            else
            {
                return await CreatePointCampaign(input);
            }
        }

        protected async Task<IdInput> CreatePointCampaign(CreateOrUpdatePointCampaignInput input)
        {
            try
            {
                foreach (var modifier in input.PointCampaign.Modifiers)
                {
                    modifier.MembershipTier = null;
                }
                var pointCampaign = input.PointCampaign.MapTo<PointCampaign>();
                foreach (var mod in pointCampaign.Modifiers)
                {
                    mod.ModifierType = null;
                }
                var createResult = await _pointCampaignRepository.InsertAndGetIdAsync(pointCampaign);
                return new IdInput() { Id = createResult };
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        protected async Task<IdInput> UpdatePointCampaign(CreateOrUpdatePointCampaignInput input)
        {
            try
            {
                var pointCampaign = input.PointCampaign.MapTo<PointCampaign>();
                
                var bfuMods = _pointCampaignModifierRepository.GetAll()
                    .AsNoTracking()
                    .Where(m => m.CampaignId == input.PointCampaign.Id).Select(m=>m.Id).ToArray();

                var incomingModIds = pointCampaign.Modifiers.Where(m => m.Id != 0).Select(m => m.Id).ToArray();
                var toDel = bfuMods.Where(m => !incomingModIds.Contains(m)).ToArray();
                var toUpd = pointCampaign.Modifiers.Select(m=>m).ToArray();

                //var toUpd = pointCampaign.Modifiers.Where(m => bfuMods.Contains(m.Id)).ToArray();

                pointCampaign.Modifiers.Clear();
                var updateResutl = await _pointCampaignRepository.UpdateAsync(pointCampaign);

                foreach (var id in toDel)
                {
                    await _pointCampaignModifierRepository.DeleteAsync(id);
                }

                foreach (var mod in toUpd)
                {
                    mod.MembershipTier = null;
                    if (mod.Id == 0)
                    {
                        await _pointCampaignModifierRepository.InsertAsync(mod);
                    }
                    else {
                        await _pointCampaignModifierRepository.UpdateAsync(mod);
                    }
                    
                }

                return new IdInput() { Id = updateResutl.Id };
            }
            catch (System.Exception ex)
            {

                throw ex;
            }
        }

        public async Task<PagedResultOutput<PointCampaignListDto>> GetAll(GetPointCampaignInput input)
        {
            var allItems = _pointCampaignRepository.GetAll()
                .Where(g => g.IsDeleted == input.IsDeleted);

            allItems = allItems.WhereIf(!input.Filter.IsNullOrEmpty(),
                g => g.Name.Contains(input.Filter));

            var output = new PagedResultOutput<PointCampaignListDto>();

            var sortedItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            output.Items = sortedItems.MapTo<List<PointCampaignListDto>>();
            output.TotalCount = output.Items.Count;

            return output;
        }

        public async Task<GetPointCampaignOutput> GetPointCampaignForUpdate(NullableIdInput input)
        {
            try
            {
                var dbPointCampaign = await _pointCampaignRepository.GetAll()
                .Include(p => p.Modifiers)
                .AsNoTracking()
                .Where(d => d.Id == input.Id).FirstOrDefaultAsync();
                    var output = new GetPointCampaignOutput();
                    output.PointCampaign = dbPointCampaign.MapTo<PointCampaignEditDto>();
                    return output;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public async Task DeletePointCampaign(NullableIdInput input)
        {
            var modIds = _pointCampaignModifierRepository.GetAll()
                .AsNoTracking()
                .Where(m => m.CampaignId == input.Id).Select(m => m.Id).ToArray();
            foreach (var modId in modIds)
            {
                await _pointCampaignModifierRepository.DeleteAsync(modId);
            }
            await _pointCampaignRepository.DeleteAsync(input.Id ?? 0);
        }

        public async Task<decimal> AddPoints(AddPointInput input) {
            PointCampaigns pointCampaigns = new PointCampaigns(input, _pointCampaignRepository, _memberAppService);
            decimal points = await pointCampaigns.GetPointsAsync();

            string description = string.IsNullOrEmpty(input.Description)? "ADD POINT TKT: " + input.TicketNo: input.Description;

            AccountDepositInput dInput = new AccountDepositInput {
                MemberId = input.MemberId,
                Reference = "TKT:" + input.TicketNo,
                Amount = points,
                Description = description
            };
            await _memberAccountAppService.DepositPoints(dInput);
            return points;
        }
        public async Task<List<PointCampaignModifierTypeListDto>> GetModifierTypes() {
            var getResult = await _pointCampaignModifierTypeRepository.GetAllListAsync();
            var dto = getResult.MapTo<List<PointCampaignModifierTypeListDto>>();
            return dto;
        }

        public async Task<RedeemPointsAsVouchersOutput> RedeemPointsAsVouchers (RedeemPointsAsVouchersInput input)
        {
            RedeemPointsAsVouchersOutput output = new RedeemPointsAsVouchersOutput();

            string description = "POINT REDEMPTION FOR VOUCHERS";

            if (string.IsNullOrWhiteSpace(input?.Member?.EntityName))
            {
                output.Message = L("InputIsNoProper");
                return output;
            }

            var member = await _memberRepository.FirstOrDefaultAsync(a => a.MemberCode.Equals(input.Member.EntityName));
            if (member == null)
            {
                output.Message = L("MemeberNotFound");
                return output;
            }

            var pointBalance = _memberAccountAppService.CheckPoints(member.Id);

            var voucherType = await _giftVoucherTypeAppService.GetGiftVoucherTypeForEdit(new NullableIdInput { Id = input.VoucherTypeId});

            if (voucherType.GiftVoucherType.ValidTill < DateTime.Now)
            {
                output.Message = L("VoucherInvalidDate");
                return output;
            }

            var pointToRedeem = input.VoucherCount* voucherType.GiftVoucherType.RequiredPoints;

            if (pointBalance < pointToRedeem)
            {
                output.Message = L("InputPointsAreMoreThanTotalPoints");
                return output;
            }

            var withdrawInput = new AccountWithdrawInput {
                MemberId = member.Id,
                Reference = "TKT:" + input.TicketNo,
                Description = description,
                Amount = pointToRedeem
            };

            var voucherAccountExist = _memberAccountAppService.CheckVoucherAccount(member.Id);

            if (!voucherAccountExist) {

                var createAcctInput = new CreateOrUpdateMemberAccountInput {
                    LocationId = 0,
                    MemberAccount = new MemberAccountEditDto() { MemberId = member.Id},
                    MemberAccountType = MemberAccountTypeEnum.VCH
                };

                await _memberAccountAppService.CreateOrUpdateMemberAccount(createAcctInput);
            }

            await _memberAccountAppService.WithdrawPoints(withdrawInput);

            var depositeInput = new AccountDepositInput
            {
                MemberId = member.Id,
                Reference = "TKT:" + input.TicketNo,
                Description = description,
                Amount = input.VoucherCount
            };

            await _memberAccountAppService.DepositVouchers(depositeInput);

            var vouchers = _giftVoucherRepository.GetAll()
                .Where(v => v.GiftVoucherTypeId == input.VoucherTypeId &&
                v.Claimed == false && v.ConnectMemberId == null ).Take(input.VoucherCount);

            if (vouchers.Count() < input.VoucherCount) {
                output.Message = L("NotEnoughVouchers");
                return output;
            }

            foreach (var voucher in vouchers)
            {
                voucher.Claimed = true;
                voucher.ConnectMemberId = member.Id;
                voucher.ClaimedDate = DateTime.Now;
                voucher.Remarks = input.TicketNo;
                await _giftVoucherRepository.UpdateAsync(voucher);
            }
             
            return output;
        }

    }
}
