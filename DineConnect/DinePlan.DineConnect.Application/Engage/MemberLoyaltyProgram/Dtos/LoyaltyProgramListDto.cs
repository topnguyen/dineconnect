﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Engage.MemberLoyaltyProgram.Dtos
{
    [AutoMapFrom(typeof(LoyaltyProgram))]
    public class LoyaltyProgramListDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Expiry { get; set; }
        public int ExpiryDays { get; set; }
        public DateTime StartDate { get; set; }
    }

    [AutoMapFrom(typeof(ProgramStage))]
    public class StageListDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Condition { get; set; }
        public int ConditionDayValue { get; set; }
        public bool PointsExpiryAllowed { get; set; }
        public decimal MinRedemptionPoints { get; set; }
        public decimal RedemptionValue { get; set; }
        public int PointExpiryDays { get; set; }
    }

    public class GetLoyaltyProgramInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    public class GetStageInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    [AutoMapTo(typeof(PointAccumulation))]
    public class PointAccumulationEditDto
    {
        public int? Id { get; set; }
        public int PAccTypeId { get; set; }
        public decimal Point { get; set; }
        public bool RoundOff { get; set; }
    }

    [AutoMapTo(typeof(LoyaltyProgram))]
    public class CreateOrUpdateProgramInput : IInputDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public bool Expiry { get; set; }
        public int ExpiryDays { get; set; }
        public DateTime StartDate { get; set; }
        //public Collection<ProgramStageEditDto> Stages { get; set; }
    }

    [AutoMapTo(typeof(ProgramStage))]
    public class CreateOrUpdateStageInput : IInputDto
    {
        public int? Id { get; set; }
        public int LoyaltyProgramId { get; set; }
        public string Name { get; set; }
        public string Condition { get; set; }
        public int ConditionDayValue { get; set; }
        public int? NextStageId { get; set; }
        public int? PreviousStageId { get; set; }
        public bool PointsExpiryAllowed { get; set; }
        public decimal MinRedemptionPoints { get; set; }
        public decimal RedemptionValue { get; set; }
        public int PointExpiryDays { get; set; }

        public Collection<PointAccumulationEditDto> PointAccumulations { get; set; }
    }

    [AutoMapTo(typeof(ProgramStage))]
    public class ProgramStageEditDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Condition { get; set; }
        public int ConditionDayValue { get; set; }
        public int Order { get; set; }
        public bool HasNext { get; set; }
        public bool PointsExpiryAllowed { get; set; }
        public decimal MinRedemptionPoints { get; set; }
        public decimal RedemptionValue { get; set; }
        public int PointExpiryDays { get; set; }

        public Collection<PointAccumulationEditDto> PointAccumulations { get; set; }
    }

    public class StageFilter
    {
        public string Id { get; set; }
        public string Label { get; set; }
        public string Type { get; set; }
        public string Input { get; set; }
        public string Operator { get; set; }
        public string Operators { get; set; }
        public string Values { get; set; }
        public decimal Value { get; set; }

    }

    public class ProgramAndStageId
    {
        public int LoyaltyProgramId { get; set; }
        public int? StageId { get; set; }

    }

    public enum PointAccumulationTypes
    {
        NumberOfVisit = 1,
        TotalAmount = 2,
        CalculateOnOrder = 3,
        CalculateProductPoints = 4
    };
}
