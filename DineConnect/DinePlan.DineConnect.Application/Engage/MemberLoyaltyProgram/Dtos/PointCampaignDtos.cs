﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Engage.MemberLoyaltyProgram.Dtos
{
    [AutoMapFrom(typeof(PointCampaign))]
    public class PointCampaignListDto : FullAuditedEntityDto
    {
        public int TenantId { get; set; }
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int OfferedPoints { get; set; }
        public decimal RequiredAmount { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime? EndDate { get; set; }
        public ICollection<PointCampaignModifierListDto> Modifiers { get; set; }
    }

    public class CreateOrUpdatePointCampaignInput : IInputDto
    {
        public PointCampaignEditDto PointCampaign { get; set; }
    }

    public class GetPointCampaignInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public string Operation { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    [AutoMapTo(typeof(PointCampaign))]
    public class PointCampaignEditDto
    {
        public PointCampaignEditDto()
        {
            this.Modifiers = new HashSet<PointCampaignModifierEditDto>();
        }
        public int TenantId { get; set; }
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int OfferedPoints { get; set; }
        public decimal RequiredAmount { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime? EndDate { get; set; }
        public ICollection<PointCampaignModifierEditDto> Modifiers { get; set; }
    }

    public class GetPointCampaignOutput : IOutputDto
    {
        public PointCampaignEditDto PointCampaign { get; set; }
    }

    /** PointCampaignModifier DTOs **/
    [AutoMapFrom(typeof(PointCampaignModifier))]
    public class PointCampaignModifierListDto : FullAuditedEntityDto
    {
        public int? Id { get; set; }
        public int CampaignId { get; set; }
        public PointCampaignModifierType ModifierType { get; set; }
        public int ModifierTypeId { get; set; }
        public decimal ModifyingFactor { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public MembershipTier MembershipTier { get; set; }
        public int MembershipTierId { get; set; }
    }

    [AutoMapTo(typeof(PointCampaignModifier))]
    public class PointCampaignModifierEditDto
    {
        public int? Id { get; set; }
        public int CampaignId { get; set; }
        public PointCampaignModifierType ModifierType { get; set; }
        public int ModifierTypeId { get; set; }
        public decimal ModifyingFactor { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public MembershipTier MembershipTier { get; set; }
        public int? MembershipTierId { get; set; }
    }

    public class GetPointCampaignModifierOutput : IOutputDto
    {
        public PointCampaignModifierEditDto PointCampaignModifier { get; set; }
    }

    /** PointCampaignModifierType **/
    [AutoMapFrom(typeof(PointCampaignModifierType))]
    public class PointCampaignModifierTypeListDto : FullAuditedEntityDto
    {
        public int? Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
    [AutoMapTo(typeof(PointCampaignModifierType))]
    public class PointCampaignModifierTypeEditDto
    {
        public int? Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }

    public enum PointCampaignModifierTypeEnum { 
        BDMTH = 1,
        TMPRD = 2,
        MTIER = 3
    }

    public class GetPointCampaignModifierTypeOutput : IOutputDto
    {
        public PointCampaignModifierTypeEditDto PointCampaignModifierType { get; set; }
    }

    /***** Add Point *****/
    public class AddPointInput {
        public int MemberId { get; set; }
        public string TicketNo { get; set; }
        public decimal TicketTotalAmount { get; set; }
        public string Description { get; set; }
    }
}