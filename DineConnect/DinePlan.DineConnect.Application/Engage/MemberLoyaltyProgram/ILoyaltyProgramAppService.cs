﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Engage.Member.Dtos.DinePlan.DineConnect.Engage.Dtos;
using DinePlan.DineConnect.Engage.MemberLoyaltyProgram.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Engage.MemberLoyaltyProgram
{
    public interface ILoyaltyProgramAppService : IApplicationService
    {
        Task<PagedResultOutput<LoyaltyProgramListDto>> GetAll(GetLoyaltyProgramInput input);
        Task<IdInput> CreateOrUpdateProgram(CreateOrUpdateProgramInput input);
        Task<CreateOrUpdateProgramInput> GetProgram(IdInput input);
        Task<ListResultOutput<ComboboxItemDto>> GetForCombobox();
    }
}
