﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.Gift;
using DinePlan.DineConnect.Engage.GiftVoucherCategorys.Dtos;

namespace DinePlan.DineConnect.Engage.Implementation
{
    public class GiftVoucherCategoryAppService : DineConnectAppServiceBase, IGiftVoucherCategoryAppService
    {
        private readonly IGiftVoucherCategoryListExcelExporter _giftvouchercategoryExporter;
        private readonly IRepository<GiftVoucherCategory> _giftvouchercategoryRepo;
        private readonly IGiftVoucherCategoryManager _giftvouchercategoryManager;

        public GiftVoucherCategoryAppService(IRepository<GiftVoucherCategory> giftvouchercategoryRepo,
            IGiftVoucherCategoryListExcelExporter giftvouchercategoryExporter,
            IGiftVoucherCategoryManager giftvouchercategoryManager)
        {
            _giftvouchercategoryRepo = giftvouchercategoryRepo;
            _giftvouchercategoryExporter = giftvouchercategoryExporter;
            _giftvouchercategoryManager = giftvouchercategoryManager;
        }

        public async Task<PagedResultOutput<GiftVoucherCategoryListDto>> GetAll(GetGiftVoucherCategoryInput input)
        {
            var allItems = _giftvouchercategoryRepo
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Name.ToUpper().Contains(input.Filter.ToUpper())||p.Code.ToUpper().Contains(input.Filter.ToUpper())
                );
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<GiftVoucherCategoryListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<GiftVoucherCategoryListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetGiftVoucherCategoryInput input)
        {
            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<GiftVoucherCategoryListDto>>();
            return _giftvouchercategoryExporter.ExportToFile(allListDtos);
        }

        public async Task<GetGiftVoucherCategoryForEditOutput> GetGiftVoucherCategoryForEdit(NullableIdInput input)
        {
            GiftVoucherCategoryEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _giftvouchercategoryRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<GiftVoucherCategoryEditDto>();
            }
            else
            {
                editDto = new GiftVoucherCategoryEditDto();
            }

            return new GetGiftVoucherCategoryForEditOutput
            {
                GiftVoucherCategory = editDto
            };
        }

        public async Task CreateOrUpdateGiftVoucherCategory(CreateOrUpdateGiftVoucherCategoryInput input)
        {
            if (input.GiftVoucherCategory.Id.HasValue)
            {
                await UpdateGiftVoucherCategory(input);
            }
            else
            {
                await CreateGiftVoucherCategory(input);
            }
        }

        public async Task DeleteGiftVoucherCategory(IdInput input)
        {
            await _giftvouchercategoryRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateGiftVoucherCategory(CreateOrUpdateGiftVoucherCategoryInput input)
        {
            var item = await _giftvouchercategoryRepo.GetAsync(input.GiftVoucherCategory.Id.Value);
            var dto = input.GiftVoucherCategory;
            item.Name = dto.Name;
            item.Code = dto.Code;
            CheckErrors(await _giftvouchercategoryManager.CreateSync(item));
        }

        protected virtual async Task CreateGiftVoucherCategory(CreateOrUpdateGiftVoucherCategoryInput input)
        {
            var dto = input.GiftVoucherCategory.MapTo<GiftVoucherCategory>();
            CheckErrors(await _giftvouchercategoryManager.CreateSync(dto));
        }
        public async Task<ListResultOutput<GiftVoucherCategoryListDto>> GetVoucherCategorys()
        {
            var voucherCategory = _giftvouchercategoryRepo.GetAll();
            return new ListResultOutput<GiftVoucherCategoryListDto>(voucherCategory.MapTo<List<GiftVoucherCategoryListDto>>());
        }
    }
}