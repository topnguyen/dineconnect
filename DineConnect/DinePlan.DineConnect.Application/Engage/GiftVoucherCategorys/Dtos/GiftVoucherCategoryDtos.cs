﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.Gift;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Engage.GiftVoucherCategorys.Dtos
{
    [AutoMapFrom(typeof(GiftVoucherCategory))]
    public class GiftVoucherCategoryListDto : FullAuditedEntityDto
    {
        public virtual string Code { get; set; }
        public virtual string Name { get; set; }
    }
    [AutoMapTo(typeof(GiftVoucherCategory))]
    public class GiftVoucherCategoryEditDto
    {
        public int? Id { get; set; }
        public virtual string Code { get; set; }
        public virtual string Name { get; set; }
    }

    public class GetGiftVoucherCategoryInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }
    public class GetGiftVoucherCategoryForEditOutput : IOutputDto
    {
        public GiftVoucherCategoryEditDto GiftVoucherCategory { get; set; }
    }
    public class CreateOrUpdateGiftVoucherCategoryInput : IInputDto
    {
        [Required]
        public GiftVoucherCategoryEditDto GiftVoucherCategory { get; set; }
    }
}

