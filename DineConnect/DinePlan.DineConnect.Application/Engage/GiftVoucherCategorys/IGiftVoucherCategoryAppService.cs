﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.GiftVoucherCategorys.Dtos;

namespace DinePlan.DineConnect.Engage
{
    public interface IGiftVoucherCategoryAppService : IApplicationService
    {
        Task<PagedResultOutput<GiftVoucherCategoryListDto>> GetAll(GetGiftVoucherCategoryInput inputDto);
        Task<FileDto> GetAllToExcel(GetGiftVoucherCategoryInput input);
        Task<GetGiftVoucherCategoryForEditOutput> GetGiftVoucherCategoryForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateGiftVoucherCategory(CreateOrUpdateGiftVoucherCategoryInput input);
        Task DeleteGiftVoucherCategory(IdInput input);
        Task<ListResultOutput<GiftVoucherCategoryListDto>> GetVoucherCategorys();
    }
}