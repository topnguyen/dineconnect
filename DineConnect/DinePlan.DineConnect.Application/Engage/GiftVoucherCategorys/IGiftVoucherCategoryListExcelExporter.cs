﻿using System.Collections.Generic;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.GiftVoucherCategorys.Dtos;

namespace DinePlan.DineConnect.Engage
{
    public interface IGiftVoucherCategoryListExcelExporter
    {
        FileDto ExportToFile(List<GiftVoucherCategoryListDto> dtos);
    }
}