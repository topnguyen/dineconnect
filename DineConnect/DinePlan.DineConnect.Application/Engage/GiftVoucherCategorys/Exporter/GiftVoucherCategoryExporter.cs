﻿using System.Collections.Generic;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.GiftVoucherCategorys.Dtos;

namespace DinePlan.DineConnect.Engage.Exporter
{
    public class GiftVoucherCategoryListExcelExporter : FileExporterBase, IGiftVoucherCategoryListExcelExporter
    {
        public FileDto ExportToFile(List<GiftVoucherCategoryListDto> dtos)
        {
            return CreateExcelPackage(
                "GiftVoucherCategoryList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("GiftVoucherCategory"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                         L("Code"),
                          L("Name")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                         _ => _.Code,
                          _ => _.Name
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}