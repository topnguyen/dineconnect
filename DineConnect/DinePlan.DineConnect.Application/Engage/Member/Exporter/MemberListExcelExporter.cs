﻿using System;
using System.Collections.Generic;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.Member.Dtos.DinePlan.DineConnect.Engage.Dtos;
using DinePlan.DineConnect.Engage.Member.Implementation;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;

namespace DinePlan.DineConnect.Engage.Member.Exporter
{
    public class MemberListExcelExporter : FileExporterBase, IMemberListExcelExporter
    {
        public FileDto ExportToFile(List<MemberListDto> dtos)
        {
            return CreateExcelPackage(
                "MemberList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Member"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }

        public async Task<FileDto> ExportToFile(GetMemberInput input, MemberAppService appService)
        {
            var file = new FileDto("Members-" + DateTime.Now.ToString("yyyy-MMMM-dd") + ".xlsx",
              MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Members"));
                sheet.OutLineApplyStyle = true;

                List<string> headers = new List<string>
                {
                    L("MemberCode"),
                    L("Name"),
                    L("BirthDay"),
                    L("Address"),
                    L("City"),
                    L("State"),
                    L("Country"),
                    L("PostalCode"),
                    L("EmailId"),
                    L("PhoneNumber"),
                    L("TotalPoints"),
                    L("LastVisitDate"),
                    L("LastVisitTime")
                };


                AddHeader(
                    sheet,
                    headers.ToArray()
                    );

                int rowCount = 2;
                int i = 0;
              
                while (true)
                {
                    input.MaxResultCount = 100;
                    var outPut = await appService.GetAll(input);
                    if (DynamicQueryable.Any(outPut.Items))
                    {
                        for (i = 0; i < outPut.Items.Count; i++)
                        {
                            sheet.Cells[i + rowCount, 1].Value = outPut.Items[i].MemberCode;
                            sheet.Cells[i + rowCount, 2].Value = outPut.Items[i].Name;

                            if (outPut.Items[i].BirthDay.HasValue)
                            {
                                sheet.Cells[i + rowCount, 3].Value = outPut.Items[i].BirthDay.Value.ToShortDateString();
                            }

                            sheet.Cells[i + rowCount, 4].Value = outPut.Items[i].Address;
                            sheet.Cells[i + rowCount, 5].Value = outPut.Items[i].City;
                            sheet.Cells[i + rowCount, 6].Value =
                                outPut.Items[i].State;
                            sheet.Cells[i + rowCount, 7].Value =
                                outPut.Items[i].Country;
                            sheet.Cells[i + rowCount, 8].Value =
                                outPut.Items[i].PostalCode;
                            sheet.Cells[i + rowCount, 9].Value =
                                outPut.Items[i].EmailId;
                            sheet.Cells[i + rowCount, 10].Value = outPut.Items[i].PhoneNumber;
                            sheet.Cells[i + rowCount, 11].Value = outPut.Items[i].TotalPoints;
                            if (outPut.Items[i].LastVisitDate.HasValue)
                            {
                                sheet.Cells[i + rowCount, 12].Value = outPut.Items[i].LastVisitDate.Value.ToShortDateString();
                                sheet.Cells[i + rowCount, 13].Value = outPut.Items[i].LastVisitDate.Value.ToShortTimeString();
                            }
                        }
                        input.SkipCount = input.SkipCount + input.MaxResultCount;
                        rowCount = input.SkipCount + 2;
                    }
                    else
                    {
                        break;
                    }
                }
                for (i = 1; i <= 1; i++)
                {
                    sheet.Column(i).AutoFit();
                }
                Save(excelPackage, file);
            }

            return file;
        }

        public Task<FileDto> ExportMemberOrder(int? id, MemberAppService memberAppService)
        {
            throw new NotImplementedException();
        }
    }
}