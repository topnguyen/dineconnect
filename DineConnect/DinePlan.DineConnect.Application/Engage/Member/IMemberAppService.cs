﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.Handler.Mapping;
using DinePlan.DineConnect.Engage.Member.Dtos.DinePlan.DineConnect.Engage.Dtos;
using DinePlan.DineConnect.Job.Engage;

namespace DinePlan.DineConnect.Engage.Member
{
    public interface IMemberAppService : IApplicationService
    {
        Task<PagedResultOutput<MemberListDto>> GetAll(GetMemberInput inputDto);
        Task<MemberStatsDto> GetAllMemberPoints(GetMemberPointInput input);
        Task<FileDto> GetAllToExcel(GetMemberInput input);
        Task<GetMemberForEditOutput> GetMemberForEdit(NullableIdInput nullableIdInput);
        Task<MemberOrderOutput> GetOrders(NullableIdInput nullableIdInput);
        Task CreateOrUpdateMemberPoints(CreateOrUpdateMemberInput input);
        Task<MessageOutputDto> CreateOrUpdateMember(CreateOrUpdateMemberInput input);
        Task DeleteMember(IdInput input);
        Task DeleteMemberPoint(IdInput input);
        Task<ListResultOutput<MemberListDto>> GetIds();
        Task<RedemOutputDto> GetMemberPoints(RedemInputDto input);
        Task<RedemOutputDto> GetPointsDetailsByName(string name);
        Task<RedemOutputDto> RedemPoints(RedemInputDto input);
        Task<RedemOutputDto> CancelPoint(RedemInputDto input);
        Task<MemberEditDto> ApiMemberInfoByUser(IdInput<long> idInput);
        Task UpdateMemberLastLocation(MemberLocationInput idInput);
        decimal CalculatePoint(decimal totalAmount, decimal pointAcc, decimal pointAccF, bool factor);
        Task<RedemOutputDto> GetMemberPointsForTotal(RedemInputDto input);

        Task CreateMemberAndPoints(InputMemberPointCalculation args);
        Task UpdateMemberPoints(ChangeMemberPoint point);
        ListResultDto<ComboboxItemDto> GetContactDetailsTypes();
        Task<GetMemberForEditOutput> GetMemberForInfo(NullableIdInput input);
    }
}