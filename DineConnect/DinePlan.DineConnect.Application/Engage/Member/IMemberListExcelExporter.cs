﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.Member.Dtos.DinePlan.DineConnect.Engage.Dtos;
using DinePlan.DineConnect.Engage.Member.Implementation;

namespace DinePlan.DineConnect.Engage.Member
{
    public interface IMemberListExcelExporter
    {
        FileDto ExportToFile(List<MemberListDto> dtos);
        Task<FileDto> ExportToFile(GetMemberInput input, MemberAppService memberAppService);
        Task<FileDto> ExportMemberOrder(int? id, MemberAppService memberAppService);
    }
}
