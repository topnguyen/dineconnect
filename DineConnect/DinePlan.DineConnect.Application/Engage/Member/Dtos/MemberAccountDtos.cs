﻿
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;

namespace DinePlan.DineConnect.Engage.Member.Dtos
{
    [AutoMapFrom(typeof(MemberAccount))]
    public class MemberAccountListDto : FullAuditedEntityDto
    {
        public int? Id { get; set; }
        public int MemberId { get; set; }
        public string Number { get; set; }
        public string Name { get; set; }
        public string Reference { get; set; }
        public int AccountTypeId { get; set; }
        public MemberAccountType AccountType { get; set; }
        public decimal Balance { get; set; }
    }

    public class CreateOrUpdateMemberAccountInput : IInputDto
    {
        public MemberAccountTypeEnum MemberAccountType { get; set; }
        public int LocationId { get; set; }
        public MemberAccountEditDto MemberAccount { get; set; }
    }

    public class GetMemberAccountInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public string Operation { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    [AutoMapTo(typeof(MemberAccount))]
    public class MemberAccountEditDto
    {
        public int? Id { get; set; }
        public int MemberId { get; set; }
        public string Number { get; set; }
        public string Name { get; set; }
        public string Reference { get; set; }
        public int AccountTypeId { get; set; }
        public MemberAccountType AccountType { get; set; }
        public decimal Balance { get; set; }
        public bool IsActive { get; set; }
    }

    public class GetMemberAccountOutput : IOutputDto
    {
        public MemberAccountEditDto MemberAccount { get; set; }
    }
    public class AccountDepositInput
    {
        public int MemberId { get; set; }
        public string Reference { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
    }

    public class AccountWithdrawInput {
        public int MemberId { get; set; }
        public string Reference { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
    }
}
