﻿using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Engage.Member.Dtos
{
    [AutoMapTo(typeof(MemberAccountTransaction))]
    public class MemberAccountTransactionListDto
    {
        public int TenantId { get; set; }
        public int AccountId { get; set; }
        public decimal PreviousBalance { get; set; }
        public decimal TransactionalUnitCount { get; set; }
        public string Reference { get; set; }
        public string Description { get; set; }
        public string CreationTime { get; set; }
    }

    public class GetMemberAccountTransactionsInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public string Operation { get; set; }
        public bool IsDeleted { get; set; }
        public int AccountId { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
}
