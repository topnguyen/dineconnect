﻿
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.Gift;
using DinePlan.DineConnect.Engage.Gift.Dtos;
using System;

namespace DinePlan.DineConnect.Engage.Member.Dtos
{
    [AutoMapFrom(typeof(MembershipTier))]
    public class MembershipTierListDto : FullAuditedEntityDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int TierLevel { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public decimal AmountToEarnOnePoint { get; set; }
        public decimal RewardingPoints { get; set; }
        public GiftVoucherTypeListDto RewardingVoucher { get; set; }
        public int RewardingVoucherId { get; set; }
        public int RewardingVoucherCount { get; set; }
        public decimal CashPaymentDiscount { get; set; }
        public decimal CreditCardPaymentDiscount { get; set; }
        public string TextOfBenefits { get; set; }
        public string TextOfConditions { get; set; }
    }

    public class CreateOrUpdateMembershipTierInput : IInputDto
    {
        public MembershipTierEditInputDto MembershipTier { get; set; }
    }

    public class GetMembershipTierInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public string Operation { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    [AutoMapTo(typeof(MembershipTier))]
    public class MembershipTierEditDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int TierLevel { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public decimal RewardingPoints { get; set; }
        public GiftVoucherTypeListDto RewardingVoucher { get; set; }
        public int RewardingVoucherId { get; set; }
        public int RewardingVoucherCount { get; set; }
        public decimal CashPaymentDiscount { get; set; }
        public decimal CreditCardPaymentDiscount { get; set; }
        public string TextOfBenefits { get; set; }
        public string TextOfConditions { get; set; }
    }

    [AutoMapTo(typeof(MembershipTier))]
    public class MembershipTierEditInputDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int TierLevel { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public decimal RewardingPoints { get; set; }
        public int RewardingVoucherId { get; set; }
        public int RewardingVoucherCount { get; set; }
        public decimal CashPaymentDiscount { get; set; }
        public decimal CreditCardPaymentDiscount { get; set; }
        public string TextOfBenefits { get; set; }
        public string TextOfConditions { get; set; }
    }

    public class GetMembershipTierOutput : IOutputDto
    {
        public MembershipTierEditDto MembershipTier { get; set; }
    }
}
