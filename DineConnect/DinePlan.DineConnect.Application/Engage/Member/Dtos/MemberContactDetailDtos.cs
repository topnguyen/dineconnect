﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Engage.Member.Dtos
{
    [AutoMapFrom(typeof(MemberContactDetail))]
    public class MemberContactDetailListDto : FullAuditedEntityDto
    {
        public int? Id { get; set; }
        public int ConnectMemberId { get; set; }
        public ContactDetailType ContactDetailType { get; set; }
        public string ContactDetailTypeName { get { return this.ContactDetailType.ToString(); } }
        public string Detail { get; set; }
        public bool AcceptToCommunicate { get; set; }
    }

    public class CreateOrUpdateMemberContactDetailInput : IInputDto
    {
        public MemberContactDetailEditDto MemberContactDetail { get; set; }
    }

    public class GetMemberContactDetailInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int ContactMemberId { get; set; }
        public string Operation { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    public class GetMemberContactDetailOutput : IOutputDto
    {
        public MemberContactDetailEditDto MemberContactDetail { get; set; }
    }

    [AutoMapTo(typeof(MemberContactDetail))]
    public class MemberContactDetailEditDto
    {
        public int? Id { get; set; }
        public int ConnectMemberId { get; set; }
        public ContactDetailType ContactDetailType { get; set; }
        public string Detail { get; set; }
        public bool AcceptToCommunicate { get; set; }
        public string ContactDetailTypeName => ((ContactDetailType)ContactDetailType).ToString();
    }
}