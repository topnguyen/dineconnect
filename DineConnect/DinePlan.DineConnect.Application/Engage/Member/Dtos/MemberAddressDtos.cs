﻿
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;

namespace DinePlan.DineConnect.Engage.Member.Dtos
{
    [AutoMapTo(typeof(MemberAddress))]
    public class MemberAddressListDto
    {
        public int? Id { get; set; }
        public virtual int ConnectMemberId { get; set; }
        public virtual string Tag { get; set; }
        public virtual bool isPrimary { get; set; }
        public virtual int LocationId { get; set; }
        public virtual string Address { get; set; }
        public virtual string Locality { get; set; }
        public virtual string City { get; set; }
        public virtual string State { get; set; }
        public virtual string Country { get; set; }
        public virtual string PostalCode { get; set; }

    }

    public class CreateOrUpdateMemberAddressInput : IInputDto
    {
        public MemberAddressEditDto MemberAddress { get; set; }
    }

    public class GetMemberAddressInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int ConnectMemberId { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    [AutoMapTo(typeof(MemberAddress))]
    public class MemberAddressEditDto
    {
        public int? Id { get; set; }
        public virtual int ConnectMemberId { get; set; }
        public virtual string Tag { get; set; }
        public virtual bool isPrimary { get; set; }
        public virtual int LocationId { get; set; }
        public virtual string Address { get; set; }
        public virtual string Locality { get; set; }
        public virtual string City { get; set; }
        public virtual string State { get; set; }
        public virtual string Country { get; set; }
        public virtual string PostalCode { get; set; }

    }

    public class GetMemberAddressOutput : IOutputDto
    {
        public MemberAddressEditDto MemberAddress { get; set; }
    }
}
