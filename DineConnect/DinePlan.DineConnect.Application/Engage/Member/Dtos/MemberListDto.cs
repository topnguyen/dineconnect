﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.Gift.Dtos;
using DinePlan.DineConnect.Engage.Handler.Mapping;

namespace DinePlan.DineConnect.Engage.Member.Dtos
{
    namespace DinePlan.DineConnect.Engage.Dtos
    {
        [AutoMapFrom(typeof(ConnectMember))]
        public class MemberListDto : FullAuditedEntityDto
        {
            public virtual string MemberCode { get; set; }
            public virtual string Name { get; set; }
            public virtual string Locality { get; set; }
            public virtual DateTime? BirthDay { get; set; }
            public virtual string Address { get; set; }
            public virtual string City { get; set; }
            public virtual string State { get; set; }
            public virtual string Country { get; set; }
            public virtual string PostalCode { get; set; }
            public virtual string EmailId { get; set; }
            public virtual string PhoneNumber { get; set; }
            public virtual decimal TotalPoints { get; set; }
            public virtual DateTime? LastVisitDate { get; set; }
            public int? LocationRefId { get; set; }
            public virtual int? DefaultAddressRefId { get; set; }
            public virtual int EditAddressRefId { get; set; }
            public List<MemberAddressListDto> MemberAddresses { get; set; }
            public virtual int? MembershipTierId { get; set; }

        }

        [AutoMapTo(typeof(ConnectMember))]
        public class MemberEditDto
        {
            public int? Id { get; set; }

            public virtual string MemberCode { get; set; }
            public virtual string Name { get; set; }
            public virtual string LastName { get; set; }
            public virtual string Locality { get; set; }
            public virtual string Address { get; set; }
            public virtual string City { get; set; }
            public virtual string State { get; set; }
            public virtual string Country { get; set; }
            public virtual string PostalCode { get; set; }
            public virtual string EmailId { get; set; }
            public virtual string PhoneNumber { get; set; }
            public virtual string CustomData { get; set; }
            public virtual decimal TotalPoints { get; set; }
            public virtual DateTime LastVisitDate { get; set; }
            public virtual DateTime? BirthDay { get; set; }

            public virtual MemberPointEditDto MemberPoint { get; set; }
            public virtual int TenantId { get; set; }
            public int? LocationRefId { get; set; }
            public virtual int? DefaultAddressRefId { get; set; }
            public virtual int EditAddressRefId { get; set; }
            public virtual long? UserId { get; set; }
            public virtual string UserName { get; set; }
            public virtual ICollection<GiftVoucherEditDto> Vouchers { get; set; }
            public virtual bool IsMemberCodeAutomaticGenerateFlag { get; set; }
            public MemberEditDto()
            {
                Vouchers = new Collection<GiftVoucherEditDto>();
            }
            public virtual int? MembershipTierId { get; set; }

        }

        //[AutoMapTo(typeof(MemberAddress))]
        //public class MemberAddressListDto
        //{
        //    public int? Id { get; set; }
        //    public virtual int ConnectMemberId { get; set; }
        //    public virtual string Tag { get; set; }
        //    public virtual bool isPrimary { get; set; }
        //    public virtual int LocationId { get; set; }
        //    public virtual string Address { get; set; }
        //    public virtual string Locality { get; set; }
        //    public virtual string City { get; set; }
        //    public virtual string State { get; set; }
        //    public virtual string Country { get; set; }
        //    public virtual string PostalCode { get; set; }

        //}
        //[AutoMapTo(typeof(MemberAddress))]
        //public class MemberAddressEditDto
        //{
        //    public int? Id { get; set; }
        //    public virtual int ConnectMemberId { get; set; }
        //    public virtual string Tag { get; set; }
        //    public virtual bool isPrimary { get; set; }
        //    public virtual int LocationId { get; set; }
        //    public virtual string Address { get; set; }
        //    public virtual string Locality { get; set; }
        //    public virtual string City { get; set; }
        //    public virtual string State { get; set; }
        //    public virtual string Country { get; set; }
        //    public virtual string PostalCode { get; set; }

        //}
        public class GetMemberInput : PagedAndSortedInputDto, IShouldNormalize
        {
            public string Filter { get; set; }
            public string Operation { get; set; }

            public void Normalize()
            {
                if (string.IsNullOrEmpty(Sorting))
                {
                    Sorting = "Id";
                }
            }
        }
        public class MemberStatsDto
        {
            public PagedResultOutput<MemberPointListDto> Members { get; set; }
            public DashboardMemberDto DashBoardDto { get; set; }
        }
        public class DashboardMemberDto
        {
            public DashboardMemberDto()
            {
                TotalPointsAccumulated = 0M;
                TotalPointsRedemed = 0M;
                AccumulatedTicket = 0M;
                RedemedTicket = 0M;

            }
            public decimal TotalPointsAccumulated { get; set; }
            public decimal TotalPointsRedemed { get; set; }
            public decimal AccumulatedTicket { get; set; }
            public decimal RedemedTicket { get; set; }

        }
        public class GetMemberPointInput : PagedAndSortedInputDto, IShouldNormalize
        {
            public string Filter { get; set; }
            public string Operation { get; set; }
            public int MemberId { get; set; }

            public void Normalize()
            {
                if (string.IsNullOrEmpty(Sorting))
                {
                    Sorting = "Id";
                }
            }
        }

        public class ChangeMemberPoint
        {
            public decimal Points { get; set; }
            public int MemberId { get; set; }

        }
        public class RedemInputDto
        {
            public TicketEntityMap Member { get; set; }
            public decimal TotalPoints { get; set; }
            public decimal TotalValue { get; set; }
            public string TicketNumber { get; set; }
            public DateTime? TicketDate { get; set; }
            public decimal TicketTotal { get; set; }
            public Guid RedemptionId { get; set; }
            public int Location { get; set; }
            public int TenantId { get; set; }

            public RedemInputDto()
            {
                Member = new TicketEntityMap();
            }

        }

        public class RedeemPointsAsVouchersInput {
            public TicketEntityMap Member { get; set; }
            public int VoucherTypeId { get; set; }
            public int VoucherCount { get; set; }
            public string TicketNo { get; set; }

            public RedeemPointsAsVouchersInput()
            {
                Member = new TicketEntityMap();
            }
        }

        public class RedeemPointsAsVouchersOutput {
            public int MemberId { get; set; }
            public decimal UsedPointCount { get; set; }
            public decimal VoucherCount { get; set; }
            public string Message { get; set; }
        }
        public class MemberLocationInput
        {
            public int MemberId { get; set; }
            public int LocationId { get; set; }
        }
        public class RedemOutputDto
        {
            public decimal TotalPoints { get; set; }
            public decimal TotalValue { get; set; }
            public string Status { get; set; }
        }
        public class GetMemberForEditOutput : IOutputDto
        {
            public MemberEditDto Member { get; set; }
            public List<MemberContactDetailEditDto> MemberContactDetail { get; set; }
            public List<MemberAddressEditDto> MemberAddress { get; set; }
        }
        public class MemberOrderOutput : IOutputDto
        {
            public List<MenuListDto> Orders { get; set; }
            public int OrdersCount { get; set; }
            public List<MenuListDto> Cancel { get; set; }
            public int CancelCount { get; set; }


        }
        public class CreateOrUpdateMemberInput : IInputDto
        {
            [Required]
            public MemberEditDto Member { get; set; }
            public List<MemberContactDetailEditDto> MemberContactDetail { get; set; }
            public List<MemberAddressEditDto> MemberAddress { get; set; }
            public string TicketNumber { get; set; }
            public DateTime? TicketDate { get; set; }
            public decimal TicketTotal { get; set; }
            public int LocationId { get; set; }
            public bool SetAsNewDefaultAddress { get; set; }
            public int MemberAddressRefId { get; set; }
            public bool AddOneMoreNewAddress { get; set; }
        }


        [AutoMapFrom(typeof(MemberPoint))]
        public class MemberPointListDto : FullAuditedEntityDto
        {
            public virtual int MemberId { get; set; }
            public virtual int LocationId { get; set; }
            public virtual string TicketNumber { get; set; }
            public virtual DateTime TicketDate { get; set; }
            public virtual decimal TicketTotal { get; set; }
            public virtual bool IsPointAccumulation { get; set; }
            public virtual bool IsPointRedemption { get; set; }
            public virtual decimal PointsAccumulation { get; set; }
            public virtual decimal PointsAccumulationFactor { get; set; }

            public virtual decimal PointRedemption { get; set; }
            public virtual decimal PointRedemptionFactor { get; set; }

            public virtual decimal TotalPoints { get; set; }
            public virtual int TenantId { get; set; }
        }


        [AutoMapTo(typeof(MemberPoint))]
        public class MemberPointEditDto
        {
            public int? Id { get; set; }
            public virtual int MemberId { get; set; }
            public virtual int LocationId { get; set; }
            public virtual string TicketNumber { get; set; }
            public virtual DateTime TicketDate { get; set; }
            public virtual decimal TicketTotal { get; set; }

            public virtual bool IsPointAccumulation { get; set; }
            public virtual bool IsPointRedemption { get; set; }

            public virtual decimal PointsAccumulation { get; set; }
            public virtual decimal PointsAccumulationFactor { get; set; }

            public virtual decimal PointsRedemption { get; set; }
            public virtual decimal PointsRedemptionFactor { get; set; }

            public virtual decimal TotalPoints { get; set; }
            public virtual bool CalculationOnOrder { get; set; }

            public virtual int TenantId { get; set; }
        }

        public class MessageOutputDto : IOutputDto
        {
            public MessageOutputDto()
            {
                OutputMessageList = new List<string>();
                ErrorMessageList = new List<string>();
                SuccessMessageList = new List<string>();
            }
            public bool SuccessFlag { get; set; }
            public bool Exists { get; set; }
            public string ErrorMessage { get; set; }
            public List<string> OutputMessageList { get; set; }
            public List<string> ErrorMessageList { get; set; }
            public List<string> SuccessMessageList { get; set; }
            public int Id { get; set; }
            public bool DuplicateEmployeeError { get; set; }
            public string DuplicateEmployeeErrorMessage { get; set; }
            public bool PrWithIn2Years { get; set; }
            public string PrWithin2YearsAlertMessage { get; set; }
        }
    }
}