﻿
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Engage.Member.Dtos;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Engage.Member
{
    public interface IMemberContactDetailAppService : IApplicationService
    {
        Task<IdInput> CreateOrUpdateMemberContactDetail(CreateOrUpdateMemberContactDetailInput input);
        Task<PagedResultOutput<MemberContactDetailListDto>> GetAll(GetMemberContactDetailInput input);
        Task<GetMemberContactDetailOutput> GetMemberContactDetailForUpdate(NullableIdInput input);
        Task DeleteMemberContactDetail(NullableIdInput input);
    }
}


