﻿
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Engage.Member.Dtos;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Engage.Member
{
    public interface IMembershipTierAppService : IApplicationService
    {
        Task<IdInput> CreateOrUpdateMembershipTier(CreateOrUpdateMembershipTierInput input);
        Task<PagedResultOutput<MembershipTierListDto>> GetAll(GetMembershipTierInput input);
        Task<GetMembershipTierOutput> GetMembershipTierForUpdate(NullableIdInput input);
        Task DeleteMembershipTier(NullableIdInput input);
    }
}
