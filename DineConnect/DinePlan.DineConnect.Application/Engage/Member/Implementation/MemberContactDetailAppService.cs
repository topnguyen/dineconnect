﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Sync;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

using DinePlan.DineConnect.Engage.Member.Dtos;

namespace DinePlan.DineConnect.Engage.Member.Implementation
{
    public class MemberContactDetailAppService : DineConnectAppServiceBase, IMemberContactDetailAppService
    {
        private readonly ISyncAppService _syncAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<MemberContactDetail> _memberContactDetailRepository;
        public MemberContactDetailAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<MemberContactDetail> memberContactDetailRepository,
            ISyncAppService syncAppService)
        {
            _syncAppService = syncAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _memberContactDetailRepository = memberContactDetailRepository;
        }

        public async Task<IdInput> CreateOrUpdateMemberContactDetail(CreateOrUpdateMemberContactDetailInput input)
        {
            if (input.MemberContactDetail.Id.HasValue && input.MemberContactDetail.Id > 0)
            {
                return await UpdateMemberContactDetail(input);
            }
            else
            {
                return await CreateMemberContactDetail(input);
            }
        }

        protected async Task<IdInput> CreateMemberContactDetail(CreateOrUpdateMemberContactDetailInput input)
        {
            var memberContactDetail = input.MemberContactDetail.MapTo<MemberContactDetail>();
            var createResult = await _memberContactDetailRepository.InsertAndGetIdAsync(memberContactDetail);
            return new IdInput() { Id = createResult };
        }

        protected async Task<IdInput> UpdateMemberContactDetail(CreateOrUpdateMemberContactDetailInput input)
        {
            var memberContactDetail = input.MemberContactDetail.MapTo<MemberContactDetail>();
            var updateResutl = await _memberContactDetailRepository.UpdateAsync(memberContactDetail);
            return new IdInput() { Id = updateResutl.Id };
        }

        public async Task<PagedResultOutput<MemberContactDetailListDto>> GetAll(GetMemberContactDetailInput input)
        {
            var allItems = _memberContactDetailRepository.GetAll()
                .AsNoTracking()
                .Where(c=>c.ConnectMember.Id == input.ContactMemberId);

            var output = new PagedResultOutput<MemberContactDetailListDto>();

            var sortedItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            output.Items = sortedItems.MapTo<List<MemberContactDetailListDto>>();
            output.TotalCount = output.Items.Count;

            return output;
        }

        public async Task<GetMemberContactDetailOutput> GetMemberContactDetailForUpdate(NullableIdInput input)
        {
            var dbDaypart = _memberContactDetailRepository.GetAll()
                .AsNoTracking()
                .Where(d => d.Id == input.Id).FirstOrDefaultAsync();
            var output = new GetMemberContactDetailOutput();
            output.MemberContactDetail = dbDaypart.MapTo<MemberContactDetailEditDto>();
            return output;
        }

        public async Task DeleteMemberContactDetail(NullableIdInput input)
        {
            await _memberContactDetailRepository.DeleteAsync(input.Id ?? 0);
        }
    }
}
