﻿
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Sync;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

using DinePlan.DineConnect.Engage.Member.Dtos;

namespace DinePlan.DineConnect.Engage.Member.Implementation
{
    public class MembershipTierAppService : DineConnectAppServiceBase, IMembershipTierAppService
    {
        private readonly ISyncAppService _syncAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<MembershipTier> _membershipTierRepository;
        public MembershipTierAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<MembershipTier> membershipTierRepository,
            ISyncAppService syncAppService)
        {
            _syncAppService = syncAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _membershipTierRepository = membershipTierRepository;
        }

        public async Task<IdInput> CreateOrUpdateMembershipTier(CreateOrUpdateMembershipTierInput input)
        {
            if (input.MembershipTier.Id.HasValue && input.MembershipTier.Id > 0)
            {
                return await UpdateMembershipTier(input);
            }
            else
            {
                return await CreateMembershipTier(input);
            }
        }

        protected async Task<IdInput> CreateMembershipTier(CreateOrUpdateMembershipTierInput input)
        {
            try
            {
                var membershipTier = input.MembershipTier.MapTo<MembershipTier>();
                var createResult = await _membershipTierRepository.InsertAndGetIdAsync(membershipTier);
                return new IdInput() { Id = createResult };
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        protected async Task<IdInput> UpdateMembershipTier(CreateOrUpdateMembershipTierInput input)
        {
            try
            {
                var membershipTier = input.MembershipTier.MapTo<MembershipTier>();
                
                var updateResutl = await _membershipTierRepository.UpdateAsync(membershipTier);
                return new IdInput() { Id = updateResutl.Id };
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public async Task<PagedResultOutput<MembershipTierListDto>> GetAll(GetMembershipTierInput input)
        {
            try
            {
                var allItems = _membershipTierRepository.GetAll()
                    .AsNoTracking()
                    .Where(g => g.IsDeleted == input.IsDeleted);

                allItems = allItems.WhereIf(!input.Filter.IsNullOrEmpty(),
                    g => g.Name.Contains(input.Filter));

                var output = new PagedResultOutput<MembershipTierListDto>();

                var sortedItems = await allItems
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                output.Items = sortedItems.MapTo<List<MembershipTierListDto>>();
                output.TotalCount = output.Items.Count;

                return output;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public async Task<GetMembershipTierOutput> GetMembershipTierForUpdate(NullableIdInput input)
        {
            var dbMembershipTier = await _membershipTierRepository.GetAll()
                .Where(d => d.Id == input.Id).FirstOrDefaultAsync();
            var output = new GetMembershipTierOutput();
            output.MembershipTier = dbMembershipTier.MapTo<MembershipTierEditDto>();
            return output;
        }

        public async Task DeleteMembershipTier(NullableIdInput input)
        {
            await _membershipTierRepository.DeleteAsync(input.Id ?? 0);
        }
    }
}
