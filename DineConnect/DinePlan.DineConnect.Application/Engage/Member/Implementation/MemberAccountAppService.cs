﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

using global::DinePlan.DineConnect.Connect.Sync;
using global::DinePlan.DineConnect.Engage.Member.Dtos;
using System;

namespace DinePlan.DineConnect.Engage.Member.Implementation
{
    public class MemberAccountAppService : DineConnectAppServiceBase, IMemberAccountAppService
    {
        private readonly ISyncAppService _syncAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<MemberAccount> _memberAccountRepository;
        private readonly IRepository<MemberAccountType> _memberAccountTypeRepository;
        private readonly IRepository<MemberAccountTransaction> _memberAccountTransactionRepository;
        public MemberAccountAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<MemberAccount> memberAccountRepository,
            IRepository<MemberAccountType> memberAccountTypeRepository,
            IRepository<MemberAccountTransaction> memberAccountTransactionRepository,
            ISyncAppService syncAppService)
        {
            _syncAppService = syncAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _memberAccountRepository = memberAccountRepository;
            _memberAccountTypeRepository = memberAccountTypeRepository;
            _memberAccountTransactionRepository = memberAccountTransactionRepository;
        }

        public async Task<IdInput> CreateOrUpdateMemberAccount(CreateOrUpdateMemberAccountInput input)
        {
            int tenantId = AbpSession.TenantId ?? 0;

            var acctType = _memberAccountTypeRepository.GetAll()
                .AsNoTracking()
                .Where(t => t.Code == input.MemberAccountType.ToString()).FirstOrDefault();

            input.MemberAccount.AccountTypeId = acctType.Id;
            input.MemberAccount.AccountType = null;
            input.MemberAccount.Name += $" ({acctType.Description})";
            input.MemberAccount.Number = GenAccountNumber(tenantId, input.LocationId, acctType.Id);
            input.MemberAccount.IsActive = true;

            if (input.MemberAccount.Id.HasValue && input.MemberAccount.Id > 0)
            {
                return await UpdateMemberAccount(input);
            }
            else
            {
                return await CreateMemberAccount(input);
            }
        }

        protected async Task<IdInput> CreateMemberAccount(CreateOrUpdateMemberAccountInput input)
        {
            try
            {
                var memberAccount = input.MemberAccount.MapTo<MemberAccount>();
                var createResult = await _memberAccountRepository.InsertAndGetIdAsync(memberAccount);
                return new IdInput() { Id = createResult };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected async Task<IdInput> UpdateMemberAccount(CreateOrUpdateMemberAccountInput input)
        {
            var memberAccount = input.MemberAccount.MapTo<MemberAccount>();
            var updateResutl = await _memberAccountRepository.UpdateAsync(memberAccount);
            return new IdInput() { Id = updateResutl.Id };
        }

        public async Task<PagedResultOutput<MemberAccountListDto>> GetAll(GetMemberAccountInput input)
        {
            var allItems = _memberAccountRepository.GetAll()
                .Where(g => g.IsDeleted == input.IsDeleted);

            allItems = allItems.WhereIf(!input.Filter.IsNullOrEmpty(),
                g => g.Name.Contains(input.Filter)||
                g.Number.Contains(input.Filter)||
                input.Filter == "MID:" + g.MemberId.ToString());

            var output = new PagedResultOutput<MemberAccountListDto>();

            var sortedItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            output.Items = sortedItems.MapTo<List<MemberAccountListDto>>();
            output.TotalCount = output.Items.Count;

            return output;
        }

        public async Task<GetMemberAccountOutput> GetMemberAccountForUpdate(NullableIdInput input)
        {
            var dbDaypart = await _memberAccountRepository.GetAll()
                .Where(d => d.Id == input.Id).FirstOrDefaultAsync();
            var output = new GetMemberAccountOutput();
            output.MemberAccount = dbDaypart.MapTo<MemberAccountEditDto>();
            return output;
        }

        public async Task DeleteMemberAccount(NullableIdInput input)
        {
            await _memberAccountRepository.DeleteAsync(input.Id ?? 0);
        }

        private string GenAccountNumber(int tenantId, int locationId, int accountTypeId)
        {
            string tenantIdStr = tenantId.ToString("00");
            string accountTypeIdStr = (accountTypeId * 10).ToString("00");
            string locationIdStr = locationId.ToString("00");
            string section1 = $"{accountTypeIdStr}{tenantIdStr}{locationIdStr}";

            var getResult = _memberAccountRepository.GetAll().AsNoTracking()
                .Where(a => a.Number.Substring(0, 8) == section1).ToArray();

            Func<string, bool> existanceCheck = new Func<string, bool>((acN) => {
                var exists = _memberAccountRepository.GetAll()
                .AsNoTracking().Any(a => a.Number == acN);
                return exists;
            });

            string accountNo = "";
            string numberStr = "";
            int adder = 1;

            do
            {
                if (getResult != null && getResult.Length > 0)
                {
                    numberStr = (getResult.Select(a => int.Parse(a.Number.Substring(8, 3)))
                        .OrderByDescending(a => a)?.FirstOrDefault()) + adder.ToString("0000");
                }
                else
                {
                    numberStr = adder.ToString("0000");
                }

                long computing = long.Parse($"{accountTypeIdStr}{tenantIdStr}{locationIdStr}{numberStr}") % 7;
                accountNo = $"{section1}{numberStr}{computing}";
                adder++;
            } while (existanceCheck(accountNo));

            return accountNo;
        }

        public bool MemberHasAccount(MemberAccountTypeEnum accountType, int memberId)
        {

            Func<bool> func = new Func<bool>(() => {
                var has = _memberAccountRepository.GetAll()
                    .AsNoTracking()
                    .Any(a => a.MemberId == memberId &&
                        a.AccountTypeId == (int)accountType);
                return has;
            });

            bool check = func();
            //await Task.Run(()=> { check = func(); }).ConfigureAwait(false);

            return check;
        }

        public async Task DepositPoints(AccountDepositInput input)
        {
            await Deposit(input, MemberAccountTypeEnum.PNT);
        }

        public async Task WithdrawPoints(AccountWithdrawInput input)
        {
            await Withdraw(input, MemberAccountTypeEnum.PNT);
        }

        public async Task DepositVouchers(AccountDepositInput input)
        {
            await Deposit(input, MemberAccountTypeEnum.VCH);
        }

        public async Task WithdrawVouchers(AccountWithdrawInput input)
        {
            await Withdraw(input, MemberAccountTypeEnum.VCH);
        }

        private async Task Deposit(AccountDepositInput input, MemberAccountTypeEnum memberAccountType)
        {
            try
            {
                int accountTypeId = (int)memberAccountType;
                var account = _memberAccountRepository.GetAll()
                    .AsNoTracking()
                    .Where(a => a.MemberId == input.MemberId &&
                        a.AccountTypeId == accountTypeId).FirstOrDefault();

                MemberAccountTransaction tran = new MemberAccountTransaction
                {
                    AccountId = account.Id,
                    PreviousBalance = account.Balance,
                    TransactionalUnitCount = input.Amount,
                    Description = input.Description,
                    Reference= input.Reference
                };

                await _memberAccountTransactionRepository.InsertAsync(tran);
                account.Balance += input.Amount;
                account.AccountType = null;
                await _memberAccountRepository.UpdateAsync(account);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal CheckPoints(int memberId)
        {

            var getOutput = _memberAccountRepository.GetAll()
                .AsNoTracking()
                .Where(t => t.MemberId == memberId).FirstOrDefault();

            if (getOutput != null) return getOutput.Balance;

            return 0;

        }

        private async Task Withdraw(AccountWithdrawInput input, MemberAccountTypeEnum memberAccountType)
        {
            int accountTypeId = (int)memberAccountType;
            var account = _memberAccountRepository.GetAll()
                .AsNoTracking()
                .Where(a => a.MemberId == input.MemberId &&
                    a.AccountTypeId == accountTypeId).FirstOrDefault();


            MemberAccountTransaction tran = new MemberAccountTransaction
            {
                AccountId = account.Id,
                PreviousBalance = account.Balance,
                TransactionalUnitCount = input.Amount * -1,
                Description = input.Description,
                Reference = input.Reference
            };

            await _memberAccountTransactionRepository.InsertAsync(tran);
            account.Balance -= input.Amount;
            account.AccountType = null;
            await _memberAccountRepository.UpdateAsync(account);
        }

        public bool CheckVoucherAccount(int memberId) {
            var accountTypeId = (int)MemberAccountTypeEnum.VCH;
            var account = _memberAccountRepository.GetAll()
                .AsNoTracking()
                .Where(a => a.MemberId == memberId &&
                    a.AccountTypeId == accountTypeId).Any();
            return account;
        }

        public async Task<PagedResultOutput<MemberAccountTransactionListDto>> GetTransactions(GetMemberAccountTransactionsInput input)
        {
            var allItems = _memberAccountTransactionRepository.GetAll()
                .AsNoTracking()
                .Where(t => t.AccountId == input.AccountId);

            var output = new PagedResultOutput<MemberAccountTransactionListDto>();

            allItems = allItems.WhereIf(!input.Filter.IsNullOrEmpty(),
                g => g.Reference.Contains(input.Filter) ||
                g.Description.Contains(input.Filter));

            var sortedItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            output.Items = sortedItems.MapTo<List<MemberAccountTransactionListDto>>();
            output.TotalCount = output.Items.Count;

            return output;
        }
    }
}
