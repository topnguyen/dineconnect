﻿
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Sync;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

using DinePlan.DineConnect.Engage.Member.Dtos;
using System;

namespace DinePlan.DineConnect.Engage.Member.Implementation
{
    public class MemberAddressAppService : DineConnectAppServiceBase, IMemberAddressAppService
    {
        private readonly ISyncAppService _syncAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<MemberAddress> _memberAddressRepository;
        public MemberAddressAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<MemberAddress> memberAddressRepository,
            ISyncAppService syncAppService)
        {
            _syncAppService = syncAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _memberAddressRepository = memberAddressRepository;
        }

        public async Task<IdInput> CreateOrUpdateMemberAddress(CreateOrUpdateMemberAddressInput input)
        {
            if (input.MemberAddress.Id.HasValue && input.MemberAddress.Id > 0)
            {
                return await UpdateMemberAddress(input);
            }
            else
            {
                return await CreateMemberAddress(input);
            }
        }

        protected async Task<IdInput> CreateMemberAddress(CreateOrUpdateMemberAddressInput input)
        {
            var memberAddress = input.MemberAddress.MapTo<MemberAddress>();
            try
            {
                var createResult = await _memberAddressRepository.InsertAndGetIdAsync(memberAddress);
                input.MemberAddress.Id = createResult;
                UpdatePrimary(input);
                return new IdInput() { Id = createResult };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void UpdatePrimary(CreateOrUpdateMemberAddressInput input) {
            if (input.MemberAddress.isPrimary) {
                var addresses = _memberAddressRepository.GetAll()
                    .AsNoTracking()
                    .Where(a => a.ConnectMemberId == input.MemberAddress.ConnectMemberId).ToArray();
                foreach (var adr in addresses)
                {
                    if (adr.Id != input.MemberAddress.Id) {
                        adr.isPrimary = false;
                        _memberAddressRepository.Update(adr);
                    }
                }
            }
        }

        protected async Task<IdInput> UpdateMemberAddress(CreateOrUpdateMemberAddressInput input)
        {
            var memberAddress = input.MemberAddress.MapTo<MemberAddress>();
            var updateResutl = await _memberAddressRepository.UpdateAsync(memberAddress);
            UpdatePrimary(input);
            return new IdInput() { Id = updateResutl.Id };
        }

        public async Task<PagedResultOutput<MemberAddressListDto>> GetAll(GetMemberAddressInput input)
        {
            var allItems = _memberAddressRepository.GetAll().AsNoTracking()
                .Where(a=>a.ConnectMemberId == input.ConnectMemberId);

            var output = new PagedResultOutput<MemberAddressListDto>();

            var sortedItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            output.Items = sortedItems.MapTo<List<MemberAddressListDto>>();
            output.TotalCount = output.Items.Count;

            return output;
        }

        public async Task<GetMemberAddressOutput> GetMemberAddressForUpdate(NullableIdInput input)
        {
            var dbDaypart = await _memberAddressRepository.GetAll()
                .Where(d => d.Id == input.Id).FirstOrDefaultAsync();
            var output = new GetMemberAddressOutput();
            output.MemberAddress = dbDaypart.MapTo<MemberAddressEditDto>();
            return output;
        }

        public async Task DeleteMemberAddress(NullableIdInput input)
        {
            await _memberAddressRepository.DeleteAsync(input.Id ?? 0);
        }
    }
}
