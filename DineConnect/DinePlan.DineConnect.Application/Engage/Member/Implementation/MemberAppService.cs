﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Features;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using Castle.Core.Logging;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Authorization.Users.Dto;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.Handler.Mapping;
using DinePlan.DineConnect.Engage.Member.Dtos;
using DinePlan.DineConnect.Engage.Member.Dtos.DinePlan.DineConnect.Engage.Dtos;
using DinePlan.DineConnect.Features;
using DinePlan.DineConnect.Job.Engage;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Engage.Member.Implementation
{
    public class MemberAppService : DineConnectAppServiceBase, IMemberAppService
    {
        private readonly FeatureChecker _featureChecker;
        private readonly ILogger _logService;
        private readonly IMemberAccountAppService _memberAccountAppService;
        private readonly IRepository<MemberAddress> _memberaddressRepo;
        private readonly IRepository<MemberContactDetail> _memberContactDetailRepo;
        private readonly IMemberListExcelExporter _memberExporter;
        private readonly IMemberManager _memberManager;
        private readonly IRepository<MemberPoint> _memberPointRepo;
        private readonly IRepository<ConnectMember> _memberRepo;
        private readonly IRepository<Order> _orderRepo;
        private readonly IRepository<MenuItemPortion> _portionRepo;
        private readonly SettingManager _settingManager;
        private readonly ITenantSettingsAppService _tenantSettingsAppService;
        private readonly IRepository<Ticket> _tRepo;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IUserAppService _userAppService;
        private readonly IUserEmailer _userEmailer;
        private readonly UserManager _userManager;

        public MemberAppService(IMemberManager memberManager, IRepository<ConnectMember> memberRepo,
            IRepository<MenuItemPortion> portionRepo,
            IRepository<MemberPoint> memberPointRepo,
            IRepository<Ticket> tRepo,
            IMemberListExcelExporter memberExporter, ILogger logService, SettingManager settingManager,
            IRepository<Order> orderRepo, FeatureChecker featureChecker,
            IRepository<MemberAddress> memberaddressRepo,
            IRepository<MemberContactDetail> memberContactDetailRepo,
            IUnitOfWorkManager unitOfWorkManager,
            UserManager userManager,
            IUserEmailer userEmailer,
            IUserAppService userAppService,
            ITenantSettingsAppService tenantSettingsAppService,
            IMemberAccountAppService memberAccountAppService)
        {
            _memberManager = memberManager;
            _memberRepo = memberRepo;
            _memberExporter = memberExporter;
            _portionRepo = portionRepo;
            _tRepo = tRepo;
            _logService = logService;
            _settingManager = settingManager;
            _orderRepo = orderRepo;
            _memberaddressRepo = memberaddressRepo;
            _featureChecker = featureChecker;
            _memberPointRepo = memberPointRepo;
            _memberContactDetailRepo = memberContactDetailRepo;
            _unitOfWorkManager = unitOfWorkManager;
            _userManager = userManager;
            _userEmailer = userEmailer;
            _userAppService = userAppService;
            _tenantSettingsAppService = tenantSettingsAppService;
            _memberAccountAppService = memberAccountAppService;
        }

        public async Task<PagedResultOutput<MemberListDto>> GetAll(GetMemberInput input)
        {
            var allItems = _memberRepo.GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrWhiteSpace(),
                    p => p != null && (p.Name.Contains(input.Filter) ||
                                       p.MemberCode != null && p.MemberCode.Contains(input.Filter) ||
                                       p.EmailId != null && p.EmailId.Contains(input.Filter) ||
                                       p.PhoneNumber != null && p.PhoneNumber.Contains(input.Filter))
                );

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<MemberListDto>>();

            var loopcnt = 0;
            foreach (var mem in allListDtos)
            {
                var lstMemberAddresses =
                    await _memberaddressRepo.GetAll().Where(t => t.ConnectMemberId == mem.Id).ToListAsync();
                var address = lstMemberAddresses.Count() == 1
                    ? lstMemberAddresses[0]
                    : lstMemberAddresses.FirstOrDefault(t => t.Id == mem.DefaultAddressRefId);

                lstMemberAddresses.Remove(address);

                if (address == null)
                {
                    loopcnt++;
                    continue;
                }

                allListDtos[loopcnt].Address = address.Address;
                allListDtos[loopcnt].Locality = address.Locality;
                allListDtos[loopcnt].City = address.City;
                allListDtos[loopcnt].State = address.State;
                allListDtos[loopcnt].Country = address.Country;
                allListDtos[loopcnt].PostalCode = address.PostalCode;
                allListDtos[loopcnt].LocationRefId = address.LocationId;
                allListDtos[loopcnt].EditAddressRefId = address.Id;

                allListDtos[loopcnt].MemberAddresses = lstMemberAddresses.MapTo<List<MemberAddressListDto>>();
                loopcnt++;
            }

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<MemberListDto>(
                allItemCount,
                allListDtos
            );
        }

        public decimal CalculatePoint(decimal totalAmount, decimal pointAcc, decimal pointAccF, bool factor)
        {
            decimal points = 0;
            if (factor)
            {
                if (totalAmount > pointAccF)
                {
                    var factorf = (int)Math.Truncate(totalAmount / pointAccF);
                    points = factorf * pointAcc;
                }
            }
            else
            {
                points = totalAmount / pointAccF * pointAcc;
            }

            return points;
        }

        public async Task<FileDto> GetAllToExcel(GetMemberInput input)
        {
            return await _memberExporter.ExportToFile(input, this);
        }

        public async Task<GetMemberForEditOutput> GetMemberForEdit(NullableIdInput input)
        {
            MemberEditDto editDto;
            var editMemberContactDetailDto = new List<MemberContactDetailEditDto>();
            var editMemberAddressDto = new List<MemberAddressEditDto>();
            if (input.Id.HasValue)
            {
                var hDto = await _memberRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<MemberEditDto>();
                //MemberAddress address;

                //if (editDto.DefaultAddressRefId != null)
                //    address = await _memberaddressRepo.FirstOrDefaultAsync(t => t.Id == editDto.DefaultAddressRefId);
                //else
                //    address = await _memberaddressRepo.FirstOrDefaultAsync(t => t.ConnectMemberId == editDto.Id);

                //if (address != null)
                //{
                //    editDto.Address = address.Address;
                //    editDto.Locality = address.Locality;
                //    editDto.City = address.City;
                //    editDto.State = address.State;
                //    editDto.Country = address.Country;
                //    editDto.PostalCode = address.PostalCode;
                //    editDto.LocationRefId = address.LocationId;
                //}
                //if (string.IsNullOrEmpty(editDto.City))
                //{
                //    editDto.City = await _settingManager.GetSettingValueAsync(AppSettings.EngageSettings.DefaultCity);
                //}
                //if (string.IsNullOrEmpty(editDto.State))
                //{
                //    editDto.State = await _settingManager.GetSettingValueAsync(AppSettings.EngageSettings.DefaultState);
                //}
                //if (string.IsNullOrEmpty(editDto.Country))
                //{
                //    editDto.Country =
                //        await _settingManager.GetSettingValueAsync(AppSettings.EngageSettings.DefaultCountry);
                //}

                var rsmemberContactDetail =
                    await _memberContactDetailRepo.GetAllListAsync(t => t.ConnectMemberId == editDto.Id);
                editMemberContactDetailDto = rsmemberContactDetail.MapTo<List<MemberContactDetailEditDto>>();

                var rsmemberAddress = await _memberaddressRepo.GetAllListAsync(t => t.ConnectMemberId == editDto.Id);
                editMemberAddressDto = rsmemberAddress.MapTo<List<MemberAddressEditDto>>();
            }
            else
            {
                editDto = new MemberEditDto
                {
                    City = await _settingManager.GetSettingValueAsync(AppSettings.EngageSettings.DefaultCity),
                    State = await _settingManager.GetSettingValueAsync(AppSettings.EngageSettings.DefaultState),
                    Country = await _settingManager.GetSettingValueAsync(AppSettings.EngageSettings.DefaultCountry)
                };
                editMemberContactDetailDto = new List<MemberContactDetailEditDto>();
                editMemberAddressDto = new List<MemberAddressEditDto>();
            }

            var setting = await _tenantSettingsAppService.GetAllSettings();
            if (setting.Engage.AutoGenerateMemberCode)
                editDto.IsMemberCodeAutomaticGenerateFlag = true;
            else
                editDto.IsMemberCodeAutomaticGenerateFlag = false;
            return new GetMemberForEditOutput
            {
                Member = editDto,
                MemberContactDetail = editMemberContactDetailDto,
                MemberAddress = editMemberAddressDto
            };
        }

        public async Task<GetMemberForEditOutput> GetMemberForInfo(NullableIdInput input)
        {
            MemberEditDto editDto;
            var editMemberContactDetailDto = new List<MemberContactDetailEditDto>();
            var editMemberAddressDto = new List<MemberAddressEditDto>();
            if (input.Id.HasValue)
            {
                var hDto = await _memberRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<MemberEditDto>();

                var rsmemberContactDetail =
                    await _memberContactDetailRepo.GetAllListAsync(t => t.ConnectMemberId == editDto.Id);
                editMemberContactDetailDto = rsmemberContactDetail.MapTo<List<MemberContactDetailEditDto>>();

                var rsmemberAddress = await _memberaddressRepo.GetAllListAsync(t => t.ConnectMemberId == editDto.Id);
                editMemberAddressDto = rsmemberAddress.MapTo<List<MemberAddressEditDto>>();
            }
            else
            {
                editDto = new MemberEditDto
                {
                    City = await _settingManager.GetSettingValueAsync(AppSettings.EngageSettings.DefaultCity),
                    State = await _settingManager.GetSettingValueAsync(AppSettings.EngageSettings.DefaultState),
                    Country = await _settingManager.GetSettingValueAsync(AppSettings.EngageSettings.DefaultCountry)
                };
                editMemberContactDetailDto = new List<MemberContactDetailEditDto>();
                editMemberAddressDto = new List<MemberAddressEditDto>();
            }

            return new GetMemberForEditOutput
            {
                Member = editDto,
                MemberContactDetail = editMemberContactDetailDto,
                MemberAddress = editMemberAddressDto
            };
        }

        public async Task<MemberOrderOutput> GetOrders(NullableIdInput input)
        {
            var output = new MemberOrderOutput();

            if (input.Id.HasValue)
            {
                var allmember =
                    await _memberPointRepo.GetAll().Where(a => a.ConnectMemberId == input.Id.Value).ToListAsync();
                var tNumbers =
                    allmember.Select(
                            a => new MemberTicketLocation { LocationId = a.LocationId, TicketNumber = a.TicketNumber })
                        .ToList();
                if (tNumbers.Any())
                {
                    var tids = new List<int>();
                    foreach (var mtn in tNumbers)
                    {
                        var order = await _orderRepo.FirstOrDefaultAsync(
                            a =>
                                a.DecreaseInventory && a.CalculatePrice && a.Ticket.LocationId == mtn.LocationId &&
                                a.Ticket.TicketNumber == mtn.TicketNumber);

                        tids.Add(order.TicketId);
                    }

                    var allOrders = _orderRepo.GetAll().Where(a => tids.Contains(a.TicketId));
                    output.Orders = await GetMenuItemList(allOrders);
                    output.OrdersCount = output.Orders.Count();
                }
            }

            return output;
        }

        public async Task CreateOrUpdateMemberPoints(CreateOrUpdateMemberInput input)
        {
            var create = true;
            if (input?.Member?.MemberCode != null)
            {
                var mInput = await _memberRepo.FirstOrDefaultAsync(a => a.MemberCode.Equals
                    (input.Member.MemberCode));
                if (mInput != null)
                {
                    await UpdateMemberPoints(input, mInput.Id);
                    create = false;
                }
            }
            else if (input?.Member?.Id != null)
            {
                await UpdateMemberPoints(input, 0);
                create = false;
            }

            if (create) await CreateMemberPoints(input);
        }

        public async Task<MessageOutputDto> CreateOrUpdateMember(CreateOrUpdateMemberInput input)
        {
            var result = await ExistAllServerLevelBusinessRules(input);
            if (result.SuccessFlag)
            {
                if (input.Member.Id.HasValue) return await UpdateMember(input);
                if (!string.IsNullOrEmpty(input.Member.MemberCode))
                {
                    var exists =
                        await
                            _memberRepo.FirstOrDefaultAsync(
                                a => a.MemberCode != null && a.MemberCode.Equals(input.Member.MemberCode));
                    if (exists != null)
                    {
                        input.Member.Id = exists.Id;
                        return await UpdateMember(input);
                    }
                }
                else
                {
                    var newCode = await CreateMemberCode();
                    input.Member.MemberCode = newCode;
                }

                return await CreateMember(input);
            }

            result.Id = -1;
            return result;
        }

        public async Task DeleteMember(IdInput input)
        {
            await _memberRepo.DeleteAsync(input.Id);
        }

        public async Task DeleteMemberPoint(IdInput input)
        {
            var delete = true;
            var reason = "";
            if (input.Id > 0)
            {
                var mPoint = await _memberPointRepo.GetAsync(input.Id);
                if (mPoint != null)
                {
                    var ticket = await _tRepo.FirstOrDefaultAsync(
                        a => a.TicketNumber == mPoint.TicketNumber && mPoint.LocationId == a.LocationId);

                    if (ticket != null)
                    {
                        var allEntities =
                            JsonConvert.DeserializeObject<IList<TicketEntityMap>>(ticket.TicketEntities);
                        if (allEntities.Any())
                        {
                            var member = allEntities.FirstOrDefault(a => a.EntityTypeId == 1);
                            if (member != null)
                            {
                                delete = false;
                                reason = L("MemberFound") + " : " + member.EntityName;
                            }
                            else
                            {
                                delete = true;
                            }
                        }
                        else
                        {
                            delete = true;
                        }
                    }
                    else
                    {
                        delete = true;
                    }
                }
                else
                {
                    delete = false;
                    reason = L("PointNotExists");
                }
            }
            else
            {
                delete = false;
                reason = L("MemeberNotFound");
            }

            if (delete)
            {
                var mPoint = await _memberPointRepo.GetAsync(input.Id);
                var memer = await _memberRepo.FirstOrDefaultAsync(a => a.Id == mPoint.ConnectMemberId);
                if (mPoint != null && memer != null)
                {
                    if (mPoint.IsPointAccumulation) memer.TotalPoints -= mPoint.TotalPoints;

                    if (mPoint.IsPointRedemption) memer.TotalPoints += mPoint.TotalPoints;
                    await _memberPointRepo.DeleteAsync(input.Id);
                    await _memberRepo.UpdateAsync(memer);
                }
            }
            else
            {
                throw new UserFriendlyException(reason);
            }
        }

        public async Task<ListResultOutput<MemberListDto>> GetIds()
        {
            var lstMember = await _memberRepo.GetAll().ToListAsync();
            return new ListResultOutput<MemberListDto>(lstMember.MapTo<List<MemberListDto>>());
        }

        public async Task<RedemOutputDto> GetPointsDetailsByName(string input)
        {
            var inputDto = new RedemInputDto { Member = { EntityName = input } };
            return await GetMemberPoints(inputDto);
        }

        public async Task<RedemOutputDto> GetMemberPoints(RedemInputDto input)
        {
            var pointRed =
                await
                    _settingManager.GetSettingValueForTenantAsync<decimal>(
                        AppSettings.EngageSettings.PointsRedemption, input.TenantId);

            var pointRedF =
                await
                    _settingManager.GetSettingValueForTenantAsync<decimal>(
                        AppSettings.EngageSettings.PointsRedemptionFactor, input.TenantId);

            var returnDto = new RedemOutputDto { Status = L("MemeberNotFound") };

            if (!string.IsNullOrWhiteSpace(input?.Member?.EntityName))
            {
                var member = await _memberRepo.FirstOrDefaultAsync(a => a.MemberCode.Equals(input.Member.EntityName));

                if (member != null)
                {
                    returnDto.TotalPoints = member.TotalPoints;
                    returnDto.TotalValue = member.TotalPoints / pointRedF * pointRed;
                    returnDto.Status = "";
                }
            }

            return returnDto;
        }

        public async Task<RedemOutputDto> GetMemberPointsForTotal(RedemInputDto input)
        {
            var pointAcc = await
                _settingManager.GetSettingValueForTenantAsync<decimal>(
                    AppSettings.EngageSettings.PointsAccumulation, input.TenantId);
            var pointAccF =
                await
                    _settingManager.GetSettingValueForTenantAsync<decimal>(
                        AppSettings.EngageSettings.PointsAccumulationFactor, input.TenantId);
            var pointRed =
                await
                    _settingManager.GetSettingValueForTenantAsync<decimal>(
                        AppSettings.EngageSettings.PointsRedemption, input.TenantId);
            var pointRedF =
                await
                    _settingManager.GetSettingValueForTenantAsync<decimal>(
                        AppSettings.EngageSettings.PointsRedemptionFactor, input.TenantId);

            var pointFac =
                await
                    _settingManager.GetSettingValueForTenantAsync<bool>(
                        AppSettings.EngageSettings.CalculatePointsOnMultiple, input.TenantId);


            var returnDto = new RedemOutputDto { Status = L("MemeberNotFound") };

            if (!string.IsNullOrWhiteSpace(input?.Member?.EntityName))
            {
                var member = await _memberRepo.FirstOrDefaultAsync(a => a.MemberCode.Equals(input.Member.EntityName));

                if (member != null)
                {
                    returnDto.TotalPoints = CalculatePoint(input.TicketTotal, pointAcc, pointAccF, pointFac);
                    returnDto.TotalValue = returnDto.TotalPoints / pointRedF * pointRed;
                    returnDto.Status = "";
                }
            }

            return returnDto;
        }

        public async Task CreateMemberAndPoints(InputMemberPointCalculation args)
        {
            try
            {
                if (await _featureChecker.IsEnabledAsync(args.TenantId, AppFeatures.Engage) &&
                    !string.IsNullOrEmpty(args.TicketEntities))
                {
                    var allEntities =
                        JsonConvert.DeserializeObject<IList<TicketEntityMap>>(args.TicketEntities);
                    if (allEntities.Any())
                    {
                        var entity = allEntities.SingleOrDefault(a => a.EntityTypeId.Equals(1));
                        if (entity != null)
                        {
                            var member = new MemberEditDto();
                            var memberPoint = new MemberPointEditDto();
                            member.MemberPoint = memberPoint;
                            var mInput = new CreateOrUpdateMemberInput
                            {
                                Member = member,
                                TicketNumber = args.TicketNumber,
                                TicketDate = args.CreatedTime,
                                TicketTotal = args.TotalAmount,
                                LocationId = args.LocationId
                            };
                            var memberName = entity.GetCustomData("Name");
                            if (string.IsNullOrEmpty(memberName))
                                memberName = entity.GetCustomData("Customer Name");

                            member.MemberCode = entity.EntityName.Trim();
                            member.Name = memberName?.Trim() ?? "";
                            member.Address = entity.GetCustomData("Address") ?? entity.GetCustomData("Address").Trim();
                            member.City = entity.GetCustomData("City") ?? entity.GetCustomData("Address").Trim();
                            member.State = entity.GetCustomData("State") ?? entity.GetCustomData("Address").Trim();
                            member.Country = entity.GetCustomData("Country") ?? entity.GetCustomData("Address").Trim();
                            member.PostalCode = entity.GetCustomData("PostalCode") ??
                                                entity.GetCustomData("Address").Trim();
                            member.EmailId = entity.GetCustomData("EmailId") ?? entity.GetCustomData("Address").Trim();
                            member.PhoneNumber = entity.GetCustomData("PhoneNumber") ??
                                                 entity.GetCustomData("Address").Trim();
                            member.TenantId = args.TenantId;
                            member.CustomData = entity.EntityCustomData;
                            memberPoint.IsPointAccumulation = false;
                            member.TotalPoints = 0;

                            if (
                                await
                                    _settingManager.GetSettingValueForTenantAsync<bool>(
                                        AppSettings.EngageSettings.OnBoardMemberFromPos, args.TenantId))
                            {
                                var dto = member.MapTo<ConnectMember>();
                                dto.LastVisitDate = null;
                                dto.TenantId = member.TenantId;
                                CheckErrors(await _memberManager.CreateOrUpdate(dto));
                            }

                            if (
                                await
                                    _settingManager.GetSettingValueForTenantAsync<bool>(
                                        AppSettings.EngageSettings.CalculatePoints, args.TenantId))
                            {
                                var pointAcc =
                                    await
                                        _settingManager.GetSettingValueForTenantAsync<decimal>(
                                            AppSettings.EngageSettings.PointsAccumulation, args.TenantId);
                                var pointAccF =
                                    await
                                        _settingManager.GetSettingValueForTenantAsync<decimal>(
                                            AppSettings.EngageSettings.PointsAccumulationFactor, args.TenantId);
                                var pointRed =
                                    await
                                        _settingManager.GetSettingValueForTenantAsync<decimal>(
                                            AppSettings.EngageSettings.PointsRedemption, args.TenantId);
                                var pointRedF =
                                    await
                                        _settingManager.GetSettingValueForTenantAsync<decimal>(
                                            AppSettings.EngageSettings.PointsRedemptionFactor, args.TenantId);

                                var pointFac =
                                    await
                                        _settingManager.GetSettingValueForTenantAsync<bool>(
                                            AppSettings.EngageSettings.CalculatePointsOnMultiple, args.TenantId);

                                member.TotalPoints = CalculatePoint(args.TotalAmount, pointAcc, pointAccF, pointFac);
                                memberPoint.IsPointAccumulation = true;
                                memberPoint.IsPointRedemption = false;
                                memberPoint.PointsAccumulation = pointAcc;
                                memberPoint.PointsRedemption = pointRed;
                                memberPoint.PointsAccumulationFactor = pointAccF;
                                memberPoint.PointsRedemptionFactor = pointRedF;
                                memberPoint.TicketTotal = args.TotalAmount;
                                memberPoint.TicketNumber = args.TicketNumber;
                                memberPoint.TicketDate = args.CreatedTime;
                                memberPoint.TenantId = args.TenantId;
                                memberPoint.LocationId = args.LocationId;
                                memberPoint.CalculationOnOrder = args.CalOnOrder;
                                await CreateOrUpdateMemberPoints(mInput);
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Fatal("Member Job ", exception);
                throw exception;
            }
        }

        public async Task UpdateMemberPoints(ChangeMemberPoint myPoints)
        {
            var member = await _memberRepo.GetAsync(myPoints.MemberId);
            if (member != null)
            {
                member.TotalPoints = myPoints.Points;
                await _memberRepo.UpdateAsync(member);
            }
        }

        [Obsolete]
        public async Task<RedemOutputDto> RedemPoints(RedemInputDto input)
        {
            var returnDto = new RedemOutputDto();
            var pointAcc =
                await
                    _settingManager.GetSettingValueForTenantAsync<decimal>(
                        AppSettings.EngageSettings.PointsAccumulation, input.TenantId);
            var pointAccF =
                await
                    _settingManager.GetSettingValueForTenantAsync<decimal>(
                        AppSettings.EngageSettings.PointsAccumulationFactor, input.TenantId);
            var pointRed =
                await
                    _settingManager.GetSettingValueForTenantAsync<decimal>(
                        AppSettings.EngageSettings.PointsRedemption, input.TenantId);
            var pointRedF =
                await
                    _settingManager.GetSettingValueForTenantAsync<decimal>(
                        AppSettings.EngageSettings.PointsRedemptionFactor, input.TenantId);

            if (string.IsNullOrWhiteSpace(input?.Member?.EntityName))
            {
                returnDto.Status = L("InputIsNoProper");
                return returnDto;
            }

            var memberOut = await _memberRepo.FirstOrDefaultAsync(a => a.MemberCode.Equals(input.Member.EntityName));
            if (memberOut == null)
            {
                returnDto.Status = L("MemeberNotFound");
                return returnDto;
            }

            if (memberOut.TotalPoints < input.TotalPoints)
            {
                returnDto.Status = L("InputPointsAreMoreThanTotalPoints");
                return returnDto;
            }

            var lastMemberPoint = _memberPointRepo.GetAll().Where(a => a.ConnectMemberId == memberOut.Id &&
                                                                       a.TicketNumber != null &&
                                                                       input.TicketNumber != null &&
                                                                       a.TicketNumber.Equals(input.TicketNumber) &&
                                                                       a.RedemtionId == input.RedemptionId);


            if (lastMemberPoint.Any())
            {
                var lastPointValue = lastMemberPoint.Last();
                memberOut.TotalPoints += lastPointValue.TotalPoints;
            }

            var rAmount = input.TotalPoints / pointRedF * pointRed;
            returnDto.TotalValue = rAmount;
            returnDto.TotalPoints = input.TotalPoints;
            memberOut.TotalPoints -= input.TotalPoints;

            if (lastMemberPoint.Any())
            {
                var lastPointValue = lastMemberPoint.Last();
                lastPointValue.TotalPoints = input.TotalPoints;
                lastPointValue.IsPointAccumulation = false;
                lastPointValue.IsPointRedemption = true;
                lastPointValue.PointsAccumulation = pointAcc;
                lastPointValue.PointsAccumulationFactor = pointAccF;
                lastPointValue.PointsRedemption = pointRed;
                lastPointValue.PointsRedemptionFactor = pointRedF;
                lastPointValue.TicketNumber = input.TicketNumber;

                _memberPointRepo.Update(lastPointValue);
            }
            else
            {
                var memberPoint = new MemberPoint
                {
                    TotalPoints = input.TotalPoints,
                    TicketNumber = input.TicketNumber,
                    TicketDate = input.TicketDate,
                    TicketTotal = input.TicketTotal,
                    LocationId = input.Location,
                    IsPointAccumulation = false,
                    IsPointRedemption = true,
                    PointsAccumulation = pointAcc,
                    PointsAccumulationFactor = pointAccF,
                    PointsRedemption = pointRed,
                    PointsRedemptionFactor = pointRedF,
                    RedemtionId = input.RedemptionId,
                    ConnectMemberId = memberOut.Id
                };
                _memberPointRepo.InsertAndGetId(memberPoint);
            }

            await _memberRepo.UpdateAsync(memberOut);
            return returnDto;
        }

        public Task<RedemOutputDto> CancelPoint(RedemInputDto input)
        {
            return null;
        }

        public async Task<MemberEditDto> ApiMemberInfoByUser(IdInput<long> idInput)
        {
            var allInfo = await _memberRepo.GetAllListAsync(a => a.UserId != null && a.UserId == idInput.Id);
            if (allInfo.Any())
            {
                var firstMember = allInfo.First();
                var editDto = firstMember.MapTo<MemberEditDto>();
                MemberAddress address;

                if (editDto.DefaultAddressRefId != null)
                    address = await _memberaddressRepo.FirstOrDefaultAsync(t => t.Id == editDto.DefaultAddressRefId);
                else
                    address = await _memberaddressRepo.FirstOrDefaultAsync(t => t.ConnectMemberId == editDto.Id);

                if (address != null)
                {
                    editDto.Address = address.Address;
                    editDto.Locality = address.Locality;
                    editDto.City = address.City;
                    editDto.State = address.State;
                    editDto.Country = address.Country;
                    editDto.PostalCode = address.PostalCode;
                    editDto.LocationRefId = address.LocationId;
                }

                return editDto;
            }

            return null;
        }

        public async Task UpdateMemberLastLocation(MemberLocationInput input)
        {
            var member = await _memberRepo.GetAsync(input.MemberId);

            if (member?.DefaultAddressRefId != null)
            {
                var address = await _memberaddressRepo.GetAsync(member.DefaultAddressRefId.Value);
                if (address != null) address.LocationId = input.LocationId;
            }
        }

        public async Task<MemberStatsDto> GetAllMemberPoints(GetMemberPointInput input)
        {
            var stats = new MemberStatsDto();
            var dash = new DashboardMemberDto();
            stats.DashBoardDto = dash;

            if (input.MemberId > 0)
            {
                var allItems = _memberPointRepo.GetAll().Where(a => a.ConnectMemberId.Equals(input.MemberId));

                var sortMenuItems = await allItems
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var allListDtos = sortMenuItems.MapTo<List<MemberPointListDto>>();

                var allItemCount = await allItems.CountAsync();


                stats.Members = new PagedResultOutput<MemberPointListDto>(
                    allItemCount,
                    allListDtos
                );

                dash.AccumulatedTicket = allItems.Count(a => a.IsPointAccumulation);
                dash.RedemedTicket = allItems.Count(a => a.IsPointRedemption);

                if (dash.AccumulatedTicket > 0M)
                    dash.TotalPointsAccumulated = allItems.Where(a => a.IsPointAccumulation).Sum(a => a.TotalPoints);
                if (dash.RedemedTicket > 0M)
                    dash.TotalPointsRedemed = allItems.Where(a => a.IsPointRedemption).Sum(a => a.TotalPoints);
            }

            return stats;
        }

        public ListResultDto<ComboboxItemDto> GetContactDetailsTypes()
        {
            var returnList = new List<ComboboxItemDto>();

            var i = 1;
            foreach (var name in Enum.GetNames(typeof(ContactDetailType)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }

            return new ListResultOutput<ComboboxItemDto>(returnList);
        }

        private async Task<string> CreateMemberCode()
        {
            var codeFormatFromSetting =
                await SettingManager.GetSettingValueAsync(AppSettings.EngageSettings.CodeFormat);
            var codeFormat = !string.IsNullOrEmpty(codeFormatFromSetting) ? codeFormatFromSetting : "CF";
            var members = await _memberRepo.GetAll().Where(x => x.MemberCode.StartsWith(codeFormat))
                .Select(x => x.MemberCode).ToListAsync();
            var listNumber = new List<long>();
            foreach (var item in members)
                if (item.Length > codeFormat.Length)
                {
                    var number = Convert.ToInt64(item.Substring(codeFormat.Length));
                    listNumber.Add(number);
                }

            var maxNumber = 0M;
            if (listNumber.Count > 0) maxNumber = listNumber.Max() + 1;
            var newCode = codeFormat + maxNumber;
            return newCode;
        }

        public async Task<MessageOutputDto> ExistAllServerLevelBusinessRules(CreateOrUpdateMemberInput input)
        {
            var errorMessage = "";
            var messageOutput = new MessageOutputDto();
            var errorList = new List<string>();
            if (input.MemberContactDetail != null && input.MemberContactDetail.Count > 0)
            {
                var exists = input.MemberContactDetail.Where(t =>
                    t.ContactDetailType == ContactDetailType.HandPhone ||
                    t.ContactDetailType == ContactDetailType.Email).ToList();
                if (exists.Count >= 2)
                {
                    messageOutput.SuccessFlag = true;
                }
                else
                {
                    messageOutput.SuccessFlag = false;
                    errorMessage = L("HandPhoneAndEmailRequired");
                    errorList.Add(errorMessage);
                }
            }

            messageOutput.SuccessFlag = true;

            var memberCode = await _memberRepo.GetAll()
                .Where(x => x.MemberCode == input.Member.MemberCode && x.Id != input.Member.Id).ToListAsync();
            if (memberCode.Count > 0)
                input.Member.Id = memberCode.First().Id;
            else
                messageOutput.Id = 0;
            messageOutput.ErrorMessageList = errorList;
            return messageOutput;
        }

        private async Task CreatePointMemberAccount(ConnectMember member, int locationId)
        {
            var createMemberAccountInput = new CreateOrUpdateMemberAccountInput
            {
                MemberAccountType = MemberAccountTypeEnum.PNT,
                LocationId = locationId,
                MemberAccount = new MemberAccountEditDto
                {
                    Balance = 0,
                    Id = 0,
                    MemberId = member.Id,
                    Name = $"{member.Name} {member.LastName}"
                }
            };

            await _memberAccountAppService.CreateOrUpdateMemberAccount(createMemberAccountInput);
        }

        private async Task CreateVoucherMemberAccount(ConnectMember member, int locationId)
        {
            var createMemberAccountInput = new CreateOrUpdateMemberAccountInput
            {
                MemberAccountType = MemberAccountTypeEnum.VCH,
                LocationId = locationId,
                MemberAccount = new MemberAccountEditDto
                {
                    Balance = 0,
                    Id = 0,
                    MemberId = member.Id,
                    Name = $"{member.Name} {member.LastName}"
                }
            };

            await _memberAccountAppService.CreateOrUpdateMemberAccount(createMemberAccountInput);
        }

        private bool HasPointMemberAccount(int memberId)
        {
            var check = _memberAccountAppService
                .MemberHasAccount(MemberAccountTypeEnum.PNT, memberId);
            return check;
        }

        private bool HasVoucherMemberAccount(int memberId)
        {
            var check = _memberAccountAppService
                .MemberHasAccount(MemberAccountTypeEnum.VCH, memberId);
            return check;
        }

        public async Task<FileDto> GetMemberOrderExport(GetMemberForEditOutput input)
        {
            if (input.Member.Id.HasValue)
                return await _memberExporter.ExportMemberOrder(input.Member.Id, this);
            return null;
        }

        private async Task<List<MenuListDto>> GetMenuItemList(IQueryable<Order> allOrders)
        {
            var allOrdersList = allOrders.MapTo<List<OrderListDto>>();
            var output = from c in allOrdersList
                group c by new { c.MenuItemPortionId }
                into g
                select new { MenuItemPortion = g.Key.MenuItemPortionId, Items = g };

            var outputDtos = new List<MenuListDto>();

            foreach (var dto in output)
            {
                var portion = await _portionRepo.GetAsync(dto.MenuItemPortion);

                if (portion != null)
                    outputDtos.Add(new MenuListDto
                    {
                        MenuItemPortionId = dto.MenuItemPortion,
                        MenuItemPortionName = portion.Name,
                        MenuItemId = portion.MenuItemId,
                        MenuItemName = portion.MenuItem.Name,
                        CategoryId = portion.MenuItem.CategoryId ?? 0,
                        CategoryName = portion.MenuItem.Category.Name ?? "",
                        Quantity = dto.Items.Sum(a => a.Quantity),
                        Price = dto.Items.Sum(a => a.Quantity * a.Price) / dto.Items.Sum(b => b.Quantity)
                    });
            }

            outputDtos = outputDtos.OrderByDescending(a => a.Quantity).ToList();

            return outputDtos;
        }

        protected virtual async Task<MessageOutputDto> UpdateMember(CreateOrUpdateMemberInput input)
        {
            try
            {
                var member = await _memberRepo.GetAsync(input.Member.Id.Value);

                var dto = input.Member;
                member.MemberCode = dto.MemberCode;
                member.Name = dto.Name;
                member.LastName = dto.LastName;
                member.EmailId = dto.EmailId;
                member.PhoneNumber = dto.PhoneNumber;
                member.CustomData = dto.CustomData;
                member.LastVisitDate = DateTime.Now;
                member.MembershipTierId = dto.MembershipTierId;

                if (dto.BirthDay != DateTime.MinValue && dto.BirthDay != null)
                    member.BirthDay = dto.BirthDay;

                if (!HasPointMemberAccount(member.Id)) await CreatePointMemberAccount(member, input.LocationId);

                if (!HasVoucherMemberAccount(member.Id)) await CreateVoucherMemberAccount(member, input.LocationId);

                await _memberRepo.InsertOrUpdateAndGetIdAsync(member);

                #region Contact Detail

                var refidsToBeRetained = new List<int>();
                if (input.MemberContactDetail != null && input.MemberContactDetail.Count > 0)
                    foreach (var lst in input.MemberContactDetail)
                        using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                        {
                            var itemdetail = await _memberContactDetailRepo.FirstOrDefaultAsync
                                (u => u.ConnectMemberId == input.Member.Id.Value && u.Id == lst.Id);
                            if (itemdetail == null)
                            {
                                var t = new MemberContactDetail
                                {
                                    ConnectMemberId = member.Id,
                                    ContactDetailType = lst.ContactDetailType,
                                    Detail = lst.Detail,
                                    AcceptToCommunicate = lst.AcceptToCommunicate
                                };
                                var ret = await _memberContactDetailRepo.InsertOrUpdateAndGetIdAsync(t);
                                refidsToBeRetained.Add(ret);
                            }
                            else
                            {
                                itemdetail.ContactDetailType = lst.ContactDetailType;
                                itemdetail.Detail = lst.Detail;
                                itemdetail.AcceptToCommunicate = lst.AcceptToCommunicate;
                                refidsToBeRetained.Add(itemdetail.Id);
                            }
                        }

                var delList = await _memberContactDetailRepo.GetAll().Where(a =>
                    a.ConnectMemberId == input.Member.Id.Value && !refidsToBeRetained.Contains(a.Id)).ToListAsync();

                var firstFlag = true;
                var cnt = 1;
                foreach (var lst in delList) await _memberContactDetailRepo.DeleteAsync(lst.Id);

                #endregion

                #region Member Address

                var refidToBeRetained = new List<int>();
                if (input.MemberAddress != null && input.MemberAddress.Count > 0)
                    foreach (var lst in input.MemberAddress)
                        using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                        {
                            var itemdetail = await _memberaddressRepo.FirstOrDefaultAsync
                                (u => u.ConnectMemberId == input.Member.Id.Value && u.Id == lst.Id);
                            if (itemdetail == null)
                            {
                                var t = new MemberAddress
                                {
                                    ConnectMemberId = member.Id,
                                    Tag = lst.Tag,
                                    Address = lst.Address,
                                    Locality = lst.Locality,
                                    City = lst.City,
                                    PostalCode = lst.PostalCode,
                                    State = lst.State,
                                    Country = lst.Country,
                                    isPrimary = lst.isPrimary
                                };
                                var ret = await _memberaddressRepo.InsertOrUpdateAndGetIdAsync(t);
                                refidToBeRetained.Add(ret);
                            }
                            else
                            {
                                itemdetail.Tag = lst.Tag;
                                itemdetail.Address = lst.Address;
                                itemdetail.Locality = lst.Locality;
                                itemdetail.City = lst.City;
                                itemdetail.PostalCode = lst.PostalCode;
                                itemdetail.State = lst.State;
                                itemdetail.Country = lst.Country;
                                itemdetail.isPrimary = lst.isPrimary;
                                refidToBeRetained.Add(itemdetail.Id);
                            }
                        }

                var deleteList = await _memberaddressRepo.GetAll().Where(a =>
                    a.ConnectMemberId == input.Member.Id.Value && !refidToBeRetained.Contains(a.Id)).ToListAsync();

                foreach (var lst in deleteList) await _memberaddressRepo.DeleteAsync(lst.Id);

                #endregion

                //MemberAddress memaddresstoedit;

                //if (input.AddOneMoreNewAddress == false)
                //    memaddresstoedit =
                //        await
                //            _memberaddressRepo.FirstOrDefaultAsync(
                //                t => t.Id == input.Member.EditAddressRefId && t.ConnectMemberId == member.Id);
                //else
                //    memaddresstoedit = null;

                //if (memaddresstoedit == null)
                //{
                //    var newaddressDto = new MemberAddress
                //    {
                //        ConnectMemberId = member.Id,
                //        LocationId = input.Member.LocationRefId,
                //        Locality = input.Member.Locality,
                //        Address = input.Member.Address,
                //        City = input.Member.City,
                //        State = input.Member.State,
                //        Country = input.Member.Country,
                //        PostalCode = input.Member.PostalCode
                //    };
                //    await _memberaddressRepo.InsertAndGetIdAsync(newaddressDto);
                //    //       if (input.SetAsNewDefaultAddress)
                //    member.DefaultAddressRefId = newaddressDto.Id;
                //}
                //else
                //{
                //    memaddresstoedit.LocationId = memaddresstoedit.LocationId;
                //    memaddresstoedit.LocationId = input.Member.LocationRefId;
                //    memaddresstoedit.Locality = input.Member.Locality;
                //    memaddresstoedit.Address = input.Member.Address;
                //    memaddresstoedit.City = input.Member.City;
                //    memaddresstoedit.State = input.Member.State;
                //    memaddresstoedit.Country = input.Member.Country;
                //    memaddresstoedit.PostalCode = input.Member.PostalCode;

                //    member.DefaultAddressRefId = memaddresstoedit.Id;
                //    await _memberaddressRepo.InsertOrUpdateAndGetIdAsync(memaddresstoedit);
                //}
                return new MessageOutputDto { Id = member.Id, SuccessFlag = true };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.InnerException + ex.Message);
            }
        }

        protected virtual async Task<MessageOutputDto> CreateMember(CreateOrUpdateMemberInput input)
        {
            var dto = input.Member.MapTo<ConnectMember>();
            dto.LastVisitDate = null;
            dto.TenantId = input.Member.TenantId;
            CheckErrors(await _memberManager.CreateSync(dto));

            await CreatePointMemberAccount(dto, input.LocationId);

            if (!HasPointMemberAccount(dto.Id)) await CreatePointMemberAccount(dto, input.LocationId);

            if (!HasVoucherMemberAccount(dto.Id)) await CreateVoucherMemberAccount(dto, input.LocationId);

            if (input.MemberContactDetail != null && input.MemberContactDetail.Count > 0)
                foreach (var lst in input.MemberContactDetail)
                {
                    var det = new MemberContactDetail
                    {
                        ConnectMemberId = dto.Id,
                        ContactDetailType = lst.ContactDetailType,
                        Detail = lst.Detail,
                        AcceptToCommunicate = lst.AcceptToCommunicate
                    };
                    await _memberContactDetailRepo.InsertOrUpdateAndGetIdAsync(det);
                }

            if (input.MemberAddress != null && input.MemberAddress.Count > 0)
                foreach (var lst in input.MemberAddress)
                {
                    var det = new MemberAddress
                    {
                        ConnectMemberId = dto.Id,
                        LocationId = lst.LocationId,
                        Locality = lst.Locality,
                        Address = lst.Address,
                        City = lst.City,
                        State = lst.State,
                        Country = lst.Country,
                        PostalCode = lst.PostalCode,
                        Tag = lst.Tag,
                        isPrimary = lst.isPrimary
                    };
                    await _memberaddressRepo.InsertOrUpdateAndGetIdAsync(det);
                }

            var isCreateMember =
                await SettingManager.GetSettingValueAsync<bool>(AppSettings.EngageSettings.CreateNewUserOnActivation);
            //#region Send Mail to the User
            if (isCreateMember && input.MemberContactDetail != null && input.MemberContactDetail.Count > 0)
            {
                var contactdetails = input.MemberContactDetail
                    .Where(t => t.ContactDetailType == ContactDetailType.Email).ToList();
                if (contactdetails != null)
                {
                    var emailAddress = contactdetails.FirstOrDefault().Detail;
                    if (!string.IsNullOrEmpty(emailAddress))
                    {
                        // create member in table user
                        var roleFromSetting =
                            await SettingManager.GetSettingValueAsync(AppSettings.EngageSettings.MemberUserRoleName);
                        var role = !string.IsNullOrEmpty(roleFromSetting) ? roleFromSetting : "Member";
                        string[] roles = { role };
                        var userMember = new CreateOrUpdateUserInput
                        {
                            SendActivationEmail = true,
                            AssignedRoleNames = roles,
                            User = new UserEditDto
                            {
                                Name = input.Member.Name,
                                Surname = input.Member.Name,
                                EmailAddress = emailAddress,
                                IsActive = false,
                                UserName = input.Member.Name
                            }
                        };
                        var user = await _userAppService.CreateMemberAsync(userMember);
                        dto.UserId = user.Id;
                        await _memberRepo.UpdateAsync(dto);
                    }
                }
            }

            return new MessageOutputDto { Id = dto.Id, SuccessFlag = true };
        }

        protected virtual async Task UpdateMemberPoints(CreateOrUpdateMemberInput input, int id)
        {
            ConnectMember member;

            if (input.Member.Id.HasValue)
                member = await _memberRepo.GetAsync(input.Member.Id.Value);
            else
                member = await _memberRepo.GetAsync(id);

            if (member != null)
            {
                var dto = input.Member;
                member.MemberCode = dto.MemberCode;
                member.Name = dto.Name;
                member.EmailId = dto.EmailId;
                member.PhoneNumber = dto.PhoneNumber;
                member.BirthDay = dto.BirthDay;
                member.CustomData = dto.CustomData;

                var createPoint = true;

                MemberPoint myMemberPoint = null;

                myMemberPoint =
                    _memberPointRepo.GetAll().FirstOrDefault(
                        a => a.ConnectMemberId == member.Id &&
                             input.TicketNumber != null && a.TicketNumber != null &&
                             a.TicketNumber.Equals(input.TicketNumber) && a.IsPointAccumulation);

                if (myMemberPoint != null)
                {
                    if (input.TicketTotal == 0M) member.TotalPoints -= myMemberPoint.TotalPoints;
                    myMemberPoint.TotalPoints = dto.TotalPoints;
                    myMemberPoint.IsPointAccumulation = dto.MemberPoint.IsPointAccumulation;
                    myMemberPoint.IsPointRedemption = dto.MemberPoint.IsPointRedemption;
                    myMemberPoint.PointsAccumulation = dto.MemberPoint.PointsAccumulation;
                    myMemberPoint.PointsAccumulationFactor = dto.MemberPoint.PointsAccumulationFactor;
                    myMemberPoint.PointsRedemption = dto.MemberPoint.PointsRedemption;
                    myMemberPoint.PointsRedemptionFactor = dto.MemberPoint.PointsRedemptionFactor;
                    myMemberPoint.CalculationOnOrder = dto.MemberPoint.CalculationOnOrder;
                    myMemberPoint.TicketTotal = input.TicketTotal;
                    createPoint = false;
                }

                if (input.TicketTotal == 0M)
                {
                    var allPoints = await _memberPointRepo.GetAllListAsync(a =>
                        input.TicketNumber != null && a.TicketNumber != null &&
                        a.TicketNumber.Equals(input.TicketNumber) && a.IsPointRedemption &&
                        a.ConnectMemberId == member.Id);

                    var myAddPoints = 0M;
                    foreach (var memberPoint in allPoints)
                    {
                        myAddPoints += memberPoint.TotalPoints;
                        memberPoint.TotalPoints = 0M;
                    }

                    member.TotalPoints += myAddPoints;
                }

                if (createPoint)
                {
                    var memberPoint = new MemberPoint
                    {
                        TotalPoints = dto.TotalPoints,
                        TicketNumber = input.TicketNumber,
                        TicketDate = input.TicketDate,
                        TicketTotal = input.TicketTotal,
                        LocationId = input.LocationId,
                        IsPointAccumulation = dto.MemberPoint.IsPointAccumulation,
                        IsPointRedemption = dto.MemberPoint.IsPointRedemption,
                        PointsAccumulation = dto.MemberPoint.PointsAccumulation,
                        PointsAccumulationFactor = dto.MemberPoint.PointsAccumulationFactor,
                        PointsRedemption = dto.MemberPoint.PointsRedemption,
                        PointsRedemptionFactor = dto.MemberPoint.PointsRedemptionFactor,
                        CalculationOnOrder = dto.MemberPoint.CalculationOnOrder,
                        ConnectMemberId = member.Id
                    };
                    member.TotalPoints = member.TotalPoints + dto.TotalPoints;
                    _memberPointRepo.InsertAndGetId(memberPoint);
                }

                await _memberRepo.UpdateAsync(member);
            }
        }

        protected virtual async Task CreateMemberPoints(CreateOrUpdateMemberInput input)
        {
            var member = input.Member.MapTo<ConnectMember>();
            var dto = input.Member;
            member.MemberCode = dto.MemberCode;
            member.Name = dto.Name;
            member.EmailId = dto.EmailId;
            member.PhoneNumber = dto.PhoneNumber;
            member.BirthDay = dto.BirthDay;
            member.CustomData = dto.CustomData;
            member.TotalPoints = dto.TotalPoints;
            member.LastVisitDate = DateTime.Now;
            try
            {
                var memberDeSucess = await _memberManager.CreateSync(member);
                if (memberDeSucess.Succeeded)
                {
                    var memberPoint = new MemberPoint
                    {
                        TotalPoints = dto.TotalPoints,
                        TicketNumber = input.TicketNumber,
                        TicketDate = input.TicketDate,
                        TicketTotal = input.TicketTotal,
                        LocationId = input.LocationId,
                        IsPointAccumulation = dto.MemberPoint.IsPointAccumulation,
                        IsPointRedemption = dto.MemberPoint.IsPointRedemption,
                        PointsAccumulation = dto.MemberPoint.PointsAccumulation,
                        PointsAccumulationFactor = dto.MemberPoint.PointsAccumulationFactor,
                        PointsRedemption = dto.MemberPoint.PointsRedemption,
                        PointsRedemptionFactor = dto.MemberPoint.PointsRedemptionFactor,
                        CalculationOnOrder = dto.MemberPoint.CalculationOnOrder,
                        ConnectMemberId = member.Id
                    };
                    _memberPointRepo.InsertAndGetId(memberPoint);
                }
            }
            catch (Exception exception)
            {
                _logService.Fatal("-----------MEMBER------------");
                _logService.Fatal(exception.Message);
                _logService.Fatal("-------END Member----------");
            }
        }
    }

    public class MemberTicketLocation
    {
        public int LocationId { get; set; }
        public string TicketNumber { get; set; }
    }
}