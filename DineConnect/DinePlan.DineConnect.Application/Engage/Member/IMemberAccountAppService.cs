﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Engage.Member.Dtos;

namespace DinePlan.DineConnect.Engage.Member
{
    public interface IMemberAccountAppService : IApplicationService
    {
        Task<IdInput> CreateOrUpdateMemberAccount(CreateOrUpdateMemberAccountInput input);
        Task<PagedResultOutput<MemberAccountListDto>> GetAll(GetMemberAccountInput input);
        Task<GetMemberAccountOutput> GetMemberAccountForUpdate(NullableIdInput input);
        Task DeleteMemberAccount(NullableIdInput input);
        bool MemberHasAccount(MemberAccountTypeEnum accountType, int memberId);
        Task DepositPoints(AccountDepositInput input);
        Task WithdrawPoints(AccountWithdrawInput input);
        decimal CheckPoints(int memberId);
        Task DepositVouchers(AccountDepositInput input);
        Task WithdrawVouchers(AccountWithdrawInput input);
        bool CheckVoucherAccount(int memberId);
        Task<PagedResultOutput<MemberAccountTransactionListDto>> GetTransactions(GetMemberAccountTransactionsInput input);
    }
}

