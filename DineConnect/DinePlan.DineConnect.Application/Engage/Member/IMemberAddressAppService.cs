﻿
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Engage.Member.Dtos;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Engage.Member
{
    public interface IMemberAddressAppService : IApplicationService
    {
        Task<IdInput> CreateOrUpdateMemberAddress(CreateOrUpdateMemberAddressInput input);
        Task<PagedResultOutput<MemberAddressListDto>> GetAll(GetMemberAddressInput input);
        Task<GetMemberAddressOutput> GetMemberAddressForUpdate(NullableIdInput input);
        Task DeleteMemberAddress(NullableIdInput input);
    }
}
