﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.Dtos;
using DinePlan.DineConnect.Engage.Impl;
using DinePlan.DineConnect.Engage.Reservation;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Utility.Exporter;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Engage.Implementation
{
    public class ReservationAppService : DineConnectAppServiceBase, IReservationAppService
    {
        private readonly IExcelExporter _exporter;
        private readonly IReservationManager _reservationManager;
        private readonly IRepository<Reservation.Reservation> _reservationRepo;
        private readonly IRepository<ConnectMember> _connectMemberRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public ReservationAppService(IReservationManager reservationManager,
            IRepository<Reservation.Reservation> reservationRepo,
            IRepository<ConnectMember> connectMemberRepo,
            IRepository<Location> locationRepo,
            IExcelExporter exporter
            , IUnitOfWorkManager unitOfWorkManager
            )
        {
            _reservationManager = reservationManager;
            _reservationRepo = reservationRepo;
            _exporter = exporter;
            _unitOfWorkManager = unitOfWorkManager;
            _connectMemberRepo = connectMemberRepo;
            _locationRepo = locationRepo;
        }

        public async Task<PagedResultOutput<ReservationListDto>> GetAll(GetReservationInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var location = _locationRepo.GetAll().WhereIf(!input.LocationRefName.IsNullOrEmpty(), t => t.Name.Contains(input.LocationRefName));

                var member = _connectMemberRepo.GetAll().WhereIf(!input.MemberCode.IsNullOrEmpty(), t => t.MemberCode.Contains(input.MemberCode) || t.Name.Contains(input.MemberCode));

                var reservation = _reservationRepo.GetAll().Where(i => i.IsDeleted == input.IsDeleted);

                DateTime? ResTime = null;

                if (input.StartDate != null && input.EndDate != null)
                {
                    ResTime = input.StartDate.Value;
                    if (input.DateFilterForAlert)
                    {
                        input.StartDate = input.StartDate.Value.Date;
                        input.EndDate = input.StartDate;
                    }

                    DateTime EndTime = input.EndDate.Value.AddMinutes(1439);

                    reservation = reservation.Where(t => t.ReservationDate >= input.StartDate && t.ReservationDate <= EndTime);
                }

                var allItems = (from res in reservation
                                join mem in member on res.ConnectMemberId equals mem.Id
                                join loc in location on res.LocationRefId equals loc.Id
                                select new ReservationListDto
                                {
                                    LocationRefId = loc.Id,
                                    LocationRefName = loc.Name,
                                    //Member = mem,
                                    MemberCode = mem.MemberCode,
                                    MemberName = mem.Name,
                                    ConnectMemberId = res.ConnectMemberId,
                                    CreationTime = res.CreationTime,
                                    Id = res.Id,
                                    Pax = res.Pax,
                                    RemainderMediums = res.RemainderMediums,
                                    RemainderRequired = res.RemainderRequired,
                                    RemainderTimes = res.RemainderTimes,
                                    Remarks = res.Remarks,
                                    ReservationDate = res.ReservationDate,
                                    Status = res.Status
                                });

                var sortMenuItems = await allItems
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var allListDtos = sortMenuItems.MapTo<List<ReservationListDto>>();

                var allItemCount = await allItems.CountAsync();

                if (input.DateFilterForAlert)
                {
                    int loopIndex = 0;
                    foreach (var det in allListDtos)
                    {
                        TimeSpan t = det.ReservationDate.Subtract(ResTime.Value);
                        if (Math.Abs(t.TotalMinutes) < 60)
                            allListDtos[loopIndex].AlreadyExists = true;

                        loopIndex++;
                    }
                }

                return new PagedResultOutput<ReservationListDto>(
                    allItemCount,
                    allListDtos
                    );
            }
        }

        public async Task<FileDto> GetAllToExcel(GetReservationInput input)
        {
            input.MaxResultCount = AppConsts.MaxPageSize;

            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<ReservationListDto>>();

            int loopIndex = 0;

            foreach (var det in allListDtos)
            {
                allListDtos[loopIndex].DateWithTimeString = allListDtos[loopIndex].ReservationDate.ToString("yyyy-MMM-dd HH:mm");
                loopIndex++;
            }
            //return _purchaseorderExporter.ExportToFile(allListDtos);

            //var allList = await _reservationRepo.GetAll().ToListAsync();
            //var allListDtos = allList.MapTo<List<ReservationListDto>>();
            var baseE = new BaseExportObject()
            {
                ExportObject = allListDtos,
                ColumnNames = new string[] { "Id", "DateWithTimeString", "MemberCode", "MemberName", "LocationRefName", "Pax", "Remarks" },
                ColumnDisplayNames = new string[] { "Id", "Date", "MemberCode", "MemberName", "LocationRefName", "Pax", "Note" }
            };
            return _exporter.ExportToFile(baseE, L("Reservation"));
        }

        public async Task<GetReservationForEditOutput> GetReservationForEdit(NullableIdInput input)
        {
            ReservationEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _reservationRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<ReservationEditDto>();
            }
            else
            {
                editDto = new ReservationEditDto();
            }

            return new GetReservationForEditOutput
            {
                Reservation = editDto
            };
        }

        public async Task CreateOrUpdateReservation(CreateOrUpdateReservationInput input)
        {
            if (input.Reservation.Id.HasValue)
            {
                await UpdateReservation(input);
            }
            else
            {
                await CreateReservation(input);
            }
        }

        public async Task DeleteReservation(IdInput input)
        {
            await _reservationRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateReservation(CreateOrUpdateReservationInput input)
        {
            var item = await _reservationRepo.GetAsync(input.Reservation.Id.Value);
            var dto = input.Reservation;

            //TODO: SERVICE Reservation Update Individually

            CheckErrors(await _reservationManager.CreateSync(item));
        }

        protected virtual async Task CreateReservation(CreateOrUpdateReservationInput input)
        {
            var dto = input.Reservation.MapTo<Reservation.Reservation>();

            CheckErrors(await _reservationManager.CreateSync(dto));
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetRemainderTimesForCombobox()
        {
            List<ComboboxItemDto> retList = new List<ComboboxItemDto>();

            string enumstring;
            Array EnumValues = System.Enum.GetValues(typeof(RemainderTimes));
            foreach (int EnumValue in EnumValues)
            {
                enumstring = Enum.GetName(typeof(RemainderTimes), EnumValue);
                retList.Add(new ComboboxItemDto { Value = EnumValue.ToString(), DisplayText = enumstring });
            }

            return
                   new ListResultOutput<ComboboxItemDto>(
                       retList.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetRemainderMediumsForCombobox()
        {
            List<ComboboxItemDto> retList = new List<ComboboxItemDto>();

            string enumstring;
            Array EnumValues = System.Enum.GetValues(typeof(RemainderMediums));
            foreach (int EnumValue in EnumValues)
            {
                enumstring = Enum.GetName(typeof(RemainderMediums), EnumValue);
                retList.Add(new ComboboxItemDto { Value = EnumValue.ToString(), DisplayText = enumstring });
            }

            return
                   new ListResultOutput<ComboboxItemDto>(
                       retList.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText)).ToList());
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _reservationRepo.GetAsync(input.Id);
                item.IsDeleted = false;

                await _reservationRepo.UpdateAsync(item);
            }
        }
    }
}