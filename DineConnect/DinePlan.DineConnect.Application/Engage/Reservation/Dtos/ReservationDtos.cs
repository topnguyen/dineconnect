﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Engage.Dtos
{
    [AutoMapFrom(typeof(Reservation.Reservation))]
    public class ReservationListDto : FullAuditedEntityDto
    {
        public virtual int LocationRefId { get; set; }

        public virtual string LocationRefName { get; set; }

        public virtual DateTime ReservationDate { get; set; }   //  Date & Time together

        public virtual int ConnectMemberId { get; set; }

        public virtual string MemberCode { get; set; }
        public virtual string MemberName { get; set; }
        //public virtual ConnectMember Member { get; set; }

        public virtual int Pax { get; set; }                  //  Does We Need  Adult / Child Details or not
        public virtual bool RemainderRequired { get; set; }
        public virtual string RemainderTimes { get; set; }    //  123   -   1 - One Day Before, 2 - Same Day , 3 - Two Hours Before
        public virtual string RemainderMediums { get; set; }    //  ES - E - Email, S - SMS, P - Phone

        public virtual string Remarks { get; set; }
        public virtual bool Status { get; set; }        //  By Default, 'Pending', 'Completed' while customers reached location

        public virtual bool AlreadyExists { get; set; }

        public virtual string DateWithTimeString { get; set; }
    }

    [AutoMapTo(typeof(Reservation.Reservation))]
    public class ReservationEditDto
    {
        public int? Id { get; set; }
        public virtual int LocationRefId { get; set; }

        public virtual DateTime ReservationDate { get; set; }   //  Date & Time together

        public virtual int ConnectMemberId { get; set; }

        public virtual int Pax { get; set; }                  //  Does We Need  Adult / Child Details or not
        public virtual bool RemainderRequired { get; set; }
        public virtual string RemainderTimes { get; set; }    //  123   -   1 - One Day Before, 2 - Same Day , 3 - Two Hours Before
        public virtual string RemainderMediums { get; set; }    //  ES - E - Email, S - SMS, P - Phone

        public virtual string Remarks { get; set; }
        public virtual bool Status { get; set; }        //  By Default, 'Pending', 'Completed' while customers reached location
    }

    public class GetReservationInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string MemberCode { get; set; }

        public string LocationRefName { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public string Operation { get; set; }

        public bool DateFilterForAlert { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "ReservationDate Desc";
            }
        }
    }

    public class GetReservationForEditOutput : IOutputDto
    {
        public ReservationEditDto Reservation { get; set; }
    }

    public class CreateOrUpdateReservationInput : IInputDto
    {
        [Required]
        public ReservationEditDto Reservation { get; set; }
    }
}