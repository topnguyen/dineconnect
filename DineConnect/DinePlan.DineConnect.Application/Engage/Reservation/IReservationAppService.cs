﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.Dtos;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Engage
{
    public interface IReservationAppService : IApplicationService
    {
        Task<PagedResultOutput<ReservationListDto>> GetAll(GetReservationInput inputDto);

        Task<FileDto> GetAllToExcel(GetReservationInput inputDto);

        Task<GetReservationForEditOutput> GetReservationForEdit(NullableIdInput nullableIdInput);

        Task CreateOrUpdateReservation(CreateOrUpdateReservationInput input);

        Task DeleteReservation(IdInput input);

        Task<ListResultOutput<ComboboxItemDto>> GetRemainderTimesForCombobox();

        Task<ListResultOutput<ComboboxItemDto>> GetRemainderMediumsForCombobox();

        Task ActivateItem(IdInput input);
    }
}