﻿using System.Collections.Generic;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.Gift.Dtos;

namespace DinePlan.DineConnect.Engage.Gift
{
    public interface IGiftVoucherTypeListExcelExporter
    {
        FileDto ExportDuplicateToFile(List<GiftVoucherListDto> dtos);
        FileDto ExportToFile(List<GiftVoucherTypeListDto> dtos);
        FileDto ExportVouchers(List<GiftVoucherListDto> allListDtos);
    }
}
