﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.Gift.DinePlan.DineConnect.Engage;
using DinePlan.DineConnect.Engage.Gift.Dtos;
using DinePlan.DineConnect.Job;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Engage.Gift.Implementation
{
    public class GiftVoucherTypeAppService : DineConnectAppServiceBase, IGiftVoucherTypeAppService
    {
        private readonly IBackgroundJobManager _bgm;
        private readonly IRepository<ConnectMember> _connMember;
        private readonly IGiftVoucherManager _giftVoucherManager;
        private readonly IRepository<GiftVoucher> _giftvoucherRepo;
        private readonly IGiftVoucherTypeListExcelExporter _giftvouchertypeExporter;
        private readonly IGiftVoucherTypeManager _giftvouchertypeManager;
        private readonly IRepository<GiftVoucherType> _giftvouchertypeRepo;
        private readonly ILocationAppService _locAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Category> _categoryRepository;

        public GiftVoucherTypeAppService(IGiftVoucherTypeManager giftvouchertypeManager,
            IGiftVoucherManager giftVoucherManager, ILocationAppService locAppService,
            IRepository<GiftVoucherType> giftVoucherTypeRepo, IRepository<GiftVoucher> giftvoucherRepo,
            IGiftVoucherTypeListExcelExporter giftvouchertypeExporter, IRepository<ConnectMember> connMember,
            IBackgroundJobManager backgroundJobManager
            , IUnitOfWorkManager unitOfWorkManager
            ,IRepository<Category> categoryRepository

            )
        {
            _giftvouchertypeManager = giftvouchertypeManager;
            _giftVoucherManager = giftVoucherManager;
            _giftvouchertypeRepo = giftVoucherTypeRepo;
            _giftvoucherRepo = giftvoucherRepo;
            _giftvouchertypeExporter = giftvouchertypeExporter;
            _connMember = connMember;
            _bgm = backgroundJobManager;
            _unitOfWorkManager = unitOfWorkManager;
            _locAppService = locAppService;
            _categoryRepository = categoryRepository;
        }

        public async Task<PagedResultOutput<GiftVoucherTypeListDto>> GetAll(GetGiftVoucherTypeInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var allItems = _giftvouchertypeRepo.GetAll()
                    .Where(i => i.IsDeleted == input.IsDeleted);
                if (input.Operation == "SEARCH")
                {
                    allItems = allItems
                        .WhereIf(
                            !input.Filter.IsNullOrEmpty(),
                            p => p.Id.Equals(input.Filter)
                        );
                }
                var allMyEnItems = SearchLocation(allItems, input.LocationGroup).OfType<GiftVoucherType>();

                var allItemCount = allMyEnItems.Count();

                var sortMenuItems = allMyEnItems.AsQueryable()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList();

                var allListDtos = sortMenuItems.MapTo<List<GiftVoucherTypeListDto>>();

                return new PagedResultOutput<GiftVoucherTypeListDto>(
                    allItemCount,
                    allListDtos
                    );
            }
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _giftvouchertypeRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<GiftVoucherTypeListDto>>();
            return _giftvouchertypeExporter.ExportToFile(allListDtos);
        }

        public async Task<FileDto> GetAllGiftVoucherToExcel(GetGiftVoucherInput inputDto)
        {
            var allList = await _giftvoucherRepo.GetAll().Where(x => x.GiftVoucherTypeId == inputDto.VoucherType).ToListAsync();
            var allListDtos = allList.MapTo<List<GiftVoucherListDto>>();
            return _giftvouchertypeExporter.ExportVouchers(allListDtos);
        }
        public async Task<GetGiftVoucherTypeForEditOutput> GetGiftVoucherTypeForEdit(NullableIdInput input)
        {
            GiftVoucherTypeEditDto editDto;
            GetGiftVoucherTypeForEditOutput output = new GetGiftVoucherTypeForEditOutput();
            if (input.Id.HasValue)
            {
                var hDto = await _giftvouchertypeRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<GiftVoucherTypeEditDto>();
            }
            else
            {
                editDto = new GiftVoucherTypeEditDto();
                editDto.ValidTill = DateTime.Now.Date.AddDays(10);
            }
            UpdateLocationAndNonLocationForEdit(editDto, output.LocationGroup);

            output.GiftVoucherType = editDto;
            return output;
        }

        public async Task<GetGiftVoucherForEditOutput> GetGiftVoucherForEdit(NullableIdInput input)

        {
            GiftVoucherEditDto editDto;
            GetGiftVoucherForEditOutput output = new GetGiftVoucherForEditOutput();

            var hDto = await _giftvoucherRepo.GetAsync(input.Id.Value);

            editDto = hDto.MapTo<GiftVoucherEditDto>();

            output.GiftVoucher = editDto;

            return output;
        }


        public async Task<IdInput> CreateOrUpdateGiftVoucherType(CreateOrUpdateGiftVoucherTypeInput input)
        {
            if (input.GiftVoucherType.Id.HasValue)
            {
                return await UpdateGiftVoucherType(input);
            }
            else
            {
                return await CreateGiftVoucherType(input);
            }
            return new IdInput { Id = 0 };
        }

        public void ImportGiftVouchers(ImportGiftVoucherInput input)
        {
            var giftVoucherList = new List<GiftVoucher>();

            foreach (var giftItem in input.GiftVoucherItems)
            {
                if (giftItem.Id == 0)
                {
                    giftVoucherList.Add(new GiftVoucher()
                    {
                        GiftVoucherTypeId = input.GiftVoucherTypeId ?? 0,
                        Voucher = giftItem.Voucher
                    });
                }
            }

            CreateGiftVoucher(giftVoucherList);
        }

        public async Task<UpdateGiftVoucherOutput> UpdateGiftVoucher(CreateOrUpdateGiftVoucherInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var vouchers = _giftvoucherRepo.GetAllList();
                if(vouchers.Where(x => x.Voucher == input.GiftVoucher.Voucher).FirstOrDefault() != null)
                {
                    return new UpdateGiftVoucherOutput()
                    {
                        IsSuccess = false,
                        Message = L("GiftVoucherNameAlreadyExisted")
                    };
                }
            }

            if (input.GiftVoucher.Id != null)
            {
                var item = await _giftvoucherRepo.GetAsync(input.GiftVoucher.Id);
                if (item.Id > 0)
                {
                    item.Voucher = input.GiftVoucher.Voucher;
                }
                return new UpdateGiftVoucherOutput()
                {
                    IsSuccess = true,
                };
            }

            return new UpdateGiftVoucherOutput() 
            {
                IsSuccess = false
            };
        }

        public async Task<string> GenerateVoucherForType(CreateOrUpdateGiftVoucherTypeInput input)
        {
            if (!input.GiftVoucherType.IsUpdateFlag)
            {
                await UpdateGiftVoucherType(input);
            }
            if (input.GiftVoucherType.Id != null)
                return await GenerateVoucher(input.GiftVoucherType.Id.Value, true);
            return "";
        }

        public async Task DeleteGiftVoucherType(IdInput input)
        {
            await _giftvouchertypeRepo.DeleteAsync(input.Id);
        }
        public async Task DeleteGiftVoucher(IdInput input)
        {
            await _giftvoucherRepo.DeleteAsync(input.Id);
        }

        public async Task DeleteMultiGiftVoucher(List<int> ids)
        {
            foreach (var id in ids)
            {
                await _giftvoucherRepo.DeleteAsync(id);
            }
        }

        public async Task<ListResultOutput<GiftVoucherTypeListDto>> GetIds()
        {
            var lstGiftVoucherType = await _giftvouchertypeRepo.GetAll().ToListAsync();
            return new ListResultOutput<GiftVoucherTypeListDto>(lstGiftVoucherType.MapTo<List<GiftVoucherTypeListDto>>());
        }

        public async Task<FileDto> GetVoucherInExcel(int voucherType)
        {
            var allList = _giftvoucherRepo.GetAllList(a => a.GiftVoucherTypeId.Equals(voucherType));
            var allListDtos = allList.MapTo<List<GiftVoucherListDto>>();
            return _giftvouchertypeExporter.ExportVouchers(allListDtos);
        }

        public async Task<string> GenerateVoucher(int id, bool repeat = false, bool autoAttach = false)
        {
            var returnStatus = "";
            var gt = await _giftvouchertypeRepo.GetAsync(id);
            var voucherCount = await _giftvoucherRepo.CountAsync(a => a.GiftVoucherTypeId.Equals(id));
            if (voucherCount > 0 && !repeat && !autoAttach)
            {
                returnStatus = L("VoucherGeneratedAlready");
            }
            else
            {
                 if (gt.VoucherDigit > 0)
                {
                    var allVou = new List<string>();
                    int count = gt.VoucherCount;
                    while (true)
                    {
                        var myVouh = GetUniqueString(gt.VoucherDigit);
                        var vocheNa = $"{gt.Prefix}{myVouh}{gt.Suffix}";
                        if (!allVou.Contains(vocheNa) && !CheckItInDb(vocheNa))
                        {
                            count = count - 1;
                            allVou.Add(vocheNa);
                            if (autoAttach)
                            {
                                returnStatus = vocheNa;
                                break;
                            }
                            if (count == 0)
                            {
                                break;
                            }
                        }
                    }
                    foreach (var vou in allVou)
                    {
                        var voucher = new GiftVoucher
                        {
                            Claimed = false,
                            GiftVoucherTypeId = gt.Id,
                            TicketNumber = "",
                            Voucher = vou,
                            ClaimedAmount = 0M
                        };
                        try
                        {
                            CheckErrors(await _giftVoucherManager.CreateSync(voucher));
                        }
                        catch (Exception exception)
                        {
                            var mess = exception.Message;
                        }
                    }
                }
                else
                {
                    returnStatus = L("VoucherDigitZeroErr");
                }
            }
            return returnStatus;
        }

        public async Task<ValidateVoucherOutput> ValidateVoucher(ValidateVoucherInput input)
        {
            var returnStatus = new ValidateVoucherOutput();

            var voucher = _giftvoucherRepo.FirstOrDefault(a => a.Voucher.Equals(input.Voucher));

            if (voucher == null)
            {
                returnStatus.IsValid = false;
                returnStatus.Reason = string.Format(L("VoucherNotValid_f"), input.Voucher);
                return returnStatus;
            }

            if (voucher.Claimed)
            {
                returnStatus.IsValid = false;
                returnStatus.Reason = string.Format(L("VoucherClaimed_f"), input.Voucher);
                return returnStatus;
            }

            if (voucher.ConnectMemberId != null)
            {
                if (input.MemberCode == null)
                {
                    returnStatus.IsValid = false;
                    returnStatus.Reason = string.Format(L("NotValid_f"), L("Voucher"), input.Voucher);
                    return returnStatus;
                }
                var connMe = await _connMember.FirstOrDefaultAsync(a => a.MemberCode.Equals(input.MemberCode));
                if (connMe == null)
                {
                    returnStatus.IsValid = false;
                    returnStatus.Reason = string.Format(L("VoucherLinked_f"), input.Voucher);
                    return returnStatus;
                }
                if (connMe.Id != voucher.ConnectMemberId)
                {
                    returnStatus.IsValid = false;
                    returnStatus.Reason = string.Format(L("VoucherLinked_f"), input.Voucher);
                    return returnStatus;
                }
            }
            var voucherType = await _giftvouchertypeRepo.GetAsync(voucher.GiftVoucherTypeId);
            if (!await _locAppService.IsLocationExists(new CheckLocationInput
            {
                LocationId = input.Location,
                Locations = voucherType.Locations,
                Group = voucherType.Group,
                LocationTag = voucherType.LocationTag,
                NonLocations = voucherType.NonLocations
            }))
            {
                returnStatus.IsValid = false;
                returnStatus.Reason = string.Format(L("VoucherNotValidInThisLocation_f"), input.Voucher);
                return returnStatus;
            }
            if (voucherType.ValidTill < DateTime.Now.Date)
            {
                returnStatus.IsValid = false;
                returnStatus.Reason = string.Format(L("VoucherDateExpired_f"), input.Voucher);
                return returnStatus;
            }

            if (voucherType.MinimumTicketTotal > 0M)
            {
                if (input.Amount < voucherType.MinimumTicketTotal)
                {
                    returnStatus.IsValid = false;
                    returnStatus.Reason = string.Format(L("VoucherTicketTotalIsNotValid_f"), input.Voucher);
                    return returnStatus;
                }
            }

            var extraVoucher =
                await
                    _giftvoucherRepo.GetAllListAsync(
                        a => a.TicketNumber != null && a.TicketNumber.Equals(input.TicketNumber));
            if (extraVoucher != null && extraVoucher.Any() && !voucherType.IsRepeatApplicable)
            {
                returnStatus.IsValid = false;
                returnStatus.Reason = string.Format(L("VoucherAlreadyApplied_f"), extraVoucher[0].Voucher);
                return returnStatus;
            }

            if (extraVoucher != null && extraVoucher.Any() && voucherType.IsRepeatApplicable &&
                voucherType.RepeatCount <= extraVoucher.Count)
            {
                returnStatus.IsValid = false;
                returnStatus.Reason = string.Format(L("VoucherClaimCounterReached_f"), input.Voucher);
                return returnStatus;
            }

            returnStatus.IsValid = true;
            returnStatus.Reason = "";
            if (voucherType.IsPercent)
            {
                returnStatus.Discount = (input.Amount * voucherType.VoucherValue) / 100;
            }
            else
            {
                returnStatus.Discount = voucherType.VoucherValue;
            }
            return returnStatus;
        }

        public async Task<int> ApplyVoucher(ValidateVoucherInput input)
        {
            var returnStatus = new ValidateVoucherOutput();
            var voucher = await _giftvoucherRepo.SingleAsync(a => a.Voucher.Equals(input.Voucher));
            var voucherType = await _giftvouchertypeRepo.GetAsync(voucher.GiftVoucherTypeId);

            if (voucherType.IsPercent)
            {
                voucher.ClaimedAmount = (input.Amount * voucherType.VoucherValue) / 100;
            }
            else
            {
                voucher.ClaimedAmount = voucherType.VoucherValue;
            }
            voucher.Claimed = true;
            voucher.ClaimedDate = DateTime.Now;
            voucher.LocationId = input.Location;
            voucher.TicketNumber = input.TicketNumber;
            await _giftvoucherRepo.UpdateAsync(voucher);
            return voucher.Id;
        }

        public async Task LinkMemberToVoucher(MemberVoucherInput input)
        {
            var member = await _connMember.GetAsync(input.MemberId);
            var voucher = await _giftvoucherRepo.FirstOrDefaultAsync(a => a.Voucher.Equals(input.Voucher));
            if (member != null)
            {
                if (voucher != null)
                {
                    if (!voucher.Claimed)
                    {
                        if (voucher.GiftVoucherType.ValidTill < DateTime.Now)
                        {
                            throw new UserFriendlyException(L("VoucherNotValid_f", input.Voucher));
                        }
                        voucher.ConnectMember = member;
                        await _giftvoucherRepo.UpdateAsync(voucher);
                    }
                    else
                    {
                        throw new UserFriendlyException(L("VoucherClaimed_f", input.Voucher));
                    }
                }
                else
                {
                    throw new UserFriendlyException(L("NotExist_f", L("Voucher"), input.Voucher));
                }
            }
            else
            {
                throw new UserFriendlyException(L("MemberIdNotExist"));
            }
        }

        public async Task UnLinkMemberFromVoucher(MemberVoucherInput input)
        {
            var member = await _connMember.GetAsync(input.MemberId);
            var voucher = await _giftvoucherRepo.FirstOrDefaultAsync(a => a.Voucher.Equals(input.Voucher));
            if (member != null)
            {
                if (voucher != null)
                {
                    if (!voucher.Claimed)
                    {
                        voucher.ConnectMember = null;
                    }
                    else
                    {
                        throw new UserFriendlyException(L("VoucherClaimed_f", input.Voucher));
                    }
                }
                else
                {
                    throw new UserFriendlyException(L("NotExist_f", L("Voucher"), input.Voucher));
                }
            }
            else
            {
                throw new UserFriendlyException(L("MemberIdNotExist"));
            }
        }

        public async Task EmailVoucher(MemberVoucherInput input)
        {
            await _bgm.EnqueueAsync<GiftVoucherJob, GiftVoucherJobArgs>(new GiftVoucherJobArgs
            {
                MemberId = input.MemberId,
                Voucher = input.Voucher
            });
        }

        public async Task<List<GiftVoucherEditDto>> MemberVouchers(MemberVoucherInput input)
        {
            var vouchers = new List<GiftVoucherEditDto>();
            var allVouchers = await _giftvoucherRepo.GetAllListAsync(a => a.ConnectMemberId == input.MemberId);
            if (allVouchers.Any())
            {
                vouchers = allVouchers.MapTo<List<GiftVoucherEditDto>>();
            }
            return vouchers;
        }

        public async Task<ValidateVoucherOutput> GenerateAutoAttachVoucher(ValidateVoucherInput input)
        {
            var returnStatus = new ValidateVoucherOutput();
            var vType = _giftvouchertypeRepo.FirstOrDefault(a => a.AutoAttach);
            if (!string.IsNullOrEmpty(input.Voucher))
            {
                vType = _giftvouchertypeRepo.FirstOrDefault(a => a.Name.Equals(input.Voucher));
                if (vType == null)
                {
                    returnStatus.IsValid = false;
                    returnStatus.Reason = string.Format(L("VoucherNotValid_f"), input.Voucher);
                    return returnStatus;
                }
            }
            if (vType == null)
            {
                returnStatus.IsValid = false;
                returnStatus.Reason = "No Auto Attach Voucher";
                return returnStatus;
            }
            if (vType.MinimumTicketTotal > 0M)
            {
                if (input.TicketTotal < vType.MinimumTicketTotal)
                {
                    returnStatus.IsValid = false;
                    returnStatus.Reason = string.Format(L("VoucherTicketTotalIsNotValid_f"), input.Voucher);
                    return returnStatus;
                }
            }
            if (vType.ValidTill < DateTime.Now.Date)
            {
                returnStatus.IsValid = false;
                returnStatus.Reason = string.Format(L("VoucherDateExpired_f"), input.Voucher);
                return returnStatus;
            }
            if (!await _locAppService.IsLocationExists(new CheckLocationInput
            {
                LocationId = input.Location,
                Locations = vType.Locations,
                Group = vType.Group,
                LocationTag = vType.LocationTag,
                NonLocations = vType.NonLocations
            }))
            {
                returnStatus.IsValid = false;
                returnStatus.Reason = string.Format(L("VoucherNotValidInThisLocation_f"), input.Voucher);
                return returnStatus;
            }
            var totalCount = await _giftvoucherRepo.CountAsync(a => a.GiftVoucherTypeId == vType.Id);
            if (totalCount >= vType.VoucherCount)
            {
                returnStatus.IsValid = false;
                returnStatus.Reason = "Total Vouchers Reached";
                return returnStatus;
            }

            var voucher = await GenerateVoucher(vType.Id, false, true);
            returnStatus.VoucherCode = voucher;
            return returnStatus;
        }

        public async Task<GiftVoucherStatsDto> GetVouchers(GetGiftVoucherInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete, AbpDataFilters.MustHaveTenant))
            {
                var stats = new GiftVoucherStatsDto();
                var sortMenuItems = new List<GiftVoucher>();
                IQueryable<GiftVoucher> allItems;
                if (input.VoucherType != 0)
                {
                    allItems = _giftvoucherRepo
                        .GetAll()
                        .WhereIf(input.VoucherType > 0,
                        p => p.GiftVoucherTypeId.Equals(input.VoucherType) && p.IsDeleted.Equals(input.IsDeleted));
                    stats.DashBoardDto = new DashboardGiftVoucher
                    {
                        TotalVoucherCount = allItems.Count(),
                        TotalClaimed = allItems.WhereIf(
                       1 > 0,
                       p => p.Claimed).Count(),
                        TotalAssigned = allItems.WhereIf(
                       1 > 0,
                       p => p.Assigned).Count()
                    };
                    if (input.Operation == "SEARCH")
                    {
                        allItems = allItems
                            .WhereIf(
                                !input.Filter.IsNullOrEmpty(),
                                p => p.Voucher.Equals(input.Filter)
                            );
                    }
                    sortMenuItems = await allItems
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();
                }
                else
                {
                    allItems = _giftvoucherRepo.GetAll();
                    stats.DashBoardDto = new DashboardGiftVoucher
                    {
                        TotalVoucherCount = allItems.Count(),
                        TotalClaimed = allItems.WhereIf(
                       1 > 0,
                       p => p.Claimed).Count(),
                        TotalAssigned = allItems.WhereIf(
                       1 > 0,
                       p => p.Assigned).Count()
                    };
                    if (input.Operation == "SEARCH")
                    {
                        allItems = allItems
                            .WhereIf(
                                !input.Filter.IsNullOrEmpty(),
                                p => p.Voucher.Equals(input.Filter)
                            );
                    }
                    sortMenuItems = allItems.ToList();
                }

                var allListDtos = sortMenuItems.MapTo<List<GiftVoucherListDto>>();

                var allItemCount = await allItems.CountAsync();

                stats.ValidGiftVouchers = new PagedResultOutput<GiftVoucherListDto>(
                    allItemCount,
                    allListDtos
                    );

                return stats;
            }
        }

        private string GetUniqueString(int voucherDigit)
        {
            var ticks = DateTime.UtcNow.Ticks.ToString();
            var myVouh = ticks.Substring(ticks.Length - voucherDigit);

            return myVouh;
        }

        private bool CheckItInDb(string myVouh)
        {
            return _giftvoucherRepo.FirstOrDefault(a => a.Voucher.Equals(myVouh)) != null;
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _giftvouchertypeRepo.GetAsync(input.Id);
                item.IsDeleted = false;

                await _giftvouchertypeRepo.UpdateAsync(item);
            }
        }

        public async Task ActivateGiftVoucher(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _giftvoucherRepo.GetAsync(input.Id);
                item.IsDeleted = false;

                await _giftvoucherRepo.UpdateAsync(item);
            }
        }

        public async Task ActivateMultiGiftVoucher(List<int> ids)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                foreach (var id in ids)
                {
                    var item = await _giftvoucherRepo.GetAsync(id);
                    item.IsDeleted = false;

                    await _giftvoucherRepo.UpdateAsync(item);
                }
            }
        }

        protected virtual async Task<IdInput> UpdateGiftVoucherType(CreateOrUpdateGiftVoucherTypeInput input)
        {
            if (input.GiftVoucherType.Id != null)
            {
                var item = await _giftvouchertypeRepo.GetAsync(input.GiftVoucherType.Id.Value);
                var dto = input.GiftVoucherType;

                UpdateLocationAndNonLocation(item, input.LocationGroup);

                if (item.Id > 0)
                {
                    var lov = await _giftvoucherRepo.GetAllListAsync(a => a.GiftVoucherTypeId == item.Id);
                    if (lov.Count == 0)
                    {
                        item.Prefix = dto.Prefix;
                        item.Suffix = dto.Suffix;
                        item.VoucherDigit = dto.VoucherDigit;
                        item.IsPointsApplicable = dto.IsPointsApplicable;
                        item.IsRepeatApplicable = dto.IsRepeatApplicable;
                        item.ValidTill = dto.ValidTill;
                        item.VoucherValue = dto.VoucherValue;
                        item.RepeatCount = dto.RepeatCount;
                        item.Type = dto.Type;
                        item.VoucherCount = dto.VoucherCount;
                        item.MinimumTicketTotal = dto.MinimumTicketTotal;
                        item.GiftVoucherCategoryId = dto.GiftVoucherCategoryId;
                    }
                    else
                    {
                        item.Name = dto.Name;
                        item.ValidTill = dto.ValidTill;
                        item.IsPointsApplicable = dto.IsPointsApplicable;
                        item.IsRepeatApplicable = dto.IsRepeatApplicable;
                        item.VoucherValue = dto.VoucherValue;
                        item.RepeatCount = dto.RepeatCount;
                        item.Type = dto.Type;
                        item.MinimumTicketTotal = dto.MinimumTicketTotal;
                        item.GiftVoucherCategoryId = dto.GiftVoucherCategoryId;
                    }
                    item.Group = input.LocationGroup.Group;
                }
                CheckErrors(await _giftvouchertypeManager.CreateSync(item));


                


                return new IdInput { Id = item.Id };
            }
            return new IdInput { Id = 0 };
        }

        protected virtual async Task<IdInput> CreateGiftVoucherType(CreateOrUpdateGiftVoucherTypeInput input)
        {
            var dto = input.GiftVoucherType.MapTo<GiftVoucherType>();
            if (dto.ValidTill <= DateTime.Now)
            {
                dto.ValidTill = DateTime.Now.AddYears(20);
            }
            UpdateLocationAndNonLocation(dto, input.LocationGroup);

            CheckErrors(await _giftvouchertypeManager.CreateSync(dto));

            return new IdInput { Id = dto.Id };
        }

        private void CreateGiftVoucher(List<GiftVoucher> dtos)
        {
            foreach (var dto in dtos)
            {
                var voucher = new GiftVoucher()
                {
                    GiftVoucherTypeId = dto.GiftVoucherTypeId,
                    Voucher = dto.Voucher
                };

                _giftvoucherRepo.InsertOrUpdate(voucher);
            }
        }

        public async Task<GiftVoucher> GetGiftVoucher(int id)
        {
            return _giftvoucherRepo.Get(id);
        }

        public async Task<FileDto> ExprotErrorToExcel(List<GiftVoucherListDto> giftVoucherListDtos)
        {
            return _giftvouchertypeExporter.ExportDuplicateToFile(giftVoucherListDtos);
        }

    }
}