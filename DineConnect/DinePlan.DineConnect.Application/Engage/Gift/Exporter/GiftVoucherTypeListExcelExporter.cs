﻿using System.Collections.Generic;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.Gift.Dtos;

namespace DinePlan.DineConnect.Engage.Gift.Exporter
{
    public class GiftVoucherTypeListExcelExporter : FileExporterBase, IGiftVoucherTypeListExcelExporter
    {
        public FileDto ExportDuplicateToFile(List<GiftVoucherListDto> dtos)
        {
            return CreateExcelPackage(
                "Duplicate.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("GiftVoucherType"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("VoucherCode")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Voucher
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }

        public FileDto ExportToFile(List<GiftVoucherTypeListDto> dtos)
        {
            return CreateExcelPackage(
                "GiftVoucherTypeList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("GiftVoucherType"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }

        public FileDto ExportVouchers(List<GiftVoucherListDto> dtos)
        {
            return CreateExcelPackage(
                "GiftVoucherTypeList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("GiftVoucherType"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Voucher"),
                        L("Claimed"),
                        L("ClaimedDate"),
                        L("ClaimedAmount"),
                        L("TicketNumber"),
                        L("LocationId"),
                        L("Remarks")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.Voucher,
                        _ => _.Claimed,
                        _ => _.ClaimedDate.ToLongDateString(),
                        _ => _.ClaimedAmount,
                        _ => _.TicketNumber,
                        _ => _.LocationId,
                        _ => _.Remarks
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}