﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Engage.Gift.Dtos
{
    [AutoMapFrom(typeof(GiftVoucherType))]
    public class GiftVoucherTypeListDto : FullAuditedEntityDto
    {
        public virtual string Name { get; set; }
        public virtual string VoucherFormat { get; set; }
        public virtual int VoucherCount { get; set; }
        public virtual decimal VoucherValue { get; set; }
        public virtual DateTime ValidTill { get; set; }
        public virtual int TotalClaimed { get; set; }
        public virtual bool AutoAttach { get; set; }
        public virtual bool IsUpdateFlag { get; set; }
        public virtual decimal RequiredPoints { get; set; }
    }

    public class GiftVoucherStatsDto
    {
        public PagedResultOutput<GiftVoucherListDto> ValidGiftVouchers { get; set; }
        public PagedResultOutput<GiftVoucherListDto> InvalidGiftVouchers { get; set; }
        public PagedResultOutput<GiftVoucherListDto> ExceedLimitationGiftVouchers { get; set; }
        public DashboardGiftVoucher DashBoardDto { get; set; }
    }

    public class DashboardGiftVoucher
    {
        public DashboardGiftVoucher()
        {
            TotalVoucherCount = 0;
            TotalClaimed = 0;
            TotalAssigned = 0;
        }

        public int TotalVoucherCount { get; set; }
        public int TotalClaimed { get; set; }
        public int TotalAssigned { get; set; }
    }

    [AutoMapFrom(typeof(GiftVoucher))]
    public class GiftVoucherListDto : FullAuditedEntityDto
    {
        public virtual int Id { get; set; }
        public virtual string Voucher { get; set; }
        public virtual bool Claimed { get; set; }
        public virtual DateTime ClaimedDate { get; set; }
        public virtual decimal ClaimedAmount { get; set; }
        public virtual string TicketNumber { get; set; }
        public virtual string LocationId { get; set; }
        public virtual string Remarks { get; set; }
    }

    [AutoMapFrom(typeof(GiftVoucher))]
    public class GiftVoucherEditDto : FullAuditedEntityDto
    {
        public virtual int Id { get; set; }
        public virtual string Voucher { get; set; }
        public virtual bool Claimed { get; set; }
        public virtual DateTime ClaimedDate { get; set; }
        public virtual decimal ClaimedAmount { get; set; }
        public virtual string TicketNumber { get; set; }
        public virtual string LocationId { get; set; }
        public virtual string Remarks { get; set; }
        public virtual GiftVoucherTypeEditDto GiftVoucherType { get; set; }
    }

    [AutoMapTo(typeof(GiftVoucherType))]
    public class GiftVoucherTypeEditDto : ConnectEditDto
    {
        public int? Id { get; set; }
        public virtual string Name { get; set; }
        public virtual int Type { get; set; }
        public virtual string Prefix { get; set; }
        public virtual string Suffix { get; set; }
        public virtual int VoucherDigit { get; set; }
        public virtual int VoucherCount { get; set; }
        public virtual decimal VoucherValue { get; set; }
        public virtual DateTime ValidTill { get; set; }
        public virtual bool IsPointsApplicable { get; set; }
        public virtual bool IsRepeatApplicable { get; set; }

        public virtual bool AutoAttach { get; set; }
        public virtual int RepeatCount { get; set; }
        public virtual decimal MinimumTicketTotal { get; set; }
        public virtual int? GiftVoucherCategoryId { get; set; }
        public virtual bool IsUpdateFlag { get; set; }
        public virtual decimal RequiredPoints { get; set; }
    }

    public class GetGiftVoucherTypeInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public GetGiftVoucherTypeInput()
        {
            LocationGroup = new LocationGroupDto();
        }

        public LocationGroupDto LocationGroup { get; set; }
        public string Filter { get; set; }
        public string Operation { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    public class GetGiftVoucherInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public GetGiftVoucherInput()
        {
            LocationGroup = new LocationGroupDto();
        }

        public LocationGroupDto LocationGroup { get; set; }
        public int VoucherType { get; set; }
        public string Filter { get; set; }
        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }

        public bool IsDeleted { get; set; }
    }

    public class GetGiftVoucherTypeForEditOutput : IOutputDto
    {
        public GiftVoucherTypeEditDto GiftVoucherType { get; set; }
        public LocationGroupDto LocationGroup { get; set; }

        public GetGiftVoucherTypeForEditOutput()
        {
            GiftVoucherType = new GiftVoucherTypeEditDto();
            LocationGroup = new LocationGroupDto();
        }
    }

    public class GetGiftVoucherForEditOutput : IOutputDto
    {
        public GiftVoucherEditDto GiftVoucher { get; set; }

        public GetGiftVoucherForEditOutput()
        {
            GiftVoucher = new GiftVoucherEditDto();
        }
    }

    public class ImportGiftVoucherInput
    {
        public int? GiftVoucherTypeId { get; set; }

        public List<GiftVoucherListDto> GiftVoucherItems { get; set; }

        public ImportGiftVoucherInput()
        {
            GiftVoucherItems = new List<GiftVoucherListDto>();
        }
    }


    public class CreateOrUpdateGiftVoucherTypeInput : IInputDto
    {
        [Required]
        public GiftVoucherTypeEditDto GiftVoucherType { get; set; }

        public LocationGroupDto LocationGroup { get; set; }

        public CreateOrUpdateGiftVoucherTypeInput()
        {
            GiftVoucherType = new GiftVoucherTypeEditDto();
            LocationGroup = new LocationGroupDto();
        }
    }

    public class CreateOrUpdateGiftVoucherInput : IInputDto
    {
        [Required]
        public GiftVoucherEditDto GiftVoucher { get; set; }

        public CreateOrUpdateGiftVoucherInput()
        {
            GiftVoucher = new GiftVoucherEditDto();
        }
    }

    public class UpdateGiftVoucherOutput
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }


    public class ValidateVoucherInput : IInputDto
    {
        public decimal Amount { get; set; }
        public string Voucher { get; set; }
        public int Location { get; set; }
        public string TicketNumber { get; set; }
        public decimal TicketTotal { get; set; }
        public string MemberCode { get; set; }
    }

    public class ValidateVoucherOutput : IOutputDto
    {
        public decimal Discount { get; set; }
        public bool IsValid { get; set; }
        public string Reason { get; set; }
        public string VoucherCode { get; set; }
    }

    public class MemberVoucherInput : IInputDto
    {
        public string Voucher { get; set; }
        public int MemberId { get; set; }
    }
}