﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.Gift.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Engage.Gift
{
    namespace DinePlan.DineConnect.Engage
    {
        public interface IGiftVoucherTypeAppService : IApplicationService
        {
            Task<PagedResultOutput<GiftVoucherTypeListDto>> GetAll(GetGiftVoucherTypeInput inputDto);

            Task<FileDto> GetAllToExcel();

            Task<FileDto> GetAllGiftVoucherToExcel(GetGiftVoucherInput inputDto);

            Task<GetGiftVoucherTypeForEditOutput> GetGiftVoucherTypeForEdit(NullableIdInput nullableIdInput);

            Task<GetGiftVoucherForEditOutput> GetGiftVoucherForEdit(NullableIdInput nullableIdInput);

            Task<IdInput> CreateOrUpdateGiftVoucherType(CreateOrUpdateGiftVoucherTypeInput input);

            void ImportGiftVouchers(ImportGiftVoucherInput input);

            Task<UpdateGiftVoucherOutput> UpdateGiftVoucher(CreateOrUpdateGiftVoucherInput input);

            Task<string> GenerateVoucherForType(CreateOrUpdateGiftVoucherTypeInput input);

            Task DeleteGiftVoucherType(IdInput input);

            Task DeleteMultiGiftVoucher(List<int> ids);

            Task ActivateMultiGiftVoucher(List<int> ids);

            Task DeleteGiftVoucher(IdInput input);

            Task<ListResultOutput<GiftVoucherTypeListDto>> GetIds();

            Task<GiftVoucherStatsDto> GetVouchers(GetGiftVoucherInput inputDto);

            Task<FileDto> GetVoucherInExcel(int voucherType);

            Task<string> GenerateVoucher(int id, bool repeat = false, bool autoAttach = false);

            Task<ValidateVoucherOutput> ValidateVoucher(ValidateVoucherInput input);

            Task<int> ApplyVoucher(ValidateVoucherInput input);

            Task LinkMemberToVoucher(MemberVoucherInput input);

            Task UnLinkMemberFromVoucher(MemberVoucherInput input);

            Task EmailVoucher(MemberVoucherInput input);

            Task<List<GiftVoucherEditDto>> MemberVouchers(MemberVoucherInput input);

            Task<ValidateVoucherOutput> GenerateAutoAttachVoucher(ValidateVoucherInput input);

            Task ActivateItem(IdInput input);

            Task ActivateGiftVoucher(IdInput input);

            Task<GiftVoucher> GetGiftVoucher(int id);

            Task<FileDto> ExprotErrorToExcel(List<GiftVoucherListDto> giftVoucherListDtos);


        }
    }
}