﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Sync;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

using DinePlan.DineConnect.Engage.ServiceInfo.Dtos;
using Abp.Net.Mail;
using DinePlan.DineConnect.ShortMessage;
using DinePlan.DineConnect.Configuration;
using Abp.Configuration;
using Castle.Core.Logging;
using DinePlan.DineConnect.Engage.Member;
using Newtonsoft.Json;
using System;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Connect.Master;

namespace DinePlan.DineConnect.Engage.ServiceInfo.Implementation
{
    public class MemberServiceInfoTemplateAppService : DineConnectAppServiceBase, IMemberServiceInfoTemplateAppService
    {
        private readonly ISyncAppService _syncAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<MemberServiceInfoTemplate> _memberServiceInfoTemplateRepository;
        private readonly IRepository<MemberServiceInfoType> _memberServiceInfoTypeRepository;
        private readonly IEmailSender _emailSender;
        private readonly IShortMessageProvider _messageProvier;
        private readonly SettingManager _settingManager;
        private readonly ILogger _logger;
        private readonly IMemberAppService _memberAppService;
        private readonly IConnectReportAppService _connectReportAppService;
        private readonly IRepository<Ticket> _ticketRepository;
        IRepository<TransactionType> _transactionTypeRepository;
        public MemberServiceInfoTemplateAppService(
            IUnitOfWorkManager unitOfWorkManager,
            IEmailSender emailSender,
            IShortMessageProvider messageProvier,
            SettingManager settingManager,
            ILogger logger,
            IRepository<MemberServiceInfoTemplate> memberServiceInfoTemplateRepository,
            IRepository<MemberServiceInfoType> memberServiceInfoTypeRepository,
            IRepository<Ticket> ticketRepository,
            IRepository<TransactionType> transactionTypeRepository,
            ISyncAppService syncAppService,
            IMemberAppService memberAppService,
            IConnectReportAppService connectReportAppService)
        {
            _syncAppService = syncAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _emailSender = emailSender;
            _messageProvier = messageProvier;
            _settingManager = settingManager;
            _logger = logger;
            _memberServiceInfoTemplateRepository = memberServiceInfoTemplateRepository;
            _memberServiceInfoTypeRepository = memberServiceInfoTypeRepository;
            _transactionTypeRepository = transactionTypeRepository;
            _ticketRepository = ticketRepository;
            _memberAppService = memberAppService;
            _connectReportAppService = connectReportAppService;
        }

        public async Task<IdInput> CreateOrUpdateMemberServiceInfoTemplate(CreateOrUpdateMemberServiceInfoTemplateInput input)
        {
            if (input.MemberServiceInfoTemplate.IsActive) {
                await SetAllInactive(input);
            }

            if (input.MemberServiceInfoTemplate.Id.HasValue && input.MemberServiceInfoTemplate.Id > 0)
            {
                return await UpdateMemberServiceInfoTemplate(input);
            }
            else
            {
                return await CreateMemberServiceInfoTemplate(input);
            }
        }

        private async Task SetAllInactive(CreateOrUpdateMemberServiceInfoTemplateInput input) {
            var getResult = _memberServiceInfoTemplateRepository.GetAll()
                .AsNoTracking()
                .Where(t => t.IsActive && 
                    t.ServiceInfoTypeId == input.MemberServiceInfoTemplate.ServiceInfoTypeId &&
                    t.Id != input.MemberServiceInfoTemplate.Id).ToArray();
            foreach (var item in getResult)
            {
                item.IsActive = false;
                await _memberServiceInfoTemplateRepository.UpdateAsync(item);
            }
        }

        protected async Task<IdInput> CreateMemberServiceInfoTemplate(CreateOrUpdateMemberServiceInfoTemplateInput input)
        {
            var memberServiceInfoTemplate = input.MemberServiceInfoTemplate.MapTo<MemberServiceInfoTemplate>();
            try
            {
                var createResult = await _memberServiceInfoTemplateRepository.InsertAndGetIdAsync(memberServiceInfoTemplate);
                return new IdInput() { Id = createResult };
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        protected async Task<IdInput> UpdateMemberServiceInfoTemplate(CreateOrUpdateMemberServiceInfoTemplateInput input)
        {
            var memberServiceInfoTemplate = input.MemberServiceInfoTemplate.MapTo<MemberServiceInfoTemplate>();
            try
            {
                var updateResutl = await _memberServiceInfoTemplateRepository.UpdateAsync(memberServiceInfoTemplate);
                return new IdInput() { Id = updateResutl.Id };
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public async Task<PagedResultOutput<MemberServiceInfoTemplateListDto>> GetAll(GetMemberServiceInfoTemplateInput input)
        {
            var allItems = _memberServiceInfoTemplateRepository.GetAll()
                .Include(t=>t.ServiceInfoType)
                .AsNoTracking()
                .Where(g => g.IsDeleted == input.IsDeleted);

            allItems = allItems.WhereIf(!input.Filter.IsNullOrEmpty(),
                g => g.Name.Contains(input.Filter));

            var output = new PagedResultOutput<MemberServiceInfoTemplateListDto>();

            var sortedItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            output.Items = sortedItems.MapTo<List<MemberServiceInfoTemplateListDto>>();
            output.TotalCount = output.Items.Count;

            return output;
        }

        public async Task<GetMemberServiceInfoTemplateOutput> GetMemberServiceInfoTemplateForUpdate(NullableIdInput input)
        {
            var dbDaypart = await _memberServiceInfoTemplateRepository.GetAll()
                .Where(d => d.Id == input.Id).FirstOrDefaultAsync();
            var output = new GetMemberServiceInfoTemplateOutput();
            output.MemberServiceInfoTemplate = dbDaypart.MapTo<MemberServiceInfoTemplateEditDto>();
            return output;
        }

        public async Task DeleteMemberServiceInfoTemplate(NullableIdInput input)
        {
            await _memberServiceInfoTemplateRepository.DeleteAsync(input.Id ?? 0);
        }

        public async Task<PagedResultOutput<MemberServiceInfoTypeListDto>> GetMemberServiceInfoTypes(GetMemberServiceInfoTypeInput input) {
            var allItems = _memberServiceInfoTypeRepository.GetAll()
                .AsNoTracking()
                .Where(g => g.IsDeleted == input.IsDeleted);

            var output = new PagedResultOutput<MemberServiceInfoTypeListDto>();

            var sortedItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            output.Items = sortedItems.MapTo<List<MemberServiceInfoTypeListDto>>();
            output.TotalCount = output.Items.Count;

            return output;
        }

        public async Task<bool> SendMemberServiceInfo(MemberServiceInfoRequest input) {
            var emailMobile = await GetMemberEmailMobileNo(input);

            var templateIds = GetTemplateIdsToUse(TemplateSelectOptionEnum.NotiMessage);
            var infoPageTemplateIds = GetTemplateIdsToUse(TemplateSelectOptionEnum.InfoPageTemplate);

            foreach (var templateId in templateIds)
            {
                if (templateId == 0) continue;

                MemberServiceInfoTypeEnum? servinceInfoType;
                var message = BuildMessageBody(templateId, out servinceInfoType);

                message = message.Replace("$[Template.Id]", infoPageTemplateIds[0].ToString());
                message = message.Replace("$[Ticket.TicketNumber]", input.TicketNo);
                message = message.Replace("$[Member.Name]", emailMobile.MemberName);
                message = message.Replace("$[Member.Id]", emailMobile.MemberId.ToString());

                if (servinceInfoType == MemberServiceInfoTypeEnum.EssNotiEmail)
                {
                    try
                    {
                        if (!string.IsNullOrEmpty(emailMobile.Email))
                        {
                            if (!string.IsNullOrEmpty(message))
                            {
                                await _emailSender.SendAsync(emailMobile.Email, "Check you invoice", message);
                                _logger.Info("Email was sent successfully to this user: " + emailMobile.Email);
                            }
                            else
                            {
                                _logger.Info("Message body is empty.");
                            }

                        }
                    }
                    catch (System.Exception ex)
                    {
                        _logger.Error("Failed sending eamil : ", ex);
                    }
                }
                else if (servinceInfoType == MemberServiceInfoTypeEnum.EssNotiSms) {
                    try
                    {
                        if (!string.IsNullOrEmpty(emailMobile.MobileNo))
                        {
                            var messType = await _settingManager.GetSettingValueAsync<int>(AppSettings.ShortMessageSettings.MessageType);
                            var messageSetting =
                                await _settingManager.GetSettingValueAsync(AppSettings.ShortMessageSettings.MessageSetting);
                            if (!string.IsNullOrEmpty(message))
                            {
                                _messageProvier.SendMessage(message, emailMobile.MobileNo, messType, messageSetting);
                                _logger.Info("Short message was sent successfully to this number: " + emailMobile.MobileNo);
                            }
                            else
                            {
                                _logger.Info("Message body is empty.");
                            }
                        }
                    }
                    catch (System.Exception ex)
                    {
                        _logger.Error("Failed sending short message : ", ex);
                    }
                }
                
            }
            return true;
        }

        private int[] GetTemplateIdsToUse(TemplateSelectOptionEnum option)
        {
            if (option == TemplateSelectOptionEnum.NotiMessage)
            {
                var id1 = Task.Run<MemberServiceInfoTemplate>(() =>
                {
                    var getResult = _memberServiceInfoTemplateRepository.GetAll()
                        .Include(t => t.ServiceInfoType)
                        .AsNoTracking()
                        .Where(t => t.IsActive == true && t.ServiceInfoType.Code == MemberServiceInfoTypeEnum.EssNotiEmail.ToString())
                        .FirstOrDefault();
                    return getResult;
                }).Result?.Id ?? 0;

                var id2 = Task.Run<MemberServiceInfoTemplate>(() =>
                {
                    var getResult = _memberServiceInfoTemplateRepository.GetAll()
                        .Include(t => t.ServiceInfoType)
                        .AsNoTracking()
                        .Where(t => t.IsActive == true && t.ServiceInfoType.Code == MemberServiceInfoTypeEnum.EssNotiSms.ToString())
                        .FirstOrDefault();
                    return getResult;
                }).Result?.Id ?? 0;
                Task.WaitAll();
                return new int[] { id1, id2 };
            }
            else {
                var getResult = _memberServiceInfoTemplateRepository.GetAll()
                    .Include(t => t.ServiceInfoType)
                    .AsNoTracking()
                    .Where(t => t.IsActive == true && t.ServiceInfoType.Code == MemberServiceInfoTypeEnum.Ess.ToString())
                    .FirstOrDefault();
                return new int[] {getResult.Id };
            }
        }

        private async Task<MemberServiceInfoEmailAndMobileNo> GetMemberEmailMobileNo(MemberServiceInfoRequest input) {
            var emailMobile = new MemberServiceInfoEmailAndMobileNo ();
            var member = await _memberAppService.GetMemberForInfo(new NullableIdInput { Id = input.MemberId });

            if (member != null) {
                
                emailMobile.Email = member.MemberContactDetail
                    .Where(c => c.ContactDetailType == ContactDetailType.Email)
                    .OrderByDescending(c => c.Id).FirstOrDefault().Detail;

                emailMobile.MobileNo = member.MemberContactDetail
                    .Where(c => c.ContactDetailType == ContactDetailType.HandPhone)
                    .OrderByDescending(c => c.Id).FirstOrDefault().Detail;

                emailMobile.MemberId = member.Member.Id??0;
                emailMobile.MemberName = member.Member.Name;
            }

            return emailMobile;
        }

        private string BuildMessageBody(int templateId, out MemberServiceInfoTypeEnum? serviceInfoType) {
            var template = GetContent(templateId,out serviceInfoType);
            if (template != null)
            {
                TemplateOutput output = JsonConvert.DeserializeObject<TemplateOutput>(template);
                return output.Html;
            }
            else {
                return "";
            }
        }

        private string GetContent(int templateId, out MemberServiceInfoTypeEnum? serviceInfoType) {
            var template = _memberServiceInfoTemplateRepository.GetAll()
                .Include(t=>t.ServiceInfoType)
                .AsNoTracking()
                .Where(t => t.Id == templateId)
                .OrderByDescending(t => t.Id).FirstOrDefault();
            if (template != null) {
                serviceInfoType = GetServiceInfoTypeEnum(template.ServiceInfoType.Code);
                return template.Template; 
            }
            serviceInfoType = null;
            return "";
        }

        public string GetTemplateContent(int templateId) {
            MemberServiceInfoTypeEnum? serviceInfoType;
            return GetContent(templateId, out serviceInfoType);
        }

        public MemberServiceInfoTemplateListDto GetTemplateById(int templateId) {
            var template = _memberServiceInfoTemplateRepository.GetAll()
                .Include(t => t.ServiceInfoType)
                .AsNoTracking()
                .Where(t => t.Id == templateId)
                .OrderByDescending(t => t.Id).FirstOrDefault();
            if (template != null)
            {
                return template.MapTo<MemberServiceInfoTemplateListDto>();
            }
            return null;
        }

        private MemberServiceInfoTypeEnum GetServiceInfoTypeEnum(string code) {
            var e = Enum.Parse(typeof(MemberServiceInfoTypeEnum), code);
            return (MemberServiceInfoTypeEnum)e;
        }

        public async Task<Ticket> GetTicketDataByTicketNo(string ticketNo) {
            var ticket = _ticketRepository.GetAll()
                .Include(t => t.Location)
                .Include(t => t.Payments)
                .Include(t => t.Transactions)
                .Include(t => t.Orders)
                .AsNoTracking()
                .Where(t => t.TicketNumber == ticketNo).FirstOrDefault() ;
            foreach (var tran in ticket.Transactions)
            {
                var trn = tran.TransactionType;
            }
            return ticket;
        }

        public MemberServiceInfoTemplateListDto GetActiveTemplate(NullableIdInput serviceInfoTypeIdInput)
        {
            var getResult = _memberServiceInfoTemplateRepository.GetAll()
                .AsNoTracking()
                .Where(t => t.IsActive == true && 
                    t.ServiceInfoTypeId == serviceInfoTypeIdInput.Id).FirstOrDefault();
            if (getResult != null)
            {
                var template = getResult.MapTo<MemberServiceInfoTemplateListDto>();
                return template;
            }
            return null;
        }

        private enum TemplateSelectOptionEnum { 
            NotiMessage,
            InfoPageTemplate
        }
    }
}

