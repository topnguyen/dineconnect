﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;

namespace DinePlan.DineConnect.Engage.ServiceInfo.Dtos
{
    [AutoMapFrom(typeof(MemberServiceInfoType))]
    public class MemberServiceInfoTypeListDto : FullAuditedEntityDto
    {
        public int? Id { get; set; }
        public virtual string Code { get; set; }
        public virtual string Description { get; set; }
    }

    public class CreateOrUpdateMemberServiceInfoTypeInput : IInputDto
    {
        public MemberServiceInfoTypeEditDto MemberServiceInfoType { get; set; }
    }

    public class GetMemberServiceInfoTypeInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public string Operation { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    [AutoMapTo(typeof(MemberServiceInfoType))]
    public class MemberServiceInfoTypeEditDto
    {
        public int? Id { get; set; }
        public virtual string Code { get; set; }
        public virtual string Description { get; set; }
    }

    public class GetMemberServiceInfoTypeOutput : IOutputDto
    {
        public MemberServiceInfoTypeEditDto MemberServiceInfoType { get; set; }
    }

    public class MemberServiceInfoRequest {
        public int MemberId { get; set; }
        public string TicketNo { get; set; }
        public string Variable1 { get; set; }
        public string Variable2 { get; set; }
        public string Variable3 { get; set; }
        public string Variable4 { get; set; }
        public string Variable5 { get; set; }
        public string Variable6 { get; set; }
        public string Variable7 { get; set; }
        public string Variable8 { get; set; }
        public string Variable9 { get; set; }
        public string Variable10 { get; set; }
    }

    public class MemberServiceInfoEmailAndMobileNo {
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public int MemberId { get; set; }
        public string MemberName { get; set; }
    }

    public class TemplateOutput
    {
        public string Assets { get; set; }
        public string Components { get; set; }
        public string Css { get; set; }
        public string Html { get; set; }
        public string Styles { get; set; }
    }
}
