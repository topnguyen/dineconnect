﻿
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;

namespace DinePlan.DineConnect.Engage.ServiceInfo.Dtos
{
    [AutoMapFrom(typeof(MemberServiceInfoTemplate))]
    public class MemberServiceInfoTemplateListDto : FullAuditedEntityDto
    {
        public int TenantId { get; set; }
        public int? Id { get; set; }
        public string Name { get; set; }
        public int ServiceInfoTypeId { get; set; }
        public MemberServiceInfoType ServiceInfoType { get; set; }
        public string Template { get; set; }
        public bool IsActive { get; set; }
        public bool StoredAsPlainText { get; set; }
    }

    public class CreateOrUpdateMemberServiceInfoTemplateInput : IInputDto
    {
        public MemberServiceInfoTemplateEditDto MemberServiceInfoTemplate { get; set; }
    }

    public class GetMemberServiceInfoTemplateInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public string Operation { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    [AutoMapTo(typeof(MemberServiceInfoTemplate))]
    public class MemberServiceInfoTemplateEditDto
    {
        public int TenantId { get; set; }
        public int? Id { get; set; }
        public string Name { get; set; }
        public int ServiceInfoTypeId { get; set; }
        public MemberServiceInfoType ServiceInfoType { get; set; }
        public string Template { get; set; }
        public bool IsActive { get; set; }
        public bool StoredAsPlainText { get; set; }
    }

    public class GetMemberServiceInfoTemplateOutput : IOutputDto
    {
        public MemberServiceInfoTemplateEditDto MemberServiceInfoTemplate { get; set; }
    }
}
