﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Engage.ServiceInfo.Dtos;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Engage.ServiceInfo
{
    public interface IMemberServiceInfoTemplateAppService : IApplicationService
    {
        Task<IdInput> CreateOrUpdateMemberServiceInfoTemplate(CreateOrUpdateMemberServiceInfoTemplateInput input);
        Task<PagedResultOutput<MemberServiceInfoTemplateListDto>> GetAll(GetMemberServiceInfoTemplateInput input);
        Task<GetMemberServiceInfoTemplateOutput> GetMemberServiceInfoTemplateForUpdate(NullableIdInput input);
        Task DeleteMemberServiceInfoTemplate(NullableIdInput input);
        Task<PagedResultOutput<MemberServiceInfoTypeListDto>> GetMemberServiceInfoTypes(GetMemberServiceInfoTypeInput input);
        Task<bool> SendMemberServiceInfo(MemberServiceInfoRequest input);
        string GetTemplateContent(int templateId);
        Task<Ticket> GetTicketDataByTicketNo(string ticketNo);
        MemberServiceInfoTemplateListDto GetTemplateById(int templateId);
        MemberServiceInfoTemplateListDto GetActiveTemplate(NullableIdInput serviceInfoTypeIdInput);
    }
}

