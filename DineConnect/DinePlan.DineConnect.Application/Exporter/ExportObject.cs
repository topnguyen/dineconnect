﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Exporter
{
    public enum ExportType { Excel = 0, HTML = 1, PDF = 2 }
}
