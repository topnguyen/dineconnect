﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Abp.Dependency;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter.Util;

namespace DinePlan.DineConnect.Exporter
{
    public class HtmlExporter : DineConnectAbstractExporter, ITransientDependency
    {
        int _pairObjectHeaderLength = 0;
        public HtmlExporter(ExportInputObject exportDataObject)
        {
            ExportDataObject = exportDataObject;
        }

        public override void Export(string folder, FileDto fileInfo, string body)
        {
            string dataRowHtml  = string.Empty;
            string tableHeader  = string.Empty;
            string totalRow     = string.Empty;

            var filePath = Path.Combine(folder, fileInfo.FileToken);
            FileInfo info = new FileInfo(filePath);

            using (var sw = info.CreateText()) {

                #region Header
                foreach (var item in ExportDataObject.Fields)
                {
                    var displayName = string.IsNullOrEmpty(item.Value.DisplayName) == true ? item.Key : item.Value.DisplayName;

                    if (item.Value.FieldType == ExportFiledType.Pair)
                    {
                        var pairObjectNames = item.Value.PairObjectDisplayNames.ToList<string>();
                        tableHeader = pairObjectNames.Aggregate(tableHeader, (current, pObjField) => current + $"<th>{pObjField.ToUpper()}</th>");
                    }
                    else
                    {
                        tableHeader += $"<th>{displayName.ToUpper()}</th>";                       
                    }
                }
                #endregion
                #region Rows

                int i = 0;
                foreach (ExportDataObject item in ExportDataObject.ExportObjects)
                {
                    string cls = "oddRow";
                    if (i++ % 2 == 0)
                        cls = "evenRow";

                    dataRowHtml += $"<tr class='{cls}'>";
                    foreach (var field in ExportDataObject.Fields)
                    {
                        var alignmentCss = "style=\"text-align:left;\"";
                        var alignment = field.Value.Alignment;
                        if (alignment == FieldAlignment.Center)
                        {
                            alignmentCss = "style=\"text-align:center;\"";
                        }
                        else if (alignment == FieldAlignment.Right)
                        {
                            alignmentCss = "style=\"text-align:right;\"";
                        }


                        if (field.Value.FieldType != ExportFiledType.Pair)
                        {
                            dataRowHtml += $"<td {alignmentCss}> {MakeBold(GetFieldValue(item, field), field.Key)} </td>";
                        }
                        else
                        {
                            //dataRowHtml += $"<td {alignmentCss}> {GetPairObjectValue(item, field)} </td>";
                            var content = GetPairObjectValue(item, field);
                            if (string.IsNullOrEmpty(content) == false)
                            {
                                dataRowHtml += content;
                            }
                            else
                            {
                                for (int k = 0; k < _pairObjectHeaderLength; k++)
                                {
                                    dataRowHtml += "<td></td>";
                                }
                            }
                        }
                    }
                    dataRowHtml += $"<tr/>";
                }
                #endregion
                #region Total
                int colSpan = 0;
                var totalText = "TOTAL";
                string alignmentCSS = "style=\"text-align:right;\"";

                foreach (var item in ExportDataObject.Fields)
                {
                    if (item.Value.FieldType == ExportFiledType.Pair)
                    {
                        foreach (var pofName in item.Value.PairObjectDisplayNames)
                        {
                            var keyName = item.Key + "_" + pofName;
                            if (_totals.ContainsKey(keyName) && _totals[keyName] > 0)
                            {
                                totalRow += $"<td {alignmentCSS}><strong>{_totals[keyName].ToString("N2")}</strong></td>";   
                            }
                            else
                            {
                                totalRow += $"<td></td>";
                            }
                        }
                    }
                    else
                    {
                        if (_totals.ContainsKey(item.Key))
                        {
                            if (colSpan > 0)
                            {
                                totalRow += $"<td colspan=\"{colSpan}\"><strong>{totalText}</td></td>";
                                colSpan = 0;
                                totalText = "";
                            }
                            totalRow += $"<td {alignmentCSS}><strong>{_totals[item.Key].ToString("N2")}</strong></td>";
                        }
                        else
                        {
                            colSpan++;
                        }
                    }
                }
                #endregion
                StringBuilder builder = new StringBuilder();

                if (string.IsNullOrEmpty(body))
                {

                    builder.Append(GetHtmlHeader());

                    builder.Append("<body>");
                    builder.Append("<h3>");
                    builder.Append(ExportDataObject.FirstPage?.Replace("{NL}", "<br/>") ?? "");
                    builder.Append("</h3><br/>");
                    builder.Append("<hr/>");
                    builder.Append("<table>");
                    builder.Append("<thead>");
                    builder.Append("<tr>");
                    builder.Append(tableHeader);
                    builder.Append("</tr>");
                    builder.Append("</thead>");
                    builder.Append("<tbody>");
                    builder.Append(dataRowHtml);
                    builder.Append("<tr style='background-color:#ccc;'>");
                    builder.Append(totalRow);
                    builder.Append("</tr>");
                    builder.Append("</tbody>");
                    builder.Append("</table><br/>");

                    builder.Append("<p>");
                    builder.Append(ExportDataObject.LastPage?.Replace("{NL}", "<br/>") ?? "");
                    builder.Append("</p><br/>");
                    builder.Append("</body>");
                    builder.Append(GetHtmlFooter());
                }
                else
                {
                    builder.Append(GetHtmlHeader());

                    builder.Append("<body>");
                    builder.Append("<h3>");
                    builder.Append(ExportDataObject.FirstPage?.Replace("{NL}", "<br/>") ?? "");
                    builder.Append("</h3><br/>");
                    builder.Append("<hr/>");
                    builder.Append(body);
                    builder.Append("<p>");
                    builder.Append(ExportDataObject.LastPage?.Replace("{NL}", "<br/>") ?? "");
                    builder.Append("</p><br/>");
                    builder.Append("</body>");
                    builder.Append(GetHtmlFooter());
                }
                var strOutput = builder.ToString();
                sw.WriteLine(strOutput);
                sw.Flush();
                sw.Close();
            }
        }

        private string GetHtmlFooter()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("</body>");
            builder.Append("</head>");
            builder.Append("</html>");
            return builder.ToString();
        }

        public string GetHtmlHeader()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("<html>");
            builder.Append("<meta http-equiv='Content-Type' content='text/html'; charset='UTF-8'>");
            builder.Append("<head>");
            builder.Append("<title> </title>");

            builder.Append(@"<style> 
             body  { 
              font:400 14px 'Calibri','Arial';
              padding:10px;
            }
            table  { 
                margin-top: 10px;
                border-collapse: collapse;
                width: 100%;
                margin-bottom: 10px;
            }

            table td, table th {
                border: 1px solid #ddd;
                padding: 8px;
            }

            table tr:hover {background-color: #ddd;}

            table th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #364150;
                color: white;
            }
            thead {
                display: table-header-group;
            }
            tfoot {
                display: table-row-group;
            }
            tr{
                page-break-inside: avoid;
            }
            blockquote {
              color:white;
              text-align:center;
            }

            h3{
              text-align:center;
              text-transform: capitalize;
              font-size: medium;
            }



                </style>");

            return builder.ToString();
        }
    }
}
