﻿using System;
using System.IO;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using OpenHtmlToPdf;

namespace DinePlan.DineConnect.Exporter.Util
{
    public class DineConnectDocExporter
    {
        public IAppFolders AppFolders { get; set; }
        private readonly ExportInputObject _inputObj;
        private FileDto fileDto;
        private string _folder;
        private string _htmlbody;

        public DineConnectDocExporter(ExportInputObject inputObj, string htmlbody) : this(inputObj)
        {
            _htmlbody = htmlbody;
        }

        public DineConnectDocExporter(ExportInputObject inputObj)
        {
            _inputObj = inputObj;
            fileDto = new FileDto();
            if (inputObj.ExportType == ExportType.HTML)
            {
                fileDto.FileName = _inputObj.FileName + ".html";
                fileDto.FileType = MimeTypeNames.ApplicationXhtmlXml;
            }
            if (inputObj.ExportType == ExportType.Excel)
            {
                fileDto.FileName = inputObj.FileName + ".xlsx";
                fileDto.FileType = MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet;
            }

        }

        public FileDto ExportFile(string folder)
        {
            _folder = folder;
            switch (_inputObj.ExportType)
            {
                case ExportType.Excel:
                    new ExcelExporter(_inputObj).Export(folder, fileDto, "");
                    break;
                case ExportType.HTML:
                    new HtmlExporter(_inputObj).Export(folder, fileDto, _htmlbody);
                    break;

                case ExportType.PDF:
                    new HtmlExporter(_inputObj).Export(folder, fileDto, _htmlbody);
                    string fileInfo = Path.Combine(folder, fileDto.FileToken);
                    bool sucess = GeneratePdfFile();
                    if (sucess)
                    {
                        if (File.Exists(fileInfo))
                        {
                            try
                            {
                                File.Delete(fileInfo);
                            }
                            catch (Exception)
                            {
                                // ignored
                            }
                        }
                    }

                    break;
            }
            return fileDto;
        }

        private bool GeneratePdfFile()
        {
            bool returnStatus = false;
            try
            {
                string filePath = Path.Combine(_folder, fileDto.FileToken);
                if (File.Exists(filePath))
                {
                    string allContent = File.ReadAllText(filePath);

                    if (!string.IsNullOrEmpty(allContent))
                    {
                        var pdf = Pdf.From(allContent).OfSize(_inputObj.PaperSize);
                        if (_inputObj.Portrait)
                        {
                            pdf.Portrait();
                        }

                        var output = pdf.Content();
                        fileDto = new FileDto
                        {
                            FileName = _inputObj.FileName + ".pdf",
                            FileType = MimeTypeNames.ApplicationPdf
                        };
                        var myPath = Path.Combine(_folder, fileDto.FileToken);

                        File.WriteAllBytes(myPath, output);
                        returnStatus = true;
                    }
                }
            }
            catch (Exception e)
            {
                returnStatus = false;
            }

            return returnStatus;
        }
    }
}
