﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Dto
{
    public class PagedInputDto : IInputDto, IPagedResultRequest
    {
        

        [Range(1, AppConsts.MaxPageSize)]
        public int MaxResultCount { get; set; }

        [Range(0, int.MaxValue)]
        public int SkipCount { get; set; }

        public PagedInputDto()
        {
            MaxResultCount = AppConsts.DefaultPageSize;
        }
    }
    public class MaxPagedInputDto : IInputDto, IPagedResultRequest
    {
        [Range(1, AppConsts.MaximumPageSize)]
        public int MaxResultCount { get; set; }

        [Range(0, int.MaxValue)]
        public int SkipCount { get; set; }

        public MaxPagedInputDto()
        {
            MaxResultCount = AppConsts.DefaultPageSize;
        }
    }
}