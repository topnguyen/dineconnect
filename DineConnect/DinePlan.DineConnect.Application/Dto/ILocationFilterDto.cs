﻿using DinePlan.DineConnect.Connect.Location;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Dto
{
    public interface ILocationFilterDto
    {
        LocationGroupDto LocationGroup { get; set; }

        int UserId { get; set; }
    }
}
