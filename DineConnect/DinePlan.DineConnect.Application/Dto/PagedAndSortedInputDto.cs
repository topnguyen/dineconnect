﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Dto
{
    public class PagedAndSortedInputDto : PagedInputDto, ISortedResultRequest
    {
        public int? Id { get; set; }

        public string Sorting { get; set; }

        public PagedAndSortedInputDto()
        {
            MaxResultCount = AppConsts.DefaultPageSize;
        }
    }

    public class MaxPagedAndSortedInputDto : MaxPagedInputDto, ISortedResultRequest
    {
        public int? Id { get; set; }
        public string Sorting { get; set; }

        public MaxPagedAndSortedInputDto()
        {
            MaxResultCount = AppConsts.MaximumPageSize;
        }
    }
}