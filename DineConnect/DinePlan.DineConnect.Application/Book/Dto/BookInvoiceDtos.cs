﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Book.Dto
{
    [AutoMapFrom(typeof(BookInvoice))]
    public class BookInvoiceListDto : FullAuditedEntityDto
    {
        public virtual int LocationId { get; set; }
        public  virtual  string LocationName { get; set; }
        public virtual long UserId { get; set; }
        public  virtual  string UserName { get; set; }
        public int TenantId { get; set; }
        public string Files { get; set; }
        public BookInvoiceStatus Status { get; set; }

        public string CurrentStatus { get; set; }

        public int? InvoiceId { get; set; }
        public  string SupplierName { get; set; }
    }
    [AutoMapTo(typeof(BookInvoice))]
    public class BookInvoiceEditDto
    {
        public int? Id { get; set; }
        public virtual int LocationId { get; set; }
        public virtual long UserId { get; set; }
        public int TenantId { get; set; }
        public string Files { get; set; }
        public BookInvoiceStatus Status { get; set; }

        public int? InvoiceId { get; set; }
    }

    public class GetBookInvoiceInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public int LocationId { get; set; }
        public virtual long UserId { get; set; }
        public string Operation { get; set; }

        

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id Desc";
            }
        }
    }
    public class GetBookInvoiceForEditOutput : IOutputDto
    {
        public BookInvoiceEditDto BookInvoice { get; set; }
    }
    public class CreateOrUpdateBookInvoiceInput : IInputDto
    {
        [Required]
        public BookInvoiceEditDto BookInvoice { get; set; }
    }
}

