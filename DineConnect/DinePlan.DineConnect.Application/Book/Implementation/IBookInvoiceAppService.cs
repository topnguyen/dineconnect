﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Book.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Book.Master
{
    public interface IBookInvoiceAppService : IApplicationService
    {
        Task<PagedResultOutput<BookInvoiceListDto>> GetAll(GetBookInvoiceInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetBookInvoiceForEditOutput> GetBookInvoiceForEdit(NullableIdInput nullableIdInput);
        Task<IdInput> CreateOrUpdateBookInvoice(CreateOrUpdateBookInvoiceInput input);
        Task DeleteBookInvoice(IdInput input);

        
    }
}