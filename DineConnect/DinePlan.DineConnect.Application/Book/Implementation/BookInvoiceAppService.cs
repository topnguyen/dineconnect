﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Reflection;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Book.Dto;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Utility.Exporter;

namespace DinePlan.DineConnect.Book.Implementation
{
    public class BookInvoiceAppService : DineConnectAppServiceBase, IBookInvoiceAppService
    {

        private readonly IExcelExporter _exporter;
        private readonly IBookInvoiceManager _bookinvoiceManager;
        private readonly IRepository<BookInvoice> _bookinvoiceRepo;
        private readonly IRepository<Location> _locationRepo;

        public BookInvoiceAppService(IBookInvoiceManager bookinvoiceManager,
            IRepository<BookInvoice> bookInvoiceRepo,
            IExcelExporter exporter,
            IRepository<Location> locationRepo)
        {
            _bookinvoiceManager = bookinvoiceManager;
            _bookinvoiceRepo = bookInvoiceRepo;
            _exporter = exporter;
            _locationRepo = locationRepo;
        }

        public async Task<PagedResultOutput<BookInvoiceListDto>> GetAll(GetBookInvoiceInput input)
        {
            var allItems = _bookinvoiceRepo.GetAll();
            allItems = allItems.WhereIf(input.LocationId>0, t => t.LocationId == input.LocationId);
            allItems = allItems.WhereIf(input.UserId > 0, t => t.UserId == input.UserId);

            var allList = (from mas in allItems
                    join loc in _locationRepo.GetAll() on mas.LocationId equals loc.Id
                    select new BookInvoiceListDto()
                    {
                        Id = mas.Id,
                        LocationId = mas.LocationId,
                        LocationName = loc.Name,
                        UserId = mas.UserId,
                        InvoiceId = mas.InvoiceId,
                        Status = mas.Status,
                        Files =  mas.Files,                        
                    }
                );

            var sortMenuItems = allList.OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<BookInvoiceListDto>>();

            var allItemCount = await allItems.CountAsync();
            foreach (var lst in allListDtos)
            {
                if (lst.Status == BookInvoiceStatus.Pending)
                    lst.CurrentStatus = L("Pending");
                else if (lst.Status == BookInvoiceStatus.Partial)
                    lst.CurrentStatus = L("Partial");
                else if (lst.Status == BookInvoiceStatus.WaitingForReason)
                    lst.CurrentStatus = L("WaitingForReason");
                else if (lst.Status == BookInvoiceStatus.Completed)
                    lst.CurrentStatus = L("Completed");
            }

            return new PagedResultOutput<BookInvoiceListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {


            var allList = await _bookinvoiceRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<BookInvoiceListDto>>();
            var baseE = new BaseExportObject()
            {
                ExportObject = allListDtos,
                ColumnNames = new string[] { "Id" }
            };
            return _exporter.ExportToFile(baseE, L("BookInvoice"));

        }

        public async Task<GetBookInvoiceForEditOutput> GetBookInvoiceForEdit(NullableIdInput input)
        {
            BookInvoiceEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _bookinvoiceRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<BookInvoiceEditDto>();
            }
            else
            {
                editDto = new BookInvoiceEditDto();
            }

            return new GetBookInvoiceForEditOutput
            {
                BookInvoice = editDto
            };
        }

        public async Task<IdInput> CreateOrUpdateBookInvoice(CreateOrUpdateBookInvoiceInput input)
        {
            if (ExistAllServerLevelBusinessRules(input) == true)
            {
                if (input.BookInvoice.Id.HasValue)
                {
                    return await UpdateBookInvoice(input);
                }
                else
                {
                    return await CreateBookInvoice(input);
                }
            }
            else
            {
                return new IdInput{Id = 0};
            }
        }

        public async Task DeleteBookInvoice(IdInput input)
        {
            await _bookinvoiceRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task<IdInput> UpdateBookInvoice(CreateOrUpdateBookInvoiceInput input)
        {
            try
            {
                var item = await _bookinvoiceRepo.GetAsync(input.BookInvoice.Id.Value);
                var dto = input.BookInvoice;

                CheckErrors(await _bookinvoiceManager.CreateSync(item));
                return new IdInput { Id = item.Id };
            }
            catch (Exception ex)
            {
                MethodBase m = MethodBase.GetCurrentMethod();
                string innerExceptionMessage = ex.InnerException != null ? ex.InnerException.Message : "";
                throw new UserFriendlyException("Method : " + m.ReflectedType.Name + " : " + m.Name + " " + ex.Message + " " + innerExceptionMessage);
            }
          
        }

        protected virtual async Task<IdInput> CreateBookInvoice(CreateOrUpdateBookInvoiceInput input)
        {
            try
            {
                var dto = input.BookInvoice.MapTo<BookInvoice>();
                dto.Status = BookInvoiceStatus.Pending;

                CheckErrors(await _bookinvoiceManager.CreateSync(dto));
                return new IdInput { Id = dto.Id };
            }
            catch (Exception ex)
            {
                MethodBase m = MethodBase.GetCurrentMethod();
                string innerExceptionMessage = ex.InnerException != null ? ex.InnerException.Message : "";
                throw new UserFriendlyException("Method : " + m.ReflectedType.Name + " : " + m.Name + " " + ex.Message + " " + innerExceptionMessage);
            }
        }

        public bool ExistAllServerLevelBusinessRules(CreateOrUpdateBookInvoiceInput input)
        {
            if (input.BookInvoice.LocationId == 0)
            {
                throw  new UserFriendlyException(L("Location") + " " + L("Error"));
            }

            if (input.BookInvoice.UserId == 0)
            {
                throw new UserFriendlyException(L("User") + " " + L("Error"));
            }

            if (input.BookInvoice.Files.IsNullOrEmpty() || input.BookInvoice.Files.IsNullOrWhiteSpace())
            {
                throw new UserFriendlyException(L("File") + " " + L("Address"));
            }
            return true;
        }
    }
}