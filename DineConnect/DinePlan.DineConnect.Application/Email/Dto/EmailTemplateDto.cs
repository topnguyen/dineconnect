﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Email.Dto
{
    public class GetEmailTemplateInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    public class GetEmailTemplateForEditOutput : IOutputDto
    {
        public EmailTemplateEditDto Template { get; set; }
    }

    public class CreateOrUpdateEmailTemplateInput : IInputDto
    {
        [Required]
        public EmailTemplateEditDto Template { get; set; }
    }

    [AutoMapFrom(typeof (Template.EmailTemplate))]
    public class EmailTemplateEditDto
    {
        public virtual int? Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Template { get; set; }
        public virtual string Variables { get; set; }
    }


    [AutoMapFrom(typeof (Template.EmailTemplate))]
    public class EmailTemplateListDto : FullAuditedEntityDto
    {
        public virtual string Name { get; set; }
        public virtual string Template { get; set; }
        public virtual string Variables { get; set; }
    }
}