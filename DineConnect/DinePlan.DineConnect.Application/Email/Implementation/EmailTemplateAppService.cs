﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Email.Dto;
using DinePlan.DineConnect.Template;

namespace DinePlan.DineConnect.Email.Implementation
{
    public class EmailTemplateAppService : DineConnectAppServiceBase, IEmailTemplateAppService
    {
        private readonly IRepository<EmailTemplate> _teManager;

        public EmailTemplateAppService(IRepository<EmailTemplate> teManager)
        {
            _teManager = teManager;
        }

        public async Task<PagedResultOutput<EmailTemplateListDto>> GetAll(GetEmailTemplateInput input)
        {
            var allItems = _teManager
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Name.Contains(input.Filter)
                );
            var sortMenuItems = new List<EmailTemplate>(allItems);

            if (input.Sorting != null)
            {
                sortMenuItems = await allItems
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();
            }
            var allListDtos = sortMenuItems.MapTo<List<EmailTemplateListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<EmailTemplateListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<GetEmailTemplateForEditOutput> GetTemplateForEdit(NullableIdInput input)
        {
            EmailTemplateEditDto editDto;

            if (input.Id.HasValue)
            {
                var category = await _teManager.GetAsync(input.Id.Value);
                editDto = category.MapTo<EmailTemplateEditDto>();
            }
            else
            {
                editDto = new EmailTemplateEditDto();
            }

            return new GetEmailTemplateForEditOutput
            {
                Template = editDto
            };
        }

        public async Task<int> CreateOrUpdateTemplate(CreateOrUpdateEmailTemplateInput input)
        {
            var returnOutput = 0;

            if (input.Template.Id.HasValue)
            {
                await UpdateTemplate(input);
                returnOutput = input.Template.Id.Value;
            }
            else
            {
                returnOutput = await CreateTemplate(input);
            }
            return returnOutput;
        }

        public async Task CreateEmailTemplateForTenant(int tenantId)
        {
            var syncC = new EmailTemplate { Name = "MEMBER VOUCHER", TenantId = tenantId,
                Template = GetMemberTemplate(),
                Variables = GetMemberVariable() };
            await _teManager.InsertAsync(syncC);
        }
        private string GetMemberVariable()
        {
            return "{MEMBER NAME}, {MEMBER CODE}, {VOUCHER NO}";
        }

        private string GetMemberTemplate()
        {
            return "<h2 style='text-align: center;'>DinePlan</h2><p style='text-align: center; '>Thank you so much for Being our Customer</p><p style='text-align: center; '><b>Here is the Happy voucher for you to Claim</b></p><h1 style='text-align: center; '>{VOUCHER NO}</h1><p style='text-align: center;'><br></p><p><br></p><hr><p style='text-align: center; '>© DinePlan - 2017</p>";
        }

        public async Task CreateEmailTemplateForResetPassword(int tenantId)
        {
            var syncC = new EmailTemplate
            {
                Name = "RESET PASSWORD",
                TenantId = tenantId,
                Template = GetResetPasswordTemplate(),
                Variables = GetResetPasswordVariable()
            };
            await _teManager.InsertAsync(syncC);
        }
        private string GetResetPasswordVariable()
        {
            return "{USERNAME}, {NEWPASSWORD}, {SENDER}";
        }

        private string GetResetPasswordTemplate()
        {
            return "<h3>Dear {USERNAME} ,</h3><br/><p>Your Password has been reset.</p><p>Your new password is <b >{NEWPASSWORD}</b> </p><br/><p>Regards,</p><p>{SENDER}</p>";
        }
        private async Task<int> CreateTemplate(CreateOrUpdateEmailTemplateInput input)
        {
            var returnValue = 0;

            var dto = input.Template.MapTo<EmailTemplate>();
            returnValue = await _teManager.InsertAndGetIdAsync(dto);
            return returnValue;
        }

        private async Task UpdateTemplate(CreateOrUpdateEmailTemplateInput input)
        {
            var item = await _teManager.GetAsync(input.Template.Id.Value);
            var dto = input.Template;
            item.Name = dto.Name;
            item.Template = dto.Template;
            item.Variables = dto.Variables;
        }
    }
}