﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Email.Dto;

namespace DinePlan.DineConnect.Email
{
    public interface IEmailTemplateAppService : IApplicationService
    {
        Task<PagedResultOutput<EmailTemplateListDto>> GetAll(GetEmailTemplateInput tenant);
        Task<GetEmailTemplateForEditOutput> GetTemplateForEdit(NullableIdInput nullableIdInput);
        Task<int> CreateOrUpdateTemplate(CreateOrUpdateEmailTemplateInput input);
        Task CreateEmailTemplateForTenant(int tenantId);
        Task CreateEmailTemplateForResetPassword(int tenantId);

    }
}