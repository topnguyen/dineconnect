using AutoMapper;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Authorization.Users.Dto;
using DinePlan.DineConnect.Connect.Card;
using DinePlan.DineConnect.Connect.ConnectCard.Card.Dtos;
using DinePlan.DineConnect.Connect.ConnectCard.CardType.Dtos;
using DinePlan.DineConnect.Connect.Delivery;
using DinePlan.DineConnect.Connect.DeliveryTicket.Dto;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Discount.Dtos;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Connect.Sync.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Till;
using DinePlan.DineConnect.Connect.TillAccount.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Connect.User.Dtos;
using DinePlan.DineConnect.Connect.Users;
using System;
using DinePlan.DineConnect.Cluster;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using DinePlan.DineConnect.Go;
using DinePlan.DineConnect.Go.Device.Dtos;

namespace DinePlan.DineConnect
{
    internal static class CustomDtoMapper
    {
        private static volatile bool _mappedBefore;
        private static readonly object SyncObj = new object();

        public static void CreateMappings()
        {
            lock (SyncObj)
            {
                if (_mappedBefore)
                {
                    return;
                }

                CreateMappingsInternal();

                _mappedBefore = true;
            }
        }

        private static void CreateMappingsInternal()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<User, UserEditDto>()
                    .ForMember(dto => dto.Password, options => options.Ignore())
                    .ReverseMap()
                    .ForMember(user => user.Password, options => options.Ignore());

                cfg.CreateMap<Ticket, TicketListDto>()
                    .ForMember(dto => dto.LocationName, opts => opts.MapFrom(a => a.Location.Name));

                cfg.CreateMap<Ticket, TicketListDto>()
                 .ForMember(dto => dto.LocationCode, opts => opts.MapFrom(a => a.Location.Code));

                cfg.CreateMap<Ticket, TicketViewDto>()
                   .ForMember(dto => dto.LocationName, opts => opts.MapFrom(a => a.Location.Name));

                cfg.CreateMap<Order, OrderListDto>()
                  .ForMember(dto => dto.TicketNumber, opts => opts.MapFrom(a => a.Ticket.TicketNumber));

                cfg.CreateMap<Order, OrderListDto>()
                    .ForMember(dto => dto.LocationName, opts => opts.MapFrom(a => a.Location.Name));

                cfg.CreateMap<Order, OrderListDto>()
                  .ForMember(dto => dto.TaxIncluded, opts => opts.MapFrom(a => a.Ticket.TaxIncluded));

                cfg.CreateMap<PlanLogger, PlanLoggerListDto>()
                    .ForMember(dto => dto.LocationName, opts => opts.MapFrom(a => a.Location.Name));

                cfg.CreateMap<Payment, PaymentListDto>()
                    .ForMember(dto => dto.PaymentTypeName, opts => opts.MapFrom(a => a.PaymentType.Name));

                cfg.CreateMap<TicketTransaction, TicketTransactionListDto>()
                    .ForMember(dto => dto.TransactionTypeName, opts => opts.MapFrom(a => a.TransactionType.Name));

                cfg.CreateMap<MenuItem, MenuItemListDto>()
                    .ForMember(dto => dto.CategoryName, opts => opts.MapFrom(a => a.Category.Name));

                cfg.CreateMap<MenuItem, MenuItemListDto>()
                    .ForMember(dto => dto.CategoryProductGroupName, opts => opts.MapFrom(a => a.Category.ProductGroup.Name));

                cfg.CreateMap<LocationMenuItem, LocationMenuItemListDto>()
                    .ForMember(dto => dto.MenuItemName, opts => opts.MapFrom(a => a.MenuItem.Name));

                cfg.CreateMap<DinePlanUser, DinePlanUserListDto>()
                    .ForMember(dto => dto.DinePlanUserRoleName, opts => opts.MapFrom(a => a.DinePlanUserRole.Name));

                cfg.CreateMap<DinePlanUser, DinePlanUserListDto>()
                    .ForMember(dto => dto.DinePlanUserRoleName, opts => opts.MapFrom(a => a.DinePlanUserRole.Name));
                cfg.CreateMap<DinePlanUser, ApiDinePlanUserListDto>()
                    .ForMember(dto => dto.DinePlanUserRoleName, opts => opts.MapFrom(a => a.DinePlanUserRole.Name));
               
                cfg.CreateMap<LocationCategoryShare, LocationCategoryShareDto>()
                    .ForMember(dto => dto.LocationName, opts => opts.MapFrom(a => a.Location.Name));

                cfg.CreateMap<LocationSyncer, LocationSyncerOutput>()
                    .ForMember(dto => dto.LocationName, opts => opts.MapFrom(a => a.Location.Name));

                cfg.CreateMap<ScreenMenuItem, ScreenMenuItemListDto>()
                   .ForMember(dto => dto.CategoryName, opts => opts.MapFrom(a => a.ScreenMenuCategory.Name));

                cfg.CreateMap<Promotion, PromotionListDto>()
                    .ForMember(destination => destination.PromotionType,
                        opt => opt.MapFrom(source => Enum.GetName(typeof(PromotionTypes), source.PromotionTypeId)));

                cfg.CreateMap<DeliveryTicket, DeliveryTicketList>()
                    .ForMember(dto => dto.LocationName, opts => opts.MapFrom(a => a.Location.Name));

                cfg.CreateMap<DeliveryTicket, DeliveryTicketDto>()
                   .ForMember(dto => dto.LocationName, opts => opts.MapFrom(a => a.Location.Name));

                cfg.CreateMap<TillTransaction, TillTransactionListDto>()
                    .ForMember(dto => dto.LocationName, opts => opts.MapFrom(a => a.Location.Name));

                cfg.CreateMap<TillTransaction, TillTransactionListDto>()
                   .ForMember(dto => dto.TillAccountName, opts => opts.MapFrom(a => a.TillAccount.Name));

                cfg.CreateMap<LastSync, LastSyncList>()
                 .ForMember(dto => dto.LocationName, opts => opts.MapFrom(a => a.Location.Name));

                cfg.CreateMap<Calculation, ApiCalculation>()
               .ForMember(dto => dto.TransactionTypeId, opts => opts.MapFrom(a => a.TransactionType.Name));

                cfg.CreateMap<LocationEditDto, Location>().ReverseMap();

                cfg.CreateMap<ConnectCardTypeEditDto, ConnectCardType>().ReverseMap();

                cfg.CreateMap<ConnectCard, ConnectCardListDto>()
                .ForMember(dto => dto.ConnectCardTypeName, opts => opts.MapFrom(a => a.ConnectCardType.Name));

                 cfg.CreateMap<Order, OrderList2Dto>()
                    .ForMember(dto => dto.TicketNumber, opts => opts.MapFrom(a => a.Ticket.TicketNumber))
                    .ForMember(dto => dto.LocationName, opts => opts.MapFrom(a => a.Location.Name))
                    .ForMember(dto => dto.Tags, opts => opts.MapFrom(a => a.MenuItem.Tag))
                    .ForMember(dto => dto.TaxIncluded, opts => opts.MapFrom(a => a.Ticket.TaxIncluded));


                cfg.CreateMap<DeliveryOrder,DeliveryOrderDto>()
                    .ForMember(dto => dto.Name, opts => opts.MapFrom(a => a.MenuItemPortion.MenuItem.Name))
                    .ForMember(dto => dto.PortionName, opts => opts.MapFrom(a => a.MenuItemPortion.Name))
                    .ForMember(dto => dto.MenuItemId, opts => opts.MapFrom(a => a.MenuItemPortion.MenuItem.Id));

                //DineGo

                cfg.CreateMap<DineGoDepartment, ApiGoDepartment>()
                    .ForMember(dto => dto.DepartmentName, opts => opts.MapFrom(a => a.Department.Name));


                //Cluster
                cfg.CreateMap<DelAggItem, DelAggItemListDto>()
                    .ForMember(dto => dto.DelAggCatName, opts => opts.MapFrom(a => a.DelAggCategory.Name));


                cfg.CreateMap<DelAggItem, DelAggItemListDto>()
                    .ForMember(dto => dto.DelAggItemGroupName, opts => opts.MapFrom(a => a.DelAggItemGroup.Name));


            });

            
        }
    }
}