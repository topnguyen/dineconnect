﻿using System.ComponentModel.DataAnnotations;
using Abp.Runtime.Validation;

namespace DinePlan.DineConnect.Configuration.Host.Dto
{
    public class GeneralSettingsEditDto : IValidate
    {
        [MaxLength(128)]
        public string WebSiteRootAddress { get; set; }
        public string Syncurl { get; set; }
        public int TenantId { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string TenantName { get; set; }
    }
}