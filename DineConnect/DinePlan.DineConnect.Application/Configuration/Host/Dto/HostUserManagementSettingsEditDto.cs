﻿using Abp.Runtime.Validation;

namespace DinePlan.DineConnect.Configuration.Host.Dto
{
    public class HostUserManagementSettingsEditDto : IValidate
    {
        public bool IsEmailConfirmationRequiredForLogin { get; set; }
    }
}