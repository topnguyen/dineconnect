﻿using Abp.Runtime.Validation;

namespace DinePlan.DineConnect.Configuration.Host.Dto
{
    public class ShortMessagesSettingsEditDto : IValidate
    {
        public int MessageType { get; set; }
        public string Settings { get; set; }
        
    }

    public enum ShortMessageTypes
    {
        Url
    };
}