using Abp.Runtime.Validation;
using System;

namespace DinePlan.DineConnect.Configuration.Tenants.Dto
{
    public class ConnectSettingsEditDto : IValidate
    {
        public decimal OperateHours { get; set; }
        public string TimeZoneUtc { get; set; }
        public int Decimals { get; set; }
        public string DateTimeFormat { get; set; }
        public string SimpleDateFormat { get; set; }

        public string Schedules { get; set; }
        public string WeekDay { get; set; }
        public string WeekEnd { get; set; }
        public string LastSyncStatuses { get; set; }
        public string BucketName { get; set; }
        public string ServerUrl { get; set; }
        public string AccessKey { get; set; }
        public string SecretKey { get; set; }
        public bool Cloudinary { get; set; }
        public bool SendEmailOnWorkDayClose { get; set; }

        public string UrbanPiperUserName { get; set; }
        public string UrbanPiperApiKey { get; set; }
        public string UrbanPiperPriceTag { get; set; }
        public string UrbanPiperUrl { get; set; }
        public string UrbanPiperMenu { get; set; }

        public bool LocationGroupAsBrand { get; set; }
        public string DeliveryHeroUrl { get; set; }
        public string DeliveryHeroUser { get; set; }
        public string DeliveryHeroPassword { get; set; }
        public string DeliveryHeroPriceTag { get; set; }
        public string DeliveryHeroPushMenuUrl { get; set; }
        public string CreditSaleReport { get; set; }
    }

    public class LastSyncStatusDto
    {
        public string Status { get; set; }

        public int StartHour { get; set; }
        public int StartMinute { get; set; }

        public int EndHour { get; set; }
        public int EndMinute { get; set; }
        public int Count { get; set; }
    }

    public class CaterSettingsDto : IValidate
    {
        public string CaterMenu { get; set; }
    }

    public class EngageSettingsEditDto : IValidate
    {
        public decimal PointsAccumulation { get; set; }
        public decimal PointsAccumulationFactor { get; set; }
        public decimal PointsRedemption { get; set; }
        public decimal PointsRedemptionFactor { get; set; }
        public bool OnBoardMemberFromPOS { get; set; }
        public bool CalculatePoints { get; set; }
        public bool CalculatePointsOnOrder { get; set; }
        public bool CalculatePointsOnMultiple { get; set; }
        public bool AutoGenerateMemberCode { get; set; }
        public string CodeFormat { get; set; }
        public bool CreateNewUserOnActivation { get; set; }
        public int MemberUserRoleID { get; set; }
        public string MemberUserRoleName { get; set; }
        public string DefaultCity { get; set; }
        public string DefaultState { get; set; }
        public string DefaultCountry { get; set; }
    }

    public class HouseSettingEditDto : IValidate
    {
        public bool SupplierMaterialTightlyCoupled { get; set; }
        public bool DirectTransferAllowed { get; set; }
        public bool InvoiceSupplierRateChangeFlag { get; set; }
        public bool TallyIntegrationFlag { get; set; }
        public bool ExtraMaterialReceivedInDc { get; set; }

        public bool InterTransferValueShown { get; set; }
        public bool SmartPrintOutExists { get; set; }

        public bool MessageNotification { get; set; }
        public bool SoftMessageNotification { get; set; }

        public bool ForceAdjustmentWithDayClose { get; set; }

        public bool MaterialCostBasedOnLastPurchase { get; set; }
        public int Decimals { get; set; }
        public bool IsMaterialCodeTreatedAsBarCode { get; set; }

        public int DefaultAveragePriceTagRefId { get; set; }

        public int NoOfMonthAvgTaken { get; set; }
        public bool IsOmitFOCMaterialQuantities { get; set; }
        public bool IsCalculateAverageRateWithoutTax { get; set; }
        public decimal AbsentDeductionPerDay { get; set; }
        public bool IsInwardPurchaseOrderReferenceFromOtherErpRequired { get; set; }
        public bool IsInwardInvoiceNumberReferenceFromOtherErpRequired { get; set; }

        public int MaterialCountToUpdate { get; set; }
        public bool WhileClosingStockTakenCanAllUnitAllowed { get; set; }
        public bool BackGroundDayCloseCanBeAllowed { get; set; }
        public decimal TransferRequestLockHours { get; set; }
        public decimal TransferRequestGraceHours { get; set; }
        public string PurchaseOrderCCMailList { get; set; }
    }

    public class SwipeSettingEditDto : IValidate
    {
        public bool CanIssueCardDirectly { get; set; }
        public bool DepositRequiredDefault { get; set; }
        public decimal DepositAmountDefault { get; set; }
        public int ExpiryDaysFromLastUsageDefault { get; set; }
        public bool RefundAllowedDefault { get; set; }
    }

    public class TouchSettingDto : IValidate
    {
        public string Currency { get; set; }
        public bool TaxInclusive { get; set; }
        public decimal Rounding { get; set; }
        public int TotalDecimal { get; set; }
        public string ReceiptHeader { get; set; }
        public string ReceiptFooter { get; set; }

        public bool IsActivity { get; set; }
        public bool IsReport { get; set; }
        public bool IsBlindShift { get; set; }

        public bool DuplicateReceipt { get; set; }
    }
    public class PasswordPolicySettingEditDto : IValidate
    {
        public int Min_Length { get; set; } //Minimum Length of Password
        public int Max_Length { get; set; } //Maximum Length of Password
        public int Min_LCL { get; set; } //Minimum Number of Lower-Case Letters 
        public int Min_UCL { get; set; } //Minimum Number of Upper-Case Letters 
        public int Min_AC { get; set; } //Minimum Number of Alphanumeric Characters 
        public int Min_SC { get; set; } //Minimum Number of Special Characters 
        public int Size { get; set; }
        public string Impermissible_Passwords { get; set; }
        public int Maximum_IT { get; set; } //Password Maximum Idle Time
        public int Password_VP { get; set; } //Password Validity Period (Days)
        public bool Allow_LIDPP { get; set; } //Allow Logon ID as Part of Password
        public bool Allow_OPPP { get; set; } //Allow Old Password as Part of New Password
        public bool Allow_UCOP { get; set; } //Allow Users to Change Their Own Passwords
        public bool Enforce { get; set; }   // Specify that the application will be enforced. Password policy On the system DineConnect Or not
        public int Maximum_NFLA { get; set; } //Maximum Number of Failed Logon Attempts
        public string Suggest_Message { get; set; }
    }


    public class PasswordPolicyInputDto
    {
        public string LoginUserName { get; set; }
        public int UserId { get; set; }
        public string Password { get; set; }
    }

    public class PasswordPolicyOutputDto
    {
        public string Description { get; set; }
        public string SamplePassword { get; set; }
        public bool ErrorFlag { get; set; }  
    }


    public class GetPassWordPolicyInputDto
    {
        public PasswordPolicySettingEditDto PasswordPolicy { get; set; }
    }
}