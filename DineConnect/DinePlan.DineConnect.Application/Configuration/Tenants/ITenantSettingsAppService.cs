﻿using System.Threading.Tasks;
using Abp.Application.Services;
using DinePlan.DineConnect.Configuration.Tenants.Dto;

namespace DinePlan.DineConnect.Configuration.Tenants
{
    public interface ITenantSettingsAppService : IApplicationService
    {
        Task<TenantSettingsEditDto> GetAllSettings();
        Task<TouchSettingDto> GetCompactSetting();
        Task UpdateAllSettings(TenantSettingsEditDto input);
        Task<bool> SmtpSettingLoginValidation(TenantSettingsEditDto input);
        Task<PasswordPolicySettingEditDto> GetPasswordPolicySetting();

    }
}
