﻿using System;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Features;
using Abp.Configuration;
using Abp.Configuration.Startup;
using Abp.Extensions;
using Abp.Net.Mail;
using Abp.Runtime.Caching;
using Abp.Runtime.Session;
using Abp.UI;
using Abp.Zero.Configuration;
using Abp.Zero.Ldap.Configuration;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Configuration.Host.Dto;
using DinePlan.DineConnect.Configuration.Tenants.Dto;
using DinePlan.DineConnect.Features;

namespace DinePlan.DineConnect.Configuration.Tenants
{
    public class TenantSettingsAppService : DineConnectAppServiceBase, ITenantSettingsAppService
    {
        private readonly IFeatureChecker _featureManager;
        private readonly IAbpZeroLdapModuleConfig _ldapModuleConfig;
        private readonly IMultiTenancyConfig _multiTenancyConfig;
        private readonly IUserAppService _userAppService;
        private readonly IUserEmailer _userEmailer;
        private readonly UserManager _userManager;
        private ICacheManager _cacheManager;

        public TenantSettingsAppService(IMultiTenancyConfig multiTenancyConfig,
            IAbpZeroLdapModuleConfig ldapModuleConfig, IFeatureChecker featureManager,
            IUserAppService userAppService,
            ICacheManager cacheManager,
            IUserEmailer userEmailer,
            UserManager userManager)
        {
            _multiTenancyConfig = multiTenancyConfig;
            _ldapModuleConfig = ldapModuleConfig;
            _featureManager = featureManager;
            _userAppService = userAppService;
            _userEmailer = userEmailer;
            _userManager = userManager;
            _cacheManager = cacheManager;
        }

        public async Task<TouchSettingDto> GetCompactSetting()
        {
            var retS = new TouchSettingDto
            {
                Currency = await FeatureChecker.GetValueAsync(AppFeatures.ConnectCurrency),
                TaxInclusive = await SettingManager.GetSettingValueAsync<bool>(AppSettings.TouchSaleSettings.TaxInclusive),
                Rounding = await SettingManager.GetSettingValueAsync<decimal>(AppSettings.TouchSaleSettings.Rounding),
                TotalDecimal = await SettingManager.GetSettingValueAsync<int>(AppSettings.TouchSaleSettings.Decimals),
                ReceiptHeader = await SettingManager.GetSettingValueAsync(AppSettings.TouchSaleSettings.ReceiptHeader),
                IsReport = await SettingManager.GetSettingValueAsync<bool>(AppSettings.TouchSaleSettings.IsReport),
                IsBlindShift = await SettingManager.GetSettingValueAsync<bool>(AppSettings.TouchSaleSettings.IsBlindShift),
                IsActivity = await SettingManager.GetSettingValueAsync<bool>(AppSettings.TouchSaleSettings.IsActivity),
                DuplicateReceipt = await SettingManager.GetSettingValueAsync<bool>(AppSettings.TouchSaleSettings.DuplicateReceipt)
            };
            return retS;
        }

        public async Task<PasswordPolicySettingEditDto> GetPasswordPolicySetting()
        {
            var setting = new PasswordPolicySettingEditDto
            {
                Min_Length = await SettingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Min_Length),
                Max_Length = await SettingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Max_Length),
                Min_LCL = await SettingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Min_LCL),
                Min_UCL = await SettingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Min_UCL),
                Min_AC = await SettingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Min_AC),
                Min_SC = await SettingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Min_SC),
                Size = await SettingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Size),
                Impermissible_Passwords = await SettingManager.GetSettingValueAsync(AppSettings.PasswordPolicySettings.Impermissible_Passwords),
                Maximum_IT = await SettingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Maximum_IT),
                Password_VP = await SettingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Password_VP),
                Allow_LIDPP = await SettingManager.GetSettingValueAsync<bool>(AppSettings.PasswordPolicySettings.Allow_LIDPP),
                Allow_OPPP = await SettingManager.GetSettingValueAsync<bool>(AppSettings.PasswordPolicySettings.Allow_OPPP),
                Allow_UCOP = await SettingManager.GetSettingValueAsync<bool>(AppSettings.PasswordPolicySettings.Allow_UCOP),
                Enforce = await SettingManager.GetSettingValueAsync<bool>(AppSettings.PasswordPolicySettings.Enforce),
                Maximum_NFLA = await SettingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Maximum_NFLA),
                Suggest_Message = await SettingManager.GetSettingValueAsync(AppSettings.PasswordPolicySettings.Suggest_Message)
            };
            return setting;
        }

        public async Task<TenantSettingsEditDto> GetAllSettings()
        {
            var settings = new TenantSettingsEditDto
            {
                UserManagement = new TenantUserManagementSettingsEditDto
                {
                    AllowSelfRegistration =
                        await
                            SettingManager.GetSettingValueAsync<bool>(AppSettings.UserManagement.AllowSelfRegistration),
                    IsNewRegisteredUserActiveByDefault =
                        await
                            SettingManager.GetSettingValueAsync<bool>(
                                AppSettings.UserManagement.IsNewRegisteredUserActiveByDefault),
                    IsEmailConfirmationRequiredForLogin =
                        await
                            SettingManager.GetSettingValueAsync<bool>(
                                AbpZeroSettingNames.UserManagement.IsEmailConfirmationRequiredForLogin),
                    UseCaptchaOnRegistration =
                        await
                            SettingManager.GetSettingValueAsync<bool>(
                                AppSettings.UserManagement.UseCaptchaOnRegistration)
                }
            };

            if (!_multiTenancyConfig.IsEnabled)
            {
                //General
                settings.General = new GeneralSettingsEditDto
                {
                    WebSiteRootAddress =
                        await SettingManager.GetSettingValueAsync(AppSettings.General.WebSiteRootAddress)
                };

                //Ldap
                if (_ldapModuleConfig.IsEnabled)
                    settings.Ldap = new LdapSettingsEditDto
                    {
                        IsModuleEnabled = true,
                        IsEnabled = await SettingManager.GetSettingValueAsync<bool>(LdapSettingNames.IsEnabled),
                        Domain = await SettingManager.GetSettingValueAsync(LdapSettingNames.Domain),
                        UserName = await SettingManager.GetSettingValueAsync(LdapSettingNames.UserName),
                        Password = await SettingManager.GetSettingValueAsync(LdapSettingNames.Password)
                    };
                else
                    settings.Ldap = new LdapSettingsEditDto
                    {
                        IsModuleEnabled = false
                    };
            }
            //Email
            settings.Email = new EmailSettingsEditDto
            {
                DefaultFromAddress =
                    await SettingManager.GetSettingValueAsync(EmailSettingNames.DefaultFromAddress),
                DefaultFromDisplayName =
                    await SettingManager.GetSettingValueAsync(EmailSettingNames.DefaultFromDisplayName),
                SmtpHost = await SettingManager.GetSettingValueAsync(EmailSettingNames.Smtp.Host),
                SmtpPort = await SettingManager.GetSettingValueAsync<int>(EmailSettingNames.Smtp.Port),
                SmtpUserName = await SettingManager.GetSettingValueAsync(EmailSettingNames.Smtp.UserName),
                SmtpPassword = await SettingManager.GetSettingValueAsync(EmailSettingNames.Smtp.Password),
                SmtpDomain = await SettingManager.GetSettingValueAsync(EmailSettingNames.Smtp.Domain),
                SmtpEnableSsl = await SettingManager.GetSettingValueAsync<bool>(EmailSettingNames.Smtp.EnableSsl),
                SmtpUseDefaultCredentials =
                    await SettingManager.GetSettingValueAsync<bool>(EmailSettingNames.Smtp.UseDefaultCredentials)
            };
            if (_ldapModuleConfig.IsEnabled)
                settings.Ldap = new LdapSettingsEditDto
                {
                    IsModuleEnabled = true,
                    IsEnabled = await SettingManager.GetSettingValueAsync<bool>(LdapSettingNames.IsEnabled),
                    Domain = await SettingManager.GetSettingValueAsync(LdapSettingNames.Domain),
                    UserName = await SettingManager.GetSettingValueAsync(LdapSettingNames.UserName),
                    Password = await SettingManager.GetSettingValueAsync(LdapSettingNames.Password)
                };
            else
                settings.Ldap = new LdapSettingsEditDto
                {
                    IsModuleEnabled = false
                };
            if (settings.General == null) settings.General = new GeneralSettingsEditDto();
            settings.General.Syncurl = await SettingManager.GetSettingValueAsync(AppSettings.General.Syncurl);
            settings.General.TenantId = await SettingManager.GetSettingValueAsync<int>(AppSettings.General.TenantId);
            settings.General.User = await SettingManager.GetSettingValueAsync(AppSettings.General.User);
            settings.General.Password = await SettingManager.GetSettingValueAsync(AppSettings.General.Password);
            settings.General.TenantName = await SettingManager.GetSettingValueAsync(AppSettings.General.TenantName);

            if (await FeatureChecker.IsEnabledAsync(AppFeatures.Connect))
                settings.Connect = new ConnectSettingsEditDto
                {
                    OperateHours =
                        await SettingManager.GetSettingValueAsync<decimal>(AppSettings.ConnectSettings.OperateHours),
                    TimeZoneUtc = await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.TimeZoneUtc),
                    Decimals = await SettingManager.GetSettingValueAsync<int>(AppSettings.ConnectSettings.Decimals),
                    DateTimeFormat =
                        await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.DateTimeFormat),
                    SimpleDateFormat =
                        await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.SimpleDateFormat),
                    Schedules = await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.Schedules),
                    WeekDay = await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.WeekDay),
                    WeekEnd = await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.WeekEnd),
                    LastSyncStatuses =
                        await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.LastSyncStatuses),
                    BucketName = await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.BucketName),
                    ServerUrl = await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.ServerUrl),
                    AccessKey = await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.AccessKey),
                    SecretKey = await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.SecretKey),
                    Cloudinary = await SettingManager.GetSettingValueAsync<bool>(AppSettings.ConnectSettings.Cloudinary),
                    SendEmailOnWorkDayClose = await SettingManager.GetSettingValueAsync<bool>(AppSettings.ConnectSettings.SendEmailOnWorkDayClose),

                    UrbanPiperUserName =
                        await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.UrbanPiperUserName),
                    UrbanPiperApiKey =
                        await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.UrbanPiperApiKey),
                    UrbanPiperPriceTag =
                        await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.UrbanPiperPriceTag),
                    UrbanPiperUrl =
                        await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.UrbanPiperUrl),
                    UrbanPiperMenu =
                        await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.UrbanPiperMenu),

                    DeliveryHeroUrl =
                        await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.DeliveryHeroUrl),
                    DeliveryHeroUser =
                        await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.DeliveryHeroUser),
                    DeliveryHeroPassword =
                        await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.DeliveryHeroPassword),
                    DeliveryHeroPriceTag =
                        await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.DeliveryHeroPriceTag),
                    DeliveryHeroPushMenuUrl =
                        await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.DeliveryHeroPushMenuUrl),

                    LocationGroupAsBrand =
                    await SettingManager.GetSettingValueAsync<bool>(AppSettings.ConnectSettings.LocationGroupAsBrand),

                    CreditSaleReport =
                    await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.CreditSaleReport),
                };

            if (await FeatureChecker.IsEnabledAsync(AppFeatures.Cater))
                settings.Cater = new CaterSettingsDto
                {
                    CaterMenu = await SettingManager.GetSettingValueAsync(AppSettings.CaterSettings.CaterMenu),
                };

            if (await FeatureChecker.IsEnabledAsync(AppFeatures.Engage))
                settings.Engage = new EngageSettingsEditDto
                {
                    PointsAccumulation =
                        await
                            SettingManager.GetSettingValueAsync<decimal>(AppSettings.EngageSettings.PointsAccumulation),
                    PointsAccumulationFactor =
                        await
                            SettingManager.GetSettingValueAsync<decimal>(
                                AppSettings.EngageSettings.PointsAccumulationFactor),
                    PointsRedemption =
                        await SettingManager.GetSettingValueAsync<decimal>(AppSettings.EngageSettings.PointsRedemption),
                    PointsRedemptionFactor =
                        await
                            SettingManager.GetSettingValueAsync<decimal>(
                                AppSettings.EngageSettings.PointsRedemptionFactor),
                    OnBoardMemberFromPOS =
                        await SettingManager.GetSettingValueAsync<bool>(AppSettings.EngageSettings
                            .OnBoardMemberFromPos),
                    CalculatePoints =
                        await SettingManager.GetSettingValueAsync<bool>(AppSettings.EngageSettings.CalculatePoints),
                    CalculatePointsOnOrder =
                        await
                            SettingManager.GetSettingValueAsync<bool>(AppSettings.EngageSettings
                                .CalculatePointsOnOrder),
                    CalculatePointsOnMultiple =
                        await
                            SettingManager.GetSettingValueAsync<bool>(
                                AppSettings.EngageSettings.CalculatePointsOnMultiple),
                    AutoGenerateMemberCode =
                    await SettingManager.GetSettingValueAsync<bool>(AppSettings.EngageSettings.AutoGenerateMemberCode),
                    CodeFormat =
                    await SettingManager.GetSettingValueAsync(AppSettings.EngageSettings.CodeFormat),
                    CreateNewUserOnActivation =
                    await SettingManager.GetSettingValueAsync<bool>(AppSettings.EngageSettings.CreateNewUserOnActivation),
                    MemberUserRoleID =
                    await SettingManager.GetSettingValueAsync<int>(AppSettings.EngageSettings.MemberUserRoleID),
                    MemberUserRoleName = await SettingManager.GetSettingValueAsync(AppSettings.EngageSettings.MemberUserRoleName),

                    DefaultCity = await SettingManager.GetSettingValueAsync(AppSettings.EngageSettings.DefaultCity),
                    DefaultState = await SettingManager.GetSettingValueAsync(AppSettings.EngageSettings.DefaultState),
                    DefaultCountry =
                        await SettingManager.GetSettingValueAsync(AppSettings.EngageSettings.DefaultCountry)
                };

            if (await FeatureChecker.IsEnabledAsync(AppFeatures.House))
                settings.House = new HouseSettingEditDto
                {
                    SupplierMaterialTightlyCoupled =
                        await
                            SettingManager.GetSettingValueAsync<bool>(
                                AppSettings.HouseSettings.SupplierMaterialTightlyCoupled),
                    DirectTransferAllowed =
                        await SettingManager.GetSettingValueAsync<bool>(AppSettings.HouseSettings
                            .DirectTransferAllowed),
                    InvoiceSupplierRateChangeFlag =
                        await
                            SettingManager.GetSettingValueAsync<bool>(
                                AppSettings.HouseSettings.InvoiceSupplierRateChangeFlag),
                    TallyIntegrationFlag =
                        await SettingManager.GetSettingValueAsync<bool>(AppSettings.HouseSettings.TallyIntegrationFlag),
                    ExtraMaterialReceivedInDc =
                        await
                            SettingManager.GetSettingValueAsync<bool>(
                                AppSettings.HouseSettings.ExtraMaterialReceivedInDc),
                    InterTransferValueShown =
                        await
                            SettingManager.GetSettingValueAsync<bool>(AppSettings.HouseSettings
                                .InterTransferValueShown),
                    SmartPrintOutExists =
                        await SettingManager.GetSettingValueAsync<bool>(AppSettings.HouseSettings.SmartPrintOutExists),
                    MessageNotification =
                        await SettingManager.GetSettingValueAsync<bool>(AppSettings.HouseSettings.MessageNotification),
                    SoftMessageNotification =
                        await
                            SettingManager.GetSettingValueAsync<bool>(AppSettings.HouseSettings
                                .SoftMessageNotification),
                    ForceAdjustmentWithDayClose =
                        await
                            SettingManager.GetSettingValueAsync<bool>(
                                AppSettings.HouseSettings.ForceAdjustmentWithDayClose),
                    MaterialCostBasedOnLastPurchase =
                        await
                            SettingManager.GetSettingValueAsync<bool>(
                                AppSettings.HouseSettings.MaterialCostBasedOnLastPurchase),
                    Decimals = await SettingManager.GetSettingValueAsync<int>(AppSettings.HouseSettings.Decimals),
                    IsMaterialCodeTreatedAsBarCode =
                        await
                            SettingManager.GetSettingValueAsync<bool>(
                                AppSettings.HouseSettings.IsMaterialCodeTreatedAsBarCode),
                    DefaultAveragePriceTagRefId =
                        await SettingManager.GetSettingValueAsync<int>(AppSettings.HouseSettings
                            .DefaultAveragePriceTagRefId),
                    NoOfMonthAvgTaken =
                        await SettingManager.GetSettingValueAsync<int>(AppSettings.HouseSettings.NoOfMonthAvgTaken),
                    IsOmitFOCMaterialQuantities =
                        await SettingManager.GetSettingValueAsync<bool>(AppSettings.HouseSettings
                            .IsOmitFOCMaterialQuantities),
                    IsCalculateAverageRateWithoutTax =
                        await SettingManager.GetSettingValueAsync<bool>(AppSettings.HouseSettings
                            .IsCalculateAverageRateWithoutTax),
                    AbsentDeductionPerDay =
                        await SettingManager.GetSettingValueAsync<decimal>(AppSettings.HouseSettings.AbsentDeductionPerDay),

                    IsInwardPurchaseOrderReferenceFromOtherErpRequired =
                        await SettingManager.GetSettingValueAsync<bool>(
                            AppSettings.HouseSettings.IsInwardPurchaseOrderReferenceFromOtherErpRequired),
                    IsInwardInvoiceNumberReferenceFromOtherErpRequired =
                        await SettingManager.GetSettingValueAsync<bool>(
                            AppSettings.HouseSettings.IsInwardInvoiceNumberReferenceFromOtherErpRequired),
                    MaterialCountToUpdate =
                        await SettingManager.GetSettingValueAsync<int>(AppSettings.HouseSettings.MaterialCountToUpdate),

                    WhileClosingStockTakenCanAllUnitAllowed = await SettingManager.GetSettingValueAsync<bool>(
                        AppSettings.HouseSettings.WhileClosingStockTakenCanAllUnitAllowed),
                    BackGroundDayCloseCanBeAllowed = await SettingManager.GetSettingValueAsync<bool>(
                        AppSettings.HouseSettings.BackGroundDayCloseCanBeAllowed),
                    TransferRequestLockHours =
                        await SettingManager.GetSettingValueAsync<decimal>(AppSettings.HouseSettings.TransferRequestLockHours),
                    TransferRequestGraceHours = await SettingManager.GetSettingValueAsync<decimal>(AppSettings.HouseSettings.TransferRequestGraceHours),
                    PurchaseOrderCCMailList = await SettingManager.GetSettingValueAsync(AppSettings.HouseSettings.PurchaseOrderCCMailList),
                };

            if (await FeatureChecker.IsEnabledAsync(AppFeatures.Touch))
                settings.Compact = new TouchSettingDto
                {
                    TaxInclusive =
                        await SettingManager.GetSettingValueAsync<bool>(AppSettings.TouchSaleSettings.TaxInclusive),
                    Rounding =
                        await SettingManager.GetSettingValueAsync<decimal>(AppSettings.TouchSaleSettings.Rounding),
                    TotalDecimal =
                        await SettingManager.GetSettingValueAsync<int>(AppSettings.TouchSaleSettings.Decimals),
                    ReceiptHeader =
                        await SettingManager.GetSettingValueAsync(AppSettings.TouchSaleSettings.ReceiptHeader),
                    ReceiptFooter =
                        await SettingManager.GetSettingValueAsync(AppSettings.TouchSaleSettings.ReceiptFooter),
                    IsActivity =
                        await SettingManager.GetSettingValueAsync<bool>(AppSettings.TouchSaleSettings.IsActivity),
                    IsReport = await SettingManager.GetSettingValueAsync<bool>(AppSettings.TouchSaleSettings.IsReport),
                    IsBlindShift =
                        await SettingManager.GetSettingValueAsync<bool>(AppSettings.TouchSaleSettings.IsBlindShift),
                    DuplicateReceipt =
                        await SettingManager.GetSettingValueAsync<bool>(AppSettings.TouchSaleSettings.DuplicateReceipt)
                };

            if (await FeatureChecker.IsEnabledAsync(AppFeatures.Swipe))
                settings.Swipe = new SwipeSettingEditDto
                {
                    CanIssueCardDirectly =
                        await SettingManager.GetSettingValueAsync<bool>(AppSettings.SwipeSettings.CanIssueCardDirectly),
                    DepositRequiredDefault =
                        await
                            SettingManager.GetSettingValueAsync<bool>(AppSettings.SwipeSettings.DepositRequiredDefault),
                    DepositAmountDefault =
                        await
                            SettingManager.GetSettingValueAsync<decimal>(AppSettings.SwipeSettings
                                .DepositAmountDefault),
                    ExpiryDaysFromLastUsageDefault =
                        await
                            SettingManager.GetSettingValueAsync<int>(
                                AppSettings.SwipeSettings.ExpiryDaysFromIssueDefault),
                    RefundAllowedDefault =
                        await SettingManager.GetSettingValueAsync<bool>(AppSettings.SwipeSettings.RefundAllowedDefault)
                };

            if (settings.PasswordPolicy == null)
                settings.PasswordPolicy = new PasswordPolicySettingEditDto
                {
                    Min_Length = await SettingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Min_Length),
                    Max_Length = await SettingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Max_Length),
                    Min_LCL = await SettingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Min_LCL),
                    Min_UCL = await SettingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Min_UCL),
                    Min_AC = await SettingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Min_AC),
                    Min_SC = await SettingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Min_SC),
                    Size = await SettingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Size),
                    Impermissible_Passwords = await SettingManager.GetSettingValueAsync(AppSettings.PasswordPolicySettings.Impermissible_Passwords),
                    Maximum_IT = await SettingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Maximum_IT),
                    Password_VP = await SettingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Password_VP),
                    Allow_LIDPP = await SettingManager.GetSettingValueAsync<bool>(AppSettings.PasswordPolicySettings.Allow_LIDPP),
                    Allow_OPPP = await SettingManager.GetSettingValueAsync<bool>(AppSettings.PasswordPolicySettings.Allow_OPPP),
                    Allow_UCOP = await SettingManager.GetSettingValueAsync<bool>(AppSettings.PasswordPolicySettings.Allow_UCOP),
                    Enforce = await SettingManager.GetSettingValueAsync<bool>(AppSettings.PasswordPolicySettings.Enforce),
                    Maximum_NFLA = await SettingManager.GetSettingValueAsync<int>(AppSettings.PasswordPolicySettings.Maximum_NFLA),
                    Suggest_Message = await SettingManager.GetSettingValueAsync(AppSettings.PasswordPolicySettings.Suggest_Message)
                };

            return settings;
        }

        public async Task UpdateAllSettings(TenantSettingsEditDto input)
        {
            #region PasswordPolicy Validation

            if (input.PasswordPolicy != null)
            {
                if (input.PasswordPolicy.Min_Length > 0 && input.PasswordPolicy.Max_Length > 0)
                {
                    int passwordLength = 0;
                    if (input.PasswordPolicy.Min_LCL > 0)
                    {
                        passwordLength = passwordLength + input.PasswordPolicy.Min_LCL;
                    }
                    if (input.PasswordPolicy.Min_UCL > 0)
                    {
                        passwordLength = passwordLength + input.PasswordPolicy.Min_UCL;
                    }
                    if (input.PasswordPolicy.Min_SC > 0)
                    {
                        passwordLength = passwordLength + input.PasswordPolicy.Min_SC;
                    }

                    if (passwordLength > input.PasswordPolicy.Max_Length)
                    {
                        throw new UserFriendlyException(L("LCL_UCL_SC_LengthError", passwordLength, input.PasswordPolicy.Max_Length));
                    }
                }
            }

            #endregion PasswordPolicy Validation

            //User management
            await
                SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.UserManagement.AllowSelfRegistration,
                    input.UserManagement.AllowSelfRegistration.ToString(CultureInfo.InvariantCulture)
                        .ToLower(CultureInfo.InvariantCulture));
            await
                SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.UserManagement.IsNewRegisteredUserActiveByDefault,
                    input.UserManagement.IsNewRegisteredUserActiveByDefault.ToString(CultureInfo.InvariantCulture)
                        .ToLower(CultureInfo.InvariantCulture));
            await
                SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AbpZeroSettingNames.UserManagement.IsEmailConfirmationRequiredForLogin,
                    input.UserManagement.IsEmailConfirmationRequiredForLogin.ToString(CultureInfo.InvariantCulture)
                        .ToLower(CultureInfo.InvariantCulture));
            await
                SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.UserManagement.UseCaptchaOnRegistration,
                    input.UserManagement.UseCaptchaOnRegistration.ToString(CultureInfo.InvariantCulture)
                        .ToLower(CultureInfo.InvariantCulture));

            if (!_multiTenancyConfig.IsEnabled)
            {
                input.ValidateHostSettings();

                //General
                await
                    SettingManager.ChangeSettingForApplicationAsync(AppSettings.General.WebSiteRootAddress,
                        input.General.WebSiteRootAddress.EnsureEndsWith('/'));

                //Ldap
                if (_ldapModuleConfig.IsEnabled)
                {
                    await
                        SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(), LdapSettingNames.IsEnabled,
                            input.Ldap.IsEnabled.ToString(CultureInfo.InvariantCulture)
                                .ToLower(CultureInfo.InvariantCulture));
                    await
                        SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(), LdapSettingNames.Domain,
                            input.Ldap.Domain.IsNullOrWhiteSpace() ? null : input.Ldap.Domain);
                    await
                        SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(), LdapSettingNames.UserName,
                            input.Ldap.UserName.IsNullOrWhiteSpace() ? null : input.Ldap.UserName);
                    await
                        SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(), LdapSettingNames.Password,
                            input.Ldap.Password.IsNullOrWhiteSpace() ? null : input.Ldap.Password);
                }
            }
            //Email
            await
                SettingManager.ChangeSettingForApplicationAsync(EmailSettingNames.DefaultFromAddress,
                    input.Email.DefaultFromAddress);
            await
                SettingManager.ChangeSettingForApplicationAsync(EmailSettingNames.DefaultFromDisplayName,
                    input.Email.DefaultFromDisplayName);
            await SettingManager.ChangeSettingForApplicationAsync(EmailSettingNames.Smtp.Host,
                input.Email.SmtpHost);
            await
                SettingManager.ChangeSettingForApplicationAsync(EmailSettingNames.Smtp.Port,
                    input.Email.SmtpPort.ToString(CultureInfo.InvariantCulture));
            await
                SettingManager.ChangeSettingForApplicationAsync(EmailSettingNames.Smtp.UserName,
                    input.Email.SmtpUserName);
            await
                SettingManager.ChangeSettingForApplicationAsync(EmailSettingNames.Smtp.Password,
                    input.Email.SmtpPassword);
            await
                SettingManager.ChangeSettingForApplicationAsync(EmailSettingNames.Smtp.Domain,
                    input.Email.SmtpDomain);
            await
                SettingManager.ChangeSettingForApplicationAsync(EmailSettingNames.Smtp.EnableSsl,
                    input.Email.SmtpEnableSsl.ToString(CultureInfo.InvariantCulture)
                        .ToLower(CultureInfo.InvariantCulture));
            await
                SettingManager.ChangeSettingForApplicationAsync(EmailSettingNames.Smtp.UseDefaultCredentials,
                    input.Email.SmtpUseDefaultCredentials.ToString(CultureInfo.InvariantCulture)
                        .ToLower(CultureInfo.InvariantCulture));
            await
                SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(), AppSettings.General.Syncurl,
                    input.General.Syncurl.ToString(CultureInfo.InvariantCulture)
                        .ToLower(CultureInfo.InvariantCulture));
            await
                SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(), AppSettings.General.TenantId,
                    input.General.TenantId.ToString(CultureInfo.InvariantCulture)
                        .ToLower(CultureInfo.InvariantCulture));
            await
                SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(), AppSettings.General.User,
                    input.General.User);
            await
                SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(), AppSettings.General.Password,
                    input.General.Password);
            await
                SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(), AppSettings.General.TenantName,
                    input.General.TenantName);

            if (await FeatureChecker.IsEnabledAsync(AppFeatures.Connect))
            {
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.ConnectSettings.OperateHours,
                        input.Connect.OperateHours.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));

                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.ConnectSettings.TimeZoneUtc,
                        input.Connect.TimeZoneUtc.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));

                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.ConnectSettings.Decimals,
                        input.Connect.Decimals.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));

                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.ConnectSettings.DateTimeFormat,
                    input.Connect.DateTimeFormat.ToString(CultureInfo.InvariantCulture));

                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.ConnectSettings.SimpleDateFormat,
                    input.Connect.SimpleDateFormat.ToString(CultureInfo.InvariantCulture));
                //.ToLower(CultureInfo.InvariantCulture));

                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.ConnectSettings.Schedules,
                    input.Connect.Schedules);

                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.ConnectSettings.WeekDay,
                    input.Connect.WeekDay);

                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.ConnectSettings.WeekEnd,
                    input.Connect.WeekEnd);

                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.ConnectSettings.LastSyncStatuses,
                    input.Connect.LastSyncStatuses);

                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.ConnectSettings.BucketName,
                    input.Connect.BucketName);

                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.ConnectSettings.ServerUrl,
                    input.Connect.ServerUrl);

                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.ConnectSettings.AccessKey,
                    input.Connect.AccessKey);

                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.ConnectSettings.SecretKey,
                    input.Connect.SecretKey);

                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.ConnectSettings.Cloudinary,
                    input.Connect.Cloudinary.ToString(CultureInfo.InvariantCulture)
                        .ToLower(CultureInfo.InvariantCulture));

                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.ConnectSettings.SendEmailOnWorkDayClose,
                    input.Connect.SendEmailOnWorkDayClose.ToString(CultureInfo.InvariantCulture)
                        .ToLower(CultureInfo.InvariantCulture));

                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.ConnectSettings.UrbanPiperUserName,
                    input.Connect.UrbanPiperUserName);

                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.ConnectSettings.UrbanPiperApiKey,
                    input.Connect.UrbanPiperApiKey);

                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.ConnectSettings.UrbanPiperPriceTag,
                    input.Connect.UrbanPiperPriceTag);

                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.ConnectSettings.UrbanPiperUrl,
                    input.Connect.UrbanPiperUrl);

                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.ConnectSettings.UrbanPiperMenu,
                    input.Connect.UrbanPiperMenu);

                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.ConnectSettings.DeliveryHeroUrl,
                    input.Connect.DeliveryHeroUrl);

                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.ConnectSettings.DeliveryHeroUser,
                    input.Connect.DeliveryHeroUser);

                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.ConnectSettings.DeliveryHeroPassword,
                    input.Connect.DeliveryHeroPassword);

                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.ConnectSettings.DeliveryHeroPriceTag,
                    input.Connect.DeliveryHeroPriceTag);

                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.ConnectSettings.DeliveryHeroPushMenuUrl,
                    input.Connect.DeliveryHeroPushMenuUrl);
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.ConnectSettings.LocationGroupAsBrand,
                        input.Connect.LocationGroupAsBrand.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));

                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                AppSettings.ConnectSettings.CreditSaleReport,
                input.Connect.CreditSaleReport);
            }

            if (await FeatureChecker.IsEnabledAsync(AppFeatures.Cater))
            {
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.CaterSettings.CaterMenu,
                        input.Cater.CaterMenu);
            }

            if (await FeatureChecker.IsEnabledAsync(AppFeatures.Engage))
            {
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.EngageSettings.PointsAccumulation,
                        input.Engage.PointsAccumulation.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.EngageSettings.PointsAccumulationFactor,
                        input.Engage.PointsAccumulationFactor.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.EngageSettings.PointsRedemption,
                        input.Engage.PointsRedemption.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.EngageSettings.PointsRedemptionFactor,
                        input.Engage.PointsRedemptionFactor.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.EngageSettings.OnBoardMemberFromPos,
                        input.Engage.OnBoardMemberFromPOS.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.EngageSettings.CalculatePoints,
                        input.Engage.CalculatePoints.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.EngageSettings.CalculatePointsOnOrder,
                        input.Engage.CalculatePointsOnOrder.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.EngageSettings.CalculatePointsOnMultiple,
                        input.Engage.CalculatePointsOnMultiple.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.EngageSettings.AutoGenerateMemberCode,
                        input.Engage.AutoGenerateMemberCode.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.EngageSettings.CodeFormat,
                        input.Engage.CodeFormat);
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.EngageSettings.CreateNewUserOnActivation,
                        input.Engage.CreateNewUserOnActivation.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.EngageSettings.MemberUserRoleID,
                        input.Engage.MemberUserRoleID.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.EngageSettings.MemberUserRoleName,
                        input.Engage.MemberUserRoleName);
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.EngageSettings.DefaultCity,
                        input.Engage.DefaultCity);

                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.EngageSettings.DefaultState,
                        input.Engage.DefaultState);

                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.EngageSettings.DefaultCountry,
                        input.Engage.DefaultCountry);
            }

            if (await FeatureChecker.IsEnabledAsync(AppFeatures.House))
            {
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.HouseSettings.SupplierMaterialTightlyCoupled,
                        input.House.SupplierMaterialTightlyCoupled.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.HouseSettings.DirectTransferAllowed,
                        input.House.DirectTransferAllowed.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.HouseSettings.InvoiceSupplierRateChangeFlag,
                        input.House.InvoiceSupplierRateChangeFlag.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.HouseSettings.TallyIntegrationFlag,
                        input.House.TallyIntegrationFlag.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.HouseSettings.ExtraMaterialReceivedInDc,
                        input.House.ExtraMaterialReceivedInDc.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.HouseSettings.InterTransferValueShown,
                        input.House.InterTransferValueShown.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.HouseSettings.SmartPrintOutExists,
                        input.House.SmartPrintOutExists.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.HouseSettings.MessageNotification,
                        input.House.MessageNotification.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.HouseSettings.SoftMessageNotification,
                        input.House.SoftMessageNotification.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.HouseSettings.ForceAdjustmentWithDayClose,
                        input.House.ForceAdjustmentWithDayClose.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.HouseSettings.MaterialCostBasedOnLastPurchase,
                        input.House.MaterialCostBasedOnLastPurchase.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.HouseSettings.Decimals,
                        input.House.Decimals.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.HouseSettings.IsMaterialCodeTreatedAsBarCode,
                        input.House.IsMaterialCodeTreatedAsBarCode.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.HouseSettings.DefaultAveragePriceTagRefId,
                        input.House.DefaultAveragePriceTagRefId.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.HouseSettings.NoOfMonthAvgTaken,
                        input.House.NoOfMonthAvgTaken.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));

                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.HouseSettings.IsOmitFOCMaterialQuantities,
                    input.House.IsOmitFOCMaterialQuantities.ToString(CultureInfo.InvariantCulture)
                        .ToLower(CultureInfo.InvariantCulture));

                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.HouseSettings.IsCalculateAverageRateWithoutTax,
                    input.House.IsCalculateAverageRateWithoutTax.ToString(CultureInfo.InvariantCulture)
                        .ToLower(CultureInfo.InvariantCulture));
                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.HouseSettings.AbsentDeductionPerDay,
                    input.House.AbsentDeductionPerDay.ToString(CultureInfo.InvariantCulture)
                        .ToUpper(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.HouseSettings.IsInwardInvoiceNumberReferenceFromOtherErpRequired,
                        input.House.IsInwardInvoiceNumberReferenceFromOtherErpRequired
                            .ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.HouseSettings.IsInwardPurchaseOrderReferenceFromOtherErpRequired,
                        input.House.IsInwardPurchaseOrderReferenceFromOtherErpRequired
                            .ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));

                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.HouseSettings.MaterialCountToUpdate,
                        input.House.MaterialCountToUpdate.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.HouseSettings.WhileClosingStockTakenCanAllUnitAllowed,
                        input.House.WhileClosingStockTakenCanAllUnitAllowed.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));

                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                   AppSettings.HouseSettings.BackGroundDayCloseCanBeAllowed,
                   input.House.BackGroundDayCloseCanBeAllowed.ToString(CultureInfo.InvariantCulture)
                       .ToLower(CultureInfo.InvariantCulture));

                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.HouseSettings.TransferRequestLockHours,
                    input.House.TransferRequestLockHours.ToString(CultureInfo.InvariantCulture)
                        .ToUpper(CultureInfo.InvariantCulture));

                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                   AppSettings.HouseSettings.TransferRequestGraceHours,
                   input.House.TransferRequestGraceHours.ToString(CultureInfo.InvariantCulture)
                       .ToUpper(CultureInfo.InvariantCulture));

                await SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                  AppSettings.HouseSettings.PurchaseOrderCCMailList,
                  input.House.PurchaseOrderCCMailList.ToString(CultureInfo.InvariantCulture)
                      .ToUpper(CultureInfo.InvariantCulture));
            }

            if (await FeatureChecker.IsEnabledAsync(AppFeatures.Swipe))
            {
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.SwipeSettings.CanIssueCardDirectly,
                        input.Swipe.CanIssueCardDirectly.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.SwipeSettings.DepositRequiredDefault,
                        input.Swipe.DepositRequiredDefault.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.SwipeSettings.DepositAmountDefault,
                        input.Swipe.DepositAmountDefault.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.SwipeSettings.ExpiryDaysFromIssueDefault,
                        input.Swipe.ExpiryDaysFromLastUsageDefault.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.SwipeSettings.RefundAllowedDefault,
                        input.Swipe.RefundAllowedDefault.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
            }

            if (await FeatureChecker.IsEnabledAsync(AppFeatures.Touch))
            {
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.TouchSaleSettings.Rounding,
                        input.Compact.Rounding.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.TouchSaleSettings.TaxInclusive,
                        input.Compact.TaxInclusive.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.TouchSaleSettings.Decimals,
                        input.Compact.TotalDecimal.ToString(CultureInfo.InvariantCulture)
                            .ToLower(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.TouchSaleSettings.ReceiptHeader, input.Compact.ReceiptHeader);
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.TouchSaleSettings.ReceiptFooter, input.Compact.ReceiptFooter);
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.TouchSaleSettings.IsBlindShift,
                        input.Compact.IsBlindShift.ToString(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.TouchSaleSettings.IsActivity,
                        input.Compact.IsActivity.ToString(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.TouchSaleSettings.IsReport,
                        input.Compact.IsReport.ToString(CultureInfo.InvariantCulture));
                await
                    SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                        AppSettings.TouchSaleSettings.DuplicateReceipt,
                        input.Compact.DuplicateReceipt.ToString(CultureInfo.InvariantCulture));
            }

            await
                SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.PasswordPolicySettings.Min_Length,
                    input.PasswordPolicy.Min_Length.ToString(CultureInfo.InvariantCulture)
                        .ToLower(CultureInfo.InvariantCulture));
            await
                SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.PasswordPolicySettings.Max_Length,
                    input.PasswordPolicy.Max_Length.ToString(CultureInfo.InvariantCulture)
                        .ToLower(CultureInfo.InvariantCulture));
            await
                 SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                     AppSettings.PasswordPolicySettings.Min_LCL,
                     input.PasswordPolicy.Min_LCL.ToString(CultureInfo.InvariantCulture)
                         .ToLower(CultureInfo.InvariantCulture));
            await
                SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.PasswordPolicySettings.Min_UCL,
                    input.PasswordPolicy.Min_UCL.ToString(CultureInfo.InvariantCulture)
                        .ToLower(CultureInfo.InvariantCulture));
            await
                SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.PasswordPolicySettings.Min_AC,
                    input.PasswordPolicy.Min_AC.ToString(CultureInfo.InvariantCulture)
                        .ToLower(CultureInfo.InvariantCulture));
            await
                SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.PasswordPolicySettings.Min_SC,
                    input.PasswordPolicy.Min_SC.ToString(CultureInfo.InvariantCulture)
                        .ToLower(CultureInfo.InvariantCulture));
            await
                SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.PasswordPolicySettings.Size,
                    input.PasswordPolicy.Size.ToString(CultureInfo.InvariantCulture)
                        .ToLower(CultureInfo.InvariantCulture));
            await
                SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.PasswordPolicySettings.Impermissible_Passwords,
                    input.PasswordPolicy.Impermissible_Passwords.ToString(CultureInfo.InvariantCulture)
                        .ToLower(CultureInfo.InvariantCulture));
            await
                SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.PasswordPolicySettings.Maximum_IT,
                    input.PasswordPolicy.Maximum_IT.ToString(CultureInfo.InvariantCulture)
                        .ToLower(CultureInfo.InvariantCulture));
            await
                SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.PasswordPolicySettings.Password_VP,
                    input.PasswordPolicy.Password_VP.ToString(CultureInfo.InvariantCulture)
                        .ToLower(CultureInfo.InvariantCulture));
            await
                SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.PasswordPolicySettings.Allow_LIDPP,
                    input.PasswordPolicy.Allow_LIDPP.ToString(CultureInfo.InvariantCulture)
                        .ToLower(CultureInfo.InvariantCulture));
            await
                SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.PasswordPolicySettings.Allow_OPPP,
                    input.PasswordPolicy.Allow_OPPP.ToString(CultureInfo.InvariantCulture)
                        .ToLower(CultureInfo.InvariantCulture));
            await
                SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.PasswordPolicySettings.Allow_UCOP,
                    input.PasswordPolicy.Allow_UCOP.ToString(CultureInfo.InvariantCulture)
                        .ToLower(CultureInfo.InvariantCulture));
            await
                SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.PasswordPolicySettings.Enforce,
                    input.PasswordPolicy.Enforce.ToString(CultureInfo.InvariantCulture)
                        .ToLower(CultureInfo.InvariantCulture));
            await
                SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.PasswordPolicySettings.Maximum_NFLA,
                    input.PasswordPolicy.Maximum_NFLA.ToString(CultureInfo.InvariantCulture)
                        .ToLower(CultureInfo.InvariantCulture));
            await
                SettingManager.ChangeSettingForTenantAsync(AbpSession.GetTenantId(),
                    AppSettings.PasswordPolicySettings.Suggest_Message,
                    input.PasswordPolicy.Suggest_Message.ToString(CultureInfo.InvariantCulture)
                        .ToLower(CultureInfo.InvariantCulture));

            _cacheManager.GetApplicationSettingsCache().Clear();
        }

        public async Task<bool> SmtpSettingLoginValidation(TenantSettingsEditDto input)
        {
            bool success = false;
            if (input.Email != null)
            {
                string emailAddress = input.Email.DefaultFromAddress;
                if (!string.IsNullOrEmpty(emailAddress))
                {
                    var tenantId = AbpSession.TenantId;
                    var user = await _userManager.Users.Where(
                u => u.EmailAddress == emailAddress && u.TenantId == tenantId
                ).FirstOrDefaultAsync();

                    if (user == null)
                    {
                        throw new UserFriendlyException(L("InvalidEmailAddress"));
                    }
                    user.SetNewPasswordResetCode();
                    var output = await _userEmailer.SendPasswordResetLinkAsync(user);
                    if (output)
                    {
                        success = true;
                    }
                    else
                    {
                        success = false;
                    }
                }
            }
            return success;
        }

        private TimeSpan ConvertStringToTimeSpan(string timeString)
        {
            var timeSpan = new TimeSpan();
            var times = timeString.Split(':');
            if (times != null && times.Length >= 3)
            {
                timeSpan = TimeSpan.FromHours(double.Parse(times[0]));
                timeSpan += TimeSpan.FromMinutes(double.Parse(times[1]));
                timeSpan += TimeSpan.FromSeconds(double.Parse(times[2]));
            }
            return timeSpan;
        }
    }
}