﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter;
using DinePlan.DineConnect.Exporter.Util;
using OpenHtmlToPdf;

namespace DinePlan.DineConnect.DataExporting.Excel.EpPlus
{
    public interface IFileExport
    {
        ExportType ExportOutputType { get; set; }
        PaperSize PaperSize { get; set; }
        bool Portrait { get; set; }
    }

    public interface IBackgroundExport
    {
        bool RunInBackground { get; set; }
        string ReportDescription { get; set; }

    }
}
