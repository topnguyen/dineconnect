﻿using System;
using System.Collections.Generic;
using System.IO;
using Abp.Collections.Extensions;
using Abp.Dependency;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace DinePlan.DineConnect.DataExporting.Base
{
    public abstract class ExporterBase : DineConnectServiceBase, ITransientDependency
    {
        public IAppFolders AppFolders { get; set; }

    }
}