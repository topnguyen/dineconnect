﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using Abp.Dependency;
using DinePlan.DineConnect.Dto;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Font = iTextSharp.text.Font;
using Rectangle = iTextSharp.text.Rectangle;

namespace DinePlan.DineConnect.DataExporting.Pdf
{
    public abstract class PdfExporterBase : DineConnectServiceBase, ITransientDependency
    {
        private PdfWriter _writer;
        public IAppFolders AppFolders { get; set; }

        static readonly BaseFont BfTimes = BaseFont.CreateFont(BaseFont.COURIER_BOLD, BaseFont.CP1252, false);
        private readonly Font _times = new Font(BfTimes, 24, Font.ITALIC, BaseColor.RED);


        protected Document GetBasePdfDocument(FileDto myfile, Rectangle rectangle, bool rotate)
        {
            var document = new Document();
            document.AddTitle("DineConnect");
            document.AddCreator("DineConnect");
            document.AddAuthor("DinePlan Suite");
            document.SetMargins(10, 10, 10, 10);
            document.SetPageSize(rotate ? rectangle.Rotate() : rectangle);
            var path = Path.Combine(AppFolders.TempFileDownloadFolder, myfile.FileToken);
            _writer = PdfWriter.GetInstance(document, new FileStream(path, FileMode.Create));
            document.Open();
            return document;
        }

        protected void Close(Document doc)
        {
            doc.Close();
            _writer?.Close();
        }
        protected void AddHeader(Document document, string test)
        {
            var header = new Paragraph(test) {Font = _times };
            document.Add(header);
        }
        protected void AddParagraph(Document document, string test)
        {
            document.Add(new Paragraph(test));
        }

        protected void AddPage(Document document)
        {
            document.NewPage();
        }

        protected PdfPTable AddPdfTable(Document document, List<string> cols)
        {
            if (cols != null && cols.Any())
            {
                int numCols = cols.Count();
                PdfPTable table = new PdfPTable(numCols) {WidthPercentage = 98};

                foreach (var col in cols)
                {
                    table.AddCell(col.Replace(" ","-"));
                }
                table.HeaderRows = 1;

                return table;
            }
            return null;
        }
        protected void AddTableRow(PdfPTable table, List<string> allStr)
        {
            if (allStr.Any())
            {
                foreach (var allS in allStr)
                {
                    table.AddCell(allS);
                }
            }
        }
        protected void AddFirstPage(Document document, string s)
        {
            document.Add(new Paragraph(s));
            document.Add(new Paragraph(DateTime.Now.ToShortDateString()));
        }

        protected void AttachTableToDocument(Document document, PdfPTable table)
        {
            document.Add(table);
        }
    }
}