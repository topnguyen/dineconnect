﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Abp.Dependency;
using DinePlan.DineConnect.Dto;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace DinePlan.DineConnect.DataExporting.Pdf
{
    public class PdfReportDto
    {
        public string Name { get; set; }
        public string ReportUser { get; set; }

        public string ReportDate { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }

        public PdfReportDto()
        {
            ReportDate = DateTime.Now.ToLongDateString();
        }
    }
}