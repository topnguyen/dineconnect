using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Auditing.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Auditing
{
    public interface IAuditLogAppService : IApplicationService
    {
        Task<PagedResultOutput<AuditLogListDto>> GetAuditLogs(GetAuditLogsInput input);

        Task<FileDto> GetAuditLogsToExcel(GetAuditLogsInput input);

        Task<PagedResultOutput<SystemLoggingListDto>> GetSystemAuditLogs(GetSystemAuditLogsInput input);

        Task<PagedResultDto<TrackerLogListDto>> GetTrackerAuditLogs(GetTrackerLogsInput input);

        Task<FileDto> GetTrackerAuditLogsToExcel(GetTrackerLogsInput input);
        ListResultDto<ComboboxItemDto> GetAuditTypes();
    }
}