﻿using Abp.Application.Services.Dto;
using Abp.Auditing;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Timing;
using DinePlan.DineConnect.Addon;
using DinePlan.DineConnect.Auditing.Dto;
using DinePlan.DineConnect.Auditing.Exporting;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Connect.Audit;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.EntityFramework;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Report;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TrackerEnabledDbContext.Common.Models;

namespace DinePlan.DineConnect.Auditing
{
    [DisableAuditing]
    [AbpAuthorize(AppPermissions.Pages_Administration_AuditLogs)]
    public class AuditLogAppService : DineConnectAppServiceBase, IAuditLogAppService
    {
        private readonly IRepository<AuditLog, long> _auditLogRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IAuditLogListExcelExporter _auditLogListExcelExporter;
        private readonly IRepository<SystemLogging> _systemLoggingRepo;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<ExternalLog> _externalLogRepo;
        private readonly IBackgroundJobManager _bgm;
        private readonly IReportBackgroundAppService _rbas;
        //private readonly ITrackerContext _trackerContext;

        public AuditLogAppService(IRepository<AuditLog, long> auditLogRepository,
            IRepository<User, long> userRepository,
            IAuditLogListExcelExporter auditLogListExcelExporter,
            IRepository<SystemLogging> systemLoggingRepo,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<ExternalLog> externalLogRepo,
            IBackgroundJobManager bgm,
            IReportBackgroundAppService rbas
            //ITrackerContext trackerContext
            )
        {
            _auditLogRepository = auditLogRepository;
            _userRepository = userRepository;
            _auditLogListExcelExporter = auditLogListExcelExporter;
            _systemLoggingRepo = systemLoggingRepo;
            _unitOfWorkManager = unitOfWorkManager;
            _externalLogRepo = externalLogRepo;
            _bgm = bgm;
            _rbas = rbas;
            //_trackerContext = trackerContext;
        }

        public async Task<PagedResultOutput<AuditLogListDto>> GetAuditLogs(GetAuditLogsInput input)
        {
            var myCo = Clock.Provider;

            var query = CreateAuditLogAndUsersQuery(input);

            var resultCount = await query.CountAsync();
            var results = await query
                .AsNoTracking()
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var auditLogListDtos = ConvertToAuditLogListDtos(results);

            return new PagedResultOutput<AuditLogListDto>(resultCount, auditLogListDtos);
        }

        public async Task<FileDto> GetAuditLogsToExcel(GetAuditLogsInput input)
        {
            var auditLogs = await CreateAuditLogAndUsersQuery(input)
                        .AsNoTracking()
                        .OrderByDescending(al => al.AuditLog.ExecutionTime)
                        .ToListAsync();

            var auditLogListDtos = ConvertToAuditLogListDtos(auditLogs);

            return _auditLogListExcelExporter.ExportToFile(auditLogListDtos);
        }

        public async Task<PagedResultOutput<SystemLoggingListDto>> GetSystemAuditLogs(GetSystemAuditLogsInput input)
        {
            var myCo = Clock.Provider;

            var query = CreateSystemLogQuery(input);

            var resultCount = await query.CountAsync();
            var results = await query
                .AsNoTracking()
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var auditLogListDtos = ConvertToAuditLogListDtos(results);

            return new PagedResultOutput<SystemLoggingListDto>(resultCount, auditLogListDtos);
        }

        public async Task<PagedResultDto<TrackerLogListDto>> GetTrackerAuditLogs(GetTrackerLogsInput input)
        {
            using (var context = new DineConnectDbContext())
            {
                EventType enventType = EventType.Modified;

                if (!input.FilterText.IsNullOrWhiteSpace())
                {
                    input.FilterText = Regex.Replace(input.FilterText.Trim(), @"\s+", " ");

                    Enum.TryParse(input.FilterText, out enventType);
                }

                var query = context.AuditLog
                    .Where(auditLog => auditLog.EventDateUTC >= input.StartDate && auditLog.EventDateUTC < input.EndDate)
                    .WhereIf(!input.UserName.IsNullOrWhiteSpace(), item => item.UserName.Contains(input.UserName))
                    .WhereIf(!input.FilterText.IsNullOrWhiteSpace(), item => item.TypeFullName.Contains(input.FilterText) || item.EventType == enventType);

                var resultCount = await query.CountAsync();

                var pagedQuery = await query
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var results = pagedQuery.MapTo<List<TrackerLogListDto>>();

                foreach (var item in results)
                {
                    var logDetails = await context.LogDetails.Where(d => d.AuditLogId == item.AuditLogId).ToListAsync();
                    item.LogDetails = logDetails.MapTo<List<TrackerAuditLogDetailDto>>();
                }

                return new PagedResultDto<TrackerLogListDto>(resultCount, results);
            }
        }

        public async Task<FileDto> GetTrackerAuditLogsToExcel(GetTrackerLogsInput input)
        {
            using (var context = new DineConnectDbContext())
            {
                var auditLogs = await context.AuditLog
                    .Where(auditLog => auditLog.EventDateUTC >= input.StartDate && auditLog.EventDateUTC < input.EndDate)
                    .WhereIf(!input.UserName.IsNullOrWhiteSpace(), item => item.UserName.Contains(input.UserName)).AsNoTracking()
                        .OrderByDescending(al => al.EventDateUTC)
                        .ToListAsync();

                var auditLogListDtos = auditLogs.MapTo<List<TrackerLogListDto>>();

                return _auditLogListExcelExporter.ExportTrackerLogToFile(auditLogListDtos);
            }
        }

        private static List<AuditLogListDto> ConvertToAuditLogListDtos(List<AuditLogAndUser> results)
        {
            return results.Select(
                result =>
                {
                    var auditLogListDto = result.AuditLog.MapTo<AuditLogListDto>();
                    auditLogListDto.UserName = result.User == null ? null : result.User.UserName;
                    auditLogListDto.ServiceName = StripNameSpace(auditLogListDto.ServiceName);
                    return auditLogListDto;
                }).ToList();
        }

        private static List<SystemLoggingListDto> ConvertToAuditLogListDtos(List<SystemLogging> results)
        {
            return results.Select(
                result =>
                {
                    var auditLogListDto = result.MapTo<SystemLoggingListDto>();
                    return auditLogListDto;
                }).ToList();
        }

        private IQueryable<AuditLogAndUser> CreateAuditLogAndUsersQuery(GetAuditLogsInput input)
        {
            var query = from auditLog in _auditLogRepository.GetAll()
                        join user in _userRepository.GetAll() on auditLog.UserId equals user.Id into userJoin
                        from joinedUser in userJoin.DefaultIfEmpty()
                        where auditLog.ExecutionTime >= input.StartDate && auditLog.ExecutionTime < input.EndDate
                        select new AuditLogAndUser { AuditLog = auditLog, User = joinedUser };

            query = query
                .WhereIf(!input.UserName.IsNullOrWhiteSpace(), item => item.User.UserName.Contains(input.UserName))
                .WhereIf(!input.ServiceName.IsNullOrWhiteSpace(), item => item.AuditLog.ServiceName.Contains(input.ServiceName))
                .WhereIf(!input.MethodName.IsNullOrWhiteSpace(), item => item.AuditLog.MethodName.Contains(input.MethodName))
                .WhereIf(!input.BrowserInfo.IsNullOrWhiteSpace(), item => item.AuditLog.BrowserInfo.Contains(input.BrowserInfo))
                .WhereIf(input.MinExecutionDuration.HasValue && input.MinExecutionDuration > 0, item => item.AuditLog.ExecutionDuration >= input.MinExecutionDuration.Value)
                .WhereIf(input.MaxExecutionDuration.HasValue && input.MaxExecutionDuration < int.MaxValue, item => item.AuditLog.ExecutionDuration <= input.MaxExecutionDuration.Value)
                .WhereIf(input.HasException == true, item => item.AuditLog.Exception != null && item.AuditLog.Exception != "")
                .WhereIf(input.HasException == false, item => item.AuditLog.Exception == null || item.AuditLog.Exception == "");
            return query;
        }

        private IQueryable<SystemLogging> CreateSystemLogQuery(GetSystemAuditLogsInput input)
        {
            var query = from auditLog in _systemLoggingRepo.GetAll()
                        where auditLog.log_date >= input.StartDate && auditLog.log_date < input.EndDate
                        select auditLog;

            query = query
                .WhereIf(input.HasException, item => item.log_level.Equals("Error"));
            ;
            return query;
        }

        private static string StripNameSpace(string serviceName)
        {
            if (serviceName.IsNullOrEmpty() || !serviceName.Contains("."))
            {
                return serviceName;
            }

            var lastDotIndex = serviceName.LastIndexOf('.');
            return serviceName.Substring(lastDotIndex + 1);
        }
        public ListResultDto<ComboboxItemDto> GetAuditTypes()
        {
            var returnList = new List<ComboboxItemDto>();

            var i = 0;
            foreach (var name in Enum.GetNames(typeof(ExternalLogType)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }
    }
}