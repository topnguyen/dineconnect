﻿using Abp.Application.Services.Dto;
using Abp.Auditing;
using Abp.AutoMapper;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.Timing;
using DinePlan.DineConnect.Addon;
using DinePlan.DineConnect.Connect.Audit;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using TrackerEnabledDbContext.Common.Models;

namespace DinePlan.DineConnect.Auditing.Dto
{
    [AutoMapFrom(typeof(AuditLog))]
    public class AuditLogListDto : EntityDto<long>
    {
        public long? UserId { get; set; }

        public string UserName { get; set; }

        public int? ImpersonatorTenantId { get; set; }

        public long? ImpersonatorUserId { get; set; }

        public string ServiceName { get; set; }

        public string MethodName { get; set; }

        public string Parameters { get; set; }

        public DateTime ExecutionTime { get; set; }

        public int ExecutionDuration { get; set; }

        public string ClientIpAddress { get; set; }

        public string ClientName { get; set; }

        public string BrowserInfo { get; set; }

        public string Exception { get; set; }

        public string CustomData { get; set; }
    }

    [AutoMapFrom(typeof(SystemLogging))]
    public class SystemLoggingListDto
    {
        public DateTime log_date { get; set; }
        public string log_level { get; set; }
        public string log_message { get; set; }
        public string log_call_site { get; set; }
        public string log_logger { get; set; }
        public string log_user_name { get; set; }
        public string log_exception { get; set; }
        public string log_stacktrace { get; set; }
        public string log_machine_name { get; set; }
    }

    [AutoMapFrom(typeof(TrackerAuditLog))]
    public class TrackerLogListDto
    {
        public long AuditLogId { get; set; }

        public string UserName { get; set; }

        public DateTime EventDateUTC { get; set; }

        public EventType EventType { get; set; }

        public string TypeFullName { get; set; }

        public string RecordId { get; set; }

        public virtual List<TrackerAuditLogDetailDto> LogDetails { get; set; } = new List<TrackerAuditLogDetailDto>();
    }

    [AutoMapFrom(typeof(TrackerAuditLogDetail))]
    public class TrackerAuditLogDetailDto
    {
        public long Id { get; set; }

        public string PropertyName { get; set; }

        public string OriginalValue { get; set; }

        public string NewValue { get; set; }

        public virtual long AuditLogId { get; set; }
    }
    [AutoMapFrom(typeof(ExternalLog))]
    public class ExternalLogListDto : FullAuditedEntityDto
    {
        public  DateTime ExternalLogTrunc { get; set; }
        public string LogTimeStr => LogTime.ToString("yyyy-MM-dd HH:mm:ss");
        public int AuditType { get; set; }
        public string LogDescription { get; set; }
        public string LogData { get; set; }
        public DateTime LogTime { get; set; }
        public string ExternalLogType => ((ExternalLogType)AuditType).ToString();
    }
    public class GetExternalLogInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<ComboboxItemDto> AuditTypes { get; set; }
        public bool IsConfirmation { get; set; }
        public bool RunInBackground { get; set; }
        public string OutputType { get; set; }
        public bool Portrait { get; set; }
        public string ReportDescription { get; set; }
        public void Normalize()
        {
            if (Sorting.IsNullOrWhiteSpace())
            {
                Sorting = "ExternalLogTrunc DESC";
            }
            if (StartDate == DateTime.MinValue)
            {
                StartDate = Clock.Now;
            }

            StartDate = StartDate.Date;

            if (EndDate == DateTime.MinValue)
            {
                EndDate = Clock.Now;
            }

            EndDate = EndDate.AddDays(1).Date;
        }
    }
}