﻿using DinePlan.DineConnect.Auditing.Dto;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Auditing.Exporting
{
    public interface IAuditLogListExcelExporter
    {
        FileDto ExportToFile(List<AuditLogListDto> auditLogListDtos);

        FileDto ExportTrackerLogToFile(List<TrackerLogListDto> auditLogListDtos);
        Task<FileDto> ExportExternalLogToFile(GetItemInput input, IConnectReportAppService appService);
    }
}