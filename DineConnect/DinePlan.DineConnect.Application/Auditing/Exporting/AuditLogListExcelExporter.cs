﻿using Abp.Extensions;
using DinePlan.DineConnect.Auditing.Dto;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using System.Collections.Generic;
using System.Threading.Tasks;
using TrackerEnabledDbContext.Common.Models;

namespace DinePlan.DineConnect.Auditing.Exporting
{
    public class AuditLogListExcelExporter : FileExporterBase, IAuditLogListExcelExporter
    {
        public FileDto ExportToFile(List<AuditLogListDto> auditLogListDtos)
        {
            return CreateExcelPackage(
                "AuditLogs.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("AuditLogs"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Time"),
                        L("UserName"),
                        L("Service"),
                        L("Action"),
                        L("Parameters"),
                        L("Duration"),
                        L("IpAddress"),
                        L("Client"),
                        L("Browser"),
                        L("ErrorState")
                    );

                    AddObjects(
                        sheet, 2, auditLogListDtos,
                        _ => _.ExecutionTime,
                        _ => _.UserName,
                        _ => _.ServiceName,
                        _ => _.MethodName,
                        _ => _.Parameters,
                        _ => _.ExecutionDuration,
                        _ => _.ClientIpAddress,
                        _ => _.ClientName,
                        _ => _.BrowserInfo,
                        _ => _.Exception.IsNullOrEmpty() ? L("Success") : _.Exception
                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(1);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 10; i++)
                    {
                        if (i.IsIn(5, 10)) //Don't AutoFit Parameters and Exception
                        {
                            continue;
                        }

                        sheet.Column(i).AutoFit();
                    }
                });
        }

        public FileDto ExportTrackerLogToFile(List<TrackerLogListDto> auditLogListDtos)
        {
            return CreateExcelPackage(
                "AuditLogs.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(base.L("AuditLogs"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        base.L("Time"),
                        base.L("UserName"),
                        base.L("Service"),
                        base.L("Action"),
                        base.L("Parameters"),
                        base.L("Duration"),
                        base.L("IpAddress"),
                        base.L("Client"),
                        base.L("Browser"),
                        base.L("ErrorState")
                    );

                    AddObjects(
                        sheet, 2, auditLogListDtos,
                        _ => _.EventDateUTC,
                        _ => _.UserName,
                        _ => GetEventType(_.EventType),
                        _ => _.TypeFullName
                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(1);
                    timeColumn.Style.Numberformat.Format = "mm-dd-yy hh:mm:ss";

                    for (var i = 1; i <= 10; i++)
                    {
                        if (i.IsIn(5, 10)) //Don't AutoFit Parameters and Exception
                        {
                            continue;
                        }

                        sheet.Column(i).AutoFit();
                    }
                });
        }
     
        public async Task<FileDto> ExportExternalLogToFile(GetItemInput input, IConnectReportAppService appService)
        {
            var file = new FileDto("External_Log_" + input.StartDate.ToString("yyyy-MM-dd") + "_" + input.EndDate.ToString("yyyy-MM-dd") + ".xlsx",                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            return await ExportExternalLog(input, appService, file);
        }
        private async Task<FileDto> ExportExternalLog(GetItemInput input, IConnectReportAppService appService, FileDto file)
        {
            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(base.L("ExternalLogs"));
                sheet.OutLineApplyStyle = true;

                AddHeader(
                    sheet,
                    base.L("Date"),
                    base.L("Type"),
                    base.L("LogDescription"),
                    base.L("LogData")
                );

                var rowCount = 2;
                input.MaxResultCount = 100;
                var outPut = await appService.GetAllExternalLog(input);

                if (outPut!=null && outPut.Items.Count>0)
                {
                    foreach (var externallog in outPut.Items)
                    {
                        var colCount = 1;

                        sheet.Cells[rowCount, colCount++].Value = externallog.LogTimeStr;
                        sheet.Cells[rowCount, colCount++].Value = externallog.ExternalLogType;
                        sheet.Cells[rowCount, colCount++].Value = externallog.LogDescription;
                        sheet.Cells[rowCount, colCount++].Value = externallog.LogData;
                        rowCount++;
                    }

                    input.SkipCount = input.SkipCount + input.MaxResultCount;
                }
                
                //Formatting cells

                for (var i = 1; i <= 10; i++)
                {
                    sheet.Column(i).AutoFit();
                }
                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }
        private static string GetEventType(EventType type)
        {
            switch (type)
            {
                case EventType.Added:
                    return "Added";

                case EventType.Deleted:
                    return "Deleted";

                case EventType.Modified:
                    return "Modified";

                case EventType.SoftDeleted:
                    return "SoftDeleted";

                case EventType.UnDeleted:
                    return "UnDeleted";

                default:
                    return "UnChanged";
            }
        }
    }
}