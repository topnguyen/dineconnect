﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.General
{
    public class IdNameDto
    {
        public IdNameDto()
        {
            
        }
        public IdNameDto(int portionId, string portionName)
        {
            this.Id = portionId;
            this.Name = portionName;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int MenuItemId { get; set; }

    }

   
}
