﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.General
{
    public class FormlyType
    {
        public FormlyType()
        {
            TemplateOptions = new FormlyTemplateOption();
            ClassName = "";
        }
        public string Key { get; set; }
        public string Type { get; set; }
        public string ClassName { get; set; }
        public object DefaultValue { get; set; }
        public FormlyTemplateOption TemplateOptions { get; set; }
    }
   
    public class FormlyTemplateOption
    {
        public FormlyTemplateOption()
        {
            Type = "text";
            Label = "";
            Placeholder = "";
            Pattern = "";
            BtnText = "";
            Min = "";
            Max = "";
            Options = new List<Option>();
        }

        public string Label { get; set; }
        public string Placeholder { get; set; }
        public string Min { get; set; }
        public string Max{ get; set; }

        public string Type { get; set; }
        public string Pattern { get; set; }
        public string BtnText { get; set; }
        public List<Option> Options { get; set; }
        public List<Field> Fields { get; set; }
    }
    public class Field
    {
        public Field()
        {
            ClassName = "";
            FieldGroup = new List<FormlyType>();
        }
        public string ClassName { get; set; }
        public List<FormlyType> FieldGroup { get; set; }

    }
	public class Option
    {
		public string Value { get; set; }
		public string Name { get; set; }
	}

    
}
