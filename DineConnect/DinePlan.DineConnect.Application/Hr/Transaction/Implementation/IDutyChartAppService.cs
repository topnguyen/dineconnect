﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Master.Dtos;

namespace DinePlan.DineConnect.Hr.Transaction
{
    public interface IDutyChartAppService : IApplicationService
    {
        Task<PagedResultOutput<DutyChartListDto>> GetAll(GetDutyChartInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetDutyChartForEditOutput> GetDutyChartForEdit(GetDutyChartForGivenDate input);
        Task<IdInput> CreateOrUpdateDutyChart(CreateOrUpdateDutyChartInput input);
        Task DeleteDutyChart(IdInput input);
        Task IncludeAllActiveEmployees(LocationInputDto input);
        Task<MaxDateWithStatus> GetDutyChartMaximumDate();
        Task<FileNameDto> GetConsolidatedExcelOfDutyChartForGivenDate(InputExcelDutyChartCompanyWiseDto input);
        Task<FileNameDto> GetExcelOfDutyChartForGivenDate(InputExcelDutyChartCompanyWiseDto input);
        Task<MessageOutput> HrOperationAlert(GetHrOperationHealthDto input);
        Task<MessageOutput> HrOperationHealthEdp(GetHrOperationHealthDto input);
        Task<EmployeeConsolidatedInformation> GetEmployeeConsolidatedInfo();
        Task<bool> RemoveDutyChart(DutyChartDetailListDto input);
        Task<List<LeaveRequestListDto>> CheckLeaveContinuation(GetLeaveEmployeesWithToDate input);

        Task<VerifyDayCloseOutput> VerifyDayClose(VerifyDayCloseDates input);
        Task<DateInput> DutyChartDayClose(IdInput input);

    }
}
