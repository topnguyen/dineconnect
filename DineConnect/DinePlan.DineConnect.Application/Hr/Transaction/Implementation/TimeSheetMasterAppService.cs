﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Abp.Application.Features;
using OfficeOpenXml;
using Abp.Domain.Uow;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Utility.Exporter;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Hr.Master;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using System.Net.Mail;
using System.Text;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Hr;
using DinePlan.DineConnect.Hr.Impl;
using DinePlan.DineConnect.Common;
using DinePlan.DineConnect.House;
using DinePlan.DineConnect.Hr.Transaction;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Features;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Exporter;
using DinePlan.DineConnect.Hr.Master.Implementation;

namespace DinePlan.DineConnect.Hr.Transaction.Implementation
{
    public class TimeSheetMasterAppService : DineConnectAppServiceBase, ITimeSheetMasterAppService
    {
        private readonly ITimeSheetMasterListExcelExporter _timesheetmasterExporter;
        private readonly ITimeSheetMasterManager _timesheetmasterManager;
        private readonly IRepository<TimeSheetMaster> _timesheetmasterRepo;
        private readonly IRepository<TimeSheetDetail> _timesheetdetailRepo;
        private readonly IRepository<TimeSheetMaterialUsage> _timesheetmaterialusageRepo;
        private readonly IRepository<TimeSheetEmployeeDetail> _timesheetemployeedetailRepo;
        private readonly IRepository<PersonalInformation> _personalinformationRepo;
        private readonly IRepository<DutyChart> _dutyChartRepo;
        private readonly IRepository<DutyChartDetail> _dutyChartDetailRepo;
        private readonly IRepository<DcShiftMaster> _shiftMasterRepo;
        private readonly IRepository<EmployeeSkillSet> _employeeskillsetRepo;
        private readonly IRepository<SkillSet> _skillsetRepo;
        private readonly IRepository<LeaveRequest> _leaveRequestRepo;
        private readonly IRepository<Company> _companyRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<AttendanceLog> _attendanceLogRepo;
        private readonly IRepository<AttendanceMachineLocation> _attendanceMachineLocationRepo;
        private readonly IRepository<StatusMailDate> _statusMailDate;
        private readonly IXmlAndJsonConvertor _xmlandjsonConvertorAppService;
        private readonly IRepository<UserDefaultInformation> _userdefaultInformationRepo;
        private readonly IRepository<WorkDay> _workDayRepo;
        private readonly IRepository<IncentiveCategory> _incentiveCategoryRepo;
        private readonly IRepository<IncentiveTag> _incentiveTagRepo;
        private readonly IRepository<IncentiveVsEntity> _incentivevsentityRepo;
        private readonly IRepository<EmployeeVsIncentive> _employeeVsIncentiveRepo;
        private readonly IRepository<PublicHoliday> _publicHolidayRepo;
        private readonly IRepository<SalaryInfo> _salaryInfoRepo;
        private readonly IRepository<ManualIncentive> _manualIncentiveRepo;
        private readonly IRepository<GpsAttendance> _gpsAttendanceRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IRepository<Unit> _unitRepo;
        private readonly IRepository<LeaveType> _leaveTypeRepo;
        private readonly IRepository<BioMetricExcludedEntry> _biometricExcludedRepo;
        private readonly SettingManager _settingManager;
        private readonly IRepository<EmployeeMailMessage> _employeeMailMessage;
        private readonly IAttendanceLogAppService _attendanceLogAppService;
        private readonly IRepository<DocumentForSkillSet> _documentForSkillSetRepo;
        private readonly IRepository<EmployeeDocumentInfo> _employeeDocumentinfoRepo;
        private readonly IRepository<DocumentInfo> _documentinfoRepo;
        private readonly ISalaryInfoAppService _salaryinfoappService;
        private readonly IDutyChartAppService _dutyChartAppService;
        private readonly IRepository<EmployeeResidentStatus> _employeeResidentStatusRepo;
        private readonly IRepository<EmployeeRace> _employeeRaceStatusRepo;
        private readonly IRepository<EmployeeCostCentreMaster> _employeeCostCentreRepo;
        private readonly IRepository<EmployeeReligion> _employeeReligionStatusRepo;
        private readonly IRepository<SalaryPaidMaster> _salaryPaidMasterRepo;
        private readonly IRepository<SalaryPaidDetail> _salaryPaidDetailRepo;
        private readonly IRepository<DcHeadMaster> _dcHeadMasterRepo;
        private readonly IRepository<DcGroupMaster> _dcGroupMasterRepo;
        private readonly IRepository<DcStationMaster> _dcstationmasterRepo;
        private readonly IRepository<DcShiftMaster> _dcshiftmasterRepo;

        int scaleForDecimalRound = 2;

        public TimeSheetMasterAppService(
            ITimeSheetMasterManager timesheetmasterManager,
            IRepository<TimeSheetMaster> timeSheetMasterRepo,
            IRepository<TimeSheetDetail> timesheetdetailRepo,
            IRepository<TimeSheetMaterialUsage> timesheetmaterialusageRepo,
            IRepository<TimeSheetEmployeeDetail> timesheetemployeedetailRepo,
            IRepository<PersonalInformation> personalinformationRepo,
            IRepository<DutyChart> dutyChartRepo,
            IRepository<DutyChartDetail> dutyChartDetailRepo,
            IRepository<DcShiftMaster> shiftMasterRepo,
            ITimeSheetMasterListExcelExporter timesheetmasterExporter,
            IRepository<EmployeeSkillSet> employeeskillsetRepo,
            IRepository<LeaveRequest> leaveRequestRepo,
            IRepository<SkillSet> skillsetRepo,
            IRepository<Company> companyRepo,
            IRepository<AttendanceLog> attendanceLogRepo,
            IRepository<StatusMailDate> statusMailDate,
            IXmlAndJsonConvertor xmlandjsonConvertorAppService,
            IRepository<UserDefaultInformation> userdefaultInformationRepo,
            IRepository<WorkDay> workDayRepo,
            IRepository<EmployeeVsIncentive> employeeVsIncentiveRepo,
            IRepository<IncentiveCategory> incentiveCategoryRepo,
            IRepository<IncentiveTag> incentiveTagRepo,
            IRepository<IncentiveVsEntity> incentivevsentityRepo,
            IRepository<PublicHoliday> publicHolidayRepo,
            IRepository<SalaryInfo> salaryInfoRepo,
            IRepository<ManualIncentive> manualIncentiveRepo,
            IRepository<AttendanceMachineLocation> attendanceMachineLocationRepo,
            IRepository<GpsAttendance> gpsAttendanceRepo,
            IRepository<Material> materialRepo,
            IRepository<Unit> unitRepo,
            IRepository<LeaveType> leaveType,
            IRepository<BioMetricExcludedEntry> biometricExcludedRepo,
            SettingManager settingManager,
            IRepository<EmployeeMailMessage> employeeMailMessage,
            IAttendanceLogAppService attendanceLogAppService,
            IRepository<DocumentForSkillSet> documentForSkillSetRepo,
            IRepository<EmployeeDocumentInfo> employeeDocumentinfoRepo,
            IRepository<DocumentInfo> documentinfoRepo,
            ISalaryInfoAppService salaryinfoappService,
            IRepository<MonthWiseWorkDay> monthwiseworkDayRepo,
            IDutyChartAppService dutyChartAppService,
            IRepository<EmployeeResidentStatus> employeeResidentStatusRepo,
            IRepository<EmployeeRace> employeeRaceStatusRepo,
            IRepository<EmployeeCostCentreMaster> employeeCostCentreRepo,
            IRepository<EmployeeReligion> employeeReligionStatusRepo,
            IRepository<SalaryPaidMaster> salaryPaidMasterRepo,
            IRepository<SalaryPaidDetail> salaryPaidDetailRepo,
            IRepository<DcHeadMaster> dcHeadMasterRepo,
            IRepository<DcGroupMaster> dcGroupMasterRepo,
            IRepository<DcStationMaster> dcstationmasterRepo,
            IRepository<DcShiftMaster> dcshiftmasterRepo,
            IRepository<Location> locationRepo
            )
        {
            _dcshiftmasterRepo = dcshiftmasterRepo;
            _dcstationmasterRepo = dcstationmasterRepo;
            _leaveRequestRepo = leaveRequestRepo;
            _employeeskillsetRepo = employeeskillsetRepo;
            _skillsetRepo = skillsetRepo;
            _timesheetmasterManager = timesheetmasterManager;
            _timesheetmasterRepo = timeSheetMasterRepo;
            _timesheetmasterExporter = timesheetmasterExporter;
            _shiftMasterRepo = shiftMasterRepo;
            _timesheetdetailRepo = timesheetdetailRepo;
            _timesheetmaterialusageRepo = timesheetmaterialusageRepo;
            _timesheetemployeedetailRepo = timesheetemployeedetailRepo;
            _personalinformationRepo = personalinformationRepo;
            _dutyChartRepo = dutyChartRepo;
            _dutyChartDetailRepo = dutyChartDetailRepo;
            _companyRepo = companyRepo;
            _attendanceLogRepo = attendanceLogRepo;
            _statusMailDate = statusMailDate;
            _xmlandjsonConvertorAppService = xmlandjsonConvertorAppService;
            _userdefaultInformationRepo = userdefaultInformationRepo;
            _workDayRepo = workDayRepo;
            _employeeVsIncentiveRepo = employeeVsIncentiveRepo;
            _incentiveTagRepo = incentiveTagRepo;
            _incentiveCategoryRepo = incentiveCategoryRepo;
            _incentivevsentityRepo = incentivevsentityRepo;
            _publicHolidayRepo = publicHolidayRepo;
            _salaryInfoRepo = salaryInfoRepo;
            _manualIncentiveRepo = manualIncentiveRepo;
            _attendanceMachineLocationRepo = attendanceMachineLocationRepo;
            _gpsAttendanceRepo = gpsAttendanceRepo;
            _materialRepo = materialRepo;
            _unitRepo = unitRepo;
            _leaveTypeRepo = leaveType;
            _biometricExcludedRepo = biometricExcludedRepo;
            _settingManager = settingManager;
            _employeeMailMessage = employeeMailMessage;
            _attendanceLogAppService = attendanceLogAppService;
            _documentForSkillSetRepo = documentForSkillSetRepo;
            _employeeDocumentinfoRepo = employeeDocumentinfoRepo;
            _documentinfoRepo = documentinfoRepo;
            _salaryinfoappService = salaryinfoappService;
            _dutyChartAppService = dutyChartAppService;
            _employeeResidentStatusRepo = employeeResidentStatusRepo;
            _employeeRaceStatusRepo = employeeRaceStatusRepo;
            _employeeCostCentreRepo = employeeCostCentreRepo;
            _employeeReligionStatusRepo = employeeReligionStatusRepo;
            _salaryPaidMasterRepo = salaryPaidMasterRepo;
            _salaryPaidDetailRepo = salaryPaidDetailRepo;
            _locationRepo = locationRepo;
            _dcHeadMasterRepo = dcHeadMasterRepo;
            _dcGroupMasterRepo = dcGroupMasterRepo;
        }



        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _timesheetmasterRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<TimeSheetMasterListDto>>();
            return _timesheetmasterExporter.ExportToFile(allListDtos);
        }





        public async Task<FileDto> GetDutyChartExcelBasedOnFilter(ExcelDutyChartExporter input)
        {
            return _timesheetmasterExporter.GetDutyChartExcelBasedOnFilter(input);
        }


        public async Task<ListResultOutput<TimeSheetMasterListDto>> GetManualTsNumbers()
        {
            var lstTimeSheetMaster = await _timesheetmasterRepo.GetAll().ToListAsync();
            return new ListResultOutput<TimeSheetMasterListDto>(lstTimeSheetMaster.MapTo<List<TimeSheetMasterListDto>>());
        }


        public async Task<TimeSheetAlreadyExist> GetTimeSheetExist(InputTimeSheetNumberWithAcYear input)
        {
            TimeSheetAlreadyExist output = new TimeSheetAlreadyExist
            {
                ExistFlag = false,
                ErrorMessage = ""
            };

            var existRecord = await _timesheetmasterRepo.GetAll().Where(a => a.CompanyRefId == input.CompanyRefId && a.AcYear == input.AcYear
                    && a.ManualTsNumber == input.ManualTsNumber).ToListAsync();

            if (existRecord.Count > 0 && existRecord != null)
            {
                output.ExistFlag = true;
                output.ErrorMessage = L("AcYear") + " " + existRecord[0].AcYear + " " + L("Date") + " " + existRecord[0].TsDateFrom.ToString("dd-MMM-yy") + " Entered On" + existRecord[0].CreationTime.ToString("dd-MMM-yy");
                return output;
            }
            else
            {
                return output;
            }
        }

        public async Task<DateTime?> SetYellowTimeSheetSubmission(TsUpdateSubmissionDto input)
        {
            var dto = await _timesheetmasterRepo.GetAsync(input.Id);

            dto.SubmissionDate = input.SubmissionDate;

            await _timesheetmasterRepo.UpdateAsync(dto);

            return dto.SubmissionDate;
        }

        public async Task<DateTime?> SetPinkTimeSheetSubmission(TsUpdateSubmissionDto input)
        {
            var dto = await _timesheetmasterRepo.GetAsync(input.Id);

            dto.SubmissionDateToInvoiceDepartment = input.SubmissionDateToInvoiceDepartment;

            await _timesheetmasterRepo.UpdateAsync(dto);

            return dto.SubmissionDateToInvoiceDepartment;
        }

        public async Task<PieChartOutputDto> GetDashBoardDutyChartData(GetDashBoardDto input)
        {
            PieChartOutputDto result = new PieChartOutputDto();

            var maxValue = await _dutyChartRepo.GetAll().Where(t => t.Status.Equals("DayClosed")).OrderByDescending(t => t.DutyChartDate).FirstOrDefaultAsync();

            DateTime maxCloseDay;
            if (maxValue != null)
                maxCloseDay = maxValue.DutyChartDate; // maxValue.DutyChartDate.AddDays(1);
            else
            {
                maxValue = await _dutyChartRepo.GetAll().OrderByDescending(t => t.DutyChartDate).FirstOrDefaultAsync();
                if (maxValue != null)
                    maxCloseDay = maxValue.DutyChartDate;
                else
                    maxCloseDay = DateTime.Today;
            }

            if (input.EndDate > maxCloseDay || input.EndDate == null)
            {
                input.EndDate = maxCloseDay;
            }

            if (input.StartDate == null)
            {
                //var dutyCharts = _dutyChartRepo.GetAll().Where(t => !t.Status.Equals("Draft"));
                var dutyCharts = _dutyChartRepo.GetAll().Where(t => t.Status.Equals("DayClosed"));


                DateTime maxDutyChartDate;

                if (dutyCharts.Count() == 0)
                {
                    input.StartDate = DateTime.Today.AddDays(-1);
                    input.EndDate = DateTime.Today.AddDays(-1);
                }
                else
                {
                    maxDutyChartDate = dutyCharts.Max(t => t.DutyChartDate);
                    input.StartDate = maxDutyChartDate;
                    input.EndDate = maxDutyChartDate;
                }
            }

            result.Header = input.StartDate.Value.ToString("yyyy-MMM-dd") + " - " + input.EndDate.Value.ToString("yyyy-MMM-dd");
            result.StartDate = input.StartDate.Value;
            result.EndDate = input.EndDate.Value;

            var dutyResults = await _dutyChartDetailRepo.GetAll().Where(t => t.DutyChartDate >= input.StartDate && t.DutyChartDate <= input.EndDate).ToListAsync();

            var rsPersonalInformation = await _personalinformationRepo.GetAllListAsync();
            var rsCompany = await _companyRepo.GetAllListAsync();
            var rsLocation = await _locationRepo.GetAllListAsync();
            var rsEmployeeResidentStatus = await _employeeResidentStatusRepo.GetAllListAsync();
            var rsEmployeeCostCentres = await _employeeCostCentreRepo.GetAllListAsync();

            var dutyChartDetails = dutyResults.MapTo<List<DutyChartDetailListDto>>();

            foreach (var lst in dutyChartDetails)
            {
                var emp = rsPersonalInformation.FirstOrDefault(t => t.Id == lst.EmployeeRefId);
                var loc = rsLocation.FirstOrDefault(t => t.Id == emp.LocationRefId);
                var company = rsCompany.FirstOrDefault(t => t.Id == loc.CompanyRefId);

                lst.CompanyRefId = loc.CompanyRefId;
                lst.CompanyRefCode = company.Code;
                lst.CompanyPrefix = company.Code;


                if (emp.EmployeeResidentStatusRefId > 0)
                {
                    var resident = rsEmployeeResidentStatus.FirstOrDefault(t => t.Id == emp.EmployeeResidentStatusRefId);
                    if (resident != null)
                    {
                        lst.ResidentStatus = resident.ResidentStatus;
                        lst.WorkStatus = resident.ResidentStatus;
                    }
                }

                if (emp.EmployeeCostCentreRefId > 0)
                {
                    var cc = rsEmployeeCostCentres.FirstOrDefault(t => t.Id == emp.EmployeeCostCentreRefId);
                    if (cc != null)
                    {
                        lst.EmployeeCostCentre = cc.CostCentre;
                    }
                }

                lst.CompanyRefCode = company.Code;

                if (lst.AllottedStatus == L("Job"))
                {
                    if (emp == null)
                    {
                        throw new UserFriendlyException(L("NotExistForId", "EMPLOYEE", lst.EmployeeRefId));
                    }

                    lst.AllottedStatus = lst.EmployeeCostCentre;

                    //if (lst.WorkLocation == L("Office"))
                    //{
                    //    var emp = rsPersonalInformation.FirstOrDefault(t => t.Id == lst.EmployeeRefId);
                    //    if (emp==null)
                    //    {
                    //        throw new UserFriendlyException(L("NotExistForId", "EMPLOYEE", lst.EmployeeRefId));
                    //    }
                    //    lst.AllottedStatus = emp.CostCentre;
                    //    //lst.AllottedStatus = L("OfficeJob");
                    //}
                    //else
                    //    lst.AllottedStatus = L("OutsideJobNotBillable");
                }
                else if (lst.AllottedStatus != "Job")
                {
                    if (!emp.StaffType.Equals("Permanent"))
                        lst.AllottedStatus = "OMIT";
                }
            }

            var strComp = L("WeekOff");
            dutyChartDetails = dutyChartDetails.Where(t => !t.AllottedStatus.Equals(strComp)).ToList();

            strComp = "OMIT";
            dutyChartDetails = dutyChartDetails.Where(t => !t.AllottedStatus.Equals(strComp)).ToList();

            var chartData = dutyChartDetails.GroupBy(t => t.AllottedStatus).Select(t => new PieChartData
            {
                name = t.Key,
                y = t.Count()
            }).ToList();

            foreach (var chart in chartData)
            {
                if (chart.name == L("Billable"))
                    chart.Color = "YELLOW";
                else if (chart.name == L("Idle"))
                    chart.Color = "RED";
                else if (chart.name == L("WeekOff"))
                    chart.Color = "GREEN";
                else if (chart.name == L("Leave"))
                    chart.Color = "CYAN";
                else if (chart.name == L("ADMIN"))
                    chart.Color = "BLUE";
                else if (chart.name == L("OPERATION"))
                    chart.Color = "PURPLE";
            }

            #region IdleList
            string idleString = L("Idle");

            var skillSet = await _skillsetRepo.GetAll().ToListAsync();
            var rsEmpSkillSet = await _employeeskillsetRepo.GetAllListAsync();


            var idleList = (from dc in dutyChartDetails.Where(t => t.AllottedStatus.Equals(idleString))
                            join pi in rsPersonalInformation on dc.EmployeeRefId equals pi.Id
                            select new EmployeeWorkStatusDatesDto
                            {
                                EmployeeRefId = dc.EmployeeRefId,
                                EmployeeRefName = pi.EmployeeName,
                                SelectedDate = dc.DutyChartDate,
                                CompanyRefId = dc.CompanyRefId,
                                CompanyRefCode = dc.CompanyRefCode
                            }
                            ).ToList().OrderBy(t => t.SelectedDate);

            var groupOutput = (from lst in idleList
                               group lst by new
                               {
                                   lst.CompanyRefId,
                                   lst.CompanyRefCode,
                                   lst.EmployeeRefId,
                                   lst.EmployeeRefName
                               } into g
                               select new EmployeeWorkStatusDatesDto
                               {
                                   CompanyRefId = g.Key.CompanyRefId,
                                   CompanyRefCode = g.Key.CompanyRefCode,
                                   EmployeeRefId = g.Key.EmployeeRefId,
                                   EmployeeRefName = g.Key.EmployeeRefName,
                                   StatusDates = g.Select(t => t.SelectedDate).ToArray().MapTo<List<DateTime>>(),
                                   NoOfDays = g.Select(t => t.SelectedDate).ToArray().Count()
                               }).ToList();

            foreach (var emp in groupOutput)
            {
                var per = rsPersonalInformation.FirstOrDefault(t => t.Id == emp.EmployeeRefId);
                if (per == null)
                {
                    throw new UserFriendlyException(L("NotExistForId", "PersonalInformation", emp.EmployeeRefId));
                }

                var empskills = rsEmpSkillSet.Where(t => t.EmployeeRefId == emp.EmployeeRefId);

                List<SkillSetEditDto> empSkillSet = new List<SkillSetEditDto>();
                string employeeSkills = "";
                foreach (var sk in empskills)
                {
                    SkillSetEditDto ns = new SkillSetEditDto();
                    var locskill = skillSet.FirstOrDefault(t => t.Id == sk.SkillSetRefId);
                    if (locskill == null)
                    {
                        throw new UserFriendlyException(L("SkillSetMismatch"), emp.EmployeeRefName);
                    }
                    ns.SkillCode = locskill.SkillCode;
                    ns.Id = locskill.Id;
                    employeeSkills = employeeSkills + locskill.SkillCode + ",";
                    empSkillSet.Add(ns);
                }
                employeeSkills = employeeSkills.Left(employeeSkills.Length - 1);
                emp.EmployeeSkills = employeeSkills;
                var skill = skillSet.FirstOrDefault(t => t.Id == per.DefaultSkillRefId);
                emp.DefaultSkillSetCode = skill.SkillCode;
                {
                    var resident = rsEmployeeResidentStatus.FirstOrDefault(t => t.Id == per.EmployeeResidentStatusRefId);
                    if (resident != null)
                    {
                        emp.WorkStatus = resident.ResidentStatus;
                    }
                }
            }

            result.EmployeeIdleList = groupOutput.OrderByDescending(t => t.NoOfDays).ToList();
            result.EmployeeIdleList = groupOutput.OrderBy(t => t.CompanyRefId).ToList();
            #endregion

            #region OutsideJobNotBillableList
            string oprNotBillableString = L("OPERATION");

            var oprNotBillableStringList = (from dc in dutyChartDetails.Where(t => t.AllottedStatus.Equals(oprNotBillableString))
                                            join pi in rsPersonalInformation on dc.EmployeeRefId equals pi.Id
                                            select new EmployeeWorkStatusDatesDto
                                            {
                                                EmployeeRefId = dc.EmployeeRefId,
                                                EmployeeRefName = pi.EmployeeName,
                                                SelectedDate = dc.DutyChartDate
                                            }
                            ).ToList().OrderBy(t => t.SelectedDate);

            var groupOprNotBillalbeOutput = (from lst in oprNotBillableStringList
                                             group lst by new { lst.EmployeeRefId, lst.EmployeeRefName } into g
                                             select new EmployeeWorkStatusDatesDto
                                             {
                                                 EmployeeRefId = g.Key.EmployeeRefId,
                                                 EmployeeRefName = g.Key.EmployeeRefName,
                                                 StatusDates = g.Select(t => t.SelectedDate).ToArray().MapTo<List<DateTime>>(),
                                                 NoOfDays = g.Select(t => t.SelectedDate).ToArray().Count()
                                             }).ToList();

            foreach (var emp in groupOprNotBillalbeOutput)
            {
                var per = rsPersonalInformation.FirstOrDefault(t => t.Id == emp.EmployeeRefId);
                if (per == null)
                {
                    throw new UserFriendlyException(L("NotExistForId", "PersonalInformation", emp.EmployeeRefId));
                }

                var empskills = rsEmpSkillSet.Where(t => t.EmployeeRefId == emp.EmployeeRefId);

                List<SkillSetEditDto> empSkillSet = new List<SkillSetEditDto>();
                string employeeSkills = "";
                foreach (var sk in empskills)
                {
                    SkillSetEditDto ns = new SkillSetEditDto();
                    var locskill = skillSet.FirstOrDefault(t => t.Id == sk.SkillSetRefId);
                    if (locskill == null)
                    {
                        throw new UserFriendlyException(L("SkillSetMismatch"), emp.EmployeeRefName);
                    }
                    ns.SkillCode = locskill.SkillCode;
                    ns.Id = locskill.Id;
                    employeeSkills = employeeSkills + locskill.SkillCode + ",";
                    empSkillSet.Add(ns);
                }
                employeeSkills = employeeSkills.Left(employeeSkills.Length - 1);
                emp.EmployeeSkills = employeeSkills;
                var skill = skillSet.FirstOrDefault(t => t.Id == per.DefaultSkillRefId);
                emp.DefaultSkillSetCode = skill.SkillCode;
                if (per.EmployeeResidentStatusRefId > 0)
                {
                    var resident = rsEmployeeResidentStatus.FirstOrDefault(t => t.Id == per.EmployeeResidentStatusRefId);
                    if (resident != null)
                    {
                        emp.WorkStatus = resident.ResidentStatus;
                    }
                }
            }
            result.EmployeeOprNotBillableList = groupOprNotBillalbeOutput.OrderByDescending(t => t.NoOfDays).ToList();
            #endregion

            #region SnrMgmt
            string snrString = L("SNR-MGMT");

            var snrMgmtList = (from dc in dutyChartDetails.Where(t => t.AllottedStatus.Equals(snrString))
                               join pi in rsPersonalInformation on dc.EmployeeRefId equals pi.Id
                               select new EmployeeWorkStatusDatesDto
                               {
                                   EmployeeRefId = dc.EmployeeRefId,
                                   EmployeeRefName = pi.EmployeeName,
                                   SelectedDate = dc.DutyChartDate,
                               }
                                     ).ToList().OrderBy(t => t.SelectedDate);

            var groupsnrListOutput = (from lst in snrMgmtList
                                      group lst by new { lst.EmployeeRefId, lst.EmployeeRefName } into g
                                      select new EmployeeWorkStatusDatesDto
                                      {
                                          EmployeeRefId = g.Key.EmployeeRefId,
                                          EmployeeRefName = g.Key.EmployeeRefName,
                                          StatusDates = g.Select(t => t.SelectedDate).ToArray().MapTo<List<DateTime>>(),
                                          NoOfDays = g.Select(t => t.SelectedDate).ToArray().Count()
                                      }).ToList();

            foreach (var emp in groupsnrListOutput)
            {
                var per = rsPersonalInformation.FirstOrDefault(t => t.Id == emp.EmployeeRefId);
                if (per == null)
                {
                    throw new UserFriendlyException(L("NotExistForId", "PersonalInformation", emp.EmployeeRefId));
                }

                var empskills = rsEmpSkillSet.Where(t => t.EmployeeRefId == emp.EmployeeRefId);

                List<SkillSetEditDto> empSkillSet = new List<SkillSetEditDto>();
                string employeeSkills = "";
                foreach (var sk in empskills)
                {
                    SkillSetEditDto ns = new SkillSetEditDto();
                    var locskill = skillSet.FirstOrDefault(t => t.Id == sk.SkillSetRefId);
                    if (locskill == null)
                    {
                        throw new UserFriendlyException(L("SkillSetMismatch"), emp.EmployeeRefName);
                    }
                    ns.SkillCode = locskill.SkillCode;
                    ns.Id = locskill.Id;
                    employeeSkills = employeeSkills + locskill.SkillCode + ",";
                    empSkillSet.Add(ns);
                }
                employeeSkills = employeeSkills.Left(employeeSkills.Length - 1);
                emp.EmployeeSkills = employeeSkills;
                var skill = skillSet.FirstOrDefault(t => t.Id == per.DefaultSkillRefId);
                emp.DefaultSkillSetCode = skill.SkillCode;
                {
                    var resident = rsEmployeeResidentStatus.FirstOrDefault(t => t.Id == per.EmployeeResidentStatusRefId);
                    if (resident != null)
                    {
                        emp.WorkStatus = resident.ResidentStatus;
                    }
                }
            }
            result.EmployeeSnrMgmtList = groupsnrListOutput.OrderByDescending(t => t.NoOfDays).OrderBy(t => t.EmployeeRefName).ToList();
            #endregion

            #region TransportList
            string transportString = L("TRANSPORT");

            var transportList = (from dc in dutyChartDetails.Where(t => t.AllottedStatus.Equals(transportString))
                                 join pi in rsPersonalInformation on dc.EmployeeRefId equals pi.Id
                                 select new EmployeeWorkStatusDatesDto
                                 {
                                     EmployeeRefId = dc.EmployeeRefId,
                                     EmployeeRefName = pi.EmployeeName,
                                     SelectedDate = dc.DutyChartDate
                                 }
                                     ).ToList().OrderBy(t => t.SelectedDate);

            var grouptransportListOutput = (from lst in transportList
                                            group lst by new { lst.EmployeeRefId, lst.EmployeeRefName } into g
                                            select new EmployeeWorkStatusDatesDto
                                            {
                                                EmployeeRefId = g.Key.EmployeeRefId,
                                                EmployeeRefName = g.Key.EmployeeRefName,
                                                StatusDates = g.Select(t => t.SelectedDate).ToArray().MapTo<List<DateTime>>(),
                                                NoOfDays = g.Select(t => t.SelectedDate).ToArray().Count()
                                            }).ToList();

            foreach (var emp in grouptransportListOutput)
            {
                var per = rsPersonalInformation.FirstOrDefault(t => t.Id == emp.EmployeeRefId);
                if (per == null)
                {
                    throw new UserFriendlyException(L("NotExistForId", "PersonalInformation", emp.EmployeeRefId));
                }

                var empskills = rsEmpSkillSet.Where(t => t.EmployeeRefId == emp.EmployeeRefId);

                List<SkillSetEditDto> empSkillSet = new List<SkillSetEditDto>();
                string employeeSkills = "";
                foreach (var sk in empskills)
                {
                    SkillSetEditDto ns = new SkillSetEditDto();
                    var locskill = skillSet.FirstOrDefault(t => t.Id == sk.SkillSetRefId);
                    if (locskill == null)
                    {
                        throw new UserFriendlyException(L("SkillSetMismatch"), emp.EmployeeRefName);
                    }
                    ns.SkillCode = locskill.SkillCode;
                    ns.Id = locskill.Id;
                    employeeSkills = employeeSkills + locskill.SkillCode + ",";
                    empSkillSet.Add(ns);
                }
                employeeSkills = employeeSkills.Left(employeeSkills.Length - 1);
                emp.EmployeeSkills = employeeSkills;
                var skill = skillSet.FirstOrDefault(t => t.Id == per.DefaultSkillRefId);
                emp.DefaultSkillSetCode = skill.SkillCode;
                {
                    var resident = rsEmployeeResidentStatus.FirstOrDefault(t => t.Id == per.EmployeeResidentStatusRefId);
                    if (resident != null)
                    {
                        emp.WorkStatus = resident.ResidentStatus;
                    }
                }

            }
            result.EmployeeTransportList = grouptransportListOutput.OrderByDescending(t => t.NoOfDays).ToList();
            #endregion

            #region OfficeList
            string officeString = L("ADMIN");

            var officeList = (from dc in dutyChartDetails.Where(t => t.AllottedStatus.Equals(officeString))
                              join pi in rsPersonalInformation on dc.EmployeeRefId equals pi.Id
                              select new EmployeeWorkStatusDatesDto
                              {
                                  EmployeeRefId = dc.EmployeeRefId,
                                  EmployeeRefName = pi.EmployeeName,
                                  SelectedDate = dc.DutyChartDate
                              }
                                     ).ToList().OrderBy(t => t.SelectedDate);

            var groupofficeListOutput = (from lst in officeList
                                         group lst by new { lst.EmployeeRefId, lst.EmployeeRefName } into g
                                         select new EmployeeWorkStatusDatesDto
                                         {
                                             EmployeeRefId = g.Key.EmployeeRefId,
                                             EmployeeRefName = g.Key.EmployeeRefName,
                                             StatusDates = g.Select(t => t.SelectedDate).ToArray().MapTo<List<DateTime>>(),
                                             NoOfDays = g.Select(t => t.SelectedDate).ToArray().Count()
                                         }).ToList();

            foreach (var emp in groupofficeListOutput)
            {
                var per = rsPersonalInformation.FirstOrDefault(t => t.Id == emp.EmployeeRefId);
                if (per == null)
                {
                    throw new UserFriendlyException(L("NotExistForId", "PersonalInformation", emp.EmployeeRefId));
                }

                var empskills = rsEmpSkillSet.Where(t => t.EmployeeRefId == emp.EmployeeRefId);

                List<SkillSetEditDto> empSkillSet = new List<SkillSetEditDto>();
                string employeeSkills = "";
                foreach (var sk in empskills)
                {
                    SkillSetEditDto ns = new SkillSetEditDto();
                    var locskill = skillSet.FirstOrDefault(t => t.Id == sk.SkillSetRefId);
                    if (locskill == null)
                    {
                        throw new UserFriendlyException(L("SkillSetMismatch"), emp.EmployeeRefName);
                    }
                    ns.SkillCode = locskill.SkillCode;
                    ns.Id = locskill.Id;
                    employeeSkills = employeeSkills + locskill.SkillCode + ",";
                    empSkillSet.Add(ns);
                }
                employeeSkills = employeeSkills.Left(employeeSkills.Length - 1);
                emp.EmployeeSkills = employeeSkills;
                var skill = skillSet.FirstOrDefault(t => t.Id == per.DefaultSkillRefId);
                emp.DefaultSkillSetCode = skill.SkillCode;
                {
                    var resident = rsEmployeeResidentStatus.FirstOrDefault(t => t.Id == per.EmployeeResidentStatusRefId);
                    if (resident != null)
                    {
                        emp.WorkStatus = resident.ResidentStatus;
                    }
                }

            }
            result.EmployeeOfficeList = groupofficeListOutput.OrderByDescending(t => t.NoOfDays).ToList();
            #endregion

            #region AbsentList

            //  Absent List
            string statusString = L("Absent");

            var absentList = (from dc in dutyChartDetails.Where(t => t.AllottedStatus.Equals(statusString))
                              join pi in rsPersonalInformation on dc.EmployeeRefId equals pi.Id
                              select new EmployeeWorkStatusDatesDto
                              {
                                  EmployeeRefId = dc.EmployeeRefId,
                                  EmployeeRefName = pi.EmployeeName,
                                  SelectedDate = dc.DutyChartDate
                              }
                            ).ToList().OrderBy(t => t.SelectedDate);

            var groupAbsentOutput = (from lst in absentList
                                     group lst by new { lst.EmployeeRefId, lst.EmployeeRefName } into g
                                     select new EmployeeWorkStatusDatesDto
                                     {
                                         EmployeeRefId = g.Key.EmployeeRefId,
                                         EmployeeRefName = g.Key.EmployeeRefName,
                                         StatusDates = g.Select(t => t.SelectedDate).ToArray().MapTo<List<DateTime>>(),
                                         NoOfDays = g.Select(t => t.SelectedDate).ToArray().Count()
                                     }).ToList();

            foreach (var emp in groupAbsentOutput)
            {
                var per = rsPersonalInformation.FirstOrDefault(t => t.Id == emp.EmployeeRefId);
                if (per == null)
                {
                    throw new UserFriendlyException(L("NotExistForId", "PersonalInformation", emp.EmployeeRefId));
                }

                var empskills = rsEmpSkillSet.Where(t => t.EmployeeRefId == emp.EmployeeRefId);

                List<SkillSetEditDto> empSkillSet = new List<SkillSetEditDto>();
                string employeeSkills = "";
                foreach (var sk in empskills)
                {
                    SkillSetEditDto ns = new SkillSetEditDto();
                    var locskill = skillSet.FirstOrDefault(t => t.Id == sk.SkillSetRefId);
                    if (locskill == null)
                    {
                        throw new UserFriendlyException(L("SkillSetMismatch"), emp.EmployeeRefName);
                    }
                    ns.SkillCode = locskill.SkillCode;
                    ns.Id = locskill.Id;
                    employeeSkills = employeeSkills + locskill.SkillCode + ",";
                    empSkillSet.Add(ns);
                }
                employeeSkills = employeeSkills.Left(employeeSkills.Length - 1);
                emp.EmployeeSkills = employeeSkills;
                var skill = skillSet.FirstOrDefault(t => t.Id == per.DefaultSkillRefId);
                emp.DefaultSkillSetCode = skill.SkillCode;
                {
                    var resident = rsEmployeeResidentStatus.FirstOrDefault(t => t.Id == per.EmployeeResidentStatusRefId);
                    if (resident != null)
                    {
                        emp.WorkStatus = resident.ResidentStatus;
                    }
                }
            }

            result.EmployeeAbsentList = groupAbsentOutput.OrderByDescending(t => t.NoOfDays).ToList();

            #endregion

            #region JobList

            //  Job List
            statusString = L("Billable");

            var jobList = (from dc in dutyChartDetails.Where(t => t.AllottedStatus.Equals(statusString))
                           join pi in rsPersonalInformation on dc.EmployeeRefId equals pi.Id
                           select new EmployeeWorkStatusDatesDto
                           {
                               EmployeeRefId = dc.EmployeeRefId,
                               EmployeeRefName = pi.EmployeeName,
                               SelectedDate = dc.DutyChartDate,
                               CompanyRefId = dc.CompanyRefId,
                               CompanyRefCode = dc.CompanyRefCode
                           }
                            ).ToList().OrderBy(t => t.SelectedDate);

            var groupJobOutput = (from lst in jobList
                                  group lst by new
                                  {
                                      lst.CompanyRefId,
                                      lst.CompanyRefCode,
                                      lst.EmployeeRefId,
                                      lst.EmployeeRefName
                                  } into g
                                  select new EmployeeWorkStatusDatesDto
                                  {
                                      CompanyRefId = g.Key.CompanyRefId,
                                      CompanyRefCode = g.Key.CompanyRefCode,
                                      EmployeeRefId = g.Key.EmployeeRefId,
                                      EmployeeRefName = g.Key.EmployeeRefName,
                                      StatusDates = g.Select(t => t.SelectedDate).ToArray().MapTo<List<DateTime>>(),
                                      NoOfDays = g.Select(t => t.SelectedDate).ToArray().Count()
                                  }).ToList();

            bool remarksFlag = false;
            result.RemarksFlag = remarksFlag;

            foreach (var emp in groupJobOutput)
            {
                var per = rsPersonalInformation.FirstOrDefault(t => t.Id == emp.EmployeeRefId);
                if (per == null)
                {
                    throw new UserFriendlyException(L("NotExistForId", "PersonalInformation", emp.EmployeeRefId));
                }

                var empskills = rsEmpSkillSet.Where(t => t.EmployeeRefId == emp.EmployeeRefId);

                List<SkillSetEditDto> empSkillSet = new List<SkillSetEditDto>();
                string employeeSkills = "";
                foreach (var sk in empskills)
                {
                    SkillSetEditDto ns = new SkillSetEditDto();
                    var locskill = skillSet.FirstOrDefault(t => t.Id == sk.SkillSetRefId);
                    if (locskill == null)
                    {
                        throw new UserFriendlyException(L("SkillSetMismatch"), emp.EmployeeRefName);
                    }
                    ns.SkillCode = locskill.SkillCode;
                    ns.Id = locskill.Id;
                    employeeSkills = employeeSkills + locskill.SkillCode + ",";


                    empSkillSet.Add(ns);
                }
                employeeSkills = employeeSkills.Left(employeeSkills.Length - 1);
                emp.EmployeeSkills = employeeSkills;
                var skill = skillSet.FirstOrDefault(t => t.Id == per.DefaultSkillRefId);
                emp.DefaultSkillSetCode = skill.SkillCode;
                {
                    var resident = rsEmployeeResidentStatus.FirstOrDefault(t => t.Id == per.EmployeeResidentStatusRefId);
                    if (resident != null)
                    {
                        emp.WorkStatus = resident.ResidentStatus;
                    }
                }

                //if (remarksFlag)
                //{
                //    var duty = dutyChartDetails.FirstOrDefault(t => t.EmployeeRefId == emp.EmployeeRefId);
                //    if (duty != null)
                //    {
                //        var costCentre = rsProjCostCentre.FirstOrDefault(t => t.Id == duty.);
                //        if (costCentre != null)
                //        {
                //            emp.Remarks = costCentre.CostCentreCode;
                //            emp.RemarksFlag = true;
                //        }
                //    }
                //}

                //emp.Skills = empSkillSet;

            }

            result.EmployeeJobList = groupJobOutput.OrderByDescending(t => t.NoOfDays).ToList();
            result.EmployeeJobList = groupJobOutput.OrderBy(t => t.CompanyRefId).ToList();
            #endregion

            #region LeaveList
            //  Leave List
            statusString = L("Leave");

            var leaveList = (from dc in dutyChartDetails.Where(t => t.AllottedStatus.Equals(statusString))
                             join pi in rsPersonalInformation on dc.EmployeeRefId equals pi.Id
                             select new EmployeeWorkStatusDatesDto
                             {
                                 EmployeeRefId = dc.EmployeeRefId,
                                 EmployeeRefName = pi.EmployeeName,
                                 SelectedDate = dc.DutyChartDate
                             }
                            ).ToList().OrderBy(t => t.SelectedDate);

            var groupLeaveOutput = (from lst in leaveList
                                    group lst by new { lst.EmployeeRefId, lst.EmployeeRefName } into g
                                    select new EmployeeWorkStatusDatesDto
                                    {
                                        EmployeeRefId = g.Key.EmployeeRefId,
                                        EmployeeRefName = g.Key.EmployeeRefName,
                                        StatusDates = g.Select(t => t.SelectedDate).ToArray().MapTo<List<DateTime>>(),
                                        NoOfDays = g.Select(t => t.SelectedDate).ToArray().Count()
                                    }).ToList();

            var rejectedString = L("Rejected");
            var rsLeaveRequest = await _leaveRequestRepo.GetAllListAsync(t => t.LeaveStatus != rejectedString);

            foreach (var emp in groupLeaveOutput)
            {
                var per = rsPersonalInformation.FirstOrDefault(t => t.Id == emp.EmployeeRefId);
                if (per == null)
                {
                    throw new UserFriendlyException(L("NotExistForId", "PersonalInformation", emp.EmployeeRefId));
                }

                var empskills = rsEmpSkillSet.Where(t => t.EmployeeRefId == emp.EmployeeRefId);

                List<SkillSetEditDto> empSkillSet = new List<SkillSetEditDto>();
                string employeeSkills = "";
                foreach (var sk in empskills)
                {
                    SkillSetEditDto ns = new SkillSetEditDto();
                    var locskill = skillSet.FirstOrDefault(t => t.Id == sk.SkillSetRefId);
                    if (locskill == null)
                    {
                        throw new UserFriendlyException(L("SkillSetMismatch"), emp.EmployeeRefName);
                    }
                    ns.SkillCode = locskill.SkillCode;
                    ns.Id = locskill.Id;
                    employeeSkills = employeeSkills + locskill.SkillCode + ",";
                    empSkillSet.Add(ns);
                }
                employeeSkills = employeeSkills.Left(employeeSkills.Length - 1);
                emp.EmployeeSkills = employeeSkills;
                var skill = skillSet.FirstOrDefault(t => t.Id == per.DefaultSkillRefId);
                emp.DefaultSkillSetCode = skill.SkillCode;
                if (per.EmployeeResidentStatusRefId > 0)
                {
                    var resident = rsEmployeeResidentStatus.FirstOrDefault(t => t.Id == per.EmployeeResidentStatusRefId);
                    if (resident != null)
                    {
                        emp.WorkStatus = resident.ResidentStatus;
                    }
                }

                if (remarksFlag)
                {
                    var leave = rsLeaveRequest.FirstOrDefault(t => t.EmployeeRefId == emp.EmployeeRefId && t.LeaveFrom <= input.StartDate.Value && t.LeaveTo >= input.StartDate.Value);
                    if (leave != null)
                    {
                        emp.Remarks = leave.LeaveFrom.ToString("dd-MMM") + "-" + leave.LeaveTo.ToString("dd-MMM") + " Days : " + leave.TotalNumberOfDays;
                        emp.RemarksFlag = true;
                    }
                }
            }
            #endregion




            result.EmployeeLeaveList = groupLeaveOutput.OrderByDescending(t => t.NoOfDays).ToList();

            result.PieChartData = chartData;




            return result;
        }

        public async Task<List<PieChartData>> AddHoursToList(List<PieChartData> inputList, string workStatus, decimal hours, DateTime? workDate, List<WorkDayListDto> rsWorkDayList)
        {
            var lstIndex = inputList.FindIndex(t => t.name.Equals(workStatus));
            if (lstIndex < 0)
            {
                throw new UserFriendlyException(L("StatusNotInList"));
            }
            decimal maxWorkHours = hours;
            if (workDate.HasValue)
            {
                int wd = (int)workDate.Value.DayOfWeek;
                var workDay = rsWorkDayList.FirstOrDefault(t => t.DayOfWeekRefId == wd);
                if (workDay == null)
                {
                    throw new UserFriendlyException(L("WorkDayNotSettingProperly"));
                }
                maxWorkHours = workDay.NoOfHourWorks;
                var ph = await _publicHolidayRepo.FirstOrDefaultAsync(t => t.HolidayDate == workDate.Value);
                if (ph == null)
                    maxWorkHours = workDay.NoOfHourWorks;
                else
                    maxWorkHours = 0;
            }
            if (hours > 0 && hours < maxWorkHours)
                inputList[lstIndex].y = inputList[lstIndex].y + hours;
            else
            {
                if (maxWorkHours == 0)
                    inputList[lstIndex].y = inputList[lstIndex].y + hours;
                else
                    inputList[lstIndex].y = inputList[lstIndex].y + maxWorkHours;
            }
            return inputList;
        }

        public async Task<PieChartOutputDto> GetEmployeeDashBoardDutyChartData(GetDashBoardDto input)
        {
            PieChartOutputDto result = new PieChartOutputDto();
            ////@@Pending Need to calculate this hours

            decimal maximumOT1Hours = 72;
            decimal maximumOT2Hours = 48;
            decimal maximumFlatOTHours = maximumOT1Hours + maximumOT2Hours;

            decimal workHoursBasedOnWorkDays = 0;
            decimal workHoursBasedOnAttendance = 0;
            decimal workHoursBasedOnClaimed = 0;

            DateTime maxCloseDay;
            if (input.MaxDayCloseDate.HasValue)
            {
                maxCloseDay = input.MaxDayCloseDate.Value;
            }
            else
            {
                var maxValue = await _dutyChartRepo.GetAll().Where(t => t.Status.Equals("DayClosed")).OrderByDescending(t => t.DutyChartDate).FirstOrDefaultAsync();
                if (maxValue != null)
                    maxCloseDay = maxValue.DutyChartDate;
                else
                {
                    maxValue = await _dutyChartRepo.GetAll().OrderByDescending(t => t.DutyChartDate).FirstOrDefaultAsync();
                    if (maxValue != null)
                        maxCloseDay = maxValue.DutyChartDate;
                    else
                        maxCloseDay = DateTime.Today;
                }
            }

            if (input.StartDate == null)
            {
                var dutyCharts = _dutyChartRepo.GetAll().Where(t => !t.Status.Equals("Draft"));
                DateTime maxDutyChartDate;

                if (dutyCharts.Count() == 0)
                {
                    input.StartDate = DateTime.Today.AddDays(-1);
                    input.EndDate = DateTime.Today.AddDays(-1);
                }
                else
                {
                    maxDutyChartDate = dutyCharts.Max(t => t.DutyChartDate);
                    input.StartDate = maxDutyChartDate;
                    input.EndDate = maxDutyChartDate;
                }
            }

            if (input.EndDate.Value.Year <= 2017 && input.EndDate.Value.Month < 2)
            {
                throw new UserFriendlyException("DateErr");
            }

            if (input.StartDate.Value.Year <= 2017 && input.StartDate.Value.Month < 12)
            {
                input.StartDate = Convert.ToDateTime("2017-Dec-01");
            }


            result.Header = input.StartDate.Value.ToString("yyyy-MMM-dd") + " - " + input.EndDate.Value.ToString("yyyy-MMM-dd");

            result.StartDate = input.StartDate.Value;
            result.EndDate = input.EndDate.Value;

            GetAttendanceLogInput attInput = new GetAttendanceLogInput
            {
                EmployeeRefId = input.EmployeeRefId,
                StartDate = input.StartDate.Value,
                EndDate = input.EndDate.Value
            };

            #region ChartData
            var dutyResults = await _dutyChartDetailRepo.GetAll().Where(t => t.DutyChartDate >= input.StartDate && t.DutyChartDate <= input.EndDate && t.EmployeeRefId == input.EmployeeRefId).ToListAsync();

            var dutyChartDetails = dutyResults.MapTo<List<DutyChartDetailListDto>>();
            var rsCompany = await _companyRepo.GetAllListAsync();
            var rsLocation = await _locationRepo.GetAllListAsync();

            //var rsPersonalInformation = await _personalinformationRepo.GetAllListAsync();
            var person = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == input.EmployeeRefId);
            if (person == null)
            {
                throw new UserFriendlyException(L("NotExistForId", "Employee", input.EmployeeRefId));
            }

            var employeeDefaultWorkDays = await _salaryinfoappService.GetEmployeeWorkDay(new GetDataBasedOnNullableEmployee { EmployeeRefId = input.EmployeeRefId });
            var rsWorkDays = employeeDefaultWorkDays.WorkDayListDtos;

            var salaryInfo = await _salaryInfoRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == input.EmployeeRefId);
            if (salaryInfo == null)
            {
                throw new UserFriendlyException(L("NotExistForId", L("Salary"), L("Employee") + " : " + input.EmployeeRefId));
            }

            var salaryMaster = await _salaryinfoappService.GetSalaryInfoForEdit(new NullableIdInput { Id = salaryInfo.Id });
            var salary = salaryMaster.SalaryInfo;

            bool isHourlySalaryMode = false;
            if (salary.SalaryMode == (int)SalaryCalculationType.HOURLY)
                isHourlySalaryMode = true;

            bool workHoursCalculationNeeded = false;

            var maximumOtHoursPerDayExists = rsWorkDays.Any(t => t.MaximumOTAllowedHours > 0);
            if (maximumOtHoursPerDayExists == true)
                workHoursCalculationNeeded = true;

            if (person.Gender.ToUpper().Equals("FEMALE"))
                workHoursCalculationNeeded = true;


            List<AttendanceParticularDateList> employeeAttenanceWithSalaryCalcuation = new List<AttendanceParticularDateList>();
            if (person.IsAttendanceRequired || workHoursCalculationNeeded || isHourlySalaryMode)
            {
                employeeAttenanceWithSalaryCalcuation = await _attendanceLogAppService.GetOTEmployeeAttendanceDateWise(attInput);
            }
            var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == person.LocationRefId);
            var company = await _companyRepo.FirstOrDefaultAsync(t => t.Id == location.CompanyRefId);

            result.EmployeeRefCode = person.EmployeeCode;
            result.CompanyRefCode = company.Code;

            var rsPublicHolidays = await _publicHolidayRepo.GetAllListAsync(t => t.HolidayDate >= input.StartDate.Value && t.HolidayDate <= input.EndDate.Value);


            if (person.LastWorkingDate != null)
            {
                result.RemarksFlag = true;
                result.Remarks = L("EmployeeLeftOn", person.LastWorkingDate.Value.ToString("dd-MMM-yy"));
            }

            foreach (var lst in dutyChartDetails)
            {
                lst.StatusCount = 1;
                company = rsCompany.FirstOrDefault(t => t.Id == lst.CompanyRefId);
                if (company.WeekDayTreatAsHalfDay != null)
                {
                    if ((int)lst.DutyChartDate.Date.DayOfWeek == company.WeekDayTreatAsHalfDay)
                    {
                        lst.StatusCount = (decimal)0.5;
                    }
                }

                if (lst.AllottedStatus == L("Job"))
                {

                }
                if (lst.AllottedStatus == L("WeekOff"))
                {
                    lst.StatusCount = 0;
                }

            }

            var weekoffstring = L("WeekOff");

            var chartData = (from dc in dutyChartDetails.Where(t => t.AllottedStatus != weekoffstring)
                             group dc by new { dc.AllottedStatus } into g
                             select new PieChartData
                             {
                                 name = g.Key.AllottedStatus,
                                 y = g.Sum(t => t.StatusCount)
                             }).ToList();

            foreach (var chart in chartData)
            {
                if (chart.name == L("Billable"))
                    chart.Color = "YELLOW";
                else if (chart.name == L("Idle"))
                    chart.Color = "RED";
                else if (chart.name == L("WeekOff"))
                    chart.Color = "GREEN";
                else if (chart.name == L("Leave"))
                    chart.Color = "CYAN";
                else if (chart.name == L("OfficeJob"))
                    chart.Color = "BLUE";
                else if (chart.name == L("OutsideJobNotBillable"))
                    chart.Color = "PURPLE";
            }

            result.PieChartData = chartData;
            #endregion

            List<PieChartData> TimeWiseWorkStatus = new List<PieChartData>();
            TimeWiseWorkStatus.Add(new PieChartData { name = L("Idle"), y = 0, Color = "RED" });
            TimeWiseWorkStatus.Add(new PieChartData { name = L("Billable"), y = 0, Color = "YELLOW" });
            TimeWiseWorkStatus.Add(new PieChartData { name = L("WeekOff"), y = 0, Color = "GREEN" });
            TimeWiseWorkStatus.Add(new PieChartData { name = L("Leave"), y = 0, Color = "CYAN" });
            TimeWiseWorkStatus.Add(new PieChartData { name = L("OfficeJob"), y = 0, Color = "GREY" });
            TimeWiseWorkStatus.Add(new PieChartData { name = L("OutsideJobNotBillable"), y = 0, Color = "PURPLE" });
            TimeWiseWorkStatus.Add(new PieChartData { name = L("OT"), y = 0, Color = "BLUE" });
            TimeWiseWorkStatus.Add(new PieChartData { name = L("ClaimedOT1"), y = 0, Color = "BROWN" });
            TimeWiseWorkStatus.Add(new PieChartData { name = L("ClaimedOT2"), y = 0, Color = "AMBER" });
            TimeWiseWorkStatus.Add(new PieChartData { name = L("Auto") + " " + L("ManualOT") + " 1", y = 0, Color = "BROWN" });
            TimeWiseWorkStatus.Add(new PieChartData { name = L("Auto") + " " + L("ManualOT") + " 2", y = 0, Color = "AMBER" });
            TimeWiseWorkStatus.Add(new PieChartData { name = L("Unknown"), y = 0, Color = "BLACK" });
            TimeWiseWorkStatus.Add(new PieChartData { name = L("PH"), y = 0, Color = "MAGENTA" });
            TimeWiseWorkStatus.Add(new PieChartData { name = L("Absent"), y = 0, Color = "GOLD" });
            TimeWiseWorkStatus.Add(new PieChartData { name = L("Course"), y = 0, Color = "ORANGE" });
            TimeWiseWorkStatus.Add(new PieChartData { name = L("Exam"), y = 0, Color = "IVORY" });
            TimeWiseWorkStatus.Add(new PieChartData { name = L("Mockup"), y = 0, Color = "BRONZE" });

            List<DutyChartDetailListDto> editListDtos = new List<DutyChartDetailListDto>();

            var dutyListExists = (from dc in _dutyChartDetailRepo.GetAll()
                            .Where(t => t.DutyChartDate >= input.StartDate && t.DutyChartDate <= input.EndDate
                                && t.EmployeeRefId == input.EmployeeRefId)
                                  select new DutyChartDetailListDto
                                  {
                                      Id = dc.Id,
                                      DutyChartRefId = dc.DutyChartRefId,
                                      AllottedStatus = dc.AllottedStatus,
                                      EmployeeRefId = dc.EmployeeRefId,
                                      StationRefId = dc.StationRefId,
                                      ShiftRefId = dc.ShiftRefId,
                                      TimeDescription = "",
                                      NightFlag = false,
                                      Remarks = "",
                                      CompletedStatus = false,
                                      DutyChartDate = dc.DutyChartDate,
                                      CompanyRefId = dc.CompanyRefId,
                                      UserSerialNumber = dc.UserSerialNumber,
                                      SkillRefId = dc.SkillRefId,
                                  }
                            ).OrderBy(t => t.DutyChartDate).ToList();

            DateTime FromDate = input.StartDate.Value;
            DateTime ToDate = input.EndDate.Value;

            var rsDcGroupMaster = await _dcGroupMasterRepo.GetAllListAsync();
            var rsDcHeadMaster = await _dcHeadMasterRepo.GetAllListAsync();
            var rsStationMaster = await _dcstationmasterRepo.GetAllListAsync();
            var rsShiftMaster = await _dcshiftmasterRepo.GetAllListAsync();


            var rsAttLog = await _attendanceLogRepo.GetAllListAsync(t => t.EmployeeRefId == input.EmployeeRefId
                && t.CheckTime >= FromDate && DbFunctions.TruncateTime(t.CheckTime) <= ToDate);
            rsAttLog = rsAttLog.OrderBy(t => t.CheckTime).ToList();

            var approvedString = L("Approved");
            var rsLeaveRequest = _leaveRequestRepo.GetAll().Where(t => t.EmployeeRefId == input.EmployeeRefId && t.LeaveStatus.ToUpper().Equals(approvedString.ToUpper()));
            var approvedLeaveRequestDetals = await rsLeaveRequest.Where(t => t.LeaveTo >= input.StartDate).OrderBy(t => t.LeaveFrom).ToListAsync();

            List<ApprovedLeaveDateList> approvedLeaveDateLists = new List<ApprovedLeaveDateList>();

            foreach (var leave in approvedLeaveRequestDetals)
            {
                DateTime leaveFrom = leave.LeaveFrom;
                do
                {
                    ApprovedLeaveDateList newLeaveApproved = new ApprovedLeaveDateList
                    {
                        WorkDate = leaveFrom,
                        ApprovedLeave = true
                    };
                    approvedLeaveDateLists.Add(newLeaveApproved);
                    leaveFrom = leaveFrom.AddDays(1);
                } while (leaveFrom <= leave.LeaveTo);

            }


            var rsGpsAttLog = await _gpsAttendanceRepo.GetAllListAsync(t => t.EmployeeRefId == input.EmployeeRefId && t.CreationTime >= FromDate
                    && DbFunctions.TruncateTime(t.CreationTime) <= ToDate);

            rsGpsAttLog = rsGpsAttLog.OrderBy(t => t.CreationTime).ToList();

            var defaultTime = await _dcstationmasterRepo.FirstOrDefaultAsync(t => t.DefaultTimeFlag == true);
            if (defaultTime == null)
            {
                throw new UserFriendlyException(L("DefaultTimeErr"));
            }

            var rsTsDetail = await _timesheetdetailRepo.GetAll()
                .Where(t => t.TsDate >= FromDate && t.TsDate <= ToDate).ToListAsync();
            int[] tsDetIds = rsTsDetail.Select(t => t.Id).ToArray();

            var rsTsEmps = await _timesheetemployeedetailRepo.GetAll()
                .Where(t => tsDetIds.Contains(t.TsDetailRefId) && input.EmployeeRefId == t.EmployeeRefId).ToListAsync();

            tsDetIds = rsTsEmps.Select(t => t.TsDetailRefId).ToArray();
            rsTsDetail = await _timesheetdetailRepo.GetAll()
                .Where(t => tsDetIds.Contains(t.Id)).ToListAsync();

            int[] tsMasIds = rsTsEmps.Select(t => t.TsMasterRefId).ToArray();
            var rsTsMaster = await _timesheetmasterRepo.GetAll().Where(t => tsMasIds.Contains(t.Id)).ToListAsync();

            var rsTsMaterialUsage = await _timesheetmaterialusageRepo.GetAll().Where(t => tsMasIds.Contains(t.TsMasterRefId)).ToListAsync();

            var rsMaterial = await _materialRepo.GetAllListAsync();

            var rsAttMachineLocation = await _attendanceMachineLocationRepo.GetAllListAsync();

            List<OtMinuteFlags> OtTimesMaster = new List<OtMinuteFlags>();
            for (int lp = 0; lp < 2880; lp++)
            {
                OtTimesMaster.Add(new OtMinuteFlags { Minute = lp, WorkFlag = false });
            }

            int yellowPending = 0;
            int pinkPending = 0;
            decimal noOfIdleDays = 0;
            int invoicePending = 0;
            int invoiceDone = 0;
            bool publicHolidayFlag = false;

            var rsSkillSets = await _skillsetRepo.GetAllListAsync();

            decimal noOfWorkingDays = 0;
            decimal noOfAbsentDays = 0;
            decimal noOfLeavePaidDays = 0;
            decimal noOfPublicHoliDays = 0;
            decimal noOfExcessLevyDays = 0;

            List<StationDetail> stationDetails = new List<StationDetail>();
            #region FindOT
            foreach (var lst in dutyListExists)
            {
                var skill = rsSkillSets.FirstOrDefault(t => t.Id == lst.SkillRefId);
                lst.SkillRefCode = skill.SkillCode;



                int wd = (int)lst.DutyChartDate.DayOfWeek;
                var workDay = rsWorkDays.FirstOrDefault(t => t.DayOfWeekRefId == wd);
                if (workDay == null)
                {
                    throw new UserFriendlyException(L("WorkDayNotSettingProperly"));
                }

                var templst = lst;
                var alreadyDateExists = editListDtos.FirstOrDefault(t => t.DutyChartDate.Date == lst.DutyChartDate.Date);
                if (alreadyDateExists == null)
                {
                    #region RequiredPerDay One Time

                    publicHolidayFlag = false;

                    if (lst.AllottedStatus == L("Job"))
                    {
                        lst.AllottedStatus = L("Job");
                        if (workDay.NoOfWorkDays > 0)
                        {
                            noOfWorkingDays = noOfWorkingDays + workDay.NoOfWorkDays;
                            workHoursBasedOnWorkDays = workHoursBasedOnWorkDays + workDay.NoOfHourWorks;
                        }
                    }
                    else if (lst.AllottedStatus == L("Absent"))
                    {
                        await AddHoursToList(TimeWiseWorkStatus, L("Absent"), 0, lst.DutyChartDate, rsWorkDays);
                        noOfAbsentDays = noOfAbsentDays + workDay.NoOfWorkDays;
                    }
                    else if (lst.AllottedStatus == L("Idle"))
                    {
                        noOfIdleDays = noOfIdleDays + workDay.NoOfWorkDays;
                        workHoursBasedOnWorkDays = workHoursBasedOnWorkDays + workDay.NoOfHourWorks;
                        result.IdleList.Add(lst);
                        await AddHoursToList(TimeWiseWorkStatus, L("Idle"), 0, lst.DutyChartDate, rsWorkDays);
                    }
                    else if (lst.AllottedStatus == L("PH"))
                    {
                        await AddHoursToList(TimeWiseWorkStatus, L("PH"), 0, lst.DutyChartDate, rsWorkDays);
                        publicHolidayFlag = true;
                        noOfPublicHoliDays = noOfPublicHoliDays + workDay.NoOfWorkDays;
                    }
                    else if (lst.AllottedStatus == L("Leave"))
                    {
                        await AddHoursToList(TimeWiseWorkStatus, L("Leave"), 0, lst.DutyChartDate, rsWorkDays);
                        bool leaveApprovalExists = approvedLeaveDateLists.Exists(t => t.WorkDate == lst.DutyChartDate);
                        if (leaveApprovalExists)
                            noOfLeavePaidDays = noOfLeavePaidDays + workDay.NoOfWorkDays;
                        else
                            noOfAbsentDays = noOfAbsentDays + workDay.NoOfWorkDays;
                    }
                    else if (lst.AllottedStatus == L("Course"))
                    {
                        await AddHoursToList(TimeWiseWorkStatus, L("Course"), 0, lst.DutyChartDate, rsWorkDays);
                        noOfWorkingDays = noOfWorkingDays + workDay.NoOfWorkDays;
                        workHoursBasedOnWorkDays = workHoursBasedOnWorkDays + workDay.NoOfHourWorks;
                    }
                    else if (lst.AllottedStatus == L("Exam"))
                    {
                        await AddHoursToList(TimeWiseWorkStatus, L("Exam"), 0, lst.DutyChartDate, rsWorkDays);
                        noOfWorkingDays = noOfWorkingDays + workDay.NoOfWorkDays;
                        workHoursBasedOnWorkDays = workHoursBasedOnWorkDays + workDay.NoOfHourWorks;
                    }
                    else if (lst.AllottedStatus == L("Mockup"))
                    {
                        await AddHoursToList(TimeWiseWorkStatus, L("Mockup"), 0, lst.DutyChartDate, rsWorkDays);
                        noOfWorkingDays = noOfWorkingDays + workDay.NoOfWorkDays;
                        workHoursBasedOnWorkDays = workHoursBasedOnWorkDays + workDay.NoOfHourWorks;
                    }
                    lst.EmployeeRefName = person.EmployeeName;
                    lst.LocationRefName = location.Code;
                    lst.CompanyRefId = company.Id;
                    lst.CompanyRefCode = company.Code;

                    #region Attendance List 
                    lst.AttendanceTimeList = new List<AttendanceTimeWithLocation>();
                    var gpsAttLogs = rsGpsAttLog.Where(t => t.EmployeeRefId == input.EmployeeRefId && t.CreationTime >= lst.DutyChartDate
                           && t.CreationTime < lst.DutyChartDate.AddDays(1));

                    if (gpsAttLogs != null && gpsAttLogs.Count() > 0)
                    {
                        List<AttendanceTimeWithLocation> attOutput = new List<AttendanceTimeWithLocation>();
                        List<DateTime> attTimes = new List<DateTime>();
                        foreach (var att in gpsAttLogs.ToList())
                        {
                            DateTime chkTime = att.CreationTime;
                            DateTime dt = new DateTime(chkTime.Year, chkTime.Month, chkTime.Day, chkTime.Hour, chkTime.Minute, 0);
                            if (!attTimes.Contains(dt))
                                attTimes.Add(dt);
                        }
                        AttendanceTimeWithLocation attDto = new AttendanceTimeWithLocation();
                        attDto.AttLocation = L("Gps");
                        attDto.AttTime = attTimes;
                        attOutput.Add(attDto);
                        lst.AttendanceTimeList.AddRange(attOutput);
                    }

                    var attlogs = rsAttLog.Where(t => t.EmployeeRefId == input.EmployeeRefId && t.CheckTime >= lst.DutyChartDate && t.CheckTime < lst.DutyChartDate.AddDays(1));

                    if (attlogs != null && attlogs.Count() > 0)
                    {
                        List<AttendanceTimeWithLocation> attOutput = new List<AttendanceTimeWithLocation>();
                        foreach (var groupatt in attlogs.Where(t=>t.AttendanceMachineLocationRefId.HasValue).GroupBy(t => t.AttendanceMachineLocationRefId))
                        {
                            List<DateTime> attTimes = new List<DateTime>();
                            foreach (var att in groupatt.ToList())
                            {
                                DateTime chkTime = att.CheckTime.Value;
                                DateTime dt = new DateTime(chkTime.Year, chkTime.Month, chkTime.Day, chkTime.Hour, chkTime.Minute, 0);
                                if (!attTimes.Contains(dt))
                                    attTimes.Add(dt);
                            }
                            var attlocation = rsAttMachineLocation.FirstOrDefault(t => t.Id == groupatt.Key);
                            AttendanceTimeWithLocation attDto = new AttendanceTimeWithLocation();
                            attDto.AttLocation = attlocation == null ? string.Empty : attlocation.MachinePrefix;
                            attDto.AttTime = attTimes;
                            attOutput.Add(attDto);
                        }
                        lst.AttendanceTimeList.AddRange(attOutput);
                    }
                    #endregion
                    if (person.IsAttendanceRequired || workHoursCalculationNeeded || isHourlySalaryMode == true)
                    {
                        var dateWorkingHours = employeeAttenanceWithSalaryCalcuation.FirstOrDefault(t => t.WorkDate.Date == lst.DutyChartDate.Date);
                        if (dateWorkingHours != null)
                        {
                            lst.ActualWorkHours = Math.Round(dateWorkingHours.ActualWorkHours, 2);
                            lst.ActualWorkHoursWithMinutes = dateWorkingHours.ActualWorkHoursWithMinutes;
                            lst.ShortageWorkHours = Math.Round(dateWorkingHours.ShortageWorkHours, 2);
                            lst.ShortageWorkHoursInMinutes = dateWorkingHours.ShortageWorkHoursInMinutes;

                            workHoursBasedOnAttendance = workHoursBasedOnAttendance + Math.Round(dateWorkingHours.ActualWorkHours, 2);

                            if (publicHolidayFlag)
                            {
                                lst.TotalAutoManualOt2Hours = Math.Round(dateWorkingHours.ActualWorkHours, 2);
                                if (lst.TotalAutoManualOt2Hours > salary.MaximumAllowedOTHoursOnPublicHoliday)
                                    lst.TotalAutoManualOt2Hours = salary.MaximumAllowedOTHoursOnPublicHoliday;


                            }
                            else if (workDay.MaximumOTAllowedHours > 0)
                            {
                                decimal manualOTHours = 0m;
                                manualOTHours = Math.Round(dateWorkingHours.ActualWorkHours, 2) - workDay.NoOfHourWorks;
                                if (manualOTHours < workDay.MinimumOTHoursNeededForAutoManualOTEligible)
                                {
                                    //  Do Nothing if Minimum OT Eligible is not reached
                                }
                                else
                                {
                                    if (manualOTHours > workDay.MaximumOTAllowedHours)
                                        manualOTHours = workDay.MaximumOTAllowedHours;
                                    if (workDay.OtRatio == (decimal)1.5)
                                    {
                                        lst.TotalAutoManualOt1Hours = manualOTHours;
                                    }
                                    else if (workDay.OtRatio == (decimal)2.0)
                                    {
                                        lst.TotalAutoManualOt2Hours = manualOTHours;
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                }

                bool manualIncentiveAllowed = true;
                if (person.IncentiveAllowedFlag == true)
                {
                    var strstatus = lst.AllottedStatus;
                    if (lst.AllottedStatus.Equals("Billable") || lst.AllottedStatus.Equals(L("Job"))
                        || lst.AllottedStatus.Equals(L("OutsideJobNotBillable")) || lst.AllottedStatus.Equals(L("OfficeJob"))
                        || lst.AllottedStatus.Equals(L("Office")) || lst.AllottedStatus.Equals(L("Mockup")) || lst.AllottedStatus.Equals(L("Idle")))
                    {
                        manualIncentiveAllowed = true;
                    }
                    if (manualIncentiveAllowed==false)
                    {
                        if (person.StaffType.Equals("Freelancer") && (lst.AllottedStatus.Equals("Standby") || lst.AllottedStatus.Equals("Course")))
                            manualIncentiveAllowed = true;
                    }
                }

                var dtcm = rsStationMaster.FirstOrDefault(dt => dt.Id == lst.StationRefId);
                if (dtcm != null)
                {
                    lst.DutyStartTime = lst.DutyChartDate.AddMinutes(dtcm.TimeIn);
                    if (lst.HalfDayWorkingDayFlag == false)
                        lst.DutyEndTime = lst.DutyChartDate.AddMinutes(dtcm.TimeOut);
                    else
                        lst.DutyEndTime = lst.DutyStartTime.AddMinutes((double)workDay.NoOfHourWorks * 60);

                    lst.TimeDescription = dtcm.TimeDescription;
                    lst.NightFlag = dtcm.NightFlag;
                }
                //else
                //    throw new UserFriendlyException(L("TimeNotDefinedForEmployee", lst.EmployeeRefName + lst.EmployeeRefCode));
                if (lst.ShiftRefId.HasValue)
                {
                    var shift = rsShiftMaster.FirstOrDefault(t => t.Id == lst.ShiftRefId);
                    lst.ShiftRefName = shift.DutyDesc;
                    var station = rsStationMaster.FirstOrDefault(t => t.Id == lst.StationRefId);
                    lst.StationRefName = station.StationName;
                    var dcGroup = rsDcGroupMaster.FirstOrDefault(t => t.Id == station.DcGroupRefId);
                    lst.DcGroupRefId = station.DcGroupRefId;
                    lst.DcGroupRefName = dcGroup.GroupName;
                    var dcHead = rsDcHeadMaster.FirstOrDefault(t => t.Id == dcGroup.DcHeadRefId);
                    lst.DcHeadRefId = dcHead.Id;
                    lst.DcHeadRefName = dcHead.HeadName;

                    StationDetail workStation = new StationDetail();
                    workStation.AllotedStatus = lst.AllottedStatus;
                    workStation.TsNgClass = lst.AllottedStatus;
                    workStation.DcHeadRefId = lst.DcHeadRefId;
                    workStation.DcHeadRefName = lst.DcHeadRefName;
                    workStation.DcGroupRefId = lst.DcGroupRefId;
                    workStation.DcGroupRefName = lst.DcGroupRefName;
                    workStation.StationRefId = lst.StationRefId;
                    workStation.StationRefName = lst.StationRefName;
                    workStation.ShiftRefId = lst.ShiftRefId;
                    workStation.ShiftRefName = lst.ShiftRefName;
                    if (lst.StationDetails == null)
                        lst.StationDetails = new List<StationDetail>();
                    lst.StationDetails.Add(workStation);
                }
                if (alreadyDateExists == null)
                {
                    lst.ManualIncentiveAllowed = manualIncentiveAllowed;
                    editListDtos.Add(lst);
                }
            }



            foreach (var lst in editListDtos)
            {
                if (lst.TotalAutoManualOt1Hours > 0 || lst.TotalAutoManualOt2Hours > 0)
                {
                    if (lst.TotalOt1Hours > 0 || lst.TotalOt2Hours > 0)
                    {
                        lst.TotalAutoManualOt1Hours = 0;
                        lst.TotalAutoManualOt2Hours = 0;
                    }
                    else
                    {
                        if (lst.TotalAutoManualOt1Hours > 0)
                            TimeWiseWorkStatus = await AddHoursToList(TimeWiseWorkStatus, L("Auto") + " " + L("ManualOT") + " 1", (lst.TotalAutoManualOt1Hours), lst.DutyChartDate, rsWorkDays);
                        else if (lst.TotalAutoManualOt2Hours > 0)
                            TimeWiseWorkStatus = await AddHoursToList(TimeWiseWorkStatus, L("Auto") + " " + L("ManualOT") + " 2", (lst.TotalAutoManualOt2Hours), lst.DutyChartDate, rsWorkDays);
                    }
                }

                if (lst.TotalOt2Hours > 8)
                {
                    var tempExistOt2 = lst.TotalOt2Hours;
                    lst.TotalOt2Hours = 8;
                    lst.TotalOt1Hours = lst.TotalOt1Hours + (tempExistOt2 - 8);
                    TimeWiseWorkStatus = await AddHoursToList(TimeWiseWorkStatus, L("ClaimedOT2"), -1 * (tempExistOt2 - 8), lst.DutyChartDate, rsWorkDays);
                    TimeWiseWorkStatus = await AddHoursToList(TimeWiseWorkStatus, L("ClaimedOT1"), (tempExistOt2 - 8), lst.DutyChartDate, rsWorkDays);
                }
            }

            #endregion

            #region IncentiveSetup

            List<EmployeeVsIncentiveListDto> OverAllIncentiveList = new List<EmployeeVsIncentiveListDto>();
            var iclist = await _incentiveCategoryRepo.GetAllListAsync();
            var rsIncentiveCategory = iclist.MapTo<List<IncentiveCategoryListDto>>();

            var ilist = await _incentiveTagRepo.GetAllListAsync(t => t.AllowanceOrDeductionRefId == (int)AllowanceOrDeductionMode.Allowance && t.IncentivePayModeId == (int)IncentivePayMode.Daily);
            var rsIncentiveTag = ilist.MapTo<List<IncentiveTagListDto>>();

            List<int> ilistRefIds = ilist.Select(t => t.Id).ToList();

            var ielist = await _incentivevsentityRepo.GetAllListAsync(t => ilistRefIds.Contains(t.IncentiveTagRefId));
            var rsIncentiveVsEntities = ielist.MapTo<List<IncentiveVsEntityListDto>>();

            var docForSkillSet = await _documentForSkillSetRepo.GetAllListAsync();
            var rsEmployeeVsDocument = await _employeeDocumentinfoRepo.GetAllListAsync(t => t.EmployeeRefId == input.EmployeeRefId);
            List<int> documentIds = rsEmployeeVsDocument.Select(t => t.DocumentInfoRefId).ToList();
            var rsDocumentInfo = await _documentinfoRepo.GetAllListAsync();

            var empinclist = await _employeeVsIncentiveRepo.GetAllListAsync(t => t.EmployeeRefId == input.EmployeeRefId);
            DateTime tomo = DateTime.Today.AddDays(1);

            foreach (var incForAllEmployee in rsIncentiveTag.Where(t => t.CanBeAssignedToAllEmployees && t.IncentiveEntryMode != (int)IncentiveEntryMode.MANUAL))
            {
                var incTag = rsIncentiveTag.FirstOrDefault(t => t.Id == incForAllEmployee.Id);
                if (incTag.IncentiveEntryMode == (int)IncentiveEntryMode.MANUAL)
                    continue;
                var exists = empinclist.FirstOrDefault(t => t.IncentiveTagRefId == incForAllEmployee.Id);
                if (exists == null)
                {
                    EmployeeVsIncentive allEmpInc = new EmployeeVsIncentive
                    {
                        IncentiveTagRefId = incForAllEmployee.Id,
                        EmployeeRefId = input.EmployeeRefId,
                    };
                    empinclist.Add(allEmpInc);
                }
            }

            var rsEmpVsIncentives = empinclist.MapTo<List<EmployeeVsIncentiveListDto>>();
            if (rsEmpVsIncentives != null && rsEmpVsIncentives.Count > 0)
            {
                var rsManualIncentives = await _manualIncentiveRepo.GetAllListAsync(t => t.EmployeeRefId == input.EmployeeRefId && DbFunctions.TruncateTime(t.IncentiveDate) >= DbFunctions.TruncateTime(input.StartDate) && DbFunctions.TruncateTime(t.IncentiveDate) <= DbFunctions.TruncateTime(input.EndDate));

                #region IncentiveSetups
                foreach (var lst in editListDtos)
                {

                    Get_WorkDayHours_With_OT_Value workDayHoursAndOt = new Get_WorkDayHours_With_OT_Value();
                    workDayHoursAndOt = await GetWorkHoursForGivenDate(new DateInput { DateId = lst.DutyChartDate }, rsWorkDays, rsPublicHolidays);
                    List<EmployeeVsIncentiveListDto> incentiveList = new List<EmployeeVsIncentiveListDto>();

                    #region ManualIncentives
                    var particularDayManualIncentiveList = rsManualIncentives.Where(t => t.IncentiveDate.Date == lst.DutyChartDate.Date);
                    foreach (var manualinc in particularDayManualIncentiveList)
                    {
                        var incTag = rsIncentiveTag.FirstOrDefault(t => t.Id == manualinc.IncentiveTagRefId);
                        EmployeeVsIncentiveListDto detailInc = new EmployeeVsIncentiveListDto();
                        detailInc.IncentiveDate = manualinc.IncentiveDate;
                        detailInc.EmployeeRefId = input.EmployeeRefId;
                        detailInc.EmployeeRefName = person.EmployeeName;
                        detailInc.IncentiveTagRefId = incTag.Id;
                        detailInc.IncentiveTagRefCode = incTag.IncentiveTagCode;
                        detailInc.DisplayNotApplicable = incTag.DisplayNotApplicable;
                        detailInc.IncludeInConsolidationPerDay = incTag.IncludeInConsolidationPerDay;
                        detailInc.IncentiveAmount = manualinc.IncentiveAmount;
                        detailInc.ManualOtHours = manualinc.ManualOtHours;
                        detailInc.IncentiveEntryMode = (int)IncentiveEntryMode.MANUAL;
                        detailInc.IncentiveEntryModeRefName = L("Manual");
                        incentiveList.Add(detailInc);
                    }
                    #endregion

                    //if (person.isBillable)
                    {
                        foreach (var inc in rsEmpVsIncentives.Where(t => t.IncentiveEntryMode != (int)IncentiveEntryMode.MANUAL))
                        {

                            var incTag = rsIncentiveTag.FirstOrDefault(t => t.Id == inc.IncentiveTagRefId);
                            if (incTag.IncentiveEntryMode == (int)IncentiveEntryMode.MANUAL)
                                continue;
                            if (lst.AllottedStatus == L("Leave"))
                                continue;
                            EmployeeVsIncentiveListDto detailInc = new EmployeeVsIncentiveListDto();
                            detailInc.EmployeeRefId = input.EmployeeRefId;
                            detailInc.EmployeeRefName = person.EmployeeName;
                            detailInc.IncentiveTagRefId = incTag.Id;
                            detailInc.IncentiveTagRefCode = incTag.IncentiveTagCode;
                            detailInc.DisplayNotApplicable = incTag.DisplayNotApplicable;
                            detailInc.IncludeInConsolidationPerDay = incTag.IncludeInConsolidationPerDay;
                            detailInc.IncentiveEntryMode = (int)IncentiveEntryMode.AUTOMATIC;
                            detailInc.IncentiveEntryModeRefName = L("Auto");
                            detailInc.IncentiveDate = lst.DutyChartDate.Date;

                            if (incTag.HoursBased == true)
                            {
                                if (incTag.FullWorkHours == true)
                                {
                                    incTag.MinimumHours = workDayHoursAndOt.FullWorkHours;
                                }
                                if (lst.TotalWorkHours < incTag.MinimumHours)
                                {
                                    if (incTag.DisplayNotApplicable)
                                        incentiveList.Add(detailInc);
                                    continue;
                                }

                                if ((lst.TotalOt1Hours + lst.TotalOt2Hours) < incTag.MinimumOtHours)
                                {
                                    if (incTag.DisplayNotApplicable)
                                        incentiveList.Add(detailInc);
                                    continue;
                                }

                                if ((lst.TotalWorkHours + lst.TotalOt1Hours + lst.TotalOt2Hours) < incTag.MinimumClaimedHours)
                                {
                                    if (incTag.DisplayNotApplicable)
                                        incentiveList.Add(detailInc);
                                    continue;
                                }
                            }

                //            lst.EligibleTsForIncentiveCalculation = lst.TimeSheetMasters;
                //            if (lst.EligibleTsDetailForIncentiveCalculation == null)
                //                lst.EligibleTsForIncentiveCalculation = new List<TimeSheetMasterListDto>();
                //            List<int> eligibleTsMasterRefIds = lst.EligibleTsForIncentiveCalculation.Select(t => t.Id).ToList();

                //            lst.EligibleTsDetailForIncentiveCalculation = rsTsDetail.Where(t => t.TsDate.Date == lst.DutyChartDate.Date && eligibleTsMasterRefIds.Contains(t.TsMasterRefId)).ToList().MapTo<List<TimeSheetDetailEditDto>>();

                //            List<int> eligibleTsDetailRefIds = lst.EligibleTsDetailForIncentiveCalculation.Select(t => t.Id).ToList();

                //            var tempEligibleEmployeeWithOthers = await _timesheetemployeedetailRepo.GetAll()
                //.Where(t => eligibleTsMasterRefIds.Contains(t.TsMasterRefId) && eligibleTsDetailRefIds.Contains(t.TsDetailRefId)).ToListAsync();

                //            lst.EligibleTsEmployeeDetail = tempEligibleEmployeeWithOthers.MapTo<List<TimeSheetEmployeeDetailEditDto>>();

                //            lst.EligibleTsMaterialUsage = rsTsMaterialUsage.Where(t => eligibleTsMasterRefIds.Contains(t.TsMasterRefId) && eligibleTsDetailRefIds.Contains(t.TsDetailRefId)).MapTo<List<TimeSheetMaterialUsageEditDto>>();
                //            foreach (var etsm in lst.EligibleTsMaterialUsage)
                //            {
                //                etsm.IncentiveFlag = true;
                //            }

                //            // If Material Quantity Based and No More records in Material Usage , skip and continue next

                //            if (incTag.IsMaterialQuantityBasedIncentiveFlag == true)
                //            {
                //                if (lst.EligibleTsMaterialUsage == null || lst.EligibleTsMaterialUsage.Count == 0)
                //                {
                //                    continue;
                //                }
                //            }


                            //if (incTag.HourlyClaimableTimeSheets)
                            //{
                            //    int[] costCentreRefIds = lst.TimeSheetMasters.Select(t => t.ProjectCostCentreRefId).ToArray();
                            //    var costCentres = rsCostCentre.Where(t => costCentreRefIds.Contains(t.Id)).ToList();
                            //    if (costCentres == null)
                            //    {
                            //        if (incTag.DisplayNotApplicable)
                            //            incentiveList.Add(detailInc);
                            //        continue;
                            //    }
                            //    else
                            //    {
                            //        List<TimeSheetMasterListDto> revisedEligibleTsForIncentiveCalculation = new List<TimeSheetMasterListDto>();
                            //        foreach (var lts in lst.TimeSheetMasters)
                            //        {
                            //            var cs = rsCostCentre.FirstOrDefault(t => t.Id == lts.ProjectCostCentreRefId);
                            //            if (cs == null)
                            //            {
                            //                throw new UserFriendlyException(L("NotExistForId", L("CostCentre"), lts.ProjectCostCentreRefId));
                            //            }
                            //            if (cs.CostMethod.Equals("TM"))
                            //            {
                            //                revisedEligibleTsForIncentiveCalculation.Add(lts);
                            //            }
                            //        }
                            //        lst.EligibleTsForIncentiveCalculation = revisedEligibleTsForIncentiveCalculation;
                            //        if (revisedEligibleTsForIncentiveCalculation.Count == 0)
                            //            continue;
                            //    }
                            //}

                            if (lst.EligibleTsForIncentiveCalculation == null)
                            {
                                var incentiveCategory = rsIncentiveCategory.FirstOrDefault(t => t.Id == incTag.IncentiveCategoryRefId);
                            }


                            #region IncentiveBasedOnEntities
                            var incEntitiesList = rsIncentiveVsEntities.Where(t => t.IncentiveTagRefId == inc.IncentiveTagRefId);

                            if (incEntitiesList != null && incEntitiesList.Count() > 0)
                            {
                                bool existFlag = true;

                                foreach (var ie in incEntitiesList)
                                {
                                    if (incTag.IsMaterialQuantityBasedIncentiveFlag == true)
                                    {
                                        if (lst.EligibleTsMaterialUsage.Where(t => t.IncentiveFlag == true).Count() == 0)
                                        {
                                            existFlag = false;
                                            break;
                                        }
                                    }

                                    //	Skill Set Reference
                                    if (ie.EntityType == (int)IncentiveEntityType.SKILLSET)
                                    {
                                        var skillSet = await _skillsetRepo.FirstOrDefaultAsync(t => t.Id == ie.EntityRefId);
                                        if (skillSet == null)
                                        {
                                            throw new UserFriendlyException(L("NotExistForId", L("Skill"), ie.EntityRefId));
                                        }
                                        else
                                        {
                                            var empSkillIdExists = await _employeeskillsetRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == input.EmployeeRefId && t.SkillSetRefId == skillSet.Id);
                                            if (ie.ExcludeFlag == false)
                                            {
                                                if (empSkillIdExists == null)
                                                {
                                                    existFlag = false;
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                if (empSkillIdExists != null)
                                                {
                                                    existFlag = false;
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    //	Team Size Reference
                                    else if (ie.EntityType == (int)IncentiveEntityType.TEAMSIZE)
                                    {
                                        var teamSize = ie.EntityRefId;
                                        if (teamSize == 0)
                                        {
                                            throw new UserFriendlyException(L("NotExistForId", L("TeamSize"), ie.EntityRefId));
                                        }
                                        else
                                        {
                                            foreach (var incTsMaster in lst.EligibleTsForIncentiveCalculation)
                                            {
                                                foreach (var incTsDetail in lst.EligibleTsDetailForIncentiveCalculation.Where(t => t.TsMasterRefId == incTsMaster.Id))
                                                {
                                                    var tsEmpCount = lst.EligibleTsEmployeeDetail.Where(t => t.TsMasterRefId == incTsDetail.TsMasterRefId && t.TsDetailRefId == incTsDetail.Id).ToList();
                                                    if (tsEmpCount.Count() != teamSize)
                                                    {
                                                        var nonEligibleList = lst.EligibleTsMaterialUsage.Where(t => t.TsMasterRefId == incTsDetail.TsMasterRefId && t.TsDetailRefId == incTsDetail.Id && t.IncentiveFlag == true).ToList();
                                                        foreach (var nel in nonEligibleList)
                                                        {
                                                            nel.IncentiveFlag = false;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    //	Work Position Reference
                                    else if (ie.EntityType == (int)IncentiveEntityType.WORKPOSITION)
                                    {
                                        var wokrPosition = ie.EntityRefId;
                                        if (wokrPosition == 0)
                                        {
                                            throw new UserFriendlyException(L("NotExistForId", L("WorkPosition"), ie.EntityRefId));
                                        }
                                        else
                                        {
                                            foreach (var incTsMaster in lst.EligibleTsForIncentiveCalculation)
                                            {
                                                foreach (var incTsDetail in lst.EligibleTsDetailForIncentiveCalculation.Where(t => t.TsMasterRefId == incTsMaster.Id))
                                                {
                                                    var tsEmpCount = lst.EligibleTsEmployeeDetail.FirstOrDefault(t => t.TsMasterRefId == incTsDetail.TsMasterRefId && t.TsDetailRefId == incTsDetail.Id && t.EmployeeRefId == lst.EmployeeRefId);
                                                    if (tsEmpCount == null || tsEmpCount.WorkPositionRefId != wokrPosition)
                                                    {
                                                        var nonEligibleList = lst.EligibleTsMaterialUsage.Where(t => t.TsMasterRefId == incTsDetail.TsMasterRefId && t.TsDetailRefId == incTsDetail.Id && t.IncentiveFlag == true).ToList();
                                                        foreach (var nel in nonEligibleList)
                                                        {
                                                            nel.IncentiveFlag = false;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    //	Material Reference
                                    else if (ie.EntityType == (int)IncentiveEntityType.MATERIAL)
                                    {
                                        List<int> materialRefIds = new List<int>();
                                        materialRefIds = incEntitiesList.Where(t => t.EitherOr_ForSameEntity == true && t.EntityType == (int)IncentiveEntityType.MATERIAL).Select(t => t.EntityRefId).ToList();

                                        if (materialRefIds.Count() == 0)
                                        {
                                            materialRefIds.Add(ie.EntityRefId);
                                        }

                                        {
                                            foreach (var incTsMaster in lst.EligibleTsForIncentiveCalculation)
                                            {
                                                foreach (var incTsDetail in lst.EligibleTsDetailForIncentiveCalculation.Where(t => t.TsMasterRefId == incTsMaster.Id))
                                                {
                                                    var nonEligibleList = lst.EligibleTsMaterialUsage.Where(t => t.TsMasterRefId == incTsDetail.TsMasterRefId && t.TsDetailRefId == incTsDetail.Id && t.IncentiveFlag == true && !materialRefIds.Contains(t.MaterialRefId)).ToList();
                                                    foreach (var nel in nonEligibleList)
                                                    {
                                                        nel.IncentiveFlag = false;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    else if (ie.EntityType == (int)IncentiveEntityType.CERTIFICATE)
                                    {
                                        List<int> certificateRefIds = new List<int>();
                                        certificateRefIds = incEntitiesList.Where(t => t.EitherOr_ForSameEntity == true && t.EntityType == (int)IncentiveEntityType.CERTIFICATE).Select(t => t.EntityRefId).ToList();

                                        if (certificateRefIds.Count() == 0)
                                        {
                                            certificateRefIds.Add(ie.EntityRefId);
                                        }

                                        var rsCertificateRequired = rsDocumentInfo.Where(t => certificateRefIds.Contains(t.Id));
                                        {
                                            foreach (var certificate in rsCertificateRequired)
                                            {
                                                var empCertificateExists = rsEmployeeVsDocument.FirstOrDefault(t => t.DocumentInfoRefId == certificate.Id);
                                                if (empCertificateExists == null)
                                                {
                                                    if (ie.ExcludeFlag == true)
                                                    {
                                                        // Do nothing 
                                                    }
                                                    else
                                                    {
                                                        inc.Remarks = certificate.DocumentName + " " + L("Certificate") + " " + L("Not") + " " + L("Uploaded");
                                                        inc.IncentiveAmount = 0;
                                                        existFlag = false;
                                                    }
                                                }
                                            }
                                        }
                                        //var documentInfoRefId = ie.EntityRefId;
                                        //if (documentInfoRefId == 0)
                                        //{
                                        //    throw new UserFriendlyException(L("NotExistForId", L("Certificate"), ie.EntityRefId));
                                        //}
                                        //else
                                        //{
                                        //    var existDocument = rsEmployeeVsDocument.FirstOrDefault(t => t.EmployeeRefId == input.EmployeeRefId && t.DocumentInfoRefId == documentInfoRefId);
                                        //    if (ie.ExcludeFlag == false)
                                        //    {
                                        //        if (existDocument == null)
                                        //        {
                                        //            existFlag = false;
                                        //            break;
                                        //        }
                                        //    }
                                        //    else
                                        //    {
                                        //        if (existDocument != null)
                                        //        {
                                        //            existFlag = false;
                                        //            break;
                                        //        }
                                        //        if (existDocument == null)
                                        //        {
                                        //            inc.Remarks = L("Certificate") + " " + L("Not") + " " + L("Uploaded");
                                        //            var doc = rsDocumentInfo.FirstOrDefault(t => t.Id == documentInfoRefId);
                                        //            if (doc != null)
                                        //                inc.Remarks = doc.DocumentName + " " + inc.Remarks;
                                        //            inc.IncentiveAmount = 0;
                                        //        }
                                        //        else
                                        //        {
                                        //            if (existDocument.EndDate.HasValue)
                                        //            {
                                        //                var alertStartingDate = existDocument.EndDate.Value.AddDays(-1 * existDocument.DefaultAlertDays);

                                        //                if (alertStartingDate <= tomo)
                                        //                {
                                        //                    TimeSpan ts = existDocument.EndDate.Value.Subtract(tomo);
                                        //                    inc.Remarks = L("Certificate") + " " + L("Due") + " " + " In" + " " + ts.Days.ToString() + " " + L("Days");
                                        //                }
                                        //                if (existDocument.EndDate <= tomo)
                                        //                {
                                        //                    TimeSpan timeSpan = existDocument.EndDate.Value.Subtract(tomo);
                                        //                    inc.Remarks = L("Certificate") + " " + "Expired" + " " + timeSpan.Days.ToString() + " " + L("Days");
                                        //                    inc.IncentiveAmount = 0;
                                        //                }
                                        //            }
                                        //        }
                                        //    }


                                        //foreach (var incTsMaster in lst.EligibleTsForIncentiveCalculation)
                                        //{
                                        //    foreach (var incTsDetail in lst.EligibleTsDetailForIncentiveCalculation.Where(t => t.TsMasterRefId == incTsMaster.Id))
                                        //    {
                                        //        var tsEmpCount = lst.EligibleTsEmployeeDetail.FirstOrDefault(t => t.TsMasterRefId == incTsDetail.TsMasterRefId && t.TsDetailRefId == incTsDetail.Id && t.EmployeeRefId == lst.EmployeeRefId);
                                        //        if (tsEmpCount == null || tsEmpCount.WorkPositionRefId != wokrPosition)
                                        //        {
                                        //            var nonEligibleList = lst.EligibleTsMaterialUsage.Where(t => t.TsMasterRefId == incTsDetail.TsMasterRefId && t.TsDetailRefId == incTsDetail.Id && t.IncentiveFlag == true).ToList();
                                        //            foreach (var nel in nonEligibleList)
                                        //            {
                                        //                nel.IncentiveFlag = false;
                                        //            }
                                        //        }
                                        //    }
                                        //}
                                    }
                                    else if (ie.EntityType == (int)IncentiveEntityType.GENDER)
                                    {
                                        string genderToBeVerified = "";
                                        if (ie.EntityRefId == 1)
                                        {
                                            genderToBeVerified = "MALE";
                                        }
                                        else if (ie.EntityRefId == 2)
                                        {
                                            genderToBeVerified = "FEMALE";
                                        }
                                        if (person.Gender.ToUpper() == genderToBeVerified.ToUpper())
                                        {

                                        }
                                        else
                                        {
                                            inc.Remarks = L("Required") + " " + L("Gender") + " " + L("Not") + " " + L("Matched");
                                            inc.IncentiveAmount = 0;
                                            existFlag = false;
                                        }
                                    }
                                    else if (ie.EntityType == (int)IncentiveEntityType.RESIDENT_STATUS)
                                    {
                                        string residentStatustobeVerified = "";
                                        var rsResStatus = await _employeeResidentStatusRepo.FirstOrDefaultAsync(t => t.Id == ie.EntityRefId);
                                        if (rsResStatus == null)
                                        {
                                            throw new UserFriendlyException(L("NotExistForId", L("ResidentStatus"), ie.EntityRefId));
                                        }
                                        residentStatustobeVerified = rsResStatus.ResidentStatus;
                                        List<int> allResidentStatusRefIds = new List<int>();
                                        allResidentStatusRefIds = incEntitiesList.Where(t => t.EitherOr_ForSameEntity == true && t.EntityType == (int)IncentiveEntityType.RESIDENT_STATUS && t.ExcludeFlag == false).Select(t => t.EntityRefId).ToList();
                                        if (allResidentStatusRefIds.Count() == 0)
                                        {
                                            allResidentStatusRefIds.Add(ie.EntityRefId);
                                        }
                                        var rsAllResidentStatusRequired = await _employeeResidentStatusRepo.GetAllListAsync(t => allResidentStatusRefIds.Contains(t.Id));

                                        var doesEmployeeHasAnyOfResidentStatus = false;
                                        if (ie.ExcludeFlag == false)
                                        {
                                            foreach (var resStatus in rsAllResidentStatusRequired)
                                            {
                                                if (resStatus.Id == person.EmployeeResidentStatusRefId)
                                                {
                                                    doesEmployeeHasAnyOfResidentStatus = true;
                                                    break;
                                                }
                                            }
                                            if (doesEmployeeHasAnyOfResidentStatus == false)
                                            {
                                                var ResStatusList = "";
                                                foreach (var errskill in rsAllResidentStatusRequired)
                                                {
                                                    ResStatusList = ResStatusList + errskill.ResidentStatus + ",";
                                                }
                                                if (ResStatusList.Length > 0)
                                                {
                                                    ResStatusList = ResStatusList.Left(ResStatusList.Length - 1);
                                                }

                                                if (allResidentStatusRefIds.Count > 1)
                                                    throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Have Any of the Resident Status Of " + ResStatusList);
                                                else
                                                    throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Have the Resident Status Of " + ResStatusList);
                                            }
                                        }
                                        else
                                        {
                                            allResidentStatusRefIds = new List<int>();
                                            allResidentStatusRefIds = incEntitiesList.Where(t => t.EntityType == (int)IncentiveEntityType.RESIDENT_STATUS && t.ExcludeFlag == true).Select(t => t.EntityRefId).ToList();
                                            if (allResidentStatusRefIds.Count() == 0)
                                            {
                                                allResidentStatusRefIds.Add(ie.EntityRefId);
                                            }
                                            rsAllResidentStatusRequired = await _employeeResidentStatusRepo.GetAllListAsync(t => allResidentStatusRefIds.Contains(t.Id));
                                            var existResidentStatus = rsAllResidentStatusRequired.Exists(t => t.Id == person.EmployeeResidentStatusRefId);

                                            if (existResidentStatus == false)
                                            {
                                                var ResStatusList = "";
                                                foreach (var errskill in rsAllResidentStatusRequired)
                                                {
                                                    ResStatusList = ResStatusList + errskill.ResidentStatus + ",";
                                                }
                                                if (ResStatusList.Length > 0)
                                                {
                                                    ResStatusList = ResStatusList.Left(ResStatusList.Length - 1);
                                                }

                                                if (allResidentStatusRefIds.Count > 1)
                                                    throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Not Have Any of the Resident Status Of " + ResStatusList);
                                                else
                                                    throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Not Have the Resident Status Of " + ResStatusList);
                                            }
                                        }
                                    }
                                    else if (ie.EntityType == (int)IncentiveEntityType.RACE)
                                    {
                                        string raceStatustobeVerified = "";
                                        var rsRaceStatus = await _employeeRaceStatusRepo.FirstOrDefaultAsync(t => t.Id == ie.EntityRefId);
                                        if (rsRaceStatus == null)
                                        {
                                            throw new UserFriendlyException(L("NotExistForId", L("Race"), ie.EntityRefId));
                                        }
                                        raceStatustobeVerified = rsRaceStatus.RaceName;

                                        {
                                            List<int> allRaceStatusRefIds = new List<int>();
                                            allRaceStatusRefIds = incEntitiesList.Where(t => t.EitherOr_ForSameEntity == true && t.EntityType == (int)IncentiveEntityType.RACE && t.ExcludeFlag == false).Select(t => t.EntityRefId).ToList();
                                            if (allRaceStatusRefIds.Count() == 0)
                                            {
                                                allRaceStatusRefIds.Add(ie.EntityRefId);
                                            }
                                            var rsAllRaceStatusRequired = await _employeeRaceStatusRepo.GetAllListAsync(t => allRaceStatusRefIds.Contains(t.Id));

                                            var doesEmployeeHasAnyOfRaceStatus = false;
                                            if (ie.ExcludeFlag == false)
                                            {
                                                foreach (var raceStatus in rsAllRaceStatusRequired)
                                                {
                                                    if (raceStatus.Id == person.RaceRefId)
                                                    {
                                                        doesEmployeeHasAnyOfRaceStatus = true;
                                                        break;
                                                    }
                                                }
                                                if (doesEmployeeHasAnyOfRaceStatus == false)
                                                {
                                                    var ResRaceList = "";
                                                    foreach (var errskill in rsAllRaceStatusRequired)
                                                    {
                                                        ResRaceList = ResRaceList + errskill.RaceName + ",";
                                                    }
                                                    if (ResRaceList.Length > 0)
                                                    {
                                                        ResRaceList = ResRaceList.Left(ResRaceList.Length - 1);
                                                    }

                                                    if (allRaceStatusRefIds.Count > 1)
                                                        throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Have Any of the Race Status Of " + ResRaceList);
                                                    else
                                                        throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Have the Race Status Of " + ResRaceList);
                                                }
                                            }
                                            else
                                            {
                                                allRaceStatusRefIds = new List<int>();
                                                allRaceStatusRefIds = incEntitiesList.Where(t => t.EntityType == (int)IncentiveEntityType.RACE && t.ExcludeFlag == true).Select(t => t.EntityRefId).ToList();
                                                if (allRaceStatusRefIds.Count() == 0)
                                                {
                                                    allRaceStatusRefIds.Add(ie.EntityRefId);
                                                }
                                                rsAllRaceStatusRequired = await _employeeRaceStatusRepo.GetAllListAsync(t => allRaceStatusRefIds.Contains(t.Id));
                                                var existResidentStatus = rsAllRaceStatusRequired.Exists(t => t.Id == person.RaceRefId);

                                                if (existResidentStatus == false)
                                                {
                                                    var raceStatusList = "";
                                                    foreach (var errskill in rsAllRaceStatusRequired)
                                                    {
                                                        raceStatusList = raceStatusList + errskill.RaceName + ",";
                                                    }
                                                    if (raceStatusList.Length > 0)
                                                    {
                                                        raceStatusList = raceStatusList.Left(raceStatusList.Length - 1);
                                                    }
                                                    if (allRaceStatusRefIds.Count > 1)
                                                        throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Not Have Any of the Race Status Of " + raceStatusList);
                                                    else
                                                        throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Not Have the Race Status Of " + raceStatusList);
                                                }
                                            }
                                        }
                                    }
                                    else if (ie.EntityType == (int)IncentiveEntityType.RELIGION)
                                    {
                                        string religionStatustobeVerified = "";
                                        var rsReligionStatus = await _employeeReligionStatusRepo.FirstOrDefaultAsync(t => t.Id == ie.EntityRefId);
                                        if (rsReligionStatus == null)
                                        {
                                            throw new UserFriendlyException(L("NotExistForId", L("Religion"), ie.EntityRefId));
                                        }
                                        religionStatustobeVerified = rsReligionStatus.ReligionName;

                                        {
                                            List<int> allReligionStatusRefIds = new List<int>();
                                            allReligionStatusRefIds = incEntitiesList.Where(t => t.EitherOr_ForSameEntity == true && t.EntityType == (int)IncentiveEntityType.RELIGION && t.ExcludeFlag == false).Select(t => t.EntityRefId).ToList();
                                            if (allReligionStatusRefIds.Count() == 0)
                                            {
                                                allReligionStatusRefIds.Add(ie.EntityRefId);
                                            }
                                            var rsAllReligionStatusRequired = await _employeeReligionStatusRepo.GetAllListAsync(t => allReligionStatusRefIds.Contains(t.Id));

                                            var doesEmployeeHasAnyOfRaceStatus = false;
                                            if (ie.ExcludeFlag == false)
                                            {
                                                foreach (var religionStatus in rsAllReligionStatusRequired)
                                                {
                                                    if (religionStatus.Id == person.ReligionRefId)
                                                    {
                                                        doesEmployeeHasAnyOfRaceStatus = true;
                                                        break;
                                                    }
                                                }
                                                if (doesEmployeeHasAnyOfRaceStatus == false)
                                                {
                                                    var ResReligionList = "";
                                                    foreach (var errskill in rsAllReligionStatusRequired)
                                                    {
                                                        ResReligionList = ResReligionList + errskill.ReligionName + ",";
                                                    }
                                                    if (ResReligionList.Length > 0)
                                                    {
                                                        ResReligionList = ResReligionList.Left(ResReligionList.Length - 1);
                                                    }

                                                    if (allReligionStatusRefIds.Count > 1)
                                                        throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Have Any of the Race Status Of " + ResReligionList);
                                                    else
                                                        throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Have the Race Status Of " + ResReligionList);
                                                }
                                            }
                                            else
                                            {
                                                allReligionStatusRefIds = new List<int>();
                                                allReligionStatusRefIds = incEntitiesList.Where(t => t.EntityType == (int)IncentiveEntityType.RACE && t.ExcludeFlag == true).Select(t => t.EntityRefId).ToList();
                                                if (allReligionStatusRefIds.Count() == 0)
                                                {
                                                    allReligionStatusRefIds.Add(ie.EntityRefId);
                                                }
                                                rsAllReligionStatusRequired = await _employeeReligionStatusRepo.GetAllListAsync(t => allReligionStatusRefIds.Contains(t.Id));
                                                var existResidentStatus = rsAllReligionStatusRequired.Exists(t => t.Id == person.ReligionRefId);

                                                if (existResidentStatus == false)
                                                {
                                                    var religionStatusList = "";
                                                    foreach (var errskill in rsAllReligionStatusRequired)
                                                    {
                                                        religionStatusList = religionStatusList + errskill.ReligionName + ",";
                                                    }
                                                    if (religionStatusList.Length > 0)
                                                    {
                                                        religionStatusList = religionStatusList.Left(religionStatusList.Length - 1);
                                                    }

                                                    if (allReligionStatusRefIds.Count > 1)
                                                        throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Not Have Any of the Race Status Of " + religionStatusList);
                                                    else
                                                        throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Not Have the Race Status Of " + religionStatusList);
                                                }
                                            }
                                        }
                                    }
                                }

                                if (existFlag == false)
                                {
                                    if (incTag.DisplayNotApplicable)
                                        incentiveList.Add(detailInc);
                                    continue;
                                }
                                else
                                {
                                    int i = 0;
                                }
                            }
                            #endregion


                            if (incTag.OverTimeBasedIncentive)
                            {

                            }

                            // 

                            if (incTag.IncenitveBasedOnSalaryTags == false)
                            {
                                if (incTag.IsMaterialQuantityBasedIncentiveFlag == false)
                                {
                                    detailInc.IncentiveAmount = incTag.IncentiveAmount;
                                }
                                else
                                {
                                    detailInc.MaterialQuantityUsed = 0;
                                    foreach (var el in lst.EligibleTsMaterialUsage.Where(t => t.IncentiveFlag == true))
                                    {
                                        detailInc.MaterialQuantityUsed = detailInc.MaterialQuantityUsed + el.QuantityUsed;
                                    }
                                    detailInc.Remarks = detailInc.Remarks + " " + "Material Used : " + detailInc.MaterialQuantityUsed;
                                    detailInc.IncentiveAmount = detailInc.MaterialQuantityUsed * incTag.IncentiveAmount;
                                    if (detailInc.IncentiveAmount == 0)
                                    {
                                        if (incTag.DisplayNotApplicable)
                                            incentiveList.Add(detailInc);
                                        continue;
                                    }
                                }
                            }
                            else
                            {
                                if (workDayHoursAndOt.OtTag == L("OT1"))
                                {
                                    detailInc.IncentiveAmount = salary.Ot1PerHour;
                                }
                                else if (workDayHoursAndOt.OtTag == L("OT2"))
                                {
                                    detailInc.IncentiveAmount = salary.Ot2PerHour;
                                }
                                else if (workDayHoursAndOt.OtTag == L("REPORTOT"))
                                {
                                    detailInc.IncentiveAmount = salary.ReportOtPerHour;
                                }
                                else if (workDayHoursAndOt.OtTag == L("NORMALOT"))
                                {
                                    detailInc.IncentiveAmount = salary.NormalOTPerHour;
                                }
                                detailInc.IncentiveAmount = detailInc.IncentiveAmount * incTag.ProportianteValueOfSalaryTags.Value;
                            }
                            incentiveList.Add(detailInc);
                        }
                    }
                    #region IncludeInConsolidationPerDay_Part
                    //	Filter for more than one trade in one day, claim/day whichever is the higher one
                    List<EmployeeVsIncentiveListDto> revisedIncentiveList = new List<EmployeeVsIncentiveListDto>();
                    revisedIncentiveList = incentiveList.Where(t => t.IncludeInConsolidationPerDay == false).ToList();
                    EmployeeVsIncentiveListDto highestIncentiveOnTheDayFromConsolidationList = null;
                    foreach (var il in incentiveList.Where(t => t.IncludeInConsolidationPerDay == true))
                    {
                        if (highestIncentiveOnTheDayFromConsolidationList == null)
                            highestIncentiveOnTheDayFromConsolidationList = il;
                        else
                        {
                            if (highestIncentiveOnTheDayFromConsolidationList.IncentiveAmount < il.IncentiveAmount)
                                highestIncentiveOnTheDayFromConsolidationList = il;
                        }
                    }
                    if (highestIncentiveOnTheDayFromConsolidationList != null)
                        revisedIncentiveList.Add(highestIncentiveOnTheDayFromConsolidationList);
                    #endregion


                    DateTime effectiveDate = new DateTime(2019, 03, 31);
                    if (lst.DutyChartDate.Date > effectiveDate)
                    {
                        foreach (var inc in revisedIncentiveList)
                        {
                            var incTag = ilist.FirstOrDefault(t => t.Id == inc.IncentiveTagRefId);
                            if (incTag.IsCertificateMandatory)
                            {
                                var incentiveVsSkill = ielist.FirstOrDefault(t => t.IncentiveTagRefId == incTag.Id && t.EntityType == (int)IncentiveEntityType.SKILLSET);
                                if (incentiveVsSkill != null)
                                {
                                    var skillRefId = incentiveVsSkill.EntityRefId;
                                    var documentMandatory = docForSkillSet.Where(t => t.SkillSetRefId == skillRefId).ToList();
                                    foreach (var dm in documentMandatory)
                                    {
                                        //foreach (var emp in input.EmployeeRefId)
                                        {
                                            var existDocument = rsEmployeeVsDocument.FirstOrDefault(t => t.EmployeeRefId == input.EmployeeRefId && t.DocumentInfoRefId == dm.DocumentRefId);
                                            if (existDocument == null)
                                            {
                                                inc.Remarks = L("Certificate") + " " + L("Not") + " " + L("Uploaded");
                                                var doc = rsDocumentInfo.FirstOrDefault(t => t.Id == dm.DocumentRefId);
                                                if (doc != null)
                                                    inc.Remarks = doc.DocumentName + " " + inc.Remarks;
                                                inc.IncentiveAmount = 0;
                                            }
                                            else
                                            {
                                                if (existDocument.EndDate.HasValue)
                                                {
                                                    var alertStartingDate = existDocument.EndDate.Value.AddDays(-1 * existDocument.DefaultAlertDays);

                                                    if (alertStartingDate <= tomo)
                                                    {
                                                        TimeSpan ts = existDocument.EndDate.Value.Subtract(tomo);
                                                        inc.Remarks = L("Certificate") + " " + L("Due") + " " + " In" + " " + ts.Days.ToString() + " " + L("Days");
                                                    }
                                                    if (existDocument.EndDate <= tomo)
                                                    {
                                                        TimeSpan timeSpan = existDocument.EndDate.Value.Subtract(tomo);
                                                        inc.Remarks = L("Certificate") + " " + "Expired" + " " + timeSpan.Days.ToString() + " " + L("Days");
                                                        inc.IncentiveAmount = 0;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    lst.IncentiveList = revisedIncentiveList;
                    OverAllIncentiveList.AddRange(revisedIncentiveList);
                }
                #endregion
            }

            #region IncentiveConsolidated
            List<IncentiveConsolidatedDto> incentiveConsolidatedList = new List<IncentiveConsolidatedDto>();
            if (OverAllIncentiveList.Count > 0)
            {
                foreach (var lst in OverAllIncentiveList.GroupBy(t => t.IncentiveTagRefId))
                {
                    var incTag = rsIncentiveTag.FirstOrDefault(t => t.Id == lst.Key);
                    IncentiveConsolidatedDto det = new IncentiveConsolidatedDto();
                    det.IncentiveTagRefId = lst.Key;
                    det.IncentiveTagRefCode = incTag.IncentiveTagCode;
                    det.IsPfPayable = incTag.IsPfPayable;
                    det.IsGovernmentInsurancePayable = incTag.IsGovernmentInsurancePayable;
                    det.OverAllManualOtHours = lst.Where(t => t.ManualOtHours.HasValue).Sum(t => t.ManualOtHours.Value);
                    det.TotalIncentiveAmount = lst.Sum(t => t.IncentiveAmount);
                    det.Count = lst.Count(t => t.IncentiveAmount != 0);
                    if (det.OverAllManualOtHours > maximumOT1Hours)
                    {
                        decimal excessHourCalculation = det.OverAllManualOtHours - maximumOT1Hours;
                        decimal excessAmountCalculation = 0;
                        decimal runningHours = 0;
                        decimal excessCount = 0;

                        List<int> leaveDaysRefIds = rsWorkDays.Where(t => t.NoOfWorkDays == 0).Select(t => t.DayOfWeekRefId).ToList();
                        var weekOffList = lst.Where(t => leaveDaysRefIds.Contains((int)t.IncentiveDate.DayOfWeek));
                        var regularList = lst.Where(t => !leaveDaysRefIds.Contains((int)t.IncentiveDate.DayOfWeek));

                        foreach (var eh in weekOffList)
                        {
                            if (runningHours >= excessHourCalculation)
                                break;
                            if (eh.ManualOtHours.HasValue)
                            {
                                runningHours = runningHours + eh.ManualOtHours.Value;
                                excessAmountCalculation = excessAmountCalculation + eh.IncentiveAmount;
                            }
                            excessCount++;
                        }

                        if (runningHours < excessHourCalculation)
                        {
                            foreach (var eh in regularList)
                            {
                                if (runningHours >= excessHourCalculation)
                                    break;
                                if (eh.ManualOtHours.HasValue)
                                {
                                    runningHours = runningHours + eh.ManualOtHours.Value;
                                    excessAmountCalculation = excessAmountCalculation + eh.IncentiveAmount;
                                }
                                excessCount++;
                            }
                        }
                        excessHourCalculation = runningHours;
                        IncentiveConsolidatedDto excessHours = new IncentiveConsolidatedDto();
                        excessHours.IncentiveTagRefId = lst.Key;
                        excessHours.IncentiveTagRefCode = L("Spl") + " " + incTag.IncentiveTagCode;
                        excessHours.OverAllManualOtHours = excessHourCalculation; // lst.Where(t => t.ManualOtHours.HasValue).Sum(t => t.ManualOtHours.Value);
                        excessHours.TotalIncentiveAmount = excessAmountCalculation;//lst.Sum(t => t.IncentiveAmount);
                        excessHours.Count = excessCount;
                        excessHours.Remarks = excessHourCalculation + " Hrs";
                        incentiveConsolidatedList.Add(excessHours);

                        det.TotalIncentiveAmount = det.TotalIncentiveAmount - excessAmountCalculation;
                        det.OverAllManualOtHours = det.OverAllManualOtHours - excessHourCalculation;
                        det.Count = det.Count - excessCount;
                    }
                    det.Remarks = ""; //    
                    if (incTag.IsMaterialQuantityBasedIncentiveFlag)
                    {
                        det.Remarks = lst.Sum(t => t.MaterialQuantityUsed) + " Count ";
                    }
                    else if (det.OverAllManualOtHours > 0)
                    {
                        if (det.OverAllManualOtHours > maximumOT1Hours)
                        {

                        }
                        det.Remarks = det.OverAllManualOtHours + " Hrs";
                    }
                    else
                    {
                        det.Remarks = det.Count + " Times";
                    }
                    incentiveConsolidatedList.Add(det);
                }
            }

            decimal claimedOt1 = 0;
            decimal claimedOt2 = 0;

            {
                if (salary.DoesFlatOTApplicable == true)
                {
                    var flatOtHours = editListDtos.Sum(t => t.TotalOt1Hours + t.TotalOt2Hours);
                    if (flatOtHours > 0)
                    {
                        decimal pendingOtHours = flatOtHours;
                        var otSlabList = salaryMaster.OtSlabList;
                        int loopCnt = 1;
                        foreach (var otSlab in otSlabList)
                        {
                            if (pendingOtHours <= 0)
                                break;
                            decimal slabHours = otSlab.NoOfHours;
                            if (pendingOtHours > slabHours)
                                pendingOtHours = pendingOtHours - slabHours;
                            else
                            {
                                slabHours = pendingOtHours;
                                pendingOtHours = 0;
                            }
                            if (slabHours > 0 && (otSlab.OTAmount > 0 || otSlab.FixedOTAmount > 0))
                            {
                                decimal amountPerUnit = 0;
                                decimal totalIncentiveAmount = 0;
                                string remarks = "";
                                if (otSlab.OTAmount > 0)
                                {
                                    amountPerUnit = otSlab.OTAmount;
                                    totalIncentiveAmount = Math.Round(slabHours * otSlab.OTAmount, 2);
                                    remarks = otSlab.FromHours + " - " + otSlab.ToHours + " : " + otSlab.OTAmount + " Per Unit";
                                }
                                else if (otSlab.FixedOTAmount > 0)
                                {
                                    amountPerUnit = otSlab.FixedOTAmount;
                                    totalIncentiveAmount = Math.Round(otSlab.FixedOTAmount, 2);
                                    remarks = otSlab.FromHours + " - " + otSlab.ToHours + " : " + otSlab.OTAmount;
                                }
                                incentiveConsolidatedList.Add(
                                                new IncentiveConsolidatedDto
                                                {
                                                    IncentiveTagRefCode = L("Flat") + " " + L("Incentive") + " " + loopCnt,
                                                    IncentiveTagRefId = -2,
                                                    Count = slabHours,
                                                    AmountPerUnit = amountPerUnit,
                                                    TotalIncentiveAmount = totalIncentiveAmount,
                                                    Remarks = remarks
                                                }
                                 );
                                loopCnt++;
                            }
                        }
                    }
                }
                else
                {
                    {
                        var ot1Hours = editListDtos.Sum(t => t.TotalOt1Hours);
                        if (salary.DoesOtSlabWiseApplicable == false)
                        {
                            if (ot1Hours > 0)
                            {
                                decimal extraOt1WorkHours = 0;
                                if (ot1Hours > maximumOT1Hours)
                                {
                                    extraOt1WorkHours = ot1Hours - maximumOT1Hours;
                                    ot1Hours = maximumOT1Hours;
                                }
                                claimedOt1 = ot1Hours;
                                incentiveConsolidatedList.Add(
                                    new IncentiveConsolidatedDto
                                    {
                                        IncentiveTagRefCode = L("ClaimedOT1"),
                                        IncentiveTagRefId = -1,
                                        Count = ot1Hours,
                                        AmountPerUnit = salary.Ot1PerHour,
                                        TotalIncentiveAmount = Math.Round(ot1Hours * salary.Ot1PerHour, 2),
                                        Remarks = ot1Hours + " " + L("Hrs")
                                    }
                                    );

                                if (extraOt1WorkHours > 0)
                                {
                                    incentiveConsolidatedList.Add(
                                                    new IncentiveConsolidatedDto
                                                    {
                                                        IncentiveTagRefCode = L("Special") + " " + L("Incentive") + " 1",
                                                        IncentiveTagRefId = -2,
                                                        Count = extraOt1WorkHours,
                                                        AmountPerUnit = salary.Ot1PerHour,
                                                        TotalIncentiveAmount = Math.Round(extraOt1WorkHours * salary.Ot1PerHour, 2),
                                                        Remarks = ot1Hours + " " + L("Count")
                                                    }
                                                        );
                                }
                            }
                        }
                        else if (salary.DoesOtSlabWiseApplicable == true)
                        {
                            if (ot1Hours > 0)
                            {
                                decimal pendingOtHours = ot1Hours;
                                var otSlabList = salaryMaster.OtSlabList.Where(t => t.OTRefId == (int)OTTypes.OT1).ToList();
                                int loopCnt = 1;
                                foreach (var otSlab in otSlabList)
                                {
                                    if (pendingOtHours <= 0)
                                        break;
                                    decimal slabHours = otSlab.NoOfHours;
                                    if (pendingOtHours > slabHours)
                                        pendingOtHours = pendingOtHours - slabHours;
                                    else
                                    {
                                        slabHours = pendingOtHours;
                                        pendingOtHours = 0;
                                    }
                                    if (slabHours > 0 && (otSlab.OTAmount > 0 || otSlab.FixedOTAmount > 0))
                                    {
                                        decimal amountPerUnit = 0;
                                        decimal totalIncentiveAmount = 0;
                                        string remarks = "";
                                        if (otSlab.OTAmount > 0)
                                        {
                                            amountPerUnit = otSlab.OTAmount;
                                            totalIncentiveAmount = Math.Round(slabHours * otSlab.OTAmount, 2);
                                            remarks = otSlab.FromHours + " - " + otSlab.ToHours + " : " + otSlab.OTAmount + " Per Unit";
                                        }
                                        else if (otSlab.FixedOTAmount > 0)
                                        {
                                            amountPerUnit = otSlab.FixedOTAmount;
                                            totalIncentiveAmount = Math.Round(otSlab.FixedOTAmount, 2);
                                            remarks = otSlab.FromHours + " - " + otSlab.ToHours + " : " + otSlab.OTAmount;
                                        }
                                        incentiveConsolidatedList.Add(
                                                        new IncentiveConsolidatedDto
                                                        {
                                                            IncentiveTagRefCode = L("Spl") + " " + L("Incentive") + " " + loopCnt,
                                                            IncentiveTagRefId = -2,
                                                            Count = slabHours,
                                                            AmountPerUnit = amountPerUnit,
                                                            TotalIncentiveAmount = totalIncentiveAmount,
                                                            Remarks = remarks
                                                        }
                                                            );
                                        loopCnt++;
                                    }
                                }
                            }
                        }
                    }
                    {
                        var ot2Hours = editListDtos.Sum(t => t.TotalOt2Hours);
                        if (salary.DoesOtSlabWiseApplicable == false)
                        {
                            if (ot2Hours > 0)
                            {
                                decimal extraOt2WorkHours = 0;
                                if (ot2Hours > maximumOT2Hours)
                                {
                                    extraOt2WorkHours = ot2Hours - maximumOT2Hours;
                                    ot2Hours = maximumOT2Hours;
                                }
                                claimedOt2 = ot2Hours;
                                incentiveConsolidatedList.Add(
                                    new IncentiveConsolidatedDto
                                    {
                                        IncentiveTagRefCode = L("ClaimedOT2"),
                                        IncentiveTagRefId = -1,
                                        Count = ot2Hours,
                                        AmountPerUnit = salary.Ot2PerHour,
                                        TotalIncentiveAmount = Math.Round(ot2Hours * salary.Ot2PerHour, 2),
                                        Remarks = ot2Hours + " " + L("Hrs")
                                    }
                                    );
                                if (extraOt2WorkHours > 0)
                                {
                                    incentiveConsolidatedList.Add(
                                                    new IncentiveConsolidatedDto
                                                    {
                                                        IncentiveTagRefCode = L("Special") + " " + L("Incentive") + " 2",
                                                        IncentiveTagRefId = -2,
                                                        Count = extraOt2WorkHours,
                                                        AmountPerUnit = salary.Ot2PerHour,
                                                        TotalIncentiveAmount = Math.Round(extraOt2WorkHours * salary.Ot2PerHour, 2),
                                                        Remarks = ot2Hours + " " + L("Count")
                                                    }
                                                        );
                                }
                            }
                        }
                        else if (salary.DoesOtSlabWiseApplicable == true)
                        {
                            if (ot2Hours > 0)
                            {
                                decimal pendingOtHours = ot2Hours;
                                var otSlabList = salaryMaster.OtSlabList.Where(t => t.OTRefId == (int)OTTypes.OT2).ToList();
                                int loopCnt = 1;
                                foreach (var otSlab in otSlabList)
                                {
                                    if (pendingOtHours <= 0)
                                        break;
                                    decimal slabHours = otSlab.NoOfHours;
                                    if (pendingOtHours > slabHours)
                                        pendingOtHours = pendingOtHours - slabHours;
                                    else
                                    {
                                        slabHours = pendingOtHours;
                                        pendingOtHours = 0;
                                    }
                                    if (slabHours > 0 && (otSlab.OTAmount > 0 || otSlab.FixedOTAmount > 0))
                                    {
                                        decimal amountPerUnit = 0;
                                        decimal totalIncentiveAmount = 0;
                                        string remarks = "";
                                        if (otSlab.OTAmount > 0)
                                        {
                                            amountPerUnit = otSlab.OTAmount;
                                            totalIncentiveAmount = Math.Round(slabHours * otSlab.OTAmount, 2);
                                            remarks = otSlab.FromHours + " - " + otSlab.ToHours + " : " + otSlab.OTAmount + " Per Unit";
                                        }
                                        else if (otSlab.FixedOTAmount > 0)
                                        {
                                            amountPerUnit = otSlab.FixedOTAmount;
                                            totalIncentiveAmount = Math.Round(otSlab.FixedOTAmount, 2);
                                            remarks = otSlab.FromHours + " - " + otSlab.ToHours + " : " + otSlab.OTAmount;
                                        }
                                        incentiveConsolidatedList.Add(
                                                        new IncentiveConsolidatedDto
                                                        {
                                                            IncentiveTagRefCode = L("Spl") + " " + L("Incentive") + " " + loopCnt,
                                                            IncentiveTagRefId = -2,
                                                            Count = slabHours,
                                                            AmountPerUnit = amountPerUnit,
                                                            TotalIncentiveAmount = totalIncentiveAmount,
                                                            Remarks = remarks
                                                        }
                                                    );
                                        loopCnt++;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            {
                var autoManualOt1Hours = editListDtos.Sum(t => t.TotalAutoManualOt1Hours);
                if (autoManualOt1Hours > 0)
                {
                    decimal extraAutoManualOtHours = 0;
                    if (autoManualOt1Hours > (maximumOT1Hours - claimedOt1))
                    {
                        extraAutoManualOtHours = autoManualOt1Hours - (maximumOT1Hours - claimedOt1);
                        autoManualOt1Hours = (maximumOT1Hours - claimedOt1);
                    }

                    incentiveConsolidatedList.Add(
                        new IncentiveConsolidatedDto
                        {
                            IncentiveTagRefCode = L("Auto") + " " + L("ManualOT") + " 1",
                            IncentiveTagRefId = -5,
                            Count = autoManualOt1Hours,
                            AmountPerUnit = salary.Ot1PerHour,
                            TotalIncentiveAmount = Math.Round(autoManualOt1Hours * salary.Ot1PerHour, 2),
                            Remarks = autoManualOt1Hours + " " + L("Hrs")
                        }
                        );

                    if (extraAutoManualOtHours > 0)
                    {
                        incentiveConsolidatedList.Add(
                                        new IncentiveConsolidatedDto
                                        {
                                            IncentiveTagRefCode = L("Special") + " " + " " + L("Incentive") + " 1",
                                            IncentiveTagRefId = -6,
                                            Count = extraAutoManualOtHours,
                                            AmountPerUnit = salary.Ot1PerHour,
                                            TotalIncentiveAmount = Math.Round(extraAutoManualOtHours * salary.Ot1PerHour, 2),
                                            Remarks = extraAutoManualOtHours + " " + L("Count")
                                        }
                                            );
                    }
                }
            }

            {
                var autoManualOt2Hours = editListDtos.Sum(t => t.TotalAutoManualOt2Hours);
                if (autoManualOt2Hours > 0)
                {
                    decimal extraAutoManualOtHours = 0;
                    if (autoManualOt2Hours > (maximumOT2Hours - claimedOt2))
                    {
                        extraAutoManualOtHours = autoManualOt2Hours - (maximumOT2Hours - claimedOt2);
                        autoManualOt2Hours = (maximumOT2Hours - claimedOt2);
                    }

                    incentiveConsolidatedList.Add(
                        new IncentiveConsolidatedDto
                        {
                            IncentiveTagRefCode = L("Auto") + " " + L("ManualOT") + " 2",
                            IncentiveTagRefId = -5,
                            Count = autoManualOt2Hours,
                            AmountPerUnit = salary.Ot2PerHour,
                            TotalIncentiveAmount = Math.Round(autoManualOt2Hours * salary.Ot2PerHour, 2),
                            Remarks = autoManualOt2Hours + " " + L("Hrs")
                        }
                        );

                    if (extraAutoManualOtHours > 0)
                    {
                        incentiveConsolidatedList.Add(
                                        new IncentiveConsolidatedDto
                                        {
                                            IncentiveTagRefCode = L("Special") + " " + " " + L("Incentive") + " 2",
                                            IncentiveTagRefId = -6,
                                            Count = extraAutoManualOtHours,
                                            AmountPerUnit = salary.Ot2PerHour,
                                            TotalIncentiveAmount = Math.Round(extraAutoManualOtHours * salary.Ot2PerHour, 2),
                                            Remarks = extraAutoManualOtHours + " " + L("Count")
                                        }
                                            );
                    }
                }
            }
            result.IncentiveConsolidatedList = incentiveConsolidatedList.OrderBy(t => t.IncentiveTagRefId).ToList();
            result.OverAllIncentiveAmount = incentiveConsolidatedList.Sum(t => t.TotalIncentiveAmount);
            #endregion

            #endregion
            if (isHourlySalaryMode == true)
            {
                if (salary.HourlySalaryModeRefId == (int)HourlySalaryMode.Claimed_Hours)
                    result.NoOfWorkHours = workHoursBasedOnClaimed;
                else if (salary.HourlySalaryModeRefId == (int)HourlySalaryMode.Attendance_Hours)
                    result.NoOfWorkHours = workHoursBasedOnAttendance;
                else if (salary.HourlySalaryModeRefId == (int)HourlySalaryMode.WorkDay_Hours)
                    result.NoOfWorkHours = workHoursBasedOnWorkDays;
            }

            result.PinkPending = pinkPending;
            result.YellowPending = yellowPending;
            result.InvoiceDone = invoiceDone;
            result.InvoicePending = invoicePending;
            result.DutyList = editListDtos;

            result.NoOfActualWorkedDays = noOfWorkingDays;
            result.IdleDays = noOfIdleDays;
            result.NoOfPublicHolidays = noOfPublicHoliDays;
            result.NoOfAbsentDays = noOfAbsentDays;
            result.NoOfPaidLeaveDays = noOfLeavePaidDays;
            result.NoOfExcessLevyDays = noOfExcessLevyDays;

            int eventId = 1;
            List<CalendarEvent> calendarEvents = new List<CalendarEvent>();
            if (input.IsCalendarEventsRequired)
            {
                foreach (var evt in editListDtos)
                {
                    string title = "";
                    string description = "";
                    string color = "";
                    string backGroundColor = "";
                    //#region UnknownTimeSheets
                    //if (evt.TimeSheetMasters == null || evt.TimeSheetMasters.Count == 0)
                    //{
                    //    CalendarEvent ce = new CalendarEvent();
                    //    ce.WorkStatus = evt.AllottedStatus;

                    //    title = evt.AllottedStatus + " ";
                    //    //if (evt.CostCentreRefCode != null)
                    //    //{
                    //    //    title = title + evt.CostCentreRefCode;
                    //    //    if (evt.IsBillable)
                    //    //    {
                    //    //        title = title + " " + L("Unknown");
                    //    //        if (evt.TimeSheetDueDate.HasValue)
                    //    //        {
                    //    //            title = title + " " + evt.TimeSheetDueDate.Value.ToString("dd-MMM-yyyy");
                    //    //            description = evt.TimeSheetDueDate.Value.ToString("dd-MMM-yyyy");
                    //    //        }
                    //    //        backGroundColor = "Black";
                    //    //        ce.TsCurrentStatus = L("Unknown");
                    //    //        ce.ProjectCostCentreRefName = evt.ProjectCostCentreRefName;
                    //    //    }
                    //    //}

                    //    if (evt.AllottedStatus.Equals(L("Billable")) || evt.AllottedStatus.Equals(L("OfficeJob")) || evt.AllottedStatus.Equals(L("Job")) || evt.AllottedStatus.Equals(L("MockUp")) || evt.AllottedStatus.Equals(L("Idle")))
                    //    {
                    //        ce.StartAt = evt.DutyStartTime;
                    //        ce.EndAt = evt.DutyEndTime;
                    //    }
                    //    else
                    //    {
                    //        ce.StartAt = evt.DutyChartDate.AddHours(8);
                    //        ce.IsFullDay = true;
                    //    }
                    //    ce.Title = title;
                    //    ce.BackGroundColor = backGroundColor;
                    //    ce.Description = description;
                    //    ce.EventId = eventId++;
                    //    calendarEvents.Add(ce);
                    //}
                    //#endregion
                    if (evt.StationDetails != null && evt.StationDetails.Count > 0)
                    {
                        foreach (var ts in evt.StationDetails)
                        {
                            CalendarEvent ce = new CalendarEvent();
                            ce.WorkStatus = evt.AllottedStatus;
                            ce.StationRefName = ts.StationRefName;
                            ce.ShiftRefName = ts.ShiftRefName;
                            ce.TsCurrentStatus = ts.AllotedStatus;

                            title = ts.DcGroupRefName + " - " + ts.StationRefName;
                            title = title + " " + ts.ShiftRefName;
                            ce.TsNgClass = ts.AllotedStatus;

                            if (ts.TsNgClass == "YellowPending")
                                backGroundColor = "rgb(245,252,154)";
                            else if (ts.TsNgClass == "PinkPending")
                                backGroundColor = "pink";
                            else if (ts.TsNgClass == "PendingForInvoice")
                                backGroundColor = "rgb(252,108,122)";
                            else if (ts.TsNgClass == "InvoiceDone")
                                backGroundColor = "rgb(120,252,154)";
                            description = "";
                            if (ts.WorkTime != null && ts.WorkTime.Length > 0)
                                description = @L("WorkTimeHours") + ": " + ts.WorkTime + " = " + ts.TotalHours;

                            if (ts.OtTime != null && ts.OtTime.Length > 0)
                                description = description + (description.Length > 0 ? " , " : "") + @L("OverTimeHours") + ": " + ts.OtTime + " = " + ts.TotalOverTimeHours;

                            if (ts.Jobremarks != null && ts.Jobremarks.Length > 0)
                                description = description + (description.Length > 0 ? " , " : "") + ts.Jobremarks;

                            if (ts.WorkStartAt.HasValue)
                                ce.StartAt = ts.WorkStartAt.Value;
                            else
                                ce.StartAt = evt.DutyStartTime;

                            if (ts.WorkEndAt.HasValue)
                                ce.EndAt = ts.WorkEndAt.Value;
                            else
                                ce.EndAt = evt.DutyEndTime;

                            ce.Title = title;
                            ce.BackGroundColor = backGroundColor;
                            ce.Description = description;

                            TsCalendarStatus calendarStatus = new TsCalendarStatus();
                            calendarStatus.AllotedStatus = ts.AllotedStatus;
                            calendarStatus.DcHeadRefId = ts.DcHeadRefId;
                            calendarStatus.DcHeadRefName = ts.DcHeadRefName;
                            calendarStatus.DcGroupRefId = ts.DcGroupRefId;
                            calendarStatus.DcGroupRefName = ts.DcGroupRefName;
                            calendarStatus.StationRefId = ts.StationRefId;
                            calendarStatus.StationRefName = ts.StationRefName;
                            calendarStatus.ShiftRefId = ts.ShiftRefId;
                            calendarStatus.ShiftRefName = ts.ShiftRefName;
                            //calendarStatus.WorkTime = ts.WorkTime;
                            //calendarStatus.OtTime = ts.OtTime;

                            ce.WorkInfo = calendarStatus;

                            ce.EventId = eventId++;

                            calendarEvents.Add(ce);
                            description = "";
                            title = "";
                            backGroundColor = "";
                            color = "";
                        }
                    }
                }
            }
            result.CalendarEvents = calendarEvents;

            TimeWiseWorkStatus = TimeWiseWorkStatus.Where(t => t.y > 0).ToList();
            result.TimeWiseWorkStatus = TimeWiseWorkStatus;

            var hroperationHealthEdp = await HrOperationHealthEdp(new GetHrOperationHealthDto
            {
                DaysTill = 45,
                EmployeeRefId = input.EmployeeRefId
            });

            if (!hroperationHealthEdp.HtmlMessage.IsNullOrEmpty())
            {
                result.HrOperationHealthHtml = result.HrOperationHealthHtml + " <BR>" + hroperationHealthEdp.HtmlMessage;
                result.HrOperationHealthText = result.HrOperationHealthText + " " + hroperationHealthEdp.ErrorMessage;
            }
            //#region CheckPreviousMonth
            //{
            //    DateTime minDate = new DateTime(2018, 12, 01);
            //    var Pendings = await _dutyChartDetailRepo.GetAll().Where(t => t.EmployeeRefId == input.EmployeeRefId && t.DutyChartDate >= minDate && t.DutyChartDate <= input.EndDate ).ToListAsync();
            //    var pendingDtos = Pendings.MapTo<List<DutyChartDetailListDto>>();

            //    tsMasIds = pendingDtos.Where(t => t.TimeSheetRefId != null).Select(t => int.Parse(t.TimeSheetRefId.ToString())).ToArray();
            //    rsTsMaster = await _timesheetmasterRepo.GetAll().Where(t => tsMasIds.Contains(t.Id)).ToListAsync();
            //    var lstTsMaster = rsTsMaster.ToList().MapTo<List<TimeSheetMasterListDto>>();

            //    rsTsDetail = await _timesheetdetailRepo.GetAll().Where(t => tsMasIds.Contains(t.TsMasterRefId)).ToListAsync();

            //    var grp = (from mas in pendingDtos
            //               group mas by new { mas.Month, mas.MonthYear } into g
            //               select new MonthWiseDcDto
            //               {
            //                   MonthYear = g.Key.MonthYear,
            //                   //Month = g.Key.Month,
            //                   Detail = g.ToList()
            //               }
            //                ).ToList();
            //    grp = grp.OrderBy(t => t.MonthYear).ToList();

            //    bool monthPendingAlreadyExist = false;
            //    List<MonthWisePendingDto> monthWisePendingList = new List<MonthWisePendingDto>();
            //    foreach (var lst in grp)
            //    {
            //        tsMasIds = lst.Detail.Where(t => t.TimeSheetRefId != null).Select(t => int.Parse(t.TimeSheetRefId.ToString())).ToArray();
            //        var lstMonthTsMaster = lstTsMaster.Where(t => tsMasIds.Contains(t.Id)).ToList();
            //        foreach (var tsMas in lstMonthTsMaster)
            //        {
            //            var lstMonthTsDetail = rsTsDetail.Where(t => t.TsMasterRefId == tsMas.Id && t.TsDate.ToString("MMM-yy").Equals(lst.MonthYear.ToString("MMM-yy"))).ToList();
            //            tsMas.NoOfDaysInTheGivenMonth = lstMonthTsDetail.Count();
            //        }
            //        var monthpendingCount = lst.Detail.Where(t => t.TimeSheetRefId == null).ToList().Count();
            //        var monthyellowPending = lstMonthTsMaster.Where(t => t.SubmissionDate == null).Sum(t => t.NoOfDaysInTheGivenMonth);
            //        var monthPinkPending = lstMonthTsMaster.Where(t => t.SubmissionDate != null && t.SubmissionDateToInvoiceDepartment == null).Sum(t => t.NoOfDaysInTheGivenMonth);
            //        var monthInvoicePending = lstMonthTsMaster.Where(t => t.SubmissionDateToInvoiceDepartment != null && t.InvoiceDate == null).Sum(t => t.NoOfDaysInTheGivenMonth);
            //        var monthInvoiceDone = lstMonthTsMaster.Where(t => t.InvoiceDate != null).Sum(t => t.NoOfDaysInTheGivenMonth);
            //        if (monthpendingCount > 0 || monthyellowPending > 0 || monthPinkPending > 0 || monthInvoicePending > 0 || monthPendingAlreadyExist == true)
            //        {
            //            //DateTime monthYear = new DateTime()
            //            MonthWisePendingDto det = new MonthWisePendingDto
            //            {
            //                MonthYear = lst.MonthYear,
            //                Month = lst.MonthYear.ToString("MMM-yy"),
            //                Pending = monthpendingCount,
            //                YellowPending = monthyellowPending,
            //                PinkPending = monthPinkPending,
            //                InvoicePending = monthInvoicePending,
            //                InvoiceDone = monthInvoiceDone
            //            };
            //            monthWisePendingList.Add(det);
            //            monthPendingAlreadyExist = true;
            //        }
            //    }
            //    result.MonthWisePendingList = monthWisePendingList;
            //}
            //#endregion

            List<WorkStatusDaysVsHours> daysVsHoursWorkStatus = new List<WorkStatusDaysVsHours>();
            foreach (var lst in result.TimeWiseWorkStatus)
            {
                daysVsHoursWorkStatus.Add(new WorkStatusDaysVsHours { WorkStatus = lst.name, Hours = lst.y });
            }

            foreach (var lst in result.PieChartData)
            {
                var exists = daysVsHoursWorkStatus.FirstOrDefault(t => t.WorkStatus == lst.name);
                if (exists == null)
                {
                    daysVsHoursWorkStatus.Add(new WorkStatusDaysVsHours { WorkStatus = lst.name, Days = lst.y });
                }
                else
                {
                    var idx = daysVsHoursWorkStatus.IndexOf(exists);
                    daysVsHoursWorkStatus[idx].Days = lst.y;
                }
            }

            decimal totalDays = daysVsHoursWorkStatus.Sum(t => t.Days);
            decimal totalHours = daysVsHoursWorkStatus.Sum(t => t.Hours);
            foreach (var lst in daysVsHoursWorkStatus)
            {
                if (lst.Hours > 0 && totalHours > 0)
                {
                    lst.PercentageHours = lst.Hours / totalHours * 100;
                }
                if (lst.Days > 0 && totalDays > 0)
                {
                    lst.PercentageDays = lst.Days / totalDays * 100;
                }
            }

            result.DaysVsHoursWorkStatus = daysVsHoursWorkStatus;


            return result;
        }

        //public async Task<GetEmployeeTsDashBoardDto> GetEmployeeDashBoardTimeSheetData(GetDashBoardDto input)
        //{

        //    var maxValue = await _dutyChartRepo.GetAll().Where(t => t.Status.Equals("DayClosed")).OrderByDescending(t => t.DutyChartDate).FirstOrDefaultAsync();

        //    DateTime maxCloseDay;
        //    if (maxValue != null)
        //        maxCloseDay = maxValue.DutyChartDate.AddDays(1);
        //    else
        //    {
        //        maxValue = await _dutyChartRepo.GetAll().OrderByDescending(t => t.DutyChartDate).FirstOrDefaultAsync();
        //        if (maxValue != null)
        //            maxCloseDay = maxValue.DutyChartDate;
        //        else
        //            maxCloseDay = DateTime.Today;
        //    }

        //    if (input.EndDate == null)
        //    {
        //        input.EndDate = maxCloseDay;
        //    }

        //    if (input.EndDate > maxCloseDay)
        //        input.EndDate = maxCloseDay;

        //    DateTime FromDate = input.StartDate.Value; ;
        //    DateTime ToDate = input.EndDate.Value;

        //    var rsTsDetail = await _timesheetdetailRepo.GetAll().Where(t => t.TsDate >= FromDate && t.TsDate <= ToDate).ToListAsync();
        //    int[] tsDetIds = rsTsDetail.Select(t => t.Id).ToArray();

        //    var rsTsEmps = await _timesheetemployeedetailRepo.GetAll().Where(t => tsDetIds.Contains(t.TsDetailRefId) && input.EmployeeRefId == t.EmployeeRefId).ToListAsync();

        //    tsDetIds = rsTsEmps.Select(t => t.TsDetailRefId).ToArray();
        //    rsTsDetail = await _timesheetdetailRepo.GetAll().Where(t => tsDetIds.Contains(t.Id)).ToListAsync();

        //    int[] tsMasIds = rsTsEmps.Select(t => t.TsMasterRefId).ToArray();
        //    var rsTsMaster = await _timesheetmasterRepo.GetAll().Where(t => tsMasIds.Contains(t.Id)).ToListAsync()
        //        ;

        //    var rsDutychartDetail = await _dutyChartDetailRepo.GetAll().Where(t => t.DutyChartDate >= FromDate && t.DutyChartDate <= ToDate && t.EmployeeRefId == input.EmployeeRefId).ToListAsync();

        //    #region ChartData

        //    MonthWiseTimeSheetData dto = new MonthWiseTimeSheetData();

        //    var pendingcountOverAll = rsDutychartDetail.Where(t => t.TimeSheetRefId == null).ToList().Count;

        //    var statusPendingOverAll = pendingcountOverAll;
        //    var yellowPendingOverAll = rsTsMaster.Where(t => t.SubmissionDate == null).Count();
        //    var PinkPendingOverAll = rsTsMaster.Where(t => t.SubmissionDate != null && t.SubmissionDateToInvoiceDepartment == null).Count();
        //    var InvoicePendingOverAll = rsTsMaster.Where(t => t.SubmissionDate != null && t.SubmissionDateToInvoiceDepartment != null && t.InvoiceDate == null).Count();
        //    var InvoiceDoneOverAll = rsTsMaster.Where(t => t.SubmissionDate != null && t.SubmissionDateToInvoiceDepartment != null && t.InvoiceDate != null).Count();

        //    dto.MonthStartDate = FromDate.ToString("dd") + " - " + ToDate.ToString("dd-MMM");

        //    dto.StatusPending = statusPendingOverAll;
        //    dto.YellowTimeSheetPending = yellowPendingOverAll;
        //    dto.PinkTimeSheetPending = PinkPendingOverAll;
        //    dto.InvoicePending = InvoicePendingOverAll;
        //    dto.InvoiceDone = InvoiceDoneOverAll;

        //    List<DayWiseTimeSheetData> detailoutput = new List<DayWiseTimeSheetData>();
        //    DateTime currentDate = FromDate;
        //    while (currentDate <= ToDate)
        //    {
        //        var pendingcount = rsDutychartDetail.Where(t => t.DutyChartDate == currentDate && t.IsBillable == true && t.TimeSheetRefId == null).ToList().Count;

        //        var statusPending = pendingcount;

        //        var tsDetailList = rsTsDetail.Where(t => t.TsDate.Date == currentDate.Date);
        //        int[] TsMasterRefids = tsDetailList.Select(t => t.TsMasterRefId).ToArray();

        //        var yellowPending = rsTsMaster.Where(t => TsMasterRefids.Contains(t.Id) && t.SubmissionDate == null).Count();

        //        var PinkPending = rsTsMaster.Where(t => TsMasterRefids.Contains(t.Id) && t.SubmissionDate != null && t.SubmissionDateToInvoiceDepartment == null).Count();

        //        var InvoicePending = rsTsMaster.Where(t => TsMasterRefids.Contains(t.Id) && t.SubmissionDate != null && t.SubmissionDateToInvoiceDepartment != null && t.InvoiceDate == null).Count();

        //        var InvoiceDone = rsTsMaster.Where(t => TsMasterRefids.Contains(t.Id) && t.SubmissionDate != null && t.SubmissionDateToInvoiceDepartment != null && t.InvoiceDate != null).Count();


        //        DayWiseTimeSheetData dayDto = new DayWiseTimeSheetData();
        //        dayDto.DateToBeDisplayed = currentDate;
        //        dayDto.DayDesc = currentDate.ToString("dd-MMM");

        //        dayDto.StatusPending = statusPending;
        //        dayDto.YellowTimeSheetPending = yellowPending;
        //        dayDto.PinkTimeSheetPending = PinkPending;
        //        dayDto.InvoicePending = InvoicePending;
        //        dayDto.InvoiceDone = InvoiceDone;

        //        detailoutput.Add(dayDto);
        //        currentDate = currentDate.AddDays(1);
        //    }


        //    #endregion

        //    GetEmployeeTsDashBoardDto output = new GetEmployeeTsDashBoardDto();
        //    output.chartData = detailoutput;
        //    output.OverAll = dto;

        //    return output;
        //}

        //public async Task<List<MessageOutput>> SendEmployeeStatusEmail(GetEmployeeAlertMailDto input)
        //{
        //    var request = HttpContext.Current.Request;
        //    string url = HttpContext.Current.Request.Url.AbsoluteUri;
        //    var baseUrl = request.Url.Scheme + "://" + request.Url.Authority;

        //    if (baseUrl.ToLower().Contains("localhost"))
        //    {
        //        return null;
        //    }

        //    MessageOutput retOutput = new MessageOutput();
        //    retOutput.SuccessFlag = true;

        //    var empDetails = await GetEmployeeDashBoardDutyChartData(new GetDashBoardDto
        //    {
        //        StartDate = input.StartDate,
        //        EndDate = input.EndDate,
        //        EmployeeRefId = input.EmployeeRefId,
        //    });

        //    string dtRange = input.StartDate.ToString("dd-MMM-yy") + " - " + input.EndDate.ToString("dd-MMM-yy");

        //    var emp = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == input.EmployeeRefId);
        //    retOutput.EmployeeRefId = emp.Id;
        //    retOutput.EmployeeRefName = emp.EmployeeName;
        //    retOutput.PersonalEmail = emp.PersonalEmail;
        //    retOutput.Email = emp.Email;
        //    retOutput.SuccessFlag = false;
        //    bool emailContentsExists = false;

        //    var company = await _companyRepo.FirstOrDefaultAsync(t => t.Id == emp.CompanyRefId);
        //    if (company == null)
        //    {
        //        throw new UserFriendlyException(L("Company" + " ?"));
        //    }

        //    var mailList = company.EmailIdList;
        //    if (mailList.IsNullOrEmpty())
        //    {
        //        throw new UserFriendlyException(L("Company") + L("EmailIdList") + " ?");
        //    }
        //    string[] emailList = mailList.Split(";");

        //    var mailMessage = new StringBuilder();

        //    var hroperationHealthEdp = await HrOperationHealthEdp(new GetHrOperationHealthDto
        //    {
        //        DaysTill = 45,
        //        EmployeeRefId = input.EmployeeRefId
        //    });

        //    retOutput.HrOperationHealthHtml = hroperationHealthEdp.HtmlMessage;
        //    retOutput.HrOperationHealthText = hroperationHealthEdp.ErrorMessage;

        //    mailMessage.AppendLine(hroperationHealthEdp.HtmlMessage);

        //    string htmlLine;

        //    htmlLine = "<p>Dear " + emp.EmployeeName + " ,</p> ";
        //    mailMessage.AppendLine(htmlLine);

        //    #region AssetManagement
        //    var assetReturn = await _assetAppService.GetEmployeeWiseAsset(new IdInput { Id = emp.Id });
        //    AssetEmployeeWiseDto ea;
        //    if (assetReturn.Count > 0)
        //    {
        //        emailContentsExists = true;
        //        ea = assetReturn[0];
        //        htmlLine = "<p>The Following Equipement Assets are in your pending List.</p> ";
        //        mailMessage.AppendLine(htmlLine);

        //        #region StatusPrinting
        //        htmlLine = "<p><span style=\"color: #ffff00; background-color: #000000;\"><strong>Equipment Asset List Consolidated</strong></span></p>";
        //        mailMessage.AppendLine(htmlLine);

        //        htmlLine = "<table border = \"2\">";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<tbody>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<tr>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<td style = \"text-align: center;\" width = \"250\">";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<p>Desc</p>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</td>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<td style = \"text-align: center;\" width = \"100\"> ";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<p> # </p>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</td>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</tr>";
        //        mailMessage.AppendLine(htmlLine);
        //        int sno = 1;
        //        foreach (var lst in ea.MaterialStatusDto)
        //        {
        //            htmlLine = "<tr>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<td style = \"text-align: left;\" width = \"250\">";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<p> " + lst.Status + " </p>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "</td>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<td style = \"text-align: center;\"  width = \"100\">";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<p>" + lst.Count + " </p> ";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "</td>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "</tr>";
        //            sno++;
        //        }

        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</tbody>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</table>";
        //        mailMessage.AppendLine(htmlLine);

        //        #endregion
        //        htmlLine = "<br> ";
        //        mailMessage.AppendLine(htmlLine);

        //        #region AssetlistPrinting
        //        htmlLine = "<p><span style=\"color: #ffff00; background-color: #000000;\"><strong>Equipment Asset List</strong></span></p>";
        //        mailMessage.AppendLine(htmlLine);

        //        htmlLine = "<table border = \"2\">";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<tbody>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<tr>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<td style = \"text-align: center;\" width = \"250\">";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<p>Machine</p>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</td>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<td style = \"text-align: center;\" width = \"120\"> ";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<p> Tagcode </p>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</td>";
        //        htmlLine = "<td style = \"text-align: center;\" width = \"500\"> ";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<p> Remarks </p>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</td>";
        //        htmlLine = "<td style = \"text-align: center;\" width = \"170\"> ";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<p> Status </p>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</td>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</tr>";
        //        mailMessage.AppendLine(htmlLine);
        //        foreach (var lst in ea.AssetMaterialWiseList)
        //        {
        //            htmlLine = "<tr>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<td style = \"text-align: center;\" width = \"250\">";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<p> " + lst.MaterialRefName + " </p>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "</td>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<td style = \"text-align: center;\" width = \"120\">";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<p>" + lst.AssetTagCode + " </p> ";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "</td>";
        //            htmlLine = "<td style = \"text-align: center;\" width = \"500\">";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<p>" + lst.Remarks + " </p> ";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "</td>";

        //            htmlLine = "<td style = \"text-align: center;\" width = \"120\">";
        //            mailMessage.AppendLine(htmlLine);
        //            if (lst.IsRepair)
        //            {
        //                string desc = "";
        //                if (lst.RepairOn != null)
        //                {
        //                    desc = lst.RepairOn.Value.ToString("yyyy-MMM-dd");
        //                }
        //                desc = desc + "/" + lst.RepairDescription;
        //                htmlLine = "<span style=\"Color:red; font-size:large; \">" + desc + " </span> ";
        //                mailMessage.AppendLine(htmlLine);
        //                htmlLine = "<br> ";
        //                mailMessage.AppendLine(htmlLine);
        //            }
        //            else
        //            {
        //                htmlLine = "<span >" + lst.WorkingStatus + " </span> ";
        //                mailMessage.AppendLine(htmlLine);
        //                htmlLine = "<br> ";
        //                mailMessage.AppendLine(htmlLine);
        //            }
        //            if (lst.AssetStatus != null)
        //            {
        //                htmlLine = "<span style=\"Color:red;font-size:large; \">" + lst.AssetStatus + " </span> ";
        //                mailMessage.AppendLine(htmlLine);
        //            }

        //            htmlLine = "</td>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "</tr>";
        //            sno++;
        //        }

        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</tbody>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</table>";
        //        mailMessage.AppendLine(htmlLine);

        //        #endregion
        //        htmlLine = "<br> ";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "If you have any queries , kindly contact the respective persons immediately.";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<br> ";
        //        mailMessage.AppendLine(htmlLine);
        //    }

        //    htmlLine = "<br> ";
        //    mailMessage.AppendLine(htmlLine);
        //    htmlLine = "<span style = \"color: #ff0000;\">Please keep Equipments & Accessories Clean and Safely.If the Equipment Lost/broken or repaired due to employee mishandling, the Company have the rights to deduct the repair cost from employee Salary. So be vigilant to handle equipment & accessories.</span>";
        //    mailMessage.AppendLine(htmlLine);
        //    htmlLine = "<br> ";
        //    mailMessage.AppendLine(htmlLine);

        //    #endregion

        //    #region PreviousMonthPendingList
        //    if (empDetails.MonthWisePendingList != null && empDetails.MonthWisePendingList.Count > 0)
        //    {
        //        emailContentsExists = true;
        //        htmlLine = "<p><span style=\"color: #ffff00; background-color: #000000;\"><strong>Pending Month Wise</strong></span></p>";
        //        mailMessage.AppendLine(htmlLine);

        //        htmlLine = "<table border = \"2\">";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<tbody>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<tr>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<td style = \"text-align: center;\" width = \"120\">";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<p>Month</p>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</td>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<td style = \"text-align: center;\" width = \"120\"> ";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<p> Pending </p>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</td>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<td style = \"text-align: center;\" width = \"120\">";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<p> Yellow </p>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</td>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<td style = \"text-align: center;\" width = \"120\">";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<p> Pink </p>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</td>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<td style = \"text-align: center;\" width = \"120\">";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<p> Invoice Pending </p>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</td>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<td style = \"text-align: center;\" width = \"120\"> ";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<p> Invoice Done </p>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</td>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</tr>";
        //        mailMessage.AppendLine(htmlLine);
        //        int sno = 1;
        //        foreach (var lst in empDetails.MonthWisePendingList)
        //        {
        //            htmlLine = "<tr>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<td style = \"text-align: center;\" width = \"120\">";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<p> " + lst.Month + " </p>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "</td>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<td style = \"text-align: center;\" width = \"120\">";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<p>" + lst.Pending + " </p> ";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "</td>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<td style = \"text-align: center;\" width = \"120\"> ";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<p> " + lst.YellowPending + " </p> ";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "</td>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<td style = \"text-align: center;\" width = \"120\">";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<p> " + lst.PinkPending + " </p> ";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "</td>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<td style = \"text-align: center;\" width = \"120\"> ";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<p>" + lst.InvoicePending + "</p>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "</td>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<td style = \"text-align: center;\" width = \"120\"> ";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<p>" + lst.InvoiceDone + "</p>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "</td>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "</tr>";
        //            sno++;
        //        }

        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</tbody>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</table>";
        //        mailMessage.AppendLine(htmlLine);

        //    }
        //    #endregion
        //    string headerText = "";
        //    if (empDetails.YellowPending > 0 && empDetails.PinkPending > 0)
        //        headerText = L("YellowPinkBothPending", empDetails.YellowPending, empDetails.PinkPending, dtRange);
        //    else if (empDetails.YellowPending > 0)
        //        headerText = L("YellowPendingOnly", empDetails.YellowPending, empDetails.PinkPending, dtRange);
        //    else if (empDetails.PinkPending > 0)
        //        headerText = L("PinkPendingOnly", empDetails.YellowPending, empDetails.PinkPending, dtRange);
        //    else
        //        headerText = L("NoTimeSheetPending", dtRange);


        //    htmlLine = "<p>" + headerText + "</p>";
        //    mailMessage.AppendLine(htmlLine);

        //    #region YellowPrint
        //    if (empDetails.YellowPending > 0)
        //    {
        //        emailContentsExists = true;
        //        htmlLine = "<p><span style=\"color: #ffff00; background-color: #000000;\"><strong>Pending YELLOW Time Sheets Days</strong></span></p>";
        //        mailMessage.AppendLine(htmlLine);

        //        htmlLine = "<table border = \"2\">";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<tbody>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<tr>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<td style = \"text-align: center;\" width = \"90\">";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<p>#</p>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</td>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<td style = \"text-align: center;\" width = \"130\"> ";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<p> Date </p>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</td>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<td style = \"text-align: center;\" width = \"345\">";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<p> Client </p>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</td>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<td style = \"text-align: center;\" width = \"275\">";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<p> Cost Centre </p>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</td>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<td style = \"text-align: center;\" width = \"100\">";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<p> JOB </p>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</td>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<td style = \"text-align: center;\" width = \"100\"> ";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<p> Due Date </p>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</td>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</tr>";
        //        mailMessage.AppendLine(htmlLine);
        //        int sno = 1;
        //        foreach (var lst in empDetails.YellowPendingList)
        //        {
        //            htmlLine = "<tr>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<td style = \"text-align: center;\" width = \"90\">";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<p> " + sno + " </p>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "</td>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<td style = \"text-align: center;\" width = \"130\">";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<p>" + lst.DutyChartDate.ToString("dd-MMM-yy (ddd)") + " </p> ";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "</td>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<td style = \"text-align: center;\" width = \"345\"> ";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<p> " + lst.ClientRefName + " </p> ";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "</td>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<td style = \"text-align: center;\" width = \"275\">";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<p> " + lst.CostCentreRefCode + " </p> ";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "</td>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<td style = \"text-align: center;\" width = \"100\"> ";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<p>" + lst.SkillRefCode + "</p>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "</td>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<td style = \"text-align: center;\" width = \"100\"> ";
        //            mailMessage.AppendLine(htmlLine);
        //            if (lst.TimeSheetDueDate != null)
        //                htmlLine = "<p>" + lst.TimeSheetDueDate == null ? "" : lst.TimeSheetDueDate.Value.ToString("dd-MMM-yy") + "</p>";
        //            else
        //                htmlLine = "";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "</td>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "</tr>";
        //            sno++;
        //        }

        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</tbody>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</table>";
        //        mailMessage.AppendLine(htmlLine);

        //    }

        //    #endregion

        //    #region PinkPendingPrint
        //    if (empDetails.PinkPending > 0)
        //    {
        //        emailContentsExists = true;
        //        htmlLine = "<p><span style=\"color: #ffff00; background-color: #000000;\"><strong>Pending PINK Time Sheets Days (Not yet get the Client Signature)</strong></span></p>";
        //        mailMessage.AppendLine(htmlLine);

        //        htmlLine = "<table border = \"2\">";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<tbody>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<tr>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<td style = \"text-align: center;\" width = \"90\">";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<p>#</p>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</td>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<td style = \"text-align: center;\" width = \"130\"> ";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<p> Date </p>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</td>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<td style = \"text-align: center;\" width = \"345\">";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<p> Client </p>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</td>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<td style = \"text-align: center;\" width = \"275\">";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<p> Cost Centre </p>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</td>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<td style = \"text-align: center;\" width = \"100\">";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<p> JOB </p>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</td>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<td style = \"text-align: center;\" width = \"100\"> ";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<p> Yellow Submitted On </p>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</td>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</tr>";
        //        mailMessage.AppendLine(htmlLine);
        //        int sno = 1;
        //        foreach (var lst in empDetails.PinkPendingList)
        //        {
        //            htmlLine = "<tr>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<td style = \"text-align: center;\" width = \"90\">";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<p> " + sno + " </p>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "</td>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<td style = \"text-align: center;\" width = \"130\">";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<p>" + lst.DutyChartDate.ToString("dd-MMM-yy (ddd)") + " </p> ";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "</td>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<td style = \"text-align: center;\" width = \"345\"> ";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<p> " + lst.ClientRefName + " </p> ";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "</td>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<td style = \"text-align: center;\" width = \"275\">";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<p> " + lst.CostCentreRefCode + " </p> ";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "</td>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<td style = \"text-align: center;\" width = \"100\"> ";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<p>" + lst.SkillRefCode + "</p>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "</td>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<td style = \"text-align: center;\" width = \"100\"> ";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "<p>" + lst.YellowSubmissionDate.Value.ToString("dd-MMM-yy") + "</p>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "</td>";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "</tr>";
        //            sno++;
        //        }

        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</tbody>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</table>";
        //        mailMessage.AppendLine(htmlLine);

        //    }

        //    #endregion

        //    if (empDetails.YellowPending > 0 || empDetails.PinkPending > 0)
        //    {
        //        htmlLine = "<p><strong>If you submit any of the time sheets already, please contact the respective in charge immediately and correct it for your Salary process.</strong></p>";
        //        mailMessage.AppendLine(htmlLine);
        //    }

        //    #region IdleDaysPrint

        //    if (empDetails.IdleDays > 0)
        //    {
        //        emailContentsExists = true;
        //        htmlLine = "<p> You have been in <strong> Idle bench </strong> for the following dates.</p> ";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<p><span style = \"color: #ff0000;\"><strong> IDLE DATES </strong></span></p>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<table border = \"2\">";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<tbody>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<tr>";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = " <td style = \"text-align: center;\" width = \"90\">";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<p>#</p> ";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</td> ";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<td style = \"text-align: center;\" width = \"130\"> ";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "<p> Date </p> ";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "</td> ";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = " </tr> ";
        //        mailMessage.AppendLine(htmlLine);

        //        int sno = 1;
        //        foreach (var lst in empDetails.IdleList)
        //        {
        //            htmlLine = " <tr> ";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "   <td style = \"text-align: center;\" width = \"90\">";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "       <p> " + sno + " </p> ";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "        </td> ";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "        <td style = \"text-align: center;\" width = \"130\"> ";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "       <p> " + lst.DutyChartDate.ToString("dd-MMM-yy (ddd)") + "  </p> ";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "        </td> ";
        //            mailMessage.AppendLine(htmlLine);
        //            htmlLine = "      </tr> ";
        //            mailMessage.AppendLine(htmlLine);
        //            sno++;
        //        }
        //        htmlLine = "         </tbody> ";
        //        mailMessage.AppendLine(htmlLine);
        //        htmlLine = "         </table>";
        //        mailMessage.AppendLine(htmlLine);
        //    }

        //    #endregion

        //    htmlLine = " <p> Please verify all the details and If you found any errors please let us know immediately by reply email to the below email ids.</p>";
        //    mailMessage.AppendLine(htmlLine);
        //    htmlLine = " <p> " + mailList + " </p>";
        //    mailMessage.AppendLine(htmlLine);
        //    htmlLine = " <p> 	Make sure all the time sheets should reach office on time and enable us to submit the invoice on time to Clients.</p> ";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "  <p> &nbsp;</p> ";
        //    mailMessage.AppendLine(htmlLine);
        //    htmlLine = "   <p> HR / EDP </p>";
        //    mailMessage.AppendLine(htmlLine);
        //    htmlLine = "     <p> A-STAR GROUP </p> ";
        //    mailMessage.AppendLine(htmlLine);
        //    htmlLine = "    <p> &nbsp;</p>";
        //    mailMessage.AppendLine(htmlLine);
        //    htmlLine = "  <p> &nbsp;</p> ";
        //    mailMessage.AppendLine(htmlLine);
        //    htmlLine = "  <p> &nbsp;</p> ";
        //    mailMessage.AppendLine(htmlLine);

        //    if (input.MailSendFlag == true && emailContentsExists == true)
        //    {
        //        try
        //        {
        //            SmtpClient client = new SmtpClient();
        //            client.DeliveryMethod = SmtpDeliveryMethod.Network;
        //            client.EnableSsl = true;
        //            client.Host = "smtp.gmail.com";
        //            client.Port = 587;

        //            System.Net.NetworkCredential credentials =
        //                new System.Net.NetworkCredential("astaredp-sg@astartesting.com.sg", "astaredp2016");
        //            client.UseDefaultCredentials = false;
        //            client.Credentials = credentials;

        //            MailMessage msg = new MailMessage();
        //            msg.From = new MailAddress("astaredp-sg@astartesting.com.sg");
        //            msg.Bcc.Add(new MailAddress("karaneeswara@gmail.com"));
        //            if (!emp.Email.IsNullOrEmpty())
        //                msg.To.Add(new MailAddress(emp.Email));
        //            if (!emp.PersonalEmail.IsNullOrEmpty())
        //                msg.To.Add(new MailAddress(emp.PersonalEmail));

        //            if (emp.Email.IsNullOrEmpty() && emp.PersonalEmail.IsNullOrEmpty())
        //            {
        //                htmlLine = " <p> <B> Note :	The Employee Does Not Have The Valid Email Id , so you received this mail and please update the employee email id immediately. </B></p> ";
        //                mailMessage.AppendLine(htmlLine);

        //                foreach (var em in emailList)
        //                {
        //                    msg.To.Add(new MailAddress(em));
        //                }
        //            }

        //            msg.Subject = "ASTAR" + " - ASSET HOLDING BY YOUR GOODSELF / JOB STATUS (" + input.StartDate.ToString("dd-MMM-yy") + " - " + input.EndDate.ToString("dd-MMM-yy") + ") "; // "This is a test Email subject";
        //            msg.IsBodyHtml = true;
        //            //msg.Body = string.Format("<html><head></head><body><b>Test HTML Email</b></body>");
        //            msg.Body = mailMessage.ToString();
        //            try
        //            {
        //                if (msg.To.Count > 0)
        //                    client.Send(msg);
        //            }
        //            catch (Exception ex)
        //            {
        //                retOutput.SuccessFlag = false;
        //                retOutput.ErrorMessage = L("NotAbleToSendMailError", ex.Message + ex.InnerException);
        //                //throw new Abp.UI.UserFriendlyException(L("NotAbleToSendMailError", ex.Message + ex.InnerException));
        //            }
        //            emailContentsExists = false;
        //            retOutput.SuccessFlag = true;
        //        }
        //        catch (Exception ex)
        //        {
        //            retOutput.SuccessFlag = false;
        //            retOutput.ErrorMessage = L("NotAbleToSendMailError", ex.Message + ex.InnerException);
        //            throw;
        //        }
        //    }

        //    retOutput.Message = mailMessage.ToString();
        //    retOutput.HtmlMessage = mailMessage.ToString();

        //    List<MessageOutput> retList = new List<MessageOutput>();
        //    retList.Add(retOutput);
        //    return retList;
        //}

        //public async Task<List<MessageOutput>> ToAllEmployeeSendStatusEmail(GetEmployeeAlertMailDto input)
        //{
        //    var request = HttpContext.Current.Request;
        //    string url = HttpContext.Current.Request.Url.AbsoluteUri;
        //    var baseUrl = request.Url.Scheme + "://" + request.Url.Authority;

        //    if (baseUrl.ToLower().Contains("localhost"))
        //    {
        //        return null;
        //    }

        //    var retList = new List<MessageOutput>();
        //    DateTime dateLeft = DateTime.Today.AddDays(-10);
        //    var emps = await _personalinformationRepo.GetAllListAsync(t => t.LastWorkingDate == null || t.LastWorkingDate.Value > dateLeft);

        //    foreach (var emp in emps)
        //    {
        //        var empDto = new GetEmployeeAlertMailDto
        //        {
        //            EmployeeRefId = emp.Id,
        //            StartDate = input.StartDate,
        //            EndDate = input.EndDate,
        //            MailSendFlag = input.MailSendFlag
        //        };

        //        var mailDetail = await SendEmployeeStatusEmail(empDto);
        //        retList.AddRange(mailDetail);
        //    }
        //    return retList;

        //}

        public async Task<MessageOutput> HrOperationHealthEdp(GetHrOperationHealthDto input)
        {
            MessageOutput output = new MessageOutput();

            string ConsolidatedPlainErrorMessage = "";

            #region Leave_But_FingerPrintFoundError
            if (input.DaysTill == null)
                input.DaysTill = 1;
            var startDate = DateTime.Today.AddDays(-1 * input.DaysTill.Value);
            var endDate = DateTime.Today.AddDays(-1 * input.DaysTill.Value);
            if (input.StartDate.HasValue)
            {
                startDate = input.StartDate.Value;
            }
            if (input.EndDate.HasValue)
            {
                endDate = input.EndDate.Value;
            }

            var rejectedString = L("Rejected");
            var rsLeaveRequest = _leaveRequestRepo.GetAll().WhereIf(input.EmployeeRefId.HasValue, t => t.EmployeeRefId == input.EmployeeRefId && t.LeaveStatus != rejectedString && t.HalfDayFlag == false);

            var leaveRequest = await rsLeaveRequest.Where(t => t.LeaveTo >= startDate && t.HalfDayFlag == false).OrderBy(t => t.EmployeeRefId).ToListAsync();
            var arrEmpRefIds = leaveRequest.Select(t => t.EmployeeRefId).ToArray();

            var rsAttLog = await _attendanceLogRepo.GetAll().Where(t => arrEmpRefIds.Contains(t.EmployeeRefId) && t.CheckTime >= startDate && t.CheckTime.Value <= endDate).ToListAsync();

            var perinfoQueryable = _personalinformationRepo.GetAll();
            if (input.InchargeEmployeeRefId.HasValue)
            {
                perinfoQueryable = perinfoQueryable.Where(t => t.InchargeEmployeeRefId == input.InchargeEmployeeRefId.Value);
            }

            var rsPerinfo = await perinfoQueryable.ToListAsync();

            bool errorFlag = false;
            string errorMessage = "";
            foreach (var lst in leaveRequest)
            {
                var attlogsExist = rsAttLog.Where(t => t.EmployeeRefId == lst.EmployeeRefId && t.CheckTime >= lst.LeaveFrom && t.CheckTime <= lst.LeaveTo).OrderBy(t => t.CheckTime).ToList();
                if (attlogsExist.Count > 0)
                {
                    var emp = rsPerinfo.FirstOrDefault(t => t.Id == lst.EmployeeRefId);
                    if (emp != null)
                    {
                        var arrTimeFound = "";
                        foreach (var ti in attlogsExist)
                        {
                            if (ti.CheckTime.Value.Hour < 6)
                            {
                                var prevDate = ti.CheckTime.Value.AddDays(-1);
                                var dutychart = await _dutyChartDetailRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == ti.EmployeeRefId && DbFunctions.TruncateTime(t.DutyChartDate) == DbFunctions.TruncateTime(prevDate));
                                if (dutychart != null)
                                {
                                    if (dutychart.NightFlag == true)
                                        continue;
                                }
                            }
                            arrTimeFound = arrTimeFound + ti.CheckTime.Value.ToString("yyyy-MMM-dd hh:mm tt") + ",";
                            errorFlag = true;
                        }
                        if (arrTimeFound.Length > 0)
                            arrTimeFound = arrTimeFound.Left(arrTimeFound.Length - 1);
                        ConsolidatedPlainErrorMessage = ConsolidatedPlainErrorMessage + L("LeaveButFingerFoundError", emp.EmployeeName, lst.LeaveFrom.ToString("yyyy-MMM-dd"), lst.LeaveTo.ToString("yyyy-MMM-dd"), arrTimeFound) + "<BR><br>";

                        errorMessage = errorMessage + L("LeaveButFingerFoundError", emp.EmployeeName, lst.LeaveFrom.ToString("yyyy-MMM-dd"), lst.LeaveTo.ToString("yyyy-MMM-dd"), arrTimeFound) + "<BR> <BR>";
                    }
                }
            }

            if (errorFlag == true)
            {
                errorMessage = "<span>" + errorMessage + "<span>";

                output.HtmlMessage = output.HtmlMessage + "<span STYLE='Color:White;Background-Color:OrangeRed'>" + L("LeaveButFingerFoundErrorHeader") + "</span>";
                output.HtmlMessage = output.HtmlMessage + "<BR>" + errorMessage + "<HR/>";

                ConsolidatedPlainErrorMessage = L("LeaveButFingerFoundErrorHeader") + "<br>" + ConsolidatedPlainErrorMessage;
            }
            errorFlag = false;
            errorMessage = "";
            #endregion

            #region Absent_Leave_In_DutyChart_But_FingerPrintFoundError
            string[] statusString = { L("Leave"), L("Absent"), L("WeekOff"), L("PH") };
            //effectiveFrom = DateTime.Today.AddDays(-1 * 10);
            rsAttLog = await _attendanceLogRepo.GetAll().Where(t => t.CheckTime >= startDate && t.CheckTime.Value <= endDate).ToListAsync();
            var rsDutychart = _dutyChartDetailRepo.GetAll().WhereIf(input.EmployeeRefId.HasValue, t => t.EmployeeRefId == input.EmployeeRefId);
            var leaveAbsentFoundList = await rsDutychart.Where(t => t.DutyChartDate >= startDate && t.DutyChartDate <= endDate && statusString.Contains(t.AllottedStatus)).OrderBy(t => t.EmployeeRefId).ToListAsync();
            arrEmpRefIds = leaveAbsentFoundList.Select(t => t.EmployeeRefId).ToArray();

            foreach (var lst in leaveAbsentFoundList)
            {
                var attlogsExist = rsAttLog.Where(t => t.EmployeeRefId == lst.EmployeeRefId && t.CheckTime.Value.Date == lst.DutyChartDate.Date).OrderBy(t => t.CheckTime.Value).ToList();
                if (attlogsExist.Count > 0)
                {
                    var emp = rsPerinfo.FirstOrDefault(t => t.Id == lst.EmployeeRefId);
                    if (emp != null)
                    {
                        var arrTimeFound = "";
                        foreach (var ti in attlogsExist)
                        {
                            if (ti.CheckTime.Value.Hour < 6)
                            {
                                var prevDate = ti.CheckTime.Value.AddDays(-1);
                                var dutychart = await _dutyChartDetailRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == ti.EmployeeRefId && DbFunctions.TruncateTime(t.DutyChartDate) == DbFunctions.TruncateTime(prevDate));
                                if (dutychart != null)
                                {
                                    if (dutychart.NightFlag == true)
                                        continue;
                                }
                            }
                            arrTimeFound = arrTimeFound + ti.CheckTime.Value.ToString("yyyy-MMM-dd hh:mm tt") + ",";
                        }

                        if (arrTimeFound.Length > 0)
                        {
                            errorFlag = true;
                            arrTimeFound = arrTimeFound.Left(arrTimeFound.Length - 1);
                        }
                        else
                        {
                            continue;
                        }

                        ConsolidatedPlainErrorMessage = ConsolidatedPlainErrorMessage + L("AbsentOrLeaveButFingerFoundError", emp.EmployeeName, lst.DutyChartDate.ToString("yyyy-MMM-dd"), lst.AllottedStatus, arrTimeFound) + "<br>";

                        errorMessage = errorMessage + L("AbsentOrLeaveButFingerFoundError", emp.EmployeeName, lst.DutyChartDate.ToString("yyyy-MMM-dd"), lst.AllottedStatus, arrTimeFound) + "<BR> <br>";
                    }
                }
            }

            if (errorFlag == true)
            {
                output.HtmlMessage = output.HtmlMessage + "<Span STYLE='Color:White;Background-Color:OrangeRed;font-size:medium'>" + L("AbsentOrLeaveButFingerFoundErrorHeader") + "</Span>";
                output.HtmlMessage = output.HtmlMessage + "<BR><BR>" + errorMessage + "<HR/>";

                ConsolidatedPlainErrorMessage = L("AbsentOrLeaveButFingerFoundErrorHeader") + "<BR><br>" + ConsolidatedPlainErrorMessage;
            }
            errorFlag = false;
            errorMessage = "";

            #endregion



            output.ErrorMessage = ConsolidatedPlainErrorMessage;
            return output;
        }

        public async Task<MessageOutput> HrOperationAlert(GetHrOperationHealthDto input)
        {
            MessageOutput output = new MessageOutput();
            bool oneDayFlag = false;
            string ConsolidatedPlainErrorMessage = "";

            #region Idle_But_NoThump_In_Office
            DateTime effectiveFrom;
            DateTime maxDate;
            if (input.StartDate.HasValue && input.EndDate.HasValue)
            {
                effectiveFrom = input.StartDate.Value;
                maxDate = effectiveFrom;
                oneDayFlag = true;
            }
            else
            {
                oneDayFlag = false;
                if (input.DaysTill == null)
                    input.DaysTill = 10;
                effectiveFrom = DateTime.Today.AddDays(-1 * input.DaysTill.Value);
                var attLocation = await _attendanceMachineLocationRepo.FirstOrDefaultAsync(t => t.Id > 0);
                if (attLocation == null)
                {
                    throw new UserFriendlyException(L("AttendanceLocation"));
                }
                var maxattLog = _attendanceLogRepo.GetAll().Where(t => t.AttendanceMachineLocationRefId.HasValue &&  t.AttendanceMachineLocationRefId == attLocation.Id && t.CheckTime > effectiveFrom);
                if (maxattLog.Any() == false)
                {
                    return output;
                }
                maxDate = effectiveFrom;
                var totalCount = maxattLog.Count();

                if (totalCount > 0)
                {
                    var maxAttDate = maxattLog.Max(t => t.CheckTime.Value);
                    maxDate = maxAttDate;
                }
                if (maxDate < effectiveFrom)
                    return output;
            }

            var rsDutychart = _dutyChartDetailRepo.GetAll().WhereIf(input.EmployeeRefId.HasValue, t => t.EmployeeRefId == input.EmployeeRefId);
            string idleString = L("Idle");
            var idleDutyList = await rsDutychart.Where(t => t.AllottedStatus.Equals(idleString) && t.DutyChartDate >= effectiveFrom && t.DutyChartDate <= maxDate).OrderBy(t => t.EmployeeRefId).ToListAsync();
            var arrEmpRefIds = idleDutyList.Select(t => t.EmployeeRefId).Distinct().ToArray();

            var rsAttLog = await _attendanceLogRepo.GetAll().Where(t => arrEmpRefIds.Contains(t.EmployeeRefId) && t.CheckTime >= effectiveFrom).ToListAsync();

            var perinfoQueryable = _personalinformationRepo.GetAll();
            if (input.InchargeEmployeeRefId.HasValue)
            {
                perinfoQueryable = perinfoQueryable.Where(t => t.InchargeEmployeeRefId == input.InchargeEmployeeRefId.Value);
            }

            var rsPerinfo = await perinfoQueryable.ToListAsync();

            var uptoTime = maxDate.AddDays(1);
            var rsGpsAttLog = await _gpsAttendanceRepo.GetAll().Where(t => arrEmpRefIds.Contains(t.EmployeeRefId)
                                && t.CreationTime >= effectiveFrom && t.CreationTime <= uptoTime).ToListAsync();


            var startDate = effectiveFrom.AddDays(-30);
            var rsBiometricExcluded = await _biometricExcludedRepo.GetAll()
                    .Where(t => arrEmpRefIds.Contains(t.EmployeeRefId) && (t.ExcludeFromDate > startDate) && (t.ExcludeToDate <= uptoTime)).ToListAsync();

            bool errorFlag = false;
            string errorMessage = "";
            foreach (var lst in idleDutyList)
            {
                var emp = rsPerinfo.FirstOrDefault(t => t.Id == lst.EmployeeRefId);
                if (emp == null)
                    continue;

                if (emp.IsAttendanceRequired == false)
                    continue;

                var excludedList = rsBiometricExcluded.Where(t => t.EmployeeRefId == lst.EmployeeRefId && t.ExcludeFromDate.Date >= lst.DutyChartDate.Date
                                && t.ExcludeToDate.Date <= lst.DutyChartDate.Date).ToList();
                if (excludedList.Count > 0)
                    continue;
                else
                {
                    int I = 9;
                }


                var attlogsExist = rsAttLog.Where(t => t.EmployeeRefId == lst.EmployeeRefId && t.CheckTime.Value.Date >= lst.DutyChartDate && t.CheckTime.Value.Date <= lst.DutyChartDate).OrderBy(t => t.CheckTime).ToList();
                if (attlogsExist.Count > 0)
                {
                    var arrTimeFound = "";
                    foreach (var ti in attlogsExist)
                    {
                        if (ti.CheckTime.Value.Hour < 6)
                        {
                            var prevDate = ti.CheckTime.Value.AddDays(-1);
                            var dutychart = await _dutyChartDetailRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == ti.EmployeeRefId && DbFunctions.TruncateTime(t.DutyChartDate) == DbFunctions.TruncateTime(prevDate));
                            if (dutychart != null)
                            {
                                if (dutychart.NightFlag == true)
                                    continue;
                            }
                            arrTimeFound = arrTimeFound + ti.CheckTime.Value.ToString("yyyy-MMM-dd hh:mm") + ",";
                        }
                        else
                        {
                            arrTimeFound = arrTimeFound + ti.CheckTime.Value.ToString("yyyy-MMM-dd hh:mm") + ",";
                            break;
                        }
                    }
                    if (arrTimeFound.Length > 0)
                    {
                        arrTimeFound = arrTimeFound.Left(arrTimeFound.Length - 1);
                    }
                    else
                    {
                        if (oneDayFlag == true)
                        {
                            ConsolidatedPlainErrorMessage = ConsolidatedPlainErrorMessage + emp.EmployeeName + " <br>";
                            errorMessage = errorMessage + emp.EmployeeName + " <BR>";
                        }
                        else
                        {
                            ConsolidatedPlainErrorMessage = ConsolidatedPlainErrorMessage + L("IdleButFingerNotFoundError", emp.EmployeeName, lst.DutyChartDate.ToString("yyyy-MMM-dd")) + "<br>";

                            errorMessage = errorMessage + L("IdleButFingerNotFoundError", emp.EmployeeName, lst.DutyChartDate.ToString("yyyy-MMM-dd")) + "<BR> <BR>";
                        }
                        errorFlag = true;
                    }
                }
                else
                {
                    // Check GPS Attendance Exists

                    var gpsattlogsExist = rsGpsAttLog.Where(t => t.EmployeeRefId == lst.EmployeeRefId
                                    && t.CreationTime.Date >= lst.DutyChartDate && t.CreationTime.Date <= lst.DutyChartDate).OrderBy(t => t.CreationTime).ToList();
                    if (gpsattlogsExist.Count > 0)
                    {
                        continue;
                    }
                    if (oneDayFlag == true)
                    {
                        ConsolidatedPlainErrorMessage = ConsolidatedPlainErrorMessage + emp.EmployeeName + " <br>";
                        errorMessage = errorMessage + emp.EmployeeName + "<BR>";
                    }
                    else
                    {
                        ConsolidatedPlainErrorMessage = ConsolidatedPlainErrorMessage + L("IdleButFingerNotFoundError", emp.EmployeeName, lst.DutyChartDate.ToString("yyyy-MMM-dd")) + "<br>";

                        errorMessage = errorMessage + L("IdleButFingerNotFoundError", emp.EmployeeName, lst.DutyChartDate.ToString("yyyy-MMM-dd")) + "<BR> <BR>";
                    }
                    errorFlag = true;
                }
            }

            if (errorFlag == true)
            {
                errorMessage = "<span>" + errorMessage + "<span>";

                output.HtmlMessage = output.HtmlMessage + "<span STYLE='Color:Blue;Background-Color:White;font-size:medium'>" + L("IdleButFingerNotFoundErrorHeader") + "</span> <BR>";
                output.HtmlMessage = output.HtmlMessage + "<BR>" + errorMessage + "<HR/>";

                ConsolidatedPlainErrorMessage = L("IdleButFingerNotFoundErrorHeader") + "<br> <br>" + ConsolidatedPlainErrorMessage;
            }
            errorFlag = false;
            errorMessage = "";
            #endregion

            output.ErrorMessage = ConsolidatedPlainErrorMessage;
            return output;
        }

        public async Task<MessageOutput> HrOfficeJobAlert(GetHrOperationHealthDto input)
        {
            MessageOutput output = new MessageOutput();
            bool oneDayFlag = false;
            string ConsolidatedPlainErrorMessage = "";

            #region OfficeJob_But_NoThump_In_Office
            DateTime effectiveFrom;
            DateTime maxDate;
            if (input.StartDate.HasValue && input.EndDate.HasValue)
            {
                effectiveFrom = input.StartDate.Value;
                maxDate = effectiveFrom;
                oneDayFlag = true;
            }
            else
            {
                oneDayFlag = false;
                if (input.DaysTill == null)
                    input.DaysTill = 10;

                effectiveFrom = DateTime.Today.AddDays(-1 * input.DaysTill.Value);

                var attLocation = await _attendanceMachineLocationRepo.FirstOrDefaultAsync(t => t.Id > 0);
                if (attLocation == null)
                {
                    throw new UserFriendlyException(L("AttendanceLocation"));
                }
                var maxattLog = _attendanceLogRepo.GetAll().Where(t => t.AttendanceMachineLocationRefId.HasValue &&  t.AttendanceMachineLocationRefId == attLocation.Id && t.CheckTime > effectiveFrom);

                if (maxattLog.Any() == false)
                {
                    return output;
                }

                maxDate = effectiveFrom;
                var totalCount = maxattLog.Count();

                if (totalCount > 0)
                {
                    var maxAttDate = maxattLog.Max(t => t.CheckTime.Value);

                    maxDate = maxAttDate;
                }
                if (maxDate < effectiveFrom)
                    return output;
            }

            var rsDutychart = _dutyChartDetailRepo.GetAll().WhereIf(input.EmployeeRefId.HasValue, t => t.EmployeeRefId == input.EmployeeRefId);
            string jobString = L("Job");
            string locationString = L("Office");

            var officeJobDutyList = await rsDutychart.Where(t => t.AllottedStatus.Equals(jobString)
                                        && t.DutyChartDate >= effectiveFrom && t.DutyChartDate <= maxDate).OrderBy(t => t.EmployeeRefId).ToListAsync();
            var arrEmpRefIds = officeJobDutyList.Select(t => t.EmployeeRefId).Distinct().ToArray();

            var uptoTime = maxDate.AddDays(1);
            var rsAttLog = await _attendanceLogRepo.GetAll().Where(t => arrEmpRefIds.Contains(t.EmployeeRefId)
                            && t.CheckTime >= effectiveFrom && t.CheckTime <= uptoTime).ToListAsync();
            var rsGpsAttLog = await _gpsAttendanceRepo.GetAll().Where(t => arrEmpRefIds.Contains(t.EmployeeRefId)
            && t.CreationTime >= effectiveFrom && t.CreationTime <= uptoTime).ToListAsync();

            var startDate = effectiveFrom.AddDays(-30);
            var rsBiometricExcluded = await _biometricExcludedRepo.GetAll()
                    .Where(t => arrEmpRefIds.Contains(t.EmployeeRefId) && (t.ExcludeFromDate > startDate) && (t.ExcludeToDate <= uptoTime)).ToListAsync();

            var perinfoQueryable = _personalinformationRepo.GetAll();
            if (input.InchargeEmployeeRefId.HasValue)
            {
                perinfoQueryable = perinfoQueryable.Where(t => t.InchargeEmployeeRefId == input.InchargeEmployeeRefId.Value);
            }

            var rsPerinfo = await perinfoQueryable.ToListAsync();
            bool errorFlag = false;
            string errorMessage = "";
            foreach (var lst in officeJobDutyList)
            {
                var emp = rsPerinfo.FirstOrDefault(t => t.Id == lst.EmployeeRefId);
                if (emp == null)
                    continue;
                if (emp.IsAttendanceRequired == false)
                    continue;

                var excludedList = rsBiometricExcluded.Where(t => t.EmployeeRefId == lst.EmployeeRefId && t.ExcludeFromDate >= lst.DutyChartDate && t.ExcludeToDate <= lst.DutyChartDate).ToList();
                if (excludedList.Count > 0)
                    continue;

                var attlogsExist = rsAttLog.Where(t => t.EmployeeRefId == lst.EmployeeRefId && t.CheckTime.Value.Date >= lst.DutyChartDate && t.CheckTime.Value.Date <= lst.DutyChartDate).OrderBy(t => t.CheckTime).ToList();
                if (attlogsExist.Count > 0)
                {
                    var arrTimeFound = "";
                    foreach (var ti in attlogsExist)
                    {
                        if (ti.CheckTime.Value.Hour < 6)
                        {
                            var prevDate = ti.CheckTime.Value.AddDays(-1);
                            var dutychart = await _dutyChartDetailRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == ti.EmployeeRefId && DbFunctions.TruncateTime(t.DutyChartDate) == DbFunctions.TruncateTime(prevDate));
                            if (dutychart != null)
                            {
                                if (dutychart.NightFlag == true)
                                    continue;
                            }
                            else
                            {
                                arrTimeFound = arrTimeFound + ti.CheckTime.Value.ToString("yyyy-MMM-dd hh:mm") + ",";
                            }
                        }
                        else
                        {
                            arrTimeFound = arrTimeFound + ti.CheckTime.Value.ToString("yyyy-MMM-dd hh:mm") + ",";
                            break;
                        }
                    }
                    if (arrTimeFound.Length > 0)
                    {
                        arrTimeFound = arrTimeFound.Left(arrTimeFound.Length - 1);
                    }
                    else
                    {
                        if (oneDayFlag == true)
                        {
                            ConsolidatedPlainErrorMessage = ConsolidatedPlainErrorMessage + emp.EmployeeName + " <br>";
                            errorMessage = errorMessage + emp.EmployeeName + "<BR>";
                        }
                        else
                        {
                            ConsolidatedPlainErrorMessage = ConsolidatedPlainErrorMessage + L("OfficeJobButFingerNotFoundError", emp.EmployeeName, lst.DutyChartDate.ToString("yyyy-MMM-dd")) + "<br>";

                            errorMessage = errorMessage + L("OfficeJobButFingerNotFoundError", emp.EmployeeName, lst.DutyChartDate.ToString("yyyy-MMM-dd")) + "<BR> <BR>";
                        }

                        errorFlag = true;
                    }
                }
                else
                {
                    // Check GPS Attendance Exists

                    var gpsattlogsExist = rsGpsAttLog.Where(t => t.EmployeeRefId == lst.EmployeeRefId
                                    && t.CreationTime.Date >= lst.DutyChartDate && t.CreationTime.Date <= lst.DutyChartDate).OrderBy(t => t.CreationTime).ToList();
                    if (gpsattlogsExist.Count > 0)
                    {
                        continue;
                    }

                    if (oneDayFlag == true)
                    {
                        ConsolidatedPlainErrorMessage = ConsolidatedPlainErrorMessage + emp.EmployeeName + " <br>";
                        errorMessage = errorMessage + emp.EmployeeName + "<BR>";
                    }
                    else
                    {
                        ConsolidatedPlainErrorMessage = ConsolidatedPlainErrorMessage + L("OfficeJobButFingerNotFoundError", emp.EmployeeName, lst.DutyChartDate.ToString("yyyy-MMM-dd")) + "<br>";

                        errorMessage = errorMessage + L("OfficeJobButFingerNotFoundError", emp.EmployeeName, lst.DutyChartDate.ToString("yyyy-MMM-dd")) + "<BR> <BR>";
                    }
                    errorFlag = true;
                }
            }

            if (errorFlag == true)
            {
                errorMessage = "<span>" + errorMessage + "<span>";

                output.HtmlMessage = output.HtmlMessage + "<span STYLE='Color:Cyan;Background-Color:White;font-size:medium'>" + L("OfficeJobButFingerNotFoundErrorHeader") + "</span> <BR>";
                output.HtmlMessage = output.HtmlMessage + "<BR>" + errorMessage + "<HR/>";

                ConsolidatedPlainErrorMessage = L("OfficeJobButFingerNotFoundError") + "<br> <br>" + ConsolidatedPlainErrorMessage;
            }
            errorFlag = false;
            errorMessage = "";
            #endregion

            output.ErrorMessage = ConsolidatedPlainErrorMessage;
            return output;
        }

        public async Task<MessageOutput> HrGpsOrBillableAlert(GetHrOperationHealthDto input)
        {
            MessageOutput output = new MessageOutput();
            bool oneDayFlag = false;
            string ConsolidatedPlainErrorMessage = "";

            #region Gps_OnlineAttendanceRequired_in_PI_But_NoFingerOrGpsAttendance
            DateTime effectiveFrom;
            DateTime maxDate;
            if (input.StartDate.HasValue && input.EndDate.HasValue)
            {
                effectiveFrom = input.StartDate.Value;
                maxDate = effectiveFrom;
                oneDayFlag = true;
            }
            else
            {
                oneDayFlag = false;
                if (input.DaysTill == null)
                    input.DaysTill = 10;
                effectiveFrom = DateTime.Today.AddDays(-1 * input.DaysTill.Value);
                var attLocation = await _attendanceMachineLocationRepo.FirstOrDefaultAsync(t => t.Id > 0);
                if (attLocation == null)
                {
                    throw new UserFriendlyException(L("AttendanceLocation"));
                }
                var maxattLog = _attendanceLogRepo.GetAll().Where(t =>t.AttendanceMachineLocationRefId.HasValue &&  t.AttendanceMachineLocationRefId == attLocation.Id && t.CheckTime > effectiveFrom);
                if (maxattLog.Any() == false)
                {
                    return output;
                }
                maxDate = effectiveFrom;
                var totalCount = maxattLog.Count();

                if (totalCount > 0)
                {
                    var maxAttDate = maxattLog.Max(t => t.CheckTime.Value);
                    maxDate = maxAttDate;
                }
                if (maxDate < effectiveFrom)
                    return output;
            }

            var perinfoQueryable = _personalinformationRepo.GetAll().Where(t => t.IsBillableAttendanceRequired == true);
            if (input.InchargeEmployeeRefId.HasValue)
            {
                perinfoQueryable = perinfoQueryable.Where(t => t.InchargeEmployeeRefId == input.InchargeEmployeeRefId.Value);
            }

            var rsPerinfo = await perinfoQueryable.ToListAsync();

            List<int> employeeRefIds = rsPerinfo.Select(t => t.Id).ToList();

            var rsDutychart = _dutyChartDetailRepo.GetAll().WhereIf(input.EmployeeRefId.HasValue, t => t.EmployeeRefId == input.EmployeeRefId);
            rsDutychart = rsDutychart.Where(t => employeeRefIds.Contains(t.EmployeeRefId));

            string[] allowedStatusList = { L("Idle"), L("Job") };
            var dutyListforAllowedStatus = await rsDutychart.Where(t => allowedStatusList.Contains(t.AllottedStatus) && t.DutyChartDate >= effectiveFrom && t.DutyChartDate <= maxDate).OrderBy(t => t.EmployeeRefId).ToListAsync();
            var arrEmpRefIds = dutyListforAllowedStatus.Select(t => t.EmployeeRefId).Distinct().ToArray();

            var rsAttLog = await _attendanceLogRepo.GetAll().Where(t => arrEmpRefIds.Contains(t.EmployeeRefId) && t.CheckTime >= effectiveFrom).ToListAsync();

            var uptoTime = maxDate.AddDays(1);
            var rsGpsAttLog = await _gpsAttendanceRepo.GetAll().Where(t => arrEmpRefIds.Contains(t.EmployeeRefId)
                                && t.CreationTime >= effectiveFrom && t.CreationTime <= uptoTime).ToListAsync();


            var startDate = effectiveFrom.AddDays(-30);
            var rsBiometricExcluded = await _biometricExcludedRepo.GetAll()
                    .Where(t => arrEmpRefIds.Contains(t.EmployeeRefId) && (t.ExcludeFromDate > startDate) && (t.ExcludeToDate <= uptoTime)).ToListAsync();

            bool errorFlag = false;
            string errorMessage = "";
            foreach (var lst in dutyListforAllowedStatus)
            {
                var emp = rsPerinfo.FirstOrDefault(t => t.Id == lst.EmployeeRefId);
                if (emp == null)
                    continue;

                if (emp.IsBillableAttendanceRequired == false)
                    continue;

                if (emp.IsAttendanceRequired == false)
                    continue;

                var excludedList = rsBiometricExcluded.Where(t => t.EmployeeRefId == lst.EmployeeRefId && t.ExcludeFromDate.Date >= lst.DutyChartDate.Date
                                && t.ExcludeToDate.Date <= lst.DutyChartDate.Date).ToList();
                if (excludedList.Count > 0)
                    continue;
                else
                {
                    int I = 9;
                }


                var attlogsExist = rsAttLog.Where(t => t.EmployeeRefId == lst.EmployeeRefId && t.CheckTime.Value.Date >= lst.DutyChartDate && t.CheckTime.Value.Date <= lst.DutyChartDate).OrderBy(t => t.CheckTime).ToList();
                if (attlogsExist.Count > 0)
                {
                    var arrTimeFound = "";
                    foreach (var ti in attlogsExist)
                    {
                        if (ti.CheckTime.Value.Hour < 6)
                        {
                            var prevDate = ti.CheckTime.Value.AddDays(-1);
                            var dutychart = await _dutyChartDetailRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == ti.EmployeeRefId && DbFunctions.TruncateTime(t.DutyChartDate) == DbFunctions.TruncateTime(prevDate));
                            if (dutychart != null)
                            {
                                if (dutychart.NightFlag == true)
                                    continue;
                            }
                            arrTimeFound = arrTimeFound + ti.CheckTime.Value.ToString("yyyy-MMM-dd hh:mm") + ",";
                        }
                        else
                        {
                            arrTimeFound = arrTimeFound + ti.CheckTime.Value.ToString("yyyy-MMM-dd hh:mm") + ",";
                            break;
                        }
                    }
                    if (arrTimeFound.Length > 0)
                    {
                        arrTimeFound = arrTimeFound.Left(arrTimeFound.Length - 1);
                    }
                    else
                    {
                        if (oneDayFlag == true)
                        {
                            ConsolidatedPlainErrorMessage = ConsolidatedPlainErrorMessage + emp.EmployeeName + " <br>";
                            errorMessage = errorMessage + emp.EmployeeName + " <BR>";
                        }
                        else
                        {
                            ConsolidatedPlainErrorMessage = ConsolidatedPlainErrorMessage + L("BillableAttendanceIssue", emp.EmployeeName, lst.DutyChartDate.ToString("yyyy-MMM-dd")) + "<br>";

                            errorMessage = errorMessage + L("BillableAttendanceIssue", emp.EmployeeName, lst.DutyChartDate.ToString("yyyy-MMM-dd")) + "<BR> <BR>";
                        }
                        errorFlag = true;
                    }
                }
                else
                {
                    // Check GPS Attendance Exists

                    var gpsattlogsExist = rsGpsAttLog.Where(t => t.EmployeeRefId == lst.EmployeeRefId
                                    && t.CreationTime.Date >= lst.DutyChartDate && t.CreationTime.Date <= lst.DutyChartDate).OrderBy(t => t.CreationTime).ToList();
                    if (gpsattlogsExist.Count > 0)
                    {
                        continue;
                    }
                    if (oneDayFlag == true)
                    {
                        ConsolidatedPlainErrorMessage = ConsolidatedPlainErrorMessage + emp.EmployeeName + " <br>";
                        errorMessage = errorMessage + emp.EmployeeName + "<BR>";
                    }
                    else
                    {
                        ConsolidatedPlainErrorMessage = ConsolidatedPlainErrorMessage + L("BillableAttendanceIssue", emp.EmployeeName, lst.DutyChartDate.ToString("yyyy-MMM-dd")) + "<br>";

                        errorMessage = errorMessage + L("BillableAttendanceIssue", emp.EmployeeName, lst.DutyChartDate.ToString("yyyy-MMM-dd")) + "<BR> <BR>";
                    }
                    errorFlag = true;
                }
            }

            if (errorFlag == true)
            {
                errorMessage = "<span>" + errorMessage + "<span>";

                output.HtmlMessage = output.HtmlMessage + "<span STYLE='Color:Blue;Background-Color:White;font-size:medium'>" + L("BillableAttendanceIssue") + "</span> <BR>";
                output.HtmlMessage = output.HtmlMessage + "<BR>" + errorMessage + "<HR/>";

                ConsolidatedPlainErrorMessage = L("BillableAttendanceIssue") + "<br> <br>" + ConsolidatedPlainErrorMessage;
            }
            errorFlag = false;
            errorMessage = "";
            #endregion

            output.ErrorMessage = ConsolidatedPlainErrorMessage;
            return output;
        }

        public string ChangeMinutstoTime(int minutesoftheDay)
        {
            if (minutesoftheDay == 0)
            {
                return "00:00";
            }

            double argMinutes = minutesoftheDay;
            int hour = int.Parse(Math.Floor(argMinutes / 60).ToString());
            if (hour > 24)
                hour = hour - 24;

            var min = (argMinutes % 60).ToString().PadLeft(2);
            if (min.Left(1).Equals(" "))
                min = "0" + min.Right(1);
            string hourString = hour >= 10 ? hour.ToString() : "0" + hour;
            string data = hourString + ":" + min;
            return data;
        }

        public async Task<List<MessageOutput>> SendDayStatusEmail(GetStatusAlertMailDto input)
        {
            MessageOutput retOutput = new MessageOutput();
            retOutput.SuccessFlag = true;

            var emp = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == input.EmployeeRefId);
            retOutput.EmployeeRefId = emp.Id;
            retOutput.EmployeeRefName = emp.EmployeeName;
            retOutput.PersonalEmail = emp.PersonalEmail;
            retOutput.Email = emp.Email;
            retOutput.SuccessFlag = false;
            bool emailContentsExists = false;

            //if (emp.BioMetricCode.Value==214)
            //{
            //	input.MailSendFlag = true;
            //}
            //else
            //{
            //	input.MailSendFlag = false;
            //}
            var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == emp.LocationRefId);
            var company = await _companyRepo.FirstOrDefaultAsync(t => t.Id == location.CompanyRefId);
            if (company == null)
            {
                throw new UserFriendlyException(L("Company" + " ?"));
            }

            var mailList = location.Email;
            if (mailList.IsNullOrEmpty())
            {
                throw new UserFriendlyException(L("Company") + L("EmailIdList") + " ?");
            }
            string[] emailList = mailList.Split(";");

            var mailMessage = new StringBuilder();

            var hroperationHealthEdp = await HrOperationHealthEdp(new GetHrOperationHealthDto
            {
                DaysTill = 45,
                EmployeeRefId = input.EmployeeRefId
            });

            retOutput.HrOperationHealthHtml = hroperationHealthEdp.HtmlMessage;
            retOutput.HrOperationHealthText = hroperationHealthEdp.ErrorMessage;

            mailMessage.AppendLine(hroperationHealthEdp.HtmlMessage);

            string htmlLine;

            htmlLine = "<p>Dear <strong> " + emp.EmployeeName + " </strong> ,</p> ";
            mailMessage.AppendLine(htmlLine);

            #region IdleDaysPrint

            DateTime fromIdleDate = input.DutyChartDate.AddDays(-5);
            DateTime toIdleDate = input.DutyChartDate.AddDays(2);

            List<DutyChartDetailListDto> JobList = new List<DutyChartDetailListDto>();
            var retlst = await _dutyChartDetailRepo.GetAllListAsync(t => t.EmployeeRefId == input.EmployeeRefId && t.DutyChartDate > fromIdleDate && t.DutyChartDate <= toIdleDate);
            JobList = retlst.MapTo<List<DutyChartDetailListDto>>();
            var onDateStatus = retlst.FirstOrDefault(t => t.DutyChartDate == input.DutyChartDate);

            if (JobList.Count > 0)
            {
                emailContentsExists = true;

                htmlLine = "<p style = \"color: #ff0000;\"> Your Job Status is  <strong> " + onDateStatus.AllottedStatus + " </strong> on " + input.DutyChartDate.ToString("dd-MMM-yyyy dddd") + " </p> ";
                mailMessage.AppendLine(htmlLine);
                htmlLine = "<p><span style = \"color: #ff0000;\"><strong>DATE WISE - JOB STATUS</strong></span></p>";
                mailMessage.AppendLine(htmlLine);
                htmlLine = "<table border = \"2\">";
                mailMessage.AppendLine(htmlLine);
                htmlLine = "<tbody>";
                mailMessage.AppendLine(htmlLine);
                htmlLine = "<tr>";
                mailMessage.AppendLine(htmlLine);
                htmlLine = " <td style = \"text-align: center;\" width = \"90\">";
                mailMessage.AppendLine(htmlLine);
                htmlLine = "<p>#</p> ";
                mailMessage.AppendLine(htmlLine);
                htmlLine = "</td> ";
                mailMessage.AppendLine(htmlLine);
                htmlLine = "<td style = \"text-align: center;\" width = \"130\"> ";
                mailMessage.AppendLine(htmlLine);
                htmlLine = "<p> Date </p> ";
                mailMessage.AppendLine(htmlLine);
                htmlLine = "</td> ";
                htmlLine = "<td style = \"text-align: center;\" width = \"130\"> ";
                mailMessage.AppendLine(htmlLine);
                htmlLine = "<p> Status </p> ";
                mailMessage.AppendLine(htmlLine);
                htmlLine = "</td> ";
                htmlLine = "<td style = \"text-align: center;\" width = \"130\"> ";
                mailMessage.AppendLine(htmlLine);
                htmlLine = "<p> Job </p> ";
                mailMessage.AppendLine(htmlLine);
                htmlLine = "</td> ";
                mailMessage.AppendLine(htmlLine);
                htmlLine = " </tr> ";
                mailMessage.AppendLine(htmlLine);

                int sno = 1;
                foreach (var lst in JobList)
                {
                    htmlLine = " <tr> ";
                    mailMessage.AppendLine(htmlLine);
                    htmlLine = "   <td style = \"text-align: center;\" width = \"90\">";
                    mailMessage.AppendLine(htmlLine);
                    htmlLine = "       <p> " + sno + " </p> ";
                    mailMessage.AppendLine(htmlLine);
                    htmlLine = "        </td> ";
                    mailMessage.AppendLine(htmlLine);
                    htmlLine = "        <td style = \"text-align: center;\" width = \"130\"> ";
                    mailMessage.AppendLine(htmlLine);
                    if (lst.DutyChartDate.Date == input.DutyChartDate.Date)
                    {
                        htmlLine = "       <p> <strong style = \"color: #ff0000;\">  " + lst.DutyChartDate.ToString("dd-MMM-yy (ddd)") + " </strong>  </p> ";
                        mailMessage.AppendLine(htmlLine);
                    }
                    else
                    {
                        htmlLine = "       <p> " + lst.DutyChartDate.ToString("dd-MMM-yy (ddd)") + "  </p> ";
                        mailMessage.AppendLine(htmlLine);
                    }


                    htmlLine = "        </td> ";
                    mailMessage.AppendLine(htmlLine);
                    htmlLine = "        <td style = \"text-align: center;\" width = \"130\"> ";
                    mailMessage.AppendLine(htmlLine);
                    if (lst.DutyChartDate.Date == input.DutyChartDate.Date)
                    {
                        htmlLine = "       <p> <strong style = \"color: #ff0000;\"> " + lst.AllottedStatus + " </strong>  </p> ";
                        mailMessage.AppendLine(htmlLine);
                    }
                    else
                    {
                        htmlLine = "       <p> " + lst.AllottedStatus + "  </p> ";
                        mailMessage.AppendLine(htmlLine);
                    }
                    htmlLine = "        </td> ";
                    mailMessage.AppendLine(htmlLine);
                    htmlLine = "        <td style = \"text-align: center;\" width = \"130\"> ";
                    mailMessage.AppendLine(htmlLine);
                    //if (lst.ProjectCostCentreRefId.HasValue)
                    //{
                    //    var costCentre = rsCostCentre.FirstOrDefault(t => t.Id == lst.ProjectCostCentreRefId);
                    //    htmlLine = "       <p> " + costCentre.CostCentreName + "  </p> ";
                    //    mailMessage.AppendLine(htmlLine);
                    //}
                    htmlLine = "        </td> ";
                    mailMessage.AppendLine(htmlLine);
                    htmlLine = "      </tr> ";
                    mailMessage.AppendLine(htmlLine);
                    sno++;
                }
                htmlLine = "         </tbody> ";
                mailMessage.AppendLine(htmlLine);
                htmlLine = "         </table>";
                mailMessage.AppendLine(htmlLine);
            }

            #endregion

            htmlLine = " <p> Please verify all the details and If you found any errors please let us know immediately by reply email to the below email ids.</p>";
            mailMessage.AppendLine(htmlLine);
            htmlLine = " <p> " + mailList + " </p>";
            mailMessage.AppendLine(htmlLine);
            htmlLine = " <p> 	Make sure all the time sheets should reach office on time and enable us to submit the invoice on time to Clients.</p> ";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "  <p> &nbsp;</p> ";
            mailMessage.AppendLine(htmlLine);
            htmlLine = "   <p> HR / EDP </p>";
            mailMessage.AppendLine(htmlLine);
            htmlLine = "     <p> A-STAR GROUP </p> ";
            mailMessage.AppendLine(htmlLine);
            htmlLine = "    <p> &nbsp;</p>";
            mailMessage.AppendLine(htmlLine);
            htmlLine = "  <p> &nbsp;</p> ";
            mailMessage.AppendLine(htmlLine);
            htmlLine = "  <p> &nbsp;</p> ";
            mailMessage.AppendLine(htmlLine);

            if (input.MailSendFlag == true && emailContentsExists == true)
            {
                try
                {
                    SmtpClient client = new SmtpClient();
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.EnableSsl = true;
                    client.Host = "smtp.gmail.com";
                    client.Port = 587;


                    System.Net.NetworkCredential credentials =
                        new System.Net.NetworkCredential("astaredp-sg@astartesting.com.sg", "astaredp2016");
                    client.UseDefaultCredentials = false;
                    client.Credentials = credentials;

                    MailMessage msg = new MailMessage();
                    msg.From = new MailAddress("astaredp-sg@astartesting.com.sg");
                    msg.Bcc.Add(new MailAddress("karaneeswara@gmail.com"));
                    if (!emp.Email.IsNullOrEmpty())
                        msg.To.Add(new MailAddress(emp.Email));
                    if (!emp.PersonalEmail.IsNullOrEmpty())
                        msg.To.Add(new MailAddress(emp.PersonalEmail));

                    if (emp.Email.IsNullOrEmpty() && emp.PersonalEmail.IsNullOrEmpty())
                    {
                        htmlLine = " <p> <B> Note :	The Employee Does Not Have The Valid Email Id , so you received this mail and please update the employee email id immediately. </B> </p> ";
                        mailMessage.AppendLine(htmlLine);

                        foreach (var em in emailList)
                        {
                            msg.To.Add(new MailAddress(em));
                        }
                    }

                    htmlLine = "Your Job Status is " + onDateStatus.AllottedStatus + "  on " + input.DutyChartDate.ToString("dd-MMM-yyyy dddd") + " ";

                    msg.Subject = htmlLine;
                    msg.IsBodyHtml = true;
                    msg.Body = mailMessage.ToString();
                    try
                    {
                        if (msg.To.Count > 0)
                            client.Send(msg);
                        EmployeeMailMessage employeeMessage = new EmployeeMailMessage
                        {
                            EmployeeRefId = emp.Id,
                            MailTime = DateTime.Now,
                            DutyChartDate = input.DutyChartDate,
                            DutyStatusMailed = onDateStatus.AllottedStatus,
                            MessageType = (int)MailMessageType.DailyDutyChartStatus_9,
                            ReceipientsMailIdsList = msg.To.ToString(),
                            MessageText = mailMessage.ToString(),
                            MessageApprovedBy = (int)AbpSession.UserId
                        };
                        await _employeeMailMessage.InsertOrUpdateAndGetIdAsync(employeeMessage);
                    }
                    catch (Exception ex)
                    {
                        retOutput.SuccessFlag = false;
                        retOutput.ErrorMessage = L("NotAbleToSendMailError", ex.Message + ex.InnerException);
                    }
                    emailContentsExists = false;
                    retOutput.SuccessFlag = true;
                }
                catch (Exception ex)
                {
                    retOutput.SuccessFlag = false;
                    retOutput.ErrorMessage = L("NotAbleToSendMailError", ex.Message + ex.InnerException);
                    throw;
                }
            }

            retOutput.Message = mailMessage.ToString();
            retOutput.HtmlMessage = mailMessage.ToString();

            List<MessageOutput> retList = new List<MessageOutput>();
            retList.Add(retOutput);
            return retList;
        }

        public async Task<List<MessageOutput>> SendDayStatusEmailToAllEmployee(GetStatusAlertMailDto input)
        {
            DateTime minDate = DateTime.Today.AddDays(-30);

            if (input.DutyChartDate <= minDate)
            {
                return null;
            }
            TimeSpan diff = input.DutyChartDate.Subtract(DateTime.Today);
            if (Math.Abs(diff.Days) > 15)
            {
                return null;
            }
            List<SimpleDutyChartDetailListDto> alreadyStatusMailed = new List<SimpleDutyChartDetailListDto>();
            if (input.MailSendFlag)
            {
                var rsstatusMail = await _statusMailDate.FirstOrDefaultAsync(t => t.DutyChartDate == input.DutyChartDate);
                if (rsstatusMail != null)
                {
                    alreadyStatusMailed = _xmlandjsonConvertorAppService.DeSerializeFromJSON<List<SimpleDutyChartDetailListDto>>(rsstatusMail.EmployeeListJson);
                }
            }

            var rsPersonalInformation = await _personalinformationRepo.GetAllListAsync();
            var retList = new List<MessageOutput>();
            string weekOffString = L("WeekOff");
            string phString = L("PH");
            var dcEmps = await _dutyChartDetailRepo.GetAllListAsync(t => t.DutyChartDate == input.DutyChartDate
                && !t.AllottedStatus.Equals(input.JobStatus) && !t.AllottedStatus.Equals(weekOffString));

            foreach (var emp in dcEmps)
            {
                if (emp.AllottedStatus == L("PH"))
                {
                    continue;
                }

                var perinfo = rsPersonalInformation.FirstOrDefault(t => t.Id == emp.EmployeeRefId);
                if (perinfo == null)
                {
                    continue;
                }
                if (perinfo.IsDayCloseStatusMailRequired == false)
                {
                    continue;
                }

                {
                    var existRecord = alreadyStatusMailed.FirstOrDefault(t => t.EmployeeRefId == emp.EmployeeRefId);
                    var dataToAdded = emp.MapTo<SimpleDutyChartDetailListDto>();
                    if (existRecord == null)
                        alreadyStatusMailed.Add(dataToAdded);
                    else
                    {
                        if (existRecord.ToString() == dataToAdded.ToString())
                        {
                            continue;
                        }
                        else
                        {
                            var existId = alreadyStatusMailed.IndexOf(existRecord);
                            alreadyStatusMailed.RemoveAt(existId);
                            alreadyStatusMailed.Add(dataToAdded);
                        }
                    }

                    var empDto = new GetStatusAlertMailDto
                    {
                        EmployeeRefId = emp.EmployeeRefId,
                        DutyChartDate = emp.DutyChartDate,
                        MailSendFlag = input.MailSendFlag
                    };
                    var mailDetail = await SendDayStatusEmail(empDto);
                    retList.AddRange(mailDetail);
                }
            }

            if (input.MailSendFlag)
            {
                var rsstatusMail = await _statusMailDate.FirstOrDefaultAsync(t => t.DutyChartDate == input.DutyChartDate);
                var jsonReturn = _xmlandjsonConvertorAppService.SerializeToJSON(alreadyStatusMailed);

                if (rsstatusMail != null)
                {
                    var editDto = await _statusMailDate.GetAsync(rsstatusMail.Id);
                    editDto.EmployeeListJson = jsonReturn;
                    await _statusMailDate.UpdateAsync(editDto);
                }
                else
                {
                    var newStatusDto = new StatusMailDate
                    {
                        DutyChartDate = input.DutyChartDate,
                        EmployeeListJson = jsonReturn
                    };
                    await _statusMailDate.InsertAsync(newStatusDto);
                }
            }


            return retList;
        }

        public async Task<List<WorkDayListDto>> GetWorkDaysDetails()
        {
            var allItems = await _workDayRepo.GetAllListAsync(t => t.EmployeeRefId == null);

            var allListDtos = allItems.MapTo<List<WorkDayListDto>>();

            return allListDtos;
        }

        public async Task<Get_WorkDayHours_With_OT_Value> GetWorkHoursForGivenDate(DateInput input, List<WorkDayListDto> rsWorkDayList, List<PublicHoliday> rsPublicHolidays)
        {
            Get_WorkDayHours_With_OT_Value result = new Get_WorkDayHours_With_OT_Value();
            int wd = (int)input.DateId.Value.DayOfWeek;
            var workDay = rsWorkDayList.FirstOrDefault(t => t.DayOfWeekRefId == wd);
            if (workDay == null)
            {
                throw new UserFriendlyException(L("WorkDayNotSettingProperly"));
            }

            if (rsPublicHolidays == null)
                rsPublicHolidays = await _publicHolidayRepo.GetAllListAsync(t => t.HolidayDate >= input.DateId && t.HolidayDate <= input.DateId);

            if (rsPublicHolidays != null)
            {
                var ph = rsPublicHolidays.FirstOrDefault(t => t.HolidayDate.Date == input.DateId);
                if (ph != null)
                {
                    result.FullWorkHours = 0;
                    result.OtTag = "OT2";
                }
                else
                {
                    result.FullWorkHours = workDay.NoOfHourWorks;
                    if (workDay.OtRatio == (decimal)1.5)
                        result.OtTag = "OT1";
                    else
                        result.OtTag = "OT2";
                }
                return result;
            }
            else
            {
                result.FullWorkHours = workDay.NoOfHourWorks;
                if (workDay.OtRatio == (decimal)1.5)
                    result.OtTag = "OT1";
                else
                    result.OtTag = "OT2";
                return result;
            }
        }

        //GetGpsEmployeeAttendance
        public async Task<List<GpsAttendanceListDto>> GetGpsEmployeeAttendance(GetDashBoardDto input)
        {
            if (!input.StartDate.HasValue)
            {
                input.StartDate = DateTime.Today.AddDays(-30);
                input.EndDate = DateTime.Now;
            }

            DateTime uptoDate = input.EndDate.Value.AddMinutes(1439);
            var rsGps = await _gpsAttendanceRepo.GetAll().Where(t => t.EmployeeRefId == input.EmployeeRefId
            && t.CreationTime >= input.StartDate.Value && t.CreationTime <= uptoDate).ToListAsync();

            List<GpsAttendanceListDto> retList = rsGps.MapTo<List<GpsAttendanceListDto>>();
            return retList;
        }


        public async Task<List<GpsAttendanceParticularDateList>> GetGpsEmployeeAttendanceDateWise(GetDashBoardDto input)
        {
            if (!input.StartDate.HasValue)
            {
                input.StartDate = DateTime.Today.AddDays(-30);
                input.EndDate = DateTime.Now;
            }
            DateTime uptoDate = input.EndDate.Value.AddMinutes(1439);
            var rsGps = await _gpsAttendanceRepo.GetAll().Where(t => t.EmployeeRefId == input.EmployeeRefId
            && t.CreationTime >= input.StartDate.Value && t.CreationTime <= uptoDate).ToListAsync();

            string tenantName = "Singapore";
            var tenant = await TenantManager.GetByIdAsync((int)AbpSession.TenantId);
            tenantName = tenant.Name.ToUpper();

            List<GpsAttendanceListDto> gpsdetail = rsGps.MapTo<List<GpsAttendanceListDto>>();
            if (tenantName.ToUpper().Equals("INDIA"))
            {
                TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                foreach (var lst in gpsdetail)
                {
                    lst.CreationTime = TimeZoneInfo.ConvertTimeFromUtc(lst.CreationTime.ToUniversalTime(), tzi);
                }
            }

            var dcList = await _dutyChartDetailRepo.GetAllListAsync(t => t.EmployeeRefId == input.EmployeeRefId
                    && t.DutyChartDate >= input.StartDate.Value && t.DutyChartDate <= input.EndDate.Value);

            var emp = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == input.EmployeeRefId);
            DateTime[] dates = gpsdetail.Select(t => t.CreationTime.Date).Distinct().ToArray();
            List<GpsAttendanceParticularDateList> output = new List<GpsAttendanceParticularDateList>();
            foreach (var dt in dates)
            {
                GpsAttendanceParticularDateList newDto = new GpsAttendanceParticularDateList();
                newDto.EmployeeRefId = emp.Id;
                newDto.EmployeeRefName = emp.EmployeeName;
                newDto.WorkDate = dt.Date;

                bool nightFlag = false;
                var dc = dcList.FirstOrDefault(t => t.DutyChartDate == dt.Date);
                if (dc == null)
                    nightFlag = false;
                else
                {
                    nightFlag = dc.NightFlag;
                }
                DateTime fromDate = dt.Date.AddHours(4);
                DateTime toDate = dt.AddHours(24);

                if (nightFlag)
                    toDate = dt.AddHours(30.5);
                var particularDateList = gpsdetail.Where(t => t.CreationTime >= fromDate && t.CreationTime <= toDate).ToList();
                if (particularDateList == null || particularDateList.Count == 0)
                {
                    var dd = dt;
                    continue;
                }
                if (particularDateList.Count() == 1)
                {
                    newDto.StartDetail = particularDateList[0];

                }
                else
                {
                    particularDateList = particularDateList.OrderBy(t => t.CreationTime).ToList();
                    var minTime = particularDateList[0];
                    var maxTime = particularDateList[particularDateList.Count - 1];
                    newDto.StartDetail = minTime;
                    newDto.EndDetail = maxTime;
                }
                if (dc != null)
                {
                    //var clientLocation = await _clientlocationRepo.FirstOrDefaultAsync(t => t.Id == dc.ClientLocationRefId);//Getting Client Location
                    //if (clientLocation != null)
                    //{
                    //    if (clientLocation.Latitude.HasValue)
                    //    {
                    //        if (newDto.StartDetail != null)
                    //        {
                    //            var distance = DistanceBetweenTwoPoint(clientLocation.Latitude.Value, clientLocation.Longitude.Value, newDto.StartDetail.Latitude, newDto.StartDetail.Longitude);
                    //            if (distance <= Convert.ToDouble(clientLocation.MinimumDistance))
                    //            {
                    //                newDto.StartDetail.IsRange = L("InRange");

                    //            }
                    //            else
                    //            {
                    //                newDto.StartDetail.IsRange = L("NotInRange");
                    //                newDto.StartDetail.GpsDistanceFromClient = distance - Convert.ToDouble(clientLocation.MinimumDistance);
                    //            }
                    //        }
                    //        if (newDto.EndDetail != null)
                    //        {
                    //            var distance = DistanceBetweenTwoPoint(clientLocation.Latitude.Value, clientLocation.Longitude.Value, newDto.EndDetail.Latitude, newDto.EndDetail.Longitude);//Return in KM
                    //            if (distance <= Convert.ToDouble(clientLocation.MinimumDistance))
                    //            {
                    //                newDto.EndDetail.IsRange = L("InRange");

                    //            }
                    //            else
                    //            {
                    //                newDto.EndDetail.IsRange = L("NotInRange");
                    //                newDto.EndDetail.GpsDistanceFromClient = distance - Convert.ToDouble(clientLocation.MinimumDistance);
                    //            }
                    //        }

                    //    }
                    //    else
                    //    {
                    //        newDto.StartDetail.RangeErrorMessage = L("NoLatLonForGivenClient", clientLocation.Name);
                    //    }
                    //}
                    //else
                    //{
                    //    newDto.StartDetail.RangeErrorMessage = L("ClientLocationNotExists");
                    //}
                }
                else
                {
                    newDto.StartDetail.RangeErrorMessage = L("DutyChartEntryNotFound", emp.EmployeeName, dt.ToString("dd-MMM-yy"));
                }
                newDto.NightFlag = nightFlag;
                output.Add(newDto);
            }
            output = output.OrderBy(t => t.WorkDate).ToList();
            return output;
        }


        private async Task AddAgeing(decimal daysdiff, AgeingAnalysis argInput)
        {
            if (daysdiff > 90)
            {
                argInput.Days90over = argInput.Days90over + 1;
            }
            else if (daysdiff > 60)
            {
                argInput.Days61to90 = argInput.Days61to90 + 1;
            }
            else if (daysdiff > 30)
            {
                argInput.Days31to60 = argInput.Days31to60 + 1;
            }
            else if (daysdiff > 15)
            {
                argInput.Days16to30 = argInput.Days16to30 + 1;
            }
            else
            {
                argInput.Days1to15 = argInput.Days1to15 + 1;
            }
        }

        private DateTime GetAcYearStartingDate(DateTime input)
        {
            if (input.Month <= 6)
                return new DateTime(input.Year - 1, 7, 1);
            else
                return new DateTime(input.Year, 7, 1);
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetWorkPositionTypeForCombobox(NullableIdInput ninput)
        {
            List<ComboboxItemDto> retList = new List<ComboboxItemDto>();

            string enumstring;
            Array EnumValues = System.Enum.GetValues(typeof(WorkPosition));
            foreach (int EnumValue in EnumValues)
            {
                enumstring = Enum.GetName(typeof(WorkPosition), EnumValue);
                retList.Add(new ComboboxItemDto { Value = EnumValue.ToString(), DisplayText = enumstring });
            }

            return
                   new ListResultOutput<ComboboxItemDto>(
                       retList.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText)).ToList());
        }

        //        private double distance(double lat1, double lon1, double lat2, double lon2)
        //        {

        //            if ((lat1 == lat2) && (lon1 == lon2))
        //            {
        //||||||| .r1854
        //        public async Task<bool> ComparsionofLatLong(GetLatLong input)
        //        {
        //            //input.Startdate = System.DateTime.Now;
        //            //input.Enddate = System.DateTime.Now;
        //            //input.EmployeeRefId = 129;
        //            input.ClientRefId = 2660;
        //            var allItems = await _clientlocationRepo.FirstOrDefaultAsync(t => t.ClientRefId == input.ClientRefId);
        //            var allListDtos = allItems.MapTo<ClientLocationListDto>();
        //            GetDashBoardDto getDashBoardDto = new GetDashBoardDto
        //            {
        //                StartDate=input.Startdate,
        //                EndDate=input.Enddate,
        //                EmployeeRefId=input.EmployeeRefId
        //            };
        //            var result=await GetGpsEmployeeAttendanceDateWise(getDashBoardDto);//Under Result:->startDetail->Lat Long
        //            var resultListDtos = result.MapTo<List<GpsAttendanceParticularDateList>>();
        //            double lat1 = Convert.ToDouble(allListDtos.Latitude);
        //            double lon1 = Convert.ToDouble(allListDtos.Longitude);
        //            double lat2 = resultListDtos[0].StartDetail.Latitude;
        //            double lon2 = resultListDtos[0].StartDetail.Longitude;
        //           var distanceValue=  distance(lat1,  lon1,  lat2,  lon2);
        //            if(distanceValue<= Convert.ToDouble(allListDtos.MinimumDistance))
        //            {
        //                return true;
        //            }
        //            else
        //            {
        //                return false;
        //            }
        //        }
        //        private double distance(double lat1, double lon1, double lat2, double lon2)
        //        {

        //            if ((lat1 == lat2) && (lon1 == lon2))
        //            {
        //=======
        //        private double DistanceBetweenTwoPoint(double fromLat, double fromLon, double toLat, double toLon)
        //        {
        //            char unit = 'K';
        //            if ((fromLat == toLat) && (fromLon == toLon))
        //            {
        //>>>>>>> .r1866
        //                return 0;
        //            }
        //            else
        //            {
        //                double theta = fromLon - toLon;
        //                double dist = Math.Sin(deg2rad(fromLat)) * Math.Sin(deg2rad(toLat)) + Math.Cos(deg2rad(fromLat)) * Math.Cos(deg2rad(toLat)) * Math.Cos(deg2rad(theta));
        //                dist = Math.Acos(dist);
        //                dist = rad2deg(dist);
        //                dist = dist * 60 * 1.1515; //Miles
        //                if (unit == 'K') //Kilometers
        //                {
        //                    dist = dist * 1.609344;
        //                }
        //                //else if (unit == 'N')
        //                //{
        //                //    dist = dist * 0.8684;
        //                //}
        //                return (dist);
        //            }
        //        }
        //        private double deg2rad(double deg)//This function converts decimal degrees to radians  
        //        {
        //            return (deg * Math.PI / 180.0);
        //        }
        //        private double rad2deg(double rad)//This function converts radians to decimal degrees  
        //        {
        //            return (rad / Math.PI * 180.0);
        //        }

        public async Task<SalaryPaidForTheMonth> GetSalaryForTheGivenMonth(SalaryPrepareDto input)
        {
            scaleForDecimalRound = 2;
            SalaryPaidForTheMonth output = new SalaryPaidForTheMonth();

            #region Validation
            if (input.MonthYear.Value == DateTime.MinValue || input.MonthYear == null)
            {
                throw new UserFriendlyException(L("Month") + " ?");
            }

            DateTime startDate = new DateTime(input.MonthYear.Value.Year, input.MonthYear.Value.Month, 01);
            var nextMonth = startDate.AddMonths(1);
            DateTime endDate = nextMonth.AddDays(-1);
            input.StartDate = startDate;
            input.EndDate = endDate;

            output.InputData = input;
            #endregion

            var temp = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == input.EmployeeRefId);
            if (temp == null)
            {
                throw new UserFriendlyException(L("NotExistForId", L("Employee"), input.EmployeeRefId));
            }

            if (output.TenantName.IsNullOrEmpty() || output.TenantName.IsNullOrWhiteSpace())
            {
                var tenant = await TenantManager.GetByIdAsync(temp.TenantId);
                output.TenantName = tenant.TenancyName;
                var t = await GetCurrentTenantAsync();
                output.TenantName = t.TenancyName;
            }
            var employee = temp.MapTo<PersonalInformationListDto>();
            output.PersonalInformation = employee;

            var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == employee.LocationRefId);
            var company = await _companyRepo.FirstOrDefaultAsync(t => t.Id == location.CompanyRefId);
            output.PersonalInformation.CompanyRefId = company.Id;
            output.PersonalInformation.CompanyRefCode = company.Code;
            output.PersonalInformation.CompanyRefName = company.Name;

            var salaryInfo = await _salaryInfoRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == input.EmployeeRefId);
            if (salaryInfo == null)
            {
                throw new UserFriendlyException(L("NotExistForId", L("Salary"), L("Employee") + " : " + input.EmployeeRefId));
            }

            var salaryMaster = await _salaryinfoappService.GetSalaryInfoForEdit(new NullableIdInput { Id = salaryInfo.Id });
            salaryMaster.SalaryInfo.OverAllAllowance = 0;
            salaryMaster.SalaryInfo.OverAllDeduction = 0;

            var salaryMonthDutyDetails = await GetEmployeeDashBoardDutyChartData(new GetDashBoardDto
            {
                EmployeeRefId = employee.Id,
                StartDate = input.StartDate,
                EndDate = input.EndDate,
                IsCalendarEventsRequired = false
            });

            if (salaryMaster.SalaryInfo.IncentiveAndOTCalculationPeriodRefId == (int)IncentiveOTCalculationPeriod.Previous_Month)
            {
                DateTime previousMonthStartDate = input.StartDate.AddMonths(-1);
                DateTime previousMonthEndDate = input.StartDate.AddDays(-1);
                var previousMonthDutyDetails = await GetEmployeeDashBoardDutyChartData(new GetDashBoardDto
                {
                    EmployeeRefId = employee.Id,
                    StartDate = previousMonthStartDate,
                    EndDate = previousMonthEndDate,
                    IsCalendarEventsRequired = false
                });
                salaryMonthDutyDetails.IncentiveConsolidatedList = previousMonthDutyDetails.IncentiveConsolidatedList;
                salaryMonthDutyDetails.OverAllIncentiveAmount = previousMonthDutyDetails.OverAllIncentiveAmount;
            }

            output = await GetSalaryValueForGivenEmployeeWithGivenDate(output, salaryMaster, salaryMonthDutyDetails);
            return output;
        }

        public async Task<SalaryPayableConsolidatedDto> GetParticularDaySalary(PerDaySalaryCost input)
        {
            DateTime executionStartTime = DateTime.Now;
            #region Validation And Get Data
            var dcDutyChart = await _dutyChartRepo.FirstOrDefaultAsync(t => DbFunctions.TruncateTime(t.DutyChartDate) == DbFunctions.TruncateTime(input.DateId) && t.LocationRefId == input.LocationRefId);

            if (dcDutyChart == null)
            {
                throw new UserFriendlyException(L("NotExistForId", L("DutyChart"), input.DateId.ToString("dd-MMM-yyyy")));
            }
            else
            {
                if (!dcDutyChart.Status.Equals(L("DayClosed")))
                {
                    throw new UserFriendlyException(L("PlsDayClose", input.DateId.ToString("dd-MMM-yyyy")));
                }
            }

            var tempDc = await _dutyChartAppService.GetDutyChartForEdit(new GetDutyChartForGivenDate
            {
                LocationRefId = input.LocationRefId,
                DutyChartDate = input.DateId
            });
            var dutyChartList = tempDc.DutyChartDetail;

            if (input.SupervisorFilter.HasValue)
            {
                if (input.SupervisorFilter.Value > 0)
                {
                    dutyChartList = dutyChartList.Where(t => t.SupervisorEmployeeRefId == input.SupervisorFilter.Value).ToList();
                }
            }
            if (input.InchargeFilter.HasValue)
            {
                if (input.InchargeFilter.Value > 0)
                {
                    dutyChartList = dutyChartList.Where(t => t.InchargeEmployeeRefId == input.InchargeFilter.Value).ToList();
                }
            }
            //var dutyChartList = await _dutyChartDetailRepo.GetAllListAsync(t => DbFunctions.TruncateTime(t.DutyChartDate) == dateInput.DateId.Date);

            #endregion

            //DateTime maxCloseDay = new DateTime();
            //var maxValue = await _dutyChartRepo.GetAll().Where(t => t.Status.Equals("DayClosed")).OrderByDescending(t => t.DutyChartDate).FirstOrDefaultAsync();
            //if (maxValue != null)
            //    maxCloseDay = maxValue.DutyChartDate;
            //else
            //{
            //    maxValue = await _dutyChartRepo.GetAll().OrderByDescending(t => t.DutyChartDate).FirstOrDefaultAsync();
            //    if (maxValue != null)
            //        maxCloseDay = maxValue.DutyChartDate;
            //    else
            //        maxCloseDay = DateTime.Today;
            //}

            List<int> workedempRefIds = dutyChartList.Select(t => t.EmployeeRefId).ToList();
            var rsPerinfo = await _personalinformationRepo.GetAllListAsync(t => workedempRefIds.Contains(t.Id));
            var rsEmployeeCostCentre = await _employeeCostCentreRepo.GetAllListAsync();

            List<SalaryPaidForTheMonth> employeeSalaryPayableList = new List<SalaryPaidForTheMonth>();
            int loopCnt = 0;
            foreach (var emp in dutyChartList)
            {
                loopCnt++;
                SalaryPaidForTheMonth output = new SalaryPaidForTheMonth();
                SalaryPrepareDto salaryinput = new SalaryPrepareDto
                {
                    StartDate = input.DateId,
                    EndDate = input.DateId,
                    EmployeeRefId = emp.EmployeeRefId,
                };
                output.InputData = salaryinput;

                var temp = rsPerinfo.First(t => t.Id == salaryinput.EmployeeRefId);
                if (temp == null)
                {
                    throw new UserFriendlyException(L("NotExistForId", L("Employee"), salaryinput.EmployeeRefId));
                }
                var employee = temp.MapTo<PersonalInformationListDto>();
                output.PersonalInformation = employee;
                var cc = rsEmployeeCostCentre.FirstOrDefault(t => t.Id == employee.EmployeeCostCentreRefId);

                output.PersonalInformation.CostCentre = cc.CostCentre;

                var salaryInfo = await _salaryInfoRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == salaryinput.EmployeeRefId);
                if (salaryInfo == null)
                {
                    throw new UserFriendlyException(L("NotExistForId", L("Salary"), L("Employee") + " : " + salaryinput.EmployeeRefId));
                }

                var salaryMaster = await _salaryinfoappService.GetSalaryInfoForEdit(new NullableIdInput { Id = salaryInfo.Id });
                salaryMaster.SalaryInfo.OverAllAllowance = 0;
                salaryMaster.SalaryInfo.OverAllDeduction = 0;

                PieChartOutputDto salaryMonthDutyDetails = new PieChartOutputDto();
                //salaryMonthDutyDetails = await GetEmployeeDashBoardDutyChartData(new GetDashBoardDto
                //{
                //    EmployeeRefId = employee.Id,
                //    StartDate = input.StartDate,
                //    EndDate = input.EndDate,
                //    IsCalendarEventsRequired = false,
                //    MaxDayCloseDate = maxCloseDay
                //});

                if (emp.AllottedStatus == L("Absent"))
                {
                    salaryMonthDutyDetails.NoOfAbsentDays = 1;
                }
                else if (emp.AllottedStatus == L("PH"))
                {
                    salaryMonthDutyDetails.NoOfPublicHolidays = 1;
                }
                else if (emp.AllottedStatus == L("Idle"))
                {
                    salaryMonthDutyDetails.IdleDays = 1;
                }
                else if (emp.AllottedStatus == L("WeekOff"))
                {
                    salaryMonthDutyDetails.NoOfActualWorkedDays = 0;
                }
                else if (emp.AllottedStatus == L("Leave"))
                {
                    if (emp.HalfDayFlag)
                    {
                        if (emp.DoesSalaryPayableForThisLeaveRequest == true)
                        {
                            salaryMonthDutyDetails.NoOfPaidLeaveDays = 0.5m;
                            salaryMonthDutyDetails.NoOfActualWorkedDays = 0.5m;
                            var dcSubList = emp.DutyDetails;

                            if (dcSubList.Count > 1)
                            {
                                var idleExists = dcSubList.Exists(t => t.AllottedStatus.Equals(L("Idle")));
                                if (idleExists)
                                {
                                    salaryMonthDutyDetails.NoOfActualWorkedDays = 0;
                                    salaryMonthDutyDetails.IdleDays = 0.5m;
                                }
                            }
                        }
                        else
                        {
                            salaryMonthDutyDetails.NoOfAbsentDays = 0.5m;
                            salaryMonthDutyDetails.NoOfActualWorkedDays = 0.5m;
                        }
                    }
                    else
                    {
                        if (emp.DoesSalaryPayableForThisLeaveRequest == true)
                        {
                            salaryMonthDutyDetails.NoOfPaidLeaveDays = 1;
                        }
                        else
                        {
                            salaryMonthDutyDetails.NoOfAbsentDays = 1;
                        }
                    }
                }
                else
                {
                    salaryMonthDutyDetails.NoOfActualWorkedDays = 1;
                }
                output = await GetSalaryValueForGivenEmployeeWithGivenDate(output, salaryMaster, salaryMonthDutyDetails);
                employeeSalaryPayableList.Add(output);
            }
            SalaryPayableConsolidatedDto salaryPayableConsolidatedDto = new SalaryPayableConsolidatedDto();
            salaryPayableConsolidatedDto.EmployeeSalaryPayableList = employeeSalaryPayableList;
            salaryPayableConsolidatedDto.GrossSalaryAmount = employeeSalaryPayableList.Sum(t => t.GrossSalaryAmount);
            salaryPayableConsolidatedDto.DeductionAmount = employeeSalaryPayableList.Sum(t => t.DeductionAmount);
            salaryPayableConsolidatedDto.NetSalaryAmount = employeeSalaryPayableList.Sum(t => t.NetSalaryAmount);
            salaryPayableConsolidatedDto.IdleSalaryAmount = employeeSalaryPayableList.Sum(t => t.IdleSalaryAmount);
            salaryPayableConsolidatedDto.AbsentLevyAmount = employeeSalaryPayableList.Sum(t => t.AbsentLevyAmount);
            salaryPayableConsolidatedDto.FixedAmount = employeeSalaryPayableList.Sum(t => t.FixedAmount);
            salaryPayableConsolidatedDto.NoOfEmployees = employeeSalaryPayableList.Count();

            if (salaryPayableConsolidatedDto.IdleSalaryAmount == 0 || salaryPayableConsolidatedDto.TotalPayable == 0)
            {
                salaryPayableConsolidatedDto.IdleSalaryPercentage = 0;
            }
            else
            {
                salaryPayableConsolidatedDto.IdleSalaryPercentage = Math.Round((salaryPayableConsolidatedDto.IdleSalaryAmount / salaryPayableConsolidatedDto.TotalPayable * 100), 2);
            }

            var salaryPayableCostCentreList = (from cc in employeeSalaryPayableList
                                               group cc by new { cc.PersonalInformation.CostCentre } into g
                                               select new SalaryPayableCostCentreWise
                                               {
                                                   CostCentre = g.Key.CostCentre,
                                                   GrossSalaryAmount = g.Sum(t => t.GrossSalaryAmount),
                                                   DeductionAmount = g.Sum(t => t.DeductionAmount),
                                                   NetSalaryAmount = g.Sum(t => t.NetSalaryAmount),
                                                   ExcessLevyAmount = g.Sum(t => t.AbsentLevyAmount),
                                                   IdleSalaryAmount = g.Sum(t => t.IdleSalaryAmount),
                                                   FixedAmount = g.Sum(t => t.FixedAmount),
                                                   NoOfEmployees = g.Count(),
                                                   IdleSalaryPercentage = g.Sum(t => t.IdleSalaryAmount) == 0 || g.Sum(t => t.TotalPayable) == 0 ? 0 : Math.Round(g.Sum(t => t.IdleSalaryAmount) / g.Sum(t => t.TotalPayable) * 100, 2),
                                               }).ToList();

            salaryPayableConsolidatedDto.SalaryPayableCostCentreList = salaryPayableCostCentreList;
            List<PieChartData> costCentrePieGraph = new List<PieChartData>();

            foreach (var lst in salaryPayableCostCentreList)
            {
                if (lst.TotalPayable > 0 && salaryPayableConsolidatedDto.TotalPayable > 0)
                {
                    lst.TotalPayablePercentage = Math.Round(lst.TotalPayable / salaryPayableConsolidatedDto.TotalPayable * 100, 2);
                }
                costCentrePieGraph.Add(new PieChartData
                {
                    name = lst.CostCentre,
                    y = lst.TotalPayable
                });
            }
            salaryPayableConsolidatedDto.CostCentrePieGraph = costCentrePieGraph;

            var salaryPayableInchargeList = (from cc in employeeSalaryPayableList
                                             group cc by new { cc.PersonalInformation.InchargeEmployeeRefId } into g
                                             select new SalaryPayableInchargeWise
                                             {
                                                 InchargeEmployeeRefId = g.Key.InchargeEmployeeRefId.Value,
                                                 GrossSalaryAmount = g.Sum(t => t.GrossSalaryAmount),
                                                 DeductionAmount = g.Sum(t => t.DeductionAmount),
                                                 NetSalaryAmount = g.Sum(t => t.NetSalaryAmount),
                                                 ExcessLevyAmount = g.Sum(t => t.AbsentLevyAmount),
                                                 IdleSalaryAmount = g.Sum(t => t.IdleSalaryAmount),
                                                 FixedAmount = g.Sum(t => t.FixedAmount),
                                                 NoOfEmployees = g.Count(),
                                                 IdleSalaryPercentage = g.Sum(t => t.IdleSalaryAmount) == 0 || g.Sum(t => t.TotalPayable) == 0 ? 0 : Math.Round(g.Sum(t => t.IdleSalaryAmount) / g.Sum(t => t.TotalPayable) * 100, 2)
                                             }).ToList();

            List<PieChartData> inchargePieGraph = new List<PieChartData>();
            foreach (var lst in salaryPayableInchargeList)
            {
                if (lst.TotalPayable > 0 && salaryPayableConsolidatedDto.TotalPayable > 0)
                {
                    lst.TotalPayablePercentage = Math.Round(lst.TotalPayable / salaryPayableConsolidatedDto.TotalPayable * 100, 2);
                }

                var incharge = rsPerinfo.FirstOrDefault(t => t.Id == lst.InchargeEmployeeRefId);
                lst.InchargeEmployeeRefName = incharge.EmployeeName;
                inchargePieGraph.Add(new PieChartData
                {
                    name = lst.InchargeEmployeeRefName,
                    y = lst.TotalPayable
                });
            }
            salaryPayableConsolidatedDto.SalaryPayableInchargeWiseList = salaryPayableInchargeList;
            salaryPayableConsolidatedDto.InchargePieGraph = inchargePieGraph;

            DateTime executionEndTime = DateTime.Now;
            TimeSpan diff = executionEndTime.Subtract(executionStartTime);
            salaryPayableConsolidatedDto.ExecutionSeconds = diff.TotalSeconds;

            return salaryPayableConsolidatedDto;
        }

        protected async Task<SalaryPaidForTheMonth> GetSalaryValueForGivenEmployeeWithGivenDate(SalaryPaidForTheMonth output, GetSalaryInfoForEditOutput argSalaryMaster, PieChartOutputDto argSalaryMonthDutyDetails)
        {
            scaleForDecimalRound = 2;
            var input = output.InputData;
            List<WorkDayListDto> rsWorkDays = new List<WorkDayListDto>();
            var employeeDefaultWorkDays = await _salaryinfoappService.GetEmployeeWorkDay(new GetDataBasedOnNullableEmployee { EmployeeRefId = input.EmployeeRefId });
            rsWorkDays = employeeDefaultWorkDays.WorkDayListDtos;

            DateTime monthStartDate = input.StartDate;
            DateTime monthEndDate = input.EndDate;
            input.MonthYear = input.StartDate;
            if (input.StartDate == input.EndDate)
            {
                DateTime startDate = new DateTime(input.MonthYear.Value.Year, input.MonthYear.Value.Month, 01);
                var nextMonth = startDate.AddMonths(1);
                DateTime endDate = nextMonth.AddDays(-1);
                monthStartDate = startDate;
                monthEndDate = endDate;
            }

            decimal noOfWorkdaysStandard = await GetNoOfWorkingDays(new GetNoOfWorkingDaysUptoGivenDateForAMonth
            {
                StartDate = monthStartDate,
                EndDate = monthEndDate,
                EffectiveFrom = monthStartDate,
                RsWorkDaysList = rsWorkDays
            });

            decimal noOfWorkdaysActual = argSalaryMonthDutyDetails.SalaryCalculatedDays;
            decimal noOfIdleDays = argSalaryMonthDutyDetails.IdleDays;

            #region Basic Salary Calculation
            decimal basicPay = 0;
            decimal idleBasicPay = 0;
            string basicPayRemarks = "";
            if (noOfWorkdaysStandard == noOfWorkdaysActual)
            {
                if (argSalaryMaster.SalaryInfo.SalaryMode == (int)SalaryCalculationType.MONTHLY)
                {
                    basicPay = argSalaryMaster.SalaryInfo.BasicSalary;
                    basicPayRemarks = noOfWorkdaysActual + " " + L("Days");

                    idleBasicPay = argSalaryMaster.SalaryInfo.BasicSalary / noOfWorkdaysStandard * noOfIdleDays;
                }
                else if (argSalaryMaster.SalaryInfo.SalaryMode == (int)SalaryCalculationType.DAILY)
                {
                    basicPay = argSalaryMaster.SalaryInfo.BasicSalary * noOfWorkdaysActual;
                    basicPayRemarks = noOfWorkdaysActual + " " + L("Days");

                    idleBasicPay = argSalaryMaster.SalaryInfo.BasicSalary * noOfIdleDays;
                }
                else if (argSalaryMaster.SalaryInfo.SalaryMode == (int)SalaryCalculationType.HOURLY)
                {
                    basicPay = argSalaryMonthDutyDetails.NoOfWorkHours * argSalaryMaster.SalaryInfo.BasicSalary;
                    basicPayRemarks = argSalaryMonthDutyDetails.NoOfWorkHours + " " + L("Hours") + " * " + argSalaryMaster.SalaryInfo.BasicSalary;
                }
            }
            else
            {
                if (argSalaryMaster.SalaryInfo.SalaryMode == (int)SalaryCalculationType.MONTHLY)
                {
                    basicPay = Math.Round(argSalaryMaster.SalaryInfo.BasicSalary / noOfWorkdaysStandard, scaleForDecimalRound) * noOfWorkdaysActual;
                    basicPayRemarks = noOfWorkdaysActual + " " + L("Days");

                    idleBasicPay = argSalaryMaster.SalaryInfo.BasicSalary / noOfWorkdaysStandard * noOfIdleDays;
                }
                else if (argSalaryMaster.SalaryInfo.SalaryMode == (int)SalaryCalculationType.DAILY)
                {
                    basicPay = argSalaryMaster.SalaryInfo.BasicSalary * noOfWorkdaysActual;
                    basicPayRemarks = noOfWorkdaysActual + " " + L("Days");

                    idleBasicPay = argSalaryMaster.SalaryInfo.BasicSalary * noOfIdleDays;
                }
                else if (argSalaryMaster.SalaryInfo.SalaryMode == (int)SalaryCalculationType.HOURLY)
                {
                    basicPay = argSalaryMonthDutyDetails.NoOfWorkHours * argSalaryMaster.SalaryInfo.BasicSalary;
                    basicPayRemarks = argSalaryMonthDutyDetails.NoOfWorkHours + " " + L("Hours") + " * " + argSalaryMaster.SalaryInfo.BasicSalary;
                }
            }
            #endregion

            decimal grossSalaryAmount = basicPay;
            decimal idleSalaryAmount = idleBasicPay;
            decimal statutoryPayable = 0;
            decimal statutoryDeductable = 0;

            #region Salary Allowance Calculation
            foreach (var lst in argSalaryMaster.AllowanceSalaryTagList)
            {

                if (lst.EffectiveFrom > input.EndDate)
                    continue;

                decimal noOfDaysAllowanceAllowed = noOfWorkdaysActual;
                if (lst.EffectiveFrom > input.StartDate)
                {
                    noOfDaysAllowanceAllowed = await GetNoOfWorkingDays(new GetNoOfWorkingDaysUptoGivenDateForAMonth
                    {
                        StartDate = input.StartDate,
                        EndDate = input.EndDate,
                        EffectiveFrom = lst.EffectiveFrom,
                        RsWorkDaysList = rsWorkDays
                    });
                    lst.IsFixedAmount = false;
                }

                if (lst.CalculationPayModeRefId == (int)IncentivePayMode.Monthly)
                {
                    if (noOfWorkdaysStandard == noOfDaysAllowanceAllowed || lst.IsFixedAmount == true)
                    {
                        lst.AmountCalculatedForSalaryProcess = lst.Amount;
                    }
                    else
                    {
                        lst.AmountCalculatedForSalaryProcess = Math.Round(lst.Amount / noOfWorkdaysStandard, scaleForDecimalRound) * noOfDaysAllowanceAllowed;
                    }
                    lst.IdleAmount = Math.Round(lst.Amount / noOfWorkdaysStandard, scaleForDecimalRound) * noOfIdleDays;
                    lst.IdleRemarks = noOfIdleDays + L("Idle") + " " + L("Days");
                    lst.Remarks = noOfDaysAllowanceAllowed + " " + L("Days");
                }
                else if (lst.CalculationPayModeRefId == (int)IncentivePayMode.Daily)
                {
                    lst.AmountCalculatedForSalaryProcess = lst.Amount * noOfDaysAllowanceAllowed;
                    lst.Remarks = noOfDaysAllowanceAllowed + " " + L("Days");

                    lst.IdleAmount = lst.Amount * noOfIdleDays;
                    lst.IdleRemarks = noOfIdleDays + L("Idle") + " " + L("Days");
                }
                grossSalaryAmount = grossSalaryAmount + lst.AmountCalculatedForSalaryProcess;
                idleSalaryAmount = idleSalaryAmount + lst.IdleAmount;
            }
            #endregion

            #region Salary Incentive - Same Month Or Previous Month

            if (argSalaryMonthDutyDetails.IncentiveConsolidatedList == null)
            {
                argSalaryMonthDutyDetails.IncentiveConsolidatedList = new List<IncentiveConsolidatedDto>();
            }
            foreach (var lst in argSalaryMonthDutyDetails.IncentiveConsolidatedList)
            {
                grossSalaryAmount = grossSalaryAmount + lst.TotalIncentiveAmount;
            }
            #endregion


            #region Salary Deduction Calculation
            decimal deductionAmount = 0;
            foreach (var lst in argSalaryMaster.DeductionSalaryTagList)
            {
                if (lst.EffectiveFrom > input.EndDate)
                    continue;

                decimal noOfDaysDeductionAllowed = noOfWorkdaysActual;

                if (lst.EffectiveFrom > input.StartDate)
                {
                    noOfDaysDeductionAllowed = await GetNoOfWorkingDays(new GetNoOfWorkingDaysUptoGivenDateForAMonth
                    {
                        StartDate = input.StartDate,
                        EndDate = input.EndDate,
                        EffectiveFrom = lst.EffectiveFrom,
                        RsWorkDaysList = rsWorkDays
                    });
                    lst.IsFixedAmount = false;
                }

                if (lst.CalculationPayModeRefId == (int)IncentivePayMode.Monthly)
                {
                    if (noOfWorkdaysStandard == noOfDaysDeductionAllowed || lst.IsFixedAmount == true)
                    {
                        lst.AmountCalculatedForSalaryProcess = lst.Amount;
                    }
                    else
                    {
                        lst.AmountCalculatedForSalaryProcess = Math.Round(lst.Amount / noOfWorkdaysStandard, scaleForDecimalRound) * noOfDaysDeductionAllowed;
                    }
                }
                else if (lst.CalculationPayModeRefId == (int)IncentivePayMode.Daily)
                {
                    lst.AmountCalculatedForSalaryProcess = lst.Amount * noOfDaysDeductionAllowed;
                }
                deductionAmount = deductionAmount + lst.AmountCalculatedForSalaryProcess;
            }

            decimal deductionForAbsent = await _settingManager.GetSettingValueAsync<decimal>(AppSettings.HouseSettings.AbsentDeductionPerDay);

            #region Salary Deduction for ABSENT Status
            if (deductionForAbsent > 0 && argSalaryMonthDutyDetails.NoOfAbsentDays > 0)
            {
                EmployeeVsSalaryTagListDto absentDaysDeduction = new EmployeeVsSalaryTagListDto
                {
                    IncentivePayModeId = (int)IncentivePayMode.Monthly,
                    AllowanceOrDeductionRefId = (int)AllowanceOrDeductionMode.Deduction,
                    EmployeeRefId = output.PersonalInformation.Id,
                    EmployeeRefName = output.PersonalInformation.EmployeeName,
                    EffectiveFrom = output.PersonalInformation.DateHired,
                    SalaryRefId = -1,
                    SalaryRefName = "Absent Deduction",
                    Amount = argSalaryMonthDutyDetails.NoOfAbsentDays * deductionForAbsent,
                    AmountCalculatedForSalaryProcess = argSalaryMonthDutyDetails.NoOfAbsentDays * deductionForAbsent,
                    IsDynamicAmount = true,
                    IsSytemGeneratedIncentive = true,
                    IsItDeductableFromGrossSalary = true,
                    Remarks = argSalaryMonthDutyDetails.NoOfAbsentDays + " " + L("Absent") + " " + L("Days")
                };
                argSalaryMaster.DeductionSalaryTagList.Add(absentDaysDeduction);
                deductionAmount = deductionAmount + absentDaysDeduction.AmountCalculatedForSalaryProcess;
            }
            #endregion

            #endregion
            argSalaryMaster.AllowanceSalaryTagList = argSalaryMaster.AllowanceSalaryTagList.Where(t => t.EffectiveFrom <= input.EndDate).ToList();
            argSalaryMaster.DeductionSalaryTagList = argSalaryMaster.DeductionSalaryTagList.Where(t => t.EffectiveFrom <= input.EndDate).ToList();


            output.BasicPay = basicPay;
            output.BasicPayRemarks = basicPayRemarks;

            output.GrossSalaryAmount = grossSalaryAmount;

            output.AllowanceSalaryTagList = argSalaryMaster.AllowanceSalaryTagList;
            output.DeductionSalaryTagList = argSalaryMaster.DeductionSalaryTagList;

            output.NoOfWorkedDays = argSalaryMonthDutyDetails.NoOfActualWorkedDays;
            output.NoOfPaidLeaveDays = argSalaryMonthDutyDetails.NoOfPaidLeaveDays;
            output.NoOfIdleDays = argSalaryMonthDutyDetails.IdleDays;
            output.NoOfAbsentDays = argSalaryMonthDutyDetails.NoOfAbsentDays;
            output.NoOfPublicHolidays = argSalaryMonthDutyDetails.NoOfPublicHolidays;
            output.NoOfStandardWorkingDays = noOfWorkdaysStandard;
            output.SalaryInfo = argSalaryMaster.SalaryInfo;

            List<EmployeeVsSalaryTagListDto> statutoryList = new List<EmployeeVsSalaryTagListDto>();
            if (argSalaryMaster.StatutoryAllowanceList != null && argSalaryMaster.StatutoryAllowanceList.Count > 0)
                statutoryList.AddRange(argSalaryMaster.StatutoryAllowanceList);
            if (argSalaryMaster.StatutoryDeductionList != null && argSalaryMaster.StatutoryDeductionList.Count > 0)
                statutoryList.AddRange(argSalaryMaster.StatutoryDeductionList);

            var existAnyNullFormula = statutoryList.FirstOrDefault(t => t.FormulaRefId.HasValue == false);
            if (existAnyNullFormula != null)
            {
                throw new UserFriendlyException(existAnyNullFormula.SalaryTagAliasName + L(" Statutory Formula Is Null"));
            }

            List<FormulaCalculationDoneDto> formulaCalculationDoneLists = new List<FormulaCalculationDoneDto>();
            List<int> formulaRefIds = statutoryList.Where(t => t.FormulaRefId.HasValue).Select(t => t.FormulaRefId.Value).ToList();
            foreach (var fid in formulaRefIds)
            {
                formulaCalculationDoneLists.Add(new FormulaCalculationDoneDto { FormulaRefId = fid, AlreadyDoneFlag = false });
            }

            var countryName = FeatureChecker.GetValue(AppFeatures.ConnectCountry);

            #region Salary Statutory Allowance Calculation
            foreach (var lst in statutoryList)
            {
                var formulaRefId = lst.FormulaRefId.Value;
                var alreadyformulaCalculatedFlag = formulaCalculationDoneLists.FirstOrDefault(t => t.FormulaRefId == formulaRefId);
                if (alreadyformulaCalculatedFlag.AlreadyDoneFlag)
                    continue;

                if (lst.IsCalculationFinished)
                    continue;

                if (lst.EffectiveFrom > input.EndDate)
                    continue;

                decimal noOfDaysAllowanceAllowed = noOfWorkdaysActual;
                if (lst.EffectiveFrom > input.StartDate)
                {
                    noOfDaysAllowanceAllowed = await GetNoOfWorkingDays(new GetNoOfWorkingDaysUptoGivenDateForAMonth
                    {
                        StartDate = input.StartDate,
                        EndDate = input.EndDate,
                        EffectiveFrom = lst.EffectiveFrom,
                        RsWorkDaysList = rsWorkDays
                    });
                    lst.IsFixedAmount = false;
                }

                #region SG Statutory
                if (countryName.ToUpper().Equals("SG"))
                {
                    if (formulaRefId == (int)SpecialFormulaTagReference.SG_Employee_CPF || formulaRefId == (int)SpecialFormulaTagReference.SG_Employer_CPF)
                    {
                        var sgCpfDto = await GetSGCPF(output);
                        {
                            var sgCpfEmployer = statutoryList.FirstOrDefault(t => t.FormulaRefId == (int)SpecialFormulaTagReference.SG_Employer_CPF);
                            sgCpfEmployer.AmountCalculatedForSalaryProcess = sgCpfDto.EmployerPF;
                            sgCpfEmployer.IsCalculationFinished = true;
                            sgCpfEmployer.IdleAmount = Math.Round(lst.AmountCalculatedForSalaryProcess / noOfWorkdaysStandard, scaleForDecimalRound) * noOfIdleDays;
                            sgCpfEmployer.IdleRemarks = noOfIdleDays + L("Idle") + " " + L("Days");
                            sgCpfEmployer.Remarks = "Calculated For " + sgCpfDto.EligiblePFSalary.ToString("####0.00");
                            statutoryPayable = statutoryPayable + sgCpfEmployer.AmountCalculatedForSalaryProcess;
                            idleSalaryAmount = idleSalaryAmount + sgCpfEmployer.IdleAmount;
                        }
                        {
                            var sgCpfEmployee = statutoryList.FirstOrDefault(t => t.FormulaRefId == (int)SpecialFormulaTagReference.SG_Employee_CPF);
                            sgCpfEmployee.AmountCalculatedForSalaryProcess = sgCpfDto.EmployeePF;
                            sgCpfEmployee.IsCalculationFinished = true;
                            sgCpfEmployee.Remarks = "Calculated For " + sgCpfDto.EligiblePFSalary.ToString("####0.00");
                            statutoryDeductable = statutoryDeductable + sgCpfEmployee.AmountCalculatedForSalaryProcess;
                            idleSalaryAmount = idleSalaryAmount + sgCpfEmployee.IdleAmount;
                        }
                    }
                    else if (formulaRefId == (int)SpecialFormulaTagReference.SG_SINDA_DEDUCTION)
                    {
                        var sgSinda = await GetSINDAFund(output);
                        {
                            var sindaTag = statutoryList.FirstOrDefault(t => t.FormulaRefId == (int)SpecialFormulaTagReference.SG_SINDA_DEDUCTION);
                            sindaTag.AmountCalculatedForSalaryProcess = sgSinda.EmployeeSINDA;
                            sindaTag.IsCalculationFinished = true;
                            sindaTag.IdleAmount = Math.Round(lst.AmountCalculatedForSalaryProcess / noOfWorkdaysStandard, scaleForDecimalRound) * noOfIdleDays;
                            sindaTag.IdleRemarks = noOfIdleDays + L("Idle") + " " + L("Days");
                            sindaTag.Remarks = "SINDA";
                            statutoryDeductable = statutoryDeductable + sindaTag.AmountCalculatedForSalaryProcess;
                            idleSalaryAmount = idleSalaryAmount + sindaTag.IdleAmount;
                        }
                    }
                    else if (formulaRefId == (int)SpecialFormulaTagReference.SG_MBMF)
                    {
                        var sgMBMF = await GetMBMF(output);
                        {
                            var tag = statutoryList.FirstOrDefault(t => t.FormulaRefId == (int)SpecialFormulaTagReference.SG_MBMF);
                            tag.AmountCalculatedForSalaryProcess = sgMBMF.EmployeeMBMF;
                            tag.IsCalculationFinished = true;
                            tag.IdleAmount = Math.Round(lst.AmountCalculatedForSalaryProcess / noOfWorkdaysStandard, scaleForDecimalRound) * noOfIdleDays;
                            tag.IdleRemarks = noOfIdleDays + L("Idle") + " " + L("Days");
                            tag.Remarks = "MBMF";
                            statutoryDeductable = statutoryDeductable + tag.AmountCalculatedForSalaryProcess;
                            idleSalaryAmount = idleSalaryAmount + tag.IdleAmount;
                        }
                    }
                    else if (formulaRefId == (int)SpecialFormulaTagReference.SG_SDL)
                    {
                        var sgSDL = await GetSDL(output);
                        {
                            var tag = statutoryList.FirstOrDefault(t => t.FormulaRefId == (int)SpecialFormulaTagReference.SG_SDL);
                            tag.AmountCalculatedForSalaryProcess = sgSDL.EmployeeSDL;
                            tag.IsCalculationFinished = true;
                            tag.IdleAmount = Math.Round(lst.AmountCalculatedForSalaryProcess / noOfWorkdaysStandard, scaleForDecimalRound) * noOfIdleDays;
                            tag.IdleRemarks = noOfIdleDays + L("Idle") + " " + L("Days");
                            tag.Remarks = "SDL";
                            //if (tag.IsItDeductableFromGrossSalary == false)
                            {
                                statutoryPayable = statutoryPayable + tag.
                                    AmountCalculatedForSalaryProcess;
                            }

                            idleSalaryAmount = idleSalaryAmount + tag.IdleAmount;
                        }
                    }
                    else if (formulaRefId == (int)SpecialFormulaTagReference.SG_CDAC)
                    {
                        //var sgSDL = await getcda(output);
                        //{
                        //    var tag = statutoryList.FirstOrDefault(t => t.FormulaRefId == (int)SpecialFormulaTagReference.SG_CDAC);
                        //    tag.AmountCalculatedForSalaryProcess = sgSDL.EmployeeSDL;
                        //    tag.IsCalculationFinished = true;
                        //    tag.IdleAmount = Math.Round(lst.AmountCalculatedForSalaryProcess / noOfWorkdaysStandard, scaleForDecimalRound) * noOfIdleDays;
                        //    tag.IdleRemarks = noOfIdleDays + L("Idle") + " " + L("Days");
                        //    tag.Remarks = "SDL";
                        //    statutoryDeductable = statutoryDeductable + tag.AmountCalculatedForSalaryProcess;
                        //    idleSalaryAmount = idleSalaryAmount + tag.IdleAmount;
                        //}
                    }
                    else if (formulaRefId == (int)SpecialFormulaTagReference.SG_Eurasian)
                    {
                        //var sgEurosian = await getsg(output);
                        //{
                        //    var tag = statutoryList.FirstOrDefault(t => t.FormulaRefId == (int)SpecialFormulaTagReference.SG_CDAC);
                        //    tag.AmountCalculatedForSalaryProcess = sgSDL.EmployeeSDL;
                        //    tag.IsCalculationFinished = true;
                        //    tag.IdleAmount = Math.Round(lst.AmountCalculatedForSalaryProcess / noOfWorkdaysStandard, scaleForDecimalRound) * noOfIdleDays;
                        //    tag.IdleRemarks = noOfIdleDays + L("Idle") + " " + L("Days");
                        //    tag.Remarks = "SDL";
                        //    statutoryDeductable = statutoryDeductable + tag.AmountCalculatedForSalaryProcess;
                        //    idleSalaryAmount = idleSalaryAmount + tag.IdleAmount;
                        //}
                    }
                }
                #endregion

                #region India Statutory
                else if (countryName.ToUpper().Equals("IN"))
                {
                    #region India EPF
                    if (formulaRefId == (int)SpecialFormulaTagReference.IN_Employee_PF || formulaRefId == (int)SpecialFormulaTagReference.IN_Employer_PF || formulaRefId == (int)SpecialFormulaTagReference.IN_Employer_EPF_ADMIN_CHARGES || formulaRefId == (int)SpecialFormulaTagReference.IN_Employer_PENSION)
                    {
                        var inPfDto = await GetIndiaEPF(output);
                        {
                            var tag = statutoryList.FirstOrDefault(t => t.FormulaRefId == (int)SpecialFormulaTagReference.IN_Employer_PF);
                            if (tag == null)
                            {
                                throw new UserFriendlyException("Missing IN_Employer_PF For " + output.PersonalInformation.EmployeeName);
                            }
                            tag.AmountCalculatedForSalaryProcess = inPfDto.EmployerPF;
                            tag.IsCalculationFinished = true;
                            tag.IdleAmount = Math.Round(lst.AmountCalculatedForSalaryProcess / noOfWorkdaysStandard, scaleForDecimalRound) * noOfIdleDays;
                            tag.IdleRemarks = noOfIdleDays + L("Idle") + " " + L("Days");
                            tag.Remarks = noOfDaysAllowanceAllowed + " " + L("Days");
                            statutoryPayable = statutoryPayable + tag.AmountCalculatedForSalaryProcess;
                            idleSalaryAmount = idleSalaryAmount + tag.IdleAmount;
                        }
                        {
                            var tag = statutoryList.FirstOrDefault(t => t.FormulaRefId == (int)SpecialFormulaTagReference.IN_Employer_PENSION);
                            if (tag == null)
                            {
                                throw new UserFriendlyException("Missing IN_Employer_PENSION For " + output.PersonalInformation.EmployeeName);
                            }
                            tag.AmountCalculatedForSalaryProcess = inPfDto.EmployerPension;
                            tag.IsCalculationFinished = true;
                            tag.IdleAmount = Math.Round(lst.AmountCalculatedForSalaryProcess / noOfWorkdaysStandard, scaleForDecimalRound) * noOfIdleDays;
                            tag.IdleRemarks = noOfIdleDays + L("Idle") + " " + L("Days");
                            tag.Remarks = noOfDaysAllowanceAllowed + " " + L("Days");
                            statutoryPayable = statutoryPayable + tag.AmountCalculatedForSalaryProcess;
                            idleSalaryAmount = idleSalaryAmount + tag.IdleAmount;
                        }
                        {
                            var tag = statutoryList.FirstOrDefault(t => t.FormulaRefId == (int)SpecialFormulaTagReference.IN_Employer_EPF_ADMIN_CHARGES);
                            if (tag == null)
                            {
                                throw new UserFriendlyException("Missing IN_Employer_EPF_ADMIN_CHARGES For " + output.PersonalInformation.EmployeeName);
                            }
                            tag.AmountCalculatedForSalaryProcess = inPfDto.EmployerAdminCharge;
                            tag.IsCalculationFinished = true;
                            tag.IdleAmount = Math.Round(lst.AmountCalculatedForSalaryProcess / noOfWorkdaysStandard, scaleForDecimalRound) * noOfIdleDays;
                            tag.IdleRemarks = noOfIdleDays + L("Idle") + " " + L("Days");
                            tag.Remarks = noOfDaysAllowanceAllowed + " " + L("Days");
                            statutoryPayable = statutoryPayable + tag.AmountCalculatedForSalaryProcess;
                            idleSalaryAmount = idleSalaryAmount + tag.IdleAmount;
                        }
                        {
                            var tag = statutoryList.FirstOrDefault(t => t.FormulaRefId == (int)SpecialFormulaTagReference.IN_Employee_PF);
                            if (tag == null)
                            {
                                throw new UserFriendlyException("Missing IN_Employee_PF For " + output.PersonalInformation.EmployeeName);
                            }
                            tag.AmountCalculatedForSalaryProcess = inPfDto.EmployeePF;
                            tag.IsCalculationFinished = true;
                            tag.IdleAmount = 0;
                            tag.Remarks = noOfDaysAllowanceAllowed + " " + L("Days");
                            statutoryDeductable = statutoryDeductable + tag.AmountCalculatedForSalaryProcess;
                            idleSalaryAmount = idleSalaryAmount + tag.IdleAmount;
                        }
                    }
                    #endregion

                    #region India ESI
                    else if (formulaRefId == (int)SpecialFormulaTagReference.IN_ESI_Employee || formulaRefId == (int)SpecialFormulaTagReference.IN_ESI_Employer)
                    {
                        var inESIDto = await GetIndiaESI(output);
                        {
                            var tag = statutoryList.FirstOrDefault(t => t.FormulaRefId == (int)SpecialFormulaTagReference.IN_ESI_Employer);
                            if (tag == null)
                            {
                                throw new UserFriendlyException("Missing IN_ESI_Employer For " + output.PersonalInformation.EmployeeName);
                            }
                            tag.AmountCalculatedForSalaryProcess = inESIDto.EmployerESI;
                            tag.IsCalculationFinished = true;
                            tag.IdleAmount = Math.Round(lst.AmountCalculatedForSalaryProcess / noOfWorkdaysStandard, scaleForDecimalRound) * noOfIdleDays;
                            tag.IdleRemarks = noOfIdleDays + L("Idle") + " " + L("Days");
                            tag.Remarks = noOfDaysAllowanceAllowed + " " + L("Days");
                            statutoryPayable = statutoryPayable + tag.AmountCalculatedForSalaryProcess;
                            idleSalaryAmount = idleSalaryAmount + tag.IdleAmount;
                        }
                        {
                            var tag = statutoryList.FirstOrDefault(t => t.FormulaRefId == (int)SpecialFormulaTagReference.IN_ESI_Employee);
                            if (tag == null)
                            {
                                throw new UserFriendlyException("Missing IN_ESI_Employee For " + output.PersonalInformation.EmployeeName);
                            }
                            tag.AmountCalculatedForSalaryProcess = inESIDto.EmployeeESI;
                            tag.IsCalculationFinished = true;
                            tag.IdleAmount = Math.Round(lst.AmountCalculatedForSalaryProcess / noOfWorkdaysStandard, scaleForDecimalRound) * noOfIdleDays;
                            tag.IdleRemarks = noOfIdleDays + L("Idle") + " " + L("Days");
                            tag.Remarks = noOfDaysAllowanceAllowed + " " + L("Days");
                            statutoryDeductable = statutoryDeductable + tag.AmountCalculatedForSalaryProcess;
                            idleSalaryAmount = idleSalaryAmount + tag.IdleAmount;
                        }
                    }
                    #endregion

                    else if (formulaRefId == (int)SpecialFormulaTagReference.IN_PROFESSIONAL_TAX)
                    {
                        //var sgSinda = await GetSINDAFund(output);
                        //{
                        //    var sindaTag = statutoryList.FirstOrDefault(t => t.FormulaRefId == (int)SpecialFormulaTagReference.SG_SINDA_DEDUCTION);
                        //    sindaTag.AmountCalculatedForSalaryProcess = sgSinda.EmployeeSINDA;
                        //    sindaTag.IsCalculationFinished = true;
                        //    sindaTag.IdleAmount = Math.Round(lst.AmountCalculatedForSalaryProcess / noOfWorkdaysStandard, scaleForDecimalRound) * noOfIdleDays;
                        //    sindaTag.IdleRemarks = noOfIdleDays + L("Idle") + " " + L("Days");
                        //    sindaTag.Remarks = "SINDA";
                        //    statutoryDeductable = statutoryDeductable + sindaTag.AmountCalculatedForSalaryProcess;
                        //    idleSalaryAmount = idleSalaryAmount + sindaTag.IdleAmount;
                        //}
                    }
                    else if (formulaRefId == (int)SpecialFormulaTagReference.IN_LABOUR_WELFARE_FUND)
                    {
                        //var sgMBMF = await GetMBMF(output);
                        //{
                        //    var tag = statutoryList.FirstOrDefault(t => t.FormulaRefId == (int)SpecialFormulaTagReference.SG_MBMF);
                        //    tag.AmountCalculatedForSalaryProcess = sgMBMF.EmployeeMBMF;
                        //    tag.IsCalculationFinished = true;
                        //    tag.IdleAmount = Math.Round(lst.AmountCalculatedForSalaryProcess / noOfWorkdaysStandard, scaleForDecimalRound) * noOfIdleDays;
                        //    tag.IdleRemarks = noOfIdleDays + L("Idle") + " " + L("Days");
                        //    tag.Remarks = "MBMF";
                        //    statutoryDeductable = statutoryDeductable + tag.AmountCalculatedForSalaryProcess;
                        //    idleSalaryAmount = idleSalaryAmount + tag.IdleAmount;
                        //}
                    }
                }
                #endregion

                //@@Pending 
            }
            #endregion

            argSalaryMaster.StatutoryAllowanceList = statutoryList.Where(t => t.AllowanceOrDeductionRefId == (int)AllowanceOrDeductionMode.Statutory_Allowance && t.EffectiveFrom <= input.EndDate).ToList();
            argSalaryMaster.StatutoryDeductionList = statutoryList.Where(t => t.AllowanceOrDeductionRefId == (int)AllowanceOrDeductionMode.Statutory_Deduction && t.EffectiveFrom <= input.EndDate).ToList();

            output.StatutoryAllowance = statutoryPayable;
            output.StatutoryDeduction = statutoryDeductable;
            output.DeductionAmount = deductionAmount;

            decimal grossDeductionAmount = deductionAmount + statutoryDeductable;
            decimal netSalaryAmount = grossSalaryAmount - grossDeductionAmount;
            output.NetSalaryAmount = netSalaryAmount;



            output.StatutoryAllowanceTagList = argSalaryMaster.StatutoryAllowanceList;
            output.StatutoryDeductionTagList = argSalaryMaster.StatutoryDeductionList;
            output.IncentiveConsolidatedList = argSalaryMonthDutyDetails.IncentiveConsolidatedList;

            if (noOfIdleDays > 0 && grossSalaryAmount > 0)
            {
                output.IdleSalaryAmount = idleSalaryAmount;
                output.IdleSalaryRemarks = noOfIdleDays + " " + L("Idle") + " " + L("Days");
                output.IdleSalarypercentage = Math.Round(idleSalaryAmount / grossSalaryAmount * 100, 2);
            }
            output.SkillSetList = argSalaryMaster.SkillSetList;


            output.SuccessFlag = true;


            //var pfList = await GetIndiaEPF(output);
            //var esiList = await GetIndiaESI(output);
            // var SINDAList = await GetSINDAFund(output);
            //var sdlList = await GetSDL(output);
            //var scpf = await GetSGCPF(output);

            //output.StoreInDbFlag = false;
            if (output.StoreInDbFlag)
            {
                //  Find Already Exists Or Not
                int newSalaryPaidMasterId;
                var existSalaryPaid = await _salaryPaidMasterRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == output.PersonalInformation.Id && DbFunctions.TruncateTime(t.StartDate) == DbFunctions.TruncateTime(output.InputData.StartDate) && DbFunctions.TruncateTime(t.StartDate) == DbFunctions.TruncateTime(output.InputData.StartDate));
                if (existSalaryPaid == null)
                {
                    SalaryPaidMaster salaryPaidMasterDto = new SalaryPaidMaster
                    {
                        EmployeeRefId = output.PersonalInformation.Id,
                        JobTitle = output.PersonalInformation.JobTitle,
                        IsBillable = output.PersonalInformation.isBillable,
                        CompanyRefId = output.PersonalInformation.CompanyRefId,
                        SalaryAsOnDate = output.InputData.EndDate, // to be verified either monthyear or end  date to be stored
                        StartDate = output.InputData.StartDate,

                        EndDate = output.InputData.EndDate,
                        SalaryMode = argSalaryMaster.SalaryInfo.SalaryMode,
                        WithEffectFrom = argSalaryMaster.SalaryInfo.WithEffectFrom,
                        BasicSalary = argSalaryMaster.SalaryInfo.BasicSalary,
                        Ot1PerHour = argSalaryMaster.SalaryInfo.Ot1PerHour,
                        Ot2PerHour = argSalaryMaster.SalaryInfo.Ot2PerHour,
                        OtAllowedIfWorkTimeOver = argSalaryMaster.SalaryInfo.OtAllowedIfWorkTimeOver,
                        ReportOtPerHour = argSalaryMaster.SalaryInfo.ReportOtPerHour,

                        IncentiveAndOTCalculationPeriodRefId = argSalaryMaster.SalaryInfo.IncentiveAndOTCalculationPeriodRefId,

                        MaximumOTAllowedHoursOnPublicHoliday = argSalaryMaster.SalaryInfo.MaximumAllowedOTHoursOnPublicHoliday,
                        NoOfStandardWorkingDays = output.NoOfStandardWorkingDays,
                        NoOfWorkedDays = output.NoOfWorkedDays,
                        NoOfPaidLeaveDays = output.NoOfPaidLeaveDays,
                        NoOfAbsentDays = output.NoOfAbsentDays,
                        NoOfIdleDays = output.NoOfIdleDays,
                        NoOfExcessLevyDays = 0, // to be included in output DTO
                        BasicPay = output.BasicPay,
                        BasicPayRemarks = output.BasicPayRemarks,
                        GrossSalaryAmount = output.GrossSalaryAmount,
                        DeductionAmount = output.DeductionAmount,
                        NetSalaryAmount = output.NetSalaryAmount,
                        IdleSalaryAmount = output.IdleSalaryAmount,
                        IdleSalaryRemarks = output.IdleSalaryRemarks,
                        IdleSalarypercentage = output.IdleSalarypercentage,
                        AbsentLevyAmount = output.AbsentLevyAmount,
                        AbsentLevyRemarks = output.AbsentLevyRemarks,
                        ExcessLevyAmount = 0, // to be added in future
                        ExcessLevyRemarks = "",
                        NoOfWorkHours = 0,
                        NoOfIdleHours = 0,
                        NoOfOT2Hours = 0,
                        StatutoryAllowance = output.StatutoryAllowance,
                        StatutoryDeduction = output.StatutoryDeduction

                        // tenant id

                    };
                    newSalaryPaidMasterId = await _salaryPaidMasterRepo.InsertOrUpdateAndGetIdAsync(salaryPaidMasterDto);
                }
                else
                {
                    newSalaryPaidMasterId = existSalaryPaid.Id;
                    existSalaryPaid.EmployeeRefId = output.PersonalInformation.Id;
                    existSalaryPaid.JobTitle = output.PersonalInformation.JobTitle;
                    existSalaryPaid.IsBillable = output.PersonalInformation.isBillable;
                    existSalaryPaid.CompanyRefId = output.PersonalInformation.CompanyRefId;
                    existSalaryPaid.SalaryAsOnDate = output.InputData.EndDate; // to be verified either monthyear or end  date to be stored
                    existSalaryPaid.StartDate = output.InputData.StartDate;

                    existSalaryPaid.EndDate = output.InputData.EndDate;
                    existSalaryPaid.SalaryMode = argSalaryMaster.SalaryInfo.SalaryMode;
                    existSalaryPaid.WithEffectFrom = argSalaryMaster.SalaryInfo.WithEffectFrom;
                    existSalaryPaid.BasicSalary = argSalaryMaster.SalaryInfo.BasicSalary;
                    existSalaryPaid.Ot1PerHour = argSalaryMaster.SalaryInfo.Ot1PerHour;
                    existSalaryPaid.Ot2PerHour = argSalaryMaster.SalaryInfo.Ot2PerHour;
                    existSalaryPaid.OtAllowedIfWorkTimeOver = argSalaryMaster.SalaryInfo.OtAllowedIfWorkTimeOver;
                    existSalaryPaid.ReportOtPerHour = argSalaryMaster.SalaryInfo.ReportOtPerHour;
                    existSalaryPaid.IncentiveAndOTCalculationPeriodRefId = argSalaryMaster.SalaryInfo.IncentiveAndOTCalculationPeriodRefId;

                    existSalaryPaid.MaximumOTAllowedHoursOnPublicHoliday = argSalaryMaster.SalaryInfo.MaximumAllowedOTHoursOnPublicHoliday;
                    existSalaryPaid.NoOfStandardWorkingDays = output.NoOfStandardWorkingDays;
                    existSalaryPaid.NoOfWorkedDays = output.NoOfWorkedDays;
                    existSalaryPaid.NoOfPaidLeaveDays = output.NoOfPaidLeaveDays;
                    existSalaryPaid.NoOfAbsentDays = output.NoOfAbsentDays;
                    existSalaryPaid.NoOfIdleDays = output.NoOfIdleDays;
                    existSalaryPaid.NoOfExcessLevyDays = 0; // to be included in output DTO
                    existSalaryPaid.BasicPay = output.BasicPay;
                    existSalaryPaid.BasicPayRemarks = output.BasicPayRemarks;
                    existSalaryPaid.GrossSalaryAmount = output.GrossSalaryAmount;
                    existSalaryPaid.DeductionAmount = output.DeductionAmount;
                    existSalaryPaid.NetSalaryAmount = output.NetSalaryAmount;
                    existSalaryPaid.IdleSalaryAmount = output.IdleSalaryAmount;
                    existSalaryPaid.IdleSalaryRemarks = output.IdleSalaryRemarks;
                    existSalaryPaid.IdleSalarypercentage = output.IdleSalarypercentage;
                    existSalaryPaid.AbsentLevyAmount = output.AbsentLevyAmount;
                    existSalaryPaid.AbsentLevyRemarks = output.AbsentLevyRemarks;
                    existSalaryPaid.ExcessLevyAmount = 0; // to be added in future
                    existSalaryPaid.ExcessLevyRemarks = "";
                    existSalaryPaid.NoOfWorkHours = 0;
                    existSalaryPaid.NoOfIdleHours = 0;
                    existSalaryPaid.NoOfOT2Hours = 0;
                    existSalaryPaid.StatutoryAllowance = output.StatutoryAllowance;
                    existSalaryPaid.StatutoryDeduction = output.StatutoryDeduction;
                    await _salaryPaidMasterRepo.UpdateAsync(existSalaryPaid);
                }





                var consolidatedSalaryDetailList = new List<EmployeeVsSalaryTagListDto>();
                consolidatedSalaryDetailList.AddRange(output.AllowanceSalaryTagList);
                consolidatedSalaryDetailList.AddRange(output.DeductionSalaryTagList);
                consolidatedSalaryDetailList.AddRange(output.StatutoryAllowanceTagList);
                consolidatedSalaryDetailList.AddRange(output.StatutoryDeductionTagList);
                List<int> availalbeTagRefIds = new List<int>();

                using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.SoftDelete))
                {
                    var rsExistDetailList = await _salaryPaidDetailRepo.GetAllListAsync(t => t.SalaryPaidMasterRefId == newSalaryPaidMasterId);
                    foreach (var saldetail in consolidatedSalaryDetailList)
                    {
                        availalbeTagRefIds.Add(saldetail.SalaryRefId);
                        var detail = rsExistDetailList.FirstOrDefault(t => t.SalaryPaidMasterRefId == newSalaryPaidMasterId && t.SalaryRefId == saldetail.SalaryRefId);
                        if (detail == null)
                        {
                            var newdetail = new SalaryPaidDetail
                            {
                                SalaryPaidMasterRefId = newSalaryPaidMasterId,
                                IdleAmount = saldetail.IdleAmount,
                                IdleRemarks = saldetail.IdleRemarks,
                                EffectiveFrom = saldetail.EffectiveFrom,
                                AllowanceOrDeductionRefId = saldetail.AllowanceOrDeductionRefId,
                                Amount = saldetail.Amount,
                                MinimumAmount = saldetail.MinimumAmount,
                                MaximumAmount = saldetail.MaximumAmount,
                                AmountCalculatedForSalaryProcess = saldetail.AmountCalculatedForSalaryProcess,
                                IsDynamicAmount = saldetail.IsDynamicAmount,
                                CalculationPayModeRefId = saldetail.CalculationPayModeRefId,
                                RollBackFrom = saldetail.RollBackFrom,
                                SalaryRefId = saldetail.SalaryRefId,
                                FormulaRefId = saldetail.FormulaRefId,
                                SalaryTagAliasName = saldetail.SalaryTagAliasName,
                                IsItDeductableFromGrossSalary = saldetail.IsItDeductableFromGrossSalary,
                                Remarks = saldetail.Remarks,
                                IsSytemGeneratedIncentive = saldetail.IsSytemGeneratedIncentive,
                                IsFixedAmount = saldetail.IsFixedAmount,
                                IncentivePayModeId = saldetail.IncentivePayModeId,
                                IsThisStatutoryAddedWithGrossSalary = saldetail.IsThisStatutoryAddedWithGrossSalary
                            };
                            var salDetailId = await _salaryPaidDetailRepo.InsertOrUpdateAndGetIdAsync(newdetail);
                        }
                        else
                        {
                            detail.SalaryPaidMasterRefId = newSalaryPaidMasterId;
                            detail.IdleAmount = saldetail.IdleAmount;
                            detail.IdleRemarks = saldetail.IdleRemarks;
                            detail.EffectiveFrom = saldetail.EffectiveFrom;
                            detail.AllowanceOrDeductionRefId = saldetail.AllowanceOrDeductionRefId;
                            detail.Amount = saldetail.Amount;
                            detail.AmountCalculatedForSalaryProcess = saldetail.AmountCalculatedForSalaryProcess;
                            detail.IsDynamicAmount = saldetail.IsDynamicAmount;
                            detail.CalculationPayModeRefId = saldetail.CalculationPayModeRefId;
                            detail.RollBackFrom = saldetail.RollBackFrom;
                            detail.SalaryRefId = saldetail.SalaryRefId;
                            detail.FormulaRefId = saldetail.FormulaRefId;
                            detail.SalaryTagAliasName = saldetail.SalaryTagAliasName;
                            detail.IsItDeductableFromGrossSalary = saldetail.IsItDeductableFromGrossSalary;
                            detail.Remarks = saldetail.Remarks;
                            detail.IsSytemGeneratedIncentive = saldetail.IsSytemGeneratedIncentive;
                            detail.IsFixedAmount = saldetail.IsFixedAmount;
                            detail.IncentivePayModeId = saldetail.IncentivePayModeId;
                            detail.IsThisStatutoryAddedWithGrossSalary = saldetail.IsThisStatutoryAddedWithGrossSalary;
                            if (detail.IsDeleted)
                            {
                                detail.DeleterUserId = null;
                                detail.DeletionTime = null;
                                detail.IsDeleted = false;
                            }
                            var salDetailId = await _salaryPaidDetailRepo.UpdateAsync(detail);
                        }

                    }
                }

                var listToBeRemoved = await _salaryPaidDetailRepo.GetAllListAsync(t => t.SalaryPaidMasterRefId == newSalaryPaidMasterId && !availalbeTagRefIds.Contains(t.SalaryRefId));
                foreach (var delDetail in listToBeRemoved)
                {
                    await _salaryPaidDetailRepo.DeleteAsync(delDetail.Id);
                }
                // If not exists , need to create a new one 
            }

            return output;
        }

        public async Task<FileDto> GetSalarySlipExcelOrPdf(SalaryPrepareDto input)
        {
            input.StoreInDbFlag = true;
            var allItems = await GetSalaryForTheGivenMonth(input);
            if (input.ExportTypeRefId == (int)ExportType.Excel)
            {
                return await _timesheetmasterExporter.GenerateSalarySlipExcelFile(allItems);
            }
            else
            {
                return await _timesheetmasterExporter.SalarySlipExcelToPdfConversion(allItems);
            }
        }


        protected async Task<decimal> GetNoOfWorkingDays(GetNoOfWorkingDaysUptoGivenDateForAMonth input)
        {
            var rsWorkDays = input.RsWorkDaysList;
            var rsPublicHolidays = await _publicHolidayRepo.GetAllListAsync(t => t.HolidayDate >= input.StartDate && t.HolidayDate <= input.EndDate);
            decimal noOfWorkDays = 0;
            DateTime workDate = input.EffectiveFrom;
            do
            {
                int wd = (int)workDate.DayOfWeek;
                var rsWorkDay = rsWorkDays.FirstOrDefault(t => t.DayOfWeekRefId == wd);
                if (rsWorkDay == null)
                {
                    throw new UserFriendlyException(L("WorkDayNotSettingProperly"));
                }
                if (input.IsWeekOffAlsoConsiderForWorkDay == false)
                {
                    noOfWorkDays = noOfWorkDays + rsWorkDay.NoOfWorkDays;
                }
                else
                {
                    noOfWorkDays++;
                }
                workDate = workDate.AddDays(1);
            } while (workDate.Date <= input.EndDate.Date);

            return noOfWorkDays;
        }

        public async Task<IndiaPFOutputDto> GetIndiaEPF(SalaryPaidForTheMonth input)
        {
            //  CalculationAfterArrivedGrossSalary  -    Needs to verify
            //  After 58 Years No PF
            var employee = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == input.InputData.EmployeeRefId);
            if (employee == null)
            {
                throw new UserFriendlyException(L("NotExistForId", L("Employee"), input.InputData.EmployeeRefId));
            }

            //@@ Pending Needs to check the age, and if the age above 58 not needed to deduct the PF 

            //@@@@ Pending Needs To find the Exclusion Tags or Salary Tags
            decimal minimumPfCeilingValue = 0m;
            decimal maximumPfCeilingValue = 15000m;
            decimal employeePfDefaultPercentage = 12m;
            decimal employerPfDefaultPercentage = 3.67m;
            decimal employerPensionDefaultPercentage = 8.33m;
            int roundedOff = 0;

            decimal eligiblePfValue = input.GrossSalaryAmount;

            if (input.ExcludedDto != null)
            {
                input.ExcludedDto = new List<ExcludedFormulaList>();

                var excludedFormulaList = input.ExcludedDto.FirstOrDefault(t => t.IncentiveTagRefId == 1);

                foreach (var lst in excludedFormulaList.ExcludedSalaryList)
                {
                    eligiblePfValue = eligiblePfValue - lst.AmountCalculatedForSalaryProcess;
                }

                foreach (var lst in excludedFormulaList.ExcludedIncentiveList)
                {
                    eligiblePfValue = eligiblePfValue - lst.Amount;
                }
            }

            //Exclude the Salary Tags those PF Payable is false
            if (input.AllowanceSalaryTagList != null)
            {
                var excludedAllowance = input.AllowanceSalaryTagList.Where(t => t.IsPfPayable == false).ToList();
                foreach (var lst in excludedAllowance)
                {
                    eligiblePfValue = eligiblePfValue - lst.AmountCalculatedForSalaryProcess;
                }
            }

            //Exclude the Incentive Tags those PF Payable is false
            if (input.IncentiveConsolidatedList != null)
            {
                var excludedIncentive = input.IncentiveConsolidatedList.Where(t => t.IsPfPayable == false).ToList();
                foreach (var lst in excludedIncentive)
                {
                    eligiblePfValue = eligiblePfValue - lst.TotalIncentiveAmount;
                }
            }
            //@@ Maximum Exclusion percentage to be defined and send the alert

            if (eligiblePfValue > maximumPfCeilingValue)
            {
                eligiblePfValue = maximumPfCeilingValue;
            }

            var employeePF = Math.Round(eligiblePfValue * employeePfDefaultPercentage / 100, roundedOff);
            //(decimal) eligiblePfValue * employeePfDefaultPercentage / 100m; //(decimal) Math.Round( eligiblePfValue * employeePfDefaultPercentage/100 ,roundedOff);
            decimal employerPF = Math.Round(eligiblePfValue * employerPfDefaultPercentage / 100, roundedOff);
            decimal employerPension = Math.Round(eligiblePfValue * employerPensionDefaultPercentage / 100, roundedOff);

            IndiaPFOutputDto outputDto = new IndiaPFOutputDto
            {
                EligiblePFSalary = eligiblePfValue,
                EmployeeRefId = input.InputData.EmployeeRefId,
                EmployeePF = employeePF,
                EmployerPF = employerPF,
                EmployerPension = employerPension
            };
            return outputDto;
        }

        public async Task<EurasianCFOutputDTO> GetEurasianCF(SalaryPaidForTheMonth input)
        {
            var employee = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == input.InputData.EmployeeRefId);
            if (employee == null)
            {
                throw new UserFriendlyException(L("NotExistForId", L("Employee"), input.InputData.EmployeeRefId));
            }


            decimal eligibleEurasionCFValue;
            decimal eligibleECFSalary = input.GrossSalaryAmount;


            if (input.ExcludedDto != null)
            {
                input.ExcludedDto = new List<ExcludedFormulaList>();

                var excludedFormulaList = input.ExcludedDto.FirstOrDefault(t => t.IncentiveTagRefId == 1);

                foreach (var lst in excludedFormulaList.ExcludedSalaryList)
                {
                    eligibleECFSalary = eligibleECFSalary - lst.AmountCalculatedForSalaryProcess;
                }

                foreach (var lst in excludedFormulaList.ExcludedIncentiveList)
                {
                    eligibleECFSalary = eligibleECFSalary - lst.Amount;
                }
            }

            //@@ Maximum Exclusion percentage to be defined and send the alert

            if (eligibleECFSalary <= 1000)
            {
                eligibleEurasionCFValue = 2;
            }
            else if (eligibleECFSalary > 1000 && eligibleECFSalary <= 1500)
            {
                eligibleEurasionCFValue = 4;
            }


            else if (eligibleECFSalary > 1500 && eligibleECFSalary <= 2500)
            {
                eligibleEurasionCFValue = 6;
            }

            else if (eligibleECFSalary > 2500 && eligibleECFSalary <= 4000)
            {
                eligibleEurasionCFValue = 9;
            }

            else if (eligibleECFSalary > 4000 && eligibleECFSalary <= 7000)
            {
                eligibleEurasionCFValue = 12;
            }
            else if (eligibleECFSalary > 7000 && eligibleECFSalary <= 10000)
            {
                eligibleEurasionCFValue = 16;
            }
            else
            {
                eligibleEurasionCFValue = 20;
            }



            EurasianCFOutputDTO outputDto = new EurasianCFOutputDTO
            {
                EmployeeRefId = input.InputData.EmployeeRefId,
                EligibleEurasionSalary = eligibleECFSalary,
                EmployeeEurasianCF = eligibleEurasionCFValue

            };
            return outputDto;
        }

        public async Task<SINDAFundOutputDto> GetSINDAFund(SalaryPaidForTheMonth input)
        {
            var employee = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == input.InputData.EmployeeRefId);
            if (employee == null)
            {
                throw new UserFriendlyException(L("NotExistForId", L("Employee"), input.InputData.EmployeeRefId));
            }


            decimal eligibleSINDAValue;
            decimal eligibleSINDASalary = input.GrossSalaryAmount;
            if (input.ExcludedDto != null)
            {
                input.ExcludedDto = new List<ExcludedFormulaList>();

                var excludedFormulaList = input.ExcludedDto.FirstOrDefault(t => t.IncentiveTagRefId == 1);

                foreach (var lst in excludedFormulaList.ExcludedSalaryList)
                {
                    eligibleSINDASalary = eligibleSINDASalary - lst.AmountCalculatedForSalaryProcess;
                }

                foreach (var lst in excludedFormulaList.ExcludedIncentiveList)
                {
                    eligibleSINDASalary = eligibleSINDASalary - lst.Amount;
                }
            }

            //@@ Maximum Exclusion percentage to be defined and send the alert

            if (eligibleSINDASalary <= 1000)
            {
                eligibleSINDAValue = 1;
            }
            else if (eligibleSINDASalary > 1000 && eligibleSINDASalary <= 1500)
            {
                eligibleSINDAValue = 3;
            }


            else if (eligibleSINDASalary > 1500 && eligibleSINDASalary <= 2500)
            {
                eligibleSINDAValue = 5;
            }

            else if (eligibleSINDASalary > 2500 && eligibleSINDASalary <= 4500)
            {
                eligibleSINDAValue = 7;
            }

            else if (eligibleSINDASalary > 4500 && eligibleSINDASalary <= 7500)
            {
                eligibleSINDAValue = 9;
            }
            else if (eligibleSINDASalary > 7500 && eligibleSINDASalary <= 10000)
            {
                eligibleSINDAValue = 12;
            }
            else if (eligibleSINDASalary > 10000 && eligibleSINDASalary <= 15000)
            {
                eligibleSINDAValue = 18;
            }
            else
            {
                eligibleSINDAValue = 30;
            }



            SINDAFundOutputDto outputDto = new SINDAFundOutputDto
            {
                EmployeeRefId = input.InputData.EmployeeRefId,
                EligbileSINDASalary = eligibleSINDASalary,
                EmployeeSINDA = eligibleSINDAValue
            };
            return outputDto;
        }

        public async Task<MBMFOutputDto> GetMBMF(SalaryPaidForTheMonth input)
        {
            var employee = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == input.InputData.EmployeeRefId);
            if (employee == null)
            {
                throw new UserFriendlyException(L("NotExistForId", L("Employee"), input.InputData.EmployeeRefId));
            }


            decimal eligibleMBMFValue;
            decimal eligibleMBMFSalary = input.GrossSalaryAmount;
            if (input.ExcludedDto != null)
            {
                input.ExcludedDto = new List<ExcludedFormulaList>();

                var excludedFormulaList = input.ExcludedDto.FirstOrDefault(t => t.IncentiveTagRefId == 1);

                foreach (var lst in excludedFormulaList.ExcludedSalaryList)
                {
                    eligibleMBMFSalary = eligibleMBMFSalary - lst.AmountCalculatedForSalaryProcess;
                }

                foreach (var lst in excludedFormulaList.ExcludedIncentiveList)
                {
                    eligibleMBMFSalary = eligibleMBMFSalary - lst.Amount;
                }
            }

            //@@ Maximum Exclusion percentage to be defined and send the alert

            if (eligibleMBMFSalary <= 1000)
            {
                eligibleMBMFValue = 3;
            }
            else if (eligibleMBMFSalary > 1000 && eligibleMBMFSalary <= 2000)
            {
                eligibleMBMFValue = 4.50m;
            }


            else if (eligibleMBMFSalary > 2000 && eligibleMBMFSalary <= 3000)
            {
                eligibleMBMFValue = 6.50m;
            }

            else if (eligibleMBMFSalary > 3000 && eligibleMBMFSalary <= 4000)
            {
                eligibleMBMFValue = 15;
            }

            else if (eligibleMBMFSalary > 4000 && eligibleMBMFSalary <= 6000)
            {
                eligibleMBMFValue = 19.50m;
            }
            else if (eligibleMBMFSalary > 6000 && eligibleMBMFSalary <= 8000)
            {
                eligibleMBMFValue = 22;
            }
            else if (eligibleMBMFSalary > 8000 && eligibleMBMFSalary <= 10000)
            {
                eligibleMBMFValue = 24;
            }
            else
            {
                eligibleMBMFValue = 26;
            }



            MBMFOutputDto outputDto = new MBMFOutputDto
            {
                EmployeeRefId = input.InputData.EmployeeRefId,
                EligbileMBMFalary = eligibleMBMFSalary,
                EmployeeMBMF = eligibleMBMFValue
            };
            return outputDto;
        }


        public async Task<SDLOutputDto> GetSDL(SalaryPaidForTheMonth input)
        {
            var employee = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == input.InputData.EmployeeRefId);
            if (employee == null)
            {
                throw new UserFriendlyException(L("NotExistForId", L("Employee"), input.InputData.EmployeeRefId));
            }


            decimal eligibleSDLValue;
            decimal eligibleSDLSalary = input.GrossSalaryAmount;
            decimal sdl_Percentage = 0.25m;
            decimal sdl_MinValue = 2.00m;
            decimal sdl_MaxValue = 11.25m;


            if (input.ExcludedDto != null)
            {
                input.ExcludedDto = new List<ExcludedFormulaList>();

                var excludedFormulaList = input.ExcludedDto.FirstOrDefault(t => t.IncentiveTagRefId == 1);

                foreach (var lst in excludedFormulaList.ExcludedSalaryList)
                {
                    eligibleSDLSalary = eligibleSDLSalary - lst.AmountCalculatedForSalaryProcess;
                }

                foreach (var lst in excludedFormulaList.ExcludedIncentiveList)
                {
                    eligibleSDLSalary = eligibleSDLSalary - lst.Amount;
                }
            }

            //@@ Maximum Exclusion percentage to be defined and send the alert

            eligibleSDLValue = Math.Round(eligibleSDLSalary * sdl_Percentage / 100, 2);

            if (eligibleSDLValue < sdl_MinValue)
            {
                eligibleSDLValue = sdl_MinValue;
            }
            else if (eligibleSDLValue > sdl_MaxValue)
            {
                eligibleSDLValue = sdl_MaxValue;

            }


            SDLOutputDto outputDto = new SDLOutputDto
            {
                EmployeeRefId = input.InputData.EmployeeRefId,
                EmployeeSDL = eligibleSDLValue,
                SDL_Percentage = sdl_Percentage
            };
            return outputDto;
        }


        public async Task<IndiaESIOutputDto> GetIndiaESI(SalaryPaidForTheMonth input)
        {
            //  CalculationAfterArrivedGrossSalary  -    Needs to verify
            //  After 58 Years No PF
            var employee = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == input.InputData.EmployeeRefId);
            if (employee == null)
            {
                throw new UserFriendlyException(L("NotExistForId", L("Employee"), input.InputData.EmployeeRefId));
            }

            //@@ Pending Needs to check the age, and if the age above 58 not needed to deduct the PF 

            //@@@@ Pending Needs To find the Exclusion Tags or Salary Tags
            decimal minimumESICeilingValue = 0m;
            decimal maximumEsiCeilingValue = 21000m;
            decimal employeeEsiDefaultPercentage = 4.75m;
            decimal employerEsiDefaultPercentage = 1.75m;
            int roundedOff = 0;


            decimal eligibleEsiValue = input.GrossSalaryAmount;

            if (input.ExcludedDto != null)
            {
                input.ExcludedDto = new List<ExcludedFormulaList>();

                var excludedFormulaList = input.ExcludedDto.FirstOrDefault(t => t.IncentiveTagRefId == 1);

                foreach (var lst in excludedFormulaList.ExcludedSalaryList)
                {
                    eligibleEsiValue = eligibleEsiValue - lst.AmountCalculatedForSalaryProcess;
                }

                foreach (var lst in excludedFormulaList.ExcludedIncentiveList)
                {
                    eligibleEsiValue = eligibleEsiValue - lst.Amount;
                }
            }
            //Exclude the Salary Tags those PF Payable is false
            if (input.AllowanceSalaryTagList != null)
            {
                var excludedAllowance = input.AllowanceSalaryTagList.Where(t => t.IsPfPayable == false).ToList();
                foreach (var lst in excludedAllowance)
                {
                    eligibleEsiValue = eligibleEsiValue - lst.AmountCalculatedForSalaryProcess;
                }
            }

            //Exclude the Incentive Tags those PF Payable is false
            if (input.IncentiveConsolidatedList != null)
            {
                var excludedIncentive = input.IncentiveConsolidatedList.Where(t => t.IsPfPayable == false).ToList();
                foreach (var lst in excludedIncentive)
                {
                    eligibleEsiValue = eligibleEsiValue - lst.TotalIncentiveAmount;
                }
            }

            //@@ Maximum Exclusion percentage to be defined and send the alert

            if (eligibleEsiValue > maximumEsiCeilingValue)
            {
                eligibleEsiValue = maximumEsiCeilingValue;
            }

            var employeeEsi = (decimal)Math.Ceiling(eligibleEsiValue * employeeEsiDefaultPercentage / 100);

            decimal employerEsi = (decimal)Math.Ceiling(eligibleEsiValue * employerEsiDefaultPercentage / 100);

            IndiaESIOutputDto outputDto = new IndiaESIOutputDto
            {
                EligibleESISalary = eligibleEsiValue,
                EmployeeRefId = input.InputData.EmployeeRefId,
                EmployeeESI = employeeEsi,
                EmployerESI = employerEsi,
            };
            return outputDto;
        }

        public async Task<SGCPFOutputDto> GetSGCPF(SalaryPaidForTheMonth input)
        {
            //  CalculationAfterArrivedGrossSalary  -    Needs to verify
            //  After 58 Years No PF
            var employee = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == input.InputData.EmployeeRefId);
            if (employee == null)
            {
                throw new UserFriendlyException(L("NotExistForId", L("Employee"), input.InputData.EmployeeRefId));
            }

            var empResidentStatus = await _employeeResidentStatusRepo.FirstOrDefaultAsync(t => t.Id == employee.EmployeeResidentStatusRefId);
            if (empResidentStatus == null)
            {
                throw new UserFriendlyException(L("Resident Status Error For ") + employee.EmployeeName);
            }

            if (empResidentStatus.ResidentStatus.ToUpper() != "CITIZEN" && empResidentStatus.ResidentStatus.ToUpper() != "PR")
            {
                throw new UserFriendlyException("Employee Should Have The Resident Status of Citizen or PR for processing CPF - Singapore");
            }

            decimal minimumCPfCeilingValue = 0m;
            decimal maximumCPfCeilingValue = 6000;

            decimal employeeCPfDefaultPercentage = 0m;
            decimal employerCPfDefaultPercentage = 0m;

            int roundedOff = 0;

            #region AgeGroup
            string ageGroup = "";

            DateTime salaryCalculationDate = input.InputData.EndDate;
            TimeSpan diff = salaryCalculationDate.Subtract(employee.DateOfBirth);
            if (diff.Days < 55 * 365)
            {
                ageGroup = "Less_Than_55";
            }
            else if (diff.Days < 60 * 365)
            {
                ageGroup = "55-60";
            }
            else if (diff.Days < 65 * 365)
            {
                ageGroup = "60-65";
            }
            else
            {
                ageGroup = "More_Than_65";
            }
            #endregion

            #region PR-Age Status
            string prAgeGroup = "";
            if (empResidentStatus.Equals("PR"))
            {
                if (input.PersonalInformation.PRStartDate.HasValue == false)
                {
                    throw new UserFriendlyException("PR Start Date && PR End Date Required");
                }

                if (input.PersonalInformation.PRNumber.IsNullOrEmpty() || input.PersonalInformation.PRNumber.IsNullOrWhiteSpace())
                {
                    throw new UserFriendlyException("PR Number Required");
                }

                TimeSpan prdiff = salaryCalculationDate.Subtract(employee.PRStartDate.Value);
                if (prdiff.Days < 1 * 365)
                {
                    prAgeGroup = "PR_1_Years";
                }
                else if (prdiff.Days < 2 * 365)
                {
                    prAgeGroup = "PR_2_Years";
                }
                else if (prdiff.Days > 2 * 365)
                {
                    prAgeGroup = "PR_More_Than_2_Years";
                }
            }
            #endregion

            decimal ordinaryWages = input.GrossSalaryAmount;
            decimal additionalWages = 0;
            decimal eligibleCPfValue = ordinaryWages + additionalWages;

            //@@@@ Pending Needs To find the Exclusion Tags or Salary Tags
            if (input.ExcludedDto != null)
            {
                input.ExcludedDto = new List<ExcludedFormulaList>();

                var excludedFormulaList = input.ExcludedDto.FirstOrDefault(t => t.IncentiveTagRefId == 1);

                foreach (var lst in excludedFormulaList.ExcludedSalaryList)
                {
                    eligibleCPfValue = eligibleCPfValue - lst.AmountCalculatedForSalaryProcess;
                }

                foreach (var lst in excludedFormulaList.ExcludedIncentiveList)
                {
                    eligibleCPfValue = eligibleCPfValue - lst.Amount;
                }
            }
            //Exclude the Salary Tags those PF Payable is false
            if (input.AllowanceSalaryTagList != null)
            {
                var excludedAllowance = input.AllowanceSalaryTagList.Where(t => t.IsPfPayable == false).ToList();

                foreach (var lst in excludedAllowance)
                {
                    eligibleCPfValue = eligibleCPfValue - lst.AmountCalculatedForSalaryProcess;
                }

            }

            //Exclude the Incentive Tags those PF Payable is false
            if (input.IncentiveConsolidatedList != null)
            {
                var excludedIncentive = input.IncentiveConsolidatedList.Where(t => t.IsPfPayable == false).ToList();

                foreach (var lst in excludedIncentive)
                {
                    eligibleCPfValue = eligibleCPfValue - lst.TotalIncentiveAmount;
                }

            }

            //@@ Maximum Exclusion percentage to be defined and send the alert

            if (eligibleCPfValue > maximumCPfCeilingValue)
            {
                eligibleCPfValue = maximumCPfCeilingValue;
            }

            decimal employeeCPF = 0;
            decimal employerCPF = 0;

            #region Define Percentage Of CPF If Citizen / PR More Than 3 Years
            if (empResidentStatus.ResidentStatus.ToUpper().Equals("CITIZEN") || prAgeGroup.Equals("PR_More_Than_2_Years"))
            {
                if (empResidentStatus.Equals("CITIZEN"))
                {

                }

                maximumCPfCeilingValue = 6000;
                switch (ageGroup)
                {
                    case "Less_Than_55":

                        if (eligibleCPfValue > 0 && eligibleCPfValue <= 50)
                        {
                            employeeCPfDefaultPercentage = 0m;
                            employerCPfDefaultPercentage = 0m;

                            employeeCPF = 0;
                            employerCPF = 0;
                        }
                        else if (eligibleCPfValue > 50 && eligibleCPfValue <= 500)
                        {
                            employeeCPfDefaultPercentage = 0;
                            employerCPfDefaultPercentage = 17m;

                            employeeCPF = Math.Round(eligibleCPfValue * employeeCPfDefaultPercentage / 100, roundedOff);
                            employerCPF = Math.Round(eligibleCPfValue * employerCPfDefaultPercentage / 100, roundedOff);
                        }
                        else if (eligibleCPfValue > 500 && eligibleCPfValue <= 750)
                        {
                            employeeCPfDefaultPercentage = 0.6m;
                            employerCPfDefaultPercentage = 17m;

                            employeeCPF = Math.Round((eligibleCPfValue - 500) * 0.6m, roundedOff);
                            employerCPF = Math.Round(eligibleCPfValue * employerCPfDefaultPercentage / 100, roundedOff);
                        }
                        else if (eligibleCPfValue > 750)
                        {
                            employeeCPfDefaultPercentage = 20m;
                            employerCPfDefaultPercentage = 17m;

                            employeeCPF = Math.Round(eligibleCPfValue * employeeCPfDefaultPercentage / 100, roundedOff);
                            employerCPF = Math.Round(eligibleCPfValue * employerCPfDefaultPercentage / 100, roundedOff);
                        }

                        if (employeeCPF > 1200)
                            employeeCPF = 1200;

                        if (employerCPF > 1020)
                            employerCPF = 1020;
                        break;

                    case "55-60":
                        if (eligibleCPfValue > 0 && eligibleCPfValue <= 50)
                        {
                            employeeCPfDefaultPercentage = 0m;
                            employerCPfDefaultPercentage = 0m;

                            employeeCPF = 0;
                            employerCPF = 0;
                        }
                        else if (eligibleCPfValue > 50 && eligibleCPfValue <= 500)
                        {
                            employeeCPfDefaultPercentage = 0;
                            employerCPfDefaultPercentage = 13m;

                            employeeCPF = Math.Round(eligibleCPfValue * employeeCPfDefaultPercentage / 100, roundedOff);
                            employerCPF = Math.Round(eligibleCPfValue * employerCPfDefaultPercentage / 100, roundedOff);
                        }
                        else if (eligibleCPfValue > 500 && eligibleCPfValue <= 750)
                        {
                            employeeCPfDefaultPercentage = 0.39m;
                            employerCPfDefaultPercentage = 13m;

                            employeeCPF = Math.Round((eligibleCPfValue - 500) * 0.39m, roundedOff);
                            employerCPF = Math.Round(eligibleCPfValue * employerCPfDefaultPercentage / 100, roundedOff);
                        }
                        else if (eligibleCPfValue > 750)
                        {
                            employeeCPfDefaultPercentage = 13m;
                            employerCPfDefaultPercentage = 13m;

                            employeeCPF = Math.Round(eligibleCPfValue * employeeCPfDefaultPercentage / 100, roundedOff);
                            employerCPF = Math.Round(eligibleCPfValue * employerCPfDefaultPercentage / 100, roundedOff);
                        }

                        if (employeeCPF > 780)
                            employeeCPF = 780;

                        if (employerCPF > 780)
                            employerCPF = 780;
                        break;
                    case "60-65":
                        if (eligibleCPfValue > 0 && eligibleCPfValue <= 50)
                        {
                            employeeCPfDefaultPercentage = 0m;
                            employerCPfDefaultPercentage = 0m;

                            employeeCPF = 0;
                            employerCPF = 0;
                        }
                        else if (eligibleCPfValue > 50 && eligibleCPfValue <= 500)
                        {
                            employeeCPfDefaultPercentage = 0;
                            employerCPfDefaultPercentage = 9m;

                            employeeCPF = Math.Round(eligibleCPfValue * employeeCPfDefaultPercentage / 100, roundedOff);
                            employerCPF = Math.Round(eligibleCPfValue * employerCPfDefaultPercentage / 100, roundedOff);
                        }
                        else if (eligibleCPfValue > 500 && eligibleCPfValue <= 750)
                        {
                            employeeCPfDefaultPercentage = 0.225m;
                            employerCPfDefaultPercentage = 9m;

                            employeeCPF = Math.Round((eligibleCPfValue - 500) * 0.225m, roundedOff);
                            employerCPF = Math.Round(eligibleCPfValue * employerCPfDefaultPercentage / 100, roundedOff);
                        }
                        else if (eligibleCPfValue > 750)
                        {
                            employeeCPfDefaultPercentage = 7.5m;
                            employerCPfDefaultPercentage = 9m;

                            employeeCPF = Math.Round(eligibleCPfValue * employeeCPfDefaultPercentage / 100, roundedOff);
                            employerCPF = Math.Round(eligibleCPfValue * employerCPfDefaultPercentage / 100, roundedOff);
                        }

                        if (employeeCPF > 450)
                            employeeCPF = 450;

                        if (employerCPF > 540)
                            employerCPF = 540;
                        break;
                    case "More_Than_65":
                        if (eligibleCPfValue > 0 && eligibleCPfValue <= 50)
                        {
                            employeeCPfDefaultPercentage = 0m;
                            employerCPfDefaultPercentage = 0m;

                            employeeCPF = 0;
                            employerCPF = 0;
                        }
                        else if (eligibleCPfValue > 50 && eligibleCPfValue <= 500)
                        {
                            employeeCPfDefaultPercentage = 0;
                            employerCPfDefaultPercentage = 7.5m;

                            employeeCPF = Math.Round(eligibleCPfValue * employeeCPfDefaultPercentage / 100, roundedOff);
                            employerCPF = Math.Round(eligibleCPfValue * employerCPfDefaultPercentage / 100, roundedOff);
                        }
                        else if (eligibleCPfValue > 500 && eligibleCPfValue <= 750)
                        {
                            employeeCPfDefaultPercentage = 0.15m;
                            employerCPfDefaultPercentage = 7.5m;

                            employeeCPF = Math.Round((eligibleCPfValue - 500) * 0.15m, roundedOff);
                            employerCPF = Math.Round(eligibleCPfValue * employerCPfDefaultPercentage / 100, roundedOff);
                        }
                        else if (eligibleCPfValue > 750)
                        {
                            employeeCPfDefaultPercentage = 5m;
                            employerCPfDefaultPercentage = 7.5m;

                            employeeCPF = Math.Round(eligibleCPfValue * employeeCPfDefaultPercentage / 100, roundedOff);
                            employerCPF = Math.Round(eligibleCPfValue * employerCPfDefaultPercentage / 100, roundedOff);
                        }

                        if (employeeCPF > 300)
                            employeeCPF = 300;

                        if (employerCPF > 450)
                            employerCPF = 450;
                        break;
                }
            }
            #endregion
            else if (prAgeGroup.Equals("PR_1_Years"))
            #region PR 1 Years
            {
                switch (ageGroup)
                {
                    case "Less_Than_55":
                        employeeCPfDefaultPercentage = 12m;

                        employerCPfDefaultPercentage = 17m;
                        break;
                    case "55-60":
                        employeeCPfDefaultPercentage = 12m;

                        employerCPfDefaultPercentage = 17m;
                        break;
                    case "60-65":
                        employeeCPfDefaultPercentage = 12m;

                        employerCPfDefaultPercentage = 17m;
                        break;
                    case "More_Than_65":
                        employeeCPfDefaultPercentage = 12m;

                        employerCPfDefaultPercentage = 17m;
                        break;
                }
            }
            #endregion




            SGCPFOutputDto outputDto = new SGCPFOutputDto
            {
                EligiblePFSalary = eligibleCPfValue,
                EmployeeRefId = input.InputData.EmployeeRefId,
                EmployeePF = employeeCPF,
                EmployerPF = employerCPF,
            };
            return outputDto;
        }


    }
}