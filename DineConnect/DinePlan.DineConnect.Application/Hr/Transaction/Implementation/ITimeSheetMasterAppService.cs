﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Hr.Transaction.Dtos;

namespace DinePlan.DineConnect.Hr.Transaction
{
    public interface ITimeSheetMasterAppService : IApplicationService
    {
        //Task<FileDto> GetAllToExcel();
        Task<ListResultOutput<TimeSheetMasterListDto>> GetManualTsNumbers();
        Task<TimeSheetAlreadyExist> GetTimeSheetExist(InputTimeSheetNumberWithAcYear input);
        Task<DateTime?> SetYellowTimeSheetSubmission(TsUpdateSubmissionDto input);
        Task<DateTime?> SetPinkTimeSheetSubmission(TsUpdateSubmissionDto input);
        //Task<GetTsDashBoardDto> GetDashBoardTimeSheetData(GetTsMonthSales input);
        Task<PieChartOutputDto> GetDashBoardDutyChartData(GetDashBoardDto input);
        Task<PieChartOutputDto> GetEmployeeDashBoardDutyChartData(GetDashBoardDto input);
        //Task<GetEmployeeTsDashBoardDto> GetEmployeeDashBoardTimeSheetData(GetDashBoardDto input);
        

        //Task<List<MessageOutput>> SendEmployeeStatusEmail(GetEmployeeAlertMailDto input);

        //Task<List<MessageOutput>> ToAllEmployeeSendStatusEmail(GetEmployeeAlertMailDto input);

        //        Task<FileDto> GetTsSubmissionToExcel(TimeSheetSubmissionDtos input);
        //Task<FileDto> GetTsSubmissionToExcel(GetTSPendigInputDto input);

        Task<MessageOutput> HrOperationHealthEdp(GetHrOperationHealthDto input);
     
        Task<List<MessageOutput>> SendDayStatusEmailToAllEmployee(GetStatusAlertMailDto input);

        Task<List<MessageOutput>> SendDayStatusEmail(GetStatusAlertMailDto input);

        Task<List<WorkDayListDto>> GetWorkDaysDetails();
        Task<MessageOutput> HrOperationAlert(GetHrOperationHealthDto input);
        Task<MessageOutput> HrOfficeJobAlert(GetHrOperationHealthDto input);
        Task<List<GpsAttendanceListDto>> GetGpsEmployeeAttendance(GetDashBoardDto input);
        Task<List<GpsAttendanceParticularDateList>> GetGpsEmployeeAttendanceDateWise(GetDashBoardDto input);
        Task<ListResultOutput<ComboboxItemDto>> GetWorkPositionTypeForCombobox(NullableIdInput ninput);
        Task<MessageOutput> HrGpsOrBillableAlert(GetHrOperationHealthDto input);
        Task<SalaryPaidForTheMonth> GetSalaryForTheGivenMonth(SalaryPrepareDto input);
        Task<SalaryPayableConsolidatedDto> GetParticularDaySalary(PerDaySalaryCost dateInput);
        Task<FileDto> GetSalarySlipExcelOrPdf(SalaryPrepareDto input);
    }
}
