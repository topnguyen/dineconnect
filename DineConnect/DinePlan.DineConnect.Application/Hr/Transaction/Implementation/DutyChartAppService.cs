﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Utility.Exporter;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Hr.Master;
using System;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using System.Net.Mail;
using System.Text;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.House.Master.Dtos;
using Abp.Domain.Uow;
using System.Reflection;

namespace DinePlan.DineConnect.Hr.Transaction.Implementation
{
    public class DutyChartAppService : DineConnectAppServiceBase, IDutyChartAppService
    {

        private readonly IDutyChartListExcelExporter _dutychartExporter;
        private readonly IExcelExporter _exporter;
        private readonly IDutyChartManager _dutychartManager;
        private readonly IRepository<DutyChart> _dutychartRepo;
        private readonly IRepository<DutyChartDetail> _dutychartdetailRepo;
        private readonly IRepository<EmployeeDefaultWorkStation> _employeedefaultworkstationRepo;
        private readonly IRepository<PersonalInformation> _personalInformationRepo;
        private readonly IRepository<Company> _companyRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<DcGroupMaster> _dcgroupmasterRepo;
        private readonly IRepository<DcHeadMaster> _dcheadmasterRepo;
        private readonly IRepository<DcGroupMasterVsSkillSet> _dcgroupmastervsskillsetRepo;
        private readonly IRepository<DcStationMaster> _dcstationmasterRepo;
        private readonly IRepository<DcShiftMaster> _dcshiftmasterRepo;
        private readonly IRepository<LeaveRequest> _leaverequestRepo;
        private readonly IRepository<LeaveType> _leavetypeRepo;
        private readonly IRepository<EmployeeSkillSet> _employeeskillsetRepo;
        private readonly IRepository<SkillSet> _skillsetRepo;
        private readonly IRepository<PublicHoliday> _publicholidayRepo;
        private readonly IRepository<LeaveRequest> _leaveRequestRepo;
        private readonly IRepository<AttendanceLog> _attendanceLogRepo;
        private readonly IRepository<AttendanceMachineLocation> _attendanceMachineLocationRepo;
        private readonly IRepository<GpsAttendance> _gpsAttendanceRepo;
        private readonly IRepository<BioMetricExcludedEntry> _biometricExcludedRepo;
        private readonly IRepository<JobTitleMaster> _jobTitleMasterRepo;

        public DutyChartAppService(IDutyChartManager dutychartManager,
            IDutyChartListExcelExporter dutychartExporter,
            IRepository<DutyChart> dutyChartRepo,
            IRepository<DutyChartDetail> dutychartdetailRepo,
            IRepository<EmployeeDefaultWorkStation> employeedefaultworkstationRepo,
            IRepository<PersonalInformation> personalinformationRepo,
            IRepository<DcStationMaster> dcstationmasterRepo,
            IRepository<DcShiftMaster> dcshiftmasterRepo,
            IRepository<Company> companyRepo,
            IRepository<LeaveRequest> leaverequestRepo,
            IRepository<EmployeeSkillSet> employeeskillsetRepo,
            IRepository<SkillSet> skillsetRepo,
            IRepository<PublicHoliday> publicholidayRepo,
            IRepository<LeaveType> leavetypeRepo,
            IRepository<DcGroupMaster> dcgroupmasterRepo,
            IRepository<DcHeadMaster> dcheadmasterRepo,
            IRepository<DcGroupMasterVsSkillSet> dcgroupmastervsskillsetRepo,
            IRepository<LeaveRequest> leaveRequestRepo,
            IRepository<AttendanceLog> attendanceLogRepo,
            IRepository<AttendanceMachineLocation> attendanceMachineLocationRepo,
            IRepository<GpsAttendance> gpsAttendanceRepo,
            IRepository<BioMetricExcludedEntry> biometricExcludedRepo,
            IRepository<Location> locationRepo,
                    IRepository<JobTitleMaster> jobTitleMasterRepo,
        IExcelExporter exporter)
        {
            _jobTitleMasterRepo = jobTitleMasterRepo;
            _dutychartManager = dutychartManager;
            _dutychartRepo = dutyChartRepo;
            _exporter = exporter;
            _dutychartdetailRepo = dutychartdetailRepo;
            _employeedefaultworkstationRepo = employeedefaultworkstationRepo;
            _personalInformationRepo = personalinformationRepo;
            _companyRepo = companyRepo;
            _leaverequestRepo = leaverequestRepo;
            _employeeskillsetRepo = employeeskillsetRepo;
            _skillsetRepo = skillsetRepo;
            _publicholidayRepo = publicholidayRepo;
            _leavetypeRepo = leavetypeRepo;
            _dcheadmasterRepo = dcheadmasterRepo;
            _dcgroupmasterRepo = dcgroupmasterRepo;
            _dcstationmasterRepo = dcstationmasterRepo;
            _dcshiftmasterRepo = dcshiftmasterRepo;
            _dcgroupmastervsskillsetRepo = dcgroupmastervsskillsetRepo;
            _leaveRequestRepo = leaveRequestRepo;
            _attendanceLogRepo = attendanceLogRepo;
            _attendanceMachineLocationRepo = attendanceMachineLocationRepo;
            _biometricExcludedRepo = biometricExcludedRepo;
            _dutychartExporter = dutychartExporter;
            _gpsAttendanceRepo = gpsAttendanceRepo;
            _locationRepo = locationRepo;
        }

        public async Task<PagedResultOutput<DutyChartListDto>> GetAll(GetDutyChartInput input)
        {
            var allItems = _dutychartRepo.GetAll();
            if (input.Operation == "SEARCH")
            {
                allItems = _dutychartRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Status.Equals(input.Filter)
               );
            }
            else
            {
                allItems = _dutychartRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Status.Contains(input.Filter)
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<DutyChartListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<DutyChartListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {


            var allList = await _dutychartRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<DutyChartListDto>>();
            var baseE = new BaseExportObject()
            {
                ExportObject = allListDtos,
                ColumnNames = new string[] { "Id" }
            };
            return _exporter.ExportToFile(baseE, L("DutyChart"));

        }

        public async Task<GetDutyChartForEditOutput> GetDutyChartForEdit(GetDutyChartForGivenDate input)
        {
            DutyChartEditDto editDto;
            List<DutyChartTreeDetail> editDutyChartTreeDetailDtos = new List<DutyChartTreeDetail>();
            bool firstTimeFlag = false;

            var existData = await _dutychartRepo.FirstOrDefaultAsync(t => t.LocationRefId == input.LocationRefId
                && DbFunctions.TruncateTime(t.DutyChartDate) == DbFunctions.TruncateTime(input.DutyChartDate));

            var isPublicHoliday = await _publicholidayRepo.FirstOrDefaultAsync(t => DbFunctions.TruncateTime(t.HolidayDate) == DbFunctions.TruncateTime(input.DutyChartDate));

            if (existData == null)
            {
                await FillDutyDetailsForNewDate(new FillDutyChart { LocationRefId = input.LocationRefId, DutyChartDate = input.DutyChartDate, DutyChartRefId = null, FirstTimeFlag = true, PublicHolidayFlag = isPublicHoliday == null ? false : true });
                firstTimeFlag = true;
            }
            else
            {
                var existDetailData = await _dutychartdetailRepo.GetAllListAsync(t => t.DutyChartRefId == existData.Id);
                if (existDetailData == null || existDetailData.Count == 0)
                {
                    await FillDutyDetailsForNewDate(new FillDutyChart { LocationRefId = input.LocationRefId, DutyChartDate = input.DutyChartDate, DutyChartRefId = existData.Id, FirstTimeFlag = true, PublicHolidayFlag = isPublicHoliday == null ? false : true });
                    firstTimeFlag = true;
                }
            }

            var hDto = await _dutychartRepo.FirstOrDefaultAsync(t => t.LocationRefId == input.LocationRefId && t.DutyChartDate == input.DutyChartDate);
            if (hDto == null)
            {
                throw new UserFriendlyException(L("DutyChart") + " " + L("Empty"));
            }

            editDto = hDto.MapTo<DutyChartEditDto>();

            if (isPublicHoliday == null)
            {
                editDto.PublicHolidayFlag = false;
            }
            else
            {
                editDto.PublicHolidayFlag = true;
            }

            List<DutyChartDetailListDto> editListDtos = new List<DutyChartDetailListDto>();

            var rsDutyDetails = _dutychartdetailRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId && t.DutyChartRefId == hDto.Id);
            var dcStationMaster = _dcstationmasterRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId);
            editListDtos = (from dc in rsDutyDetails
                            join stn in dcStationMaster on
                            dc.StationRefId equals stn.Id into LeftJoinedDc
                            from joinedpc in LeftJoinedDc.DefaultIfEmpty()
                            select new DutyChartDetailListDto
                            {
                                Id = dc.Id,
                                DutyChartRefId = dc.DutyChartRefId,
                                AllottedStatus = dc.AllottedStatus,
                                EmployeeRefId = dc.EmployeeRefId,
                                StationRefId = dc.StationRefId,
                                ShiftRefId = dc.ShiftRefId,
                                CompanyRefId = dc.CompanyRefId,
                                LocationRefId = dc.LocationRefId,
                                TimeDescription = "Fill",
                                TimeIn = dc.TimeIn,
                                TimeOut = dc.TimeOut,
                                OtTimeIn = dc.OtTimeIn,
                                OtTimeOut = dc.OtTimeOut,
                                NightFlag = false,
                                Remarks = "",
                                DutyChartDate = input.DutyChartDate,
                                UserSerialNumber = dc.UserSerialNumber,
                                SkillRefId = dc.SkillRefId,
                                HalfDayFlag = dc.HalfDayFlag,
                                BreakMinutes = dc.BreakMinutes,
                            }
                            ).OrderBy(t => t.EmployeeRefId).ToList();


            var rsPersonalInformation = await _personalInformationRepo.GetAllListAsync();
            var rsSkillSet = await _skillsetRepo.GetAllListAsync();

            if (firstTimeFlag == false)
            {
                //var currentWorkingCount = rsPersonalInformation.Where(t => t.DateHired <= input.DutyChartDate && (t.LastWorkingDate == null || t.LastWorkingDate >= input.DutyChartDate)).Count();
                List<int> personalInformationEmpIds = rsPersonalInformation.Where(t => t.LocationRefId == input.LocationRefId && t.DateHired <= input.DutyChartDate
                //&& t.CreationTime.Date <= input.DutyChartDate 
                && (t.LastWorkingDate == null || t.LastWorkingDate >= input.DutyChartDate)).Select(t => t.Id).ToList();
                List<int> dutyEmpList = editListDtos.Select(t => t.EmployeeRefId).ToList();
                var inListButNotInList2 = personalInformationEmpIds.Except(dutyEmpList).ToList();
                if (inListButNotInList2.Count > 0)
                {
                    var empToInsert = rsPersonalInformation.Where(t => t.LocationRefId == input.LocationRefId && t.DateHired <= input.DutyChartDate && (t.LastWorkingDate == null || t.LastWorkingDate >= input.DutyChartDate) && !dutyEmpList.Contains(t.Id)).ToList();

                    await FillDutyDetailsForNewDate(new FillDutyChart { LocationRefId = input.LocationRefId, DutyChartDate = input.DutyChartDate, DutyChartRefId = existData.Id, FirstTimeFlag = firstTimeFlag, PublicHolidayFlag = isPublicHoliday == null ? false : true });

                    rsDutyDetails = _dutychartdetailRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId && t.DutyChartRefId == hDto.Id);
                    dcStationMaster = _dcstationmasterRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId);

                    editListDtos = (from dc in rsDutyDetails
                                    join stn in dcStationMaster
                                    on dc.StationRefId equals stn.Id into LeftJoinedPc
                                    from joinedpc in LeftJoinedPc.DefaultIfEmpty()
                                    select new DutyChartDetailListDto
                                    {
                                        Id = dc.Id,
                                        DutyChartRefId = dc.DutyChartRefId,
                                        AllottedStatus = dc.AllottedStatus,
                                        EmployeeRefId = dc.EmployeeRefId,
                                        CompanyRefId = dc.CompanyRefId,
                                        LocationRefId = dc.LocationRefId,
                                        DcHeadRefName = "Fill",
                                        DcGroupRefName = "Fill",
                                        TimeDescription = "Fill",
                                        TimeIn = dc.TimeIn,
                                        TimeOut = dc.TimeOut,
                                        OtTimeIn = dc.OtTimeIn,
                                        OtTimeOut = dc.OtTimeOut,
                                        NightFlag = false,
                                        Remarks = "",
                                        DutyChartDate = input.DutyChartDate,

                                        UserSerialNumber = dc.UserSerialNumber,
                                        SkillRefId = dc.SkillRefId,
                                        HalfDayFlag = dc.HalfDayFlag,
                                        BreakMinutes = dc.BreakMinutes,
                                    }
                        ).ToList();
                }
            }

            var rsSkills = await _skillsetRepo.GetAll().ToListAsync();
            var rsEmpSkillSet = await _employeeskillsetRepo.GetAllListAsync();
            var rsCompany = await _companyRepo.GetAllListAsync();
            var rsLocation = await _locationRepo.GetAllListAsync();
            var rejectedString = L("Rejected");
            var rsLeaveRequest = await _leaverequestRepo.GetAllListAsync(t => t.LeaveStatus != rejectedString);
            var rsDcHead = await _dcheadmasterRepo.GetAllListAsync();
            var rsDcGroup = await _dcgroupmasterRepo.GetAllListAsync();
            var rsDcStation = await _dcstationmasterRepo.GetAllListAsync();
            var rsDcShift = await _dcshiftmasterRepo.GetAllListAsync();

            var defaultTime = await _dcstationmasterRepo.FirstOrDefaultAsync(t => t.LocationRefId == input.LocationRefId && t.DefaultTimeFlag == true);
            if (defaultTime == null)
            {
                defaultTime = await _dcstationmasterRepo.FirstOrDefaultAsync(t => t.LocationRefId == input.LocationRefId);
                //if (defaultTime==null)
                //    throw new UserFriendlyException(L("DefaultTimeErr"));
            }

            foreach (var lst in editListDtos)
            {
                var skill = rsSkills.FirstOrDefault(t => t.Id == lst.SkillRefId);
                if (skill == null)
                {
                    throw new UserFriendlyException(lst.EmployeeRefCode + "-" + lst.EmployeeRefName + " Skill Id " + lst.SkillRefId + " Not Exist");
                }
                lst.SkillRefCode = skill.SkillCode;

                if (defaultTime != null && lst.TimeDescription.IsNullOrEmpty())
                {
                    lst.TimeDescription = defaultTime.TimeDescription;
                    lst.NightFlag = defaultTime.NightFlag;
                }

                var shift = rsDcShift.FirstOrDefault(t => t.Id == lst.ShiftRefId);
                if (shift != null)
                {
                    lst.ShiftRefName = shift.DutyDesc;
                    lst.TimeDescription = shift.TimeDescription;
                    lst.NightFlag = shift.NightFlag;
                }

                var stn = rsDcStation.FirstOrDefault(t => t.Id == lst.StationRefId);
                if (stn != null)
                {
                    var grp = rsDcGroup.FirstOrDefault(t => t.Id == stn.DcGroupRefId);
                    if (grp != null)
                    {
                        var head = rsDcHead.FirstOrDefault(t => t.Id == grp.DcHeadRefId);
                        if (head != null)
                        {
                            lst.DcHeadRefId = head.Id;
                            lst.DcHeadRefName = head.HeadName;
                        }
                        lst.DcGroupRefId = grp.Id;
                        lst.DcGroupRefName = grp.GroupName;
                    }
                    lst.StationRefName = stn.StationName;

                }


                lst.DutyChartDate = input.DutyChartDate.Date;

                var per = rsPersonalInformation.FirstOrDefault(p => p.Id == lst.EmployeeRefId);
                lst.EmployeeRefCode = per.EmployeeCode;

                if (per.ActiveStatus == false)
                {
                    if (per.LastWorkingDate < input.DutyChartDate.Date)
                    {
                        lst.ShouldBeDeleted = true;
                        await _dutychartdetailRepo.DeleteAsync(lst.Id);
                        continue;
                    }
                }

                lst.EmployeeRefName = per.EmployeeName;

                if (lst.LocationRefId == 0 || lst.CompanyRefId == 0)
                {
                    var location = rsLocation.FirstOrDefault(t => t.Id == per.LocationRefId);

                    lst.LocationRefId = location.Id;
                    lst.LocationRefName = location.Code;

                    var company = rsCompany.FirstOrDefault(t => t.Id == location.CompanyRefId);
                    lst.CompanyRefId = company.Id;
                    lst.CompanyRefCode = company.Code;
                    lst.CompanyRefName = company.Name;
                }
                else
                {
                    var location = rsLocation.FirstOrDefault(t => t.Id == lst.LocationRefId);

                    lst.LocationRefId = location.Id;
                    lst.LocationRefName = location.Code;

                    var company = rsCompany.FirstOrDefault(t => t.Id == location.CompanyRefId);
                    lst.CompanyRefId = company.Id;
                    lst.CompanyRefCode = company.Code;
                    lst.CompanyRefName = company.Name;
                }


                lst.ProfilePictureId = per.ProfilePictureId;
                lst.SupervisorEmployeeRefId = per.SupervisorEmployeeRefId;
                lst.InchargeEmployeeRefId = per.InchargeEmployeeRefId;
                if (lst.SupervisorEmployeeRefId.HasValue)
                {
                    var svr = rsPersonalInformation.FirstOrDefault(t => t.Id == lst.SupervisorEmployeeRefId.Value);
                    if (svr != null)
                    {
                        lst.SupervisorEmployeeRefName = svr.EmployeeName;
                    }
                }

                if (lst.InchargeEmployeeRefId.HasValue)
                {
                    var mgr = rsPersonalInformation.FirstOrDefault(t => t.Id == lst.InchargeEmployeeRefId.Value);
                    if (mgr != null)
                    {
                        lst.InchargeEmployeeRefName = mgr.EmployeeName;
                    }
                }

                var empskills = rsEmpSkillSet.Where(t => t.EmployeeRefId == lst.EmployeeRefId);

                List<SkillSetEditDto> empSkillSet = new List<SkillSetEditDto>();
                foreach (var sk in empskills)
                {
                    SkillSetEditDto ns = new SkillSetEditDto();
                    var locskill = rsSkills.FirstOrDefault(t => t.Id == sk.SkillSetRefId);
                    if (locskill == null)
                    {
                        throw new UserFriendlyException(L("SkillSetMismatch"), lst.EmployeeRefName);
                    }
                    ns.SkillCode = locskill.SkillCode;
                    ns.Id = locskill.Id;
                    empSkillSet.Add(ns);
                }

                lst.Skills = empSkillSet;

                bool jobFlag = true;
                //  Set The Alloted Status
                var leaveApplied = rsLeaveRequest.FirstOrDefault(t => t.EmployeeRefId == per.Id && input.DutyChartDate.Date
                    >= t.LeaveFrom && input.DutyChartDate.Date <= t.LeaveTo);

                if (leaveApplied != null)
                {
                    if (leaveApplied.HalfDayFlag == false)
                    {
                        if (lst.AllottedStatus != L("Leave"))
                        {

                            if (lst.Id > 0)
                            {
                                var dtoLeaveEmpUpdate = await _dutychartdetailRepo.GetAsync(lst.Id);
                                dtoLeaveEmpUpdate.AllottedStatus = L("Leave");
                                await _dutychartdetailRepo.InsertOrUpdateAndGetIdAsync(dtoLeaveEmpUpdate);
                            }
                            //var editDutyDetail = await _dutychartdetailRepo.FirstOrDefault(t=>t.EmployeeRefId==lst.EmployeeRefId && t.DutyChartRefId == lst.DutyChartRefId )
                        }
                        lst.AllottedStatus = L("Leave");
                        jobFlag = false;
                        if (per.DefaultWeekOff == (int)input.DutyChartDate.DayOfWeek)
                        {
                            lst.AllottedStatus = L("WeekOff");
                            jobFlag = false;
                        }
                    }
                }
                else if (per.DefaultWeekOff == (int)input.DutyChartDate.DayOfWeek && firstTimeFlag == true)
                {
                    lst.AllottedStatus = L("WeekOff");
                    jobFlag = false;
                }
                else if (per.DefaultWeekOff == (int)input.DutyChartDate.DayOfWeek && lst.AllottedStatus != L("Job"))
                {
                    lst.AllottedStatus = L("WeekOff");
                    jobFlag = false;
                }
                else if (per.DefaultWeekOff == (int)input.DutyChartDate.DayOfWeek && lst.AllottedStatus == L("Job")
                    && lst.StationRefId == null && firstTimeFlag == true)
                {
                    lst.AllottedStatus = L("WeekOff");
                    jobFlag = false;
                }
                else if (per.ActiveStatus == false && per.LastWorkingDate == null)
                {
                    lst.AllottedStatus = L("Absent");
                    jobFlag = false;
                }
                else if (lst.AllottedStatus == L("Idle"))
                {
                    jobFlag = false;
                }

                if (jobFlag == false)
                {
                    continue;
                }
                else
                {

                }

                //else
                //    throw new UserFriendlyException(L("TimeNotDefinedForEmployee", lst.EmployeeRefName + lst.EmployeeRefCode));
            }

            editListDtos = editListDtos.Where(t => t.ShouldBeDeleted == false).ToList();

            foreach (var group in editListDtos.GroupBy(t => t.EmployeeRefCode))
            {
                DutyChartTreeDetail ab = new DutyChartTreeDetail();

                List<DutyChartDetailListDto> d = new List<DutyChartDetailListDto>();

                foreach (var dutyList in group.OrderBy(t => t.EmployeeRefCode))
                {
                    ab.EmployeeRefCode = group.Key;
                    ab.EmployeeRefId = dutyList.EmployeeRefId;
                    ab.EmployeeRefName = dutyList.EmployeeRefName;
                    ab.SupervisorEmployeeRefId = dutyList.SupervisorEmployeeRefId;
                    ab.SupervisorEmployeeRefName = dutyList.SupervisorEmployeeRefName;
                    ab.InchargeEmployeeRefId = dutyList.InchargeEmployeeRefId;
                    ab.InchargeEmployeeRefName = dutyList.InchargeEmployeeRefName;
                    ab.DutyChartDate = dutyList.DutyChartDate;
                    ab.DutyChartRefId = dutyList.DutyChartRefId;
                    ab.CompanyRefCode = dutyList.CompanyRefCode;
                    ab.CompanyRefId = dutyList.CompanyRefId;
                    ab.UserSerialNumber = dutyList.UserSerialNumber;
                    ab.AllottedStatus = dutyList.AllottedStatus;
                    ab.ProfilePictureId = dutyList.ProfilePictureId;
                    ab.SkillRefId = dutyList.SkillRefId;
                    ab.SkillRefCode = dutyList.SkillRefCode;
                    ab.TimeIn = dutyList.TimeIn;
                    ab.TimeOut = dutyList.TimeOut;
                    ab.OtTimeIn = dutyList.OtTimeIn;
                    ab.OtTimeOut = dutyList.OtTimeOut;
                    ab.TimeDescription = dutyList.TimeDescription;
                    ab.Skills = dutyList.Skills;
                    ab.HalfDayFlag = dutyList.HalfDayFlag;
                    ab.BreakMinutes = dutyList.BreakMinutes;
                    ab.LocationRefId = dutyList.LocationRefId;
                    ab.LocationRefName = dutyList.LocationRefName;
                    ab.CompanyRefId = dutyList.CompanyRefId;
                    ab.CompanyRefCode = dutyList.CompanyRefCode;
                    ab.ShiftRefId = dutyList.ShiftRefId;
                    ab.ShiftRefName = dutyList.ShiftRefName;
                    ab.StationRefId = dutyList.StationRefId;
                    ab.StationRefName = dutyList.StationRefName;

                    if (ab.LocationRemarks == null || ab.LocationRemarks.IsNullOrEmpty())
                    {
                        ab.LocationRemarks = dutyList.LocationRefName;
                        ab.MultipleSkillRemarks = dutyList.SkillRefCode;
                        ab.MultipleTimeDescriptionRemarks = dutyList.TimeDescription;
                        ab.NumberOfLocations = 1;
                    }
                    else
                    {
                        if (ab.NumberOfLocations == 1)
                        {
                            ab.LocationRemarks = "1. " + ab.LocationRemarks;
                            ab.MultipleSkillRemarks = "1. " + ab.MultipleSkillRemarks;
                            ab.MultipleTimeDescriptionRemarks = "1. " + ab.MultipleTimeDescriptionRemarks;
                        }
                        ab.NumberOfLocations = ab.NumberOfLocations + 1;
                        ab.LocationRemarks = ab.LocationRemarks + ", " + ab.NumberOfLocations + ". " + dutyList.LocationRefName;
                        ab.MultipleSkillRemarks = ab.MultipleSkillRemarks + " , " + ab.NumberOfLocations + ". " + dutyList.SkillRefCode;
                        ab.MultipleTimeDescriptionRemarks = ab.MultipleTimeDescriptionRemarks + " , " + ab.NumberOfLocations + ". " + dutyList.TimeDescription;

                    }

                    if (ab.LocationRemarks == null || ab.LocationRemarks.IsNullOrEmpty())
                    {
                        ab.LocationRemarks = "";
                        ab.NumberOfLocations = 0;
                    }

                    DutyChartDetailListDto elements = new DutyChartDetailListDto
                    {
                        Id = dutyList.Id,
                        //Sno = dutyList.Sno,
                        DutyChartRefId = dutyList.DutyChartRefId,
                        DutyChartDate = dutyList.DutyChartDate,
                        CompanyRefId = dutyList.CompanyRefId,
                        CompanyRefCode = dutyList.CompanyRefCode,
                        LocationRefId = dutyList.LocationRefId,
                        LocationRefName = dutyList.LocationRefName,
                        UserSerialNumber = dutyList.UserSerialNumber,
                        EmployeeRefId = dutyList.EmployeeRefId,
                        EmployeeRefCode = group.Key,
                        EmployeeRefName = dutyList.EmployeeRefName,
                        AllottedStatus = dutyList.AllottedStatus,
                        TimeDescription = dutyList.TimeDescription,
                        TimeIn = dutyList.TimeIn,
                        TimeOut = dutyList.TimeOut,
                        NightFlag = dutyList.NightFlag,
                        Remarks = dutyList.Remarks,
                        SkillRefId = dutyList.SkillRefId,
                        SkillRefCode = dutyList.SkillRefCode,
                        ProfilePictureId = dutyList.ProfilePictureId,
                        HalfDayFlag = dutyList.HalfDayFlag,
                        BreakMinutes = dutyList.BreakMinutes,
                        DcHeadRefId = dutyList.DcHeadRefId,
                        DcHeadRefName = dutyList.DcHeadRefName,
                        DcGroupRefId = dutyList.DcGroupRefId,
                        DcGroupRefName = dutyList.DcGroupRefName,
                        StationRefId = dutyList.ShiftRefId,
                        StationRefName = dutyList.StationRefName,
                        ShiftRefId = dutyList.ShiftRefId,
                        ShiftRefName = dutyList.ShiftRefName
                    };

                    d.Add(elements);
                    //if (ab.EmployeeRefId == 129 || ab.EmployeeRefId == 1)
                    //{
                    //    elements.UserSerialNumber = elements.UserSerialNumber + 1;
                    //    elements.Id = 0;
                    //    d.Add(elements);
                    //}
                }

                ab.DutyDetails = d;

                editDutyChartTreeDetailDtos.Add(ab);

            }

            return new GetDutyChartForEditOutput
            {
                DutyChart = editDto,
                DutyChartDetail = editDutyChartTreeDetailDtos
            };
        }

        public async Task<ReturnSuccessOrFailureWithMessage> ExistAllServerLevelBusinessRules(CreateOrUpdateDutyChartInput input)
        {
            if (input.DutyChart.LocationRefId == 0)
            {
                throw new UserFriendlyException(L("Location") + " " + L("Error"));
            }
            var rsStationMaster = await _dcstationmasterRepo.GetAllListAsync();
            var rsShiftMaster = await _dcshiftmasterRepo.GetAllListAsync();

            foreach (var lst in input.DutyChartDetail)
            {
                if (lst.LocationRefId == 0)
                    throw new UserFriendlyException(L("Location") + " " + L("Error"));

                if (input.InternalCallFlag == false)
                {
                    if (lst.AllottedStatus == L("Job") && (lst.ShiftRefId == null || lst.ShiftRefId == 0 || lst.StationRefId == null || lst.ShiftRefId == 0))
                    {
                        var emp = await _personalInformationRepo.FirstOrDefaultAsync(t => t.Id == lst.EmployeeRefId);
                        throw new UserFriendlyException(emp.EmployeeName + " " + L("JobButNotAssigedShift"));
                    }

                    if (lst.AllottedStatus == L("Job"))
                    {
                        var stn = rsStationMaster.Where(t => t.Id == lst.StationRefId);
                        if (stn == null)
                        {
                            var emp = await _personalInformationRepo.FirstOrDefaultAsync(t => t.Id == lst.EmployeeRefId);
                            throw new UserFriendlyException(emp.EmployeeName + " " + L("JobButNotAssigedShift"));
                        }
                        var shift = rsStationMaster.Where(t => t.Id == lst.ShiftRefId);
                        if (shift == null)
                        {
                            var emp = await _personalInformationRepo.FirstOrDefaultAsync(t => t.Id == lst.EmployeeRefId);
                            throw new UserFriendlyException(emp.EmployeeName + " " + L("JobButNotAssigedShift"));
                        }
                    }
                    else
                    {
                        if (lst.StationRefId.HasValue)
                            lst.StationRefId = null;
                        if (lst.ShiftRefId.HasValue)
                            lst.ShiftRefId = null;
                    }

                }
            }

            List<PersonalInformation> rsPerInfos = new List<PersonalInformation>();
            if (input.DutyChartDetail.Count == 0)
            {
                throw new UserFriendlyException(L("Detail") + " " + L("Empty"));
            }



            return new ReturnSuccessOrFailureWithMessage { UserErrorNumber = 0, Message = "Success" };

        }

        public async Task<bool> RemoveDutyChart(DutyChartDetailListDto input)
        {
            var delDutyDto = await _dutychartdetailRepo.FirstOrDefaultAsync(t => t.Id == input.Id);
            if (delDutyDto == null)
            {
                return true;
            }
            if (delDutyDto.EmployeeRefId != input.EmployeeRefId)
            {
                throw new UserFriendlyException(L("Employee") + " ?");
            }
            if (delDutyDto.DutyChartDate.Date != input.DutyChartDate.Date)
            {
                throw new UserFriendlyException(L("Date") + " ?");
            }
            await _dutychartdetailRepo.DeleteAsync(delDutyDto);
            return true;
        }

        public async Task<IdInput> CreateOrUpdateDutyChart(CreateOrUpdateDutyChartInput input)
        {
            if (input.DutyChartDetail.Count == 0)
            {
                return null;
            }
            var returnRules = await ExistAllServerLevelBusinessRules(input);
            if (returnRules.UserErrorNumber == 0)
            {
                if (input.DutyChart.Id.HasValue)
                {
                    return await UpdateDutyChart(input);
                }
                else
                {
                    return await CreateDutyChart(input);
                }
            }
            else
            {
                return new IdInput { Id = -1 };
            }
        }


        public async Task DeleteDutyChart(IdInput input)
        {
            await _dutychartRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task<IdInput> UpdateDutyChart(CreateOrUpdateDutyChartInput input)
        {
            var master = await _dutychartRepo.GetAsync(input.DutyChart.Id.Value);

            if (input.SaveStatus != null && input.SaveStatus.Equals("Draft"))
            {
                if (master.Status == null || master.Status.Length == 0)
                    master.Status = input.SaveStatus;
            }
            else if (input.DutyChartDetail.Count > 1)
            {
                if (master.Status != null && master.Status.Equals("DayClosed"))
                {

                }
                else
                {
                    master.Status = input.SaveStatus;
                }
            }

            var defaultTime = await _dcstationmasterRepo.FirstOrDefaultAsync(t => t.LocationRefId == input.LocationRefId && t.DefaultTimeFlag == true);
            if (defaultTime == null)
            {
                //string locationName = "";
                //var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
                //if (loc != null)
                //    locationName = loc.Code;
                //throw new UserFriendlyException(L("DefaultTimeErr") + " " + L("Location") + " " + locationName);
            }

            if (input.SaveStatus == null && master.Status == null)
            {
                if (master.Status != null && master.Status.Equals("DayClosed"))
                {

                }
                else
                {
                    master.Status = L("Finished");
                }
            }

            var dtomaster = input.DutyChart;

            var dtoDetail = input.DutyChartDetail;

            foreach (var dto in dtoDetail)
            {
                bool changesCostCentreFlag = false;
                DutyChartDetail item = new DutyChartDetail();

                #region VerifyLeaveRequestIfStatusIsInLeave
                if (dto.AllottedStatus == L("Leave") || dto.AllottedStatus == L("ML") || dto.AllottedStatus == L("Native"))
                {
                    var rejectedString = L("Rejected");
                    var existLeaveRequest = await _leaverequestRepo.FirstOrDefaultAsync(t => t.LeaveStatus != rejectedString && t.EmployeeRefId == dto.EmployeeRefId && dto.DutyChartDate >= t.LeaveFrom && dto.DutyChartDate <= t.LeaveTo);
                    if (existLeaveRequest == null)
                    {
                        var pi = await _personalInformationRepo.FirstOrDefaultAsync(t => t.Id == dto.EmployeeRefId);
                        throw new UserFriendlyException(L("LeaveRequestNotEntered", pi.EmployeeCode + " " + pi.EmployeeName, dto.DutyChartDate.ToString("dd-MMM-yyyy"), dto.AllottedStatus));
                    }
                }
                #endregion

                var exist = await _dutychartdetailRepo.FirstOrDefaultAsync(t => t.DutyChartDate == input.DutyChart.DutyChartDate
                            && t.EmployeeRefId == dto.EmployeeRefId && t.UserSerialNumber == dto.UserSerialNumber);

                if (exist == null)
                {
                    using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.SoftDelete))
                    {
                        exist = await _dutychartdetailRepo.FirstOrDefaultAsync(t => t.DutyChartDate == input.DutyChart.DutyChartDate
                            && t.EmployeeRefId == dto.EmployeeRefId && t.UserSerialNumber == dto.UserSerialNumber);
                        if (exist != null)
                        {
                            dto.Id = exist.Id;
                            exist.DeleterUserId = null;
                            exist.DeletionTime = null;
                            exist.IsDeleted = false;
                            await _dutychartdetailRepo.UpdateAsync(exist);
                            continue;
                        }
                    }
                }

                if (dto.SkillRefCode == null || dto.SkillRefCode == "")
                {
                    var skill = await _skillsetRepo.FirstOrDefaultAsync(t => t.Id == dto.SkillRefId);
                    dto.SkillRefCode = skill.SkillCode;
                }

                try
                {


                    if (dto.Id > 0)
                    {
                        item = await _dutychartdetailRepo.GetAsync(dto.Id);

                        item.DutyChartRefId = master.Id;
                        item.CompanyRefId = dto.CompanyRefId;
                        item.DutyChartDate = master.DutyChartDate;
                        item.EmployeeRefId = dto.EmployeeRefId;
                        item.AllottedStatus = dto.AllottedStatus;
                        item.SkillRefId = dto.SkillRefId;
                        item.ShiftRefId = dto.ShiftRefId;
                        item.StationRefId = dto.StationRefId;
                        item.TimeIn = dto.TimeIn;
                        item.TimeOut = dto.TimeOut;
                        item.OtTimeIn = dto.OtTimeIn;
                        item.OtTimeOut = dto.OtTimeOut;
                        item.NightFlag = dto.NightFlag;
                        item.Remarks = dto.Remarks;
                        item.UserSerialNumber = dto.UserSerialNumber;

                        if (dto.TimeDescription.IsNullOrEmpty())
                        {
                            if (defaultTime != null)
                            {
                                dto.TimeIn = defaultTime.TimeIn;
                                dto.TimeOut = defaultTime.TimeOut;
                            }
                        }
                        item.TimeIn = dto.TimeIn;
                        item.TimeOut = dto.TimeOut;
                        item.OtTimeIn = dto.OtTimeIn;
                        item.OtTimeOut = dto.OtTimeOut;
                        item.HalfDayFlag = dto.HalfDayFlag;
                        item.BreakMinutes = dto.BreakMinutes;
                        await _dutychartdetailRepo.InsertOrUpdateAndGetIdAsync(item);
                    }
                    else
                    {
                        changesCostCentreFlag = true;
                        item.DutyChartRefId = master.Id;
                        item.CompanyRefId = dto.CompanyRefId;
                        item.LocationRefId = dto.LocationRefId;
                        item.DutyChartDate = master.DutyChartDate;
                        item.EmployeeRefId = dto.EmployeeRefId;
                        item.AllottedStatus = dto.AllottedStatus;
                        item.SkillRefId = dto.SkillRefId;
                        item.ShiftRefId = dto.ShiftRefId;
                        item.StationRefId = dto.StationRefId;
                        item.TimeIn = dto.TimeIn;
                        item.TimeOut = dto.TimeOut;
                        item.OtTimeIn = dto.OtTimeIn;
                        item.OtTimeOut = dto.OtTimeOut;
                        item.NightFlag = dto.NightFlag;
                        item.Remarks = dto.Remarks;
                        item.UserSerialNumber = dto.UserSerialNumber;
                        if (dto.TimeDescription.IsNullOrEmpty())
                        {
                            if (defaultTime != null)
                            {
                                dto.TimeIn = defaultTime.TimeIn;
                                dto.TimeOut = defaultTime.TimeOut;
                            }
                        }
                        item.TimeIn = dto.TimeIn;
                        item.TimeOut = dto.TimeOut;
                        item.OtTimeIn = dto.OtTimeIn;
                        item.OtTimeOut = dto.OtTimeOut;

                        item.HalfDayFlag = dto.HalfDayFlag;
                        item.BreakMinutes = dto.BreakMinutes;
                        await _dutychartdetailRepo.InsertOrUpdateAndGetIdAsync(item);
                    }

                }
                catch (Exception ex)
                {
                    MethodBase m = MethodBase.GetCurrentMethod();
                    string innerExceptionMessage = ex.InnerException != null ? ex.InnerException.Message : "";
                    var emp = await _personalInformationRepo.FirstOrDefaultAsync(t => t.Id == dto.EmployeeRefId);
                    string prjErrorMessage = emp.Id + " " + emp.EmployeeName;
                    throw new UserFriendlyException(prjErrorMessage + " " + "Method : " + m.ReflectedType.Name + " : " + m.Name + " " + ex.Message + " " + innerExceptionMessage);
                }
                #region SetAsDefaultPartCoding
                if (dto.IsSetAsDefault)
                {
                    var dtoDefaultWs = await _employeedefaultworkstationRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == dto.EmployeeRefId);
                    if (dtoDefaultWs == null)
                        continue;

                    var editDefaultWs = await _employeedefaultworkstationRepo.GetAsync(dtoDefaultWs.Id);

                    editDefaultWs.SkillRefId = dto.SkillRefId;
                    editDefaultWs.ShiftRefId = dto.ShiftRefId;

                    if (dto.TimeDescription.IsNullOrEmpty())
                    {
                        if (defaultTime != null)
                        {
                            dto.TimeIn = defaultTime.TimeIn;
                            dto.TimeOut = defaultTime.TimeOut;
                        }
                    }

                    await _employeedefaultworkstationRepo.InsertOrUpdateAndGetIdAsync(editDefaultWs);
                }
                #endregion
            }

            CheckErrors(await _dutychartManager.CreateSync(master));

            return new IdInput { Id = master.Id };
        }

        protected virtual async Task<IdInput> CreateDutyChart(CreateOrUpdateDutyChartInput input)
        {
            var master = input.DutyChart.MapTo<DutyChart>();
            var masterid = await _dutychartRepo.InsertAndGetIdAsync(master);
            var dtoDetail = input.DutyChartDetail;

            foreach (var dto in dtoDetail)
            {
                DutyChartDetail item = new DutyChartDetail
                {
                    DutyChartRefId = master.Id,
                    CompanyRefId = dto.CompanyRefId,
                    DutyChartDate = dto.DutyChartDate,
                    EmployeeRefId = dto.EmployeeRefId,
                    AllottedStatus = dto.AllottedStatus,
                    SkillRefId = dto.SkillRefId,
                    StationRefId = dto.StationRefId,
                    ShiftRefId = dto.ShiftRefId,
                    NightFlag = dto.NightFlag,
                    Remarks = dto.Remarks,
                    TimeIn = dto.TimeIn,
                    TimeOut = dto.TimeOut,
                    OtTimeIn = dto.OtTimeIn,
                    OtTimeOut = dto.OtTimeOut,
                    UserSerialNumber = dto.UserSerialNumber,
                    LocationRefId = dto.LocationRefId
                };

                await _dutychartdetailRepo.InsertOrUpdateAndGetIdAsync(item);
            }

            CheckErrors(await _dutychartManager.CreateSync(master));

            return new IdInput { Id = master.Id };
        }



        public async Task IncludeAllActiveEmployees(LocationInputDto input)
        {

            var existAlready = await (from m in _employeedefaultworkstationRepo.GetAll()
                                      join p in _personalInformationRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId)
                                      on m.EmployeeRefId equals p.Id
                                      select m.EmployeeRefId).ToListAsync();

            var notExistList = await (from perinfo in _personalInformationRepo.GetAll().Where(m => m.LocationRefId == input.LocationRefId && !existAlready.Contains(m.Id) && m.ActiveStatus == true) select perinfo).ToListAsync();

            var defaultTime = await _dcstationmasterRepo.FirstOrDefaultAsync(t => t.LocationRefId == input.LocationRefId && t.DefaultTimeFlag == true);
            if (defaultTime == null)
            {
                //string locationName = "";
                //var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.LocationRefId);
                //if (loc != null)
                //    locationName = loc.Code;
                //throw new UserFriendlyException(L("DefaultTimeErr") + " " + L("Location") + " " + locationName);
            }

            foreach (var lst in notExistList)
            {
                var skillSet = await _skillsetRepo.FirstOrDefaultAsync(t => t.Id == lst.DefaultSkillRefId);
                int? stationRefId = null;
                int? shiftRefId = null;

                if (skillSet != null)
                {
                    var groupvsskill = await _dcgroupmastervsskillsetRepo.FirstOrDefaultAsync(t => t.SkillSetRefId == lst.DefaultSkillRefId);
                    if (groupvsskill != null)
                    {
                        var station = await _dcstationmasterRepo.FirstOrDefaultAsync(t => t.DcGroupRefId == groupvsskill.DcGropupRefId);
                        if (station != null)
                        {
                            stationRefId = station.Id;
                            var shift = await _dcshiftmasterRepo.FirstOrDefaultAsync(t => t.StationRefId == stationRefId);
                            if (shift != null)
                            {
                                shiftRefId = shift.Id;
                            }
                        }
                    }
                }

                EmployeeDefaultWorkStation newDto = new EmployeeDefaultWorkStation
                {
                    EmployeeRefId = lst.Id,
                    SkillRefId = lst.DefaultSkillRefId,
                    StationRefId = stationRefId,
                    ShiftRefId = shiftRefId,
                };

                var retId2 = await _employeedefaultworkstationRepo.InsertAndGetIdAsync(newDto);
            }
        }

        public async Task<MaxDateWithStatus> GetDutyChartMaximumDate()
        {
            MaxDateWithStatus output = new MaxDateWithStatus();

            var maxValueDayClosed = await _dutychartRepo.GetAll().Where(t => t.Status.Equals("DayClosed")).OrderByDescending(t => t.DutyChartDate).FirstOrDefaultAsync();

            var maxValueFinished = await _dutychartRepo.GetAll().OrderByDescending(t => t.DutyChartDate).FirstOrDefaultAsync();

            DateTime returnDate;
            if (maxValueDayClosed != null)
            {
                DateTime maxDate = maxValueDayClosed.DutyChartDate;
                if (maxValueDayClosed.DutyChartDate < maxValueFinished.DutyChartDate)
                {
                    maxDate = maxValueFinished.DutyChartDate;
                    returnDate = maxDate;
                }
                else
                {
                    returnDate = maxDate.AddDays(1);
                }

            }
            else
            {
                maxValueDayClosed = await _dutychartRepo.GetAll().OrderByDescending(t => t.DutyChartDate).FirstOrDefaultAsync();
                if (maxValueDayClosed != null)
                    returnDate = maxValueDayClosed.DutyChartDate;
                else
                    returnDate = DateTime.Today.AddDays(1);
            }

            output.MaxDate = returnDate;

            var dateFrom = returnDate.AddDays(-10);
            var lastNDaysClosed = await _dutychartRepo.GetAll().Where(t => t.DutyChartDate > dateFrom && !t.Status.Equals("DayClosed")).OrderBy(t => t.DutyChartDate).ToListAsync();
            if (lastNDaysClosed == null)
            {
                output.DateWiseStatusList = new List<DateWiseStatus>();
                return output;
            }
            else
            {
                DateTime maxFrom = dateFrom.AddDays(-60);
                var rsDraftRecords = await _dutychartRepo.GetAll().Where(t => t.DutyChartDate >= maxFrom && t.DutyChartDate <= dateFrom && !t.Status.Equals("DayClosed")).OrderBy(t => t.DutyChartDate).ToListAsync();

                List<DateWiseStatus> datewiseList = new List<DateWiseStatus>();

                foreach (var lst in rsDraftRecords)
                {
                    datewiseList.Add(new DateWiseStatus { DutychartDate = lst.DutyChartDate, Status = L(lst.Status) });
                }

                lastNDaysClosed = await _dutychartRepo.GetAll().Where(t => t.DutyChartDate > dateFrom).OrderBy(t => t.DutyChartDate).ToListAsync();
                foreach (var lst in lastNDaysClosed)
                {
                    datewiseList.Add(new DateWiseStatus { DutychartDate = lst.DutyChartDate, Status = L(lst.Status) });
                }
                output.DateWiseStatusList = datewiseList.OrderByDescending(t => t.DutychartDate).ToList();

                //returnDate = lastNDaysClosed[0].DutyChartDate;
                return output;
            }
        }

        public async Task<EmployeeConsolidatedInformation> GetEmployeeConsolidatedInfo()
        {
            var empList = await _personalInformationRepo.GetAll().Where(t => t.ActiveStatus == true && t.LastWorkingDate == null).ToListAsync();

            List<EmployeeSkillWiseCount> skillset = new List<EmployeeSkillWiseCount>();
            foreach (var group in empList.GroupBy(t => t.DefaultSkillRefId))
            {
                var sk = await _skillsetRepo.FirstOrDefaultAsync(t => t.Id == group.Key);
                EmployeeSkillWiseCount skill = new EmployeeSkillWiseCount();
                skill.Skill = sk.SkillCode;
                skill.Count = group.Count();
                skillset.Add(skill);
            }

            EmployeeConsolidatedInformation empConsolidated = new EmployeeConsolidatedInformation();
            empConsolidated.EmployeeCount = empList.Count();
            empConsolidated.SkillWiseCount = skillset;

            return empConsolidated;
        }


        public async Task<MessageOutput> HrOperationHealthEdp(GetHrOperationHealthDto input)
        {
            MessageOutput output = new House.Master.Dtos.MessageOutput();

            string ConsolidatedPlainErrorMessage = "";

            #region Leave_But_FingerPrintFoundError
            if (input.DaysTill == null)
                input.DaysTill = 1;
            var startDate = DateTime.Today.AddDays(-1 * input.DaysTill.Value);
            var endDate = DateTime.Today.AddDays(-1 * input.DaysTill.Value);
            if (input.StartDate.HasValue)
            {
                startDate = input.StartDate.Value;
            }
            if (input.EndDate.HasValue)
            {
                endDate = input.EndDate.Value;
            }

            var rejectedString = L("Rejected");
            var rsLeaveRequest = _leaveRequestRepo.GetAll().WhereIf(input.EmployeeRefId.HasValue, t => t.EmployeeRefId == input.EmployeeRefId && t.LeaveStatus != rejectedString && t.HalfDayFlag == false);

            var leaveRequest = await rsLeaveRequest.Where(t => t.LeaveTo >= startDate && t.HalfDayFlag == false).OrderBy(t => t.EmployeeRefId).ToListAsync();
            var arrEmpRefIds = leaveRequest.Select(t => t.EmployeeRefId).ToArray();

            var rsAttLog = await _attendanceLogRepo.GetAll().Where(t => arrEmpRefIds.Contains(t.EmployeeRefId) && t.CheckTime >= startDate && t.CheckTime.Value <= endDate).ToListAsync();

            var perinfoQueryable = _personalInformationRepo.GetAll();
            if (input.InchargeEmployeeRefId.HasValue)
            {
                perinfoQueryable = perinfoQueryable.Where(t => t.InchargeEmployeeRefId == input.InchargeEmployeeRefId.Value);
            }

            var rsPerinfo = await perinfoQueryable.ToListAsync();

            bool errorFlag = false;
            string errorMessage = "";
            foreach (var lst in leaveRequest)
            {
                var attlogsExist = rsAttLog.Where(t => t.EmployeeRefId == lst.EmployeeRefId && t.CheckTime >= lst.LeaveFrom && t.CheckTime <= lst.LeaveTo).OrderBy(t => t.CheckTime).ToList();
                if (attlogsExist.Count > 0)
                {
                    var emp = rsPerinfo.FirstOrDefault(t => t.Id == lst.EmployeeRefId);
                    if (emp != null)
                    {
                        var arrTimeFound = "";
                        foreach (var ti in attlogsExist)
                        {
                            if (ti.CheckTime.Value.Hour < 6)
                            {
                                var prevDate = ti.CheckTime.Value.AddDays(-1);
                                var dutychart = await _dutychartdetailRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == ti.EmployeeRefId && DbFunctions.TruncateTime(t.DutyChartDate) == DbFunctions.TruncateTime(prevDate));
                                if (dutychart != null)
                                {
                                    if (dutychart.NightFlag == true)
                                        continue;
                                }
                            }
                            arrTimeFound = arrTimeFound + ti.CheckTime.Value.ToString("yyyy-MMM-dd hh:mm tt") + ",";
                            errorFlag = true;
                        }
                        if (arrTimeFound.Length > 0)
                            arrTimeFound = arrTimeFound.Left(arrTimeFound.Length - 1);
                        ConsolidatedPlainErrorMessage = ConsolidatedPlainErrorMessage + L("LeaveButFingerFoundError", emp.EmployeeName, lst.LeaveFrom.ToString("yyyy-MMM-dd"), lst.LeaveTo.ToString("yyyy-MMM-dd"), arrTimeFound) + "<BR><br>";

                        errorMessage = errorMessage + L("LeaveButFingerFoundError", emp.EmployeeName, lst.LeaveFrom.ToString("yyyy-MMM-dd"), lst.LeaveTo.ToString("yyyy-MMM-dd"), arrTimeFound) + "<BR> <BR>";
                    }
                }
            }

            if (errorFlag == true)
            {
                errorMessage = "<span>" + errorMessage + "<span>";

                output.HtmlMessage = output.HtmlMessage + "<span STYLE='Color:White;Background-Color:OrangeRed'>" + L("LeaveButFingerFoundErrorHeader") + "</span>";
                output.HtmlMessage = output.HtmlMessage + "<BR>" + errorMessage + "<HR/>";

                ConsolidatedPlainErrorMessage = L("LeaveButFingerFoundErrorHeader") + "<br>" + ConsolidatedPlainErrorMessage;
            }
            errorFlag = false;
            errorMessage = "";
            #endregion

            #region Absent_Leave_In_DutyChart_But_FingerPrintFoundError
            string[] statusString = { L("Leave"), L("Absent"), L("WeekOff"), L("PH") };
            //effectiveFrom = DateTime.Today.AddDays(-1 * 10);
            rsAttLog = await _attendanceLogRepo.GetAll().Where(t => t.CheckTime >= startDate && t.CheckTime.Value <= endDate).ToListAsync();
            var rsDutychart = _dutychartdetailRepo.GetAll().WhereIf(input.EmployeeRefId.HasValue, t => t.EmployeeRefId == input.EmployeeRefId);
            var leaveAbsentFoundList = await rsDutychart.Where(t => t.DutyChartDate >= startDate && t.DutyChartDate <= endDate && statusString.Contains(t.AllottedStatus)).OrderBy(t => t.EmployeeRefId).ToListAsync();
            arrEmpRefIds = leaveAbsentFoundList.Select(t => t.EmployeeRefId).ToArray();

            foreach (var lst in leaveAbsentFoundList)
            {
                var attlogsExist = rsAttLog.Where(t => t.EmployeeRefId == lst.EmployeeRefId && t.CheckTime.Value.Date == lst.DutyChartDate.Date).OrderBy(t => t.CheckTime.Value).ToList();
                if (attlogsExist.Count > 0)
                {
                    var emp = rsPerinfo.FirstOrDefault(t => t.Id == lst.EmployeeRefId);
                    if (emp != null)
                    {
                        var arrTimeFound = "";
                        foreach (var ti in attlogsExist)
                        {
                            if (ti.CheckTime.Value.Hour < 6)
                            {
                                var prevDate = ti.CheckTime.Value.AddDays(-1);
                                var dutychart = await _dutychartdetailRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == ti.EmployeeRefId && DbFunctions.TruncateTime(t.DutyChartDate) == DbFunctions.TruncateTime(prevDate));
                                if (dutychart != null)
                                {
                                    if (dutychart.NightFlag == true)
                                        continue;
                                }
                            }
                            arrTimeFound = arrTimeFound + ti.CheckTime.Value.ToString("yyyy-MMM-dd hh:mm tt") + ",";
                        }

                        if (arrTimeFound.Length > 0)
                        {
                            errorFlag = true;
                            arrTimeFound = arrTimeFound.Left(arrTimeFound.Length - 1);
                        }
                        else
                        {
                            continue;
                        }

                        ConsolidatedPlainErrorMessage = ConsolidatedPlainErrorMessage + L("AbsentOrLeaveButFingerFoundError", emp.EmployeeName, lst.DutyChartDate.ToString("yyyy-MMM-dd"), lst.AllottedStatus, arrTimeFound) + "<br>";

                        errorMessage = errorMessage + L("AbsentOrLeaveButFingerFoundError", emp.EmployeeName, lst.DutyChartDate.ToString("yyyy-MMM-dd"), lst.AllottedStatus, arrTimeFound) + "<BR> <br>";
                    }
                }
            }

            if (errorFlag == true)
            {
                output.HtmlMessage = output.HtmlMessage + "<Span STYLE='Color:White;Background-Color:OrangeRed;font-size:medium'>" + L("AbsentOrLeaveButFingerFoundErrorHeader") + "</Span>";
                output.HtmlMessage = output.HtmlMessage + "<BR><BR>" + errorMessage + "<HR/>";

                ConsolidatedPlainErrorMessage = L("AbsentOrLeaveButFingerFoundErrorHeader") + "<BR><br>" + ConsolidatedPlainErrorMessage;
            }
            errorFlag = false;
            errorMessage = "";

            #endregion



            output.ErrorMessage = ConsolidatedPlainErrorMessage;
            return output;
        }


        public async Task<MessageOutput> HrOperationAlert(GetHrOperationHealthDto input)
        {
            MessageOutput output = new MessageOutput();
            bool oneDayFlag = false;
            string ConsolidatedPlainErrorMessage = "";

            #region Idle_But_NoThump_In_Office
            DateTime effectiveFrom;
            DateTime maxDate;
            if (input.StartDate.HasValue && input.EndDate.HasValue)
            {
                effectiveFrom = input.StartDate.Value;
                maxDate = effectiveFrom;
                oneDayFlag = true;
            }
            else
            {
                oneDayFlag = false;
                if (input.DaysTill == null)
                    input.DaysTill = 10;
                effectiveFrom = DateTime.Today.AddDays(-1 * input.DaysTill.Value);
                var attLocation = await _attendanceMachineLocationRepo.FirstOrDefaultAsync(t => t.Id > 0);
                if (attLocation == null)
                {
                    return null;
                    throw new UserFriendlyException(L("AttendanceLocation") + " " + L("Required"));
                }
                var maxattLog = _attendanceLogRepo.GetAll().Where(t => t.AttendanceMachineLocationRefId == attLocation.Id && t.CheckTime > effectiveFrom);
                if (maxattLog.Any() == false)
                {
                    return output;
                }
                maxDate = effectiveFrom;
                var totalCount = maxattLog.Count();

                if (totalCount > 0)
                {
                    var maxAttDate = maxattLog.Max(t => t.CheckTime.Value);
                    maxDate = maxAttDate;
                }
                if (maxDate < effectiveFrom)
                    return output;
            }

            var rsDutychart = _dutychartdetailRepo.GetAll().WhereIf(input.EmployeeRefId.HasValue, t => t.EmployeeRefId == input.EmployeeRefId);
            string idleString = L("Idle");
            var idleDutyList = await rsDutychart.Where(t => t.AllottedStatus.Equals(idleString) && t.DutyChartDate >= effectiveFrom && t.DutyChartDate <= maxDate).OrderBy(t => t.EmployeeRefId).ToListAsync();
            var arrEmpRefIds = idleDutyList.Select(t => t.EmployeeRefId).Distinct().ToArray();

            var rsAttLog = await _attendanceLogRepo.GetAll().Where(t => arrEmpRefIds.Contains(t.EmployeeRefId) && t.CheckTime >= effectiveFrom).ToListAsync();

            var perinfoQueryable = _personalInformationRepo.GetAll();
            if (input.InchargeEmployeeRefId.HasValue)
            {
                perinfoQueryable = perinfoQueryable.Where(t => t.InchargeEmployeeRefId == input.InchargeEmployeeRefId.Value);
            }

            var rsPerinfo = await perinfoQueryable.ToListAsync();

            var uptoTime = maxDate.AddDays(1);
            var rsGpsAttLog = await _gpsAttendanceRepo.GetAll().Where(t => arrEmpRefIds.Contains(t.EmployeeRefId)
                                && t.CreationTime >= effectiveFrom && t.CreationTime <= uptoTime).ToListAsync();


            var startDate = effectiveFrom.AddDays(-30);
            var rsBiometricExcluded = await _biometricExcludedRepo.GetAll()
                    .Where(t => arrEmpRefIds.Contains(t.EmployeeRefId) && (t.ExcludeFromDate > startDate) && (t.ExcludeToDate <= uptoTime)).ToListAsync();

            bool errorFlag = false;
            string errorMessage = "";
            foreach (var lst in idleDutyList)
            {
                var emp = rsPerinfo.FirstOrDefault(t => t.Id == lst.EmployeeRefId);
                if (emp == null)
                    continue;

                if (emp.IsAttendanceRequired == false)
                    continue;

                var excludedList = rsBiometricExcluded.Where(t => t.EmployeeRefId == lst.EmployeeRefId && t.ExcludeFromDate.Date >= lst.DutyChartDate.Date
                                && t.ExcludeToDate.Date <= lst.DutyChartDate.Date).ToList();
                if (excludedList.Count > 0)
                    continue;
                else
                {
                    int I = 9;
                }


                var attlogsExist = rsAttLog.Where(t => t.EmployeeRefId == lst.EmployeeRefId && t.CheckTime.Value.Date >= lst.DutyChartDate && t.CheckTime.Value.Date <= lst.DutyChartDate).OrderBy(t => t.CheckTime).ToList();
                if (attlogsExist.Count > 0)
                {
                    var arrTimeFound = "";
                    foreach (var ti in attlogsExist)
                    {
                        if (ti.CheckTime.Value.Hour < 6)
                        {
                            var prevDate = ti.CheckTime.Value.AddDays(-1);
                            var dutychart = await _dutychartdetailRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == ti.EmployeeRefId && DbFunctions.TruncateTime(t.DutyChartDate) == DbFunctions.TruncateTime(prevDate));
                            if (dutychart != null)
                            {
                                if (dutychart.NightFlag == true)
                                    continue;
                            }
                            arrTimeFound = arrTimeFound + ti.CheckTime.Value.ToString("yyyy-MMM-dd hh:mm") + ",";
                        }
                        else
                        {
                            arrTimeFound = arrTimeFound + ti.CheckTime.Value.ToString("yyyy-MMM-dd hh:mm") + ",";
                            break;
                        }
                    }
                    if (arrTimeFound.Length > 0)
                    {
                        arrTimeFound = arrTimeFound.Left(arrTimeFound.Length - 1);
                    }
                    else
                    {
                        if (oneDayFlag == true)
                        {
                            ConsolidatedPlainErrorMessage = ConsolidatedPlainErrorMessage + emp.EmployeeName + " <br>";
                            errorMessage = errorMessage + emp.EmployeeName + " <BR>";
                        }
                        else
                        {
                            ConsolidatedPlainErrorMessage = ConsolidatedPlainErrorMessage + L("IdleButFingerNotFoundError", emp.EmployeeName, lst.DutyChartDate.ToString("yyyy-MMM-dd")) + "<br>";

                            errorMessage = errorMessage + L("IdleButFingerNotFoundError", emp.EmployeeName, lst.DutyChartDate.ToString("yyyy-MMM-dd")) + "<BR> <BR>";
                        }
                        errorFlag = true;
                    }
                }
                else
                {
                    // Check GPS Attendance Exists

                    var gpsattlogsExist = rsGpsAttLog.Where(t => t.EmployeeRefId == lst.EmployeeRefId
                                    && t.CreationTime.Date >= lst.DutyChartDate && t.CreationTime.Date <= lst.DutyChartDate).OrderBy(t => t.CreationTime).ToList();
                    if (gpsattlogsExist.Count > 0)
                    {
                        continue;
                    }
                    if (oneDayFlag == true)
                    {
                        ConsolidatedPlainErrorMessage = ConsolidatedPlainErrorMessage + emp.EmployeeName + " <br>";
                        errorMessage = errorMessage + emp.EmployeeName + "<BR>";
                    }
                    else
                    {
                        ConsolidatedPlainErrorMessage = ConsolidatedPlainErrorMessage + L("IdleButFingerNotFoundError", emp.EmployeeName, lst.DutyChartDate.ToString("yyyy-MMM-dd")) + "<br>";

                        errorMessage = errorMessage + L("IdleButFingerNotFoundError", emp.EmployeeName, lst.DutyChartDate.ToString("yyyy-MMM-dd")) + "<BR> <BR>";
                    }
                    errorFlag = true;
                }
            }

            if (errorFlag == true)
            {
                errorMessage = "<span>" + errorMessage + "<span>";

                output.HtmlMessage = output.HtmlMessage + "<span STYLE='Color:Blue;Background-Color:White;font-size:medium'>" + L("IdleButFingerNotFoundErrorHeader") + "</span> <BR>";
                output.HtmlMessage = output.HtmlMessage + "<BR>" + errorMessage + "<HR/>";

                ConsolidatedPlainErrorMessage = L("IdleButFingerNotFoundErrorHeader") + "<br> <br>" + ConsolidatedPlainErrorMessage;
            }
            errorFlag = false;
            errorMessage = "";
            #endregion

            output.ErrorMessage = ConsolidatedPlainErrorMessage;
            return output;
        }

        public async Task<FileNameDto> GetExcelOfDutyChartForGivenDate(InputExcelDutyChartCompanyWiseDto input)
        {
            List<OutputExcelDutyChartCompanyDto> output = new List<OutputExcelDutyChartCompanyDto>();
            var rsDutyChart = _dutychartRepo.GetAll().Where(t => DbFunctions.TruncateTime(t.DutyChartDate) == DbFunctions.TruncateTime(input.DutyChartDate));

            if (rsDutyChart == null || rsDutyChart.Count() == 0)
            {
                throw new UserFriendlyException(L("NoRecordsFound"));
            }


            var rsDutychartDetails = await (from mas in rsDutyChart
                                            join det in _dutychartdetailRepo.GetAll() on mas.Id equals det.DutyChartRefId
                                            join per in _personalInformationRepo.GetAll() on det.EmployeeRefId equals per.Id
                                            select new DutyChartDetailListDto
                                            {
                                                CompanyRefId = det.CompanyRefId,
                                                DutyChartDate = det.DutyChartDate,
                                                SkillRefId = det.SkillRefId,
                                                EmployeeRefId = det.EmployeeRefId,
                                                EmployeeRefCode = per.EmployeeCode,
                                                EmployeeRefName = per.EmployeeName,
                                                AllottedStatus = det.AllottedStatus,
                                                ShiftRefId = det.ShiftRefId,
                                                StationRefId = det.StationRefId
                                            }
                                            ).ToListAsync();

            int[] arrIdleEmpList = rsDutychartDetails.Where(t => t.AllottedStatus.Equals("Idle")).Select(t => t.EmployeeRefId).ToArray();
            var rsEmployeeSkillSet = await _employeeskillsetRepo.GetAllListAsync(t => arrIdleEmpList.Contains(t.EmployeeRefId));
            var rsSkillSets = await _skillsetRepo.GetAllListAsync();
            foreach (var lst in rsDutychartDetails.Where(t => t.AllottedStatus.Equals("Idle")))
            {
                var skillList = rsEmployeeSkillSet.Where(t => t.EmployeeRefId == lst.EmployeeRefId).ToList();
                int[] skillRefIds = skillList.Select(t => t.SkillSetRefId).ToArray();
                string[] arrSkill = rsSkillSets.Where(t => skillRefIds.Contains(t.Id)).Select(t => t.SkillCode).ToArray();
                lst.KnownSkillSetList = string.Join(",", arrSkill);
            }

            var rsCompanies = await _companyRepo.GetAllListAsync();
            rsDutychartDetails = rsDutychartDetails.OrderBy(t => t.CompanyRefId).ToList(); ;
            foreach (var lst in rsDutychartDetails.GroupBy(t => t.CompanyRefId))
            {
                var company = rsCompanies.FirstOrDefault(t => t.Id == lst.Key);
                OutputExcelDutyChartCompanyDto newDto = new OutputExcelDutyChartCompanyDto();
                newDto.CompanyRefId = lst.Key;
                newDto.CompanyRefName = company.Name;
                newDto.DutyChartDate = input.DutyChartDate;
                newDto.NoOfWorkers = lst.Count();
                newDto.DayDetail = lst.OrderBy(t => t.StationRefId).OrderBy(t => t.SkillRefId).OrderByDescending(t => t.AllottedStatus).ToList();
                output.Add(newDto);
            }
            output = output.OrderBy(t => t.CompanyRefId).ToList();
            return await _dutychartExporter.DutyChartCompanyWiseReportForGivenDate(output, input.TenantName, input.DutyChartDate, input.ExportType);
        }

        public async Task<FileNameDto> GetConsolidatedExcelOfDutyChartForGivenDate(InputExcelDutyChartCompanyWiseDto input)
        {
            List<OutputExcelDutyChartCompanyDto> output = new List<OutputExcelDutyChartCompanyDto>();

            var rsDutyChart = _dutychartRepo.GetAll().Where(t => DbFunctions.TruncateTime(t.DutyChartDate) == DbFunctions.TruncateTime(input.DutyChartDate));

            if (rsDutyChart == null || rsDutyChart.Count() == 0)
            {
                throw new UserFriendlyException(L("NoRecordsFound"));
            }

            var rsEmployee = _personalInformationRepo.GetAll();
            if (input.SuperVisortEmployeeRefId.HasValue)
            {
                if (input.SuperVisortEmployeeRefId.Value > 0)
                {
                    rsEmployee = rsEmployee.Where(t => t.SupervisorEmployeeRefId == input.SuperVisortEmployeeRefId.Value);
                }
            }
            if (input.InchargeEmployeeRefId.HasValue)
            {
                if (input.InchargeEmployeeRefId.Value > 0)
                {
                    rsEmployee = rsEmployee.Where(t => t.InchargeEmployeeRefId == input.InchargeEmployeeRefId.Value);
                }
            }

            var rsDutychartDetails = await (from mas in rsDutyChart
                                            join det in _dutychartdetailRepo.GetAll() on mas.Id equals det.DutyChartRefId
                                            join per in rsEmployee on det.EmployeeRefId equals per.Id
                                            join jt in _jobTitleMasterRepo.GetAll() on per.JobTitleRefId equals jt.Id
                                            select new DutyChartDetailListDto
                                            {
                                                CompanyRefId = det.CompanyRefId,
                                                DutyChartDate = det.DutyChartDate,
                                                SkillRefId = det.SkillRefId,
                                                EmployeeRefId = det.EmployeeRefId,
                                                EmployeeRefCode = per.EmployeeCode,
                                                EmployeeRefName = per.EmployeeName,
                                                AllottedStatus = det.AllottedStatus,
                                                InchargeEmployeeRefId = per.InchargeEmployeeRefId,
                                                SupervisorEmployeeRefId = per.SupervisorEmployeeRefId,
                                                JobTitle = jt.JobTitle,
                                                JobTitleRefId = per.JobTitleRefId.Value,
                                                UserSerialNumber = per.UserSerialNumber
                                            }
                                            ).ToListAsync();

            var rsCompanies = await _companyRepo.GetAllListAsync();
            #region InchargeAndSupervisor
            List<int> empRefIdsRsRm = rsDutychartDetails.Where(t => t.SupervisorEmployeeRefId.HasValue).Select(t => t.SupervisorEmployeeRefId.Value).ToList();
            empRefIdsRsRm.AddRange(rsDutychartDetails.Where(t => t.InchargeEmployeeRefId.HasValue).Select(t => t.InchargeEmployeeRefId.Value).ToList());
            var rsPersonalIncharges = await _personalInformationRepo.GetAllListAsync(t => empRefIdsRsRm.Contains(t.Id));

            foreach (var lst in rsDutychartDetails)
            {
                var empsvr = rsPersonalIncharges.FirstOrDefault(t => t.Id == lst.SupervisorEmployeeRefId);
                var empmgr = rsPersonalIncharges.FirstOrDefault(t => t.Id == lst.InchargeEmployeeRefId);
                if (empsvr == null)
                {
                    throw new UserFriendlyException(L("NotExistForId", L("Supervisor"), lst.EmployeeRefCode));
                }
                if (empmgr == null)
                {
                    throw new UserFriendlyException(L("NotExistForId", L("Manager"), lst.InchargeEmployeeRefId));
                }
                lst.SupervisorEmployeeRefName = empsvr.EmployeeName;
                lst.InchargeEmployeeRefName = empmgr.EmployeeName;

                var company = rsCompanies.FirstOrDefault(t => t.Id == lst.CompanyRefId);
                lst.CompanyRefName = company.Name;
                lst.CompanyPrefix = company.Code;
            }
            #endregion

            #region Idle Employees Skill Sets
            int[] arrIdleEmpList = rsDutychartDetails.Where(t => t.AllottedStatus.Equals("Idle")).Select(t => t.EmployeeRefId).ToArray();
            var rsEmployeeSkillSet = await _employeeskillsetRepo.GetAllListAsync(t => arrIdleEmpList.Contains(t.EmployeeRefId));
            var rsSkillSets = await _skillsetRepo.GetAllListAsync();
            foreach (var lst in rsDutychartDetails.Where(t => t.AllottedStatus.Equals("Idle")))
            {
                var skillList = rsEmployeeSkillSet.Where(t => t.EmployeeRefId == lst.EmployeeRefId).ToList();
                int[] skillRefIds = skillList.Select(t => t.SkillSetRefId).ToArray();
                string[] arrSkill = rsSkillSets.Where(t => skillRefIds.Contains(t.Id)).Select(t => t.SkillCode).ToArray();
                lst.KnownSkillSetList = string.Join(",", arrSkill);
            }
            #endregion



            return await _dutychartExporter.DutyChartConsolidatedCompanyWiseReportForGivenDate(rsDutychartDetails, input.TenantName, input.DutyChartDate, input.ExportType, input);
        }

        public async Task<List<LeaveRequestListDto>> CheckLeaveContinuation(GetLeaveEmployeesWithToDate input)
        {
            var rejectedString = L("Rejected");
            var rsLeaveRequest = _leaveRequestRepo.GetAll().Where(t => DbFunctions.TruncateTime(t.LeaveTo) == DbFunctions.TruncateTime(input.LeaveToDate)
                && t.TotalNumberOfDays >= input.NoOfDaysMinimum && t.LeaveStatus != rejectedString);
            int[] arrEmpRefIs = await rsLeaveRequest.Select(t => t.EmployeeRefId).ToArrayAsync();
            var chkDate = input.LeaveToDate.AddDays(1);
            var rsAttLogs = _attendanceLogRepo.GetAll().Where(t => arrEmpRefIs.Contains(t.EmployeeRefId) &&
                       DbFunctions.TruncateTime(t.CheckTime) == DbFunctions.TruncateTime(chkDate));
            arrEmpRefIs = await rsAttLogs.Select(t => t.EmployeeRefId).ToArrayAsync();
            DateTime nxtDay = input.LeaveToDate.AddDays(1);

            var arrEmpsNextDayOnWardsLeave = _leaveRequestRepo.GetAll().Where(t => DbFunctions.TruncateTime(t.LeaveFrom) == DbFunctions.TruncateTime(nxtDay)
                                            && arrEmpRefIs.Contains(t.EmployeeRefId) && t.LeaveStatus != rejectedString);
            int[] arrNextDayLeaveEmpRefIds = await arrEmpsNextDayOnWardsLeave.Select(t => t.EmployeeRefId).ToArrayAsync();

            var allItems = await (from lr in rsLeaveRequest.Where(t => !arrEmpRefIs.Contains(t.EmployeeRefId) && !arrNextDayLeaveEmpRefIds.Contains(t.EmployeeRefId))
                                  join pi in _personalInformationRepo.GetAll().Where(t => t.ActiveStatus == true) on lr.EmployeeRefId equals pi.Id
                                  join loc in _locationRepo.GetAll() on pi.LocationRefId equals loc.Id
                                  join co in _companyRepo.GetAll() on loc.CompanyRefId equals co.Id
                                  join lt in _leavetypeRepo.GetAll() on lr.LeaveTypeRefCode equals lt.Id
                                  select new LeaveRequestListDto()
                                  {
                                      Id = lr.Id,
                                      CompanyRefCode = co.Code,
                                      LocationRefCode = loc.Code,
                                      EmployeeRefId = lr.EmployeeRefId,
                                      EmployeeRefCode = pi.EmployeeCode,
                                      EmployeeRefName = pi.EmployeeName,
                                      LeaveTypeRefCode = lr.LeaveTypeRefCode,
                                      LeaveTypeRefName = lt.LeaveTypeName,
                                      DateOfApply = lr.DateOfApply,
                                      LeaveFrom = lr.LeaveFrom,
                                      LeaveTo = lr.LeaveTo,
                                      LeaveStatus = lr.LeaveStatus,
                                      TotalNumberOfDays = lr.TotalNumberOfDays,
                                      CreationTime = lr.CreationTime,
                                      LeaveReason = lr.LeaveReason,
                                      NationalIdentificationNumber = pi.NationalIdentificationNumber,
                                  }).ToListAsync();
            return allItems;
        }

        //public async Task<List<MessageOutput>> SendDayStatusEmailToAllEmployee(GetStatusAlertMailDto input)
        //{
        //    DateTime minDate = DateTime.Today.AddDays(-30);

        //    if (input.DutyChartDate <= minDate)
        //    {
        //        return null;
        //    }
        //    TimeSpan diff = input.DutyChartDate.Subtract(DateTime.Today);
        //    if (Math.Abs(diff.Days) > 15)
        //    {
        //        return null;
        //    }
        //    List<SimpleDutyChartDetailListDto> alreadyStatusMailed = new List<SimpleDutyChartDetailListDto>();
        //    if (input.MailSendFlag)
        //    {
        //        //var rsstatusMail = await _statusMailDate.FirstOrDefaultAsync(t => t.DutyChartDate == input.DutyChartDate);
        //        //if (rsstatusMail != null)
        //        //{
        //        //    alreadyStatusMailed = _xmlandjsonConvertorAppService.DeSerializeFromJSON<List<SimpleDutyChartDetailListDto>>(rsstatusMail.EmployeeListJson);
        //        //}
        //    }

        //    var rsPersonalInformation = await _personalInformationRepo.GetAllListAsync();
        //    var retList = new List<MessageOutput>();
        //    string weekOffString = L("WeekOff");
        //    string phString = L("PH");
        //    var dcEmps = await _dutychartdetailRepo.GetAllListAsync(t => t.DutyChartDate == input.DutyChartDate
        //        && !t.AllottedStatus.Equals(input.JobStatus) && !t.AllottedStatus.Equals(weekOffString));

        //    foreach (var emp in dcEmps)
        //    {
        //        if (emp.AllottedStatus == L("PH"))
        //        {
        //            continue;
        //        }

        //        var perinfo = rsPersonalInformation.FirstOrDefault(t => t.Id == emp.EmployeeRefId);
        //        if (perinfo == null)
        //        {
        //            continue;
        //        }
        //        if (perinfo.IsDayCloseStatusMailRequired == false)
        //        {
        //            continue;
        //        }

        //        {
        //            var existRecord = alreadyStatusMailed.FirstOrDefault(t => t.EmployeeRefId == emp.EmployeeRefId);
        //            var dataToAdded = emp.MapTo<SimpleDutyChartDetailListDto>();
        //            if (existRecord == null)
        //                alreadyStatusMailed.Add(dataToAdded);
        //            else
        //            {
        //                if (existRecord.ToString() == dataToAdded.ToString())
        //                {
        //                    continue;
        //                }
        //                else
        //                {
        //                    var existId = alreadyStatusMailed.IndexOf(existRecord);
        //                    alreadyStatusMailed.RemoveAt(existId);
        //                    alreadyStatusMailed.Add(dataToAdded);
        //                }
        //            }

        //            var empDto = new GetStatusAlertMailDto
        //            {
        //                EmployeeRefId = emp.EmployeeRefId,
        //                DutyChartDate = emp.DutyChartDate,
        //                MailSendFlag = input.MailSendFlag
        //            };
        //            var mailDetail = await SendDayStatusEmail(empDto);
        //            retList.AddRange(mailDetail);
        //        }
        //    }

        //    if (input.MailSendFlag)
        //    {
        //        var rsstatusMail = await _statusMailDate.FirstOrDefaultAsync(t => t.DutyChartDate == input.DutyChartDate);
        //        var jsonReturn = _xmlandjsonConvertorAppService.SerializeToJSON(alreadyStatusMailed);

        //        if (rsstatusMail != null)
        //        {
        //            var editDto = await _statusMailDate.GetAsync(rsstatusMail.Id);
        //            editDto.EmployeeListJson = jsonReturn;
        //            await _statusMailDate.UpdateAsync(editDto);
        //        }
        //        else
        //        {
        //            var newStatusDto = new StatusMailDate
        //            {
        //                DutyChartDate = input.DutyChartDate,
        //                EmployeeListJson = jsonReturn
        //            };
        //            await _statusMailDate.InsertAsync(newStatusDto);
        //        }
        //    }


        //    return retList;
        //}

        //public async Task<List<MessageOutput>> SendDayStatusEmail(GetStatusAlertMailDto input)
        //{
        //    MessageOutput retOutput = new MessageOutput();
        //    retOutput.SuccessFlag = true;

        //    var emp = await _personalInformationRepo.FirstOrDefaultAsync(t => t.Id == input.EmployeeRefId);
        //    retOutput.EmployeeRefId = emp.Id;
        //    retOutput.EmployeeRefName = emp.EmployeeName;
        //    retOutput.PersonalEmail = emp.PersonalEmail;
        //    retOutput.Email = emp.Email;
        //    retOutput.SuccessFlag = false;
        //    bool emailContentsExists = false;

        //    //if (emp.BioMetricCode.Value==214)
        //    //{
        //    //	input.MailSendFlag = true;
        //    //}
        //    //else
        //    //{
        //    //	input.MailSendFlag = false;
        //    //}
        //    var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == emp.LocationRefId);

        //    var company = await _companyRepo.FirstOrDefaultAsync(t => t.Id == location.CompanyRefId);
        //    if (company == null)
        //    {
        //        throw new UserFriendlyException(L("Company" + " ?"));
        //    }

        //    var mailList = location.Email;
        //    if (mailList.IsNullOrEmpty())
        //    {
        //        throw new UserFriendlyException(L("Company") + L("EmailIdList") + " ?");
        //    }
        //    string[] emailList = mailList.Split(";");

        //    var mailMessage = new StringBuilder();

        //    var hroperationHealthEdp = await HrOperationHealthEdp(new GetHrOperationHealthDto
        //    {
        //        DaysTill = 45,
        //        EmployeeRefId = input.EmployeeRefId
        //    });

        //    retOutput.HrOperationHealthHtml = hroperationHealthEdp.HtmlMessage;
        //    retOutput.HrOperationHealthText = hroperationHealthEdp.ErrorMessage;

        //    mailMessage.AppendLine(hroperationHealthEdp.HtmlMessage);

        //    string htmlLine;

        //    htmlLine = "<p>Dear <strong> " + emp.EmployeeName + " </strong> ,</p> ";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = " <p> Please verify all the details and If you found any errors please let us know immediately by reply email to the below email ids.</p>";
        //    mailMessage.AppendLine(htmlLine);
        //    htmlLine = " <p> " + mailList + " </p>";
        //    mailMessage.AppendLine(htmlLine);
        //    htmlLine = " <p> 	Make sure all the time sheets should reach office on time and enable us to submit the invoice on time to Clients.</p> ";
        //    mailMessage.AppendLine(htmlLine);

        //    htmlLine = "  <p> &nbsp;</p> ";
        //    mailMessage.AppendLine(htmlLine);
        //    htmlLine = "   <p> HR / EDP </p>";
        //    mailMessage.AppendLine(htmlLine);
        //    htmlLine = "     <p> A-STAR GROUP </p> ";
        //    mailMessage.AppendLine(htmlLine);
        //    htmlLine = "    <p> &nbsp;</p>";
        //    mailMessage.AppendLine(htmlLine);
        //    htmlLine = "  <p> &nbsp;</p> ";
        //    mailMessage.AppendLine(htmlLine);
        //    htmlLine = "  <p> &nbsp;</p> ";
        //    mailMessage.AppendLine(htmlLine);

        //    if (input.MailSendFlag == true && emailContentsExists == true)
        //    {
        //        try
        //        {
        //            SmtpClient client = new SmtpClient();
        //            client.DeliveryMethod = SmtpDeliveryMethod.Network;
        //            client.EnableSsl = true;
        //            client.Host = "smtp.gmail.com";
        //            client.Port = 587;


        //            System.Net.NetworkCredential credentials =
        //                new System.Net.NetworkCredential("email@email.com", "emailpassword");
        //            client.UseDefaultCredentials = false;
        //            client.Credentials = credentials;

        //            MailMessage msg = new MailMessage();
        //            if (!emp.Email.IsNullOrEmpty())
        //                msg.To.Add(new MailAddress(emp.Email));
        //            if (!emp.PersonalEmail.IsNullOrEmpty())
        //                msg.To.Add(new MailAddress(emp.PersonalEmail));

        //            if (emp.Email.IsNullOrEmpty() && emp.PersonalEmail.IsNullOrEmpty())
        //            {
        //                htmlLine = " <p> <B> Note :	The Employee Does Not Have The Valid Email Id , so you received this mail and please update the employee email id immediately. </B> </p> ";
        //                mailMessage.AppendLine(htmlLine);

        //                foreach (var em in emailList)
        //                {
        //                    msg.To.Add(new MailAddress(em));
        //                }
        //            }

        //            htmlLine = "Your Job Status is " + onDateStatus.AllottedStatus + "  on " + input.DutyChartDate.ToString("dd-MMM-yyyy dddd") + " ";
        //            var onDateStatus = retlst.FirstOrDefault(t => t.DutyChartDate == input.DutyChartDate);

        //            msg.Subject = htmlLine;
        //            msg.IsBodyHtml = true;
        //            msg.Body = mailMessage.ToString();
        //            try
        //            {
        //                if (msg.To.Count > 0)
        //                    client.Send(msg);
        //                EmployeeMailMessage employeeMessage = new EmployeeMailMessage
        //                {
        //                    EmployeeRefId = emp.Id,
        //                    MailTime = DateTime.Now,
        //                    DutyChartDate = input.DutyChartDate,
        //                    DutyStatusMailed = onDateStatus.AllottedStatus,
        //                    MessageType = (int)MailMessageType.DailyDutyChartStatus_9,
        //                    ReceipientsMailIdsList = msg.To.ToString(),
        //                    MessageText = mailMessage.ToString(),
        //                    MessageApprovedBy = (int)AbpSession.UserId
        //                };
        //                await _employeeMailMessage.InsertOrUpdateAndGetIdAsync(employeeMessage);
        //            }
        //            catch (Exception ex)
        //            {
        //                retOutput.SuccessFlag = false;
        //                retOutput.ErrorMessage = L("NotAbleToSendMailError", ex.Message + ex.InnerException);
        //            }
        //            emailContentsExists = false;
        //            retOutput.SuccessFlag = true;
        //        }
        //        catch (Exception ex)
        //        {
        //            retOutput.SuccessFlag = false;
        //            retOutput.ErrorMessage = L("NotAbleToSendMailError", ex.Message + ex.InnerException);
        //            throw;
        //        }
        //    }

        //    retOutput.Message = mailMessage.ToString();
        //    retOutput.HtmlMessage = mailMessage.ToString();

        //    List<MessageOutput> retList = new List<MessageOutput>();
        //    retList.Add(retOutput);
        //    return retList;
        //}

        public async Task FillDutyDetailsForNewDate(FillDutyChart input)
        {

            List<DutyChartDetailListDto> editListDtos;
            editListDtos = new List<DutyChartDetailListDto>();

            List<DutyChartDetail> previousDateData = new List<DutyChartDetail>();
            DateTime prevDate = input.DutyChartDate.AddDays(-1);
            DateTime onedayBeforePrevDate = input.DutyChartDate.AddDays(-6);
            previousDateData = await _dutychartdetailRepo.GetAllListAsync(t => t.LocationRefId == input.LocationRefId && DbFunctions.TruncateTime(t.DutyChartDate) > DbFunctions.TruncateTime(onedayBeforePrevDate) /*DbFunctions.TruncateTime(prevDate) || DbFunctions.TruncateTime(t.DutyChartDate) == DbFunctions.TruncateTime(onedayBeforePrevDate)*/);
            previousDateData = previousDateData.OrderByDescending(t => t.DutyChartDate).ToList();

            List<DutyChartDetail> existData = new List<DutyChartDetail>();
            int[] existEmpIdsinDutyChart;
            if (input.DutyChartRefId != null)
                existData = await _dutychartdetailRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId && t.DutyChartRefId == input.DutyChartRefId).ToListAsync();

            existEmpIdsinDutyChart = existData.Select(t => t.EmployeeRefId).ToArray();

            var rsPersonalInformation = await _personalInformationRepo.GetAllListAsync();

            rsPersonalInformation = rsPersonalInformation.Where(t => t.LocationRefId == input.LocationRefId && t.DateHired <= input.DutyChartDate
                && (t.LastWorkingDate == null || t.LastWorkingDate >= input.DutyChartDate) && !existEmpIdsinDutyChart.Contains(t.Id)).ToList();

            var rsDefaultWorkStation = await _employeedefaultworkstationRepo.GetAllListAsync();

            editListDtos = (from ews in rsDefaultWorkStation
                            join perinfo in rsPersonalInformation
                            on ews.EmployeeRefId equals perinfo.Id
                            select new DutyChartDetailListDto
                            {
                                DutyChartRefId = 0,
                                AllottedStatus = "",
                                EmployeeRefId = ews.EmployeeRefId,
                                ShiftRefId = ews.ShiftRefId,
                                StationRefId = ews.StationRefId,
                                SkillRefId = ews.SkillRefId,
                                TimeDescription = "Fill",
                                NightFlag = false,
                                Remarks = "",
                                UserSerialNumber = ews.UserSerialNumber,
                            }
                          ).ToList();
            if (editListDtos == null || editListDtos.Count == 0)
            {
                return;
            }
            var rsCompany = await _companyRepo.GetAllListAsync();
            var rsSkills = await _skillsetRepo.GetAll().ToListAsync();
            var rsEmpSkillSet = await _employeeskillsetRepo.GetAllListAsync();
            var rsLocation = await _locationRepo.GetAllListAsync();
            var rejectedString = L("Rejected");
            var rsLeaveRequest = await _leaverequestRepo.GetAllListAsync(t => t.LeaveStatus != rejectedString);
            var rsDcHead = await _dcheadmasterRepo.GetAllListAsync();
            var rsDcGroup = await _dcgroupmasterRepo.GetAllListAsync();
            var rsDcStation = await _dcstationmasterRepo.GetAllListAsync();
            var rsDcShift = await _dcshiftmasterRepo.GetAllListAsync();
            var defaultTime = await _dcstationmasterRepo.FirstOrDefaultAsync(t => t.DefaultTimeFlag == true);
            if (defaultTime == null)
            {
                defaultTime = await _dcstationmasterRepo.FirstOrDefaultAsync(t => t.LocationRefId == input.LocationRefId);
                //if (defaultTime == null)
                //    throw new UserFriendlyException(L("DefaultTimeErr"));
            }


            foreach (var lst in editListDtos)
            {
                lst.DutyChartDate = input.DutyChartDate;

                var per = rsPersonalInformation.FirstOrDefault(p => p.Id.Equals(lst.EmployeeRefId));

                if (per.IsFixedCostCentreInDutyChart == false || (per.IsFixedCostCentreInDutyChart == true && per.DutyFixedUptoDate >= input.DutyChartDate))
                {
                    lst.StationRefId = null;
                    lst.ShiftRefId = null;
                }

                var location = rsLocation.FirstOrDefault(t => t.Id == per.LocationRefId);
                var company = rsCompany.FirstOrDefault(t => t.Id == location.CompanyRefId);
                lst.LocationRefId = location.Id;
                lst.EmployeeRefCode = per.EmployeeCode;
                lst.EmployeeRefName = per.EmployeeName;
                lst.CompanyRefId = location.CompanyRefId;
                lst.UserSerialNumber = per.UserSerialNumber;

                if (per.ActiveStatus == false && per.LastWorkingDate == null)
                {
                    lst.AllottedStatus = L("Absent");
                }
                else if (per.DefaultWeekOff == (int)input.DutyChartDate.DayOfWeek)
                {
                    lst.AllottedStatus = L("WeekOff");
                }
                else if (input.PublicHolidayFlag == true)
                {
                    lst.AllottedStatus = L("PH");
                }
                else if (input.FirstTimeFlag == true)
                {
                    var prevDay = previousDateData.FirstOrDefault(t => t.EmployeeRefId == lst.EmployeeRefId);
                    if (prevDay != null)
                    {
                        List<int> ids = new List<int>(); ;

                        if (prevDay.AllottedStatus.Equals(L("WeekOff")) || prevDay.AllottedStatus.Equals(L("PH")))
                        {
                            ids.Add(prevDay.Id);
                            prevDay = previousDateData.FirstOrDefault(t => t.EmployeeRefId == lst.EmployeeRefId && !ids.Contains(t.Id));
                        }
                        if (prevDay != null)
                        {
                            if (prevDay != null && prevDay.AllottedStatus.Equals(L("WeekOff")) || prevDay.AllottedStatus.Equals(L("PH")))
                            {
                                ids.Add(prevDay.Id);
                                prevDay = previousDateData.FirstOrDefault(t => t.EmployeeRefId == lst.EmployeeRefId && !ids.Contains(t.Id));
                            }
                        }
                        if (prevDay != null)
                        {
                            if (prevDay != null && prevDay.AllottedStatus.Equals(L("WeekOff")) || prevDay.AllottedStatus.Equals(L("PH")))
                            {
                                ids.Add(prevDay.Id);
                                prevDay = previousDateData.FirstOrDefault(t => t.EmployeeRefId == lst.EmployeeRefId && !ids.Contains(t.Id));
                            }
                        }
                        if (prevDay != null)
                        {
                            if (prevDay != null && prevDay.AllottedStatus.Equals(L("WeekOff")) || prevDay.AllottedStatus.Equals(L("PH")))
                            {
                                ids.Add(prevDay.Id);
                                prevDay = previousDateData.FirstOrDefault(t => t.EmployeeRefId == lst.EmployeeRefId && !ids.Contains(t.Id));
                            }
                        }

                        if (prevDay != null)
                        {
                            if (!prevDay.AllottedStatus.Equals(L("Job")))
                            {
                                lst.AllottedStatus = L("Idle");
                            }
                            else
                            {
                                lst.AllottedStatus = L("Job");
                            }
                        }
                        else
                        {
                            lst.AllottedStatus = L("Job");
                        }
                    }
                    else
                    {
                        lst.AllottedStatus = L("Job");
                    }
                }
                else if (input.FirstTimeFlag == false)
                {
                    //var emp = rsPersonalInformation.FirstOrDefault(t => t.Id ==lst.EmployeeRefId);
                    //if (emp==null)
                    //{
                    //    throw new UserFriendlyException(L("Employee") + " ?");
                    //}
                    //if (emp.DefaultSkillSetCode.ToUpper().Equals("OFFICE") && emp.isBillable == false && emp.IsAttendanceRequired==true)
                    //{
                    //    lst.AllottedStatus = L("Absent");
                    //}
                    //else
                    {
                        lst.AllottedStatus = L("Absent");
                        lst.ShiftRefId = null;
                        lst.StationRefId = null;
                    }
                }

                lst.CompanyRefCode = company.Code;

                var skill = rsSkills.FirstOrDefault(t => t.Id == lst.SkillRefId);
                if (skill != null)
                {
                    lst.SkillRefCode = skill.SkillCode;
                }

                var dtcm = defaultTime;
                if (dtcm == null)
                {
                    //throw new UserFriendlyException(lst.EmployeeRefName + " " + L("Time") + " " + L("Issue"));
                }
                if (defaultTime != null && lst.TimeDescription.IsNullOrEmpty())
                {
                    lst.TimeDescription = defaultTime.TimeDescription;
                    lst.NightFlag = defaultTime.NightFlag;
                }

                var shift = rsDcShift.FirstOrDefault(t => t.Id == lst.ShiftRefId);
                if (shift != null)
                {
                    lst.ShiftRefName = shift.DutyDesc;
                    lst.TimeDescription = shift.TimeDescription;
                    lst.NightFlag = shift.NightFlag;
                }

                var stn = rsDcStation.FirstOrDefault(t => t.Id == lst.StationRefId);
                if (stn != null)
                {
                    var grp = rsDcGroup.FirstOrDefault(t => t.Id == stn.DcGroupRefId);
                    if (grp != null)
                    {
                        var head = rsDcHead.FirstOrDefault(t => t.Id == grp.DcHeadRefId);
                        if (head != null)
                        {
                            lst.DcHeadRefId = head.Id;
                            lst.DcHeadRefName = head.HeadName;
                        }
                        lst.DcGroupRefId = grp.Id;
                        lst.DcGroupRefName = grp.GroupName;
                    }
                    lst.StationRefName = stn.StationName;

                }




                //if (dtcm.SaturdayHalfDay == true && input.DutyChartDate.DayOfWeek == DayOfWeek.Saturday)
                //{
                //    lst.HalfDayFlag = true;
                //    lst.BreakMinutes = 0;
                //    lst.CompanyManPowerBreakMinutes = 0;
                //}
                //else
                {
                    lst.HalfDayFlag = false;
                    lst.BreakMinutes = dtcm.BreakMinutes;
                }


                if (lst.AllottedStatus == L("Absent"))
                {
                    lst.TimeDescription = "";
                    lst.TimeIn = 0;
                    lst.TimeOut = 0;
                    lst.NightFlag = false;
                    lst.BreakMinutes = 0;
                }


            }

            CreateOrUpdateDutyChartInput newDutyChartDto = new CreateOrUpdateDutyChartInput();

            DutyChartEditDto mas = new DutyChartEditDto();
            mas.Id = input.DutyChartRefId;
            mas.DutyChartDate = input.DutyChartDate;
            mas.EmployeeCount = editListDtos.Count();
            mas.LocationRefId = input.LocationRefId;
            mas.Status = "Draft";

            newDutyChartDto.DutyChart = mas;
            newDutyChartDto.DutyChartDetail = editListDtos;
            newDutyChartDto.InternalCallFlag = true;
            await CreateOrUpdateDutyChart(newDutyChartDto);
        }

        public async Task<VerifyDayCloseOutput> VerifyDayClose(VerifyDayCloseDates input)
        {
            VerifyDayCloseOutput output = new VerifyDayCloseOutput();
            DateTime fromDate = input.DateId.AddDays(-1 * input.NoOfDays);
            DateTime minStartDate = new DateTime(2018, 04, 30);
            var dcList = await _dutychartRepo.GetAllListAsync(t => t.LocationRefId == input.LocationRefId && t.DutyChartDate > minStartDate && DbFunctions.TruncateTime(t.DutyChartDate) >= DbFunctions.TruncateTime(fromDate)
                        && DbFunctions.TruncateTime(t.DutyChartDate) < DbFunctions.TruncateTime(input.DateId) && !t.Status.Equals("DayClosed"));
            if (dcList.Count == 0)
            {
                output.SuccessFlag = true;
                return output;
            }

            List<string> errorMessageList = new List<string>();
            foreach (var lst in dcList)
            {
                errorMessageList.Add(L("PlsDayClose", lst.DutyChartDate.ToString("ddd, dd MMM yyy")));
            }
            output.SuccessFlag = false;
            output.ErrorMessageList = errorMessageList;
            return output;

        }

        public virtual async Task<DateInput> DutyChartDayClose(IdInput input)
        {
            #region VerifyLeaveRequestIfStatusIsInLeave
            var detail = await _dutychartdetailRepo.GetAllListAsync(t => t.DutyChartRefId == input.Id);
            foreach (var dto in detail)
            {
                if (dto.AllottedStatus == L("Leave") || dto.AllottedStatus == L("ML") || dto.AllottedStatus == L("Native"))
                {
                    var rejectedString = L("Rejected");
                    var existLeaveRequest = await _leaverequestRepo.FirstOrDefaultAsync(t => t.LeaveStatus != rejectedString &&
                    t.EmployeeRefId == dto.EmployeeRefId && dto.DutyChartDate >= t.LeaveFrom && dto.DutyChartDate <= t.LeaveTo);
                    if (existLeaveRequest == null)
                    {
                        var pi = await _personalInformationRepo.FirstOrDefaultAsync(t => t.Id == dto.EmployeeRefId);
                        throw new UserFriendlyException(L("LeaveRequestNotEntered", pi.EmployeeCode + " " + pi.EmployeeName, dto.DutyChartDate.ToString("dd-MMM-yyyy"), dto.AllottedStatus));
                    }
                }
            }

            #endregion

            #region Verify_Project_Cost_Centre_For_Onsite_Job
            foreach (var dto in detail.Where(t => t.AllottedStatus.Equals(L("Job")) && (t.ShiftRefId == null || t.StationRefId == null)))
            {
                var pi = await _personalInformationRepo.FirstOrDefaultAsync(t => t.Id == dto.EmployeeRefId);
                throw new UserFriendlyException(L("CostCentreNotAssigned", pi.EmployeeCode + " " + pi.EmployeeName, dto.DutyChartDate.ToString("dd-MMM-yyyy"), dto.AllottedStatus));
            }

            #endregion

            var master = await _dutychartRepo.GetAsync(input.Id);
            master.Status = L("DayClosed");


            CheckErrors(await _dutychartManager.CreateSync(master));

            //var rsLeaveTypes = await _leavetypeRepo.GetAllListAsync();
            //string pendingString = L("Pending");
            //var rsLeavePending = await _leaverequestRepo.GetAll()
            //      .Where(t => DbFunctions.TruncateTime(t.LeaveTo) <= DbFunctions.TruncateTime(master.DutyChartDate)
            //      && t.LeaveStatus.Equals(pendingString)).ToListAsync();
            //foreach (var lst in rsLeavePending)
            //{
            //    var lt = rsLeaveTypes.FirstOrDefault(t => t.Id == lst.LeaveTypeRefCode);
            //    if (lt.IsSupportingDocumentsRequired == false)
            //    {
            //        lst.LeaveStatus = L("Approved");
            //    }
            //    else
            //    {
            //        if (lst.IsSupportingDocumentsOverRide == false && !string.IsNullOrEmpty(lst.FileName))
            //        {
            //            lst.LeaveStatus = L("WaitingForDocument");
            //        }
            //        else
            //        {
            //            lst.LeaveStatus = L("Approved");
            //        }
            //    }
            //    await _leaverequestRepo.UpdateAsync(lst);
            //}
            return new DateInput { DateId = master.DutyChartDate };
        }

    }
}