﻿
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Hr.Transaction
{
    public interface ITimeSheetMasterListExcelExporter
    {
        FileDto ExportToFile(List<TimeSheetMasterListDto> dtos);
        //FileDto GenerateExcelFile(SalaryPaidForTheMonth dtos);
        //FileDto TsSubmissionExportToFile(TimeSheetSubmissionDtos dtos);

        FileDto GetDutyChartExcelBasedOnFilter(ExcelDutyChartExporter dtos);
        FileDto ExportParticularLocationAttendanceToFile(List<EmployeeAttendanceVsDutyDto> dtos);
        Task<FileDto> TimesheetReportXlsxToPdf(TimesheetInput input);
        Task<FileDto> SalarySlipExcelToPdfConversion(SalaryPaidForTheMonth dtos);
        Task<FileDto> GenerateSalarySlipExcelFile(SalaryPaidForTheMonth dtos);
    }


}
