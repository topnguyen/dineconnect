﻿
using System.Collections.Generic;

using System.Linq;
using System.Threading.Tasks;
using System;
using Abp.Domain.Repositories;
using Abp.UI;
using System.IO;
using OfficeOpenXml;
using System.Reflection;
using DinePlan.DineConnect.Hr;
using System.Windows.Media.Media3D;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Hr.Master.Dtos;
using Abp.Extensions;

namespace DinePlan.DineConnect.Hr.Transaction.Exporter
{
    public class TimeSheetMasterListExcelExporter : FileExporterBase, ITimeSheetMasterListExcelExporter
    {
        private readonly IRepository<TimeSheetMaster> _timesheetMasterRepo;
        private readonly IRepository<TimeSheetDetail> _timesheetDetailRepo;
        private readonly IRepository<TimeSheetEmployeeDetail> _timesheetEmployeeDetailRepo;
        private readonly IRepository<TimeSheetMaterialUsage> _timesheetMaterialDetailRepo;
        private readonly IRepository<PersonalInformation> _employeeRepo;
        private readonly IRepository<TimeSheetEmployeeDetail> _timesheetemployeeRepo;
        private readonly IRepository<Company> _companyRepo;
        private readonly IRepository<IncentiveTag> _incentiveTagRepo;

        public TimeSheetMasterListExcelExporter(
             IRepository<TimeSheetMaster> timesheetMasterRepo,
              IRepository<TimeSheetDetail> timesheetDetailRepo,
              IRepository<TimeSheetEmployeeDetail> timesheetEmployeeDetailRepo,
              IRepository<TimeSheetMaterialUsage> timesheetMaterialDetailRepo,
                     IRepository<PersonalInformation> employeeRepo,
            IRepository<TimeSheetEmployeeDetail> timesheetemployeeRepo,
            IRepository<Company> companyRepo,
            IRepository<IncentiveTag> incentiveTagRepo
             )
        {
            _timesheetMasterRepo = timesheetMasterRepo;
            _timesheetDetailRepo = timesheetDetailRepo;
            _timesheetEmployeeDetailRepo = timesheetEmployeeDetailRepo;
            _timesheetMaterialDetailRepo = timesheetMaterialDetailRepo;
            _employeeRepo = employeeRepo;
            _timesheetemployeeRepo = timesheetemployeeRepo;
            _companyRepo = companyRepo;
            _incentiveTagRepo = incentiveTagRepo;
        }
        public FileDto ExportToFile(List<TimeSheetMasterListDto> dtos)
        {
            return CreateExcelPackage(
                "TimeSheetMasterList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("TimeSheetMaster"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id
                        );

                    for (var i = 1; i <= 10; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
        public  FileDto GenerateExcelFile(SalaryPaidForTheMonth dtos)
        {
            string templatePath = System.AppDomain.CurrentDomain.BaseDirectory;
            templatePath = templatePath + @"Common\\Report\\SalarySingapore";
            templatePath = templatePath + @"\\SalarySlip.xlsx";
            //                string templateFileName = templatePath;


            #region FilePathDefinition

            string subPath = "\\DocumentFiles\\";

            var phyPath = Path.GetFullPath(subPath);

            bool exists = Directory.Exists((phyPath));

            if (!exists)
            {
                System.IO.Directory.CreateDirectory(phyPath);
            }

            subPath = "\\DocumentFiles\\" + "\\Reports\\";

            phyPath = Path.GetFullPath(subPath);

            exists = Directory.Exists((phyPath));

            if (!exists)
            {
                System.IO.Directory.CreateDirectory(phyPath);
            }

            subPath = "\\DocumentFiles\\" + "\\Reports\\SalarySingapore";

            phyPath = Path.GetFullPath(subPath);

            exists = Directory.Exists((phyPath));

            if (!exists)
            {
                System.IO.Directory.CreateDirectory(phyPath);
            }
            //templatePath = phyPath;

            subPath = "\\DocumentFiles\\" + "\\Reports\\SalarySingapore\\" /*+ input.EmployeeRefId*/;

            phyPath = Path.GetFullPath(subPath);

            exists = Directory.Exists((phyPath));

            if (!exists)
            {
                System.IO.Directory.CreateDirectory(phyPath);
            }
            #endregion

            string templateFileName = templatePath;

            string excelLocation = phyPath + "\\" + dtos.PersonalInformation.EmployeeCode+ dtos.PersonalInformation.EmployeeName+dtos.InputData.StartDate.ToString("dd MMM yyyy")+dtos.InputData.EndDate.ToString("dd MMM yyyy") + ".xlsx";
            string outputPdfLocation = phyPath + "\\" + dtos.PersonalInformation.EmployeeCode + dtos.PersonalInformation.EmployeeName + dtos.InputData.StartDate.ToString("dd MMM yyyy") + dtos.InputData.EndDate.ToString("dd MMM yyyy") + ".pdf";

            FileInfo fileSaveAs = new FileInfo(excelLocation);

            FileInfo file = new FileInfo(templateFileName);
            ExcelPackage excelPackage;
            try
            {
                excelPackage = new ExcelPackage(file);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("File Open Error" + file.FullName + " " + ex.Message);
                throw new UserFriendlyException(ex.Message);
            }
            try
            {
                ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                ExcelWorksheet sheet = excelWorkBook.Worksheets.First();

                #region EXCEL
                #region Header

                ChangeText(sheet, "B1", "<COMPANYNAME>", dtos.SalaryInfo.CompanyRefName);
                ChangeText(sheet, "B2", "<ADDRESS1>", dtos.PersonalInformation.PerAddress1);
                ChangeText(sheet, "B2", "<ADDRESS2>", dtos.PersonalInformation.PerAddress2);
                ChangeText(sheet, "B3", "<ADDRESS3>", dtos.PersonalInformation.PerAddress3);
                //ChangeText(sheet, "B3", "<CITYNAME>", dtos.PersonalInformation.);
                ChangeText(sheet, "B4", "<COUNTRYNAME>", "");
                //ChangeText(sheet, "C9", "<DEPARTMENT>", company.Name);
                //ChangeText(sheet, "C10", "<JOBTITLE>", dtos.PersonalInformation.JobTitle);
                //ChangeText(sheet, "C11", "<PAYBASIS>", company.Name);
                //ChangeText(sheet, "C12", "<PAYPERIOD>", company.Name);
                //ChangeText(sheet, "C13", "<PAYMODE>", company.Name);
                ChangeText(sheet, "G9", "<EMPLOYEENAME>", dtos.PersonalInformation.EmployeeName);
                ChangeText(sheet, "G10", "<EMPLOYEECODE>", dtos.PersonalInformation.EmployeeCode);
                ChangeText(sheet, "J10", "<PASSPORT>", dtos.PersonalInformation.PassPortNumber);
                ChangeText(sheet, "G11", "<BANKNAME>", dtos.PersonalInformation.BankName);
                ChangeText(sheet, "G12", "<ACCOUNTNO>", dtos.PersonalInformation.AccountNumber);
                ChangeText(sheet, "G13", "<CPFACCOUNTNO>", dtos.PersonalInformation.CPFAccountNumber);


                #endregion


                #endregion


            }
            catch (Exception ex)
            {
                MethodBase m = MethodBase.GetCurrentMethod();
                string innerExceptionMessage = ex.InnerException != null ? ex.InnerException.Message : "";
                throw new UserFriendlyException(" Method : " + m.ReflectedType.Name + " : " + m.Name + " " + ex.Message + " InnerException : " + innerExceptionMessage);
            }

            //try
            //{
            //    #region Save_FileName_InDb_MpiReport
            //    var rsMas = await _salary.GetAsync(input.MpiReportRefId);
            //    rsMas.FileName = dbsaveFileName;
            //    await _mpiMasterRepo.UpdateAsync(rsMas);
            //}
            //catch (Exception ex)
            //{
            //    MethodBase m = MethodBase.GetCurrentMethod();
            //    string innerExceptionMessage = ex.InnerException != null ? ex.InnerException.Message : "";
            //    throw new UserFriendlyException("Method : " + m.ReflectedType.Name + " : " + m.Name + " " + ex.Message + " " + innerExceptionMessage);
            //}

            //#endregion

            //GC.Collect();
            //GC.WaitForPendingFinalizers();
            //GC.Collect();
            //GC.WaitForPendingFinalizers();

            //GC.Collect();
            //GC.WaitForPendingFinalizers();
            //GC.Collect();
            //GC.WaitForPendingFinalizers();
            return null;

        }

        public async Task<FileDto> GenerateSalarySlipExcelFile(SalaryPaidForTheMonth dtos)
        {
            var company = await _companyRepo.FirstOrDefaultAsync(t => t.Id == dtos.PersonalInformation.CompanyRefId);
            string templatePath = System.AppDomain.CurrentDomain.BaseDirectory;
            templatePath = templatePath + @"Common\\Report\\SalarySingapore";
            templatePath = templatePath + @"\\SalarySlip.xlsx";

            if (dtos.TenantName.IsNullOrEmpty() || dtos.TenantName.IsNullOrWhiteSpace())
            {
                throw new UserFriendlyException("Tenant Name is Wrong");
            }
            //                string templateFileName = templatePath;


            #region FilePathDefinition

            string subPath = "\\DocumentFiles\\";

            var phyPath = Path.GetFullPath(subPath);

            bool exists = Directory.Exists((phyPath));

            if (!exists)
            {
                System.IO.Directory.CreateDirectory(phyPath);
            }

            subPath = "\\DocumentFiles\\" + "\\Reports\\";

            phyPath = Path.GetFullPath(subPath);

            exists = Directory.Exists((phyPath));

            if (!exists)
            {
                System.IO.Directory.CreateDirectory(phyPath);
            }

            subPath = "\\DocumentFiles\\" + "\\Reports\\SalarySingapore";

            phyPath = Path.GetFullPath(subPath);

            exists = Directory.Exists((phyPath));

            if (!exists)
            {
                System.IO.Directory.CreateDirectory(phyPath);
            }
            //templatePath = phyPath;

            subPath = "\\DocumentFiles\\" + "\\Reports\\" + dtos.TenantName + "\\" + dtos.InputData.EndDate.ToString("MMMyyyy") + "\\" + dtos.PersonalInformation.Id;

            phyPath = Path.GetFullPath(subPath);

            exists = Directory.Exists((phyPath));

            if (!exists)
            {
                System.IO.Directory.CreateDirectory(phyPath);
            }
            //string subPath = "\\DocumentFiles\\" + dtos.TenantName + "\\SalaryPreparation\\" + dtos.InputData.EndDate.ToString("MMMyyyy") + "";

            //string phyPath = Path.GetFullPath(subPath);

            //bool exists = Directory.Exists((phyPath));

            //if (!exists)
            //{
            //    System.IO.Directory.CreateDirectory(phyPath);
            //}
            #endregion

            string templateFileName = templatePath;

            string excelLocation = phyPath + "\\" + dtos.PersonalInformation.EmployeeCode + "_" + dtos.PersonalInformation.EmployeeName +/* dtos.InputData.EndDate.ToString("MMMyyyy") +*/ ".xlsx";
            string outputPdfLocation = phyPath + "\\" + dtos.PersonalInformation.EmployeeCode + "_" + dtos.PersonalInformation.EmployeeName +/* dtos.InputData.EndDate.ToString("MMMyyyy") + */".pdf";

            FileInfo fileSaveAs = new FileInfo(excelLocation);

            FileInfo file = new FileInfo(templateFileName);
            ExcelPackage excelPackage;
            try
            {
                excelPackage = new ExcelPackage(file);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("File Open Error" + file.FullName + " " + ex.Message);
                throw new UserFriendlyException(ex.Message);
            }
            //try
            {
                ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                ExcelWorksheet sheet = excelWorkBook.Worksheets.First();

                #region EXCEL
                #region Header

                ChangeText(sheet, "B1", "<COMPANYNAME>", company.Name);
                string address1and2 = "";
                if (!company.Address1.IsNullOrEmpty())
                    address1and2 = company.Address1;

                if (!company.Address2.IsNullOrEmpty())
                    address1and2 = address1and2 + (address1and2.IsNullOrEmpty() == true ? "" : " , ") + company.Address2;

                string address3 = "";
                if (!company.Address3.IsNullOrEmpty())
                    address3 = company.Address3;

                if (!company.City.IsNullOrEmpty())
                    address3 = address3 + (address3.IsNullOrEmpty() == true ? "" : " , ") + company.City;

                if (!company.PostalCode.IsNullOrEmpty())
                    address3 = address3 + (address3.IsNullOrEmpty() == true ? "" : " , ") + company.PostalCode;

                ChangeText(sheet, "B2", "<ADDRESS1,ADDRESS2>", address1and2);
                ChangeText(sheet, "B3", "<ADDRESS3,CITYNAME><PINCODE>", address3);

                string paidDateString = dtos.InputData.EndDate.AddDays(1).ToString("dd-MMM-yyyy");
                ChangeText(sheet, "I6", "<DATE>", paidDateString);

                string salaryPaidRange = dtos.InputData.StartDate.ToString("dd-MMM-yyyy") + " - " + dtos.InputData.EndDate.ToString("dd-MMM-yyyy");
                ChangeText(sheet, "B8", "<SALARYPERIOD>", salaryPaidRange);

                ChangeText(sheet, "C9", "<DEPARTMENT>", "");
                ChangeText(sheet, "C10", "<JOBTITLE>", dtos.PersonalInformation.JobTitle);

                #region Pay Basic
                string payBasis = "";
                if (dtos.SalaryInfo.SalaryMode == (int)SalaryCalculationType.DAILY)
                {
                    payBasis = "Daily";
                }
                else if (dtos.SalaryInfo.SalaryMode == (int)SalaryCalculationType.MONTHLY)
                {
                    payBasis = "Monthly";
                }
                else if (dtos.SalaryInfo.SalaryMode == (int)SalaryCalculationType.HOURLY)
                {
                    payBasis = "Hourly";
                }

                ChangeText(sheet, "C11", "<PAYBASIS>", payBasis);
                #endregion

                #region Pay Mode
                string payMode = "";
                if (!dtos.PersonalInformation.BankName.IsNullOrEmpty())
                {
                    payMode = "Bank " + dtos.PersonalInformation.BankName;
                }
                ChangeText(sheet, "C13", "<PAYMODE>", payMode);
                #endregion

                #region Employee Box
                ChangeText(sheet, "G9", "<EMPLOYEENAME>", dtos.PersonalInformation.EmployeeName);
                ChangeText(sheet, "G10", "<EMPLOYEECODE>", dtos.PersonalInformation.EmployeeCode);
                if (!dtos.PersonalInformation.NationalIdentificationNumber.IsNullOrEmpty())
                {
                    ChangeText(sheet, "I10", "<NRIC/PASSPORT>", "ID");
                    ChangeText(sheet, "J10", "<PASSPORT>", dtos.PersonalInformation.NationalIdentificationNumber);
                }
                //else if (!dtos.PersonalInformation.FINNumber.IsNullOrEmpty())
                //{
                //    ChangeText(sheet, "I10", "<NRIC/PASSPORT>", "FIN");
                //    ChangeText(sheet, "J10", "<PASSPORT>", dtos.PersonalInformation.FINNumber);
                //}
                else if (!dtos.PersonalInformation.PassPortNumber.IsNullOrEmpty())
                {
                    ChangeText(sheet, "I10", "<NRIC/PASSPORT>", "PASSPORT");
                    ChangeText(sheet, "J10", "<PASSPORT>", dtos.PersonalInformation.PassPortNumber);
                }

                ChangeText(sheet, "G11", "<BANKNAME>", dtos.PersonalInformation.BankName);
                ChangeText(sheet, "G12", "<ACCOUNTNO>", dtos.PersonalInformation.AccountNumber);
                ChangeText(sheet, "G13", "<CPFACCOUNTNO>", dtos.PersonalInformation.CPFAccountNumber);
                //Byte[] bytes = company.LogoPictureId.Value.ToByteArray();
                //AddImageFromByteWithGivenSize(sheet, 1, 2, bytes, "Picture 9", 10, 50, 0, 128, 128);
                #endregion
                #endregion

                #region Salary Detail

                #region Allowance Side
                int row = 18;
                sheet.Cells[row, 2].Value = "Basic Pay";
                sheet.Cells[row, 4].Value = dtos.BasicPayRemarks;
                sheet.Cells[row, 6].Value = dtos.BasicPay;
                row++;
                foreach (var list in dtos.AllowanceSalaryTagList)
                {
                    sheet.Cells[row, 2].Value = list.SalaryRefName;
                    sheet.Cells[row, 4].Value = list.Remarks;
                    sheet.Cells[row, 6].Value = list.AmountCalculatedForSalaryProcess;
                    row++;
                }
                foreach (var list in dtos.IncentiveConsolidatedList)
                {
                    sheet.Cells[row, 2].Value = list.IncentiveTagRefCode;
                    sheet.Cells[row, 4].Value = list.Remarks;
                    sheet.Cells[row, 6].Value = list.TotalIncentiveAmount;
                    row++;
                }
                #endregion

                #region Deduction Side
                row = 18;
                foreach (var list in dtos.DeductionSalaryTagList)
                {
                    sheet.Cells[row, 7].Value = list.SalaryRefName;
                    sheet.Cells[row, 9].Value = list.Remarks;
                    sheet.Cells[row, 11].Value = list.AmountCalculatedForSalaryProcess;
                    row++;
                }
                foreach (var list in dtos.StatutoryDeductionTagList)
                {
                    sheet.Cells[row, 7].Value = list.SalaryRefName;
                    sheet.Cells[row, 9].Value = list.Remarks;
                    sheet.Cells[row, 11].Value = list.AmountCalculatedForSalaryProcess;
                    row++;
                }
                #endregion

                #region Total Allowance and Deduction
                row = 39;
                sheet.Cells[row, 6].Value = dtos.GrossSalaryAmount;
                sheet.Cells[row, 11].Value = dtos.GrossDeduction;
                row++;
                sheet.Cells[row, 11].Value = dtos.NetSalaryAmount;
                #endregion

                #endregion

                #region Salary Footer For SDL Note
                var incTag = await _incentiveTagRepo.FirstOrDefaultAsync(t => t.FormulaRefId.HasValue && t.FormulaRefId == (int)SpecialFormulaTagReference.SG_SDL);
                if (incTag != null)
                {
                    var sdl = dtos.StatutoryAllowanceTagList.FirstOrDefault(t => t.SalaryRefId == incTag.Id);
                    if (sdl != null)
                        AddText(sheet, "K42", sdl.AmountCalculatedForSalaryProcess);
                }
                #endregion

                #region Salary Footer For Employer Contribution
                incTag = await _incentiveTagRepo.FirstOrDefaultAsync(t => t.FormulaRefId.HasValue && t.FormulaRefId == (int)SpecialFormulaTagReference.SG_Employer_CPF);
                if (incTag != null)
                {
                    var emrPf = dtos.StatutoryAllowanceTagList.FirstOrDefault(t => t.SalaryRefId == incTag.Id);
                    if (emrPf != null)
                        AddText(sheet, "F42", emrPf.AmountCalculatedForSalaryProcess);
                }
                #endregion

                #endregion

                #region Logo_Handling
                var employeeCompanyRefId = dtos.PersonalInformation.CompanyRefId;
                var companies = await _companyRepo.GetAllListAsync();
                foreach (var com in companies)
                {
                    try
                    {
                        if (com.Id != employeeCompanyRefId)
                        {
                            sheet.Drawings.Remove(com.Code);
                        }
                    }
                    catch (Exception)
                    {
                        int tt = 15;
                    }
                }
                #endregion
            }
            //catch (Exception ex)
            //{
            //    MethodBase m = MethodBase.GetCurrentMethod();
            //    string innerExceptionMessage = ex.InnerException != null ? ex.InnerException.Message : "";
            //    throw new UserFriendlyException(" Method : " + m.ReflectedType.Name + " : " + m.Name + " " + ex.Message + " InnerException : " + innerExceptionMessage);
            //}
            excelPackage.SaveAs(fileSaveAs);
            excelPackage = null;
            string excel = phyPath + "\\" + dtos.PersonalInformation.EmployeeCode + "_" + dtos.PersonalInformation.EmployeeName /*+ dtos.InputData.StartDate.ToString("dd MMM yyyy") + dtos.InputData.EndDate.ToString("dd MMM yyyy")*/ + ".xlsx";
            string output = phyPath + "\\" + dtos.PersonalInformation.EmployeeCode + "_" + dtos.PersonalInformation.EmployeeName /*+ dtos.InputData.StartDate.ToString("dd MMM yyyy") + dtos.InputData.EndDate.ToString("dd MMM yyyy") */+ ".pdf";
            ExportWorkbookToPdf(excel, output);
            //try
            //{
            //    #region Save_FileName_InDb_MpiReport
            //    var rsMas = await _salary.GetAsync(input.MpiReportRefId);
            //    rsMas.FileName = dbsaveFileName;
            //    await _mpiMasterRepo.UpdateAsync(rsMas);
            //}
            //catch (Exception ex)
            //{
            //    MethodBase m = MethodBase.GetCurrentMethod();
            //    string innerExceptionMessage = ex.InnerException != null ? ex.InnerException.Message : "";
            //    throw new UserFriendlyException("Method : " + m.ReflectedType.Name + " : " + m.Name + " " + ex.Message + " " + innerExceptionMessage);
            //}

            //#endregion

            //GC.Collect();
            //GC.WaitForPendingFinalizers();
            //GC.Collect();
            //GC.WaitForPendingFinalizers();

            //GC.Collect();
            //GC.WaitForPendingFinalizers();
            //GC.Collect();
            //GC.WaitForPendingFinalizers();
            return null;

        }
        public async Task<FileDto> SalarySlipExcelToPdfConversion(SalaryPaidForTheMonth dtos)
        {
            var excel = await GenerateSalarySlipExcelFile(dtos);

            //#region FilePathDefinition

            //string subPath = "\\DocumentFiles\\";

            //var phyPath = Path.GetFullPath(subPath);

            //bool exists = Directory.Exists((phyPath));

            //if (!exists)
            //{
            //    System.IO.Directory.CreateDirectory(phyPath);
            //}

            //subPath = "\\DocumentFiles\\" + "\\Reports\\";

            //phyPath = Path.GetFullPath(subPath);

            //exists = Directory.Exists((phyPath));

            //if (!exists)
            //{
            //    System.IO.Directory.CreateDirectory(phyPath);
            //}

            //subPath = "\\DocumentFiles\\" + "\\Reports\\SalarySingapore";

            //phyPath = Path.GetFullPath(subPath);

            //exists = Directory.Exists((phyPath));

            //if (!exists)
            //{
            //    System.IO.Directory.CreateDirectory(phyPath);
            //}

            ////string subPath = "\\DocumentFiles\\" + dtos.TenantName + "\\SalaryPreparation\\" + dtos.InputData.EndDate.ToString("MMMyyyy") + "";
            ////string phyPath = Path.GetFullPath(subPath);

            ////bool exists = Directory.Exists((phyPath));

            ////if (!exists)
            ////{
            ////    System.IO.Directory.CreateDirectory(phyPath);
            ////}
            //#endregion

            //string excelLocation = phyPath + "\\" + dtos.PersonalInformation.EmployeeCode + "_" + dtos.PersonalInformation.EmployeeName /*+ dtos.InputData.StartDate.ToString("dd MMM yyyy") + dtos.InputData.EndDate.ToString("dd MMM yyyy")*/ + ".xlsx";
            //string outputPdfLocation = phyPath + "\\" + dtos.PersonalInformation.EmployeeCode + "_" + dtos.PersonalInformation.EmployeeName /*+ dtos.InputData.StartDate.ToString("dd MMM yyyy") + dtos.InputData.EndDate.ToString("dd MMM yyyy") */+ ".pdf";
            //ExportWorkbookToPdf(excelLocation, outputPdfLocation);
            return null;
        }

        public FileDto ExportParticularLocationAttendanceToFile(List<EmployeeAttendanceVsDutyDto> dtos)
        {
            return CreateExcelPackage(
                "KeppelVerificationAttendance.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Keppel"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Code"),
                        L("Employee"),
                        L("Date"),
                        L("ClientId"),
                        L("ClientName"),
                        L("Status"),
                        L("Error"),
                        L("ErrorMessage")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.EmployeeRefCode,
                        _ => _.EmployeeRefName,
                        _ => _.DutyChartDate.ToString("dd-MMM-yyyy"),
                        _ => _.DutyClientRefId,
                        _ => _.DutyClientRefName,
                        _ => _.AllottedStatus,
                        _ => _.ErrorFlag == true ? "Error" : "",
                        _ => _.ErrorFlag == true ? _.ErrorMessage : ""
                        );

                    for (var i = 1; i <= 10; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }

          public FileDto GetDutyChartExcelBasedOnFilter(ExcelDutyChartExporter input)
        {
            var dtos = input.result;
            return CreateExcelPackage(
                "DutyChart.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("DutyChart"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Company"),
                        L("Employee"),
                        L("Skill")
                        );
                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.CompanyRefCode,
                        _ => _.EmployeeRefName,
                        _ => _.DefaultSkillSetCode
                        );

                    for (var i = 1; i <= 11; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }

        public async Task<string> ExportWorkbookToPdf(string workbookPath, string outputPath)
        {
            // If either required string is null or empty, stop and bail out
            if (string.IsNullOrEmpty(workbookPath) || string.IsNullOrEmpty(outputPath))
            {
                return null;
            }

            // Create COM Objects
            Microsoft.Office.Interop.Excel.Application excelApplication;
            Microsoft.Office.Interop.Excel.Workbook excelWorkbook;

            // Create new instance of Excel
            excelApplication = new Microsoft.Office.Interop.Excel.Application();

            // Make the process invisible to the user
            excelApplication.ScreenUpdating = false;

            // Make the process silent
            excelApplication.DisplayAlerts = false;

            // Open the workbook that you wish to export to PDF
            excelWorkbook = excelApplication.Workbooks.Open(workbookPath);

            // If the workbook failed to open, stop, clean up, and bail out
            if (excelWorkbook == null)
            {
                excelApplication.Quit();

                excelApplication = null;
                excelWorkbook = null;

                return null;
            }

            var exportSuccessful = true;
            try
            {
                // Call Excel's native export function (valid in Office 2007 and Office 2010, AFAIK)
                excelWorkbook.ExportAsFixedFormat(Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypePDF, outputPath);
            }
            catch (System.Exception ex)
            {
                // Mark the export as failed for the return value...
                exportSuccessful = false;

                // Do something with any exceptions here, if you wish...
                // MessageBox.Show...        
            }
            finally
            {
                // Close the workbook, quit the Excel, and clean up regardless of the results...
                excelWorkbook.Close();
                excelApplication.Quit();

                excelApplication = null;
                excelWorkbook = null;
            }

            // You can use the following method to automatically open the PDF after export if you wish
            // Make sure that the file actually exists first...
            if (System.IO.File.Exists(outputPath))
            {
                System.Diagnostics.Process.Start(outputPath);
            }

            return outputPath;

        }

        public async Task<FileDto> TimesheetReportXlsxToPdf(TimesheetInput input)
        {
            //try
            {
                var mpiReportId = input.TimesheetRefId;
                //GetDetailsFromCostCentre
                string templatePath;
                #region FilePathDefinition
                var mpiReport = await _timesheetMasterRepo.FirstOrDefaultAsync(t => t.Id == input.TimesheetRefId);
                if (mpiReport == null)
                {
                    throw new UserFriendlyException(L("ReportIdDoesNotExist", L("MPI"), input.TimesheetRefId));
                }

                string subPath = "\\DocumentFiles\\" + input.TenantName;

                var phyPath = Path.GetFullPath(subPath);

                bool exists = Directory.Exists((phyPath));//System.IO.Server.MapPath

                if (!exists)
                {
                    System.IO.Directory.CreateDirectory(phyPath);
                }

                subPath = "\\DocumentFiles\\" + input.TenantName + "\\Reports\\";

                phyPath = Path.GetFullPath(subPath);

                exists = Directory.Exists((phyPath));//System.IO.Server.MapPath

                if (!exists)
                {
                    System.IO.Directory.CreateDirectory(phyPath);
                }

                subPath = "\\DocumentFiles\\" + input.TenantName + "\\Reports\\TimeSheet";

                phyPath = Path.GetFullPath(subPath);

                exists = Directory.Exists((phyPath));//System.IO.Server.MapPath

                if (!exists)
                {
                    System.IO.Directory.CreateDirectory(phyPath);
                }
                templatePath = phyPath;

                subPath = "\\DocumentFiles\\" + input.TenantName + "\\Reports\\TimeSheet\\" + input.TimesheetRefId;

                phyPath = Path.GetFullPath(subPath);

                exists = Directory.Exists((phyPath));//System.IO.Server.MapPath

                if (!exists)
                {
                    System.IO.Directory.CreateDirectory(phyPath);
                }
                #endregion

                string excelLocation = phyPath + "\\" + mpiReport.FileName + ".xlsx";
                string outputPdfLocation = phyPath + "\\" + mpiReport.FileName + ".pdf";
                await ExportWorkbookToPdf(excelLocation, outputPdfLocation);

                //string newpath = Directory.GetCurrentDirectory();
                //string fileToCopy = excelLocation;// "c:\\myFolder\\myFile.txt";
                //string destinationDirectory = newpath;// "c:\\myDestinationFolder\\";
                //string destinationFile = destinationDirectory + Path.GetFileName(fileToCopy);

                //GC.Collect();
                //GC.WaitForPendingFinalizers();
                //GC.Collect();
                //GC.WaitForPendingFinalizers();

                //try
                //{
                //    string sp = "~//UploadedFiles"; //+ mpiReport.Id;
                //    var phyUpPath = Path.GetFullPath(sp);
                //    bool upexists = Directory.Exists((phyUpPath));
                //    if (!upexists)
                //    {
                //        System.IO.Directory.CreateDirectory(phyPath);
                //    }
                //    destinationFile = HttpContext.Current.Server.MapPath(sp) + "\\" + Path.GetFileName(fileToCopy);
                //    File.Copy(fileToCopy, destinationFile, true);
                //}
                //catch (Exception ex)
                //{
                //    Console.WriteLine(ex.StackTrace);
                //    throw new UserFriendlyException(destinationFile + " " + " " + "File Copy " + ex.Message);
                //}

                //GC.Collect();
                //GC.WaitForPendingFinalizers();
                //GC.Collect();
                //GC.WaitForPendingFinalizers();

                //string ErrLocation = "";
                //#region ExcelToPdfConversion
                //try
                //{

                //    Microsoft.Office.Interop.Excel.Application excelApplication = new Microsoft.Office.Interop.Excel.Application();
                //    ErrLocation = "App Open";
                //    excelApplication.Visible = false;

                //    ErrLocation = "DisplayAlerts";
                //    excelApplication.DisplayAlerts = false;
                //    excelApplication.ScreenUpdating = false;

                //    ErrLocation = "Open - ";
                //    Microsoft.Office.Interop.Excel.Workbook excelWorkbook = excelApplication.Workbooks.Open(destinationFile, ReadOnly: true, CorruptLoad: true);

                //    if (excelWorkbook == null)
                //    {
                //        excelApplication.Quit();
                //        excelApplication = null;
                //        excelWorkbook = null;
                //        MethodBase m = MethodBase.GetCurrentMethod();
                //        throw new UserFriendlyException(ErrLocation + " Method : " + m.ReflectedType.Name + " : " + m.Name + " " + " Work Book Null ");
                //    }

                //    object misValue = System.Reflection.Missing.Value;
                //    Microsoft.Office.Interop.Excel.XlFixedFormatType paramExportFormat = Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypePDF;
                //    Microsoft.Office.Interop.Excel.XlFixedFormatQuality paramExportQuality = Microsoft.Office.Interop.Excel.XlFixedFormatQuality.xlQualityStandard;
                //    bool paramIncludeDocProps = true;

                //    //xlWorkBook.ExportAsFixedFormat(paramExportFormat, paramExportFilePath, paramExportQuality, paramIncludeDocProps, paramIgnorePrintAreas, 1, 1, paramOpenAfterPublish, misValue);
                //    ErrLocation = "Export As Fixed Format";
                //    //excelWorkbook.ExportAsFixedFormat(paramExportFormat, outputPdfLocation, paramExportQuality, paramIncludeDocProps, paramIgnorePrintAreas,1,1, paramOpenAfterPublish, misValue);
                //    excelWorkbook.ExportAsFixedFormat(paramExportFormat, outputPdfLocation, paramExportQuality, paramIncludeDocProps);
                //    ErrLocation = "Close Work Book";
                //    excelWorkbook.Close();
                //    ErrLocation = "App Close";
                //    excelApplication.Quit();
                //    excelApplication = null;

                //    if (excelWorkbook != null) Marshal.ReleaseComObject(excelWorkbook);
                //    if (excelApplication != null) Marshal.ReleaseComObject(excelApplication);
                //}
                //catch (Exception ex)
                //{
                //    MethodBase m = MethodBase.GetCurrentMethod();
                //    string innerExceptionMessage = ex.InnerException != null ? ex.InnerException.Message : "";
                //    throw new UserFriendlyException(ErrLocation + " Method : " + m.ReflectedType.Name + " : " + m.Name + " " + ex.Message + " InnerException : " + innerExceptionMessage);
                //}
                //#endregion
                //GC.Collect();
                //GC.WaitForPendingFinalizers();
                //GC.Collect();
                //GC.WaitForPendingFinalizers();

                return null;
            }
        }


    }
}