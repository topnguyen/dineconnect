﻿using System.Collections.Generic;

using System.Linq;
using Abp.Domain.Repositories;
using Abp.UI;

using System;
using Abp.Extensions;
using System.Threading.Tasks;
using System.IO;
using OfficeOpenXml;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Web;
using OfficeOpenXml.Drawing.Chart;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Hr;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Hr.Master.Exporter
{
    public class DutyChartListExcelExporter : FileExporterBase, IDutyChartListExcelExporter
    {
        private readonly IRepository<Company> _companyRepo;
        private readonly IRepository<SkillSet> _skillsetRepo;
        private readonly IRepository<PersonalInformation> _personalinformationRepo;

        public DutyChartListExcelExporter(IRepository<Company> companyRepo,
                 IRepository<SkillSet> skillsetRepo,
                 IRepository<PersonalInformation> personalinformationRepo
              )
        {
            _companyRepo = companyRepo;
            _skillsetRepo = skillsetRepo;
            _personalinformationRepo = personalinformationRepo;
        }

        public string ChangeMinutstoTime(int minutesoftheDay)
        {
            if (minutesoftheDay == 0)
            {
                return "";
            }

            double argMinutes = minutesoftheDay;
            int hour = int.Parse(Math.Floor(argMinutes / 60).ToString());
            if (hour > 24)
                hour = hour - 24;

            var min = (argMinutes % 60).ToString().PadLeft(2);
            if (min.Left(1).Equals(" "))
                min = "0" + min.Right(1);
            string hourString = hour >= 10 ? hour.ToString() : "0" + hour;
            string data = hourString + ":" + min;
            return data;
        }

        public async Task<FileDto> DutyChartToExcel(List<OutputExcelDutyChartDto> input)
        {
            var rsPers = await _personalinformationRepo.GetAllListAsync();

            return CreateExcelPackage(
            "DutyChartList" + ".xlsx",
            excelPackage =>
            {
                {
                    int rowCount = 1;
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("ConsolidatedDutyChart"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                  sheet,
                  rowCount,
                  L("#"),
                  L("Date"),
                  L("Count")
                 );
                    rowCount++;
                    int sno = 1;
                    foreach (var lst in input.OrderBy(t => t.DutyChartDate))
                    {
                        AddDetail(sheet, rowCount, 1, sno);
                        AddDetail(sheet, rowCount, 2, lst.DutyChartDate.ToString("dd-MMM-yyyy"));
                        AddDetail(sheet, rowCount, 3, lst.NoOfWorkers);
                        sno++;
                        rowCount++;
                    }

                    for (var i = 1; i <= 10; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                    var myChart = sheet.Drawings.AddChart("Status", eChartType.ColumnStacked);
                    //ExcelChart myChart = sheet.Drawings.AddChart("chart", eChartType.BarStacked3D);

                    var series1 = "B2" + ":" + "B" + rowCount;
                    var series2 = "C2" + ":" + "C" + rowCount;
                    // Define series for the chart
                    myChart.Series.Add(series2, series1);
                    myChart.Border.Fill.Color = System.Drawing.Color.Green;
                    myChart.Title.Text = "Day Vs No Of Employees";
                    myChart.SetSize(900, 700);

                    // Add to 6th row and to the 6th column
                    myChart.SetPosition(1, 0, 4, 0);
                }

                foreach (var group in input.OrderBy(t => t.DutyChartDate))
                {
                    int rowCount = 1;
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("DutyChart") + group.DutyChartDate.ToString("dd-MMM-yyyy"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                    sheet,
                    rowCount,
                    L("#"),
                    L("Date"),
                    //L("Company"),
                    L("Employee"),
                    L("WPNo/NRIC/FIN#"),
                    L("ClientName"),
                    L("CostCentre"),
                    L("Skill")
                 );

                    rowCount++;
                    int sno = 1;
                    foreach (var lst in group.DayDetail.OrderBy(t => t.EmployeeRefId))
                    {
                        var per = rsPers.FirstOrDefault(t => t.Id == lst.EmployeeRefId);
                        string wpnumber = "";
                        if (per.NationalIdentificationNumber != null && per.NationalIdentificationNumber.Length > 0)
                        {
                            wpnumber = per.NationalIdentificationNumber;
                        }
                        else
                        {
                            wpnumber = "";
                        }
                        AddDetail(sheet, rowCount, 1, sno);
                        AddDetail(sheet, rowCount, 2, lst.DutyChartDate.ToString("dd-MMM-yyyy"));
                        // AddDetail(sheet, rowCount, 3, lst.CompanyRefCode);
                        AddDetail(sheet, rowCount, 3, per.EmployeeName);
                        AddDetail(sheet, rowCount, 4, wpnumber);
                        AddDetail(sheet, rowCount, 5, ""/* lst.ClientRefName*/);
                        //AddDetail(sheet, rowCount, 6, lst.ProjectCostCentreRefName);
                        //AddDetail(sheet, rowCount, 7, lst.SkillRefCode);
                        sno++;
                        rowCount++;
                    }
                    for (var i = 1; i <= 20; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                }


            });
        }

        public async Task<FileNameDto> DutyChartConsolidatedCompanyWiseReportForGivenDate(
       List<DutyChartDetailListDto> workList, string tenantName, DateTime dutyChartDate, int exportType, InputExcelDutyChartCompanyWiseDto input)
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
            GC.WaitForPendingFinalizers();

            {
                var rsPersonalInformation = await _personalinformationRepo.GetAllListAsync();

                #region FilePathDefinition

                string subPath = "\\DocumentFiles\\" + tenantName;

                var phyPath = Path.GetFullPath(subPath);

                bool exists = Directory.Exists((phyPath));//System.IO.Server.MapPath

                if (!exists)
                {
                    System.IO.Directory.CreateDirectory(phyPath);
                }

                subPath = "\\DocumentFiles\\" + tenantName + "\\Reports\\";

                phyPath = Path.GetFullPath(subPath);

                exists = Directory.Exists((phyPath));//System.IO.Server.MapPath

                if (!exists)
                {
                    System.IO.Directory.CreateDirectory(phyPath);
                }

                subPath = "\\DocumentFiles\\" + tenantName + "\\Reports\\General";

                phyPath = Path.GetFullPath(subPath);

                exists = Directory.Exists((phyPath));//System.IO.Server.MapPath

                if (!exists)
                {
                    System.IO.Directory.CreateDirectory(phyPath);
                }

                #endregion

                string templatePath = System.AppDomain.CurrentDomain.BaseDirectory;
                templatePath = templatePath + @"Common\\Report\\General";
                templatePath = templatePath + @"\\Consolidated Group Employee List.xlsx";
                string templateFileName = templatePath;

                string dbsaveFileName = @" Consolidated Group Employee List (" + dutyChartDate.ToString("dd-MMM-yyyy )");
                if (input.InchargeEmployeeRefId.HasValue)
                {
                    var emp = rsPersonalInformation.FirstOrDefault(t => t.Id == input.InchargeEmployeeRefId.Value);
                    if (emp != null)
                    {
                        dbsaveFileName = emp.EmployeeName + "-" + dbsaveFileName;
                    }
                }

                if (input.SuperVisortEmployeeRefId.HasValue)
                {
                    var emp = rsPersonalInformation.FirstOrDefault(t => t.Id == input.SuperVisortEmployeeRefId.Value);
                    if (emp != null)
                    {
                        dbsaveFileName = emp.EmployeeName + "-" + dbsaveFileName;
                    }
                }

                string excelLocation = phyPath + "\\" + dbsaveFileName + ".xlsx";
                string outputPdfLocation = phyPath + "\\" + dbsaveFileName + ".pdf";

                FileInfo fileSaveAs = new FileInfo(excelLocation);

                FileInfo file = new FileInfo(templateFileName);
                ExcelPackage excelPackage;
                try
                {
                    excelPackage = new ExcelPackage(file);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException("File Open Error" + file.FullName + " " + ex.Message);
                    throw new UserFriendlyException(ex.Message);
                }



                var rsSkills = await _skillsetRepo.GetAllListAsync();

                ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                ExcelWorksheet sheet = excelWorkBook.Worksheets.First();
                string inspectionDate = dutyChartDate.ToString("dd-MMM-yyyy (ddd)");

                decimal noofSheetsRequired = 1; // input.Count;
                for (int sheetCount = 0; sheetCount < noofSheetsRequired; sheetCount++)
                {
                    if (sheetCount == 0)
                    {
                        sheet.Name = L("Consolidated") + " - " + inspectionDate; //data.CompanyPrefix;
                    }
                    //else
                    //{
                    //    excelWorkBook.Worksheets.Add(data.CompanyPrefix, sheet);
                    //}
                }

                #region FirstSheetDetail



                //foreach (var company in input)
                {
                    int row = 4;
                    int sno = 1;
                    sheet = excelWorkBook.Worksheets.First();
                    ChangeText(sheet, "A1", "<HEADER>", L("Consolidated") + " - " + inspectionDate);
                    //AddText(sheet, "G1", inspectionDate);

                    foreach (var lst in workList)
                    {
                       
                    }

                    workList = workList.OrderBy(t => t.InchargeEmployeeRefId).ThenBy(t => t.SupervisorEmployeeRefId).ToList();

                    #region Detail_Print

                    foreach (var lst in workList)
                    {
                        var skill = rsSkills.FirstOrDefault(t => t.Id == lst.SkillRefId);

                        string clientCode = "";
                        string skillCode = "";

                        if (lst.AllottedStatus.Equals("Job") || lst.AllottedStatus.Equals("Mockup"))
                        {
                            // Do Noting
                        }
                        else
                        {
                            lst.ShiftRefId = null;
                            lst.StationRefId = null;
                            lst.TimeDescription = "";
                        }
                        skillCode = skill.SkillCode;

                       
                        sheet.Cells[row, 1].Value = sno++;
                        sheet.Cells[row, 2].Value = lst.EmployeeRefCode;
                        sheet.Cells[row, 3].Value = lst.EmployeeRefName;
                        sheet.Cells[row, 4].Value = lst.CompanyPrefix;
                        //sheet.Cells[row, 5].Value = lst.ResidentStatus;
                        //sheet.Cells[row, 6].Value = lst.WorkingSector;
                        //sheet.Cells[row, 7].Value = lst.DefaultBillableFlag == true ? L("Billable") : L("Non-Billable");

                        sheet.Cells[row, 10].Value = lst.JobTitle;

                        sheet.Cells[row, 11].Value = skillCode.IsNullOrEmpty() ? lst.ShiftRefId.ToString() : skillCode;

                        sheet.Cells[row, 12].Value = lst.SupervisorEmployeeRefName;
                        sheet.Cells[row, 13].Value = lst.InchargeEmployeeRefName;

                        //sheet.Cells[row, 14].Value = lst.ClientRefName;
                        //sheet.Cells[row, 15].Value = lst.ProjectRefName;
                        //sheet.Cells[row, 16].Value = lst.ClientLocationRefName;
                        //sheet.Cells[row, 17].Value = costCentreCode; // lst.ProjectCostCentreRefName;

                        row++;
                    }
                    row++;

                    #endregion

                    row++;
                    row++;
                    sheet.Cells[row, 2].Value = L("Status");
                    sheet.Cells[row, 2].Style.Font.Bold = true;
                    sheet.Cells[row, 2].Style.Font.UnderLine = true;
                    sheet.Cells[row, 3].Value = L("Count");
                    sheet.Cells[row, 3].Style.Font.Bold = true;
                    sheet.Cells[row, 3].Style.Font.UnderLine = true;
                    sheet.Cells[row, 4].Value = L("Billable");
                    sheet.Cells[row, 4].Style.Font.Bold = true;
                    sheet.Cells[row, 4].Style.Font.UnderLine = true;
                    sheet.Cells[row, 5].Value = L("Non-Billable");
                    sheet.Cells[row, 5].Style.Font.Bold = true;
                    sheet.Cells[row, 5].Style.Font.UnderLine = true;

                    row++;

                    int totalBillableCount = 0;
                    int totalNonBillableCount = 0;

                    foreach (var lst in workList.GroupBy(t => t.AllottedStatus))
                    {
                        sheet.Cells[row, 2].Value = lst.Key;
                        sheet.Cells[row, 3].Value = lst.Count();
                        //int billableCount = lst.Where(t => t.IsBillable == true).Count();
                        //int nonbillableCount = lst.Where(t => t.IsBillable == false).Count();

                        //totalBillableCount = totalBillableCount + billableCount;
                        //totalNonBillableCount = totalNonBillableCount + nonbillableCount;

                        //if (billableCount > 0)
                        //{
                        //    sheet.Cells[row, 4].Value = billableCount;
                        //}
                        //if (nonbillableCount > 0)
                        //{
                        //    sheet.Cells[row, 5].Value = nonbillableCount;
                        //}
                        row++;
                    }
                    row++;
                    sheet.Cells[row, 2].Value = "Total";
                    sheet.Cells[row, 3].Value = totalBillableCount + totalNonBillableCount;
                    sheet.Cells[row, 4].Value = totalBillableCount;
                    sheet.Cells[row, 5].Value = totalNonBillableCount;
                    row++;
                }

                #endregion


                excelPackage.SaveAs(fileSaveAs);

                excelPackage = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();

                string newpath = Directory.GetCurrentDirectory();

                string fileToCopy = excelLocation;// "c:\\myFolder\\myFile.txt";
                string destinationDirectory = newpath;// "c:\\myDestinationFolder\\";
                string destinationFile = destinationDirectory + Path.GetFileName(fileToCopy);

                try
                {
                    //string pathToFiles = Server.MapPath("/UploadedFiles");
                    string sp = "~//UploadedFiles"; //+ mpiReport.Id;
                                                    //bool spexists = System.IO.Directory.Exists(sp);
                    var phyUpPath = Path.GetFullPath(sp);
                    bool upexists = Directory.Exists((phyUpPath));
                    if (!upexists)
                    {
                        System.IO.Directory.CreateDirectory(phyPath);
                    }
                    destinationFile = HttpContext.Current.Server.MapPath(sp) + "\\" + Path.GetFileName(fileToCopy);
                    File.Copy(fileToCopy, destinationFile, true);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                    throw new UserFriendlyException(destinationFile + " " + " " + "File Copy " + ex.Message);
                }

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();

                string ErrLocation = "";
                #region ExcelToPdfConversion
                try
                {

                    Microsoft.Office.Interop.Excel.Application excelApplication = new Microsoft.Office.Interop.Excel.Application();
                    ErrLocation = "App Open";
                    excelApplication.Visible = false;

                    ErrLocation = "DisplayAlerts";
                    excelApplication.DisplayAlerts = false;
                    excelApplication.ScreenUpdating = false;

                    //ErrLocation = "Read Only Open - ";
                    //Microsoft.Office.Interop.Excel.Workbook wkb = app.Workbooks.Open(excelLocation, ReadOnly: true);
                    ErrLocation = "Open - ";
                    Microsoft.Office.Interop.Excel.Workbook excelWorkbook = excelApplication.Workbooks.Open(destinationFile, ReadOnly: true, CorruptLoad: true);

                    if (excelWorkbook == null)
                    {
                        excelApplication.Quit();
                        excelApplication = null;
                        excelWorkbook = null;
                        MethodBase m = MethodBase.GetCurrentMethod();
                        throw new UserFriendlyException(ErrLocation + " Method : " + m.ReflectedType.Name + " : " + m.Name + " " + " Work Book Null ");
                    }

                    object misValue = System.Reflection.Missing.Value;
                    //string paramExportFilePath = @"C:\Test2.pdf";
                    Microsoft.Office.Interop.Excel.XlFixedFormatType paramExportFormat = Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypePDF;
                    Microsoft.Office.Interop.Excel.XlFixedFormatQuality paramExportQuality = Microsoft.Office.Interop.Excel.XlFixedFormatQuality.xlQualityStandard;
                    //bool paramOpenAfterPublish = false;
                    bool paramIncludeDocProps = true;
                    //bool paramIgnorePrintAreas = false;

                    //xlWorkBook.ExportAsFixedFormat(paramExportFormat, paramExportFilePath, paramExportQuality, paramIncludeDocProps, paramIgnorePrintAreas, 1, 1, paramOpenAfterPublish, misValue);
                    ErrLocation = "Export As Fixed Format";
                    //excelWorkbook.ExportAsFixedFormat(paramExportFormat, outputPdfLocation, paramExportQuality, paramIncludeDocProps, paramIgnorePrintAreas,1,1, paramOpenAfterPublish, misValue);
                    excelWorkbook.ExportAsFixedFormat(paramExportFormat, outputPdfLocation, paramExportQuality, paramIncludeDocProps);
                    ErrLocation = "Close Work Book";
                    excelWorkbook.Close();
                    ErrLocation = "App Close";
                    excelApplication.Quit();
                    excelApplication = null;

                    if (excelWorkbook != null) Marshal.ReleaseComObject(excelWorkbook);
                    if (excelApplication != null) Marshal.ReleaseComObject(excelApplication);
                }
                catch (Exception ex)
                {
                    MethodBase m = MethodBase.GetCurrentMethod();
                    //Console.WriteLine("Executing {0}.{1}",
                    //                  m.ReflectedType.Name, m.Name);
                    string innerExceptionMessage = ex.InnerException != null ? ex.InnerException.Message : "";
                    throw new UserFriendlyException(ErrLocation + " Method : " + m.ReflectedType.Name + " : " + m.Name + " " + ex.Message + " InnerException : " + innerExceptionMessage);
                    //Console.WriteLine(ex.StackTrace);
                    //throw new UserFriendlyException(destinationFile + " " + ErrLocation + " " + "Excel To Pdf Conversion " + ex.Message + " Inner Ex : " + ex.InnerException.Message);
                }
                #endregion
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();

                if (exportType == 0)
                    return new FileNameDto { FileName = fileToCopy, FileExtenstionType = "xlsx", FilePath = fileToCopy };
                else
                    return new FileNameDto { FileName = outputPdfLocation, FileExtenstionType = "PDF", FilePath = outputPdfLocation };
            }
        }

        public async Task<FileNameDto> DutyChartCompanyWiseReportForGivenDate(
            List<OutputExcelDutyChartCompanyDto> input, string tenantName, DateTime dutyChartDate, int exportType)
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
            GC.WaitForPendingFinalizers();

            #region FilePathDefinition

            string subPath = "\\DocumentFiles\\" + tenantName;

            var phyPath = Path.GetFullPath(subPath);

            bool exists = Directory.Exists((phyPath));//System.IO.Server.MapPath

            if (!exists)
            {
                System.IO.Directory.CreateDirectory(phyPath);
            }

            subPath = "\\DocumentFiles\\" + tenantName + "\\Reports\\";

            phyPath = Path.GetFullPath(subPath);

            exists = Directory.Exists((phyPath));//System.IO.Server.MapPath

            if (!exists)
            {
                System.IO.Directory.CreateDirectory(phyPath);
            }

            subPath = "\\DocumentFiles\\" + tenantName + "\\Reports\\General";

            phyPath = Path.GetFullPath(subPath);

            exists = Directory.Exists((phyPath));//System.IO.Server.MapPath

            if (!exists)
            {
                System.IO.Directory.CreateDirectory(phyPath);
            }

            #endregion

            string templatePath = System.AppDomain.CurrentDomain.BaseDirectory;
            templatePath = templatePath + @"Common\\Report\\General";
            templatePath = templatePath + @"\\Group Employee List.xlsx";
            string templateFileName = templatePath;

            string dbsaveFileName = @" Group Employee List (" + dutyChartDate.ToString("dd-MMM-yyyy )");
            string excelLocation = phyPath + "\\" + dbsaveFileName + ".xlsx";
            string outputPdfLocation = phyPath + "\\" + dbsaveFileName + ".pdf";

            FileInfo fileSaveAs = new FileInfo(excelLocation);

            FileInfo file = new FileInfo(templateFileName);
            ExcelPackage excelPackage;
            try
            {
                excelPackage = new ExcelPackage(file);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("File Open Error" + file.FullName + " " + ex.Message);
                throw new UserFriendlyException(ex.Message);
            }

            // var rsClients = await _clientRepo.GetAllListAsync();
            var rsPersonalInformation = await _personalinformationRepo.GetAllListAsync();
            var rsSkills = await _skillsetRepo.GetAllListAsync();

            ExcelWorkbook excelWorkBook = excelPackage.Workbook;
            ExcelWorksheet sheet = excelWorkBook.Worksheets.First();

            decimal noofSheetsRequired = input.Count;
            for (int sheetCount = 0; sheetCount < noofSheetsRequired; sheetCount++)
            {
                var data = input[sheetCount];
                if (sheetCount == 0)
                {
                    sheet.Name = data.CompanyRefName;
                }
                else
                {
                    excelWorkBook.Worksheets.Add(data.CompanyRefName, sheet);
                }
            }

            #region FirstSheetDetail
            string inspectionDate = dutyChartDate.ToString("dd-MMM-yyyy (ddd)");

            foreach (var company in input)
            {
                sheet = excelWorkBook.Worksheets[company.CompanyRefName];
                ChangeText(sheet, "A1", "<COMPANYNAME>", company.CompanyRefName);
                AddText(sheet, "G1", inspectionDate);

                int row = 3;
                int sno = 1;

                var companyDetail = company.DayDetail;

                List<EmployeeCostCentreWiseDutyChartDto> detailList = new List<EmployeeCostCentreWiseDutyChartDto>();

                foreach (var lst in company.DayDetail)
                {
                    EmployeeCostCentreWiseDutyChartDto ec = new EmployeeCostCentreWiseDutyChartDto();
                    ec.CompanyRefId = company.CompanyRefId;
                    ec.CompanyRefName = company.CompanyRefName;
                    detailList.Add(ec);
                }

                detailList = detailList.OrderBy(t => t.CompanyRefId).ToList();

                #region Detail_Print
                foreach (var resstatus in detailList)
                {
                    string resStatus = ""  + " (" + resstatus.Detail.Count + ")";
                    AddHeaderWithMergeWithColumnCount(sheet, row, 1, 5, resStatus);
                    row++;
                    foreach (var lst in resstatus.Detail)
                    {
                        var skill = rsSkills.FirstOrDefault(t => t.Id == lst.SkillRefId);

                        string clientCode = "";
                        string skillCode = "";

                        if (lst.AllottedStatus.Equals("Job") || lst.AllottedStatus.Equals("Mockup"))
                        {
                            // Do Noting
                        }
                        else
                        {
                            lst.StationRefId = null;
                            lst.ShiftRefId = null;
                            lst.TimeDescription = "";
                        }

                      
                        }
                        row++;
                    }
                    #endregion

                    row++;
                    row++;
                    sheet.Cells[row, 2].Value = L("Status");
                    sheet.Cells[row, 2].Style.Font.Bold = true;
                    sheet.Cells[row, 2].Style.Font.UnderLine = true;
                    sheet.Cells[row, 3].Value = L("Count");
                    sheet.Cells[row, 3].Style.Font.Bold = true;
                    sheet.Cells[row, 3].Style.Font.UnderLine = true;
                    sheet.Cells[row, 4].Value = L("Billable");
                    sheet.Cells[row, 4].Style.Font.Bold = true;
                    sheet.Cells[row, 4].Style.Font.UnderLine = true;
                    sheet.Cells[row, 5].Value = L("Non-Billable");
                    sheet.Cells[row, 5].Style.Font.Bold = true;
                    sheet.Cells[row, 5].Style.Font.UnderLine = true;

                    row++;

                    int totalBillableCount = 0;
                    int totalNonBillableCount = 0;

                    foreach (var lst in company.DayDetail.GroupBy(t => t.AllottedStatus))
                    {
                        sheet.Cells[row, 2].Value = lst.Key;
                        sheet.Cells[row, 3].Value = lst.Count();
                        //int billableCount = lst.Where(t => t.IsBillable == true).Count();
                        //int nonbillableCount = lst.Where(t => t.IsBillable == false).Count();

                        //totalBillableCount = totalBillableCount + billableCount;
                        //totalNonBillableCount = totalNonBillableCount + nonbillableCount;

                        //if (billableCount > 0)
                        //{
                        //    sheet.Cells[row, 4].Value = billableCount;
                        //}
                        //if (nonbillableCount > 0)
                        //{
                        //    sheet.Cells[row, 5].Value = nonbillableCount;
                        //}
                        row++;
                    }
                    row++;
                    sheet.Cells[row, 2].Value = "Total";
                    sheet.Cells[row, 3].Value = totalBillableCount + totalNonBillableCount;
                    sheet.Cells[row, 4].Value = totalBillableCount;
                    sheet.Cells[row, 5].Value = totalNonBillableCount;
                    row++;
                }

                #endregion


                excelPackage.SaveAs(fileSaveAs);

                excelPackage = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();

                string newpath = Directory.GetCurrentDirectory();

                string fileToCopy = excelLocation;// "c:\\myFolder\\myFile.txt";
                string destinationDirectory = newpath;// "c:\\myDestinationFolder\\";
                string destinationFile = destinationDirectory + Path.GetFileName(fileToCopy);

                try
                {
                    //string pathToFiles = Server.MapPath("/UploadedFiles");
                    string sp = "~//UploadedFiles"; //+ mpiReport.Id;
                                                    //bool spexists = System.IO.Directory.Exists(sp);
                    var phyUpPath = Path.GetFullPath(sp);
                    bool upexists = Directory.Exists((phyUpPath));
                    if (!upexists)
                    {
                        System.IO.Directory.CreateDirectory(phyPath);
                    }
                    destinationFile = HttpContext.Current.Server.MapPath(sp) + "\\" + Path.GetFileName(fileToCopy);
                    File.Copy(fileToCopy, destinationFile, true);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                    throw new UserFriendlyException(destinationFile + " " + " " + "File Copy " + ex.Message);
                }

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();

                string ErrLocation = "";
                #region ExcelToPdfConversion
                try
                {

                    Microsoft.Office.Interop.Excel.Application excelApplication = new Microsoft.Office.Interop.Excel.Application();
                    ErrLocation = "App Open";
                    excelApplication.Visible = false;

                    ErrLocation = "DisplayAlerts";
                    excelApplication.DisplayAlerts = false;
                    excelApplication.ScreenUpdating = false;

                    //ErrLocation = "Read Only Open - ";
                    //Microsoft.Office.Interop.Excel.Workbook wkb = app.Workbooks.Open(excelLocation, ReadOnly: true);
                    ErrLocation = "Open - ";
                    Microsoft.Office.Interop.Excel.Workbook excelWorkbook = excelApplication.Workbooks.Open(destinationFile, ReadOnly: true, CorruptLoad: true);

                    if (excelWorkbook == null)
                    {
                        excelApplication.Quit();
                        excelApplication = null;
                        excelWorkbook = null;
                        MethodBase m = MethodBase.GetCurrentMethod();
                        throw new UserFriendlyException(ErrLocation + " Method : " + m.ReflectedType.Name + " : " + m.Name + " " + " Work Book Null ");
                    }

                    object misValue = System.Reflection.Missing.Value;
                    //string paramExportFilePath = @"C:\Test2.pdf";
                    Microsoft.Office.Interop.Excel.XlFixedFormatType paramExportFormat = Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypePDF;
                    Microsoft.Office.Interop.Excel.XlFixedFormatQuality paramExportQuality = Microsoft.Office.Interop.Excel.XlFixedFormatQuality.xlQualityStandard;
                    //bool paramOpenAfterPublish = false;
                    bool paramIncludeDocProps = true;
                    //bool paramIgnorePrintAreas = false;

                    //xlWorkBook.ExportAsFixedFormat(paramExportFormat, paramExportFilePath, paramExportQuality, paramIncludeDocProps, paramIgnorePrintAreas, 1, 1, paramOpenAfterPublish, misValue);
                    ErrLocation = "Export As Fixed Format";
                    //excelWorkbook.ExportAsFixedFormat(paramExportFormat, outputPdfLocation, paramExportQuality, paramIncludeDocProps, paramIgnorePrintAreas,1,1, paramOpenAfterPublish, misValue);
                    excelWorkbook.ExportAsFixedFormat(paramExportFormat, outputPdfLocation, paramExportQuality, paramIncludeDocProps);
                    ErrLocation = "Close Work Book";
                    excelWorkbook.Close();
                    ErrLocation = "App Close";
                    excelApplication.Quit();
                    excelApplication = null;

                    if (excelWorkbook != null) Marshal.ReleaseComObject(excelWorkbook);
                    if (excelApplication != null) Marshal.ReleaseComObject(excelApplication);
                }
                catch (Exception ex)
                {
                    MethodBase m = MethodBase.GetCurrentMethod();
                    //Console.WriteLine("Executing {0}.{1}",
                    //                  m.ReflectedType.Name, m.Name);
                    string innerExceptionMessage = ex.InnerException != null ? ex.InnerException.Message : "";
                    throw new UserFriendlyException(ErrLocation + " Method : " + m.ReflectedType.Name + " : " + m.Name + " " + ex.Message + " InnerException : " + innerExceptionMessage);
                    //Console.WriteLine(ex.StackTrace);
                    //throw new UserFriendlyException(destinationFile + " " + ErrLocation + " " + "Excel To Pdf Conversion " + ex.Message + " Inner Ex : " + ex.InnerException.Message);
                }
                #endregion
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();

                if (exportType == 0)
                    return new FileNameDto { FileName = fileToCopy, FileExtenstionType = "xlsx", FilePath = fileToCopy };
                else
                    return new FileNameDto { FileName = outputPdfLocation, FileExtenstionType = "PDF", FilePath = outputPdfLocation };

            }

        }
    }
