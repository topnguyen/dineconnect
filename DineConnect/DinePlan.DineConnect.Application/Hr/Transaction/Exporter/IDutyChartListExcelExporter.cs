﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Hr.Transaction.Dtos;

namespace DinePlan.DineConnect.Hr.Master
{
    public interface IDutyChartListExcelExporter
    {
       Task<FileDto> DutyChartToExcel(List<OutputExcelDutyChartDto> input);

        Task<FileNameDto> DutyChartCompanyWiseReportForGivenDate(List<OutputExcelDutyChartCompanyDto> input, string tenantName, DateTime dutyChartDate, int ExportType);

        Task<FileNameDto> DutyChartConsolidatedCompanyWiseReportForGivenDate(List<DutyChartDetailListDto> workList, string tenantName, DateTime dutyChartDate, int ExportType, InputExcelDutyChartCompanyWiseDto input);

    }
}