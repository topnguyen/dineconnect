﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using DinePlan.DineConnect.Hr.Master.Dtos;
using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Transaction.Dtos;

namespace DinePlan.DineConnect.Hr.Transaction.Dtos
{
    [AutoMapFrom(typeof(DutyChart))]
    public class DutyChartListDto : FullAuditedEntityDto
    {
        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }
        public DateTime DutyChartDate { get; set; }
        public string Status { get; set; }

    }
    [AutoMapTo(typeof(DutyChart))]
    public class DutyChartEditDto
    {
        public int? Id { get; set; }
        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }
        public DateTime DutyChartDate { get; set; }
        public string Status { get; set; }
        public int EmployeeCount { get; set; }

        public bool PublicHolidayFlag { get; set; }
    }

    [AutoMapFrom(typeof(DutyChartDetail))]
    public class DutyChartDetailListDto : FullAuditedEntityDto
    {
        public int Sno { get; set; }
        public int DutyChartRefId { get; set; }
        public DateTime DutyChartDate { get; set; }
        public int CompanyRefId { get; set; }
        public string CompanyRefCode { get; set; }
        public string CompanyRefName { get; set; }
        public string CompanyPrefix { get; set; }
        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }
        public decimal UserSerialNumber { get; set; }
        public int EmployeeRefId { get; set; }
        public string EmployeeRefCode { get; set; }
        public string EmployeeRefName { get; set; }
        public string AllottedStatus { get; set; }
        public int DcHeadRefId { get; set; }
        public string DcHeadRefName { get; set; }
        public int DcGroupRefId { get; set; }
        public string DcGroupRefName { get; set; }
        public int? StationRefId { get; set; }
        public string StationRefName { get; set; }
        public int? ShiftRefId { get; set; }
        public string ShiftRefName { get; set; }
        public string TimeDescription { get; set; }
        public int TimeIn { get; set; }
        public int TimeOut { get; set; }
        public int OtTimeIn { get; set; }
        public int OtTimeOut { get; set; }
        public bool NightFlag { get; set; }
        public string Remarks { get; set; }
        public bool IsSetAsDefault { get; set; }
        public int SkillRefId { get; set; }
        public string SkillRefCode { get; set; }

        public List<SkillSetEditDto> Skills;
        public virtual Guid? ProfilePictureId { get; set; }

        public bool ShouldBeDeleted { get; set; }

        public decimal StatusCount { get; set; }

        public List<AttendanceTimeWithLocation> AttendanceTimeList { get; set; }

        public bool HalfDayFlag { get; set; }

        public int BreakMinutes { get; set; }

        public decimal TotalWorkHours { get; set; }
        public decimal TotalOt1Hours { get; set; }
        public decimal TotalOt2Hours { get; set; }
        public decimal TotalAutoManualOt1Hours { get; set; }
        public decimal TotalAutoManualOt2Hours { get; set; }
        public List<EmployeeVsIncentiveListDto> IncentiveList { get; set; }

        public string Month => DutyChartDate.ToString("MMM-yy");
        public DateTime MonthYear => new DateTime(DutyChartDate.Year, DutyChartDate.Month, 01);

        public string KnownSkillSetList { get; set; }

        public DateTime DutyStartTime { get; set; }
        public DateTime DutyEndTime { get; set; }

        public bool HalfDayWorkingDayFlag { get; set; }
        public string ResidentStatus { get; set; }
        public string EmployeeCostCentre { get; set; }
        public string WorkStatus { get; set; }
        public int JobTitleRefId { get; set; }
        public string JobTitle { get; set; }
        public int? SupervisorEmployeeRefId { get; set; }
        public string SupervisorEmployeeRefName { get; set; }
        public int? InchargeEmployeeRefId { get; set; }
        public string InchargeEmployeeRefName { get; set; }
        public bool DoesSalaryPayableForThisLeaveRequest { get; set; }
        public bool CompletedStatus { get; set; }
        public bool ManualIncentiveAllowed { get; set; }

        public List<StationDetail> StationDetails { get; set; }
        public List<TimeSheetMasterListDto> TimeSheetMasters { get; set; }

        public List<TimeSheetMasterListDto> EligibleTsForIncentiveCalculation { get; set; }

        public List<TimeSheetEmployeeDetailEditDto> EligibleTsEmployeeDetail = new List<TimeSheetEmployeeDetailEditDto>();
        public List<TimeSheetDetailEditDto> EligibleTsDetailForIncentiveCalculation = new List<TimeSheetDetailEditDto>();

        public List<TimeSheetMaterialUsageEditDto> EligibleTsMaterialUsage = new List<TimeSheetMaterialUsageEditDto>();
        public decimal ActualWorkHours { get; set; }
        public string ActualWorkHoursWithMinutes { get; set; }
        public decimal ShortageWorkHours { get; set; }
        public string ShortageWorkHoursInMinutes { get; set; }
    }

    [AutoMapTo(typeof(DutyChartDetail))]
    public class DutyChartDetailEditDto
    {
        public int? Id { get; set; }
        public int DutyChartRefId { get; set; }
        public int Sno { get; set; }
        public DateTime DutyChartDate { get; set; }
        public int CompanyRefId { get; set; }
        public int EmployeeRefId { get; set; }
        public string EmployeeRefCode { get; set; }
        public string AllottedStatus { get; set; }
        public string SkillRefCode { get; set; }
        public int? StationRefId { get; set; }
        public string StationRefName { get; set; }
        public int? ShiftRefId { get; set; }
        public string ShiftRefName { get; set; }
        public bool NightFlag { get; set; }

        public int TimeIn { get; set; }

        public int TimeOut { get; set; }

        public int OtTimeIn { get; set; }

        public int OtTimeOut { get; set; }

        public bool BreakFlag { get; set; }

        public int? BreakOut { get; set; }

        public int? BreakIn { get; set; }
        public int BreakMinutes { get; set; }

        public string Remarks { get; set; }

        public bool HalfDayFlag { get; set; }

        public virtual Guid? ProfilePictureId { get; set; }

        public int? SupervisorEmployeeRefId { get; set; }
        public int? InchargeEmployeeRefId { get; set; }
    }

    public class DutyChartTreeDetail : IOutputDto
    {
        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }

        public int DutyChartRefId { get; set; }
        public DateTime DutyChartDate { get; set; }
        public int CompanyRefId { get; set; }
        public string CompanyRefCode { get; set; }
        public decimal UserSerialNumber { get; set; }
        public int EmployeeRefId { get; set; }
        public string EmployeeRefCode { get; set; }
        public string EmployeeRefName { get; set; }
        public string AllottedStatus { get; set; }
        public int SkillRefId { get; set; }
        public string SkillRefCode { get; set; }
        public int? StationRefId { get; set; }
        public string StationRefName { get; set; }
        public int? ShiftRefId { get; set; }
        public string ShiftRefName { get; set; }
        public bool NightFlag { get; set; }

        public int TimeIn { get; set; }

        public int TimeOut { get; set; }

        public int OtTimeIn { get; set; }

        public int OtTimeOut { get; set; }

        public bool BreakFlag { get; set; }

        public int? BreakOut { get; set; }

        public int? BreakIn { get; set; }
        public int BreakMinutes { get; set; }

        public List<SkillSetEditDto> Skills;
        public int NumberOfLocations { get; set; }
        public string LocationRemarks { get; set; }

        public string MultipleSkillRemarks { get; set; }
        public string MultipleTimeDescriptionRemarks { get; set; }
        public string TimeDescription { get; set; }

        public bool HalfDayFlag { get; set; }

        public virtual Guid? ProfilePictureId { get; set; }
        public bool IsSetAsDefault { get; set; }
        public List<DutyChartDetailListDto> DutyDetails { get; set; }

        public int? SupervisorEmployeeRefId { get; set; }
        public string SupervisorEmployeeRefName { get; set; }
        public int? InchargeEmployeeRefId { get; set; }
        public string InchargeEmployeeRefName { get; set; }
        public bool DoesSalaryPayableForThisLeaveRequest { get; set; }
    }


    public class AttendanceTimeWithLocation
    {
        public string AttLocation { get; set; }
        public List<DateTime> AttTime { get; set; }

    }

    public class GetDutyChartInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    public class GetDutyChartForEditOutput : IOutputDto
    {
        public DutyChartEditDto DutyChart { get; set; }
        public List<DutyChartTreeDetail> DutyChartDetail { get; set; }
        public string SaveStatus { get; set; }
    }
    public class CreateOrUpdateDutyChartInput : IInputDto
    {
        public int LocationRefId { get; set; }
        [Required]
        public DutyChartEditDto DutyChart { get; set; }
        public List<DutyChartDetailListDto> DutyChartDetail { get; set; }
        public string SaveStatus { get; set; }

        public bool InternalCallFlag { get; set; }

    }
    public class ChangeDutyChartStatusInputDto : IInputDto
    {
        public int EmployeeRefId { get; set; }
        public int DetailId { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
    }

    public class MaxDateWithStatus
    {
        public DateTime MaxDate { get; set; }
        public bool SuccessFlag { get; set; }
        public List<DateWiseStatus> DateWiseStatusList { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class VerifyDayCloseOutput : IOutputDto
    {
        public bool SuccessFlag { get; set; }
        public List<string> ErrorMessageList { get; set; }
    }

    public class DateWiseStatus
    {
        public DateTime DutychartDate { get; set; }
        public string Status { get; set; }
    }

    [AutoMapFrom(typeof(DutyChartDetail))]
    public class SimpleDutyChartDetailListDto
    {
        public int DutyChartRefId { get; set; }
        public int CompanyRefId { get; set; }
        public int EmployeeRefId { get; set; }
        public string AllottedStatus { get; set; }
    }


    public class InputExcelDutyChartDto : IInputDto
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public List<ComboboxItemDto> ClientList { get; set; }
    }

    public class OutputExcelDutyChartDto : IOutputDto
    {
        public DateTime DutyChartDate { get; set; }
        public int NoOfWorkers { get; set; }
        public List<DutyChartDetailListDto> DayDetail { get; set; }
    }

    public class InputExcelDutyChartCompanyWiseDto : IInputDto
    {
        public DateTime DutyChartDate { get; set; }

        public string TenantName { get; set; }

        public int ExportType { get; set; }
        public int? SuperVisortEmployeeRefId { get; set; }
        public int? InchargeEmployeeRefId { get; set; }

    }

    public class OutputExcelDutyChartCompanyDto : IOutputDto
    {
        public DateTime DutyChartDate { get; set; }
        public int CompanyRefId { get; set; }
        public string CompanyRefName { get; set; }
        public int NoOfWorkers { get; set; }

        public List<DutyChartDetailListDto> DayDetail { get; set; }
    }

    public class EmployeeCostCentreWiseDutyChartDto
    {
        public int CompanyRefId { get; set; }
        public string CompanyRefName { get; set; }
        
        public List<DutyChartDetailListDto> Detail { get; set; }
    }


    public class CalendarEvent
    {
        public int EventId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string WorkStatus { get; set; }
        public string StationRefName { get; set; }
        public  string ShiftRefName { get; set; }
        public string TsCurrentStatus { get; set; }
        public string TsNgClass { get; set; }
        public DateTime? StartAt { get; set; }
        public DateTime? EndAt { get; set; }
        public bool IsFullDay { get; set; }
        public string BackGroundColor { get; set; }
        public string ForeColor { get; set; }
        public TsCalendarStatus WorkInfo { get; set; }

    }

    public class TsCalendarStatus
    {
        public string AllotedStatus { get; set; }
        public int DcHeadRefId { get; set; }
        public string DcHeadRefName { get; set; }
        public int DcGroupRefId { get; set; }
        public string DcGroupRefName { get; set; }
        public int? StationRefId { get; set; }
        public string StationRefName { get; set; }
        public int? ShiftRefId { get; set; }
        public string ShiftRefName { get; set; }
        public string TimeDescription { get; set; }
        public string WorkTime { get; set; }
        public string OtTime { get; set; }
        public string Jobremarks { get; set; }
    }

    public class DutyChartStatusDto
    {
        public string DutyChartIdleStatus { get; set; }
        public string DutyChartStatus { get; set; }
    }

    public class EmployeeInput : IInputDto
    {
        public int EmployeeRefId { get; set; }
    }

    public class GetLatLong
    {
        public DateTime Startdate { get; set; }
        public DateTime Enddate { get; set; }
        public int EmployeeRefId { get; set; }
    }

    public class FileNameDto : IInputDto
    {
        public string FileName { get; set; }
        public string FileExtenstionType { get; set; }
        public string FilePath { get; set; }
    }

    public class EmployeeConsolidatedInformation
    {
        public int EmployeeCount { get; set; }

        public List<EmployeeSkillWiseCount> SkillWiseCount;

    }
    public class EmployeeSkillWiseCount
    {
        public string Skill { get; set; }
        public int Count { get; set; }
    }

    public class GetHrOperationHealthDto : IInputDto
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? EmployeeRefId { get; set; }
        public bool EmailErrorShown { get; set; }
        public bool MailSendFlag { get; set; }
        public int? DaysTill { get; set; }
        public int? InchargeEmployeeRefId { get; set; }
    }

    public class GetStatusAlertMailDto : IInputDto
    {
        public DateTime DutyChartDate { get; set; }
        public int EmployeeRefId { get; set; }
        public bool EmailErrorShown { get; set; }
        public bool MailSendFlag { get; set; }
        public string JobStatus { get; set; }
    }
    public class GetDutyChartForGivenDate : IInputDto
    {
        public int LocationRefId { get; set; }
        public DateTime DutyChartDate { get; set; }
    }


    public class FillDutyChart : IInputDto
    {
        public int LocationRefId { get; set; }
        public DateTime DutyChartDate { get; set; }
        public int? DutyChartRefId { get; set; }
        public bool FirstTimeFlag { get; set; }
        public bool PublicHolidayFlag { get; set; }

    }

    public class StationDetail
    {
        public string AllotedStatus { get; set; }
        public string TsNgClass { get; set; }
        public int DcHeadRefId { get; set; }
        public string DcHeadRefName { get; set; }
        public int DcGroupRefId { get; set; }
        public string DcGroupRefName { get; set; }
        public int? StationRefId { get; set; }
        public string StationRefName { get; set; }
        public int? ShiftRefId { get; set; }
        public string ShiftRefName { get; set; }
        public string TimeDescription { get; set; }
        public decimal TotalManHours { get; set; }
        public DateTime? WorkStartAt { get; set; }
        public DateTime? WorkEndAt { get; set; }
        public string WorkTime { get; set; }
        public string OtTime { get; set; }
        public decimal TotalHours { get; set; }
        public decimal TotalOverTimeHours { get; set; }
        public string Jobremarks { get; set; }
    }
}

