﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using System;
using System.Collections.Generic;
using DinePlan.DineConnect.Hr;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Hr.Transaction.Dtos;

namespace DinePlan.DineConnect.Hr.Transaction.Dtos
{
    [AutoMapFrom(typeof(TimeSheetMaster))]
    public class TimeSheetMasterListDto : FullAuditedEntityDto
    {
        public int CompanyRefId { get; set; }

        public int AcYear { get; set; }

        public string TsMode { get; set; }    //  D   ,   W   , M

        public long ManualTsNumber { get; set; }

        public int ProjectCostCentreRefId { get; set; }
        public string ProjectCostCentreRefName { get; set; }


        public string SkillRefCode { get; set; }

        public int? ClientRefId { get; set; }
        public string ClientRefName { get; set; }

        public int? ProjectRefId { get; set; }

        public int? ClientLocationRefId { get; set; }

        public int NdtProcedureRefId { get; set; }
        public string NdtProcedureAbbreviation { get; set; }

        public DateTime TsDateFrom { get; set; }
        public DateTime TsDateTo { get; set; }
        public decimal TotalManHours { get; set; }

        public decimal TotalHours { get; set; }
        public decimal TotalOverTimeHours { get; set; }


        public int TsPreparedEmployeeRefId { get; set; }
        public DateTime? PreparedDate { get; set; }
        public string ApprovedClientRepName { get; set; }
        public string ApprovedClientDesignation { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public DateTime? SubmissionDate { get; set; }
        public string TsStatus { get; set; }        //  P - Pending, C - Completed
        public DateTime? SubmissionDateToInvoiceDepartment { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public string SaleInvoiceReferenceNumber { get; set; }

        public string CurrentStatus { get; set; }

        public List<TimeSheetDetailEditDto> TimeSheetDetail { get; set; }
        public string TsNgClass { get; set; }
        public DateTime? WorkStartAt { get; set; }
        public DateTime? WorkEndAt { get; set; }
        public string WorkTime { get; set; }
        public string OtTime { get; set; }
        public int NoOfDaysInTheGivenMonth { get; set; }
        public string FileName { get; set; }
        public string FileExtenstionType { get; set; }
        public bool TsUploaded => FileName == null || FileName.Equals("") ? false : true;
        public string Jobremarks { get; set; }
        public string RtSourceType { get; set; }
        public string MainCode { get; set; }
        public string ProjectNumber { get; set; }
        public string Module { get; set; }
        public string Location { get; set; }
        public string WoNumber { get; set; }
        public string PoNumber { get; set; }
        public string JVRNumber { get; set; }
        public bool DoesThisAttachmentSupply { get; set; }
    }

    public class HourWiseDutyStatus
    {
        public List<PieChartData> TimeWiseWorkStatus { get; set; }
        public decimal Ot1Hours { get; set; }
        public decimal Ot2Hours { get; set; }
    }

    [AutoMapTo(typeof(TimeSheetMaster))]
    public class TimeSheetMasterEditDto
    {

        public int? Id { get; set; }
        public int CompanyRefId { get; set; }

        public int AcYear { get; set; }

        public string TsMode { get; set; }    //  D   ,   W   , M

        public long ManualTsNumber { get; set; }

        public int ProjectCostCentreRefId { get; set; }

        public string ProjectCostCentreRefCode { get; set; }

        public int? ClientRefId { get; set; }

        public int? ProjectRefId { get; set; }

        public int? ClientLocationRefId { get; set; }

        [Required]
        public int NdtProcedureRefId { get; set; }

        public DateTime TsDateFrom { get; set; }

        public DateTime TsDateTo { get; set; }

        public decimal TotalManHours { get; set; }

        public decimal TotalOverTimeHours { get; set; }

        public int TsPreparedEmployeeRefId { get; set; }

        public DateTime? PreparedDate { get; set; }

        public string ApprovedClientRepName { get; set; }

        public string ApprovedClientDesignation { get; set; }

        public DateTime? ApprovedDate { get; set; }
        public DateTime? SubmissionDate { get; set; }

        public string TsStatus { get; set; }        //  P - Pending, C - Completed

        public DateTime? SubmissionDateToInvoiceDepartment { get; set; }

        public DateTime? InvoiceDate { get; set; }

        public string SaleInvoiceReferenceNumber { get; set; }
        public string FileName { get; set; }
        public string FileExtenstionType { get; set; }
        public string RtSourceType { get; set; }
        public bool TsUploaded => FileName == null || FileName.Equals("") ? false : true;
        public string MainCode { get; set; }
        public string ProjectNumber { get; set; }
        public string Module { get; set; }
        public string Location { get; set; }
        public string WoNumber { get; set; }
        public string PoNumber { get; set; }
        public string JVRNumber { get; set; }
        public bool DoesThisAttachmentSupply { get; set; }
        
    }
  


    [AutoMapFrom(typeof(TimeSheetDetail))]
    public class TimeSheetDetailEditDto : FullAuditedEntityDto
    {
        public int TsMasterRefId { get; set; }
        public DateTime TsDate { get; set; }
        public string WorkDescription { get; set; }
        public int FromHour { get; set; }
        public int ToHour { get; set; }
        public int BreakOutHour { get; set; }
        public int BreakInHour { get; set; }

        public decimal TotalHours { get; set; }

        public int OverTimeFromHour { get; set; }

        public int OverTimeToHour { get; set; }

        public decimal TotalOverTimeHours { get; set; }

        public string Remarks { get; set; }

        public List<TimeSheetEmployeeDetailEditDto> TimeSheetEmployeeDetail { get; set; }

        public List<TimeSheetMaterialUsageEditDto> TimeSheetMaterialUsage { get; set; }

        public int RevisedOverTimeToHour { get; set; }
    }


    [AutoMapFrom(typeof(TimeSheetEmployeeDetail))]
    public class TimeSheetEmployeeDetailEditDto : FullAuditedEntityDto
    {
        public int TsMasterRefId { get; set; }

        public int TsDetailRefId { get; set; }

        public int EmployeeRefId { get; set; }

        public string EmployeeRefName { get; set; }
        public int? WorkPositionRefId { get; set; }

    }

    [AutoMapFrom(typeof(TimeSheetMaterialUsage))]
    public class TimeSheetMaterialUsageEditDto : FullAuditedEntityDto
    {
        public int TsMasterRefId { get; set; }

        public int TsDetailRefId { get; set; }

        public int MaterialRefId { get; set; }

        public string MaterialRefName { get; set; }

        public string MaterialTypeRefName { get; set; }

        public decimal QuantityUsed { get; set; }

        public int UnitRefId { get; set; }
        public string UnitRefName { get; set; }

        public decimal MinimumQuantity { get; set; }

        [Required]
        public decimal MinimumWastageQuantity { get; set; }

        [Required]
        public bool IsMandatory { get; set; }

        public bool IncentiveFlag { get; set; }
    }

    public class TimeSheetViewDto : FullAuditedEntityDto
    {
        public int NdtProcedureRefId { get; set; }
        public string NdtProcedureRefName { get; set; }

        public int CompanyRefId { get; set; }
        public string CompanyPrefix { get; set; }
        public int TsMasterRefId { get; set; }

        public int TsDetailRefId { get; set; }

        public int EmployeeRefId { get; set; }

        public string EmployeeRefName { get; set; }

        public long ManualTsNumber { get; set; }

        public DateTime TsDate { get; set; }

        public string ProjectCostCentreRefCode { get; set; }

        public int FromHour { get; set; }

        public string FromHourString { get; set; }

        public int ToHour { get; set; }
        public string ToHourString { get; set; }

        public int BreakOutHour { get; set; }

        public int BreakInHour { get; set; }

        public decimal TotalHours { get; set; }

        public int OverTimeFromHour { get; set; }

        public string OvertTimeFromHourString { get; set; }

        public int OverTimeToHour { get; set; }
        public string OverTimeToHourString { get; set; }

        public decimal TotalOverTimeHours { get; set; }

        public DateTime? SubmissionDate { get; set; }
        public string TsStatus { get; set; }        //  P - Pending, C - Completed
        public DateTime? SubmissionDateToInvoiceDepartment { get; set; }

        public DateTime? InvoiceDate { get; set; }
        public string FileName { get; set; }
        public string FileExtenstionType { get; set; }
        public string RtSourceType { get; set; }
        public bool TsUploaded => FileName == null || FileName.Equals("") ? false : true;
    }


    public class TimeSheetAlreadyExist : IOutputDto
    {
        public bool ExistFlag { get; set; }
        public string ErrorMessage { get; set; }
        public long ManualTsNumber { get; set; }
        public int TimeSheetRefId { get; set; }

    }

    [AutoMapFrom(typeof(TimeSheetMaster))]
    public class TsSubmissionDto : FullAuditedEntityDto
    {

        public int AcYear { get; set; }

        public string TsMode { get; set; }    //  D   ,   W   , M

        public long ManualTsNumber { get; set; }

        public int CompanyRefId { get; set; }
        public string CompanyRefName { get; set; }

        public int ProjectCostCentreRefId { get; set; }
        public string ProjectCostCentreRefName { get; set; }

        public string SkillRefCode { get; set; }

        public int? ClientRefId { get; set; }

        public string ClientRefName { get; set; }

        public int? ProjectRefId { get; set; }

        public int? ClientLocationRefId { get; set; }

        public int NdtProcedureRefId { get; set; }

        public string NdtProcedureRefName { get; set; }

        public DateTime TsDateFrom { get; set; }

        public DateTime TsDateTo { get; set; }

        public decimal TotalManHours { get; set; }

        public decimal TotalOverTimeHours { get; set; }

        public int TsPreparedEmployeeRefId { get; set; }

        public DateTime? PreparedDate { get; set; }

        public string ApprovedClientRepName { get; set; }

        public string ApprovedClientDesignation { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public DateTime? SubmissionDate { get; set; }
        public string TsStatus { get; set; }        //  P - Pending, C - Completed
        public DateTime? SubmissionDateToInvoiceDepartment { get; set; }

        public DateTime? InvoiceDate { get; set; }

        public string SaleInvoiceReferenceNumber { get; set; }

        public List<TimeSheetEmployeeDetailEditDto> TsEmployeeDetail { get; set; }

        public string Remarks { get; set; }
        public string FileName { get; set; }
        public string FileExtenstionType { get; set; }
        public bool TsUploaded => FileName == null || FileName.Equals("") ? false : true;
    }

    public class TsUpdateSubmissionDto
    {
        public int Id { get; set; }
        public DateTime? SubmissionDate { get; set; }
        public DateTime? SubmissionDateToInvoiceDepartment { get; set; }
    }

    public class InputTimeSheetNumberWithAcYear : IInputDto
    {
        public int CompanyRefId { get; set; }
        public int AcYear { get; set; }
        public long ManualTsNumber { get; set; }

    }

    public class WhetherTimeSheetExistForEmployeeOnGivenDateInputDto : IInputDto
    {
        public int EmployeeRefId { get; set; }
        public int SkillSetRefId { get; set; }
        public string SkillSetRefCode { get; set; }
        public int NdtProcedureRefId { get; set; }
        public DateTime DutyChartDate { get; set; }
        public int ClientRefId { get; set; }
        public int ProjectCostCentreRefId { get; set; }
    }

    public class TimeSheetAlreadyExistForEmployeeOnGivenDateInputDto : IInputDto
    {
        public List<EmployeeWithSkillSetViewDto> EmployeeList { get; set; }
        public int TimeSheetRefId { get; set; }
        public DateTime DutyChartDate { get; set; }
    }

    public class LinkWithTimeSheetInputDto : IInputDto
    {
        public int EmployeeRefId { get; set; }
        public DateTime DutyChartDate { get; set; }
        public int TimeSheetRefId { get; set; }
    }

    public class GetTimeSheetMasterInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public bool IsCalledForPendingCountOnly { get; set; }
        public bool IsDetailRequired { get; set; }
        public bool JobDateRangeFlag { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string EmployeeRefName { get; set; }
        public int? ManualTsNumber { get; set; }
        public string ManaulTsLists { get; set; }
        public int ProjectCostCentreRefId { get; set; }
        public int SkillRefId { get; set; }
        public string SkillRefCode { get; set; }
        public bool YellowPending { get; set; }
        public bool PinkPending { get; set; }
        public bool InvoicePending { get; set; }
        public bool InvoiceFinished { get; set; }
        public string Filter { get; set; }
        public int? EmployeeRefId { get; set; }
        public int? ClientRefId { get; set; }
        public string Operation { get; set; }
        public bool ShowPendingFlag { get; set; }

        public bool DueDateRangeFlag { get; set; }
        public DateTime? DueStartDate { get; set; }
        public DateTime? DueEndDate { get; set; }

        public List<ComboboxItemDto> ClientList { get; set; }
        public List<SimplePersonalInformationListDto> EmployeeList { get; set; }
        public List<CompanyListDto> Companies { get; set; }
        public bool IsDateWiseEmployeeStatusRequired { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime Desc";
            }
        }
    }
    public class GetTimeSheetMasterForEditOutput : IOutputDto
    {
        public TimeSheetMasterEditDto TimeSheetMaster { get; set; }
        public List<TimeSheetDetailEditDto> TimeSheetDetail { get; set; }
        public List<TimeSheetEmployeeDetailEditDto> EmployeeWorkPosition { get; set; }
        public int[] EmployeeIdList { get; set; }
    }

    public class CreateOrUpdateTimeSheetMasterInput : IInputDto
    {

        public TimeSheetMasterEditDto TimeSheetMaster { get; set; }
        public List<TimeSheetDetailEditDto> TimeSheetDetail { get; set; }

        public bool OverRideEmployeeTimeDetails { get; set; }
        public string TsSubmissionStatus { get; set; }
        public bool CloseAllDutyChartForTheClient { get; set; }

        public bool AuthorisationForChangeInDutyChart { get; set; }
        public bool SameClientSomeMoreTimeSheetPending { get; set; }
    }

    public class MonthWiseTimeSheetData : IOutputDto
    {
        public string MonthStartDate { get; set; }
        public int StatusPending { get; set; }
        public int YellowTimeSheetPending { get; set; }
        public int PinkTimeSheetPending { get; set; }
        public int InvoicePending { get; set; }
        public int InvoiceDone { get; set; }
    }

    public class DayWiseTimeSheetData : IOutputDto
    {
        public DateTime DateToBeDisplayed { get; set; }
        public string DayDesc { get; set; }
        public int StatusPending { get; set; }
        public int YellowTimeSheetPending { get; set; }
        public int PinkTimeSheetPending { get; set; }
        public int InvoicePending { get; set; }
        public int InvoiceDone { get; set; }
    }

    public class GetAssetUsageInputDto : IInputDto
    {
        public int AssetRefId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }

    public class GetTsDashBoardDto : IOutputDto
    {
        public List<MonthWiseTimeSheetData> chartData;
        public string Header { get; set; }
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string MonthStartDate { get; set; }
        public int StatusPending { get; set; }
        public int YellowTimeSheetPending { get; set; }
        public int PinkTimeSheetPending { get; set; }
        public int InvoicePending { get; set; }
        public int InvoiceDone { get; set; }

        public List<EmployeeWorkStatusDatesDto> EmployeeUnknownList = new List<EmployeeWorkStatusDatesDto>();

        public List<EmployeeWorkStatusDatesDto> EmployeeYellowList = new List<EmployeeWorkStatusDatesDto>();

        public List<EmployeeWorkStatusDatesDto> EmployeePinkList = new List<EmployeeWorkStatusDatesDto>();

        public List<EmployeeWorkStatusDatesDto> EmployeeInvoicePendingList = new List<EmployeeWorkStatusDatesDto>();

        public List<EmployeeWorkStatusDatesDto> EmployeeInvoiceDoneList = new List<EmployeeWorkStatusDatesDto>();

    }

    public class EmployeeWorkStatusDatesDto : IOutputDto
    {
        public int CompanyRefId { get; set; }
        public string CompanyRefCode { get; set; }

        public int EmployeeRefId { get; set; }

        public string EmployeeRefName { get; set; }

        public string DefaultSkillSetCode { get; set; }
        public string EmployeeSkills { get; set; }

        //public List<SkillSetEditDto> Skills;

        public string WorkStatus { get; set; }

        public DateTime SelectedDate { get; set; }

        public int NoOfDays { get; set; }

        public List<DateTime> StatusDates { get; set; }

        public bool RemarksFlag { get; set; }
        public string Remarks { get; set; }

    }

    public class GetEmployeeTsDashBoardDto : IOutputDto
    {
        public List<DayWiseTimeSheetData> chartData;
        public MonthWiseTimeSheetData OverAll;
    }

    public class TimeSheetSubmissionDtos : IOutputDto
    {
        public List<TsSubmissionDto> TimeSheetDetails { get; set; }
        public List<DutyChartDetailListDto> TimeSheetUnknownPendingDetails { get; set; }
        public List<ClientWisePending> ClientWisePendingList { get; set; }
        public List<ProjectCostCentreWisePending> ProjectCostCentreWisePendingList { get; set; }
        public List<SkillWisePending> SkillWisePendingList { get; set; }
        public List<EmployeeWisePending> EmployeeWisePendingList { get; set; }
        public List<TsSubmissionDto> TsNumberWiseDetails { get; set; }
        public List<DateTime> WorkDateList { get; set; }
        public List<EmployeeWiseTimeSheetStatus> EmployeeWiseTimeSheetStatus { get; set; }
        public string DateRangeString { get; set; }
        public int TsPendingCount { get; set; }
    }


    public class EmployeeWiseTimeSheetStatus
    {
        public int CompanyRefId { get; set; }
        public int EmployeeRefId { get; set; }
        public CompanyEditDto Company { get; set; }
        public PersonalInformationListDto Employee { get; set; }
        public string Remarks { get; set; }
        public bool DoesTheEmployeeIsBillable { get; set; }
        public int BillableCount { get; set; }
        public int OtherJobCount { get; set; }
        public int IdleCount { get; set; }
        public int LeaveCount { get; set; }
        public int AbsentCount { get; set; }
        public int UnknownCount { get; set; }
        public int PendingCount { get; set; }
        public int InvoicePendingCount { get; set; }
        public int InvoicedCount { get; set; }

        public int DayInOutExistsCount { get; set; }

        public int DayAtleastOneGpsExistsCount { get; set; }

        public int DayWorkHoursTalliedCount { get; set; }

        public int DayGpsMissingCount { get; set; }

        public List<DutyChartTimeSheetStatus> DutyChartTimeSheetStatus { get; set; }
    }

    public class DutyChartTimeSheetStatus
    {
        public DateTime DutyChartDate { get; set; }
        public bool IsBillable { get; set; }
        public string DateStatus { get; set; }
        public bool DoesDayInOutExists { get; set; }

        public bool DoesAtleastOneGpsExists { get; set; }

        public bool DoesDayWorkHoursTallied { get; set; }
        public bool DoesGpsMissing { get; set; }

        public DateTime? InTime { get; set; }
        public DateTime? OutTime { get; set; }
        public string Remarks { get; set; }

        public decimal ActualWorkHours { get; set; }

        public List<DutyChartDetailListDto> DutyChartList { get; set; }
        public List<TimeSheetMasterEditDto> TimeSheetMasterList { get; set; }
    }


    public class EmployeeDateWiseTimeSheetSubmissionDtos : IOutputDto
    {
        public List<DateTime> WorkDateList { get; set; }
        public List<EmployeeWiseTimeSheetStatus> EmployeeWiseTimeSheetStatus { get; set; }
        public string DateRangeString { get; set; }
    }

    public class ClientWisePending : IOutputDto
    {
        public int ClientRefId { get; set; }
        public string ClientRefName { get; set; }
        public string ClientRefCode { get; set; }
        public int PendingCount { get; set; }
        public bool ShowDetailFlag { get; set; }
        public List<DutyChartDetailListDto> Details { get; set; }
    }
    public class PendingTagWisePending : IOutputDto
    {
        public int? PendingTagRefId { get; set; }
        public string PendingTagCode { get; set; }
        public int PendingCount { get; set; }
        public bool ShowDetailFlag { get; set; }
        public List<DutyChartDetailListDto> Details { get; set; }
    }

    public class ProjectCostCentreWisePending : IOutputDto
    {
        public int ProjectCostCentreRefId { get; set; }
        public string ProjectCostCentreRefName { get; set; }
        public int PendingCount { get; set; }
        public bool ShowDetailFlag { get; set; }
        public List<DutyChartDetailListDto> Details { get; set; }
    }

    public class SkillWisePending : IOutputDto
    {
        public int SkillRefId { get; set; }
        public string SkillRefCode { get; set; }
        public int PendingCount { get; set; }
        public bool ShowDetailFlag { get; set; }
        public List<DutyChartDetailListDto> Details { get; set; }
    }

    public class DateWisePending : IOutputDto
    {
        public DateTime InspectionDate { get; set; }
        public int PendingCount { get; set; }
        public bool ShowDetailFlag { get; set; }
        public List<DutyChartDetailListDto> Details { get; set; }
    }
    public class EmployeeWisePending : IOutputDto
    {
        public int EmployeeRefId { get; set; }
        public string EmployeeRefCode { get; set; }
        public string EmployeeRefName { get; set; }
        
        public int PendingCount { get; set; }
        public bool ShowDetailFlag { get; set; }
        public List<DutyChartDetailListDto> Details { get; set; }
    }
    public class TimeSheetEnteredEmployeeWithCount
    {
        public long? UserId { get; set; }
        public string UserName { get; set; }
        public int EmployeeRefId { get; set; }
        public string EmployeeRefName { get; set; }
        public int TimeSheetEnteredCount { get; set; }
        public int PendingTimeSheetUpload { get; set; }
        public double AvgTimeSheetEnteredCount { get; set; }
        public int DetailCount { get; set; }
        public double AvgDetailCount { get; set; }
        public int ManDaysCount { get; set; }
        public double AvgManDaysCount { get; set; }


    }

    public class ExcelDutyChartExporter : IOutputDto
    {
        public string Header { get; set; }

        public List<EmployeeWorkStatusDatesDto> result { get; set; }
    }

    public class TimeSheetDocumentDto : IInputDto
    {
        public int TsMasterId { get; set; }
        //public int DocumentInfoRefId { get; set; }
        //public string Data { get; set; }
        public string TenantName { get; set; }
    }

    public class CompanyWiseClientWiseTimeSheetPending : IOutputDto
    {
        public int CompanyRefId { get; set; }
        public string CompanyRefName { get; set; }
        public int ClientRefId { get; set; }
        public string ClientRefName { get; set; }
        public int PendingCount { get; set; }
        public bool ShowDetailFlag { get; set; }
        public bool IsRelatedCompany { get; set; }
        public AgeingAnalysis PendingTimeSheets { get; set; }
        public AgeingAnalysis InvoicePendingTimeSheets { get; set; }
        public AgeingAnalysis InvoiceDoneTimeSheets { get; set; }
    }

    public class AgeingAnalysis
    {
        public int OverAllPending { get; set; }
        public int Days90over { get; set; }
        public int Days61to90 { get; set; }
        public int Days31to60 { get; set; }
        public int Days16to30 { get; set; }
        public int Days1to15 { get; set; }
    }

    public class ProcedureWisePending : IOutputDto
    {
        public int CompanyRefId { get; set; }
        public string CompanyRefName { get; set; }
        public int ClientRefId { get; set; }
        public string ClientRefName { get; set; }
        public int PendingCount { get; set; }
        public bool ShowDetailFlag { get; set; }
        public bool IsRelatedCompany { get; set; }
        public List<ProcedureAgeingAnalysis> PendingList { get; set; }
    }
    public class ProcedureAgeingAnalysis
    {
        public int CompanyRefId { get; set; }
        public int ProcedureRefId { get; set; }
        public string ProcedureRefName { get; set; }
        public int PendingCount { get; set; }
    }

    public enum WorkPosition
    {
        LEADER = 1,
        ASST = 2,
        NO_INCENTIVE = 3
    }

    public class GetTSPendigInputDto : PagedAndSortedInputDto, IShouldNormalize
    {
        public bool OnlyBillableEmployeeOnly { get; set; }
        public bool IsCalledForPendingCountOnly { get; set; }
        public bool IsDetailRequired { get; set; }
        public bool JobDateRangeFlag { get; set; }
        public bool IsAlreadySubmittedTsStatusRequired { get; set; }
        public bool ShowPendingFlag { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string EmployeeRefName { get; set; }
        public string ManaulTsLists { get; set; }
        public int ProjectCostCentreRefId { get; set; }
        public int SkillRefId { get; set; }
        public string SkillRefCode { get; set; }
        public int? EmployeeRefId { get; set; }
        public int? ClientRefId { get; set; }
        public bool DueDateRangeFlag { get; set; }
        public DateTime? DueStartDate { get; set; }
        public DateTime? DueEndDate { get; set; }

        public List<ComboboxItemDto> ClientList { get; set; }
        public List<SimplePersonalInformationListDto> EmployeeList { get; set; }
        public List<SimplePersonalInformationListDto> SupervisorList { get; set; }
        public List<SimplePersonalInformationListDto> ManagerList { get; set; }
        public List<CompanyListDto> Companies { get; set; }
        public bool IsDateWiseEmployeeStatusRequired { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime Desc";
            }
        }
    }

    public enum MailMessageType
    {
        DueAmountAlert1 =1,
        OverDueAlert2= 2,
        ClientRequestPending3 = 3,
        LeaveRequest4 = 4,
        LeaveApproved5 = 5,
        LeaveRejected6 = 6,
        LeaveDocumentPending7=7,
        ReportScannedCopyMail_8 = 8,
        DailyDutyChartStatus_9 = 9,
        Others_10 = 10
    };

    public class TimesheetInput
    {
        public int TimesheetRefId { get; set; }
        //public int ClientRefId { get; set; }
        public string TenantName { get; set; }
        public string FileExtenstionType { get; set; }

        public bool PdfOutput { get; set; }
    }

    public class EmployeeAttendanceVsDutyDto
    {
        public int EmployeeRefId { get; set; }
        public string EmployeeRefCode { get; set; }
        public string EmployeeRefName { get; set; }
        public int AttClientRefId { get; set; }
        public string AttClientRefName { get; set; }
        public int DutyClientRefId { get; set; }
        public string DutyClientRefName { get; set; }
        public bool ErrorFlag { get; set; }
        public string AllottedStatus { get; set; }
        public string ErrorMessage { get; set; }
        public DateTime DutyChartDate { get; set; }
    }


    public class ApprovedLeaveDateList
    {
        public DateTime WorkDate { get; set; }
        public bool ApprovedLeave { get; set; }
    }

    public class PieChartOutputDto : IOutputDto
    {
        public decimal NoOfActualWorkedDays { get; set; }
        public decimal NoOfPublicHolidays { get; set; }
        public decimal NoOfPaidLeaveDays { get; set; }
        public decimal NoOfAbsentDays { get; set; }
        public decimal NoOfExcessLevyDays { get; set; }
        public decimal SalaryCalculatedDays => NoOfActualWorkedDays + NoOfPaidLeaveDays + NoOfPublicHolidays + IdleDays;
        public List<PieChartData> PieChartData { get; set; }
        public List<PieChartData> TimeWiseWorkStatus { get; set; }
        public List<WorkStatusDaysVsHours> DaysVsHoursWorkStatus { get; set; }
        public string Header { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public List<DutyChartDetailListDto> DutyList { get; set; }
        public List<CalendarEvent> CalendarEvents { get; set; }
        public List<IncentiveConsolidatedDto> IncentiveConsolidatedList { get; set; }
        public decimal OverAllIncentiveAmount { get; set; }
        public List<DutyChartDetailListDto> YellowPendingList = new List<DutyChartDetailListDto>();
        public List<DutyChartDetailListDto> PinkPendingList = new List<DutyChartDetailListDto>();
        public List<DutyChartDetailListDto> IdleList = new List<DutyChartDetailListDto>();


        public List<EmployeeWorkStatusDatesDto> EmployeeIdleList = new List<EmployeeWorkStatusDatesDto>();

        public List<EmployeeWorkStatusDatesDto> EmployeeLeaveList = new List<EmployeeWorkStatusDatesDto>();

        public List<EmployeeWorkStatusDatesDto> EmployeeJobList = new List<EmployeeWorkStatusDatesDto>();

        public List<EmployeeWorkStatusDatesDto> EmployeeAbsentList = new List<EmployeeWorkStatusDatesDto>();

        public List<EmployeeWorkStatusDatesDto> EmployeeOprNotBillableList = new List<EmployeeWorkStatusDatesDto>();

        public List<EmployeeWorkStatusDatesDto> EmployeeOfficeList = new List<EmployeeWorkStatusDatesDto>();

        public List<EmployeeWorkStatusDatesDto> EmployeeSnrMgmtList = new List<EmployeeWorkStatusDatesDto>();

        public List<EmployeeWorkStatusDatesDto> EmployeeTransportList = new List<EmployeeWorkStatusDatesDto>();

        public decimal NoOfWorkHours { get; set; }

        public bool RemarksFlag { get; set; }

        public string Remarks { get; set; }

        public int YellowPending { get; set; }
        public int PinkPending { get; set; }
        public int InvoicePending { get; set; }
        public int InvoiceDone { get; set; }
        public decimal IdleDays { get; set; }

        public string HrOperationHealthText { get; set; }
        public string HrOperationHealthHtml { get; set; }

        public string EmployeeRefCode { get; set; }
        public string CompanyRefCode { get; set; }

        public List<MonthWisePendingDto> MonthWisePendingList = new List<MonthWisePendingDto>();
    }

    public class GetDashBoardDto : IInputDto
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string DateMode { get; set; }    //  Daily, Fortnightly, Monthly, Quarterly, HalfYearly

        public int EmployeeRefId { get; set; }
        public bool IsCalendarEventsRequired { get; set; }

        public DateTime? MaxDayCloseDate { get; set; }
    }

    public class MonthWisePendingDto : IOutputDto
    {
        public DateTime MonthYear { get; set; }
        public string Month { get; set; }
        public int Pending { get; set; }
        public int YellowPending { get; set; }
        public int PinkPending { get; set; }
        public int InvoicePending { get; set; }
        public int InvoiceDone { get; set; }
    }
    public class GetEmployeeAlertMailDto : IInputDto
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int EmployeeRefId { get; set; }
        public bool EmailErrorShown { get; set; }
        public bool MailSendFlag { get; set; }

        public string JobStatus { get; set; }
    }

    public class PieChartData
    {
        public string name { get; set; }
        public decimal y { get; set; }

        public string Color { get; set; }

    }

    public class WorkStatusDaysVsHours
    {
        public string WorkStatus { get; set; }
        public decimal Days { get; set; }
        public decimal PercentageDays { get; set; }
        public decimal Hours { get; set; }
        public decimal PercentageHours { get; set; }
    }


}

