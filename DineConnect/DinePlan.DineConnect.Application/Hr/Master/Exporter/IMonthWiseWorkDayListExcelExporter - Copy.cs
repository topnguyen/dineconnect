﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Hr.Master.Dtos;

namespace DinePlan.DineConnect.Hr.Master
{
    public interface IMonthWiseWorkDayListExcelExporter
    {
        FileDto ExportToFile(List<MonthWiseWorkDayListDto> dtos);
    }
}