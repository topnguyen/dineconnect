﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;


namespace DinePlan.DineConnect.Hr.Master
{
    public interface IPublicHolidayListExcelExporter
    {
        FileDto ExportToFile(List<PublicHolidayListDto> dtos);
    }
}
