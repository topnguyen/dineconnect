﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using DinePlan.DineConnect.Dto;


namespace DinePlan.DineConnect.Hr.Transaction
{
	public interface IIncentiveTagListExcelExporter
	{
		FileDto ExportToFile(List<IncentiveTagListDto> dtos);

		FileDto IncentiveCategoryExportToFile(List<IncentiveCategoryListDto> dtos);
	}
}