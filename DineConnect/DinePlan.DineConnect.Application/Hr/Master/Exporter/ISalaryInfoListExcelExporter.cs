﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;


namespace DinePlan.DineConnect.Hr.Master
{
    public interface ISalaryInfoListExcelExporter
    {
        FileDto ExportToFile(List<SalaryInfoListDto> dtos);
    }
}