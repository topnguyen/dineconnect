﻿using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using System;
using System.Linq;
using Abp.Extensions;

namespace DinePlan.DineConnect.Hr.Master.Exporter
{
    public class AttendanceLogListExcelExporter : FileExporterBase, IAttendanceLogListExcelExporter
    {
       
		public FileDto ExportParticularDateWiseToFile(List<AttendanceParticularDateList> dtos)
		{
			return CreateExcelPackage(
				"AttendanceParticularLogList.xlsx",
				excelPackage =>
				{
					var sheet = excelPackage.Workbook.Worksheets.Add(L("AttendanceLogParticular"));
					sheet.OutLineApplyStyle = true;

                    AddHeader(
						sheet,
						L("BiometricCode"),
                        L("EmpId"),
                        L("Code"),
                        L("EmployeeName"),
                        L("WorkDate"),
                        L("Times")
                        );

					AddObjects(
						sheet, 2, dtos,
						_ => _.BioMetricCode,
						_ => _.EmployeeRefId,
						_ => _.EmployeeRefCode,
						_ => _.EmployeeRefName,
						_ => _.WorkDate.ToString("dd-MMM-yy ddd"),
						_ => _.TimeList
						);

					for (var i = 1; i <= 10; i++)
					{
						sheet.Column(i).AutoFit();
					}
				});
		}


        public FileDto ExportPerDaySalaryPerEmployeeWithOT(List<AttendanceParticularDateList> dtos, string argFileName)
        {
            return CreateExcelPackage(
                argFileName + ".xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("AttendanceLogParticular"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        //L("BiometricCode"),
                        // L("EmpId"),
                        L("Code"),
                        L("EmployeeName"),
                        L("WorkDate"),
                        L("Times"),
                        L("Salary"),
                        L("Std") + " " + L("Hours"),
                        L("Salary") + "/" + L("Day"),
                        L("InTime"),
                        L("OutTime"),
                        L("Hours"),
                        L("Shortage"),
                        L("OT"),
                        L("Salary") + "/" + L("WorkDate"),
                        L("Remarks")
                        );

                    int row = 2;
                    int col = 1;
                    foreach (var lst in dtos)
                    {
                        col = 1;
                        AddDetail(sheet, row, 1, lst.EmployeeRefCode);
                        AddDetail(sheet, row, 2, lst.EmployeeRefName);
                        AddDetail(sheet, row, 3, lst.WorkDate.ToString("dd-MMM-yy ddd"));
                        AddDetail(sheet, row, 4, lst.TimeList);
                        AddDetail(sheet, row, 5, Math.Round(lst.Salary, 2));
                        AddDetail(sheet, row, 6, lst.WorkHoursPerDayWithMinutes);
                        AddDetail(sheet, row, 7, Math.Round(lst.StandardSalaryCurrentDay));
                        AddDetail(sheet, row, 8, lst.InTime == null ? "" : lst.InTime.Value.ToString("h:mm tt"));
                        AddDetail(sheet, row, 9, lst.OutTime == null ? "" : lst.OutTime.Value.ToString("h:mm tt"));
                        AddDetail(sheet, row, 10, lst.ActualWorkHoursWithMinutes);
                        AddDetail(sheet, row, 11, lst.ShortageWorkHours > 0 ? lst.ShortageWorkHoursInMinutes : "", OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                        AddDetail(sheet, row, 12, lst.OTWorkHours > 0 ? lst.OTWorkHoursInMinutes : "", OfficeOpenXml.Style.ExcelHorizontalAlignment.Right);
                        AddDetail(sheet, row, 13, Math.Round(lst.ActualSalaryCurrentDay, 2));
                        AddDetail(sheet, row, 14, lst.Remarks);
                        row++;
                    }

                    var totalStandardWorkHours = dtos.Sum(t => t.WorkHoursPerDay);
                    var totalWorkHours = dtos.Sum(testc => testc.ActualWorkHours);
                    var shortageHours = dtos.Sum(testc => testc.ShortageWorkHours);

                    var strtotalStandardWorkHoursWithMinutes = ChangeMinutstoTime((int)(totalStandardWorkHours * 60));

                    var strtotalWorkHours = ChangeMinutstoTime((int)(totalWorkHours * 60));

                    var strshortageHours = ChangeMinutstoTime((int)(shortageHours * 60));

                    AddDetail(sheet, row, 6, strtotalStandardWorkHoursWithMinutes);
                    AddDetail(sheet, row, 10, strtotalWorkHours);
                    AddDetail(sheet, row, 11, strshortageHours);
                    if (shortageHours > 0)
                        AddDetail(sheet, row, 14, "Shortage In Work Hours");

                    for (var i = 1; i <= 20; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }

                });
        }
        public string ChangeMinutstoTime(int minutesoftheDay)
        {
            if (minutesoftheDay == 0)
            {
                return "";
            }

            double argMinutes = minutesoftheDay;
            int hour = int.Parse(Math.Floor(argMinutes / 60).ToString());
            if (hour > 24)
                hour = hour - 24;

            var min = (argMinutes % 60).ToString().PadLeft(2);
            if (min.Left(1).Equals(" "))
                min = "0" + min.Right(1);
            if (min == " 0")
                min = "00";
            string hourString = hour >= 10 ? hour.ToString() : "0" + hour;
            string data = hourString + ":" + min;
            return data;
        }

    }
}