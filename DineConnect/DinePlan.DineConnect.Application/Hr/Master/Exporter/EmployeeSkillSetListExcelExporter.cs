﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Hr.Master.Exporter
{
    public class EmployeeSkillSetListExcelExporter : FileExporterBase, IEmployeeSkillSetListExcelExporter
    {
        public FileDto ExportToFile(List<EmployeeSkillSetViewDto> dtos)
        {
            return CreateExcelPackage(
                "EmployeeSkillSetList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("EmployeeSkillSet"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("#"),
                        L("Employee"),
                        L("SkillSet")
                        );

                    int sno=1;

                    AddObjects(
                        sheet, 2, dtos,
                        _ => sno++,
                        _ => _.EmployeeRefName,
                        _ => _.SkillSetRefCode
                        );

                    for (var i = 1; i <= 10; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}