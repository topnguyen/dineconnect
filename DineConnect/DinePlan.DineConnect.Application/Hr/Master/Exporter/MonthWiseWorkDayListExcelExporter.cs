﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Hr.Master.Dtos;

namespace DinePlan.DineConnect.Hr.Master.Exporter
{
    public class MonthWiseWorkDayListExcelExporter : FileExporterBase, IMonthWiseWorkDayListExcelExporter
    {
        public FileDto ExportToFile(List<MonthWiseWorkDayListDto> dtos)
        {
            return CreateExcelPackage(
                "MonthWiseWorkDayList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("MonthWiseWorkDay"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id
                        );

                    for (var i = 1; i <= 10; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}