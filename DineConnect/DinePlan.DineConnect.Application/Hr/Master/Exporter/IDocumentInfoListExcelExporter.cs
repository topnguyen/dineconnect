﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;


namespace DinePlan.DineConnect.Hr.Master
{
    public interface IDocumentInfoListExcelExporter
    {
        FileDto ExportToFile(List<DocumentInfoListDto> dtos);
    }
}

