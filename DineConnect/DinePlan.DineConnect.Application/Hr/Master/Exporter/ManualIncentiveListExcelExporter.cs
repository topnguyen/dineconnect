﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Hr.Master.Dtos;

namespace DinePlan.DineConnect.Hr.Transaction.Exporter
{
	public class ManualIncentiveListExcelExporter : FileExporterBase, IManualIncentiveListExcelExporter
	{
		public FileDto ExportToFile(List<ManualIncentiveListDto> dtos)
		{
			return CreateExcelPackage(
				"ManualIncentiveList.xlsx",
				excelPackage =>
				{
					var sheet = excelPackage.Workbook.Worksheets.Add(L("ManualIncentive"));
					sheet.OutLineApplyStyle = true;

					AddHeader(
						sheet,
						L("Id")
						);

					AddObjects(
						sheet, 2, dtos,
						_ => _.Id
						);

					for (var i = 1; i <= 10; i++)
					{
						sheet.Column(i).AutoFit();
					}
				});
		}
	}
}