﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using DinePlan.DineConnect.Dto;


namespace DinePlan.DineConnect.Hr.Transaction
{
    public interface ILeaveRequestListExcelExporter
    {
        FileDto ExportToFile(List<LeaveRequestListDto> dtos);
    }
}