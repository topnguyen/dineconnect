﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Hr.Transaction.Exporter
{
	public class IncentiveTagListExcelExporter : FileExporterBase, IIncentiveTagListExcelExporter
	{
		public FileDto ExportToFile(List<IncentiveTagListDto> dtos)
		{
			return CreateExcelPackage(
				"IncentiveTagList.xlsx",
				excelPackage =>
				{
					var sheet = excelPackage.Workbook.Worksheets.Add(L("IncentiveTag"));
					sheet.OutLineApplyStyle = true;

					AddHeader(
						sheet,
						L("Id"),
						L("Category"),
						L("Code"),
						L("Name"),
						L("HouseBased"),
						L("FullWorkHours"),
						L("MinimumHours"),
						L("MinimumOTHours"),
						L("MinimumClaimedHours"),
						L("IncludeInConsolidationPerDay"),
						L("IncentiveAmount")
						);

					AddObjects(
						sheet, 2, dtos,
						_ => _.Id,
						_ => _.IncentiveCategoryRefName,
						_ => _.IncentiveTagCode,
						_ => _.IncentiveTagName,
						_ => _.HoursBased,
						_ => _.FullWorkHours,
						_ => _.MinimumHours,
						_ => _.MinimumOtHours,
						_ => _.MinimumClaimedHours,
						_ => _.IncludeInConsolidationPerDay,
						_ => _.IncentiveAmount
						);

					for (var i = 1; i <= 10; i++)
					{
						sheet.Column(i).AutoFit();
					}
				});
		}

		public FileDto IncentiveCategoryExportToFile(List<IncentiveCategoryListDto> dtos)
		{
			return CreateExcelPackage(
				"IncentiveCategoryList.xlsx",
				excelPackage =>
				{
					var sheet = excelPackage.Workbook.Worksheets.Add(L("IncentiveCategory"));
					sheet.OutLineApplyStyle = true;

					AddHeader(
						sheet,
						L("Id"),
						L("Code"),
						L("Name"),
						L("BasedOnSkillSet"),
						L("BasedOnQuantity"),
						L("MaximumIncenitivePerQuanityPerTimeSheet"),
						L("OverSeasBasedAllowanceOnly"),
						L("ExcludedOverseasList")
						);

					AddObjects(
						sheet, 2, dtos,
						_ => _.Id,
						_ => _.IncentiveCategoryCode,
						_ => _.IncentiveCategoryName,
						_ => _.BasedOnSkillSet,
						_ => _.BasedOnQuantity,
						_ => _.MaximumIncenitivePerQuanityPerTimeSheet
						);

					for (var i = 1; i <= 10; i++)
					{
						sheet.Column(i).AutoFit();
					}
				});
		}
	}
}
