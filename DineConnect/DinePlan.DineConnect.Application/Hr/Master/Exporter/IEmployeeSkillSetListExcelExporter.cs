﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;


namespace DinePlan.DineConnect.Hr.Master
{
    public interface IEmployeeSkillSetListExcelExporter
    {
        FileDto ExportToFile(List<EmployeeSkillSetViewDto> dtos);
    }
}