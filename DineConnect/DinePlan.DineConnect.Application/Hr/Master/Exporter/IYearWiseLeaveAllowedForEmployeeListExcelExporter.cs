﻿using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Inventory.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Hr;


namespace DinePlan.DineConnect.Hr.Inventory
{
    public interface IYearWiseLeaveAllowedForEmployeeListExcelExporter
    {
        FileDto ExportToFile(List<YearWiseLeaveAllowedForEmployeeListDto> dtos);
    }
}