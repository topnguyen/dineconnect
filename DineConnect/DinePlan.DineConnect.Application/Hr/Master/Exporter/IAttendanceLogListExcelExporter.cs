﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;


namespace DinePlan.DineConnect.Hr.Master
{
    public interface IAttendanceLogListExcelExporter
    {
        FileDto ExportParticularDateWiseToFile(List<AttendanceParticularDateList> dtos);

        FileDto ExportPerDaySalaryPerEmployeeWithOT(List<AttendanceParticularDateList> dtos, string argFileName);
    }
}
