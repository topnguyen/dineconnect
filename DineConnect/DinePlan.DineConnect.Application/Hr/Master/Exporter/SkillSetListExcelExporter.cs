﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Hr.Master.Exporter
{
    public class SkillSetListExcelExporter : FileExporterBase, ISkillSetListExcelExporter
    {
        public FileDto ExportToFile(List<SkillSetListDto> dtos)
        {
            return CreateExcelPackage(
                "SkillSetList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("SkillSet"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("SkillSet"),
                        L("Description")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.SkillCode,
                        _ => _.Description
                        );

                    for (var i = 1; i <= 10; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}