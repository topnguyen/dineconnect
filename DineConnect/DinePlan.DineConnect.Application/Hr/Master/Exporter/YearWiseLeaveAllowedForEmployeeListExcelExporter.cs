﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Inventory.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Hr.Inventory.Exporter
{
    public class YearWiseLeaveAllowedForEmployeeListExcelExporter : FileExporterBase, IYearWiseLeaveAllowedForEmployeeListExcelExporter
    {
        public FileDto ExportToFile(List<YearWiseLeaveAllowedForEmployeeListDto> dtos)
        {
            return CreateExcelPackage(
                "YearWiseLeaveAllowedForEmployeeList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("YearWiseLeaveAllowedForEmployee"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id
                        );

                    for (var i = 1; i <= 10; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
