﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Hr.Master;

namespace DinePlan.DineConnect.Hr.Master.Exporter
{
	public class EmployeeVsIncentiveListExcelExporter : FileExporterBase, IEmployeeVsIncentiveListExcelExporter
	{
		public FileDto ExportToFile(List<EmployeeVsIncentiveListDto> dtos)
		{
			return CreateExcelPackage(
				"EmployeeVsIncentiveList.xlsx",
				excelPackage =>
				{
					var sheet = excelPackage.Workbook.Worksheets.Add(L("EmployeeVsIncentive"));
					sheet.OutLineApplyStyle = true;

					AddHeader(
						sheet,
						L("Id"),
                        L("EmployeeRefCode"),
                        L("EmployeeRefName"),
                        L("IncentiveTagRefCode"),
                        L("EffectiveFrom"),
                        L("RollBackFrom")
                        );
                    AddObjects(
						sheet, 2, dtos,
						_ => _.Id,
						_ => _.EmployeeRefCode,
						_ => _.EmployeeRefName,
						_ => _.IncentiveTagRefCode,
						_ => _.EffectiveFrom.ToString("dd-MMM-yyyy"),
						_ => _.RollBackFrom==null?"":_.RollBackFrom.Value.ToString("dd-MMM-yyyy") 
						);

					for (var i = 1; i <= 10; i++)
					{
						sheet.Column(i).AutoFit();
					}
				});
		}
	}
}