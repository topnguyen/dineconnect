﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Hr.Master.Dtos;


namespace DinePlan.DineConnect.Hr.Transaction
{
	public interface IManualIncentiveListExcelExporter
	{
		FileDto ExportToFile(List<ManualIncentiveListDto> dtos);
	}
}
