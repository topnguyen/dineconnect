﻿using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using OfficeOpenXml;

namespace DinePlan.DineConnect.Hr.Master.Exporter
{
    public class PersonalInformationListExcelExporter : FileExporterBase, IPersonalInformationListExcelExporter
    {
        public FileDto PersonalInformationExportToFile(List<PersonalInformationListDto> dtos)
        {
            return CreateExcelPackage(
                "PersonalInformationList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("PersonalInformation"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Company"),
                        L("EmployeeName"),
                        L("EmployeeCode"),
                        L("UserName"),
                        L("DateOfBirth"),
                        L("Gender"),
                        L("UserSerialNumber"),
                        L("BioMetricCode"),
                        L("DateHired"),
                        L("DefaultSkillSet"),
                        L("JobTitle"),
                        L("StaffType"),
                        L("LastWorkingDate"),
                        L("Email"),
						L("Email2"),
                        L("IsAttendanceRequired"),
                        L("IsBillable"),
                        L("CostCentre"),
                        L("Supervisor"),
                        L("Manager")                        
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.CompanyRefName,
                        _ => _.EmployeeName,
                        _ => _.EmployeeCode,
                        _ => _.LoginUserName,
                         _ => _.DateOfBirth.ToString("yyyy-MMM-dd"),
                         _ => _.Gender,
                        _ => _.UserSerialNumber,
                        _ => _.BioMetricCode,
                        _ => _.DateHired.ToString("yyyy-MMM-dd"),
                        _ => _.DefaultSkillSetCode,
                        _ => _.JobTitle,
                        _ => _.StaffType,
                        _ => _.LastWorkingDate !=null ?_.LastWorkingDate.Value.ToString("yyyy-MMM-dd") : "",
                        _ => _.Email,
						_ => _.PersonalEmail,
                        _ => _.IsAttendanceRequired,
                        _ => _.isBillable,
                        _ => _.SupervisorEmployeeRefName,
                        _ => _.InchargeEmployeeRefName
                        );

                    for (var i = 1; i <= 30; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }

                    // rows and columns indices
                    int startRowIndex = 1;
                    int startingColIndex = 1;
                    int totalColIndex = 30;

                    // rowIndex holds the current running row index
                    int toRowIndex = dtos.Count+2;

                    AutoFilterRangeGiven(sheet, startRowIndex, startingColIndex, toRowIndex, totalColIndex);


                });
        }

        public FileDto SkillSetExportToFile(List<SkillSetListDto> dtos)
        {
            return CreateExcelPackage(
                "SkillSetList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("SkillSet"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id
                        );

                    for (var i = 1; i <= 10; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
