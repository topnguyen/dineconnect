﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Hr.Transaction.Exporter
{
    public class LeaveTypeListExcelExporter : FileExporterBase, ILeaveTypeListExcelExporter
    {
        public FileDto ExportToFile(List<LeaveTypeListDto> dtos)
        {
            return CreateExcelPackage(
                "LeaveTypeList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("LeaveType"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id
                        );

                    for (var i = 1; i <= 10; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
