﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Hr.Transaction.Exporter
{
    public class LeaveRequestListExcelExporter : FileExporterBase, ILeaveRequestListExcelExporter
    {
        public FileDto ExportToFile(List<LeaveRequestListDto> dtos)
        {
            return CreateExcelPackage(
                "LeaveRequestList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("LeaveRequest"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,L("Company"),
                                L("Employee"),
								L("LeaveType"),
								L("LeaveReason"),
								L("From"),
								L("To"),
								L("Days"),
								L("HalfDay"),
								L("Status"),
								L("Fin"),
                                L("NIC"),
                                L("ResidentStatus")
								);

                    AddObjects(
                        sheet, 2, dtos,
								_ => _.CompanyRefCode,
								_ => _.EmployeeRefName,
								_ => _.LeaveTypeRefName,
								_ => _.LeaveReason,
								_ => _.LeaveFrom.ToString("dd-MMM-yy"),
								_ => _.LeaveTo.ToString("dd-MMM-yy"),
								_ => _.TotalNumberOfDays,
								_ => _.HalfDayFlag==true?"YES":"NO",
								_ => _.LeaveStatus,
                                _ => _.NationalIdentificationNumber
                        );

                    for (var i = 1; i <= 12; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
