﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Hr.Master.Exporter
{
    public class EmployeeDocumentInfoListExcelExporter : FileExporterBase, IEmployeeDocumentInfoListExcelExporter
    {
        public FileDto PendingDocumentExportToFile(HealthDocumentOutput input)
        {
            string fileName="Document";

            if (input.PendingList.Count>0)
                fileName = fileName + " " + input.PendingList[0].DocumentInfoRefName;
            else if (input.CompletedList.Count>0)
                fileName = fileName + " " + input.CompletedList[0].DocumentInfoRefName;

            return CreateExcelPackage(
                fileName +" " + " Employee List.xlsx",
                excelPackage =>
                {
                    var pendingSheet = excelPackage.Workbook.Worksheets.Add(L("Pending"));
                    pendingSheet.OutLineApplyStyle = true;

                    AddHeader(
                        pendingSheet,
                        L("DocumentName"),
                        L("Code"),
                        L("Employee"),
                        L("DOJ")
                        );

                    AddObjects(
                        pendingSheet, 2, input.PendingList,
                        _ => _.DocumentInfoRefName,
                        _ => _.EmployeeRefCode,
                        _ => _.EmployeeRefName,
                        _ => _.DateOfJoining.Value.ToString("dd-MMM-yy")
                        );

                    for (var i = 1; i <= 6; i++)
                    {
                        pendingSheet.Column(i).AutoFit();
                    }

                    var completedSheet = excelPackage.Workbook.Worksheets.Add(L("Completed"));
                    completedSheet.OutLineApplyStyle = true;

                    AddHeader(
                       completedSheet,
                       L("Code"),
                       L("Name"),
                       L("Document"),
                       L("Reference"),
                       L("DefaultAlertDays"),
                       L("ValidityFrom"),
                       L("ValidityTo"),
                       L("AlertFrom"),
                       L("FileName"),
                       L("Document") + " " + L("Alert") + " " + L("Status"),
                       L("Days")
                       );

                    AddObjects(
                        completedSheet, 2, input.CompletedList,
                        _ => _.EmployeeRefCode,
                        _ => _.EmployeeRefName,
                        _ => _.DocumentInfoRefName,
                        _ => _.DocReferenceNumber,
                        _ => _.DefaultAlertDays,
                        _ => _.StartDate != null ? _.StartDate.Value.ToString("dd-MMM-yy") : "",
                        _ => _.EndDate != null ? _.EndDate.Value.ToString("dd-MMM-yy") : "",
                        _ => _.AlertStartingDate!=null? _.AlertStartingDate.Value.ToString("dd-MMM-yy"):"",
                        _ => _.FileName,
                        _ => _.Status,
                        _ => _.NoOfDays 
                        );

                    for (var i = 1; i <= 15; i++)
                    {
                        completedSheet.Column(i).AutoFit();
                    }

                });
        }

        public FileDto ExportToFile(List<EmployeeDocumentInfoListDto> dtos)
        {
            return CreateExcelPackage(
                "EmployeeDocumentInfoList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("EmployeeDocumentInfo"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Employee"),
                        L("DocumentName"),
                        L("Reference"),
                        L("DefaultAlertDays"),
                        L("StartDate"),
                        L("EndDate"),
                        L("AlertFrom"),
                        L("FileName"),
                        //L("FileExtenstionType"),
                        L("Status")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.EmployeeRefName,
                        _ => _.DocumentInfoRefName,
                        _ => _.DocReferenceNumber,
                        _ => _.DefaultAlertDays,
                        _ => _.StartDate != null ? _.StartDate.Value.ToString("dd-MMM-yy") : "",
                        _ => _.EndDate != null ? _.EndDate.Value.ToString("dd-MMM-yy") : "",
                        _ => _.AlertStartingDate != null ? _.AlertStartingDate.Value.ToString("dd-MMM-yy") : "",
                        _ => _.FileName,
                        //_ => _.FileExtenstionType,
                        _ => _.Status
                        );

                    for (var i = 1; i <= 15; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
