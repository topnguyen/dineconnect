﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Hr.Master.Exporter
{
	public class WorkDayListExcelExporter : FileExporterBase, IWorkDayListExcelExporter
	{
		public FileDto ExportToFile(List<WorkDayListDto> dtos)
		{
			return CreateExcelPackage(
				"WorkDayList.xlsx",
				excelPackage =>
				{
					var sheet = excelPackage.Workbook.Worksheets.Add(L("WorkDay"));
					sheet.OutLineApplyStyle = true;

					AddHeader(
						sheet,
						L("Id"),
						L("Day")
						);

					AddObjects(
						sheet, 2, dtos,
						_ => _.Id,
						_ => _.DayOfWeekRefName
						);

					for (var i = 1; i <= 10; i++)
					{
						sheet.Column(i).AutoFit();
					}
				});
		}
	}
}
