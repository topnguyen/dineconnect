﻿
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using DinePlan.DineConnect.Connect.Master.Dtos;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Hr.Master.Dtos
{
    [AutoMapFrom(typeof(DcStationMaster))]
    public class DcStationMasterListDto : FullAuditedEntityDto
    {
        public int CompanyRefId { get; set; }
        public string CompanyRefName { get; set; }

        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }

        public string StationName { get; set; }
        public int DcHeadRefId { get; set; }
        public string DcHeadRefName { get; set; }

        public int DcGroupRefId { get; set; }
        public string DcGroupRefName { get; set; }

        public int MaximumStaffRequired { get; set; }
        public int MinimumStaffRequired { get; set; }
        public decimal PriorityLevel { get; set; }

        public bool IsMandatory { get; set; }

        public bool IsActive { get; set; }
        public bool NightFlag { get; set; }

        public bool DefaultTimeFlag { get; set; }

        [Required]
        public int TimeIn { get; set; }

        [Required]
        public bool BreakFlag { get; set; }

        [Required]
        public int BreakOut { get; set; }

        [Required]
        public int BreakIn { get; set; }

        public int BreakMinutes { get; set; }

        [Required]
        public int TimeOut { get; set; }

        public string TimeDescription { get; set; }

        public string TimeInHours { get; set; }

        public string TimeInMinutes { get; set; }

        public string TimeOutHours { get; set; }

        public string TimeOutMinutes { get; set; }

        public string BreakOutHours { get; set; }

        public string BreakOutMinutes { get; set; }
        public string BreakInHours { get; set; }

        public string BreakInMinutes { get; set; }

    }

    [AutoMapTo(typeof(DcStationMaster))]

    public class DcStationMasterEditDto
    {
        public int? Id { get; set; }

        public int CompanyRefId { get; set; }

        public string CompanyRefName { get; set; }

        public int LocationRefId { get; set; }

        public string LocationRefName { get; set; }

        public string StationName { get; set; }

        public int DcHeadRefId { get; set; }

        public int DcGroupRefId { get; set; }

        public int MaximumStaffRequired { get; set; }
        public int MinimumStaffRequired { get; set; }
        public decimal PriorityLevel { get; set; }

        public bool IsMandatory { get; set; }

        public bool IsActive { get; set; }
        public bool NightFlag { get; set; }

        public bool DefaultTimeFlag { get; set; }

        [Required]
        public int TimeIn { get; set; }

        [Required]
        public bool BreakFlag { get; set; }

        [Required]
        public int BreakOut { get; set; }

        [Required]
        public int BreakIn { get; set; }

        public int BreakMinutes { get; set; }

        [Required]
        public int TimeOut { get; set; }
        public string TimeDescription { get; set; }

    }

    public class GetDcStationMasterInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int? LocationRefId { get; set; }
        public List<LocationListDto> Locations { get; set; }
        public int? DcHeadRefId { get; set; }
        public string HeadName { get; set; }
        public int? DcGroupRefId { get; set; }
        public string GroupName { get; set; }
        public string StationName { get; set; }
        public string Filter { get; set; }
        public string Operation { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
    public class GetDcStationMasterForEditOutput : IOutputDto
    {
        public DcStationMasterEditDto DcStationMaster { get; set; }
    }
    public class CreateOrUpdateDcStationMasterInput : IInputDto
    {
        [Required]
        public DcStationMasterEditDto DcStationMaster { get; set; }
    }
}

