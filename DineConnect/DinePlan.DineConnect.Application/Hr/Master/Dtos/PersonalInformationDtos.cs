﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using DinePlan.DineConnect.Hr;
using System.Collections.Generic;
using DinePlan.DineConnect.MultiTenancy;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Hr;
using DinePlan.DineConnect.Connect.Master.Dtos;

namespace DinePlan.DineConnect.Hr.Master.Dtos
{
    [AutoMapFrom(typeof(PersonalInformation))]
    public class SimplePersonalInformationListDto
    {
        public int Id { get; set; }
        public int CompanyRefId { get; set; }
        public string CompanyRefName { get; set; }
        public string CompanyRefCode { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeCode { get; set; }
        public string StaffType { get; set; }
        public string FINNumber { get; set; }
        public bool IsCustomerRelationShipPortFolioExists { get; set; }
        public bool IsAttendanceRequired { get; set; }
        public bool isBillable { get; set; }
        public int DefaultWeekOff { get; set; }
        public string HandPhone1 { get; set; }
        public string HandPhone2 { get; set; }
        public string Email { get; set; }
        public string PersonalEmail { get; set; }
        public bool IsSmartAttendanceFingerAllowed { get; set; }
        public bool IsDayCloseStatusMailRequired { get; set; }
        public virtual Guid? ProfilePictureId { get; set; }
        public int JobTitleRefId { get; set; }
        public string JobTitle { get; set; }
        public string SkillSetList { get; set; }
    }

    [AutoMapFrom(typeof(PersonalInformation))]
    public class PersonalInformationListDto : FullAuditedEntityDto
    {

        public int CompanyRefId { get; set; }
        public string CompanyRefName { get; set; }
        public string CompanyRefCode { get; set; }
        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }

        public string BioMetricCode { get; set; }
        public decimal UserSerialNumber { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeCode { get; set; }
        public string StaffType { get; set; }
        public string NationalIdentificationNumber { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string MaritalStatus { get; set; }
        public string PassPortNumber { get; set; }
        public DateTime DateHired { get; set; }
        public int JobTitleRefId { get; set; }
        public string JobTitle { get; set; }
        public DateTime? LastWorkingDate { get; set; }
        public bool ActiveStatus { get; set; }
        public string BankName { get; set; }
        public string AccountNumber { get; set; }
        public string CPFAccountNumber { get; set; }
        public string IDType { get; set; }
        public int DefaultSkillRefId { get; set; }
        public string DefaultSkillSetCode { get; set; }
        public virtual Guid? ProfilePictureId { get; set; }
        public bool IsAttendanceRequired { get; set; }
        public bool isBillable { get; set; }
        public string PerAddress1 { get; set; }
        public string PerAddress2 { get; set; }
        public string PerAddress3 { get; set; }
        public string PerAddress4 { get; set; }
        public string PostalCode { get; set; }
        public string ComAddress1 { get; set; }
        public string ComAddress2 { get; set; }
        public string ComAddress3 { get; set; }
        public string ComAddress4 { get; set; }
        public string ZipCode { get; set; }
        public int DefaultWeekOff { get; set; }
        public string HandPhone1 { get; set; }
        public string HandPhone2 { get; set; }
        public string Email { get; set; }
        public string PersonalEmail { get; set; }

        [Required]
        public string LoginUserName { get; set; }
        public string LeaveIntimationEmailList { get; set; }

        public bool IncentiveAllowedFlag { get; set; }
        public int MobileLiveSyncMinutes { get; set; }
        public string WorkingSector { get; set; }
        public int? SupervisorEmployeeRefId { get; set; }
        public string SupervisorEmployeeRefName { get; set; }

        public int? InchargeEmployeeRefId { get; set; }
        public string InchargeEmployeeRefName { get; set; }
        public bool IsFixedCostCentreInDutyChart { get; set; }
        public DateTime? DutyFixedUptoDate { get; set; }
        public bool IsSmartAttendanceFingerAllowed { get; set; }
        public bool IsDayCloseStatusMailRequired { get; set; }
        public int EmployeeResidentStatusRefId { get; set; }
        public int RaceRefId { get; set; }
        public int EmployeeCostCentreRefId { get; set; }
        public string CostCentre { get; set; }
        public int EmployeeReligionRefId { get; set; }
        public int ReligionRefId { get; set; }
        public string WPClassNumber { get; set; }
        public DateTime? WorkPassStartDate { get; set; }
        public DateTime? WorkPassEndDate { get; set; }
        public string PRNumber { get; set; }
        public DateTime? PRStartDate { get; set; }
        public DateTime? PREndDate { get; set; }
    }

    [AutoMapTo(typeof(PersonalInformation))]
    public class PersonalInformationEditDto
    {
        public int? Id { get; set; }
        public int CompanyRefId { get; set; }
        public string CompanyRefName { get; set; }
        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }
        public string BioMetricCode { get; set; }
        public string EmployeeName { get; set; }
        public decimal UserSerialNumber { get; set; }
        public string EmployeeCode { get; set; }

        public string EmployeeGivenName { get; set; }

        public string EmployeeSurName { get; set; }

        public string StaffType { get; set; }
        public string NationalIdentificationNumber { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string MaritalStatus { get; set; }
        public string PassPortNumber { get; set; }

        public DateTime DateHired { get; set; }
        public int JobTitleRefId { get; set; }
        //public string JobTitle { get; set; }

        public DateTime? LastWorkingDate { get; set; }
        public bool ActiveStatus { get; set; }

        public int DefaultSkillRefId { get; set; }
        public string DefaultSkillSetCode { get; set; }
        public virtual Guid? ProfilePictureId { get; set; }
        public int DefaultWeekOff { get; set; }
        public bool IsAttendanceRequired { get; set; }
        public bool isBillable { get; set; }
        public string PerAddress1 { get; set; }
        public string PerAddress2 { get; set; }
        public string PerAddress3 { get; set; }
        public string PerAddress4 { get; set; }
        public string PostalCode { get; set; }
        public string ComAddress1 { get; set; }
        public string ComAddress2 { get; set; }
        public string ComAddress3 { get; set; }
        public string ComAddress4 { get; set; }
        public string ZipCode { get; set; }
        public string Email { get; set; }
        public string PersonalEmail { get; set; }
        [Required]
        public string LoginUserName { get; set; }

        public string HandPhone1 { get; set; }
        public string HandPhone2 { get; set; }

        public string LeaveIntimationEmailList { get; set; }
        public bool IncentiveAllowedFlag { get; set; }
        public int? SupervisorEmployeeRefId { get; set; }
        public int? InchargeEmployeeRefId { get; set; }
        public bool IsFixedCostCentreInDutyChart { get; set; }
        public DateTime? DutyFixedUptoDate { get; set; }
        public bool IsSmartAttendanceFingerAllowed { get; set; }
        public bool IsDayCloseStatusMailRequired { get; set; }

        public int EmployeeResidentStatusRefId { get; set; }
        public int RaceRefId { get; set; }
        public int EmployeeCostCentreRefId { get; set; }
        public int ReligionRefId { get; set; }
        public int EmployeeReligionRefId { get; set; }
        public string WPClassNumber { get; set; }
        public DateTime? WorkPassStartDate { get; set; }
        public DateTime? WorkPassEndDate { get; set; }
        public string PRNumber { get; set; }
        public DateTime? PRStartDate { get; set; }
        public DateTime? PREndDate { get; set; }
        public int? DinePlanUserId { get; set; }
    }



    public class EmployeeWithSkillSetViewDto
    {
        public int EmployeeRefId { get; set; }
        public string EmployeeRefName { get; set; }
        public string EmployeeRefCode { get; set; }
        public int CompanyRefId { get; set; }
        public string CompanyRefName { get; set; }
        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }
        public int DefaultSkillRefId { get; set; }
        public string DefaultSkillSetCode { get; set; }
        public int JobTitleRefId { get; set; }
        public string JobTitle { get; set; }
        public string Gender { get; set; }
        public List<EmployeeKnownSkillSetList> EmployeeKnownSkillSetList { get; set; }
        //public List<IncentiveTagListDto> AllowedIncentives { get; set; }

        public bool ActiveStatus { get; set; }
        public DateTime? LastWorkingDate { get; set; }
        public int? SupervisorEmployeeRefId { get; set; }
        public int? InchargeEmployeeRefId { get; set; }
        public bool IsSmartAttendanceFingerAllowed { get; set; }
        public bool IsDayCloseStatusMailRequired { get; set; }
        public int EmployeeResidentStatusRefId { get; set; }
        public int RaceRefId { get; set; }
        public int ReligionRefId { get; set; }
        public int EmployeeCostCentreRefId { get; set; }
        public int EmployeeReligionRefId { get; set; }
    }

    public class EmployeeWithSalaryViewDto
    {
        public int EmployeeRefId { get; set; }
        public string EmployeeRefName { get; set; }
        public string EmployeeRefCode { get; set; }
        public int CompanyRefId { get; set; }
        public string CompanyRefName { get; set; }
        public int DefaultSkillRefId { get; set; }
        public string DefaultSkillSetCode { get; set; }
        public decimal MonthlyCtc { get; set; }
        public decimal PerDayCtc { get; set; }
        public decimal PerHourCtcForThisMonth { get; set; }
        public int JobTitleRefId { get; set; }
        public string JobTitle { get; set; }
        public bool IsSmartAttendanceFingerAllowed { get; set; }
        public bool IsDayCloseStatusMailRequired { get; set; }
        public int EmployeeResidentStatusRefId { get; set; }
        public int ReligionRefId { get; set; }
        public int RaceRefId { get; set; }
        public int EmployeeCostCentreRefId { get; set; }
        public int EmployeeReligionRefId { get; set; }
    }


    [AutoMapTo(typeof(SkillSet))]
    public class EmployeeKnownSkillSetList
    {
        public int SkillSetRefId { get; set; }
        public string SkillSetRefName { get; set; }
    }

    public class GetPersonalInformationInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public int EmployeeRefId { get; set; }
        public String EmployeeRefCode { get; set; }
        public string EmployeeName { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int JobTitleRefId { get; set; }
        public string JobTitle { get; set; }
        public int DefaultSkillRefId { get; set; }
        public string DefaultSkillSetCode { get; set; }
        public string Company { get; set; }
        public bool ResignedFlag { get; set; }
        public bool ActiveFlag { get; set; }

        public int? SupervisorEmployeeRefId { get; set; }
        public int? InchargeEmployeeRefId { get; set; }


        public List<CompanyListDto> Companies { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }

    public class GetPersonalInformationForEditOutput : IOutputDto
    {
        public PersonalInformationEditDto PersonalInformation { get; set; }
        public List<ComboboxItemDto> SkillSetList { get; set; }

    }

    public class CreateOrUpdatePersonalInformationInput : IInputDto
    {
        [Required]
        public PersonalInformationEditDto PersonalInformation { get; set; }
        public List<ComboboxItemDto> SkillSetList { get; set; }

        public bool UpdateSerialNumberRequired { get; set; }

    }

    public class PictureDto : IInputDto
    {
        public int Id { get; set; }
        public int EmpId { get; set; }
        public string Data { get; set; }
        public string Name { get; set; }
    }

    public class EmployeeDocumentDto : IInputDto
    {
        public int Id { get; set; }
        public int EmployeeRefId { get; set; }
        public int DocumentInfoRefId { get; set; }
    }
    public class SalarySlipDto : IInputDto
    {
        public int Id { get; set; }
        public int EmployeeRefId { get; set; }
        public DateTime Month { get; set; }
        public string TenantName { get; set; }
    }

    public class NullableDateInput : IInputDto
    {
        public DateTime? DateId { get; set; }
    }

   
    public class MaterialInput : IInputDto
    {
        public int MaterialRefId { get; set; }
    }

    public class BooleanInput : IInputDto
    {
        public bool boolValue { get; set; }
    }

    public class VerifyDayCloseDates : IInputDto
    {
        public int LocationRefId { get; set; }
        public DateTime DateId { get; set; }
        public int NoOfDays { get; set; }
    }


    public class DateRangeInput : IInputDto
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }

    [AutoMapTo(typeof(UserDefaultInformation))]
    public class UserDefaultInformationViewDto
    {
        public virtual long UserId { get; set; }

        public virtual int EmployeeRefId { get; set; }

        public string EmployeeRefName { get; set; }


    }

    public class GetEmployeeMaximumInput : IInputDto
    {
        public int CompanyRefId { get; set; }
        public string CompanyCode { get; set; }
    }

    public class EmployeeMaximumDetails : IOutputDto
    {
        public string EmployeeCode { get; set; }
        public string BioMetricCode { get; set; }
    }

    public class RegisterUserDto : IInputDto
    {
        /// <summary>
        /// Not required for single-tenant applications.
        /// </summary>
        [StringLength(Tenant.MaxTenancyNameLength)]
        public string TenancyName { get; set; }

        [Required]
        [StringLength(User.MaxNameLength)]
        public string Name { get; set; }

        [Required]
        [StringLength(User.MaxSurnameLength)]
        public string Surname { get; set; }

        [StringLength(User.MaxUserNameLength)]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(User.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }

        [StringLength(User.MaxPlainPasswordLength)]
        public string Password { get; set; }

        public bool IsExternalLogin { get; set; }
    }

    public class EmployeeBasicDetails
    {
        public int EmployeeRefId { get; set; }
        public string EmployeeRefName { get; set; }
        public string EmployeeRefCode { get; set; }
        public int CompanyRefId { get; set; }
        public string CompanyRefName { get; set; }
        public int DefaultSkillRefId { get; set; }
        public string DefaultSkillSetCode { get; set; }
        public int JobTitleRefId { get; set; }
        public string JobTitle
        {
            get; set;
        }

        public bool ActiveStatus { get; set; }

        public DateTime? LastWorkingDate { get; set; }
        public DateTime? LastOfficialWorkingDate { get; set; }
        public int MobileLiveSyncMinutes { get; set; }
        public string WorkingSector { get; set; }
        public int? SupervisorEmployeeRefId { get; set; }
        public int? InchargeEmployeeRefId { get; set; }

    }

    public class WeekDaysDto
    {
        public int WeekDayNumber { get; set; }
        public string WeekDayName { get; set; }
    }

    public class UserNameInput : IInputDto
    {
        public string UserName { get; set; }
    }
    public class EmployeeIdInput : IInputDto
    {
        public int EmployeeRefId { get; set; }
    }
    public class LocalPermissionDto
    {
        public string PermissionName { get; set; }
        public bool IsGranted { get; set; }
    }
    public class GpsAttendanceOutputDto : IOutputDto
    {
        public DateTime Date { get; set; }
        public DateTime UTCTime { get; set; }
        public DateTime ServerTime { get; set; }
        public string Status { get; set; }
        public string Location { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Altitude { get; set; }
        public string Address { get; set; }
        public bool IsSmartAttendanceFingerReceived { get; set; }
        public int TenantId { get; set; }
    }

    public class GpsAttendanceParticularDateList
    {
        public int EmployeeRefId { get; set; }
        public string EmployeeRefName { get; set; }
        public bool NightFlag { get; set; }
        public DateTime WorkDate { get; set; }
        public GpsAttendanceListDto StartDetail { get; set; }
        public GpsAttendanceListDto EndDetail { get; set; }
        
    }

    [AutoMapFrom(typeof(GpsAttendance))]
    public class GpsAttendanceListDto : FullAuditedEntityDto
    {
        public int EmployeeRefId { get; set; }
        public string DateTime { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Altitude { get; set; }
        public string Address { get; set; }
        public byte[] Photo { get; set; }
        public int AttendanceStatus { get; set; }
        public int LoginEmployeeRefId { get; set; }
        public string IsRange { get; set; }
        public double GpsDistanceFromClient { get; set; }
        public string RangeErrorMessage { get; set; }
    }


    public class GetEmployeeStatusDto : IInputDto
    {
        public DateTime GivenDate { get; set; }
        public int EmployeeRefId { get; set; }
    }


    public class EmailInput : IInputDto
    {
        public int? Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int EmployeeRefId { get; set; }
        public string Operation { get; set; }
    }

    public class GetEmployeeDashBoardDto : IInputDto
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string DateMode { get; set; }    //  Daily, Fortnightly, Monthly, Quarterly, HalfYearly

        public int EmployeeRefId { get; set; }
        public bool IsCalendarEventsRequired { get; set; }

    }

    public class StringInput : IInputDto
    {
        public string StringId { get; set; }
    }

    public class ReturnSuccessOrFailureWithMessage
    {
        public bool SuccessFlag { get; set; }
        public int UserErrorNumber { get; set; }
        public string Message { get; set; }
    }

    public class ReceivableMessageOutput : IOutputDto
    {
        public int Id { get; set; }
        public string EmailRecieverName { get; set; }
        public string EmailRecieverId { get; set; }
        public string Email { get; set; }
        public string HtmlMessage { get; set; }
        public bool SuccessFlag { get; set; }
        public string SuccessMessage { get; set; }
        public string ErrorMessage { get; set; }
        public bool IsContactExists { get; set; }
    }


   
}

