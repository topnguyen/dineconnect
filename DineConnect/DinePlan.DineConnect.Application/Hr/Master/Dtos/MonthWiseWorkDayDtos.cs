﻿
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Hr.Master.Dtos
{
    [AutoMapFrom(typeof(MonthWiseWorkDay))]
    public class MonthWiseWorkDayListDto : FullAuditedEntityDto
    {
        //TODO: DTO MonthWiseWorkDay Properties Missing
        //YOU CAN REFER ATTRIBUTES IN 
        public string MonthYear { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int NoOfDays { get; set; }
        public int NoOfLeaves { get; set; }
        public int NoOfGovernmentHolidays { get; set; }
        public int NoOfWorkDays { get; set; }
        public int SalaryCalculationDays { get; set; }
    }

    [AutoMapTo(typeof(MonthWiseWorkDay))]
    public class MonthWiseWorkDayEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO MonthWiseWorkDay Properties Missing
        public string MonthYear { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int NoOfDays { get; set; }
        public int NoOfLeaves { get; set; }
        public int NoOfGovernmentHolidays { get; set; }
        public int NoOfWorkDays { get; set; }
        public int SalaryCalculationDays { get; set; }
    }

    public class GetMonthWiseWorkDayInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetMonthWiseWorkDayForEditOutput : IOutputDto
    {
        public MonthWiseWorkDayEditDto MonthWiseWorkDay { get; set; }
    }
    public class CreateOrUpdateMonthWiseWorkDayInput : IInputDto
    {
        [Required]
        public MonthWiseWorkDayEditDto MonthWiseWorkDay { get; set; }
    }
}
