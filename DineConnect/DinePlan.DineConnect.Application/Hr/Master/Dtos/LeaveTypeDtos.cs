﻿
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using System.Collections.Generic;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Hr.Transaction.Dtos
{
    [AutoMapFrom(typeof(LeaveType))]
    public class LeaveTypeListDto : FullAuditedEntityDto
    {
        public string LeaveTypeName { get; set; }
        public string LeaveTypeShortName { get; set; }
        public Decimal NumberOfLeaveAllowed { get; set; }
        public DateTime CalculationPeriodStarts { get; set; }
        public DateTime CalculationPeriodEnds { get; set; }
        public bool MaleGenderAllowed { get; set; }
        public bool FemaleGenderAllowed { get; set; }
        public bool AllowedForMarriedEmployeesOnly { get; set; }
        public bool IsSupportingDocumentsRequired { get; set; }

        public bool EligibleOnlyAnnualLeaveExhausted { get; set; }

        public decimal DefaultNumberOfLeaveAllowed { get; set; }
   }

    [AutoMapTo(typeof(LeaveType))]
    public class LeaveTypeEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO LeaveType Properties Missing
        public string LeaveTypeName { get; set; }
        public string LeaveTypeShortName { get; set; }
        public Decimal NumberOfLeaveAllowed { get; set; }
        public DateTime CalculationPeriodStarts { get; set; }
        public DateTime CalculationPeriodEnds { get; set; }
        public bool MaleGenderAllowed { get; set; }
        public bool FemaleGenderAllowed { get; set; }
        public bool AllowedForMarriedEmployeesOnly { get; set; }
        public bool IsSupportingDocumentsRequired { get; set; }

        public bool EligibleOnlyAnnualLeaveExhausted { get; set; }

        public decimal DefaultNumberOfLeaveAllowed { get; set; }
   }


    public class GetLeaveTypeInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    
    public class GetLeaveTypeForEditOutput : IOutputDto
    {
        public LeaveTypeEditDto LeaveType { get; set; }
        public List<LeaveType> LeaveExhaustedList { get; set; }
    }
    public class CreateOrUpdateLeaveTypeInput : IInputDto
    {
        [Required]
        public LeaveTypeEditDto LeaveType { get; set; }
        public List<LeaveType> LeaveExhaustedList { get; set; }

    }
}

