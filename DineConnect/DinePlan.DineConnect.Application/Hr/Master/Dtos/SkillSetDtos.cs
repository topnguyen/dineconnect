﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Hr.Master.Dtos
{
    [AutoMapFrom(typeof(SkillSet))]
    public class SkillSetListDto : FullAuditedEntityDto
    {
        public virtual string SkillCode { get; set; }
        
        public virtual string Description { get; set; }
        public bool IsActive { get; set; }

    }


	[AutoMapTo(typeof(SkillSet))]
    public class SkillSetEditDto
    {
        public int? Id { get; set; }
        public virtual string SkillCode { get; set; }

        public virtual string Description { get; set; }
        public bool IsActive { get; set; }

    }

    public class GetSkillSetInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetSkillSetForEditOutput : IOutputDto
    {
        public SkillSetEditDto SkillSet { get; set; }
    }
    public class CreateOrUpdateSkillSetInput : IInputDto
    {
        [Required]
        public SkillSetEditDto SkillSet { get; set; }
    }

	
}

