﻿

using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Hr.Master.Dtos
{
    [AutoMapFrom(typeof(EmployeeDocumentInfo))]
    public class EmployeeDocumentInfoListDto : FullAuditedEntityDto
    {
        //TODO: DTO EmployeeDocumentInfo Properties Missing
        //YOU CAN REFER ATTRIBUTES IN 
        public int EmployeeRefId { get; set; }
        public int DocumentInfoRefId { get; set; }
        public string EmployeeRefName { get; set; }
        public string EmployeeRefCode { get; set; }
        public string DocumentInfoRefName { get; set; }
        public string DocReferenceNumber { get; set; }
        public int DefaultAlertDays { get; set; }
        public bool LifeTimeFlag { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? DateOfJoining { get; set; }
        public DateTime? AlertStartingDate { get; set; }

        public string FileName { get; set; }
        public string FileExtenstionType { get; set; }

        public string Status { get; set; }
        public string DocumentAlertStatus { get; set; }

        public string NoOfDays { get; set; }

        public string Remarks { get; set; }


    }
    [AutoMapTo(typeof(EmployeeDocumentInfo))]
    public class EmployeeDocumentInfoEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO EmployeeDocumentInfo Properties Missing
        public int EmployeeRefId { get; set; }
        public int DocumentInfoRefId { get; set; }
        public string DocReferenceNumber { get; set; }
        public int DefaultAlertDays { get; set; }
        public bool LifeTimeFlag { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public string FileName { get; set; }
        public string FileExtenstionType { get; set; }

    }

    public class GetEmployeeDocumentInfoInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public bool ShowResignedEmployeeAlso { get; set; }
        public bool AlertPendingFlag { get; set; }
         public bool AlertExpiredFlag { get; set; }
         public string Filter { get; set; }
        public int? EmployeeRefId { get; set; }
        public string Operation { get; set; }
        public int? DocumentInfoRefId { get; set; }

        public DateTime? StartDate { get; set; }
      public DateTime? EndDate { get; set; }
      public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetEmployeeDocumentInfoForEditOutput : IOutputDto
    {
        public EmployeeDocumentInfoEditDto EmployeeDocumentInfo { get; set; }
    }
    public class CreateOrUpdateEmployeeDocumentInfoInput : IInputDto
    {
        [Required]
        public EmployeeDocumentInfoEditDto EmployeeDocumentInfo { get; set; }
    }
    public class DocumentDto : IInputDto
    {
        public int Id { get; set; }
        public int EmployeeRefId { get; set; }

        public int DocumentInfoRefId { get; set; }
        public string Data { get; set; }
    }

    public class HealthDocumentDto : IInputDto
    {
        public string FilterName { get; set; }
        public List<PersonalInformationListDto> Employees { get; set; }
        public List<DocumentInfoListDto> Documents { get; set; }
        public List<SkillSetListDto> SkillSet { get; set; }

        public int EmployeeRefId { get; set; }
        public List<DocumentInfoListDto> DocumentInfoListDtos { get; set; }
        public int DocumentRefId { get; set; }
        public int SkillRefId { get; set; }
        public bool PendingFlag { get; set; }
    }

    public class HealthDocumentOutput : IOutputDto
    {
        public List<EmployeeDocumentInfoListDto> PendingList;

        public List<EmployeeDocumentInfoListDto> CompletedList;

        public string FileName { get; set; }

    }

	
}

