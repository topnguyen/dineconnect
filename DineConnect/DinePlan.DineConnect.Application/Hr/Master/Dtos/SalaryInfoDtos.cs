﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using DinePlan.DineConnect.Hr;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Hr.Transaction.Dtos;

namespace DinePlan.DineConnect.Hr.Master.Dtos
{
    [AutoMapFrom(typeof(SalaryInfo))]
    public class SalaryInfoListDto : FullAuditedEntityDto
    {
        public int EmployeeRefId { get; set; }
        public int SalaryMode { get; set; }
        public DateTime? WithEffectFrom { get; set; }
        public string EmployeeRefCode { get; set; }
        public string EmployeeRefName { get; set; }
        public virtual Guid? ProfilePictureId { get; set; }
        public int JobTitleRefId { get; set; }
        public string JobTitle { get; set; }
        public string CompanyRefCode { get; set; }
        public string CompanyRefName { get; set; }
        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }

        public decimal BasicSalary { get; set; }
        public decimal Levy { get; set; }
        public decimal Ot1PerHour { get; set; }
        public decimal Ot2PerHour { get; set; }
        public bool OtAllowedIfWorkTimeOver { get; set; }
        public DateTime? MaximumOTAllowedEffectiveFrom { get; set; }
        public decimal ReportOtPerHour { get; set; }
        public decimal NormalOTPerHour { get; set; }
        public DateTime? LastWorkingDate { get; set; }
        public int IncentiveAndOTCalculationPeriodRefId { get; set; }   //  
        public string IncentiveAndOTCalculationPeriodRefName { get; set; }   //  
        public decimal MaximumAllowedOTHoursOnPublicHoliday { get; set; }
        public bool ResignedFlag => LastWorkingDate == null ? false : true;
        public decimal IdleStandByCost { get; set; }
        public int? HourlySalaryModeRefId { get; set; }
        public string HourlySalaryModeRefName { get; set; }
    }

    [AutoMapTo(typeof(SalaryInfo))]
    public class SalaryInfoEditDto
    {
        public int? Id { get; set; }
        public int EmployeeRefId { get; set; }
        public string EmployeeRefCode { get; set; }
        public string EmployeeRefName { get; set; }
        
        public DateTime DateHired { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string ResidentStatus { get; set; }
        public string Race { get; set; }
        public string Religion { get; set; }
        public string JobTitle { get; set; }

        public virtual Guid? ProfilePictureId { get; set; }

        public DateTime? WithEffectFrom { get; set; }

        public int CompanyRefId { get; set; }
        public string CompanyPrefix { get; set; }
        public string CompanyRefName { get; set; }
        public int SalaryMode { get; set; }
        public decimal BasicSalary { get; set; }
        public decimal Levy { get; set; }
        public decimal Ot1PerHour { get; set; }
        public decimal Ot2PerHour { get; set; }
        public bool OtAllowedIfWorkTimeOver { get; set; }
        public decimal ReportOtPerHour { get; set; }
        public decimal NormalOTPerHour { get; set; }
        public int IncentiveAndOTCalculationPeriodRefId { get; set; }   //  
        public string IncentiveAndOTCalculationPeriodRefName { get; set; }   //  
        public DateTime? MaximumOTAllowedEffectiveFrom { get; set; }
        public int MaximumAllowedOTHoursOnPublicHoliday { get; set; }
        public decimal OverAllAllowance { get; set; }
        public decimal OverAllDeduction { get; set; }
        public decimal OverAllStatutoryAllowance { get; set; }
        public decimal OverAllStatutoryDeduction { get; set; }
        public bool DifferentFromDefaultWorkDayValues { get; set; }
        public bool DoesOtSlabWiseApplicable { get; set; }
        public bool DoesFlatOTApplicable { get; set; }
        public decimal IdleStandByCost { get; set; }
        public int? HourlySalaryModeRefId { get; set; }
    }

    [AutoMapFrom(typeof(EmployeeVsSalaryTag))]
    public class EmployeeVsSalaryTagListDto : FullAuditedEntityDto
    {
        public int IncentivePayModeId { get; set; }   //  1   Daily , 2 Monthly
        public int AllowanceOrDeductionRefId { get; set; }    //  1 Allowance Payable , 2 Deduction  Receivable, 3 Statu
        public string CompanyRefCode { get; set; }
        public int EmployeeRefId { get; set; }
        public string EmployeeCode { get; set; }
        public string EmployeeRefName { get; set; }
        public string JobTitle { get; set; }
        public int SalaryRefId { get; set; }
        public string SalaryRefName { get; set; }
        public string SalaryTagAliasName { get; set; }
        public DateTime EffectiveFrom { get; set; }
        public DateTime? RollBackFrom { get; set; }
        public decimal Amount { get; set; }
        public decimal MinimumAmount { get; set; }
        public decimal MaximumAmount { get; set; }
        public bool IsDynamicAmount { get; set; }
        public int? SalaryReferenceEntityRefId { get; set; }
        public string Remarks { get; set; }
        public string CancellationRemarks { get; set; }
        public string RemoveRemarks { get; set; }
        public int CalculationPayModeRefId { get; set; } // Daily = 1 or Monthly = 2
        public string CalculationPayModeRefName { get; set; }
        public bool IsFixedAmount { get; set; }
        public bool IsSytemGeneratedIncentive { get; set; }
        public int? FormulaRefId { get; set; }
        public bool IsItDeductableFromGrossSalary { get; set; }
        public bool IsThisStatutoryAddedWithGrossSalary { get; set; }
        public decimal AmountCalculatedForSalaryProcess { get; set; }
        public decimal IdleAmount { get; set; }
        public string IdleRemarks { get; set; }
        public bool IsCalculationFinished { get; set; }
        public bool IsPfPayable { get; set; }
        public bool IsGovernmentInsurancePayable { get; set; }
    }

    public class EmployeeSalaryTagAddOrEditDto
    {
        public EmployeeVsSalaryTagListDto EmployeeVsSalaryTag { get; set; }
    }

    public class EmployeeOTSlabAddOrEditDto
    {
        public OtBasedOnSlabListDto OtBasedOnSlab { get; set; }
    }
    public class GetSalaryInfoInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public List<CompanyListDto> CompanyList { get; set; }
        public List<SkillSetListDto> SkillSetList { get; set; }
        public List<LocationListDto> LocationList { get; set; }
        public int EmployeeRefId { get; set; }
        public string Filter { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }

    public class GetSalaryInfoForEditOutput : IOutputDto
    {
        public SalaryInfoEditDto SalaryInfo { get; set; }
        public List<SkillSetListDto> SkillSetList { get; set; }
        public List<IncentiveTagListDto> IncentiveList { get; set; }
        public List<EmployeeVsSalaryTagListDto> AllowanceSalaryTagList { get; set; }
        public List<EmployeeVsSalaryTagListDto> DeductionSalaryTagList { get; set; }
        public List<EmployeeVsSalaryTagListDto> StatutoryAllowanceList { get; set; }
        public List<EmployeeVsSalaryTagListDto> StatutoryDeductionList { get; set; }
        public List<ManualOTDayWiseRestrictionsListDto> ManualOTHoursList { get; set; }
        public List<WorkDayListDto> WorkDayListDtos { get; set; }
        public List<OtBasedOnSlabListDto> OtSlabList { get; set; }
    }

    [AutoMapFrom(typeof(OtBasedOnSlab))]
    public class OtBasedOnSlabListDto
    {
        public int Id { get; set; }
        public int EmployeeRefId { get; set; }
        public int OTRefId { get; set; }
        public decimal FromHours { get; set; }
        public decimal ToHours { get; set; }
        public decimal OTAmount { get; set; }
        public decimal FixedOTAmount { get; set; }
        public decimal NoOfHours { get; set; }
        public bool EditFlag { get; set; }
    }

    public class ManualOTDayWiseRestrictionsListDto
    {
        public int EmployeeRefId { get; set; }
        public int DayOfWeekRefId { get; set; }
        public string DayOfWeekRefName { get; set; }
        public decimal MaximumOTAllowedHours { get; set; }
    }


    public class CreateOrUpdateSalaryInfoInput : IInputDto
    {
        public int EmployeeRefId { get; set; }
        [Required]
        public SalaryInfoEditDto SalaryInfo { get; set; }

        public List<IncentiveTagListDto> IncentiveList { get; set; }

        public List<EmployeeVsSalaryTagListDto> AllowanceSalaryTagList { get; set; }
        public List<EmployeeVsSalaryTagListDto> DeductionSalaryTagList { get; set; }

        public List<EmployeeVsSalaryTagListDto> StatutoryAllowanceSalaryTagList { get; set; }
        public List<EmployeeVsSalaryTagListDto> StatutoryDeductionSalaryTagList { get; set; }

        public List<ManualOTDayWiseRestrictionsListDto> ManualOTHoursList { get; set; }

    }

    public class EurasianCFOutputDTO : IOutputDto
    {
        public int EmployeeRefId { get; set; }
        public decimal EligibleEurasionSalary { get; set; }
        public decimal EmployeeEurasianCF { get; set; }

    }

    public class IndiaPFOutputDto : IOutputDto
    {
        public int EmployeeRefId { get; set; }
        public decimal EligiblePFSalary { get; set; }   //  Maximum Ceiling 15k if company decides, otherwise full value can be taken
        public decimal EmployeePF { get; set; } //  Eligible Salary * 12 /100 
        public decimal EmployerPF { get; set; } //  Eligible Salary * 3.67 /100 
        public decimal EmployerPension { get; set; }  //  Eligible Salary * 8.33 /100
        public decimal EmployerAdminCharge { get; set; }  //  Eligible Salary * 1.10 /100
        public decimal Employee_PF_Share_Percentage { get; set; }
        public decimal Employer_PF_Share_Percentage { get; set; }
        public decimal Employer_Pension_Share_Percentage { get; set; }
        public decimal Employer_Admin_Share_Percentage { get; set; }
    }

    public class SINDAFundOutputDto : IOutputDto
    {
        public int EmployeeRefId { get; set; }
        public decimal EligbileSINDASalary { get; set; }
        public decimal EmployeeSINDA { get; set; }
    }

    public class MBMFOutputDto : IOutputDto
    {
        public int EmployeeRefId { get; set; }
        public decimal EligbileMBMFalary { get; set; }
        public decimal EmployeeMBMF { get; set; }

    }

    public class SDLOutputDto : IOutputDto
    {
        public int EmployeeRefId { get; set; }
        public decimal EmployeeSDL { get; set; }
        public decimal SDL_Percentage { get; set; }
    }
    public class IndiaESIOutputDto : IOutputDto
    {
        public int EmployeeRefId { get; set; }
        public decimal EligibleESISalary { get; set; }   //  Maximum Ceiling 21k 
        public decimal EmployeeESI { get; set; } //  Eligible Salary * 4.75 /100 
        public decimal EmployerESI { get; set; } //  Eligible Salary * 1.75 /100 
        public decimal Employee_ESI_Share_Percentage { get; set; }
        public decimal Employer_ESI_Share_Percentage { get; set; }
    }

    public class SGCPFOutputDto : IOutputDto
    {
        public int EmployeeRefId { get; set; }
        public decimal EligiblePFSalary { get; set; }
        public decimal EmployeePF { get; set; }
        public decimal EmployerPF { get; set; }
    }


    public enum SalaryCalculationType
    {
        MONTHLY,
        DAILY,
        HOURLY
    };

    public enum SalaryTags
    {
        BASIC,
        OT_VALUE_OF_THE_DAY,
        OT1,
        OT2,
        Report_OT,
        Normal_OT
    };


    public class GetDataBasedOnNullableEmployee : IInputDto
    {
        public int? EmployeeRefId { get; set; }
    }

    public class GetEmployeeWorkDayDtos : IOutputDto
    {
        public List<WorkDayListDto> WorkDayListDtos { get; set; }
        public bool DifferentFromDefaultWorkDayValues { get; set; }
    }

    public class SalaryPrepareDto : IInputDto
    {
        public int EmployeeRefId { get; set; }
        public DateTime? MonthYear { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string FileExtenstionType { get; set; }
        public int ExportTypeRefId { get; set; }    //  0 - Excel ,2 - PDF
        public string MonthYearString { get; set; }
        public bool StoreInDbFlag { get; set; }
    }


    public class GetNoOfWorkingDaysUptoGivenDateForAMonth : IInputDto
    {
        public int EmployeeRefId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime EffectiveFrom { get; set; }

        public List<WorkDayListDto> RsWorkDaysList { get; set; }

        public bool IsWeekOffAlsoConsiderForWorkDay { get; set; }

    }

    public class PerDaySalaryCost
    {
        public DateTime DateId { get; set; }
        public int? SupervisorFilter { get; set; }
        public int? InchargeFilter { get; set; }
        public int LocationRefId { get; set; }
    }
    public class SalaryPayableConsolidatedDto : IOutputDto
    {
        public int NoOfEmployees { get; set; }
        public decimal GrossSalaryAmount { get; set; }
        public decimal DeductionAmount { get; set; }
        public decimal NetSalaryAmount { get; set; }
        public decimal IdleSalaryAmount { get; set; }
        public decimal IdleSalaryPercentage { get; set; }
        public decimal AbsentLevyAmount { get; set; }
        public decimal FixedAmount { get; set; }
        public decimal TotalPayable => GrossSalaryAmount + FixedAmount;
        public List<SalaryPaidForTheMonth> EmployeeSalaryPayableList { get; set; }
        public List<SalaryPayableCostCentreWise> SalaryPayableCostCentreList { get; set; }

        public List<PieChartData> CostCentrePieGraph { get; set; }

        public List<SalaryPayableInchargeWise> SalaryPayableInchargeWiseList { get; set; }
        public List<PieChartData> InchargePieGraph { get; set; }

        public double ExecutionSeconds { get; set; }
    }

    public class SalaryPayableCostCentreWise
    {
        public string CostCentre { get; set; }
        public int NoOfEmployees { get; set; }
        public decimal GrossSalaryAmount { get; set; }
        public decimal DeductionAmount { get; set; }
        public decimal NetSalaryAmount { get; set; }
        public decimal IdleSalaryAmount { get; set; }
        public decimal IdleSalaryPercentage { get; set; }
        public decimal ExcessLevyAmount { get; set; }
        public decimal FixedAmount { get; set; }
        public decimal TotalPayable => GrossSalaryAmount + FixedAmount;
        public decimal TotalPayablePercentage { get; set; }
    }

    public class SalaryPayableInchargeWise
    {
        public int InchargeEmployeeRefId { get; set; }
        public string InchargeEmployeeRefName { get; set; }

        public int NoOfEmployees { get; set; }
        public decimal GrossSalaryAmount { get; set; }
        public decimal DeductionAmount { get; set; }
        public decimal NetSalaryAmount { get; set; }
        public decimal IdleSalaryAmount { get; set; }
        public decimal IdleSalaryPercentage { get; set; }
        public decimal ExcessLevyAmount { get; set; }
        public decimal FixedAmount { get; set; }
        public decimal TotalPayable => GrossSalaryAmount + FixedAmount;
        public decimal TotalPayablePercentage { get; set; }
    }


    public class SalaryPaidForTheMonth
    {
        public bool SuccessFlag { get; set; }
        public SalaryPrepareDto InputData { get; set; }
        public PersonalInformationListDto PersonalInformation { get; set; }
        public List<SkillSetListDto> SkillSetList { get; set; }
        public List<IncentiveTagListDto> IncentiveList { get; set; }
        public List<IncentiveConsolidatedDto> IncentiveConsolidatedList { get; set; }
        public List<EmployeeVsSalaryTagListDto> AllowanceSalaryTagList { get; set; }
        public List<EmployeeVsSalaryTagListDto> DeductionSalaryTagList { get; set; }
        public List<EmployeeVsSalaryTagListDto> StatutoryDeductionTagList { get; set; }
        public List<EmployeeVsSalaryTagListDto> StatutoryAllowanceTagList { get; set; }

        public List<ExcludedFormulaList> ExcludedDto { get; set; }

        public SalaryInfoEditDto SalaryInfo { get; set; }
        public decimal NoOfStandardWorkingDays { get; set; }
        public decimal NoOfWorkedDays { get; set; }
        public decimal NoOfPaidLeaveDays { get; set; }
        public decimal NoOfAbsentDays { get; set; }
        public decimal NoOfIdleDays { get; set; }
        public decimal NoOfPublicHolidays { get; set; }
        public decimal SalaryCalculatedDays => NoOfWorkedDays + NoOfPaidLeaveDays + NoOfPublicHolidays + NoOfIdleDays;
        public decimal BasicPay { get; set; }
        public string BasicPayRemarks { get; set; }
        public decimal GrossSalaryAmount { get; set; }
        public decimal DeductionAmount { get; set; }
        public decimal StatutoryAllowance { get; set; }
        public decimal StatutoryDeduction { get; set; }
        public decimal GrossDeduction => DeductionAmount + StatutoryDeduction;
        public decimal NetSalaryAmount { get; set; }
        public decimal NetCTC => GrossSalaryAmount + StatutoryAllowance;
        public decimal IdleSalaryAmount { get; set; }
        public string IdleSalaryRemarks { get; set; }
        public decimal IdleSalarypercentage { get; set; }
        public decimal AbsentLevyAmount { get; set; }
        public string AbsentLevyRemarks { get; set; }
        public decimal FixedAmount { get; set; }
        public decimal TotalPayable => GrossSalaryAmount + FixedAmount;
        public string Remarks { get; set; }

        public bool StoreInDbFlag { get; set; }
        public string TenantName { get; set; }



    }


    public class ExcludedFormulaList
    {
        public int IncentiveTagRefId { get; set; }
        public List<EmployeeVsSalaryTagListDto> ExcludedSalaryList { get; set; }
        public List<EmployeeVsIncentiveListDto> ExcludedIncentiveList { get; set; }
    }

    public class FormulaCalculationDoneDto
    {
        public int FormulaRefId { get; set; }
        public bool AlreadyDoneFlag { get; set; }
    }


    public class EmployeeSalaryDto
    {
        public int EmployeeRefId { get; set; }
        public int SalaryRefId { get; set; }
    }


}