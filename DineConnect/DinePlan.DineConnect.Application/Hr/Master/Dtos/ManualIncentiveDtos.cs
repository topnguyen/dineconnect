﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Hr.Transaction.Dtos;

namespace DinePlan.DineConnect.Hr.Master.Dtos
{
	[AutoMapFrom(typeof(ManualIncentive))]
	public class ManualIncentiveListDto : FullAuditedEntityDto
	{
		public DateTime IncentiveDate { get; set; }
		public int EmployeeRefId { get; set; }
		public string EmployeeRefCode { get; set; }
		public string EmployeeRefName { get; set; }
		public int IncentiveTagRefId { get; set; }
		public string IncentiveTagRefCode { get; set; }
		public decimal IncentiveAmount { get; set; }
		public decimal? ManualOtHours { get; set; }
		public string Remarks { get; set; }

		public int? RecommendedUserId { get; set; }
		public string RecommendedUserName { get; set; }

		public int? ApprovedUserId { get; set; }
		public	string ApprovedUserName { get; set; }

		public DateTime? ApprovedDate { get; set; }
	}

	[AutoMapTo(typeof(ManualIncentive))]
	public class ManualIncentiveEditDto
	{
		public int? Id { get; set; }
		public DateTime IncentiveDate { get; set; }
		public int EmployeeRefId { get; set; }
		public string EmployeeRefCode { get; set; }
		public string EmployeeRefName { get; set; }
		public int IncentiveTagRefId { get; set; }
		public string IncentiveTagRefCode { get; set; }
		public decimal IncentiveAmount { get; set; }
		public decimal? ManualOtHours { get; set; }
		public string Remarks { get; set; }
		public int? RecommendedUserId { get; set; }
		public string RecommendedUserName { get; set; }
		public int? ApprovedUserId { get; set; }
		public string ApprovedUserName { get; set; }
		public DateTime? ApprovedDate { get; set; }
	}

	public class GetManualIncentiveInput : PagedAndSortedInputDto, IShouldNormalize
	{
		public DateTime? StartDate { get; set; }
		public DateTime? EndDate { get; set; }

		public List<PersonalInformation> EmployeeList { get; set; }
		public string EmployeeRefName {get;set;}
      public List<IncentiveTagListDto> IncentiveTags { get; set; }

      public int? IncentiveTagRefId { get; set; }
		public string Filter { get; set; }

		public string Operation { get; set; }

		public void Normalize()
		{
			if (string.IsNullOrEmpty(Sorting))
			{
				Sorting = "Id Desc";
			}
		}
	}
	public class GetManualIncentiveForEditOutput : IOutputDto
	{
		public ManualIncentiveEditDto ManualIncentive { get; set; }
	}
	public class CreateOrUpdateManualIncentiveInput : IInputDto
	{
		[Required]
		public ManualIncentiveEditDto ManualIncentive { get; set; }

		public List<EmployeeWithSkillSetViewDto> EmployeeList { get; set; }
	}
}

