﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Hr.Master.Dtos
{
    [AutoMapFrom(typeof(WorkDay))]
    public class WorkDayListDto : FullAuditedEntityDto
    {
        public int? EmployeeRefId { get; set; }
        public string EmployeeRefCode { get; set; }
        public string EmployeeRefName { get; set; }
        public int DayOfWeekRefId { get; set; }
        public string DayOfWeekRefName { get; set; }
        public string DayOfWeekShortName { get; set; }
        public decimal NoOfHourWorks { get; set; }
        public decimal RestHours { get; set; }
        public decimal OtRatio { get; set; }
        public decimal NoOfWorkDays { get; set; }
        public decimal MinimumOTHoursNeededForAutoManualOTEligible { get; set; }
        public decimal MaximumOTAllowedHours { get; set; }
    }

    [AutoMapTo(typeof(WorkDay))]
    public class WorkDayEditDto
    {
        public int? Id { get; set; }
        public int? EmployeeRefId { get; set; }
        public int DayOfWeekRefId { get; set; }
        public string DayOfWeekRefName { get; set; }
        public string DayOfWeekShortName { get; set; }
        public decimal NoOfHourWorks { get; set; }
        public decimal RestHours { get; set; }
        public decimal OtRatio { get; set; }
        public decimal NoOfWorkDays { get; set; }
        public decimal MinimumOTHoursNeededForAutoManualOTEligible { get; set; }
        public decimal MaximumOTAllowedHours { get; set; }
    }
    public class GetWorkDayInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int? EmployeeRefId { get; set; }
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
    public class GetWorkDayForEditOutput : IOutputDto
    {
        public WorkDayEditDto WorkDay { get; set; }
    }
    public class CreateOrUpdateWorkDayInput : IInputDto
    {
        [Required]
        public WorkDayEditDto WorkDay { get; set; }
    }

    public class Get_WorkDayHours_With_OT_Value : IOutputDto
    {
        public decimal FullWorkHours { get; set; }
        public string OtTag { get; set; }
    }
}

