﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Hr.Master.Dtos
{
    public enum CheckType
	{
		TimeIn=0,			
		TimeOut=1,
		BreakOut=2,
		BreakIn=3,
		OnDutyOut=4,
		OnDutyIn=5
	}

	public class ApiMessageReturn : IOutputDto
	{
		public virtual int Id { get; set; }
		public virtual string ErrorMessage { get; set; }
	}
}
