﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Hr.Master.Dtos
{
    [AutoMapFrom(typeof(AttendanceLog))]
    public class AttendanceLogListDto : FullAuditedEntityDto
    {
		public int? AttendanceMachineLocationRefId { get; set; }
		public string AttendanceMachineLocationRefName { get; set; }
        public string BioMetricCode { get; set; }
        public int EmployeeRefId { get; set; }
        public string EmployeeRefCode { get; set; }
        public string EmployeeRefName { get; set; }
        public DateTime? CheckTime { get; set; }
        public string CheckTypeCode { get; set; }
        public int VerifyMode { get; set; }
        public string AttendanceStatus { get; set; }        
        public int? AuthorisedBy { get; set; }

        public string AuthorisedByWhom { get; set; }
        public string ReasonForDeletion { get; set; }
        public bool GpsAttendanceList { get; set; }
        public GpsAttendanceListDto GpsDetails { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Altitude { get; set; }
        public string IsRange { get; set; }
        public double GpsDistance { get; set; }
        public string RangeErrorMessage { get; set; }
        public int TenantId { get; set; }
    }

    [AutoMapTo(typeof(AttendanceLog))]
    public class AttendanceLogEditDto
    {
		public int AttendanceMachineLocationRefId { get; set; }
		public string AttendanceMachineLocationRefName { get; set; }
		public int? Id { get; set; }
        public int LocationRefId { get; set; }
        public int EmployeeRefId { get; set; }
        public string BioMetricCode { get; set; }
        public DateTime? CheckTime { get; set; }
        public string CheckTypeCode { get; set; }
        public int CheckType { get; set; }

        public int VerifyMode { get; set; }
        public int? AuthorisedBy { get; set; }
        public string ReasonForDeletion { get; set; }
        public bool Deleted { get; set; }
    }
    public class ApiEmployeeAttendanceDto
    {
        public int? Id { get; set; }
        public int LocationRefId { get; set; }
        public int EmployeeRefId { get; set; }
        public DateTime? CheckTime { get; set; }
        public int CheckType { get; set; }
        public int TenantId { get; set; }

        public bool Deleted { get; set; }
    }

    public class GetAttendanceLogInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int EmployeeRefId { get; set; }
        public string Filter { get; set; }
        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CheckTime Desc";
            }
        }
    }
    public class GetAttendanceLogForEditOutput : IOutputDto
    {
        public AttendanceLogEditDto AttendanceLog { get; set; }
    }
    public class CreateOrUpdateAttendanceLogInput : IInputDto
    {
        [Required]
        public AttendanceLogEditDto AttendanceLog { get; set; }
    }

	public enum AttendanceStatus
	{
		IN = 1,
		OUT = 2,
		BREAKOUT = 3,
		BREAKIN = 4,
		LIVE = 5
	}

	public class AttendanceParticularDateList
	{
		public string BioMetricCode { get; set; }
		public int EmployeeRefId { get; set; }
		public string EmployeeRefCode { get; set; }
		public string EmployeeRefName { get; set; }
		public bool NightFlag { get; set; }
		public DateTime WorkDate { get; set; }
		public string TimeList { get; set; }

        public List<AttendanceLogListDto> AttendanceLogListDtos { get; set; }

        public decimal Salary { get; set; }
        public decimal WorkHoursPerDay { get; set; }
        public string WorkHoursPerDayWithMinutes { get; set; }
        public decimal RestHours { get; set; }
        public decimal OtRatio { get; set; }
        public decimal MaximumOTAllowedHours { get; set; }

        public DateTime? InTime { get; set; }
        public DateTime? OutTime { get; set; }
        public DateTime? BreakOutTime { get; set; }
        public DateTime? BreakInTime { get; set; }
        public decimal ActualWorkHours { get; set; }
        public string ActualWorkHoursWithMinutes { get; set; }
        public decimal ShortageWorkHours { get; set; }
        public string ShortageWorkHoursInMinutes { get; set; }
        public decimal OTWorkHours { get; set; }
        public string OTWorkHoursInMinutes { get; set; }
        public decimal StandardSalaryCurrentDay { get; set; }
        public decimal ActualSalaryCurrentDay { get; set; }
        public decimal ShortageAmount { get; set; }
        public decimal OtAmount { get; set; }
        public string Remarks { get; set; }

	}
    public class GpsRangeFromClientLocation
    {
        public bool IsRange { get; set; }
        public double DistanceFromClient { get; set; }
        public string ErrorMessage { get; set; }
    }
    public class GetGpsRangeFromClientLocation
    {
        public int EmployeeRefId { get; set; }
        public DateTime? Startdate { get; set; }
        public DateTime? Enddate { get; set; }
        public GpsAttendanceListDto GpsDetails { get; set; }
        public int? GpsAttendanceRefId { get; set; }
    }

    public class DeleteAttendanceLog : IInputDto
    {
        public int Id { get; set; }
        public int EmployeeRefId { get; set; }

        public string AttMode { get; set; }
    }
}

