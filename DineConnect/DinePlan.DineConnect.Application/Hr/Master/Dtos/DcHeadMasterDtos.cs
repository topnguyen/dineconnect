﻿

using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;

namespace DinePlan.DineConnect.Hr.Master.Dtos
{
    [AutoMapFrom(typeof(DcHeadMaster))]
    public class DcHeadMasterListDto : FullAuditedEntityDto
    {
        public string HeadName { get; set; }
        public bool IsActive { get; set; }
    }
    [AutoMapTo(typeof(DcHeadMaster))]
    public class DcHeadMasterEditDto
    {
        public int? Id { get; set; }
        public string HeadName { get; set; }
        public bool IsActive { get; set; }
    }

    public class GetDcHeadMasterInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
    public class GetDcHeadMasterForEditOutput : IOutputDto
    {
        public DcHeadMasterEditDto DcHeadMaster { get; set; }
    }
    public class CreateOrUpdateDcHeadMasterInput : IInputDto
    {
        [Required]
        public DcHeadMasterEditDto DcHeadMaster { get; set; }
    }
}

