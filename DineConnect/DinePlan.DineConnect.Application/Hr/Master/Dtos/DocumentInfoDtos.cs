﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Hr.Master.Dtos
{
    [AutoMapFrom(typeof(DocumentInfo))]
    public class DocumentInfoListDto : FullAuditedEntityDto
    {
        public string DocumentName { get; set; }
        public DocumentType DocumentType { get; set; }
        public string Document { get; set; }
        public bool LifeTimeFlag { get; set; }
        public int DefaultAlertDays { get; set; }
        public int DefaultExpiryDays { get; set; }
    }

    [AutoMapTo(typeof(DocumentForSkillSet))]
    public class DocumentForSkillSetEditDto
    {
        public int? Id { get; set; }
        public DocumentType DocumentType { get; set; }
        public int DocumentRefId { get; set; }
        public string DocumentRefName { get; set; }
        public int SkillSetRefId { get; set; }
        public string SkillSetRefName { get; set; }
    }

    [AutoMapTo(typeof(DocumentInfo))]
    public class DocumentInfoEditDto
    {
        public int? Id { get; set; }
        public DocumentType DocumentType { get; set; }
        public string DocumentName { get; set; }

        public bool LifeTimeFlag { get; set; }

        public int DefaultAlertDays { get; set; }
        public int DefaultExpiryDays { get; set; }

    }

    public class GetDocumentInfoInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public string Operation { get; set; }
        public DocumentType? DocumentType { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetDocumentInfoForEditOutput : IOutputDto
    {
        public DocumentInfoEditDto DocumentInfo { get; set; }
        public List<ComboboxItemDto> SkillSetList { get; set; }
    }

    public class CreateOrUpdateDocumentInfoInput : IInputDto
    {
        [Required]
        public DocumentInfoEditDto DocumentInfo { get; set; }
        public List<ComboboxItemDto> SkillSetList { get; set; }
    }

}

