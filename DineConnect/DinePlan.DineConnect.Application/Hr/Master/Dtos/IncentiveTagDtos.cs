﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Hr.Transaction.Dtos
{
	[AutoMapFrom(typeof(IncentiveCategory))]
	public class IncentiveCategoryListDto : FullAuditedEntityDto
	{
		public string IncentiveCategoryCode { get; set; }

		[MaxLength(50)]
		public string IncentiveCategoryName { get; set; }

		public bool BasedOnSkillSet { get; set; }   //	Either one should be true
		public bool BasedOnQuantity { get; set; }   //	Either One should be true
		public decimal MaximumIncenitivePerQuanityPerTimeSheet { get; set; } //	-1 for No measurements

	}

	[AutoMapTo(typeof(IncentiveCategory))]
	public class IncentiveCategoryEditDto
	{
		public int? Id { get; set; }

		public string IncentiveCategoryCode { get; set; }

		[MaxLength(50)]
		public string IncentiveCategoryName { get; set; }

		public bool BasedOnSkillSet { get; set; }   //	Either one should be true
		public bool BasedOnQuantity { get; set; }   //	Either One should be true
		public decimal MaximumIncenitivePerQuanityPerTimeSheet { get; set; } //	-1 for No measurements

	}

	public class GetIncentiveCategoryInput : PagedAndSortedInputDto, IShouldNormalize
	{
		public string Filter { get; set; }

		public string Operation { get; set; }

		public void Normalize()
		{
			if (string.IsNullOrEmpty(Sorting))
			{
				Sorting = "Id";
			}
		}
	}

	public class GetIncentiveCategoryForEditOutput : IOutputDto
	{
		public IncentiveCategoryEditDto IncentiveCategory { get; set; }
	}
	public class CreateOrUpdateIncentiveCategoryInput : IInputDto
	{
		[Required]
		public IncentiveCategoryEditDto IncentiveCategory { get; set; }
	}


	[AutoMapFrom(typeof(IncentiveTag))]
	public class IncentiveTagListDto : FullAuditedEntityDto
	{
        public int IncentivePayModeId { get; set; }   //  1   Daily , 2 Monthly
        public string IncentivePayModeRefName { get; set; }
        public int AllowanceOrDeductionRefId { get; set; }    //  1 Allowance Payable , 2 Deduction  Receivable
        public string AllowanceOrDeductionRefName { get; set; }
        public int IncentiveCategoryRefId { get; set; }
		public string IncentiveCategoryRefName { get; set; }
		
		[MaxLength(30)]
		public string IncentiveTagCode { get; set; }
		public string IncentiveTagName { get; set; }

		public string IncentiveDescription { get; set; }

		public bool HoursBased { get; set; }
		public bool FullWorkHours { get; set; }
		public decimal MinimumHours { get; set; }
		public decimal MinimumOtHours { get; set; }
		public decimal MinimumClaimedHours { get; set; }
		public bool IncludeInConsolidationPerDay { get; set; }
		public decimal IncentiveAmount { get; set; }
		public bool IncenitveBasedOnSalaryTags { get; set; }
		public int? SalaryTagRefId { get; set; }
		public decimal? ProportianteValueOfSalaryTags { get; set; }
		public bool OverTimeBasedIncentive { get; set; }
		public bool DisplayNotApplicable { get; set; }
		public bool HourlyClaimableTimeSheets { get; set; }

		public int IncentiveEntryMode { get; set; }
		public decimal MinRange { get; set; }
		public decimal MaxRange { get; set; }
		public bool CanIssueToIdleStatus { get; set; }
		public bool ActiveStatus { get; set; }
		public bool CanBeAssignedToAllEmployees { get; set; }
		public bool OnlyOnePersonOfTheTeamCanReceived { get; set; }
		public bool ProjectCostCentreMandatory { get; set; }
		public bool NdtProcedureMandatory { get; set; }
		public bool TimeSheetRequiredForGivenDate { get; set; }
        public bool IsMaterialQuantityBasedIncentiveFlag { get; set; }
        public bool IsCertificateMandatory { get; set; }
        public string CalculationFormula { get; set; }
        public int? FormulaRefId { get; set; }
        public string FormulaRefName { get; set; }
        public bool AnySpecialFormulaRefId { get; set; }
        public decimal CalculationFormulaMinimumValue { get; set; }
        public decimal CalculationFormulaMaximumValue { get; set; }
        public bool OnlyAllowedOnWorkDay { get; set; }
        public bool WorkDayHourBasedIncentive { get; set; }
        public bool DoesIssueOnlyForBillableEmployee { get; set; }
        public bool IsSytemGeneratedIncentive { get; set; }
        public bool IsItDeductableFromGrossSalary { get; set; }
        public bool IsFixedAmount { get; set; }
        public bool IsBulkUpdateAllowed { get; set; }
        public bool IsPfPayable { get; set; }
        public bool IsGovernmentInsurancePayable { get; set; }

    }

    [AutoMapTo(typeof(IncentiveTag))]
	public class IncentiveTagEditDto
	{
		public int? Id { get; set; }

        public int IncentivePayModeId { get; set; }   //  1   Daily , 2 Monthly
        public int AllowanceOrDeductionRefId { get; set; }    //  1 Allowance Payable , 2 Deduction  Receivable

        public int IncentiveCategoryRefId { get; set; }
		
		[MaxLength(30)]
		public string IncentiveTagCode { get; set; }

		[MaxLength(50)]
		public string IncentiveTagName { get; set; }

		public string IncentiveDescription { get; set; }

		public bool HoursBased { get; set; }
		public bool FullWorkHours { get; set; }
		public decimal MinimumHours { get; set; }
		public decimal MinimumOtHours { get; set; }
		public decimal MinimumClaimedHours { get; set; }
		public bool IncludeInConsolidationPerDay { get; set; }
		public decimal IncentiveAmount { get; set; }
		public bool IncenitveBasedOnSalaryTags { get; set; }
		
		public int? SalaryTagRefId { get; set; }
		public decimal? ProportianteValueOfSalaryTags { get; set; }
		public bool OverTimeBasedIncentive { get; set; }
		public bool DisplayNotApplicable { get; set; }
		public bool HourlyClaimableTimeSheets { get; set; }
		public int IncentiveEntryMode { get; set; }
		public decimal MinRange { get; set; }
		public decimal MaxRange { get; set; }
		public bool CanIssueToIdleStatus { get; set; }
		public bool ActiveStatus { get; set; }
		public bool CanBeAssignedToAllEmployees { get; set; }
		public bool OnlyOnePersonOfTheTeamCanReceived { get; set; }
		public bool ProjectCostCentreMandatory { get; set; }
		public bool NdtProcedureMandatory { get; set; }
		public bool TimeSheetRequiredForGivenDate { get; set; }
        public bool IsMaterialQuantityBasedIncentiveFlag { get; set; }
        public bool IsCertificateMandatory { get; set; }
        public string CalculationFormula { get; set; }
        public int? FormulaRefId { get; set; }
        public bool AnySpecialFormulaRefId { get; set; }
        public decimal CalculationFormulaMinimumValue { get; set; }
        public decimal CalculationFormulaMaximumValue { get; set; }
        public bool OnlyAllowedOnWorkDay { get; set; }
        public bool WorkDayHourBasedIncentive { get; set; }
        public bool DoesIssueOnlyForBillableEmployee { get; set; }
        public bool IsSytemGeneratedIncentive { get; set; }
        public bool IsItDeductableFromGrossSalary { get; set; }
        public bool IsFixedAmount { get; set; }
        public bool IsBulkUpdateAllowed { get; set; }
        public bool IsPfPayable { get; set; }
        public bool IsGovernmentInsurancePayable { get; set; }

    }

    public class GetIncentiveTagInput : PagedAndSortedInputDto, IShouldNormalize
	{
		public string Filter { get; set; }

		public string Operation { get; set; }

		public void Normalize()
		{
			if (string.IsNullOrEmpty(Sorting))
			{
				Sorting = "Id Desc";
			}
		}
	}

	
	public class GetIncentiveTagForEditOutput : IOutputDto
	{
        public IncentiveTagEditDto IncentiveTag { get; set; }
        public List<IncentiveVsEntityListDto> IncentiveVsEntityList { get; set; }
        public List<IncentivePersonalInformationListDto> EmployeeList { get; set; }
    }

	public class CreateOrUpdateIncentiveTagInput : IInputDto
	{
		[Required]
		public IncentiveTagEditDto IncentiveTag { get; set; }
		public List<IncentiveVsEntityListDto> IncentiveVsEntityList { get; set; }
	}

    public class EmployeeIncentiveDto
    {
        public int EmployeeRefId { get; set; }
        public int IncentiveRefId { get; set; }
    }

    [AutoMapFrom(typeof(PersonalInformation))]
    public class IncentivePersonalInformationListDto
    {
        public int Id { get; set; }
        public int CompanyRefId { get; set; }
        public string CompanyRefName { get; set; }
        public string CompanyRefCode { get; set; }
        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeCode { get; set; }
        public string EmployeeAliasNameForReport { get; set; }
        public string JobTitle { get; set; }
        public string SkillSetList { get; set; }
        public DateTime DateHired { get; set; }
        public string StaffType { get; set; }
        public string FINNumber { get; set; }
        public bool IsAttendanceRequired { get; set; }
        public bool IsBillableAttendanceRequired { get; set; }
        public bool IncentiveCanBeDeleted { get; set; }
        public string RelatedWithSalaryOrIncentive { get; set; }
    }

    [AutoMapFrom(typeof(IncentiveVsEntity))]
	public class IncentiveVsEntityListDto : FullAuditedEntityDto
	{
		public int IncentiveTagRefId { get; set; }
		public string IncentiveTagRefName { get; set; }
		public int EntityType { get; set; }
		public string EntityTypeRefName { get; set; }
		public int EntityRefId { get; set; }    //	0	For All
		public bool ExcludeFlag { get; set; }
        public bool EitherOr_ForSameEntity { get; set; }
    }


    public class IncentiveConsolidatedDto : IOutputDto
    {
        public int IncentiveTagCategoryRefId { get; set; }
        public string IncentiveTagCategoryRefCode { get; set; }
        public int IncentiveTagRefId { get; set; }
        public string IncentiveTagRefCode { get; set; }
        public decimal AmountPerUnit { get; set; }
        public decimal OverAllManualOtHours { get; set; }
        public decimal Count { get; set; }
        public decimal TotalIncentiveAmount { get; set; }
        public string Remarks { get; set; }
        public bool IsPfPayable { get; set; }
        public bool IsGovernmentInsurancePayable { get; set; }

    }


    public class NullableEmployeeId : IInputDto
    {
        public int? EmployeeRefId { get; set; }
    }
}