﻿
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Hr.Master.Dtos
{
    [AutoMapFrom(typeof(UserDefaultInformation))]
    public class UserDefaultInformationListDto : FullAuditedEntityDto
    {
        public virtual long UserId { get; set; }
        public virtual string UserName { get; set; }
        public virtual int? EmployeeRefId { get; set; }
        public string EmployeeRefName { get; set; }
        public string Roles { get; set; }
   }

    [AutoMapTo(typeof(UserDefaultInformation))]
    public class UserDefaultInformationEditDto
    {
        public int? Id { get; set; }
        public virtual long UserId { get; set; }
        public virtual int? EmployeeRefId { get; set; }
        //public List<UserVsCostCentreListDto> ProjectCostCentreList { get; set; }
        
    }

   public class GetUserDefaultInformationInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public bool EmployeeSearchFlag { get; set; }
        public string Filter { get; set; }
        public string Operation { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "EmployeeRefName";
            }
        }
    }
    public class GetUserDefaultInformationForEditOutput : IOutputDto
    {
        public UserDefaultInformationEditDto UserDefaultInformation { get; set; }
    }
    public class CreateOrUpdateUserDefaultInformationInput : IInputDto
    {
        [Required]
        public UserDefaultInformationEditDto UserDefaultInformation { get; set; }
    }
}

