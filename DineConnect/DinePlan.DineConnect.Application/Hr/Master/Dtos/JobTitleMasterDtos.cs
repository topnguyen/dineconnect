﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Hr.Master.Dtos
{
    [AutoMapFrom(typeof(JobTitleMaster))]
    public class JobTitleMasterListDto : FullAuditedEntityDto
    {
        public string JobTitle { get; set; }
    }
    [AutoMapTo(typeof(JobTitleMaster))]
    public class JobTitleMasterEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO JobTitleMaster Properties Missing
        public string JobTitle { get; set; }
    }

    public class GetJobTitleMasterInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
    public class GetJobTitleMasterForEditOutput : IOutputDto
    {
        public JobTitleMasterEditDto JobTitleMaster { get; set; }
    }
    public class CreateOrUpdateJobTitleMasterInput : IInputDto
    {
        [Required]
        public JobTitleMasterEditDto JobTitleMaster { get; set; }
    }
}
