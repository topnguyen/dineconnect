﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Hr.Master.Dtos
{
    [AutoMapFrom(typeof(DcGroupMaster))]
    public class DcGroupMasterListDto : FullAuditedEntityDto
    {
        public int DcHeadRefId { get; set; }

        public string DcHeadRefName { get; set; }

        public string GroupName { get; set; }

        public decimal PriorityLevel { get; set; }

        public bool IsActive { get; set; }

        public string SkillRefName { get; set; }

        public List<SkillSet> SkillSets { get; set; }
        
    }

    [AutoMapTo(typeof(DcGroupMaster))]
    public class DcGroupMasterEditDto
    {
        public int? Id { get; set; }
        public int DcHeadRefId { get; set; }

        public string GroupName { get; set; }

        public decimal PriorityLevel { get; set; }

        public bool IsActive { get; set; }

    }

    public class GetDcGroupMasterInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string HeadName { get; set; }
        
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
    public class GetDcGroupMasterForEditOutput : IOutputDto
    {
        public DcGroupMasterEditDto DcGroupMaster { get; set; }
        public List<ComboboxItemDto> LinkedSkillSetList { get; set; }
    }
    public class CreateOrUpdateDcGroupMasterInput : IInputDto
    {
        [Required]
        public DcGroupMasterEditDto DcGroupMaster { get; set; }
        public List<ComboboxItemDto> LinkedSkillSetList { get; set; }

    }


    [AutoMapTo(typeof(DcGroupMasterVsSkillSet))]
    public class DcGroupMasterVsSkillSetListDto
    {
        public int DcGropupRefId { get; set; }

        public string GroupName { get; set; }

        public int SkillSetRefId { get; set; }
        public string SkillSetRefName { get; set; }
    }

    public class HeadCodeDto
    {
        public int DcHeadRefId { get; set; }
    }

    public class GroupCodeDto
    {
        public int DcGroupRefId { get; set; }
    }

    public class LocationGroupCodeDto
    {
        public int LocationRefId { get; set; }
        public int DcGroupRefId { get; set; }
    }

    public class LocationStationCodeDto
    {
        public int LocationRefId { get; set; }
        public int StationRefId { get; set; }
    }


    public class SkillCodeDto
    {
        public int SkillRefId { get; set; }
    }
}

