﻿
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Hr.Inventory.Dtos
{
    [AutoMapFrom(typeof(YearWiseLeaveAllowedForEmployee))]
    public class YearWiseLeaveAllowedForEmployeeListDto : FullAuditedEntityDto
    {
        //TODO: DTO YearWiseLeaveAllowedForEmployee Properties Missing
        //YOU CAN REFER ATTRIBUTES IN 
        public int AcYear { get; set; }
        public DateTime LeaveAcYearFrom { get; set; }
        public DateTime LeaveAcYearTo { get; set; }
        public int EmployeeRefId { get; set; }
        public string EmployeeRefName { get; set; }
        public int LeaveTypeRefCode { get; set; }
        public string LeaveTypeRefName { get; set; }
        public Decimal NumberOfLeaveAllowed { get; set; }
    }

    [AutoMapTo(typeof(YearWiseLeaveAllowedForEmployee))]
    public class YearWiseLeaveAllowedForEmployeeEditDto
    {
        public int? Id { get; set; }
        public int AcYear { get; set; }
        public DateTime LeaveAcYearFrom { get; set; }
        public DateTime LeaveAcYearTo { get; set; }
        public int EmployeeRefId { get; set; }
        public string EmployeeRefName { get; set; }
        public int LeaveTypeRefCode { get; set; }
        public string LeaveTypeRefName { get; set; }
        public decimal MaximumLeaveAllowed { get; set; }
        public decimal NumberOfLeaveAllowed { get; set; }
        public decimal NumberOfLeaveTaken { get; set; }
        public decimal NumberOfLeaveRemaining => NumberOfLeaveAllowed - NumberOfLeaveTaken;
        public string Gender { get; set; }
        public bool AllowedForMarriedEmployeesOnly { get; set; }
        public string MaritalStatus { get; set; }
    }

    public class GetYearWiseLeaveAllowedForEmployeeInput : PagedAndSortedInputDto, IShouldNormalize
    {

        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }


    public class GetYearWiseLeaveAllowedForEmployeeForEditOutput : IOutputDto
    {
        public List<YearWiseLeaveAllowedForEmployeeEditDto> YearWiseLeaveAllowedForEmployee { get; set; }
    }
    public class CreateOrUpdateYearWiseLeaveAllowedForEmployeeInput : IInputDto
    {
        [Required]
        public int EmployeeRefId { get; set; }
        public int AcYear { get; set; }

        [Required]
        public List<YearWiseLeaveAllowedForEmployeeEditDto> YearWiseLeaveAllowedForEmployee { get; set; }
    }

    public class YearWithEmployeeDto
    {
        public int EmployeeRefId { get; set; }
        public int AcYear { get; set; }
    }
}