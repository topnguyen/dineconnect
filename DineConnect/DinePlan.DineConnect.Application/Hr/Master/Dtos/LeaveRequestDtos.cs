﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Hr.Transaction.Dtos
{
    [AutoMapFrom(typeof(LeaveRequest))]
    public class LeaveRequestListDto : FullAuditedEntityDto
    {
        //TODO: DTO LeaveRequest Properties Missing
        //YOU CAN REFER ATTRIBUTES IN
        public string CompanyRefCode { get; set; }
        public string LocationRefCode { get; set; }
        public int EmployeeRefId { get; set; }
        public string EmployeeRefCode { get; set; }
        public string EmployeeRefName { get; set; }
        public string Gender { get; set; }
        public int LeaveTypeRefCode { get; set; }
        public string LeaveTypeRefName { get; set; }
        public DateTime DateOfApply { get; set; }
        public DateTime LeaveFrom { get; set; }
        public DateTime LeaveTo { get; set; }
        public Decimal TotalNumberOfDays { get; set; }
        public string LeaveReason { get; set; }
        public string LeaveStatus { get; set; }
        public string HandOverWorkTo { get; set; }
        public string ContactOfWorkIncharge { get; set; }
        public bool HalfDayFlag { get; set; }
        public string HalfDayTimeSpecification { get; set; }

        public string NationalIdentificationNumber { get; set; }
        public string FileName { get; set; }
        public string FileExtenstionType { get; set; }
        public bool IsSupportingDocumentsOverRide { get; set; }
        public int? SupportingDocumentsOverRideUserId { get; set; }
        public int? CommonMailMessageRefId { get; set; }
        public bool DoesSalaryPayableForThisLeaveRequest { get; set; }
    }
    public class LeaveStatusListDto
    {
        public int LeaveTypeRefCode { get; set; }
        public string LeaveTypeName { get; set; }
        public string LeaveTypeShortName { get; set; }
        public decimal NumberOfLeaveAllowed { get; set; }
        public decimal NumberOfLeaveTaken { get; set; }
        public decimal NumberOfLeaveRemaining { get; set; }
    }
    public class LeaveAlreadyTaken
    {
        public bool AlreadyAppliedFlag { get; set; }
        public string ErrorMessage { get; set; }
    }

    [AutoMapTo(typeof(LeaveRequest))]
    public class LeaveRequestEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO LeaveRequest Properties Missing
        public int EmployeeRefId { get; set; }
        public string EmployeeRefName { get; set; }
        public string Gender { get; set; }
        public int LeaveTypeRefCode { get; set; }
        public string LeaveTypeRefName { get; set; }
        public DateTime DateOfApply { get; set; }
        public DateTime LeaveFrom { get; set; }
        public DateTime LeaveTo { get; set; }
        public Decimal TotalNumberOfDays { get; set; }
        public string LeaveReason { get; set; }
        public string LeaveStatus { get; set; }
        public string HandOverWorkTo { get; set; }
        public string ContactOfWorkIncharge { get; set; }
        public bool HalfDayFlag { get; set; }
        public string HalfDayTimeSpecification { get; set; }
        public int? ApprovedPersonRefId { get; set; }

        public DateTime? ApprovedTime { get; set; }
        public string ApprovedRemarks { get; set; }
        public string FileName { get; set; }
        public string FileExtenstionType { get; set; }
        public bool IsSupportingDocumentsOverRide { get; set; }
        public bool IsDocumentRequired { get; set; }
        public int? SupportingDocumentsOverRideUserId { get; set; }
        public int? CommonMailMessageRefId { get; set; }
        public bool DoesSalaryPayableForThisLeaveRequest { get; set; }


    }

    public class GetLeaveRequestInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int? Id { get; set; }
        public string Filter { get; set; }
        public int EmployeeRefId { get; set; }
        public string Operation { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public int? AcYear { get; set; }
        public int? LeaveTypeRefCode { get; set; }
        public bool OmitRejected { get; set; }
        public decimal MinimumDays { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetLeaveStatusInput
    {
        public int EmployeeRefId { get; set; }
        public DateTime LeaveYear { get; set; }
    }
    public class GetLeaveRequestForEditOutput : IOutputDto
    {
        public LeaveRequestEditDto LeaveRequest { get; set; }
    }
    public class CreateOrUpdateLeaveRequestInput : IInputDto
    {
        [Required]
        public LeaveRequestEditDto LeaveRequest { get; set; }

        public int? AuthorizationId { get; set; }
    }

    public class GetLeaveEmployeesWithToDate : IInputDto
    {
        public DateTime LeaveToDate { get; set; }
        public decimal NoOfDaysMinimum { get; set; }
    }

    public class LeaveCalculationDto : IInputDto
    {
        public int EmployeeRefId { get; set; }
        public DateTime LeaveFrom { get; set; }
        public DateTime LeaveTo { get; set; }
    }

    public class DateWithWorkDayDto : IOutputDto
    {
        public DateTime WorkDate { get; set; }
        public decimal WorkDay { get; set; }
    }
    public class LeaveDocumentDto : IInputDto
    {
        public int Id { get; set; }
        public int LeaveRequestRefId { get; set; }

        public int DocumentInfoRefId { get; set; }
        public string Data { get; set; }
    }
    public class SendLeaveRequestMailDto
    {
        public LeaveRequestEditDto LeaveRequest { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeEmail { get; set; }
        public string CompanyEmailIdList { get; set; }
        public string InchargePersonName { get; set; }
        public string InchargePersonEmail { get; set; }
        public string CompanyName { get; set; }

    }

    public enum LeaveTypeStatusEnum
    {
        Pending,
        Approved,
        Rejected,
        WaitingForDocument
    }

}

