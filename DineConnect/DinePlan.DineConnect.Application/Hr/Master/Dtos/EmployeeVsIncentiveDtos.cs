﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Hr.Master.Dtos
{
	[AutoMapFrom(typeof(EmployeeVsIncentive))]
	public class EmployeeVsIncentiveListDto : FullAuditedEntityDto
	{
        public DateTime IncentiveDate { get; set; }
        public int EmployeeRefId { get; set; }
		public string EmployeeRefCode { get; set; }
		public string EmployeeRefName { get; set; }
		public int IncentiveTagRefId { get; set; }
		public string IncentiveTagRefCode { get; set; }
		public DateTime EffectiveFrom { get; set; }
		public DateTime? RollBackFrom { get; set; }
		public bool IsActive => RollBackFrom == null ? true : false;
		public decimal IncentiveAmount { get; set; }
		public decimal? ManualOtHours { get; set; }
		public string Remarks { get; set; }
		public bool DisplayNotApplicable { get; set; }
		public bool IncludeInConsolidationPerDay { get; set; }
		public int IncentiveEntryMode { get; set; } 
		public string IncentiveEntryModeRefName { get; set; }
        public decimal MaterialQuantityUsed { get; set; }
        public decimal Amount { get; set; }
        public string ReasonRemarks { get; set; }
        public string RemoveRemarks { get; set; }

    }

	[AutoMapTo(typeof(EmployeeVsIncentive))]
	public class EmployeeVsIncentiveEditDto
	{
		public int? Id { get; set; }
		public int EmployeeRefId { get; set; }
		public int IncentiveTagRefId { get; set; }
		public DateTime EffectiveFrom { get; set; }
		public DateTime? RollBackFrom { get; set; }
        public decimal Amount { get; set; }
        public string ReasonRemarks { get; set; }
        public string RemoveRemarks { get; set; }
    }

	public class GetEmployeeVsIncentiveInput : PagedAndSortedInputDto, IShouldNormalize
	{
		public string Filter { get; set; }

		public string Operation { get; set; }

		public void Normalize()
		{
			if (string.IsNullOrEmpty(Sorting))
			{
				Sorting = "EmployeeRefName";
			}
		}
	}
	public class GetEmployeeVsIncentiveForEditOutput : IOutputDto
	{
		public EmployeeVsIncentiveEditDto EmployeeVsIncentive { get; set; }
	}
	public class CreateOrUpdateEmployeeVsIncentiveInput : IInputDto
	{
		[Required]
		public EmployeeVsIncentiveEditDto EmployeeVsIncentive { get; set; }
	}

	public class OtMinuteFlags 
	{
		public int  Minute { get; set; }
		public bool WorkFlag { get; set; }
	}

}
