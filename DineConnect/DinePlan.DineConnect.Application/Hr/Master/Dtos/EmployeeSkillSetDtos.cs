﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Hr.Master.Dtos
{
    [AutoMapFrom(typeof(EmployeeSkillSet))]
    public class EmployeeSkillSetListDto : FullAuditedEntityDto
    {
        //TODO: DTO EmployeeSkillSet Properties Missing
        //YOU CAN REFER ATTRIBUTES IN 
        public int EmployeeRefId { get; set; }
        public string EmployeeRefName { get; set; }
        public int SkillSetRefId { get; set; }
        public string SkillSetRefCode{ get; set; }
    }

    public class EmployeeSkillSetViewDto : FullAuditedEntityDto
    {
        public int EmployeeRefId { get; set; }
        public string EmployeeRefName { get; set; }
        public string SkillSetRefCode { get; set; }
        public bool ActiveStatus { get; set; }
    }

    [AutoMapTo(typeof(EmployeeSkillSet))]
    public class EmployeeSkillSetEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO EmployeeSkillSet Properties Missing
        public int EmployeeRefId { get; set; }
        public string EmployeeRefName { get; set; }
        public int SkillSetRefId { get; set; }
        public string SkillSetRefCode { get; set; }
        public List<SkillSetEditDto> Skills;
    }

    public class GetEmployeeSkillSetInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }
        public string SkillFilter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetEmployeeSkillSetForEditOutput : IOutputDto
    {
        public List<ComboboxItemDto> SkillSetList { get; set; }
        public EmployeeSkillSetEditDto EmployeeSkillSet { get; set; }
    }

    public class CreateOrUpdateEmployeeSkillSetInput : IInputDto
    {
        public List<ComboboxItemDto> SkillSetList { get; set; }

        [Required]
        public EmployeeSkillSetEditDto EmployeeSkillSet { get; set; }
    }
}
