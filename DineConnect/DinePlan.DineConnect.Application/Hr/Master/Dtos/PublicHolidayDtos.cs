﻿
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Hr.Master.Dtos
{
    [AutoMapFrom(typeof(PublicHoliday))]
    public class PublicHolidayListDto : FullAuditedEntityDto
    {
        //TODO: DTO PublicHoliday Properties Missing
        //YOU CAN REFER ATTRIBUTES IN 
        public virtual DateTime HolidayDate { get; set; }
        public virtual String HolidayReason { get; set; }
		public virtual decimal OTProportion { get; set; }
	}
    [AutoMapTo(typeof(PublicHoliday))]
    public class PublicHolidayEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO PublicHoliday Properties Missing
        public virtual DateTime HolidayDate { get; set; }
        public virtual String HolidayReason { get; set; }
		public virtual decimal OTProportion { get; set; }
	}

    public class GetPublicHolidayInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetPublicHolidayForEditOutput : IOutputDto
    {
        public PublicHolidayEditDto PublicHoliday { get; set; }
    }
    public class CreateOrUpdatePublicHolidayInput : IInputDto
    {
        [Required]
        public PublicHolidayEditDto PublicHoliday { get; set; }
    }
}

