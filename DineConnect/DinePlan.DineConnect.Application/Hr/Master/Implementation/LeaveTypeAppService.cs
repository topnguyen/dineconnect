﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.Hr.Impl;
using System;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Hr.Transaction.Implementation
{
    public class LeaveTypeAppService : DineConnectAppServiceBase, ILeaveTypeAppService
    {

        private readonly ILeaveTypeListExcelExporter _leavetypeExporter;
        private readonly ILeaveTypeManager _leavetypeManager;
        private readonly IRepository<LeaveType> _leavetypeRepo;
        private readonly IRepository<LeaveExhaustedList> _leaveExhaustedListRepo;

        public LeaveTypeAppService(ILeaveTypeManager leavetypeManager,
            IRepository<LeaveType> leaveTypeRepo,
            ILeaveTypeListExcelExporter leavetypeExporter,
            IRepository<LeaveExhaustedList> leaveExhaustedListRepo)
        {
            _leavetypeManager = leavetypeManager;
            _leavetypeRepo = leaveTypeRepo;
            _leavetypeExporter = leavetypeExporter;
            _leaveExhaustedListRepo = leaveExhaustedListRepo;
        }

        public async Task<PagedResultOutput<LeaveTypeListDto>> GetAll(GetLeaveTypeInput input)
        {
            var allItems = _leavetypeRepo.GetAll();
            if (input.Operation.IsNullOrEmpty())
            {
                allItems = _leavetypeRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.LeaveTypeName.Contains(input.Filter)
               );
            }
            else
            {
                allItems = _leavetypeRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.LeaveTypeName.Contains(input.Filter)
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<LeaveTypeListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<LeaveTypeListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _leavetypeRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<LeaveTypeListDto>>();
            return _leavetypeExporter.ExportToFile(allListDtos);
        }

        public async Task<GetLeaveTypeForEditOutput> GetLeaveTypeForEdit(NullableIdInput input)
        {
            LeaveTypeEditDto editDto;
            List<LeaveType> editExhaustedList = new List<LeaveType>();

            if (input.Id.HasValue)
            {
                var hDto = await _leavetypeRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<LeaveTypeEditDto>();
                if (editDto.EligibleOnlyAnnualLeaveExhausted)
                {
                    var existList = await _leaveExhaustedListRepo.GetAllListAsync(t => t.LeaveTypeRefId == editDto.Id);
                    if (existList.Count > 0)
                    {
                        int[] exhaustedRefIds = existList.Select(t => t.LeaveExhaustedRefId).ToArray();
                        editExhaustedList = await _leavetypeRepo.GetAllListAsync(t => exhaustedRefIds.Contains(t.Id));
                    }
                }
            }
            else
            {
                editDto = new LeaveTypeEditDto();
                editExhaustedList = new List<LeaveType>();
            }

            return new GetLeaveTypeForEditOutput
            {
                LeaveType = editDto,
                LeaveExhaustedList = editExhaustedList
            };
        }

        public async Task CreateOrUpdateLeaveType(CreateOrUpdateLeaveTypeInput input)
        {
            if (input.LeaveType.Id.HasValue)
            {
                await UpdateLeaveType(input);
            }
            else
            {
                await CreateLeaveType(input);
            }
        }

        public async Task DeleteLeaveType(IdInput input)
        {
            var existAnyReferinLeaveExhausted = await _leaveExhaustedListRepo.GetAllListAsync(t => t.LeaveExhaustedRefId == input.Id);
            if (existAnyReferinLeaveExhausted != null)
            {
                int recCount = existAnyReferinLeaveExhausted.Count();
                throw new UserFriendlyException(L("ReferenceExists", L("Leave"), L("Specified") + " " + L("Leave")) + " " + recCount.ToString());
            }

            var existLeaveExhaustedReferences = await _leaveExhaustedListRepo.GetAllListAsync(t => t.LeaveTypeRefId == input.Id);
            foreach (var lst in existLeaveExhaustedReferences)
            {
                await _leaveExhaustedListRepo.DeleteAsync(lst.Id);
            }

            await _leavetypeRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateLeaveType(CreateOrUpdateLeaveTypeInput input)
        {
            var item = await _leavetypeRepo.GetAsync(input.LeaveType.Id.Value);
            var dto = input.LeaveType;

            //TODO: SERVICE LeaveType Update Individually
            item.LeaveTypeName = dto.LeaveTypeName;
            item.LeaveTypeShortName = dto.LeaveTypeShortName;
            item.NumberOfLeaveAllowed = dto.NumberOfLeaveAllowed;
            item.DefaultNumberOfLeaveAllowed = dto.DefaultNumberOfLeaveAllowed;
            item.CalculationPeriodStarts = dto.CalculationPeriodStarts;
            item.CalculationPeriodEnds = dto.CalculationPeriodEnds;
            item.MaleGenderAllowed = dto.MaleGenderAllowed;
            item.FemaleGenderAllowed = dto.FemaleGenderAllowed;
            item.IsSupportingDocumentsRequired = dto.IsSupportingDocumentsRequired;
            item.EligibleOnlyAnnualLeaveExhausted = dto.EligibleOnlyAnnualLeaveExhausted;
            item.AllowedForMarriedEmployeesOnly = dto.AllowedForMarriedEmployeesOnly;
            CheckErrors(await _leavetypeManager.CreateSync(item));

            List<int> leaveExhaustedToBeRetained = new List<int>();
            if (input.LeaveType.EligibleOnlyAnnualLeaveExhausted == true)
            {
                foreach (var lst in input.LeaveExhaustedList)
                {
                    LeaveExhaustedList existAlready = await _leaveExhaustedListRepo.FirstOrDefaultAsync(t => t.LeaveTypeRefId == item.Id
                        && t.LeaveExhaustedRefId == lst.Id);
                    if (existAlready != null)
                    {
                        leaveExhaustedToBeRetained.Add(existAlready.Id);
                        continue;
                    }
                    else
                    {
                        LeaveExhaustedList newSubDto = new LeaveExhaustedList
                        {
                            LeaveTypeRefId = item.Id,
                            LeaveExhaustedRefId = lst.Id
                        };
                        var newid = await _leaveExhaustedListRepo.InsertOrUpdateAndGetIdAsync(newSubDto);
                        leaveExhaustedToBeRetained.Add(newid);
                    }
                }
            }

            var delList = await _leaveExhaustedListRepo.GetAllListAsync(t => t.LeaveTypeRefId == item.Id && !leaveExhaustedToBeRetained.Contains(t.Id));
            foreach (var lst in delList)
            {
                await _leaveExhaustedListRepo.DeleteAsync(lst.Id);
            }

        }

        protected virtual async Task CreateLeaveType(CreateOrUpdateLeaveTypeInput input)
        {
            var dto = input.LeaveType.MapTo<LeaveType>();

            CheckErrors(await _leavetypeManager.CreateSync(dto));

            if (input.LeaveType.EligibleOnlyAnnualLeaveExhausted == true)
            {
                foreach (var lst in input.LeaveExhaustedList)
                {
                    LeaveExhaustedList newSubDto = new LeaveExhaustedList
                    {
                        LeaveTypeRefId = dto.Id,
                        LeaveExhaustedRefId = lst.Id
                    };
                    await _leaveExhaustedListRepo.InsertOrUpdateAndGetIdAsync(newSubDto);
                }
            }
        }

        public async Task<ListResultOutput<LeaveTypeListDto>> GetLeaveTypes()
        {
            var lstLeaveType = await _leavetypeRepo.GetAll().ToListAsync();
            return new ListResultOutput<LeaveTypeListDto>(lstLeaveType.MapTo<List<LeaveTypeListDto>>());
        }
    }
}