﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.House.Master.Dtos;

namespace DinePlan.DineConnect.Hr.Transaction
{
    public interface ILeaveRequestAppService : IApplicationService
    {
        Task<LeaveAlreadyTaken> IsEmployeeLeaveRequestOverLappingWithExistingRequestDates(LeaveRequestEditDto input);
        Task<PagedResultOutput<LeaveRequestListDto>> GetAll(GetLeaveRequestInput inputDto);
        Task<GetLeaveRequestForEditOutput> GetLeaveRequestForEdit(NullableIdInput nullableIdInput);
        Task<IdInput> CreateOrUpdateLeaveRequest(CreateOrUpdateLeaveRequestInput input);
        Task<ReceivableMessageOutput> SendMailForApproveLeaveRequest(CreateOrUpdateLeaveRequestInput input);
        Task<ReceivableMessageOutput> SendMailForRejectLeaveRequest(CreateOrUpdateLeaveRequestInput input);
        Task DeleteLeaveRequest(IdInput input);
        Task<GetLeaveRequestForEditOutput> GetLeaveRequestForPrint(IdInput input);
        Task<ListResultOutput<ComboboxItemDto>> GetLeaveTypeForCombobox();

        Task ApproveLeaveRequest(CreateOrUpdateLeaveRequestInput input);
		  Task<FileDto> GetAllToExcel(GetLeaveRequestInput input);
        Task<decimal> GetNumberOfLeaveDays(LeaveCalculationDto input);

        Task<List<LeaveTypeListDto>> GetLeaveTypes();
        Task<List<LeaveTypeListDto>> GetLeaveTypesForFemale();
        Task<List<LeaveTypeListDto>> GetLeaveTypesForMale();

        Task<MessageOutput> Leave_ActutalSet(NullableIdInput input);

        Task<ReceivableMessageOutput> SendMailForLeaveRequest(IdInput input);
     }
}