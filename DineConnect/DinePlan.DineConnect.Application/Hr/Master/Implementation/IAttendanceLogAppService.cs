﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Hr.Master.Dtos;

namespace DinePlan.DineConnect.Hr.Master.Implementation
{
    public interface IAttendanceLogAppService : IApplicationService
    {
        Task<PagedResultOutput<AttendanceLogListDto>> GetAll(GetAttendanceLogInput inputDto);
        Task<GetAttendanceLogForEditOutput> GetAttendanceLogForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateAttendanceLog(CreateOrUpdateAttendanceLogInput input);
        Task DeleteAttendanceLog(DeleteAttendanceLog input);
        Task<PagedResultOutput<AttendanceLogListDto>> GetAllForIndex(GetAttendanceLogInput input);
		Task<List<AttendanceParticularDateList>> GetGpsEmployeeAttendanceDateWise(GetAttendanceLogInput input);
		Task<FileDto> GetAttendanceLogToExcel(GetAttendanceLogInput input);
        Task<List<AttendanceParticularDateList>> GetOTEmployeeAttendanceDateWise(GetAttendanceLogInput input);
        Task<FileDto> GetOTLogToExcel(GetAttendanceLogInput input);
        Task<ApiMessageReturn> ApiCreateEmployeeAttendance(ApiEmployeeAttendanceDto input);

    }
}