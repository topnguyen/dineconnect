﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Hr.Master
{
    public interface IMonthWiseWorkDayAppService : IApplicationService
    {
        Task<PagedResultOutput<MonthWiseWorkDayListDto>> GetAll(GetMonthWiseWorkDayInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetMonthWiseWorkDayForEditOutput> GetMonthWiseWorkDayForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateMonthWiseWorkDay(CreateOrUpdateMonthWiseWorkDayInput input);
        Task DeleteMonthWiseWorkDay(IdInput input);

        Task<ListResultOutput<MonthWiseWorkDayListDto>> GetDocumentNames();
    }
}