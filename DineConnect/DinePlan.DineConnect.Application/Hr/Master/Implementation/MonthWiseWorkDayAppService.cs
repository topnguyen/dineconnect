﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.Hr.Impl;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Hr.Master.Implementation
{
    public class MonthWiseWorkDayAppService : DineConnectAppServiceBase, IMonthWiseWorkDayAppService
    {

        private readonly IMonthWiseWorkDayListExcelExporter _monthwiseworkdayExporter;
        private readonly IMonthWiseWorkDayManager _monthwiseworkdayManager;
        private readonly IRepository<MonthWiseWorkDay> _monthwiseworkdayRepo;

        public MonthWiseWorkDayAppService(IMonthWiseWorkDayManager monthwiseworkdayManager,
            IRepository<MonthWiseWorkDay> monthWiseWorkDayRepo,
            IMonthWiseWorkDayListExcelExporter monthwiseworkdayExporter)
        {
            _monthwiseworkdayManager = monthwiseworkdayManager;
            _monthwiseworkdayRepo = monthWiseWorkDayRepo;
            _monthwiseworkdayExporter = monthwiseworkdayExporter;

        }

        public async Task<PagedResultOutput<MonthWiseWorkDayListDto>> GetAll(GetMonthWiseWorkDayInput input)
        {
            var allItems = _monthwiseworkdayRepo.GetAll();
            if (input.Operation.IsNullOrEmpty())
            {
                allItems = _monthwiseworkdayRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.MonthYear.Contains(input.Filter)
               );
            }
            else
            {
                allItems = _monthwiseworkdayRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.MonthYear.Contains(input.Filter)
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<MonthWiseWorkDayListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<MonthWiseWorkDayListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _monthwiseworkdayRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<MonthWiseWorkDayListDto>>();
            return _monthwiseworkdayExporter.ExportToFile(allListDtos);
        }

        public async Task<GetMonthWiseWorkDayForEditOutput> GetMonthWiseWorkDayForEdit(NullableIdInput input)
        {
            MonthWiseWorkDayEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _monthwiseworkdayRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<MonthWiseWorkDayEditDto>();
            }
            else
            {
                editDto = new MonthWiseWorkDayEditDto();
            }

            return new GetMonthWiseWorkDayForEditOutput
            {
                MonthWiseWorkDay = editDto
            };
        }

        public async Task CreateOrUpdateMonthWiseWorkDay(CreateOrUpdateMonthWiseWorkDayInput input)
        {
            if (input.MonthWiseWorkDay.Id.HasValue)
            {
                await UpdateMonthWiseWorkDay(input);
            }
            else
            {
                await CreateMonthWiseWorkDay(input);
            }
        }

        public async Task DeleteMonthWiseWorkDay(IdInput input)
        {
            await _monthwiseworkdayRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateMonthWiseWorkDay(CreateOrUpdateMonthWiseWorkDayInput input)
        {
            var item = await _monthwiseworkdayRepo.GetAsync(input.MonthWiseWorkDay.Id.Value);
            var dto = input.MonthWiseWorkDay;

            //TODO: SERVICE MonthWiseWorkDay Update Individually
            item.MonthYear = dto.MonthYear;
            item.StartDate = dto.StartDate;
            item.EndDate = dto.EndDate;
            item.NoOfDays = dto.NoOfDays;
            item.NoOfGovernmentHolidays = dto.NoOfGovernmentHolidays;
            item.NoOfLeaves = dto.NoOfLeaves;
            item.NoOfWorkDays = dto.NoOfWorkDays;
            item.SalaryCalculationDays = dto.SalaryCalculationDays;
            

            CheckErrors(await _monthwiseworkdayManager.CreateSync(item));
        }

        protected virtual async Task CreateMonthWiseWorkDay(CreateOrUpdateMonthWiseWorkDayInput input)
        {
            var dto = input.MonthWiseWorkDay.MapTo<MonthWiseWorkDay>();

            CheckErrors(await _monthwiseworkdayManager.CreateSync(dto));
        }

        public async Task<ListResultOutput<MonthWiseWorkDayListDto>> GetDocumentNames()
        {
            var lstMonthWiseWorkDay = await _monthwiseworkdayRepo.GetAll().ToListAsync();
            return new ListResultOutput<MonthWiseWorkDayListDto>(lstMonthWiseWorkDay.MapTo<List<MonthWiseWorkDayListDto>>());
        }
    }
}
