﻿
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.Hr.Impl;
using System;
using Abp.Domain.Uow;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Hr.Master.Implementation
{
    public class EmployeeSkillSetAppService : DineConnectAppServiceBase, IEmployeeSkillSetAppService
    {

        private readonly IEmployeeSkillSetListExcelExporter _employeeskillsetExporter;
        private readonly IEmployeeSkillSetManager _employeeskillsetManager;
        private readonly IRepository<EmployeeSkillSet> _employeeskillsetRepo;
        private readonly IRepository<SkillSet> _skillsetRepo;
        private readonly IRepository<PersonalInformation> _personalinformationRepo;

        public EmployeeSkillSetAppService(IEmployeeSkillSetManager employeeskillsetManager,
            IRepository<EmployeeSkillSet> employeeSkillSetRepo,
            IEmployeeSkillSetListExcelExporter employeeskillsetExporter
            , IRepository<PersonalInformation> personalinformationRepo
            , IRepository<SkillSet> skillsetRepo)
        {
            _employeeskillsetManager = employeeskillsetManager;
            _employeeskillsetRepo = employeeSkillSetRepo;
            _employeeskillsetExporter = employeeskillsetExporter;
            _skillsetRepo = skillsetRepo;
            _personalinformationRepo = personalinformationRepo;
        }

        public async Task<PagedResultOutput<EmployeeSkillSetListDto>> GetAll(GetEmployeeSkillSetInput input)
        {
            var allItems = (from empskill in _employeeskillsetRepo.GetAll()
                            join personal in _personalinformationRepo.GetAll()
                            on empskill.EmployeeRefId equals personal.Id
                            join skill in _skillsetRepo.GetAll()
                            on empskill.SkillSetRefId equals skill.Id
                            select new EmployeeSkillSetListDto()
                            {
                                Id = empskill.Id,
                                 EmployeeRefId= empskill.EmployeeRefId,
                                EmployeeRefName = personal.EmployeeName,
                                SkillSetRefId = empskill.SkillSetRefId,
                                SkillSetRefCode = skill.SkillCode,
                                CreationTime=empskill.CreationTime
                            }).WhereIf(
                              !input.Filter.IsNullOrEmpty(),
                              p => p.EmployeeRefName.Contains(input.Filter)
              );


            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<EmployeeSkillSetListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<EmployeeSkillSetListDto>(
                allItemCount,
                allListDtos
                );
        }

 public async Task<PagedResultOutput<EmployeeSkillSetViewDto>> GetAllSkillWithOneRecord(GetEmployeeSkillSetInput input)
      {
            var rsSkill = _skillsetRepo.GetAll();
            if (!input.SkillFilter.IsNullOrEmpty())
            {
                rsSkill = rsSkill.Where(t => t.SkillCode.Contains(input.SkillFilter));
            }

            var allItems = (from empskill in _employeeskillsetRepo.GetAll()
                            join personal in _personalinformationRepo.GetAll().Where(t=>t.ActiveStatus==true)
                            on empskill.EmployeeRefId equals personal.Id
                            join skill in rsSkill
                            on empskill.SkillSetRefId equals skill.Id
                            select new EmployeeSkillSetViewDto()
                            {
                                EmployeeRefId = empskill.EmployeeRefId,
                                EmployeeRefName = personal.EmployeeName,
                                CreationTime = personal.CreationTime,
                                ActiveStatus = personal.ActiveStatus
                            }).WhereIf(
                              !input.Filter.IsNullOrEmpty(),
                              p => p.EmployeeRefName.Contains(input.Filter) 
              ).Distinct();


            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<EmployeeSkillSetViewDto>>();

            var SkillSetDtos = await _skillsetRepo.GetAllListAsync();

            int loopIndex = 0;
            foreach(var emp in allListDtos)
            {
                var skillList = await _employeeskillsetRepo.GetAll().Where(t => t.EmployeeRefId == emp.EmployeeRefId).ToListAsync();
                int[] skillRefIds = skillList.Select(t => t.SkillSetRefId).ToArray();
                allListDtos[loopIndex].Id = skillList[0].Id;
                string[] arrSkill = SkillSetDtos.Where(t => skillRefIds.Contains(t.Id)).Select(t => t.SkillCode).ToArray();
                allListDtos[loopIndex].SkillSetRefCode = string.Join(",", arrSkill);
                loopIndex++;
            }

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<EmployeeSkillSetViewDto>(
                allItemCount,
                allListDtos
                );
        }

      public async Task<FileDto> GetAllToExcel(GetEmployeeSkillSetInput input)
        {
            input.MaxResultCount = AppConsts.MaxPageSize;
            var allList = await GetAllSkillWithOneRecord(input);
            var allListDtos = allList.Items.MapTo<List<EmployeeSkillSetViewDto>>();
            return _employeeskillsetExporter.ExportToFile(allListDtos);
        }

        public async Task<GetEmployeeSkillSetForEditOutput> GetEmployeeSkillSetForEdit(NullableIdInput input)
        {
            EmployeeSkillSetEditDto editDto;
            List<ComboboxItemDto> detailDto;

            if (input.Id.HasValue)
            {
                var hDto = await _employeeskillsetRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<EmployeeSkillSetEditDto>();

                detailDto = await (from es in _employeeskillsetRepo.GetAll().Where(t => t.EmployeeRefId == hDto.EmployeeRefId)
                                   join skill in _skillsetRepo.GetAll() on
                                   es.SkillSetRefId equals skill.Id
                                   select new ComboboxItemDto
                                   {
                                       Value = skill.Id.ToString(),
                                       DisplayText = skill.SkillCode
                                   }).ToListAsync();
            }
            else
            {
                editDto = new EmployeeSkillSetEditDto();
                detailDto = new List<ComboboxItemDto>();
            }

            return new GetEmployeeSkillSetForEditOutput
            {
                EmployeeSkillSet = editDto,
                SkillSetList = detailDto
            };
        }

        public async Task CreateOrUpdateEmployeeSkillSet(CreateOrUpdateEmployeeSkillSetInput input)
        {
            await UpdateEmployeeSkillSet(input);
        }

        public async Task DeleteEmployeeSkillSet(IdInput input)
        {
            await _employeeskillsetRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateEmployeeSkillSet(CreateOrUpdateEmployeeSkillSetInput input)
        {
            if (input.SkillSetList.Count == 0)
            {
                throw new UserFriendlyException("SkillSetErr");
            }

            var dto = input.EmployeeSkillSet;

            List<int> skillSetsToBeRetained = new List<int>();

            if (input.SkillSetList != null && input.SkillSetList.Count > 0)
            {
                foreach (var skill in input.SkillSetList)
                {
                    int skillsetrefid = int.Parse(skill.Value);
                    skillSetsToBeRetained.Add(skillsetrefid);

                    using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.SoftDelete))
                    {
                        var existAlready = await _employeeskillsetRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == dto.EmployeeRefId
                            && t.SkillSetRefId == skillsetrefid);
                        if (existAlready == null)
                        {
                            var newskillset = new EmployeeSkillSet
                            {
                                EmployeeRefId = dto.EmployeeRefId,
                                SkillSetRefId = int.Parse(skill.Value),
                            };
                            await _employeeskillsetRepo.InsertOrUpdateAndGetIdAsync(newskillset);
                        }
                        else
                        {
                            if (existAlready.IsDeleted)
                            {
                                existAlready.DeleterUserId = null;
                                existAlready.DeletionTime = null;
                                existAlready.IsDeleted = false;
                                await _employeeskillsetRepo.InsertOrUpdateAndGetIdAsync(existAlready);
                            }
                        }
                    }
                }
            }

            var delskillsetlst = await _employeeskillsetRepo.GetAll().Where(a => a.EmployeeRefId == dto.EmployeeRefId 
                && !skillSetsToBeRetained.Contains(a.SkillSetRefId)).ToListAsync();

            foreach (var a in delskillsetlst)
            {
               await _employeeskillsetRepo.DeleteAsync(a.Id);
            }
        }
    }
}