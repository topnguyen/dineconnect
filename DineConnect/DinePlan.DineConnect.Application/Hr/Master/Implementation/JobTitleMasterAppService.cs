﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Hr;
using DinePlan.DineConnect.Hr.Impl;
using DinePlan.DineConnect.Hr.Master.Dtos;

namespace DinePlan.DineConnect.Hr.Master.Implementation
{
    public class JobTitleMasterAppService : DineConnectAppServiceBase, IJobTitleMasterAppService
    {

        //private readonly IJobTitleMasterListExcelExporter _jobtitlemasterExporter;
        private readonly IJobTitleMasterManager _jobtitlemasterManager;
        private readonly IRepository<JobTitleMaster> _jobtitlemasterRepo;

        public JobTitleMasterAppService(IJobTitleMasterManager jobtitlemasterManager,
            IRepository<JobTitleMaster> jobTitleMasterRepo)
            //,IJobTitleMasterListExcelExporter jobtitlemasterExporter)
        {
            _jobtitlemasterManager = jobtitlemasterManager;
            _jobtitlemasterRepo = jobTitleMasterRepo;
          //  _jobtitlemasterExporter = jobtitlemasterExporter;

        }

        public async Task<PagedResultOutput<JobTitleMasterListDto>> GetAll(GetJobTitleMasterInput input)
        {
            var allItems = _jobtitlemasterRepo.GetAll();
            if (input.Operation.IsNullOrEmpty())
            {
                allItems = _jobtitlemasterRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.JobTitle.Equals(input.Filter)
               );
            }
            else
            {
                allItems = _jobtitlemasterRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.JobTitle.Contains(input.Filter)
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<JobTitleMasterListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<JobTitleMasterListDto>(
                allItemCount,
                allListDtos
                );
        }

        //public async Task<FileDto> GetAllToExcel()
        //{
        //    var allList = await _jobtitlemasterRepo.GetAll().ToListAsync();
        //    var allListDtos = allList.MapTo<List<JobTitleMasterListDto>>();
        //    return _jobtitlemasterExporter.ExportToFile(allListDtos);
        //}

        public async Task<GetJobTitleMasterForEditOutput> GetJobTitleMasterForEdit(NullableIdInput input)
        {
            JobTitleMasterEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _jobtitlemasterRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<JobTitleMasterEditDto>();
            }
            else
            {
                editDto = new JobTitleMasterEditDto();
            }

            return new GetJobTitleMasterForEditOutput
            {
                JobTitleMaster = editDto
            };
        }

        public async Task CreateOrUpdateJobTitleMaster(CreateOrUpdateJobTitleMasterInput input)
        {
            if (input.JobTitleMaster.Id.HasValue)
            {
                await UpdateJobTitleMaster(input);
            }
            else
            {
                await CreateJobTitleMaster(input);
            }
        }

        public async Task DeleteJobTitleMaster(IdInput input)
        {
            await _jobtitlemasterRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateJobTitleMaster(CreateOrUpdateJobTitleMasterInput input)
        {
            var item = await _jobtitlemasterRepo.GetAsync(input.JobTitleMaster.Id.Value);
            var dto = input.JobTitleMaster;

            //TODO: SERVICE JobTitleMaster Update Individually
            item.JobTitle = dto.JobTitle;

            CheckErrors(await _jobtitlemasterManager.CreateSync(item));
        }

        protected virtual async Task CreateJobTitleMaster(CreateOrUpdateJobTitleMasterInput input)
        {
            var dto = input.JobTitleMaster.MapTo<JobTitleMaster>();

            CheckErrors(await _jobtitlemasterManager.CreateSync(dto));
        }

        public async Task<ListResultOutput<JobTitleMasterListDto>> Gets()
        {
            var lstJobTitleMaster = await _jobtitlemasterRepo.GetAll().ToListAsync();
            return new ListResultOutput<JobTitleMasterListDto>(lstJobTitleMaster.MapTo<List<JobTitleMasterListDto>>());
        }
    }
}