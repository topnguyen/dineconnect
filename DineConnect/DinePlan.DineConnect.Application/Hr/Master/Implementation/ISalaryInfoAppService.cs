﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Hr.Master
{
    public interface ISalaryInfoAppService : IApplicationService
    {
        Task<PagedResultOutput<SalaryInfoListDto>> GetAll(GetSalaryInfoInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetSalaryInfoForEditOutput> GetSalaryInfoForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateSalaryInfo(CreateOrUpdateSalaryInfoInput input);
        Task DeleteSalaryInfo(IdInput input);
		Task<ListResultOutput<ComboboxItemDto>> GetSalaryTagsForCombobox();
        Task<ListResultOutput<ComboboxItemDto>> GetSalaryCalculationTypeForCombobox();

        Task<bool> DeleteAllowanceOrDeduction(EmployeeSalaryTagAddOrEditDto salaryTag);

        Task<EmployeeVsSalaryTagListDto> AddOrEditAllowanceOrDeduction(EmployeeSalaryTagAddOrEditDto salaryTag);
        Task<bool> CheckBusinessRuleForIncentiveTag(CreateOrUpdateSalaryInfoInput input);

        Task<GetEmployeeWorkDayDtos> GetEmployeeWorkDay(GetDataBasedOnNullableEmployee input);
        Task<ListResultOutput<ComboboxItemDto>> GetOTAndIncentivePeriodTagsForCombobox();
        Task<ListResultOutput<ComboboxItemDto>> GetSpecialFormulaTagsForCombobox();
        Task<List<EmployeeVsSalaryTagListDto>> BulkUpdateForAllEligibleEmployeeForThisIncentive(IdInput input);
        Task DeleteEmployeeParticularSalaryTag(EmployeeSalaryDto input);
        Task<ListResultOutput<ComboboxItemDto>> GetOTTypeTagsForCombobox();
        Task<OtBasedOnSlabListDto> AddOrEditOtSlabs(EmployeeOTSlabAddOrEditDto input);

        Task<OtBasedOnSlabListDto> DeleteOtSlabs(EmployeeOTSlabAddOrEditDto input);
        Task<ListResultOutput<ComboboxItemDto>> GetHourlySalaryModeForCombobox();


    }
}