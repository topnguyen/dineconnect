﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Utility.Exporter;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Connect.Master;
using System;

namespace DinePlan.DineConnect.Hr.Master.Implementation
{
    public class DcStationMasterAppService : DineConnectAppServiceBase, IDcStationMasterAppService
    {

        private readonly IExcelExporter _exporter;
        private readonly IDcStationMasterManager _dcstationmasterManager;
        private readonly IRepository<DcHeadMaster> _dcHeadmasterRepo;
        private readonly IRepository<DcGroupMaster> _dcGroupMasterRepo;
        private readonly IRepository<DcStationMaster> _dcstationmasterRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<Company> _companyRepo;
        private readonly IRepository<DcShiftMaster> _dcshiftMasterRepo;

        public DcStationMasterAppService(IDcStationMasterManager dcstationmasterManager,
            IRepository<DcHeadMaster> dcHeadmasterRepo,
            IRepository<DcGroupMaster> dcGroupMasterRepo,
            IRepository<DcStationMaster> dcStationMasterRepo,
            IRepository<Location> locationRepo,
            IRepository<Company> companyRepo,
            IRepository<DcShiftMaster> dcshiftMasterRepo,
            IExcelExporter exporter)
        {
            _dcGroupMasterRepo = dcGroupMasterRepo;
            _dcHeadmasterRepo = dcHeadmasterRepo;
            _dcstationmasterManager = dcstationmasterManager;
            _dcstationmasterRepo = dcStationMasterRepo;
            _exporter = exporter;
            _locationRepo = locationRepo;
            _companyRepo = companyRepo;
            _dcshiftMasterRepo = dcshiftMasterRepo;
        }

        public async Task<PagedResultOutput<DcStationMasterListDto>> GetAll(GetDcStationMasterInput input)
        {
            var dcGroup = _dcGroupMasterRepo.GetAll();
            if (input.DcGroupRefId.HasValue)
            {
                if (input.DcGroupRefId.Value > 0)
                {
                    dcGroup = dcGroup.Where(t => t.Id == input.DcGroupRefId.Value);
                }
            }

            var dcHead = _dcHeadmasterRepo.GetAll();
            if (input.DcHeadRefId.HasValue)
            {
                if (input.DcHeadRefId.Value > 0)
                {
                    dcHead = dcHead.Where(t => t.Id == input.DcHeadRefId.Value);
                }
            }

            List<int> locationRefIds = new List<int>();

            var rsLocation = _locationRepo.GetAll();
            if (input.LocationRefId.HasValue)
            {
                if (input.LocationRefId.Value > 0)
                    locationRefIds.Add(input.LocationRefId.Value);
            }

            if (input.Locations!=null && input.Locations.Count>0)
            {
                locationRefIds.AddRange(input.Locations.Select(t => t.Id).ToList());
            }
            if (locationRefIds.Count>0)
                rsLocation = rsLocation.Where(t => locationRefIds.Contains(t.Id));

            var rsStation = _dcstationmasterRepo.GetAll();
            if (!input.StationName.IsNullOrEmpty())
            {
                rsStation = rsStation.Where(t => t.StationName.Contains(input.StationName));
            }

            var allItems = (from stn in rsStation
                            join grp in dcGroup
                            on stn.DcGroupRefId equals grp.Id
                            join head in dcHead
                            on grp.DcHeadRefId equals head.Id
                            join loc in rsLocation
                            on stn.LocationRefId equals loc.Id
                            join comp in _companyRepo.GetAll()
                            on loc.CompanyRefId equals comp.Id
                            select new DcStationMasterListDto
                            {
                                Id = stn.Id,
                                CompanyRefId = comp.Id,
                                CompanyRefName = comp.Code,
                                LocationRefId = stn.LocationRefId,
                                LocationRefName = loc.Code,
                                StationName = stn.StationName,
                                DcGroupRefId = stn.DcGroupRefId,
                                DcGroupRefName = grp.GroupName,
                                DcHeadRefId = grp.DcHeadRefId,
                                DcHeadRefName = head.HeadName,
                                IsMandatory = stn.IsMandatory,
                                IsActive = stn.IsActive,
                                PriorityLevel = stn.PriorityLevel,
                                NightFlag = stn.NightFlag,
                                DefaultTimeFlag = stn.DefaultTimeFlag,
                                TimeIn = stn.TimeIn,
                                BreakFlag = stn.BreakFlag,
                                BreakOut = stn.BreakOut,
                                BreakIn = stn.BreakIn,
                                TimeOut = stn.TimeOut,
                                MinimumStaffRequired = stn.MinimumStaffRequired,
                                MaximumStaffRequired = stn.MaximumStaffRequired,
                                BreakMinutes = stn.BreakMinutes,
                                TimeDescription = stn.TimeDescription
                            });

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<DcStationMasterListDto>>();

            var allItemCount = await allItems.CountAsync();

            foreach(var lst in allListDtos)
            {
                lst.TimeInHours = GetHours(new IdInput { Id = lst.TimeIn });
                lst.TimeInMinutes = GetMinutes(new IdInput { Id = lst.TimeIn });

                if (lst.BreakFlag)
                {
                    lst.BreakOutHours = GetHours(new IdInput { Id = lst.BreakOut });
                    lst.BreakOutMinutes = GetMinutes(new IdInput { Id = lst.BreakOut });

                    lst.BreakInHours = GetHours(new IdInput { Id = lst.BreakIn });
                    lst.BreakInMinutes = GetMinutes(new IdInput { Id = lst.BreakIn });
                }

                lst.TimeOutHours = GetHours(new IdInput { Id = lst.TimeOut });
                lst.TimeOutMinutes = GetMinutes(new IdInput { Id = lst.TimeOut });

                lst.TimeDescription = L("TI") + " : " + lst.TimeInHours + ':' + lst.TimeInMinutes.PadLeft(2,'0'); // String("0" + vm.timeinminutes).slice(-2);

                if (lst.NightFlag== true)
                    lst.TimeDescription = lst.TimeDescription + " (N)";

                if (lst.BreakFlag == true)
                {
                    lst.TimeDescription = lst.TimeDescription + " - " + L("BO") + " : " + lst.BreakOutHours + ':' + lst.BreakOutMinutes.PadLeft(2, '0'); //String("0" + lst.BreakOutMinutes).slice(-2);
                    lst.TimeDescription = lst.TimeDescription + " - " + L("BI") + " : " + lst.BreakInHours + ':'  + lst.BreakInMinutes.PadLeft(2, '0'); // String("0" + lst.BreakInMinutes).slice(-2);
                }

                lst.TimeDescription = lst.TimeDescription + " - " + L("TO") + " : " + lst.TimeOutHours + ':' + lst.TimeOutMinutes.PadLeft(2, '0'); // String("0" + lst.TimeOutHours).slice(-2);
            }

            return new PagedResultOutput<DcStationMasterListDto>(
                allItemCount,
                allListDtos
                );
        }

        public string  GetHours (IdInput input)
        {
            var hrs = Math.Floor( (decimal) input.Id / 60);
            return hrs.ToString();
        }

        public string GetMinutes(IdInput input)
        {
            var minutes = ((decimal)input.Id % 60);
            return minutes.ToString();
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _dcstationmasterRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<DcStationMasterListDto>>();
            var baseE = new BaseExportObject()
            {
                ExportObject = allListDtos,
                ColumnNames = new string[] { "Id" }
            };
            return _exporter.ExportToFile(baseE, L("DcStationMaster"));

        }

        public async Task<GetDcStationMasterForEditOutput> GetDcStationMasterForEdit(NullableIdInput input)
        {
            DcStationMasterEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _dcstationmasterRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<DcStationMasterEditDto>();

                var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == editDto.LocationRefId);
                editDto.LocationRefName = location.Code;

                var company = await _companyRepo.FirstOrDefaultAsync(t => t.Id == location.CompanyRefId);
                editDto.CompanyRefName = company.Code;

                var dcGroup = await _dcGroupMasterRepo.FirstOrDefaultAsync(t => t.Id == editDto.DcGroupRefId);
                editDto.DcHeadRefId = dcGroup.DcHeadRefId;

            }
            else
            {
                editDto = new DcStationMasterEditDto();
            }

            return new GetDcStationMasterForEditOutput
            {
                DcStationMaster = editDto
            };
        }

        public async Task<bool> ExistAllServerLevelBusinessRules(CreateOrUpdateDcStationMasterInput input)
        {
            var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.DcStationMaster.LocationRefId);
            if (location == null)
            {
                throw new UserFriendlyException(L("Loation") + "?");
            }

            var dcM = input.DcStationMaster;

            if (dcM.BreakFlag==false)
            {
                if (dcM.NightFlag == false)
                {
                    if (dcM.TimeIn > dcM.TimeOut)
                    {
                        throw new UserFriendlyException(L("TimeInTimeOutInfo"));
                    }
                }
            }
            else if (dcM.BreakFlag==true)
            {
                if (dcM.NightFlag == false)
                {
                    if (dcM.TimeIn > dcM.TimeOut)
                    {
                        throw new UserFriendlyException(L("TimeInTimeOutInfo"));
                    }
                    if (dcM.TimeIn > dcM.BreakOut)
                    {
                        throw new UserFriendlyException(L("TimeInBreakOutInfo"));
                    }
                    if (dcM.BreakOut > dcM.BreakIn)
                    {
                        throw new UserFriendlyException(L("BreakFlagInfo"));
                    }
                    if (dcM.BreakIn > dcM.TimeOut)
                    {
                        throw new UserFriendlyException(L("TimeOutBreakInInfo"));
                    }
                }
            }


            return true;

        }

        public async Task CreateOrUpdateDcStationMaster(CreateOrUpdateDcStationMasterInput input)
        {
            var output = await ExistAllServerLevelBusinessRules(input);

            if (input.DcStationMaster.Id.HasValue)
            {
                await UpdateDcStationMaster(input);
            }
            else
            {
                await CreateDcStationMaster(input);
            }
        }

        public async Task DeleteDcStationMaster(IdInput input)
        {
            var existAnyRef = await _dcshiftMasterRepo.FirstOrDefaultAsync(t => t.StationRefId == input.Id);
            if (existAnyRef!=null)
            {
                throw new UserFriendlyException(L("ReferenceExists", L("DcShiftMaster"), L("DcStationMaster")));
            }
            await _dcstationmasterRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateDcStationMaster(CreateOrUpdateDcStationMasterInput input)
        {
            var item = await _dcstationmasterRepo.GetAsync(input.DcStationMaster.Id.Value);
            var dto = input.DcStationMaster;

            item.LocationRefId = dto.LocationRefId;
            item.DcGroupRefId = dto.DcGroupRefId;
            item.StationName = dto.StationName;
            item.MaximumStaffRequired = dto.MaximumStaffRequired;
            item.MinimumStaffRequired = dto.MinimumStaffRequired;
            item.PriorityLevel = dto.PriorityLevel;
            item.IsMandatory = dto.IsMandatory;
            item.IsActive = dto.IsActive;
            item.TimeIn = dto.TimeIn;
            item.BreakOut = dto.BreakOut;
            item.BreakIn = dto.BreakIn;
            item.BreakMinutes = dto.BreakMinutes;
            item.TimeOut = dto.TimeOut;

            item.BreakFlag = dto.BreakFlag;
            item.NightFlag = dto.NightFlag;
            item.DefaultTimeFlag = dto.DefaultTimeFlag;
            item.TimeDescription = dto.TimeDescription;

            CheckErrors(await _dcstationmasterManager.CreateSync(item));

            if (dto.DefaultTimeFlag == true)
            {
                var existDefault = await _dcstationmasterRepo.GetAll().Where(t => t.Id != item.Id && t.DefaultTimeFlag == true).ToListAsync();
                foreach (var dc in existDefault)
                {
                    var defaultvalue = await _dcstationmasterRepo.FirstOrDefaultAsync(t => t.Id == dc.Id);
                    defaultvalue.DefaultTimeFlag = false;
                    await _dcstationmasterRepo.UpdateAsync(defaultvalue);
                }
            }
        }

        protected virtual async Task CreateDcStationMaster(CreateOrUpdateDcStationMasterInput input)
        {
            var dto = input.DcStationMaster.MapTo<DcStationMaster>();
            dto.IsActive = true;
            CheckErrors(await _dcstationmasterManager.CreateSync(dto));
        }

        public async Task<ListResultOutput<DcStationMasterListDto>> GetStationNames()
        {
            var lstDcStationMaster = await _dcstationmasterRepo.GetAll().ToListAsync();
            return new ListResultOutput<DcStationMasterListDto>(lstDcStationMaster.MapTo<List<DcStationMasterListDto>>());
        }

        public async Task<ListResultOutput<DcStationMasterListDto>> GetStationNames(GroupCodeDto input)
        {
            var lstDcStationMaster = await _dcstationmasterRepo.GetAll().Where(t=>t.DcGroupRefId==input.DcGroupRefId).ToListAsync();
            return new ListResultOutput<DcStationMasterListDto>(lstDcStationMaster.MapTo<List<DcStationMasterListDto>>());
        }

        public async Task<ListResultOutput<DcStationMasterListDto>> GetStationNames(LocationGroupCodeDto input)
        {
            var lstDcStationMaster = await _dcstationmasterRepo.GetAll().Where(t => t.LocationRefId==input.LocationRefId && t.DcGroupRefId == input.DcGroupRefId).ToListAsync();
            return new ListResultOutput<DcStationMasterListDto>(lstDcStationMaster.MapTo<List<DcStationMasterListDto>>());
        }

        public async Task<int> GetMaxUserSerialNumber()
        {

            try
            {
                var maxSno = await _dcstationmasterRepo.GetAll().Select(t => t.PriorityLevel).MaxAsync();
                return (int.Parse(maxSno.ToString()) + 1);
            }
            catch (System.Exception)
            {
                return 1;
            }


        }
    }
}
