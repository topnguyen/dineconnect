﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Hr.Transaction
{
	public interface IIncentiveTagAppService : IApplicationService
	{
		Task<PagedResultOutput<IncentiveCategoryListDto>> GetAllIncentiveCategory(GetIncentiveCategoryInput input);
		Task<FileDto> GetAllIncentiveCategoryToExcel();
		Task<GetIncentiveCategoryForEditOutput> GetIncentiveCategoryForEdit(NullableIdInput input);
		Task CreateOrUpdateIncentiveCategory(CreateOrUpdateIncentiveCategoryInput input);
		Task DeleteIncentiveCategory(IdInput input);
		Task<ListResultOutput<IncentiveCategoryListDto>> GetIncentiveCategoryCodes();
		Task<PagedResultOutput<IncentiveTagListDto>> GetAll(GetIncentiveTagInput inputDto);
		Task<FileDto> GetAllToExcel();
		Task<GetIncentiveTagForEditOutput> GetIncentiveTagForEdit(NullableIdInput nullableIdInput);
		Task CreateOrUpdateIncentiveTag(CreateOrUpdateIncentiveTagInput input);
		Task DeleteIncentiveTag(IdInput input);
		Task<ListResultOutput<IncentiveTagListDto>> GetIncentiveTagCodes();
		Task<ListResultOutput<ComboboxItemDto>> GetIncetivEntityTypeForCombobox();
		Task<ListResultOutput<ComboboxItemDto>> GetIncentiveGenerationModeForCombobox();
		Task<ListResultOutput<IncentiveTagListDto>> GetNonAutomaticIncentiveTagCodes();
        Task DeleteEmployeeParticularIncentive(EmployeeIncentiveDto input);
        Task<ListResultOutput<ComboboxItemDto>> GetIncentivePayModeForCombobox();
        Task<ListResultOutput<ComboboxItemDto>> GetAllowanceOrDeductionModeForCombobox();
        Task<ListResultOutput<IncentiveTagListDto>> GetSalaryTagCodes();
        Task<ListResultOutput<IncentiveTagListDto>> GetNonAutomaticSalaryTagCodes();
        Task<ListResultOutput<IncentiveTagListDto>> GetAllowanceSalaryTagCodes(NullableEmployeeId input);
        Task<ListResultOutput<IncentiveTagListDto>> GetDeductionSalaryTagCodes(NullableEmployeeId input);
        Task<bool> CheckSystemIncentiveList();

        Task ImportSystemIncentiveList();

        Task<ListResultOutput<IncentiveTagListDto>> GetStatutoryAllowanceSalaryTagCodes(NullableEmployeeId input);
        Task<ListResultOutput<IncentiveTagListDto>> GetStatutoryDeductionSalaryTagCodes(NullableEmployeeId input);

    }
}