﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.Hr.Impl;
using System;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using System.Reflection;
using System.IO;
using System.Text;
using System.Net.Mail;
using DinePlan.DineConnect.Hr;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.House.Master.Dtos;

namespace DinePlan.DineConnect.Hr.Master.Implementation
{
    public class EmployeeDocumentInfoAppService : DineConnectAppServiceBase, IEmployeeDocumentInfoAppService
    {

        private readonly IEmployeeDocumentInfoListExcelExporter _employeedocumentinfoExporter;
        private readonly IEmployeeDocumentInfoManager _employeedocumentinfoManager;
        private readonly IRepository<EmployeeDocumentInfo> _employeedocumentinfoRepo;
        private readonly IRepository<PersonalInformation> _personalInformationRepo;
        private readonly IRepository<DocumentInfo> _documentinfoRepo;
        private readonly IRepository<DocumentForSkillSet> _documentForSkillSetRepo;
        private readonly IRepository<SkillSet> _skillSetRepo;
        private readonly IRepository<EmployeeSkillSet> _employeeskillSetRepo;
        private readonly IRepository<Company> _companyRepo;
        private readonly IRepository<Location> _locationRepo;


        public EmployeeDocumentInfoAppService(IEmployeeDocumentInfoManager employeedocumentinfoManager,
            IRepository<EmployeeDocumentInfo> employeeDocumentInfoRepo,
            IEmployeeDocumentInfoListExcelExporter employeedocumentinfoExporter,
            IRepository<PersonalInformation> personalInformationRepo,
            IRepository<DocumentForSkillSet> documentForSkillSetRepo,
            IRepository<SkillSet> skillSetRepo,
            IRepository<DocumentInfo> documentinfoRepo,
             IRepository<Company> companyRepo,
            IRepository<EmployeeSkillSet> employeeskillSetRepo,
            IRepository<Location> locationRepo)
        {
            _employeedocumentinfoManager = employeedocumentinfoManager;
            _employeedocumentinfoRepo = employeeDocumentInfoRepo;
            _employeedocumentinfoExporter = employeedocumentinfoExporter;
            _personalInformationRepo = personalInformationRepo;
            _documentinfoRepo = documentinfoRepo;
            _documentForSkillSetRepo = documentForSkillSetRepo;
            _skillSetRepo = skillSetRepo;
            _employeeskillSetRepo = employeeskillSetRepo;
            _companyRepo = companyRepo;
            _locationRepo = locationRepo;
        }

        public async Task<PagedResultOutput<EmployeeDocumentInfoListDto>> GetAll(GetEmployeeDocumentInfoInput input)
        {
            int maxResultCount = input.MaxResultCount;
            IQueryable<EmployeeDocumentInfoListDto> allItems;

            IQueryable<PersonalInformation> dvPerinfo;

            dvPerinfo = _personalInformationRepo.GetAll().
                     WhereIf(input.EmployeeRefId > 0, t => t.Id == input.EmployeeRefId);

            if (!input.ShowResignedEmployeeAlso)
                dvPerinfo = dvPerinfo.Where(t => t.LastWorkingDate == null);

            if (input.EmployeeRefId != null)
            {
                dvPerinfo = dvPerinfo.Where(t => t.Id == input.EmployeeRefId);
            }

            var rsDocuinfo = _documentinfoRepo.GetAll();
            if (input.DocumentInfoRefId.HasValue)
            {
                if (input.DocumentInfoRefId > 0)
                {
                    rsDocuinfo = rsDocuinfo.Where(t => t.Id == input.DocumentInfoRefId);
                }
            }

            var rsEmployeeDocs = _employeedocumentinfoRepo.GetAll();

            if (input.StartDate.HasValue)
            {
                rsEmployeeDocs = rsEmployeeDocs.Where(t => t.EndDate == null || (t.EndDate >= input.StartDate.Value && t.EndDate <= input.EndDate.Value));
            }
            DateTime tomo = DateTime.Today.AddDays(1);
            if (input.AlertPendingFlag || input.AlertExpiredFlag)
            {
                rsEmployeeDocs = rsEmployeeDocs.Where(t => t.EndDate.HasValue);
                input.MaxResultCount = AppConsts.MaxPageSize;
                //rsEmployeeDocs = rsEmployeeDocs.Where(t => t.AlertFrom<= tomo || t.EndDate.Value <= tomo);
            }
            string strActive = L("Active");
            allItems = (from empdoc in rsEmployeeDocs
                        join personal in dvPerinfo
                           on empdoc.EmployeeRefId equals personal.Id
                        join doc in rsDocuinfo
                        on empdoc.DocumentInfoRefId equals doc.Id
                        select new EmployeeDocumentInfoListDto()
                        {
                            Id = empdoc.Id,
                            EmployeeRefId = empdoc.EmployeeRefId,
                            EmployeeRefName = personal.EmployeeName,
                            EmployeeRefCode = personal.EmployeeCode,
                            DocumentInfoRefId = empdoc.DocumentInfoRefId,
                            DocumentInfoRefName = doc.DocumentName,
                            DocReferenceNumber = empdoc.DocReferenceNumber,
                            LifeTimeFlag = empdoc.LifeTimeFlag,
                            StartDate = empdoc.StartDate,
                            EndDate = empdoc.EndDate,
                            DefaultAlertDays = empdoc.DefaultAlertDays,
                            FileName = empdoc.FileName,
                            CreationTime = empdoc.CreationTime,
                            Status = strActive
                        }).WhereIf(
                        !input.Filter.IsNullOrEmpty(),
                        p => p.EmployeeRefName.Contains(input.Filter));

            List<EmployeeDocumentInfoListDto> sortMenuItems = new List<EmployeeDocumentInfoListDto>();
            var allItemCount = await allItems.CountAsync();

            try
            {
                sortMenuItems = await allItems
                   .OrderBy(input.Sorting)
                   .PageBy(input)
                   .ToListAsync();

                foreach (var lst in sortMenuItems)
                {
                    if (lst.EndDate.HasValue)
                    {
                        lst.AlertStartingDate = lst.EndDate.Value.AddDays(-1 * lst.DefaultAlertDays);

                        if (lst.AlertStartingDate <= tomo)
                        {
                            lst.Status = "Pending";
                            lst.DocumentAlertStatus = "Y";
                            TimeSpan timeSpan = lst.EndDate.Value.Subtract(tomo);
                            lst.NoOfDays = timeSpan.Days.ToString();
                        }
                        if (lst.EndDate <= tomo)
                        {
                            lst.Status = "Expired";
                            lst.DocumentAlertStatus = "R";
                            TimeSpan timeSpan = lst.EndDate.Value.Subtract(tomo);
                            lst.NoOfDays = timeSpan.Days.ToString();
                        }
                    }
                }
                if (input.AlertExpiredFlag || input.AlertPendingFlag)
                {
                    sortMenuItems = sortMenuItems.Where(t => t.AlertStartingDate <= tomo).ToList();
                    if (input.AlertExpiredFlag == true && input.AlertPendingFlag == true)
                        sortMenuItems = sortMenuItems.Where(t => t.Status.Equals("Expired") || t.Status.Equals("Pending")).ToList();
                    else if (input.AlertPendingFlag)
                        sortMenuItems = sortMenuItems.Where(t => t.Status.Equals("Pending")).ToList();
                    else if (input.AlertExpiredFlag)
                        sortMenuItems = sortMenuItems.Where(t => t.Status.Equals("Expired")).ToList();

                    sortMenuItems = sortMenuItems.OrderByDescending(t => t.Status).OrderBy(input.Sorting).ToList();
                    input.MaxResultCount = maxResultCount;
                    allItemCount = sortMenuItems.Count();
                }
            }
            catch (Exception ex)
            {
                MethodBase m = MethodBase.GetCurrentMethod();
                string innerExceptionMessage = ex.InnerException != null ? ex.InnerException.Message : "";
                throw new UserFriendlyException("Method : " + m.ReflectedType.Name + " : " + m.Name + " " + ex.Message + " " + innerExceptionMessage);
            }

            var allListDtos = sortMenuItems.MapTo<List<EmployeeDocumentInfoListDto>>();

            return new PagedResultOutput<EmployeeDocumentInfoListDto>(
                  allItemCount,
                  allListDtos
                  );
        }

        public async Task<FileDto> GetAllToExcel(GetEmployeeDocumentInfoInput input)
        {
            input.MaxResultCount = AppConsts.MaxPageSize;
            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<EmployeeDocumentInfoListDto>>();
            return _employeedocumentinfoExporter.ExportToFile(allListDtos);
        }

        public async Task<GetEmployeeDocumentInfoForEditOutput> GetEmployeeDocumentInfoForEdit(NullableIdInput input)
        {
            EmployeeDocumentInfoEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _employeedocumentinfoRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<EmployeeDocumentInfoEditDto>();
            }
            else
            {
                editDto = new EmployeeDocumentInfoEditDto();
            }

            return new GetEmployeeDocumentInfoForEditOutput
            {
                EmployeeDocumentInfo = editDto
            };
        }

        public async Task<IdInput> CreateOrUpdateEmployeeDocumentInfo(CreateOrUpdateEmployeeDocumentInfoInput input)
        {
            if (input.EmployeeDocumentInfo.Id.HasValue)
            {
                return await UpdateEmployeeDocumentInfo(input);
            }
            else
            {
                return await CreateEmployeeDocumentInfo(input);
            }
        }

        public async Task DeleteEmployeeDocumentInfo(IdInput input)
        {
            await _employeedocumentinfoRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task<IdInput> UpdateEmployeeDocumentInfo(CreateOrUpdateEmployeeDocumentInfoInput input)
        {
            var item = await _employeedocumentinfoRepo.GetAsync(input.EmployeeDocumentInfo.Id.Value);
            var dto = input.EmployeeDocumentInfo;

            //TODO: SERVICE EmployeeDocumentInfo Update Individually
            item.EmployeeRefId = dto.EmployeeRefId;
            item.DocumentInfoRefId = dto.DocumentInfoRefId;
            item.DefaultAlertDays = dto.DefaultAlertDays;
            item.StartDate = dto.StartDate;
            item.EndDate = dto.EndDate;
            item.LifeTimeFlag = dto.LifeTimeFlag;
            item.FileName = dto.FileName;
            item.FileExtenstionType = dto.FileExtenstionType;
            item.DocReferenceNumber = dto.DocReferenceNumber;

            CheckErrors(await _employeedocumentinfoManager.CreateSync(item));

            return new IdInput { Id = item.Id };
        }

        protected virtual async Task<IdInput> CreateEmployeeDocumentInfo(CreateOrUpdateEmployeeDocumentInfoInput input)
        {
            var dto = input.EmployeeDocumentInfo.MapTo<EmployeeDocumentInfo>();

            dto.FileExtenstionType = "PDF";

            //var dt = Convert.ToDateTime(dto.StartDate);
            //var startdate = dt.ToShortDateString();
            //if (startdate == "1/1/0001")
            //{
            //    dto.StartDate = null;
            //}

            CheckErrors(await _employeedocumentinfoManager.CreateSync(dto));

            return new IdInput { Id = dto.Id };
        }


        public async Task<ListResultOutput<ComboboxItemDto>> GetDocumentInfoForCombobox()
        {
            var lst = await _documentinfoRepo.GetAll().ToListAsync();
            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Select(e => new ComboboxItemDto(e.Id.ToString(), e.DocumentName)).ToList());
        }

        public async Task<List<DocumentInfoEditDto>> GetDocumentInfo()
        {
            List<DocumentInfoEditDto> returnOutput;

            var lst = await _documentinfoRepo.GetAll().Where(p => p.DocumentType == DocumentType.AllType || p.DocumentType == DocumentType.Employee).ToListAsync();

            returnOutput = lst.MapTo<List<DocumentInfoEditDto>>();

            return returnOutput;

        }

        public async Task<bool> EmployeeDocumentAlreadyExist(DocumentDto input)
        {
            if (_employeedocumentinfoRepo.GetAll().Any(a => a.DocumentInfoRefId == input.DocumentInfoRefId && a.EmployeeRefId == input.EmployeeRefId))
            {
                string[] strArrays = { L("DocumentAlreadyExistsForThisEmployee") };
                return true;
            }
            else
            {
                return false;
            }

        }

        public async Task<HealthDocumentOutput> GetPendingDocumentInfo(HealthDocumentDto input)
        {
            HealthDocumentOutput output = new HealthDocumentOutput();
            output.FileName = "";

            List<EmployeeDocumentInfoListDto> completedList = new List<EmployeeDocumentInfoListDto>();
            List<EmployeeDocumentInfoListDto> pendingList = new List<EmployeeDocumentInfoListDto>();

            List<int> documentPendingList = new List<int>();
            if (input.DocumentRefId>0)
            {
                documentPendingList.Add(input.DocumentRefId);
            }
            else if (input.DocumentInfoListDtos!=null && input.DocumentInfoListDtos.Count>0)
            {
                documentPendingList = input.DocumentInfoListDtos.Select(t => t.Id).ToList();
            }
            else
            {
                var docMaster = await _documentinfoRepo.GetAllListAsync();
                documentPendingList = docMaster.Select(t => t.Id).ToList();
            }


            #region Document Status - Document Type Wise , Employee Wise
            foreach (var documentRefId in documentPendingList)
            {
                var doc = await _documentinfoRepo.FirstOrDefaultAsync(t => t.Id == documentRefId);
                if (doc == null)
                    throw new UserFriendlyException(L("Document") + " " + L("NotExists"));

                var rsPersonalInfo = await _personalInformationRepo.GetAllListAsync(t => t.ActiveStatus == true);

                int[] empRefIds = new int[1];
                if (input.FilterName == "DOCS" || documentRefId != 0)
                {
                    var rsSkillLinked = await _documentForSkillSetRepo.GetAll().Where(t => t.DocumentRefId == documentRefId).ToListAsync();
                    int[] skillRefIds = new int[1];

                    if (rsSkillLinked != null)
                        skillRefIds = rsSkillLinked.Select(t => t.SkillSetRefId).ToArray();

                    var rsEmpSkillsets = await _employeeskillSetRepo.GetAll().Where(t => skillRefIds.Contains(t.SkillSetRefId)).ToListAsync();

                    if (rsEmpSkillsets != null)
                        empRefIds = rsEmpSkillsets.Select(t => t.EmployeeRefId).ToArray();
                }

                rsPersonalInfo = rsPersonalInfo.Where(t => empRefIds.Contains(t.Id)).ToList().OrderBy(t => t.EmployeeName).ToList();
                var rsEmployeeDocs = await _employeedocumentinfoRepo.GetAllListAsync((t => t.DocumentInfoRefId == documentRefId && empRefIds.Contains(t.EmployeeRefId)));
                DateTime tomo = DateTime.Today.AddDays(1);
                foreach (var emp in rsPersonalInfo)
                {
                    var docsExists = rsEmployeeDocs.FirstOrDefault(t => t.EmployeeRefId == emp.Id && t.DocumentInfoRefId == documentRefId);

                    EmployeeDocumentInfoListDto newDto = new EmployeeDocumentInfoListDto();
                    newDto.EmployeeRefId = emp.Id;
                    newDto.EmployeeRefCode = emp.EmployeeCode;
                    newDto.EmployeeRefName = emp.EmployeeName;
                    newDto.DateOfJoining = emp.DateHired;
                    newDto.DocumentInfoRefId = documentRefId;
                    newDto.DocumentInfoRefName = doc.DocumentName;

                    if (docsExists != null)
                    {
                        newDto.DocReferenceNumber = docsExists.DocReferenceNumber;
                        newDto.DefaultAlertDays = docsExists.DefaultAlertDays;
                        newDto.StartDate = docsExists.StartDate;
                        newDto.EndDate = docsExists.EndDate;
                        newDto.FileName = docsExists.FileName;
                        newDto.FileExtenstionType = docsExists.FileExtenstionType;
                        if (newDto.EndDate.HasValue)
                        {
                            newDto.AlertStartingDate = newDto.EndDate.Value.AddDays(-1 * newDto.DefaultAlertDays);
                            if (newDto.AlertStartingDate <= tomo)
                            {
                                newDto.DocumentAlertStatus = "Y";
                                newDto.Status = "Pending";
                                TimeSpan timeSpan = newDto.EndDate.Value.Subtract(tomo);
                                newDto.NoOfDays = timeSpan.Days.ToString();
                            }
                            if (newDto.EndDate <= tomo)
                            {
                                newDto.DocumentAlertStatus = "R";
                                newDto.Status = "Expired";
                                TimeSpan timeSpan = newDto.EndDate.Value.Subtract(tomo);
                                newDto.NoOfDays = timeSpan.Days.ToString();
                            }
                        }
                        completedList.Add(newDto);
                    }
                    else
                    {
                        pendingList.Add(newDto);
                    }
                }
            }
            #endregion

        
            output.PendingList = pendingList;
            output.CompletedList = completedList;
            return output;

        }


        public async Task<FileDto> GetPendingDocumentInfoIntoExcel(HealthDocumentDto input)
        {
            var allList = await GetPendingDocumentInfo(input);
            return _employeedocumentinfoExporter.PendingDocumentExportToFile(allList);
        }

        public async Task<MessageOutput> SendEmailDocumentInfo(IdInput input)
        {
            MessageOutput output = new MessageOutput();
            EmployeeDocumentDto data = new EmployeeDocumentDto();
            var documentinfo = await _employeedocumentinfoRepo.FirstOrDefaultAsync(t => t.Id == input.Id);
            //Attachment file path
            if (documentinfo == null)
            {
                output.ErrorMessage = L("NotExistForId", documentinfo.EmployeeRefId);
                output.SuccessFlag = false;
                return output;
            }
            else
            {
                var personal = await _personalInformationRepo.FirstOrDefaultAsync(t => t.Id == documentinfo.EmployeeRefId);
                if (personal == null)
                {
                    throw new UserFriendlyException(L("Employee") + " ?");
                }
                if (personal.Email.IsNullOrEmpty() && personal.PersonalEmail.IsNullOrEmpty())
                {
                    output.ErrorMessage = L("EmailIdIsEmpty", personal.EmployeeCode + " - " + personal.EmployeeName);
                    output.SuccessFlag = false;
                    return output;
                }

                var rsTenant = await TenantManager.GetByIdAsync(documentinfo.TenantId);
                data.EmployeeRefId = documentinfo.EmployeeRefId;
                data.DocumentInfoRefId = documentinfo.DocumentInfoRefId;


                var DocumentInfoRefNo = documentinfo.DocReferenceNumber;
                //Tenant Name
                string EmpRefId;


                EmpRefId = personal.EmployeeCode.ToUpper().ToString();
                var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == personal.LocationRefId);

                var company = await _companyRepo.FirstOrDefaultAsync(t => t.Id == location.CompanyRefId);

                string companyCode = company.Code;
                var lenCC = companyCode.Length;


                string subPath = "\\DocumentFiles\\" + EmpRefId + "\\";

                var phyPath = Path.GetFullPath(subPath);

                bool exists = Directory.Exists((phyPath));//System.IO.Server.MapPath

                if (!exists)
                {
                    throw new UserFriendlyException(EmpRefId + " " + L("FolderDoesNotExist"));
                }

                var cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = documentinfo.FileName,
                    Inline = true,
                };

                var mailMessage = new StringBuilder();
                string pdfFilePath = phyPath + documentinfo.FileName;
                //emailTemplate.Replace("{EMAIL_BODY}", mailMessage.ToString());
                string htmlLine;

                htmlLine = "<p><strong>Dear " + personal.EmployeeName + " ,</strong></p> ";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<p>Please find your document information <strong> " + documentinfo.DocReferenceNumber + " </strong> as an attachment</p>";
                mailMessage.AppendLine(htmlLine);

                htmlLine = "<p> &nbsp;</p> ";
                mailMessage.AppendLine(htmlLine);
                htmlLine = "<p> <strong> HR / EDP </strong> </p>";
                mailMessage.AppendLine(htmlLine);
                htmlLine = "<p> <strong> " + company.Name + " </strong> </p> ";
                mailMessage.AppendLine(htmlLine);
                htmlLine = "  <p> &nbsp;</p> ";
                mailMessage.AppendLine(htmlLine);
                {
                    try
                    {
                        SmtpClient client = new SmtpClient();
                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        client.EnableSsl = true;
                        client.Host = "smtp.gmail.com";
                        client.Port = 587;

                        System.Net.NetworkCredential credentials =
                            new System.Net.NetworkCredential("hr@company", "");
                        client.UseDefaultCredentials = false;
                        client.Credentials = credentials;

                        MailMessage msg = new MailMessage();
                        msg.From = new MailAddress("hr@company");
                        if (!personal.Email.IsNullOrEmpty())
                            msg.To.Add(new MailAddress(personal.Email));
                        if (!personal.PersonalEmail.IsNullOrEmpty() && personal.PersonalEmail != personal.Email)
                            msg.To.Add(new MailAddress(personal.PersonalEmail));


                        msg.Subject = "Employee document info  " + documentinfo.DocReferenceNumber; // "This is a test Email subject";
                        msg.IsBodyHtml = true;
                        msg.Body = mailMessage.ToString();
                        //Task<MessageOutput> attachment;
                        //attachment = GetSalarySlipAttachment(data);
                        try
                        {
                            msg.Attachments.Add(new Attachment(pdfFilePath));
                            if (msg.To.Count > 0)
                                client.Send(msg);
                        }
                        catch (Exception ex)
                        {
                            output.SuccessFlag = false;
                            output.ErrorMessage = L("NotAbleToSendMailError", ex.Message + ex.InnerException);
                        }
                        output.SuccessFlag = true;
                    }
                    catch (Exception ex)
                    {
                        output.SuccessFlag = false;
                        output.ErrorMessage = L("NotAbleToSendMailError", ex.Message + ex.InnerException);
                        throw;
                    }
                }
            }
            return output;
        }
    }
}