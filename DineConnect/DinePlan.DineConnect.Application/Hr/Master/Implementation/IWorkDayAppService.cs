﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Hr.Master
{
	public interface IWorkDayAppService : IApplicationService
	{
		Task<PagedResultOutput<WorkDayListDto>> GetAll(GetWorkDayInput inputDto);
		Task<FileDto> GetAllToExcel();
		Task<GetWorkDayForEditOutput> GetWorkDayForEdit(NullableIdInput nullableIdInput);
		Task CreateOrUpdateWorkDay(CreateOrUpdateWorkDayInput input);
		Task DeleteWorkDay(IdInput input);

		Task<ListResultOutput<WorkDayListDto>> GetDayOfWeekRefNames();
	}
}