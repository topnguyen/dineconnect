﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Hr.Master
{
	public interface IEmployeeVsIncentiveAppService : IApplicationService
	{
		Task<PagedResultOutput<EmployeeVsIncentiveListDto>> GetAll(GetEmployeeVsIncentiveInput inputDto);
		Task<FileDto> GetAllToExcel();
		Task<GetEmployeeVsIncentiveForEditOutput> GetEmployeeVsIncentiveForEdit(NullableIdInput nullableIdInput);
		Task CreateOrUpdateEmployeeVsIncentive(CreateOrUpdateEmployeeVsIncentiveInput input);
		Task DeleteEmployeeVsIncentive(IdInput input);

		Task<ListResultOutput<EmployeeVsIncentiveListDto>> Gets();
	}
}
