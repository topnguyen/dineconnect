﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Hr.Master
{
    public interface IDcHeadMasterAppService : IApplicationService
    {
        Task<PagedResultOutput<DcHeadMasterListDto>> GetAll(GetDcHeadMasterInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetDcHeadMasterForEditOutput> GetDcHeadMasterForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateDcHeadMaster(CreateOrUpdateDcHeadMasterInput input);
        Task DeleteDcHeadMaster(IdInput input);
        Task<ListResultOutput<DcHeadMasterListDto>> GetHeadNames();
        Task<IdInput> GetHeaderIdByName(string headName);

    }
}