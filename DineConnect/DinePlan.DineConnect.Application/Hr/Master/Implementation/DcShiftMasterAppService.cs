﻿
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Utility.Exporter;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Connect.Master;
using System;

namespace DinePlan.DineConnect.Hr.Master.Implementation
{
    public class DcShiftMasterAppService : DineConnectAppServiceBase, IDcShiftMasterAppService
    {

        private readonly IExcelExporter _exporter;
        private readonly IDcShiftMasterManager _dcshiftmasterManager;
        private readonly IRepository<DcShiftMaster> _dcshiftmasterRepo;
        private readonly IRepository<DcHeadMaster> _dcHeadmasterRepo;
        private readonly IRepository<DcGroupMaster> _dcGroupMasterRepo;
        private readonly IRepository<DcStationMaster> _dcstationmasterRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<Company> _companyRepo;
        private readonly IRepository<DutyChartDetail> _dutychartdetailRepo;

        public DcShiftMasterAppService(IDcShiftMasterManager dcshiftmasterManager,
            IRepository<DcShiftMaster> dcShiftMasterRepo,
            IRepository<DcHeadMaster> dcHeadmasterRepo,
            IRepository<DcGroupMaster> dcGroupMasterRepo,
            IRepository<DcStationMaster> dcStationMasterRepo,
            IRepository<Location> locationRepo,
            IRepository<Company> companyRepo,
            IRepository<DutyChartDetail> dutychartdetailRepo,
            IExcelExporter exporter)
        {
            _dcshiftmasterManager = dcshiftmasterManager;
            _dcshiftmasterRepo = dcShiftMasterRepo;
            _exporter = exporter;
            _dcGroupMasterRepo = dcGroupMasterRepo;
            _dcHeadmasterRepo = dcHeadmasterRepo;
            _dcstationmasterRepo = dcStationMasterRepo;
            _exporter = exporter;
            _locationRepo = locationRepo;
            _companyRepo = companyRepo;
            _dutychartdetailRepo = dutychartdetailRepo;
        }

        public async Task<PagedResultOutput<DcShiftMasterListDto>> GetAll(GetDcShiftMasterInput input)
        {
            var dcGroup = _dcGroupMasterRepo.GetAll();
            if (input.DcGroupRefId.HasValue)
            {
                if (input.DcGroupRefId.Value > 0)
                {
                    dcGroup = dcGroup.Where(t => t.Id == input.DcGroupRefId.Value);
                }
            }

            var dcHead = _dcHeadmasterRepo.GetAll();
            if (input.DcHeadRefId.HasValue)
            {
                if (input.DcHeadRefId.Value > 0)
                {
                    dcHead = dcHead.Where(t => t.Id == input.DcHeadRefId.Value);
                }
            }

            List<int> locationRefIds = new List<int>();

            var rsLocation = _locationRepo.GetAll();
            if (input.LocationRefId.HasValue)
            {
                if (input.LocationRefId.Value > 0)
                    locationRefIds.Add(input.LocationRefId.Value);
            }

            if (input.Locations != null && input.Locations.Count > 0)
            {
                locationRefIds.AddRange(input.Locations.Select(t => t.Id).ToList());
            }
            if (locationRefIds.Count > 0)
                rsLocation = rsLocation.Where(t => locationRefIds.Contains(t.Id));

            var rsStation = _dcstationmasterRepo.GetAll();
            if (!input.StationName.IsNullOrEmpty())
            {
                rsStation = rsStation.Where(t => t.StationName.Contains(input.StationName));
            }

            var rsShift = _dcshiftmasterRepo.GetAll();
            if (!input.DutyDesc.IsNullOrEmpty())
            {
                rsShift = rsShift.Where(t => t.DutyDesc.Contains(input.DutyDesc));
            }

            var allItems = (from shift in rsShift
                            join stn in rsStation on shift.StationRefId equals stn.Id
                            join grp in dcGroup on stn.DcGroupRefId equals grp.Id
                            join head in dcHead on grp.DcHeadRefId equals head.Id
                            join loc in rsLocation on stn.LocationRefId equals loc.Id
                            join comp in _companyRepo.GetAll() on loc.CompanyRefId equals comp.Id
                            select new DcShiftMasterListDto
                            {
                                Id = shift.Id,
                                CompanyRefId = comp.Id,
                                CompanyRefName = comp.Code,
                                LocationRefId = shift.LocationRefId,
                                LocationRefName = loc.Code,
                                StationRefId = stn.Id,
                                StationRefName = stn.StationName,
                                DcGroupRefId = stn.DcGroupRefId,
                                DcGroupRefName = grp.GroupName,
                                DcHeadRefId = grp.DcHeadRefId,
                                DcHeadRefName = head.HeadName,
                                IsMandatory = shift.IsMandatory,
                                IsActive = shift.IsActive,
                                PriorityLevel = shift.PriorityLevel,
                                NightFlag = shift.NightFlag,
                                TimeIn = shift.TimeIn,
                                BreakFlag = shift.BreakFlag,
                                BreakOut = shift.BreakOut,
                                BreakIn = shift.BreakIn,
                                TimeOut = shift.TimeOut,
                                MinimumStaffRequired = shift.MinimumStaffRequired,
                                MaximumStaffRequired = shift.MaximumStaffRequired,
                                BreakMinutes = shift.BreakMinutes,
                                DutyDesc = shift.DutyDesc,
                                TimeDescription = shift.TimeDescription
                            });

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<DcShiftMasterListDto>>();

            var allItemCount = await allItems.CountAsync();

            foreach (var lst in allListDtos)
            {
                lst.TimeInHours = GetHours(new IdInput { Id = lst.TimeIn });
                lst.TimeInMinutes = GetMinutes(new IdInput { Id = lst.TimeIn });

                if (lst.BreakFlag)
                {
                    lst.BreakOutHours = GetHours(new IdInput { Id = lst.BreakOut });
                    lst.BreakOutMinutes = GetMinutes(new IdInput { Id = lst.BreakOut });

                    lst.BreakInHours = GetHours(new IdInput { Id = lst.BreakIn });
                    lst.BreakInMinutes = GetMinutes(new IdInput { Id = lst.BreakIn });
                }

                lst.TimeOutHours = GetHours(new IdInput { Id = lst.TimeOut });
                lst.TimeOutMinutes = GetMinutes(new IdInput { Id = lst.TimeOut });

                lst.TimeDescription = L("TI") + " : " + lst.TimeInHours + ':' + lst.TimeInMinutes.PadLeft(2, '0'); // String("0" + vm.timeinminutes).slice(-2);

                if (lst.NightFlag == true)
                    lst.TimeDescription = lst.TimeDescription + " (N)";

                if (lst.BreakFlag == true)
                {
                    lst.TimeDescription = lst.TimeDescription + " - " + L("BO") + " : " + lst.BreakOutHours + ':' + lst.BreakOutMinutes.PadLeft(2, '0'); //String("0" + lst.BreakOutMinutes).slice(-2);
                    lst.TimeDescription = lst.TimeDescription + " - " + L("BI") + " : " + lst.BreakInHours + ':' + lst.BreakInMinutes.PadLeft(2, '0'); // String("0" + lst.BreakInMinutes).slice(-2);
                }

                lst.TimeDescription = lst.TimeDescription + " - " + L("TO") + " : " + lst.TimeOutHours + ':' + lst.TimeOutMinutes.PadLeft(2, '0'); // String("0" + lst.TimeOutHours).slice(-2);
            }

            return new PagedResultOutput<DcShiftMasterListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {


            var allList = await _dcshiftmasterRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<DcShiftMasterListDto>>();
            var baseE = new BaseExportObject()
            {
                ExportObject = allListDtos,
                ColumnNames = new string[] { "Id" }
            };
            return _exporter.ExportToFile(baseE, L("DcShiftMaster"));

        }

        public async Task<GetDcShiftMasterForEditOutput> GetDcShiftMasterForEdit(NullableIdInput input)
        {
            DcShiftMasterEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _dcshiftmasterRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<DcShiftMasterEditDto>();


                var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == editDto.LocationRefId);
                editDto.LocationRefName = location.Code;

                var company = await _companyRepo.FirstOrDefaultAsync(t => t.Id == location.CompanyRefId);
                editDto.CompanyRefName = company.Code;

                var dcStation = await _dcstationmasterRepo.FirstOrDefaultAsync(t => t.Id == editDto.StationRefId);
                editDto.DcGroupRefId = dcStation.DcGroupRefId;

                var dcGroup = await _dcGroupMasterRepo.FirstOrDefaultAsync(t => t.Id == dcStation.DcGroupRefId);
                editDto.DcHeadRefId = dcGroup.DcHeadRefId;
            }
            else
            {
                editDto = new DcShiftMasterEditDto();
            }

            return new GetDcShiftMasterForEditOutput
            {
                DcShiftMaster = editDto
            };
        }

        public async Task<bool> ExistAllServerLevelBusinessRules(CreateOrUpdateDcShiftMasterInput input)
        {
            var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.DcShiftMaster.LocationRefId);
            if (location == null)
            {
                throw new UserFriendlyException(L("Loation") + "?");
            }

            var dcM = input.DcShiftMaster;

            if (dcM.BreakFlag == false)
            {
                if (dcM.NightFlag == false)
                {
                    if (dcM.TimeIn > dcM.TimeOut)
                    {
                        throw new UserFriendlyException(L("TimeInTimeOutInfo"));
                    }
                }
            }
            else if (dcM.BreakFlag == true)
            {
                if (dcM.NightFlag == false)
                {
                    if (dcM.TimeIn > dcM.TimeOut)
                    {
                        throw new UserFriendlyException(L("TimeInTimeOutInfo"));
                    }
                    if (dcM.TimeIn > dcM.BreakOut)
                    {
                        throw new UserFriendlyException(L("TimeInBreakOutInfo"));
                    }
                    if (dcM.BreakOut > dcM.BreakIn)
                    {
                        throw new UserFriendlyException(L("BreakFlagInfo"));
                    }
                    if (dcM.BreakIn > dcM.TimeOut)
                    {
                        throw new UserFriendlyException(L("TimeOutBreakInInfo"));
                    }
                }
            }
            return true;
        }

        public async Task<IdInput> CreateOrUpdateDcShiftMaster(CreateOrUpdateDcShiftMasterInput input)
        {

            if (input.DcShiftMaster.Id.HasValue)
            {
                return await UpdateDcShiftMaster(input);
            }
            else
            {
                return await CreateDcShiftMaster(input);
            }
        }

        public async Task DeleteDcShiftMaster(IdInput input)
        {
            var existAnyRef = await _dutychartdetailRepo.FirstOrDefaultAsync(t => t.ShiftRefId == input.Id);
            if (existAnyRef != null)
            {
                throw new UserFriendlyException(L("ReferenceExists", L("DutyChart"), L("Shift")));
            }
            await _dcshiftmasterRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task<IdInput> UpdateDcShiftMaster(CreateOrUpdateDcShiftMasterInput input)
        {
            var item = await _dcshiftmasterRepo.GetAsync(input.DcShiftMaster.Id.Value);
            var dto = input.DcShiftMaster;

            item.LocationRefId = dto.LocationRefId;
            item.StationRefId = dto.StationRefId;
            item.DutyDesc = dto.DutyDesc;
            item.MaximumStaffRequired = dto.MaximumStaffRequired;
            item.MinimumStaffRequired = dto.MinimumStaffRequired;
            item.PriorityLevel = dto.PriorityLevel;
            item.IsMandatory = dto.IsMandatory;
            item.IsActive = dto.IsActive;
            item.TimeIn = dto.TimeIn;
            item.BreakOut = dto.BreakOut;
            item.BreakIn = dto.BreakIn;
            item.BreakMinutes = dto.BreakMinutes;
            item.TimeOut = dto.TimeOut;
            item.TimeDescription = dto.TimeDescription;
            item.BreakFlag = dto.BreakFlag;
            item.NightFlag = dto.NightFlag;

            CheckErrors(await _dcshiftmasterManager.CreateSync(item));
            return new IdInput { Id = item.Id };
        }

        protected virtual async Task<IdInput> CreateDcShiftMaster(CreateOrUpdateDcShiftMasterInput input)
        {
            var dto = input.DcShiftMaster.MapTo<DcShiftMaster>();
            dto.IsActive = true;
            CheckErrors(await _dcshiftmasterManager.CreateSync(dto));
            return new IdInput { Id = dto.Id };
        }

        public async Task<ListResultOutput<DcShiftMasterListDto>> GetDutyDescs()
        {
            var lstDcShiftMaster = await _dcshiftmasterRepo.GetAll().ToListAsync();
            return new ListResultOutput<DcShiftMasterListDto>(lstDcShiftMaster.MapTo<List<DcShiftMasterListDto>>());
        }

        public async Task<ListResultOutput<DcShiftMasterListDto>> GetShiftNames(LocationStationCodeDto input)
        {
            var lstDcShiftMaster = await _dcshiftmasterRepo.GetAll().Where(t => t.LocationRefId == input.LocationRefId && t.StationRefId == input.StationRefId).ToListAsync();
            return new ListResultOutput<DcShiftMasterListDto>(lstDcShiftMaster.MapTo<List<DcShiftMasterListDto>>());
        }

        public string GetHours(IdInput input)
        {
            var hrs = Math.Floor((decimal)input.Id / 60);
            return hrs.ToString();
        }

        public string GetMinutes(IdInput input)
        {
            var minutes = ((decimal)input.Id % 60);
            return minutes.ToString();
        }

        public async Task<int> GetMaxUserSerialNumber()
        {
            try
            {
                var maxSno = await _dcshiftmasterRepo.GetAll().Select(t => t.PriorityLevel).MaxAsync();
                return (int.Parse(maxSno.ToString()) + 1);
            }
            catch (System.Exception)
            {
                return 1;
            }
        }

        public async Task<IdInput> CloneStationAsShift(IdInput stationinput)
        {
            var station =
                await
                    _dcstationmasterRepo.FirstOrDefaultAsync(t => t.Id == stationinput.Id);

            if (station == null)
            {
                throw new UserFriendlyException(L("NotExistForId", L("Station"), stationinput.Id));
            }
            CreateOrUpdateDcShiftMasterInput input = null;

            var shiftDto = new DcShiftMasterEditDto // DcShiftMasterEditDto
            {
                LocationRefId = station.LocationRefId,
                StationRefId = station.Id,
                DutyDesc = station.TimeDescription,
                MinimumStaffRequired = station.MinimumStaffRequired,
                MaximumStaffRequired = station.MaximumStaffRequired,
                PriorityLevel = station.PriorityLevel,
                IsMandatory = station.IsMandatory,
                IsActive = true,
                TimeDescription = station.TimeDescription,
                TimeIn = station.TimeIn,
                BreakFlag = station.BreakFlag,
                BreakOut = station.BreakOut,
                TimeOut = station.TimeOut,
                BreakIn = station.BreakIn,
                NightFlag = station.NightFlag,
            };

            input = new CreateOrUpdateDcShiftMasterInput
            {
                DcShiftMaster = shiftDto
            };

            if (input != null)
            {
                return await CreateOrUpdateDcShiftMaster(input);
            }
            return null;
        }

    }


}
