﻿
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.Hr.Impl;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Hr.Master.Implementation
{
	public class WorkDayAppService : DineConnectAppServiceBase, IWorkDayAppService
	{

		private readonly IWorkDayListExcelExporter _workdayExporter;
		private readonly IWorkDayManager _workdayManager;
		private readonly IRepository<WorkDay> _workdayRepo;

		public WorkDayAppService(IWorkDayManager workdayManager,
			IRepository<WorkDay> workDayRepo,
			IWorkDayListExcelExporter workdayExporter)
		{
			_workdayManager = workdayManager;
			_workdayRepo = workDayRepo;
			_workdayExporter = workdayExporter;

		}

		public async Task<PagedResultOutput<WorkDayListDto>> GetAll(GetWorkDayInput input)
		{
			var allItems = _workdayRepo.GetAll();
			if (input.Operation.IsNullOrEmpty())
			{
				allItems = _workdayRepo
			   .GetAll()
			   .WhereIf(
				   !input.Filter.IsNullOrEmpty(),
				   p => p.DayOfWeekRefName.Equals(input.Filter)
			   );
			}
			else
			{
				allItems = _workdayRepo
			   .GetAll()
			   .WhereIf(
				   !input.Filter.IsNullOrEmpty(),
				   p => p.DayOfWeekRefName.Contains(input.Filter)
			   );
			}
			var sortMenuItems = await allItems
				.OrderBy(input.Sorting)
				.PageBy(input)
				.ToListAsync();

			var allListDtos = sortMenuItems.MapTo<List<WorkDayListDto>>();

			var allItemCount = await allItems.CountAsync();

			return new PagedResultOutput<WorkDayListDto>(
				allItemCount,
				allListDtos
				);
		}

		public async Task<FileDto> GetAllToExcel()
		{
			var allList = await _workdayRepo.GetAll().ToListAsync();
			var allListDtos = allList.MapTo<List<WorkDayListDto>>();
			return _workdayExporter.ExportToFile(allListDtos);
		}

		public async Task<GetWorkDayForEditOutput> GetWorkDayForEdit(NullableIdInput input)
		{
			WorkDayEditDto editDto;

			if (input.Id.HasValue)
			{
				var hDto = await _workdayRepo.GetAsync(input.Id.Value);
				editDto = hDto.MapTo<WorkDayEditDto>();
			}
			else
			{
				editDto = new WorkDayEditDto();
			}

			return new GetWorkDayForEditOutput
			{
				WorkDay = editDto
			};
		}

		public async Task<ReturnSuccessOrFailureWithMessage> ExistAllServerLevelBusinessRules(CreateOrUpdateWorkDayInput input)
		{
			if (input.WorkDay.NoOfHourWorks + input.WorkDay.RestHours > 24)
			{
				throw new UserFriendlyException(L("TotalWorkHourIncludingRestHoursShouldBeLessThanOrEqual24Hours"));
			}
			return new ReturnSuccessOrFailureWithMessage { UserErrorNumber = 0, Message = "Success" };
		}

		public async Task CreateOrUpdateWorkDay(CreateOrUpdateWorkDayInput input)
		{
			var returnRules = await ExistAllServerLevelBusinessRules(input);
			if (returnRules.UserErrorNumber == 0)
			{
				if (input.WorkDay.Id.HasValue)
				{
					await UpdateWorkDay(input);
				}
				else
				{
					await CreateWorkDay(input);
				}
			}
		}

		public async Task DeleteWorkDay(IdInput input)
		{
			await _workdayRepo.DeleteAsync(input.Id);
		}

		protected virtual async Task UpdateWorkDay(CreateOrUpdateWorkDayInput input)
		{
			var item = await _workdayRepo.GetAsync(input.WorkDay.Id.Value);
			var dto = input.WorkDay;

			item.RestHours = dto.RestHours;
			item.NoOfHourWorks = dto.NoOfHourWorks;
			item.OtRatio = dto.OtRatio;
			item.NoOfWorkDays = dto.NoOfWorkDays;


			CheckErrors(await _workdayManager.CreateSync(item));
		}

		protected virtual async Task CreateWorkDay(CreateOrUpdateWorkDayInput input)
		{
			var dto = input.WorkDay.MapTo<WorkDay>();

			CheckErrors(await _workdayManager.CreateSync(dto));
		}

		public async Task<ListResultOutput<WorkDayListDto>> GetDayOfWeekRefNames()
		{
			var lstWorkDay = await _workdayRepo.GetAll().ToListAsync();
			return new ListResultOutput<WorkDayListDto>(lstWorkDay.MapTo<List<WorkDayListDto>>());
		}
	}
}