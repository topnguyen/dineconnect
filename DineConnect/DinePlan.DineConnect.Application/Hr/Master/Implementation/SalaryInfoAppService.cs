﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Hr.Impl;
using System;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using Abp.UI;
using DinePlan.DineConnect.Features;
using Abp.Application.Features;
using DinePlan.DineConnect.Hr;
using DinePlan.DineConnect.Connect.Master;

namespace DinePlan.DineConnect.Hr.Master.Implementation
{
    public class SalaryInfoAppService : DineConnectAppServiceBase, ISalaryInfoAppService
    {

        private readonly ISalaryInfoListExcelExporter _salaryinfoExporter;
        private readonly ISalaryInfoManager _salaryinfoManager;
        private readonly IRepository<SalaryInfo> _salaryinfoRepo;
        private readonly IRepository<PersonalInformation> _personalinformationRepo;
        private readonly IRepository<Company> _companyRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<EmployeeVsIncentive> _employeeVsIncentiveRepo;
        private readonly IRepository<IncentiveTag> _incentiveTagRepo;
        private readonly IRepository<IncentiveExcludedList> _incentiveExcludedRepo;
        private readonly IRepository<EmployeeVsSalaryTag> _employeevsSalaryTagRepo;
        private readonly IRepository<IncentiveCategory> _incentiveCategoryRepo;
        private readonly IRepository<IncentiveVsEntity> _incentivevsentityRepo;
        private readonly IRepository<DocumentForSkillSet> _documentForSkillSetRepo;
        private readonly IRepository<EmployeeDocumentInfo> _employeeDocumentinfoRepo;
        private readonly IRepository<DocumentInfo> _documentinfoRepo;
        private readonly IRepository<SkillSet> _skillsetRepo;
        private readonly IRepository<EmployeeSkillSet> _employeeskillsetRepo;
        private readonly IRepository<WorkDay> _workDayRepo;
        private readonly IRepository<EmployeeResidentStatus> _employeeResidentStatusRepo;
        private readonly IRepository<EmployeeRace> _employeeRaceStatusRepo;
        private readonly IRepository<EmployeeReligion> _employeeReligionStatusRepo;
        private readonly IRepository<OtBasedOnSlab> _otBasedOnSlabRepo;
        private readonly IRepository<JobTitleMaster> _jobTitleMasterRepo;

        public SalaryInfoAppService(ISalaryInfoManager salaryinfoManager,
            IRepository<SalaryInfo> salaryInfoRepo,
            IRepository<Company> companyRepo,
            ISalaryInfoListExcelExporter salaryinfoExporter,
            IRepository<PersonalInformation> personalinformationRepo,
            IRepository<IncentiveTag> incentiveTagRepo,
            IRepository<EmployeeVsIncentive> employeeVsIncentiveRepo,
            IRepository<IncentiveExcludedList> incentiveExcludedRepo,
            IRepository<EmployeeVsSalaryTag> employeevsSalaryTagRepo,
            IRepository<IncentiveCategory> incentiveCategoryRepo,
            IRepository<IncentiveVsEntity> incentivevsentityRepo,
            IRepository<DocumentForSkillSet> documentForSkillSetRepo,
            IRepository<EmployeeDocumentInfo> employeeDocumentinfoRepo,
            IRepository<DocumentInfo> documentinfoRepo,
            IRepository<SkillSet> skillsetRepo,
            IRepository<EmployeeSkillSet> employeeskillsetRepo,
            IRepository<WorkDay> workDayRepo,
            IRepository<EmployeeResidentStatus> employeeResidentStatusRepo,
            IRepository<EmployeeRace> employeeRaceStatusRepo,
            IRepository<Location> locationRepo,
             IRepository<EmployeeReligion> employeeReligionStatusRepo,
            IRepository<OtBasedOnSlab> otBasedOnSlabRepo,
                    IRepository<JobTitleMaster> jobTitleMasterRepo

        )
        {
            _salaryinfoManager = salaryinfoManager;
            _salaryinfoRepo = salaryInfoRepo;
            _salaryinfoExporter = salaryinfoExporter;
            _personalinformationRepo = personalinformationRepo;
            _companyRepo = companyRepo;
            _employeeVsIncentiveRepo = employeeVsIncentiveRepo;
            _incentiveTagRepo = incentiveTagRepo;
            _incentiveExcludedRepo = incentiveExcludedRepo;
            _employeevsSalaryTagRepo = employeevsSalaryTagRepo;
            _incentiveCategoryRepo = incentiveCategoryRepo;
            _incentivevsentityRepo = incentivevsentityRepo;
            _documentForSkillSetRepo = documentForSkillSetRepo;
            _employeeDocumentinfoRepo = employeeDocumentinfoRepo;
            _documentinfoRepo = documentinfoRepo;
            _skillsetRepo = skillsetRepo;
            _employeeskillsetRepo = employeeskillsetRepo;
            _workDayRepo = workDayRepo;
            _employeeResidentStatusRepo = employeeResidentStatusRepo;
            _employeeRaceStatusRepo = employeeRaceStatusRepo;
            _locationRepo = locationRepo;
            _employeeReligionStatusRepo = employeeReligionStatusRepo;
            _otBasedOnSlabRepo = otBasedOnSlabRepo;
            _jobTitleMasterRepo = jobTitleMasterRepo;
        }

        public async Task<PagedResultOutput<SalaryInfoListDto>> GetAll(GetSalaryInfoInput input)
        {
            var rsCompany = _companyRepo.GetAll();
            if (input.CompanyList != null)
            {
                if (input.CompanyList.Count > 0)
                {
                    List<int> companyRefIds = input.CompanyList.Select(t => t.Id).ToList();
                    rsCompany = rsCompany.Where(t => companyRefIds.Contains(t.Id));
                }
            }

            var rsLocation = _locationRepo.GetAll();
            if (input.LocationList != null)
            {
                if (input.LocationList.Count > 0)
                {
                    List<int> locationRefIds = input.LocationList.Select(t => t.Id).ToList();
                    rsLocation = rsLocation.Where(t => locationRefIds.Contains(t.Id));
                }
            }

            var allItems = (from salin in _salaryinfoRepo.GetAll()
                            join pi in _personalinformationRepo.GetAll()
                            on salin.EmployeeRefId equals pi.Id
                            join location in rsLocation
                               on pi.LocationRefId equals location.Id
                            join company in rsCompany
                           on location.CompanyRefId equals company.Id
                            join jt in _jobTitleMasterRepo.GetAll() on pi.JobTitleRefId equals jt.Id
                            select new SalaryInfoListDto()
                            {
                                Id = salin.Id,
                                EmployeeRefId = salin.EmployeeRefId,
                                EmployeeRefCode = pi.EmployeeCode,
                                EmployeeRefName = pi.EmployeeName,
                                ProfilePictureId = pi.ProfilePictureId,
                                LocationRefId = pi.LocationRefId,
                                LocationRefName = location.Code,
                                JobTitleRefId = pi.JobTitleRefId.Value,
                                JobTitle = jt.JobTitle,
                                CompanyRefCode = company.Code,
                                CompanyRefName = company.Name,
                                SalaryMode = salin.SalaryMode,
                                WithEffectFrom = salin.WithEffectFrom,
                                BasicSalary = salin.BasicSalary,
                                Levy = salin.Levy,
                                CreationTime = salin.CreationTime,
                                Ot1PerHour = salin.Ot1PerHour,
                                Ot2PerHour = salin.Ot2PerHour,
                                OtAllowedIfWorkTimeOver = salin.OtAllowedIfWorkTimeOver,
                                LastWorkingDate = pi.LastWorkingDate,
                                ReportOtPerHour = salin.ReportOtPerHour,
                                NormalOTPerHour = salin.NormalOTPerHour,
                                MaximumOTAllowedEffectiveFrom = salin.MaximumOTAllowedEffectiveFrom,
                                MaximumAllowedOTHoursOnPublicHoliday = salin.MaximumAllowedOTHoursOnPublicHoliday,
                                IdleStandByCost = salin.IdleStandByCost,
                                IncentiveAndOTCalculationPeriodRefId = salin.IncentiveAndOTCalculationPeriodRefId,
                                HourlySalaryModeRefId = salin.HourlySalaryModeRefId
                            }).WhereIf(
                               !input.Filter.IsNullOrEmpty(),
                               p => p.EmployeeRefName.Contains(input.Filter)
               );

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<SalaryInfoListDto>>();

            var allItemCount = await allItems.CountAsync();

            foreach (var lst in allListDtos)
            {
                lst.IncentiveAndOTCalculationPeriodRefName = lst.IncentiveAndOTCalculationPeriodRefId == (int)IncentiveOTCalculationPeriod.Salary_Month ? "Salary Month" : "Previous Month";
                if (lst.HourlySalaryModeRefId.HasValue)
                {
                    if (lst.HourlySalaryModeRefId == (int)HourlySalaryMode.Claimed_Hours)
                        lst.HourlySalaryModeRefName = "Claimed Hours";
                    else if (lst.HourlySalaryModeRefId == (int)HourlySalaryMode.Attendance_Hours)
                        lst.HourlySalaryModeRefName = "Attendance Hours";
                    else if (lst.HourlySalaryModeRefId == (int)HourlySalaryMode.WorkDay_Hours)
                        lst.HourlySalaryModeRefName = "Work Day Hours";
                }
            }


            return new PagedResultOutput<SalaryInfoListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _salaryinfoRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<SalaryInfoListDto>>();
            return _salaryinfoExporter.ExportToFile(allListDtos);
        }

        public async Task<GetEmployeeWorkDayDtos> GetEmployeeWorkDay(GetDataBasedOnNullableEmployee input)
        {
            GetEmployeeWorkDayDtos output = new GetEmployeeWorkDayDtos();
            var rsWorkDays = await _workDayRepo.GetAllListAsync(t => t.EmployeeRefId == input.EmployeeRefId);
            output.DifferentFromDefaultWorkDayValues = true;
            if (rsWorkDays.Count == 0)
                output.DifferentFromDefaultWorkDayValues = false;
            if (rsWorkDays.Count != 7)
            {
                List<int> existDayIds = rsWorkDays.Select(t => t.DayOfWeekRefId).ToList();
                var defaultWorkDays = await _workDayRepo.GetAllListAsync(t => t.EmployeeRefId == null && !existDayIds.Contains(t.DayOfWeekRefId));
                rsWorkDays.AddRange(defaultWorkDays);
            }

            var workDayDtos = rsWorkDays.MapTo<List<WorkDayListDto>>();
            workDayDtos = workDayDtos.OrderBy(t => t.DayOfWeekRefId).ToList();
            output.WorkDayListDtos = workDayDtos;
            return output;
        }

        public async Task<GetSalaryInfoForEditOutput> GetSalaryInfoForEdit(NullableIdInput input)
        {
            SalaryInfoEditDto editDto;
            List<IncentiveTagListDto> editDetailDtos;
            List<EmployeeVsSalaryTagListDto> editAllowanceDtos = new List<EmployeeVsSalaryTagListDto>();
            List<EmployeeVsSalaryTagListDto> editDeductionDtos = new List<EmployeeVsSalaryTagListDto>();
            List<EmployeeVsSalaryTagListDto> editStatutoryAllowanceDtos = new List<EmployeeVsSalaryTagListDto>();
            List<EmployeeVsSalaryTagListDto> editStatutoryDeductionDtos = new List<EmployeeVsSalaryTagListDto>();
            List<SkillSetListDto> skilldetailDto = new List<SkillSetListDto>();
            List<ManualOTDayWiseRestrictionsListDto> manualOTHoursList = new List<ManualOTDayWiseRestrictionsListDto>();
            List<WorkDayListDto> workDayListDtos = new List<WorkDayListDto>();
            List<OtBasedOnSlabListDto> editOtSlabLists = new List<OtBasedOnSlabListDto>();

            if (input.Id.HasValue)
            {
                var hDto = await _salaryinfoRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<SalaryInfoEditDto>();
                var pi = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == editDto.EmployeeRefId);
                editDto.EmployeeRefCode = pi.EmployeeCode;
                editDto.EmployeeRefName = pi.EmployeeName;
                editDto.ProfilePictureId = pi.ProfilePictureId;


                editDto.DateHired = pi.DateHired;
                editDto.DateOfBirth = pi.DateOfBirth;
                var jt = await _jobTitleMasterRepo.FirstOrDefaultAsync(t => t.Id == pi.JobTitleRefId);
                if (jt!=null)
                    editDto.JobTitle = jt.JobTitle;

                var rsRace = await _employeeRaceStatusRepo.FirstOrDefaultAsync(t => t.Id == pi.RaceRefId);
                editDto.Race = rsRace.RaceName;

                var rsResdient = await _employeeResidentStatusRepo.FirstOrDefaultAsync(t => t.Id == pi.EmployeeResidentStatusRefId);
                editDto.ResidentStatus = rsResdient.ResidentStatus;

                var rsReligion = await _employeeReligionStatusRepo.FirstOrDefaultAsync(t => t.Id == pi.ReligionRefId);
                editDto.Religion = rsReligion.ReligionName;

                var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == pi.LocationRefId);

                var company = await _companyRepo.FirstOrDefaultAsync(t => t.Id == location.CompanyRefId);
                editDto.CompanyRefId = company.Id;
                editDto.CompanyRefName = company.Name;
                editDto.CompanyPrefix = company.Code;

                var rsSkill = await (from es in _employeeskillsetRepo.GetAll().Where(t => t.EmployeeRefId == editDto.EmployeeRefId)
                                     join skill in _skillsetRepo.GetAll() on
                                     es.SkillSetRefId equals skill.Id
                                     select skill).ToListAsync();

                skilldetailDto = rsSkill.MapTo<List<SkillSetListDto>>();

                var rsIncentiveTagList = await _incentiveTagRepo.GetAllListAsync();

                var employeeVsIncentives = await _employeeVsIncentiveRepo.GetAllListAsync(t => t.EmployeeRefId == editDto.EmployeeRefId);

                if (employeeVsIncentives != null && employeeVsIncentives.Count > 0)
                {
                    int[] arrIds = employeeVsIncentives.Select(t => t.IncentiveTagRefId).ToArray();
                    var re = rsIncentiveTagList.Where(t => arrIds.Contains(t.Id) && t.IncentivePayModeId == (int)IncentivePayMode.Daily).ToList();
                    editDetailDtos = re.MapTo<List<IncentiveTagListDto>>();
                }
                else
                {
                    editDetailDtos = new List<IncentiveTagListDto>();
                }

                var rsOtSlabs = await _otBasedOnSlabRepo.GetAllListAsync(t => t.EmployeeRefId == editDto.EmployeeRefId);
                if (rsOtSlabs == null || rsOtSlabs.Count == 0)
                {
                    editOtSlabLists = new List<OtBasedOnSlabListDto>();
                    editDto.DoesOtSlabWiseApplicable = false;
                }
                else
                {
                    editOtSlabLists = rsOtSlabs.MapTo<List<OtBasedOnSlabListDto>>();
                    editDto.DoesOtSlabWiseApplicable = true;
                    if (editDto.Ot1PerHour > 0 || editDto.Ot2PerHour > 0)
                    {
                        throw new UserFriendlyException("If OT Slab List, Exists then you should change OT1 and OT2 Should Be Zero");
                    }
                    var flatOtCount = editOtSlabLists.Count(t => t.OTRefId == (int)OTTypes.FlatOT);
                    if (flatOtCount == 0)
                        editDto.DoesFlatOTApplicable = false;
                    else if (flatOtCount == editOtSlabLists.Count())
                        editDto.DoesFlatOTApplicable = true;
                    else
                    {
                        throw new UserFriendlyException("Flat OT Can not be added If OT1 / OT2 Exists");
                    }
                    var gpOutput = editOtSlabLists.GroupBy(t => t.OTRefId);
                    foreach (var gp in gpOutput)
                    {
                        var otGroup = gp.ToList().OrderBy(t => t.FromHours);
                        int otLoopCnt = 1;
                        foreach (var slab in otGroup)
                        {
                            if (slab.OTAmount > 0 && slab.FixedOTAmount > 0)
                            {
                                throw new UserFriendlyException("Either OT Amount or Fixed Amount should have value");
                            }

                            if (otLoopCnt == 1)
                                slab.NoOfHours = slab.ToHours - slab.FromHours + 1;
                            else
                                slab.NoOfHours = slab.ToHours - slab.FromHours;
                        }
                    }
                }

                var empSalaryTags = await _employeevsSalaryTagRepo.GetAllListAsync(t => t.EmployeeRefId == editDto.EmployeeRefId);
                if (empSalaryTags != null && empSalaryTags.Count > 0)
                {
                    var rsAllowanceTagList = rsIncentiveTagList.Where(t => t.AllowanceOrDeductionRefId == (int)AllowanceOrDeductionMode.Allowance);
                    var rsDeductionTagList = rsIncentiveTagList.Where(t => t.AllowanceOrDeductionRefId == (int)AllowanceOrDeductionMode.Deduction);

                    var rsStatutoryAllowanceTagList = rsIncentiveTagList.Where(t => t.AllowanceOrDeductionRefId == (int)AllowanceOrDeductionMode.Statutory_Allowance);
                    var rsStatutoryDeductionTagList = rsIncentiveTagList.Where(t => t.AllowanceOrDeductionRefId == (int)AllowanceOrDeductionMode.Statutory_Deduction);

                    List<int> allownaceRefIds = rsAllowanceTagList.Select(t => t.Id).ToList();
                    List<int> deductionRefIds = rsDeductionTagList.Select(t => t.Id).ToList();

                    var editSalaryDetailsDtos = empSalaryTags.MapTo<List<EmployeeVsSalaryTagListDto>>();
                    foreach (var salTag in editSalaryDetailsDtos)
                    {
                        var tag = rsIncentiveTagList.FirstOrDefault(t => t.Id == salTag.SalaryRefId);
                        salTag.SalaryRefName = tag.IncentiveTagCode;
                        salTag.IsSytemGeneratedIncentive = tag.IsSytemGeneratedIncentive;
                        salTag.IsPfPayable = tag.IsPfPayable;
                        salTag.IsGovernmentInsurancePayable = tag.IsGovernmentInsurancePayable;
                        salTag.FormulaRefId = tag.FormulaRefId;
                        if (tag.IsSytemGeneratedIncentive)
                        {
                            if (salTag.FormulaRefId.HasValue)
                            {
                                salTag.FormulaRefId = tag.FormulaRefId;
                            }
                            else
                            {
                                throw new UserFriendlyException("System Generated Incentive " + tag.IncentiveTagCode + " Formula Not Handled. Contact System Administrator");
                            }
                        }

                        salTag.IsItDeductableFromGrossSalary = tag.IsItDeductableFromGrossSalary;
                        if (salTag.SalaryTagAliasName != null && salTag.SalaryTagAliasName.Length > 0)
                            salTag.SalaryRefName = salTag.SalaryTagAliasName;

                        salTag.IncentivePayModeId = tag.IncentivePayModeId;
                        salTag.AllowanceOrDeductionRefId = tag.AllowanceOrDeductionRefId;
                        if (salTag.CalculationPayModeRefId == (int)IncentivePayMode.Daily)
                            salTag.CalculationPayModeRefName = L("Daily");
                        else
                            salTag.CalculationPayModeRefName = L("Monthly");
                    }

                    editAllowanceDtos = editSalaryDetailsDtos.Where(t => t.AllowanceOrDeductionRefId == (int)AllowanceOrDeductionMode.Allowance).ToList();
                    editDeductionDtos = editSalaryDetailsDtos.Where(t => t.AllowanceOrDeductionRefId == (int)AllowanceOrDeductionMode.Deduction).ToList();
                    editStatutoryAllowanceDtos = editSalaryDetailsDtos.Where(t => t.AllowanceOrDeductionRefId == (int)AllowanceOrDeductionMode.Statutory_Allowance).ToList();
                    editStatutoryDeductionDtos = editSalaryDetailsDtos.Where(t => t.AllowanceOrDeductionRefId == (int)AllowanceOrDeductionMode.Statutory_Deduction).ToList();
                }
                else
                {
                    editAllowanceDtos = new List<EmployeeVsSalaryTagListDto>();
                    editDeductionDtos = new List<EmployeeVsSalaryTagListDto>();
                    editStatutoryAllowanceDtos = new List<EmployeeVsSalaryTagListDto>();
                    editStatutoryDeductionDtos = new List<EmployeeVsSalaryTagListDto>();
                }

                if (editAllowanceDtos.Count(t => t.CalculationPayModeRefId == (int)IncentivePayMode.Daily) > 0)
                {
                    editDto.OverAllAllowance = 0;
                }
                else
                {
                    editDto.OverAllAllowance = editAllowanceDtos.Sum(t => t.Amount);
                }

                if (editDeductionDtos.Count(t => t.CalculationPayModeRefId == (int)IncentivePayMode.Daily) > 0)
                {
                    editDto.OverAllDeduction = 0;
                }
                else
                {
                    editDto.OverAllDeduction = editDeductionDtos.Sum(t => t.Amount);
                }
                if (editStatutoryAllowanceDtos.Count(t => t.CalculationPayModeRefId == (int)IncentivePayMode.Daily) > 0)
                {
                    editDto.OverAllStatutoryAllowance = 0;
                }
                else
                {
                    editDto.OverAllStatutoryAllowance = editStatutoryAllowanceDtos.Sum(t => t.Amount);
                }

                if (editStatutoryDeductionDtos.Count(t => t.CalculationPayModeRefId == (int)IncentivePayMode.Daily) > 0)
                {
                    editDto.OverAllStatutoryDeduction = 0;
                }
                else
                {
                    editDto.OverAllStatutoryDeduction = editStatutoryDeductionDtos.Sum(t => t.Amount);
                }
            }
            else
            {
                editDto = new SalaryInfoEditDto();
                editDetailDtos = new List<IncentiveTagListDto>();
                editAllowanceDtos = new List<EmployeeVsSalaryTagListDto>();
                editDeductionDtos = new List<EmployeeVsSalaryTagListDto>();
                skilldetailDto = new List<SkillSetListDto>();
                manualOTHoursList = new List<ManualOTDayWiseRestrictionsListDto>();
                editOtSlabLists = new List<OtBasedOnSlabListDto>();
            }

            var workDayOutput = await GetEmployeeWorkDay(new GetDataBasedOnNullableEmployee { EmployeeRefId = editDto.EmployeeRefId });

            editDto.DifferentFromDefaultWorkDayValues = workDayOutput.DifferentFromDefaultWorkDayValues;

            return new GetSalaryInfoForEditOutput
            {
                SalaryInfo = editDto,
                IncentiveList = editDetailDtos,
                AllowanceSalaryTagList = editAllowanceDtos,
                DeductionSalaryTagList = editDeductionDtos,
                SkillSetList = skilldetailDto,
                ManualOTHoursList = manualOTHoursList,
                WorkDayListDtos = workDayOutput.WorkDayListDtos,
                StatutoryAllowanceList = editStatutoryAllowanceDtos,
                StatutoryDeductionList = editStatutoryDeductionDtos,
                OtSlabList = editOtSlabLists
            };
        }

        protected async Task<ReturnSuccessOrFailureWithMessage> ExistAllServerLevelBusinessRulesForIncentives(CreateOrUpdateSalaryInfoInput input)
        {
            var emp = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == input.SalaryInfo.EmployeeRefId);
            if (emp == null)
            {
                throw new UserFriendlyException(L("Employee") + " " + input.SalaryInfo.EmployeeRefId + " " + L("NotExists"));
            }

            if (emp.LastWorkingDate.HasValue)
            {
                TimeSpan diff = DateTime.Now.Subtract(emp.LastWorkingDate.Value);
                if (Math.Abs(diff.Days) > 60)
                {
                    throw new UserFriendlyException(L("EmployeeResignedAlready", emp.EmployeeCode + " - " + emp.EmployeeName, emp.LastWorkingDate.Value.ToString("dd-MMM-yyyy")) + " " + "You can edit upto 60 days from Last Working Day Only");
                }
            }
            input.EmployeeRefId = emp.Id;

            var incentiveRefIds = input.IncentiveList.Select(t => t.Id).ToList();
            if (input.AllowanceSalaryTagList != null)
                incentiveRefIds.AddRange(input.AllowanceSalaryTagList.Select(t => t.SalaryRefId).ToList());

            if (input.DeductionSalaryTagList != null)
                incentiveRefIds.AddRange(input.DeductionSalaryTagList.Select(t => t.SalaryRefId).ToList());

            if (input.StatutoryAllowanceSalaryTagList != null)
                incentiveRefIds.AddRange(input.StatutoryAllowanceSalaryTagList.Select(t => t.SalaryRefId).ToList());

            if (input.StatutoryDeductionSalaryTagList != null)
                incentiveRefIds.AddRange(input.StatutoryDeductionSalaryTagList.Select(t => t.SalaryRefId).ToList());


            var ilist = await _incentiveTagRepo.GetAllListAsync(t => incentiveRefIds.Contains(t.Id));
            var rsIncentiveTag = ilist.MapTo<List<IncentiveTagListDto>>();
            var rsExcludedIncenitveTags = await _incentiveExcludedRepo.GetAllListAsync(t => incentiveRefIds.Contains(t.IncentiveTagRefId));

            foreach (var incTag in rsIncentiveTag)
            {
                var excludedExists = rsExcludedIncenitveTags.Where(t => t.IncentiveTagRefId == incTag.Id).ToList();
                if (excludedExists.Count == 0 || excludedExists == null)
                {
                    continue;
                }
                else
                {
                    List<int> excludedTagIds = excludedExists.Select(t => t.Id).ToList();
                    var excludedTagListAssigned = rsIncentiveTag.Where(t => excludedTagIds.Contains(t.Id)).ToList();
                    if (excludedTagListAssigned.Count > 0 && excludedTagListAssigned != null)
                    {
                        var excludeTag = excludedTagListAssigned.FirstOrDefault();
                        throw new UserFriendlyException(L("ExcludeTagIdCanNotAssigned", incTag.IncentiveTagCode, excludeTag.IncentiveTagCode));
                    }
                }

            }

            #region Min Range Max Range - Salary Tags
            if (input.AllowanceSalaryTagList != null)
            {
                foreach (var lst in input.AllowanceSalaryTagList)
                {
                    var incTag = rsIncentiveTag.FirstOrDefault(t => t.Id == lst.SalaryRefId);
                    if (incTag == null)
                    {
                        throw new UserFriendlyException(L("NotExistForId", L("Salary") + " " + L("Tag"), lst.SalaryRefId));
                    }

                    if (lst.Amount > 0 && incTag.MinRange > 0 && incTag.MaxRange > 0)
                    {
                        if (lst.Amount < incTag.MinRange)
                        {
                            throw new UserFriendlyException(incTag.IncentiveTagCode + " -  Amount  " + lst.Amount + " but Minimim Amount as Per Setting  : " + incTag.MinRange);
                        }
                        if (lst.Amount > incTag.MaxRange)
                        {
                            throw new UserFriendlyException(incTag.IncentiveTagCode + " - Amount : " + lst.Amount + " but Maximum Amount as Per Setting  : " + incTag.MaxRange);
                        }
                    }
                }
            }

            if (input.DeductionSalaryTagList != null)
            {
                foreach (var lst in input.DeductionSalaryTagList)
                {
                    var incTag = rsIncentiveTag.FirstOrDefault(t => t.Id == lst.SalaryRefId);
                    if (incTag == null)
                    {
                        throw new UserFriendlyException(L("NotExistForId", L("Salary") + " " + L("Tag"), lst.SalaryRefId));
                    }

                    if (lst.Amount > 0 && incTag.MinRange > 0 && incTag.MaxRange > 0)
                    {
                        if (lst.Amount < incTag.MinRange)
                        {
                            throw new UserFriendlyException(incTag.IncentiveTagCode + " - Amount : " + lst.Amount + " but Minimim Amount as Per Setting  : " + incTag.MinRange);
                        }
                        if (lst.Amount > incTag.MaxRange)
                        {
                            throw new UserFriendlyException(incTag.IncentiveTagCode + " - Amount : " + lst.Amount + " but Maximum Amount as Per Setting  : " + incTag.MaxRange);
                        }
                    }
                }
            }
            #endregion

            #region IncentiveBusinessRules
            {
                var iclist = await _incentiveCategoryRepo.GetAllListAsync();
                var rsIncentiveCategory = iclist.MapTo<List<IncentiveCategoryListDto>>();

                List<int> ilistRefIds = ilist.Select(t => t.Id).ToList();

                var ielist = await _incentivevsentityRepo.GetAllListAsync(t => ilistRefIds.Contains(t.IncentiveTagRefId));
                var rsIncentiveVsEntities = ielist.MapTo<List<IncentiveVsEntityListDto>>();

                var docForSkillSet = await _documentForSkillSetRepo.GetAllListAsync();
                var rsEmployeeVsDocument = await _employeeDocumentinfoRepo.GetAllListAsync(t => t.EmployeeRefId == input.EmployeeRefId);
                List<int> documentIds = rsEmployeeVsDocument.Select(t => t.DocumentInfoRefId).ToList();
                var rsDocumentInfo = await _documentinfoRepo.GetAllListAsync();

                var empinclist = await _employeeVsIncentiveRepo.GetAllListAsync(t => t.EmployeeRefId == input.EmployeeRefId);
                DateTime tomo = DateTime.Today.AddDays(1);

                foreach (var inc in rsIncentiveTag)
                {
                    var incTag = rsIncentiveTag.FirstOrDefault(t => t.Id == inc.Id);

                    #region IncentiveBasedOnEntities
                    var incEntitiesList = rsIncentiveVsEntities.Where(t => t.IncentiveTagRefId == inc.Id);
                    if (incEntitiesList != null && incEntitiesList.Count() > 0)
                    {
                        foreach (var ie in incEntitiesList)
                        {
                            //	Skill Set Reference
                            if (ie.EntityType == (int)IncentiveEntityType.SKILLSET)
                            {
                                var skillSet = await _skillsetRepo.FirstOrDefaultAsync(t => t.Id == ie.EntityRefId);
                                if (skillSet == null)
                                {
                                    throw new UserFriendlyException(L("NotExistForId", L("Skill"), ie.EntityRefId));
                                }
                                else
                                {
                                    List<int> allSkillRefIds = new List<int>();
                                    allSkillRefIds = incEntitiesList.Where(t => t.EitherOr_ForSameEntity == true && t.EntityType == (int)IncentiveEntityType.SKILLSET && t.ExcludeFlag == false).Select(t => t.EntityRefId).ToList();
                                    if (allSkillRefIds.Count() == 0)
                                    {
                                        allSkillRefIds.Add(ie.EntityRefId);
                                    }
                                    var rsAllSkillRequired = await _skillsetRepo.GetAllListAsync(t => allSkillRefIds.Contains(t.Id));

                                    var employeeAllSkills = await _employeeskillsetRepo.GetAllListAsync(t => t.EmployeeRefId == input.EmployeeRefId && allSkillRefIds.Contains(t.SkillSetRefId));
                                    var doesEmployeeHasAnyOfSkillId = false;
                                    if (ie.ExcludeFlag == false)
                                    {
                                        foreach (var skill in rsAllSkillRequired)
                                        {
                                            var empWithParticularSkill = employeeAllSkills.FirstOrDefault(t => t.EmployeeRefId == input.EmployeeRefId && t.SkillSetRefId == skill.Id);
                                            if (empWithParticularSkill != null)
                                            {
                                                doesEmployeeHasAnyOfSkillId = true;
                                                break;
                                            }
                                        }
                                        if (doesEmployeeHasAnyOfSkillId == false)
                                        {
                                            var skillList = "";
                                            foreach (var errskill in rsAllSkillRequired)
                                            {
                                                skillList = skillList + errskill.SkillCode + ",";
                                            }
                                            if (skillList.Length > 0)
                                            {
                                                skillList = skillList.Left(skillList.Length - 1);
                                            }

                                            if (allSkillRefIds.Count > 1)
                                                throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Have Any of the Skill Of " + skillList);
                                            else
                                                throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Have the Skill Of " + skillList);
                                        }
                                    }
                                    else
                                    {
                                        allSkillRefIds = new List<int>();
                                        allSkillRefIds = incEntitiesList.Where(t => t.EntityType == (int)IncentiveEntityType.SKILLSET && t.ExcludeFlag == true).Select(t => t.EntityRefId).ToList();
                                        if (allSkillRefIds.Count() == 0)
                                        {
                                            allSkillRefIds.Add(ie.EntityRefId);
                                        }
                                        rsAllSkillRequired = await _skillsetRepo.GetAllListAsync(t => allSkillRefIds.Contains(t.Id));
                                        if (employeeAllSkills != null)
                                        {
                                            var skillList = "";
                                            foreach (var errskill in rsAllSkillRequired)
                                            {
                                                skillList = skillList + errskill.SkillCode + ",";
                                            }
                                            if (skillList.Length > 0)
                                            {
                                                skillList = skillList.Left(skillList.Length - 1);
                                            }

                                            if (allSkillRefIds.Count > 1)
                                                throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Not Have Any of the Skill Of " + skillList);
                                            else
                                                throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Not Have the Skill Of " + skillList);
                                        }
                                    }
                                }
                            }

                            else if (ie.EntityType == (int)IncentiveEntityType.CERTIFICATE)
                            {
                                List<int> certificateRefIds = new List<int>();
                                certificateRefIds = incEntitiesList.Where(t => t.EitherOr_ForSameEntity == true && t.EntityType == (int)IncentiveEntityType.CERTIFICATE).Select(t => t.EntityRefId).ToList();

                                if (certificateRefIds.Count() == 0)
                                {
                                    certificateRefIds.Add(ie.EntityRefId);
                                }

                                var rsCertificateRequired = rsDocumentInfo.Where(t => certificateRefIds.Contains(t.Id));

                                foreach (var certificate in rsCertificateRequired)
                                {
                                    var empCertificateExists = rsEmployeeVsDocument.FirstOrDefault(t => t.DocumentInfoRefId == certificate.Id);
                                    if (empCertificateExists == null)
                                    {
                                        if (ie.ExcludeFlag == true)
                                        {
                                            // Do nothing 

                                        }
                                        else
                                        {
                                            throw new UserFriendlyException(incTag.IncentiveTagCode + " " + certificate.DocumentName + " " + L("Certificate") + " " + L("Not") + " " + L("Uploaded"));
                                        }
                                    }
                                    else
                                    {
                                        if (ie.ExcludeFlag == true)
                                        {
                                            certificateRefIds = new List<int>();
                                            certificateRefIds = incEntitiesList.Where(t => t.EntityType == (int)IncentiveEntityType.CERTIFICATE && t.ExcludeFlag == true).Select(t => t.EntityRefId).ToList();

                                            if (certificateRefIds.Count() == 0)
                                            {
                                                certificateRefIds.Add(ie.EntityRefId);
                                            }
                                            rsCertificateRequired = rsDocumentInfo.Where(t => certificateRefIds.Contains(t.Id));

                                        }
                                        if (empCertificateExists.EndDate.HasValue)
                                        {
                                            var alertStartingDate = empCertificateExists.EndDate.Value.AddDays(-1 * empCertificateExists.DefaultAlertDays);

                                            if (alertStartingDate <= tomo)
                                            {
                                                TimeSpan ts = empCertificateExists.EndDate.Value.Subtract(tomo);
                                                //inc.Remarks = L("Certificate") + " " + L("Due") + " " + " In" + " " + ts.Days.ToString() + " " + L("Days");
                                            }
                                            if (empCertificateExists.EndDate <= tomo)
                                            {
                                                TimeSpan timeSpan = empCertificateExists.EndDate.Value.Subtract(tomo);
                                                throw new UserFriendlyException(incTag.IncentiveTagCode + " " + L("Certificate") + " " + "Expired" + " " + timeSpan.Days.ToString() + " " + L("Days"));
                                            }
                                        }
                                    }
                                }

                            }
                            else if (ie.EntityType == (int)IncentiveEntityType.GENDER)
                            {
                                string genderToBeVerified = "";
                                if (ie.EntityRefId == 1)
                                {
                                    genderToBeVerified = "MALE";
                                }
                                else if (ie.EntityRefId == 2)
                                {
                                    genderToBeVerified = "FEMALE";
                                }
                                if (emp.Gender.ToUpper() == genderToBeVerified.ToUpper())
                                {

                                }
                                else
                                {
                                    throw new UserFriendlyException(incTag.IncentiveTagCode + " " + L("Required") + " " + L("Gender") + " " + L("Not") + " " + L("Matched"));
                                }
                            }
                            else if (ie.EntityType == (int)IncentiveEntityType.RESIDENT_STATUS)
                            {
                                string residentStatustobeVerified = "";
                                var rsResStatus = await _employeeResidentStatusRepo.FirstOrDefaultAsync(t => t.Id == ie.EntityRefId);
                                if (rsResStatus == null)
                                {
                                    throw new UserFriendlyException(L("NotExistForId", L("ResidentStatus"), ie.EntityRefId));
                                }
                                residentStatustobeVerified = rsResStatus.ResidentStatus;
                                List<int> allResidentStatusRefIds = new List<int>();
                                allResidentStatusRefIds = incEntitiesList.Where(t => t.EitherOr_ForSameEntity == true && t.EntityType == (int)IncentiveEntityType.RESIDENT_STATUS && t.ExcludeFlag == false).Select(t => t.EntityRefId).ToList();
                                if (allResidentStatusRefIds.Count() == 0)
                                {
                                    allResidentStatusRefIds.Add(ie.EntityRefId);
                                }
                                var rsAllResidentStatusRequired = await _employeeResidentStatusRepo.GetAllListAsync(t => allResidentStatusRefIds.Contains(t.Id));

                                var doesEmployeeHasAnyOfResidentStatus = false;
                                if (ie.ExcludeFlag == false)
                                {
                                    foreach (var resStatus in rsAllResidentStatusRequired)
                                    {
                                        if (resStatus.Id == emp.EmployeeResidentStatusRefId)
                                        {
                                            doesEmployeeHasAnyOfResidentStatus = true;
                                            break;
                                        }
                                    }
                                    if (doesEmployeeHasAnyOfResidentStatus == false)
                                    {
                                        var ResStatusList = "";
                                        foreach (var errskill in rsAllResidentStatusRequired)
                                        {
                                            ResStatusList = ResStatusList + errskill.ResidentStatus + ",";
                                        }
                                        if (ResStatusList.Length > 0)
                                        {
                                            ResStatusList = ResStatusList.Left(ResStatusList.Length - 1);
                                        }

                                        if (allResidentStatusRefIds.Count > 1)
                                            throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Have Any of the Resident Status Of " + ResStatusList);
                                        else
                                            throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Have the Resident Status Of " + ResStatusList);
                                    }
                                }
                                else
                                {
                                    allResidentStatusRefIds = new List<int>();
                                    allResidentStatusRefIds = incEntitiesList.Where(t => t.EntityType == (int)IncentiveEntityType.RESIDENT_STATUS && t.ExcludeFlag == true).Select(t => t.EntityRefId).ToList();
                                    if (allResidentStatusRefIds.Count() == 0)
                                    {
                                        allResidentStatusRefIds.Add(ie.EntityRefId);
                                    }
                                    rsAllResidentStatusRequired = await _employeeResidentStatusRepo.GetAllListAsync(t => allResidentStatusRefIds.Contains(t.Id));
                                    var existResidentStatus = rsAllResidentStatusRequired.Exists(t => t.Id == emp.EmployeeResidentStatusRefId);

                                    if (existResidentStatus == false)
                                    {
                                        var ResStatusList = "";
                                        foreach (var errskill in rsAllResidentStatusRequired)
                                        {
                                            ResStatusList = ResStatusList + errskill.ResidentStatus + ",";
                                        }
                                        if (ResStatusList.Length > 0)
                                        {
                                            ResStatusList = ResStatusList.Left(ResStatusList.Length - 1);
                                        }

                                        if (allResidentStatusRefIds.Count > 1)
                                            throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Not Have Any of the Resident Status Of " + ResStatusList);
                                        else
                                            throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Not Have the Resident Status Of " + ResStatusList);
                                    }
                                }

                            }
                            else if (ie.EntityType == (int)IncentiveEntityType.RACE)
                            {
                                string raceStatustobeVerified = "";
                                var rsRaceStatus = await _employeeRaceStatusRepo.FirstOrDefaultAsync(t => t.Id == ie.EntityRefId);
                                if (rsRaceStatus == null)
                                {
                                    throw new UserFriendlyException(L("NotExistForId", L("Race"), ie.EntityRefId));
                                }
                                raceStatustobeVerified = rsRaceStatus.RaceName;

                                {
                                    List<int> allRaceStatusRefIds = new List<int>();
                                    allRaceStatusRefIds = incEntitiesList.Where(t => t.EitherOr_ForSameEntity == true && t.EntityType == (int)IncentiveEntityType.RACE && t.ExcludeFlag == false).Select(t => t.EntityRefId).ToList();
                                    if (allRaceStatusRefIds.Count() == 0)
                                    {
                                        allRaceStatusRefIds.Add(ie.EntityRefId);
                                    }
                                    var rsAllRaceStatusRequired = await _employeeRaceStatusRepo.GetAllListAsync(t => allRaceStatusRefIds.Contains(t.Id));

                                    var doesEmployeeHasAnyOfRaceStatus = false;
                                    if (ie.ExcludeFlag == false)
                                    {
                                        foreach (var raceStatus in rsAllRaceStatusRequired)
                                        {
                                            if (raceStatus.Id == emp.RaceRefId)
                                            {
                                                doesEmployeeHasAnyOfRaceStatus = true;
                                                break;
                                            }
                                        }
                                        if (doesEmployeeHasAnyOfRaceStatus == false)
                                        {
                                            var ResRaceList = "";
                                            foreach (var errskill in rsAllRaceStatusRequired)
                                            {
                                                ResRaceList = ResRaceList + errskill.RaceName + ",";
                                            }
                                            if (ResRaceList.Length > 0)
                                            {
                                                ResRaceList = ResRaceList.Left(ResRaceList.Length - 1);
                                            }

                                            if (allRaceStatusRefIds.Count > 1)
                                                throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Have Any of the Race Status Of " + ResRaceList);
                                            else
                                                throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Have the Race Status Of " + ResRaceList);
                                        }
                                    }
                                    else
                                    {
                                        allRaceStatusRefIds = new List<int>();
                                        allRaceStatusRefIds = incEntitiesList.Where(t => t.EntityType == (int)IncentiveEntityType.RACE && t.ExcludeFlag == true).Select(t => t.EntityRefId).ToList();
                                        if (allRaceStatusRefIds.Count() == 0)
                                        {
                                            allRaceStatusRefIds.Add(ie.EntityRefId);
                                        }
                                        rsAllRaceStatusRequired = await _employeeRaceStatusRepo.GetAllListAsync(t => allRaceStatusRefIds.Contains(t.Id));
                                        var existResidentStatus = rsAllRaceStatusRequired.Exists(t => t.Id == emp.RaceRefId);

                                        if (existResidentStatus == false)
                                        {
                                            var raceStatusList = "";
                                            foreach (var errskill in rsAllRaceStatusRequired)
                                            {
                                                raceStatusList = raceStatusList + errskill.RaceName + ",";
                                            }
                                            if (raceStatusList.Length > 0)
                                            {
                                                raceStatusList = raceStatusList.Left(raceStatusList.Length - 1);
                                            }

                                            if (allRaceStatusRefIds.Count > 1)
                                                throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Not Have Any of the Race Status Of " + raceStatusList);
                                            else
                                                throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Not Have the Race Status Of " + raceStatusList);
                                        }
                                    }
                                }
                            }
                            else if (ie.EntityType == (int)IncentiveEntityType.RELIGION)
                            {
                                string religionStatustobeVerified = "";
                                var rsReligionStatus = await _employeeReligionStatusRepo.FirstOrDefaultAsync(t => t.Id == ie.EntityRefId);
                                if (rsReligionStatus == null)
                                {
                                    throw new UserFriendlyException(L("NotExistForId", L("Religion"), ie.EntityRefId));
                                }
                                religionStatustobeVerified = rsReligionStatus.ReligionName;

                                {
                                    List<int> allReligionStatusRefIds = new List<int>();
                                    allReligionStatusRefIds = incEntitiesList.Where(t => t.EitherOr_ForSameEntity == true && t.EntityType == (int)IncentiveEntityType.RELIGION && t.ExcludeFlag == false).Select(t => t.EntityRefId).ToList();
                                    if (allReligionStatusRefIds.Count() == 0)
                                    {
                                        allReligionStatusRefIds.Add(ie.EntityRefId);
                                    }
                                    var rsAllReligionStatusRequired = await _employeeReligionStatusRepo.GetAllListAsync(t => allReligionStatusRefIds.Contains(t.Id));

                                    var doesEmployeeHasAnyOfRaceStatus = false;
                                    if (ie.ExcludeFlag == false)
                                    {
                                        foreach (var religionStatus in rsAllReligionStatusRequired)
                                        {
                                            if (religionStatus.Id == emp.ReligionRefId)
                                            {
                                                doesEmployeeHasAnyOfRaceStatus = true;
                                                break;
                                            }
                                        }
                                        if (doesEmployeeHasAnyOfRaceStatus == false)
                                        {
                                            var ResReligionList = "";
                                            foreach (var errskill in rsAllReligionStatusRequired)
                                            {
                                                ResReligionList = ResReligionList + errskill.ReligionName + ",";
                                            }
                                            if (ResReligionList.Length > 0)
                                            {
                                                ResReligionList = ResReligionList.Left(ResReligionList.Length - 1);
                                            }

                                            if (allReligionStatusRefIds.Count > 1)
                                                throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Have Any of the Race Status Of " + ResReligionList);
                                            else
                                                throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Have the Race Status Of " + ResReligionList);
                                        }
                                    }
                                    else
                                    {
                                        allReligionStatusRefIds = new List<int>();
                                        allReligionStatusRefIds = incEntitiesList.Where(t => t.EntityType == (int)IncentiveEntityType.RACE && t.ExcludeFlag == true).Select(t => t.EntityRefId).ToList();
                                        if (allReligionStatusRefIds.Count() == 0)
                                        {
                                            allReligionStatusRefIds.Add(ie.EntityRefId);
                                        }
                                        rsAllReligionStatusRequired = await _employeeReligionStatusRepo.GetAllListAsync(t => allReligionStatusRefIds.Contains(t.Id));
                                        var existResidentStatus = rsAllReligionStatusRequired.Exists(t => t.Id == emp.ReligionRefId);

                                        if (existResidentStatus == false)
                                        {
                                            var religionStatusList = "";
                                            foreach (var errskill in rsAllReligionStatusRequired)
                                            {
                                                religionStatusList = religionStatusList + errskill.ReligionName + ",";
                                            }
                                            if (religionStatusList.Length > 0)
                                            {
                                                religionStatusList = religionStatusList.Left(religionStatusList.Length - 1);
                                            }

                                            if (allReligionStatusRefIds.Count > 1)
                                                throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Not Have Any of the Race Status Of " + religionStatusList);
                                            else
                                                throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Not Have the Race Status Of " + religionStatusList);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            #endregion

            return new ReturnSuccessOrFailureWithMessage { SuccessFlag = true, UserErrorNumber = 1, Message = "Success" };
        }

        public async Task<List<EmployeeVsSalaryTagListDto>> BulkUpdateForAllEligibleEmployeeForThisIncentive(IdInput input)
        {
            var rsIncTag = await _incentiveTagRepo.FirstOrDefaultAsync(t => t.Id == input.Id);
            var rsIncenitveVsEntity = await _incentivevsentityRepo.GetAllListAsync(t => t.IncentiveTagRefId == rsIncTag.Id);
            //if (rsIncenitveVsEntity.Count == 0)
            //{
            //    throw new UserFriendlyException("Entity Error");
            //}
            var employeeVsIncentive = await _employeeVsIncentiveRepo.GetAllListAsync(t => t.IncentiveTagRefId == rsIncTag.Id);
            List<int> employeeRefIdsAlready = employeeVsIncentive.Select(t => t.EmployeeRefId).ToList();
            var QueriyablePerInfo = _personalinformationRepo.GetAll().Where(t => !employeeRefIdsAlready.Contains(t.Id) && t.ActiveStatus == true);

            foreach (var rule in rsIncenitveVsEntity)
            {
                if (rule.EntityType == (int)IncentiveEntityType.RACE)
                {
                    List<int> allRaceStatusRefIds = new List<int>();
                    allRaceStatusRefIds = rsIncenitveVsEntity.Where(t => t.EntityType == (int)IncentiveEntityType.RACE).Select(t => t.EntityRefId).ToList();

                    QueriyablePerInfo = QueriyablePerInfo.Where(t => allRaceStatusRefIds.Contains(t.RaceRefId));
                }
                else if (rule.EntityType == (int)IncentiveEntityType.RELIGION)
                {
                    List<int> allReligtionStatusRefIds = new List<int>();
                    allReligtionStatusRefIds = rsIncenitveVsEntity.Where(t => t.EntityType == (int)IncentiveEntityType.RELIGION).Select(t => t.EntityRefId).ToList();

                    QueriyablePerInfo = QueriyablePerInfo.Where(t => allReligtionStatusRefIds.Contains(t.ReligionRefId));
                }
                else if (rule.EntityType == (int)IncentiveEntityType.RESIDENT_STATUS)
                {
                    List<int> allResidentStatusRefIds = new List<int>();
                    allResidentStatusRefIds = rsIncenitveVsEntity.Where(t => t.EntityType == (int)IncentiveEntityType.RESIDENT_STATUS).Select(t => t.EntityRefId).ToList();

                    QueriyablePerInfo = QueriyablePerInfo.Where(t => allResidentStatusRefIds.Contains(t.EmployeeResidentStatusRefId));
                }
                else if (rule.EntityType == (int)IncentiveEntityType.SKILLSET)
                {
                    List<int> allSkillSetRefIds = new List<int>();
                    allSkillSetRefIds = rsIncenitveVsEntity.Where(t => t.EntityType == (int)IncentiveEntityType.SKILLSET).Select(t => t.EntityRefId).ToList();
                    var empVsSkillset = await _employeeskillsetRepo.GetAllListAsync(t => allSkillSetRefIds.Contains(t.SkillSetRefId));
                    var empListRefId = empVsSkillset.Select(t => t.EmployeeRefId).ToList();
                    QueriyablePerInfo = QueriyablePerInfo.Where(t => empListRefId.Contains(t.Id));
                }
                else if (rule.EntityType == (int)IncentiveEntityType.CERTIFICATE)
                {
                    List<int> allDocumentRefIds = new List<int>();
                    allDocumentRefIds = rsIncenitveVsEntity.Where(t => t.EntityType == (int)IncentiveEntityType.SKILLSET).Select(t => t.EntityRefId).ToList();
                    var empVsDocument = await _employeeDocumentinfoRepo.GetAllListAsync(t => allDocumentRefIds.Contains(t.DocumentInfoRefId));
                    var empListRefId = empVsDocument.Select(t => t.EmployeeRefId).ToList();
                    QueriyablePerInfo = QueriyablePerInfo.Where(t => empListRefId.Contains(t.Id));
                }
                else if (rule.EntityType == (int)IncentiveEntityType.GENDER)
                {
                    if (rule.EntityRefId == 1)
                        QueriyablePerInfo = QueriyablePerInfo.Where(t => t.Gender.ToUpper().Equals("MALE"));
                    else if (rule.EntityRefId == 2)
                        QueriyablePerInfo = QueriyablePerInfo.Where(t => t.Gender.ToUpper().Equals("FEMALE"));
                }
                else if (rule.EntityType == (int)IncentiveEntityType.MATERIAL)
                {
                    throw new UserFriendlyException("Material Related Incentive Can not be used in Bulk Update");
                }
                else if (rule.EntityType == (int)IncentiveEntityType.WORKPOSITION)
                {
                    throw new UserFriendlyException("Work Position Related Incentive Can not be used in Bulk Update");
                }
                else if (rule.EntityType == (int)IncentiveEntityType.TEAMSIZE)
                {
                    throw new UserFriendlyException("Team Size Related Incentive Can not be used in Bulk Update");
                }
            }
            var rsPerinfo = await QueriyablePerInfo.ToListAsync();

            List<EmployeeVsSalaryTagListDto> outputList = new List<EmployeeVsSalaryTagListDto>();
            int loopCnt = 0;
            foreach (var emp in rsPerinfo)
            {
                EmployeeVsSalaryTagListDto newEmpvsIncentiveDto = new EmployeeVsSalaryTagListDto
                {
                    IncentivePayModeId = rsIncTag.IncentivePayModeId,
                    AllowanceOrDeductionRefId = rsIncTag.AllowanceOrDeductionRefId,
                    EmployeeRefId = emp.Id,
                    EmployeeCode = emp.EmployeeCode,
                    EmployeeRefName = emp.EmployeeName,
                    SalaryRefId = rsIncTag.Id,
                    SalaryRefName = rsIncTag.IncentiveTagCode,
                    SalaryTagAliasName = "",
                    EffectiveFrom = emp.DateHired,
                    Amount = rsIncTag.IncentiveAmount,
                    IsDynamicAmount = false,
                    AmountCalculatedForSalaryProcess = rsIncTag.IncentiveAmount,
                    CalculationPayModeRefId = rsIncTag.IncentivePayModeId,
                    IsFixedAmount = rsIncTag.IsFixedAmount,
                    IsSytemGeneratedIncentive = rsIncTag.IsSytemGeneratedIncentive,
                    FormulaRefId = rsIncTag.FormulaRefId,
                    IsItDeductableFromGrossSalary = rsIncTag.IsItDeductableFromGrossSalary
                };
                EmployeeSalaryTagAddOrEditDto dto = new EmployeeSalaryTagAddOrEditDto
                {
                    EmployeeVsSalaryTag = newEmpvsIncentiveDto
                };
                var newemployeevsIncentiveAdded = await AddOrEditAllowanceOrDeduction(dto);
                outputList.Add(newemployeevsIncentiveAdded);
                loopCnt++;
            }
            return outputList;
        }

        public async Task<EmployeeVsSalaryTagListDto> AddOrEditAllowanceOrDeduction(EmployeeSalaryTagAddOrEditDto input)
        {
            EmployeeVsSalaryTagListDto output = new EmployeeVsSalaryTagListDto();

            var incTag = await _incentiveTagRepo.FirstOrDefaultAsync(t => t.Id == input.EmployeeVsSalaryTag.SalaryRefId);
            if (incTag == null)
            {
                throw new UserFriendlyException(L("NotExistForId", L("Salary") + " " + L("Tag"), input.EmployeeVsSalaryTag.SalaryRefId));
            }

            if (input.EmployeeVsSalaryTag.Amount > 0 && incTag.MinRange > 0 && incTag.MaxRange > 0)
            {
                if (input.EmployeeVsSalaryTag.Amount < incTag.MinRange)
                {
                    throw new UserFriendlyException("Amount : " + input.EmployeeVsSalaryTag.Amount + " but Minimim Amount as Per Setting  : " + incTag.MinRange);
                }
                if (input.EmployeeVsSalaryTag.Amount > incTag.MaxRange)
                {
                    throw new UserFriendlyException("Amount : " + input.EmployeeVsSalaryTag.Amount + " but Maximum Amount as Per Setting  : " + incTag.MaxRange);
                }
            }

            CreateOrUpdateSalaryInfoInput datatoValidate = new CreateOrUpdateSalaryInfoInput
            {
                EmployeeRefId = input.EmployeeVsSalaryTag.EmployeeRefId,
                AllowanceSalaryTagList = new List<EmployeeVsSalaryTagListDto>(),
                DeductionSalaryTagList = new List<EmployeeVsSalaryTagListDto>(),
                StatutoryAllowanceSalaryTagList = new List<EmployeeVsSalaryTagListDto>(),
                StatutoryDeductionSalaryTagList = new List<EmployeeVsSalaryTagListDto>(),
                IncentiveList = new List<IncentiveTagListDto>(),
                SalaryInfo = new SalaryInfoEditDto()
            };

            datatoValidate.SalaryInfo.EmployeeRefId = input.EmployeeVsSalaryTag.EmployeeRefId;
            if (input.EmployeeVsSalaryTag.AllowanceOrDeductionRefId == (int)AllowanceOrDeductionMode.Allowance)
            {
                datatoValidate.AllowanceSalaryTagList.Add(input.EmployeeVsSalaryTag);
            }
            else if (input.EmployeeVsSalaryTag.AllowanceOrDeductionRefId == (int)AllowanceOrDeductionMode.Deduction)
            {
                datatoValidate.DeductionSalaryTagList.Add(input.EmployeeVsSalaryTag);
            }
            else if (input.EmployeeVsSalaryTag.AllowanceOrDeductionRefId == (int)AllowanceOrDeductionMode.Statutory_Allowance)
            {
                datatoValidate.StatutoryAllowanceSalaryTagList.Add(input.EmployeeVsSalaryTag);
            }
            else if (input.EmployeeVsSalaryTag.AllowanceOrDeductionRefId == (int)AllowanceOrDeductionMode.Statutory_Deduction)
            {
                datatoValidate.StatutoryDeductionSalaryTagList.Add(input.EmployeeVsSalaryTag);
            }

            var result = await ExistAllServerLevelBusinessRulesForIncentives(datatoValidate);

            if (result.UserErrorNumber != 1)
                return null;

            #region Salary Tag Handling

            var salaryTag = input.EmployeeVsSalaryTag;

            var exist = await _employeevsSalaryTagRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == salaryTag.EmployeeRefId && t.SalaryRefId == salaryTag.SalaryRefId);
            if (exist == null)
            {
                //DateTime temp = DateTime.Today.AddMonths(-1);
                //DateTime effectiveDate = new DateTime(temp.Year, temp.Month, 01);
                DateTime effectiveDate = salaryTag.EffectiveFrom;
                EmployeeVsSalaryTag newDto = new EmployeeVsSalaryTag
                {
                    EmployeeRefId = salaryTag.EmployeeRefId,
                    SalaryRefId = salaryTag.SalaryRefId,
                    SalaryTagAliasName = salaryTag.SalaryTagAliasName,
                    EffectiveFrom = effectiveDate,
                    Remarks = salaryTag.Remarks,
                    RollBackFrom = null,
                    Amount = salaryTag.Amount,
                    MinimumAmount = salaryTag.MinimumAmount,
                    MaximumAmount = salaryTag.MaximumAmount,
                    CalculationPayModeRefId = salaryTag.CalculationPayModeRefId,
                    IsFixedAmount = salaryTag.IsFixedAmount,
                };
                await _employeevsSalaryTagRepo.InsertOrUpdateAndGetIdAsync(newDto);
                output = newDto.MapTo<EmployeeVsSalaryTagListDto>();
            }
            else
            {
                exist.Amount = salaryTag.Amount;
                exist.EffectiveFrom = salaryTag.EffectiveFrom;
                exist.CalculationPayModeRefId = salaryTag.CalculationPayModeRefId;
                exist.SalaryTagAliasName = salaryTag.SalaryTagAliasName;
                exist.Remarks = salaryTag.Remarks;
                exist.IsFixedAmount = salaryTag.IsFixedAmount;
                exist.MinimumAmount = salaryTag.MinimumAmount;
                exist.MaximumAmount = salaryTag.MaximumAmount;
                await _employeevsSalaryTagRepo.UpdateAsync(exist);
                output = exist.MapTo<EmployeeVsSalaryTagListDto>();
            }

            #region Find Related Incentive Mode For Statutory
            if (incTag.IsSytemGeneratedIncentive == true)
            {
                EmployeeVsIncentive existWithEmployee = new EmployeeVsIncentive();
                List<IncentiveTag> existEECPFList = new List<IncentiveTag>();

                if (incTag.FormulaRefId == (int)SpecialFormulaTagReference.SG_Employer_CPF || incTag.FormulaRefId == (int)SpecialFormulaTagReference.SG_Employee_CPF)
                {
                    var existEECPF = await _incentiveTagRepo.FirstOrDefaultAsync(t => t.FormulaRefId == (int)SpecialFormulaTagReference.SG_Employee_CPF);
                    if (existEECPF == null)
                    {
                        throw new UserFriendlyException("SG Employee CPF - Missing");
                    }
                    if (incTag.Id != existEECPF.Id)
                        existEECPFList.Add(existEECPF);

                    existEECPF = await _incentiveTagRepo.FirstOrDefaultAsync(t => t.FormulaRefId == (int)SpecialFormulaTagReference.SG_Employer_CPF);
                    if (existEECPF == null)
                    {
                        throw new UserFriendlyException("SG Employer CPF - Missing");
                    }
                    if (incTag.Id != existEECPF.Id)
                        existEECPFList.Add(existEECPF);
                }
                else if (incTag.FormulaRefId == (int)SpecialFormulaTagReference.IN_Employee_PF || incTag.FormulaRefId == (int)SpecialFormulaTagReference.IN_Employer_PF || incTag.FormulaRefId == (int)SpecialFormulaTagReference.IN_Employer_PENSION)
                {
                    var existEECPF = await _incentiveTagRepo.FirstOrDefaultAsync(t => t.FormulaRefId == (int)SpecialFormulaTagReference.IN_Employee_PF);
                    if (existEECPF == null)
                    {
                        throw new UserFriendlyException("IN Employee PF - Missing");
                    }
                    if (incTag.Id != existEECPF.Id)
                        existEECPFList.Add(existEECPF);

                    existEECPF = await _incentiveTagRepo.FirstOrDefaultAsync(t => t.FormulaRefId == (int)SpecialFormulaTagReference.IN_Employer_PF);
                    if (existEECPF == null)
                    {
                        throw new UserFriendlyException("IN Employer PF - Missing");
                    }
                    if (incTag.Id != existEECPF.Id)
                        existEECPFList.Add(existEECPF);

                    existEECPF = await _incentiveTagRepo.FirstOrDefaultAsync(t => t.FormulaRefId == (int)SpecialFormulaTagReference.IN_Employer_PENSION);
                    if (existEECPF == null)
                    {
                        throw new UserFriendlyException("IN Employee Pension - Missing");
                    }
                    if (incTag.Id != existEECPF.Id)
                        existEECPFList.Add(existEECPF);

                    existEECPF = await _incentiveTagRepo.FirstOrDefaultAsync(t => t.FormulaRefId == (int)SpecialFormulaTagReference.IN_Employer_EPF_ADMIN_CHARGES);
                    if (existEECPF == null)
                    {
                        throw new UserFriendlyException("IN Employee Admin Charges - Missing");
                    }
                    if (incTag.Id != existEECPF.Id)
                        existEECPFList.Add(existEECPF);

                }
                else if (incTag.FormulaRefId == (int)SpecialFormulaTagReference.IN_ESI_Employee || incTag.FormulaRefId == (int)SpecialFormulaTagReference.IN_ESI_Employer)
                {
                    var existEECPF = await _incentiveTagRepo.FirstOrDefaultAsync(t => t.FormulaRefId == (int)SpecialFormulaTagReference.IN_ESI_Employee);
                    if (existEECPF == null)
                    {
                        throw new UserFriendlyException("IN Employee ESI - Missing");
                    }
                    if (incTag.Id != existEECPF.Id)
                        existEECPFList.Add(existEECPF);

                    existEECPF = await _incentiveTagRepo.FirstOrDefaultAsync(t => t.FormulaRefId == (int)SpecialFormulaTagReference.IN_ESI_Employer);
                    if (existEECPF == null)
                    {
                        throw new UserFriendlyException("IN Employer ESI - Missing");
                    }
                    if (incTag.Id != existEECPF.Id)
                        existEECPFList.Add(existEECPF);
                }

                foreach (var newIncList in existEECPFList)
                {
                    existWithEmployee = await _employeeVsIncentiveRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == salaryTag.EmployeeRefId && t.IncentiveTagRefId == newIncList.Id);
                    if (existWithEmployee == null)
                    {
                        #region Need To Add Child Node of Exist
                        DateTime effectiveDate = salaryTag.EffectiveFrom;
                        EmployeeVsSalaryTag newDto = new EmployeeVsSalaryTag
                        {
                            EmployeeRefId = salaryTag.EmployeeRefId,
                            SalaryRefId = newIncList.Id,
                            SalaryTagAliasName = newIncList.IncentiveTagName,
                            EffectiveFrom = effectiveDate,
                            Remarks = "",
                            RollBackFrom = null,
                            Amount = newIncList.IncentiveAmount,
                            MinimumAmount = 0,
                            MaximumAmount = 0,
                            CalculationPayModeRefId = newIncList.IncentivePayModeId,
                            IsFixedAmount = false,
                        };
                        await _employeevsSalaryTagRepo.InsertOrUpdateAndGetIdAsync(newDto);
                        #endregion
                    }
                }
            }
            #endregion

            var person = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == output.EmployeeRefId);
            output.EmployeeRefName = person.EmployeeName;
            output.EmployeeCode = person.EmployeeCode;


            var jt = await _jobTitleMasterRepo.FirstOrDefaultAsync(t => t.Id == person.JobTitleRefId);
            if (jt != null)
                output.JobTitle = jt.JobTitle;
            var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == person.LocationRefId);

            var company = await _companyRepo.FirstOrDefaultAsync(t => t.Id == location.CompanyRefId);
            output.CompanyRefCode = company.Code;

            return output;
            #endregion
        }

        public async Task<OtBasedOnSlabListDto> AddOrEditOtSlabs(EmployeeOTSlabAddOrEditDto input)
        {
            OtBasedOnSlabListDto output = new OtBasedOnSlabListDto();

            var salaryInfo = await _salaryinfoRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == input.OtBasedOnSlab.EmployeeRefId);
            if (salaryInfo == null)
            {
                throw new UserFriendlyException(L("RecordsNotFound"));
            }
            if (salaryInfo.Ot1PerHour > 0 || salaryInfo.Ot2PerHour > 0 || salaryInfo.ReportOtPerHour > 0)
            {
                throw new UserFriendlyException("If OTSlab List, Exists then you should change OT1 , OT2 and Report OT Should Be Zero");
            }

            if (input.OtBasedOnSlab.OTAmount > 0 && input.OtBasedOnSlab.FixedOTAmount > 0)
            {
                throw new UserFriendlyException("Either OT Amount or Fixed Amount should have value");
            }

            #region Flat ot Exists Then OT1 and OT2 Not allowed and Vice Versa
            if (input.OtBasedOnSlab.OTRefId == (int)OTTypes.FlatOT)
            {
                var alreadyExistOt1OrOt2 = await _otBasedOnSlabRepo.GetAllListAsync(t => t.EmployeeRefId == input.OtBasedOnSlab.EmployeeRefId && (t.OTRefId == (int)OTTypes.OT1 || t.OTRefId == (int)OTTypes.OT2));
                if (alreadyExistOt1OrOt2.Count > 0)
                {
                    throw new UserFriendlyException("Flat OT Can not be added If OT1 / OT2 Exists");
                }
            }
            else if (input.OtBasedOnSlab.OTRefId == (int)OTTypes.OT1 || input.OtBasedOnSlab.OTRefId == (int)OTTypes.OT2)
            {
                var alreadyExistOt1OrOt2 = await _otBasedOnSlabRepo.GetAllListAsync(t => t.EmployeeRefId == input.OtBasedOnSlab.EmployeeRefId && (t.OTRefId == (int)OTTypes.FlatOT));
                if (alreadyExistOt1OrOt2.Count > 0)
                {
                    throw new UserFriendlyException("OT1 / OT2 Can not be added If Flat OT Exists");
                }
            }
            #endregion

            var alreadyExist = await _otBasedOnSlabRepo.GetAllListAsync(t => t.EmployeeRefId == input.OtBasedOnSlab.EmployeeRefId && t.OTRefId == input.OtBasedOnSlab.OTRefId && t.ToHours <= input.OtBasedOnSlab.FromHours && t.Id != input.OtBasedOnSlab.Id);
            if (alreadyExist != null && alreadyExist.Count > 0)
            {
                var previousRecords = alreadyExist.MapTo<List<OtBasedOnSlabListDto>>();
                previousRecords = previousRecords.OrderBy(t => t.ToHours).ToList();
                if (previousRecords.Last().ToHours != input.OtBasedOnSlab.FromHours)
                {
                    throw new UserFriendlyException("From Hours Should Be " + previousRecords.Last().ToHours);
                }
            }

            #region OT Slab Based Handling

            var otBasedOnSlab = input.OtBasedOnSlab;
            var exist = new OtBasedOnSlab();

            if (otBasedOnSlab.Id == 0)
            {
                exist = null;
            }
            else
            {
                exist = await _otBasedOnSlabRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == otBasedOnSlab.EmployeeRefId && t.OTRefId == otBasedOnSlab.OTRefId && t.Id == otBasedOnSlab.Id);
                if (exist == null)
                {
                    throw new UserFriendlyException("Error");
                }
            }
            if (exist == null)
            {
                OtBasedOnSlab newDto = new OtBasedOnSlab
                {
                    EmployeeRefId = otBasedOnSlab.EmployeeRefId,
                    OTRefId = otBasedOnSlab.OTRefId,
                    FromHours = otBasedOnSlab.FromHours,
                    ToHours = otBasedOnSlab.ToHours,
                    OTAmount = otBasedOnSlab.OTAmount,
                    FixedOTAmount = otBasedOnSlab.FixedOTAmount
                };
                await _otBasedOnSlabRepo.InsertOrUpdateAndGetIdAsync(newDto);
                output = newDto.MapTo<OtBasedOnSlabListDto>();
            }
            else
            {
                exist.FromHours = otBasedOnSlab.FromHours;
                exist.ToHours = otBasedOnSlab.ToHours;
                exist.OTAmount = otBasedOnSlab.OTAmount;
                exist.FixedOTAmount = otBasedOnSlab.FixedOTAmount;
                await _otBasedOnSlabRepo.UpdateAsync(exist);
                output = exist.MapTo<OtBasedOnSlabListDto>();
            }

            return output;
            #endregion
        }

        public async Task<OtBasedOnSlabListDto> DeleteOtSlabs(EmployeeOTSlabAddOrEditDto input)
        {
            OtBasedOnSlabListDto output = new OtBasedOnSlabListDto();

            var salaryInfo = await _salaryinfoRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == input.OtBasedOnSlab.EmployeeRefId);
            if (salaryInfo == null)
            {
                throw new UserFriendlyException(L("RecordsNotFound"));
            }
            if (salaryInfo.Ot1PerHour > 0 || salaryInfo.Ot2PerHour > 0 || salaryInfo.ReportOtPerHour > 0)
            {
                throw new UserFriendlyException("If OTSlab List, Exists then you should change OT1 , OT2 and Report OT Should Be Zero");
            }

            var recordsToBeDeleted = await _otBasedOnSlabRepo.GetAllListAsync(t => t.EmployeeRefId == input.OtBasedOnSlab.EmployeeRefId && t.OTRefId == input.OtBasedOnSlab.OTRefId && t.FromHours >= input.OtBasedOnSlab.FromHours);

            #region OT Slab Based Handling

            foreach (var lst in recordsToBeDeleted)
            {
                await _otBasedOnSlabRepo.DeleteAsync(t => t.Id == lst.Id);
            }

            return output;
            #endregion
        }

        public async Task DeleteEmployeeParticularSalaryTag(EmployeeSalaryDto input)
        {
            var exists = await _employeevsSalaryTagRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == input.EmployeeRefId && t.SalaryRefId == input.SalaryRefId);
            if (exists != null)
            {
                EmployeeVsSalaryTagListDto employeeVsSalaryTagListDto = new EmployeeVsSalaryTagListDto();
                employeeVsSalaryTagListDto = exists.MapTo<EmployeeVsSalaryTagListDto>();
                EmployeeSalaryTagAddOrEditDto dto = new EmployeeSalaryTagAddOrEditDto
                {
                    EmployeeVsSalaryTag = employeeVsSalaryTagListDto
                };
                await DeleteAllowanceOrDeduction(dto);
            }
        }


        public async Task<bool> DeleteAllowanceOrDeduction(EmployeeSalaryTagAddOrEditDto input)
        {
            var salaryTag = input.EmployeeVsSalaryTag;

            var incTag = await _incentiveTagRepo.FirstOrDefaultAsync(t => t.Id == salaryTag.SalaryRefId);
            if (incTag == null)
            {
                throw new UserFriendlyException(L("NotExistForId", L("Salary") + " " + L("Tag"), salaryTag.SalaryRefId));
            }

            var emp = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == salaryTag.EmployeeRefId);
            if (emp == null)
            {
                throw new UserFriendlyException(L("NotExistForId", L("Employee"), salaryTag.SalaryRefId));
            }
            List<EmployeeVsSalaryTag> toBedeletedList = new List<EmployeeVsSalaryTag>();

            var exist = await _employeevsSalaryTagRepo.FirstOrDefaultAsync(a => a.EmployeeRefId == salaryTag.EmployeeRefId
                    && a.SalaryRefId == salaryTag.SalaryRefId && a.Id == salaryTag.Id);
            if (exist == null)
            {
                throw new UserFriendlyException(L("NoMoreRecordsFoundOnData", L("Incentive")));
            }
            else
            {
                if (incTag.IsSytemGeneratedIncentive)
                {
                    List<int> incentiveRefIds = new List<int>();
                    List<IncentiveTag> incentiveRelatedList = new List<IncentiveTag>();

                    if (incTag.FormulaRefId == (int)SpecialFormulaTagReference.SG_Employer_CPF || incTag.FormulaRefId == (int)SpecialFormulaTagReference.SG_Employee_CPF)
                    {
                        incentiveRelatedList = await _incentiveTagRepo.GetAllListAsync(t => t.FormulaRefId == (int)SpecialFormulaTagReference.SG_Employer_CPF || t.FormulaRefId == (int)SpecialFormulaTagReference.SG_Employee_CPF);
                    }
                    else if (incTag.FormulaRefId == (int)SpecialFormulaTagReference.IN_Employee_PF || incTag.FormulaRefId == (int)SpecialFormulaTagReference.IN_Employer_PF || incTag.FormulaRefId == (int)SpecialFormulaTagReference.IN_Employer_PENSION || incTag.FormulaRefId == (int)SpecialFormulaTagReference.IN_Employer_EPF_ADMIN_CHARGES)
                    {
                        incentiveRelatedList = await _incentiveTagRepo.GetAllListAsync(t => t.FormulaRefId == (int)SpecialFormulaTagReference.IN_Employee_PF || t.FormulaRefId == (int)SpecialFormulaTagReference.IN_Employer_PF || t.FormulaRefId == (int)SpecialFormulaTagReference.IN_Employer_PENSION || t.FormulaRefId == (int)SpecialFormulaTagReference.IN_Employer_EPF_ADMIN_CHARGES);
                    }
                    else if (incTag.FormulaRefId == (int)SpecialFormulaTagReference.IN_ESI_Employee || incTag.FormulaRefId == (int)SpecialFormulaTagReference.IN_ESI_Employer)
                    {
                        incentiveRelatedList = await _incentiveTagRepo.GetAllListAsync(t => t.FormulaRefId == (int)SpecialFormulaTagReference.IN_ESI_Employee || t.FormulaRefId == (int)SpecialFormulaTagReference.IN_ESI_Employer);
                    }
                    else
                    {
                        incentiveRelatedList.Add(incTag);
                    }
                    incentiveRefIds = incentiveRelatedList.Select(t => t.Id).ToList();
                    var empIncentiveListToBeDeleted = await _employeevsSalaryTagRepo.GetAllListAsync(a => a.EmployeeRefId == salaryTag.EmployeeRefId
                    && incentiveRefIds.Contains(a.SalaryRefId));
                    toBedeletedList.AddRange(empIncentiveListToBeDeleted);
                }
                else
                {
                    toBedeletedList.Add(exist);
                }
                foreach (var delLst in toBedeletedList)
                {
                    await _employeevsSalaryTagRepo.DeleteAsync(delLst.Id);
                }
                return true;
            }

        }



        protected async Task<ReturnSuccessOrFailureWithMessage> ExistAllServerLevelBusinessRules(CreateOrUpdateSalaryInfoInput input)
        {
            var emp = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == input.SalaryInfo.EmployeeRefId);
            if (emp == null)
            {
                throw new UserFriendlyException(L("Employee") + " " + input.SalaryInfo.EmployeeRefId + " " + L("NotExists"));
            }

            if (emp.LastWorkingDate.HasValue)
            {
                TimeSpan diff = DateTime.Now.Subtract(emp.LastWorkingDate.Value);
                if (Math.Abs(diff.Days) > 60)
                {
                    throw new UserFriendlyException(L("EmployeeResignedAlready", emp.EmployeeCode + " - " + emp.EmployeeName, emp.LastWorkingDate.Value.ToString("dd-MMM-yyyy")) + " " + "You can edit upto 60 days from Last Working Day Only");
                }
            }

            if (input.SalaryInfo.IncentiveAndOTCalculationPeriodRefId == 0)
            {
                throw new UserFriendlyException(L("OT") + " " + L("Incentive") + " " + L("Period") + " ?");

            }

            var rsOTSlabs = await _otBasedOnSlabRepo.GetAllListAsync(t => t.EmployeeRefId == input.SalaryInfo.EmployeeRefId);
            if (rsOTSlabs != null && rsOTSlabs.Count > 0)
            {
                if (input.SalaryInfo.Ot1PerHour > 0 || input.SalaryInfo.Ot2PerHour > 0)
                {
                    throw new UserFriendlyException("If OTSlab List, Exists then you should change OT1 and OT2 Should Be Zero");
                }
            }

            if (input.SalaryInfo.SalaryMode == (int)SalaryCalculationType.HOURLY)
            {
                if (input.SalaryInfo.HourlySalaryModeRefId >= 1 && input.SalaryInfo.HourlySalaryModeRefId <= 3)
                {
                    //   Do Nothing
                }
                else
                {
                    throw new UserFriendlyException(L("Hourly") + " " + L("Calculation") + " " + L("Mode") + " ?");
                }
            }
            else
            {
                input.SalaryInfo.HourlySalaryModeRefId = null;
            }
            return new ReturnSuccessOrFailureWithMessage { SuccessFlag = true, UserErrorNumber = 1, Message = "Success" };
        }


        public async Task CreateOrUpdateSalaryInfo(CreateOrUpdateSalaryInfoInput input)
        {
            var result = await ExistAllServerLevelBusinessRules(input);
            var incentiveResult = await ExistAllServerLevelBusinessRulesForIncentives(input);

            if (result.UserErrorNumber == 1 && incentiveResult.UserErrorNumber == 1)
            {
                if (input.SalaryInfo.Id.HasValue)
                {
                    await UpdateSalaryInfo(input);
                }
                else
                {
                    await CreateSalaryInfo(input);
                }
            }
        }

        public async Task DeleteSalaryInfo(IdInput input)
        {
            await _salaryinfoRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateSalaryInfo(CreateOrUpdateSalaryInfoInput input)
        {
            var item = await _salaryinfoRepo.GetAsync(input.SalaryInfo.Id.Value);
            var dto = input.SalaryInfo;

            item.EmployeeRefId = dto.EmployeeRefId;
            item.BasicSalary = dto.BasicSalary;
            item.SalaryMode = dto.SalaryMode;
            item.WithEffectFrom = dto.WithEffectFrom;
            item.Levy = dto.Levy;
            item.Ot1PerHour = dto.Ot1PerHour;
            item.Ot2PerHour = dto.Ot2PerHour;
            item.OtAllowedIfWorkTimeOver = dto.OtAllowedIfWorkTimeOver;
            item.ReportOtPerHour = dto.ReportOtPerHour;
            item.NormalOTPerHour = dto.NormalOTPerHour;
            item.MaximumOTAllowedEffectiveFrom = dto.MaximumOTAllowedEffectiveFrom;
            item.MaximumAllowedOTHoursOnPublicHoliday = dto.MaximumAllowedOTHoursOnPublicHoliday;
            item.IncentiveAndOTCalculationPeriodRefId = dto.IncentiveAndOTCalculationPeriodRefId;
            item.IdleStandByCost = dto.IdleStandByCost;
            item.HourlySalaryModeRefId = dto.HourlySalaryModeRefId;

            CheckErrors(await _salaryinfoManager.CreateSync(item));

            #region Incentive Tag Handling
            {
                List<int> alreadyExists = new List<int>();
                if (input.IncentiveList != null && input.IncentiveList.Count > 0)
                {
                    foreach (var incentive in input.IncentiveList)
                    {
                        alreadyExists.Add(incentive.Id);
                        var exist = await _employeeVsIncentiveRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == item.EmployeeRefId && t.IncentiveTagRefId == incentive.Id);
                        if (exist == null)
                        {
                            DateTime temp = DateTime.Today.AddMonths(-1);
                            DateTime effectiveDate = new DateTime(temp.Year, temp.Month, 01);
                            EmployeeVsIncentive newDto = new EmployeeVsIncentive
                            {
                                EmployeeRefId = item.EmployeeRefId,
                                IncentiveTagRefId = incentive.Id,
                                EffectiveFrom = effectiveDate,
                                ReasonRemarks = ""
                            };
                            await _employeeVsIncentiveRepo.InsertOrUpdateAndGetIdAsync(newDto);
                        }
                    }
                }

                var delIMlst = await _employeeVsIncentiveRepo.GetAll().Where(a => a.EmployeeRefId == item.EmployeeRefId
                    && !alreadyExists.Contains(a.IncentiveTagRefId)).ToListAsync();

                foreach (var a in delIMlst)
                {
                    await _employeeVsIncentiveRepo.DeleteAsync(a.Id);
                }
            }
            #endregion

            //bool isOtManualHoursExists = input.ManualOTHoursList.Exists(t => t.MaximumOTAllowedHours > 0);
            //List<int> manualOtHoursIds = new List<int>();

            // if (isOtManualHoursExists == true)

            //#region Manual OT Hours 
            //{
            //    if (input.ManualOTHoursList != null && input.ManualOTHoursList.Count > 0)
            //    {
            //        foreach (var manualOt in input.ManualOTHoursList)
            //        {
            //            var exist = await _manualOTDaywiserestrictionRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == item.EmployeeRefId && t.DayOfWeekRefId == manualOt.DayOfWeekRefId);
            //            if (exist == null)
            //            {
            //                ManualOTDayWiseRestrictions newDto = new ManualOTDayWiseRestrictions
            //                {
            //                    EmployeeRefId = item.EmployeeRefId,
            //                    MaximumOTAllowedHours = manualOt.MaximumOTAllowedHours
            //                };
            //                await _manualOTDaywiserestrictionRepo.InsertOrUpdateAndGetIdAsync(newDto);
            //            }
            //            else
            //            {
            //                if (exist.MaximumOTAllowedHours != manualOt.MaximumOTAllowedHours)
            //                {
            //                    exist.MaximumOTAllowedHours = manualOt.MaximumOTAllowedHours;
            //                    await _manualOTDaywiserestrictionRepo.UpdateAsync(exist);
            //                }
            //            }
            //        }
            //    }
            //}
            //#endregion

            //var existRecords = await _manualOTDaywiserestrictionRepo.GetAllListAsync(t => t.EmployeeRefId == item.EmployeeRefId);
            //if (existRecords.Count(t => t.MaximumOTAllowedHours == 0) == 7)
            //{
            //    await _manualOTDaywiserestrictionRepo.DeleteAsync(t => t.EmployeeRefId == item.EmployeeRefId);
            //}
        }

        protected virtual async Task CreateSalaryInfo(CreateOrUpdateSalaryInfoInput input)
        {
            var dto = input.SalaryInfo.MapTo<SalaryInfo>();

            CheckErrors(await _salaryinfoManager.CreateSync(dto));
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetHourlySalaryModeForCombobox()
        {
            List<ComboboxItemDto> retList = new List<ComboboxItemDto>();

            string enumstring;
            Array EnumValues = System.Enum.GetValues(typeof(HourlySalaryMode));
            foreach (int EnumValue in EnumValues)
            {
                enumstring = Enum.GetName(typeof(HourlySalaryMode), EnumValue);
                retList.Add(new ComboboxItemDto { Value = EnumValue.ToString(), DisplayText = enumstring });
            }
            return
                   new ListResultOutput<ComboboxItemDto>(
                       retList.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetOTTypeTagsForCombobox()
        {
            List<ComboboxItemDto> retList = new List<ComboboxItemDto>();

            string enumstring;
            Array EnumValues = System.Enum.GetValues(typeof(OTTypes));
            foreach (int EnumValue in EnumValues)
            {
                enumstring = Enum.GetName(typeof(OTTypes), EnumValue);
                retList.Add(new ComboboxItemDto { Value = EnumValue.ToString(), DisplayText = enumstring });
            }

            return
                   new ListResultOutput<ComboboxItemDto>(
                       retList.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetSalaryTagsForCombobox()
        {
            List<ComboboxItemDto> retList = new List<ComboboxItemDto>();

            string enumstring;
            Array EnumValues = System.Enum.GetValues(typeof(SalaryTags));
            foreach (int EnumValue in EnumValues)
            {
                enumstring = Enum.GetName(typeof(SalaryTags), EnumValue);
                retList.Add(new ComboboxItemDto { Value = EnumValue.ToString(), DisplayText = enumstring });
            }

            return
                   new ListResultOutput<ComboboxItemDto>(
                       retList.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetSalaryCalculationTypeForCombobox()
        {
            List<ComboboxItemDto> retList = new List<ComboboxItemDto>();

            string enumstring;
            Array EnumValues = System.Enum.GetValues(typeof(SalaryCalculationType));
            foreach (int EnumValue in EnumValues)
            {
                enumstring = Enum.GetName(typeof(SalaryCalculationType), EnumValue);
                retList.Add(new ComboboxItemDto { Value = EnumValue.ToString(), DisplayText = enumstring });
            }

            return
                   new ListResultOutput<ComboboxItemDto>(
                       retList.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetOTAndIncentivePeriodTagsForCombobox()
        {
            List<ComboboxItemDto> retList = new List<ComboboxItemDto>();

            string enumstring;
            Array EnumValues = System.Enum.GetValues(typeof(IncentiveOTCalculationPeriod));
            foreach (int EnumValue in EnumValues)
            {
                enumstring = Enum.GetName(typeof(IncentiveOTCalculationPeriod), EnumValue);
                retList.Add(new ComboboxItemDto { Value = EnumValue.ToString(), DisplayText = enumstring });
            }

            return
                   new ListResultOutput<ComboboxItemDto>(
                       retList.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText)).ToList());
        }

        //public async Task<List<ComboboxItemDto>> GetStatutoryIncentiveList()
        //{

        //}

        public async Task<ListResultOutput<ComboboxItemDto>> GetSpecialFormulaTagsForCombobox()
        {
            List<ComboboxItemDto> retList = new List<ComboboxItemDto>();
            var countryName = FeatureChecker.GetValue(AppFeatures.ConnectCountry);
            var searchWithCountStartName = countryName + "_";
            string enumstring;
            Array EnumValues = System.Enum.GetValues(typeof(SpecialFormulaTagReference));
            foreach (int EnumValue in EnumValues)
            {
                enumstring = Enum.GetName(typeof(SpecialFormulaTagReference), EnumValue);
                if (enumstring.StartsWith(searchWithCountStartName))
                {
                    retList.Add(new ComboboxItemDto { Value = EnumValue.ToString(), DisplayText = enumstring });
                }
            }

            return
                   new ListResultOutput<ComboboxItemDto>(
                       retList.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText)).ToList());
        }

        public async Task<bool> CheckBusinessRuleForIncentiveTag(CreateOrUpdateSalaryInfoInput input)
        {
            var incentiveResult = await ExistAllServerLevelBusinessRulesForIncentives(input);
            if (incentiveResult.SuccessFlag == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }




    }
}
