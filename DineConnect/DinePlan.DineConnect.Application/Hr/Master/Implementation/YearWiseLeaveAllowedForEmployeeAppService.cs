﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.Hr.Impl;
using System;
using DinePlan.DineConnect.Hr.Inventory;
using DinePlan.DineConnect.Hr.Inventory.Dtos;
using DinePlan.DineConnect.Hr;
using Abp.Domain.Uow;
using DinePlan.DineConnect.House.Master.Dtos;
using System.Reflection;

namespace DinePlan.DineConnect.Hr.Transaction.Implementation
{
    public class YearWiseLeaveAllowedForEmployeeAppService : DineConnectAppServiceBase, IYearWiseLeaveAllowedForEmployeeAppService
    {
        private readonly IYearWiseLeaveAllowedForEmployeeListExcelExporter _yearwiseleaveallowedforemployeeExporter;
        private readonly IYearWiseLeaveAllowedForEmployeeManager _yearwiseleaveallowedforemployeeManager;
        private readonly IRepository<YearWiseLeaveAllowedForEmployee> _yearwiseleaveallowedforemployeeRepo;
        private readonly IRepository<PersonalInformation> _personalinformationRepo;
        private readonly IRepository<LeaveType> _leavetypeRepo;
        private readonly IRepository<LeaveRequest> _leaveRequestRepo;


        public YearWiseLeaveAllowedForEmployeeAppService(IYearWiseLeaveAllowedForEmployeeManager yearwiseleaveallowedforemployeeManager,
           IRepository<YearWiseLeaveAllowedForEmployee> yearWiseLeaveAllowedForEmployeeRepo,
           IYearWiseLeaveAllowedForEmployeeListExcelExporter yearwiseleaveallowedforemployeeExporter,
           IRepository<PersonalInformation> personalinformationRepo,
           IRepository<LeaveType> leavetypeRepo,
           IRepository<LeaveRequest> leaveRequestRepo)
        {
            _yearwiseleaveallowedforemployeeManager = yearwiseleaveallowedforemployeeManager;
            _yearwiseleaveallowedforemployeeRepo = yearWiseLeaveAllowedForEmployeeRepo;
            _yearwiseleaveallowedforemployeeExporter = yearwiseleaveallowedforemployeeExporter;
            _personalinformationRepo = personalinformationRepo;
            _leavetypeRepo = leavetypeRepo;
            _leaveRequestRepo = leaveRequestRepo;
        }

        public async Task<PagedResultOutput<YearWiseLeaveAllowedForEmployeeListDto>> GetAll(GetYearWiseLeaveAllowedForEmployeeInput input)
        {

            var allItems = (from ywlafe in _yearwiseleaveallowedforemployeeRepo.GetAll()
                            join pi in _personalinformationRepo.GetAll() on
                            ywlafe.EmployeeRefId equals pi.Id
                            join lt in _leavetypeRepo.GetAll() on
                            ywlafe.LeaveTypeRefCode equals lt.Id
                            select new YearWiseLeaveAllowedForEmployeeListDto()
                            {
                                Id = ywlafe.Id,
                                EmployeeRefName = pi.EmployeeName,
                                LeaveTypeRefName = lt.LeaveTypeName,
                                CreationTime = ywlafe.CreationTime
                            }).WhereIf(
                                 !input.Filter.IsNullOrEmpty(),
                                 p => p.EmployeeRefName.Contains(input.Filter)

               );


            var sortMenuItems = await allItems
                  .OrderBy(input.Sorting)
                  .PageBy(input)
                  .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<YearWiseLeaveAllowedForEmployeeListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<YearWiseLeaveAllowedForEmployeeListDto>(
                  allItemCount,
                  allListDtos
                  );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _yearwiseleaveallowedforemployeeRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<YearWiseLeaveAllowedForEmployeeListDto>>();
            return _yearwiseleaveallowedforemployeeExporter.ExportToFile(allListDtos);
        }

        public async Task<GetYearWiseLeaveAllowedForEmployeeForEditOutput> GetYearWiseLeaveAllowedForEmployeeForEdit(YearWithEmployeeDto input)
        {
            List<YearWiseLeaveAllowedForEmployeeEditDto> editDetailsDto;
            var leaveMaster = _leavetypeRepo.GetAll();
            var per = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == input.EmployeeRefId);
            if (per.Gender.ToUpper().Equals("MALE"))
            {
                leaveMaster = leaveMaster.Where(t => t.MaleGenderAllowed == true);
            }
            else
            {
                leaveMaster = leaveMaster.Where(t => t.FemaleGenderAllowed == true);
            }

            //if (per.MaritalStatus==true)

            var hDto = await (from mas in _yearwiseleaveallowedforemployeeRepo.GetAll().Where(t => t.EmployeeRefId == input.EmployeeRefId
                             && t.AcYear == input.AcYear)
                              join leaveType in _leavetypeRepo.GetAll() on mas.LeaveTypeRefCode equals leaveType.Id
                              select new YearWiseLeaveAllowedForEmployeeEditDto
                              {
                                  AcYear = input.AcYear,
                                  EmployeeRefId = input.EmployeeRefId,
                                  EmployeeRefName = per.EmployeeName,
                                  LeaveTypeRefCode = leaveType.Id,
                                  LeaveTypeRefName = leaveType.LeaveTypeName,
                                  MaximumLeaveAllowed = leaveType.NumberOfLeaveAllowed,
                                  NumberOfLeaveAllowed = mas.NumberOfLeaveAllowed,
                                  AllowedForMarriedEmployeesOnly = leaveType.AllowedForMarriedEmployeesOnly
                              }).ToListAsync();

            editDetailsDto = hDto.MapTo<List<YearWiseLeaveAllowedForEmployeeEditDto>>();
            if (editDetailsDto.Count == leaveMaster.Count())
            {
                //Do nothing
            }
            else
            {
                var arrExistLeaveIds = editDetailsDto.Select(t => t.LeaveTypeRefCode).ToArray();

                var newEditDtos = await (from leave in leaveMaster.Where(t => !arrExistLeaveIds.Contains(t.Id))
                                         select new YearWiseLeaveAllowedForEmployeeEditDto
                                         {
                                             AcYear = input.AcYear,
                                             EmployeeRefId = input.EmployeeRefId,
                                             EmployeeRefName = per.EmployeeName,
                                             LeaveTypeRefCode = leave.Id,
                                             LeaveTypeRefName = leave.LeaveTypeName,
                                             MaximumLeaveAllowed = leave.NumberOfLeaveAllowed,
                                             NumberOfLeaveAllowed = leave.DefaultNumberOfLeaveAllowed,
                                             AllowedForMarriedEmployeesOnly = leave.AllowedForMarriedEmployeesOnly
                                         }).ToListAsync();
                editDetailsDto.AddRange(newEditDtos);
            }

            if (editDetailsDto.Count > 0)
            {
                var tempLst = new List<YearWiseLeaveAllowedForEmployeeEditDto>();
                foreach (var lst in editDetailsDto)
                {
                    if (lst.AllowedForMarriedEmployeesOnly == true)
                    {
                        if (per.MaritalStatus == L("Single") || per.MaritalStatus.IsNullOrEmpty())
                            continue;
                    }
                    tempLst.Add(lst);
                }
                editDetailsDto = tempLst;
            }

            DateTime fromDate = new DateTime(input.AcYear, 01, 01);
            DateTime toDate = new DateTime(input.AcYear, 12, 31);
            var rejectedString = L("Rejected");
            var rsalreadyAvailedLeave = await _leaveRequestRepo.GetAllListAsync(t => t.LeaveFrom >= fromDate && t.LeaveTo <= toDate
                && t.EmployeeRefId == input.EmployeeRefId && t.LeaveStatus != rejectedString);

            foreach (var lst in editDetailsDto)
            {
                var leaveTypeAvailed = rsalreadyAvailedLeave.Where(t => t.LeaveTypeRefCode == lst.LeaveTypeRefCode).Sum(t => t.TotalNumberOfDays);
                lst.NumberOfLeaveTaken = leaveTypeAvailed;
            }

            return new GetYearWiseLeaveAllowedForEmployeeForEditOutput
            {
                YearWiseLeaveAllowedForEmployee = editDetailsDto
            };
        }

        public async Task CreateOrUpdateYearWiseLeaveAllowedForEmployee(CreateOrUpdateYearWiseLeaveAllowedForEmployeeInput input)
        {
            if (input.AcYear == 0 || input.AcYear <= (DateTime.Today.Year - 1))
            {
                throw new UserFriendlyException(L("AcYear") + input.AcYear + " " + L("Wrong"));
            }

            if (input.EmployeeRefId == 0)
            {
                throw new UserFriendlyException(L("Employee") + " " + L("Id") + input.EmployeeRefId + " " + L("Wrong"));
            }

            var emp = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == input.EmployeeRefId);
            if (emp == null)
            {
                throw new UserFriendlyException(L("Employee") + " " + L("Id") + input.EmployeeRefId + " " + L("Wrong"));
            }

            if (emp.LastWorkingDate.HasValue)
            {
                throw new UserFriendlyException(L("Employee") + " " + L("Resigned") + emp.EmployeeName + " " + L("Already"));
            }

            await UpdateYearWiseLeaveAllowedForEmployee(input);
        }

        public async Task DeleteYearWiseLeaveAllowedForEmployee(IdInput input)
        {
            await _yearwiseleaveallowedforemployeeRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateYearWiseLeaveAllowedForEmployee(CreateOrUpdateYearWiseLeaveAllowedForEmployeeInput input)
        {
            var lstMaster = await _leavetypeRepo.GetAllListAsync();
            List<int> leaveIdsTobeRetained = new List<int>();
            foreach (var dto in input.YearWiseLeaveAllowedForEmployee)
            {
                var leaveCons = lstMaster.FirstOrDefault(t => t.Id == dto.LeaveTypeRefCode);
                if (leaveCons.NumberOfLeaveAllowed < dto.NumberOfLeaveAllowed)
                {
                    throw new UserFriendlyException(L("LeaveDaysError", leaveCons.NumberOfLeaveAllowed, dto.NumberOfLeaveAllowed, leaveCons.LeaveTypeName));
                }
                dto.LeaveAcYearFrom = new DateTime(dto.AcYear, 01, 01);
                dto.LeaveAcYearTo = new DateTime(dto.AcYear, 12, 31);
                YearWiseLeaveAllowedForEmployee item;
                item = await _yearwiseleaveallowedforemployeeRepo.FirstOrDefaultAsync(
                   t => t.EmployeeRefId == dto.EmployeeRefId && t.AcYear == dto.AcYear && t.LeaveTypeRefCode == dto.LeaveTypeRefCode);

                if (item == null)
                {
                    item = new YearWiseLeaveAllowedForEmployee
                    {
                        AcYear = dto.AcYear,
                        LeaveAcYearFrom = dto.LeaveAcYearFrom,
                        LeaveAcYearTo = dto.LeaveAcYearTo,
                        EmployeeRefId = dto.EmployeeRefId,
                        LeaveTypeRefCode = dto.LeaveTypeRefCode,
                        NumberOfLeaveAllowed = dto.NumberOfLeaveAllowed
                    };
                    await _yearwiseleaveallowedforemployeeRepo.InsertOrUpdateAndGetIdAsync(item);
                    leaveIdsTobeRetained.Add(item.Id);
                }
                else
                {
                    if (item.NumberOfLeaveAllowed != dto.NumberOfLeaveAllowed || item.LeaveTypeRefCode != dto.LeaveTypeRefCode)
                    {
                        item.AcYear = dto.AcYear;
                        item.LeaveAcYearFrom = dto.LeaveAcYearFrom;
                        item.LeaveAcYearTo = dto.LeaveAcYearTo;
                        item.EmployeeRefId = dto.EmployeeRefId;
                        item.LeaveTypeRefCode = dto.LeaveTypeRefCode;
                        item.NumberOfLeaveAllowed = dto.NumberOfLeaveAllowed;
                        await _yearwiseleaveallowedforemployeeRepo.InsertOrUpdateAndGetIdAsync(item);

                    }
                    leaveIdsTobeRetained.Add(item.Id);
                }
            }

            var delList = await _yearwiseleaveallowedforemployeeRepo.GetAll()
                        .Where(t => t.AcYear == input.AcYear && t.EmployeeRefId == input.EmployeeRefId &&
                                !leaveIdsTobeRetained.Contains(t.Id)).ToListAsync();

            foreach (var a in delList)
            {
                await _yearwiseleaveallowedforemployeeRepo.DeleteAsync(a.Id);
            }


        }

        public async Task<List<YearWiseLeaveAllowedForEmployeeEditDto>> GetEmployeeWiseAcYearWiseLeave(YearWithEmployeeDto input)
        {
            if (input.AcYear == 0 || input.AcYear < 2017)
            {
                return null;
            }

            var leaveList = await (from mas in _yearwiseleaveallowedforemployeeRepo.GetAll()
                              .Where(t => t.EmployeeRefId == input.EmployeeRefId && t.AcYear == input.AcYear)
                                   join leave in _leavetypeRepo.GetAll() on mas.LeaveTypeRefCode equals leave.Id
                                   select new YearWiseLeaveAllowedForEmployeeEditDto
                                   {
                                       AcYear = input.AcYear,
                                       EmployeeRefId = input.EmployeeRefId,
                                       LeaveTypeRefCode = leave.Id,
                                       LeaveTypeRefName = leave.LeaveTypeName,
                                       NumberOfLeaveAllowed = mas.NumberOfLeaveAllowed
                                   }).ToListAsync();

            #region If Yearly Leave Not Have Records 
            if (leaveList.Count == 0)
            {
                var defaultList = await GetYearWiseLeaveAllowedForEmployeeForEdit(new YearWithEmployeeDto
                {
                    EmployeeRefId = input.EmployeeRefId,
                    AcYear = input.AcYear
                });
                CreateOrUpdateYearWiseLeaveAllowedForEmployeeInput newDto = new CreateOrUpdateYearWiseLeaveAllowedForEmployeeInput();
                newDto.YearWiseLeaveAllowedForEmployee = defaultList.YearWiseLeaveAllowedForEmployee;
                newDto.AcYear = input.AcYear;
                newDto.EmployeeRefId = input.EmployeeRefId;
                await CreateOrUpdateYearWiseLeaveAllowedForEmployee(newDto);
                leaveList = await (from mas in _yearwiseleaveallowedforemployeeRepo.GetAll()
                               .Where(t => t.EmployeeRefId == input.EmployeeRefId && t.AcYear == input.AcYear)
                                   join leave in _leavetypeRepo.GetAll() on mas.LeaveTypeRefCode equals leave.Id
                                   select new YearWiseLeaveAllowedForEmployeeEditDto
                                   {
                                       AcYear = input.AcYear,
                                       EmployeeRefId = input.EmployeeRefId,
                                       LeaveTypeRefCode = leave.Id,
                                       LeaveTypeRefName = leave.LeaveTypeName,
                                       NumberOfLeaveAllowed = mas.NumberOfLeaveAllowed
                                   }).ToListAsync();

                if (leaveList.Count == 0)
                {
                    throw new UserFriendlyException(L("AllowedLeaveForEmployeeNotFixedByHR"));
                }
            }
            #endregion

            DateTime fromDate = new DateTime(input.AcYear, 01, 01);
            DateTime toDate = new DateTime(input.AcYear, 12, 31);
            var rejectedString = L("Rejected");
            var rsalreadyAvailedLeave = await _leaveRequestRepo.GetAllListAsync(t => t.LeaveFrom >= fromDate && t.LeaveTo <= toDate
                && t.EmployeeRefId == input.EmployeeRefId && t.LeaveStatus != rejectedString);

            foreach (var lst in leaveList)
            {
                var leaveTypeAvailed = rsalreadyAvailedLeave.Where(t => t.LeaveTypeRefCode == lst.LeaveTypeRefCode).Sum(t => t.TotalNumberOfDays);
                lst.NumberOfLeaveTaken = leaveTypeAvailed;
            }
            leaveList = leaveList.Where(t => t.NumberOfLeaveAllowed > 0).ToList();
            return leaveList;
        }

        [UnitOfWork]
        public async Task<MessageOutput> UpdateLeaveTypeForGivenAcYearForAll(YearWithEmployeeDto input)
        {
            if (input.AcYear == 0 || input.AcYear <= (DateTime.Today.Year - 1))
            {
                throw new UserFriendlyException(L("AcYear") + input.AcYear + " " + L("Wrong"));
            }

            MessageOutput output = new MessageOutput();
            output.SuccessFlag = false;
            var employeeList = await _personalinformationRepo.GetAllListAsync(t => t.ActiveStatus == true && !t.LastWorkingDate.HasValue);
            foreach (var emp in employeeList)
            {
                try
                {
                    YearWithEmployeeDto inputForEdit = new YearWithEmployeeDto
                    {
                        AcYear = input.AcYear,
                        EmployeeRefId = emp.Id
                    };

                    var existList = await GetYearWiseLeaveAllowedForEmployeeForEdit(inputForEdit);

                    CreateOrUpdateYearWiseLeaveAllowedForEmployeeInput outputDto = new CreateOrUpdateYearWiseLeaveAllowedForEmployeeInput();
                    outputDto.EmployeeRefId = emp.Id;
                    outputDto.AcYear = input.AcYear;
                    outputDto.YearWiseLeaveAllowedForEmployee = existList.YearWiseLeaveAllowedForEmployee;

                    await CreateOrUpdateYearWiseLeaveAllowedForEmployee(outputDto);
                }
                catch (Exception ex)
                {
                    MethodBase m = MethodBase.GetCurrentMethod();
                    string innerExceptionMessage = ex.InnerException != null ? ex.InnerException.Message : "";
                    throw new UserFriendlyException(" Method : " + m.ReflectedType.Name + " : " + m.Name + " " + ex.Message + " InnerException : " + innerExceptionMessage + " " + emp.EmployeeName);
                }
            }

            output.SuccessMessage = employeeList.Count + " " + L("Records") + " " + L("Updated");
            output.SuccessFlag = true;
            return output;
        }

    }
}
