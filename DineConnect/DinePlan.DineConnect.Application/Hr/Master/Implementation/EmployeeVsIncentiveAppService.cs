﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.Hr.Impl;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Hr.Master.Implementation
{
	public class EmployeeVsIncentiveAppService : DineConnectAppServiceBase, IEmployeeVsIncentiveAppService
	{

		private readonly IEmployeeVsIncentiveListExcelExporter _employeevsincentiveExporter;
		private readonly IEmployeeVsIncentiveManager _employeevsincentiveManager;
		private readonly IRepository<EmployeeVsIncentive> _employeevsincentiveRepo;
		private readonly IRepository<PersonalInformation> _personalinformationRepo;
		private readonly IRepository<IncentiveTag> _incentivetagRepo;

		public EmployeeVsIncentiveAppService(IEmployeeVsIncentiveManager employeevsincentiveManager,
			IRepository<EmployeeVsIncentive> employeeVsIncentiveRepo,
			IEmployeeVsIncentiveListExcelExporter employeevsincentiveExporter,
			IRepository<PersonalInformation> personalinformationRepo,
			IRepository<IncentiveTag> incentivetagRepo
			)
		{
			_employeevsincentiveManager = employeevsincentiveManager;
			_employeevsincentiveRepo = employeeVsIncentiveRepo;
			_employeevsincentiveExporter = employeevsincentiveExporter;
			_personalinformationRepo = personalinformationRepo;
			_incentivetagRepo = incentivetagRepo;
		}

		public async Task<PagedResultOutput<EmployeeVsIncentiveListDto>> GetAll(GetEmployeeVsIncentiveInput input)
		{
			var allItems = (from empvsince in _employeevsincentiveRepo.GetAll()
							join per in _personalinformationRepo.GetAll() on empvsince.EmployeeRefId equals per.Id
							join ince in _incentivetagRepo.GetAll() on empvsince.IncentiveTagRefId equals ince.Id
							select new EmployeeVsIncentiveListDto
							{
								EmployeeRefId = empvsince.EmployeeRefId,
								EmployeeRefCode = per.EmployeeCode,
								EmployeeRefName = per.EmployeeName,
								IncentiveTagRefId = empvsince.IncentiveTagRefId,
								IncentiveTagRefCode = ince.IncentiveTagCode,
								EffectiveFrom = empvsince.EffectiveFrom,
								RollBackFrom = empvsince.RollBackFrom
							});


			var sortMenuItems = await allItems
				.OrderBy(input.Sorting)
				.PageBy(input)
				.ToListAsync();

			var allListDtos = sortMenuItems.MapTo<List<EmployeeVsIncentiveListDto>>();

			var allItemCount = await allItems.CountAsync();

			return new PagedResultOutput<EmployeeVsIncentiveListDto>(
				allItemCount,
				allListDtos
				);
		}

		public async Task<FileDto> GetAllToExcel()
		{
			var allList = await _employeevsincentiveRepo.GetAll().ToListAsync();
			var allListDtos = allList.MapTo<List<EmployeeVsIncentiveListDto>>();
			return _employeevsincentiveExporter.ExportToFile(allListDtos);
		}

		public async Task<GetEmployeeVsIncentiveForEditOutput> GetEmployeeVsIncentiveForEdit(NullableIdInput input)
		{
			EmployeeVsIncentiveEditDto editDto;

			if (input.Id.HasValue)
			{
				var hDto = await _employeevsincentiveRepo.GetAsync(input.Id.Value);
				editDto = hDto.MapTo<EmployeeVsIncentiveEditDto>();
			}
			else
			{
				editDto = new EmployeeVsIncentiveEditDto();
			}

			return new GetEmployeeVsIncentiveForEditOutput
			{
				EmployeeVsIncentive = editDto
			};
		}

		public async Task CreateOrUpdateEmployeeVsIncentive(CreateOrUpdateEmployeeVsIncentiveInput input)
		{
			if (input.EmployeeVsIncentive.Id.HasValue)
			{
				await UpdateEmployeeVsIncentive(input);
			}
			else
			{
				await CreateEmployeeVsIncentive(input);
			}
		}

		public async Task DeleteEmployeeVsIncentive(IdInput input)
		{
			await _employeevsincentiveRepo.DeleteAsync(input.Id);
		}

		protected virtual async Task UpdateEmployeeVsIncentive(CreateOrUpdateEmployeeVsIncentiveInput input)
		{
			var item = await _employeevsincentiveRepo.GetAsync(input.EmployeeVsIncentive.Id.Value);
			var dto = input.EmployeeVsIncentive;

			item.EmployeeRefId = dto.EmployeeRefId;
			item.IncentiveTagRefId = dto.IncentiveTagRefId;
			item.EffectiveFrom = dto.EffectiveFrom;
			item.RollBackFrom = dto.RollBackFrom;

			CheckErrors(await _employeevsincentiveManager.CreateSync(item));
		}

		protected virtual async Task CreateEmployeeVsIncentive(CreateOrUpdateEmployeeVsIncentiveInput input)
		{
			var dto = input.EmployeeVsIncentive.MapTo<EmployeeVsIncentive>();

			CheckErrors(await _employeevsincentiveManager.CreateSync(dto));
		}

		public async Task<ListResultOutput<EmployeeVsIncentiveListDto>> Gets()
		{
			var lstEmployeeVsIncentive = await _employeevsincentiveRepo.GetAll().ToListAsync();
			return new ListResultOutput<EmployeeVsIncentiveListDto>(lstEmployeeVsIncentive.MapTo<List<EmployeeVsIncentiveListDto>>());
		}
	}
}
