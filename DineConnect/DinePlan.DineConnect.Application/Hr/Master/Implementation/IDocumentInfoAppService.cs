﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Hr.Master
{
    public interface IDocumentInfoAppService : IApplicationService
    {
        Task<PagedResultOutput<DocumentInfoListDto>> GetAll(GetDocumentInfoInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetDocumentInfoForEditOutput> GetDocumentInfoForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateDocumentInfo(CreateOrUpdateDocumentInfoInput input);
        Task DeleteDocumentInfo(IdInput input);

        Task<ListResultOutput<DocumentInfoListDto>> GetDocumentNames();
        Task<ListResultOutput<ComboboxItemDto>> GetDocumentTypeForCombobox();
    }
}
