﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.Hr.Impl;
using System;
using System.Reflection;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Features;
using Abp.Application.Features;
using DinePlan.DineConnect.Connect.Master;

namespace DinePlan.DineConnect.Hr.Transaction.Implementation
{
    public class IncentiveTagAppService : DineConnectAppServiceBase, IIncentiveTagAppService
    {

        private readonly IIncentiveTagListExcelExporter _incentivetagExporter;
        private readonly IIncentiveTagManager _incentivetagManager;
        private readonly IRepository<IncentiveTag> _incentivetagRepo;
        private readonly IRepository<IncentiveVsEntity> _incentivevsentityRepo;
        private readonly IRepository<IncentiveCategory> _incentivecategoryRepo;
        private readonly IRepository<EmployeeVsIncentive> _employeeVsIncentiveRepo;
        private readonly IRepository<ManualIncentive> _manualIncentiveRepo;
        private readonly IRepository<PersonalInformation> _personalInformationRepo;
        private readonly IRepository<Company> _companyRepo;
        private readonly IRepository<SkillSet> _skillSetRepo;
        private readonly IRepository<EmployeeSkillSet> _employeeSkillSetRepo;
        private readonly IRepository<EmployeeVsSalaryTag> _employeeVsSalaryTagRepo;
        private readonly IRepository<EmployeeResidentStatus> _employeeResidentStatusRepo;
        private readonly IRepository<EmployeeRace> _employeeRaceStatusRepo;
        private readonly IRepository<EmployeeReligion> _employeeReligionRepo;
        private readonly IRepository<Location> _locationRepo;

        public IncentiveTagAppService(
            IIncentiveTagManager incentivetagManager,
            IRepository<IncentiveTag> IncentiveTagRepo,
            IIncentiveTagListExcelExporter incentivetagExporter,
            IRepository<IncentiveCategory> incentivecategoryRepo,
            IRepository<IncentiveVsEntity> incentivevsentityRepo,
            IRepository<EmployeeVsIncentive> employeeVsIncentiveRepo,
            IRepository<ManualIncentive> manualIncentiveRepo,
            IRepository<PersonalInformation> personalInformationRepo,
            IRepository<Company> companyRepo,
            IRepository<SkillSet> skillSetRepo,
            IRepository<EmployeeSkillSet> employeeSkillSetRepo,
            IRepository<EmployeeVsSalaryTag> employeeVsSalaryTagRepo,
            IRepository<EmployeeResidentStatus> employeeResidentStatusRepo,
            IRepository<EmployeeRace> employeeRaceStatusRepo,
            IRepository<EmployeeReligion> employeeReligionRepo,
            IRepository<Location> locationRepo)
        {
            _incentivetagManager = incentivetagManager;
            _incentivetagRepo = IncentiveTagRepo;
            _incentivetagExporter = incentivetagExporter;
            _incentivecategoryRepo = incentivecategoryRepo;
            _incentivevsentityRepo = incentivevsentityRepo;
            _employeeVsIncentiveRepo = employeeVsIncentiveRepo;
            _manualIncentiveRepo = manualIncentiveRepo;
            _personalInformationRepo = personalInformationRepo;
            _companyRepo = companyRepo;
            _skillSetRepo = skillSetRepo;
            _employeeSkillSetRepo = employeeSkillSetRepo;
            _employeeVsSalaryTagRepo = employeeVsSalaryTagRepo;
            _employeeResidentStatusRepo = employeeResidentStatusRepo;
            _employeeRaceStatusRepo = employeeRaceStatusRepo;
            _employeeReligionRepo = employeeReligionRepo;
            _locationRepo = locationRepo;
        }

        public async Task<PagedResultOutput<IncentiveTagListDto>> GetAll(GetIncentiveTagInput input)
        {
            var allItems = _incentivetagRepo.GetAll();

            allItems = _incentivetagRepo
            .GetAll()
            .WhereIf(
                !input.Filter.IsNullOrEmpty(),
                p => p.IncentiveTagCode.Contains(input.Filter)
            );

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<IncentiveTagListDto>>();

            var allItemCount = await allItems.CountAsync();

            foreach (var lst in allListDtos)
            {
                lst.IncentivePayModeRefName = lst.IncentivePayModeId == (int)IncentivePayMode.Daily ? L("Daily") : lst.IncentivePayModeId == (int)IncentivePayMode.Monthly ? "Monthly" : "";

                lst.AllowanceOrDeductionRefName = lst.AllowanceOrDeductionRefId == (int)AllowanceOrDeductionMode.Allowance ? L("Allowance") : lst.AllowanceOrDeductionRefId == (int)AllowanceOrDeductionMode.Deduction ? "Deduction" : lst.AllowanceOrDeductionRefId == (int)AllowanceOrDeductionMode.Statutory_Allowance ? "Statutory Allowance" : lst.AllowanceOrDeductionRefId == (int)AllowanceOrDeductionMode.Statutory_Deduction ? "Statutory Deduction" : "";
            }

            return new PagedResultOutput<IncentiveTagListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _incentivetagRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<IncentiveTagListDto>>();
            return _incentivetagExporter.ExportToFile(allListDtos);
        }

        public async Task<GetIncentiveTagForEditOutput> GetIncentiveTagForEdit(NullableIdInput input)
        {
            IncentiveTagEditDto editDto;
            List<IncentiveVsEntityListDto> editDetails;
            List<IncentivePersonalInformationListDto> employeeList = new List<IncentivePersonalInformationListDto>();

            if (input.Id.HasValue)
            {
                var hDto = await _incentivetagRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<IncentiveTagEditDto>();

                var a = await _incentivevsentityRepo.GetAllListAsync(t => t.IncentiveTagRefId == editDto.Id);
                if (a.Count > 0)
                    editDetails = a.MapTo<List<IncentiveVsEntityListDto>>();
                else
                    editDetails = new List<IncentiveVsEntityListDto>();

                List<int> employeeRefIds = new List<int>();
                bool canBeDeleted = false;
                string relatedTag = "";
                var empVsIncentives = await _employeeVsIncentiveRepo.GetAllListAsync(t => t.IncentiveTagRefId == input.Id.Value);
                if (empVsIncentives.Count > 0)
                {
                    employeeRefIds = empVsIncentives.Select(t => t.EmployeeRefId).ToList();
                    canBeDeleted = true;
                    relatedTag = "Incentive";
                }
                else
                {
                    var empVsSalaryTags = await _employeeVsSalaryTagRepo.GetAllListAsync(t => t.SalaryRefId == input.Id.Value);
                    if (empVsSalaryTags.Count > 0)
                    {
                        canBeDeleted = false;
                        relatedTag = "Salary";
                        employeeRefIds.AddRange(empVsSalaryTags.Select(t => t.EmployeeRefId).ToList());
                    }
                }

                var empList = await _personalInformationRepo.GetAllListAsync(t => t.ActiveStatus == true && employeeRefIds.Contains(t.Id));
                employeeList = empList.MapTo<List<IncentivePersonalInformationListDto>>();
                var rsCompany = await _companyRepo.GetAllListAsync();
                var rsEmployeeSkillSet = await _employeeSkillSetRepo.GetAllListAsync(t => employeeRefIds.Contains(t.EmployeeRefId));
                var rsSkillSet = await _skillSetRepo.GetAllListAsync();
                var rsLocation = await _locationRepo.GetAllListAsync();

                foreach (var lst in employeeList)
                {
                    lst.IncentiveCanBeDeleted = canBeDeleted;
                    lst.RelatedWithSalaryOrIncentive = relatedTag;
                    var loc = rsLocation.FirstOrDefault(t => t.Id == lst.LocationRefId);
                    lst.CompanyRefCode = rsCompany.FirstOrDefault(t => t.Id == loc.CompanyRefId).Code;
                    var empSkillSet = rsEmployeeSkillSet.Where(t => t.EmployeeRefId == lst.Id);
                    foreach (var skill in empSkillSet)
                    {
                        var sk = rsSkillSet.FirstOrDefault(t => t.Id == skill.SkillSetRefId);
                        lst.SkillSetList = lst.SkillSetList + sk.SkillCode + ", ";
                    }
                    if (lst.SkillSetList.Length > 0)
                        lst.SkillSetList = lst.SkillSetList.Left(lst.SkillSetList.Length - 2);
                }
            }
            else
            {
                editDto = new IncentiveTagEditDto();
                editDetails = new List<IncentiveVsEntityListDto>();
            }

            return new GetIncentiveTagForEditOutput
            {
                IncentiveTag = editDto,
                IncentiveVsEntityList = editDetails,
                EmployeeList = employeeList
            };
        }

        public async Task<ReturnSuccessOrFailureWithMessage> ExistAllServerLevelBusinessRules(CreateOrUpdateIncentiveTagInput input)
        {
            if (input.IncentiveTag.IncentiveAmount > 0 && input.IncentiveTag.IncenitveBasedOnSalaryTags == true)
            {
                throw new UserFriendlyException(L("IncenitveBasedOnSalaryTags") + " " + L("Incentive") + L("Amount"));
            }

            if (input.IncentiveTag.IncentiveAmount == 0)
            {
                input.IncentiveTag.MinRange = 0;
                input.IncentiveTag.MaxRange = 0;
            }

            if (input.IncentiveTag.IncentiveAmount < input.IncentiveTag.MinRange)
                throw new UserFriendlyException("Minimum Range Not Valid");
            if (input.IncentiveTag.IncentiveAmount > input.IncentiveTag.MaxRange)
                throw new UserFriendlyException("Minimum Range Not Valid");

            if (input.IncentiveTag.IncenitveBasedOnSalaryTags == true && input.IncentiveTag.ProportianteValueOfSalaryTags == 0)
            {
                throw new UserFriendlyException(L("ProportianteValueOfSalaryTagsZeroError"));
            }

            if (input.IncentiveTag.IncenitveBasedOnSalaryTags == true && input.IncentiveTag.ProportianteValueOfSalaryTags == null)
            {
                throw new UserFriendlyException(L("ProportianteValueOfSalaryTagsZeroError"));
            }

            if (input.IncentiveTag.IncentiveEntryMode == (int)IncentiveEntryMode.AUTOMATIC)
            {
                input.IncentiveTag.MinRange = input.IncentiveTag.IncentiveAmount;
                input.IncentiveTag.MaxRange = input.IncentiveTag.IncentiveAmount;
            }
            else
            {
                if (input.IncentiveTag.MinRange == 0)
                {
                    input.IncentiveTag.MinRange = input.IncentiveTag.IncentiveAmount;
                }
                if (input.IncentiveTag.MaxRange == 0)
                {
                    input.IncentiveTag.MaxRange = input.IncentiveTag.IncentiveAmount;
                }
            }

            if (input.IncentiveTag.MinRange > input.IncentiveTag.MaxRange)
            {
                throw new UserFriendlyException(L("CheckIncentiveAmountRange"));
            }

            if (input.IncentiveTag.IsCertificateMandatory)
            {
                var skill = input.IncentiveVsEntityList.FirstOrDefault(t => t.EntityType == (int)(int)IncentiveEntityType.SKILLSET);
                if (skill == null)
                {
                    throw new UserFriendlyException(L("IncentiveCertificateMandatoryError"));
                }
            }


            return new ReturnSuccessOrFailureWithMessage { UserErrorNumber = 0, Message = L("Success") };
        }

        public async Task CreateOrUpdateIncentiveTag(CreateOrUpdateIncentiveTagInput input)
        {
            var returnRules = await ExistAllServerLevelBusinessRules(input);
            if (returnRules.UserErrorNumber == 0)
            {
                if (input.IncentiveTag.Id.HasValue)
                {
                    await UpdateIncentiveTag(input);
                }
                else
                {
                    await CreateIncentiveTag(input);
                }
            }
        }


        public async Task DeleteEmployeeParticularIncentive(EmployeeIncentiveDto input)
        {
            var exists = await _employeeVsIncentiveRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == input.EmployeeRefId && t.IncentiveTagRefId == input.IncentiveRefId);
            if (exists != null)
            {
                await _employeeVsIncentiveRepo.DeleteAsync(exists.Id);
            }
        }



        public async Task DeleteIncentiveTag(IdInput input)
        {
            var exists = await _employeeVsIncentiveRepo.FirstOrDefaultAsync(t => t.IncentiveTagRefId == input.Id);
            if (exists != null)
            {
                throw new UserFriendlyException(L("IncentiveDeleteEmployeeReferenceError"));
            }
            var miexists = await _manualIncentiveRepo.FirstOrDefaultAsync(t => t.IncentiveTagRefId == input.Id);
            if (miexists != null)
            {
                throw new UserFriendlyException(L("IncentiveGivenAlreadyDeleteError"));
            }

            var existsSalaryTags = await _employeeVsSalaryTagRepo.FirstOrDefaultAsync(t => t.SalaryRefId == input.Id);
            if (existsSalaryTags != null)
            {
                throw new UserFriendlyException(L("ReferenceExists", L("Salary"), L("Incentive")));
            }

            //@@@Pending
            // Check SalaryPaidProcess Table 

            var incEntities = await _incentivevsentityRepo.GetAllListAsync(t => t.IncentiveTagRefId == input.Id);
            foreach (var lst in incEntities)
            {
                await _incentivevsentityRepo.DeleteAsync(lst.Id);
            }
            await _incentivetagRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateIncentiveTag(CreateOrUpdateIncentiveTagInput input)
        {
            try
            {

                var item = await _incentivetagRepo.GetAsync(input.IncentiveTag.Id.Value);
                var dto = input.IncentiveTag;

                item.IncentivePayModeId = dto.IncentivePayModeId;
                item.AllowanceOrDeductionRefId = dto.AllowanceOrDeductionRefId;
                item.IncentiveTagCode = dto.IncentiveTagCode;
                item.IncentiveTagName = dto.IncentiveTagName;
                item.IncentiveCategoryRefId = dto.IncentiveCategoryRefId;
                item.IncentiveDescription = dto.IncentiveDescription;
                item.HoursBased = dto.HoursBased;
                item.FullWorkHours = dto.FullWorkHours;
                item.MinimumHours = dto.MinimumHours;
                item.IncludeInConsolidationPerDay = dto.IncludeInConsolidationPerDay;
                item.IncentiveAmount = dto.IncentiveAmount;
                item.MinimumOtHours = dto.MinimumOtHours;
                item.IncenitveBasedOnSalaryTags = dto.IncenitveBasedOnSalaryTags;
                item.SalaryTagRefId = dto.SalaryTagRefId;
                item.ProportianteValueOfSalaryTags = dto.ProportianteValueOfSalaryTags;
                item.MinimumClaimedHours = dto.MinimumClaimedHours;
                item.OverTimeBasedIncentive = dto.OverTimeBasedIncentive;
                item.DisplayNotApplicable = dto.DisplayNotApplicable;
                item.HourlyClaimableTimeSheets = dto.HourlyClaimableTimeSheets;
                item.MinRange = dto.MinRange;
                item.MaxRange = dto.MaxRange;
                item.IncentiveEntryMode = dto.IncentiveEntryMode;
                item.CanIssueToIdleStatus = dto.CanIssueToIdleStatus;
                item.ActiveStatus = dto.ActiveStatus;
                item.CanBeAssignedToAllEmployees = dto.CanBeAssignedToAllEmployees;
                item.OnlyOnePersonOfTheTeamCanReceived = dto.OnlyOnePersonOfTheTeamCanReceived;
                item.TimeSheetRequiredForGivenDate = dto.TimeSheetRequiredForGivenDate;
                item.IsMaterialQuantityBasedIncentiveFlag = dto.IsMaterialQuantityBasedIncentiveFlag;
                item.IsCertificateMandatory = dto.IsCertificateMandatory;

                item.CalculationFormula = dto.CalculationFormula;
                item.AnySpecialFormulaRefId = dto.AnySpecialFormulaRefId;
                item.FormulaRefId = dto.FormulaRefId;
                item.CalculationFormulaMinimumValue = dto.CalculationFormulaMinimumValue;
                item.CalculationFormulaMaximumValue = dto.CalculationFormulaMaximumValue;
                item.OnlyAllowedOnWorkDay = dto.OnlyAllowedOnWorkDay;
                item.WorkDayHourBasedIncentive = dto.WorkDayHourBasedIncentive;
                item.DoesIssueOnlyForBillableEmployee = dto.DoesIssueOnlyForBillableEmployee;
                item.IsSytemGeneratedIncentive = dto.IsSytemGeneratedIncentive;
                item.IsItDeductableFromGrossSalary = dto.IsItDeductableFromGrossSalary;
                item.IsFixedAmount = dto.IsFixedAmount;
                item.IsBulkUpdateAllowed = dto.IsBulkUpdateAllowed;
                item.IsPfPayable = dto.IsPfPayable;
                item.IsGovernmentInsurancePayable = dto.IsGovernmentInsurancePayable;

                CheckErrors(await _incentivetagManager.CreateSync(item));

                List<int> incentiveEntityToBeRetained = new List<int>();
                if (input.IncentiveVsEntityList != null && input.IncentiveVsEntityList.Count > 0)
                {
                    foreach (var items in input.IncentiveVsEntityList)
                    {
                        if (items.EntityRefId == 0)
                        {
                            throw new UserFriendlyException("References");
                        }
                        IncentiveVsEntity existingDetail;
                        if (items.Id == 0)
                            existingDetail = await _incentivevsentityRepo.FirstOrDefaultAsync(u => u.IncentiveTagRefId == dto.Id && u.EntityRefId == items.EntityRefId && u.EntityType == items.EntityType && u.ExcludeFlag == items.ExcludeFlag);
                        else
                        {
                            existingDetail = await _incentivevsentityRepo.FirstOrDefaultAsync(u => u.Id == items.Id);
                        }

                        if (existingDetail == null)  //Add new record
                        {
                            IncentiveVsEntity det = new IncentiveVsEntity();
                            det.IncentiveTagRefId = (int)dto.Id;
                            det.EntityType = items.EntityType;
                            det.EntityRefId = items.EntityRefId;
                            det.ExcludeFlag = items.ExcludeFlag;
                            det.EitherOr_ForSameEntity = items.EitherOr_ForSameEntity;
                            var retId2 = await _incentivevsentityRepo.InsertAndGetIdAsync(det);
                            incentiveEntityToBeRetained.Add(retId2);
                        }
                        else
                        {
                            var editDetailDto = await _incentivevsentityRepo.GetAsync(existingDetail.Id);
                            editDetailDto.IncentiveTagRefId = (int)dto.Id;
                            editDetailDto.EntityType = items.EntityType;
                            editDetailDto.EntityRefId = items.EntityRefId;
                            editDetailDto.ExcludeFlag = items.ExcludeFlag;
                            editDetailDto.EitherOr_ForSameEntity = items.EitherOr_ForSameEntity;
                            var retId3 = await _incentivevsentityRepo.InsertOrUpdateAndGetIdAsync(editDetailDto);
                            incentiveEntityToBeRetained.Add(retId3);
                        }
                    }
                }

                var delMatList = await _incentivevsentityRepo.GetAll().Where(a => a.IncentiveTagRefId == input.IncentiveTag.Id.Value && !incentiveEntityToBeRetained.Contains(a.Id)).ToListAsync();

                foreach (var a in delMatList)
                {
                    await _incentivevsentityRepo.DeleteAsync(a.Id);
                }
            }
            catch (Exception ex)
            {
                MethodBase m = MethodBase.GetCurrentMethod();
                string innerExceptionMessage = ex.InnerException != null ? ex.InnerException.Message : "";
                throw new UserFriendlyException(" Method : " + m.ReflectedType.Name + " : " + m.Name + " " + ex.Message + " InnerException : " + innerExceptionMessage);
            }
        }

        protected virtual async Task CreateIncentiveTag(CreateOrUpdateIncentiveTagInput input)
        {
            try
            {
                var dto = input.IncentiveTag.MapTo<IncentiveTag>();

                CheckErrors(await _incentivetagManager.CreateSync(dto));

                if (input.IncentiveVsEntityList != null && input.IncentiveVsEntityList.Count > 0)
                {
                    foreach (var ie in input.IncentiveVsEntityList)
                    {
                        IncentiveVsEntity det = new IncentiveVsEntity();
                        det.IncentiveTagRefId = (int)dto.Id;
                        det.EntityType = ie.EntityType;
                        det.EntityRefId = ie.EntityRefId;
                        det.ExcludeFlag = ie.ExcludeFlag;
                        det.EitherOr_ForSameEntity = ie.EitherOr_ForSameEntity;
                        var retId2 = await _incentivevsentityRepo.InsertAndGetIdAsync(det);
                    }
                }

            }
            catch (Exception ex)
            {
                MethodBase m = MethodBase.GetCurrentMethod();
                string innerExceptionMessage = ex.InnerException != null ? ex.InnerException.Message : "";
                throw new UserFriendlyException(" Method : " + m.ReflectedType.Name + " : " + m.Name + " " + ex.Message + " InnerException : " + innerExceptionMessage);
            }


        }

        public async Task<ListResultOutput<IncentiveTagListDto>> GetIncentiveTagCodes()
        {
            var lstIncentiveTag = await _incentivetagRepo.GetAll().Where(t => t.ActiveStatus == true && t.IncentivePayModeId == (int)IncentivePayMode.Daily).ToListAsync();
            return new ListResultOutput<IncentiveTagListDto>(lstIncentiveTag.MapTo<List<IncentiveTagListDto>>());
        }

        public async Task<ListResultOutput<IncentiveTagListDto>> GetNonAutomaticIncentiveTagCodes()
        {
            var lstIncentiveTag = await _incentivetagRepo.GetAll().Where(t => t.IncentiveEntryMode != (int)IncentiveEntryMode.AUTOMATIC && t.ActiveStatus == true && t.IncentivePayModeId == (int)IncentivePayMode.Daily).ToListAsync();
            return new ListResultOutput<IncentiveTagListDto>(lstIncentiveTag.MapTo<List<IncentiveTagListDto>>());
        }


        public async Task<ListResultOutput<IncentiveTagListDto>> GetSalaryTagCodes()
        {
            var lstIncentiveTag = await _incentivetagRepo.GetAll().Where(t => t.ActiveStatus == true && t.IncentivePayModeId == (int)IncentivePayMode.Monthly).ToListAsync();
            return new ListResultOutput<IncentiveTagListDto>(lstIncentiveTag.MapTo<List<IncentiveTagListDto>>());
        }

        public async Task<ListResultOutput<IncentiveTagListDto>> GetNonAutomaticSalaryTagCodes()
        {
            var lstIncentiveTag = await _incentivetagRepo.GetAll().Where(t => t.IncentiveEntryMode != (int)IncentiveEntryMode.AUTOMATIC && t.ActiveStatus == true && t.IncentivePayModeId == (int)IncentivePayMode.Monthly).ToListAsync();
            return new ListResultOutput<IncentiveTagListDto>(lstIncentiveTag.MapTo<List<IncentiveTagListDto>>());
        }


        public async Task<ListResultOutput<IncentiveTagListDto>> GetAllowanceSalaryTagCodes(NullableEmployeeId input)
        {
            List<int> incentiveToBeOmitted = new List<int>();
            PersonalInformation personalInformation = null;
            if (input.EmployeeRefId.HasValue)
            {
                var empIncentives = await _employeeVsSalaryTagRepo.GetAllListAsync(t => t.EmployeeRefId == input.EmployeeRefId.Value);
                if (empIncentives.Count > 0)
                {
                    incentiveToBeOmitted.AddRange(empIncentives.Select(t => t.SalaryRefId).ToList());
                }
                personalInformation = await _personalInformationRepo.FirstOrDefaultAsync(t => t.Id == input.EmployeeRefId);
            }

            var lstIncentiveTag = await _incentivetagRepo.GetAll().Where(t => t.ActiveStatus == true && t.IncentivePayModeId == (int)IncentivePayMode.Monthly && t.AllowanceOrDeductionRefId == (int)AllowanceOrDeductionMode.Allowance && !incentiveToBeOmitted.Contains(t.Id)).OrderBy(t => t.IncentiveTagCode).ToListAsync();
            if (personalInformation != null)
            {
                if (personalInformation.isBillable == false)
                {
                    lstIncentiveTag = lstIncentiveTag.Where(t => t.DoesIssueOnlyForBillableEmployee == false).ToList();
                }
            }
            return new ListResultOutput<IncentiveTagListDto>(lstIncentiveTag.MapTo<List<IncentiveTagListDto>>());
        }

        public async Task<ListResultOutput<IncentiveTagListDto>> GetStatutoryAllowanceSalaryTagCodes(NullableEmployeeId input)
        {
            List<int> incentiveToBeOmitted = new List<int>();
            PersonalInformation personalInformation = null;
            if (input.EmployeeRefId.HasValue)
            {
                var empIncentives = await _employeeVsSalaryTagRepo.GetAllListAsync(t => t.EmployeeRefId == input.EmployeeRefId.Value);
                if (empIncentives.Count > 0)
                {
                    incentiveToBeOmitted.AddRange(empIncentives.Select(t => t.SalaryRefId).ToList());
                }
                personalInformation = await _personalInformationRepo.FirstOrDefaultAsync(t => t.Id == input.EmployeeRefId);
            }

            var lstIncentiveTag = await _incentivetagRepo.GetAll().Where(t => t.ActiveStatus == true && t.IncentivePayModeId == (int)IncentivePayMode.Monthly && t.AllowanceOrDeductionRefId == (int)AllowanceOrDeductionMode.Statutory_Allowance && !incentiveToBeOmitted.Contains(t.Id)).OrderBy(t => t.IncentiveTagCode).ToListAsync();
            if (personalInformation != null)
            {
                if (personalInformation.isBillable == false)
                {
                    lstIncentiveTag = lstIncentiveTag.Where(t => t.DoesIssueOnlyForBillableEmployee == false).ToList();
                }
            }
            return new ListResultOutput<IncentiveTagListDto>(lstIncentiveTag.MapTo<List<IncentiveTagListDto>>());
        }



        public async Task<ListResultOutput<IncentiveTagListDto>> GetDeductionSalaryTagCodes(NullableEmployeeId input)
        {
            List<int> incentiveToBeOmitted = new List<int>();
            PersonalInformation personalInformation = null;
            if (input.EmployeeRefId.HasValue)
            {
                var empIncentives = await _employeeVsSalaryTagRepo.GetAllListAsync(t => t.EmployeeRefId == input.EmployeeRefId.Value);
                if (empIncentives.Count > 0)
                {
                    incentiveToBeOmitted.AddRange(empIncentives.Select(t => t.SalaryRefId).ToList());
                }
                personalInformation = await _personalInformationRepo.FirstOrDefaultAsync(t => t.Id == input.EmployeeRefId);
            }
            var lstIncentiveTag = await _incentivetagRepo.GetAll().Where(t => t.ActiveStatus == true && t.IncentivePayModeId == (int)IncentivePayMode.Monthly && t.AllowanceOrDeductionRefId == (int)AllowanceOrDeductionMode.Deduction && !incentiveToBeOmitted.Contains(t.Id)).ToListAsync();
            if (personalInformation != null)
            {
                if (personalInformation.isBillable == false)
                {
                    lstIncentiveTag = lstIncentiveTag.Where(t => t.DoesIssueOnlyForBillableEmployee == false).ToList();
                }
            }
            return new ListResultOutput<IncentiveTagListDto>(lstIncentiveTag.MapTo<List<IncentiveTagListDto>>());
        }

        public async Task<ListResultOutput<IncentiveTagListDto>> GetStatutoryDeductionSalaryTagCodes(NullableEmployeeId input)
        {
            List<int> incentiveToBeOmitted = new List<int>();
            PersonalInformation personalInformation = null;
            if (input.EmployeeRefId.HasValue)
            {
                var empIncentives = await _employeeVsSalaryTagRepo.GetAllListAsync(t => t.EmployeeRefId == input.EmployeeRefId.Value);
                if (empIncentives.Count > 0)
                {
                    incentiveToBeOmitted.AddRange(empIncentives.Select(t => t.SalaryRefId).ToList());
                }
                personalInformation = await _personalInformationRepo.FirstOrDefaultAsync(t => t.Id == input.EmployeeRefId);
            }
            var lstIncentiveTag = await _incentivetagRepo.GetAll().Where(t => t.ActiveStatus == true && t.IncentivePayModeId == (int)IncentivePayMode.Monthly && t.AllowanceOrDeductionRefId == (int)AllowanceOrDeductionMode.Statutory_Deduction && !incentiveToBeOmitted.Contains(t.Id)).ToListAsync();
            if (personalInformation != null)
            {
                if (personalInformation.isBillable == false)
                {
                    lstIncentiveTag = lstIncentiveTag.Where(t => t.DoesIssueOnlyForBillableEmployee == false).ToList();
                }
            }
            return new ListResultOutput<IncentiveTagListDto>(lstIncentiveTag.MapTo<List<IncentiveTagListDto>>());
        }


        public async Task<PagedResultOutput<IncentiveCategoryListDto>> GetAllIncentiveCategory(GetIncentiveCategoryInput input)
        {
            var allItems = _incentivecategoryRepo
            .GetAll()
            .WhereIf(
                !input.Filter.IsNullOrEmpty(),
                p => p.IncentiveCategoryCode.Contains(input.Filter)
            );

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<IncentiveCategoryListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<IncentiveCategoryListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllIncentiveCategoryToExcel()
        {
            var allList = await _incentivecategoryRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<IncentiveCategoryListDto>>();
            return _incentivetagExporter.IncentiveCategoryExportToFile(allListDtos);
        }

        public async Task<GetIncentiveCategoryForEditOutput> GetIncentiveCategoryForEdit(NullableIdInput input)
        {
            IncentiveCategoryEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _incentivecategoryRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<IncentiveCategoryEditDto>();
            }
            else
            {
                editDto = new IncentiveCategoryEditDto();
            }

            return new GetIncentiveCategoryForEditOutput
            {
                IncentiveCategory = editDto
            };
        }

        public async Task CreateOrUpdateIncentiveCategory(CreateOrUpdateIncentiveCategoryInput input)
        {
            if (input.IncentiveCategory.Id.HasValue)
            {
                await UpdateIncentiveCategory(input);
            }
            else
            {
                await CreateIncentiveCategory(input);
            }
        }

        public async Task DeleteIncentiveCategory(IdInput input)
        {
            var exists = await _incentivetagRepo.FirstOrDefaultAsync(t => t.IncentiveCategoryRefId == input.Id);
            if (exists == null)
                await _incentivecategoryRepo.DeleteAsync(input.Id);
            else
                throw new UserFriendlyException(L("IncentiveTagExistsForThisCategory"));
        }

        protected virtual async Task UpdateIncentiveCategory(CreateOrUpdateIncentiveCategoryInput input)
        {
            var item = await _incentivecategoryRepo.GetAsync(input.IncentiveCategory.Id.Value);
            var dto = input.IncentiveCategory;
            item.IncentiveCategoryCode = dto.IncentiveCategoryCode;
            item.IncentiveCategoryName = dto.IncentiveCategoryName;
            item.BasedOnSkillSet = dto.BasedOnSkillSet;
            item.BasedOnQuantity = dto.BasedOnQuantity;
            item.MaximumIncenitivePerQuanityPerTimeSheet = dto.MaximumIncenitivePerQuanityPerTimeSheet;


            CheckErrors(await _incentivetagManager.IncentiveCategoryCreateSync(item));
        }

        protected virtual async Task CreateIncentiveCategory(CreateOrUpdateIncentiveCategoryInput input)
        {
            var dto = input.IncentiveCategory.MapTo<IncentiveCategory>();

            CheckErrors(await _incentivetagManager.IncentiveCategoryCreateSync(dto));
        }

        public async Task<ListResultOutput<IncentiveCategoryListDto>> GetIncentiveCategoryCodes()
        {
            var lstIncentiveCategory = await _incentivecategoryRepo.GetAll().ToListAsync();
            return new ListResultOutput<IncentiveCategoryListDto>(lstIncentiveCategory.MapTo<List<IncentiveCategoryListDto>>());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetIncetivEntityTypeForCombobox()
        {
            List<ComboboxItemDto> retList = new List<ComboboxItemDto>();

            string enumstring;
            Array EnumValues = System.Enum.GetValues(typeof(IncentiveEntityType));
            foreach (int EnumValue in EnumValues)
            {
                enumstring = Enum.GetName(typeof(IncentiveEntityType), EnumValue);
                retList.Add(new ComboboxItemDto { Value = EnumValue.ToString(), DisplayText = enumstring });
            }

            return
                   new ListResultOutput<ComboboxItemDto>(
                       retList.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetIncentiveGenerationModeForCombobox()
        {
            List<ComboboxItemDto> retList = new List<ComboboxItemDto>();

            string enumstring;
            Array EnumValues = System.Enum.GetValues(typeof(IncentiveEntryMode));
            foreach (int EnumValue in EnumValues)
            {
                enumstring = Enum.GetName(typeof(IncentiveEntryMode), EnumValue);
                retList.Add(new ComboboxItemDto { Value = EnumValue.ToString(), DisplayText = enumstring });
            }

            return
                   new ListResultOutput<ComboboxItemDto>(
                       retList.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetAllowanceOrDeductionModeForCombobox()
        {
            List<ComboboxItemDto> retList = new List<ComboboxItemDto>();

            string enumstring;
            Array EnumValues = System.Enum.GetValues(typeof(AllowanceOrDeductionMode));
            foreach (int EnumValue in EnumValues)
            {
                enumstring = Enum.GetName(typeof(AllowanceOrDeductionMode), EnumValue);
                retList.Add(new ComboboxItemDto { Value = EnumValue.ToString(), DisplayText = enumstring });
            }

            return
                   new ListResultOutput<ComboboxItemDto>(
                       retList.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetIncentivePayModeForCombobox()
        {
            List<ComboboxItemDto> retList = new List<ComboboxItemDto>();

            string enumstring;
            Array EnumValues = System.Enum.GetValues(typeof(IncentivePayMode));
            foreach (int EnumValue in EnumValues)
            {
                enumstring = Enum.GetName(typeof(IncentivePayMode), EnumValue);
                retList.Add(new ComboboxItemDto { Value = EnumValue.ToString(), DisplayText = enumstring });
            }

            return
                   new ListResultOutput<ComboboxItemDto>(
                       retList.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText)).ToList());
        }

        public async Task<bool> CheckSystemIncentiveList()
        {
            var systemIncentiveExists = await _incentivetagRepo.CountAsync(t => t.IsSytemGeneratedIncentive == true);
            if (systemIncentiveExists == 0)
            {
                return true;
            }
            return false;
        }


        public async Task ImportSystemIncentiveList()
        {
            IncentiveCategoryListDto allowanceCategoryDto = new IncentiveCategoryListDto();
            IncentiveCategoryListDto deductionCategoryDto = new IncentiveCategoryListDto();

            IncentiveCategoryListDto statutoryAllowanceCategoryDto = new IncentiveCategoryListDto();
            IncentiveCategoryListDto statutoryDeductionCategoryDto = new IncentiveCategoryListDto();

            #region Category Dto


            var allowanceCategory = await _incentivecategoryRepo.FirstOrDefaultAsync(t => t.IncentiveCategoryCode.ToUpper().Equals("Allowance"));
            if (allowanceCategory == null)
            {
                IncentiveCategoryEditDto allowanceEditDto = new IncentiveCategoryEditDto
                {
                    IncentiveCategoryCode = "Allowance",
                    IncentiveCategoryName = "Allowance",
                };
                CreateOrUpdateIncentiveCategoryInput inputCatgDto = new CreateOrUpdateIncentiveCategoryInput { IncentiveCategory = allowanceEditDto };
                await CreateOrUpdateIncentiveCategory(inputCatgDto);
                allowanceCategory = await _incentivecategoryRepo.FirstOrDefaultAsync(t => t.IncentiveCategoryCode.ToUpper().Equals(allowanceEditDto.IncentiveCategoryCode.ToUpper()));
            }
            allowanceCategoryDto = allowanceCategory.MapTo<IncentiveCategoryListDto>();

            var deductionCategory = await _incentivecategoryRepo.FirstOrDefaultAsync(t => t.IncentiveCategoryCode.ToUpper().Equals("Deduction"));
            if (deductionCategory == null)
            {
                IncentiveCategoryEditDto deductionEditDto = new IncentiveCategoryEditDto
                {
                    IncentiveCategoryCode = "Deduction",
                    IncentiveCategoryName = "Deduction",
                };
                CreateOrUpdateIncentiveCategoryInput inputCatgDto = new CreateOrUpdateIncentiveCategoryInput { IncentiveCategory = deductionEditDto };
                await CreateOrUpdateIncentiveCategory(inputCatgDto);
                deductionCategory = await _incentivecategoryRepo.FirstOrDefaultAsync(t => t.IncentiveCategoryCode.ToUpper().Equals(deductionEditDto.IncentiveCategoryCode.ToUpper()));
            }
            deductionCategoryDto = deductionCategory.MapTo<IncentiveCategoryListDto>();

            var statutoryAllowanceCategory = await _incentivecategoryRepo.FirstOrDefaultAsync(t => t.IncentiveCategoryCode.ToUpper().Equals("STATUTORY_ALLOWANCE"));
            if (statutoryAllowanceCategory == null)
            {
                IncentiveCategoryEditDto allowanceEditDto = new IncentiveCategoryEditDto
                {
                    IncentiveCategoryCode = "STATUTORY_ALLOWANCE",
                    IncentiveCategoryName = "STATUTORY_ALLOWANCE",
                };
                CreateOrUpdateIncentiveCategoryInput inputCatgDto = new CreateOrUpdateIncentiveCategoryInput { IncentiveCategory = allowanceEditDto };
                await CreateOrUpdateIncentiveCategory(inputCatgDto);
                statutoryAllowanceCategory = await _incentivecategoryRepo.FirstOrDefaultAsync(t => t.IncentiveCategoryCode.ToUpper().Equals("STATUTORY_ALLOWANCE"));
            }
            statutoryAllowanceCategoryDto = statutoryAllowanceCategory.MapTo<IncentiveCategoryListDto>();

            var statutoryDeductionCategory = await _incentivecategoryRepo.FirstOrDefaultAsync(t => t.IncentiveCategoryCode.ToUpper().Equals("STATUTORY_DEDUCTION"));
            if (statutoryDeductionCategory == null)
            {
                IncentiveCategoryEditDto deductionEditDto = new IncentiveCategoryEditDto
                {
                    IncentiveCategoryCode = "STATUTORY_DEDUCTION",
                    IncentiveCategoryName = "STATUTORY_DEDUCTION",
                };
                CreateOrUpdateIncentiveCategoryInput inputCatgDto = new CreateOrUpdateIncentiveCategoryInput { IncentiveCategory = deductionEditDto };
                await CreateOrUpdateIncentiveCategory(inputCatgDto);
                statutoryDeductionCategory = await _incentivecategoryRepo.FirstOrDefaultAsync(t => t.IncentiveCategoryCode.ToUpper().Equals("STATUTORY_DEDUCTION"));
            }
            statutoryDeductionCategoryDto = statutoryDeductionCategory.MapTo<IncentiveCategoryListDto>();
            #endregion

            #region General Incentive
            {
                IncentiveTag incTag = new IncentiveTag(AbpSession.TenantId.Value, (int)IncentivePayMode.Monthly, (int)AllowanceOrDeductionMode.Allowance, "", 0, "Increment", "Increment", "Increment", (int)IncentiveEntryMode.AUTOMATIC, true, false, null, 0, 1000, false, false);
                incTag.IncentiveCategoryRefId = allowanceCategory.Id;

                var newincTag = await _incentivetagRepo.FirstOrDefaultAsync(t => t.IncentiveTagCode.ToUpper().Equals(incTag.IncentiveTagCode.ToUpper()));
                if (newincTag == null)
                {
                    IncentiveTagEditDto sgcpfTag = new IncentiveTagEditDto
                    {
                        IncentivePayModeId = incTag.IncentivePayModeId,
                        AllowanceOrDeductionRefId = incTag.AllowanceOrDeductionRefId,
                        IncentiveCategoryRefId = incTag.IncentiveCategoryRefId,
                        IncentiveTagCode = incTag.IncentiveTagCode,
                        IncentiveTagName = incTag.IncentiveTagName,
                        IncentiveDescription = incTag.IncentiveDescription,
                        IncentiveEntryMode = incTag.IncentiveEntryMode,
                        ActiveStatus = incTag.ActiveStatus,
                        AnySpecialFormulaRefId = incTag.AnySpecialFormulaRefId,
                        FormulaRefId = incTag.FormulaRefId,
                        CalculationFormulaMinimumValue = incTag.CalculationFormulaMinimumValue,
                        CalculationFormulaMaximumValue = incTag.CalculationFormulaMaximumValue,
                        IsSytemGeneratedIncentive = incTag.IsSytemGeneratedIncentive,
                        IsItDeductableFromGrossSalary = incTag.IsItDeductableFromGrossSalary
                    };

                    List<IncentiveVsEntityListDto> incentiveVsEntityList = new List<IncentiveVsEntityListDto>();

                    CreateOrUpdateIncentiveTagInput inputTagDto = new CreateOrUpdateIncentiveTagInput
                    {
                        IncentiveTag = sgcpfTag,
                        IncentiveVsEntityList = incentiveVsEntityList
                    };
                    await CreateOrUpdateIncentiveTag(inputTagDto);
                    newincTag = await _incentivetagRepo.FirstOrDefaultAsync(t => t.IncentiveTagCode.ToUpper().Equals(incTag.IncentiveTagCode));
                }
            }
            #endregion
            #region General Allowance
            {
                IncentiveTag incTag = new IncentiveTag(AbpSession.TenantId.Value, (int)IncentivePayMode.Monthly, (int)AllowanceOrDeductionMode.Allowance, "", 0, "Allowance", "Allowance", "Allowance", (int)IncentiveEntryMode.AUTOMATIC, true, false, null, 0, 10000, false, false);
                incTag.IncentiveCategoryRefId = allowanceCategory.Id;

                var newincTag = await _incentivetagRepo.FirstOrDefaultAsync(t => t.IncentiveTagCode.ToUpper().Equals(incTag.IncentiveTagCode.ToUpper()));
                if (newincTag == null)
                {
                    IncentiveTagEditDto sgcpfTag = new IncentiveTagEditDto
                    {
                        IncentivePayModeId = incTag.IncentivePayModeId,
                        AllowanceOrDeductionRefId = incTag.AllowanceOrDeductionRefId,
                        IncentiveCategoryRefId = incTag.IncentiveCategoryRefId,
                        IncentiveTagCode = incTag.IncentiveTagCode,
                        IncentiveTagName = incTag.IncentiveTagName,
                        IncentiveDescription = incTag.IncentiveDescription,
                        IncentiveEntryMode = incTag.IncentiveEntryMode,
                        ActiveStatus = incTag.ActiveStatus,
                        AnySpecialFormulaRefId = incTag.AnySpecialFormulaRefId,
                        FormulaRefId = incTag.FormulaRefId,
                        CalculationFormulaMinimumValue = incTag.CalculationFormulaMinimumValue,
                        CalculationFormulaMaximumValue = incTag.CalculationFormulaMaximumValue,
                        IsSytemGeneratedIncentive = incTag.IsSytemGeneratedIncentive,
                        IsItDeductableFromGrossSalary = incTag.IsItDeductableFromGrossSalary
                    };

                    List<IncentiveVsEntityListDto> incentiveVsEntityList = new List<IncentiveVsEntityListDto>();

                    CreateOrUpdateIncentiveTagInput inputTagDto = new CreateOrUpdateIncentiveTagInput
                    {
                        IncentiveTag = sgcpfTag,
                        IncentiveVsEntityList = incentiveVsEntityList
                    };
                    await CreateOrUpdateIncentiveTag(inputTagDto);
                    newincTag = await _incentivetagRepo.FirstOrDefaultAsync(t => t.IncentiveTagCode.ToUpper().Equals(incTag.IncentiveTagCode));
                }
            }
            #endregion
            #region Phone Allowance
            {
                IncentiveTag incTag = new IncentiveTag(AbpSession.TenantId.Value, (int)IncentivePayMode.Monthly, (int)AllowanceOrDeductionMode.Allowance, "", 0, "Phone Allowance", "Phone Allowance", "Phone Allowance", (int)IncentiveEntryMode.AUTOMATIC, true, false, null, 0, 300, false, false);
                incTag.IncentiveCategoryRefId = allowanceCategory.Id;

                var newincTag = await _incentivetagRepo.FirstOrDefaultAsync(t => t.IncentiveTagCode.ToUpper().Equals(incTag.IncentiveTagCode.ToUpper()));
                if (newincTag == null)
                {
                    IncentiveTagEditDto sgcpfTag = new IncentiveTagEditDto
                    {
                        IncentivePayModeId = incTag.IncentivePayModeId,
                        AllowanceOrDeductionRefId = incTag.AllowanceOrDeductionRefId,
                        IncentiveCategoryRefId = incTag.IncentiveCategoryRefId,
                        IncentiveTagCode = incTag.IncentiveTagCode,
                        IncentiveTagName = incTag.IncentiveTagName,
                        IncentiveDescription = incTag.IncentiveDescription,
                        IncentiveEntryMode = incTag.IncentiveEntryMode,
                        ActiveStatus = incTag.ActiveStatus,
                        AnySpecialFormulaRefId = incTag.AnySpecialFormulaRefId,
                        FormulaRefId = incTag.FormulaRefId,
                        CalculationFormulaMinimumValue = incTag.CalculationFormulaMinimumValue,
                        CalculationFormulaMaximumValue = incTag.CalculationFormulaMaximumValue,
                        IsSytemGeneratedIncentive = incTag.IsSytemGeneratedIncentive,
                        IsItDeductableFromGrossSalary = incTag.IsItDeductableFromGrossSalary,
                        IsPfPayable = incTag.IsPfPayable,
                        IsGovernmentInsurancePayable = incTag.IsGovernmentInsurancePayable,
                    };

                    List<IncentiveVsEntityListDto> incentiveVsEntityList = new List<IncentiveVsEntityListDto>();

                    CreateOrUpdateIncentiveTagInput inputTagDto = new CreateOrUpdateIncentiveTagInput
                    {
                        IncentiveTag = sgcpfTag,
                        IncentiveVsEntityList = incentiveVsEntityList
                    };
                    await CreateOrUpdateIncentiveTag(inputTagDto);
                    newincTag = await _incentivetagRepo.FirstOrDefaultAsync(t => t.IncentiveTagCode.ToUpper().Equals(incTag.IncentiveTagCode));
                }
            }
            #endregion

            #region Accomodation Deduction
            {
                IncentiveTag incTag = new IncentiveTag(AbpSession.TenantId.Value, (int)IncentivePayMode.Monthly, (int)AllowanceOrDeductionMode.Deduction, "", 0, "Accommodation", "Accommodation", "Accommodation", (int)IncentiveEntryMode.MANUAL, true, false, null, 0, 0, false, false);
                incTag.IncentiveCategoryRefId = deductionCategory.Id;

                var newincTag = await _incentivetagRepo.FirstOrDefaultAsync(t => t.IncentiveTagCode.ToUpper().Equals(incTag.IncentiveTagCode.ToUpper()));
                if (newincTag == null)
                {
                    IncentiveTagEditDto sgcpfTag = new IncentiveTagEditDto
                    {
                        IncentivePayModeId = incTag.IncentivePayModeId,
                        AllowanceOrDeductionRefId = incTag.AllowanceOrDeductionRefId,
                        IncentiveCategoryRefId = incTag.IncentiveCategoryRefId,
                        IncentiveTagCode = incTag.IncentiveTagCode,
                        IncentiveTagName = incTag.IncentiveTagName,
                        IncentiveDescription = incTag.IncentiveDescription,
                        IncentiveEntryMode = incTag.IncentiveEntryMode,
                        ActiveStatus = incTag.ActiveStatus,
                        AnySpecialFormulaRefId = incTag.AnySpecialFormulaRefId,
                        FormulaRefId = incTag.FormulaRefId,
                        CalculationFormulaMinimumValue = incTag.CalculationFormulaMinimumValue,
                        CalculationFormulaMaximumValue = incTag.CalculationFormulaMaximumValue,
                        IsSytemGeneratedIncentive = false,
                        IsItDeductableFromGrossSalary = incTag.IsItDeductableFromGrossSalary
                    };

                    List<IncentiveVsEntityListDto> incentiveVsEntityList = new List<IncentiveVsEntityListDto>();

                    CreateOrUpdateIncentiveTagInput inputTagDto = new CreateOrUpdateIncentiveTagInput
                    {
                        IncentiveTag = sgcpfTag,
                        IncentiveVsEntityList = incentiveVsEntityList
                    };
                    await CreateOrUpdateIncentiveTag(inputTagDto);
                    newincTag = await _incentivetagRepo.FirstOrDefaultAsync(t => t.IncentiveTagCode.ToUpper().Equals(incTag.IncentiveTagCode));
                }
            }
            #endregion

            #region Loan Deduction
            {
                IncentiveTag incTag = new IncentiveTag(AbpSession.TenantId.Value, (int)IncentivePayMode.Monthly, (int)AllowanceOrDeductionMode.Deduction, "", 0, "Loan Deduction", "Loan Deduction", "Loan Deduction", (int)IncentiveEntryMode.AUTOMATIC, true, false, null, 0, 0, false, false);
                incTag.IncentiveCategoryRefId = deductionCategory.Id;

                var newincTag = await _incentivetagRepo.FirstOrDefaultAsync(t => t.IncentiveTagCode.ToUpper().Equals(incTag.IncentiveTagCode.ToUpper()));
                if (newincTag == null)
                {
                    IncentiveTagEditDto sgcpfTag = new IncentiveTagEditDto
                    {
                        IncentivePayModeId = incTag.IncentivePayModeId,
                        AllowanceOrDeductionRefId = incTag.AllowanceOrDeductionRefId,
                        IncentiveCategoryRefId = incTag.IncentiveCategoryRefId,
                        IncentiveTagCode = incTag.IncentiveTagCode,
                        IncentiveTagName = incTag.IncentiveTagName,
                        IncentiveDescription = incTag.IncentiveDescription,
                        IncentiveEntryMode = incTag.IncentiveEntryMode,
                        ActiveStatus = incTag.ActiveStatus,
                        AnySpecialFormulaRefId = incTag.AnySpecialFormulaRefId,
                        FormulaRefId = incTag.FormulaRefId,
                        CalculationFormulaMinimumValue = incTag.CalculationFormulaMinimumValue,
                        CalculationFormulaMaximumValue = incTag.CalculationFormulaMaximumValue,
                        IsSytemGeneratedIncentive = incTag.IsSytemGeneratedIncentive,
                        IsItDeductableFromGrossSalary = incTag.IsItDeductableFromGrossSalary
                    };

                    List<IncentiveVsEntityListDto> incentiveVsEntityList = new List<IncentiveVsEntityListDto>();

                    CreateOrUpdateIncentiveTagInput inputTagDto = new CreateOrUpdateIncentiveTagInput
                    {
                        IncentiveTag = sgcpfTag,
                        IncentiveVsEntityList = incentiveVsEntityList
                    };
                    await CreateOrUpdateIncentiveTag(inputTagDto);
                    newincTag = await _incentivetagRepo.FirstOrDefaultAsync(t => t.IncentiveTagCode.ToUpper().Equals(incTag.IncentiveTagCode));
                }
            }
            #endregion

            var countryName = FeatureChecker.GetValue(AppFeatures.ConnectCountry);

            if (countryName.ToUpper().Equals("SG"))
            {
                bool isDeductableFromSalary = false;
                #region SG Levy - Paid By Company 
                {
                    IncentiveTag incTag = new IncentiveTag(AbpSession.TenantId.Value, (int)IncentivePayMode.Monthly, (int)AllowanceOrDeductionMode.Statutory_Allowance, "STATUTORY_ALLOWANCE", 0, "Levy", "Foreign Worker Levy", "Foreign Worker Levy", (int)IncentiveEntryMode.AUTOMATIC, true, true, (int)SpecialFormulaTagReference.Levy, 0, 1200, true, false);
                    incTag.IncentiveCategoryRefId = statutoryAllowanceCategoryDto.Id;

                    var sgLevy = await _incentivetagRepo.FirstOrDefaultAsync(t => t.IncentiveTagCode.ToUpper().Equals(incTag.IncentiveTagCode.ToUpper()));
                    if (sgLevy == null)
                    {
                        IncentiveTagEditDto sgcpfTag = new IncentiveTagEditDto
                        {
                            IncentivePayModeId = incTag.IncentivePayModeId,
                            AllowanceOrDeductionRefId = incTag.AllowanceOrDeductionRefId,
                            IncentiveCategoryRefId = incTag.IncentiveCategoryRefId,
                            IncentiveTagCode = incTag.IncentiveTagCode,
                            IncentiveTagName = incTag.IncentiveTagName,
                            IncentiveDescription = incTag.IncentiveDescription,
                            IncentiveEntryMode = incTag.IncentiveEntryMode,
                            ActiveStatus = incTag.ActiveStatus,
                            AnySpecialFormulaRefId = incTag.AnySpecialFormulaRefId,
                            FormulaRefId = incTag.FormulaRefId,
                            CalculationFormulaMinimumValue = incTag.CalculationFormulaMinimumValue,
                            CalculationFormulaMaximumValue = incTag.CalculationFormulaMaximumValue,
                            IsSytemGeneratedIncentive = false,
                            IsItDeductableFromGrossSalary = incTag.IsItDeductableFromGrossSalary
                        };

                        List<IncentiveVsEntityListDto> incentiveVsEntityList = new List<IncentiveVsEntityListDto>();

                        var residentStatus = await _employeeResidentStatusRepo.FirstOrDefaultAsync(t => t.ResidentStatus.ToUpper().Equals("SP"));
                        if (residentStatus == null)
                        {
                            throw new UserFriendlyException("S Pass - Resident Status Not Found");
                        }
                        else
                        {
                            IncentiveVsEntityListDto newincVsEntity = new IncentiveVsEntityListDto
                            {
                                EntityType = (int)IncentiveEntityType.RESIDENT_STATUS,
                                EntityRefId = residentStatus.Id,
                                EitherOr_ForSameEntity = true,
                                ExcludeFlag = false
                            };
                            incentiveVsEntityList.Add(newincVsEntity);
                        }


                        residentStatus = await _employeeResidentStatusRepo.FirstOrDefaultAsync(t => t.ResidentStatus.ToUpper().Equals("WP"));
                        if (residentStatus == null)
                        {
                            throw new UserFriendlyException("WP - Resident Status Not Found");
                        }
                        else
                        {
                            IncentiveVsEntityListDto newincVsEntity = new IncentiveVsEntityListDto
                            {
                                EntityType = (int)IncentiveEntityType.RESIDENT_STATUS,
                                EntityRefId = residentStatus.Id,
                                EitherOr_ForSameEntity = true,
                                ExcludeFlag = false
                            };
                            incentiveVsEntityList.Add(newincVsEntity);
                        }

                        CreateOrUpdateIncentiveTagInput inputTagDto = new CreateOrUpdateIncentiveTagInput
                        {
                            IncentiveTag = sgcpfTag,
                            IncentiveVsEntityList = incentiveVsEntityList
                        };
                        await CreateOrUpdateIncentiveTag(inputTagDto);
                        sgLevy = await _incentivetagRepo.FirstOrDefaultAsync(t => t.IncentiveTagCode.ToUpper().Equals(incTag.IncentiveTagCode));
                    }
                }
                #endregion


                isDeductableFromSalary = false;
                #region SG CPF - Employer Contribution - Paid By Company 
                {
                    IncentiveTag incTag = new IncentiveTag(AbpSession.TenantId.Value, (int)IncentivePayMode.Monthly, (int)AllowanceOrDeductionMode.Statutory_Allowance, "STATUTORY_ALLOWANCE", 0, "CPF-Employer Contribution", "CPF-Employer Contribution", "CPF-Employer Contribution", (int)IncentiveEntryMode.AUTOMATIC, true, true, (int)SpecialFormulaTagReference.SG_Employer_CPF, 0, 1200, true, isDeductableFromSalary);
                    incTag.IncentiveCategoryRefId = statutoryAllowanceCategoryDto.Id;

                    var sgErCPF = await _incentivetagRepo.FirstOrDefaultAsync(t => t.IncentiveTagCode.ToUpper().Equals(incTag.IncentiveTagCode.ToUpper()));
                    if (sgErCPF == null)
                    {
                        IncentiveTagEditDto sgcpfTag = new IncentiveTagEditDto
                        {
                            IncentivePayModeId = incTag.IncentivePayModeId,
                            AllowanceOrDeductionRefId = incTag.AllowanceOrDeductionRefId,
                            IncentiveCategoryRefId = incTag.IncentiveCategoryRefId,
                            IncentiveTagCode = incTag.IncentiveTagCode,
                            IncentiveTagName = incTag.IncentiveTagName,
                            IncentiveDescription = incTag.IncentiveDescription,
                            IncentiveEntryMode = incTag.IncentiveEntryMode,
                            ActiveStatus = incTag.ActiveStatus,
                            AnySpecialFormulaRefId = incTag.AnySpecialFormulaRefId,
                            FormulaRefId = incTag.FormulaRefId,
                            CalculationFormulaMinimumValue = incTag.CalculationFormulaMinimumValue,
                            CalculationFormulaMaximumValue = incTag.CalculationFormulaMaximumValue,
                            IsSytemGeneratedIncentive = true,
                            IsItDeductableFromGrossSalary = incTag.IsItDeductableFromGrossSalary
                        };

                        List<IncentiveVsEntityListDto> incentiveVsEntityList = new List<IncentiveVsEntityListDto>();

                        var residentStatus = await _employeeResidentStatusRepo.FirstOrDefaultAsync(t => t.ResidentStatus.ToUpper().Equals("CITIZEN"));
                        if (residentStatus == null)
                        {
                            throw new UserFriendlyException("Citizen - Resident Status Not Found");
                        }
                        else
                        {
                            IncentiveVsEntityListDto newincVsEntity = new IncentiveVsEntityListDto
                            {
                                EntityType = (int)IncentiveEntityType.RESIDENT_STATUS,
                                EntityRefId = residentStatus.Id,
                                EitherOr_ForSameEntity = true,
                                ExcludeFlag = false
                            };
                            incentiveVsEntityList.Add(newincVsEntity);
                        }


                        residentStatus = await _employeeResidentStatusRepo.FirstOrDefaultAsync(t => t.ResidentStatus.ToUpper().Equals("PR"));
                        if (residentStatus == null)
                        {
                            throw new UserFriendlyException("PR - Resident Status Not Found");
                        }
                        else
                        {
                            IncentiveVsEntityListDto newincVsEntity = new IncentiveVsEntityListDto
                            {
                                EntityType = (int)IncentiveEntityType.RESIDENT_STATUS,
                                EntityRefId = residentStatus.Id,
                                EitherOr_ForSameEntity = true,
                                ExcludeFlag = false
                            };
                            incentiveVsEntityList.Add(newincVsEntity);
                        }

                        CreateOrUpdateIncentiveTagInput inputTagDto = new CreateOrUpdateIncentiveTagInput
                        {
                            IncentiveTag = sgcpfTag,
                            IncentiveVsEntityList = incentiveVsEntityList
                        };
                        await CreateOrUpdateIncentiveTag(inputTagDto);
                        sgErCPF = await _incentivetagRepo.FirstOrDefaultAsync(t => t.IncentiveTagCode.ToUpper().Equals(incTag.IncentiveTagCode));
                    }
                }
                #endregion

                isDeductableFromSalary = true;
                #region SG CPF - Employee Contribution - Deducted From Salary
                {
                    var incTag = new IncentiveTag(AbpSession.TenantId.Value, (int)IncentivePayMode.Monthly, (int)AllowanceOrDeductionMode.Statutory_Deduction, "STATUTORY_DEDUCTION", 0, "CPF-Employee Contribution", "CPF-Employee Contribution", "CPF-Employee Contribution", (int)IncentiveEntryMode.AUTOMATIC, true, true, (int)SpecialFormulaTagReference.SG_Employee_CPF, 0, 1020, true, isDeductableFromSalary);
                    incTag.IncentiveCategoryRefId = statutoryDeductionCategoryDto.Id;

                    var sgEeCPF = await _incentivetagRepo.FirstOrDefaultAsync(t => t.IncentiveTagCode.ToUpper().Equals(incTag.IncentiveTagCode.ToUpper()));
                    if (sgEeCPF == null)
                    {
                        IncentiveTagEditDto sgcpfTag = new IncentiveTagEditDto
                        {
                            IncentivePayModeId = incTag.IncentivePayModeId,
                            AllowanceOrDeductionRefId = incTag.AllowanceOrDeductionRefId,
                            IncentiveCategoryRefId = incTag.IncentiveCategoryRefId,
                            IncentiveTagCode = incTag.IncentiveTagCode,
                            IncentiveTagName = incTag.IncentiveTagName,
                            IncentiveDescription = incTag.IncentiveDescription,
                            IncentiveEntryMode = incTag.IncentiveEntryMode,
                            ActiveStatus = incTag.ActiveStatus,
                            AnySpecialFormulaRefId = incTag.AnySpecialFormulaRefId,
                            FormulaRefId = incTag.FormulaRefId,
                            CalculationFormulaMinimumValue = incTag.CalculationFormulaMinimumValue,
                            CalculationFormulaMaximumValue = incTag.CalculationFormulaMaximumValue,
                            IsSytemGeneratedIncentive = true,
                            IsItDeductableFromGrossSalary = incTag.IsItDeductableFromGrossSalary
                        };

                        List<IncentiveVsEntityListDto> incentiveVsEntityList = new List<IncentiveVsEntityListDto>();

                        var residentStatus = await _employeeResidentStatusRepo.FirstOrDefaultAsync(t => t.ResidentStatus.ToUpper().Equals("CITIZEN"));
                        if (residentStatus == null)
                        {
                            throw new UserFriendlyException("Citizen - Resident Status Not Found");
                        }
                        else
                        {
                            IncentiveVsEntityListDto newincVsEntity = new IncentiveVsEntityListDto
                            {
                                EntityType = (int)IncentiveEntityType.RESIDENT_STATUS,
                                EntityRefId = residentStatus.Id,
                                EitherOr_ForSameEntity = true,
                                ExcludeFlag = false
                            };
                            incentiveVsEntityList.Add(newincVsEntity);
                        }


                        residentStatus = await _employeeResidentStatusRepo.FirstOrDefaultAsync(t => t.ResidentStatus.ToUpper().Equals("PR"));
                        if (residentStatus == null)
                        {
                            throw new UserFriendlyException("PR - Resident Status Not Found");
                        }
                        else
                        {
                            IncentiveVsEntityListDto newincVsEntity = new IncentiveVsEntityListDto
                            {
                                EntityType = (int)IncentiveEntityType.RESIDENT_STATUS,
                                EntityRefId = residentStatus.Id,
                                EitherOr_ForSameEntity = true,
                                ExcludeFlag = false
                            };
                            incentiveVsEntityList.Add(newincVsEntity);
                        }

                        CreateOrUpdateIncentiveTagInput inputTagDto = new CreateOrUpdateIncentiveTagInput
                        {
                            IncentiveTag = sgcpfTag,
                            IncentiveVsEntityList = incentiveVsEntityList
                        };
                        await CreateOrUpdateIncentiveTag(inputTagDto);
                    }
                }
                #endregion

                isDeductableFromSalary = false;
                #region SDL Fund  - Deducted From Salary
                {
                    var incTag = new IncentiveTag(AbpSession.TenantId.Value, (int)IncentivePayMode.Monthly, (int)AllowanceOrDeductionMode.Statutory_Allowance, "STATUTORY_ALLOWANCE", 0, "SDL", "SDL", "Skill Development Fund", (int)IncentiveEntryMode.AUTOMATIC, true, true, (int)SpecialFormulaTagReference.SG_SDL, 0, 0, true, isDeductableFromSalary);
                    incTag.IncentiveCategoryRefId = statutoryAllowanceCategory.Id;

                    var sgSindaFund = await _incentivetagRepo.FirstOrDefaultAsync(t => t.IncentiveTagCode.ToUpper().Equals(incTag.IncentiveTagCode.ToUpper()));
                    if (sgSindaFund == null)
                    {
                        IncentiveTagEditDto sgcpfTag = new IncentiveTagEditDto
                        {
                            IncentivePayModeId = incTag.IncentivePayModeId,
                            AllowanceOrDeductionRefId = incTag.AllowanceOrDeductionRefId,
                            IncentiveCategoryRefId = incTag.IncentiveCategoryRefId,
                            IncentiveTagCode = incTag.IncentiveTagCode,
                            IncentiveTagName = incTag.IncentiveTagName,
                            IncentiveDescription = incTag.IncentiveDescription,
                            IncentiveEntryMode = incTag.IncentiveEntryMode,
                            ActiveStatus = incTag.ActiveStatus,
                            AnySpecialFormulaRefId = incTag.AnySpecialFormulaRefId,
                            FormulaRefId = incTag.FormulaRefId,
                            CalculationFormulaMinimumValue = incTag.CalculationFormulaMinimumValue,
                            CalculationFormulaMaximumValue = incTag.CalculationFormulaMaximumValue,
                            IsSytemGeneratedIncentive = true,
                            IsItDeductableFromGrossSalary = incTag.IsItDeductableFromGrossSalary,
                            CanBeAssignedToAllEmployees = true,
                            IsPfPayable = incTag.IsPfPayable,
                            IsGovernmentInsurancePayable = incTag.IsGovernmentInsurancePayable,
                        };

                        List<IncentiveVsEntityListDto> incentiveVsEntityList = new List<IncentiveVsEntityListDto>();

                        //var race = await _employeeRaceStatusRepo.FirstOrDefaultAsync(t => t.RaceName.ToUpper().Equals("INDIAN"));
                        //if (race == null)
                        //{
                        //    throw new UserFriendlyException("INDIAN - Race Status Not Found");
                        //}
                        //else
                        //{
                        //    IncentiveVsEntityListDto newincVsEntity = new IncentiveVsEntityListDto
                        //    {
                        //        EntityType = (int)IncentiveEntityType.RACE,
                        //        EntityRefId = race.Id,
                        //        EitherOr_ForSameEntity = false,
                        //        ExcludeFlag = false
                        //    };
                        //    incentiveVsEntityList.Add(newincVsEntity);
                        //}

                        CreateOrUpdateIncentiveTagInput inputTagDto = new CreateOrUpdateIncentiveTagInput
                        {
                            IncentiveTag = sgcpfTag,
                            IncentiveVsEntityList = incentiveVsEntityList
                        };
                        await CreateOrUpdateIncentiveTag(inputTagDto);
                    }

                }
                #endregion

                isDeductableFromSalary = true;
                #region SINDA Fund  - Deducted From Salary
                {
                    var incTag = new IncentiveTag(AbpSession.TenantId.Value, (int)IncentivePayMode.Monthly, (int)AllowanceOrDeductionMode.Statutory_Deduction, "STATUTORY_DEDUCTION", 0, "SINDA Fund", "SINDA Fund", "SINDA Fund", (int)IncentiveEntryMode.AUTOMATIC, true, true, (int)SpecialFormulaTagReference.SG_SINDA_DEDUCTION, 0, 0, true, isDeductableFromSalary);
                    incTag.IncentiveCategoryRefId = statutoryDeductionCategoryDto.Id;

                    var sgSindaFund = await _incentivetagRepo.FirstOrDefaultAsync(t => t.IncentiveTagCode.ToUpper().Equals(incTag.IncentiveTagCode.ToUpper()));
                    if (sgSindaFund == null)
                    {
                        IncentiveTagEditDto sgcpfTag = new IncentiveTagEditDto
                        {
                            IncentivePayModeId = incTag.IncentivePayModeId,
                            AllowanceOrDeductionRefId = incTag.AllowanceOrDeductionRefId,
                            IncentiveCategoryRefId = incTag.IncentiveCategoryRefId,
                            IncentiveTagCode = incTag.IncentiveTagCode,
                            IncentiveTagName = incTag.IncentiveTagName,
                            IncentiveDescription = incTag.IncentiveDescription,
                            IncentiveEntryMode = incTag.IncentiveEntryMode,
                            ActiveStatus = incTag.ActiveStatus,
                            AnySpecialFormulaRefId = incTag.AnySpecialFormulaRefId,
                            FormulaRefId = incTag.FormulaRefId,
                            CalculationFormulaMinimumValue = incTag.CalculationFormulaMinimumValue,
                            CalculationFormulaMaximumValue = incTag.CalculationFormulaMaximumValue,
                            IsSytemGeneratedIncentive = true,
                            IsItDeductableFromGrossSalary = incTag.IsItDeductableFromGrossSalary
                        };

                        List<IncentiveVsEntityListDto> incentiveVsEntityList = new List<IncentiveVsEntityListDto>();

                        var race = await _employeeRaceStatusRepo.FirstOrDefaultAsync(t => t.RaceName.ToUpper().Equals("INDIAN"));
                        if (race == null)
                        {
                            throw new UserFriendlyException("INDIAN - Race Status Not Found");
                        }
                        else
                        {
                            IncentiveVsEntityListDto newincVsEntity = new IncentiveVsEntityListDto
                            {
                                EntityType = (int)IncentiveEntityType.RACE,
                                EntityRefId = race.Id,
                                EitherOr_ForSameEntity = false,
                                ExcludeFlag = false
                            };
                            incentiveVsEntityList.Add(newincVsEntity);
                        }

                        CreateOrUpdateIncentiveTagInput inputTagDto = new CreateOrUpdateIncentiveTagInput
                        {
                            IncentiveTag = sgcpfTag,
                            IncentiveVsEntityList = incentiveVsEntityList
                        };
                        await CreateOrUpdateIncentiveTag(inputTagDto);
                    }

                }
                #endregion

                isDeductableFromSalary = true;
                #region MBMF Fund  - Deducted From Salary
                {
                    var incTag = new IncentiveTag(AbpSession.TenantId.Value, (int)IncentivePayMode.Monthly, (int)AllowanceOrDeductionMode.Statutory_Deduction, "STATUTORY_DEDUCTION", 0, "MBMF Fund", "MBMF Fund", "MBMF Fund", (int)IncentiveEntryMode.AUTOMATIC, true, true, (int)SpecialFormulaTagReference.SG_MBMF, 0, 0, true, isDeductableFromSalary);
                    incTag.IncentiveCategoryRefId = statutoryDeductionCategoryDto.Id;

                    var sgSindaFund = await _incentivetagRepo.FirstOrDefaultAsync(t => t.IncentiveTagCode.ToUpper().Equals(incTag.IncentiveTagCode.ToUpper()));
                    if (sgSindaFund == null)
                    {
                        IncentiveTagEditDto sgcpfTag = new IncentiveTagEditDto
                        {
                            IncentivePayModeId = incTag.IncentivePayModeId,
                            AllowanceOrDeductionRefId = incTag.AllowanceOrDeductionRefId,
                            IncentiveCategoryRefId = incTag.IncentiveCategoryRefId,
                            IncentiveTagCode = incTag.IncentiveTagCode,
                            IncentiveTagName = incTag.IncentiveTagName,
                            IncentiveDescription = incTag.IncentiveDescription,
                            IncentiveEntryMode = incTag.IncentiveEntryMode,
                            ActiveStatus = incTag.ActiveStatus,
                            AnySpecialFormulaRefId = incTag.AnySpecialFormulaRefId,
                            FormulaRefId = incTag.FormulaRefId,
                            CalculationFormulaMinimumValue = incTag.CalculationFormulaMinimumValue,
                            CalculationFormulaMaximumValue = incTag.CalculationFormulaMaximumValue,
                            IsSytemGeneratedIncentive = true,
                            IsItDeductableFromGrossSalary = incTag.IsItDeductableFromGrossSalary
                        };

                        List<IncentiveVsEntityListDto> incentiveVsEntityList = new List<IncentiveVsEntityListDto>();

                        var religion = await _employeeReligionRepo.FirstOrDefaultAsync(t => t.ReligionName.ToUpper().Equals("ISLAM"));
                        if (religion == null)
                        {
                            throw new UserFriendlyException("Islam - Religion Status Not Found");
                        }
                        else
                        {
                            IncentiveVsEntityListDto newincVsEntity = new IncentiveVsEntityListDto
                            {
                                EntityType = (int)IncentiveEntityType.RELIGION,
                                EntityRefId = religion.Id,
                                EitherOr_ForSameEntity = false,
                                ExcludeFlag = false
                            };
                            incentiveVsEntityList.Add(newincVsEntity);
                        }

                        CreateOrUpdateIncentiveTagInput inputTagDto = new CreateOrUpdateIncentiveTagInput
                        {
                            IncentiveTag = sgcpfTag,
                            IncentiveVsEntityList = incentiveVsEntityList
                        };
                        await CreateOrUpdateIncentiveTag(inputTagDto);
                    }

                }
                #endregion

                isDeductableFromSalary = true;
                #region CDAC Fund  - Deducted From Salary
                {
                    var incTag = new IncentiveTag(AbpSession.TenantId.Value, (int)IncentivePayMode.Monthly, (int)AllowanceOrDeductionMode.Statutory_Deduction, "STATUTORY_DEDUCTION", 0, "CDAC Fund", "CDAC Fund", "CDAC Fund", (int)IncentiveEntryMode.AUTOMATIC, true, true, (int)SpecialFormulaTagReference.SG_CDAC, 0, 0, true, isDeductableFromSalary);
                    incTag.IncentiveCategoryRefId = statutoryDeductionCategoryDto.Id;

                    var sgSindaFund = await _incentivetagRepo.FirstOrDefaultAsync(t => t.IncentiveTagCode.ToUpper().Equals(incTag.IncentiveTagCode.ToUpper()));
                    if (sgSindaFund == null)
                    {
                        IncentiveTagEditDto sgcpfTag = new IncentiveTagEditDto
                        {
                            IncentivePayModeId = incTag.IncentivePayModeId,
                            AllowanceOrDeductionRefId = incTag.AllowanceOrDeductionRefId,
                            IncentiveCategoryRefId = incTag.IncentiveCategoryRefId,
                            IncentiveTagCode = incTag.IncentiveTagCode,
                            IncentiveTagName = incTag.IncentiveTagName,
                            IncentiveDescription = incTag.IncentiveDescription,
                            IncentiveEntryMode = incTag.IncentiveEntryMode,
                            ActiveStatus = incTag.ActiveStatus,
                            AnySpecialFormulaRefId = incTag.AnySpecialFormulaRefId,
                            FormulaRefId = incTag.FormulaRefId,
                            CalculationFormulaMinimumValue = incTag.CalculationFormulaMinimumValue,
                            CalculationFormulaMaximumValue = incTag.CalculationFormulaMaximumValue,
                            IsSytemGeneratedIncentive = true,
                            IsItDeductableFromGrossSalary = incTag.IsItDeductableFromGrossSalary
                        };

                        List<IncentiveVsEntityListDto> incentiveVsEntityList = new List<IncentiveVsEntityListDto>();

                        var race = await _employeeRaceStatusRepo.FirstOrDefaultAsync(t => t.RaceName.ToUpper().Equals("Chinese"));
                        if (race == null)
                        {
                            throw new UserFriendlyException("Chinese - Race Status Not Found");
                        }
                        else
                        {
                            IncentiveVsEntityListDto newincVsEntity = new IncentiveVsEntityListDto
                            {
                                EntityType = (int)IncentiveEntityType.RACE,
                                EntityRefId = race.Id,
                                EitherOr_ForSameEntity = false,
                                ExcludeFlag = false
                            };
                            incentiveVsEntityList.Add(newincVsEntity);
                        }

                        CreateOrUpdateIncentiveTagInput inputTagDto = new CreateOrUpdateIncentiveTagInput
                        {
                            IncentiveTag = sgcpfTag,
                            IncentiveVsEntityList = incentiveVsEntityList
                        };
                        await CreateOrUpdateIncentiveTag(inputTagDto);
                    }

                }
                #endregion

                isDeductableFromSalary = true;
                #region Eurosian Fund  - Deducted From Salary
                {
                    var incTag = new IncentiveTag(AbpSession.TenantId.Value, (int)IncentivePayMode.Monthly, (int)AllowanceOrDeductionMode.Statutory_Deduction, "STATUTORY_DEDUCTION", 0, "Eurasian Fund", "Eurasian Fund", "Eurasian Fund", (int)IncentiveEntryMode.AUTOMATIC, true, true, (int)SpecialFormulaTagReference.SG_Eurasian, 0, 0, true, isDeductableFromSalary);
                    incTag.IncentiveCategoryRefId = statutoryDeductionCategoryDto.Id;

                    var sgSindaFund = await _incentivetagRepo.FirstOrDefaultAsync(t => t.IncentiveTagCode.ToUpper().Equals(incTag.IncentiveTagCode.ToUpper()));
                    if (sgSindaFund == null)
                    {
                        IncentiveTagEditDto sgcpfTag = new IncentiveTagEditDto
                        {
                            IncentivePayModeId = incTag.IncentivePayModeId,
                            AllowanceOrDeductionRefId = incTag.AllowanceOrDeductionRefId,
                            IncentiveCategoryRefId = incTag.IncentiveCategoryRefId,
                            IncentiveTagCode = incTag.IncentiveTagCode,
                            IncentiveTagName = incTag.IncentiveTagName,
                            IncentiveDescription = incTag.IncentiveDescription,
                            IncentiveEntryMode = incTag.IncentiveEntryMode,
                            ActiveStatus = incTag.ActiveStatus,
                            AnySpecialFormulaRefId = incTag.AnySpecialFormulaRefId,
                            FormulaRefId = incTag.FormulaRefId,
                            CalculationFormulaMinimumValue = incTag.CalculationFormulaMinimumValue,
                            CalculationFormulaMaximumValue = incTag.CalculationFormulaMaximumValue,
                            IsSytemGeneratedIncentive = true,
                            IsItDeductableFromGrossSalary = incTag.IsItDeductableFromGrossSalary
                        };

                        List<IncentiveVsEntityListDto> incentiveVsEntityList = new List<IncentiveVsEntityListDto>();

                        var race = await _employeeRaceStatusRepo.FirstOrDefaultAsync(t => t.RaceName.ToUpper().Equals("Eurasian"));
                        if (race == null)
                        {
                            throw new UserFriendlyException("Eurasian - Race Status Not Found");
                        }
                        else
                        {
                            IncentiveVsEntityListDto newincVsEntity = new IncentiveVsEntityListDto
                            {
                                EntityType = (int)IncentiveEntityType.RACE,
                                EntityRefId = race.Id,
                                EitherOr_ForSameEntity = false,
                                ExcludeFlag = false
                            };
                            incentiveVsEntityList.Add(newincVsEntity);
                        }

                        CreateOrUpdateIncentiveTagInput inputTagDto = new CreateOrUpdateIncentiveTagInput
                        {
                            IncentiveTag = sgcpfTag,
                            IncentiveVsEntityList = incentiveVsEntityList
                        };
                        await CreateOrUpdateIncentiveTag(inputTagDto);
                    }
                }
                #endregion


            }

            if (countryName.ToUpper().Equals("IN"))
            {
                bool isDeductableFromSalary = false;
                #region IN PF - Employer PF Contribution - Paid By Company 
                {
                    IncentiveTag incTag = new IncentiveTag(AbpSession.TenantId.Value, (int)IncentivePayMode.Monthly, (int)AllowanceOrDeductionMode.Statutory_Allowance, "STATUTORY_ALLOWANCE", 0, "PF-Employer Contribution", "PF-Employer Contribution", "PF-Employer Contribution", (int)IncentiveEntryMode.AUTOMATIC, true, true, (int)SpecialFormulaTagReference.IN_Employer_PF, 0, 0, true, isDeductableFromSalary);
                    incTag.IncentiveCategoryRefId = statutoryAllowanceCategoryDto.Id;

                    var inErPF = await _incentivetagRepo.FirstOrDefaultAsync(t => t.IncentiveTagCode.ToUpper().Equals(incTag.IncentiveTagCode.ToUpper()));
                    if (inErPF == null)
                    {
                        IncentiveTagEditDto sgcpfTag = new IncentiveTagEditDto
                        {
                            IncentivePayModeId = incTag.IncentivePayModeId,
                            AllowanceOrDeductionRefId = incTag.AllowanceOrDeductionRefId,
                            IncentiveCategoryRefId = incTag.IncentiveCategoryRefId,
                            IncentiveTagCode = incTag.IncentiveTagCode,
                            IncentiveTagName = incTag.IncentiveTagName,
                            IncentiveDescription = incTag.IncentiveDescription,
                            IncentiveEntryMode = incTag.IncentiveEntryMode,
                            ActiveStatus = incTag.ActiveStatus,
                            AnySpecialFormulaRefId = incTag.AnySpecialFormulaRefId,
                            FormulaRefId = incTag.FormulaRefId,
                            CalculationFormulaMinimumValue = incTag.CalculationFormulaMinimumValue,
                            CalculationFormulaMaximumValue = incTag.CalculationFormulaMaximumValue,
                            IsSytemGeneratedIncentive = true,
                            IsItDeductableFromGrossSalary = incTag.IsItDeductableFromGrossSalary
                        };

                        List<IncentiveVsEntityListDto> incentiveVsEntityList = new List<IncentiveVsEntityListDto>();
                        CreateOrUpdateIncentiveTagInput inputTagDto = new CreateOrUpdateIncentiveTagInput
                        {
                            IncentiveTag = sgcpfTag,
                            IncentiveVsEntityList = incentiveVsEntityList
                        };
                        await CreateOrUpdateIncentiveTag(inputTagDto);
                    }
                }
                #endregion

                isDeductableFromSalary = false;
                #region IN PF - Employer Pension Contribution - Paid By Company 
                {
                    IncentiveTag incTag = new IncentiveTag(AbpSession.TenantId.Value, (int)IncentivePayMode.Monthly, (int)AllowanceOrDeductionMode.Statutory_Allowance, "STATUTORY_ALLOWANCE", 0, "Pension-Employer Contribution", "Pension-Employer Contribution", "Pension-Employer Contribution", (int)IncentiveEntryMode.AUTOMATIC, true, true, (int)SpecialFormulaTagReference.IN_Employer_PENSION, 0, 0, true, isDeductableFromSalary);
                    incTag.IncentiveCategoryRefId = statutoryAllowanceCategoryDto.Id;

                    var inErPension = await _incentivetagRepo.FirstOrDefaultAsync(t => t.IncentiveTagCode.ToUpper().Equals(incTag.IncentiveTagCode.ToUpper()));
                    if (inErPension == null)
                    {
                        IncentiveTagEditDto sgcpfTag = new IncentiveTagEditDto
                        {
                            IncentivePayModeId = incTag.IncentivePayModeId,
                            AllowanceOrDeductionRefId = incTag.AllowanceOrDeductionRefId,
                            IncentiveCategoryRefId = incTag.IncentiveCategoryRefId,
                            IncentiveTagCode = incTag.IncentiveTagCode,
                            IncentiveTagName = incTag.IncentiveTagName,
                            IncentiveDescription = incTag.IncentiveDescription,
                            IncentiveEntryMode = incTag.IncentiveEntryMode,
                            ActiveStatus = incTag.ActiveStatus,
                            AnySpecialFormulaRefId = incTag.AnySpecialFormulaRefId,
                            FormulaRefId = incTag.FormulaRefId,
                            CalculationFormulaMinimumValue = incTag.CalculationFormulaMinimumValue,
                            CalculationFormulaMaximumValue = incTag.CalculationFormulaMaximumValue,
                            IsSytemGeneratedIncentive = true,
                            IsItDeductableFromGrossSalary = incTag.IsItDeductableFromGrossSalary
                        };

                        List<IncentiveVsEntityListDto> incentiveVsEntityList = new List<IncentiveVsEntityListDto>();
                        CreateOrUpdateIncentiveTagInput inputTagDto = new CreateOrUpdateIncentiveTagInput
                        {
                            IncentiveTag = sgcpfTag,
                            IncentiveVsEntityList = incentiveVsEntityList
                        };
                        await CreateOrUpdateIncentiveTag(inputTagDto);
                    }
                }
                #endregion

                isDeductableFromSalary = true;
                #region IN PF - Employee Contribution - Deducted From Salary
                {
                    var incTag = new IncentiveTag(AbpSession.TenantId.Value, (int)IncentivePayMode.Monthly, (int)AllowanceOrDeductionMode.Statutory_Deduction, "STATUTORY_DEDUCTION", 0, "PF-Employee Contribution", "PF-Employee Contribution", "PF-Employee Contribution", (int)IncentiveEntryMode.AUTOMATIC, true, true, (int)SpecialFormulaTagReference.IN_Employee_PF, 0, 0, true, isDeductableFromSalary);
                    incTag.IncentiveCategoryRefId = statutoryDeductionCategoryDto.Id;

                    var inEePF = await _incentivetagRepo.FirstOrDefaultAsync(t => t.IncentiveTagCode.ToUpper().Equals(incTag.IncentiveTagCode.ToUpper()));
                    if (inEePF == null)
                    {
                        IncentiveTagEditDto sgcpfTag = new IncentiveTagEditDto
                        {
                            IncentivePayModeId = incTag.IncentivePayModeId,
                            AllowanceOrDeductionRefId = incTag.AllowanceOrDeductionRefId,
                            IncentiveCategoryRefId = incTag.IncentiveCategoryRefId,
                            IncentiveTagCode = incTag.IncentiveTagCode,
                            IncentiveTagName = incTag.IncentiveTagName,
                            IncentiveDescription = incTag.IncentiveDescription,
                            IncentiveEntryMode = incTag.IncentiveEntryMode,
                            ActiveStatus = incTag.ActiveStatus,
                            AnySpecialFormulaRefId = incTag.AnySpecialFormulaRefId,
                            FormulaRefId = incTag.FormulaRefId,
                            CalculationFormulaMinimumValue = incTag.CalculationFormulaMinimumValue,
                            CalculationFormulaMaximumValue = incTag.CalculationFormulaMaximumValue,
                            IsSytemGeneratedIncentive = true,
                            IsItDeductableFromGrossSalary = incTag.IsItDeductableFromGrossSalary
                        };

                        List<IncentiveVsEntityListDto> incentiveVsEntityList = new List<IncentiveVsEntityListDto>();

                        CreateOrUpdateIncentiveTagInput inputTagDto = new CreateOrUpdateIncentiveTagInput
                        {
                            IncentiveTag = sgcpfTag,
                            IncentiveVsEntityList = incentiveVsEntityList
                        };
                        await CreateOrUpdateIncentiveTag(inputTagDto);
                    }
                }
                #endregion

                isDeductableFromSalary = false;
                #region IN ESI ER Fund  - Deducted From Salary
                {
                    var incTag = new IncentiveTag(AbpSession.TenantId.Value, (int)IncentivePayMode.Monthly, (int)AllowanceOrDeductionMode.Statutory_Deduction, "STATUTORY_DEDUCTION", 0, "ESI Employer Contribution", "ESI Employer Contribution", "ESI Employer Contribution", (int)IncentiveEntryMode.AUTOMATIC, true, true, (int)SpecialFormulaTagReference.IN_ESI_Employer, 0, 0, true, isDeductableFromSalary);
                    incTag.IncentiveCategoryRefId = statutoryAllowanceCategoryDto.Id;

                    var esiErTag = await _incentivetagRepo.FirstOrDefaultAsync(t => t.IncentiveTagCode.ToUpper().Equals(incTag.IncentiveTagCode.ToUpper()));
                    if (esiErTag == null)
                    {
                        IncentiveTagEditDto sgcpfTag = new IncentiveTagEditDto
                        {
                            IncentivePayModeId = incTag.IncentivePayModeId,
                            AllowanceOrDeductionRefId = incTag.AllowanceOrDeductionRefId,
                            IncentiveCategoryRefId = incTag.IncentiveCategoryRefId,
                            IncentiveTagCode = incTag.IncentiveTagCode,
                            IncentiveTagName = incTag.IncentiveTagName,
                            IncentiveDescription = incTag.IncentiveDescription,
                            IncentiveEntryMode = incTag.IncentiveEntryMode,
                            ActiveStatus = incTag.ActiveStatus,
                            AnySpecialFormulaRefId = incTag.AnySpecialFormulaRefId,
                            FormulaRefId = incTag.FormulaRefId,
                            CalculationFormulaMinimumValue = incTag.CalculationFormulaMinimumValue,
                            CalculationFormulaMaximumValue = incTag.CalculationFormulaMaximumValue,
                            IsSytemGeneratedIncentive = true,
                            IsItDeductableFromGrossSalary = incTag.IsItDeductableFromGrossSalary
                        };

                        List<IncentiveVsEntityListDto> incentiveVsEntityList = new List<IncentiveVsEntityListDto>();


                        CreateOrUpdateIncentiveTagInput inputTagDto = new CreateOrUpdateIncentiveTagInput
                        {
                            IncentiveTag = sgcpfTag,
                            IncentiveVsEntityList = incentiveVsEntityList
                        };
                        await CreateOrUpdateIncentiveTag(inputTagDto);
                    }

                }
                #endregion

                isDeductableFromSalary = true;
                #region IN ESI EE Fund  - Deducted From Salary
                {
                    var incTag = new IncentiveTag(AbpSession.TenantId.Value, (int)IncentivePayMode.Monthly, (int)AllowanceOrDeductionMode.Statutory_Deduction, "STATUTORY_DEDUCTION", 0, "ESI Employee Contribution", "ESI Employee Contribution", "ESI Employee Contribution", (int)IncentiveEntryMode.AUTOMATIC, true, true, (int)SpecialFormulaTagReference.IN_ESI_Employee, 0, 0, true, isDeductableFromSalary);
                    incTag.IncentiveCategoryRefId = statutoryDeductionCategoryDto.Id;

                    var esiErTag = await _incentivetagRepo.FirstOrDefaultAsync(t => t.IncentiveTagCode.ToUpper().Equals(incTag.IncentiveTagCode.ToUpper()));
                    if (esiErTag == null)
                    {
                        IncentiveTagEditDto sgcpfTag = new IncentiveTagEditDto
                        {
                            IncentivePayModeId = incTag.IncentivePayModeId,
                            AllowanceOrDeductionRefId = incTag.AllowanceOrDeductionRefId,
                            IncentiveCategoryRefId = incTag.IncentiveCategoryRefId,
                            IncentiveTagCode = incTag.IncentiveTagCode,
                            IncentiveTagName = incTag.IncentiveTagName,
                            IncentiveDescription = incTag.IncentiveDescription,
                            IncentiveEntryMode = incTag.IncentiveEntryMode,
                            ActiveStatus = incTag.ActiveStatus,
                            AnySpecialFormulaRefId = incTag.AnySpecialFormulaRefId,
                            FormulaRefId = incTag.FormulaRefId,
                            CalculationFormulaMinimumValue = incTag.CalculationFormulaMinimumValue,
                            CalculationFormulaMaximumValue = incTag.CalculationFormulaMaximumValue,
                            IsSytemGeneratedIncentive = true,
                            IsItDeductableFromGrossSalary = incTag.IsItDeductableFromGrossSalary
                        };

                        List<IncentiveVsEntityListDto> incentiveVsEntityList = new List<IncentiveVsEntityListDto>();


                        CreateOrUpdateIncentiveTagInput inputTagDto = new CreateOrUpdateIncentiveTagInput
                        {
                            IncentiveTag = sgcpfTag,
                            IncentiveVsEntityList = incentiveVsEntityList
                        };
                        await CreateOrUpdateIncentiveTag(inputTagDto);
                    }

                }
                #endregion

                isDeductableFromSalary = true;
                #region IN Labour Welfare Fund  - Deducted From Salary
                {
                    var incTag = new IncentiveTag(AbpSession.TenantId.Value, (int)IncentivePayMode.Monthly, (int)AllowanceOrDeductionMode.Statutory_Deduction, "STATUTORY_DEDUCTION", 0, "Labour Welfare Fund", "Labour Welfare Fund", "Labour Welfare Fund", (int)IncentiveEntryMode.AUTOMATIC, true, true, (int)SpecialFormulaTagReference.IN_LABOUR_WELFARE_FUND, 0, 0, true, isDeductableFromSalary);
                    incTag.IncentiveCategoryRefId = statutoryDeductionCategoryDto.Id;

                    var esiErTag = await _incentivetagRepo.FirstOrDefaultAsync(t => t.IncentiveTagCode.ToUpper().Equals(incTag.IncentiveTagCode.ToUpper()));
                    if (esiErTag == null)
                    {
                        IncentiveTagEditDto sgcpfTag = new IncentiveTagEditDto
                        {
                            IncentivePayModeId = incTag.IncentivePayModeId,
                            AllowanceOrDeductionRefId = incTag.AllowanceOrDeductionRefId,
                            IncentiveCategoryRefId = incTag.IncentiveCategoryRefId,
                            IncentiveTagCode = incTag.IncentiveTagCode,
                            IncentiveTagName = incTag.IncentiveTagName,
                            IncentiveDescription = incTag.IncentiveDescription,
                            IncentiveEntryMode = incTag.IncentiveEntryMode,
                            ActiveStatus = incTag.ActiveStatus,
                            AnySpecialFormulaRefId = incTag.AnySpecialFormulaRefId,
                            FormulaRefId = incTag.FormulaRefId,
                            CalculationFormulaMinimumValue = incTag.CalculationFormulaMinimumValue,
                            CalculationFormulaMaximumValue = incTag.CalculationFormulaMaximumValue,
                            IsSytemGeneratedIncentive = true,
                            IsItDeductableFromGrossSalary = incTag.IsItDeductableFromGrossSalary
                        };

                        List<IncentiveVsEntityListDto> incentiveVsEntityList = new List<IncentiveVsEntityListDto>();


                        CreateOrUpdateIncentiveTagInput inputTagDto = new CreateOrUpdateIncentiveTagInput
                        {
                            IncentiveTag = sgcpfTag,
                            IncentiveVsEntityList = incentiveVsEntityList
                        };
                        await CreateOrUpdateIncentiveTag(inputTagDto);
                    }

                }
                #endregion

                isDeductableFromSalary = true;
                #region IN Professional Tax - Deducted From Salary
                {
                    var incTag = new IncentiveTag(AbpSession.TenantId.Value, (int)IncentivePayMode.Monthly, (int)AllowanceOrDeductionMode.Statutory_Deduction, "STATUTORY_DEDUCTION", 0, "Professional Tax", "Professional Tax", "Professional Tax", (int)IncentiveEntryMode.AUTOMATIC, true, true, (int)SpecialFormulaTagReference.IN_PROFESSIONAL_TAX, 0, 0, true, isDeductableFromSalary);
                    incTag.IncentiveCategoryRefId = statutoryDeductionCategoryDto.Id;

                    var esiErTag = await _incentivetagRepo.FirstOrDefaultAsync(t => t.IncentiveTagCode.ToUpper().Equals(incTag.IncentiveTagCode.ToUpper()));
                    if (esiErTag == null)
                    {
                        IncentiveTagEditDto sgcpfTag = new IncentiveTagEditDto
                        {
                            IncentivePayModeId = incTag.IncentivePayModeId,
                            AllowanceOrDeductionRefId = incTag.AllowanceOrDeductionRefId,
                            IncentiveCategoryRefId = incTag.IncentiveCategoryRefId,
                            IncentiveTagCode = incTag.IncentiveTagCode,
                            IncentiveTagName = incTag.IncentiveTagName,
                            IncentiveDescription = incTag.IncentiveDescription,
                            IncentiveEntryMode = incTag.IncentiveEntryMode,
                            ActiveStatus = incTag.ActiveStatus,
                            AnySpecialFormulaRefId = incTag.AnySpecialFormulaRefId,
                            FormulaRefId = incTag.FormulaRefId,
                            CalculationFormulaMinimumValue = incTag.CalculationFormulaMinimumValue,
                            CalculationFormulaMaximumValue = incTag.CalculationFormulaMaximumValue,
                            IsSytemGeneratedIncentive = true,
                            IsItDeductableFromGrossSalary = incTag.IsItDeductableFromGrossSalary
                        };

                        List<IncentiveVsEntityListDto> incentiveVsEntityList = new List<IncentiveVsEntityListDto>();


                        CreateOrUpdateIncentiveTagInput inputTagDto = new CreateOrUpdateIncentiveTagInput
                        {
                            IncentiveTag = sgcpfTag,
                            IncentiveVsEntityList = incentiveVsEntityList
                        };
                        await CreateOrUpdateIncentiveTag(inputTagDto);
                    }

                }
                #endregion

            }
        }
    }
}