﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Hr.Master.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Hr;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Hr.Report.Dto;

namespace DinePlan.DineConnect.Hr.Master
{
    public interface IPersonalInformationAppService : IApplicationService
    {
        Task<PagedResultOutput<PersonalInformationListDto>> GetAll(GetPersonalInformationInput inputDto);
        Task<FileDto> GetAllToExcel(GetPersonalInformationInput inputDto);
        Task<GetPersonalInformationForEditOutput> GetPersonalInformationForEdit(NullableIdInput nullableIdInput);
        Task<IdInput> CreateOrUpdatePersonalInformation(CreateOrUpdatePersonalInformationInput input);
        Task DeletePersonalInformation(IdInput input);
        Task<ListResultOutput<PersonalInformationListDto>> GetEmployeeNames();
        Task<List<EmployeeWithSkillSetViewDto>> GetEmployeeWithSkillSetForCombobox();
        Task<List<EmployeeWithSkillSetViewDto>> GetEmployeeWithSkillSetForCombobox(NullableIdInput input);
        Task<List<EmployeeWithSkillSetViewDto>> GetParticularEmployeeWithSkillSetForCombobox(IdInput input);

        Task<List<EmployeeBasicDetails>> GetEmployeeBasicDetails();
        Task<IdInput> GetUserSerialNumberMax();
        Task<PagedResultOutput<PersonalInformationListDto>> GetStaffTypeForSelectedEmp(GetPersonalInformationInput input);
        Task<ListResultOutput<ComboboxItemDto>> GetEmployeeForCombobox();
        Task<ListResultOutput<ComboboxItemDto>> GetEmployeeCodeForCombobox();
        Task<ListResultOutput<ComboboxItemDto>> GetCompanyForCombobox();
        Task<ListResultOutput<ComboboxItemDto>> GetCompanyCodeForCombobox();
        Task<ListResultOutput<ComboboxItemDto>> GetSkillSetForCombobox();
        Task<EmployeeMaximumDetails> GetMaximumEmpCodeAndBioMetricNumber(GetEmployeeMaximumInput input);
        Task<User> RegisterUser(RegisterUserDto model);
        Task<IdInput> UpdateResignationPersonalInformation(CreateOrUpdatePersonalInformationInput input);

        Task<ListResultOutput<ComboboxItemDto>> GetActiveEmployeeForCombobox();
        Task<MessageOutput> GetEmployeeStatus(GetEmployeeStatusDto input);
        Task<List<WeekDaysDto>> GetWeekDaysForCombobox();
        Task<PagedResultOutput<PersonalInformationListDto>> GetEmployeeBasedOnEmployeeCode(GetPersonalInformationInput inputDto);
        Task<IdInput> UpdateGps(GpsAttendance input);
        Task<IdInput> UpdateGpsLive(GpsLive input);
        Task<MessageOutput> SendPassWordForSpecialOperation(EmailInput input);
        Task<PersonalInformationListDto> GetEmployeeDetailsBasedOnUserName(UserNameInput input);
        List<LocalPermissionDto> GetPermissionsForSmartAttendance(UserNameInput input);
       // Task<List<EmployeeWithSkillSetViewDto>> GetIncentiveAllowedEmployeeWithSkillSetForCombobox();
        Task<List<GpsAttendanceOutputDto>> GetGpsAttendanceList(GetEmployeeDashBoardDto input);
        Task<List<GpsAttendanceOutputDto>> GetGpsLiveList(GetEmployeeDashBoardDto input);
        Task<string> GetAndroidMobileVersion();
        Task<string> GetIosMobileVersion();
               
        Task UpdateFixedCostCentreInDutyChart(PersonalInformationEditDto input);
        Task<IdInput> UpdateFixedCostCentre(PersonalInformationListDto input);

        Task<ListResultOutput<SimplePersonalInformationListDto>> GetSimpleEmployeeDetails();

        Task<SimplePersonalInformationListDto> GetEmployeeDetailsBasedOnEmployeeRefId(EmployeeIdInput input);
        Task<string> GetSkillSetListOfGivenEmployee(EmployeeIdInput input);
        Task<ListResultOutput<SimplePersonalInformationListDto>> GetSimpleInchargeEmployeeDetails();

        Task<ListResultOutput<SimplePersonalInformationListDto>> GetSimpleSupervisorEmployeeDetails();
        Task<string> GetCompanyNameById(IdInput input);

        Task<ListResultOutput<ComboboxItemDto>> GetCostCentreMasterList();
        Task<ListResultOutput<ComboboxItemDto>> GetDepartmentMasterList();
        Task<ListResultOutput<ComboboxItemDto>> GetResidentStatusList();
        Task<ListResultOutput<ComboboxItemDto>> GetRacesStatusList();
        Task<ListResultOutput<ComboboxItemDto>> GetReligionList();
        Task ImportPersonalInformationSampleList(IdInput locinput);

        Task<EmployeeRawOutput> ApiUpdateRawInfo(EmployeeRawInput input);
        Task<EmployeeRawOutput> ApiGetEmployeeRawInfo(EmployeeRawInput input);
        Task<ListResultOutput<JobTitleMasterEditDto>> GetJobTitleDetails();

    }
}