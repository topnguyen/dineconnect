﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.Hr.Impl;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Hr.Master.Implementation
{
    public class SkillSetAppService : DineConnectAppServiceBase, ISkillSetAppService
    {

        private readonly ISkillSetListExcelExporter _skillsetExporter;
        private readonly ISkillSetManager _skillsetManager;
        private readonly IRepository<SkillSet> _skillsetRepo;
        private readonly IRepository<EmployeeSkillSet> _employeeSkillSetRepo;

        public SkillSetAppService(ISkillSetManager skillsetManager,
            IRepository<SkillSet> skillSetRepo,
            ISkillSetListExcelExporter skillsetExporter,
            IRepository<EmployeeSkillSet> employeeSkillSetRepo)
        {
            _skillsetManager = skillsetManager;
            _skillsetRepo = skillSetRepo;
            _skillsetExporter = skillsetExporter;
            _employeeSkillSetRepo = employeeSkillSetRepo;
        }

        public async Task<PagedResultOutput<SkillSetListDto>> GetAll(GetSkillSetInput input)
        {
            var lstWithNdtProcedure = (from sst in _skillsetRepo.GetAll()
                                       select new SkillSetListDto()
                                       {
                                           Id = sst.Id,
                                           SkillCode = sst.SkillCode,
                                           Description = sst.Description,
                                           IsActive = sst.IsActive,
                                           CreationTime = sst.CreationTime
                                       }).WhereIf(
                             !input.Filter.IsNullOrEmpty(),
                             p => p.SkillCode.Contains(input.Filter)
             );

            var allItems = lstWithNdtProcedure;

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<SkillSetListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<SkillSetListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _skillsetRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<SkillSetListDto>>();
            return _skillsetExporter.ExportToFile(allListDtos);
        }

        public async Task<GetSkillSetForEditOutput> GetSkillSetForEdit(NullableIdInput input)
        {
            SkillSetEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _skillsetRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<SkillSetEditDto>();
            }
            else
            {
                editDto = new SkillSetEditDto();
            }

            return new GetSkillSetForEditOutput
            {
                SkillSet = editDto
            };
        }

        public async Task CreateOrUpdateSkillSet(CreateOrUpdateSkillSetInput input)
        {
            if (input.SkillSet.Id.HasValue)
            {
                await UpdateSkillSet(input);
            }
            else
            {
                await CreateSkillSet(input);
            }
        }

        public async Task DeleteSkillSet(IdInput input)
        {
            var exists = await _employeeSkillSetRepo.FirstOrDefaultAsync(t => t.SkillSetRefId == input.Id);
            if (exists!=null)
            {
                throw new UserFriendlyException(L("ReferenceExists", L("Skill"), L("Employee")));
            }

            await _skillsetRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateSkillSet(CreateOrUpdateSkillSetInput input)
        {
            var item = await _skillsetRepo.GetAsync(input.SkillSet.Id.Value);
            var dto = input.SkillSet;

            //TODO: SERVICE SkillSet Update Individually
            item.SkillCode = dto.SkillCode;
            item.Description = dto.Description;
            item.IsActive = dto.IsActive;

            CheckErrors(await _skillsetManager.CreateSync(item));
        }

        protected virtual async Task CreateSkillSet(CreateOrUpdateSkillSetInput input)
        {
            var dto = input.SkillSet.MapTo<SkillSet>();
            //@@Pending need to change in Cshtml and delete the below line
            dto.IsActive = true;
            CheckErrors(await _skillsetManager.CreateSync(dto));
        }

        public async Task<ListResultOutput<SkillSetListDto>> GetCodes()
        {
            var lstSkillSet = await _skillsetRepo.GetAll().ToListAsync();
            return new ListResultOutput<SkillSetListDto>(lstSkillSet.MapTo<List<SkillSetListDto>>());
        }


    }
}
