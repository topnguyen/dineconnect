﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using DinePlan.DineConnect.House.Master.Dtos;

namespace DinePlan.DineConnect.Hr.Master
{
    public interface IEmployeeDocumentInfoAppService : IApplicationService
    {
        Task<PagedResultOutput<EmployeeDocumentInfoListDto>> GetAll(GetEmployeeDocumentInfoInput inputDto);
        Task<FileDto> GetAllToExcel(GetEmployeeDocumentInfoInput input);
        Task<GetEmployeeDocumentInfoForEditOutput> GetEmployeeDocumentInfoForEdit(NullableIdInput nullableIdInput);
        Task<IdInput> CreateOrUpdateEmployeeDocumentInfo(CreateOrUpdateEmployeeDocumentInfoInput input);
        Task DeleteEmployeeDocumentInfo(IdInput input);

        Task<ListResultOutput<ComboboxItemDto>> GetDocumentInfoForCombobox();
        Task<List<DocumentInfoEditDto>> GetDocumentInfo();

        Task<bool> EmployeeDocumentAlreadyExist(DocumentDto input);
        //Task<List<HealthOfOrganisationListDto>> GetHealthOfEmployeeDocumentInfo(HealthDateInputDto input);

        Task<HealthDocumentOutput> GetPendingDocumentInfo(HealthDocumentDto input);
        Task<FileDto> GetPendingDocumentInfoIntoExcel(HealthDocumentDto input);
        Task<MessageOutput> SendEmailDocumentInfo(IdInput input);

    }
}