﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Hr.Master
{
    public interface IDcStationMasterAppService : IApplicationService
    {
        Task<PagedResultOutput<DcStationMasterListDto>> GetAll(GetDcStationMasterInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetDcStationMasterForEditOutput> GetDcStationMasterForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateDcStationMaster(CreateOrUpdateDcStationMasterInput input);
        Task DeleteDcStationMaster(IdInput input);

        Task<ListResultOutput<DcStationMasterListDto>> GetStationNames();
        Task<ListResultOutput<DcStationMasterListDto>> GetStationNames(GroupCodeDto input);
        Task<ListResultOutput<DcStationMasterListDto>> GetStationNames(LocationGroupCodeDto input);

        Task<int> GetMaxUserSerialNumber();

    }
}
