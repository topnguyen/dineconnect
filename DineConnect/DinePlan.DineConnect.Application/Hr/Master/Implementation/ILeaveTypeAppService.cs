﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Hr.Transaction
{
    public interface ILeaveTypeAppService : IApplicationService
    {
        Task<PagedResultOutput<LeaveTypeListDto>> GetAll(GetLeaveTypeInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetLeaveTypeForEditOutput> GetLeaveTypeForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateLeaveType(CreateOrUpdateLeaveTypeInput input);
        Task DeleteLeaveType(IdInput input);

        Task<ListResultOutput<LeaveTypeListDto>> GetLeaveTypes();
    }
}