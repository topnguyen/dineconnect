﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.Hr.Impl;
using System;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using DinePlan.DineConnect.Hr;
using System.Reflection;
using System.Diagnostics;

namespace DinePlan.DineConnect.Hr.Master.Implementation
{
    public class AttendanceLogAppService : DineConnectAppServiceBase, IAttendanceLogAppService
    {

        private readonly IAttendanceLogListExcelExporter _attendancelogExporter;
        private readonly IAttendanceLogManager _attendancelogManager;
        private readonly IRepository<AttendanceLog> _attendancelogRepo;
        private readonly IRepository<PersonalInformation> _personalInformationRepo;
        private readonly IRepository<AttendanceMachineLocation> _attendanceMachineLocationRepo;
        private readonly IRepository<DutyChartDetail> _dutychartDetailRepo;
        private readonly IRepository<GpsAttendance> _gpsAttendanceListRepo;
        private readonly IRepository<SalaryInfo> _salaryInfoRepo;
        private readonly ISalaryInfoAppService _salaryinfoAppService;
        private readonly IRepository<MonthWiseWorkDay> _monthwiseWorkDayRepo;


        public AttendanceLogAppService(IAttendanceLogManager attendancelogManager,
            IRepository<AttendanceLog> attendanceLogRepo,
            IAttendanceLogListExcelExporter attendancelogExporter,
            IRepository<PersonalInformation> personalInformationRepo,
            IRepository<AttendanceMachineLocation> attendanceMachineLocationRepo,
            IRepository<DutyChartDetail> dutychartDetailRepo,
            IRepository<GpsAttendance> gpsAttendanceListRepo,
            IRepository<SalaryInfo> salaryInfoRepo,
            ISalaryInfoAppService salaryinfoAppService,
            IRepository<MonthWiseWorkDay> monthwiseWorkDayRepo)
        {
            _attendancelogManager = attendancelogManager;
            _attendancelogRepo = attendanceLogRepo;
            _attendancelogExporter = attendancelogExporter;
            _personalInformationRepo = personalInformationRepo;
            _attendanceMachineLocationRepo = attendanceMachineLocationRepo;
            _dutychartDetailRepo = dutychartDetailRepo;
            _gpsAttendanceListRepo = gpsAttendanceListRepo;
            _salaryInfoRepo = salaryInfoRepo;
            _salaryinfoAppService = salaryinfoAppService;
            _monthwiseWorkDayRepo = monthwiseWorkDayRepo;
        }

        public async Task<PagedResultOutput<AttendanceLogListDto>> GetAll(GetAttendanceLogInput input)
        {
            var allItems = (from attLog in _attendancelogRepo.GetAll()
                            join perInfo in _personalInformationRepo.GetAll().WhereIf(!input.Filter.IsNullOrEmpty(), t => t.EmployeeName.Contains(input.Filter))
                            on attLog.EmployeeRefId equals perInfo.Id
                            join attMachines in _attendanceMachineLocationRepo.GetAll() on attLog.AttendanceMachineLocationRefId equals attMachines.Id into joinedAttLog
                            from attMachines in joinedAttLog.DefaultIfEmpty()
                            select new AttendanceLogListDto
                            {
                                Id = attLog.Id,
                                AuthorisedBy = attLog.AuthorisedBy,
                                BioMetricCode = perInfo.BioMetricCode,
                                EmployeeRefId = attLog.EmployeeRefId,
                                EmployeeRefCode = perInfo.EmployeeCode,
                                EmployeeRefName = perInfo.EmployeeName,
                                CheckTime = attLog.CheckTime,
                                CheckTypeCode = attLog.CheckTypeCode,
                                VerifyMode = attLog.VerifyMode,
                                CreationTime = attLog.CreationTime,
                                AttendanceMachineLocationRefId = attLog.AttendanceMachineLocationRefId,
                                AttendanceMachineLocationRefName = attMachines == null ? string.Empty : attMachines.MachinePrefix
                            }).OrderByDescending(t => t.CheckTime);

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<AttendanceLogListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<AttendanceLogListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<PagedResultOutput<AttendanceLogListDto>> GetAllForIndex(GetAttendanceLogInput input)
        {

            DateTime? FromDate, ToDate;
            FromDate = input.StartDate;
            ToDate = input.EndDate;

            IQueryable<AttendanceLog> tsDetail;
            IQueryable<GpsAttendance> gpsRsDetail;
            if (input.EmployeeRefId == 0)
            {
                if (FromDate == null)
                    FromDate = DateTime.Now.AddDays(-7);

                if (ToDate == null)
                    ToDate = DateTime.Now;
                else
                    ToDate = ToDate.Value.AddMinutes(1439);

                tsDetail = _attendancelogRepo.GetAll().Where(t => t.CheckTime >= FromDate && t.CheckTime <= ToDate);
                gpsRsDetail = _gpsAttendanceListRepo.GetAll().Where(t => t.CreationTime >= FromDate && t.CreationTime <= ToDate);
            }
            else
            {
                if (FromDate == null)
                    FromDate = DateTime.Now.AddDays(-31);

                if (ToDate == null)
                    ToDate = DateTime.Now;
                else
                    ToDate = ToDate.Value.AddMinutes(1439);

                tsDetail = _attendancelogRepo.GetAll().Where(t => t.CheckTime >= FromDate && t.CheckTime <= ToDate && t.EmployeeRefId == input.EmployeeRefId);
                gpsRsDetail = _gpsAttendanceListRepo.GetAll().Where(t => t.CreationTime >= FromDate && t.CreationTime <= ToDate && t.EmployeeRefId == input.EmployeeRefId);
            }

            var allItems = (from attLog in tsDetail
                            join personal in _personalInformationRepo.GetAll().WhereIf(input.EmployeeRefId > 0, t => t.Id == input.EmployeeRefId)
                            on attLog.EmployeeRefId equals personal.Id
                            join attMachines in _attendanceMachineLocationRepo.GetAll() on attLog.AttendanceMachineLocationRefId equals attMachines.Id into joinedAttLog
                            from attMachines in joinedAttLog.DefaultIfEmpty()
                            select new AttendanceLogListDto
                            {
                                Id = attLog.Id,
                                BioMetricCode = personal.BioMetricCode,
                                EmployeeRefId = attLog.EmployeeRefId,
                                EmployeeRefCode = personal.EmployeeCode,
                                EmployeeRefName = personal.EmployeeName,
                                CheckTime = attLog.CheckTime,
                                VerifyMode = attLog.VerifyMode,
                                AttendanceMachineLocationRefId = attLog.AttendanceMachineLocationRefId,
                                AttendanceMachineLocationRefName = attMachines == null ? string.Empty : attMachines.MachinePrefix
                            }).OrderByDescending(t => t.CheckTime);

            if (input.Sorting.IsNullOrEmpty())
                input.Sorting = "CheckTime Desc";

            var attList = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            #region GpsAttendance
            var allGpsItems = (from gpsDto in gpsRsDetail
                               join personal in _personalInformationRepo.GetAll().WhereIf(input.EmployeeRefId > 0, t => t.Id == input.EmployeeRefId)
                               on gpsDto.EmployeeRefId equals personal.Id
                               select new AttendanceLogListDto
                               {
                                   Id = gpsDto.Id,
                                   BioMetricCode = personal.BioMetricCode ,
                                   EmployeeRefId = gpsDto.EmployeeRefId,
                                   EmployeeRefCode = personal.EmployeeCode,
                                   EmployeeRefName = personal.EmployeeName,
                                   CheckTime = gpsDto.CreationTime,
                                   CheckTypeCode = gpsDto.AttendanceStatus == (int)AttendanceStatus.IN ? "TI" : gpsDto.AttendanceStatus == (int)AttendanceStatus.OUT ? "TO" : gpsDto.AttendanceStatus == (int)AttendanceStatus.BREAKIN ? "BI" : gpsDto.AttendanceStatus == (int)AttendanceStatus.BREAKOUT ? "BO" : gpsDto.AttendanceStatus == (int)AttendanceStatus.LIVE ? "LI" : "EX",
                                   VerifyMode = 0,
                                   AttendanceStatus = "Gps", // (gpsDto.AttendanceStatus == 0) ? "Finger" : "Manual",
                                   AttendanceMachineLocationRefId = 11, //attLog.AttendanceMachineLocationRefId,
                                   AttendanceMachineLocationRefName = "Gps",// attMachines == null ? string.Empty : attMachines.MachinePrefix
                                   TenantId = personal.TenantId
                               }).OrderByDescending(t => t.CheckTime);

            if (input.Sorting.IsNullOrEmpty())
                input.Sorting = "CheckTime Desc";

            var allGpsList = await allGpsItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            if (allGpsList.Count > 0)
            {
                var tenant = await TenantManager.GetByIdAsync((int)allGpsList[0].TenantId);
                string tenantName = tenant.Name.ToUpper();

                if (tenantName.ToUpper().Equals("INDIA"))
                {
                    TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                    foreach (var lst in allGpsList)
                    {
                        var UTCTime = lst.CheckTime.Value.ToUniversalTime();
                        var ServerTime = lst.CheckTime;
                        lst.CheckTime = TimeZoneInfo.ConvertTimeFromUtc(lst.CheckTime.Value.ToUniversalTime(), tzi);
                    }
                }
            }
            #endregion
            List<AttendanceLogListDto> output = new List<AttendanceLogListDto>();
            output.AddRange(allGpsList);
            output.AddRange(attList);
            output = output.OrderBy(input.Sorting).ToList();

            var allItemCount = await allItems.CountAsync();
            return new PagedResultOutput<AttendanceLogListDto>(
                allItemCount,
                output
                );
        }

        public async Task<GetAttendanceLogForEditOutput> GetAttendanceLogForEdit(NullableIdInput input)
        {
            AttendanceLogEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _attendancelogRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<AttendanceLogEditDto>();
            }
            else
            {
                editDto = new AttendanceLogEditDto();
            }

            return new GetAttendanceLogForEditOutput
            {
                AttendanceLog = editDto
            };
        }

        public async Task CreateOrUpdateAttendanceLog(CreateOrUpdateAttendanceLogInput input)
        {
            if (input.AttendanceLog.Id.HasValue)
            {
                await UpdateAttendanceLog(input);
            }
            else
            {
                await CreateAttendanceLog(input);
            }
        }

        public async Task DeleteAttendanceLog(DeleteAttendanceLog input)
        {
            if (input.AttMode.ToUpper().Equals("GPS"))
            {
                var exists = await _gpsAttendanceListRepo.FirstOrDefaultAsync(t => t.Id == input.Id);
                if (exists.EmployeeRefId == input.EmployeeRefId)
                {
                    await _gpsAttendanceListRepo.DeleteAsync(input.Id);
                }
                else
                {
                    throw new UserFriendlyException(L("Employee") + " ?");
                }
            }
            else
            {
                var exists = await _attendancelogRepo.FirstOrDefaultAsync(t => t.Id == input.Id);
                if (exists.EmployeeRefId == input.EmployeeRefId)
                {
                    await _attendancelogRepo.DeleteAsync(input.Id);
                }
                else
                {
                    throw new UserFriendlyException(L("Employee") + " ?");
                }
            }
        }

        protected virtual async Task UpdateAttendanceLog(CreateOrUpdateAttendanceLogInput input)
        {
            var item = await _attendancelogRepo.GetAsync(input.AttendanceLog.Id.Value);
            var dto = input.AttendanceLog;

            item.EmployeeRefId = dto.EmployeeRefId;
            item.VerifyMode = dto.VerifyMode;
            item.CheckTime = dto.CheckTime;
            item.AuthorisedBy = dto.AuthorisedBy;
            CheckErrors(await _attendancelogManager.CreateSync(item));
        }

        protected virtual async Task CreateAttendanceLog(CreateOrUpdateAttendanceLogInput input)
        {
            var dto = input.AttendanceLog.MapTo<AttendanceLog>();
            CheckErrors(await _attendancelogManager.CreateSync(dto));
        }


        public async Task<List<AttendanceParticularDateList>> GetGpsEmployeeAttendanceDateWise(GetAttendanceLogInput input)
        {
            if (!input.StartDate.HasValue)
            {
                input.StartDate = DateTime.Today.AddDays(-30);
                input.EndDate = DateTime.Now;
            }

            GetAttendanceLogInput ginput = new GetAttendanceLogInput
            {
                EmployeeRefId = input.EmployeeRefId,
                StartDate = input.StartDate,
                EndDate = input.EndDate,
                MaxResultCount = AppConsts.MaxPageSize
            };


            var rsGps = await GetAllForIndex(ginput);
            List<AttendanceLogListDto> attLogdetail = rsGps.Items.MapTo<List<AttendanceLogListDto>>();
            attLogdetail = attLogdetail.Where(t => t.CheckTime.HasValue).ToList();
            int[] arrEmployeeRefIds = attLogdetail.Select(t => t.EmployeeRefId).Distinct().ToArray();
            var rsEmps = await _personalInformationRepo.GetAllListAsync(t => arrEmployeeRefIds.Contains(t.Id));
            var dcList = await _dutychartDetailRepo.GetAllListAsync(t => arrEmployeeRefIds.Contains(t.EmployeeRefId) && t.DutyChartDate >= input.StartDate.Value && t.DutyChartDate <= input.EndDate.Value);

            List<AttendanceParticularDateList> output = new List<AttendanceParticularDateList>();

            List<DateTime> dates = new List<DateTime>();
            if (attLogdetail != null && attLogdetail.Count > 0)
                dates = attLogdetail.Select(t => t.CheckTime.Value.Date).Distinct().ToList();
            PersonalInformation emp = new PersonalInformation();
            try
            {
                foreach (var argEmp in arrEmployeeRefIds)
                {
                    emp = rsEmps.FirstOrDefault(t => t.Id == argEmp);
                    if (emp == null)
                        continue;

                    foreach (var dt in dates)
                    {
                        AttendanceParticularDateList newDto = new AttendanceParticularDateList();

                        newDto.BioMetricCode = emp.BioMetricCode;
                        newDto.EmployeeRefId = emp.Id;
                        newDto.EmployeeRefCode = emp.EmployeeCode;
                        newDto.EmployeeRefName = emp.EmployeeName;
                        newDto.WorkDate = dt.Date;

                        bool nightFlag = false;
                        if (dcList == null)
                            continue;
                        var dc = dcList.FirstOrDefault(t => t.EmployeeRefId == emp.Id && t.DutyChartDate == dt.Date);
                        if (dc == null)
                            nightFlag = false;
                        else
                            nightFlag = dc.NightFlag;

                        DateTime fromDate = dt.Date.AddHours(4);
                        DateTime toDate = dt.AddHours(24);
                        if (nightFlag)
                            toDate = dt.AddHours(32);
                        var particularDateList = attLogdetail.Where(t => t.EmployeeRefId == emp.Id && t.CheckTime >= fromDate && t.CheckTime <= toDate).ToList();

                        if (particularDateList == null || particularDateList.Count == 0)
                            continue;
                        particularDateList = particularDateList.OrderBy(t => t.CheckTime).ToList();
                        foreach (var lst in particularDateList)
                        {
                            if (lst.CheckTime.HasValue)
                            {
                                newDto.TimeList = newDto.TimeList + lst.AttendanceMachineLocationRefName + " - " + lst.CheckTime.Value.ToString("HH:mm:ss") + ",";
                            }
                        }

                        if (!newDto.TimeList.IsNullOrEmpty())
                        {
                            if (newDto.TimeList.Length > 0)
                                newDto.TimeList = newDto.TimeList.Left(newDto.TimeList.Length - 1);
                        }
                        newDto.NightFlag = nightFlag;
                        output.Add(newDto);
                    }
                }

            }
            catch (Exception ex)
            {
                // Get stack trace for the exception with source file information
                var st = new StackTrace(ex, true);
                // Get the top stack frame
                var frame = st.GetFrame(0);
                // Get the line number from the stack frame
                var lineNumber = frame.GetFileLineNumber();

                MethodBase m = MethodBase.GetCurrentMethod();
                string innerExceptionMessage = ex.InnerException != null ? ex.InnerException.Message : "";
                throw new UserFriendlyException(emp == null ? "" : emp.EmployeeName + " Method : " + m.ReflectedType.Name + " : " + m.Name + " " + ex.Message + " InnerException : " + innerExceptionMessage);
            }
            return output;
        }

        public async Task<FileDto> GetAttendanceLogToExcel(GetAttendanceLogInput input)
        {
            var allList = await GetGpsEmployeeAttendanceDateWise(input);
            var allListDtos = allList.MapTo<List<AttendanceParticularDateList>>();
            return _attendancelogExporter.ExportParticularDateWiseToFile(allListDtos);
        }

        public async Task<FileDto> GetOTLogToExcel(GetAttendanceLogInput input)
        {
            var emp = await _personalInformationRepo.FirstOrDefaultAsync(t => t.Id == input.EmployeeRefId);
            if (emp == null)
            {
                return null;
            }

            var allList = await GetOTEmployeeAttendanceDateWise(input);
            var allListDtos = allList.MapTo<List<AttendanceParticularDateList>>();

            return _attendancelogExporter.ExportPerDaySalaryPerEmployeeWithOT(allListDtos, emp.EmployeeName);
        }

        public async Task<List<AttendanceParticularDateList>> GetOTEmployeeAttendanceDateWise(GetAttendanceLogInput input)
        {
            if (!input.StartDate.HasValue)
            {
                input.StartDate = DateTime.Today.AddDays(-30);
                input.EndDate = DateTime.Now;
            }

            GetAttendanceLogInput ginput = new GetAttendanceLogInput
            {
                EmployeeRefId = input.EmployeeRefId,
                StartDate = input.StartDate,
                EndDate = input.EndDate,
                MaxResultCount = AppConsts.MaxPageSize
            };

            var rsGps = await GetAllForIndex(ginput);
            List<AttendanceLogListDto> attLogdetail = rsGps.Items.MapTo<List<AttendanceLogListDto>>();
            int[] arrEmployeeRefIds = attLogdetail.Select(t => t.EmployeeRefId).Distinct().ToArray();
            var rsEmps = await _personalInformationRepo.GetAllListAsync(t => arrEmployeeRefIds.Contains(t.Id));
            var rsSalary = await _salaryInfoRepo.GetAllListAsync();

            var employeeDefaultWorkDays = await _salaryinfoAppService.GetEmployeeWorkDay(new GetDataBasedOnNullableEmployee { EmployeeRefId = input.EmployeeRefId });
            var rsWorkDays = employeeDefaultWorkDays.WorkDayListDtos;
            var rsMonthWiseWorkDays = await _monthwiseWorkDayRepo.GetAllListAsync();

            var dcList = await _dutychartDetailRepo.GetAllListAsync(t => arrEmployeeRefIds.Contains(t.EmployeeRefId) && t.DutyChartDate >= input.StartDate.Value && t.DutyChartDate <= input.EndDate.Value);

            List<AttendanceParticularDateList> output = new List<AttendanceParticularDateList>();

            foreach (var argEmp in arrEmployeeRefIds)
            {
                var emp = rsEmps.FirstOrDefault(t => t.Id == argEmp);

                List<DateTime> dates = attLogdetail.Select(t => t.CheckTime.Value.Date).Distinct().ToList();

                foreach (var dt in dates)
                {
                    AttendanceParticularDateList newDto = new AttendanceParticularDateList();

                    newDto.BioMetricCode = emp.BioMetricCode;
                    newDto.EmployeeRefId = emp.Id;
                    newDto.EmployeeRefCode = emp.EmployeeCode;
                    newDto.EmployeeRefName = emp.EmployeeName;
                    newDto.WorkDate = dt.Date;

                    bool nightFlag = false;
                    var dc = dcList.FirstOrDefault(t => t.EmployeeRefId == emp.Id && t.DutyChartDate == dt.Date);
                    if (dc == null)
                        nightFlag = false;
                    else
                        nightFlag = dc.NightFlag;

                    DateTime fromDate = dt.Date.AddHours(4);
                    DateTime toDate = dt.AddHours(24);
                    if (nightFlag)
                        toDate = dt.AddHours(32);
                    var particularDateList = attLogdetail.Where(t => t.EmployeeRefId == emp.Id && t.CheckTime >= fromDate && t.CheckTime <= toDate).ToList();

                    if (particularDateList == null || particularDateList.Count == 0)
                        continue;
                    particularDateList = particularDateList.OrderBy(t => t.CheckTime).ToList();
                    newDto.AttendanceLogListDtos = particularDateList;
                    foreach (var lst in particularDateList)
                    {
                        newDto.TimeList = newDto.TimeList + lst.AttendanceMachineLocationRefName + " - " + lst.CheckTime.Value.ToString("HH:mm:ss") + ",";
                    }

                    if (!newDto.TimeList.IsNullOrEmpty())
                    {
                        if (newDto.TimeList.Length > 0)
                            newDto.TimeList = newDto.TimeList.Left(newDto.TimeList.Length - 1);
                    }
                    newDto.NightFlag = nightFlag;
                    var dayOfWeek = newDto.WorkDate.DayOfWeek;
                    var a = (int)dayOfWeek;
                    var workDay = rsWorkDays.FirstOrDefault(t => t.DayOfWeekRefId == (int)dayOfWeek);
                    if (workDay == null)
                    {
                        throw new UserFriendlyException("Work Day Not Defined For : " + (int)dayOfWeek);
                    }

                    var checkInTime = particularDateList.FirstOrDefault(t => t.CheckTypeCode == "TI");
                    if (checkInTime == null)
                    {
                        newDto.Remarks = newDto.Remarks + " " + " Check IN Time Not Found";
                    }
                    else
                    {
                        newDto.InTime = checkInTime.CheckTime.Value;
                    }
                    var checkOutTime = particularDateList.LastOrDefault(t => t.CheckTypeCode == "TO");
                    if (checkOutTime == null)
                    {
                        newDto.Remarks = newDto.Remarks + " " + " Check OUT Time Not Found";
                    }
                    else
                    {
                        newDto.OutTime = checkOutTime.CheckTime.Value;
                    }

                    if (checkInTime == null || checkOutTime == null)
                    {
                        if (particularDateList.Count == 2)
                        {
                            newDto.InTime = particularDateList[0].CheckTime.Value;
                            newDto.OutTime = particularDateList[1].CheckTime.Value;
                            newDto.Remarks = "";
                        }
                        else if (particularDateList.Count == 1)
                        {
                            newDto.InTime = particularDateList[0].CheckTime.Value;
                            newDto.Remarks = " Check OUT Time Not Found";
                        }
                        else if (particularDateList.Count > 1)
                        {
                            newDto.InTime = particularDateList.First().CheckTime.Value;
                            newDto.OutTime = particularDateList.Last().CheckTime.Value;
                            newDto.Remarks = "";
                        }
                        else if (particularDateList.Count > 2)
                        {
                            newDto.InTime = particularDateList.First().CheckTime.Value;
                            newDto.OutTime = particularDateList.Last().CheckTime.Value;
                            newDto.Remarks = "";
                        }
                    }

                    if (newDto.InTime != null && newDto.OutTime != null)
                    {
                        TimeSpan ts = newDto.OutTime.Value - newDto.InTime.Value;
                        newDto.ActualWorkHours = (decimal)ts.TotalHours;
                        if (a == 6 || a == 7)
                        {

                        }
                        else
                        {
                            newDto.ActualWorkHours = newDto.ActualWorkHours - workDay.RestHours;
                        }
                    }
                    else
                    {
                        newDto.ActualWorkHours = 0;
                    }

                    newDto.WorkHoursPerDay = workDay.NoOfHourWorks; // + workDay.RestHours;
                    newDto.WorkHoursPerDayWithMinutes = ChangeMinutstoTime((int)(newDto.WorkHoursPerDay * 60));
                    newDto.RestHours = workDay.RestHours;
                    newDto.OtRatio = workDay.OtRatio;
                    newDto.MaximumOTAllowedHours = workDay.MaximumOTAllowedHours;

                    #region SalaryCalculation
                    var salary = rsSalary.FirstOrDefault(t => t.Id == newDto.EmployeeRefId);
                    if (salary == null)
                    {
                        newDto.Remarks = "Salary Records Not Found";
                        newDto.Salary = 0;
                        newDto.ActualSalaryCurrentDay = 0;
                    }
                    else
                    {
                        var monthWorkDays = rsMonthWiseWorkDays.FirstOrDefault(t => newDto.WorkDate >= t.StartDate && newDto.WorkDate <= t.EndDate);
                        if (monthWorkDays == null)
                        {
                            newDto.Remarks = "Month Wise Work Days Not Defined";
                            newDto.Salary = 0;
                            newDto.StandardSalaryCurrentDay = 0;
                        }
                        else
                        {
                            decimal noofDaysInTheMonth = monthWorkDays.NoOfWorkDays;

                            var consolidatedSalary = salary.BasicSalary;
                            if (salary.SalaryMode == (int)SalaryCalculationType.MONTHLY)
                            {
                                newDto.Salary = consolidatedSalary;
                                newDto.StandardSalaryCurrentDay = Math.Round(consolidatedSalary / noofDaysInTheMonth, 2);
                            }
                            else if (salary.SalaryMode == (int)SalaryCalculationType.DAILY)
                            {
                                newDto.Salary = Math.Round(consolidatedSalary * noofDaysInTheMonth, 2);
                                newDto.StandardSalaryCurrentDay = consolidatedSalary;
                            }
                            else if (salary.SalaryMode == (int)SalaryCalculationType.HOURLY)
                            {
                                newDto.Salary = Math.Round(consolidatedSalary * noofDaysInTheMonth * newDto.WorkHoursPerDay, 2);
                                newDto.StandardSalaryCurrentDay = Math.Round(consolidatedSalary * newDto.WorkHoursPerDay);
                            }
                            else
                            {
                                newDto.Remarks = "Salary Type Not Defined";
                                newDto.Salary = 0;
                                newDto.StandardSalaryCurrentDay = 0;
                            }
                        }
                    }
                    #endregion

                    if (newDto.ActualWorkHours > 0)
                    {
                        if (newDto.ActualWorkHours > newDto.WorkHoursPerDay)
                        {
                            newDto.OTWorkHours = Math.Round(newDto.ActualWorkHours - newDto.WorkHoursPerDay, 2);
                            newDto.OTWorkHoursInMinutes = ChangeMinutstoTime((int)((newDto.ActualWorkHours - newDto.WorkHoursPerDay) * 60));
                            newDto.ShortageWorkHours = 0;
                            if (salary == null)
                            {
                                continue;
                            }
                            if (salary.Ot1PerHour > 0 || salary.Ot2PerHour > 0)
                            {
                                if (workDay.OtRatio == (decimal)1.5)
                                {
                                    newDto.OtAmount = newDto.OTWorkHours * salary.Ot1PerHour;
                                }
                                else if (workDay.OtRatio == (decimal)2.0)
                                {
                                    newDto.OtAmount = newDto.OTWorkHours * salary.Ot2PerHour;
                                }
                            }
                            else
                                newDto.OtAmount = 0;
                        }
                        else if (newDto.ActualWorkHours < newDto.WorkHoursPerDay)
                        {
                            newDto.OTWorkHours = 0;
                            newDto.ShortageWorkHours = Math.Round(newDto.ActualWorkHours - newDto.WorkHoursPerDay, 2);
                            newDto.ShortageWorkHoursInMinutes = ChangeMinutstoTime((int)((newDto.ActualWorkHours - newDto.WorkHoursPerDay) * 60));
                            newDto.ShortageAmount = Math.Round(newDto.ShortageWorkHours * (newDto.StandardSalaryCurrentDay / workDay.NoOfHourWorks), 2);
                        }
                        newDto.ActualSalaryCurrentDay = Math.Round(newDto.StandardSalaryCurrentDay + newDto.OtAmount + newDto.ShortageAmount, 2);
                    }
                    newDto.ActualWorkHoursWithMinutes = ChangeMinutstoTime((int)(newDto.ActualWorkHours * 60));
                    output.Add(newDto);
                }
            }
            return output;
        }


        public string ChangeMinutstoTime(int minutesoftheDay)
        {
            if (minutesoftheDay == 0)
            {
                return "00:00";
            }

            double argMinutes = minutesoftheDay;
            int hour = int.Parse(Math.Floor(argMinutes / 60).ToString());
            if (hour > 24)
                hour = hour - 24;

            var min = (argMinutes % 60).ToString().PadLeft(2);
            if (min.Left(1).Equals(" "))
                min = "0" + min.Right(1);
            string hourString = hour >= 10 ? hour.ToString() : "0" + hour;
            string data = hourString + ":" + min;
            return data;
        }

        public virtual async Task<ApiMessageReturn> ApiCreateEmployeeAttendance(ApiEmployeeAttendanceDto input)
        {
            ApiMessageReturn output = new ApiMessageReturn();
            try
            {
                if (input.EmployeeRefId == 0)
                {
                    output.ErrorMessage = "No Employee";
                    output.Id = -1;

                    return output;
                }

                if (input.CheckTime == null)
                {
                    output.ErrorMessage = "No Check Time";
                    output.Id = -1;
                    return output;
                }

                var myDate  = input.CheckTime.Value.Date;

                var updateTrans = false;
                AttendanceLog trans = null;
                if (input.Id.HasValue && input.Id > 0)
                {
                    updateTrans = true;
                    trans = await _attendancelogRepo.GetAsync(input.Id.Value);
                }
                else
                {
                    var mTrans =
                            _attendancelogRepo.GetAll().Where(
                                a =>
                                    a.LocationRefId == input.LocationRefId && a.TenantId == input.TenantId && 
                                    a.EmployeeRefId == input.EmployeeRefId & a.CheckTime == myDate && a.CheckType == input.CheckType);

                    if (mTrans.Any())
                    {
                        trans = mTrans.LastOrDefault(a => a.CheckTime == input.CheckTime.Value);
                    }

                    if (trans != null)
                    {
                        updateTrans = true;
                    }
                }
                if (trans == null)
                    trans = new AttendanceLog();

                trans.LocationRefId = input.LocationRefId;
                trans.EmployeeRefId = input.EmployeeRefId;
                trans.CheckTime = input.CheckTime;
                trans.CheckType = input.CheckType;
                trans.TenantId = input.TenantId;
                trans.CheckTimeTrunc = input.CheckTime.Value.Date;

                if (input.CheckType >= 0)
                {
                    CheckType myType = (CheckType) input.CheckType;
                    trans.CheckTypeCode = myType.ToString();
                }
                trans.Deleted = input.Deleted;

                var id = 0;
                if (updateTrans)
                {
                    id = trans.Id;
                }
                else
                {
                    id = await _attendancelogRepo.InsertAndGetIdAsync(trans);
                }

                output.Id = id;
            }
            catch (Exception ex)
            {
                output.Id = -1;
                output.ErrorMessage = ex.Message;
            }

            return output;
        }
    }
}