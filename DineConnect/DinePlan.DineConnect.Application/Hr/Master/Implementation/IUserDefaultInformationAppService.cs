﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Hr.Master
{
    public interface IUserDefaultInformationAppService : IApplicationService
    {
        Task<PagedResultOutput<UserDefaultInformationListDto>> GetAll(GetUserDefaultInformationInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetUserDefaultInformationForEditOutput> GetUserDefaultInformationForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateUserDefaultInformation(CreateOrUpdateUserDefaultInformationInput input);
        Task DeleteUserDefaultInformation(IdInput input);

        Task<ListResultOutput<UserDefaultInformationListDto>> GetUserIds();
    }
}