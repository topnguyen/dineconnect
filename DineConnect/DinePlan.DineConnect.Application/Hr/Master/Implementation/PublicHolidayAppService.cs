﻿
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.Hr.Impl;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Hr.Master.Implementation
{
    public class PublicHolidayAppService : DineConnectAppServiceBase, IPublicHolidayAppService
    {

        private readonly IPublicHolidayListExcelExporter _publicholidayExporter;
        private readonly IPublicHolidayManager _publicholidayManager;
        private readonly IRepository<PublicHoliday> _publicholidayRepo;

        public PublicHolidayAppService(IPublicHolidayManager publicholidayManager,
            IRepository<PublicHoliday> publicHolidayRepo,
            IPublicHolidayListExcelExporter publicholidayExporter)
        {
            _publicholidayManager = publicholidayManager;
            _publicholidayRepo = publicHolidayRepo;
            _publicholidayExporter = publicholidayExporter;

        }

        public async Task<PagedResultOutput<PublicHolidayListDto>> GetAll(GetPublicHolidayInput input)
        {
            var allItems = _publicholidayRepo.GetAll();
            if (input.Operation.IsNullOrEmpty())
            {
                allItems = _publicholidayRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.HolidayReason.Contains(input.Filter)
               );
            }
            else
            {
                allItems = _publicholidayRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.HolidayReason.Contains(input.Filter)
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<PublicHolidayListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<PublicHolidayListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _publicholidayRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<PublicHolidayListDto>>();
            return _publicholidayExporter.ExportToFile(allListDtos);
        }

        public async Task<GetPublicHolidayForEditOutput> GetPublicHolidayForEdit(NullableIdInput input)
        {
            PublicHolidayEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _publicholidayRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<PublicHolidayEditDto>();
            }
            else
            {
                editDto = new PublicHolidayEditDto();
            }

            return new GetPublicHolidayForEditOutput
            {
                PublicHoliday = editDto
            };
        }

        public async Task CreateOrUpdatePublicHoliday(CreateOrUpdatePublicHolidayInput input)
        {
            if (input.PublicHoliday.Id.HasValue)
            {
                await UpdatePublicHoliday(input);
            }
            else
            {
                await CreatePublicHoliday(input);
            }
        }

        public async Task DeletePublicHoliday(IdInput input)
        {
            await _publicholidayRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdatePublicHoliday(CreateOrUpdatePublicHolidayInput input)
        {
            var item = await _publicholidayRepo.GetAsync(input.PublicHoliday.Id.Value);
            var dto = input.PublicHoliday;

            //TODO: SERVICE PublicHoliday Update Individually
            item.HolidayDate = dto.HolidayDate;
            item.HolidayReason = dto.HolidayReason;
            item.OTProportion=dto.OTProportion;

            CheckErrors(await _publicholidayManager.CreateSync(item));
        }

        protected virtual async Task CreatePublicHoliday(CreateOrUpdatePublicHolidayInput input)
        {
            var dto = input.PublicHoliday.MapTo<PublicHoliday>();

            CheckErrors(await _publicholidayManager.CreateSync(dto));
        }

        public async Task<ListResultOutput<PublicHolidayListDto>> GetHolidayDates()
        {
            var lstPublicHoliday = await _publicholidayRepo.GetAll().ToListAsync();
            return new ListResultOutput<PublicHolidayListDto>(lstPublicHoliday.MapTo<List<PublicHolidayListDto>>());
        }


    }
}