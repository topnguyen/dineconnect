﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using DinePlan.DineConnect.Hr.Master.Dtos;

namespace DinePlan.DineConnect.Hr.Transaction
{
	public interface IManualIncentiveAppService : IApplicationService
	{
		Task<PagedResultOutput<ManualIncentiveListDto>> GetAll(GetManualIncentiveInput inputDto);
		Task<FileDto> GetAllToExcel();
		Task<GetManualIncentiveForEditOutput> GetManualIncentiveForEdit(NullableIdInput nullableIdInput);
		Task<List<EmployeeVsIncentiveListDto>> CreateOrUpdateManualIncentive(CreateOrUpdateManualIncentiveInput input);
		Task DeleteManualIncentive(IdInput input);
	}
}
