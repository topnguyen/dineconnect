﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.Hr.Impl;
using System;
using System.Reflection;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Hr.Master.Implementation
{
    public class DocumentInfoAppService : DineConnectAppServiceBase, IDocumentInfoAppService
    {

        private readonly IDocumentInfoListExcelExporter _documentinfoExporter;
        private readonly IDocumentInfoManager _documentinfoManager;
        private readonly IRepository<DocumentInfo> _documentinfoRepo;
        private readonly IRepository<DocumentForSkillSet> _documentForSkillSetRepo;
        private readonly IRepository<SkillSet> _skillSetRepo;

        public DocumentInfoAppService(IDocumentInfoManager documentinfoManager,
            IRepository<DocumentInfo> documentInfoRepo,
            IDocumentInfoListExcelExporter documentinfoExporter,
            IRepository<DocumentForSkillSet> documentForSkillSetRepo,
            IRepository<SkillSet> skillSetRepo)
        {
            _documentinfoManager = documentinfoManager;
            _documentinfoRepo = documentInfoRepo;
            _documentinfoExporter = documentinfoExporter;
            _documentForSkillSetRepo = documentForSkillSetRepo;
            _skillSetRepo = skillSetRepo;
        }

        public async Task<PagedResultOutput<DocumentInfoListDto>> GetAll(GetDocumentInfoInput input)
        {
            try
            {
                var allItems = _documentinfoRepo.GetAll();
                if (input.Operation.IsNullOrEmpty())
                {
                    allItems = _documentinfoRepo
                   .GetAll()
                   .WhereIf(
                       !input.Filter.IsNullOrEmpty(),
                       p => p.DocumentName.Contains(input.Filter)
                   );
                }
                else
                {
                    allItems = _documentinfoRepo
                   .GetAll()
                   .WhereIf(
                       !input.Filter.IsNullOrEmpty(),
                       p => p.DocumentName.Contains(input.Filter)
                   );
                }
                allItems = allItems.WhereIf(input.DocumentType != null, p => p.DocumentType == input.DocumentType);
                var sortMenuItems = await allItems
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var allListDtos = sortMenuItems.MapTo<List<DocumentInfoListDto>>();

                foreach (var lst in allListDtos)
                {
                    lst.Document = lst.DocumentType == DocumentType.AllType ? L("All") : lst.DocumentType == DocumentType.Employee ? L("Employee") : lst.DocumentType == DocumentType.Company ? L("Company") : L("Unknown");
                }

                var allItemCount = await allItems.CountAsync();

                return new PagedResultOutput<DocumentInfoListDto>(
                    allItemCount,
                    allListDtos
                    );
            }
            catch (Exception ex)
            {
                MethodBase m = MethodBase.GetCurrentMethod();
                string innerExceptionMessage = ex.InnerException != null ? ex.InnerException.Message : "";
                throw new UserFriendlyException(" Method : " + m.ReflectedType.Name + " : " + m.Name + " " + ex.Message + " InnerException : " + innerExceptionMessage);
            }

        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _documentinfoRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<DocumentInfoListDto>>();
            return _documentinfoExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDocumentInfoForEditOutput> GetDocumentInfoForEdit(NullableIdInput input)
        {
            DocumentInfoEditDto editDto;
            List<ComboboxItemDto> editUnitLinkComboDtos = new List<ComboboxItemDto>();

            if (input.Id.HasValue)
            {
                var hDto = await _documentinfoRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<DocumentInfoEditDto>();

                var mSkillListDto = (from mul in _documentForSkillSetRepo.GetAll()
                                     where mul.DocumentRefId == editDto.Id
                                     select new DocumentForSkillSetEditDto()
                                     {
                                         Id = mul.Id,
                                         SkillSetRefId = mul.SkillSetRefId,
                                         DocumentRefId = mul.DocumentRefId
                                     }).ToList();


                if (mSkillListDto != null && mSkillListDto.Count() > 0)
                {
                    foreach (var lst in mSkillListDto)
                    {
                        var skillName = await _skillSetRepo.GetAsync(lst.SkillSetRefId);

                        editUnitLinkComboDtos.Add(new ComboboxItemDto
                        {
                            Value = lst.SkillSetRefId.ToString(),
                            DisplayText = skillName.SkillCode,
                        });

                    }
                }
            }
            else
            {
                editDto = new DocumentInfoEditDto();
                editUnitLinkComboDtos = new List<ComboboxItemDto>();
            }

            return new GetDocumentInfoForEditOutput
            {
                DocumentInfo = editDto,
                SkillSetList = editUnitLinkComboDtos
            };
        }

        public async Task CreateOrUpdateDocumentInfo(CreateOrUpdateDocumentInfoInput input)
        {
            if (input.DocumentInfo.Id.HasValue)
            {
                await UpdateDocumentInfo(input);
            }
            else
            {
                await CreateDocumentInfo(input);
            }
        }

        public async Task DeleteDocumentInfo(IdInput input)
        {
            await _documentinfoRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateDocumentInfo(CreateOrUpdateDocumentInfoInput input)
        {
            try
            {
                var item = await _documentinfoRepo.GetAsync(input.DocumentInfo.Id.Value);
                var dto = input.DocumentInfo;

                item.DocumentName = dto.DocumentName;
                item.DefaultAlertDays = dto.DefaultAlertDays;
                item.LifeTimeFlag = dto.LifeTimeFlag;
                item.DefaultExpiryDays = dto.DefaultExpiryDays;
                item.DocumentType = dto.DocumentType;
                CheckErrors(await _documentinfoManager.CreateSync(item));

                List<int> skillsToBeRetained = new List<int>();

                if (input.SkillSetList != null && input.SkillSetList.Count > 0)
                {
                    foreach (var b in input.SkillSetList)
                    {
                        int skillRefId = int.Parse(b.Value);
                        skillsToBeRetained.Add(skillRefId);
                        if (_documentForSkillSetRepo.GetAllList(u => u.DocumentRefId == input.DocumentInfo.Id.Value && u.SkillSetRefId.Equals(skillRefId)).Count == 0)
                        {
                            var ddto = new DocumentForSkillSet
                            {
                                DocumentRefId = input.DocumentInfo.Id.Value,
                                SkillSetRefId = int.Parse(b.Value)
                            };

                            await _documentForSkillSetRepo.InsertAsync(ddto);
                        }
                        else
                        {
                            var editRecord = _documentForSkillSetRepo.FirstOrDefault(sm => sm.DocumentRefId == input.DocumentInfo.Id.Value && sm.SkillSetRefId == skillRefId);

                            var itemskill = await _documentForSkillSetRepo.GetAsync(editRecord.Id);
                            itemskill.SkillSetRefId = int.Parse(b.Value);
                            itemskill.DocumentRefId = input.DocumentInfo.Id.Value;
                            await _documentForSkillSetRepo.InsertOrUpdateAndGetIdAsync(itemskill);
                        }
                    }
                }

                var delskillMaterialList = _documentForSkillSetRepo.GetAll().Where(a => a.DocumentRefId == input.DocumentInfo.Id.Value && !skillsToBeRetained.Contains(a.SkillSetRefId)).ToList();

                foreach (var a in delskillMaterialList)
                {
                    _documentForSkillSetRepo.Delete(a.Id);
                }
            }
            catch (System.Exception)
            {
                throw;
            }

        }

        protected virtual async Task CreateDocumentInfo(CreateOrUpdateDocumentInfoInput input)
        {
            var dto = input.DocumentInfo.MapTo<DocumentInfo>();
            CheckErrors(await _documentinfoManager.CreateSync(dto));

            if (input.SkillSetList != null)
            {
                foreach (var lst in input.SkillSetList)
                {
                    var ddto = new DocumentForSkillSet
                    {
                        DocumentRefId = dto.Id,
                        SkillSetRefId = int.Parse(lst.Value)
                    };

                    await _documentForSkillSetRepo.InsertAsync(ddto);
                }
            }
        }

        public async Task<ListResultOutput<DocumentInfoListDto>> GetDocumentNames()
        {
            var lstDocumentInfo = await _documentinfoRepo.GetAll().ToListAsync();
            return new ListResultOutput<DocumentInfoListDto>(lstDocumentInfo.MapTo<List<DocumentInfoListDto>>());
        }
        public async Task<ListResultOutput<ComboboxItemDto>> GetDocumentTypeForCombobox()
        {
            List<ComboboxItemDto> retList = new List<ComboboxItemDto>();

            string enumstring;
            Array EnumValues = System.Enum.GetValues(typeof(DocumentType));
            foreach (int EnumValue in EnumValues)
            {
                enumstring = Enum.GetName(typeof(DocumentType), EnumValue);
                retList.Add(new ComboboxItemDto { Value = EnumValue.ToString(), DisplayText = enumstring });
            }

            return
                   new ListResultOutput<ComboboxItemDto>(
                       retList.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText)).ToList());
        }
    }
}

