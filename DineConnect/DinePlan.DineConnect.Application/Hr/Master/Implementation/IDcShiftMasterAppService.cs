﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Hr.Master
{
    public interface IDcShiftMasterAppService : IApplicationService
    {
        Task<PagedResultOutput<DcShiftMasterListDto>> GetAll(GetDcShiftMasterInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetDcShiftMasterForEditOutput> GetDcShiftMasterForEdit(NullableIdInput nullableIdInput);
        Task<IdInput> CreateOrUpdateDcShiftMaster(CreateOrUpdateDcShiftMasterInput input);
        Task DeleteDcShiftMaster(IdInput input);

        Task<ListResultOutput<DcShiftMasterListDto>> GetDutyDescs();
        Task<int> GetMaxUserSerialNumber();
        Task<ListResultOutput<DcShiftMasterListDto>> GetShiftNames(LocationStationCodeDto input);

        Task<IdInput> CloneStationAsShift(IdInput stationinput);
    }
}

