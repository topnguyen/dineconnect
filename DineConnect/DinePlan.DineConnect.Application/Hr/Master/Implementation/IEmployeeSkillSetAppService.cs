﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Hr.Master
{
    public interface IEmployeeSkillSetAppService : IApplicationService
    {
        Task<PagedResultOutput<EmployeeSkillSetListDto>> GetAll(GetEmployeeSkillSetInput inputDto);
        Task<FileDto> GetAllToExcel(GetEmployeeSkillSetInput input);
        Task<GetEmployeeSkillSetForEditOutput> GetEmployeeSkillSetForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateEmployeeSkillSet(CreateOrUpdateEmployeeSkillSetInput input);
        Task DeleteEmployeeSkillSet(IdInput input);
        Task<PagedResultOutput<EmployeeSkillSetViewDto>> GetAllSkillWithOneRecord(GetEmployeeSkillSetInput input);

    }
}