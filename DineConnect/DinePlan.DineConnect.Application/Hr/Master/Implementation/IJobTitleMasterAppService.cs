﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Hr.Master.Implementation
{
    public interface IJobTitleMasterAppService : IApplicationService
    {
        Task<PagedResultOutput<JobTitleMasterListDto>> GetAll(GetJobTitleMasterInput inputDto);
        //Task<FileDto> GetAllToExcel();
        Task<GetJobTitleMasterForEditOutput> GetJobTitleMasterForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateJobTitleMaster(CreateOrUpdateJobTitleMasterInput input);
        Task DeleteJobTitleMaster(IdInput input);

        Task<ListResultOutput<JobTitleMasterListDto>> Gets();
    }
}