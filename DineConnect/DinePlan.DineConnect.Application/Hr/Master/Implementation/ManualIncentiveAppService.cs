﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.Hr.Impl;
using DinePlan.DineConnect.Hr.Master.Dtos;
using System;
using DinePlan.DineConnect.Hr.Inventory.Dtos;
using DinePlan.DineConnect.Hr.Master;
using DinePlan.DineConnect.Hr;
using DinePlan.DineConnect.House.Master.Dtos;

namespace DinePlan.DineConnect.Hr.Transaction.Implementation
{
    public class ManualIncentiveAppService : DineConnectAppServiceBase, IManualIncentiveAppService
    {

        private readonly IManualIncentiveListExcelExporter _manualincentiveExporter;
        private readonly IManualIncentiveManager _manualincentiveManager;
        private readonly IRepository<ManualIncentive> _manualincentiveRepo;
        private readonly IRepository<PersonalInformation> _personalInformationRepo;
        private readonly IRepository<IncentiveTag> _incentiveTagRepo;
        //private readonly IRepository<NdtProcedure> _ndtProcedureRepo;
        //private readonly IRepository<ProjectCostCentre> _projectCostCentreRepo;
        private readonly IRepository<DutyChartDetail> _dutychartdetailRepo;
        private readonly IRepository<EmployeeVsIncentive> _employeeVsIncentiveRepo;
        private readonly IRepository<SalaryInfo> _salaryRepo;
        private readonly ISalaryInfoAppService _salaryinfoAppService;
        private readonly IRepository<PublicHoliday> _publicHolidayRepo;
        private readonly IRepository<SkillSet> _skillSetRepo;
        //private readonly IRepository<TimeSheetDetail> _timesheetDetailRepo;
        //private readonly IRepository<TimeSheetEmployeeDetail> _timesheetEmployeeDetailRepo;
        private readonly IRepository<IncentiveVsEntity> _incentiveVsEntityRepo;
        private readonly IRepository<EmployeeSkillSet> _employeeskillsetRepo;
        //private readonly IRepository<DocumentForSkillSet> _documentForSkillSetRepo;
        private readonly IRepository<EmployeeDocumentInfo> _employeeDocumentinfoRepo;
        private readonly IRepository<DocumentInfo> _documentInforepo;
        private readonly IRepository<EmployeeResidentStatus> _employeeResidentStatusRepo;
        private readonly IRepository<EmployeeRace> _employeeRaceStatusRepo;

        public ManualIncentiveAppService(IManualIncentiveManager manualincentiveManager,
            IRepository<EmployeeSkillSet> employeeskillsetRepo,
            //IRepository<TimeSheetDetail> timesheetDetailRepo,
            //IRepository<TimeSheetEmployeeDetail> timesheetEmployeeDetailRepo,
            IRepository<ManualIncentive> manualIncentiveRepo,
            IRepository<PersonalInformation> personalInformationRepo,
            IRepository<IncentiveTag> incentiveTagRepo,
            //IRepository<NdtProcedure> ndtProcedureRepo,
            //IRepository<ProjectCostCentre> projectCostCentreRepo,
            IManualIncentiveListExcelExporter manualincentiveExporter,
            IRepository<DutyChartDetail> dutychartdetailRepo,
            IRepository<EmployeeVsIncentive> employeeVsIncentiveRepo,
            IRepository<SalaryInfo> salaryRepo,
            IRepository<PublicHoliday> publicHolidayRepo,
            IRepository<SkillSet> skillSetRepo,
            IRepository<IncentiveVsEntity> incentiveVsEntityRepo,
            //IRepository<DocumentForSkillSet> documentForSkillSetRepo,
            IRepository<EmployeeDocumentInfo> employeeDocumentinfoRepo,
            ISalaryInfoAppService salaryinfoAppService,
            IRepository<DocumentInfo> documentInforepo,
            IRepository<EmployeeResidentStatus> employeeResidentStatusRepo,
            IRepository<EmployeeRace> employeeRaceStatusRepo)
        {
            _manualincentiveManager = manualincentiveManager;
            _manualincentiveRepo = manualIncentiveRepo;
            _manualincentiveExporter = manualincentiveExporter;
            _personalInformationRepo = personalInformationRepo;
            _incentiveTagRepo = incentiveTagRepo;
            //_ndtProcedureRepo = ndtProcedureRepo;
            //_projectCostCentreRepo = projectCostCentreRepo;
            _dutychartdetailRepo = dutychartdetailRepo;
            _employeeVsIncentiveRepo = employeeVsIncentiveRepo;
            _salaryRepo = salaryRepo;
            _salaryinfoAppService = salaryinfoAppService;
            _publicHolidayRepo = publicHolidayRepo;
            _skillSetRepo = skillSetRepo;
            //_timesheetDetailRepo = timesheetDetailRepo;
            //_timesheetEmployeeDetailRepo = timesheetEmployeeDetailRepo;
            _incentiveVsEntityRepo = incentiveVsEntityRepo;
            _employeeskillsetRepo = employeeskillsetRepo;
            //_documentForSkillSetRepo = documentForSkillSetRepo;
            _employeeDocumentinfoRepo = employeeDocumentinfoRepo;
            _documentInforepo = documentInforepo;
            _employeeResidentStatusRepo = employeeResidentStatusRepo;
            _employeeRaceStatusRepo = employeeRaceStatusRepo;
        }

        public async Task<PagedResultOutput<ManualIncentiveListDto>> GetAll(GetManualIncentiveInput input)
        {
            var allItems = _manualincentiveRepo.GetAll();

            if (input.StartDate.HasValue)
            {
                allItems = allItems.Where(t => t.IncentiveDate >= input.StartDate.Value && t.IncentiveDate <= input.EndDate.Value);
            }

            var personalInformation = _personalInformationRepo.GetAll();
            if (!input.EmployeeRefName.IsNullOrEmpty())
            {
                personalInformation = personalInformation.Where(t => t.EmployeeName.Contains(input.EmployeeRefName) || t.EmployeeCode.Contains(input.EmployeeRefName));
            }

            var incentiveTags = _incentiveTagRepo.GetAll();
            if (input.IncentiveTagRefId.HasValue)
            {
                incentiveTags = incentiveTags.Where(t => t.Id == input.IncentiveTagRefId.Value);
            }

            if (input.IncentiveTags != null && input.IncentiveTags.Count > 0)
            {
                int[] arrIds = input.IncentiveTags.Select(t => t.Id).ToArray();
                incentiveTags = incentiveTags.Where(t => arrIds.Contains(t.Id));
            }

            var outputItems = (from mi in allItems
                               join per in personalInformation
                               on mi.EmployeeRefId equals per.Id
                               join incTag in incentiveTags on mi.IncentiveTagRefId equals incTag.Id
                               select new ManualIncentiveListDto
                               {
                                   Id = mi.Id,
                                   IncentiveDate = mi.IncentiveDate,
                                   EmployeeRefId = mi.EmployeeRefId,
                                   EmployeeRefCode = per.EmployeeCode,
                                   EmployeeRefName = per.EmployeeName,
                                   IncentiveAmount = mi.IncentiveAmount,
                                   ManualOtHours = mi.ManualOtHours,
                                   IncentiveTagRefId = mi.IncentiveTagRefId,
                                   IncentiveTagRefCode = incTag.IncentiveTagCode,
                                   Remarks = mi.Remarks,
                                   ApprovedUserId = mi.ApprovedUserId,
                                   ApprovedDate = mi.ApprovedDate,
                                   RecommendedUserId = mi.RecommendedUserId,
                                   ApprovedUserName = "",
                                   CreationTime = mi.CreationTime
                               }
                               );

            var sortMenuItems = await outputItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<ManualIncentiveListDto>>();

            var allItemCount = await allItems.CountAsync();

            if (input.Sorting.Equals("ProjectCostCentreRefCode") || input.Sorting.Equals("NdtProcedureRefCode"))
            {
                allListDtos = allListDtos.OrderBy(input.Sorting).ToList();
            }

            return new PagedResultOutput<ManualIncentiveListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _manualincentiveRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<ManualIncentiveListDto>>();
            return _manualincentiveExporter.ExportToFile(allListDtos);
        }

        public async Task<GetManualIncentiveForEditOutput> GetManualIncentiveForEdit(NullableIdInput input)
        {
            ManualIncentiveEditDto editDto;
            List<EmployeeWithSkillSetViewDto> employeeList = new List<EmployeeWithSkillSetViewDto>();
            if (input.Id.HasValue)
            {
                var hDto = await _manualincentiveRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<ManualIncentiveEditDto>();
             
            }
            else
            {
                editDto = new ManualIncentiveEditDto();
            }

            return new GetManualIncentiveForEditOutput
            {
                ManualIncentive = editDto,
            };
        }

        public async Task<ReturnSuccessOrFailureWithMessage> ExistAllServerLevelBusinessRules(CreateOrUpdateManualIncentiveInput input)
        {
            input.ManualIncentive.IncentiveDate = input.ManualIncentive.IncentiveDate.Date;
            if (input.EmployeeList == null || input.EmployeeList.Count == 0)
            {
                throw new UserFriendlyException(L("NoMoreDetailFound"));
            }

            if (input.ManualIncentive.IncentiveDate > DateTime.Today)
            {
                throw new UserFriendlyException(L("FutureDateNotAllowed"));
            }

            if (input.ManualIncentive.IncentiveDate.Date < DateTime.Today.AddDays(-95))
            {
                throw new UserFriendlyException(L("DateNotInRange"));
            }

            var incTag = await _incentiveTagRepo.FirstOrDefaultAsync(t => t.Id == input.ManualIncentive.IncentiveTagRefId);

            if (incTag.IncenitveBasedOnSalaryTags == false)
            {
                if (input.ManualIncentive.IncentiveAmount == 0)
                {
                    throw new UserFriendlyException(L("ZeroNotAllowed"));
                }

                if (input.ManualIncentive.IncentiveAmount < incTag.MinRange || input.ManualIncentive.IncentiveAmount > incTag.MaxRange)
                {
                    throw new UserFriendlyException(L("NotInRangeIncentiveAmount", input.ManualIncentive.IncentiveAmount, incTag.MinRange, incTag.MaxRange));
                }
            }
            else
            {
                if (incTag.OverTimeBasedIncentive)
                {
                    if (input.ManualIncentive.ManualOtHours == 0)
                    {
                        throw new UserFriendlyException(L("ManualOTHoursShouldNotBeZero"));
                    }
                }
            }

            foreach (var emp in input.EmployeeList)
            {
                var per = await _personalInformationRepo.FirstOrDefaultAsync(t => t.Id == emp.EmployeeRefId);
                if (per.IncentiveAllowedFlag == false)
                {
                    throw new UserFriendlyException(L("IncentiveCanNotBeAssignedToThisEmployee", per.EmployeeCode + " - " + per.EmployeeName));
                }
                if (incTag.CanBeAssignedToAllEmployees == false)
                {
                    var allowedIncentives = await _employeeVsIncentiveRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == emp.EmployeeRefId && t.IncentiveTagRefId == input.ManualIncentive.IncentiveTagRefId);
                    if (allowedIncentives == null)
                    {
                        throw new UserFriendlyException(L("IncentiveNotMappedToEmployee", per.EmployeeCode + " - " + per.EmployeeName, incTag.IncentiveTagCode));
                    }
                }

                #region CheckEmployeeWorking
                if (per.LastWorkingDate.HasValue)
                {
                    if (per.LastWorkingDate.Value < input.ManualIncentive.IncentiveDate)
                    {
                        throw new UserFriendlyException(L("EmployeeResignedAlready", per.EmployeeCode + " - " + per.EmployeeName, per.LastWorkingDate.Value.ToString("yyyy-MMM-dd")));
                    }
                }
                #endregion

                #region CheckDutyChartExist - Working Or Not
                var dc = await _dutychartdetailRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == emp.EmployeeRefId && DbFunctions.TruncateTime(t.DutyChartDate) == DbFunctions.TruncateTime(input.ManualIncentive.IncentiveDate));
                if (dc == null)
                {
                    throw new UserFriendlyException(L("DutyChartNotFoundForEmployee", per.EmployeeCode + " - " + per.EmployeeName, input.ManualIncentive.IncentiveDate.ToString("yyyy-MMM-dd")));
                }
                else if (dc.AllottedStatus.Equals(L("Job")) || dc.AllottedStatus.Equals(L("Mockup")))
                {
                    //Can pay the incentive
                }
                else if (dc.AllottedStatus.Equals(L("Idle")) && incTag.CanIssueToIdleStatus)
                {
                    //Can pay the incentive Even if Idle Status
                }
                else
                {
                    throw new UserFriendlyException(L("JobStatusForEmployeeOnDate", per.EmployeeCode + " - " + per.EmployeeName, input.ManualIncentive.IncentiveDate.ToString("yyyy-MMM-dd"), L(dc.AllottedStatus)));
                }
                #endregion

                //#region CheckAtLeastOneTimeSheetExistsOnTheDay
                //if (incTag.TimeSheetRequiredForGivenDate == true)
                //{
                //    var tsDet = (from det in _timesheetDetailRepo.GetAll()
                //                    .Where(t => DbFunctions.TruncateTime(t.TsDate) == DbFunctions.TruncateTime(input.ManualIncentive.IncentiveDate))
                //                 join empDet in _timesheetEmployeeDetailRepo.GetAll().Where(t => t.EmployeeRefId == emp.EmployeeRefId)
                //                 on det.Id equals empDet.TsDetailRefId
                //                 select det);
                //    if (tsDet.Count() == 0)
                //    {
                //        throw new UserFriendlyException(L("NoMoreTimeSheetDetailOnTheGivenDate", per.EmployeeCode + " - " + per.EmployeeName, input.ManualIncentive.IncentiveDate.ToString("yyyy-MMM-dd")));
                //    }

                //}
                //#endregion

                #region Check IncentiveVsEntities
                string errorMessage = "";
                var ielist = await _incentiveVsEntityRepo.GetAllListAsync(t => t.IncentiveTagRefId == incTag.Id);
                var incEntitiesList = ielist.MapTo<List<IncentiveVsEntityListDto>>();
                List<DutyChartDetail> lstDcDetail = new List<DutyChartDetail>();
                lstDcDetail.Add(dc);
                if (incEntitiesList != null && incEntitiesList.Count() > 0)
                {
                    bool existFlag = true;
                    foreach (var ie in incEntitiesList)
                    {
                        //	Skill Set Reference
                        if (ie.EntityType == (int)IncentiveEntityType.SKILLSET)
                        {
                            var skillSet = await _skillSetRepo.FirstOrDefaultAsync(t => t.Id == ie.EntityRefId);
                            if (skillSet == null)
                            {
                                throw new UserFriendlyException(L("NotExistForId", L("Skill"), ie.EntityRefId));
                            }
                            else
                            {
                                var empSkillIdExists = await _employeeskillsetRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == emp.EmployeeRefId && t.SkillSetRefId == skillSet.Id);
                                if (ie.ExcludeFlag == false)
                                {
                                    if (empSkillIdExists == null)
                                    {
                                        existFlag = false;
                                        errorMessage = L("Skill");
                                        break;
                                    }
                                }
                                else
                                {
                                    if (empSkillIdExists != null)
                                    {
                                        existFlag = false;
                                        errorMessage = L("Skill");
                                        break;
                                    }
                                }
                            }
                        }
                        else if (ie.EntityType == (int)IncentiveEntityType.GENDER)
                        {
                            string genderToBeVerified = "";
                            if (ie.EntityRefId == 1)
                            {
                                genderToBeVerified = "MALE";
                            }
                            else if (ie.EntityRefId == 2)
                            {
                                genderToBeVerified = "FEMALE";
                            }
                            if (per.Gender.ToUpper() == genderToBeVerified.ToUpper())
                            {

                            }
                            else
                            {
                                throw new UserFriendlyException(incTag.IncentiveTagCode + " " + L("Required") + " " + L("Gender") + " " + L("Not") + " " + L("Matched"));
                            }
                        }
                        else if (ie.EntityType == (int)IncentiveEntityType.RESIDENT_STATUS)
                        {
                            string residentStatustobeVerified = "";
                            var rsResStatus = await _employeeResidentStatusRepo.FirstOrDefaultAsync(t => t.Id == ie.EntityRefId);
                            if (rsResStatus == null)
                            {
                                throw new UserFriendlyException(L("NotExistForId", L("ResidentStatus"), ie.EntityRefId));
                            }
                            residentStatustobeVerified = rsResStatus.ResidentStatus;
                            List<int> allResidentStatusRefIds = new List<int>();
                            allResidentStatusRefIds = incEntitiesList.Where(t => t.EitherOr_ForSameEntity == true && ie.EntityType == (int)IncentiveEntityType.RESIDENT_STATUS && t.ExcludeFlag == false).Select(t => t.EntityRefId).ToList();
                            if (allResidentStatusRefIds.Count() == 0)
                            {
                                allResidentStatusRefIds.Add(ie.EntityRefId);
                            }
                            var rsAllResidentStatusRequired = await _employeeResidentStatusRepo.GetAllListAsync(t => allResidentStatusRefIds.Contains(t.Id));

                            var doesEmployeeHasAnyOfResidentStatus = false;
                            if (ie.ExcludeFlag == false)
                            {
                                foreach (var resStatus in rsAllResidentStatusRequired)
                                {
                                    if (resStatus.Id == per.EmployeeResidentStatusRefId)
                                    {
                                        doesEmployeeHasAnyOfResidentStatus = true;
                                        break;
                                    }
                                }
                                if (doesEmployeeHasAnyOfResidentStatus == false)
                                {
                                    var ResStatusList = "";
                                    foreach (var errskill in rsAllResidentStatusRequired)
                                    {
                                        ResStatusList = ResStatusList + errskill.ResidentStatus + ",";
                                    }
                                    if (ResStatusList.Length > 0)
                                    {
                                        ResStatusList = ResStatusList.Left(ResStatusList.Length - 1);
                                    }

                                    if (allResidentStatusRefIds.Count > 1)
                                        throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Have Any of the Resident Status Of " + ResStatusList);
                                    else
                                        throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Have the Resident Status Of " + ResStatusList);
                                }
                            }
                            else
                            {
                                allResidentStatusRefIds = new List<int>();
                                allResidentStatusRefIds = incEntitiesList.Where(t => ie.EntityType == (int)IncentiveEntityType.RESIDENT_STATUS && t.ExcludeFlag == true).Select(t => t.EntityRefId).ToList();
                                if (allResidentStatusRefIds.Count() == 0)
                                {
                                    allResidentStatusRefIds.Add(ie.EntityRefId);
                                }
                                rsAllResidentStatusRequired = await _employeeResidentStatusRepo.GetAllListAsync(t => allResidentStatusRefIds.Contains(t.Id));
                                var existResidentStatus = rsAllResidentStatusRequired.Exists(t => t.Id == per.EmployeeResidentStatusRefId);

                                if (existResidentStatus == false)
                                {
                                    var ResStatusList = "";
                                    foreach (var errskill in rsAllResidentStatusRequired)
                                    {
                                        ResStatusList = ResStatusList + errskill.ResidentStatus + ",";
                                    }
                                    if (ResStatusList.Length > 0)
                                    {
                                        ResStatusList = ResStatusList.Left(ResStatusList.Length - 1);
                                    }

                                    if (allResidentStatusRefIds.Count > 1)
                                        throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Not Have Any of the Resident Status Of " + ResStatusList);
                                    else
                                        throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Not Have the Resident Status Of " + ResStatusList);
                                }
                            }
                        }
                        else if (ie.EntityType == (int)IncentiveEntityType.RACE)
                        {
                            string raceStatustobeVerified = "";
                            var rsRaceStatus = await _employeeRaceStatusRepo.FirstOrDefaultAsync(t => t.Id == ie.EntityRefId);
                            if (rsRaceStatus == null)
                            {
                                throw new UserFriendlyException(L("NotExistForId", L("Race"), ie.EntityRefId));
                            }
                            raceStatustobeVerified = rsRaceStatus.RaceName;

                            {
                                List<int> allRaceStatusRefIds = new List<int>();
                                allRaceStatusRefIds = incEntitiesList.Where(t => t.EitherOr_ForSameEntity == true && ie.EntityType == (int)IncentiveEntityType.RACE && t.ExcludeFlag == false).Select(t => t.EntityRefId).ToList();
                                if (allRaceStatusRefIds.Count() == 0)
                                {
                                    allRaceStatusRefIds.Add(ie.EntityRefId);
                                }
                                var rsAllRaceStatusRequired = await _employeeRaceStatusRepo.GetAllListAsync(t => allRaceStatusRefIds.Contains(t.Id));

                                var doesEmployeeHasAnyOfRaceStatus = false;
                                if (ie.ExcludeFlag == false)
                                {
                                    foreach (var raceStatus in rsAllRaceStatusRequired)
                                    {
                                        if (raceStatus.Id == per.RaceRefId)
                                        {
                                            doesEmployeeHasAnyOfRaceStatus = true;
                                            break;
                                        }
                                    }
                                    if (doesEmployeeHasAnyOfRaceStatus == false)
                                    {
                                        var ResRaceList = "";
                                        foreach (var errskill in rsAllRaceStatusRequired)
                                        {
                                            ResRaceList = ResRaceList + errskill.RaceName + ",";
                                        }
                                        if (ResRaceList.Length > 0)
                                        {
                                            ResRaceList = ResRaceList.Left(ResRaceList.Length - 1);
                                        }

                                        if (allRaceStatusRefIds.Count > 1)
                                            throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Have Any of the Race Status Of " + ResRaceList);
                                        else
                                            throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Have the Race Status Of " + ResRaceList);
                                    }
                                }
                                else
                                {
                                    allRaceStatusRefIds = new List<int>();
                                    allRaceStatusRefIds = incEntitiesList.Where(t => ie.EntityType == (int)IncentiveEntityType.RACE && t.ExcludeFlag == true).Select(t => t.EntityRefId).ToList();
                                    if (allRaceStatusRefIds.Count() == 0)
                                    {
                                        allRaceStatusRefIds.Add(ie.EntityRefId);
                                    }
                                    rsAllRaceStatusRequired = await _employeeRaceStatusRepo.GetAllListAsync(t => allRaceStatusRefIds.Contains(t.Id));
                                    var existResidentStatus = rsAllRaceStatusRequired.Exists(t => t.Id==per.RaceRefId);

                                    if (existResidentStatus == false)
                                    {
                                        var raceStatusList = "";
                                        foreach (var errskill in rsAllRaceStatusRequired)
                                        {
                                            raceStatusList = raceStatusList + errskill.RaceName + ",";
                                        }
                                        if (raceStatusList.Length > 0)
                                        {
                                            raceStatusList = raceStatusList.Left(raceStatusList.Length - 1);
                                        }
                                        if (allRaceStatusRefIds.Count > 1)
                                            throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Not Have Any of the Race Status Of " + raceStatusList);
                                        else
                                            throw new UserFriendlyException(incTag.IncentiveTagCode + " - " + "Employee Should Not Have the Race Status Of " + raceStatusList);
                                    }
                                }
                            }
                        }
                    }
                    if (existFlag == false)
                    {
                        throw new UserFriendlyException(L("IncentiveEntityIssue", errorMessage));
                        //continue;
                    }
                    else
                    {
                        int i = 0;
                    }
                }
                #endregion

            }

          

            return new ReturnSuccessOrFailureWithMessage { UserErrorNumber = 0, Message = "Success" };
        }


        public async Task<List<EmployeeVsIncentiveListDto>> CreateOrUpdateManualIncentive(CreateOrUpdateManualIncentiveInput input)
        {
            var returnRules = await ExistAllServerLevelBusinessRules(input);
            if (returnRules.UserErrorNumber == 0)
            {
                if (input.ManualIncentive.Id.HasValue)
                {
                    return await UpdateManualIncentive(input);
                }
                else
                {
                    return await CreateManualIncentive(input);
                }
            }
            else
            {
                return null;
            }
        }

        public async Task DeleteManualIncentive(IdInput input)
        {
            await _manualincentiveRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task<List<EmployeeVsIncentiveListDto>> UpdateManualIncentive(CreateOrUpdateManualIncentiveInput input)
        {
            List<EmployeeVsIncentiveListDto> returnList = new List<EmployeeVsIncentiveListDto>();

            foreach (var emp in input.EmployeeList)
            {
                var item = await _manualincentiveRepo.GetAsync(input.ManualIncentive.Id.Value);
                var dto = input.ManualIncentive;

                var incTag = await _incentiveTagRepo.FirstOrDefaultAsync(t => t.Id == dto.IncentiveTagRefId);

                item.IncentiveDate = dto.IncentiveDate;
                item.EmployeeRefId = emp.EmployeeRefId;
                item.IncentiveTagRefId = dto.IncentiveTagRefId;
                item.Remarks = dto.Remarks;
                item.RecommendedUserId = dto.RecommendedUserId;
                item.ApprovedUserId = dto.ApprovedUserId;
                item.ApprovedDate = dto.ApprovedDate;
                item.ManualOtHours = dto.ManualOtHours;
                //if (incTag.IncenitveBasedOnSalaryTags == true)
                //{
                //	if (incTag.SalaryTagRefId.HasValue == false)
                //	{
                //		throw new UserFriendlyException(L("IncenitveBasedOnSalaryTags") + " ?");
                //	}
                //	var salary = await _salaryRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == emp.EmployeeRefId);
                //	if (incTag.SalaryTagRefId.Value == (int)SalaryTags.OT1)
                //	{
                //		dto.IncentiveAmount = salary.Ot1PerHour * incTag.ProportianteValueOfSalaryTags.Value;
                //	}
                //	else if (incTag.SalaryTagRefId.Value == (int)SalaryTags.OT2)
                //	{
                //		dto.IncentiveAmount = salary.Ot2PerHour * incTag.ProportianteValueOfSalaryTags.Value;
                //	}
                //	else if (incTag.SalaryTagRefId.Value == (int)SalaryTags.OT_VALUE_OF_THE_DAY)
                //	{
                //		Get_WorkDayHours_With_OT_Value workDayHoursAndOt = new Get_WorkDayHours_With_OT_Value();
                //		workDayHoursAndOt = await GetWorkHoursForGivenDate(new DateInput { DateId = input.ManualIncentive.IncentiveDate }, null, null);
                //		if (workDayHoursAndOt.OtTag == L("OT1"))
                //		{
                //			dto.IncentiveAmount = salary.Ot1PerHour * incTag.ProportianteValueOfSalaryTags.Value;
                //		}
                //		else if (workDayHoursAndOt.OtTag == L("OT2"))
                //		{
                //			dto.IncentiveAmount = salary.Ot2PerHour * incTag.ProportianteValueOfSalaryTags.Value;
                //		}
                //	}
                //}
                if (incTag.IncenitveBasedOnSalaryTags == true)
                {
                    decimal HoursForOtCalculation = 1;
                    if (incTag.OverTimeBasedIncentive)
                    {
                        HoursForOtCalculation = dto.ManualOtHours.Value;
                    }
                    if (incTag.SalaryTagRefId.HasValue == false)
                    {
                        throw new UserFriendlyException(L("IncenitveBasedOnSalaryTags") + " ?");
                    }
                    var salary = await _salaryRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == emp.EmployeeRefId);
                    if (incTag.SalaryTagRefId.Value == (int)SalaryTags.OT1)
                    {
                        dto.IncentiveAmount = HoursForOtCalculation * salary.Ot1PerHour * incTag.ProportianteValueOfSalaryTags.Value;
                    }
                    else if (incTag.SalaryTagRefId.Value == (int)SalaryTags.OT2)
                    {
                        dto.IncentiveAmount = HoursForOtCalculation * salary.Ot2PerHour * incTag.ProportianteValueOfSalaryTags.Value;
                    }
                    else if (incTag.SalaryTagRefId.Value == (int)SalaryTags.OT_VALUE_OF_THE_DAY)
                    {
                        Get_WorkDayHours_With_OT_Value workDayHoursAndOt = new Get_WorkDayHours_With_OT_Value();
                        workDayHoursAndOt = await GetWorkHoursForGivenDate(new DateInput { DateId = input.ManualIncentive.IncentiveDate }, null, null, new EmployeeBasicDetails { EmployeeRefId = emp.EmployeeRefId });
                        if (workDayHoursAndOt.OtTag == L("OT1"))
                        {
                            dto.IncentiveAmount = HoursForOtCalculation * salary.Ot1PerHour * incTag.ProportianteValueOfSalaryTags.Value;
                        }
                        else if (workDayHoursAndOt.OtTag == L("OT2"))
                        {
                            dto.IncentiveAmount = HoursForOtCalculation * salary.Ot2PerHour * incTag.ProportianteValueOfSalaryTags.Value;
                        }
                    }
                
                }
                item.IncentiveAmount = dto.IncentiveAmount;

                CheckErrors(await _manualincentiveManager.CreateSync(item));

                EmployeeVsIncentiveListDto newDet = new EmployeeVsIncentiveListDto();
                newDet.EmployeeRefId = emp.EmployeeRefId;
                newDet.IncentiveTagRefId = dto.IncentiveTagRefId;
                newDet.IncentiveTagRefCode = incTag.IncentiveTagCode;
                newDet.IncentiveAmount = dto.IncentiveAmount;
                newDet.ManualOtHours = dto.ManualOtHours;
                newDet.Remarks = dto.Remarks;
                newDet.DisplayNotApplicable = incTag.DisplayNotApplicable;
                newDet.IncludeInConsolidationPerDay = incTag.IncludeInConsolidationPerDay;
                newDet.IncentiveEntryMode = (int)IncentiveEntryMode.MANUAL;
                newDet.IncentiveEntryModeRefName = L("Manual");
                returnList.Add(newDet);
            }

            return returnList;
        }

        protected virtual async Task<List<EmployeeVsIncentiveListDto>> CreateManualIncentive(CreateOrUpdateManualIncentiveInput input)
        {
            List<EmployeeVsIncentiveListDto> returnList = new List<EmployeeVsIncentiveListDto>();

            foreach (var emp in input.EmployeeList)
            {
                var dto = input.ManualIncentive.MapTo<ManualIncentive>();
                dto.EmployeeRefId = emp.EmployeeRefId;

                var incTag = await _incentiveTagRepo.FirstOrDefaultAsync(t => t.Id == dto.IncentiveTagRefId);

                EmployeeVsIncentiveListDto newDet = new EmployeeVsIncentiveListDto();
                if (incTag.IncenitveBasedOnSalaryTags == true)
                {
                    decimal HoursForOtCalculation = 1;
                    if (incTag.OverTimeBasedIncentive)
                    {
                        HoursForOtCalculation = dto.ManualOtHours.Value;
                    }
                    if (incTag.SalaryTagRefId.HasValue == false)
                    {
                        throw new UserFriendlyException(L("IncenitveBasedOnSalaryTags") + " ?");
                    }
                    var salary = await _salaryRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == emp.EmployeeRefId);
                    if (incTag.SalaryTagRefId.Value == (int)SalaryTags.OT1)
                    {
                        dto.IncentiveAmount = HoursForOtCalculation * salary.Ot1PerHour * incTag.ProportianteValueOfSalaryTags.Value;
                    }
                    else if (incTag.SalaryTagRefId.Value == (int)SalaryTags.OT2)
                    {
                        dto.IncentiveAmount = HoursForOtCalculation * salary.Ot2PerHour * incTag.ProportianteValueOfSalaryTags.Value;
                    }
                    else if (incTag.SalaryTagRefId.Value == (int)SalaryTags.OT_VALUE_OF_THE_DAY)
                    {
                        Get_WorkDayHours_With_OT_Value workDayHoursAndOt = new Get_WorkDayHours_With_OT_Value();
                        workDayHoursAndOt = await GetWorkHoursForGivenDate(new DateInput { DateId = input.ManualIncentive.IncentiveDate }, null, null, new EmployeeBasicDetails { EmployeeRefId = emp.EmployeeRefId });
                        if (workDayHoursAndOt.OtTag == L("OT1"))
                        {
                            dto.IncentiveAmount = HoursForOtCalculation * salary.Ot1PerHour * incTag.ProportianteValueOfSalaryTags.Value;
                        }
                        else if (workDayHoursAndOt.OtTag == L("OT2"))
                        {
                            dto.IncentiveAmount = HoursForOtCalculation * salary.Ot2PerHour * incTag.ProportianteValueOfSalaryTags.Value;
                        }
                    }
                  
                    if (newDet.IncentiveAmount == 0)
                    {
                        if (salary.Ot1PerHour == 0)
                            throw new UserFriendlyException(L("OT1") + " " + L("ZeroNotAllowed"));
                        else if (salary.Ot2PerHour == 0)
                            throw new UserFriendlyException(L("OT2") + " " + L("ZeroNotAllowed"));
                        else if (salary.ReportOtPerHour==0)
                            throw new UserFriendlyException(L("Report") + " " + L("OT") + " " + L("ZeroNotAllowed"));
                        else if (salary.NormalOTPerHour == 0)
                            throw new UserFriendlyException(L("Normal") + " " + L("OT") + " " + L("ZeroNotAllowed"));
                    }
                }

                newDet.IncentiveAmount = dto.IncentiveAmount;
                newDet.ManualOtHours = dto.ManualOtHours;


                CheckErrors(await _manualincentiveManager.CreateSync(dto));

                newDet.EmployeeRefId = emp.EmployeeRefId;
                newDet.IncentiveTagRefId = dto.IncentiveTagRefId;
                newDet.IncentiveTagRefCode = incTag.IncentiveTagCode;
                newDet.Remarks = dto.Remarks;
                newDet.DisplayNotApplicable = incTag.DisplayNotApplicable;
                newDet.IncludeInConsolidationPerDay = incTag.IncludeInConsolidationPerDay;
                newDet.IncentiveEntryMode = (int)IncentiveEntryMode.MANUAL;
                newDet.IncentiveEntryModeRefName = L("Manual");
                returnList.Add(newDet);
            }
            return returnList;
        }

        public async Task<Get_WorkDayHours_With_OT_Value> GetWorkHoursForGivenDate(DateInput input, List<WorkDayListDto> rsWorkDays, List<PublicHoliday> rsPublicHolidays, EmployeeBasicDetails employeeDetails )
        {
            Get_WorkDayHours_With_OT_Value result = new Get_WorkDayHours_With_OT_Value();
            int wd = (int)input.DateId.Value.DayOfWeek;
            if (rsWorkDays == null)
            {
                var employeeDefaultWorkDays = await _salaryinfoAppService.GetEmployeeWorkDay(new GetDataBasedOnNullableEmployee { EmployeeRefId = employeeDetails.EmployeeRefId });
                rsWorkDays = employeeDefaultWorkDays.WorkDayListDtos;
            }
            var workDay = rsWorkDays.FirstOrDefault(t => t.DayOfWeekRefId == wd);
            if (workDay == null)
            {
                throw new UserFriendlyException(L("WorkDayNotSettingProperly"));
            }

            if (rsPublicHolidays == null)
                rsPublicHolidays = await _publicHolidayRepo.GetAllListAsync(t => t.HolidayDate >= input.DateId && t.HolidayDate <= input.DateId);

            if (rsPublicHolidays != null)
            {
                var ph = rsPublicHolidays.FirstOrDefault(t => t.HolidayDate.Date == input.DateId);
                if (ph != null)
                {
                    result.FullWorkHours = 0;
                    result.OtTag = "OT2";
                }
                else
                {
                    result.FullWorkHours = workDay.NoOfHourWorks;
                    if (workDay.OtRatio == (decimal)1.5)
                        result.OtTag = "OT1";
                    else
                        result.OtTag = "OT2";
                }
                return result;
            }
            else
            {
                result.FullWorkHours = workDay.NoOfHourWorks;
                if (workDay.OtRatio == (decimal)1.5)
                    result.OtTag = "OT1";
                else
                    result.OtTag = "OT2";
                return result;
            }
        }

    }
}
