﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Hr.Master
{
    public interface IPublicHolidayAppService : IApplicationService
    {
        Task<PagedResultOutput<PublicHolidayListDto>> GetAll(GetPublicHolidayInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetPublicHolidayForEditOutput> GetPublicHolidayForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdatePublicHoliday(CreateOrUpdatePublicHolidayInput input);
        Task DeletePublicHoliday(IdInput input);

        Task<ListResultOutput<PublicHolidayListDto>> GetHolidayDates();
    }
}