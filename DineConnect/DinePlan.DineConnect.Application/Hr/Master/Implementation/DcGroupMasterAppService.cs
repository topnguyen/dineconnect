﻿
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Utility.Exporter;
using DinePlan.DineConnect.Utility;

namespace DinePlan.DineConnect.Hr.Master.Implementation
{
    public class DcGroupMasterAppService : DineConnectAppServiceBase, IDcGroupMasterAppService
    {

        private readonly IExcelExporter _exporter;
        private readonly IDcGroupMasterManager _dcgroupmasterManager;
        private readonly IRepository<DcHeadMaster> _dcheadmasterRepo;
        private readonly IRepository<DcGroupMaster> _dcgroupmasterRepo;
        private readonly IRepository<DcGroupMasterVsSkillSet> _dcgroupmastervsSkillSetRepo;
        private readonly IRepository<SkillSet> _skillSetRepo;

        public DcGroupMasterAppService(IDcGroupMasterManager dcgroupmasterManager,
            IRepository<DcGroupMaster> dcGroupMasterRepo,
            IRepository<SkillSet> skillSetRepo,
            IRepository<DcGroupMasterVsSkillSet> dcgroupmastervsSkillSetRepo,
            IRepository<DcHeadMaster> dcheadmasterRepo,
        IExcelExporter exporter)
        {
            _dcgroupmasterManager = dcgroupmasterManager;
            _dcgroupmasterRepo = dcGroupMasterRepo;
            _exporter = exporter;
            _dcgroupmastervsSkillSetRepo = dcgroupmastervsSkillSetRepo;
            _skillSetRepo = skillSetRepo;
            _dcheadmasterRepo = dcheadmasterRepo;
        }

        public async Task<PagedResultOutput<DcGroupMasterListDto>> GetAll(GetDcGroupMasterInput input)
        {
            //var allItems = _dcgroupmasterRepo.GetAll();

            var allItems = (from grp in  _dcgroupmasterRepo.GetAll() 
                join head in _dcheadmasterRepo.GetAll() on 
                grp.DcHeadRefId equals head.Id
                        select new DcGroupMasterListDto
                        {
                            DcHeadRefId = head.Id,
                            DcHeadRefName = head.HeadName,
                            Id = grp.Id,
                            GroupName = grp.GroupName,
                            PriorityLevel = grp.PriorityLevel,
                            IsActive = grp.IsActive
                        });

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<DcGroupMasterListDto>>();

            var allItemCount = await allItems.CountAsync();

            foreach (var lst in allListDtos)
            {
                var skillLinked = await _dcgroupmastervsSkillSetRepo.GetAllListAsync(t => t.DcGropupRefId == lst.Id);
                if (skillLinked != null && skillLinked.Count > 0)
                {
                    List<int> skillRefIds = skillLinked.Select(t => t.SkillSetRefId).ToList();
                    lst.SkillSets = await _skillSetRepo.GetAllListAsync(t => skillRefIds.Contains(t.Id));
                    var skillNameList = "";
                    foreach(var skill in lst.SkillSets)
                    {
                        skillNameList = skillNameList + skill.SkillCode + ", ";
                    }
                    skillNameList = skillNameList.Left(skillNameList.Length - 2);
                    lst.SkillRefName = skillNameList;
                }
            }

            return new PagedResultOutput<DcGroupMasterListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _dcgroupmasterRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<DcGroupMasterListDto>>();
            var baseE = new BaseExportObject()
            {
                ExportObject = allListDtos,
                ColumnNames = new string[] { "Id" }
            };
            return _exporter.ExportToFile(baseE, L("DcGroupMaster"));
        }

        public async Task<GetDcGroupMasterForEditOutput> GetDcGroupMasterForEdit(NullableIdInput input)
        {
            DcGroupMasterEditDto editDto;
            List<ComboboxItemDto> linkedSkillSetList = new List<ComboboxItemDto>();

            if (input.Id.HasValue)
            {
                var hDto = await _dcgroupmasterRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<DcGroupMasterEditDto>();

                var skillLinked = await _dcgroupmastervsSkillSetRepo.GetAllListAsync(t => t.DcGropupRefId == editDto.Id);
                if (skillLinked != null && skillLinked.Count > 0)
                {
                    List<int> skillRefIds = skillLinked.Select(t => t.SkillSetRefId).ToList();
                    var SkillSets = await _skillSetRepo.GetAllListAsync(t => skillRefIds.Contains(t.Id));
                    foreach (var skill in SkillSets)
                    {
                        ComboboxItemDto det = new ComboboxItemDto
                        {
                            Value = skill.Id.ToString(),
                            DisplayText = skill.SkillCode
                        };
                        linkedSkillSetList.Add(det);
                    }
                }
            }
            else
            {
                editDto = new DcGroupMasterEditDto();
                linkedSkillSetList = new List<ComboboxItemDto>();
            }

            return new GetDcGroupMasterForEditOutput
            {
                DcGroupMaster = editDto,
                LinkedSkillSetList = linkedSkillSetList
            };
        }

        public async Task CreateOrUpdateDcGroupMaster(CreateOrUpdateDcGroupMasterInput input)
        {
            if (input.DcGroupMaster.Id.HasValue)
            {
                await UpdateDcGroupMaster(input);
            }
            else
            {
                await CreateDcGroupMaster(input);
            }
        }

        public async Task DeleteDcGroupMaster(IdInput input)
        {
            await _dcgroupmasterRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateDcGroupMaster(CreateOrUpdateDcGroupMasterInput input)
        {
            var item = await _dcgroupmasterRepo.GetAsync(input.DcGroupMaster.Id.Value);
            var dto = input.DcGroupMaster;

            item.GroupName = dto.GroupName;
            item.PriorityLevel = dto.PriorityLevel;
            item.IsActive = dto.IsActive;

            CheckErrors(await _dcgroupmasterManager.CreateSync(item));

            List<int> idtoBeRetained = new List<int>();

            foreach (var lst in input.LinkedSkillSetList)
            {
                var skillRefId = int.Parse(lst.Value);
                idtoBeRetained.Add(skillRefId);
                var skill = await _skillSetRepo.FirstOrDefaultAsync(t => t.Id == skillRefId);
                if (skill != null)
                {
                    DcGroupMasterVsSkillSet det = new DcGroupMasterVsSkillSet
                    {
                        DcGropupRefId = item.Id,
                        SkillSetRefId = skillRefId
                    };
                    await _dcgroupmastervsSkillSetRepo.InsertOrUpdateAndGetIdAsync(det);
                }
            }

            var delList = await _dcgroupmastervsSkillSetRepo.GetAll().Where(t => t.DcGropupRefId == item.Id && !idtoBeRetained.Contains(t.SkillSetRefId)).ToListAsync();
            foreach (var lst in delList)
            {
                await _dcgroupmastervsSkillSetRepo.DeleteAsync(lst.Id);
            }

        }

        protected virtual async Task CreateDcGroupMaster(CreateOrUpdateDcGroupMasterInput input)
        {
            var dto = input.DcGroupMaster.MapTo<DcGroupMaster>();
            dto.IsActive = true;

            CheckErrors(await _dcgroupmasterManager.CreateSync(dto));

            foreach (var lst in input.LinkedSkillSetList)
            {
                var skillRefId = int.Parse(lst.Value);
                var skill = await _skillSetRepo.FirstOrDefaultAsync(t => t.Id == skillRefId);
                if (skill != null)
                {
                    DcGroupMasterVsSkillSet det = new DcGroupMasterVsSkillSet
                    {
                        DcGropupRefId = dto.Id,
                        SkillSetRefId = skillRefId
                    };
                    await _dcgroupmastervsSkillSetRepo.InsertOrUpdateAndGetIdAsync(det);
                }
            }
        }

        public async Task<ListResultOutput<DcGroupMasterListDto>> GetGroupNames()
        {
            var lstDcGroupMaster = await _dcgroupmasterRepo.GetAll().ToListAsync();
            return new ListResultOutput<DcGroupMasterListDto>(lstDcGroupMaster.MapTo<List<DcGroupMasterListDto>>());
        }

        public async Task<ListResultOutput<DcGroupMasterListDto>> GetGroupNames(HeadCodeDto input)
        {
            var lstDcGroupMaster = await _dcgroupmasterRepo.GetAll().Where(t=>t.DcHeadRefId==input.DcHeadRefId).ToListAsync();
            return new ListResultOutput<DcGroupMasterListDto>(lstDcGroupMaster.MapTo<List<DcGroupMasterListDto>>());
        }

        public async Task<ListResultOutput<DcGroupMasterListDto>> GetGroupNamesBasedOnSkill(SkillCodeDto input)
        {
            var groupvsSkill = await _dcgroupmastervsSkillSetRepo.GetAllListAsync(t => t.SkillSetRefId == input.SkillRefId);
            List<int> groupRefIds = groupvsSkill.Select(t => t.DcGropupRefId).ToList();
            var lstDcGroupMaster = await _dcgroupmasterRepo.GetAll().Where(t => groupRefIds.Contains(t.Id)).ToListAsync();
            return new ListResultOutput<DcGroupMasterListDto>(lstDcGroupMaster.MapTo<List<DcGroupMasterListDto>>());
        }
    }
}