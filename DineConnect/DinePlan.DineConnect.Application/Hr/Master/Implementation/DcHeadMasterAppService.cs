﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Utility.Exporter;
using DinePlan.DineConnect.Utility;

namespace DinePlan.DineConnect.Hr.Master.Implementation
{
    public class DcHeadMasterAppService : DineConnectAppServiceBase, IDcHeadMasterAppService
    {

        private readonly IExcelExporter _exporter;
        private readonly IDcHeadMasterManager _dcheadmasterManager;
        private readonly IRepository<DcHeadMaster> _dcheadmasterRepo;
        private readonly IRepository<DcGroupMaster> _dcGroupMaster;

        public DcHeadMasterAppService(IDcHeadMasterManager dcheadmasterManager,
            IRepository<DcHeadMaster> dcHeadMasterRepo,
            IRepository<DcGroupMaster> dcGroupMaster,
            IExcelExporter exporter)
        {
            _dcheadmasterManager = dcheadmasterManager;
            _dcheadmasterRepo = dcHeadMasterRepo;
            _exporter = exporter;
            _dcGroupMaster = dcGroupMaster;
        }

        public async Task<PagedResultOutput<DcHeadMasterListDto>> GetAll(GetDcHeadMasterInput input)
        {
            var allItems = _dcheadmasterRepo.GetAll();
            if (input.Operation == "SEARCH")
            {
                allItems = _dcheadmasterRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.HeadName.Equals(input.Filter)
               );
            }
            else
            {
                allItems = _dcheadmasterRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.HeadName.Contains(input.Filter)
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<DcHeadMasterListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<DcHeadMasterListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {


            var allList = await _dcheadmasterRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<DcHeadMasterListDto>>();
            var baseE = new BaseExportObject()
            {
                ExportObject = allListDtos,
                ColumnNames = new string[] { "Id" }
            };
            return _exporter.ExportToFile(baseE, L("DcHeadMaster"));

        }

        public async Task<GetDcHeadMasterForEditOutput> GetDcHeadMasterForEdit(NullableIdInput input)
        {
            DcHeadMasterEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _dcheadmasterRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<DcHeadMasterEditDto>();
            }
            else
            {
                editDto = new DcHeadMasterEditDto();
            }

            return new GetDcHeadMasterForEditOutput
            {
                DcHeadMaster = editDto
            };
        }

        public async Task CreateOrUpdateDcHeadMaster(CreateOrUpdateDcHeadMasterInput input)
        {
            if (input.DcHeadMaster.Id.HasValue)
            {
                await UpdateDcHeadMaster(input);
            }
            else
            {
                await CreateDcHeadMaster(input);
            }
        }

        public async Task DeleteDcHeadMaster(IdInput input)
        {
            var existAnyReference = await _dcGroupMaster.FirstOrDefaultAsync(t => t.DcHeadRefId == input.Id);
            if (existAnyReference != null)
            {
                throw new UserFriendlyException(L("ReferenceExists", L("DcHeadMaster"), L("DcGroupMaster")));
            }
            await _dcheadmasterRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateDcHeadMaster(CreateOrUpdateDcHeadMasterInput input)
        {
            var item = await _dcheadmasterRepo.GetAsync(input.DcHeadMaster.Id.Value);
            var dto = input.DcHeadMaster;

            item.HeadName = dto.HeadName;
            item.IsActive = dto.IsActive;

            CheckErrors(await _dcheadmasterManager.CreateSync(item));
        }

        protected virtual async Task CreateDcHeadMaster(CreateOrUpdateDcHeadMasterInput input)
        {
            var dto = input.DcHeadMaster.MapTo<DcHeadMaster>();
            dto.IsActive = true;
            CheckErrors(await _dcheadmasterManager.CreateSync(dto));
        }

        public async Task<ListResultOutput<DcHeadMasterListDto>> GetHeadNames()
        {
            var lstDcHeadMaster = await _dcheadmasterRepo.GetAll().ToListAsync();
            return new ListResultOutput<DcHeadMasterListDto>(lstDcHeadMaster.MapTo<List<DcHeadMasterListDto>>());
        }

        public async Task<IdInput> GetHeaderIdByName(string headName)
        {
            var lstId = await _dcheadmasterRepo.FirstOrDefaultAsync(t => t.HeadName.ToUpper().Equals(headName.ToUpper()));

            if (lstId == null)
            {
                return new IdInput
                {
                    Id = 0
                };
            }
            else
            {
                return new IdInput
                {
                    Id = lstId.Id
                };
            }
        }
    }
}
