﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Hr.Impl;
using System;
using DinePlan.DineConnect.Storage;
using Abp.UI;
using Abp.Domain.Uow;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.MultiTenancy;
using Abp.Configuration.Startup;
using Microsoft.AspNet.Identity;
using Abp.Notifications;
using Abp.Authorization.Users;
using DinePlan.DineConnect.Authorization.Roles;
using DinePlan.DineConnect.Notifications;
using Abp.Runtime.Session;
using DinePlan.DineConnect.Hr;
using System.Text;
using Abp.Net.Mail;
using System.Net.Mail;
using DinePlan.DineConnect.Sessions.Dto;
using DinePlan.DineConnect.Configuration;
using Abp.Authorization;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Hr.Report.Dto;

namespace DinePlan.DineConnect.Hr.Master.Implementation
{
    public class PersonalInformationAppService : DineConnectAppServiceBase, IPersonalInformationAppService
    {
        private readonly IPersonalInformationListExcelExporter _personalinformationExporter;
        private readonly IPersonalInformationManager _personalinformationManager;
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly IRepository<PersonalInformation> _personalinformationRepo;
        private readonly IRepository<Company> _companyRepo;
        private readonly IRepository<EmployeeSkillSet> _employeeskillsetRepo;
        private readonly IRepository<SkillSet> _skillsetRepo;
        private readonly IRepository<SalaryInfo> _salaryinfoRepo;
        private IEmployeeSkillSetAppService _employeeskillsetappservice;
        private readonly IMultiTenancyConfig _multiTenancyConfig;
        private readonly TenantManager _tenantManager;
        private readonly INotificationSubscriptionManager _notificationSubscriptionManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly RoleManager _roleManager;
        private readonly UserManager _userManager;
        private readonly IUserEmailer _userEmailer;
        private readonly IAppNotifier _appNotifier;
        private readonly IRepository<UserDefaultInformation> _userdefaultinformationRepo;
        private readonly IRepository<LeaveRequest> _leaverequestRepo;
        private readonly IRepository<GpsAttendance> _gpsattendanceRepo;
        private readonly IRepository<GpsLive> _gpsliveRepo;
        private readonly IRepository<LeaveType> _leavetypeRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<EmployeeCostCentreMaster> _employeeCostCentreMasterRepo;
        private readonly IRepository<EmployeeDepartmentMaster> _employeeDepartmentMasterRepo;
        private readonly IRepository<EmployeeResidentStatus> _employeeResidentStatusRepo;
        private readonly IRepository<EmployeeRace> _employeeRaceStatusRepo;
        private readonly IRepository<EmployeeReligion> _employeeReligionStatusRepo;
        private readonly IRepository<EmployeeRawInfo> _emRawManager;
        private readonly IRepository<JobTitleMaster> _jobTitleMasterRepo;
        

        public PersonalInformationAppService(IPersonalInformationManager personalinformationManager,
            IRepository<PersonalInformation> personalInformationRepo, IBinaryObjectManager binaryObjectManager,
            IPersonalInformationListExcelExporter personalinformationExporter,
            IMultiTenancyConfig multiTenancyConfig,
            TenantManager tenantManager,
            IRepository<Company> companyRepo,
            IRepository<EmployeeSkillSet> employeeskillsetRepo,
            IRepository<SalaryInfo> salaryinfoRepo,
            IRepository<SkillSet> skillsetRepo,
            INotificationSubscriptionManager notificationSubscriptionManager,
            IEmployeeSkillSetAppService employeeskillsetappservice,
            IUnitOfWorkManager unitOfWorkManager,
            RoleManager roleManager,
            IRepository<UserDefaultInformation> userdefaultinformationRepo,
            UserManager userManager,
            IUserEmailer userEmailer,
            IAppNotifier appNotifier,
               IRepository<LeaveRequest> leaverequestRepo,
               IRepository<GpsAttendance> gpsattendanceRepo,
               IRepository<GpsLive> gpsliveRepo,
            IRepository<LeaveType> leavetypeRepo,
            IRepository<Location> locationRepo,
            IRepository<EmployeeCostCentreMaster> employeeCostCentreMasterRepo,
            IRepository<EmployeeDepartmentMaster> employeeDepartmentMasterRepo,
            IRepository<EmployeeResidentStatus> employeeResidentStatusRepo,
            IRepository<EmployeeRace> employeeRaceStatusRepo,
            IRepository<EmployeeReligion> employeeReligionStatusRepo,
            IRepository<EmployeeRawInfo> emRawManager,
            IRepository<JobTitleMaster> jobTitleMasterRepo

            )
        {
            _personalinformationManager = personalinformationManager;
            _personalinformationRepo = personalInformationRepo;
            _personalinformationExporter = personalinformationExporter;
            _companyRepo = companyRepo;
            _skillsetRepo = skillsetRepo;
            _binaryObjectManager = binaryObjectManager;
            _employeeskillsetRepo = employeeskillsetRepo;
            _salaryinfoRepo = salaryinfoRepo;
            _employeeskillsetappservice = employeeskillsetappservice;
            _multiTenancyConfig = multiTenancyConfig;
            _tenantManager = tenantManager;
            _notificationSubscriptionManager = notificationSubscriptionManager;
            _unitOfWorkManager = unitOfWorkManager;
            _roleManager = roleManager;
            _userManager = userManager;
            _userEmailer = userEmailer;
            _appNotifier = appNotifier;
            _userdefaultinformationRepo = userdefaultinformationRepo;
            _leaverequestRepo = leaverequestRepo;
            _gpsattendanceRepo = gpsattendanceRepo;
            _gpsliveRepo = gpsliveRepo;
            _leavetypeRepo = leavetypeRepo;
            _locationRepo = locationRepo;
            _employeeCostCentreMasterRepo = employeeCostCentreMasterRepo;
            _employeeDepartmentMasterRepo = employeeDepartmentMasterRepo;
            _employeeResidentStatusRepo = employeeResidentStatusRepo;
            _employeeRaceStatusRepo = employeeRaceStatusRepo;
            _employeeReligionStatusRepo = employeeReligionStatusRepo;
            _emRawManager = emRawManager;
            _jobTitleMasterRepo = jobTitleMasterRepo;
        }

        public async Task<PagedResultOutput<PersonalInformationListDto>> GetAll(GetPersonalInformationInput input)
        {
            var rsCompany = _companyRepo.GetAll();
            List<int> companiesRefIds = new List<int>();
            if (input.Companies != null && input.Companies.Count > 0)
            {
                companiesRefIds = input.Companies.Select(t => t.Id).ToList();
                rsCompany = rsCompany.Where(t => companiesRefIds.Contains(t.Id));
            }

            var PerInfo = _personalinformationRepo.GetAll();
            if (input.JobTitleRefId > 0)
                PerInfo = PerInfo.Where(t => t.JobTitleRefId == input.JobTitleRefId);

            PerInfo = PerInfo.WhereIf(input.DefaultSkillRefId > 0, p => p.DefaultSkillRefId == (input.DefaultSkillRefId));

            if (input.ResignedFlag)
            {
                PerInfo = PerInfo.Where(t => t.LastWorkingDate != null);
            }

            if (input.ActiveFlag)
            {
                PerInfo = PerInfo.Where(t => t.LastWorkingDate == null);
            }

            if (input.StartDate != null && input.EndDate != null)
            {
                PerInfo = PerInfo.Where(t => t.DateHired >= input.StartDate && t.DateHired <= input.EndDate);
            }

            if (!input.EmployeeRefCode.IsNullOrEmpty())
            {
                PerInfo = PerInfo.Where(t => t.EmployeeCode.ToUpper().Contains(input.EmployeeRefCode.ToUpper()));
            }

            if (!input.Filter.IsNullOrEmpty())
            {
                PerInfo = PerInfo.Where(t => t.EmployeeName.ToUpper().Contains(input.Filter.ToUpper()));
            }

            if (input.SupervisorEmployeeRefId.HasValue)
            {
                PerInfo = PerInfo.Where(t => t.SupervisorEmployeeRefId == input.SupervisorEmployeeRefId.Value);
            }

            if (input.InchargeEmployeeRefId.HasValue)
            {
                PerInfo = PerInfo.Where(t => t.InchargeEmployeeRefId == input.InchargeEmployeeRefId.Value);
            }

            var allItems = (from perso in PerInfo
                            join loc in _locationRepo.GetAll()
                            on perso.LocationRefId equals loc.Id
                            join com in rsCompany
                            on loc.CompanyRefId equals com.Id
                            join skill in _skillsetRepo.GetAll() on
                            perso.DefaultSkillRefId equals skill.Id
                            join jt in _jobTitleMasterRepo.GetAll() on perso.JobTitleRefId equals jt.Id
                            select new PersonalInformationListDto()
                            {
                                Id = perso.Id,
                                CompanyRefName = com.Name,
                                CompanyRefCode = com.Code,
                                EmployeeCode = perso.EmployeeCode,
                                EmployeeName = perso.EmployeeName,
                                JobTitle = jt.JobTitle,
                                JobTitleRefId = perso.JobTitleRefId.Value,
                                DefaultSkillRefId = perso.DefaultSkillRefId,
                                DefaultSkillSetCode = skill.SkillCode,
                                BioMetricCode = perso.BioMetricCode,
                                UserSerialNumber = perso.UserSerialNumber,
                                DateOfBirth = perso.DateOfBirth,
                                Gender = perso.Gender,
                                DateHired = perso.DateHired,
                                StaffType = perso.StaffType,
                                CreationTime = perso.CreationTime,
                                LastWorkingDate = perso.LastWorkingDate,
                                ActiveStatus = perso.ActiveStatus,
                                Email = perso.Email,
                                PersonalEmail = perso.PersonalEmail,
                                PostalCode = perso.PostalCode,
                                PassPortNumber = perso.PassPortNumber,
                                IsAttendanceRequired = perso.IsAttendanceRequired,
                                CompanyRefId = loc.CompanyRefId,
                                IsFixedCostCentreInDutyChart = perso.IsFixedCostCentreInDutyChart,
                                DutyFixedUptoDate = perso.DutyFixedUptoDate,
                                IsSmartAttendanceFingerAllowed = perso.IsSmartAttendanceFingerAllowed,
                                SupervisorEmployeeRefId = perso.SupervisorEmployeeRefId,
                                InchargeEmployeeRefId = perso.InchargeEmployeeRefId
                            }
               );

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<PersonalInformationListDto>>();

            var allItemCount = await allItems.CountAsync();

            List<int> arrEmployeeRefIds = allListDtos.Select(t => t.Id).ToList();
            var rsuserWithEmployeeLinkExists = await _userdefaultinformationRepo.GetAllListAsync(t => t.EmployeeRefId.HasValue && arrEmployeeRefIds.Contains(t.EmployeeRefId.Value));

            List<int> arrSupInchargeRefIds = new List<int>();
            arrSupInchargeRefIds.AddRange(allListDtos.Where(t => t.SupervisorEmployeeRefId.HasValue).Select(t => t.SupervisorEmployeeRefId.Value).ToList());
            arrSupInchargeRefIds.AddRange(allListDtos.Where(t => t.InchargeEmployeeRefId.HasValue).Select(t => t.InchargeEmployeeRefId.Value).ToList());

            var rsSuperInchargeList = await _personalinformationRepo.GetAllListAsync(t => arrSupInchargeRefIds.Contains(t.Id));


            foreach (var lst in allListDtos)
            {
                var userWithEmployeeLinkExists = rsuserWithEmployeeLinkExists.FirstOrDefault(t => t.EmployeeRefId == lst.Id);
                if (userWithEmployeeLinkExists != null)
                {
                    var user = await _userManager.FindByIdAsync(userWithEmployeeLinkExists.UserId);
                    if (user != null)
                        lst.LoginUserName = user.UserName;
                }
                if (lst.SupervisorEmployeeRefId.HasValue)
                {
                    var sup = rsSuperInchargeList.FirstOrDefault(t => t.Id == lst.SupervisorEmployeeRefId);
                    if (sup != null)
                        lst.SupervisorEmployeeRefName = sup.EmployeeName;
                }
                if (lst.InchargeEmployeeRefId.HasValue)
                {
                    var sup = rsSuperInchargeList.FirstOrDefault(t => t.Id == lst.InchargeEmployeeRefId);
                    if (sup != null)
                        lst.InchargeEmployeeRefName = sup.EmployeeName;
                }
            }

            return new PagedResultOutput<PersonalInformationListDto>(
                allItemCount,
                 allListDtos
                );
        }

        public async Task<PagedResultOutput<PersonalInformationListDto>> GetEmployeeBasedOnEmployeeCode(GetPersonalInformationInput input)
        {
            var comp = _companyRepo.GetAll().WhereIf(!input.Company.IsNullOrEmpty(), c => c.Code.Contains(input.Company));

            var PerInfo = _personalinformationRepo.GetAll().Where(t => t.EmployeeCode.Equals(input.EmployeeRefCode));
            if (!PerInfo.Any())
            {
                PerInfo = _personalinformationRepo.GetAll().Where(t => t.BioMetricCode.Equals(input.EmployeeRefCode));
            }
            if (!PerInfo.Any())
            {
                PerInfo = _personalinformationRepo.GetAll().Where(t => t.EmployeeName.Equals(input.EmployeeRefCode));
            }

            var allItems = (from perso in PerInfo
                            join loc in _locationRepo.GetAll()
                          on perso.LocationRefId equals loc.Id
                            join com in comp
                            on loc.CompanyRefId equals com.Id
                            join skill in _skillsetRepo.GetAll()
                            on perso.DefaultSkillRefId equals skill.Id
                            join jt in _jobTitleMasterRepo.GetAll() on perso.JobTitleRefId equals jt.Id
                            select new PersonalInformationListDto()
                            {
                                Id = perso.Id,
                                CompanyRefName = com.Name,
                                CompanyRefCode = com.Code,
                                EmployeeCode = perso.EmployeeCode,
                                EmployeeName = perso.EmployeeName,
                                JobTitle = jt.JobTitle,
                                JobTitleRefId = perso.JobTitleRefId.Value,
                                DefaultSkillRefId = perso.DefaultSkillRefId,
                                DefaultSkillSetCode = skill.SkillCode,
                                BioMetricCode = perso.BioMetricCode,
                                UserSerialNumber = perso.UserSerialNumber,
                                DateOfBirth = perso.DateOfBirth,
                                Gender = perso.Gender,
                                DateHired = perso.DateHired,
                                StaffType = perso.StaffType,
                                CreationTime = perso.CreationTime,
                                LastWorkingDate = perso.LastWorkingDate,
                                ActiveStatus = perso.ActiveStatus,
                                Email = perso.Email,
                                PersonalEmail = perso.PersonalEmail,
                                PostalCode = perso.PostalCode,
                                PassPortNumber = perso.PassPortNumber,
                                IsAttendanceRequired = perso.IsAttendanceRequired,
                                CompanyRefId = com.Id
                            }
                 );

            var sortMenuItems = await allItems
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<PersonalInformationListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<PersonalInformationListDto>(
                    allItemCount,
                    allListDtos
                    );
        }




        public async Task<PagedResultOutput<PersonalInformationListDto>> GetStaffTypeForSelectedEmp(GetPersonalInformationInput input)
        {
            var allItems = (from perso in _personalinformationRepo.GetAll().Where(t => t.Id.Equals(input.EmployeeRefId))
                            select new PersonalInformationListDto()
                            {
                                Id = perso.Id,
                                EmployeeCode = perso.EmployeeCode,
                                EmployeeName = perso.EmployeeName,
                                StaffType = perso.StaffType,
                                CreationTime = perso.CreationTime
                            }).WhereIf(
                               !input.Filter.IsNullOrEmpty(),
                               p => p.EmployeeName.Contains(input.Filter)
               );

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<PersonalInformationListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<PersonalInformationListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetPersonalInformationInput input)
        {
            input.MaxResultCount = 10000;

            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<PersonalInformationListDto>>();
            return _personalinformationExporter.PersonalInformationExportToFile(allListDtos);
        }

        public async Task<GetPersonalInformationForEditOutput> GetPersonalInformationForEdit(NullableIdInput input)
        {
            PersonalInformationEditDto editDto;
            List<ComboboxItemDto> skilldetailDto = new List<ComboboxItemDto>();

            if (input.Id.HasValue)
            {
                var hDto = await _personalinformationRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<PersonalInformationEditDto>();

                var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == editDto.LocationRefId);
                editDto.LocationRefName = location.Name;

                var company = await _companyRepo.FirstOrDefaultAsync(t => t.Id == location.CompanyRefId);
                editDto.CompanyRefName = company.Name;

                var userWithEmployeeLinkExists = await _userdefaultinformationRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == input.Id.Value);
                if (userWithEmployeeLinkExists != null)
                {
                    var user = await _userManager.FindByIdAsync(userWithEmployeeLinkExists.UserId);
                    if (user != null)
                        editDto.LoginUserName = user.UserName;
                }


                skilldetailDto = await (from es in _employeeskillsetRepo.GetAll().Where(t => t.EmployeeRefId == input.Id.Value)
                                        join skill in _skillsetRepo.GetAll() on
                                        es.SkillSetRefId equals skill.Id
                                        select new ComboboxItemDto
                                        {
                                            Value = skill.Id.ToString(),
                                            DisplayText = skill.SkillCode
                                        }).ToListAsync();

            }
            else
            {
                editDto = new PersonalInformationEditDto();
                editDto.IsDayCloseStatusMailRequired = true;
                skilldetailDto = new List<ComboboxItemDto>();
            }

            return new GetPersonalInformationForEditOutput
            {
                PersonalInformation = editDto,
                SkillSetList = skilldetailDto
            };
        }


        public async Task<bool> ExistAllServerLevelBusinessRules(CreateOrUpdatePersonalInformationInput input)
        {

            int defaultSkillRefId = input.PersonalInformation.DefaultSkillRefId;

            if (defaultSkillRefId == 0)
            {
                throw new UserFriendlyException(L("DefaultSkillSetCodeErr"));
            }

            var skillExists =await _skillsetRepo.FirstOrDefaultAsync(t => t.Id == (defaultSkillRefId));

            if (skillExists == null)
            {
                throw new UserFriendlyException(L("SkillCodeDoesNotExist", defaultSkillRefId));
            }

            DateTime dob = input.PersonalInformation.DateOfBirth;
            TimeSpan diff = DateTime.Now.Subtract(dob);
            //  App Setting HR - Minimum Age For Employeement 
            int minimumAge = 14;
            if (Math.Abs(diff.TotalDays) < (minimumAge * 365))
            {
                throw new UserFriendlyException(L("AgeIssue_LessThan_MinimumAge", minimumAge));
            }

            if (dob > DateTime.Now)
            {
                throw new UserFriendlyException(L("FutureDateErr"));
            }

            DateTime datehired = input.PersonalInformation.DateHired;
            int maxFutureDays = 45;
            if (datehired > DateTime.Now.AddDays(maxFutureDays))
            {
                throw new UserFriendlyException(L("FutureDateNDaysErr", maxFutureDays));
            }

            if (datehired.Subtract(dob).TotalDays < (minimumAge * 365))
            {
                throw new UserFriendlyException(L("MismatchBetweenDOBAndDateHired"));
            }

            //if (input.PersonalInformation.InchargeEmployeeRefId.HasValue)
            //{
            //    var inchargeEmp = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == input.PersonalInformation.InchargeEmployeeRefId);
            //    if (inchargeEmp.ActiveStatus == false)
            //    {
            //        throw new UserFriendlyException(L("InchargeEmployeeShouldBeActive", inchargeEmp.EmployeeName));
            //    }
            //}
            if (input.PersonalInformation.Id.HasValue)
            {
                var empCode = await _personalinformationRepo.FirstOrDefaultAsync(t => t.EmployeeCode == input.PersonalInformation.EmployeeCode);
                if (empCode != null)
                {
                    if (empCode.Id != input.PersonalInformation.Id)
                    {
                        throw new UserFriendlyException(L("EmployeeCodeAlreadyExists") + " " + empCode.EmployeeName);
                    }
                }
                else
                {

                }
            }
            return true;

        }

        public async Task<IdInput> CreateOrUpdatePersonalInformation(CreateOrUpdatePersonalInformationInput input)
        {
            var result = await ExistAllServerLevelBusinessRules(input);
            if (result==true)
            {

                if (input.PersonalInformation.Id.HasValue)
                {
                    return await UpdatePersonalInformation(input);
                }
                else
                {
                    return await CreatePersonalInformation(input);
                }
            }
            else
            {
                return new IdInput { Id = -1 };
            }
        }

        public async Task DeletePersonalInformation(IdInput input)
        {
            //await _personalinformationRepo.DeleteAsync(input.Id);
        }



        public async Task<IdInput> UpdateResignationPersonalInformation(CreateOrUpdatePersonalInformationInput input)
        {

            var dto = input.PersonalInformation;

            if (dto.LastWorkingDate == null)
            {
                throw new UserFriendlyException(L("OfficialLastWorkingDate") + " " + L("Error"));
            }

            if (dto.LastWorkingDate == null)
            {
                throw new UserFriendlyException(L("LastWorkingDate") + " " + L("Error"));
            }

            DateTime dateLastWorkingDate = dto.LastWorkingDate.Value;

            if (dateLastWorkingDate > DateTime.Now)
            {
                throw new UserFriendlyException(L("FutureDateErr"));
            }

            DateTime resingedDateNext = dto.LastWorkingDate.Value.AddDays(1);
            var employeeRefId = dto.Id;

            //var existAnyGatePassPending = await _gatepassmasterRepo.GetAll().Where(t => t.CompletedTime == null && t.AccountableEmployeeRefId == employeeRefId).ToListAsync();

            //if (existAnyGatePassPending.Count() > 0)
            //{
            //    string errorMessage = L("GatePassPending") + " : ";

            //    foreach (var lst in existAnyGatePassPending)
            //    {
            //        errorMessage = errorMessage + lst.GatePassReference + ",";
            //    }
            //    errorMessage = errorMessage.Left(errorMessage.Length - 1);
            //    throw new UserFriendlyException(errorMessage);
            //}
            var rejectedString = L("Rejected");
            var rsLeaveRecords = await _leaverequestRepo.GetAll().Where(t => t.EmployeeRefId == employeeRefId
            && resingedDateNext >= t.LeaveFrom && resingedDateNext <= t.LeaveTo && t.LeaveStatus != rejectedString).ToListAsync();

            if (rsLeaveRecords.Count() > 0)
            {
                var existLeaveRecords = rsLeaveRecords.MapTo<List<LeaveRequestListDto>>();
                foreach (var lst in existLeaveRecords)
                {
                    if (lst.LeaveFrom > input.PersonalInformation.LastWorkingDate.Value)
                    {
                        await _leaverequestRepo.DeleteAsync(lst.Id);
                        continue;
                    }
                    if (lst.LeaveTo > input.PersonalInformation.LastWorkingDate.Value)
                    {
                        var editLeavedto = await _leaverequestRepo.GetAsync(lst.Id);
                        editLeavedto.LeaveTo = input.PersonalInformation.LastWorkingDate.Value;
                        await _leaverequestRepo.UpdateAsync(editLeavedto);
                    }
                    //else
                    //{
                    //	throw new UserFriendlyException(L("LeaveAlreadyApplied", lst.LeaveFrom.ToString("yyyy-MMM-dd"), lst.LeaveTo.ToString("yyyy-MMM-dd"), lst.LeaveReason) + " " + L("LeaveEntryCanNotBeAfterResignDate"));
                    //}
                }
            }

            //var lstIncentives = await _manualIncentiveRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == input.PersonalInformation.Id
            //&& t.IncentiveDate > input.PersonalInformation.LastWorkingDate.Value);

            //if (lstIncentives != null)
            //{
            //    throw new UserFriendlyException(L("ManualIncentiveAssignedAfterTheResignedDate"));
            //}


            //var lstDutyCharts = await _dutychartdetailRepo.GetAll()
            //    .Where(t => t.EmployeeRefId == input.PersonalInformation.Id
            //    && t.DutyChartDate > input.PersonalInformation.LastOfficialWorkingDate).ToListAsync();

            //foreach (var lst in lstDutyCharts)
            //{
            //    await _dutychartdetailRepo.DeleteAsync(lst.Id);
            //}

            var item = await _personalinformationRepo.GetAsync(input.PersonalInformation.Id.Value);
            //TODO: SERVICE PersonalInformation Update Individually
            item.ActiveStatus = false;
            item.LastWorkingDate = dto.LastWorkingDate;

            var userDefault = await _userdefaultinformationRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == input.PersonalInformation.Id.Value);
            if (userDefault != null)
            {
                var userSystem = await _userManager.FindByIdAsync(userDefault.UserId);
                userSystem.IsActive = false;
                await _userManager.UpdateAsync(userSystem);
            }

            CheckErrors(await _personalinformationManager.CreateSync(item));

            return new IdInput { Id = dto.Id.Value };

        }

        protected virtual async Task<IdInput> UpdatePersonalInformation(CreateOrUpdatePersonalInformationInput input)
        {
            var item = await _personalinformationRepo.GetAsync(input.PersonalInformation.Id.Value);
            var dto = input.PersonalInformation;

            dto.UserSerialNumber = Math.Ceiling(dto.UserSerialNumber);

            item.LocationRefId = dto.LocationRefId;
            item.EmployeeCode = dto.EmployeeCode;
            item.EmployeeGivenName = dto.EmployeeGivenName;
            item.EmployeeSurName = dto.EmployeeSurName;
            item.EmployeeName = dto.EmployeeName;
            item.NationalIdentificationNumber = dto.NationalIdentificationNumber;
            item.StaffType = dto.StaffType;
            item.DefaultSkillRefId = dto.DefaultSkillRefId;
            item.Gender = dto.Gender;
            item.DateHired = dto.DateHired;
            item.JobTitleRefId = dto.JobTitleRefId;
            item.ActiveStatus = dto.ActiveStatus;
            item.LastWorkingDate = dto.LastWorkingDate;
            item.MaritalStatus = dto.MaritalStatus;
            item.DateOfBirth = dto.DateOfBirth;
            item.PassPortNumber = dto.PassPortNumber;
            item.UserSerialNumber = dto.UserSerialNumber;
            item.BioMetricCode = dto.BioMetricCode;
            item.ProfilePictureId = dto.ProfilePictureId;
            item.DefaultWeekOff = (int)DayOfWeek.Sunday;
            item.IsAttendanceRequired = dto.IsAttendanceRequired;
            item.ActiveStatus = dto.ActiveStatus;
            item.Email = dto.Email;
            item.PersonalEmail = dto.PersonalEmail;
            item.PerAddress1 = dto.PerAddress1;
            item.PerAddress2 = dto.PerAddress2;
            item.PerAddress3 = dto.PerAddress3;
            item.PerAddress4 = dto.PerAddress4;
            item.ComAddress1 = dto.ComAddress1;
            item.ComAddress2 = dto.ComAddress2;
            item.ComAddress3 = dto.ComAddress3;
            item.ComAddress4 = dto.ComAddress4;
            item.PostalCode = dto.PostalCode;
            item.ZipCode = dto.ZipCode;
            item.Email = dto.Email;
            item.HandPhone1 = dto.HandPhone1;
            item.HandPhone2 = dto.HandPhone2;
            item.IncentiveAllowedFlag = dto.IncentiveAllowedFlag;
            item.SupervisorEmployeeRefId = dto.SupervisorEmployeeRefId;
            item.InchargeEmployeeRefId = dto.InchargeEmployeeRefId;

            item.IsFixedCostCentreInDutyChart = dto.IsFixedCostCentreInDutyChart;
            item.DutyFixedUptoDate = dto.DutyFixedUptoDate;
            item.IsSmartAttendanceFingerAllowed = dto.IsSmartAttendanceFingerAllowed;
            item.IsDayCloseStatusMailRequired = dto.IsDayCloseStatusMailRequired;
            item.RaceRefId = dto.RaceRefId;
            item.EmployeeResidentStatusRefId = dto.EmployeeResidentStatusRefId;
            item.EmployeeCostCentreRefId = dto.EmployeeCostCentreRefId;
            item.ReligionRefId = dto.ReligionRefId;

            item.PRStartDate = dto.PRStartDate;
            item.PREndDate = dto.PREndDate;
            item.PRNumber = dto.PRNumber;
            item.DinePlanUserId = dto.DinePlanUserId;

            CheckErrors(await _personalinformationManager.CreateSync(item));

            //if (OldSkillSet != item.DefaultSkillSetCode)
            {
                var skill = await _skillsetRepo.FirstOrDefaultAsync(t => t.Id == (dto.DefaultSkillRefId));

                List<ComboboxItemDto> skillSetList = new List<ComboboxItemDto>();

                ComboboxItemDto s = new ComboboxItemDto();
                s.Value = skill.Id.ToString();
                s.DisplayText = skill.SkillCode;
                skillSetList.Add(s);

                if (input.SkillSetList != null)
                {
                    foreach (var lst in input.SkillSetList)
                    {
                        var alreadySkillExists = skillSetList.FirstOrDefault(t => t.Value == lst.Value);
                        if (alreadySkillExists == null)
                        {
                            ComboboxItemDto newSkill = new ComboboxItemDto();
                            newSkill.Value = lst.Value;
                            newSkill.DisplayText = lst.DisplayText;
                            skillSetList.Add(newSkill);
                        }
                    }
                }

                EmployeeSkillSetEditDto employeeSkillDetail = new EmployeeSkillSetEditDto();
                employeeSkillDetail.EmployeeRefId = item.Id;
                employeeSkillDetail.SkillSetRefId = skill.Id;

                CreateOrUpdateEmployeeSkillSetInput empSkillsetDto = new CreateOrUpdateEmployeeSkillSetInput
                {
                    EmployeeSkillSet = employeeSkillDetail,
                    SkillSetList = skillSetList
                };

                await _employeeskillsetappservice.CreateOrUpdateEmployeeSkillSet(empSkillsetDto);
            }

            if (input.UpdateSerialNumberRequired == true)
                await UpdateUserSerialNumber(new IdInput { Id = dto.Id.Value });

            if (input.PersonalInformation.ActiveStatus)
            {
                var userExistAlready = await _userdefaultinformationRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == dto.Id.Value);

                if (userExistAlready == null && !dto.LoginUserName.IsNullOrEmpty() && dto.LoginUserName.Length > 0)
                {
                    var email = dto.Email;
                    if (email == null || email.Length == 0)
                    {
                        email = dto.PersonalEmail;
                        if (email == null || email.Length == 0)
                        {
                            email = "";
                        }
                        dto.Email = email;
                    }

                    string password = "123456";
                    if (userExistAlready == null)
                    {
                        password = "123456";
                    }
                    else
                    {

                    }

                    var tenant = await _tenantManager.GetByIdAsync(AbpSession.TenantId.Value);

                    var user = await _userManager.FindByNameOrEmailAsync(dto.LoginUserName);
                    if (user == null)
                    {

                        string nameToDisplay = "";
                        string surnameToDisplay = "";

                        string[] nameArray = dto.EmployeeName.ToUpper().Trim().Split(' ');
                        int idx = 0;
                        foreach (var name in nameArray)
                        {
                            if (idx == 0)
                            {
                                surnameToDisplay = name;
                            }
                            else
                            {
                                nameToDisplay = nameToDisplay + name + " ";
                            }
                            idx++;
                        }
                        nameToDisplay = nameToDisplay.Trim();
                        if (nameToDisplay == "")
                            nameToDisplay = surnameToDisplay;

                        var userCreated = await RegisterUser(new RegisterUserDto
                        {
                            TenancyName = tenant.TenancyName,
                            Name = nameToDisplay,
                            Surname = surnameToDisplay,
                            EmailAddress = dto.Email,
                            Password = password,
                            UserName = dto.LoginUserName,
                            IsExternalLogin = false,
                        });

                        UserDefaultInformation udDto = new UserDefaultInformation();
                        udDto.EmployeeRefId = dto.Id.Value;
                        udDto.UserId = userCreated.Id;

                        await _userdefaultinformationRepo.InsertAndGetIdAsync(udDto);
                    }
                }

            }
            return new IdInput { Id = item.Id };
        }

        protected virtual async Task<IdInput> CreatePersonalInformation(CreateOrUpdatePersonalInformationInput input)
        {
            var dto = input.PersonalInformation.MapTo<PersonalInformation>();
            dto.ActiveStatus = true;
            dto.IsAttendanceRequired = true;

            if (input.UpdateSerialNumberRequired == true)
                await UpdateUserSerialNumber(new IdInput { Id = dto.Id });

            CheckErrors(await _personalinformationManager.CreateSync(dto));

            var skill = await _skillsetRepo.FirstOrDefaultAsync(t => t.Id == (dto.DefaultSkillRefId));

            List<ComboboxItemDto> skillSetList = new List<ComboboxItemDto>();

            ComboboxItemDto s = new ComboboxItemDto();
            s.Value = skill.Id.ToString();
            s.DisplayText = skill.SkillCode;
            skillSetList.Add(s);

            if (input.SkillSetList != null)
            {
                foreach (var lst in input.SkillSetList)
                {
                    var alreadySkillExists = skillSetList.FirstOrDefault(t => t.Value == lst.Value);
                    if (alreadySkillExists == null)
                    {
                        ComboboxItemDto newSkill = new ComboboxItemDto();
                        newSkill.Value = lst.Value;
                        newSkill.DisplayText = lst.DisplayText;
                        skillSetList.Add(newSkill);
                    }
                }
            }

            EmployeeSkillSetEditDto employeeSkillDetail = new EmployeeSkillSetEditDto();
            employeeSkillDetail.EmployeeRefId = dto.Id;
            employeeSkillDetail.SkillSetRefId = skill.Id;

            CreateOrUpdateEmployeeSkillSetInput empSkillsetDto = new CreateOrUpdateEmployeeSkillSetInput
            {
                EmployeeSkillSet = employeeSkillDetail,
                SkillSetList = skillSetList
            };

            await _employeeskillsetappservice.CreateOrUpdateEmployeeSkillSet(empSkillsetDto);

            SalaryInfo salaryDto = new SalaryInfo
            {
                EmployeeRefId = dto.Id,
                SalaryMode = 0,
                BasicSalary = 0,
                Levy = 0,
                Ot1PerHour = 0,
                Ot2PerHour = 0,
                ReportOtPerHour = 0,
                NormalOTPerHour  = 0,
                IncentiveAndOTCalculationPeriodRefId = (int) IncentiveOTCalculationPeriod.Salary_Month
            };

            await _salaryinfoRepo.InsertOrUpdateAndGetIdAsync(salaryDto);

            var userExistAlready = await _userdefaultinformationRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == dto.Id);

            if (userExistAlready == null && !input.PersonalInformation.LoginUserName.IsNullOrEmpty() && input.PersonalInformation.LoginUserName.Length > 0)
            {
                var email = dto.Email;
                if (email == null || email.Length == 0)
                {
                    email = dto.PersonalEmail;
                    if (email == null || email.Length == 0)
                    {
                        email = dto.EmployeeName.Replace(" ", "").ToLower() + "dummy@gmail.com";
                    }
                    dto.Email = email;
                }

                string password = "123456";
                if (userExistAlready == null)
                {
                    password = "123456";
                }
                else
                {

                }

                var tenant = await _tenantManager.GetByIdAsync(AbpSession.TenantId.Value);
                var user = await _userManager.FindByNameOrEmailAsync(input.PersonalInformation.LoginUserName);
                if (user == null)
                {
                    string nameToDisplay = "";
                    string surnameToDisplay = "";

                    string[] nameArray = dto.EmployeeName.ToUpper().Trim().Split(' ');
                    int idx = 0;
                    foreach (var name in nameArray)
                    {
                        if (idx == 0)
                        {
                            surnameToDisplay = name;
                            if (nameArray.Length == 1)
                            {
                                nameToDisplay = name;
                            }
                        }
                        else
                        {
                            nameToDisplay = nameToDisplay + name + " ";
                        }
                        idx++;
                    }
                    nameToDisplay = nameToDisplay.Trim();

                    var userCreated = await RegisterUser(new RegisterUserDto
                    {
                        TenancyName = tenant.TenancyName,
                        Name = nameToDisplay,
                        Surname = surnameToDisplay,
                        EmailAddress = dto.Email,
                        Password = password,
                        UserName = input.PersonalInformation.LoginUserName,
                        IsExternalLogin = false,

                    });

                    //var userCreated = await RegisterUser(new RegisterUserDto
                    //{
                    //    TenancyName = tenant.TenancyName,
                    //    Name = dto.EmployeeName,
                    //    Surname = dto.EmployeeName,
                    //    EmailAddress = dto.Email,
                    //    Password = password,
                    //    UserName = input.PersonalInformation.LoginUserName,
                    //    IsExternalLogin = false
                    //});

                    UserDefaultInformation udDto = new UserDefaultInformation();
                    udDto.EmployeeRefId = dto.Id;
                    udDto.UserId = userCreated.Id;

                    await _userdefaultinformationRepo.InsertAndGetIdAsync(udDto);
                }
            }


            return new IdInput { Id = dto.Id };
        }

        public async Task<ListResultOutput<SimplePersonalInformationListDto>> GetSimpleEmployeeDetails()
        {
            var lstPersonalInformation = await _personalinformationRepo.GetAllListAsync(t => t.ActiveStatus == true);
            return new ListResultOutput<SimplePersonalInformationListDto>(lstPersonalInformation.MapTo<List<SimplePersonalInformationListDto>>());
        }

        public async Task<ListResultOutput<SimplePersonalInformationListDto>> GetSimpleInchargeEmployeeDetails()
        {
            var lstInchargeList = await _personalinformationRepo.GetAllListAsync(t => t.ActiveStatus == true);
            List<int> inchargeRefIds = lstInchargeList.Where(t => t.InchargeEmployeeRefId.HasValue).Select(t => t.InchargeEmployeeRefId.Value).Distinct().ToList();
            var lstPersonalInformation = await _personalinformationRepo.GetAllListAsync(t => inchargeRefIds.Contains(t.Id));
            return new ListResultOutput<SimplePersonalInformationListDto>(lstPersonalInformation.MapTo<List<SimplePersonalInformationListDto>>());
        }

        public async Task<ListResultOutput<SimplePersonalInformationListDto>> GetSimpleSupervisorEmployeeDetails()
        {
            var lstSupervisorList = await _personalinformationRepo.GetAllListAsync(t => t.ActiveStatus == true);
            List<int> supervisorRefIds = lstSupervisorList.Where(t => t.SupervisorEmployeeRefId.HasValue).Select(t => t.SupervisorEmployeeRefId.Value).Distinct().ToList();
            var lstPersonalInformation = await _personalinformationRepo.GetAllListAsync(t => supervisorRefIds.Contains(t.Id));
            return new ListResultOutput<SimplePersonalInformationListDto>(lstPersonalInformation.MapTo<List<SimplePersonalInformationListDto>>());
        }


        public async Task<ListResultOutput<PersonalInformationListDto>> GetEmployeeNames()
        {
            var lstPersonalInformation = await _personalinformationRepo.GetAllListAsync(t => t.ActiveStatus == true);
            return new ListResultOutput<PersonalInformationListDto>(lstPersonalInformation.MapTo<List<PersonalInformationListDto>>());
        }

        public async Task<IdInput> GetUserSerialNumberMax()
        {
            var lstPerinfo = await _personalinformationRepo.GetAll().ToListAsync();
            int maxUserSno = lstPerinfo.Count + 1;
            return new IdInput
            {
                Id = maxUserSno
            };
        }

        public async Task UpdateUserSerialNumber(IdInput input)
        {
            var perinfo = await _personalinformationRepo.FirstOrDefaultAsync(m => m.Id == input.Id);

            if (perinfo == null)
                return;

            var lstPerinfo = await _personalinformationRepo.GetAll().Where(m => m.Id != input.Id && m.UserSerialNumber >= perinfo.UserSerialNumber).OrderBy(m => m.UserSerialNumber).ToListAsync();

            int userSnoToSet = (int)perinfo.UserSerialNumber + 1;

            foreach (var lstmat in lstPerinfo)
            {
                var editDto = await _personalinformationRepo.GetAsync(lstmat.Id);
                editDto.UserSerialNumber = userSnoToSet;
                userSnoToSet++;
            }
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetEmployeeForCombobox()
        {
            var lst = await GetEmployeeNames();

            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.EmployeeName)).ToList());
        }

        public async Task<List<WeekDaysDto>> GetWeekDaysForCombobox()
        {
            List<WeekDaysDto> lst = new List<WeekDaysDto>();
            lst.Add(new WeekDaysDto { WeekDayNumber = (int)DayOfWeek.Sunday, WeekDayName = "Sunday" });
            lst.Add(new WeekDaysDto { WeekDayNumber = (int)DayOfWeek.Monday, WeekDayName = "Monday" });
            lst.Add(new WeekDaysDto { WeekDayNumber = (int)DayOfWeek.Tuesday, WeekDayName = "Tuesday" });
            lst.Add(new WeekDaysDto { WeekDayNumber = (int)DayOfWeek.Wednesday, WeekDayName = "Wednesday" });
            lst.Add(new WeekDaysDto { WeekDayNumber = (int)DayOfWeek.Thursday, WeekDayName = "Thursday" });
            lst.Add(new WeekDaysDto { WeekDayNumber = (int)DayOfWeek.Friday, WeekDayName = "Friday" });
            lst.Add(new WeekDaysDto { WeekDayNumber = (int)DayOfWeek.Saturday, WeekDayName = "Saturday" });

            return lst;

        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetActiveEmployeeForCombobox()
        {
            var lst = await _personalinformationRepo.GetAll().Where(t => t.LastWorkingDate == null).ToListAsync();
            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Select(e => new ComboboxItemDto(e.Id.ToString(), e.EmployeeName)).ToList());
        }


        public async Task<List<EmployeeBasicDetails>> GetEmployeeBasicDetails()
        {
            var lstOutput = await (from per in _personalinformationRepo.GetAll()
                                   join loc in _locationRepo.GetAll()
                                   on per.LocationRefId equals loc.Id
                                   join com in _companyRepo.GetAll()
                                   on loc.CompanyRefId equals com.Id
                                   join skill in _skillsetRepo.GetAll()
                                   on per.DefaultSkillRefId equals skill.Id
                                   join jt in _jobTitleMasterRepo.GetAll() on per.JobTitleRefId equals jt.Id
                                   select new EmployeeBasicDetails
                                   {
                                       EmployeeRefCode = per.EmployeeCode,
                                       EmployeeRefId = per.Id,
                                       EmployeeRefName = per.EmployeeName,
                                       CompanyRefId = loc.CompanyRefId,
                                       CompanyRefName = com.Name,
                                       DefaultSkillRefId = per.DefaultSkillRefId,
                                       DefaultSkillSetCode = skill.SkillCode,
                                       JobTitle = jt.JobTitle,
                                       JobTitleRefId = per.JobTitleRefId.Value,
                                       ActiveStatus = per.ActiveStatus,
                                       LastWorkingDate = per.LastWorkingDate,
                                       SupervisorEmployeeRefId = per.SupervisorEmployeeRefId,
                                       InchargeEmployeeRefId = per.InchargeEmployeeRefId
                                   }).ToListAsync();

            return lstOutput.OrderByDescending(t => t.ActiveStatus).ToList();
        }

        public async Task<List<EmployeeWithSkillSetViewDto>> GetEmployeeWithSkillSetForCombobox()
        {
            return await GetEmployeeWithSkillSetForCombobox(new NullableIdInput { Id = null });
        }

        public async Task<List<EmployeeWithSkillSetViewDto>> GetParticularEmployeeWithSkillSetForCombobox(IdInput input)
        {
            List<EmployeeWithSkillSetViewDto> lstOutput = new List<EmployeeWithSkillSetViewDto>();
            lstOutput = await (from per in _personalinformationRepo.GetAll().Where(t => t.Id == input.Id)
                               join loc in _locationRepo.GetAll()
                               on per.LocationRefId equals loc.Id
                               join com in _companyRepo.GetAll()
                               on loc.CompanyRefId equals com.Id
                               join skill in _skillsetRepo.GetAll()
                               on per.DefaultSkillRefId equals skill.Id
                               join jt in _jobTitleMasterRepo.GetAll() on per.JobTitleRefId equals jt.Id
                               select new EmployeeWithSkillSetViewDto
                               {
                                   EmployeeRefCode = per.EmployeeCode,
                                   EmployeeRefId = per.Id,
                                   EmployeeRefName = per.EmployeeName,
                                   CompanyRefId = loc.CompanyRefId,
                                   CompanyRefName = com.Name,
                                   DefaultSkillRefId = per.DefaultSkillRefId,
                                   DefaultSkillSetCode = skill.SkillCode,
                                   JobTitle = jt.JobTitle,
                                   JobTitleRefId = per.JobTitleRefId.Value,
                                   Gender = per.Gender,
                                   LastWorkingDate = per.LastWorkingDate,
                                   ActiveStatus = per.ActiveStatus
                               }).ToListAsync();

            var rsEmpSkillSets = await _employeeskillsetRepo.GetAllListAsync(t => t.EmployeeRefId == input.Id);
            var rsSkillSets = await _skillsetRepo.GetAllListAsync();


            foreach (var det in lstOutput)
            {
                var skillSetKnown = (from empskill in rsEmpSkillSets
                                            .Where(t => t.EmployeeRefId == det.EmployeeRefId)
                                     join skill in rsSkillSets on empskill.SkillSetRefId equals skill.Id
                                     select new EmployeeKnownSkillSetList
                                     {
                                         SkillSetRefId = skill.Id,
                                         SkillSetRefName = skill.SkillCode,
                                     }
                                        ).ToList();
                det.EmployeeKnownSkillSetList = skillSetKnown;
            }
            return lstOutput.OrderByDescending(t => t.ActiveStatus).ToList();
        }

        public async Task<List<EmployeeWithSkillSetViewDto>> GetEmployeeWithSkillSetForCombobox(NullableIdInput input)
        {
            //if (input!=null)
            List<EmployeeWithSkillSetViewDto> lstOutput = new List<EmployeeWithSkillSetViewDto>();
            if (input == null || input.Id == null || input.Id == 0)
            {
                lstOutput = await (from per in _personalinformationRepo.GetAll()
                                   join loc in _locationRepo.GetAll()
                              on per.LocationRefId equals loc.Id
                                   join com in _companyRepo.GetAll()
                                   on loc.CompanyRefId equals com.Id
                                   join skill in _skillsetRepo.GetAll()
                                   on per.DefaultSkillRefId equals skill.Id
                                   join jt in _jobTitleMasterRepo.GetAll() on per.JobTitleRefId equals jt.Id
                                   select new EmployeeWithSkillSetViewDto
                                   {
                                       EmployeeRefCode = per.EmployeeCode,
                                       EmployeeRefId = per.Id,
                                       EmployeeRefName = per.EmployeeName,
                                       CompanyRefId = loc.CompanyRefId,
                                       CompanyRefName = com.Name,
                                       DefaultSkillRefId = per.DefaultSkillRefId,
                                       DefaultSkillSetCode = skill.SkillCode,
                                       JobTitle = jt.JobTitle,
                                       JobTitleRefId = per.JobTitleRefId.Value,
                                       Gender = per.Gender,
                                       LastWorkingDate = per.LastWorkingDate,
                                       ActiveStatus = per.ActiveStatus
                                   }).ToListAsync();
            }
            else
            {
                lstOutput = await (from per in _personalinformationRepo.GetAll().WhereIf(input.Id == 1, t => t.ActiveStatus == true)
                                   join loc in _locationRepo.GetAll()
                             on per.LocationRefId equals loc.Id
                                   join com in _companyRepo.GetAll()
                                   on loc.CompanyRefId equals com.Id
                                   join skill in _skillsetRepo.GetAll()
                                 on per.DefaultSkillRefId equals skill.Id
                                   join jt in _jobTitleMasterRepo.GetAll() on per.JobTitleRefId equals jt.Id
                                   select new EmployeeWithSkillSetViewDto
                                   {
                                       EmployeeRefCode = per.EmployeeCode,
                                       EmployeeRefId = per.Id,
                                       EmployeeRefName = per.EmployeeName,
                                       CompanyRefId = loc.CompanyRefId,
                                       CompanyRefName = com.Name,
                                       DefaultSkillRefId = per.DefaultSkillRefId,
                                       DefaultSkillSetCode = skill.SkillCode,
                                       JobTitle = jt.JobTitle,
                                       JobTitleRefId = per.JobTitleRefId.Value,
                                       Gender = per.Gender,
                                       LastWorkingDate = per.LastWorkingDate,
                                       ActiveStatus = per.ActiveStatus
                                   }).ToListAsync();

            }

            var rsSkillSets = await _skillsetRepo.GetAllListAsync();
            var rsEmpSkillSets = await _employeeskillsetRepo.GetAllListAsync();

            foreach (var det in lstOutput)
            {
                var skillSetKnown = (from empskill in rsEmpSkillSets
                                            .Where(t => t.EmployeeRefId == det.EmployeeRefId)
                                     join skill in rsSkillSets on empskill.SkillSetRefId equals skill.Id
                                     select new EmployeeKnownSkillSetList
                                     {
                                         SkillSetRefId = skill.Id,
                                         SkillSetRefName = skill.SkillCode,
                                     }
                                        ).ToList();
                det.EmployeeKnownSkillSetList = skillSetKnown;
            }
            return lstOutput.OrderByDescending(t => t.ActiveStatus).ToList();
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetEmployeeCodeForCombobox()
        {
            var lst = await GetEmployeeNames();
            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.EmployeeCode)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetCompanyForCombobox()
        {
            var lstCompany = await _companyRepo.GetAll().ToListAsync();
            return
                new ListResultOutput<ComboboxItemDto>(
                    lstCompany.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Code)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetCompanyCodeForCombobox()
        {
            var lstCompany = await _companyRepo.GetAll().ToListAsync();
            return
                new ListResultOutput<ComboboxItemDto>(
                    lstCompany.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Code)).ToList());
        }


        public async Task<ListResultOutput<ComboboxItemDto>> GetSkillSetForCombobox()
        {
            var lst = await _skillsetRepo.GetAll().ToListAsync();

            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Select(e => new ComboboxItemDto(e.Id.ToString(), e.SkillCode)).ToList());
        }

        public async Task<EmployeeMaximumDetails> GetMaximumEmpCodeAndBioMetricNumber(GetEmployeeMaximumInput input)
        {
            var company = await _companyRepo.FirstOrDefaultAsync(t => t.Id == input.CompanyRefId);
            if (company == null)
            {
                throw new UserFriendlyException(L("SelectCompany"));
            }

            PersonalInformation maxEmployeeCode = null;


          
           

            return new EmployeeMaximumDetails
            {
                EmployeeCode = "",
                BioMetricCode = ""
            };

        }

        [UnitOfWork]
        public virtual async Task<User> RegisterUser(RegisterUserDto model)
        {
            try
            {

                if (!_multiTenancyConfig.IsEnabled)
                {
                    model.TenancyName = Tenant.DefaultTenantName;
                }
                else if (model.TenancyName.IsNullOrEmpty())
                {
                    throw new UserFriendlyException(L("TenantNameCanNotBeEmpty"));
                }

                var tenant = await GetActiveTenantAsync(model.TenancyName);

                var user = new User
                {
                    TenantId = tenant.Id,
                    Name = model.Name,
                    Surname = model.Surname,
                    EmailAddress = model.EmailAddress,
                    IsActive = true
                };

                user.ShouldChangePasswordOnNextLogin = true;

                user.UserName = model.UserName;
                user.Password = new PasswordHasher().HashPassword(model.Password);

                _unitOfWorkManager.Current.EnableFilter(AbpDataFilters.MayHaveTenant);
                _unitOfWorkManager.Current.SetFilterParameter(AbpDataFilters.MayHaveTenant, AbpDataFilters.Parameters.TenantId, tenant.Id);

                user.Roles = new List<UserRole>();
                foreach (var defaultRole in await _roleManager.Roles.Where(r => r.IsDefault).ToListAsync())
                {
                    user.Roles.Add(new UserRole { RoleId = defaultRole.Id });
                }

                CheckErrors(await _userManager.CreateAsync(user));
                await _unitOfWorkManager.Current.SaveChangesAsync();


                if (!user.IsEmailConfirmed)
                {
                    //user.SetNewEmailConfirmationCode();
                    //await _userEmailer.SendEmailActivationLinkAsync(user);
                }

                //Notifications
                await _notificationSubscriptionManager.SubscribeToAllAvailableNotificationsAsync(user.TenantId, user.Id);
                await _appNotifier.WelcomeToTheApplicationAsync(user);
                await _appNotifier.NewUserRegisteredAsync(user);

                return user;
            }
            catch (UserFriendlyException ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }


        private async Task<Tenant> GetActiveTenantAsync(string tenancyName)
        {
            var tenant = await _tenantManager.FindByTenancyNameAsync(tenancyName);
            if (tenant == null)
            {
                throw new UserFriendlyException(L("ThereIsNoTenantDefinedWithName{0}", tenancyName));
            }

            if (!tenant.IsActive)
            {
                throw new UserFriendlyException(L("TenantIsNotActive", tenancyName));
            }

            return tenant;
        }

        public async Task<MessageOutput> GetEmployeeStatus(GetEmployeeStatusDto input)
        {
            MessageOutput output = new MessageOutput();
            var per = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == input.EmployeeRefId);

            if (per.LastWorkingDate != null)
            {
                output.Message = output.Message + L("EmployeeAlreadResigned", per.EmployeeName, per.LastWorkingDate.Value.ToString("yyyy-MMM-dd"));
            }

            var rejectedString = L("Rejected");
            var existLeaveRecords = await _leaverequestRepo.GetAll().Where(t => t.EmployeeRefId == input.EmployeeRefId
                && input.GivenDate >= t.LeaveFrom && input.GivenDate <= t.LeaveTo && t.LeaveStatus != rejectedString).ToListAsync();

            if (existLeaveRecords.Count() > 0)
            {
                foreach (var lst in existLeaveRecords)
                {
                    output.Message = L("InLeave", lst.LeaveFrom.ToString("dd-MMM") + " - " + lst.LeaveTo.ToString("dd-MMM-yy"));
                }
            }
            return output;
        }

        public async Task<IdInput> UpdateGps(GpsAttendance input)
        {
            if (1 == 1)  //ExistAllServerLevelBusinessRules(input))
            {
                var a = await _gpsattendanceRepo.InsertOrUpdateAndGetIdAsync(input);
                return new IdInput { Id = a };
            }
            else
            {
                return new IdInput { Id = -1 };
            }
        }

        public async Task<IdInput> UpdateGpsLive(GpsLive input)
        {
            if (1 == 1)  //ExistAllServerLevelBusinessRules(input))
            {
                try
                {
                    var a = await _gpsliveRepo.InsertAndGetIdAsync(input);
                    return new IdInput { Id = a };
                }
                catch (Exception ex)
                {

                    throw new UserFriendlyException(ex.Message + ex.InnerException.Message);
                }
            }
            else
            {
                return new IdInput { Id = -1 };
            }
        }


        public async Task<List<GpsAttendanceOutputDto>> GetGpsAttendanceList(GetEmployeeDashBoardDto input)
        {
            DateTime fromDate;
            DateTime toDate;

            #region SelectDate
            if (input.StartDate.HasValue && input.EndDate.HasValue)
            {
                fromDate = input.StartDate.Value;
                toDate = input.EndDate.Value;
            }
            else
            {
                fromDate = DateTime.Now.AddDays(-5);
                toDate = DateTime.Now;
            }
            #endregion

            #region Get_Data _ LINQ
            var per = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == input.EmployeeRefId);
            string tenantName = "Singapore";


            if (per == null)
            {
                return null;
            }
            else
            {
                var tenant = await TenantManager.GetByIdAsync(per.TenantId);
                tenantName = tenant.Name.ToUpper();
            }

            var rsGpsData = await (from gps in _gpsattendanceRepo.GetAll()
                                    .Where(t => t.EmployeeRefId == input.EmployeeRefId
                                    && t.CreationTime >= fromDate && DbFunctions.TruncateTime(t.CreationTime) <= toDate.Date && t.AttendanceStatus != 5)
                                   select new GpsAttendanceOutputDto
                                   {
                                       Date = gps.CreationTime, // .ToUniversalTime(),
                                       Status = gps.AttendanceStatus == 1 ? "IN" : gps.AttendanceStatus == 2 ? "OUT" : gps.AttendanceStatus == 3 ? "BREAK OUT" : gps.AttendanceStatus == 4 ? "BREAK IN" : "LIVE",
                                       Location = gps.Address,
                                       IsSmartAttendanceFingerReceived = gps.IsSmartAttendanceFingerReceived,
                                       TenantId = per.TenantId
                                   }
                        ).ToListAsync();


            if (tenantName.ToUpper().Equals("INDIA"))
            {
                TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
                foreach (var lst in rsGpsData)
                {
                    lst.UTCTime = lst.Date.ToUniversalTime();
                    lst.ServerTime = lst.Date;
                    lst.Date = TimeZoneInfo.ConvertTimeFromUtc(lst.Date.ToUniversalTime(), tzi);
                }
            }
            return rsGpsData;

            #endregion

        }

        public async Task<List<GpsAttendanceOutputDto>> GetGpsLiveList(GetEmployeeDashBoardDto input)
        {
            DateTime fromDate;
            DateTime toDate;

            #region SelectDate
            if (input.StartDate.HasValue && input.EndDate.HasValue)
            {
                fromDate = input.StartDate.Value;
                toDate = input.EndDate.Value;
            }
            else
            {
                fromDate = DateTime.Now.AddDays(-3);
                toDate = DateTime.Now;
            }
            #endregion

            #region Get_Data _ LINQ
            var rsGpsData = await (from gps in _gpsliveRepo.GetAll()
                                    .Where(t => t.EmployeeRefId == input.EmployeeRefId
                                    && t.CreationTime >= fromDate && t.CreationTime <= toDate)
                                   select new GpsAttendanceOutputDto
                                   {
                                       Date = gps.CreationTime,
                                       Status = "LIVE",
                                       Latitude = gps.Latitude,
                                       Longitude = gps.Longitude,
                                       Altitude = gps.Altitude,
                                       Address = gps.Address
                                   }
                                   ).ToListAsync();
            return rsGpsData;

            #endregion

        }
        public double distanceBetweenTwoPoints(double lat1, double lon1, double lat2, double lon2)
        {
            var R = 6371; //Radius of the earth in KM
            var dLat = (lat2 - lat1) * Math.PI / 180;
            var dLon = (lon2 - lat2) * Math.PI / 180;
            var a = 0.5 - Math.Cos(dLat) / 2 + Math.Cos(lat1 * Math.PI / 180) * Math.Cos(lat2 * Math.PI / 180) * (1 - Math.Cos(dLon)) / 2;
            var result = R * 2 * Math.Asin(Math.Sqrt(a));
            return result; //Returns the distance between two co-ordinates in KM.
        }


        public async Task<MessageOutput> SendPassWordForSpecialOperation(EmailInput input)
        {
            MessageOutput retOutput = new MessageOutput();
            retOutput.SuccessFlag = true;

            var per = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == input.EmployeeRefId);
            if (per == null)
            {
                throw new UserFriendlyException(L("EmailDoesNotExist"));
            }

            var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == per.LocationRefId);

            var company = await _companyRepo.FirstOrDefaultAsync(t => t.Id == location.CompanyRefId);


            retOutput.EmployeeRefId = per.Id;
            retOutput.EmployeeRefName = per.EmployeeName;
            retOutput.PersonalEmail = per.Email;
            retOutput.SuccessFlag = false;
            string passWord = "123456";

            var mailMessage = new StringBuilder();

            string htmlLine;

            htmlLine = "<p>Dear " + per.EmployeeName + " ,</p> ";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<p><strong>Based On your request, we have mailed your OTP : <br/> <br/>Your  Password Is: " + passWord + "</strong></p>";
            mailMessage.AppendLine(htmlLine);

            htmlLine = "<p> &nbsp;</p> ";
            mailMessage.AppendLine(htmlLine);
            htmlLine = "<p> HR / EDP </p>";
            mailMessage.AppendLine(htmlLine);
            htmlLine = "<p>  " + company.Name + " </p> ";
            mailMessage.AppendLine(htmlLine);
            htmlLine = "  <p> &nbsp;</p> ";
            mailMessage.AppendLine(htmlLine);

            {
                try
                {
                    SmtpClient client = new SmtpClient();
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.EnableSsl = true;
                    client.Host = "smtp.gmail.com";
                    client.Port = 587;

                    System.Net.NetworkCredential credentials =
                        new System.Net.NetworkCredential("hr@company", "");
                    client.UseDefaultCredentials = false;
                    client.Credentials = credentials;

                    MailMessage msg = new MailMessage();
                    msg.From = new MailAddress("hr@company");
                    if (!per.Email.IsNullOrEmpty())
                        msg.To.Add(new MailAddress(per.Email));
                    if (!per.PersonalEmail.IsNullOrEmpty())
                        msg.To.Add(new MailAddress(per.PersonalEmail));


                    msg.Subject = company.Name + "OTP For Your Login "; // "This is a test Email subject";
                    msg.IsBodyHtml = true;
                    msg.Body = mailMessage.ToString();
                    try
                    {
                        if (msg.To.Count > 0)
                            client.Send(msg);
                    }
                    catch (Exception ex)
                    {
                        retOutput.SuccessFlag = false;
                        retOutput.ErrorMessage = L("NotAbleToSendMailError", ex.Message + ex.InnerException);
                    }
                    retOutput.SuccessFlag = true;
                }
                catch (Exception ex)
                {
                    retOutput.SuccessFlag = false;
                    retOutput.ErrorMessage = L("NotAbleToSendMailError", ex.Message + ex.InnerException);
                    throw;
                }
            }

            retOutput.Message = mailMessage.ToString();
            retOutput.HtmlMessage = mailMessage.ToString();

            return retOutput;
        }

        public async Task<PersonalInformationListDto> GetEmployeeDetailsBasedOnUserName(UserNameInput input)
        {
            var user1 = await UserManager.FindByNameOrEmailAsync(input.UserName);
            var user2 = await _userManager.FindByNameOrEmailAsync(input.UserName);

            var userId = user1.Id;

            var ud = await _userdefaultinformationRepo.FirstOrDefaultAsync(t => t.UserId == userId);

            if (ud == null)
                return null;
            else
            {
                PersonalInformationListDto retEmp = new PersonalInformationListDto();
                var emp = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == ud.EmployeeRefId.Value);
                if (emp != null)
                {
                    retEmp = emp.MapTo<PersonalInformationListDto>();
                    if (retEmp.MobileLiveSyncMinutes == 0)
                        retEmp.MobileLiveSyncMinutes = 5;
                }
                else
                {
                    return null;
                }
                return retEmp;
            }
        }
        public List<LocalPermissionDto> GetPermissionsForSmartAttendance(UserNameInput input)
        {
            List<LocalPermissionDto> localPermissions = new List<LocalPermissionDto>();

            localPermissions.Add(new LocalPermissionDto() { PermissionName = "DUTYCHART", IsGranted = true });
            localPermissions.Add(new LocalPermissionDto() { PermissionName = "LEAVEREQUEST", IsGranted = true });

            if (PermissionChecker.IsGranted("Pages.Tenant.Hr.Transaction.ClientMeeting.Create"))
            {
                localPermissions.Add(new LocalPermissionDto() { PermissionName = "CLIENTMEETING", IsGranted = true });
            }

            localPermissions.Add(new LocalPermissionDto() { PermissionName = "ATTENDANCELOG", IsGranted = true });

            if (PermissionChecker.IsGranted("Pages.Tenant.Hr.NdtReport"))
            {
                localPermissions.Add(new LocalPermissionDto() { PermissionName = "REPORTS", IsGranted = true });
            }

            localPermissions.Add(new LocalPermissionDto() { PermissionName = "DOCUMENTINFO", IsGranted = true });
            localPermissions.Add(new LocalPermissionDto() { PermissionName = "SALARYSLIP", IsGranted = true });

            return localPermissions;
        }

        public async Task<string> GetAndroidMobileVersion()
        {
            var version = "1.0"; // await SettingManager.GetSettingValueAsync(AppSettings.General.MobileVersion);
            return version;
        }

        public async Task<string> GetIosMobileVersion()
        {
            var version = "1.0"; // await SettingManager.GetSettingValueAsync(AppSettings.General.IosVersion);
            return version;
        }





        public async Task<ListResultOutput<ComboboxItemDto>> GetEmployeeBasedOnGivenProcedureCodeForCombobox(StringInput input)
        {
            var arrSkillList = input.StringId.Split(",");
            var lstSkillset = await _skillsetRepo.GetAllListAsync(t => arrSkillList.Contains(t.SkillCode));
            int[] arrSkillsets = lstSkillset.Select(t => t.Id).ToList().ToArray();
            DateTime fromDate = DateTime.Today;
            fromDate = fromDate.AddDays(-5);
            var lst = await (from mas in _personalinformationRepo.GetAll().Where(t => t.LastWorkingDate == null || t.LastWorkingDate > fromDate)
                             join empskill in _employeeskillsetRepo.GetAll().Where(t => arrSkillsets.Contains(t.SkillSetRefId))
                             on mas.Id equals empskill.EmployeeRefId
                             select new ComboboxItemDto
                             {
                                 Value = mas.Id.ToString(),
                                 DisplayText = mas.EmployeeName
                             }
                             ).ToListAsync();

            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText)).ToList());
        }

        public async Task UpdateFixedCostCentreInDutyChart(PersonalInformationEditDto input)
        {
            var perEdit = await _personalinformationRepo.GetAsync(input.Id.Value);
            perEdit.DutyFixedUptoDate = input.DutyFixedUptoDate;
            if (input.DutyFixedUptoDate == null)
                perEdit.IsFixedCostCentreInDutyChart = false;
            else
                perEdit.IsFixedCostCentreInDutyChart = true;

            await _personalinformationRepo.UpdateAsync(perEdit);
        }

        public async Task<IdInput> UpdateFixedCostCentre(PersonalInformationListDto input)
        {
            var per = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == input.Id);
            if (per == null)
            {
                throw new UserFriendlyException(L("EmployeeNotExist"));
            }

            if (per.ActiveStatus == false)
            {
                throw new UserFriendlyException(L("EmployeeNotExist"));
            }

            per.IsFixedCostCentreInDutyChart = input.IsFixedCostCentreInDutyChart;
            if (input.DutyFixedUptoDate.HasValue)
            {
                per.DutyFixedUptoDate = input.DutyFixedUptoDate.Value;
            }
            else
            {
                per.DutyFixedUptoDate = null;
            }

            await _personalinformationRepo.UpdateAsync(per);

            return new IdInput { Id = per.Id };
        }
        public async Task<SimplePersonalInformationListDto> GetEmployeeDetailsBasedOnEmployeeRefId(EmployeeIdInput input)
        {
            var allitems = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == input.EmployeeRefId);
            return allitems.MapTo<SimplePersonalInformationListDto>();
        }

        public async Task<string> GetSkillSetListOfGivenEmployee(EmployeeIdInput input)
        {
            var rsEmpSkills = await _employeeskillsetRepo.GetAllListAsync(t => t.EmployeeRefId == input.EmployeeRefId);

            string empSkillSet = "";
            foreach (var sk in rsEmpSkills)
            {
                var locskill = await _skillsetRepo.FirstOrDefaultAsync(t => t.Id == sk.SkillSetRefId);
                empSkillSet = empSkillSet + locskill.SkillCode + ",";
            }
            if (empSkillSet.Length > 0)
            {
                empSkillSet = empSkillSet.Left(empSkillSet.Length - 1);
            }
            return empSkillSet;
        }

        public async Task<string> GetCompanyNameById(IdInput input)
        {
            var lstId = await _companyRepo.FirstOrDefaultAsync(a => a.Id == input.Id && a.TenantId == AbpSession.TenantId);

            if (lstId == null)
            {
                return "";
            }
            else
            {
                return lstId.Name;
            }
        }


        public async Task<ListResultOutput<ComboboxItemDto>> GetCostCentreMasterList()
        {
            var lstNdtProcedure = await _employeeCostCentreMasterRepo.GetAll().ToListAsync();

            return
                new ListResultOutput<ComboboxItemDto>(
                    lstNdtProcedure.Select(e => new ComboboxItemDto(e.Id.ToString(), e.CostCentre)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetDepartmentMasterList()
        {
            var lstNdtProcedure = await _employeeDepartmentMasterRepo.GetAll().ToListAsync();

            return
                new ListResultOutput<ComboboxItemDto>(
                    lstNdtProcedure.Select(e => new ComboboxItemDto(e.Id.ToString(), e.DepartmentName)).ToList());
        }


        public async Task<ListResultOutput<ComboboxItemDto>> GetResidentStatusList()
        {
            var lst = await _employeeResidentStatusRepo.GetAll().ToListAsync();

            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Select(e => new ComboboxItemDto(e.Id.ToString(), e.ResidentStatus)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetRacesStatusList()
        {
            var lst = await _employeeRaceStatusRepo.GetAll().ToListAsync();

            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Select(e => new ComboboxItemDto(e.Id.ToString(), e.RaceName)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetReligionList()
        {
            var lst = await _employeeReligionStatusRepo.GetAll().ToListAsync();

            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Select(e => new ComboboxItemDto(e.Id.ToString(), e.ReligionName)).ToList());
        }

        // Generate a random string with a given size  
        public string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }

        // Generate a random number between two numbers
        public int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }

        // Generate a random password  
        public string RandomPassword()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(RandomString(4, true));
            builder.Append(RandomNumber(1000, 9999));
            builder.Append(RandomString(2, false));
            return builder.ToString();
        }

        public async Task ImportPersonalInformationSampleList(IdInput locinput)
        {
            var rsReligions = await _employeeReligionStatusRepo.GetAllListAsync();
            var rsRaces = await _employeeRaceStatusRepo.GetAllListAsync();
            var rsCostCentres = await _employeeCostCentreMasterRepo.GetAllListAsync();
            var rsResidentStatus = await _employeeResidentStatusRepo.GetAllListAsync();

            var rsSkillSet = await _skillsetRepo.GetAllListAsync();
            if (rsSkillSet.Count==0)
            {
                throw new UserFriendlyException("Import Skill");
            }
            var rsLocation = await _locationRepo.GetAllListAsync(t=>t.Id==locinput.Id);
            int locationRefId = rsLocation.FirstOrDefault().Id;
            var rsJobTitles = await _jobTitleMasterRepo.GetAllListAsync();

            int managerReferenceId = 0;
            int supervisorRefId = 0;

            int userSerialNumber = 1;

            #region Manager Employee
            {
                var religionName = "Islam";
                var raceName = "Malay";
                var costCentreName = "Admin";
                var skillName = "Manager";
                var jobTitle = "Manager";
                var residentStatusName = "Citizen";
                var employeeGivenName = "Kader";
                var employeeSurName = "Sulaiman";
                var employeeName = employeeGivenName + " " + employeeSurName;
                var bioMetricCode = "001";
                var employeeCode = "SAM" + bioMetricCode;
                Random random = new Random();
                var yearBefore = random.Next(18, 55);
                var currentDate = DateTime.Today;
                var dateOfBirth = new DateTime(currentDate.AddYears(-1 * yearBefore).Year, random.Next(1, 12), random.Next(1, 25));
                var defaultWeekOff = (int)DayOfWeek.Sunday;

                var prevMonth = currentDate.Date.AddDays(-40);
                var dateHired = new DateTime(prevMonth.Year, prevMonth.Month, 01);

                #region Get Default
                var skill = rsSkillSet.FirstOrDefault(t => t.SkillCode.ToUpper().Equals(skillName.ToUpper()));
                if (skill == null)
                {
                    skill = rsSkillSet.FirstOrDefault();
                }
                var jt = rsJobTitles.FirstOrDefault(t => t.JobTitle == jobTitle);
                if (jt == null)
                    jt = rsJobTitles.FirstOrDefault();

                var religion = rsReligions.FirstOrDefault(t => t.ReligionName.ToUpper().Equals(religionName.ToUpper()));
                if (religion == null)
                {
                    religion = rsReligions.FirstOrDefault();
                }

                var race = rsRaces.FirstOrDefault(t => t.RaceName.ToUpper().Equals(raceName.ToUpper()));
                if (race == null)
                {
                    race = rsRaces.FirstOrDefault();
                }

                var costCentre = rsCostCentres.FirstOrDefault(t => t.CostCentre.ToUpper().Equals(costCentreName.ToUpper()));
                if (costCentre == null)
                {
                    costCentre = rsCostCentres.FirstOrDefault();
                }

                var residentStatus = rsResidentStatus.FirstOrDefault(t => t.ResidentStatus.ToUpper().Equals(residentStatusName.ToUpper()));
                if (residentStatus == null)
                {
                    residentStatus = rsResidentStatus.FirstOrDefault();
                }
                #endregion

                PersonalInformation pi = new PersonalInformation();
                var newEmployee = await _personalinformationRepo.FirstOrDefaultAsync(t => t.EmployeeCode.ToUpper().Equals(employeeCode.ToUpper()));

                if (newEmployee == null)
                {
                    PersonalInformationEditDto newEmpDto = new PersonalInformationEditDto
                    {
                        LocationRefId = locationRefId,
                        BioMetricCode = bioMetricCode,
                        EmployeeCode = employeeCode,
                        UserSerialNumber = userSerialNumber++,
                        EmployeeGivenName = employeeGivenName,
                        EmployeeSurName = employeeSurName,
                        EmployeeName = employeeName,
                        StaffType = "Permanent",
                        EmployeeResidentStatusRefId = residentStatus.Id,
                        RaceRefId = race.Id,
                        EmployeeCostCentreRefId = costCentre.Id,
                        EmployeeReligionRefId = religion.Id,
                        ReligionRefId = religion.Id,
                        DateOfBirth = dateOfBirth,
                        Gender = "Male",
                        PassPortNumber = RandomString(8, false),
                        JobTitleRefId = jt.Id,
                        ActiveStatus = true,
                        DefaultSkillRefId = skill.Id,
                        DefaultWeekOff = defaultWeekOff,
                        IsAttendanceRequired = true,
                        LoginUserName = employeeCode.ToLower(),
                        IsFixedCostCentreInDutyChart = true,
                        IsSmartAttendanceFingerAllowed = true,
                        IsDayCloseStatusMailRequired = true,
                        DateHired = dateHired
                    };

                    newEmpDto.Email = "dummy" + newEmpDto.BioMetricCode + "@gmail.com";

                    var idOutput = await CreateOrUpdatePersonalInformation(new CreateOrUpdatePersonalInformationInput { PersonalInformation = newEmpDto });

                    managerReferenceId = idOutput.Id;

                    var mgr = await _personalinformationRepo.GetAsync(managerReferenceId);
                    mgr.InchargeEmployeeRefId = managerReferenceId;
                    mgr.SupervisorEmployeeRefId = managerReferenceId;

                    await _personalinformationRepo.UpdateAsync(mgr);
                 
                }
            }
            #endregion

            #region Supervisor Employee
            {
                var religionName = "Islam";
                var raceName = "Indian";
                var costCentreName = "Admin";
                var skillName = "Steward";
                var jobTitle = "Steward";
                var residentStatusName = "Citizen";
                var employeeGivenName = "Abdul";
                var employeeSurName = "Rahman";
                var employeeName = employeeGivenName + " " + employeeSurName;
                var bioMetricCode = "002";
                var employeeCode = "SAM" + bioMetricCode;
                Random random = new Random();
                var yearBefore = random.Next(18, 55);
                var currentDate = DateTime.Today;
                var dateOfBirth = new DateTime(currentDate.AddYears(-1 * yearBefore).Year, random.Next(1, 12), random.Next(1, 25));
                var defaultWeekOff = (int)DayOfWeek.Sunday;

                var prevMonth = currentDate.Date.AddDays(-40);
                var dateHired = new DateTime(prevMonth.Year, prevMonth.Month, 01);


                #region Get Default
                var skill = rsSkillSet.FirstOrDefault(t => t.SkillCode.ToUpper().Equals(skillName.ToUpper()));
                if (skill == null)
                {
                    skill = rsSkillSet.FirstOrDefault();
                }
                var jt = rsJobTitles.FirstOrDefault(t => t.JobTitle == jobTitle);
                if (jt == null)
                    jt = rsJobTitles.FirstOrDefault();

                var religion = rsReligions.FirstOrDefault(t => t.ReligionName.ToUpper().Equals(religionName.ToUpper()));
                if (religion == null)
                {
                    religion = rsReligions.FirstOrDefault();
                }

                var race = rsRaces.FirstOrDefault(t => t.RaceName.ToUpper().Equals(raceName.ToUpper()));
                if (race == null)
                {
                    race = rsRaces.FirstOrDefault();
                }

                var costCentre = rsCostCentres.FirstOrDefault(t => t.CostCentre.ToUpper().Equals(costCentreName.ToUpper()));
                if (costCentre == null)
                {
                    costCentre = rsCostCentres.FirstOrDefault();
                }

                var residentStatus = rsResidentStatus.FirstOrDefault(t => t.ResidentStatus.ToUpper().Equals(residentStatusName.ToUpper()));
                if (residentStatus == null)
                {
                    residentStatus = rsResidentStatus.FirstOrDefault();
                }
                #endregion

                PersonalInformation pi = new PersonalInformation();
                var newEmployee = await _personalinformationRepo.FirstOrDefaultAsync(t => t.EmployeeCode.ToUpper().Equals(employeeCode.ToUpper()));
                if (newEmployee == null)
                {
                    PersonalInformationEditDto newEmpDto = new PersonalInformationEditDto
                    {
                        LocationRefId = locationRefId,
                        BioMetricCode = bioMetricCode,
                        EmployeeCode = employeeCode,
                        UserSerialNumber = userSerialNumber++,
                        EmployeeGivenName = employeeGivenName,
                        EmployeeSurName = employeeSurName,
                        EmployeeName = employeeName,
                        StaffType = "Permanent",
                        EmployeeResidentStatusRefId = residentStatus.Id,
                        RaceRefId = race.Id,
                        EmployeeCostCentreRefId = costCentre.Id,
                        EmployeeReligionRefId = religion.Id,
                        ReligionRefId = religion.Id,
                        DateOfBirth = dateOfBirth,
                        Gender = "Male",
                        PassPortNumber = RandomString(8, false),
                        JobTitleRefId = jt.Id,
                        ActiveStatus = true,
                        DefaultSkillRefId = skill.Id,
                        DefaultWeekOff = defaultWeekOff,
                        IsAttendanceRequired = true,
                        Email = "dummy" + userSerialNumber + "@gmail.com",
                        LoginUserName = employeeCode.ToLower(),
                        IsFixedCostCentreInDutyChart = true,
                        IsSmartAttendanceFingerAllowed = true,
                        IsDayCloseStatusMailRequired = true,
                        SupervisorEmployeeRefId = managerReferenceId,
                        InchargeEmployeeRefId = managerReferenceId,
                        DateHired = dateHired
                    };
                    newEmpDto.Email = "dummy" + newEmpDto.BioMetricCode + "@gmail.com";
                    var idOutput = await CreateOrUpdatePersonalInformation(new CreateOrUpdatePersonalInformationInput { PersonalInformation = newEmpDto });

                    supervisorRefId = idOutput.Id;
                }
            }

            #endregion

            #region Production Employee 1 
            {
                var religionName = "Hindu";
                var raceName = "Indian";
                var costCentreName = "Operation";
                var skillName = "Cook";
                var jobTitle = "Cook";
                var residentStatusName = "PR";
                var employeeGivenName = "Rajan";
                var employeeSurName = "Muthuraman";
                var employeeName = employeeGivenName + " " + employeeSurName;
                var bioMetricCode = "003";
                var employeeCode = "SAM" + bioMetricCode;
                Random random = new Random();
                var yearBefore = random.Next(18, 55);
                var currentDate = DateTime.Today;
                var dateOfBirth = new DateTime(currentDate.AddYears(-1 * yearBefore).Year, random.Next(1, 12), random.Next(1, 25));
                var defaultWeekOff = (int)DayOfWeek.Sunday;

                var prevMonth = currentDate.Date.AddDays(-40);
                var dateHired = new DateTime(prevMonth.Year, prevMonth.Month, 01);

                #region Get Default
                var skill = rsSkillSet.FirstOrDefault(t => t.SkillCode.ToUpper().Equals(skillName.ToUpper()));
                if (skill == null)
                {
                    skill = rsSkillSet.FirstOrDefault();
                }
                var jt = rsJobTitles.FirstOrDefault(t => t.JobTitle == jobTitle);
                if (jt == null)
                    jt = rsJobTitles.FirstOrDefault();

                var religion = rsReligions.FirstOrDefault(t => t.ReligionName.ToUpper().Equals(religionName.ToUpper()));
                if (religion == null)
                {
                    religion = rsReligions.FirstOrDefault();
                }

                var race = rsRaces.FirstOrDefault(t => t.RaceName.ToUpper().Equals(raceName.ToUpper()));
                if (race == null)
                {
                    race = rsRaces.FirstOrDefault();
                }

                var costCentre = rsCostCentres.FirstOrDefault(t => t.CostCentre.ToUpper().Equals(costCentreName.ToUpper()));
                if (costCentre == null)
                {
                    costCentre = rsCostCentres.FirstOrDefault();
                }

                var residentStatus = rsResidentStatus.FirstOrDefault(t => t.ResidentStatus.ToUpper().Equals(residentStatusName.ToUpper()));
                if (residentStatus == null)
                {
                    residentStatus = rsResidentStatus.FirstOrDefault();
                }
                #endregion

                PersonalInformation pi = new PersonalInformation();
                var newEmployee = await _personalinformationRepo.FirstOrDefaultAsync(t => t.EmployeeCode.ToUpper().Equals(employeeCode.ToUpper()));
                if (newEmployee == null)
                {
                    PersonalInformationEditDto newEmpDto = new PersonalInformationEditDto
                    {
                        LocationRefId = locationRefId,
                        BioMetricCode = bioMetricCode,
                        EmployeeCode = employeeCode,
                        UserSerialNumber = userSerialNumber++,
                        EmployeeGivenName = employeeGivenName,
                        EmployeeSurName = employeeSurName,
                        EmployeeName = employeeName,
                        StaffType = "Permanent",
                        EmployeeResidentStatusRefId = residentStatus.Id,
                        RaceRefId = race.Id,
                        EmployeeCostCentreRefId = costCentre.Id,
                        EmployeeReligionRefId = religion.Id,
                        ReligionRefId = religion.Id,
                        DateOfBirth = dateOfBirth,
                        Gender = "Male",
                        PassPortNumber = RandomString(8, false),
                        JobTitleRefId = jt.Id,
                        ActiveStatus = true,
                        DefaultSkillRefId = skill.Id,
                        DefaultWeekOff = defaultWeekOff,
                        IsAttendanceRequired = true,
                        Email = "dummy" + userSerialNumber + "@gmail.com",
                        LoginUserName = employeeCode.ToLower(),
                        IsFixedCostCentreInDutyChart = true,
                        IsSmartAttendanceFingerAllowed = true,
                        IsDayCloseStatusMailRequired = true,
                        SupervisorEmployeeRefId = supervisorRefId,
                        InchargeEmployeeRefId = managerReferenceId,
                        DateHired = dateHired
                    };
                    newEmpDto.Email = "dummy" + newEmpDto.BioMetricCode + "@gmail.com";

                    var idOutput = await CreateOrUpdatePersonalInformation(new CreateOrUpdatePersonalInformationInput { PersonalInformation = newEmpDto });
                }
            }
            #endregion

            #region Production Employee 2 
            {
                var religionName = "Islam";
                var raceName = "Indian";
                var costCentreName = "Operation";
                var skillName = "Dosa";
                var jobTitle = "Cook";
                var residentStatusName = "WP";
                var employeeGivenName = "Sheik ";
                var employeeSurName = "Hammeed";
                var employeeName = employeeGivenName + " " + employeeSurName;
                var bioMetricCode = "004";
                var employeeCode = "SAM" + bioMetricCode;
                Random random = new Random();
                var yearBefore = random.Next(18, 55);
                var currentDate = DateTime.Today;
                var dateOfBirth = new DateTime(currentDate.AddYears(-1 * yearBefore).Year, random.Next(1, 12), random.Next(1, 25));
                var defaultWeekOff = (int)DayOfWeek.Sunday;

                var prevMonth = currentDate.Date.AddDays(-40);
                var dateHired = new DateTime(prevMonth.Year, prevMonth.Month, 01);

                #region Get Default
                var skill = rsSkillSet.FirstOrDefault(t => t.SkillCode.ToUpper().Equals(skillName.ToUpper()));
                if (skill == null)
                {
                    skill = rsSkillSet.FirstOrDefault();
                }
                var jt = rsJobTitles.FirstOrDefault(t => t.JobTitle == jobTitle);
                if (jt == null)
                    jt = rsJobTitles.FirstOrDefault();

                var religion = rsReligions.FirstOrDefault(t => t.ReligionName.ToUpper().Equals(religionName.ToUpper()));
                if (religion == null)
                {
                    religion = rsReligions.FirstOrDefault();
                }

                var race = rsRaces.FirstOrDefault(t => t.RaceName.ToUpper().Equals(raceName.ToUpper()));
                if (race == null)
                {
                    race = rsRaces.FirstOrDefault();
                }

                var costCentre = rsCostCentres.FirstOrDefault(t => t.CostCentre.ToUpper().Equals(costCentreName.ToUpper()));
                if (costCentre == null)
                {
                    costCentre = rsCostCentres.FirstOrDefault();
                }

                var residentStatus = rsResidentStatus.FirstOrDefault(t => t.ResidentStatus.ToUpper().Equals(residentStatusName.ToUpper()));
                if (residentStatus == null)
                {
                    residentStatus = rsResidentStatus.FirstOrDefault();
                }
                #endregion

                PersonalInformation pi = new PersonalInformation();
                var newEmployee = await _personalinformationRepo.FirstOrDefaultAsync(t => t.EmployeeCode.ToUpper().Equals(employeeCode.ToUpper()));
                if (newEmployee == null)
                {
                    PersonalInformationEditDto newEmpDto = new PersonalInformationEditDto
                    {
                        LocationRefId = locationRefId,
                        BioMetricCode = bioMetricCode,
                        EmployeeCode = employeeCode,
                        UserSerialNumber = userSerialNumber++,
                        EmployeeGivenName = employeeGivenName,
                        EmployeeSurName = employeeSurName,
                        EmployeeName = employeeName,
                        StaffType = "Permanent",
                        EmployeeResidentStatusRefId = residentStatus.Id,
                        RaceRefId = race.Id,
                        EmployeeCostCentreRefId = costCentre.Id,
                        EmployeeReligionRefId = religion.Id,
                        ReligionRefId = religion.Id,
                        DateOfBirth = dateOfBirth,
                        Gender = "Male",
                        PassPortNumber = RandomString(8, false),
                        JobTitleRefId = jt.Id,
                        ActiveStatus = true,
                        DefaultSkillRefId = skill.Id,
                        DefaultWeekOff = defaultWeekOff,
                        IsAttendanceRequired = true,
                        LoginUserName = employeeCode.ToLower(),
                        IsFixedCostCentreInDutyChart = true,
                        IsSmartAttendanceFingerAllowed = true,
                        IsDayCloseStatusMailRequired = true,
                        SupervisorEmployeeRefId = supervisorRefId,
                        InchargeEmployeeRefId = managerReferenceId,
                        DateHired = dateHired
                    };
                    newEmpDto.Email = "dummy" + newEmpDto.BioMetricCode + "@gmail.com";

                    var idOutput = await CreateOrUpdatePersonalInformation(new CreateOrUpdatePersonalInformationInput { PersonalInformation = newEmpDto });

                }
            }
            #endregion

            #region Production Employee 3 
            {
                var religionName = "Hindu";
                var raceName = "Indian";
                var costCentreName = "Operation";
                var skillName = "Cashier";
                var jobTitle = "Coffee Maker";
                var residentStatusName = "PR";
                var employeeGivenName = "Balan";
                var employeeSurName = "Kathiresan";
                var employeeName = employeeGivenName + " " + employeeSurName;
                var bioMetricCode = "005";
                var employeeCode = "SAM" + bioMetricCode;
                Random random = new Random();
                var yearBefore = random.Next(18, 55);
                var currentDate = DateTime.Today;
                var dateOfBirth = new DateTime(currentDate.AddYears(-1 * yearBefore).Year, random.Next(1, 12), random.Next(1, 25));
                var defaultWeekOff = (int)DayOfWeek.Sunday;

                var prevMonth = currentDate.Date.AddDays(-40);
                var dateHired = new DateTime(prevMonth.Year, prevMonth.Month, 01);

                #region Get Default
                var skill = rsSkillSet.FirstOrDefault(t => t.SkillCode.ToUpper().Equals(skillName.ToUpper()));
                if (skill == null)
                {
                    skill = rsSkillSet.FirstOrDefault();
                }
                var jt = rsJobTitles.FirstOrDefault(t => t.JobTitle == jobTitle);
                if (jt == null)
                    jt = rsJobTitles.FirstOrDefault();

                var religion = rsReligions.FirstOrDefault(t => t.ReligionName.ToUpper().Equals(religionName.ToUpper()));
                if (religion == null)
                {
                    religion = rsReligions.FirstOrDefault();
                }

                var race = rsRaces.FirstOrDefault(t => t.RaceName.ToUpper().Equals(raceName.ToUpper()));
                if (race == null)
                {
                    race = rsRaces.FirstOrDefault();
                }

                var costCentre = rsCostCentres.FirstOrDefault(t => t.CostCentre.ToUpper().Equals(costCentreName.ToUpper()));
                if (costCentre == null)
                {
                    costCentre = rsCostCentres.FirstOrDefault();
                }

                var residentStatus = rsResidentStatus.FirstOrDefault(t => t.ResidentStatus.ToUpper().Equals(residentStatusName.ToUpper()));
                if (residentStatus == null)
                {
                    residentStatus = rsResidentStatus.FirstOrDefault();
                }
                #endregion

                PersonalInformation pi = new PersonalInformation();
                var newEmployee = await _personalinformationRepo.FirstOrDefaultAsync(t => t.EmployeeCode.ToUpper().Equals(employeeCode.ToUpper()));
                if (newEmployee == null)
                {
                    PersonalInformationEditDto newEmpDto = new PersonalInformationEditDto
                    {
                        LocationRefId = locationRefId,
                        BioMetricCode = bioMetricCode,
                        EmployeeCode = employeeCode,
                        UserSerialNumber = userSerialNumber++,
                        EmployeeGivenName = employeeGivenName,
                        EmployeeSurName = employeeSurName,
                        EmployeeName = employeeName,
                        StaffType = "Permanent",
                        EmployeeResidentStatusRefId = residentStatus.Id,
                        RaceRefId = race.Id,
                        EmployeeCostCentreRefId = costCentre.Id,
                        EmployeeReligionRefId = religion.Id,
                        ReligionRefId = religion.Id,
                        DateOfBirth = dateOfBirth,
                        Gender = "Male",
                        PassPortNumber = RandomString(8, false),
                        JobTitleRefId = jt.Id,
                        ActiveStatus = true,
                        DefaultSkillRefId = skill.Id,
                        DefaultWeekOff = defaultWeekOff,
                        IsAttendanceRequired = true,
                        LoginUserName = employeeCode.ToLower(),
                        IsFixedCostCentreInDutyChart = true,
                        IsSmartAttendanceFingerAllowed = true,
                        IsDayCloseStatusMailRequired = true,
                        SupervisorEmployeeRefId = supervisorRefId,
                        InchargeEmployeeRefId = managerReferenceId,
                        DateHired = dateHired
                    };
                    newEmpDto.Email = "dummy" + newEmpDto.BioMetricCode + "@gmail.com";

                    var idOutput = await CreateOrUpdatePersonalInformation(new CreateOrUpdatePersonalInformationInput { PersonalInformation = newEmpDto });

                }
            }
            #endregion

            #region Production Employee 4 
            {
                var religionName = "Hindu";
                var raceName = "Indian";
                var costCentreName = "Operation";
                var skillName = "Dosa";
                var jobTitle = "Dosa Maker";
                var residentStatusName = "PR";
                var employeeGivenName = "Sundaram";
                var employeeSurName = "Rajam";
                var employeeName = employeeGivenName + " " + employeeSurName;
                var bioMetricCode = "006";
                var employeeCode = "SAM" + bioMetricCode;
                Random random = new Random();
                var yearBefore = random.Next(18, 55);
                var currentDate = DateTime.Today;
                var dateOfBirth = new DateTime(currentDate.AddYears(-1 * yearBefore).Year, random.Next(1, 12), random.Next(1, 25));
                var defaultWeekOff = (int)DayOfWeek.Sunday;

                var prevMonth = currentDate.Date.AddDays(-40);
                var dateHired = new DateTime(prevMonth.Year, prevMonth.Month, 01);

                #region Get Default
                var skill = rsSkillSet.FirstOrDefault(t => t.SkillCode.ToUpper().Equals(skillName.ToUpper()));
                if (skill == null)
                {
                    skill = rsSkillSet.FirstOrDefault();
                }
                var jt = rsJobTitles.FirstOrDefault(t => t.JobTitle == jobTitle);
                if (jt == null)
                    jt = rsJobTitles.FirstOrDefault();

                var religion = rsReligions.FirstOrDefault(t => t.ReligionName.ToUpper().Equals(religionName.ToUpper()));
                if (religion == null)
                {
                    religion = rsReligions.FirstOrDefault();
                }

                var race = rsRaces.FirstOrDefault(t => t.RaceName.ToUpper().Equals(raceName.ToUpper()));
                if (race == null)
                {
                    race = rsRaces.FirstOrDefault();
                }

                var costCentre = rsCostCentres.FirstOrDefault(t => t.CostCentre.ToUpper().Equals(costCentreName.ToUpper()));
                if (costCentre == null)
                {
                    costCentre = rsCostCentres.FirstOrDefault();
                }

                var residentStatus = rsResidentStatus.FirstOrDefault(t => t.ResidentStatus.ToUpper().Equals(residentStatusName.ToUpper()));
                if (residentStatus == null)
                {
                    residentStatus = rsResidentStatus.FirstOrDefault();
                }
                #endregion

                PersonalInformation pi = new PersonalInformation();
                var newEmployee = await _personalinformationRepo.FirstOrDefaultAsync(t => t.EmployeeCode.ToUpper().Equals(employeeCode.ToUpper()));
                if (newEmployee == null)
                {
                    PersonalInformationEditDto newEmpDto = new PersonalInformationEditDto
                    {
                        LocationRefId = locationRefId,
                        BioMetricCode = bioMetricCode,
                        EmployeeCode = employeeCode,
                        UserSerialNumber = userSerialNumber++,
                        EmployeeGivenName = employeeGivenName,
                        EmployeeSurName = employeeSurName,
                        EmployeeName = employeeName,
                        StaffType = "Permanent",
                        EmployeeResidentStatusRefId = residentStatus.Id,
                        RaceRefId = race.Id,
                        EmployeeCostCentreRefId = costCentre.Id,
                        EmployeeReligionRefId = religion.Id,
                        ReligionRefId = religion.Id,
                        DateOfBirth = dateOfBirth,
                        Gender = "Male",
                        PassPortNumber = RandomString(8, false),
                        JobTitleRefId = jt.Id,
                        ActiveStatus = true,
                        DefaultSkillRefId = skill.Id,
                        DefaultWeekOff = defaultWeekOff,
                        IsAttendanceRequired = true,
                        LoginUserName = employeeCode.ToLower(),
                        IsFixedCostCentreInDutyChart = true,
                        IsSmartAttendanceFingerAllowed = true,
                        IsDayCloseStatusMailRequired = true,
                        SupervisorEmployeeRefId = supervisorRefId,
                        InchargeEmployeeRefId = managerReferenceId,
                        DateHired = dateHired
                    };
                    newEmpDto.Email = "dummy" + newEmpDto.BioMetricCode + "@gmail.com";

                    var idOutput = await CreateOrUpdatePersonalInformation(new CreateOrUpdatePersonalInformationInput { PersonalInformation = newEmpDto });

                }
            }
            #endregion

            #region Production Employee 5 
            {
                var religionName = "Islam";
                var raceName = "Malay";
                var costCentreName = "Operation";
                var skillName = "Driver";
                var jobTitle = "Driver";
                var residentStatusName = "Citizen";
                var employeeGivenName = "Hajila";
                var employeeSurName = "Mohammed";
                var employeeName = employeeGivenName + " " + employeeSurName;
                var bioMetricCode = "007";
                var employeeCode = "SAM" + bioMetricCode;
                Random random = new Random();
                var yearBefore = random.Next(18, 55);
                var currentDate = DateTime.Today;
                var dateOfBirth = new DateTime(currentDate.AddYears(-1 * yearBefore).Year, random.Next(1, 12), random.Next(1, 25));
                var defaultWeekOff = (int)DayOfWeek.Sunday;

                var prevMonth = currentDate.Date.AddDays(-40);
                var dateHired = new DateTime(prevMonth.Year, prevMonth.Month, 01);

                #region Get Default
                var skill = rsSkillSet.FirstOrDefault(t => t.SkillCode.ToUpper().Equals(skillName.ToUpper()));
                if (skill == null)
                {
                    skill = rsSkillSet.FirstOrDefault();
                }
                var jt = rsJobTitles.FirstOrDefault(t => t.JobTitle == jobTitle);
                if (jt == null)
                    jt = rsJobTitles.FirstOrDefault();

                var religion = rsReligions.FirstOrDefault(t => t.ReligionName.ToUpper().Equals(religionName.ToUpper()));
                if (religion == null)
                {
                    religion = rsReligions.FirstOrDefault();
                }

                var race = rsRaces.FirstOrDefault(t => t.RaceName.ToUpper().Equals(raceName.ToUpper()));
                if (race == null)
                {
                    race = rsRaces.FirstOrDefault();
                }

                var costCentre = rsCostCentres.FirstOrDefault(t => t.CostCentre.ToUpper().Equals(costCentreName.ToUpper()));
                if (costCentre == null)
                {
                    costCentre = rsCostCentres.FirstOrDefault();
                }

                var residentStatus = rsResidentStatus.FirstOrDefault(t => t.ResidentStatus.ToUpper().Equals(residentStatusName.ToUpper()));
                if (residentStatus == null)
                {
                    residentStatus = rsResidentStatus.FirstOrDefault();
                }
                #endregion

                PersonalInformation pi = new PersonalInformation();
                var newEmployee = await _personalinformationRepo.FirstOrDefaultAsync(t => t.EmployeeCode.ToUpper().Equals(employeeCode.ToUpper()));
                if (newEmployee == null)
                {
                    PersonalInformationEditDto newEmpDto = new PersonalInformationEditDto
                    {
                        LocationRefId = locationRefId,
                        BioMetricCode = bioMetricCode,
                        EmployeeCode = employeeCode,
                        UserSerialNumber = userSerialNumber++,
                        EmployeeGivenName = employeeGivenName,
                        EmployeeSurName = employeeSurName,
                        EmployeeName = employeeName,
                        StaffType = "Permanent",
                        EmployeeResidentStatusRefId = residentStatus.Id,
                        RaceRefId = race.Id,
                        EmployeeCostCentreRefId = costCentre.Id,
                        EmployeeReligionRefId = religion.Id,
                        ReligionRefId = religion.Id,
                        DateOfBirth = dateOfBirth,
                        Gender = "Male",
                        PassPortNumber = RandomString(8, false),
                        JobTitleRefId = jt.Id,
                        ActiveStatus = true,
                        DefaultSkillRefId = skill.Id,
                        DefaultWeekOff = defaultWeekOff,
                        IsAttendanceRequired = true,
                        LoginUserName = employeeCode.ToLower(),
                        IsFixedCostCentreInDutyChart = true,
                        IsSmartAttendanceFingerAllowed = true,
                        IsDayCloseStatusMailRequired = true,
                        SupervisorEmployeeRefId = supervisorRefId,
                        InchargeEmployeeRefId = managerReferenceId,
                        DateHired = dateHired
                    };
                    newEmpDto.Email = "dummy" + newEmpDto.BioMetricCode + "@gmail.com";

                    var idOutput = await CreateOrUpdatePersonalInformation(new CreateOrUpdatePersonalInformationInput { PersonalInformation = newEmpDto });

                }
            }
            #endregion

            
            #region Production Employee 6 
            {
                var religionName = "Islam";
                var raceName = "Malay";
                var costCentreName = "Operation";
                var skillName = "Driver";
                var jobTitle = "Driver";
                var residentStatusName = "Citizen";
                var employeeGivenName = "Razali";
                var employeeSurName = "Ahmed";
                var employeeName = employeeGivenName + " " + employeeSurName;
                var bioMetricCode = "008" +
                    "";
                var employeeCode = "SAM" + bioMetricCode;
                Random random = new Random();
                var yearBefore = random.Next(18, 55);
                var currentDate = DateTime.Today;
                var dateOfBirth = new DateTime(currentDate.AddYears(-1 * yearBefore).Year, random.Next(1, 12), random.Next(1, 25));
                var defaultWeekOff = (int)DayOfWeek.Sunday;

                var prevMonth = currentDate.Date.AddDays(-40);
                var dateHired = new DateTime(prevMonth.Year, prevMonth.Month, 01);

                #region Get Default
                var skill = rsSkillSet.FirstOrDefault(t => t.SkillCode.ToUpper().Equals(skillName.ToUpper()));
                if (skill == null)
                {
                    skill = rsSkillSet.FirstOrDefault();
                }
                var jt = rsJobTitles.FirstOrDefault(t => t.JobTitle == jobTitle);
                if (jt == null)
                    jt = rsJobTitles.FirstOrDefault();

                var religion = rsReligions.FirstOrDefault(t => t.ReligionName.ToUpper().Equals(religionName.ToUpper()));
                if (religion == null)
                {
                    religion = rsReligions.FirstOrDefault();
                }

                var race = rsRaces.FirstOrDefault(t => t.RaceName.ToUpper().Equals(raceName.ToUpper()));
                if (race == null)
                {
                    race = rsRaces.FirstOrDefault();
                }

                var costCentre = rsCostCentres.FirstOrDefault(t => t.CostCentre.ToUpper().Equals(costCentreName.ToUpper()));
                if (costCentre == null)
                {
                    costCentre = rsCostCentres.FirstOrDefault();
                }

                var residentStatus = rsResidentStatus.FirstOrDefault(t => t.ResidentStatus.ToUpper().Equals(residentStatusName.ToUpper()));
                if (residentStatus == null)
                {
                    residentStatus = rsResidentStatus.FirstOrDefault();
                }
                #endregion

                PersonalInformation pi = new PersonalInformation();
                var newEmployee = await _personalinformationRepo.FirstOrDefaultAsync(t => t.EmployeeCode.ToUpper().Equals(employeeCode.ToUpper()));
                if (newEmployee == null)
                {
                    PersonalInformationEditDto newEmpDto = new PersonalInformationEditDto
                    {
                        LocationRefId = locationRefId,
                        BioMetricCode = bioMetricCode,
                        EmployeeCode = employeeCode,
                        UserSerialNumber = userSerialNumber++,
                        EmployeeGivenName = employeeGivenName,
                        EmployeeSurName = employeeSurName,
                        EmployeeName = employeeName,
                        StaffType = "Permanent",
                        EmployeeResidentStatusRefId = residentStatus.Id,
                        RaceRefId = race.Id,
                        EmployeeCostCentreRefId = costCentre.Id,
                        EmployeeReligionRefId = religion.Id,
                        ReligionRefId = religion.Id,
                        DateOfBirth = dateOfBirth,
                        Gender = "Male",
                        PassPortNumber = RandomString(8, false),
                        JobTitleRefId = jt.Id,
                        ActiveStatus = true,
                        DefaultSkillRefId = skill.Id,
                        DefaultWeekOff = defaultWeekOff,
                        IsAttendanceRequired = true,
                        LoginUserName = employeeCode.ToLower(),
                        IsFixedCostCentreInDutyChart = true,
                        IsSmartAttendanceFingerAllowed = true,
                        IsDayCloseStatusMailRequired = true,
                        SupervisorEmployeeRefId = supervisorRefId,
                        InchargeEmployeeRefId = managerReferenceId,
                        DateHired = dateHired
                    };
                    newEmpDto.Email = "dummy" + newEmpDto.BioMetricCode + "@gmail.com";

                    var idOutput = await CreateOrUpdatePersonalInformation(new CreateOrUpdatePersonalInformationInput { PersonalInformation = newEmpDto });

                }
            }
            #endregion

            #region Production Employee 7
            {
                var religionName = "Islam";
                var raceName = "Malay";
                var costCentreName = "Operation";
                var skillName = "Cashier";
                var jobTitle = "Cashier";
                var residentStatusName = "Citizen";
                var employeeGivenName = "Sukor";
                var employeeSurName = "Haliji";
                var employeeName = employeeGivenName + " " + employeeSurName;
                var bioMetricCode = "009" +
                    "";
                var employeeCode = "SAM" + bioMetricCode;
                Random random = new Random();
                var yearBefore = random.Next(18, 55);
                var currentDate = DateTime.Today;
                var dateOfBirth = new DateTime(currentDate.AddYears(-1 * yearBefore).Year, random.Next(1, 12), random.Next(1, 25));
                var defaultWeekOff = (int)DayOfWeek.Sunday;

                var prevMonth = currentDate.Date.AddDays(-40);
                var dateHired = new DateTime(prevMonth.Year, prevMonth.Month, 01);

                #region Get Default
                var skill = rsSkillSet.FirstOrDefault(t => t.SkillCode.ToUpper().Equals(skillName.ToUpper()));
                if (skill == null)
                {
                    skill = rsSkillSet.FirstOrDefault();
                }
                var jt = rsJobTitles.FirstOrDefault(t => t.JobTitle == jobTitle);
                if (jt == null)
                    jt = rsJobTitles.FirstOrDefault();

                var religion = rsReligions.FirstOrDefault(t => t.ReligionName.ToUpper().Equals(religionName.ToUpper()));
                if (religion == null)
                {
                    religion = rsReligions.FirstOrDefault();
                }

                var race = rsRaces.FirstOrDefault(t => t.RaceName.ToUpper().Equals(raceName.ToUpper()));
                if (race == null)
                {
                    race = rsRaces.FirstOrDefault();
                }

                var costCentre = rsCostCentres.FirstOrDefault(t => t.CostCentre.ToUpper().Equals(costCentreName.ToUpper()));
                if (costCentre == null)
                {
                    costCentre = rsCostCentres.FirstOrDefault();
                }

                var residentStatus = rsResidentStatus.FirstOrDefault(t => t.ResidentStatus.ToUpper().Equals(residentStatusName.ToUpper()));
                if (residentStatus == null)
                {
                    residentStatus = rsResidentStatus.FirstOrDefault();
                }
                #endregion

                PersonalInformation pi = new PersonalInformation();
                var newEmployee = await _personalinformationRepo.FirstOrDefaultAsync(t => t.EmployeeCode.ToUpper().Equals(employeeCode.ToUpper()));
                if (newEmployee == null)
                {
                    PersonalInformationEditDto newEmpDto = new PersonalInformationEditDto
                    {
                        LocationRefId = locationRefId,
                        BioMetricCode = bioMetricCode,
                        EmployeeCode = employeeCode,
                        UserSerialNumber = userSerialNumber++,
                        EmployeeGivenName = employeeGivenName,
                        EmployeeSurName = employeeSurName,
                        EmployeeName = employeeName,
                        StaffType = "Permanent",
                        EmployeeResidentStatusRefId = residentStatus.Id,
                        RaceRefId = race.Id,
                        EmployeeCostCentreRefId = costCentre.Id,
                        EmployeeReligionRefId = religion.Id,
                        ReligionRefId = religion.Id,
                        DateOfBirth = dateOfBirth,
                        Gender = "Male",
                        PassPortNumber = RandomString(8, false),
                        JobTitleRefId = jt.Id,
                        ActiveStatus = true,
                        DefaultSkillRefId = skill.Id,
                        DefaultWeekOff = defaultWeekOff,
                        IsAttendanceRequired = true,
                        LoginUserName = employeeCode.ToLower(),
                        IsFixedCostCentreInDutyChart = true,
                        IsSmartAttendanceFingerAllowed = true,
                        IsDayCloseStatusMailRequired = true,
                        SupervisorEmployeeRefId = supervisorRefId,
                        InchargeEmployeeRefId = managerReferenceId,
                        DateHired = dateHired
                    };
                    newEmpDto.Email = "dummy" + newEmpDto.BioMetricCode + "@gmail.com";

                    var idOutput = await CreateOrUpdatePersonalInformation(new CreateOrUpdatePersonalInformationInput { PersonalInformation = newEmpDto });

                }
            }
            #endregion

        }

        public async Task<EmployeeRawOutput> ApiUpdateRawInfo(EmployeeRawInput input)
        {
            var output = new EmployeeRawOutput();
            if (string.IsNullOrEmpty(input.Code))
            {
                output.Error = true;
                output.ErrorDescription = string.Format(L("NotExist_F"), L("Code"));
                return output;
            }
            var employee = await _personalinformationRepo.FirstOrDefaultAsync(a => a.EmployeeCode.Equals(input.Code));
            if (employee == null)
            {
                output.Error = true;
                output.ErrorDescription = string.Format(L("NotFound_F"), L("Employee"));
                return output;
            }
            bool add = false;
            EmployeeRawInfo rawInfo;
            if (employee.RawId.HasValue && employee.RawId.Value > 0)
            {
                rawInfo = await _emRawManager.GetAsync(employee.RawId.Value);
            }
            else
            {
                rawInfo = new EmployeeRawInfo();
                add = true;
            }
            rawInfo.Identity = input.Identity;
            rawInfo.IdenitityData = input.IdenitityData;

            if (add)
            {
                employee.EmployeeRawInfo = rawInfo;

            }
            await _personalinformationRepo.UpdateAsync(employee);
            return output;
        }

        public async Task<EmployeeRawOutput> ApiGetEmployeeRawInfo(EmployeeRawInput input)
        {
            /*
             * Only Code is enough
             */
            var output = new EmployeeRawOutput();
            if (string.IsNullOrEmpty(input.Code))
            {
                output.Error = true;
                output.ErrorDescription = string.Format(L("NotExist_F"), L("Code"));
                return output;
            }
            var employee = await _personalinformationRepo.FirstOrDefaultAsync(a => a.EmployeeCode.Equals(input.Code));
            if (employee == null)
            {
                output.Error = true;
                output.ErrorDescription = string.Format(L("NotFound_F"), L("Employee"));
                return output;
            }
            if (!employee.ActiveStatus)
            {
                output.Error = true;
                output.ErrorDescription = string.Format(L("NotActive_F"), L("Employee"));
                return output;
            }
            
            output.Code = employee.EmployeeCode;
            output.Name = employee.EmployeeName;
            output.Id = employee.Id;
            output.DinePlanUserId = employee.DinePlanUserId;

            if (employee.RawId.HasValue && employee.RawId.Value > 0)
            {
                var rawInfo = await _emRawManager.GetAsync(employee.RawId.Value);
                if (rawInfo == null)
                {
                     return output;
                }
                else
                {
                    output.IdenitityData = rawInfo.IdenitityData;
                    output.Identity = rawInfo.Identity;
                    output.Code = employee.EmployeeCode;
                    output.Name = employee.EmployeeName;
                    output.Id = employee.Id;
                    return output;
                }
            }
            return output;


        }

        public async Task<ListResultOutput<JobTitleMasterEditDto>> GetJobTitleDetails()
        {
            var lstJobTitleDetails = await _jobTitleMasterRepo.GetAllListAsync();
            return new ListResultOutput<JobTitleMasterEditDto>(lstJobTitleDetails.MapTo<List<JobTitleMasterEditDto>>());
        }
    }
}
