﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Hr.Inventory.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Hr.Inventory
{
    public interface IYearWiseLeaveAllowedForEmployeeAppService : IApplicationService
    {
        Task<PagedResultOutput<YearWiseLeaveAllowedForEmployeeListDto>> GetAll(GetYearWiseLeaveAllowedForEmployeeInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetYearWiseLeaveAllowedForEmployeeForEditOutput> GetYearWiseLeaveAllowedForEmployeeForEdit(YearWithEmployeeDto input);
        Task CreateOrUpdateYearWiseLeaveAllowedForEmployee(CreateOrUpdateYearWiseLeaveAllowedForEmployeeInput input);
        Task DeleteYearWiseLeaveAllowedForEmployee(IdInput input);

      Task<List<YearWiseLeaveAllowedForEmployeeEditDto>> GetEmployeeWiseAcYearWiseLeave(YearWithEmployeeDto input);

    }
}

