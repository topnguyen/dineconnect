﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Hr.Master
{
    public interface IDcGroupMasterAppService : IApplicationService
    {
        Task<PagedResultOutput<DcGroupMasterListDto>> GetAll(GetDcGroupMasterInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetDcGroupMasterForEditOutput> GetDcGroupMasterForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateDcGroupMaster(CreateOrUpdateDcGroupMasterInput input);
        Task DeleteDcGroupMaster(IdInput input);

        Task<ListResultOutput<DcGroupMasterListDto>> GetGroupNames();
        Task<ListResultOutput<DcGroupMasterListDto>> GetGroupNames(HeadCodeDto input);
        Task<ListResultOutput<DcGroupMasterListDto>> GetGroupNamesBasedOnSkill(SkillCodeDto input);


    }
}

