﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Hr.Master
{
    public interface ISkillSetAppService : IApplicationService
    {
        Task<PagedResultOutput<SkillSetListDto>> GetAll(GetSkillSetInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetSkillSetForEditOutput> GetSkillSetForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateSkillSet(CreateOrUpdateSkillSetInput input);
        Task DeleteSkillSet(IdInput input);

        Task<ListResultOutput<SkillSetListDto>> GetCodes();
    }
}