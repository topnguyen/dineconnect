﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.Hr.Impl;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Authorization.Users.Dto;
using DinePlan.DineConnect.Hr;

namespace DinePlan.DineConnect.Hr.Master.Implementation
{
    public class UserDefaultInformationAppService : DineConnectAppServiceBase, IUserDefaultInformationAppService
    {

        private readonly IUserDefaultInformationListExcelExporter _userdefaultinformationExporter;
        private readonly IUserDefaultInformationManager _userdefaultinformationManager;
        private readonly IRepository<UserDefaultInformation> _userdefaultinformationRepo;
        private readonly IRepository<PersonalInformation> _personalinformationRepo;
        //private readonly IRepository<Client> _clientRepo;
        //private readonly IRepository<ProjectCostCentre> _projectcostcentreRepo;
        private readonly IUserAppService _userAppService;
        //private readonly IRepository<UserVsCostCentre> _userVsCostCentreRepo;

        public UserDefaultInformationAppService(IUserDefaultInformationManager userdefaultinformationManager,
            IRepository<UserDefaultInformation> userDefaultInformationsRepo,
            IUserDefaultInformationListExcelExporter userdefaultinformationExporter
             , IRepository<PersonalInformation> personalinformationRepo
            ,
            //IRepository<Client> clientRepo,
            //IRepository<ProjectCostCentre> projectcostcentreRepo,
            //IRepository<UserVsCostCentre> userVsCostCentreRepo,
            IUserAppService userAppService)
        {
            _userdefaultinformationManager = userdefaultinformationManager;
            _userdefaultinformationRepo = userDefaultInformationsRepo;
            _userdefaultinformationExporter = userdefaultinformationExporter;
            _personalinformationRepo = personalinformationRepo;
            _userAppService = userAppService;
            //_clientRepo = clientRepo;
            //_projectcostcentreRepo = projectcostcentreRepo;
            //_userVsCostCentreRepo = userVsCostCentreRepo;
        }

        public async Task<PagedResultOutput<UserDefaultInformationListDto>> GetAll(GetUserDefaultInformationInput input)
        {
            var users = await _userAppService.GetUsers(new
                  GetUsersInput
            {
                MaxResultCount = 1000
            });


            var userList = users.Items;
            if (!input.Filter.IsNullOrEmpty())
            {
                userList = userList.Where(t => t.Name.Contains(input.Filter) || t.UserName.Contains(input.Filter)).ToList();
            }
            List<long> userRefIds = userList.Select(t => t.Id).ToList();

            var userItems = (from udi in _userdefaultinformationRepo.GetAll().Where(t => userRefIds.Contains(t.UserId))
                             select new UserDefaultInformationListDto()
                             {
                                 Id = udi.Id,
                                 UserId = udi.UserId,
                                 EmployeeRefId = udi.EmployeeRefId
                             });

            List<PersonalInformation> rsPerinfo = new List<PersonalInformation>();
            

            if (input.EmployeeSearchFlag)
            {
                rsPerinfo = await _personalinformationRepo.GetAll()
                   .WhereIf(!input.Filter.IsNullOrEmpty(), t => t.EmployeeName.Contains(input.Filter) || t.EmployeeCode.Contains(input.Filter)).ToListAsync();
                userItems = userItems.Where(t => t.EmployeeRefId.HasValue);
            }

            var outputitems = userItems;

            var sortMenuItems = await outputitems.ToListAsync();

            foreach (var lst in sortMenuItems)
            {
                var userName = userList.FirstOrDefault(t => t.Id == lst.UserId);
                if (userName != null)
                {
                    lst.UserName = userName.UserName;
                    foreach (var r in userName.Roles)
                    {
                        lst.Roles = lst.Roles + r.RoleName + ",";
                    }
                    if (lst.Roles.Length > 0)
                        lst.Roles = lst.Roles.Substring(0, lst.Roles.Length - 1);

                    if (lst.EmployeeRefId.HasValue)
                    {
                        var emp = rsPerinfo.FirstOrDefault(t => t.Id == lst.EmployeeRefId.Value);
                        if (emp != null)
                            lst.EmployeeRefName = emp.EmployeeName;
                    }
                }
            }

            var allListDtos = sortMenuItems;

            allListDtos = allListDtos.OrderBy(input.Sorting).ToList();

            var allItemCount = allListDtos.Count();

            return new PagedResultOutput<UserDefaultInformationListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _userdefaultinformationRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<UserDefaultInformationListDto>>();
            return _userdefaultinformationExporter.ExportToFile(allListDtos);
        }

        public async Task<GetUserDefaultInformationForEditOutput> GetUserDefaultInformationForEdit(NullableIdInput input)
        {
            UserDefaultInformationEditDto editDto;
            if (input.Id.HasValue)
            {
                var hDto = await _userdefaultinformationRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<UserDefaultInformationEditDto>();
            }
            else
            {
                editDto = new UserDefaultInformationEditDto();
            }

            return new GetUserDefaultInformationForEditOutput
            {
                UserDefaultInformation = editDto
            };
        }

        public async Task CreateOrUpdateUserDefaultInformation(CreateOrUpdateUserDefaultInformationInput input)
        {
            if (input.UserDefaultInformation.Id.HasValue)
            {
                await UpdateUserDefaultInformation(input);
            }
            else
            {
                await CreateUserDefaultInformation(input);
            }
        }

        public async Task DeleteUserDefaultInformation(IdInput input)
        {
            await _userdefaultinformationRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateUserDefaultInformation(CreateOrUpdateUserDefaultInformationInput input)
        {
            var item = await _userdefaultinformationRepo.GetAsync(input.UserDefaultInformation.Id.Value);
            var dto = input.UserDefaultInformation;

            //TODO: SERVICE UserDefaultInformation Update Individually
            item.UserId = dto.UserId;
            item.EmployeeRefId = dto.EmployeeRefId;

            CheckErrors(await _userdefaultinformationManager.CreateSync(item));
        }

        protected virtual async Task CreateUserDefaultInformation(CreateOrUpdateUserDefaultInformationInput input)
        {
            var dto = input.UserDefaultInformation.MapTo<UserDefaultInformation>();
            CheckErrors(await _userdefaultinformationManager.CreateSync(dto));
        }

        public async Task<ListResultOutput<UserDefaultInformationListDto>> GetUserIds()
        {
            var lstUserDefaultInformation = await _userdefaultinformationRepo.GetAll().ToListAsync();
            return new ListResultOutput<UserDefaultInformationListDto>(lstUserDefaultInformation.MapTo<List<UserDefaultInformationListDto>>());
        }
    }
}
