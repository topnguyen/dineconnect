﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Hr;
using DinePlan.DineConnect.Hr.Impl;
using DinePlan.DineConnect.Hr.Inventory.Dtos;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Hr.Transaction.Dtos;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;

namespace DinePlan.DineConnect.Hr.Transaction.Implementation
{
    public class LeaveRequestAppService : DineConnectAppServiceBase, ILeaveRequestAppService
    {
        private readonly ILeaveRequestListExcelExporter _leaverequestExporter;
        private readonly ILeaveRequestManager _leaverequestManager;
        private readonly IRepository<LeaveRequest> _leaverequestRepo;
        private readonly IRepository<PersonalInformation> _personalinformationRepo;
        private readonly IRepository<LeaveType> _leavetypeRepo;
        private readonly IRepository<Company> _companyRepo;
        //private readonly IRepository<DutyChart> _dutychartrepo;
        private readonly IRepository<DutyChartDetail> _dutychartdetailrepo;
        //private readonly IRepository<DutyTimeChartMaster> _dutytimechartmasterRepo;
        private readonly IRepository<WorkDay> _workdayRepo;
        private readonly IRepository<PublicHoliday> _publicHoliday;
        private readonly IRepository<YearWiseLeaveAllowedForEmployee> _yearwiseEmployeeLeaveRepo;
        private readonly YearWiseLeaveAllowedForEmployeeAppService _yearWiseLeaveAllowedForEmployeeAppService;
        private readonly IRepository<CommonMailMessage> _commonMailMessageRepo;
        private readonly IRepository<UserDefaultInformation> _userDefaultInformationRepo;
        private readonly IRepository<LeaveExhaustedList> _leaveExhaustedListRepo;
        private readonly IRepository<Location> _locationRepo;


        public LeaveRequestAppService(ILeaveRequestManager leaverequestManager,
            IRepository<LeaveRequest> leaveRequestRepo,
            ILeaveRequestListExcelExporter leaverequestExporter,
            IRepository<PersonalInformation> personalinformationRepo,
            IRepository<Company> companyRepo,
            IRepository<LeaveType> leavetypeRepo,
            //IRepository<DutyChart> dutychartrepo,
            IRepository<DutyChartDetail> dutychartdetailrepo,
            //IRepository<DutyTimeChartMaster> dutytimechartmasterRepo,
            IRepository<WorkDay> workdayRepo,
            IRepository<PublicHoliday> publicHoliday,
            IRepository<YearWiseLeaveAllowedForEmployee> yearwiseEmployeeLeaveRepo,
            YearWiseLeaveAllowedForEmployeeAppService yearWiseLeaveAllowedForEmployeeAppService,
            IRepository<CommonMailMessage> commonMailMessageRepo,
            IRepository<UserDefaultInformation> userDefaultInformation,
            IRepository<LeaveExhaustedList> leaveExhaustedListRepo,
            IRepository<Location> locationRepo
        )
        {
            _leaverequestManager = leaverequestManager;
            _leaverequestRepo = leaveRequestRepo;
            _leaverequestExporter = leaverequestExporter;
            _personalinformationRepo = personalinformationRepo;
            _leavetypeRepo = leavetypeRepo;
            _companyRepo = companyRepo;
            _dutychartdetailrepo = dutychartdetailrepo;
            //_dutychartrepo = dutychartrepo;
            //_dutytimechartmasterRepo = dutytimechartmasterRepo;
            _workdayRepo = workdayRepo;
            _publicHoliday = publicHoliday;
            _yearwiseEmployeeLeaveRepo = yearwiseEmployeeLeaveRepo;
            _yearWiseLeaveAllowedForEmployeeAppService = yearWiseLeaveAllowedForEmployeeAppService;
            _commonMailMessageRepo = commonMailMessageRepo;
            _userDefaultInformationRepo = userDefaultInformation;
            _leaveExhaustedListRepo = leaveExhaustedListRepo;
            _locationRepo = locationRepo;
        }

        public async Task<PagedResultOutput<LeaveRequestListDto>> GetAll(GetLeaveRequestInput input)
        {
            IQueryable<LeaveRequestListDto> allItems;

            if (input.AcYear.HasValue)
            {
                input.StartDate = new DateTime(input.AcYear.Value, 01, 01);
                input.EndDate = new DateTime(input.AcYear.Value, 12, 31);
            }

            var rsLeaveRequest = _leaverequestRepo.GetAll();
            if (input.MinimumDays > 0)
            {
                rsLeaveRequest = rsLeaveRequest.Where(t => t.TotalNumberOfDays > input.MinimumDays);
            }

            if (input.EmployeeRefId > 0)
            {
                rsLeaveRequest = rsLeaveRequest.Where(t => t.EmployeeRefId == input.EmployeeRefId);
            }

            if (input.OmitRejected)
            {
                var rejectedString = L("Rejected");
                rsLeaveRequest = rsLeaveRequest.Where(t => t.LeaveStatus != rejectedString);
            }
            if (input.LeaveTypeRefCode.HasValue)
            {
                rsLeaveRequest = rsLeaveRequest.Where(t => t.LeaveTypeRefCode == input.LeaveTypeRefCode);
            }
            if (input.StartDate != DateTime.MinValue)
            {
                rsLeaveRequest = rsLeaveRequest.Where(t => (input.StartDate >= t.LeaveFrom && input.StartDate <= t.LeaveTo)
                                || (input.EndDate >= t.LeaveFrom && input.EndDate <= t.LeaveTo)
                                || (t.LeaveFrom >= input.StartDate && t.LeaveFrom <= input.EndDate)
                                || (t.LeaveTo >= input.StartDate && t.LeaveTo <= input.EndDate));
            }


            allItems = (from lr in rsLeaveRequest
                        join pi in _personalinformationRepo.GetAll()
                        on lr.EmployeeRefId equals pi.Id
                        join loc in _locationRepo.GetAll() 
                        on pi.LocationRefId equals loc.Id
                        join co in _companyRepo.GetAll()
                        on loc.CompanyRefId equals co.Id
                        join lt in _leavetypeRepo.GetAll()
                        on lr.LeaveTypeRefCode equals lt.Id
                        select new LeaveRequestListDto()
                        {
                            Id = lr.Id,
                            CompanyRefCode = co.Code,
                            EmployeeRefId = lr.EmployeeRefId,
                            EmployeeRefName = pi.EmployeeName,
                            LeaveTypeRefCode = lr.LeaveTypeRefCode,
                            LeaveTypeRefName = lt.LeaveTypeName,
                            DateOfApply = lr.DateOfApply,
                            HalfDayFlag = lr.HalfDayFlag,
                            HalfDayTimeSpecification = lr.HalfDayTimeSpecification,
                            LeaveFrom = lr.LeaveFrom,
                            LeaveTo = lr.LeaveTo,
                            LeaveStatus = lr.LeaveStatus,
                            TotalNumberOfDays = lr.TotalNumberOfDays,
                            CreationTime = lr.CreationTime,
                            LeaveReason = lr.LeaveReason,
                            NationalIdentificationNumber = pi.NationalIdentificationNumber,
                            ContactOfWorkIncharge = lr.ContactOfWorkIncharge,
                            FileName = lr.FileName,
                            FileExtenstionType = lr.FileExtenstionType,
                            IsSupportingDocumentsOverRide = lr.IsSupportingDocumentsOverRide,
                            CommonMailMessageRefId = lr.CommonMailMessageRefId
                        }).WhereIf(
                        !input.Filter.IsNullOrEmpty(),
                        p => p.EmployeeRefName.Contains(input.Filter)
                );

            var sortMenuItems = await allItems
            .OrderBy(input.Sorting)
            .PageBy(input)
            .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<LeaveRequestListDto>>();
            List<string> refEmpids = allListDtos.Where(t => !t.HandOverWorkTo.IsNullOrEmpty()).Select(t => t.HandOverWorkTo).ToList();
            var rsPerInfos = await _personalinformationRepo.GetAllListAsync(t => refEmpids.Contains(t.Id.ToString()));
            foreach (var lst in allListDtos)
            {
                if (!lst.HandOverWorkTo.IsNullOrEmpty())
                {
                    var per = rsPerInfos.FirstOrDefault(t => t.Id.ToString() == lst.HandOverWorkTo);
                    lst.HandOverWorkTo = per.EmployeeName;
                }

            }
            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<LeaveRequestListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetLeaveRequestInput input)
        {
            input.MaxResultCount = AppConsts.MaxPageSize;
            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<LeaveRequestListDto>>();
            return _leaverequestExporter.ExportToFile(allListDtos);
        }

        public async Task<GetLeaveRequestForEditOutput> GetLeaveRequestForEdit(NullableIdInput input)
        {
            LeaveRequestEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _leaverequestRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<LeaveRequestEditDto>();
                var emp = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == editDto.EmployeeRefId);
                editDto.Gender = emp.Gender;
                if (editDto.IsSupportingDocumentsOverRide)
                {
                    editDto.IsDocumentRequired = false;
                }
                else
                {
                    var leavetType = await _leavetypeRepo.FirstOrDefaultAsync(t => t.Id == editDto.LeaveTypeRefCode);
                    if (leavetType.IsSupportingDocumentsRequired)
                    {
                        editDto.IsDocumentRequired = true;
                    }
                    else
                    {
                        editDto.IsDocumentRequired = false;
                    }
                }
            }
            else
            {
                editDto = new LeaveRequestEditDto();
            }

            return new GetLeaveRequestForEditOutput
            {
                LeaveRequest = editDto
            };
        }

        public async Task<GetLeaveRequestForEditOutput> GetLeaveRequestForPrint(IdInput input)
        {

            var allItems = await (from lr in _leaverequestRepo.GetAll().Where(t => t.Id == input.Id)
                                  join pi in _personalinformationRepo.GetAll()
                                  on lr.EmployeeRefId equals pi.Id
                                  join lt in _leavetypeRepo.GetAll()
                                  on lr.LeaveTypeRefCode equals lt.Id

                                  select new LeaveRequestEditDto()
                                  {
                                      Id = lr.Id,
                                      EmployeeRefId = lr.EmployeeRefId,
                                      EmployeeRefName = pi.EmployeeName,
                                      LeaveTypeRefName = lt.LeaveTypeName,
                                      DateOfApply = lr.DateOfApply,
                                      LeaveReason = lr.LeaveReason,
                                      LeaveFrom = lr.LeaveFrom,
                                      LeaveTo = lr.LeaveTo,
                                      LeaveStatus = lr.LeaveStatus,
                                      TotalNumberOfDays = lr.TotalNumberOfDays,
                                      HandOverWorkTo = lr.HandOverWorkTo,
                                      ContactOfWorkIncharge = lr.ContactOfWorkIncharge

                                  }).FirstOrDefaultAsync();


            return new GetLeaveRequestForEditOutput
            {
                LeaveRequest = allItems
            };
        }

        public async Task<LeaveAlreadyTaken> IsEmployeeLeaveRequestOverLappingWithExistingRequestDates(LeaveRequestEditDto input)
        {
            LeaveAlreadyTaken output = new LeaveAlreadyTaken();

            var per = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == input.EmployeeRefId);
            if (per.LastWorkingDate != null || per.ActiveStatus == false)
            {
                throw new UserFriendlyException(L("EmployeeResignedAlready", per.EmployeeName, per.LastWorkingDate.Value.ToString("yyyy-MMM-dd")));
            }

            if (input.LeaveFrom.Year != input.LeaveTo.Year)
            {
                throw new UserFriendlyException(L("LeaveFromToShouldBeInSameAcYear"));
            }

            int existId = 0;

            if (input.Id != null)
                existId = input.Id.Value;

            if (input.LeaveReason == null || input.LeaveReason.Length == 0)
            {
                output.ErrorMessage = L("Reason") + " " + L("Required");
            }

            var existRecordsFromDate = await _leaverequestRepo.GetAll().Where(t => t.EmployeeRefId == input.EmployeeRefId && input.LeaveFrom >= t.LeaveFrom && input.LeaveFrom <= t.LeaveTo && t.Id != existId).ToListAsync();

            var existRecordsToDate = await _leaverequestRepo.GetAll().Where(t => t.EmployeeRefId == input.EmployeeRefId && input.LeaveTo >= t.LeaveFrom && input.LeaveTo <= t.LeaveTo && t.Id != existId).ToListAsync();

            var existRecordsDbFromDate = await _leaverequestRepo.GetAll().Where(t => t.EmployeeRefId == input.EmployeeRefId && t.LeaveFrom >= input.LeaveFrom && t.LeaveFrom <= input.LeaveTo && t.Id != existId).ToListAsync();

            var existRecordsDbToDate = await _leaverequestRepo.GetAll().Where(t => t.EmployeeRefId == input.EmployeeRefId && t.LeaveTo >= input.LeaveFrom && t.LeaveTo <= input.LeaveTo && t.Id != existId).ToListAsync();


            var existRecords = existRecordsFromDate.Union(existRecordsToDate).Union(existRecordsDbFromDate).Union(existRecordsDbToDate).ToList();

            if (existRecords.Count() > 0)
            {
                output.AlreadyAppliedFlag = true;
                foreach (var lst in existRecords)
                {
                    output.ErrorMessage = output.ErrorMessage + " " + L("LeaveAlreadyApplied", lst.LeaveFrom.ToString("yyyy-MMM-dd"), lst.LeaveTo.ToString("yyyy-MMM-dd"), lst.LeaveReason);
                }
            }
            else
            {
                output.AlreadyAppliedFlag = false;
            }

            return output;
        }

        public async Task<ReturnSuccessOrFailureWithMessage> ExistAllServerLevelBusinessRules(CreateOrUpdateLeaveRequestInput input)
        {
            var leaveType = await _leavetypeRepo.FirstOrDefaultAsync(input.LeaveRequest.LeaveTypeRefCode);
            if (leaveType == null)
            {
                throw new UserFriendlyException(L("LeaveType") + " ?");
            }
            if (input.LeaveRequest.LeaveFrom.Year != input.LeaveRequest.LeaveTo.Year)
            {
                throw new UserFriendlyException(L("LeaveFromToShouldBeInSameAcYear"));
            }
            int AcYear = input.LeaveRequest.LeaveFrom.Year;
            var leaveMaster = await _yearwiseEmployeeLeaveRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == input.LeaveRequest.EmployeeRefId && t.AcYear == AcYear && t.LeaveTypeRefCode == input.LeaveRequest.LeaveTypeRefCode);

            if (leaveMaster == null)
            {
                DateTime fromDate = new DateTime(AcYear, 01, 01);
                DateTime toDate = new DateTime(AcYear, 12, 01);
                YearWiseLeaveAllowedForEmployee newLeaveDto = new YearWiseLeaveAllowedForEmployee
                {
                    AcYear = AcYear,
                    LeaveAcYearFrom = fromDate,
                    LeaveAcYearTo = toDate,
                    EmployeeRefId = input.LeaveRequest.EmployeeRefId,
                    LeaveTypeRefCode = leaveType.Id,
                    NumberOfLeaveAllowed = leaveType.NumberOfLeaveAllowed
                };
                await _yearwiseEmployeeLeaveRepo.InsertOrUpdateAndGetIdAsync(newLeaveDto);
                leaveMaster = await _yearwiseEmployeeLeaveRepo.FirstOrDefaultAsync(t => t.EmployeeRefId == input.LeaveRequest.EmployeeRefId && t.AcYear == AcYear && t.LeaveTypeRefCode == input.LeaveRequest.LeaveTypeRefCode);
            }

            var leaveTakenAcYear = await _leaverequestRepo.GetAllListAsync(t => t.EmployeeRefId == input.LeaveRequest.EmployeeRefId
                  && t.LeaveFrom.Year == AcYear && t.LeaveTo.Year == AcYear && t.LeaveTypeRefCode == input.LeaveRequest.LeaveTypeRefCode);
            if (input.LeaveRequest.Id.HasValue)
            {
                leaveTakenAcYear = leaveTakenAcYear.Where(t => t.Id != input.LeaveRequest.Id.Value).ToList();
            }

            decimal totalLeaveTaken = leaveTakenAcYear.Sum(t => t.TotalNumberOfDays);

            if ((totalLeaveTaken + input.LeaveRequest.TotalNumberOfDays) > leaveMaster.NumberOfLeaveAllowed)
            {
                string errorMessage = L("ExceedingErr");
                errorMessage = errorMessage + " " + L("Allowed") + leaveMaster.NumberOfLeaveAllowed;
                errorMessage = errorMessage + " " + L("AlreadyTaken") + totalLeaveTaken;
                errorMessage = errorMessage + " " + L("Available") + (leaveMaster.NumberOfLeaveAllowed - totalLeaveTaken);
                errorMessage = errorMessage + " " + L("Requested") + input.LeaveRequest.TotalNumberOfDays;
                throw new UserFriendlyException(errorMessage);
            }

            #region Leave Request - Exhausted List Verification
            var rsLeaveType = await _leavetypeRepo.FirstOrDefaultAsync(t => t.Id == input.LeaveRequest.LeaveTypeRefCode);
            if (rsLeaveType == null)
            {
                throw new UserFriendlyException(L("NotExistForId", L("LeaveType"), input.LeaveRequest.LeaveTypeRefCode));
            }
            if (rsLeaveType.EligibleOnlyAnnualLeaveExhausted)
            {
                var rsExhausstedList = await _leaveExhaustedListRepo.GetAllListAsync(t => t.LeaveTypeRefId == rsLeaveType.Id);
                if (rsExhausstedList != null || rsExhausstedList.Count > 0)
                {
                    var yearWithEmployeeDto = new YearWithEmployeeDto()
                    {
                        EmployeeRefId = input.LeaveRequest.EmployeeRefId,
                        AcYear = input.LeaveRequest.LeaveFrom.Year
                    };
                    var yearWiseLeaveSummary = await _yearWiseLeaveAllowedForEmployeeAppService.GetEmployeeWiseAcYearWiseLeave(yearWithEmployeeDto);
                    string errorMessage = "";
                    foreach (var lst in rsExhausstedList)
                    {
                        YearWiseLeaveAllowedForEmployeeEditDto exhauseListLeaveExists = yearWiseLeaveSummary.FirstOrDefault(t => t.LeaveTypeRefCode == lst.LeaveExhaustedRefId);
                        if (exhauseListLeaveExists.NumberOfLeaveRemaining > 0)
                        {
                            var lt = await _leavetypeRepo.FirstOrDefaultAsync(t => t.Id == lst.LeaveExhaustedRefId);
                            errorMessage += lt.LeaveTypeName + ",";
                        }
                    }
                    if (errorMessage.Length > 0)
                    {
                        errorMessage = errorMessage.Left(errorMessage.Length - 1);
                        throw new UserFriendlyException(L("SpecifiedLeaveNotExhausted", errorMessage, rsLeaveType.LeaveTypeName));
                    }
                }
            }
            #endregion

            return new ReturnSuccessOrFailureWithMessage { UserErrorNumber = 0, Message = "Success" };
        }

        public async Task<IdInput> CreateOrUpdateLeaveRequest(CreateOrUpdateLeaveRequestInput input)
        {

            //var defaultTime = await _dutytimechartmasterRepo.FirstOrDefaultAsync(t => t.DefaultTimeFlag == true);
            //if (defaultTime == null)
            //{
            //    throw new UserFriendlyException(L("DefaultTimeErr"));
            //}

            var existRecords = await IsEmployeeLeaveRequestOverLappingWithExistingRequestDates(input.LeaveRequest);
            if (existRecords.AlreadyAppliedFlag || existRecords.ErrorMessage != null)
            {
                throw new UserFriendlyException(existRecords.ErrorMessage);
            }
            // Verify Days Allowed

            var returnRules = await ExistAllServerLevelBusinessRules(input);

            IdInput returnId;
            if (input.LeaveRequest.Id.HasValue)
            {
                returnId = await UpdateLeaveRequest(input);
            }
            else
            {
                returnId = await CreateLeaveRequest(input);
            }

            input.LeaveRequest.Id = returnId.Id;

            if (input.AuthorizationId.HasValue)
            {
                if (input.AuthorizationId == 2)
                    await ApproveLeaveRequest(input);

                else if (input.AuthorizationId == 3)
                    await RejectLeaveRequest(input);
            }

            #region Leave_Entry_In_Dutychart_If_DutchartExists
            var dcs = await _dutychartdetailrepo.GetAll().Where(t => t.EmployeeRefId == input.LeaveRequest.EmployeeRefId && t.DutyChartDate >= input.LeaveRequest.LeaveFrom && t.DutyChartDate <= input.LeaveRequest.LeaveTo).ToListAsync();

            foreach (var lst in dcs.Where(t => !t.AllottedStatus.Equals(L("Leave"))))
            {
                var dtoLeaveEmpUpdate = await _dutychartdetailrepo.GetAsync(lst.Id);
                dtoLeaveEmpUpdate.AllottedStatus = L("Leave");
                dtoLeaveEmpUpdate.NightFlag = false;
                dtoLeaveEmpUpdate.ShiftRefId = null;
                dtoLeaveEmpUpdate.StationRefId = null;
                await _dutychartdetailrepo.UpdateAsync(dtoLeaveEmpUpdate);
            }

            #endregion
            //await Leave_ActutalSet(new NullableIdInput { Id = input.LeaveRequest.EmployeeRefId });

            return returnId;
        }

        public async Task DeleteLeaveRequest(IdInput input)
        {
            var leavedetail = await _leaverequestRepo.FirstOrDefaultAsync(t => t.Id == input.Id);
            if (leavedetail != null)
            {
                //Check Duty Chart Details
                var dutychartdetail = await _dutychartdetailrepo.GetAllListAsync(t => t.EmployeeRefId == leavedetail.EmployeeRefId && t.DutyChartDate >= leavedetail.LeaveFrom && t.DutyChartDate <= leavedetail.LeaveTo);
                foreach (var dto in dutychartdetail)
                {
                    if (dto.AllottedStatus == L("Leave") || dto.AllottedStatus == L("ML") || dto.AllottedStatus == L("Native"))
                    {
                        //var pi = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == dto.EmployeeRefId);
                        //throw new UserFriendlyException(L("DutyChartNeedsToChangeFromLeaveFirst", pi.EmployeeCode + " " + pi.EmployeeName, dto.DutyChartDate.ToString("dd-MMM-yyyy")));

                        dto.AllottedStatus = L("Idle");
                    }

                }
            }
            await _leaverequestRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task<IdInput> UpdateLeaveRequest(CreateOrUpdateLeaveRequestInput input)
        {
            var item = await _leaverequestRepo.GetAsync(input.LeaveRequest.Id.Value);
            var dto = input.LeaveRequest;

            //TODO: SERVICE LeaveRequest Update Individually
            item.EmployeeRefId = dto.EmployeeRefId;
            item.LeaveTypeRefCode = dto.LeaveTypeRefCode;
            item.LeaveFrom = dto.LeaveFrom;
            item.LeaveTo = dto.LeaveTo;
            item.LeaveReason = dto.LeaveReason;
            item.LeaveStatus = dto.LeaveStatus;
            item.TotalNumberOfDays = dto.TotalNumberOfDays;
            item.ContactOfWorkIncharge = dto.ContactOfWorkIncharge;
            item.HandOverWorkTo = dto.HandOverWorkTo;
            item.DateOfApply = dto.DateOfApply;
            item.HalfDayFlag = dto.HalfDayFlag;
            item.HalfDayTimeSpecification = dto.HalfDayTimeSpecification;
            if (input.AuthorizationId == 2)
            {
                item.ApprovedPersonRefId = (int)AbpSession.UserId;
                item.ApprovedTime = DateTime.Now;
                item.LeaveStatus = L("Approved");
                item.ApprovedRemarks = L("LeaveRequest") + L("Approved");
            }
            else if (input.AuthorizationId == 3)
            {
                item.ApprovedPersonRefId = (int)AbpSession.UserId;
                item.ApprovedTime = DateTime.Now;
                item.LeaveStatus = L("Rejected");
                item.ApprovedRemarks = dto.ApprovedRemarks;
            }
            //item.ApprovedPersonRefId = (int)AbpSession.UserId;

            CheckErrors(await _leaverequestManager.CreateSync(item));
            return new IdInput { Id = item.Id };
        }

        public async Task ApproveLeaveRequest(CreateOrUpdateLeaveRequestInput input)
        {
            var item = await _leaverequestRepo.GetAsync(input.LeaveRequest.Id.Value);
            var dto = input.LeaveRequest;

            item.ApprovedPersonRefId = (int)AbpSession.UserId;
            item.ApprovedTime = DateTime.Now;
            item.LeaveStatus = L("Approved");
            item.ApprovedRemarks = L("Auto") + L("Approval");

            CheckErrors(await _leaverequestManager.CreateSync(item));
        }
        public async Task<ReceivableMessageOutput> SendMailForApproveLeaveRequest(CreateOrUpdateLeaveRequestInput input)
        {
            ReceivableMessageOutput output = new ReceivableMessageOutput();
            output.SuccessFlag = false;

            var request = HttpContext.Current.Request;
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            var baseUrl = request.Url.Scheme + "://" + request.Url.Authority;
            var leaveType = await _leavetypeRepo.FirstOrDefaultAsync(t => t.Id == input.LeaveRequest.LeaveTypeRefCode);
            if (leaveType == null)
            {
                throw new UserFriendlyException(L("LeaveType") + L("Error"));
            }

            var appliedEmp = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == input.LeaveRequest.EmployeeRefId);
            if (appliedEmp == null)
            {
                throw new UserFriendlyException(L("Error"));
            }

            #region Incharge Person Email
            var inchargePerson = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == appliedEmp.InchargeEmployeeRefId);
            PersonalInformation senderPerson;
            if (inchargePerson == null)
            {
                throw new UserFriendlyException(L("InchargeName") + L("Unknown") + L("Error"));
            }
            else
            {
                senderPerson = inchargePerson;
            }
            if (senderPerson.Email.IsNullOrEmpty() || senderPerson.Email.IsNullOrWhiteSpace())
            {
                throw new UserFriendlyException(L("EmailIdIsEmpty", inchargePerson.EmployeeName));
            }

            var approvedUser = await _userDefaultInformationRepo.FirstOrDefaultAsync(t => t.UserId == (int)AbpSession.UserId);

            if (approvedUser == null)
            {
                throw new UserFriendlyException(L("User") + L("Unknown") + L("Error"));
            }

            var approvedEmp = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == approvedUser.EmployeeRefId);
            if (approvedEmp == null)
            {
                throw new UserFriendlyException(L("Employee") + L("Unknown") + L("Error"));
            }
            else
            {
                senderPerson = approvedEmp;
            }
            if (approvedEmp.Email.IsNullOrEmpty() || approvedEmp.Email.IsNullOrWhiteSpace())
            {
                throw new UserFriendlyException(L("ApprovedBy") + L("EmailIdIsEmpty", approvedEmp.EmployeeName));
            }
            #endregion

            var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == approvedEmp.LocationRefId);

            if (location == null)
            {
                throw new UserFriendlyException(L("Location") + L("Unknown") + L("Error"));
            }

            if (location.Email.IsNullOrEmpty() || location.Email.IsNullOrWhiteSpace())
            {
                throw new UserFriendlyException(L("EmailIdIsEmpty", location.Name));
            }

            var company = await _companyRepo.FirstOrDefaultAsync(t => t.Id == location.CompanyRefId);


            var yearWithEmployeeDto = new YearWithEmployeeDto()
            {
                EmployeeRefId = input.LeaveRequest.EmployeeRefId,
                AcYear = input.LeaveRequest.LeaveFrom.Year
            };
            decimal yearWiseAlreadyAvailed = 0;
            decimal yearWiseAvailable = 0;
            decimal yearWiseAllowed = 0;
            decimal balanceAfterApprove = 0;
            var yearWiseLeaveSummary = await _yearWiseLeaveAllowedForEmployeeAppService.GetEmployeeWiseAcYearWiseLeave(yearWithEmployeeDto);
            var yearWiseLeaveSummaryList = yearWiseLeaveSummary.MapTo<List<YearWiseLeaveAllowedForEmployeeEditDto>>();
            foreach (var lst in yearWiseLeaveSummaryList)
            {
                if (lst.LeaveTypeRefCode == input.LeaveRequest.LeaveTypeRefCode)
                {
                    yearWiseAllowed = lst.NumberOfLeaveAllowed;
                    yearWiseAlreadyAvailed = lst.NumberOfLeaveTaken - input.LeaveRequest.TotalNumberOfDays;
                    yearWiseAvailable = lst.NumberOfLeaveRemaining + input.LeaveRequest.TotalNumberOfDays;
                    balanceAfterApprove = lst.NumberOfLeaveRemaining;
                }
            }

            GetLeaveRequestInput getOldLeave = new GetLeaveRequestInput()
            {
                EmployeeRefId = input.LeaveRequest.EmployeeRefId,
                LeaveTypeRefCode = input.LeaveRequest.LeaveTypeRefCode,
                AcYear = input.LeaveRequest.DateOfApply.Year,
                MaxResultCount = 10000,
                Sorting = "CreationTime DESC",
                OmitRejected = true
            };
            var oldLeaves = await GetAll(getOldLeave);
            var oldLeaveList = oldLeaves.Items.MapTo<List<LeaveRequestListDto>>();

            MailMessage msg = new MailMessage();

            #region Adding Email Address to Mail
            msg.From = new MailAddress("hr@company");
            msg.To.Add(new MailAddress(senderPerson.Email));
            msg.To.Add(new MailAddress(appliedEmp.Email));
            string[] companyEmailList = location.Email.Split(";");

            foreach (var email in companyEmailList)
            {
                msg.CC.Add(new MailAddress(email));
            }
            #endregion
            string outputHtml = "";

            #region MailHeader
            string headerText = "";
            headerText = headerText + " <!DOCTYPE html> ";
            headerText = headerText + "<html> ";
            headerText = headerText + "<head>";
            headerText = headerText + "</head>";
            headerText = headerText + "<body style=\"color: #000066;\">";  //0000ff
            headerText = headerText + "<p><span>Dear @@CustomerCode@@,</span></p>";

            headerText = headerText.Replace("@@CustomerCode@@", appliedEmp.EmployeeName);
            outputHtml = outputHtml + headerText;

            #endregion

            #region Description
            string description = "";

            description += "<p><span>Your below Leave Request have been approved. Please hand over your duty to the person in charge during your absent if there is any.</span></p>";
            //description += "<p><span><b><u>Remarks Note: " + input.LeaveRequest.ApprovedRemarks + "</u></b></span></p>";
            outputHtml = outputHtml + description;
            #endregion

            #region Table Header
            string tableHeader = "";
            tableHeader = tableHeader + "<table border = \"1\">";
            tableHeader = tableHeader + "<tbody>";
            tableHeader = tableHeader + "<tr>";
            tableHeader = tableHeader + "<td style=\"width:122px; \">";
            tableHeader = tableHeader + "<p><span>Leave Type</span></p> ";
            tableHeader = tableHeader + "</td> ";
            tableHeader = tableHeader + "<td style=\"width:156px; \"> ";
            tableHeader = tableHeader + "<p><span>Reason</span></p> ";
            tableHeader = tableHeader + "</td> ";
            tableHeader = tableHeader + "<td style=\"text-align: right; \" width=\"110\"> ";
            tableHeader = tableHeader + "<p><span>Leave From</span></p> ";
            tableHeader = tableHeader + "</td> ";
            tableHeader = tableHeader + "<td style=\"text-align: right; \" width=\"110\"> ";
            tableHeader = tableHeader + "<p><span>Leave To</span></p> ";
            tableHeader = tableHeader + "</td> ";
            tableHeader = tableHeader + "<td style=\"text-align: right; \" width=\"110\"> ";
            tableHeader = tableHeader + "<p><span>Number Of Days</span></p> ";
            tableHeader = tableHeader + "</td> ";
            //tableHeader = tableHeader + "<td style=\"text-align: right; \" width=\"110\"> ";
            //tableHeader = tableHeader + "<p><span>Already Availed</span></p> ";
            //tableHeader = tableHeader + "</td> ";
            //tableHeader = tableHeader + "<td style=\"text-align: right; \" width=\"110\"> ";
            //tableHeader = tableHeader + "<p><span>Available</span></p> ";
            //tableHeader = tableHeader + "</td> ";
            tableHeader = tableHeader + "</tr>";
            outputHtml += tableHeader;
            #endregion

            #region Table Detail
            string tableloopRow = "";
            tableloopRow = tableloopRow + "<tr> ";
            tableloopRow = tableloopRow + "<td>";
            tableloopRow = tableloopRow + "<p><span>@@LEAVETYPE@@<br>";
            tableloopRow = tableloopRow + "</span></p>";
            tableloopRow = tableloopRow + "</td>";
            tableloopRow = tableloopRow + "<td>";
            tableloopRow = tableloopRow + "<p><span>@@REASON@@</span></p>";
            tableloopRow = tableloopRow + "</td>";
            tableloopRow = tableloopRow + "<td style=\"text-align:right; \">";
            tableloopRow = tableloopRow + "<p><span>&nbsp;@@LEAVEFROM@@</span></p>";
            tableloopRow = tableloopRow + "</td>";
            tableloopRow = tableloopRow + "<td style=\"text-align:right; \">";
            tableloopRow = tableloopRow + "<p><span>&nbsp;</span><span>@@LEAVETO@@</span></p>";
            tableloopRow = tableloopRow + "</td>";
            tableloopRow = tableloopRow + "<td style=\"text-align:right; \">";
            tableloopRow = tableloopRow + "<p><span>&nbsp;</span><span>@@NOOFDAYS@@</span></p>";
            tableloopRow = tableloopRow + "</td>";
            //tableloopRow = tableloopRow + "<td style=\"text-align:right; \">";
            //tableloopRow = tableloopRow + "<p><span>&nbsp;</span><span>@@ALREADYAVAILED@@</span></p>";
            //tableloopRow = tableloopRow + "</td>";
            //tableloopRow = tableloopRow + "<td style=\"text-align:right; \">";
            //tableloopRow = tableloopRow + "<p><span>&nbsp;</span><span>@@AVAILABLE@@</span></p>";
            //tableloopRow = tableloopRow + "</td>";
            tableloopRow = tableloopRow + "</tr>";

            string tableRow = tableloopRow;
            tableRow = tableRow.Replace("@@LEAVETYPE@@", leaveType.LeaveTypeName);
            tableRow = tableRow.Replace("@@REASON@@", input.LeaveRequest.LeaveReason);
            tableRow = tableRow.Replace("@@LEAVEFROM@@", input.LeaveRequest.LeaveFrom.ToString("dd-MMM-yyyy"));
            tableRow = tableRow.Replace("@@LEAVETO@@", input.LeaveRequest.LeaveTo.ToString("dd-MMM-yyyy"));
            tableRow = tableRow.Replace("@@NOOFDAYS@@", input.LeaveRequest.TotalNumberOfDays.ToString());
            //tableRow = tableRow.Replace("@@ALREADYAVAILED@@", yearWiseAlreadyAvailed.ToString());
            //tableRow = tableRow.Replace("@@AVAILABLE@@", yearWiseAvailable.ToString());
            outputHtml = outputHtml + tableRow;

            #endregion

            #region Table Footer
            string tableFooter = "";
            tableFooter += "</tbody>";
            tableFooter += "</table>";
            outputHtml += tableFooter;
            #endregion

            if (oldLeaveList.Count > 0)
            {
                outputHtml += "<br>";
                outputHtml += "<p><span><u><strong>Your Leave History</strong></u></span></p>";

                #region Table Header of Old Leave
                string tableHeader1 = "";
                tableHeader1 += "<table border = \"1\">";
                tableHeader1 += "<tbody>";
                tableHeader1 += "<tr>";
                tableHeader1 += "<td style=\"text-align: center; \" width:122px; \">";
                tableHeader1 += "<p><span>Leave Type</span></p> ";
                tableHeader1 += "</td> ";
                tableHeader1 += "<td style=\"text-align: center; \" width:122px; \"> ";
                tableHeader1 += "<p><span>Reason</span></p> ";
                tableHeader1 += "</td> ";
                tableHeader1 += "<td style=\"text-align: right; \" width=\"110\"> ";
                tableHeader1 += "<p><span>Leave From</span></p> ";
                tableHeader1 += "</td> ";
                tableHeader1 += "<td style=\"text-align: right; \" width=\"110\"> ";
                tableHeader1 += "<p><span>Leave To</span></p> ";
                tableHeader1 += "</td> ";
                tableHeader1 += "<td style=\"text-align: right; \" width=\"110\"> ";
                tableHeader1 += "<p><span>Number Of Days</span></p> ";
                tableHeader1 += "</td> ";
                tableHeader1 += "</tr>";
                outputHtml = outputHtml + tableHeader1;
                #endregion

                #region Table Loop Row of Old Leave
                decimal totalLeaveAlreadyTaken = 0;

                string tableloopRow1 = "";
                tableloopRow1 += "<tr> ";
                tableloopRow1 += "<td>";
                tableloopRow1 += "<p><span>@@OLDLEAVETYPE@@<br>";
                tableloopRow1 += "</span></p>";
                tableloopRow1 += "</td>";
                tableloopRow1 += "<td>";
                tableloopRow1 += "<p><span>@@OLDLEAVEREASON@@</span></p>";
                tableloopRow1 += "</td>";
                tableloopRow1 += "<td style=\"text-align:right; \">";
                tableloopRow1 += "<p><span>&nbsp;@@OLDLEAVEFROM@@</span></p>";
                tableloopRow1 += "</td>";
                tableloopRow1 += "<td style=\"text-align:right; \">";
                tableloopRow1 += "<p><span>&nbsp;</span><span>@@OLDLEAVETO@@</span></p>";
                tableloopRow1 += "</td>";
                tableloopRow1 += "<td style=\"text-align:right; \">";
                tableloopRow1 += "<p><span>&nbsp;</span><span>@@OLDNOOFDAYS@@</span></p>";
                tableloopRow1 += "</td>";
                tableloopRow1 += "</tr>";

                //var listInvoices = clientPending.CompanyWisePendingList[0].PendingInvoices.Items;
                //listInvoices = listInvoices.Where(t => t.OverDueDays < 0).ToList();
                //listInvoices = listInvoices.Where(t => Math.Abs(t.OverDueDays) >= noOfDays).ToList();
                foreach (var lst in oldLeaveList)
                {
                    if (lst.Id != input.LeaveRequest.Id)
                    {
                        string tableRow1 = tableloopRow1;
                        tableRow1 = tableRow1.Replace("@@OLDLEAVETYPE@@", lst.LeaveTypeRefName);
                        tableRow1 = tableRow1.Replace("@@OLDLEAVEREASON@@", lst.LeaveReason);
                        tableRow1 = tableRow1.Replace("@@OLDLEAVEFROM@@", lst.LeaveFrom.ToString("dd-MMM-yyyy"));
                        tableRow1 = tableRow1.Replace("@@OLDLEAVETO@@", lst.LeaveTo.ToString("dd-MMM-yyyy"));
                        tableRow1 = tableRow1.Replace("@@OLDNOOFDAYS@@", lst.TotalNumberOfDays.ToString());
                        totalLeaveAlreadyTaken += lst.TotalNumberOfDays;
                        outputHtml = outputHtml + tableRow1;
                    }
                }
                #endregion

                #region Table Footer of Old Leave
                string tableFooter1 = "";
                tableFooter1 = tableFooter1 + "<tr>";
                tableFooter1 = tableFooter1 + "<td colspan=\"4\">";
                tableFooter1 = tableFooter1 + "<p style=\"text-align: right; \"><span><strong>Already Availed </strong></span></p>";
                tableFooter1 = tableFooter1 + "</td>";
                tableFooter1 = tableFooter1 + "<td width=\"110\">";
                tableFooter1 = tableFooter1 + "<p style=\"text-align:right; \"><span><strong>@@TOTAL@@</strong></span></p>";
                tableFooter1 = tableFooter1 + "</td>";
                tableFooter1 = tableFooter1 + "</tr>";
                tableFooter1 = tableFooter1 + "</tbody>";
                tableFooter1 = tableFooter1 + "</table>";
                tableFooter1 = tableFooter1.Replace("@@TOTAL@@", totalLeaveAlreadyTaken.ToString());
                outputHtml = outputHtml + tableFooter1;
                #endregion
            }

            #region MailFooter
            string description1 = "";
            //description1 = description1 + "<p><span><a href=\"" + baseUrl + "/Application/Index#/tenant/hr-master-leaverequest/" + input.LeaveRequest.Id + "\">Click Here to approve the Leave Request!!!</a></span> </p>";
            description1 += "<p><span>*** This is Auto Generated By System ***</span></p>";
            description1 += "<p><span>Regards,</span></p>";
            description1 += "<p><span>" + senderPerson.EmployeeName + "</span></p>";
            description1 += "<p><span>Employee Code &nbsp; : &nbsp;" + senderPerson.EmployeeCode + " </span></p>";
            //description1 += "<p><span>" + senderPerson.Department + "</span></p>";
            description1 += "<p><span>@@COMPANYNAME@@</span></p>";
            description1 += "<p></p>";
            description1 += "</body>";
            description1 += "</html>";

            description1 = description1.Replace("@@COMPANYNAME@@", company.Name);
            outputHtml = outputHtml + description1;
            #endregion
            //string MailSubject = appliedEmp.EmployeeName + "(" + appliedEmp.EmployeeCode + ")" + " requested Leave From " + input.LeaveRequest.LeaveFrom.ToString("dd-MMM-yyyy") + " To " + input.LeaveRequest.LeaveTo.ToString("dd-MMM-yyyy") + " For " + input.LeaveRequest.LeaveReason;

            string MailSubject = "Your Leave Request (From " + input.LeaveRequest.LeaveFrom.ToString("dd-MMM-yyyy") + " To " + input.LeaveRequest.LeaveTo.ToString("dd-MMM-yyyy") + ") has been approved by " + senderPerson.EmployeeName;
            //input.MailSendFlag = true;

            #region Sending Mail
            string mailMessage = outputHtml;
            try
            {
                SmtpClient client = new SmtpClient();
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.EnableSsl = true;
                client.Host = "smtp.gmail.com";
                client.Port = 587;

                System.Net.NetworkCredential credentials =
                  new System.Net.NetworkCredential("hr@company", "");
                client.UseDefaultCredentials = false;
                client.Credentials = credentials;

                msg.Subject = MailSubject;
                msg.IsBodyHtml = true;
                msg.Body = mailMessage.ToString();
                try
                {

                    if (msg.To.Count > 0)
                        client.Send(msg);

                    string receiptnames = msg.To.ToString();
                    CommonMailMessage commonMailMessage = new CommonMailMessage
                    {
                        MailTime = DateTime.Now,
                        MessageType = 4,
                        ReceipientsMailIdsList = receiptnames,
                        MessageText = mailMessage,
                        MessageApprovedBy = (int)AbpSession.UserId
                    };
                    await _commonMailMessageRepo.InsertOrUpdateAndGetIdAsync(commonMailMessage);
                    input.LeaveRequest.CommonMailMessageRefId = commonMailMessage.Id;
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("NotAbleToSendMailError", ex.Message + ex.InnerException));
                }

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("NotAbleToSendMailError", ex.Message + ex.InnerException));
            }
            #endregion

            output.EmailRecieverName = appliedEmp.EmployeeName;
            output.EmailRecieverId = appliedEmp.Email;
            output.SuccessFlag = true;
            output.HtmlMessage = outputHtml;
            return output;
        }
        public async Task<ReceivableMessageOutput> SendMailForRejectLeaveRequest(CreateOrUpdateLeaveRequestInput input)
        {
            ReceivableMessageOutput output = new ReceivableMessageOutput();
            output.SuccessFlag = false;

            var request = HttpContext.Current.Request;
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            var baseUrl = request.Url.Scheme + "://" + request.Url.Authority;
            var leaveRequest = await _leaverequestRepo.FirstOrDefaultAsync(t => t.Id == input.LeaveRequest.Id);
            if (leaveRequest == null)
            {
                throw new UserFriendlyException(L("LeaveRequest") + L("Error"));
            }
            var leaveType = await _leavetypeRepo.FirstOrDefaultAsync(t => t.Id == input.LeaveRequest.LeaveTypeRefCode);
            if (leaveType == null)
            {
                throw new UserFriendlyException(L("LeaveType") + L("Error"));
            }
            //if (leaveRequest.LeaveStatus.Equals(L("Pending")))
            //{

            //}
            //else if (leaveRequest.LeaveStatus.Equals(L("Rejected")))
            //{
            //    throw new UserFriendlyException(L("LeaveRequest") + L("Rejected"));
            //}
            //else
            //{
            //    throw new UserFriendlyException(L("Already") + L("Approved"));
            //}

            var appliedEmp = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == input.LeaveRequest.EmployeeRefId);
            if (appliedEmp == null)
            {
                throw new UserFriendlyException(L("Error"));
            }


            var inchargePerson = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == appliedEmp.InchargeEmployeeRefId);
            PersonalInformation senderPerson;
            if (inchargePerson == null)
            {
                throw new UserFriendlyException(L("Error"));
            }
            else
            {
                senderPerson = inchargePerson;
            }
            if (senderPerson.Email.IsNullOrEmpty() || senderPerson.Email.IsNullOrWhiteSpace())
            {
                throw new UserFriendlyException(L("EmailIdIsEmpty", inchargePerson.EmployeeName));
            }

            var approvedUser = await _userDefaultInformationRepo.FirstOrDefaultAsync(t => t.UserId == input.LeaveRequest.ApprovedPersonRefId);

            if (approvedUser == null)
            {
                throw new UserFriendlyException(L("User") + L("Unknown") + L("Error"));
            }

            var approvedEmp = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == approvedUser.EmployeeRefId);
            if (approvedEmp == null)
            {
                throw new UserFriendlyException(L("Employee") + L("Unknown") + L("Error"));
            }
            else
            {
                senderPerson = approvedEmp;
            }
            if (approvedEmp.Email.IsNullOrEmpty() || approvedEmp.Email.IsNullOrWhiteSpace())
            {
                throw new UserFriendlyException(L("ApprovedBy") + L("EmailIdIsEmpty", approvedEmp.EmployeeName));
            }

            var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == approvedEmp.LocationRefId);

            if (location == null)
            {
                throw new UserFriendlyException(L("Location") + L("Unknown") + L("Error"));
            }

            if (location.Email.IsNullOrEmpty() || location.Email.IsNullOrWhiteSpace())
            {
                throw new UserFriendlyException(L("EmailIdIsEmpty", location.Name));
            }
            var company = await _companyRepo.FirstOrDefaultAsync(t => t.Id == location.CompanyRefId);

            GetLeaveRequestInput getOldLeave = new GetLeaveRequestInput()
            {
                EmployeeRefId = input.LeaveRequest.EmployeeRefId,
                LeaveTypeRefCode = input.LeaveRequest.LeaveTypeRefCode,
                AcYear = input.LeaveRequest.DateOfApply.Year,
                MaxResultCount = 10000,
                Sorting = "CreationTime DESC",
                OmitRejected = true
            };
            var yearWithEmployeeDto = new YearWithEmployeeDto()
            {
                EmployeeRefId = input.LeaveRequest.EmployeeRefId,
                AcYear = input.LeaveRequest.LeaveFrom.Year
            };
            decimal yearWiseAlreadyAvailed = 0;
            decimal yearWiseAvailable = 0;
            decimal yearWiseAllowed = 0;
            decimal balanceAfterApprove = 0;
            var yearWiseLeaveSummary = await _yearWiseLeaveAllowedForEmployeeAppService.GetEmployeeWiseAcYearWiseLeave(yearWithEmployeeDto);
            var yearWiseLeaveSummaryList = yearWiseLeaveSummary.MapTo<List<YearWiseLeaveAllowedForEmployeeEditDto>>();
            foreach (var lst in yearWiseLeaveSummaryList)
            {
                if (lst.LeaveTypeRefCode == input.LeaveRequest.LeaveTypeRefCode)
                {
                    yearWiseAllowed = lst.NumberOfLeaveAllowed;
                    yearWiseAlreadyAvailed = lst.NumberOfLeaveAllowed - input.LeaveRequest.TotalNumberOfDays;
                    yearWiseAvailable = lst.NumberOfLeaveRemaining + input.LeaveRequest.TotalNumberOfDays;
                    balanceAfterApprove = lst.NumberOfLeaveRemaining;
                }
            }
            var oldLeaves = await GetAll(getOldLeave);
            var oldLeaveList = oldLeaves.Items.MapTo<List<LeaveRequestListDto>>();

            MailMessage msg = new MailMessage();

            #region Adding Email Address to Mail
            msg.From = new MailAddress("hr@company");
            msg.To.Add(new MailAddress(senderPerson.Email));
            msg.To.Add(new MailAddress(appliedEmp.Email));
            string[] companyEmailList = location.Email.Split(";");

            foreach (var email in companyEmailList)
            {
                msg.CC.Add(new MailAddress(email));
            }
            #endregion

            string outputHtml = "";

            #region MailHeader
            string headerText = "";
            headerText = headerText + " <!DOCTYPE html> ";
            headerText = headerText + "<html> ";
            headerText = headerText + "<head>";
            headerText = headerText + "</head>";
            headerText = headerText + "<body style=\"color: #000066;\">";  //0000ff
            headerText = headerText + "<p><span>Dear @@CustomerCode@@,</span></p>";

            headerText = headerText.Replace("@@CustomerCode@@", appliedEmp.EmployeeName);
            outputHtml = outputHtml + headerText;

            #endregion

            #region Description
            string description = "";

            description += "<p><span>Your below Leave Request has been rejected.</span></p>";
            description += "<p><span><b><u>Remarks Note: " + input.LeaveRequest.ApprovedRemarks + "</u></b></span></p>";
            description += "<p><span>Please discuss with your incharge or superior.</span></p>";

            outputHtml = outputHtml + description;
            #endregion

            #region Table Header
            string tableHeader = "";
            tableHeader = tableHeader + "<table border = \"1\">";
            tableHeader = tableHeader + "<tbody>";
            tableHeader = tableHeader + "<tr>";
            tableHeader = tableHeader + "<td style=\"width:122px; \">";
            tableHeader = tableHeader + "<p><span>Leave Type</span></p> ";
            tableHeader = tableHeader + "</td> ";
            tableHeader = tableHeader + "<td style=\"width:156px; \"> ";
            tableHeader = tableHeader + "<p><span>Reason</span></p> ";
            tableHeader = tableHeader + "</td> ";
            tableHeader = tableHeader + "<td style=\"text-align: right; \" width=\"110\"> ";
            tableHeader = tableHeader + "<p><span>Leave From</span></p> ";
            tableHeader = tableHeader + "</td> ";
            tableHeader = tableHeader + "<td style=\"text-align: right; \" width=\"110\"> ";
            tableHeader = tableHeader + "<p><span>Leave To</span></p> ";
            tableHeader = tableHeader + "</td> ";
            tableHeader = tableHeader + "<td style=\"text-align: right; \" width=\"110\"> ";
            tableHeader = tableHeader + "<p><span>Number Of Days</span></p> ";
            tableHeader = tableHeader + "</td> ";
            tableHeader = tableHeader + "</tr>";
            outputHtml += tableHeader;
            #endregion

            #region Table Detail
            string tableloopRow = "";
            tableloopRow = tableloopRow + "<tr> ";
            tableloopRow = tableloopRow + "<td>";
            tableloopRow = tableloopRow + "<p><span>@@LEAVETYPE@@<br>";
            tableloopRow = tableloopRow + "</span></p>";
            tableloopRow = tableloopRow + "</td>";
            tableloopRow = tableloopRow + "<td>";
            tableloopRow = tableloopRow + "<p><span>@@REASON@@</span></p>";
            tableloopRow = tableloopRow + "</td>";
            tableloopRow = tableloopRow + "<td style=\"text-align:right; \">";
            tableloopRow = tableloopRow + "<p><span>&nbsp;@@LEAVEFROM@@</span></p>";
            tableloopRow = tableloopRow + "</td>";
            tableloopRow = tableloopRow + "<td style=\"text-align:right; \">";
            tableloopRow = tableloopRow + "<p><span>&nbsp;</span><span>@@LEAVETO@@</span></p>";
            tableloopRow = tableloopRow + "</td>";
            tableloopRow = tableloopRow + "<td style=\"text-align:right; \">";
            tableloopRow = tableloopRow + "<p><span>&nbsp;</span><span>@@NOOFDAYS@@</span></p>";
            tableloopRow = tableloopRow + "</td>";
            tableloopRow = tableloopRow + "</tr>";

            string tableRow = tableloopRow;
            tableRow = tableRow.Replace("@@LEAVETYPE@@", leaveType.LeaveTypeName);
            tableRow = tableRow.Replace("@@REASON@@", input.LeaveRequest.LeaveReason);
            tableRow = tableRow.Replace("@@LEAVEFROM@@", input.LeaveRequest.LeaveFrom.ToString("dd-MMM-yyyy"));
            tableRow = tableRow.Replace("@@LEAVETO@@", input.LeaveRequest.LeaveTo.ToString("dd-MMM-yyyy"));
            tableRow = tableRow.Replace("@@NOOFDAYS@@", input.LeaveRequest.TotalNumberOfDays.ToString());
            outputHtml = outputHtml + tableRow;

            #endregion

            #region Table Footer
            string tableFooter = "";
            tableFooter += "</tbody>";
            tableFooter += "</table>";
            outputHtml += tableFooter;
            #endregion

            if (oldLeaveList.Count > 0)
            {
                outputHtml += "<br>";
                outputHtml += "<p><span><u><strong>Your Leave History</strong></u></span></p>";

                #region Table Header of Old Leave
                string tableHeader1 = "";
                tableHeader1 += "<table border = \"1\">";
                tableHeader1 += "<tbody>";
                tableHeader1 += "<tr>";
                tableHeader1 += "<td style=\"text-align: center; \" width:122px; \">";
                tableHeader1 += "<p><span>Leave Type</span></p> ";
                tableHeader1 += "</td> ";
                tableHeader1 += "<td style=\"text-align: center; \" width:122px; \"> ";
                tableHeader1 += "<p><span>Reason</span></p> ";
                tableHeader1 += "</td> ";
                tableHeader1 += "<td style=\"text-align: right; \" width=\"110\"> ";
                tableHeader1 += "<p><span>Leave From</span></p> ";
                tableHeader1 += "</td> ";
                tableHeader1 += "<td style=\"text-align: right; \" width=\"110\"> ";
                tableHeader1 += "<p><span>Leave To</span></p> ";
                tableHeader1 += "</td> ";
                tableHeader1 += "<td style=\"text-align: right; \" width=\"110\"> ";
                tableHeader1 += "<p><span>Number Of Days</span></p> ";
                tableHeader1 += "</td> ";
                tableHeader1 += "</tr>";
                outputHtml = outputHtml + tableHeader1;
                #endregion

                #region Table Loop Row of Old Leave
                decimal totalLeaveAlreadyTaken = 0;

                string tableloopRow1 = "";
                tableloopRow1 += "<tr> ";
                tableloopRow1 += "<td>";
                tableloopRow1 += "<p><span>@@OLDLEAVETYPE@@<br>";
                tableloopRow1 += "</span></p>";
                tableloopRow1 += "</td>";
                tableloopRow1 += "<td>";
                tableloopRow1 += "<p><span>@@OLDLEAVEREASON@@</span></p>";
                tableloopRow1 += "</td>";
                tableloopRow1 += "<td style=\"text-align:right; \">";
                tableloopRow1 += "<p><span>&nbsp;@@OLDLEAVEFROM@@</span></p>";
                tableloopRow1 += "</td>";
                tableloopRow1 += "<td style=\"text-align:right; \">";
                tableloopRow1 += "<p><span>&nbsp;</span><span>@@OLDLEAVETO@@</span></p>";
                tableloopRow1 += "</td>";
                tableloopRow1 += "<td style=\"text-align:right; \">";
                tableloopRow1 += "<p><span>&nbsp;</span><span>@@OLDNOOFDAYS@@</span></p>";
                tableloopRow1 += "</td>";
                tableloopRow1 += "</tr>";

                //var listInvoices = clientPending.CompanyWisePendingList[0].PendingInvoices.Items;
                //listInvoices = listInvoices.Where(t => t.OverDueDays < 0).ToList();
                //listInvoices = listInvoices.Where(t => Math.Abs(t.OverDueDays) >= noOfDays).ToList();
                foreach (var lst in oldLeaveList)
                {
                    if (lst.Id != input.LeaveRequest.Id)
                    {
                        string tableRow1 = tableloopRow1;
                        tableRow1 = tableRow1.Replace("@@OLDLEAVETYPE@@", lst.LeaveTypeRefName);
                        tableRow1 = tableRow1.Replace("@@OLDLEAVEREASON@@", lst.LeaveReason);
                        tableRow1 = tableRow1.Replace("@@OLDLEAVEFROM@@", lst.LeaveFrom.ToString("dd-MMM-yyyy"));
                        tableRow1 = tableRow1.Replace("@@OLDLEAVETO@@", lst.LeaveTo.ToString("dd-MMM-yyyy"));
                        tableRow1 = tableRow1.Replace("@@OLDNOOFDAYS@@", lst.TotalNumberOfDays.ToString());
                        totalLeaveAlreadyTaken += lst.TotalNumberOfDays;
                        outputHtml = outputHtml + tableRow1;
                    }
                }
                #endregion

                #region Table Footer of Old Leave
                string tableFooter1 = "";
                tableFooter1 = tableFooter1 + "<tr>";
                tableFooter1 = tableFooter1 + "<td colspan=\"4\">";
                tableFooter1 = tableFooter1 + "<p style=\"text-align: right; \"><span><strong>Already Availed </strong></span></p>";
                tableFooter1 = tableFooter1 + "</td>";
                tableFooter1 = tableFooter1 + "<td width=\"110\">";
                tableFooter1 = tableFooter1 + "<p style=\"text-align:right; \"><span><strong>@@TOTAL@@</strong></span></p>";
                tableFooter1 = tableFooter1 + "</td>";
                tableFooter1 = tableFooter1 + "</tr>";
                tableFooter1 = tableFooter1 + "</tbody>";
                tableFooter1 = tableFooter1 + "</table>";
                tableFooter1 = tableFooter1.Replace("@@TOTAL@@", totalLeaveAlreadyTaken.ToString());
                outputHtml = outputHtml + tableFooter1;
                #endregion
            }

            #region MailFooter
            string description1 = "";
            //description1 = description1 + "<p><span><a href=\"" + baseUrl + "/Application/Index#/tenant/hr-master-leaverequest/" + input.LeaveRequest.Id + "\">Click Here to approve the Leave Request!!!</a></span> </p>";
            description1 += "<p><span>*** This is Auto Generated By System ***</span></p>";
            description1 += "<p><span>Regards,</span></p>";
            description1 += "<p><span>" + senderPerson.EmployeeName + "</span></p>";
            description1 += "<p><span>Employee Code &nbsp; : &nbsp;" + senderPerson.EmployeeCode + " </span></p>";
            //description1 += "<p><span>" + senderPerson.Department + "</span></p>";
            description1 += "<p><span>@@COMPANYNAME@@</span></p>";
            description1 += "<p></p>";
            description1 += "</body>";
            description1 += "</html>";

            description1 = description1.Replace("@@COMPANYNAME@@", company.Name);
            outputHtml = outputHtml + description1;
            #endregion

            //string MailSubject = appliedEmp.EmployeeName + "(" + appliedEmp.EmployeeCode + ")" + " requested Leave From " + input.LeaveRequest.LeaveFrom.ToString("dd-MMM-yyyy") + " To " + input.LeaveRequest.LeaveTo.ToString("dd-MMM-yyyy") + " For " + input.LeaveRequest.LeaveReason;

            string MailSubject = "Your Leave Request (From " + input.LeaveRequest.LeaveFrom.ToString("dd-MMM-yyyy") + " To " + input.LeaveRequest.LeaveTo.ToString("dd-MMM-yyyy") + ") has been Rejected by " + senderPerson.EmployeeName;
            //input.MailSendFlag = true;

            #region Sending Mail
            string mailMessage = outputHtml;
            try
            {
                SmtpClient client = new SmtpClient();
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.EnableSsl = true;
                client.Host = "smtp.gmail.com";
                client.Port = 587;

                System.Net.NetworkCredential credentials =
                  new System.Net.NetworkCredential("hr@company", "");
                client.UseDefaultCredentials = false;
                client.Credentials = credentials;

                msg.Subject = MailSubject;
                msg.IsBodyHtml = true;
                msg.Body = mailMessage.ToString();
                try
                {

                    if (msg.To.Count > 0)
                        client.Send(msg);

                    string receiptnames = msg.To.ToString();
                    CommonMailMessage commonMailMessage = new CommonMailMessage
                    {
                        MailTime = DateTime.Now,
                        MessageType = 5,
                        ReceipientsMailIdsList = receiptnames,
                        MessageText = mailMessage,
                        MessageApprovedBy = (int)AbpSession.UserId
                    };
                    await _commonMailMessageRepo.InsertOrUpdateAndGetIdAsync(commonMailMessage);
                    input.LeaveRequest.CommonMailMessageRefId = commonMailMessage.Id;
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("NotAbleToSendMailError", ex.Message + ex.InnerException));
                }

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("NotAbleToSendMailError", ex.Message + ex.InnerException));
            }
            #endregion

            output.EmailRecieverName = appliedEmp.EmployeeName;
            output.EmailRecieverId = appliedEmp.Email;
            output.SuccessFlag = true;
            output.HtmlMessage = outputHtml;
            return output;
        }
        public async Task RejectLeaveRequest(CreateOrUpdateLeaveRequestInput input)
        {
            var item = await _leaverequestRepo.GetAsync(input.LeaveRequest.Id.Value);
            var dto = input.LeaveRequest;

            item.ApprovedPersonRefId = (int)AbpSession.UserId;
            item.ApprovedTime = DateTime.Now;
            item.LeaveStatus = L("Rejected");
            item.ApprovedRemarks = dto.ApprovedRemarks;

            CheckErrors(await _leaverequestManager.CreateSync(item));
        }
        protected virtual async Task<IdInput> CreateLeaveRequest(CreateOrUpdateLeaveRequestInput input)
        {
            var dto = input.LeaveRequest.MapTo<LeaveRequest>();

            CheckErrors(await _leaverequestManager.CreateSync(dto));

            return new IdInput { Id = dto.Id };
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetLeaveTypeForCombobox()
        {
            var lst = await _leavetypeRepo.GetAll().ToListAsync();

            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Select(e => new ComboboxItemDto(e.Id.ToString(), e.LeaveTypeName)).ToList());
        }
        public async Task<List<LeaveTypeListDto>> GetLeaveTypes()
        {
            var lst = await _leavetypeRepo.GetAll().ToListAsync();
            var output = lst.MapTo<List<LeaveTypeListDto>>();
            return output;
        }

        public async Task<List<LeaveTypeListDto>> GetLeaveTypesForMale()
        {
            var lst = await _leavetypeRepo.GetAll().Where(t => t.MaleGenderAllowed == true).ToListAsync();
            var output = lst.MapTo<List<LeaveTypeListDto>>();
            return output;
        }

        public async Task<List<LeaveTypeListDto>> GetLeaveTypesForFemale()
        {
            var lst = await _leavetypeRepo.GetAll().Where(t => t.FemaleGenderAllowed == true).ToListAsync();
            var output = lst.MapTo<List<LeaveTypeListDto>>();
            return output;
        }

        public async Task<decimal> GetNumberOfLeaveDays(LeaveCalculationDto input)
        {
            var rsWorkDays = await _workdayRepo.GetAllListAsync();
            var rsPublicHolidays = await _publicHoliday.GetAll()
                     .Where(t => t.HolidayDate >= input.LeaveFrom && t.HolidayDate <= input.LeaveTo).ToListAsync();
            if (rsWorkDays == null || rsWorkDays.Count == 0)
            {
                throw new UserFriendlyException("WorkDaysNotSetted");
            }
            var emp = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == input.EmployeeRefId);
            if (emp == null)
            {
                throw new UserFriendlyException(L("NotExistForId", L("Employee"), input.EmployeeRefId));
            }
            var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == emp.LocationRefId);

            var company = await _companyRepo.FirstOrDefaultAsync(t => t.Id == location.CompanyRefId);
            if (company == null)
            {
                throw new UserFriendlyException(L("NotExistForId", L("Company"), input.EmployeeRefId));
            }
            DateTime fromDt = input.LeaveFrom;

            List<DateWithWorkDayDto> datesList = new List<DateWithWorkDayDto>();
            while (fromDt <= input.LeaveTo)
            {
                DateWithWorkDayDto det = new DateWithWorkDayDto();
                det.WorkDate = fromDt;
                int dw = (int)fromDt.DayOfWeek;
                var wd = rsWorkDays.FirstOrDefault(t => t.DayOfWeekRefId == dw);
                if (wd == null)
                {
                    throw new UserFriendlyException(L("NotExistForId", L("WorkDay"), dw));
                }
                // Check Public Holiday First
                var ph = rsPublicHolidays.FirstOrDefault(t => t.HolidayDate.Date == fromDt.Date);
                if (ph != null)
                {
                    det.WorkDay = 0;
                }
                else if (dw == emp.DefaultWeekOff)
                {
                    det.WorkDay = 0;
                }
                else
                {
                    det.WorkDay = 1;
                    if (company.NeedToTreatAnyWorkDayAsHalfDay == true)
                    {
                        if (company.WeekDayTreatAsHalfDay.HasValue)
                        {
                            if (dw == company.WeekDayTreatAsHalfDay.Value)
                                det.WorkDay = (decimal)0.5;
                        }
                    }
                }
                datesList.Add(det);
                fromDt = fromDt.AddDays(1);
            }
            decimal totalLeaveDays = datesList.Sum(t => t.WorkDay);
            return totalLeaveDays;
        }

        public async Task<MessageOutput> Leave_ActutalSet(NullableIdInput input)
        {
            var lstLeaves = await _leaverequestRepo.GetAllListAsync(t => t.HalfDayFlag == false);
            if (input.Id.HasValue)
            {
                lstLeaves = lstLeaves.Where(t => t.EmployeeRefId == input.Id.Value).ToList();
            }

            foreach (var lst in lstLeaves)
            {
                if (lst.HalfDayFlag == true)
                    continue;
                var ld = await GetNumberOfLeaveDays(new LeaveCalculationDto { EmployeeRefId = lst.EmployeeRefId, LeaveFrom = lst.LeaveFrom, LeaveTo = lst.LeaveTo });
                if (ld > lst.TotalNumberOfDays)
                {
                    throw new UserFriendlyException("Error");
                }
                if (ld == lst.TotalNumberOfDays)
                    continue;
                lst.TotalNumberOfDays = ld;
            }
            return null;
        }
        public async Task<ReceivableMessageOutput> SendMailForLeaveRequest(IdInput input)
        {
            ReceivableMessageOutput output = new ReceivableMessageOutput();
            output.SuccessFlag = false;

            var request = HttpContext.Current.Request;
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            var baseUrl = request.Url.Scheme + "://" + request.Url.Authority;
            var leaveRequest = await _leaverequestRepo.FirstOrDefaultAsync(t => t.Id == input.Id);
            if (leaveRequest == null)
            {
                throw new UserFriendlyException(L("LeaveRequest") + L("Error"));
            }

            if (leaveRequest.LeaveStatus.Equals(L("Pending")))
            {

            }
            else if (leaveRequest.LeaveStatus.Equals(L("Rejected")))
            {
                throw new UserFriendlyException(L("LeaveRequest") + L("Rejected"));
            }
            else
            {
                throw new UserFriendlyException(L("Already") + L("Approved"));
            }

            var appliedEmp = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == leaveRequest.EmployeeRefId);
            if (appliedEmp == null)
            {
                throw new UserFriendlyException(L("Error"));
            }

            var inchargePerson = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == appliedEmp.InchargeEmployeeRefId);
            string receiverEmailID;
            if (inchargePerson == null)
            {
                throw new UserFriendlyException(L("Error"));
            }
            else
            {
                receiverEmailID = inchargePerson.Email;
            }


            string handoverEmailID = "";
            if (!string.IsNullOrWhiteSpace(leaveRequest.HandOverWorkTo))
            {
                int handOverEmpRefId = Convert.ToInt32(leaveRequest.HandOverWorkTo);
                var handOverPerson = await _personalinformationRepo.FirstOrDefaultAsync(t => t.Id == handOverEmpRefId);

                if (handOverPerson == null)
                {
                    throw new UserFriendlyException(L("HandOverWorkTo") + L("Error"));
                }

                if (handOverPerson.Email != inchargePerson.Email)
                {
                    handoverEmailID = handOverPerson.Email;
                }
            }


            var location = await _locationRepo.FirstOrDefaultAsync(t => t.Id == appliedEmp.LocationRefId);

            if (location == null)
            {
                throw new UserFriendlyException(L("Error"));
            }

            if (location.Email.IsNullOrEmpty() || location.Email.IsNullOrWhiteSpace())
            {
                throw new UserFriendlyException(L("EmailIdIsEmpty", location.Name));
            }
            var company = await _companyRepo.FirstOrDefaultAsync(t => t.Id == location.CompanyRefId);

            GetLeaveRequestInput getOldLeave = new GetLeaveRequestInput()
            {
                EmployeeRefId = leaveRequest.EmployeeRefId,
                LeaveTypeRefCode = leaveRequest.LeaveTypeRefCode,
                AcYear = leaveRequest.DateOfApply.Year,
                MaxResultCount = 10000,
                Sorting = "CreationTime DESC"
            };
            var yearWithEmployeeDto = new YearWithEmployeeDto()
            {
                EmployeeRefId = leaveRequest.EmployeeRefId,
                AcYear = leaveRequest.LeaveFrom.Year
            };
            decimal yearWiseAlreadyAvailed = 0;
            decimal yearWiseAvailable = 0;
            decimal yearWiseAllowed = 0;
            decimal balanceAfterApprove = 0;
            var yearWiseLeaveSummary = await _yearWiseLeaveAllowedForEmployeeAppService.GetEmployeeWiseAcYearWiseLeave(yearWithEmployeeDto);
            var yearWiseLeaveSummaryList = yearWiseLeaveSummary.MapTo<List<YearWiseLeaveAllowedForEmployeeEditDto>>();
            foreach (var lst in yearWiseLeaveSummaryList)
            {
                if (lst.LeaveTypeRefCode == leaveRequest.LeaveTypeRefCode)
                {
                    yearWiseAllowed = lst.NumberOfLeaveAllowed;
                    yearWiseAlreadyAvailed = lst.NumberOfLeaveTaken - leaveRequest.TotalNumberOfDays;
                    yearWiseAvailable = lst.NumberOfLeaveRemaining + leaveRequest.TotalNumberOfDays;
                    balanceAfterApprove = lst.NumberOfLeaveRemaining;
                }
            }
            var oldLeaves = await GetAll(getOldLeave);
            var oldLeaveList = oldLeaves.Items.MapTo<List<LeaveRequestListDto>>();

            MailMessage msg = new MailMessage();
            msg.From = new MailAddress("hr@company");
            msg.To.Add(new MailAddress(receiverEmailID));
            msg.To.Add(new MailAddress(appliedEmp.Email));
            string[] companyEmailList = location.Email.Split(";");

            foreach (var email in companyEmailList)
            {
                msg.CC.Add(new MailAddress(email));
            }
            if (handoverEmailID != "")
            {
                msg.CC.Add(new MailAddress(handoverEmailID));
            }


            string outputHtml = "";

            #region MailHeader
            string headerText = "";
            headerText = headerText + " <!DOCTYPE html> ";
            headerText = headerText + "<html> ";
            headerText = headerText + "<head>";
            headerText = headerText + "</head>";
            headerText = headerText + "<body style=\"color: #000066;\">";  //0000ff
            headerText = headerText + "<p><span>Dear @@CustomerCode@@,</span></p>";

            headerText = headerText.Replace("@@CustomerCode@@", inchargePerson.EmployeeName);
            outputHtml = outputHtml + headerText;

            #endregion

            #region Description
            string description = "";

            description = description + "<p><span>Please Grant me the below Leave Request</span></p>";

            outputHtml = outputHtml + description;
            #endregion

            #region Table Header
            string tableHeader = "";
            tableHeader = tableHeader + "<table border = \"1\">";
            tableHeader = tableHeader + "<tbody>";
            tableHeader = tableHeader + "<tr>";
            tableHeader = tableHeader + "<td style=\"width:122px; \">";
            tableHeader = tableHeader + "<p><span>Leave Type</span></p> ";
            tableHeader = tableHeader + "</td> ";
            tableHeader = tableHeader + "<td style=\"width:156px; \"> ";
            tableHeader = tableHeader + "<p><span>Reason</span></p> ";
            tableHeader = tableHeader + "</td> ";
            tableHeader = tableHeader + "<td style=\"text-align: right; \" width=\"110\"> ";
            tableHeader = tableHeader + "<p><span>Leave From</span></p> ";
            tableHeader = tableHeader + "</td> ";
            tableHeader = tableHeader + "<td style=\"text-align: right; \" width=\"110\"> ";
            tableHeader = tableHeader + "<p><span>Leave To</span></p> ";
            tableHeader = tableHeader + "</td> ";
            tableHeader = tableHeader + "<td style=\"text-align: right; \" width=\"110\"> ";
            tableHeader = tableHeader + "<p><span>Number Of Days</span></p> ";
            tableHeader = tableHeader + "</td> ";
            //tableHeader = tableHeader + "<td style=\"text-align: right; \" width=\"110\"> ";
            //tableHeader = tableHeader + "<p><span>Already Availed</span></p> ";
            //tableHeader = tableHeader + "</td> ";
            //tableHeader = tableHeader + "<td style=\"text-align: right; \" width=\"110\"> ";
            //tableHeader = tableHeader + "<p><span>Available</span></p> ";
            //tableHeader = tableHeader + "</td> ";
            tableHeader = tableHeader + "</tr>";
            outputHtml += tableHeader;
            #endregion

            #region Table Detail
            string tableloopRow = "";
            tableloopRow = tableloopRow + "<tr> ";
            tableloopRow = tableloopRow + "<td>";
            tableloopRow = tableloopRow + "<p><span>@@LEAVETYPE@@<br>";
            tableloopRow = tableloopRow + "</span></p>";
            tableloopRow = tableloopRow + "</td>";
            tableloopRow = tableloopRow + "<td>";
            tableloopRow = tableloopRow + "<p><span>@@REASON@@</span></p>";
            tableloopRow = tableloopRow + "</td>";
            tableloopRow = tableloopRow + "<td style=\"text-align:right; \">";
            tableloopRow = tableloopRow + "<p><span>&nbsp;@@LEAVEFROM@@</span></p>";
            tableloopRow = tableloopRow + "</td>";
            tableloopRow = tableloopRow + "<td style=\"text-align:right; \">";
            tableloopRow = tableloopRow + "<p><span>&nbsp;</span><span>@@LEAVETO@@</span></p>";
            tableloopRow = tableloopRow + "</td>";
            tableloopRow = tableloopRow + "<td style=\"text-align:right; \">";
            tableloopRow = tableloopRow + "<p><span>&nbsp;</span><span>@@NOOFDAYS@@</span></p>";
            tableloopRow = tableloopRow + "</td>";
            //tableloopRow = tableloopRow + "<td style=\"text-align:right; \">";
            //tableloopRow = tableloopRow + "<p><span>&nbsp;</span><span>@@ALREADYAVAILED@@</span></p>";
            //tableloopRow = tableloopRow + "</td>";
            //tableloopRow = tableloopRow + "<td style=\"text-align:right; \">";
            //tableloopRow = tableloopRow + "<p><span>&nbsp;</span><span>@@AVAILABLE@@</span></p>";
            //tableloopRow = tableloopRow + "</td>";
            tableloopRow = tableloopRow + "</tr>";

            string tableRow = tableloopRow;
            tableRow = tableRow.Replace("@@LEAVETYPE@@", oldLeaveList[0].LeaveTypeRefName);
            tableRow = tableRow.Replace("@@REASON@@", leaveRequest.LeaveReason);
            tableRow = tableRow.Replace("@@LEAVEFROM@@", leaveRequest.LeaveFrom.ToString("dd-MMM-yyyy"));
            tableRow = tableRow.Replace("@@LEAVETO@@", leaveRequest.LeaveTo.ToString("dd-MMM-yyyy"));
            tableRow = tableRow.Replace("@@NOOFDAYS@@", leaveRequest.TotalNumberOfDays.ToString());
            //tableRow = tableRow.Replace("@@ALREADYAVAILED@@", yearWiseAlreadyAvailed.ToString());
            //tableRow = tableRow.Replace("@@AVAILABLE@@", yearWiseAvailable.ToString());
            outputHtml = outputHtml + tableRow;

            #endregion

            #region Table Footer
            string tableFooter = "";
            tableFooter += "</tbody>";
            tableFooter += "</table>";
            outputHtml += tableFooter;
            #endregion

            if (yearWiseAlreadyAvailed > 0)
            {
                outputHtml += "<br>";
                outputHtml += "<p><span><u><strong>Leave History</strong></u></span></p>";

                #region Table Header of Old Leave
                string tableHeader1 = "";
                tableHeader1 += "<table border = \"1\">";
                tableHeader1 += "<tbody>";
                tableHeader1 += "<tr>";
                tableHeader1 += "<td style=\"text-align: center; \" width:122px; \">";
                tableHeader1 += "<p><span>Leave Type</span></p> ";
                tableHeader1 += "</td> ";
                tableHeader1 += "<td style=\"text-align: center; \" width:122px; \"> ";
                tableHeader1 += "<p><span>Reason</span></p> ";
                tableHeader1 += "</td> ";
                tableHeader1 += "<td style=\"text-align: right; \" width=\"110\"> ";
                tableHeader1 += "<p><span>Leave From</span></p> ";
                tableHeader1 += "</td> ";
                tableHeader1 += "<td style=\"text-align: right; \" width=\"110\"> ";
                tableHeader1 += "<p><span>Leave To</span></p> ";
                tableHeader1 += "</td> ";
                tableHeader1 += "<td style=\"text-align: right; \" width=\"110\"> ";
                tableHeader1 += "<p><span>Number Of Days</span></p> ";
                tableHeader1 += "</td> ";
                tableHeader1 += "</tr>";
                outputHtml = outputHtml + tableHeader1;
                #endregion

                #region Table Loop Row of Old Leave
                decimal totalLeaveAlreadyTaken = 0;

                string tableloopRow1 = "";
                tableloopRow1 += "<tr> ";
                tableloopRow1 += "<td>";
                tableloopRow1 += "<p><span>@@OLDLEAVETYPE@@<br>";
                tableloopRow1 += "</span></p>";
                tableloopRow1 += "</td>";
                tableloopRow1 += "<td>";
                tableloopRow1 += "<p><span>@@OLDLEAVEREASON@@</span></p>";
                tableloopRow1 += "</td>";
                tableloopRow1 += "<td style=\"text-align:right; \">";
                tableloopRow1 += "<p><span>&nbsp;@@OLDLEAVEFROM@@</span></p>";
                tableloopRow1 += "</td>";
                tableloopRow1 += "<td style=\"text-align:right; \">";
                tableloopRow1 += "<p><span>&nbsp;</span><span>@@OLDLEAVETO@@</span></p>";
                tableloopRow1 += "</td>";
                tableloopRow1 += "<td style=\"text-align:right; \">";
                tableloopRow1 += "<p><span>&nbsp;</span><span>@@OLDNOOFDAYS@@</span></p>";
                tableloopRow1 += "</td>";
                tableloopRow1 += "</tr>";

                //var listInvoices = clientPending.CompanyWisePendingList[0].PendingInvoices.Items;
                //listInvoices = listInvoices.Where(t => t.OverDueDays < 0).ToList();
                //listInvoices = listInvoices.Where(t => Math.Abs(t.OverDueDays) >= noOfDays).ToList();
                foreach (var lst in oldLeaveList)
                {
                    if (lst.Id != input.Id)
                    {
                        string tableRow1 = tableloopRow1;
                        tableRow1 = tableRow1.Replace("@@OLDLEAVETYPE@@", lst.LeaveTypeRefName);
                        tableRow1 = tableRow1.Replace("@@OLDLEAVEREASON@@", lst.LeaveReason);
                        tableRow1 = tableRow1.Replace("@@OLDLEAVEFROM@@", lst.LeaveFrom.ToString("dd-MMM-yyyy"));
                        tableRow1 = tableRow1.Replace("@@OLDLEAVETO@@", lst.LeaveTo.ToString("dd-MMM-yyyy"));
                        tableRow1 = tableRow1.Replace("@@OLDNOOFDAYS@@", lst.TotalNumberOfDays.ToString());
                        totalLeaveAlreadyTaken += lst.TotalNumberOfDays;
                        outputHtml = outputHtml + tableRow1;
                    }
                }
                #endregion

                #region Table Footer of Old Leave
                string tableFooter1 = "";
                tableFooter1 = tableFooter1 + "<tr>";
                tableFooter1 = tableFooter1 + "<td colspan=\"4\">";
                tableFooter1 = tableFooter1 + "<p style=\"text-align: right; \"><span><strong>Already Availed </strong></span></p>";
                tableFooter1 = tableFooter1 + "</td>";
                tableFooter1 = tableFooter1 + "<td width=\"110\">";
                tableFooter1 = tableFooter1 + "<p style=\"text-align:right; \"><span><strong>@@TOTAL@@</strong></span></p>";
                tableFooter1 = tableFooter1 + "</td>";
                tableFooter1 = tableFooter1 + "</tr>";
                tableFooter1 = tableFooter1 + "</tbody>";
                tableFooter1 = tableFooter1 + "</table>";
                tableFooter1 = tableFooter1.Replace("@@TOTAL@@", totalLeaveAlreadyTaken.ToString());
                outputHtml = outputHtml + tableFooter1;
                #endregion
            }

            outputHtml += "<br>";
            outputHtml += "<p><span><u><strong>Leave Summary</strong></u></span></p>";
            #region Table Header of LeaveTypeSummary
            string tableHeader2 = "";
            tableHeader2 += "<table border = \"1\">";
            tableHeader2 += "<tbody>";
            tableHeader2 += "<tr>";
            tableHeader2 += "<td style=\"text-align: center; \" width:122px; \">";
            tableHeader2 += "<p><span>Leave Type</span></p> ";
            tableHeader2 += "</td> ";
            tableHeader2 += "<td style=\"text-align: center; \" width=\"110\"> ";
            tableHeader2 += "<p><span>Total Eligible</span></p> ";
            tableHeader2 += "</td> ";
            tableHeader2 += "<td style=\"text-align: right; \" width=\"110\"> ";
            tableHeader2 += "<p><span>Already Availed</span></p> ";
            tableHeader2 += "</td> ";
            tableHeader2 += "<td style=\"text-align: right; \" width=\"110\"> ";
            tableHeader2 += "<p><span>Available</span></p> ";
            tableHeader2 += "</td> ";
            tableHeader2 += "<td style=\"text-align: right; \" width=\"110\"> ";
            tableHeader2 += "<p><span>Current Requested</span></p> ";
            tableHeader2 += "</td> ";
            tableHeader2 += "<td style=\"text-align: right; \" width=\"150\"> ";
            tableHeader2 += "<p><span>Balance After Approval</span></p> ";
            tableHeader2 += "</td> ";
            tableHeader2 += "</tr>";
            outputHtml += tableHeader2;
            #endregion

            #region Table Detail of LeaveTypeSummary
            string tableloopRow2 = "";
            tableloopRow2 += "<tr> ";
            tableloopRow2 += "<td style=\"text-align:center; \">";
            tableloopRow2 += "<p><span>@@SUMLeaveType@@<br>";
            tableloopRow2 += "</span></p>";
            tableloopRow2 += "</td>";
            tableloopRow2 += "<td style=\"text-align:right; \">";
            tableloopRow2 += "<p><span>@@SUMTotalEligible@@</span></p>";
            tableloopRow2 += "</td>";
            tableloopRow2 += "<td style=\"text-align:right; \">";
            tableloopRow2 += "<p><span>&nbsp;@@SUMAlreadyAvailed@@</span></p>";
            tableloopRow2 += "</td>";
            tableloopRow2 += "<td style=\"text-align:right; \">";
            tableloopRow2 += "<p><span>&nbsp;</span><span>@@SUMAvailable@@</span></p>";
            tableloopRow2 += "</td>";
            tableloopRow2 += "<td style=\"text-align:right; \">";
            tableloopRow2 += "<p><span>&nbsp;</span><span>@@SUMCurrentRequested@@</span></p>";
            tableloopRow2 += "</td>";
            tableloopRow2 += "<td style=\"text-align:right; \">";
            tableloopRow2 += "<p><span>&nbsp;</span><span>@@SUMBalanceAfterApproval@@</span></p>";
            tableloopRow2 += "</td>";
            tableloopRow2 += "</tr>";

            string tableRow2 = tableloopRow2;

            tableRow2 = tableRow2.Replace("@@SUMLeaveType@@", oldLeaveList[0].LeaveTypeRefName);
            tableRow2 = tableRow2.Replace("@@SUMTotalEligible@@", yearWiseAllowed.ToString());
            tableRow2 = tableRow2.Replace("@@SUMAlreadyAvailed@@", yearWiseAlreadyAvailed.ToString());
            tableRow2 = tableRow2.Replace("@@SUMAvailable@@", yearWiseAvailable.ToString());
            tableRow2 = tableRow2.Replace("@@SUMCurrentRequested@@", leaveRequest.TotalNumberOfDays.ToString());
            tableRow2 = tableRow2.Replace("@@SUMBalanceAfterApproval@@", balanceAfterApprove.ToString());
            outputHtml = outputHtml + tableRow2;
            #endregion

            #region Table Footer of LeaveTypeSummary
            string tableFooter2 = "";
            tableFooter2 += "</tbody>";
            tableFooter2 += "</table>";
            outputHtml += tableFooter2;
            #endregion

            #region Description1
            string description1 = "";
            description1 = description1 + "<p><span><a href=\"" + baseUrl + "/Application/Index#/tenant/hr-master-leaverequest/" + leaveRequest.Id + "\">Click Here to Approve/Reject the Leave Request!!!</a></span> </p>";
            description1 += "<p><span>*** This is Auto Generated By System ***</span></p>";
            description1 += "<p><span>Regards,</span></p>";
            description1 += "<p><span>" + appliedEmp.EmployeeName + "</span></p>";
            description1 += "<p><span>Employee Code &nbsp; : &nbsp;" + appliedEmp.EmployeeCode + " </span></p>";
            //description1 += "<p><span>" + appliedEmp.Department + "</span></p>";
            description1 += "<p><span>@@COMPANYNAME@@</span></p>";
            description1 += "<p></p>";
            description1 += "</body>";
            description1 += "</html>";

            description1 = description1.Replace("@@COMPANYNAME@@", company.Name);
            outputHtml = outputHtml + description1;
            #endregion

            string MailSubject = appliedEmp.EmployeeName + "(" + appliedEmp.EmployeeCode + ")" + " requested Leave From " + leaveRequest.LeaveFrom.ToString("dd-MMM-yyyy") + " To " + leaveRequest.LeaveTo.ToString("dd-MMM-yyyy") + " For " + leaveRequest.LeaveReason;
            //input.MailSendFlag = true;

            #region Sending Mail
            string mailMessage = outputHtml;
            try
            {
                SmtpClient client = new SmtpClient();
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.EnableSsl = true;
                client.Host = "smtp.gmail.com";
                client.Port = 587;
            
                System.Net.NetworkCredential credentials =
                  new System.Net.NetworkCredential("hr@company", "");
                client.UseDefaultCredentials = false;
                client.Credentials = credentials;

                msg.Subject = MailSubject;
                msg.IsBodyHtml = true;
                msg.Body = mailMessage.ToString();
                try
                {

                    if (msg.To.Count > 0)
                        client.Send(msg);

                    string receiptnames = msg.To.ToString();
                    CommonMailMessage commonMailMessage = new CommonMailMessage
                    {
                        MailTime = DateTime.Now,
                        MessageType = 3,
                        ReceipientsMailIdsList = receiptnames,
                        MessageText = mailMessage,
                        MessageApprovedBy = (int)AbpSession.UserId
                    };
                    await _commonMailMessageRepo.InsertOrUpdateAndGetIdAsync(commonMailMessage);
                    leaveRequest.CommonMailMessageRefId = commonMailMessage.Id;
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("NotAbleToSendMailError", ex.Message + ex.InnerException));
                }

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(L("NotAbleToSendMailError", ex.Message + ex.InnerException));
            }
            #endregion

            output.EmailRecieverName = inchargePerson.EmployeeName;
            output.EmailRecieverId = receiverEmailID;
            output.SuccessFlag = true;
            output.HtmlMessage = outputHtml;
            return output;
        }

    }
}