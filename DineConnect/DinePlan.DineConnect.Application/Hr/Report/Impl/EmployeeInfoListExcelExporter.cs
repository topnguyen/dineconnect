﻿using System;
using System.Drawing;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Abp.Configuration;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Hr.Report.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace DinePlan.DineConnect.Hr.Report.Impl
{
	public class EmployeeInfoListExcelExporter : FileExporterBase, IEmployeeInfoListExcelExporter
	{

		public EmployeeInfoListExcelExporter(SettingManager settingManager)
		{
			_roundDecimals = settingManager.GetSettingValue<int>(AppSettings.ConnectSettings.Decimals);
			_timeFormat = settingManager.GetSettingValue(AppSettings.ConnectSettings.DateTimeFormat);
		}


		public async Task<FileDto> ExportTimeAttendanceReportToFile(TimeAttendanceReportInput input,
			IEmployeeInfoAppService empService)
		{
			var builder = new StringBuilder();
			builder.Append(L("TimeAttendanceReport"));
			builder.Append(L("-"));
			builder.Append(!input.StartDate.Equals(DateTime.MinValue)
				? input.StartDate.ToString(_simpleDateFormat)
				: DateTime.Now.ToString(_simpleDateFormat));
			builder.Append("_");
			builder.Append(!input.EndDate.Equals(DateTime.MinValue)
				? input.EndDate.ToString(_simpleDateFormat)
				: DateTime.Now.ToString(_simpleDateFormat));

			var file = new FileDto(builder + ".xlsx",
				MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
			if (input.ExportOutputType == 0)
			{
				using (var excelPackage = new ExcelPackage())
				{
					await GetTimeAttendance(excelPackage, input, empService);
					Save(excelPackage, file);
				}
				return ProcessFile(input, file);
			}
			else
			{
				var contentHTML = await ExportDatatableToHtml(input, empService);
				var result = ProcessFileHTML(file, contentHTML);
				return result;
			}
		}

		private async Task GetTimeAttendance(ExcelPackage package, TimeAttendanceReportInput input, IEmployeeInfoAppService appService)
		{

			var sheet = package.Workbook.Worksheets.Add(L("TimeAttendanceReport"));
			sheet.OutLineApplyStyle = true;
			var outPut = await appService.TimeAttendanceReport(input);

			AddHeader(
				sheet,
				L("Location"),
				L("LocationName"),
				L("Employee"),
				L("Name"),
				L("Designation"),
				L("ClockIn"),
				L("ClockOut"),
				L("WorkingHours"),
				L("SummaryWorkingHours")
			);

			AddObjects(
				sheet, 2, outPut.TimeAttendances,
				_ => _.LocationId,
				_ => _.LocationName,
				_ => _.EmployeeCode,
				_ => _.EmployeeName,
				_ => _.Position,
				_ => _.ClockIn.HasValue ? _.ClockIn.Value.ToString(_timeFormat) : "",
				_ => _.ClockOut.HasValue ? _.ClockOut.Value.ToString(_timeFormat) : "",
				_ => _.WorkingHours,
				_ => _.SummaryWorkingHours
			);

			for (var i = 1; i <= 10; i++) sheet.Column(i).AutoFit();

			for (var i = 0; i < outPut.TimeAttendances.Count; i++)
				if (TimeSpan.Compare(outPut.TimeAttendances[i].SummaryWorkingHourTS, input.SummaryWorkingHoursHighlightPoint) == 1)
				{
					var row = $"A{i + 2}:I{i + 2}";
					sheet.Cells[row].Style.Fill.PatternType = ExcelFillStyle.Solid;
					sheet.Cells[row].Style.Fill.BackgroundColor.SetColor(Color.Yellow);
				}
		}


		private FileDto ProcessFileHTML(FileDto file, string fileHTML)
		{
			File.WriteAllText(Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken), fileHTML);
			file.FileType = MimeTypeNames.ApplicationXhtmlXml;
			file.FileName = Path.GetFileNameWithoutExtension(file.FileName) + ".html";
			return file;
		}

		private async Task<string> ExportDatatableToHtml(TimeAttendanceReportInput input, IEmployeeInfoAppService appService)
		{
			var dtos = await appService.TimeAttendanceReport(input);
			StringBuilder strHTMLBuilder = new StringBuilder();
			strHTMLBuilder.Append("<html>");
			strHTMLBuilder.Append("<head>");
			strHTMLBuilder.Append("</head>");

			strHTMLBuilder.Append(@"<style> 
             body  { 
              font:400 12px 'Tahoma';
              font-size: 12px;
              padding:10px;
            }
            table  { 
                margin-top: 10px;
                border-collapse: collapse;
                width: 100%;
                margin-bottom: 10px;
            }

            table td, table th {
                border: 1px solid #ddd;
                padding: 8px;
            }
            table th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #364150;
                color: white;
            }
            thead {
                display: table-header-group;
            }
            tfoot {
                display: table-row-group;
            }
            tr{
                page-break-inside: avoid;
            }
            .text-right{
              text-align:right;
            }

			.margin_bottom_0 {
				margin-bottom: 0 !important;
			}
            .margin_top_0 {
				margin-top: 0 !important;
			}

			.border_bottom_none {
				border-bottom: none !important;
			}

			.custom_table_date {
				margin-bottom: 0px !important;
			}

			.display_flex {
				display: flex;
			}
			.w-100 {
				width: 100%;
			}
	
			.float-right {
				float: right;
			}

			.font_weight_bold {
				font-weight: bold;
			}
            .font_14{
				font-size: 14;
             }
            .text-center{
               text-align: center;          
            }
            .color_yellow {
                  background-color: yellow;
           }

            table tr:hover {background-color: #ddd;}
            </style>");


			strHTMLBuilder.Append("<body>");
			strHTMLBuilder.Append("<div>");
			// add header
			strHTMLBuilder.Append("<table class='table table-bordered custom_table_date border_bottom_none'>");
			strHTMLBuilder.Append("<tr class='font_14'>");
			strHTMLBuilder.Append($"<td colspan='12' class='border_bottom_none text-center font_weight_bold'>{L("TimeAttendanceReport")}</td>");
			strHTMLBuilder.Append("</tr>");


			strHTMLBuilder.Append("<tr class=' font_14'>");
			strHTMLBuilder.Append("</tr>");

			strHTMLBuilder.Append("<tr class=' font_14'>");
			strHTMLBuilder.Append($"<th class=' border_bottom_none text-center font_weight_bold'>{L("Location")}</th>");
			strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("LocationName")}</th>");
			strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("Employee")}</th>");
			strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("Name")}</th>");
			strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("Designation")}</th>");
			strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("ClockIn")}</th>");
			strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("ClockOut")}</th>");
			strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("WorkingHours")}</th>");
			strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("SummaryWorkingHours")}</th>");
			strHTMLBuilder.Append("</tr>");

			// add content
			foreach (var item in dtos.TimeAttendances)
			{																												 
				var highlight = TimeSpan.Compare(item.SummaryWorkingHourTS, input.SummaryWorkingHoursHighlightPoint) == 1 ? "color_yellow" : "";
				strHTMLBuilder.Append($"<tr class='font_14 {highlight}' >");
				strHTMLBuilder.Append($"<td> { item.LocationId}</td> ");
				strHTMLBuilder.Append($"<td> { item.LocationName}</td>");
				strHTMLBuilder.Append($"<td> { item.EmployeeCode}</td>");
				strHTMLBuilder.Append($"<td> { item.EmployeeName}</td>");
				strHTMLBuilder.Append($"<td> { item.Position}</td>");
				strHTMLBuilder.Append($"<td> { (item.ClockIn.HasValue ? item.ClockIn.Value.ToString(_timeFormat) : string.Empty)}</td>");
				strHTMLBuilder.Append($"<td> { (item.ClockOut.HasValue ? item.ClockOut.Value.ToString(_timeFormat) : string.Empty)}</td>");
				strHTMLBuilder.Append($"<td> { item.WorkingHours }</td>");
				strHTMLBuilder.Append($"<td> { item.SummaryWorkingHours }</td>");
			
				strHTMLBuilder.Append("</tr>");
			}
			strHTMLBuilder.Append("</table>");
			strHTMLBuilder.Append("</div>");
			string Htmltext = strHTMLBuilder.ToString();
			return Htmltext;
		}

	}
}