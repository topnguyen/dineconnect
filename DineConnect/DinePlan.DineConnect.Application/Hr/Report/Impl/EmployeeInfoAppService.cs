﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.BackgroundJobs;
using Abp.Collections.Extensions;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Hr.Report.Dto;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Report;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace DinePlan.DineConnect.Hr.Report.Impl
{
    public class EmployeeInfoAppService : DineConnectAppServiceBase, IEmployeeInfoAppService
    {
        private readonly IRepository<AttendanceLog> _attendanceLogRepo;

        private readonly IBackgroundJobManager _bgm;
        private readonly IEmployeeInfoListExcelExporter _employeeinfoExporter;
        private readonly IRepository<JobTitleMaster> _jobTitleMasterRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly ILocationAppService _locService;
        private readonly IRepository<PersonalInformation> _personalInformationRepo;
        private readonly IReportBackgroundAppService _rbas;
        private readonly IUnitOfWorkManager _unitOfManager;
        private readonly int _roundDecimals = 2;


        public EmployeeInfoAppService(SettingManager settingManager, IUnitOfWorkManager uow,
            IEmployeeInfoListExcelExporter employeeinfoExporter,
            IRepository<Location> locationRepo,
            IRepository<AttendanceLog> attendanceLogRepo,
            IRepository<PersonalInformation> personalInformationRepo,
            IRepository<JobTitleMaster> jobTitleMasterRepo,
            ILocationAppService locService,
            IReportBackgroundAppService rbas,
            IBackgroundJobManager bgm)
        {
            _employeeinfoExporter = employeeinfoExporter;
            _locationRepo = locationRepo;
            _attendanceLogRepo = attendanceLogRepo;
            _personalInformationRepo = personalInformationRepo;
            _jobTitleMasterRepo = jobTitleMasterRepo;
            _locService = locService;
            _rbas = rbas;
            _bgm = bgm;
            _roundDecimals = settingManager.GetSettingValue<int>(AppSettings.ConnectSettings.Decimals);
            _unitOfManager = uow;
        }


        public async Task<TimeAttendancesReport> TimeAttendanceReport(TimeAttendanceReportInput input)
        {
            var disableFilters = new List<string> {AbpDataFilters.SoftDelete};

            var attendances = new List<TimeAttendanceReport>();

            var timeAttendancesReport = new TimeAttendancesReport
                {TimeAttendances = attendances};


            using (_unitOfManager.Current.DisableFilter(disableFilters.ToArray()))
            {
                var attendanceLogs = _attendanceLogRepo.GetAll().Where(t => t.CheckTime != null &&
                                                                            t.CheckTimeTrunc >= input.StartDate &&
                                                                            t.CheckTimeTrunc <= input.EndDate
                                                                            && ((CheckType) t.CheckType ==
                                                                                CheckType.TimeIn ||
                                                                                (CheckType) t.CheckType ==
                                                                                CheckType.TimeOut));


                if (attendanceLogs.Any())
                {
                    if (input.Location > 0)
                    {
                        attendanceLogs = attendanceLogs.Where(a => a.LocationRefId == input.Location);
                    }
                    else if (input.Locations != null && input.Locations.Any())
                    {
                        var locations = input.Locations.Select(a => a.Id);
                        attendanceLogs = attendanceLogs.Where(a => locations.Contains(a.LocationRefId));
                    }
                    else if (input.LocationGroup != null && !input.LocationGroup.Group &&
                             !input.LocationGroup.Groups.Any()
                             && !input.LocationGroup.Locations.Any())
                    {
                        var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                        {
                            Locations = input.LocationGroup.Locations,
                            Group = false
                        });
                        if (locations.Any())
                            attendanceLogs = attendanceLogs.Where(a => locations.Contains(a.LocationRefId));
                    }
                    else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
                    {
                        var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                        {
                            Locations = input.LocationGroup.Locations,
                            Group = false
                        });
                        attendanceLogs = attendanceLogs.Where(a => locations.Contains(a.LocationRefId));
                    }
                    else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
                    {
                        var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                        {
                            Groups = input.LocationGroup.Groups,
                            Group = true
                        });
                        attendanceLogs = attendanceLogs.Where(a => locations.Contains(a.LocationRefId));
                    }

                    if (input.LocationGroup?.NonLocations != null && input.LocationGroup.NonLocations.Any())
                    {
                        var nonLocations = input.LocationGroup.NonLocations.Select(a => a.Id);
                        attendanceLogs = attendanceLogs.Where(a => !nonLocations.Contains(a.LocationRefId));
                    }


                    foreach (var attendanceLog in attendanceLogs.GroupBy(a =>
                        new
                        {
                            Employee = a.EmployeeRefId,
                            Location = a.LocationRefId,
                            Date = a.CheckTimeTrunc
                        }))
                        if (attendanceLog.Count() == 1)
                        {
                            var myClock = attendanceLog.LastOrDefault();
                            var employee = await _personalInformationRepo.GetAsync(attendanceLog.Key.Employee);
                            var plant = await _locationRepo.GetAsync(attendanceLog.Key.Location);
                            JobTitleMaster jobTitleMaster = null;
                            if (employee != null && employee.JobTitleRefId != null)
                                jobTitleMaster = await _jobTitleMasterRepo.GetAsync(employee.JobTitleRefId.Value);

                            var attendance = new TimeAttendanceReport
                            {
                                LocationName = plant != null ? plant.Name : null,
                                EmployeeId = employee != null ? employee.Id.ToString() : null,
                                EmployeeName = employee != null ? employee.EmployeeName : null,
                                EmployeeCode = employee != null ? employee.EmployeeCode : null,
                                LocationId = plant != null ? plant.Id.ToString() : null,
                                Position = jobTitleMaster != null ? jobTitleMaster.JobTitle : null,
                                ClockIn = (CheckType) myClock.CheckType == CheckType.TimeIn ? myClock.CheckTime : null,
                                ClockOut = (CheckType) myClock.CheckType == CheckType.TimeOut ? myClock.CheckTime : null
                            };
                            attendances.Add(attendance);
                        }
                        else
                        {
                            var employee = await _personalInformationRepo.GetAsync(attendanceLog.Key.Employee);
                            var plant = await _locationRepo.GetAsync(attendanceLog.Key.Location);
                            JobTitleMaster jobTitleMaster = null;
                            if (employee != null && employee.JobTitleRefId != null)
                                jobTitleMaster = await _jobTitleMasterRepo.GetAsync(employee.JobTitleRefId.Value);

                            var timeIn = true;
                            var timeOut = true;

                            AttendanceLog timeInLog = null;
                            AttendanceLog timeOutLog = null;

                            TimeAttendanceReport lastAttendance = null;
                            foreach (var log in attendanceLog.OrderBy(a => a.CheckTime))
                            {
                                var myType = (CheckType) log.CheckType;

                                if (myType == CheckType.TimeIn)
                                {
                                    if (timeIn)
                                    {
                                        timeInLog = log;
                                        timeIn = false;
                                        continue;
                                    }

                                    if (!timeOut)
                                    {
                                        var myTimeAttendanceReport = AddAttendance(plant, employee, jobTitleMaster, timeInLog, timeOutLog,
                                            lastAttendance, attendances);

                                        lastAttendance = myTimeAttendanceReport;

                                        timeIn = false;
                                        timeInLog = log;

                                        timeOut = true;
                                        timeOutLog = null;
                                    }
                                }

                                if (myType == CheckType.TimeOut)
                                {
                                    timeOutLog = log;
                                    timeOut = false;
                                }
                            }

                            if (!timeIn && !timeOut && timeInLog != null && timeOutLog != null)
                            {
                                var myTimeAttendanceReport = AddAttendance(plant, employee, jobTitleMaster, timeInLog, timeOutLog,
                                    lastAttendance, attendances);
                            }
                        }
                    if(!input.FilterEmployeeName.IsNullOrWhiteSpace())
                    {
                        attendances = attendances.Where(t => t.EmployeeName.Contains(input.FilterEmployeeName)).ToList();
                    }

                    if (!string.IsNullOrEmpty(input.DynamicFilter))
                    {
                        var jsonSerializerSettings = new JsonSerializerSettings
                        {
                            ContractResolver =
                        new CamelCasePropertyNamesContractResolver()
                        };
                        var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                        if (filRule?.Rules != null)
                        {
                            foreach (var lst in filRule.Rules)
                            {
                                if (lst.Field == "Location")
                                {
                                    attendances =
                                        attendances.Where(t => t.LocationId==lst.Value).ToList();
                                }
                                else if (lst.Field == "Name")
                                {
                                    attendances =
                                        attendances.Where(t => t.EmployeeName.ToUpper().Contains(lst.Value.ToUpper())).ToList();
                                }
                                else if (lst.Field == "EmployeeId")
                                {
                                    attendances =
                                        attendances.Where(t => t.EmployeeId==lst.Value).ToList();
                                }
                                else if (lst.Field == "Position")
                                {
                                    attendances =
                                        attendances.Where(t => t.Position.ToUpper().Contains(lst.Value.ToUpper())).ToList();
                                }
                            }
                        }
                    }
                }
                
                timeAttendancesReport.TimeAttendances =
                    attendances.AsQueryable().OrderBy(input.Sorting).PageBy(input).ToList();
            }

            return timeAttendancesReport;
        }
        private TimeAttendanceReport AddAttendance(Location plant, PersonalInformation employee, JobTitleMaster jobTitleMaster,
            AttendanceLog timeInLog, AttendanceLog timeOutLog, TimeAttendanceReport lastAttendance,
            List<TimeAttendanceReport> attendees)
        {
            var attendance = new TimeAttendanceReport
            {
                LocationName = plant != null ? plant.Name : null,
                EmployeeId = employee != null ? employee.Id.ToString() : null,
                EmployeeName = employee != null ? employee.EmployeeName : null,
                LocationId = plant != null ? plant.Id.ToString() : null,
                Position = jobTitleMaster != null ? jobTitleMaster.JobTitle : null,
                ClockIn = timeInLog.CheckTime,
                ClockOut = timeOutLog.CheckTime
            };

            var myTime = CalculteWorkingHoursInDate(attendance.ClockIn, attendance.ClockOut);
            if (myTime.HasValue)
            {
                attendance.WorkingHoursTS = myTime.Value;
                attendance.WorkingHours =
                    $"{myTime.Value.Hours:D2}:{myTime.Value.Minutes:D2}";
                attendance.SummaryWorkingHourTS = myTime.Value;
            }

            attendees.Add(attendance);
            if (lastAttendance != null)
                if (lastAttendance.LocationId == attendance.LocationId &&
                    lastAttendance.EmployeeId == attendance.EmployeeId)
                    attendance.SummaryWorkingHourTS += lastAttendance.SummaryWorkingHourTS;

            return attendance;
        }
        public async Task<FileDto> GetExcelOfTimeAttendanceReport(TimeAttendanceReportInput input)
        {
            input.SummaryWorkingHoursHighlightPoint = TimeAttendanceReportConstants.SummaryWorkingHoursHighlightPoint;
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.TIMEATTENDANCE,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.TIMEATTENDANCE,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value,
                            TimeAttendanceReportInput = input
                        });
                }
                else
                {
                    return await _employeeinfoExporter.ExportTimeAttendanceReportToFile(input, this);
                }
            }

            return null;
        }

      

      
        private TimeSpan? CalculteWorkingHoursInDate(DateTime? checkTime, DateTime? logCheckTime)
        {
            if (checkTime != null && logCheckTime != null)
            {
                var myCheckInTime = checkTime.Value;
                var myCheckOutTime = logCheckTime.Value;

                return myCheckOutTime.Subtract(myCheckInTime);
            }

            return null;
        }


        public class TimeAttendanceReportConstants
        {
            public static TimeSpan SummaryWorkingHoursHighlightPoint = new TimeSpan(0, 12, 0, 0);
        }
    }
}