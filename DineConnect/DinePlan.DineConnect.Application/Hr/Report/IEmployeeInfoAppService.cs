﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Hr.Report.Dto;

namespace DinePlan.DineConnect.Hr.Report
{
    public interface IEmployeeInfoAppService : IApplicationService
    {
        Task<TimeAttendancesReport> TimeAttendanceReport(TimeAttendanceReportInput input);
        Task<FileDto> GetExcelOfTimeAttendanceReport(TimeAttendanceReportInput input);

    }
}