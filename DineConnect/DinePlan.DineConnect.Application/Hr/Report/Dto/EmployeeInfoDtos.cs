﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Extensions;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter;
using DinePlan.DineConnect.Swipe.Master.Dtos;
using OpenHtmlToPdf;

namespace DinePlan.DineConnect.Hr.Report.Dto
{
    [AutoMapFrom(typeof(PersonalInformation))]
    public class EmployeeInfoListDto : FullAuditedEntityDto
    {
        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string HomePhone { get; set; }
        public string MobilePhone { get; set; }
        public bool IsActive { get; set; }
        public virtual int? CardRefId { get; set; }
        public long? UserId { get; set; }
        public int? EmployeeDepartmentId { get; set; }

		public int TimeStart { get; set; }
    }
	[AutoMapTo(typeof(PersonalInformation))]
	public class EmployeeInfoEditDto
	{
		public int? Id { get; set; }
		public string EmployeeName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Notes { get; set; }
        public string Email { get; set; }
        public string HomePhone { get; set; }
        public string MobilePhone { get; set; }
        public DateTime HireDate { get; set; }
        public string WageType { get; set; }
        public int Wage { get; set; }
        public string SkillLevel { get; set; }
        public int MaxWeeklyHours { get; set; }
        public int EmployeeId { get; set; }
        public int PunchId { get; set; }


        public string EmployeeCode { get; set; }
		public bool IsActive { get; set; }
        public long? UserId { get; set; }
        public int? EmployeeDepartmentId { get; set; }
		public virtual int? CardRefId { get; set; }

		public virtual SwipeCardEditDto CardInfo {get;set;}
    }

    public class GetEmployeeInfoInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
    public class GetEmployeeInfoForEditOutput : IOutputDto
    {
        public EmployeeInfoEditDto EmployeeInfo { get; set; }
    }
    public class CreateOrUpdateEmployeeInfoInput : IInputDto
    {
        [Required]
        public EmployeeInfoEditDto EmployeeInfo { get; set; }
    }


    public class EmployeeRawInput : IInputDto
    {
        public string Code { get; set; }
        public string Identity { get; set; }
        public byte[] IdenitityData { get; set; }
    }

    public class EmployeeRawOutput : IOutputDto
    {
        public bool Error { get; set; }
        public string ErrorDescription{ get; set; }
        public string Identity { get; set; }
        public byte[] IdenitityData { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
        public int? DinePlanUserId { get; set; }
    }

    public class TimeAttendancesReport
    {
        public List<TimeAttendanceReport> TimeAttendances { get; set; }
    }
    public class TimeAttendanceReport
    {
        public string LocationId { get; set; }
        public string LocationName { get; set; }
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string Position { get; set; }
        public DateTime? ClockIn { get; set; }
        public DateTime? ClockOut { get; set; }
        public string EmployeeCode { get; set; }

        public string WorkingHours { get; set; }
        public TimeSpan WorkingHoursTS { get; set; }


        public string SummaryWorkingHours => $"{SummaryWorkingHourTS.Hours:D2}:{SummaryWorkingHourTS.Minutes:D2}";

        public TimeSpan SummaryWorkingHourTS { get; set; }
    }

    public class TimeAttendanceReportInput: PagedAndSortedInputDto, IShouldNormalize, IFileExport, IBackgroundExport
    {

        public TimeAttendanceReportInput()
        {
            Locations = new List<SimpleLocationDto>();
            LocationGroup = new LocationGroupDto();
        }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string FilterEmployeeName { get; set; }
        public List<SimpleLocationDto> Locations { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public int Location { get; set; }
        public bool RunInBackground { get; set; }
        public string ReportDescription { get; set; }
        public FileDto FileDto { get; set; }
        public TimeSpan SummaryWorkingHoursHighlightPoint { get; set; }
        public ExportType ExportOutputType { get; set; }
        public PaperSize PaperSize { get; set; }
        public bool Portrait { get; set; }
        public string DynamicFilter { get; set; }
        public void Normalize()
        {
            if (Sorting.IsNullOrWhiteSpace())
            {
                Sorting = "EmployeeId";
            }
        }

    }
}

