﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Hr.Report.Dto;

namespace DinePlan.DineConnect.Hr.Report
{
    public interface IEmployeeInfoListExcelExporter
    {

        Task<FileDto> ExportTimeAttendanceReportToFile(TimeAttendanceReportInput input, IEmployeeInfoAppService empService);

    }
}