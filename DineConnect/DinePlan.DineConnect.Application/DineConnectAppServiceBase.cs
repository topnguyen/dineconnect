﻿using Abp.Application.Services;
using Abp.IdentityFramework;
using Abp.MultiTenancy;
using Abp.Runtime.Session;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Connect;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Filter;
using DinePlan.DineConnect.MultiTenancy;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using Abp.Configuration;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Audit;
using DinePlan.DineConnect.Helper;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect
{
    /// <summary>
    /// All application services in this application is derived from this class.
    /// We can add common application service methods here.
    /// </summary>

    public abstract class DineConnectAppServiceBase : ApplicationService
    {
        public TenantManager TenantManager { get; set; }

        public UserManager UserManager { get; set; }

        private string _dTFormat = "yyyy-MM-dd hh:mm tt";

        private  string _dateFormat = "yyyy-MM-dd";

        protected DineConnectAppServiceBase()
        {
            LocalizationSourceName = DineConnectConsts.LocalizationSourceName;
        }

        protected virtual Task<User> GetCurrentUserAsync()
        {
            var user = UserManager.FindByIdAsync(AbpSession.GetUserId());
            if (user == null)
            {
                throw new ApplicationException("There is no current user!");
            }

            return user;
        }

        protected virtual int? GetCurrentUserLocationAsync()
        {
            return UserManager.GetDefaultLocationForUserAsync(AbpSession.GetUserId());
        }
        protected ExternalLog LogException(Exception e, ExternalLogType logType)
        {
            var myLog = new ExternalLog
            {
                AuditType = (int) logType,
                LogData = e.Message,
                LogDescription = FlattenException(e),
                LogTime = DateTime.Now,
                ExternalLogTrunc = DateTime.Now.Date
            };
            return myLog;
        }

        private static string FlattenException(Exception exception)
        {
            var stringBuilder = new StringBuilder();

            while (exception != null)
            {
                stringBuilder.AppendLine(exception.Message);
                stringBuilder.AppendLine(exception.StackTrace);

                exception = exception.InnerException;
            }

            return stringBuilder.ToString();
        }
        protected virtual User GetCurrentUser()
        {
            var user = UserManager.FindById(AbpSession.GetUserId());
            if (user == null)
            {
                throw new ApplicationException("There is no current user!");
            }

            return user;
        }

        protected virtual Task<Location> GetLocationInfo(int? argLocationId)
        {
            return UserManager.GetDefaultLocationInfoAsync(argLocationId);
        }

        protected virtual Task<Tenant> GetCurrentTenantAsync()
        {
            return TenantManager.GetByIdAsync(AbpSession.GetTenantId());
        }

        protected virtual Tenant GetCurrentTenant()
        {
            return TenantManager.GetById(AbpSession.GetTenantId());
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
        public virtual string GetDateFormat()
        {
            try
            {
                if (SettingManager != null && GetCurrentTenant() != null && GetCurrentTenant().Id > 0)
                {
                    var myFormat = SettingManager.GetSettingValueForTenant(AppSettings.ConnectSettings.SimpleDateFormat,
                        GetCurrentTenant().Id);
                    if (!string.IsNullOrEmpty(myFormat))
                    {
                        _dateFormat = myFormat;
                    }
                }
            }
            catch (Exception)
            {
                // ignored
            }
            return _dateFormat;
        }
        public virtual string GetDateTimeFormat()
        {

            try
            {
                if (SettingManager != null && GetCurrentTenant() != null && GetCurrentTenant().Id > 0)
                {
                    var myFormat = SettingManager.GetSettingValueForTenant(AppSettings.ConnectSettings.DateTimeFormat,
                        GetCurrentTenant().Id);
                    if (!string.IsNullOrEmpty(myFormat))
                    {
                        _dTFormat = myFormat;
                    }
                }
            }
            catch (Exception)
            {
                // ignored
            }
            return _dTFormat;
            
        }

        protected void UpdateLocationAndNonLocationForEdit(ConnectEditDto editDto, LocationGroupDto output)
        {
            /*Start Group*/
            output.Group = editDto.Group;
            output.LocationTag = editDto.LocationTag;
            if (!string.IsNullOrEmpty(editDto.Locations))
            {
                if (editDto.Group)
                {
                    output.Groups =
                        JsonConvert.DeserializeObject<List<SimpleLocationGroupDto>>(editDto.Locations);
                }
                else if (editDto.LocationTag)
                {
                    output.LocationTags =
                        JsonConvert.DeserializeObject<List<SimpleLocationTagDto>>(editDto.Locations);
                }
                else
                {
                    output.Locations =
                        JsonConvert.DeserializeObject<List<SimpleLocationDto>>(editDto.Locations);
                }
            }

            if (!string.IsNullOrEmpty(editDto.NonLocations))
            {
                output.NonLocations =
                    JsonConvert.DeserializeObject<List<SimpleLocationDto>>(editDto.NonLocations);
            }
            /*End Group*/
        }

        protected void UpdateLocationAndNonLocation(ConnectFullMultiTenantAuditEntity item, LocationGroupDto lgroup)
        {
            var allLocations = "";
            var allNonLocation = "";

            if (lgroup != null)
            {
                if (lgroup.Group && DynamicQueryable.Any(lgroup.Groups))
                {
                    allLocations = JsonConvert.SerializeObject(lgroup.Groups);
                }
                else if (!lgroup.Group && lgroup.LocationTag && DynamicQueryable.Any(lgroup.LocationTags))
                {
                    allLocations = JsonConvert.SerializeObject(lgroup.LocationTags);
                }
                else if (!lgroup.Group && !lgroup.LocationTag && DynamicQueryable.Any(lgroup.Locations))
                {
                    allLocations = JsonConvert.SerializeObject(lgroup.Locations);
                }

                if (lgroup.NonLocations != null && DynamicQueryable.Any(lgroup.NonLocations))
                {
                    allNonLocation = JsonConvert.SerializeObject(lgroup.NonLocations);
                }
                item.Group = lgroup.Group;
                item.LocationTag = lgroup.LocationTag;
            }
            item.Locations = allLocations;
            item.NonLocations = allNonLocation;
        }

        protected IEnumerable<ConnectFullMultiTenantAuditEntity> SearchLocation(IQueryable<ConnectFullMultiTenantAuditEntity> allEntities, LocationGroupDto lgGroup)
        {
            if (allEntities.Any() && lgGroup != null)
            {
                if (!lgGroup.LocationTag && !lgGroup.Group && lgGroup.Locations.Any())
                {
                    var allCodes = lgGroup.Locations.Select(a => a.Id).ToList();
                    var mllEntities = allEntities.Where(a=>!a.LocationTag && !a.Group && !string.IsNullOrEmpty(a.Locations)).ToList();
                    var finalReturnEntities = mllEntities.Where(a =>
                        (!string.IsNullOrEmpty(a.Locations) && CompareList(a.Locations, allCodes)) || string.IsNullOrEmpty(a.Locations));

                    return finalReturnEntities;
                }

                if (lgGroup.Group & lgGroup.Groups.Any())
                {
                    var allCodes = lgGroup.Groups.Select(a => a.Id).ToList();
                    var mllEntities = allEntities.Where(a => !string.IsNullOrEmpty(a.Locations) && a.Group).ToList();
                    var finalReturnEntities = mllEntities.Where(a => CompareList(a.Locations, allCodes));
                    return finalReturnEntities;

                }

                if (lgGroup.LocationTag & lgGroup.LocationTags.Any())
                {

                    var allCodes = lgGroup.LocationTags.Select(a => a.Id).ToList();
                    var mllEntities = allEntities.Where(a => !string.IsNullOrEmpty(a.Locations) && a.LocationTag).ToList();
                    var finalReturnEntities = mllEntities.Where(a => CompareList(a.Locations, allCodes));
                    return finalReturnEntities;
                }
            }
            return allEntities.ToList();
           
        }

        protected List<int> GetLocations(ILocationAppService locationService, ILocationFilterDto input)
        {
            
            if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                return locationService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
            }
            if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                return locationService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true
                });
            }

            if (input.LocationGroup?.NonLocations != null && input.LocationGroup.NonLocations.Any())
            {
                return input.LocationGroup.NonLocations.Select(a => a.Id).ToList();
            }

            return locationService.GetLocationForUserAndGroup(new LocationGroupDto
            {
                Locations = new List<SimpleLocationDto>(),
                Group = false,
                UserId = input.UserId
            });
        }

        protected bool CompareList(string locations, List<int> listB)
        {
            if (!string.IsNullOrEmpty(locations))
            {
                var allLo = JsonHelper.Deserialize<List<SimpleLocationDto>>(locations);
                if (allLo != null && allLo.Any())
                {
                    List<int> listA = allLo.Select(a => a.Id).ToList();
                    return listA.Any(x => listB.Contains(x));

                }
            }

            return false;
        }
    }
}