﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Swipe.Master.Dtos;

namespace DinePlan.DineConnect.Swipe.Master
{
    public interface ISwipeCardAppService : IApplicationService
    {
        Task<PagedResultOutput<SwipeCardListDto>> GetAll(GetSwipeCardInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetSwipeCardForEditOutput> GetSwipeCardForEdit(NullableIdInput nullableIdInput);
		Task<SwipeCardListDto> CreateOrUpdateSwipeCard(CreateOrUpdateSwipeCardInput input);
        Task DeleteSwipeCard(IdInput input);
		Task<ListResultOutput<SwipeCardTypeListDto>> GetSwipeCardTypes();
		Task<string> GenerateSwipeCards(GenerateCardsInputDto input);
	}
}