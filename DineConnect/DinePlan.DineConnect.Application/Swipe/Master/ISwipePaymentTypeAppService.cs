﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Swipe.Master.Dtos;

namespace DinePlan.DineConnect.Swipe.Master
{
    public interface ISwipePaymentTypeAppService : IApplicationService
    {
        Task<PagedResultOutput<SwipePaymentTypeListDto>> GetAll(GetSwipePaymentTypeInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetSwipePaymentTypeForEditOutput> GetSwipePaymentTypeForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateSwipePaymentType(CreateOrUpdateSwipePaymentTypeInput input);
        Task DeleteSwipePaymentType(IdInput input);
        Task<ListResultDto<ApiSwipePayment>> ApiGetAll();
    }
}