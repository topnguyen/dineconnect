﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Swipe.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Swipe.Master
{
    public interface ISwipeCardListExcelExporter
    {
        FileDto ExportToFile(List<SwipeCardListDto> dtos);
    }
}
