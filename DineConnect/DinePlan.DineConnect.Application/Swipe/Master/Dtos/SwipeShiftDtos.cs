﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Swipe.Master.Dtos
{
	[AutoMapFrom(typeof(SwipeShift))]
	public class SwipeShiftListDto : FullAuditedEntityDto
	{
		//TODO: DTO SwipeShift Properties Missing
		//YOU CAN REFER ATTRIBUTES IN 
		public virtual int UserId { get; set; }
		public virtual string UserName { get; set; }
		public virtual DateTime ShiftStartTime { get; set; }
		public virtual DateTime? ShiftEndTime { get; set; }
		public virtual decimal TenderOpenAmount { get; set; }
		public virtual decimal TenderCredit { get; set; }
		public virtual decimal TenderDebit { get; set; }

        public virtual decimal TillShiftCashIn { get; set; }
        public virtual decimal TillShiftCashOut { get; set; }
        public virtual decimal TenderCloseAmount { get; set; }
		public virtual decimal ActualTenderClosing { get; set; }    //	Automatically based on input SwipePaymentTenders details
		public virtual decimal UserTenderEntered { get; set; }
		public virtual string ExcessShortageStatus { get; set; }    //	Excess, Shortage , No Difference
		public virtual decimal ExcessShortageAmount { get; set; }


		public List<SwipeShiftTenderListDto> SwipeShiftTenderList { get; set; }

		public string StrStartTime { get; set; }
		public string StrEndTime { get; set; }
	}

	[AutoMapTo(typeof(SwipeShift))]
	public class SwipeShiftEditDto
	{
		public int? Id { get; set; }
		//TODO: DTO SwipeShift Properties Missing
		public virtual int UserId { get; set; }
		public virtual DateTime ShiftStartTime { get; set; }
		public virtual DateTime? ShiftEndTime { get; set; }
		public virtual decimal TenderOpenAmount { get; set; }
		public virtual decimal TenderCredit { get; set; }
		public virtual decimal TenderDebit { get; set; }

        public virtual decimal TillShiftCashIn { get; set; }
        public virtual decimal TillShiftCashOut { get; set; }

        public virtual decimal ActualTenderClosing { get; set; }    //	Automatically based on input SwipePaymentTenders details
		public virtual decimal UserTenderEntered { get; set; }
		public virtual string ExcessShortageStatus { get; set; }    //	Excess, Shortage , No Difference
		public virtual decimal ExcessShortageAmount { get; set; }
		public virtual decimal TenderCloseAmount { get; set; }
		
		public List<SwipeShiftTenderListDto> SwipeShiftTenderList { get; set; }

		public List<SwipePaymentTypeEditDto> OpenPaymentList { get; set; }

        public List<SwipePaymentTypeEditDto> CashInPaymentList { get; set; }
        public List<SwipePaymentTypeEditDto> CashOutPaymentList { get; set; }

		public List<SwipePaymentTypeEditDto> ClosePaymentList { get; set; }
		public virtual int ShiftInOutStatus { get; set; }
	}


	[AutoMapFrom(typeof(SwipeShiftTender))]
	public class SwipeShiftTenderListDto : FullAuditedEntityDto
	{
		public virtual int ShiftRefId { get; set; }

		public virtual int ShiftInOutStatus { get; set; }   //	0 For Shift In , 1 For Shift Close Out

		public virtual int PaymentTypeId { get; set; }

		public virtual decimal ActualAmount { get; set; }   //	Only Shown for Shift Close Out

		public virtual decimal TenderedAmount { get; set; }

	}

	public class GetSwipeShiftInput : PagedAndSortedInputDto, IShouldNormalize
	{
		public string Filter { get; set; }

		public string Operation { get; set; }

		public string UserName { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public List<ComboboxItemDto> ShiftExcessShortageStatusList { get; set; }

		public void Normalize()
		{
			if (string.IsNullOrEmpty(Sorting))
			{
				Sorting = "ShiftStartTime";
			}
		}
	}

	public class GetSwipeShiftDetailInput : PagedAndSortedInputDto, IShouldNormalize
	{
		public string Filter { get; set; }

		public string UserName { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public List<ComboboxItemDto> ShiftExcessShortageStatusList { get; set; }
		public List<ComboboxItemDto> TransactionList { get; set; }
		public List<SwipePaymentTypeListDto> PaymentTypeList { get; set; }

		public void Normalize()
		{
			if (string.IsNullOrEmpty(Sorting))
			{
				Sorting = "Id";
			}
		}
	}

	public class GetSwipeShiftForEditOutput : IOutputDto
	{
		public SwipeShiftEditDto SwipeShift { get; set; }
	}
	public class CreateOrUpdateSwipeShiftInput : IInputDto
	{
		[Required]
		public SwipeShiftEditDto SwipeShift { get; set; }

	}

	public class UserIdInput : IInputDto
	{
		public int UserId { get; set; }
	}
}

