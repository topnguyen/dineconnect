﻿
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using DinePlan.DineConnect.Engage;

namespace DinePlan.DineConnect.Swipe.Master.Dtos
{
	[AutoMapFrom(typeof(ConnectMember))]
	public class SwipeMemberListDto : FullAuditedEntityDto
	{
		//TODO: DTO SwipeMember Properties Missing
		//YOU CAN REFER ATTRIBUTES IN 
		[MaxLength(30)]
		public virtual string MemberCode { get; set; }
		public virtual string Name { get; set; }
		public virtual string EmailId { get; set; }
		public virtual string AlternateEmailId { get; set; }
		public virtual string PhoneNumber { get; set; }
		public virtual string AlternatePhoneNumber { get; set; }
		public virtual string AddressLine1 { get; set; }
		public virtual string Locality { get; set; }
		public virtual string City { get; set; }
		public virtual string State { get; set; }
		public virtual string Country { get; set; }
		public virtual string PostalCode { get; set; }
		public virtual int? DefaultAddressRefId { get; set; }
		public virtual bool IsActive { get; set; }
	}
	[AutoMapTo(typeof(ConnectMember))]
	public class SwipeMemberEditDto
	{
		public int? Id { get; set; }
		//TODO: DTO SwipeMember Properties Missing
		[MaxLength(30)]
		public virtual string MemberCode { get; set; }
		public virtual string Name { get; set; }
		public virtual string EmailId { get; set; }
		public virtual string AlternateEmailId { get; set; }
		public virtual string PhoneNumber { get; set; }
		public virtual string AddressLine1 { get; set; }
		public virtual string Locality { get; set; }
		public virtual string City { get; set; }
		public virtual string State { get; set; }
		public virtual string Country { get; set; }
		public virtual string PostalCode { get; set; }
		public virtual bool IsActive { get; set; }
		public virtual int? DefaultAddressRefId { get; set; }
	}

	public class GetSwipeMemberInput : PagedAndSortedInputDto, IShouldNormalize
	{
		public string Filter { get; set; }

		public string Operation { get; set; }

		public string MemberCode { get; set; }
		public string PhoneNumber { get; set; }
		public string EmailId { get; set; }

		public bool? IsActive { get; set; }
		public void Normalize()
		{
			if (string.IsNullOrEmpty(Sorting))
			{
				Sorting = "Id";
			}
		}
	}
	public class GetSwipeMemberForEditOutput : IOutputDto
	{
		public SwipeMemberEditDto SwipeMember { get; set; }
	}
	public class CreateOrUpdateSwipeMemberInput : IInputDto
	{
		[Required]
		public SwipeMemberEditDto SwipeMember { get; set; }
	}

}

