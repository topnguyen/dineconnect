﻿

using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;

namespace DinePlan.DineConnect.Swipe.Master.Dtos
{
	[AutoMapFrom(typeof(SwipeCard))]
	public class SwipeCardListDto : FullAuditedEntityDto
	{
		public virtual int CardTypeRefId { get; set; }
		public virtual string CardNumber { get; set; }
		public virtual decimal Balance { get; set; }
		public virtual bool ActiveStatus { get; set; }
		public virtual bool DepositRequired { get; set; }

		public virtual decimal DepositAmount { get; set; }

		public virtual int? ExpiryDaysFromIssue { get; set; }

		public virtual bool RefundAllowed { get; set; }
		
		public virtual bool CanIssueWithOutMember { get; set; }
		public virtual bool CanExcludeTaxAndCharges { get; set; }
		public virtual bool IsEmployeeCard { get; set; }
		public virtual bool IsAutoRechargeCard { get; set; }
		public virtual decimal DailyLimit { get; set; }
		public virtual decimal WeeklyLimit { get; set; }
		public virtual decimal MonthlyLimit { get; set; }
		public virtual decimal YearlyLimit { get; set; }
        public virtual decimal MinimumTopUpAmount { get; set; }
        public virtual string LimitType { get; set; }
		public virtual decimal LimitAmount { get; set; }
        

    }

	[AutoMapTo(typeof(SwipeCard))]
    public class SwipeCardEditDto
    {
        public int? Id { get; set; }
		//TODO: DTO SwipeCard Properties Missing
		public virtual int CardTypeRefId { get; set; }
		public virtual string CardNumber { get; set; }
        public virtual decimal Balance { get; set; }
        public virtual bool ActiveStatus { get; set; }
        public virtual bool DepositRequired { get; set; }
        public virtual decimal DepositAmount { get; set; }
        public virtual int? ExpiryDaysFromIssue { get; set; }
        public virtual bool RefundAllowed { get; set; }
		public virtual bool CanIssueWithOutMember { get; set; }
		public virtual bool CanExcludeTaxAndCharges { get; set; }
		public virtual bool IsEmployeeCard { get; set; }
		public virtual bool IsAutoRechargeCard { get; set; }
		public virtual decimal DailyLimit { get; set; }
		public virtual decimal WeeklyLimit { get; set; }
		public virtual decimal MonthlyLimit { get; set; }
		public virtual decimal YearlyLimit { get; set; }
        public virtual decimal MinimumTopUpAmount { get; set; }
    }

    public class GetSwipeCardInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class GetSwipeCardForEditOutput : IOutputDto
    {
        public SwipeCardEditDto SwipeCard { get; set; }
    }
    public class CreateOrUpdateSwipeCardInput : IInputDto
    {
        [Required]
        public SwipeCardEditDto SwipeCard { get; set; }
		public bool AutoCreateFlag { get; set; }
        public bool Import { get; set; }

    }
}

