﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Swipe.Master.Dtos
{
    [AutoMapFrom(typeof(SwipePaymentType))]
    public class SwipePaymentTypeListDto : FullAuditedEntityDto
    {
        public virtual string Name { get; set; }
        public virtual bool AcceptChange { get; set; }
        public virtual bool Hide { get; set; }
        public virtual string AccountCode { get; set; }
        public virtual int SortOrder { get; set; }
        public bool TopupAcceptFlag { get; set; }
        public bool RefundAcceptFlag { get; set; }
    }
    [AutoMapTo(typeof(SwipePaymentType))]
    public class SwipePaymentTypeEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO SwipePaymentType Properties Missing
        public virtual string Name { get; set; }
        public virtual bool AcceptChange { get; set; }
        public virtual bool Hide { get; set; }
        public virtual string AccountCode { get; set; }
        public virtual int SortOrder { get; set; }
        public bool TopupAcceptFlag { get; set; }
        public bool RefundAcceptFlag { get; set; }

        public decimal OpenBalance { get; set; }
        public decimal TillShiftCashIn { get; set; }
        public decimal TillShiftCashOut { get; set; }
        public decimal Credit { get; set; }
        public decimal Debit { get; set; }
        public decimal ActualAmount { get; set; }
		public decimal TenderedAmount { get; set; }
    }

    public class GetSwipePaymentTypeInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "SortOrder";
            }
        }
    }
    public class GetSwipePaymentTypeForEditOutput : IOutputDto
    {
        public SwipePaymentTypeEditDto SwipePaymentType { get; set; }
    }
    public class CreateOrUpdateSwipePaymentTypeInput : IInputDto
    {
        [Required]
        public SwipePaymentTypeEditDto SwipePaymentType { get; set; }
    }

    [AutoMapFrom(typeof(SwipePaymentType))]
    public class ApiSwipePayment 
    {
        public virtual string Name { get; set; }
        public virtual bool AcceptChange { get; set; }
        public virtual bool Hide { get; set; }
        public virtual string AccountCode { get; set; }
        public virtual int SortOrder { get; set; }
        public bool TopupAcceptFlag { get; set; }
        public bool RefundAcceptFlag { get; set; }
    }
}

