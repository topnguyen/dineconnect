﻿
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Swipe.Master.Dtos;
using DinePlan.DineConnect.Engage;
using DinePlan.DineConnect.Hr.Report.Dto;

namespace DinePlan.DineConnect.Swipe.Transaction.Dtos
{
	[AutoMapFrom(typeof(MemberCard))]
	public class MemberCardListDto : FullAuditedEntityDto
	{
		public virtual int? MemberRefId { get; set; }
		public virtual string MemberCode { get; set; }
		public virtual string MemberPhone { get; set; }
		public virtual string MemberEmail { get; set; }
		public virtual int CardTypeRefId { get; set; }
		public virtual int CardRefId { get; set; }
		public virtual string CardNumber { get; set; }
		public virtual bool IsPrimaryCard { get; set; }
		public virtual int PrimaryCardRefId { get; set; }
		public virtual string PrimaryCardNumer { get; set; }
		public virtual string PrimaryCardMemberCode { get; set; }
		public virtual DateTime IssueDate { get; set; }
		public virtual DateTime? ReturnDate { get; set; }
		public virtual DateTime? ExpiryDate { get; set; }
		public virtual decimal DepositAmount { get; set; }

		public virtual bool IsActive { get; set; }
		public List<SwipePaymentTenderListDto> SwipePaymentTenderList { get; set; }
		public virtual bool CanIssueWithOutMember { get; set; }

		public virtual int? EmployeeRefId { get; set; }
		public virtual string EmployeeRefCode { get; set; }
		public virtual string EmployeeRefName { get; set; }
		public virtual decimal Balance { get; set; }
		
		public virtual decimal DepositBalance { get; set; }
	}

	[AutoMapTo(typeof(MemberCard))]
	public class MemberCardEditDto
	{
		public int? Id { get; set; }

		public virtual int? MemberRefId { get; set; }
		public virtual string MemberCode { get; set; }

		public virtual string PhoneNumber { get; set; }
		public virtual string Email { get; set; }

		public virtual int CardRefId { get; set; }
		public virtual string CardNumber { get; set; }
		public virtual bool IsPrimaryCard { get; set; }

		public virtual int PrimaryCardRefId { get; set; }
		public virtual string PrimaryCardNumber { get; set; }

		public virtual DateTime IssueDate { get; set; }
		public virtual DateTime? ReturnDate { get; set; }
		public virtual DateTime? ExpiryDate { get; set; }
		public virtual decimal DepositAmount { get; set; }

		public virtual bool IsActive { get; set; }

		public List<SwipePaymentTenderListDto> SwipePaymentTenderList { get; set; }

		public virtual int? ShiftRefId { get; set; }

		public virtual bool CanIssueWithOutMember { get; set; }
		public virtual bool IsEmployeeCard { get; set; }
		public virtual int? EmployeeRefId { get; set; }

	}

	public class GetMemberCardInput : PagedAndSortedInputDto, IShouldNormalize
	{
		public string Filter { get; set; }
		public int? MemberRefId { get; set; }
		public string MemberCode { get; set; }
		public string PhoneNumber { get; set; }
		public string EmailId { get; set; }
		public string CardNumber { get; set; }

		public bool? ActiveMemberCardsFlag { get; set; }

		public bool TransactionDetailNotNeeded { get; set; }

		public int? CardRefId { get; set; }

		public bool? IsActive { get; set; }

		public bool BalanceNeeded { get; set; }

		public int? EmployeeRefId { get; set; }
		public DateTime? StartDate { get; set; }
		public DateTime? EndDate { get; set; }
		public int LastNRecord { get; set; }
		public void Normalize()
		{
			if (string.IsNullOrEmpty(Sorting))
			{
				Sorting = "Id";
			}
		}
	}

	public class SendCardNumberDto  : PagedAndSortedInputDto, IShouldNormalize
	{
		public int? CardRefId { get; set; }
		public string CardNumber { get; set; }
		public void Normalize()
		{
			if (string.IsNullOrEmpty(Sorting))
			{
				Sorting = "Id";
			}
		}
	}
	public class GetMemberCardForEditOutput : IOutputDto
	{
		public MemberCardEditDto MemberCard { get; set; }
	}

	public class ApiTopupCardInputDto : IInputDto
	{
		public int CardRefId { get; set; }
		public decimal TopUpAmount { get; set; }
		public List<SwipePaymentTenderListDto> SwipePaymentTenderList { get; set; }

	}

	public class ApiTopUpCardReturnDto : IOutputDto
	{
		public decimal Balance { get; set; }
		public string ErrorMessage { get; set; }
	}

    public class BatchMemberCardTransactionDto : IInputDto
    {
        public List<CreateOrUpdateMemberCardInput> CardList { get; set; }
        public decimal BatchTopUpAmount { get; set; }
        public decimal BatchRefundAmount { get; set; }
        public bool BatchSupplementaryCardFlag { get; set; }
        public decimal BatchDepositCollectedAmount { get; set; }
        public decimal BatchDepositRefundedAmount { get; set; }
        public List<SwipePaymentTenderListDto> BatchPaymentTenderList { get; set; }


    }

    public class CreateOrUpdateMemberCardInput : IInputDto
	{
		[Required]
		public MemberCardEditDto MemberCard { get; set; }
		public decimal TopUpAmount { get; set; }
		public decimal RefundAmount { get; set; }
		public bool supplementaryCardFlag { get; set; }
		public decimal DepositCollectedAmount { get; set; }
		public decimal DepositRefundedAmount { get; set; }
        public bool IsBatchUpdate { get; set; }
	}

	
	public class SwipeCardBalanceDto : IOutputDto
	{

		public virtual bool CanExcludeTaxAndCharges { get; set; }
		public virtual decimal Balance { get; set; }
		public virtual decimal DepositBalance { get; set; }
		public virtual string ErrorMessage { get; set; }
		public virtual bool IsEmployeeCard { get; set; }
		public virtual bool IsAutoRechargeCard { get; set; }
		public virtual string CardLimitPeriod { get; set; }
		public virtual decimal CardLimit { get; set; }
        public virtual bool IsFirstTimeCard { get; set; }
	}


	[AutoMapFrom(typeof(ConnectMember))]
	public class MemberViewDto : IOutputDto
	{
		public virtual int? Id { get; set; }
		public virtual int? MemberRefId { get; set; }
		public virtual string MemberCode { get; set; }
		public virtual int? EmployeeRefId { get; set; }
		public virtual string EmployeeRefName { get; set; }

		public virtual string Name { get; set; }
		public virtual string EmailId { get; set; }
		public virtual string AlternateEmailId { get; set; }
		public virtual string PhoneNumber { get; set; }
		public virtual string AlternatePhoneNumber { get; set; }
		public virtual string AddressLine1 { get; set; }
		public virtual string AddressLine2 { get; set; }
		public virtual string Locality { get; set; }
		public virtual string City { get; set; }
		public virtual string State { get; set; }
		public virtual string Country { get; set; }
		public virtual string PostalCode { get; set; }
		public virtual bool IsActive { get; set; }
		public virtual int? DefaultAddressRefId { get; set; }

		public virtual int? CardRefId { get; set; }
		public virtual string CardNumber { get; set; }
		public virtual decimal Balance { get; set; }
		public List<MemberCardDetailDto> MemberCards { get; set; }
		public List<SwipeCardLedgerListDto> CardTransactionDetails { get; set; }
		public virtual decimal DepositBalance { get; set; }
	}

	[AutoMapFrom(typeof(MemberCard))]
	public class MemberCardDetailDto : FullAuditedEntityDto
	{
		public virtual int? MemberRefId { get; set; }
		public virtual int CardRefId { get; set; }
		public virtual string CardNumber { get; set; }
		public virtual bool IsPrimaryCard { get; set; }
		public virtual int PrimaryCardRefId { get; set; }
		public virtual string PrimaryCardNumer { get; set; }
		public virtual DateTime IssueDate { get; set; }
		public virtual DateTime? ReturnDate { get; set; }
		public virtual DateTime? ExpiryDate { get; set; }
		public virtual bool IsActive { get; set; }
		public virtual int? EmployeeRefId { get; set; }
		public virtual string EmployeeRefName { get; set; }

	}

	[AutoMapFrom(typeof(SwipeCardLedger))]
	public class SwipeCardLedgerListDto : FullAuditedEntityDto
	{
		public virtual string UserName { get; set; }
		public virtual DateTime LedgerTime { get; set; }

		public virtual string LedgerTimeString { get; set; }

		public virtual int? MemberRefId { get; set; }
		public virtual string MemberRefName { get; set; }
		public virtual int? EmployeeRefId { get; set; }
		public virtual string EmployeeRefName { get; set; }
		public virtual int CardTypeRefId { get; set; }
		public virtual string CardType { get; set; }

		public virtual int CardRefId { get; set; }
		public virtual string CardNumber { get; set; }
		public virtual int PrimaryCardRefId { get; set; }
		public virtual string PrimaryCardNumber { get; set; }
		public virtual int TransactionType { get; set; }
		public virtual string TransactionTypeName { get; set; }
		public virtual string Description { get; set; }
		public virtual decimal OpeningBalance { get; set; }
		public virtual decimal Credit { get; set; }
		public virtual decimal Debit { get; set; }
		public virtual decimal ClosingBalance { get; set; }
		public virtual int? LocationId { get; set; }
		public virtual string TicketNumber { get; set; }
		public virtual int? ShiftRefId { get; set; }

		public List<SwipePaymentTenderListDto> PaymentDetailList { get; set; }

		public SwipeMemberListDto SwipeMember { get; set; }

		public EmployeeInfoListDto Employee { get; set; }

	
	}

	[AutoMapFrom(typeof(SwipeCardLedger))]
	public class ApiLedgerInput : FullAuditedEntityDto
	{
		public virtual DateTime LedgerTime { get; set; }
		public virtual string CardNumber { get; set; }
		public virtual int TransactionType { get; set; }
		public virtual string Description { get; set; }
		public virtual decimal AmountDebitFromSwipeCard { get; set; }
		public virtual int? LocationId { get; set; }
		public virtual string TicketNumber { get; set; }
		public int TenantId { get; set; }
	}

	[AutoMapFrom(typeof(SwipePaymentTender))]
	public class SwipePaymentTenderListDto : CreationAuditedEntity
	{
		public virtual int SwipeCardLedgerRefId { get; set; }
		public virtual int PaymentTypeId { get; set; }
		public virtual string PaymentTypeName { get; set; }
		public virtual decimal TenderedAmount { get; set; }
		public virtual decimal Amount { get; set; }
		public virtual string PaymentUserName { get; set; }
        public virtual decimal PendingAmount { get; set; }
	}



	public class SwipeCardDepositDto : IOutputDto
	{
		public virtual decimal TotalDeposit { get; set; }
		public virtual string ErrorMessage { get; set; }
	}

	public enum SwipeTransaction
	{
		TOPUP,					//	
		REFUND,
		REISSUE_CARD,
		CLOSE_CARD,
		REDEEM,
		DEPOSIT,
		DEPOSIT_REFUND,
	}


	public class GetMemberCardDetails 
	{
		public int? MemberRefId { get; set; }

		public int? EmployeeRefId { get; set; }
	}

	[AutoMapFrom(typeof(ConnectMember))]
	public class MemberTransactionListDto : IOutputDto
	{
		public virtual int? Id { get; set; }
		public virtual int? MemberRefId { get; set; }
		public virtual string MemberCode { get; set; }
		public virtual int? EmployeeRefId { get; set; }
		public virtual string EmployeeRefName { get; set; }
		public virtual string Name { get; set; }
		public virtual string EmailId { get; set; }
		public virtual string AlternateEmailId { get; set; }
		public virtual string PhoneNumber { get; set; }
		public virtual string AlternatePhoneNumber { get; set; }
		public virtual string AddressLine1 { get; set; }
		public virtual string AddressLine2 { get; set; }
		public virtual string Locality { get; set; }
		public virtual string City { get; set; }
		public virtual string State { get; set; }
		public virtual string Country { get; set; }
		public virtual string PostalCode { get; set; }
		public virtual bool IsActive { get; set; }
		public virtual int? DefaultAddressRefId { get; set; }

		public int NoOfPrimaryCards { get; set; }

		public List<CardInformationDto> CardDetails { get; set; }
	}

	public class CardInformationDto
	{
		public virtual int? CardRefId { get; set; }
		public virtual string CardNumber { get; set; }
		public virtual decimal Balance { get; set; }
		public virtual decimal DepositBalance { get; set; }

		public List<MemberCardDetailDto> MemberCards { get; set; }
		public List<SwipeCardLedgerListDto> CardTransactionDetails { get; set; }
		
	}


}

