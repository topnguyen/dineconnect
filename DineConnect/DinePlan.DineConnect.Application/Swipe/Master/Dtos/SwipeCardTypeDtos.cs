﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Ticket.CategoryReport.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Swipe.Master.Dtos
{
    [AutoMapFrom(typeof(SwipeCardType))]
    public class SwipeCardTypeListDto : FullAuditedEntityDto
    {
        public virtual string Name { get; set; }
        public virtual bool DepositRequired { get; set; }
        public virtual decimal DepositAmount { get; set; }
        public virtual int? ExpiryDaysFromIssue { get; set; }
        public virtual bool RefundAllowed { get; set; }
        public virtual bool CanIssueWithOutMember { get; set; }
        public virtual bool CanExcludeTaxAndCharges { get; set; }
        public virtual bool IsEmployeeCard { get; set; }
        public virtual bool IsAutoRechargeCard { get; set; }
        public virtual decimal DailyLimit { get; set; }
        public virtual decimal WeeklyLimit { get; set; }
        public virtual decimal MonthlyLimit { get; set; }
        public virtual decimal YearlyLimit { get; set; }
        public virtual string AutoActivateRegExpression { get; set; }
        public virtual decimal MinimumTopUpAmount { get; set; }
    }

    [AutoMapTo(typeof(SwipeCardType))]
    public class SwipeCardTypeEditDto
    {
        public int? Id { get; set; }

        //TODO: DTO SwipeCardType Properties Missing
        public virtual string Name { get; set; }

        public virtual bool DepositRequired { get; set; }
        public virtual decimal DepositAmount { get; set; }
        public virtual int? ExpiryDaysFromIssue { get; set; }
        public virtual bool RefundAllowed { get; set; }
        public virtual bool CanIssueWithOutMember { get; set; }
        public virtual bool CanExcludeTaxAndCharges { get; set; }
        public virtual bool IsEmployeeCard { get; set; }
        public virtual bool IsAutoRechargeCard { get; set; }
        public virtual decimal DailyLimit { get; set; }
        public virtual decimal WeeklyLimit { get; set; }
        public virtual decimal MonthlyLimit { get; set; }
        public virtual decimal YearlyLimit { get; set; }
        public virtual string AutoActivateRegExpression { get; set; }
        public virtual decimal MinimumTopUpAmount { get; set; }
    }

    public class GetSwipeCardTypeInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    public class GetSwipeCardTypeForEditOutput : IOutputDto
    {
        public SwipeCardTypeEditDto SwipeCardType { get; set; }
        public List<SwipeCardListDto> CardList { get; set; }
    }

    public class CreateOrUpdateSwipeCardTypeInput : IInputDto
    {
        [Required]
        public SwipeCardTypeEditDto SwipeCardType { get; set; }
    }

    public class GenerateCardsInputDto : IInputDto
    {
        public virtual int CardTypeRefId { get; set; }
        public virtual string Prefix { get; set; }
        public virtual string Suffix { get; set; }
        public virtual int CardNumberOfDigits { get; set; }
        public virtual int CardStartingNumber { get; set; }
        public virtual int CardCount { get; set; }
    }

    public class SwipeCardDashBoardReport : IOutputDto
    {
        public int NumberOfCardsGenerated { get; set; }
        public int NumberOfCardsActivated { get; set; }
        public int NumberOfCardsDorment { get; set; }
        public decimal TotalAmountBalance { get; set; }
        public decimal TotalAmountConfiscated { get; set; }
        public decimal RefundableDepositCollected { get; set; }
        public decimal NonRefundableDepositCollected { get; set; }
        public List<SwipeCardTypeReport> SwipeCardTypeList { get; set; }
    }

    public class SwipeCardTypeReport : IOutputDto
    {
        public int CardTypeRefId { get; set; }
        public SwipeCardTypeListDto CardDetails { get; set; }
        public int NumberOfCardsGenerated { get; set; }
        public int NumberOfCardsActivated { get; set; }
        public int NumberOfCardsDorment { get; set; }
        public decimal TotalAmountBalance { get; set; }
        public decimal TotalAmountConfiscated { get; set; }
        public decimal RefundableDepositCollected { get; set; }
        public decimal NonRefundableDepositCollected { get; set; }
    }

    public class GetSwipeChartInput : IInputDto
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

    public class SwipeTransactionStatsDto
    {
        public PagedResultOutput<CategoryReportDto> CategoryList { get; set; }
        public List<string> Transactions { get; set; }
        public List<BarChartOutputDto> ChartOutput { get; set; }
    }
}