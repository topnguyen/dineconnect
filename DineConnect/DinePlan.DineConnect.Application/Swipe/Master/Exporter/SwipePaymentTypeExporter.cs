﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Swipe.Transaction.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Swipe.Master.Dtos;

namespace DinePlan.DineConnect.Swipe.Transaction.Exporter
{
    public class SwipePaymentTypeListExcelExporter : FileExporterBase, ISwipePaymentTypeListExcelExporter
    {
        public FileDto ExportToFile(List<SwipePaymentTypeListDto> dtos)
        {
            return CreateExcelPackage(
                "SwipePaymentTypeList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("SwipePaymentType"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}

