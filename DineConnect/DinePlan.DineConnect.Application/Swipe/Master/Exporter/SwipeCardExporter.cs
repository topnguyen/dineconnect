﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Swipe.Master.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Swipe.Master.Exporter
{
    public class SwipeCardListExcelExporter : FileExporterBase, ISwipeCardListExcelExporter
    {
        public FileDto ExportToFile(List<SwipeCardListDto> dtos)
        {
            return CreateExcelPackage(
                "SwipeCardList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("SwipeCard"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}

