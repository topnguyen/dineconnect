﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Swipe.Master.Dtos;
using DinePlan.DineConnect.Swipe.Transaction.Dtos;

namespace DinePlan.DineConnect.Swipe.Master
{
	public interface IMemberCardAppService : IApplicationService
	{
		Task<List<IdInput>> AttachEmployeeCard(CreateOrUpdateMemberCardInput input);
		Task<PagedResultOutput<MemberCardListDto>> GetAll(GetMemberCardInput inputDto);
		Task<FileDto> GetAllToExcel();
		Task<GetMemberCardForEditOutput> GetMemberCardForEdit(NullableIdInput nullableIdInput);
		Task<List<IdInput>> CreateOrUpdateMemberCard(CreateOrUpdateMemberCardInput input);

		Task DeactivateMemberCard(IdInput input);

		Task<PagedResultOutput<MemberCardListDto>> GetMemberCardListAll(GetMemberCardInput input);

		Task<ListResultOutput<SwipeCardListDto>> GetSwipeCardDetails();
		Task<PagedResultOutput<MemberViewDto>> GetMemberViewListAll(GetMemberCardInput input);
		Task<MemberTransactionListDto> GetCardTransactionList(GetMemberCardInput input);
		Task<ListResultOutput<ComboboxItemDto>> GetSwipeTransactionTypeForCombobox();

		Task<SwipeCardBalanceDto> ApiGetSwipeCardCurrentBalance(SwipeCardListDto input);
		Task<IdInput> ApiUpdateSwipeCardLedger(ApiLedgerInput input);
		Task<SwipeCardDepositDto> ApiGetSwipeCardCurrentDepositBalance(SwipeCardListDto input);
		Task<SwipeCardDashBoardReport> GetSwipeCardDashBoardReport();

		Task<SwipeTransactionStatsDto> GetCardTransactionChart(GetSwipeChartInput input);
		Task<PagedResultOutput<MemberViewDto>> GetMemberIndexView(GetMemberCardInput input);
		Task<SwipeCardLedgerListDto> GetSwipeCardLedgerDetail(IdInput input);
		Task<PagedResultOutput<MemberCardListDto>> GetAllSwipeCardsForMember(GetMemberCardDetails input);
		Task<ApiTopUpCardReturnDto> ApiTopUpCard(ApiTopupCardInputDto input);
        Task<List<IdInput>> BatchUpdateMemberCard(BatchMemberCardTransactionDto input);



    }
}

