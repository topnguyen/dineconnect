﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Swipe.Master.Dtos;
using DinePlan.DineConnect.Swipe.Transaction.Dtos;

namespace DinePlan.DineConnect.Swipe.Master
{
	public interface ISwipeShiftAppService : IApplicationService
	{
		Task<PagedResultOutput<SwipeShiftListDto>> GetAll(GetSwipeShiftInput inputDto);
		Task<FileDto> GetAllToExcel(GetSwipeShiftInput input);
		Task<GetSwipeShiftForEditOutput> GetSwipeShiftForEdit(NullableIdInput nullableIdInput);
		Task CreateOrUpdateSwipeShift(CreateOrUpdateSwipeShiftInput input);
		Task DeleteSwipeShift(IdInput input);

		Task<SwipeShiftEditDto> GetLoginStatus(UserIdInput input);
		Task<PagedResultOutput<SwipeShiftListDto>> GetShiftViewAll(GetSwipeShiftInput input);
		Task<PagedResultOutput<SwipeCardLedgerListDto>> GetShiftDetailViewAll(GetSwipeShiftDetailInput input);
		Task<FileDto> GetAllToExcelShiftDetail(GetSwipeShiftDetailInput input);
        Task<IdInput> AppendTillShiftCashInOut(CreateOrUpdateSwipeShiftInput input);


    }
}
