﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Swipe.Master.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage;

namespace DinePlan.DineConnect.Swipe.Master
{
	public interface ISwipeMemberAppService : IApplicationService
	{
		Task<PagedResultOutput<SwipeMemberListDto>> GetAll(GetSwipeMemberInput inputDto);
		Task<FileDto> GetAllToExcel();
		Task<GetSwipeMemberForEditOutput> GetSwipeMemberForEdit(NullableIdInput nullableIdInput);
		Task<SwipeMemberEditDto> CreateOrUpdateSwipeMember(CreateOrUpdateSwipeMemberInput input);
		Task DeleteSwipeMember(IdInput input);

		//Task<ListResultOutput<SwipeMemberListDto>> GetMemberCodes();
		Task<ListResultOutput<ComboboxItemDto>> GetSwipeMemberForCombobox();
		Task<SwipeMemberListDto> MemberAlreadyExists(GetSwipeMemberInput input);
		Task<PagedResultOutput<SwipeMemberListDto>> GetMemberList(GetSwipeMemberInput input);

	}
}