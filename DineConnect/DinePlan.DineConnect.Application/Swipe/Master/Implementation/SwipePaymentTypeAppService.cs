﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Swipe.Master.Dtos;
using DinePlan.DineConnect.Swipe.Transaction;

namespace DinePlan.DineConnect.Swipe.Master.Implementation
{
    public class SwipePaymentTypeAppService : DineConnectAppServiceBase, ISwipePaymentTypeAppService
    {
        private readonly ISwipePaymentTypeListExcelExporter _swipepaymenttypeExporter;
        private readonly IRepository<SwipePaymentType> _swipepaymenttypeRepo;

        public SwipePaymentTypeAppService(IRepository<SwipePaymentType> swipepaymenttypeRepo,
            ISwipePaymentTypeListExcelExporter swipepaymenttypeExporter)
        {
            _swipepaymenttypeRepo = swipepaymenttypeRepo;
            _swipepaymenttypeExporter = swipepaymenttypeExporter;
        }

        public async Task<PagedResultOutput<SwipePaymentTypeListDto>> GetAll(GetSwipePaymentTypeInput input)
        {
            var allItems = _swipepaymenttypeRepo
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Name.Contains(input.Filter)
                );
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<SwipePaymentTypeListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<SwipePaymentTypeListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _swipepaymenttypeRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<SwipePaymentTypeListDto>>();
            return _swipepaymenttypeExporter.ExportToFile(allListDtos);
        }

        public async Task<GetSwipePaymentTypeForEditOutput> GetSwipePaymentTypeForEdit(NullableIdInput input)
        {
            SwipePaymentTypeEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _swipepaymenttypeRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<SwipePaymentTypeEditDto>();
            }
            else
            {
                editDto = new SwipePaymentTypeEditDto();
            }

            return new GetSwipePaymentTypeForEditOutput
            {
                SwipePaymentType = editDto
            };
        }

        public async Task CreateOrUpdateSwipePaymentType(CreateOrUpdateSwipePaymentTypeInput input)
        {
            if (input.SwipePaymentType.Id.HasValue)
            {
                await UpdateSwipePaymentType(input);
            }
            else
            {
                await CreateSwipePaymentType(input);
            }
        }

        public async Task DeleteSwipePaymentType(IdInput input)
        {
            await _swipepaymenttypeRepo.DeleteAsync(input.Id);
        }

        public async Task<ListResultDto<ApiSwipePayment>> ApiGetAll()
        {
            var allItems = await _swipepaymenttypeRepo
                .GetAllListAsync();
            var allListDtos = allItems.MapTo<List<ApiSwipePayment>>();
            return new ListResultDto<ApiSwipePayment>(allListDtos);
        }

        protected virtual async Task UpdateSwipePaymentType(CreateOrUpdateSwipePaymentTypeInput input)
        {
            var item = await _swipepaymenttypeRepo.GetAsync(input.SwipePaymentType.Id.Value);
            var dto = input.SwipePaymentType;
            //TODO: SERVICE SwipePaymentType Update Individually
            item.AcceptChange = dto.AcceptChange;
            item.AccountCode = dto.AccountCode;
            item.RefundAcceptFlag = dto.RefundAcceptFlag;
            item.TopupAcceptFlag = dto.TopupAcceptFlag;
            item.Name = dto.Name;
            item.SortOrder = dto.SortOrder;
            item.Hide = dto.Hide;

        }

        protected virtual async Task CreateSwipePaymentType(CreateOrUpdateSwipePaymentTypeInput input)
        {
            var dto = input.SwipePaymentType.MapTo<SwipePaymentType>();
            await _swipepaymenttypeRepo.InsertAsync(dto);
        }
    }
}

