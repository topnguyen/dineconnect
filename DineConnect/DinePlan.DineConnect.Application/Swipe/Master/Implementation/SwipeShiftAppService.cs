﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Authorization.Users.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Swipe.Master.Dtos;
using DinePlan.DineConnect.Swipe.Transaction.Dtos;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Utility.Exporter;

namespace DinePlan.DineConnect.Swipe.Master.Implementation
{
    public class SwipeShiftAppService : DineConnectAppServiceBase, ISwipeShiftAppService
    {
        private readonly IExcelExporter _exporter;
        private readonly IRepository<SwipeCardLedger> _ledgerRepo;
        private readonly IRepository<SwipePaymentTender> _paymenttenderRepo;
        private readonly IRepository<SwipeShiftTender> _shifttenderRepo;
        private readonly IRepository<SwipeCard> _swipecardRepo;
        private readonly IRepository<SwipePaymentType> _swipepaymentTypeRepo;
        private readonly ISwipeShiftManager _swipeshiftManager;
        private readonly IRepository<SwipeShift> _swipeshiftRepo;
        private readonly IUserAppService _userAppService;

        public SwipeShiftAppService(ISwipeShiftManager swipeshiftManager,
            IRepository<SwipeShift> swipeShiftRepo,
            IExcelExporter exporter,
            IRepository<SwipeShiftTender> tenderRepo,
            IRepository<SwipeCardLedger> ledgerRepo,
            IRepository<SwipePaymentType> swipepaymentTypeRepo,
            IRepository<SwipePaymentTender> paymenttenderRepo,
            IRepository<SwipeCard> swipecardRepo,
            IUserAppService userAppService)
        {
            _swipeshiftManager = swipeshiftManager;
            _swipeshiftRepo = swipeShiftRepo;
            _exporter = exporter;
            _shifttenderRepo = tenderRepo;
            _ledgerRepo = ledgerRepo;
            _swipepaymentTypeRepo = swipepaymentTypeRepo;
            _paymenttenderRepo = paymenttenderRepo;
            _userAppService = userAppService;
            _swipecardRepo = swipecardRepo;
        }

        public async Task<PagedResultOutput<SwipeShiftListDto>> GetAll(GetSwipeShiftInput input)
        {
            var allItems = _swipeshiftRepo.GetAll();

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<SwipeShiftListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<SwipeShiftListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<GetSwipeShiftForEditOutput> GetSwipeShiftForEdit(NullableIdInput input)
        {
            SwipeShiftEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _swipeshiftRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<SwipeShiftEditDto>();

                var lstLedgers = await _ledgerRepo.GetAllListAsync(t => t.ShiftRefId == editDto.Id);

                editDto.TenderCredit = lstLedgers.Sum(t => t.Credit);
                editDto.TenderDebit = lstLedgers.Sum(t => t.Debit);
                editDto.ActualTenderClosing = editDto.TenderOpenAmount + editDto.TillShiftCashIn + editDto.TenderCredit - editDto.TenderDebit - editDto.TillShiftCashOut;

                var paymentTypeList = await _swipepaymentTypeRepo.GetAll().OrderBy(t => t.SortOrder).ToListAsync();
                List<SwipePaymentTypeEditDto> openPaymentList = new List<SwipePaymentTypeEditDto>();
                List<SwipePaymentTypeEditDto> cashInPaymentList = new List<SwipePaymentTypeEditDto>();
                List<SwipePaymentTypeEditDto> cashOutPaymentList = new List<SwipePaymentTypeEditDto>();
                {
                    #region OpenTenderList

                    var openTenderList =
                        await _shifttenderRepo.GetAllListAsync(t => t.ShiftInOutStatus == 0 && t.ShiftRefId == editDto.Id);

                    var existIds = openTenderList.Select(t => t.PaymentTypeId).ToArray();

                    openPaymentList =
                        paymentTypeList.Where(t => existIds.Contains(t.Id)).MapTo<List<SwipePaymentTypeEditDto>>();

                    foreach (var lst in openPaymentList)
                    {
                        var entered = openTenderList.FirstOrDefault(t => t.PaymentTypeId == lst.Id);
                        lst.TenderedAmount = entered.TenderedAmount;
                    }

                    editDto.OpenPaymentList = openPaymentList;


                    #endregion
                }

                {
                    #region CashInTenderList

                    var cashInTenderList =
                        await _shifttenderRepo.GetAllListAsync(t => t.ShiftInOutStatus == 2 && t.ShiftRefId == editDto.Id);

                    var existIds = cashInTenderList.Select(t => t.PaymentTypeId).ToArray();

                    cashInPaymentList =
                        paymentTypeList.Where(t => existIds.Contains(t.Id)).MapTo<List<SwipePaymentTypeEditDto>>();

                    foreach (var lst in cashInPaymentList)
                    {
                        var entered = cashInTenderList.Where(t => t.PaymentTypeId == lst.Id);
                        lst.TenderedAmount = entered.Sum(t => t.TenderedAmount);
                    }

                    editDto.CashInPaymentList = cashInPaymentList;


                    #endregion
                }

                {
                    #region CashOutTenderList

                    var cashOutTenderList =
                        await _shifttenderRepo.GetAllListAsync(t => t.ShiftInOutStatus == 3 && t.ShiftRefId == editDto.Id);

                    var existIds = cashOutTenderList.Select(t => t.PaymentTypeId).ToArray();

                    cashOutPaymentList =
                        paymentTypeList.Where(t => existIds.Contains(t.Id)).MapTo<List<SwipePaymentTypeEditDto>>();

                    foreach (var lst in cashOutPaymentList)
                    {
                        var entered = cashOutTenderList.Where(t => t.PaymentTypeId == lst.Id);
                        lst.TenderedAmount = entered.Sum(t => t.TenderedAmount);
                    }

                    editDto.CashInPaymentList = cashOutPaymentList;


                    #endregion
                }

                {
                    #region ClosingTenderList

                    var closePaymentList =
                        paymentTypeList.Where(t => t.TopupAcceptFlag).MapTo<List<SwipePaymentTypeEditDto>>();


                    var arrCreditLedgerids =
                        lstLedgers.Where(
                            t =>
                                t.TransactionType == (int)SwipeTransaction.TOPUP ||
                                t.TransactionType == (int)SwipeTransaction.DEPOSIT ||
                                t.TransactionType == (int)SwipeTransaction.REISSUE_CARD).Select(t => t.Id).ToArray();

                    var creditList = await _paymenttenderRepo.
                        GetAllListAsync(t => arrCreditLedgerids.Contains(t.SwipeCardLedgerRefId));

                    var arrDebitLedgerids =
                        lstLedgers.Where(
                            t =>
                                t.TransactionType == (int)SwipeTransaction.REFUND ||
                                t.TransactionType == (int)SwipeTransaction.DEPOSIT_REFUND ||
                                t.TransactionType == (int)SwipeTransaction.CLOSE_CARD).Select(t => t.Id).ToArray();

                    var debitList = await _paymenttenderRepo.
                        GetAllListAsync(t => arrDebitLedgerids.Contains(t.SwipeCardLedgerRefId));


                    foreach (var lst in closePaymentList)
                    {
                        var creditAmount = creditList.Where(t => t.PaymentTypeId == lst.Id).Sum(t => t.TenderedAmount);
                        var debitAmount = debitList.Where(t => t.PaymentTypeId == lst.Id).Sum(t => t.TenderedAmount);

                        var lstopenTender = openPaymentList.FirstOrDefault(t => t.Id == lst.Id);
                        var openTender = lstopenTender == null ? 0 : lstopenTender.TenderedAmount;

                        var lstCashInTender = cashInPaymentList.Where(t => t.Id == lst.Id);
                        var cashInTender = lstCashInTender == null || lstCashInTender.Count() == 0 ? 0 : lstCashInTender.Sum(t => t.TenderedAmount);

                        var lstCashOutTender = cashOutPaymentList.Where(t => t.Id == lst.Id);
                        var cashOutTender = lstCashOutTender == null || lstCashOutTender.Count() == 0 ? 0 : lstCashOutTender.Sum(t => t.TenderedAmount);

                        lst.OpenBalance = openTender;
                        lst.TillShiftCashIn = cashInTender;
                        lst.TillShiftCashOut = cashOutTender;
                        lst.Credit = creditAmount;
                        lst.Debit = debitAmount;

                        lst.ActualAmount = openTender + cashInTender - cashOutTender + creditAmount - debitAmount;
                    }

                    editDto.ClosePaymentList = closePaymentList;

                    #endregion
                }
            }
            else
            {
                editDto = new SwipeShiftEditDto();
            }

            return new GetSwipeShiftForEditOutput
            {
                SwipeShift = editDto
            };
        }

        public async Task CreateOrUpdateSwipeShift(CreateOrUpdateSwipeShiftInput input)
        {
            var output = await ExistAllServerLevelBusinessRules(input);

            if (output == false)
                return;

            if (input.SwipeShift.Id.HasValue)
            {
                await UpdateSwipeShift(input);
            }
            else
            {
                await CreateSwipeShift(input);
            }
        }

        public async Task DeleteSwipeShift(IdInput input)
        {
            await _swipeshiftRepo.DeleteAsync(input.Id);
        }

        public async Task<SwipeShiftEditDto> GetLoginStatus(UserIdInput input)
        {
            var shiftExist =
                await _swipeshiftRepo.FirstOrDefaultAsync(t => t.UserId == input.UserId && t.ShiftEndTime == null);
            if (shiftExist != null)
            {
                var returnDto = shiftExist.MapTo<SwipeShiftEditDto>();
                returnDto.ShiftInOutStatus = 1;
                return returnDto;
            }
            return new SwipeShiftEditDto
            {
                UserId = input.UserId,
                ShiftInOutStatus = 0
            };
        }

        public async Task<FileDto> GetAllToExcel(GetSwipeShiftInput input)
        {
            input.MaxResultCount = AppConsts.MaxPageSize;
            var result = await GetShiftViewAll(input);
            var allListDtos = result.Items.MapTo<List<SwipeShiftListDto>>();
            foreach (var lst in allListDtos)
            {
                lst.StrStartTime = lst.ShiftStartTime.ToString("dd-MMM-yyyy HH:mm");
                lst.StrEndTime = lst.ShiftEndTime == null ? "" : lst.ShiftEndTime.Value.ToString("dd-MMM-yyyy HH:mm");
            }

            var baseE = new BaseExportObject
            {
                ExportObject = allListDtos,
                ColumnNames =
                    new[]
                    {
                        "UserName", "StrStartTime", "StrEndTime", "TenderOpenAmount", "TenderCredit", "TenderDebit",
                        "TenderCloseAmount", "ActualTenderClosing", "ExcessShortageStatus", "ExcessShortageAmount"
                    },
                ColumnDisplayNames =
                    new[]
                    {
                        L("UserName"), L("StartTime"), L("EndTime"), L("TenderOpenAmount"), L("Credit"), L("Debit"),
                        L("TenderCloseAmount"), L("Actual"), L("ExcessShortageStatus"), L("ExcessShortageAmount")
                    },
                BreakOnLineColumn = "UserName"
            };
            return _exporter.ExportToFile(baseE, L("SwipeShift"));
        }

        public async Task<PagedResultOutput<SwipeShiftListDto>> GetShiftViewAll(GetSwipeShiftInput input)
        {
            var allItems =
                _swipeshiftRepo.GetAll()
                    .Where(
                        t => /*t.ShiftEndTime!=null && */
                            DbFunctions.TruncateTime(t.ShiftStartTime) >= DbFunctions.TruncateTime(input.StartDate) &&
                            DbFunctions.TruncateTime(t.ShiftStartTime) <= DbFunctions.TruncateTime(input.EndDate));

            if (input.ShiftExcessShortageStatusList.Count > 0)
            {
                var arrTransaction = input.ShiftExcessShortageStatusList.Select(t => t.DisplayText.ToUpper()).ToArray();

                allItems = allItems.Where(t => arrTransaction.Contains(t.ExcessShortageStatus.ToUpper()));
            }


            var users = await _userAppService.GetUsers(new
                GetUsersInput
            {
                MaxResultCount = 1000
            });

            var userList = users.Items;

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<SwipeShiftListDto>>();
            foreach (var lst in allListDtos)
            {
                var cu = userList.FirstOrDefault(t => t.Id == lst.UserId);
                if (cu != null)
                    lst.UserName = cu.Name.ToUpper();
            }

            if (!input.UserName.IsNullOrEmpty())
            {
                allListDtos = allListDtos.Where(t => input.UserName.ToUpper().Contains(t.UserName.ToUpper())).ToList();
            }
            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<SwipeShiftListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<PagedResultOutput<SwipeCardLedgerListDto>> GetShiftDetailViewAll(
            GetSwipeShiftDetailInput input)
        {
            #region MasterSearch

            var allItems = _swipeshiftRepo.GetAll().Where(t => DbFunctions.TruncateTime(t.ShiftStartTime)
                                                               >= DbFunctions.TruncateTime(input.StartDate) &&
                                                               DbFunctions.TruncateTime(t.ShiftStartTime)
                                                               <= DbFunctions.TruncateTime(input.EndDate));

            if (input.ShiftExcessShortageStatusList.Count > 0)
            {
                var arrTransaction = input.ShiftExcessShortageStatusList.Select(t => t.DisplayText.ToUpper()).ToArray();

                allItems = allItems.Where(t => arrTransaction.Contains(t.ExcessShortageStatus.ToUpper()));
            }


            var users = await _userAppService.GetUsers(new
                GetUsersInput
            {
                MaxResultCount = 1000
            });

            var userList = users.Items;

            var sortMenuItems = await allItems.ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<SwipeShiftListDto>>();
            foreach (var lst in allListDtos)
            {
                var cu = userList.FirstOrDefault(t => t.Id == lst.UserId);
                if (cu != null)
                    lst.UserName = cu.Name.ToUpper();
            }

            if (!input.UserName.IsNullOrEmpty())
            {
                allListDtos = allListDtos.Where(t => input.UserName.ToUpper().Contains(t.UserName.ToUpper())).ToList();
            }

            #endregion

            var shiftRefIds = allListDtos.Select(t => t.Id).ToArray();

            var shiftDetail = _ledgerRepo.GetAll().Where(t => DbFunctions.TruncateTime(t.LedgerTime)
                                                              >= DbFunctions.TruncateTime(input.StartDate) &&
                                                              DbFunctions.TruncateTime(t.LedgerTime)
                                                              <= DbFunctions.TruncateTime(input.EndDate));

            if (input.TransactionList.Count > 0)
            {
                var transactionTypeRefIds = input.TransactionList.Select(t => int.Parse(t.Value)).ToArray();
                shiftDetail = shiftDetail.Where(t => transactionTypeRefIds.Contains(t.TransactionType));
            }

            if (input.PaymentTypeList.Count > 0)
            {
                var paymentTypeRefIds = input.PaymentTypeList.Select(t => t.Id).ToArray();

                var filterShiftRefIds = shiftDetail.Select(t => t.Id).ToArray();

                var rsTenderPayment =
                    _paymenttenderRepo.GetAll()
                        .Where(
                            t =>
                                paymentTypeRefIds.Contains(t.PaymentTypeId) &&
                                filterShiftRefIds.Contains(t.SwipeCardLedgerRefId));

                var filterSwipeCardLedgerRefIds = rsTenderPayment.Select(t => t.SwipeCardLedgerRefId).ToArray();

                shiftDetail = shiftDetail.Where(t => filterSwipeCardLedgerRefIds.Contains(t.Id));
                //shiftDetail = shiftDetail.Where(t => paymentTypeRefIds.Contains(t.TransactionType));
            }

            var sortshiftDetail = await (from a in shiftDetail
                                         join c in _swipecardRepo.GetAll() on a.CardRefId equals c.Id
                                         select new SwipeCardLedgerListDto
                                         {
                                             Id = a.Id,
                                             LedgerTime = a.LedgerTime,
                                             MemberRefId = a.MemberRefId,
                                             EmployeeRefId = a.EmployeeRefId,
                                             CardRefId = a.CardRefId,
                                             CardNumber = c.CardNumber,
                                             TransactionType = a.TransactionType,
                                             Description = a.Description,
                                             OpeningBalance = a.OpeningBalance,
                                             Credit = a.Credit,
                                             Debit = a.Debit,
                                             ClosingBalance = a.ClosingBalance,
                                             ShiftRefId = a.ShiftRefId,
                                             CardTypeRefId = a.SwipeCards.CardTypeRefId,
                                             TicketNumber = a.TicketNumber,
                                             LocationId = a.LocationId
                                         })
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            return new PagedResultOutput<SwipeCardLedgerListDto>(
                sortshiftDetail.Count,
                sortshiftDetail.MapTo<List<SwipeCardLedgerListDto>>()
                );
        }

        public async Task<FileDto> GetAllToExcelShiftDetail(GetSwipeShiftDetailInput input)
        {
            input.MaxResultCount = AppConsts.MaxPageSize;
            var result = await GetShiftDetailViewAll(input);
            var allListDtos = result.Items.MapTo<List<SwipeCardLedgerListDto>>();
            foreach (var lst in allListDtos)
            {
                lst.LedgerTimeString = lst.LedgerTime.ToString("dd-MMM-yyyy HH:mm");
            }

            var baseE = new BaseExportObject
            {
                ExportObject = allListDtos,
                ColumnNames =
                    new[]
                    {
                        "LedgerTimeString", "CardNumber", "Description", "OpeningBalance", "Credit", "Debit",
                        "ClosingBalance","TicketNumber","LocationId"
                    },
                ColumnDisplayNames =
                    new[]
                    {
                        L("Time"), L("CardNumber"), L("Description"), L("OpeningBalance"), L("Credit"), L("Debit"),
                        L("ClosingBalance"),L("TicketNumber"),"Location"
                    }
            };
            return _exporter.ExportToFile(baseE, L("SwipeShiftDetail"));
        }

        public async Task<bool> ExistAllServerLevelBusinessRules(CreateOrUpdateSwipeShiftInput input)
        {
            var swipeShift = input.SwipeShift;

            if (swipeShift.SwipeShiftTenderList.Count == 0)
            {
                if (swipeShift.ShiftInOutStatus == 0)
                    throw new UserFriendlyException(L("ShiftOpenTenderInformationMissing"));
                throw new UserFriendlyException(L("ShiftCloseTenderInformationMissing"));
            }


            return true;
        }

        protected virtual async Task UpdateSwipeShift(CreateOrUpdateSwipeShiftInput input)
        {
            var item = await _swipeshiftRepo.GetAsync(input.SwipeShift.Id.Value);
            var dto = input.SwipeShift;

            item.ShiftEndTime = DateTime.Now;
            item.TenderCredit = dto.TenderCredit;
            item.TenderDebit = dto.TenderDebit;
            item.ActualTenderClosing = dto.ActualTenderClosing;
            item.UserTenderEntered = dto.UserTenderEntered;
            item.TenderCloseAmount = dto.UserTenderEntered;
            decimal diffAmount = (dto.UserTenderEntered) -  (item.TenderOpenAmount + item.TillShiftCashIn + item.TenderCredit - item.TenderDebit - item.TillShiftCashOut);

            if (dto.ExcessShortageAmount != (diffAmount))
            {
                throw new UserFriendlyException("ErrorInExcessShortageAmount");
            }
            item.ExcessShortageAmount = dto.ExcessShortageAmount;
            item.ExcessShortageStatus = dto.ExcessShortageStatus;

            CheckErrors(await _swipeshiftManager.CreateSync(item));
        }

        protected virtual async Task UpdateTillCashIn(CreateOrUpdateSwipeShiftInput input)
        {
            var item = await _swipeshiftRepo.GetAsync(input.SwipeShift.Id.Value);
            var dto = input.SwipeShift;

            item.TillShiftCashIn = item.TillShiftCashIn + dto.TillShiftCashIn;

            CheckErrors(await _swipeshiftManager.CreateSync(item));
        }

        protected virtual async Task CreateSwipeShift(CreateOrUpdateSwipeShiftInput input)
        {
            var dto = input.SwipeShift.MapTo<SwipeShift>();

            dto.ShiftStartTime = DateTime.Now;
            dto.ShiftEndTime = null;
            dto.TenderOpenAmount = input.SwipeShift.SwipeShiftTenderList.Sum(t => t.TenderedAmount);

            CheckErrors(await _swipeshiftManager.CreateSync(dto));

            foreach (var tenderOpen in input.SwipeShift.SwipeShiftTenderList)
            {
                var paymentDto = new SwipeShiftTender
                {
                    ShiftRefId = dto.Id,
                    ShiftInOutStatus = 0, //	Shift In 
                    PaymentTypeId = tenderOpen.PaymentTypeId,
                    //PaymentUserName = payment.PaymentUserName,
                    TenderedAmount = tenderOpen.TenderedAmount,
                    ActualAmount = 0
                };
                await _shifttenderRepo.InsertAndGetIdAsync(paymentDto);
            }
        }

        public async Task<bool> ExistAllServerLevelBusinessRulesForCashInCashOut(CreateOrUpdateSwipeShiftInput input)
        {
            var swipeShift = input.SwipeShift;

            if (swipeShift.SwipeShiftTenderList.Count == 0)
            {
                throw new UserFriendlyException(L("TenderInformationMissing"));
            }

            if (swipeShift.ShiftInOutStatus != 2 && swipeShift.ShiftInOutStatus != 3)
            {
                throw new UserFriendlyException(L("CashInCashOutOnlyAllowed"));
            }

            #region CashIn Business Rules 
            //  Needs to verify Cash 
            if (input.SwipeShift.ShiftInOutStatus == 2)
            {
                if (swipeShift.TillShiftCashIn == 0)
                {
                    throw new UserFriendlyException(L("CashIn") + " ? ");
                }
                decimal tempAmt = 0;
                foreach (var lst in swipeShift.SwipeShiftTenderList)
                {
                    tempAmt += lst.TenderedAmount;
                }
                if (tempAmt != swipeShift.TillShiftCashIn)
                {
                    throw new UserFriendlyException(L("TotalMismatchWithDetail") + " ? ");
                }
            }
            #endregion


            #region CashOut Business Rules 
            //  Needs to verify Cash 
            if (input.SwipeShift.ShiftInOutStatus == 3)
            {
                if (swipeShift.TillShiftCashOut == 0)
                {
                    throw new UserFriendlyException(L("CashOut") + " ? ");
                }
                decimal tempAmt = 0;
                foreach (var lst in swipeShift.SwipeShiftTenderList)
                {
                    tempAmt += lst.TenderedAmount;
                }
                if (tempAmt != swipeShift.TillShiftCashOut)
                {
                    throw new UserFriendlyException(L("TotalMismatchWithDetail") + " ? ");
                }

                //  Cash Out Amount Should Verified With Maximum Amount Available and Should not greater than that
                var shiftRs = await GetSwipeShiftForEdit(new NullableIdInput { Id = input.SwipeShift.Id.Value });
                var maxClosingValue = shiftRs.SwipeShift.ActualTenderClosing;

                if (swipeShift.TillShiftCashOut > maxClosingValue)
                {
                    throw new UserFriendlyException(L("AvaiableAmountLessThanCashOutAmount", maxClosingValue, swipeShift.TillShiftCashOut) + " ? ");
                }
            }
            #endregion
            return true;
        }

        public async Task<IdInput> AppendTillShiftCashInOut(CreateOrUpdateSwipeShiftInput input)
        {
            var output = await ExistAllServerLevelBusinessRulesForCashInCashOut(input);

            var item = await _swipeshiftRepo.GetAsync(input.SwipeShift.Id.Value);
            var dto = input.SwipeShift;

            if (input.SwipeShift.ShiftInOutStatus == 2)
                item.TillShiftCashIn = item.TillShiftCashIn + dto.TillShiftCashIn;
            else 
                item.TillShiftCashOut = item.TillShiftCashOut + dto.TillShiftCashOut;

            foreach (var tenderOpen in input.SwipeShift.SwipeShiftTenderList)
            {
                var paymentDto = new SwipeShiftTender
                {
                    ShiftRefId = item.Id,
                    ShiftInOutStatus = input.SwipeShift.ShiftInOutStatus, //	Shift In 
                    PaymentTypeId = tenderOpen.PaymentTypeId,
                    TenderedAmount = tenderOpen.TenderedAmount,
                    ActualAmount = 0
                };
                await _shifttenderRepo.InsertAndGetIdAsync(paymentDto);
            }

            return new IdInput { Id = 1 };
        }

    }
}