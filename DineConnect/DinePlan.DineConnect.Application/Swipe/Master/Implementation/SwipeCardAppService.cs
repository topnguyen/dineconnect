﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Swipe.Master.Dtos;
using DinePlan.DineConnect.Swipe.Transaction.Dtos;
using System.Text.RegularExpressions;

namespace DinePlan.DineConnect.Swipe.Master.Implementation
{
	public class SwipeCardAppService : DineConnectAppServiceBase, ISwipeCardAppService
	{
		private readonly IRepository<SwipeCardType> _cardTypeRepo;
		private readonly IMemberCardAppService _mebercardAppService;
		private readonly ISwipeCardListExcelExporter _swipecardExporter;
		private readonly ISwipeCardManager _swipecardManager;
		private readonly IRepository<SwipeCard> _swipecardRepo;

		public SwipeCardAppService(IRepository<SwipeCard> swipecardRepo,
			ISwipeCardListExcelExporter swipecardExporter,
			ISwipeCardManager swipecardManager,
			IRepository<SwipeCardType> cardTypeRepo,
			IMemberCardAppService mebercardAppService
			)
		{
			_swipecardRepo = swipecardRepo;
			_swipecardExporter = swipecardExporter;
			_cardTypeRepo = cardTypeRepo;
			_swipecardManager = swipecardManager;
			_mebercardAppService = mebercardAppService;
		}

		public async Task<PagedResultOutput<SwipeCardListDto>> GetAll(GetSwipeCardInput input)
		{
			var allItems = _swipecardRepo
				.GetAll()
				.WhereIf(
					!input.Filter.IsNullOrEmpty(),
					p => p.CardNumber.Equals(input.Filter)
				);
			var sortMenuItems = await allItems
				.OrderBy(input.Sorting)
				.PageBy(input)
				.ToListAsync();

			var allListDtos = sortMenuItems.MapTo<List<SwipeCardListDto>>();

			var allItemCount = await allItems.CountAsync();

			return new PagedResultOutput<SwipeCardListDto>(
				allItemCount,
				allListDtos
				);
		}

		public async Task<FileDto> GetAllToExcel()
		{
			var allList = await _swipecardRepo.GetAll().ToListAsync();
			var allListDtos = allList.MapTo<List<SwipeCardListDto>>();
			return _swipecardExporter.ExportToFile(allListDtos);
		}

		public async Task<GetSwipeCardForEditOutput> GetSwipeCardForEdit(NullableIdInput input)
		{
			SwipeCardEditDto editDto;

			if (input.Id.HasValue)
			{
				var hDto = await _swipecardRepo.GetAsync(input.Id.Value);
				editDto = hDto.MapTo<SwipeCardEditDto>();
			}
			else
			{
				editDto = new SwipeCardEditDto();
			}

			return new GetSwipeCardForEditOutput
			{
				SwipeCard = editDto
			};
		}

		public async Task<SwipeCardListDto> CreateOrUpdateSwipeCard(CreateOrUpdateSwipeCardInput input)
		{
			var ret = await ExistAllServerLevelBusinessRules(input);

			if (input.Import && !string.IsNullOrEmpty(input?.SwipeCard?.CardNumber))
			{
				var myCard = await _swipecardRepo.FirstOrDefaultAsync(a => a.CardNumber.Equals(input.SwipeCard.CardNumber));
				if (myCard != null)
				{
					input.SwipeCard.Id = myCard.Id;
				}
			}

			if (ret)
			{
				if (input.SwipeCard.Id.HasValue)
				{
					return await UpdateSwipeCard(input);
				}
				else
				{
					return await CreateSwipeCard(input);
				}
			}
			else
			{
				return null;
			}
		}

		public async Task DeleteSwipeCard(IdInput input)
		{
			await _swipecardRepo.DeleteAsync(input.Id);
		}

		public async Task<ListResultOutput<SwipeCardTypeListDto>> GetSwipeCardTypes()
		{
			var lst = await _cardTypeRepo.GetAll().ToListAsync();
			return new ListResultOutput<SwipeCardTypeListDto>(lst
				.MapTo<List<SwipeCardTypeListDto>>());
		}

		public async Task<string> GenerateSwipeCards(GenerateCardsInputDto input)
		{
			var returnStatus = "";
			var ct = await _cardTypeRepo.FirstOrDefaultAsync(t => t.Id == input.CardTypeRefId);
			if (ct == null)
			{
				throw new UserFriendlyException(L("CardTypeIdNotExist"));
			}

			if (input.CardNumberOfDigits + (input.Prefix.IsNullOrEmpty() ? 0 : input.Prefix.Length) +
				(input.Suffix.IsNullOrEmpty() ? 0 : input.Suffix.Length) > 20)
			{
				throw new UserFriendlyException(L("CardDigitsShouldNotBeGreaterThanNDigits", 20));
			}

			var allVou = new List<string>();

			for (var i = 0; i < input.CardCount; i++)
			{
				var myStr = input.CardStartingNumber + i; //GetUniqueString(allVou, gt.VoucherDigit);
				var cardNumber = myStr.ToString().Trim();
				var digitsDiff = input.CardNumberOfDigits - cardNumber.Trim().Length;
				if (digitsDiff > 0)
				{
					cardNumber = cardNumber.Trim().PadLeft(input.CardNumberOfDigits, '0');
				}
				else if (digitsDiff < 0)
					throw new UserFriendlyException(L("CardDigitsLessThanCardNumberLength", cardNumber,
						cardNumber.Trim().Length, input.CardNumberOfDigits));

				var cardNumberInSpecifiedDigits = $"{input.Prefix}{cardNumber}{input.Suffix}";

				bool activeStatus = false;

				//************************************ General Card Reg Expression  **************************

				//Visa: ^ 4[0 - 9]{ 12} (?:[0 - 9]{ 3})?$ All Visa card numbers start with a 4.New cards have 16 digits.Old cards have 13.
				//MasterCard: ^ 5[1 - 5][0 - 9]{ 14}$ All MasterCard numbers start with the numbers 51 through 55.All have 16 digits.
				//American Express: ^ 3[47][0 - 9]{ 13}$ American Express card numbers start with 34 or 37 and have 15 digits.
				//Diners Club: ^ 3(?:0[0 - 5] |[68][0 - 9])[0 - 9]{ 11}$ Diners Club card numbers begin with 300 through 305, 36 or 38.All have 14 digits.There are Diners Club cards that begin with 5 and have 16 digits.These are a joint venture between Diners Club and MasterCard, and should be processed like a MasterCard.
				//Discover: ^ 6(?:011 | 5[0 - 9]{ 2})[0-9]{12}$ Discover card numbers begin with 6011 or 65. All have 16 digits.
				//JCB: ^(?:2131|1800|35\d{3})\d{11}$ JCB cards beginning with 2131 or 1800 have 15 digits.JCB cards beginning with 35 have 16 digits.

				if (!ct.AutoActivateRegExpression.IsNullOrEmpty() && ct.AutoActivateRegExpression.Length > 0)
				{
					string pattern = ct.AutoActivateRegExpression.Replace(@"\\", @"\");

					if (Regex.IsMatch(cardNumberInSpecifiedDigits, pattern))
					{
						activeStatus = true;
					}
				}

				var swipeCard = new SwipeCard
				{
					CardTypeRefId = input.CardTypeRefId,
					CardNumber = cardNumberInSpecifiedDigits,
					Balance = 0M,
					ActiveStatus = activeStatus,
					DepositRequired = ct.DepositRequired,
					DepositAmount = ct.DepositAmount,
					RefundAllowed = ct.RefundAllowed,
					ExpiryDaysFromIssue = ct.ExpiryDaysFromIssue,
					CanIssueWithOutMember = ct.CanIssueWithOutMember,
					CanExcludeTaxAndCharges = ct.CanExcludeTaxAndCharges,
					IsEmployeeCard = ct.IsEmployeeCard,
					IsAutoRechargeCard = ct.IsAutoRechargeCard,
					DailyLimit = ct.DailyLimit,
					WeeklyLimit = ct.WeeklyLimit,
					MonthlyLimit = ct.MonthlyLimit,
					YearlyLimit = ct.YearlyLimit,
					MinimumTopUpAmount = ct.MinimumTopUpAmount
				};

				try
				{
					CheckErrors(await _swipecardManager.CreateSync(swipeCard));
				}
				catch (Exception exception)
				{
					var mess = exception.Message;
					throw new UserFriendlyException(mess);
				}

				//	If Auto Recharge Card Without Member- Activate Card Directly

				if (swipeCard.IsAutoRechargeCard && swipeCard.CanIssueWithOutMember && swipeCard.DepositAmount == 0 &&
					swipeCard.IsEmployeeCard == false)
				{
					var mc = new MemberCardEditDto
					{
						MemberRefId = null,
						CardRefId = swipeCard.Id,
						IsPrimaryCard = true,
						PrimaryCardRefId = swipeCard.Id,
						IssueDate = DateTime.Now,
						ReturnDate = null,
						ExpiryDate = null,
						DepositAmount = 0,
						IsActive = true,
						EmployeeRefId = null,
						ShiftRefId = null,
						CanIssueWithOutMember = true,
						IsEmployeeCard = false
					};

					var mcDto = new CreateOrUpdateMemberCardInput
					{
						MemberCard = mc,
						DepositCollectedAmount = 0,
						DepositRefundedAmount = 0,
						RefundAmount = 0,
						supplementaryCardFlag = false,
						TopUpAmount = 0
					};


					await _mebercardAppService.CreateOrUpdateMemberCard(mcDto);
				}
			}
			return returnStatus;
		}

		public async Task<bool> ExistAllServerLevelBusinessRules(CreateOrUpdateSwipeCardInput input)
		{
			if (input.AutoCreateFlag)
			{
				var cardType = await _cardTypeRepo.FirstOrDefaultAsync(t => t.Id == input.SwipeCard.CardTypeRefId);
				if (cardType == null)
				{
					throw new UserFriendlyException(L("CardTypeIdDoesNotExist", input.SwipeCard.CardTypeRefId));
				}
				var dtoSwipeCard = new SwipeCardEditDto
				{
					CardNumber = input.SwipeCard.CardNumber,
					CardTypeRefId = input.SwipeCard.CardTypeRefId,
					ActiveStatus = true,
					DailyLimit = cardType.DailyLimit,
					Balance = 0,
					DepositAmount = cardType.DepositAmount,
					DepositRequired = cardType.DepositRequired,
					CanExcludeTaxAndCharges = cardType.CanExcludeTaxAndCharges,
					ExpiryDaysFromIssue = cardType.ExpiryDaysFromIssue,
					CanIssueWithOutMember = cardType.CanIssueWithOutMember,
					IsAutoRechargeCard = cardType.IsAutoRechargeCard,
					IsEmployeeCard = cardType.IsEmployeeCard,
					MonthlyLimit = cardType.MonthlyLimit,
					RefundAllowed = cardType.RefundAllowed,
					WeeklyLimit = cardType.WeeklyLimit,
					YearlyLimit = cardType.YearlyLimit,
					MinimumTopUpAmount = cardType.MinimumTopUpAmount
				};
				input.SwipeCard = dtoSwipeCard;
			}

			var swipeCard = input.SwipeCard;

			if (swipeCard.Id == null)
			{
				var cardExists = await _swipecardRepo.FirstOrDefaultAsync(t => t.CardNumber == swipeCard.CardNumber);
				if (cardExists != null)
				{
					throw new UserFriendlyException(L("SwipeCardAlreadyExists", swipeCard.CardNumber));
				}
			}

			if (swipeCard.DepositRequired && swipeCard.DepositAmount == 0)
			{
				throw new UserFriendlyException(L("DepositAmount") + " ?");
			}

			if (swipeCard.ActiveStatus == false && swipeCard.Balance > 0)
			{
				throw new UserFriendlyException(L("BalanceActiveStatusError"));
			}


			if (swipeCard.IsAutoRechargeCard)
			{
				if (swipeCard.DailyLimit == 0 && swipeCard.WeeklyLimit == 0 && swipeCard.MonthlyLimit == 0 &&
					swipeCard.YearlyLimit == 0)
				{
					throw new UserFriendlyException(L("AutoRechargeCardLimitNeeded"));
				}
			}

			if (swipeCard.IsEmployeeCard && swipeCard.CanIssueWithOutMember)
			{
				throw new UserFriendlyException(L("CardAssignedToEmployeeSoCanIssueWithOutMemberError"));
			}

			#region AutoRechargeLimitCheck

			if (swipeCard.IsAutoRechargeCard)
			{
				var LimitFixed = false;
				var LimitType = "";
				if (swipeCard.DailyLimit > 0)
				{
					if (LimitFixed == false)
					{
						LimitType = L("Daily");
						LimitFixed = true;
					}
					else
					{
						throw new UserFriendlyException(L("LimitCanBeAnyOneOnlyError", LimitType));
					}
				}
				if (swipeCard.WeeklyLimit > 0)
				{
					if (LimitFixed == false)
					{
						LimitType = L("Weekly");
						LimitFixed = true;
					}
					else
					{
						throw new UserFriendlyException(L("LimitCanBeAnyOneOnlyError", LimitType));
					}
				}
				if (swipeCard.MonthlyLimit > 0)
				{
					if (LimitFixed == false)
					{
						LimitType = L("Monthly");
						LimitFixed = true;
					}
					else
					{
						throw new UserFriendlyException(L("LimitCanBeAnyOneOnlyError", LimitType));
					}
				}
				if (swipeCard.YearlyLimit > 0)
				{
					if (LimitFixed == false)
					{
						LimitType = L("Yearly");
						LimitFixed = true;
					}
					else
					{
						throw new UserFriendlyException(L("LimitCanBeAnyOneOnlyError", LimitType));
					}
				}
			}

			#endregion

			return true;
		}

		protected virtual async Task<SwipeCardListDto> UpdateSwipeCard(CreateOrUpdateSwipeCardInput input)
		{
			var item = await _swipecardRepo.GetAsync(input.SwipeCard.Id.Value);
			var dto = input.SwipeCard;
			//TODO: SERVICE SwipeCard Update Individually
			item.CardTypeRefId = dto.CardTypeRefId;
			item.CardNumber = dto.CardNumber;
			item.DepositAmount = dto.DepositAmount;
			item.DepositRequired = dto.DepositRequired;
			item.ExpiryDaysFromIssue = dto.ExpiryDaysFromIssue;
			item.RefundAllowed = dto.RefundAllowed;
			item.Balance = dto.Balance;
			item.ActiveStatus = dto.ActiveStatus;
			item.CanIssueWithOutMember = dto.CanIssueWithOutMember;
			item.CanExcludeTaxAndCharges = dto.CanExcludeTaxAndCharges;
			item.IsEmployeeCard = dto.IsEmployeeCard;
			item.IsAutoRechargeCard = dto.IsAutoRechargeCard;
			item.DailyLimit = dto.DailyLimit;
			item.WeeklyLimit = dto.WeeklyLimit;
			item.YearlyLimit = dto.YearlyLimit;
			item.MonthlyLimit = dto.MonthlyLimit;
			item.MinimumTopUpAmount = dto.MinimumTopUpAmount;
			
			CheckErrors(await _swipecardManager.CreateSync(item));
			return item.MapTo<SwipeCardListDto>();
		}

		protected virtual async Task<SwipeCardListDto> CreateSwipeCard(CreateOrUpdateSwipeCardInput input)
		{
			var dto = input.SwipeCard.MapTo<SwipeCard>();

			CheckErrors(await _swipecardManager.CreateSync(dto));
			return dto.MapTo<SwipeCardListDto>();
		}
	}
}