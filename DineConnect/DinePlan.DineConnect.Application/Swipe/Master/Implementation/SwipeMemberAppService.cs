﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Swipe.Master.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Utility.Exporter;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Engage;
using DinePlan.DineConnect.Engage.Member;
using DinePlan.DineConnect.Engage.Member.Dtos.DinePlan.DineConnect.Engage.Dtos;

namespace DinePlan.DineConnect.Swipe.Master.Implementation
{
	public class SwipeMemberAppService : DineConnectAppServiceBase, ISwipeMemberAppService
	{

		private readonly IExcelExporter _exporter;
		private readonly ISwipeMemberManager _swipememberManager;
		private readonly IRepository<ConnectMember> _swipememberRepo;
		private readonly IMemberAppService _memberAppService;
		private readonly IRepository<MemberAddress> _memberAddressRepo;

		public SwipeMemberAppService(ISwipeMemberManager swipememberManager,
			IRepository<ConnectMember> swipeMemberRepo,
			IExcelExporter exporter,
			IMemberAppService memberAppService,
			IRepository<MemberAddress> memberAddressRepo)
		{
			_swipememberManager = swipememberManager;
			_swipememberRepo = swipeMemberRepo;
			_exporter = exporter;
			_memberAppService = memberAppService;
			_memberAddressRepo = memberAddressRepo;
		}

		public async Task<PagedResultOutput<SwipeMemberListDto>> GetAll(GetSwipeMemberInput input)
		{
			var allItems = _swipememberRepo.GetAll();
			
			if (!input.Filter.IsNullOrEmpty())
			{
				allItems = allItems.Where(t => t.MemberCode.Contains(input.Filter) || t.PhoneNumber.Contains(input.Filter) || t.EmailId.Contains(input.Filter) || t.Name.Contains(input.Filter));
			}

			var sortMenuItems = await allItems
				.OrderBy(input.Sorting)
				.PageBy(input)
				.ToListAsync();

			var allListDtos = sortMenuItems.MapTo<List<SwipeMemberListDto>>();

			var allItemCount = await allItems.CountAsync();
			foreach(var lst in allListDtos)
			{
				MemberAddress ma;
				if (lst.DefaultAddressRefId!=null)
					ma = await _memberAddressRepo.FirstOrDefaultAsync(t => t.Id == lst.DefaultAddressRefId.Value);
				else
					ma = await _memberAddressRepo.FirstOrDefaultAsync(t => t.ConnectMemberId == lst.Id);
				if (ma != null)
				{
					lst.AddressLine1 = ma.Address;
					lst.Locality = ma.Locality;
					lst.City = ma.City;
					lst.State = ma.State;
					lst.Country = ma.Country;
					lst.PostalCode = ma.PostalCode;
				}
			}

			return new PagedResultOutput<SwipeMemberListDto>(
				allItemCount,
				allListDtos
				);
		}

		public async Task<FileDto> GetAllToExcel()
		{


			var allList = await _swipememberRepo.GetAll().ToListAsync();
			var allListDtos = allList.MapTo<List<SwipeMemberListDto>>();
			var baseE = new BaseExportObject()
			{
				ExportObject = allListDtos,
				ColumnNames = new string[] { "Id" }
			};
			return _exporter.ExportToFile(baseE, L("SwipeMember"));

		}

		public async Task<GetSwipeMemberForEditOutput> GetSwipeMemberForEdit(NullableIdInput input)
		{
			SwipeMemberEditDto editDto;

			if (input.Id.HasValue)
			{
				var hDto = await _swipememberRepo.GetAsync(input.Id.Value);
				editDto = hDto.MapTo<SwipeMemberEditDto>();
				MemberAddress ma;
				if (editDto.DefaultAddressRefId != null)
					ma = await _memberAddressRepo.FirstOrDefaultAsync(t => t.Id == editDto.DefaultAddressRefId.Value);
				else
					ma = await _memberAddressRepo.FirstOrDefaultAsync(t => t.ConnectMemberId == editDto.Id);
				if (ma != null)
				{
					editDto.AddressLine1 = ma.Address;
					editDto.Locality = ma.Locality;
					editDto.City = ma.City;
					editDto.State = ma.State;
					editDto.Country = ma.Country;
					editDto.PostalCode = ma.PostalCode;
				}
			}
			else
			{
				editDto = new SwipeMemberEditDto();
			}

			return new GetSwipeMemberForEditOutput
			{
				SwipeMember = editDto
			};
		}

		public async Task<SwipeMemberEditDto> CreateOrUpdateSwipeMember(CreateOrUpdateSwipeMemberInput input)
		{
			MemberEditDto member = new MemberEditDto();
			member.MemberCode = input.SwipeMember.MemberCode;
			member.Name = input.SwipeMember.Name;
			member.Address = input.SwipeMember.AddressLine1;
			member.Locality = input.SwipeMember.Locality;
			member.City = input.SwipeMember.City;
			member.State = input.SwipeMember.State;
			member.Country = input.SwipeMember.Country;
			member.PostalCode = input.SwipeMember.PostalCode;
			member.EmailId = input.SwipeMember.EmailId;
			member.PhoneNumber = input.SwipeMember.PhoneNumber;
			member.EmailId = input.SwipeMember.EmailId;
			member.Id = input.SwipeMember.Id;
			
			if (member.Id!=null)
				member.EditAddressRefId = input.SwipeMember.DefaultAddressRefId.Value;

			var ret = await _memberAppService.CreateOrUpdateMember(
				new CreateOrUpdateMemberInput {
					Member = member,AddOneMoreNewAddress=false,
					SetAsNewDefaultAddress = true				
			});

			var output = await GetSwipeMemberForEdit(new NullableIdInput { Id = ret.Id });
			return output.SwipeMember;
		}

		public async Task DeleteSwipeMember(IdInput input)
		{

			await _memberAppService.DeleteMember(input);
		}

	  public async Task<ListResultOutput<ComboboxItemDto>> GetSwipeMemberForCombobox()
		{
			var lstSwipeMember = await _swipememberRepo.GetAll().ToListAsync();
			return
				new ListResultOutput<ComboboxItemDto>(
					lstSwipeMember.Select(e => new ComboboxItemDto(e.Id.ToString(), e.MemberCode)).ToList());
		}

		public async Task<SwipeMemberListDto> MemberAlreadyExists(GetSwipeMemberInput input)
		{

			var existAlready = await _swipememberRepo.FirstOrDefaultAsync(t => t.MemberCode.ToUpper() == input.MemberCode.ToUpper());


			return existAlready.MapTo<SwipeMemberListDto>();
		}


		public async Task<PagedResultOutput<SwipeMemberListDto>> GetMemberList(GetSwipeMemberInput input)
		{
			var allItems = _swipememberRepo.GetAll();

			if (!input.Filter.IsNullOrEmpty())
			{
				allItems = allItems.Where(t => t.MemberCode.Contains(input.Filter) || t.PhoneNumber.Contains(input.Filter) || t.EmailId.Contains(input.Filter));
			}

			if (!input.PhoneNumber.IsNullOrEmpty())
			{
				allItems = allItems.Where(t =>  t.PhoneNumber.Contains(input.PhoneNumber));
			}

			if (!input.EmailId.IsNullOrEmpty())
			{
				allItems = allItems.Where(t => t.EmailId.Contains(input.EmailId));
			}

			if (!input.MemberCode.IsNullOrEmpty())
			{
				allItems = allItems.Where(t => t.MemberCode.Contains(input.MemberCode));
			}

			if (input.IsActive!=null)
			{
				allItems = allItems.Where(t => t.IsActive == input.IsActive.Value);
			}

			var sortMenuItems = await allItems
				.OrderBy(input.Sorting)
				.PageBy(input)
				.ToListAsync();

			var allListDtos = sortMenuItems.MapTo<List<SwipeMemberListDto>>();

			var allItemCount = await allItems.CountAsync();

			return new PagedResultOutput<SwipeMemberListDto>(
				allItemCount,
				allListDtos
				);
		}
	}
}
