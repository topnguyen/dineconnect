﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Swipe.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Utility.Exporter;
using DinePlan.DineConnect.Utility;

namespace DinePlan.DineConnect.Swipe.Master.Implementation
{
	public class SwipeCardTypeAppService : DineConnectAppServiceBase, ISwipeCardTypeAppService
	{

		private readonly IExcelExporter _exporter;
		private readonly ISwipeCardTypeManager _swipecardtypeManager;
		private readonly IRepository<SwipeCardType> _swipecardtypeRepo;
		private readonly IRepository<SwipeCard> _swipecardRepo;

		public SwipeCardTypeAppService(ISwipeCardTypeManager swipecardtypeManager,
			IRepository<SwipeCardType> swipeCardTypeRepo,
			IExcelExporter exporter,
			IRepository<SwipeCard> swipecardRepo)
		{
			_swipecardtypeManager = swipecardtypeManager;
			_swipecardtypeRepo = swipeCardTypeRepo;
			_exporter = exporter;
			_swipecardRepo = swipecardRepo;
		}

		public async Task<PagedResultOutput<SwipeCardTypeListDto>> GetAll(GetSwipeCardTypeInput input)
		{
			var allItems = _swipecardtypeRepo.GetAll();
			if (input.Operation == "SEARCH")
			{
				allItems = _swipecardtypeRepo
			   .GetAll()
			   .WhereIf(
				   !input.Filter.IsNullOrEmpty(),
				   p => p.Name.Equals(input.Filter)
			   );
			}
			else
			{
				allItems = _swipecardtypeRepo
			   .GetAll()
			   .WhereIf(
				   !input.Filter.IsNullOrEmpty(),
				   p => p.Name.Contains(input.Filter)
			   );
			}
			var sortMenuItems = await allItems
				.OrderBy(input.Sorting)
				.PageBy(input)
				.ToListAsync();

			var allListDtos = sortMenuItems.MapTo<List<SwipeCardTypeListDto>>();

			var allItemCount = await allItems.CountAsync();

			return new PagedResultOutput<SwipeCardTypeListDto>(
				allItemCount,
				allListDtos
				);
		}

		public async Task<FileDto> GetAllToExcel()
		{
			var allList = await _swipecardtypeRepo.GetAll().ToListAsync();
			var allListDtos = allList.MapTo<List<SwipeCardTypeListDto>>();
			var baseE = new BaseExportObject()
			{
				ExportObject = allListDtos,
				ColumnNames = new string[] { "Id" }
			};
			return _exporter.ExportToFile(baseE, L("SwipeCardType"));

		}

		public async Task<GetSwipeCardTypeForEditOutput> GetSwipeCardTypeForEdit(NullableIdInput input)
		{
			SwipeCardTypeEditDto editDto;
			List<SwipeCardListDto> editCardListDtos;

			if (input.Id.HasValue)
			{
				var hDto = await _swipecardtypeRepo.GetAsync(input.Id.Value);
				editDto = hDto.MapTo<SwipeCardTypeEditDto>();
				var det = await _swipecardRepo.GetAll().Where(t => t.CardTypeRefId == editDto.Id).OrderByDescending(t=>t.CreationTime).ToListAsync();
				editCardListDtos = det.MapTo<List<SwipeCardListDto>>();
				foreach(var lst in editCardListDtos)
				{
					if (lst.IsAutoRechargeCard==true)
					{
						if (lst.DailyLimit>0)
						{
							lst.LimitAmount = lst.DailyLimit;
							lst.LimitType = L("Daily");
						}
						else if (lst.WeeklyLimit > 0)
						{
							lst.LimitAmount = lst.WeeklyLimit;
							lst.LimitType = L("Weekly");
						}
						else if (lst.MonthlyLimit > 0)
						{
							lst.LimitAmount = lst.MonthlyLimit;
							lst.LimitType = L("Monthly");
						}
						else
							if (lst.YearlyLimit > 0)
						{
							lst.LimitAmount = lst.YearlyLimit;
							lst.LimitType = L("Yearly");
						}
					}
				}
			}
			else
			{
				editDto = new SwipeCardTypeEditDto();
				editCardListDtos = new List<SwipeCardListDto>();
			}

		    if (editDto.ExpiryDaysFromIssue == null)
		    {
		        editDto.ExpiryDaysFromIssue = 0;
		    }

			return new GetSwipeCardTypeForEditOutput
			{
				SwipeCardType = editDto,
				CardList = editCardListDtos
			};
		}

		public async Task<bool> ExistAllServerLevelBusinessRules(CreateOrUpdateSwipeCardTypeInput input)
		{
			var swipeCardType = input.SwipeCardType;

			if (swipeCardType.Id == null)
			{
				var cardExists = await _swipecardtypeRepo.FirstOrDefaultAsync(t => t.Name== swipeCardType.Name);
				if (cardExists != null)
				{
					throw new UserFriendlyException(L("NameAlreadyExists", swipeCardType.Name));
				}
			}

			if (swipeCardType.DepositRequired == true && swipeCardType.DepositAmount == 0)
			{
				throw new UserFriendlyException(L("DepositAmount") + " ?");
			}

			if (swipeCardType.IsAutoRechargeCard == true)
			{
				if (swipeCardType.DailyLimit == 0 && swipeCardType.WeeklyLimit == 0 && swipeCardType.MonthlyLimit == 0 && swipeCardType.YearlyLimit == 0)
				{
					throw new UserFriendlyException(L("AutoRechargeCardLimitNeeded"));
				}
			}

			if (swipeCardType.IsEmployeeCard == true && swipeCardType.CanIssueWithOutMember==true)
			{
					throw new UserFriendlyException(L("CardAssignedToEmployeeSoCanIssueWithOutMemberError"));
			}

			#region AutoRechargeLimitCheck
			if (swipeCardType.IsAutoRechargeCard)
			{
				bool LimitFixed = false;
				string LimitType = "";
				if (swipeCardType.DailyLimit>0)
				{
					if (LimitFixed==false)
					{
						LimitType = L("Daily");
						LimitFixed = true;
					}
					else
					{
						throw new UserFriendlyException(L("LimitCanBeAnyOneOnlyError", LimitType));
					}
				}
				if (swipeCardType.WeeklyLimit > 0)
				{
					if (LimitFixed == false)
					{
						LimitType = L("Weekly");
						LimitFixed = true;
					}
					else
					{
						throw new UserFriendlyException(L("LimitCanBeAnyOneOnlyError", LimitType));
					}
				}
				if (swipeCardType.MonthlyLimit > 0)
				{
					if (LimitFixed == false)
					{
						LimitType = L("Monthly");
						LimitFixed = true;
					}
					else
					{
						throw new UserFriendlyException(L("LimitCanBeAnyOneOnlyError", LimitType));
					}
				}
				if (swipeCardType.YearlyLimit > 0)
				{
					if (LimitFixed == false)
					{
						LimitType = L("Yearly");
						LimitFixed = true;
					}
					else
					{
						throw new UserFriendlyException(L("LimitCanBeAnyOneOnlyError", LimitType));
					}
				}

			}
			#endregion

			return true;
		}

		public async Task<IdInput> CreateOrUpdateSwipeCardType(CreateOrUpdateSwipeCardTypeInput input)
		{
			var ret = await ExistAllServerLevelBusinessRules(input);
			if (ret == true)
			{
				if (input.SwipeCardType.Id.HasValue)
				{
					return	await UpdateSwipeCardType(input);
				}
				else
				{
					return await CreateSwipeCardType(input);
				}
			}
			else

			{
				return new IdInput { Id = 0 };
			}
		}

		public async Task DeleteSwipeCardType(IdInput input)
		{
			await _swipecardtypeRepo.DeleteAsync(input.Id);
		}

		protected virtual async Task<IdInput> UpdateSwipeCardType(CreateOrUpdateSwipeCardTypeInput input)
		{
			var item = await _swipecardtypeRepo.GetAsync(input.SwipeCardType.Id.Value);
			var dto = input.SwipeCardType;

			item.Name = dto.Name;
			item.DepositAmount = dto.DepositAmount;
			item.DepositRequired = dto.DepositRequired;
			item.ExpiryDaysFromIssue = dto.ExpiryDaysFromIssue;
			item.RefundAllowed = dto.RefundAllowed;
			item.CanIssueWithOutMember = dto.CanIssueWithOutMember;
			item.CanExcludeTaxAndCharges = dto.CanExcludeTaxAndCharges;
			item.IsEmployeeCard = dto.IsEmployeeCard;
			item.IsAutoRechargeCard = dto.IsAutoRechargeCard;
			item.DailyLimit = dto.DailyLimit;
			item.WeeklyLimit = dto.WeeklyLimit;
			item.MonthlyLimit = dto.MonthlyLimit;
			item.YearlyLimit = dto.YearlyLimit;
			item.AutoActivateRegExpression = dto.AutoActivateRegExpression;
            item.MinimumTopUpAmount = dto.MinimumTopUpAmount;

			CheckErrors(await _swipecardtypeManager.CreateSync(item));
			return new IdInput { Id = item.Id };
		}

		protected virtual async Task<IdInput> CreateSwipeCardType(CreateOrUpdateSwipeCardTypeInput input)
		{
			var dto = input.SwipeCardType.MapTo<SwipeCardType>();

			CheckErrors(await _swipecardtypeManager.CreateSync(dto));

			return new IdInput { Id = dto.Id };
		}

		public async Task<ListResultOutput<SwipeCardTypeListDto>> GetNames()
		{
			var lstSwipeCardType = await _swipecardtypeRepo.GetAll().ToListAsync();
			return new ListResultOutput<SwipeCardTypeListDto>(lstSwipeCardType.MapTo<List<SwipeCardTypeListDto>>());
		}

	}
}
