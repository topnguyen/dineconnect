﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Authorization.Users.Dto;
using DinePlan.DineConnect.Common;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Hr;
using DinePlan.DineConnect.Hr.Report.Dto;
using DinePlan.DineConnect.Swipe.Master.Dtos;
using DinePlan.DineConnect.Swipe.Transaction.Dtos;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Utility.Exporter;

namespace DinePlan.DineConnect.Swipe.Master.Implementation
{
    public class MemberCardAppService : DineConnectAppServiceBase, IMemberCardAppService
    {
        private readonly IRepository<PersonalInformation> _employeeRepo;
        private readonly IExcelExporter _exporter;
        private readonly IRepository<MemberAddress> _memberAddressRepo;
        private readonly IMemberCardManager _membercardManager;
        private readonly IRepository<MemberCard> _membercardRepo;
        private readonly IRepository<SwipePaymentTender> _paymenttenderRepo;
        private readonly IRepository<SwipePaymentType> _paymenttypeRepo;
        private readonly SettingManager _settingManager;
        private readonly IRepository<SwipeCard> _swipecardRepo;
        private readonly IRepository<SwipeCardType> _swipecardtypeRepo;
        private readonly IRepository<SwipeCardLedger> _swipeledgerRepo;
        private readonly IRepository<ConnectMember> _swipememberRepo;
        private readonly IUserAppService _userAppService;
        private readonly IXmlAndJsonConvertor _xmlandjsonConvertorAppService;

        public MemberCardAppService(IMemberCardManager membercardManager,
            IRepository<MemberCard> memberCardRepo,
            IRepository<SwipeCard> swipecardRepo,
            IRepository<ConnectMember> swipememberRepo,
            IRepository<SwipePaymentTender> paymenttenderRepo,
            IRepository<SwipeCardLedger> swipeledgerRepo,
            IRepository<SwipeCardType> swipecardtypeRepo,
            IRepository<SwipePaymentType> paymenttypeRepo,
            IRepository<MemberAddress> memberAddressRepo,
            SettingManager settingManager,
            IExcelExporter exporter,
            IUserAppService userAppService,
            IRepository<PersonalInformation> employeeRepo,
            IXmlAndJsonConvertor xmlandjsonConvertorAppService)
        {
            _membercardManager = membercardManager;
            _membercardRepo = memberCardRepo;
            _exporter = exporter;
            _swipecardRepo = swipecardRepo;
            _swipememberRepo = swipememberRepo;
            _paymenttenderRepo = paymenttenderRepo;
            _swipeledgerRepo = swipeledgerRepo;
            _settingManager = settingManager;
            _swipecardtypeRepo = swipecardtypeRepo;
            _paymenttypeRepo = paymenttypeRepo;
            _userAppService = userAppService;
            _employeeRepo = employeeRepo;
            _memberAddressRepo = memberAddressRepo;
            _xmlandjsonConvertorAppService = xmlandjsonConvertorAppService;
        }

        public async Task<PagedResultOutput<MemberCardListDto>> GetAll(GetMemberCardInput input)
        {
            var allItems = _membercardRepo.GetAll();

            allItems = _membercardRepo
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Id.ToString().Contains(input.Filter)
                );

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<MemberCardListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<MemberCardListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _membercardRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<MemberCardListDto>>();
            var baseE = new BaseExportObject
            {
                ExportObject = allListDtos,
                ColumnNames = new[] {"Id","CardNumber","DepositAmount","Balance"}
            };
            return _exporter.ExportToFile(baseE, L("MemberCard"));
        }

        public async Task<GetMemberCardForEditOutput> GetMemberCardForEdit(NullableIdInput input)
        {
            MemberCardEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _membercardRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<MemberCardEditDto>();
                var card = await _swipecardRepo.FirstOrDefaultAsync(t => t.Id == editDto.CardRefId);
                if (card == null)
                {
                    throw new UserFriendlyException(L("CardIdNotExist", editDto.CardRefId));
                }
                editDto.CardNumber = card.CardNumber;

                if (editDto.MemberRefId.HasValue)
                {
                    var swipemember = await _swipememberRepo.FirstOrDefaultAsync(t => t.Id == editDto.MemberRefId);
                    if (swipemember == null)
                    {
                        throw new UserFriendlyException(L("CardIdNotExist", editDto.CardRefId));
                    }
                    editDto.MemberCode = swipemember.MemberCode;
                    editDto.Email = swipemember.EmailId;
                    editDto.PhoneNumber = swipemember.PhoneNumber;
                }
                else
                {
                    editDto.MemberCode = L("Guest");
                    editDto.Email = "";
                    editDto.PhoneNumber = "";
                }
            }
            else
            {
                editDto = new MemberCardEditDto();
            }

            return new GetMemberCardForEditOutput
            {
                MemberCard = editDto
            };
        }

        public async Task DeactivateMemberCard(IdInput input)
        {
            var mc = await _membercardRepo.FirstOrDefaultAsync(t => t.Id == input.Id);
            if (mc != null)
            {
                #region CheckCardHaveBalance  // IF Card Have Balance can not allow them to Deactivate

                var swipecard = await _swipecardRepo.FirstOrDefaultAsync(t => t.Id == mc.CardRefId);
                if (swipecard.Balance > 0)
                {
                    throw new UserFriendlyException(L("CardHavingBalanceAmount", swipecard.Balance));
                }

                #endregion

                #region CheckAnySupplementaryCardReferThisCard

                var isAnySupplementaryReference =
                    await _membercardRepo.FirstOrDefaultAsync(t => t.IsActive && t.PrimaryCardRefId == mc.CardRefId);
                if (isAnySupplementaryReference != null)
                {
                    var supcard =
                        await _swipecardRepo.FirstOrDefaultAsync(t => t.Id == isAnySupplementaryReference.CardRefId);

                    throw new UserFriendlyException(L("SupplementaryCardReferThisPrimaryCard", supcard.CardNumber,
                        swipecard.CardNumber));
                }

                #endregion

                var editDtos = await _membercardRepo.GetAsync(mc.Id);
                editDtos.IsActive = false;
                await _membercardRepo.InsertOrUpdateAndGetIdAsync(editDtos);

                if (mc.MemberRefId.HasValue)
                {
                    var anyMoreCardsAlloted =
                        await _membercardRepo.FirstOrDefaultAsync(t => t.MemberRefId == mc.MemberRefId && t.IsActive);

                    if (anyMoreCardsAlloted != null) //	Having one more cards
                    {
                        //	Do nothing
                    }
                    else
                    {
                        if (mc.MemberRefId.HasValue)
                        {
                            var memberDto = await _swipememberRepo.GetAsync(mc.MemberRefId.Value);
                            memberDto.IsActive = false;
                            await _swipememberRepo.InsertOrUpdateAndGetIdAsync(memberDto);
                        }
                    }
                }
            }
        }

        public async Task<List<IdInput>> AttachEmployeeCard(CreateOrUpdateMemberCardInput input)
        {
            var output = await ExistAllServerLevelBusinessRules(input);
            if (output == false)
                return null;

            if (input.MemberCard.IsEmployeeCard == false)
            {
                throw new UserFriendlyException(L("ThisCardTypeIsNotForEmployee"));
            }

            var swipeCard = await _swipecardRepo.FirstOrDefaultAsync(t => t.Id == input.MemberCard.CardRefId);

            if (swipeCard.CanIssueWithOutMember == false && swipeCard.IsEmployeeCard)
            {
                var mc = new MemberCardEditDto
                {
                    MemberRefId = null,
                    CardRefId = swipeCard.Id,
                    IsPrimaryCard = true,
                    PrimaryCardRefId = swipeCard.Id,
                    IssueDate = DateTime.Now,
                    ReturnDate = null,
                    ExpiryDate = null,
                    DepositAmount = 0,
                    IsActive = true,
                    IsEmployeeCard = true,
                    EmployeeRefId = input.MemberCard.EmployeeRefId,
                    ShiftRefId = null,
                    CanIssueWithOutMember = true
                };

                var mcDto = new CreateOrUpdateMemberCardInput
                {
                    MemberCard = mc,
                    DepositCollectedAmount = input.DepositCollectedAmount,
                    DepositRefundedAmount = 0,
                    RefundAmount = 0,
                    supplementaryCardFlag = false,
                    TopUpAmount = 0
                };
                return await CreateOrUpdateMemberCard(mcDto);
            }
            return null;
        }

        public async Task<ApiTopUpCardReturnDto> ApiTopUpCard(ApiTopupCardInputDto input)
        {
            var output = new ApiTopUpCardReturnDto();

            var card = await _swipecardRepo.FirstOrDefaultAsync(t => t.Id == input.CardRefId);
            if (card == null)
            {
                output.ErrorMessage = L("SwipeCardNotFound", input.CardRefId);
                return output;
            }

            var mc = await _membercardRepo.FirstOrDefaultAsync(t => t.IsActive && t.CardRefId == input.CardRefId);
            if (mc == null)
            {
                output.ErrorMessage = L("CardIdNotExist", input.CardRefId);
                return output;
            }

            if (input.SwipePaymentTenderList == null || input.SwipePaymentTenderList.Count == 0)
            {
                output.ErrorMessage = L("PaymentModeNeeded");
                return output;
            }

            var mcnew = new MemberCardEditDto();
            mcnew = mc.MapTo<MemberCardEditDto>();
            mcnew.SwipePaymentTenderList = input.SwipePaymentTenderList;

            var newDto = new CreateOrUpdateMemberCardInput
            {
                MemberCard = mcnew,
                supplementaryCardFlag = false,
                TopUpAmount = input.TopUpAmount
            };

            var ret = await CreateOrUpdateMemberCard(newDto);

            var balance = await GetCardBalance(card);

            output.Balance = balance;
            output.ErrorMessage = "";
            return output;
        }

        public async Task<List<IdInput>> CreateOrUpdateMemberCard(CreateOrUpdateMemberCardInput input)
        {
            var output = await ExistAllServerLevelBusinessRules(input);
            if (output == false)
                return null;
            return await CreateMemberCard(input);
        }

        public async Task<SwipeCardBalanceDto> ApiGetSwipeCardCurrentBalance(SwipeCardListDto input)
        {
            var output = new SwipeCardBalanceDto
            {
                ErrorMessage = "",
                Balance = 0,
                DepositBalance = 0,
                CanExcludeTaxAndCharges = false,
                IsAutoRechargeCard = false,
                IsEmployeeCard = false
            };

            SwipeCardDepositDto depositDetail;

            var card = await _swipecardRepo.FirstOrDefaultAsync(t => t.Id == input.Id);
            if (card == null)
            {
                card =
                    await _swipecardRepo.FirstOrDefaultAsync(t => t.CardNumber.ToUpper() == input.CardNumber.ToUpper());
                if (card == null)
                {
                    output.ErrorMessage = L("CardNumberDoesNotExist", input.CardNumber);
                    return output;
                }
            }
            if (card.ActiveStatus == false)
            {
                output.ErrorMessage = L("CardIsNotInActiveStatus", card.CardNumber);
                output.Balance = card.Balance;
                output.CanExcludeTaxAndCharges = card.CanExcludeTaxAndCharges;
                output.IsEmployeeCard = card.IsEmployeeCard;
                output.IsAutoRechargeCard = card.IsAutoRechargeCard;
                depositDetail = await ApiGetSwipeCardCurrentDepositBalance(
                    new SwipeCardListDto {Id = card.Id});
                output.DepositBalance = depositDetail.TotalDeposit;
                output.IsFirstTimeCard = true;
                return output;
            }

            output.IsFirstTimeCard = false;
            var membercard = await _membercardRepo.FirstOrDefaultAsync(t => t.CardRefId == card.Id && t.IsActive);

            if (membercard == null)
            {
                output.ErrorMessage = L("MemberCardNotInActiveStatus");
                if (card != null)
                {
                    output.Balance = card.Balance;
                    output.CanExcludeTaxAndCharges = card.CanExcludeTaxAndCharges;
                    output.IsEmployeeCard = card.IsEmployeeCard;
                    output.IsAutoRechargeCard = card.IsAutoRechargeCard;
                    depositDetail = await ApiGetSwipeCardCurrentDepositBalance(
                        new SwipeCardListDto {Id = card.Id});
                    output.DepositBalance = depositDetail.TotalDeposit;
                }
                return output;
            }

            if (membercard.ExpiryDate != null && membercard.ExpiryDate < DateTime.Now)
            {
                output.ErrorMessage = L("CardAlreadyExpired", membercard.ExpiryDate.Value.ToString("yyyy-MMM-dd"));
                card = await _swipecardRepo.FirstOrDefaultAsync(t => t.Id == membercard.PrimaryCardRefId);
                if (card != null)
                {
                    output.Balance = card.Balance;
                    output.CanExcludeTaxAndCharges = card.CanExcludeTaxAndCharges;
                    output.IsEmployeeCard = card.IsEmployeeCard;
                    output.IsAutoRechargeCard = card.IsAutoRechargeCard;
                    depositDetail = await ApiGetSwipeCardCurrentDepositBalance(
                        new SwipeCardListDto {Id = card.Id});
                    output.DepositBalance = depositDetail.TotalDeposit;
                }
                return output;
            }

            if (membercard.IsPrimaryCard == false)
            {
                card = await _swipecardRepo.FirstOrDefaultAsync(t => t.Id == membercard.PrimaryCardRefId);
                if (card == null)
                {
                    output.ErrorMessage = L("PrimaryCardNumberDoesNotExist", input.CardNumber);
                    return output;
                }
            }

            output.Balance = await GetCardBalance(card);
            output.CanExcludeTaxAndCharges = card.CanExcludeTaxAndCharges;
            output.IsEmployeeCard = card.IsEmployeeCard;
            output.IsAutoRechargeCard = card.IsAutoRechargeCard;

            depositDetail = await ApiGetSwipeCardCurrentDepositBalance(
                new SwipeCardListDto {Id = card.Id});
            output.DepositBalance = depositDetail.TotalDeposit;
            return output;
        }

        public async Task<ListResultOutput<SwipeCardListDto>> GetSwipeCardDetails()
        {
            var lst = await _swipecardRepo.GetAll().ToListAsync();
            return new ListResultOutput<SwipeCardListDto>(lst.MapTo<List<SwipeCardListDto>>());
        }

        public Task<PagedResultOutput<MemberCardListDto>> GetAllSwipeCardsForMember(GetMemberCardDetails input)
        {
            if (input.EmployeeRefId == null && input.MemberRefId == null)
            {
                throw new UserFriendlyException(L("EitherMemberIdOrEmployeeIdRequired"));
            }

            if (input.EmployeeRefId != null && input.MemberRefId != null)
            {
                throw new UserFriendlyException(L("Error"));
            }

            if (input.MemberRefId.HasValue)
            {
                return GetMemberCardListAll(new GetMemberCardInput
                {
                    MemberRefId = input.MemberRefId,
                    EmployeeRefId = null,
                    BalanceNeeded = true
                });
            }
            if (input.EmployeeRefId.HasValue)
            {
                return GetMemberCardListAll(new GetMemberCardInput
                {
                    MemberRefId = null,
                    EmployeeRefId = input.EmployeeRefId,
                    BalanceNeeded = true
                });
            }
            return null;
        }

        public async Task<PagedResultOutput<MemberCardListDto>> GetMemberCardListAll(GetMemberCardInput input)
        {
            var allMembers = _swipememberRepo.GetAll();

            if (!input.Filter.IsNullOrEmpty())
            {
                allMembers =
                    allMembers.Where(
                        t =>
                            t.MemberCode.Contains(input.Filter) || t.PhoneNumber.Contains(input.Filter) ||
                            t.EmailId.Contains(input.Filter));
            }

            if (!input.PhoneNumber.IsNullOrEmpty())
            {
                allMembers = allMembers.Where(t => t.PhoneNumber.Contains(input.PhoneNumber));
            }

            if (!input.EmailId.IsNullOrEmpty())
            {
                allMembers = allMembers.Where(t => t.EmailId.Contains(input.EmailId));
            }

            if (!input.MemberCode.IsNullOrEmpty())
            {
                allMembers = allMembers.Where(t => t.MemberCode.Contains(input.MemberCode));
            }

            if (input.MemberRefId != null)
            {
                allMembers = allMembers.Where(t => t.Id == input.MemberRefId.Value);
            }


            if (input.IsActive != null)
            {
                allMembers = allMembers.Where(t => t.IsActive == input.IsActive.Value);
            }

            var allMemberCards = _membercardRepo.GetAll();

            if (input.ActiveMemberCardsFlag != null)
            {
                if (input.ActiveMemberCardsFlag == true)
                    allMemberCards = allMemberCards.Where(t => t.IsActive);
            }

            if (input.MemberRefId.HasValue)
            {
                allMemberCards = allMemberCards.Where(t => t.MemberRefId == input.MemberRefId.Value);
            }
            else if (input.EmployeeRefId.HasValue)
            {
                allMemberCards = allMemberCards.Where(t => t.EmployeeRefId == input.EmployeeRefId.Value);
            }

            var allCards = _swipecardRepo.GetAll();

            if (!allMembers.Any())
            {
                var cardExist =
                    await _swipecardRepo.FirstOrDefaultAsync(t => t.CardNumber.ToUpper() == input.Filter.ToUpper());
                if (cardExist != null)
                {
                    var membercards =
                        await _membercardRepo.GetAll().Where(t => t.CardRefId == cardExist.Id).ToListAsync();
                    var arrMemberRefIds = membercards.Select(t => t.MemberRefId.Value).ToArray();
                    allMembers = _swipememberRepo.GetAll().Where(t => arrMemberRefIds.Contains(t.Id));
                }
            }

            var guestString = L("Guest");
            var sortMenuItems = await (from mc in allMemberCards
                join sm in allMembers
                    on mc.MemberRefId equals sm.Id into leftjoinmat
                from lj in leftjoinmat.DefaultIfEmpty()
                join ca in allCards on mc.CardRefId equals ca.Id
                select new MemberCardListDto
                {
                    Id = mc.Id,
                    MemberRefId = mc.MemberRefId,
                    MemberCode = lj.MemberCode ?? guestString,
                    MemberPhone = lj.PhoneNumber ?? "",
                    MemberEmail = lj.EmailId ?? "",
                    CardRefId = mc.CardRefId,
                    CardNumber = ca.CardNumber,
                    IsPrimaryCard = mc.IsPrimaryCard,
                    PrimaryCardRefId = mc.PrimaryCardRefId,
                    PrimaryCardNumer = "",
                    IssueDate = mc.IssueDate,
                    ReturnDate = mc.ReturnDate,
                    ExpiryDate = mc.ExpiryDate,
                    IsActive = mc.IsActive,
                    DepositAmount = mc.DepositAmount,
                    EmployeeRefId = mc.EmployeeRefId,
                    EmployeeRefCode = "",
                    EmployeeRefName = "",
                    CardTypeRefId = ca.CardTypeRefId
                }
                ).ToListAsync();

            var rsCards = await allCards.ToListAsync();
            var rsMember = await _swipememberRepo.GetAll().ToListAsync();
            var rsEmployee = await _employeeRepo.GetAll().ToListAsync();
            foreach (var lst in sortMenuItems)
            {
                #region PrimarCardReference

                var primarycard = rsCards.FirstOrDefault(t => t.Id == lst.PrimaryCardRefId);
                if (primarycard == null)
                {
                    throw new UserFriendlyException(L("PrimaryCardDoesNotExist", lst.PrimaryCardRefId));
                }
                lst.PrimaryCardNumer = primarycard.CardNumber;

                var mc =
                    await
                        _membercardRepo.FirstOrDefaultAsync(t => t.IsPrimaryCard && t.CardRefId == lst.PrimaryCardRefId);
                if (mc == null)
                {
                    throw new UserFriendlyException(L("PrimaryCardDoesNotExist", primarycard.CardNumber));
                }

                if (mc.MemberRefId.HasValue)
                {
                    var member = rsMember.FirstOrDefault(t => t.Id == mc.MemberRefId);
                    if (member == null)
                    {
                        throw new UserFriendlyException(L("MemberIdNotExist", mc.MemberRefId));
                    }
                    lst.PrimaryCardMemberCode = member.MemberCode;
                }

                #endregion

                #region Get Employee Info

                if (lst.EmployeeRefId.HasValue)
                {
                    var employee = rsEmployee.FirstOrDefault(t => t.Id == lst.EmployeeRefId);
                    if (employee == null)
                    {
                        throw new UserFriendlyException(L("EmployeeNotExist", lst.EmployeeRefId));
                    }
                    lst.EmployeeRefCode = employee.EmployeeCode;
                    lst.EmployeeRefName = employee.EmployeeCode + " - " + employee.EmployeeName;
                }

                #endregion

                if (input.BalanceNeeded)
                {
                    if (lst.IsPrimaryCard)
                    {
                        lst.Balance = primarycard.Balance;
                        var card = rsCards.FirstOrDefault(t => t.Id == lst.CardRefId);
                        if (card.IsAutoRechargeCard)
                        {
                            lst.Balance = await GetCardBalance(card);
                        }
                        var depositBalance =
                            await
                                ApiGetSwipeCardCurrentDepositBalance(new SwipeCardListDto {CardNumber = lst.CardNumber});
                        lst.DepositBalance = depositBalance.TotalDeposit;
                    }
                }
            }

            return new PagedResultOutput<MemberCardListDto>(
                sortMenuItems.Count,
                sortMenuItems
                );
        }

        public async Task<PagedResultOutput<MemberViewDto>> GetMemberViewListAll(GetMemberCardInput input)
        {
            var allMembers = _swipememberRepo.GetAll();

            if (!input.MemberCode.IsNullOrEmpty())
            {
                allMembers = allMembers.Where(t => t.MemberCode.Contains(input.MemberCode));
            }

            if (input.IsActive != null)
            {
                allMembers = allMembers.Where(t => t.IsActive == input.IsActive.Value);
            }

            if (!input.PhoneNumber.IsNullOrEmpty())
            {
                allMembers = allMembers.Where(t => t.PhoneNumber.Contains(input.PhoneNumber));
            }

            if (!input.EmailId.IsNullOrEmpty())
            {
                allMembers = allMembers.Where(t => t.EmailId.Contains(input.EmailId));
            }

            var allCards = _swipecardRepo.GetAll();
            var allMemberCards = _membercardRepo.GetAll().Where(t => t.IsPrimaryCard && t.IsActive);

            if (!input.CardNumber.IsNullOrEmpty())
            {
                allCards = allCards.Where(t => t.CardNumber.Contains(input.CardNumber));
            }

            if (!input.Filter.IsNullOrEmpty())
            {
                allMembers =
                    allMembers.Where(
                        t =>
                            t.MemberCode.Contains(input.Filter) || t.PhoneNumber.Contains(input.Filter) ||
                            t.EmailId.Contains(input.Filter));

                allCards = allCards.Where(t => t.CardNumber.Contains(input.Filter));
                var arrCardRefIds = allCards.ToList().Select(t => t.Id).ToArray();
                if (arrCardRefIds.Length > 0)
                {
                    var lstMemberCards = allMemberCards.Where(t => arrCardRefIds.Contains(t.CardRefId)).ToList();
                    var arrMemberRefIds = lstMemberCards.Select(t => t.MemberRefId.Value).ToArray();

                    if (arrMemberRefIds.Length > 0)
                        allMembers = allMembers.Where(t => arrMemberRefIds.Contains(t.Id));
                }
            }

            if (allMembers.Count() == 0)
            {
                var cardExist =
                    await _swipecardRepo.FirstOrDefaultAsync(t => t.CardNumber.ToUpper() == input.Filter.ToUpper());
                if (cardExist != null)
                {
                    var membercards =
                        await _membercardRepo.GetAll().Where(t => t.CardRefId == cardExist.Id).ToListAsync();
                    var arrMemberRefIds = membercards.Select(t => t.MemberRefId.Value).ToArray();
                    allMembers = _swipememberRepo.GetAll().Where(t => arrMemberRefIds.Contains(t.Id));
                }
            }

            //var sortMenuItems = allMembers.ToList().MapTo<List<MemberViewDto>>();
            var guestString = L("Guest");

            var sortMenuItems = await (from mc in allMemberCards
                join sm in allMembers
                    on mc.MemberRefId equals sm.Id into leftjoinmat
                from lj in leftjoinmat.DefaultIfEmpty()
                select new MemberViewDto
                {
                    Id = mc.MemberRefId,
                    MemberRefId = mc.MemberRefId,
                    MemberCode = lj.MemberCode == null ? guestString : lj.MemberCode,
                    Name = lj.Name == null ? guestString : lj.Name,
                    PhoneNumber = lj.PhoneNumber == null ? "" : lj.PhoneNumber,
                    EmailId = lj.EmailId == null ? "" : lj.EmailId,
                    CardRefId = mc.CardRefId,
                    IsActive = mc.IsActive
                }).ToListAsync();

            allCards = _swipecardRepo.GetAll();
            allMemberCards = _membercardRepo.GetAll();
            var rsCards = await _swipecardRepo.GetAllListAsync();
            var rsMemberCards = await _membercardRepo.GetAllListAsync();

            foreach (var lst in sortMenuItems)
            {
                IEnumerable<MemberCard> lstMcs;
                if (lst.MemberRefId.HasValue)
                {
                    lstMcs = rsMemberCards.Where(t => t.IsActive && t.MemberRefId == lst.MemberRefId);
                }
                else
                {
                    lstMcs =
                        rsMemberCards.Where(
                            t => t.IsActive && t.MemberRefId == lst.MemberRefId && t.CardRefId == lst.CardRefId);
                }
                lst.MemberCards = lstMcs.MapTo<List<MemberCardDetailDto>>();
                //if (lst.MemberCards.Where(t=>t.IsPrimaryCard==true).Count()>1)
                //{
                //	throw new UserFriendlyException(L("MemberCanNotHaveMoreThanOnePrimaryCard",lst.MemberCode + " " + lst.Name));
                //}
                foreach (var mc in lst.MemberCards.Where(t => t.IsActive))
                {
                    var card = rsCards.FirstOrDefault(t => t.Id == mc.CardRefId);
                    if (card.ActiveStatus)
                    {
                        mc.CardNumber = card.CardNumber;
                        if (mc.IsPrimaryCard == false)
                        {
                            var primarycard = rsCards.FirstOrDefault(t => t.Id == mc.PrimaryCardRefId);
                            if (primarycard == null)
                            {
                                throw new UserFriendlyException(L("PrimaryCardDoesNotExist", mc.PrimaryCardRefId));
                            }
                            mc.PrimaryCardNumer = primarycard.CardNumber;
                        }
                        else
                        {
                            if (lst.CardRefId != null)
                            {
                                lst.CardRefId = mc.CardRefId;
                                lst.CardNumber = card.CardNumber;
                                lst.Balance = card.Balance;
                                var depositBalance =
                                    await
                                        ApiGetSwipeCardCurrentDepositBalance(new SwipeCardListDto
                                        {
                                            CardNumber = lst.CardNumber
                                        });
                                lst.DepositBalance = depositBalance.TotalDeposit;
                            }
                        }
                    }
                }
            }

            return new PagedResultOutput<MemberViewDto>(
                sortMenuItems.Count,
                sortMenuItems
                );
        }

        public async Task<MemberTransactionListDto> GetCardTransactionList(GetMemberCardInput input)
        {
            if (input.LastNRecord == 0)
            {
                input.LastNRecord = 10;
            }
            var swipecard = new SwipeCard();
            ConnectMember member;
            MemberCard memberCard = null;

            #region MemberInformations

            if (!input.MemberRefId.HasValue)
            {
                if (input.CardNumber.IsNullOrEmpty() && input.CardRefId.HasValue == false)
                {
                    throw new UserFriendlyException(L("CardNumberShouldNotBeEmpty"));
                }
                if (input.CardRefId.HasValue)
                    swipecard = await _swipecardRepo.FirstOrDefaultAsync(t => t.Id == input.CardRefId.Value);
                else
                    swipecard =
                        await
                            _swipecardRepo.FirstOrDefaultAsync(
                                t => t.CardNumber.ToUpper().Equals(input.CardNumber.ToUpper()));

                if (swipecard == null)
                {
                    throw new UserFriendlyException(L("CardNumberDoesNotExist", input.CardNumber));
                }

                memberCard = await _membercardRepo.FirstOrDefaultAsync(t => t.CardRefId == swipecard.Id && t.IsActive);
                if (memberCard == null)
                {
                    var mclist = _membercardRepo.GetAll().Where(t => t.CardRefId == swipecard.Id);
                    if (mclist.Count() > 0)
                    {
                        var maxId = mclist.Max(t => t.Id);
                        memberCard = await _membercardRepo.FirstOrDefaultAsync(t => t.Id == maxId);
                    }
                    if (memberCard == null)
                    {
                        return new MemberTransactionListDto();
                    }
                }

                if (memberCard.IsPrimaryCard == false)
                    swipecard = await _swipecardRepo.FirstOrDefaultAsync(t => t.Id == memberCard.PrimaryCardRefId);
                if (memberCard.MemberRefId.HasValue)
                {
                    member = await _swipememberRepo.FirstOrDefaultAsync(t => t.Id == memberCard.MemberRefId);

                    if (member == null)
                    {
                        throw new UserFriendlyException(L("MemberNotFound", memberCard.MemberRefId));
                    }
                }
                else
                {
                    member = new ConnectMember
                    {
                        Name = L("Guest"),
                        MemberCode = L("Guest"),
                        IsActive = true
                    };
                }
            }
            else
            {
                member = await _swipememberRepo.FirstOrDefaultAsync(t => t.Id == input.MemberRefId.Value);

                if (member == null)
                {
                    member = new ConnectMember
                    {
                        Name = L("Guest"),
                        MemberCode = L("Guest"),
                        IsActive = true
                    };
                }
            }

            #endregion

            var output = member.MapTo<MemberTransactionListDto>();

            if (memberCard != null)
            {
                if (memberCard.EmployeeRefId.HasValue)
                {
                    var employee = await _employeeRepo.FirstOrDefaultAsync(t => t.Id == memberCard.EmployeeRefId);
                    if (employee != null)
                    {
                        output.EmployeeRefId = employee.Id;
                        output.EmployeeRefName = employee.EmployeeName;
                    }
                }
            }

            var rsCards =  _swipecardRepo.GetAll();
            List<MemberCard> rsMemberCards;
            List<MemberCard> mcPrimaryCards;

            if (output.Id == 0)
            {
                if (memberCard != null && memberCard.EmployeeRefId.HasValue)
                {
                    output.MemberRefId = null;
                    mcPrimaryCards =
                        await
                            _membercardRepo.GetAll()
                                .Where(t => t.EmployeeRefId == memberCard.EmployeeRefId && t.IsActive && t.IsPrimaryCard)
                                .ToListAsync();
                    var arrCardRefids = mcPrimaryCards.Select(t => t.CardRefId).ToArray();
                    rsMemberCards =
                        await
                            _membercardRepo.GetAllListAsync(
                                t =>
                                    t.IsActive && t.EmployeeRefId == memberCard.EmployeeRefId &&
                                    arrCardRefids.Contains(t.CardRefId));
                }
                else
                {
                    if (input.CardRefId.HasValue)
                    {
                        output.MemberRefId = null;
                        rsCards = _swipecardRepo.GetAll().Where(t => t.Id == input.CardRefId.Value);
                        rsMemberCards =
                            await
                                _membercardRepo.GetAllListAsync(
                                    t =>
                                        t.MemberRefId == null && t.EmployeeRefId == null &&
                                        t.CardRefId == input.CardRefId.Value && t.IsActive);
                        mcPrimaryCards =
                            await
                                _membercardRepo.GetAll()
                                    .Where(
                                        t =>
                                            t.MemberRefId == null && t.EmployeeRefId == null && t.IsActive &&
                                            t.CardRefId == input.CardRefId.Value && t.IsPrimaryCard)
                                    .ToListAsync();
                    }
                    else
                    {
                        throw new UserFriendlyException(L("MemberAndCardIdDoesNotExist", input.MemberRefId,
                            input.CardRefId));
                    }
                }
            }
            else
            {
                output.MemberRefId = output.Id;
                MemberAddress ma;
                if (output.DefaultAddressRefId != null)
                    ma = await _memberAddressRepo.FirstOrDefaultAsync(t => t.Id == output.DefaultAddressRefId.Value);
                else
                    ma = await _memberAddressRepo.FirstOrDefaultAsync(t => t.ConnectMemberId == output.Id);
                if (ma != null)
                {
                    output.AddressLine1 = ma.Address;
                    output.Locality = ma.Locality;
                    output.City = ma.City;
                    output.State = ma.State;
                    output.Country = ma.Country;
                    output.PostalCode = ma.PostalCode;
                }
                rsCards = _swipecardRepo.GetAll();
                mcPrimaryCards =
                    await
                        _membercardRepo.GetAll()
                            .Where(t => t.MemberRefId == output.MemberRefId && t.IsActive && t.IsPrimaryCard)
                            .ToListAsync();
                rsMemberCards =
                    await _membercardRepo.GetAllListAsync(t => t.IsActive && t.MemberRefId == output.MemberRefId);
            }

            var cardDetails = new List<CardInformationDto>();

            foreach (var mpc in mcPrimaryCards)
            {
                var mainCard = rsCards.FirstOrDefault(t => t.Id == mpc.CardRefId);
                
                if (mainCard == null)
                    continue;

                var cardInfo = new CardInformationDto
                {
                    CardRefId = mpc.CardRefId,
                    CardNumber = mainCard.CardNumber,
                    Balance = await GetCardBalance(mainCard)
                };
                var depositbalance =
                    await ApiGetSwipeCardCurrentDepositBalance(new SwipeCardListDto {CardNumber = mainCard.CardNumber});
                cardInfo.DepositBalance = depositbalance.TotalDeposit;

                IEnumerable<MemberCard> lstMcs;
                if (output.MemberRefId.HasValue)
                {
                    lstMcs =
                        rsMemberCards.Where(
                            t =>
                                t.IsActive && t.MemberRefId == output.MemberRefId && t.PrimaryCardRefId == mpc.CardRefId);
                }
                else
                {
                    if (output.EmployeeRefId.HasValue)
                    {
                        lstMcs =
                            rsMemberCards.Where(
                                t =>
                                    t.IsActive && t.MemberRefId == null && t.EmployeeRefId == output.EmployeeRefId &&
                                    t.PrimaryCardRefId == mpc.CardRefId);
                    }
                    else
                    {
                        lstMcs =
                            rsMemberCards.Where(
                                t =>
                                    t.IsActive && t.MemberRefId == null && t.EmployeeRefId == null &&
                                    t.CardRefId == mpc.CardRefId && t.PrimaryCardRefId == mpc.CardRefId);
                    }
                }

                cardInfo.MemberCards = lstMcs.MapTo<List<MemberCardDetailDto>>();

                foreach (var mc in cardInfo.MemberCards)
                {
                    var card = rsCards.FirstOrDefault(t => t.Id == mc.CardRefId);
                    mc.CardNumber = card.CardNumber;
                    if (mc.IsPrimaryCard == false)
                    {
                        var primaryCard = await _swipecardRepo.FirstOrDefaultAsync(t => t.Id == mc.PrimaryCardRefId);
                        if (primaryCard == null)
                        {
                            throw new UserFriendlyException(L("PrimaryCardDoesNotExist", mc.PrimaryCardRefId));
                        }
                        mc.PrimaryCardNumer = primaryCard.CardNumber;
                    }
                    else
                    {
                        mc.PrimaryCardNumer = card.CardNumber;
                    }
                }

                if (input.TransactionDetailNotNeeded == false)
                {
                    List<SwipeCardLedger> rsTran;
                    var rsLedger = _swipeledgerRepo.GetAll();
                    if (input.StartDate.HasValue && input.EndDate.HasValue)
                    {
                        rsLedger =
                            rsLedger.Where(
                                t =>
                                    DbFunctions.TruncateTime(t.LedgerTime) >=
                                    DbFunctions.TruncateTime(input.StartDate.Value) &&
                                    DbFunctions.TruncateTime(t.LedgerTime) <=
                                    DbFunctions.TruncateTime(input.EndDate.Value));
                    }
                    if (output.MemberRefId.HasValue)
                    {
                        rsTran =
                            await
                                rsLedger.Where(
                                    t => t.MemberRefId == output.MemberRefId && t.PrimaryCardRefId == swipecard.Id)
                                    .OrderByDescending(t => t.Id)
                                    .ToListAsync();
                    }
                    else
                    {
                        if (output.EmployeeRefId.HasValue)
                        {
                            rsTran =
                                await
                                    rsLedger.Where(
                                        t =>
                                            t.EmployeeRefId == output.EmployeeRefId &&
                                            t.PrimaryCardRefId == swipecard.Id)
                                        .OrderByDescending(t => t.Id)
                                        .ToListAsync();
                        }
                        else
                        {
                            rsTran =
                                await
                                    rsLedger.Where(
                                        t =>
                                            t.MemberRefId == output.MemberRefId && t.CardRefId == cardInfo.CardRefId &&
                                            t.PrimaryCardRefId == swipecard.Id)
                                        .OrderByDescending(t => t.Id)
                                        .ToListAsync();
                        }
                    }

                    if (!input.StartDate.HasValue)
                    {
                        if (rsTran.Count > input.LastNRecord)
                            rsTran = rsTran.Take(input.LastNRecord).ToList();
                    }

                    rsTran = rsTran.OrderBy(t => t.Id).ToList();

                    var a = await GetSwipeTransactionTypeForCombobox();
                    var swipeTransactionTypeList = a.Items.ToList();

                    var transactionList = rsTran.MapTo<List<SwipeCardLedgerListDto>>();
                    foreach (var lst in transactionList)
                    {
                        var card = rsCards.FirstOrDefault(t => t.Id == lst.CardRefId);
                        if (card == null)
                            throw new UserFriendlyException(L("CardIdNotExist", lst.CardRefId));

                        lst.CardNumber = card.CardNumber;

                        if (lst.PrimaryCardRefId == lst.CardRefId)
                        {
                            lst.CardType = L("Primary");
                        }
                        else
                        {
                            memberCard =
                                await _membercardRepo.FirstOrDefaultAsync(t => t.CardRefId == card.Id && t.IsActive);
                            if (memberCard == null)
                            {
                                var mclist = _membercardRepo.GetAll().Where(t => t.CardRefId == card.Id);
                                if (mclist.Any())
                                {
                                    var maxId = mclist.Max(t => t.Id);
                                    memberCard = await _membercardRepo.FirstOrDefaultAsync(t => t.Id == maxId);
                                }
                                if (memberCard == null)
                                {
                                    continue;
                                }
                            }
                            lst.CardType = L(memberCard.IsPrimaryCard ? "Primary" : "Supplementary");
                        }

                        var transactionType =
                            swipeTransactionTypeList.FirstOrDefault(t => int.Parse(t.Value) == lst.TransactionType);
                        if (transactionType == null)
                        {
                            throw new UserFriendlyException(L("TransactionTypeIdNotExist", lst.TransactionType));
                        }
                        lst.TransactionTypeName = transactionType.DisplayText;
                    }
                    cardInfo.CardTransactionDetails = transactionList;
                }
                cardDetails.Add(cardInfo);
            }
            output.CardDetails = cardDetails;
            output.NoOfPrimaryCards = output.CardDetails.Count;
            return output;
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetSwipeTransactionTypeForCombobox()
        {
            var retList = new List<ComboboxItemDto>();

            string enumstring;
            var EnumValues = Enum.GetValues(typeof (SwipeTransaction));
            foreach (int EnumValue in EnumValues)
            {
                enumstring = Enum.GetName(typeof (SwipeTransaction), EnumValue);
                retList.Add(new ComboboxItemDto {Value = EnumValue.ToString(), DisplayText = enumstring});
            }

            return
                new ListResultOutput<ComboboxItemDto>(
                    retList.Select(e => new ComboboxItemDto(e.Value.ToString(), e.DisplayText)).ToList());
        }

        public async Task<IdInput> ApiUpdateSwipeCardLedger(ApiLedgerInput input)
        {
            if (string.IsNullOrEmpty(input.CardNumber))
            {
                throw new UserFriendlyException(L("CardNumberShouldNotBeEmpty"));
            }

            if (input.TransactionType == (int) SwipeTransaction.REDEEM)
            {
                var cardBalance =
                    await ApiGetSwipeCardCurrentBalance(new SwipeCardListDto {CardNumber = input.CardNumber});
                if (cardBalance.Balance < input.AmountDebitFromSwipeCard)
                {
                    throw new UserFriendlyException(L("BalanceNotAvailableInTheCardNumber", input.CardNumber,
                        cardBalance.Balance));
                }
            }


            var card = await _swipecardRepo.FirstOrDefaultAsync(a => a.CardNumber.Equals(input.CardNumber));
            if (card == null)
            {
                throw new UserFriendlyException(L("CardIdNotExist", input.CardNumber));
            }
            if (card.ActiveStatus == false)
            {
                throw new UserFriendlyException(L("CardIsNotInActiveStatus", card.CardNumber));
            }

            var membercard = await _membercardRepo.FirstOrDefaultAsync(t => t.CardRefId == card.Id && t.IsActive);
            if (membercard == null)
            {
                throw new UserFriendlyException(L("CardNotIssuedToAnyMember", input.CardNumber));
            }
            var primaryCard = card.Id;
            if (!membercard.IsPrimaryCard)
            {
                primaryCard = membercard.PrimaryCardRefId;
            }

            var newLedger = new SwipeCardLedger
            {
                LedgerTime = input.LedgerTime,
                CardRefId = card.Id,
                PrimaryCardRefId = primaryCard,
                Debit = input.AmountDebitFromSwipeCard,
                TransactionType = input.TransactionType,
                Description = input.Description,
                TicketNumber = input.TicketNumber,
                LocationId = input.LocationId,
                MemberRefId = membercard.MemberRefId,
                EmployeeRefId = membercard.EmployeeRefId
            };


            var tranId =
                await
                    UpdateSwipeMemberCardLedger(newLedger, membercard.MemberRefId == null ? true : false,
                        membercard.EmployeeRefId == null ? false : true);

            return tranId;
        }

        public async Task<SwipeCardDepositDto> ApiGetSwipeCardCurrentDepositBalance(SwipeCardListDto input)
        {
            var output = new SwipeCardDepositDto
            {
                ErrorMessage = "",
                TotalDeposit = 0
            };

            var card = await _swipecardRepo.FirstOrDefaultAsync(t => t.Id == input.Id);
            if (card == null)
            {
                card =
                    await _swipecardRepo.FirstOrDefaultAsync(t => t.CardNumber.ToUpper() == input.CardNumber.ToUpper());
                if (card == null)
                {
                    output.ErrorMessage = L("CardNumberDoesNotExist", input.CardNumber);
                    return output;
                }
            }

            var membercard = await _membercardRepo.FirstOrDefaultAsync(t => t.CardRefId == card.Id && t.IsActive);

            if (membercard == null)
            {
                output.ErrorMessage = L("MemberCardNotInActiveStatus");
                return output;
            }

            if (membercard.ExpiryDate < DateTime.Now)
            {
                output.ErrorMessage = L("CardAlreadyExpired", card.CardNumber,
                    membercard.ExpiryDate.Value.ToString("yyyy-MMM-dd"));
                return output;
            }
            var primaryCardRefId = 0;

            if (membercard.IsPrimaryCard == false)
            {
                card = await _swipecardRepo.FirstOrDefaultAsync(t => t.Id == membercard.PrimaryCardRefId);
                if (card == null)
                {
                    output.ErrorMessage = L("PrimaryCardNumberDoesNotExist", input.CardNumber);
                    return output;
                }
                primaryCardRefId = card.Id;
            }
            else
            {
                primaryCardRefId = membercard.CardRefId;
            }

            List<MemberCard> lstMemberCards;
            if (membercard.MemberRefId.HasValue)
            {
                lstMemberCards =
                    await
                        _membercardRepo.GetAll()
                            .Where(
                                t =>
                                    t.IsActive && t.MemberRefId == membercard.MemberRefId &&
                                    (t.PrimaryCardRefId == primaryCardRefId))
                            .ToListAsync();
            }
            else
            {
                if (membercard.EmployeeRefId.HasValue)
                {
                    lstMemberCards =
                        await
                            _membercardRepo.GetAll()
                                .Where(
                                    t =>
                                        t.IsActive && t.EmployeeRefId == membercard.EmployeeRefId &&
                                        t.MemberRefId == membercard.MemberRefId &&
                                        (t.PrimaryCardRefId == primaryCardRefId))
                                .ToListAsync();
                }
                else
                {
                    lstMemberCards =
                        await
                            _membercardRepo.GetAll()
                                .Where(
                                    t =>
                                        t.IsActive && t.MemberRefId == membercard.MemberRefId &&
                                        t.CardRefId == membercard.CardRefId && t.IsPrimaryCard)
                                .ToListAsync();
                }
            }

            var depositAmount = lstMemberCards.Sum(t => t.DepositAmount);

            output.TotalDeposit = depositAmount;
            return output;
        }

        public async Task<SwipeCardDashBoardReport> GetSwipeCardDashBoardReport()
        {
            var output = new SwipeCardDashBoardReport();

            var lstCardTypes = new List<SwipeCardTypeReport>();

            var rsGt = await _swipecardtypeRepo.GetAllListAsync();
            foreach (var lst in rsGt)
            {
                var detDto = new SwipeCardTypeReport();
                detDto.CardTypeRefId = lst.Id;
                detDto.CardDetails = lst.MapTo<SwipeCardTypeListDto>();
                ;

                var rsCards =
                    await _swipecardRepo.GetAll().Where(t => t.CardTypeRefId == detDto.CardTypeRefId).ToListAsync();
                var arrCardRefIds = rsCards.Select(t => t.Id).ToArray();

                var rsMemberCards =
                    await
                        _membercardRepo.GetAll()
                            .Where(t => t.IsActive && arrCardRefIds.Contains(t.CardRefId))
                            .ToListAsync();

                var rsActiveMemberCards =
                    rsMemberCards.Where(t => t.IsActive && (t.ExpiryDate == null || t.ExpiryDate > DateTime.Today))
                        .ToList();

                detDto.NumberOfCardsActivated = rsActiveMemberCards.Count;

                detDto.NumberOfCardsDorment = rsCards.Count - detDto.NumberOfCardsActivated;

                detDto.NumberOfCardsGenerated = rsCards.Count;

                var memberActiveCards = rsActiveMemberCards.Select(t => t.CardRefId).ToArray();
                var rsActiveCards = rsCards.Where(t => memberActiveCards.Contains(t.Id)).ToList();

                detDto.TotalAmountBalance = rsActiveCards.Sum(t => t.Balance);
                var rsDormentCards = rsCards.Where(t => !memberActiveCards.Contains(t.Id)).ToList();

                detDto.TotalAmountConfiscated = rsDormentCards.Sum(t => t.Balance);

                var arrRefundableCardRefId = rsCards.Where(t => t.RefundAllowed).Select(t => t.Id).ToArray();
                var rsRefunableDeposit =
                    rsMemberCards.Where(t => t.IsActive && arrRefundableCardRefId.Contains(t.CardRefId)).ToList();

                detDto.RefundableDepositCollected = rsRefunableDeposit.Sum(t => t.DepositAmount);


                var arrNonRefundableCardRefId = rsCards.Where(t => t.RefundAllowed == false).Select(t => t.Id).ToArray();
                var rsNonRefunableDeposit =
                    rsMemberCards.Where(t => t.IsActive && arrNonRefundableCardRefId.Contains(t.CardRefId));
                detDto.NonRefundableDepositCollected = rsNonRefunableDeposit.Sum(t => t.DepositAmount);
                lstCardTypes.Add(detDto);
            }

            output.SwipeCardTypeList = lstCardTypes;
            output.NumberOfCardsGenerated = lstCardTypes.Sum(t => t.NumberOfCardsGenerated);
            output.NumberOfCardsActivated = lstCardTypes.Sum(t => t.NumberOfCardsActivated);
            output.NumberOfCardsDorment = lstCardTypes.Sum(t => t.NumberOfCardsDorment);
            output.TotalAmountBalance = lstCardTypes.Sum(t => t.TotalAmountBalance);
            output.TotalAmountConfiscated = lstCardTypes.Sum(t => t.TotalAmountConfiscated);
            output.RefundableDepositCollected = lstCardTypes.Sum(t => t.RefundableDepositCollected);
            output.NonRefundableDepositCollected = lstCardTypes.Sum(t => t.NonRefundableDepositCollected);

            return output;
        }

        public async Task<SwipeTransactionStatsDto> GetCardTransactionChart(GetSwipeChartInput input)
        {
            var result = new SwipeTransactionStatsDto();

            var chartOutput = new List<BarChartOutputDto>();
            var rsCardLedgers =
                await
                    _swipeledgerRepo.GetAll()
                        .Where(
                            t =>
                                t.LedgerTime >= input.StartDate &&
                                DbFunctions.TruncateTime(t.LedgerTime) <= input.EndDate)
                        .ToListAsync();
            var lstTransactionTypes = await GetSwipeTransactionTypeForCombobox();
            foreach (var lst in rsCardLedgers.GroupBy(t => t.TransactionType))
            {
                var tt = lstTransactionTypes.Items.FirstOrDefault(t => t.Value == lst.Key.ToString());
                var name = tt.DisplayText;
                var data = lst.Sum(t => t.Debit + t.Credit);

                chartOutput.Add(new BarChartOutputDto
                {
                    name = name,
                    data = new List<decimal> {data}
                });
            }
            result.Transactions = chartOutput.Select(t => t.name).ToList();
            result.ChartOutput = chartOutput;
            return result;
        }

        public async Task<PagedResultOutput<MemberViewDto>> GetMemberIndexView(GetMemberCardInput input)
        {
            var allMembers = _swipememberRepo.GetAll();
            var allCards = _swipecardRepo.GetAll();
            var allMemberCards = _membercardRepo.GetAll().Where(t => t.IsPrimaryCard && t.IsActive);
            var arrMemberRefIds = new List<int?>();
            var filterFlag = false;

            if (input.IsActive != null)
            {
                allMembers = allMembers.Where(t => t.IsActive == input.IsActive.Value);
                filterFlag = true;
            }

            var cardNumberExists = false;
            var memberWithoutRegnExists = false;
            if (!input.Filter.IsNullOrEmpty())
            {
                allCards = allCards.Where(t => t.CardNumber.Contains(input.Filter));
                var arrCardRefIds = allCards.ToList().Select(t => t.Id).ToArray();
                if (arrCardRefIds.Length > 0)
                {
                    cardNumberExists = true;
                    var lstMemberCards = allMemberCards.Where(t => arrCardRefIds.Contains(t.CardRefId)).ToList();
                    var arrCardMemberRefIds = lstMemberCards.Select(t => t.MemberRefId).ToList();
                    if (arrCardMemberRefIds.Contains(null))
                        memberWithoutRegnExists = true;
                    arrMemberRefIds.AddRange(arrCardMemberRefIds);
                    //if (arrMemberRefIds.Length > 0)
                    //	allMembers = allMembers.Where(t => arrMemberRefIds.Contains(t.Id));
                }
                filterFlag = true;
            }

            if (!input.Filter.IsNullOrEmpty())
            {
                var arrRefIds =
                    allMembers.Where(
                        t =>
                            t.MemberCode.Contains(input.Filter) || t.PhoneNumber.Contains(input.Filter) ||
                            t.EmailId.Contains(input.Filter)).Select(t => t.Id).ToList();
                arrMemberRefIds.AddRange(arrRefIds.MapTo<List<int?>>());
                filterFlag = true;
            }


            if (filterFlag)
            {
                if (memberWithoutRegnExists)
                    allMemberCards =
                        allMemberCards.Where(t => arrMemberRefIds.Contains(t.MemberRefId.Value) || t.MemberRefId == null);
                else
                    allMemberCards = allMemberCards.Where(t => arrMemberRefIds.Contains(t.MemberRefId.Value));
            }

            var guestString = L("Guest");

            var sortMenuItems = await (from mc in allMemberCards
                join sm in allMembers
                    on mc.MemberRefId equals sm.Id into leftjoinmat
                from lj in leftjoinmat.DefaultIfEmpty()
                select new MemberViewDto
                {
                    Id = mc.MemberRefId,
                    MemberRefId = mc.MemberRefId,
                    MemberCode = lj.MemberCode == null ? guestString : lj.MemberCode,
                    Name = lj.Name == null ? guestString : lj.Name,
                    PhoneNumber = lj.PhoneNumber == null ? "" : lj.PhoneNumber,
                    //AlternatePhoneNumber = lj.AlternatePhoneNumber == null ? "" : lj.AlternatePhoneNumber,
                    EmailId = lj.EmailId == null ? "" : lj.EmailId,
                    //AlternateEmailId = lj.AlternateEmailId == null ? "" : lj.AlternateEmailId,
                    //AddressLine1 = lj.AddressLine1 == null ? "" : lj.AddressLine1,
                    //AddressLine2 = lj.AddressLine2 == null ? "" : lj.AddressLine2,
                    //Locality = lj.Locality == null ? "" : lj.Locality,
                    //City = lj.City == null ? "" : lj.City,
                    CardRefId = mc.CardRefId,
                    IsActive = mc.IsActive,
                    EmployeeRefId = mc.EmployeeRefId
                }).ToListAsync();

            allCards = _swipecardRepo.GetAll();
            allMemberCards = _membercardRepo.GetAll();
            var rsCards = await _swipecardRepo.GetAllListAsync();
            var rsMemberCards = await _membercardRepo.GetAllListAsync();
            var rsEmployees = await _employeeRepo.GetAllListAsync();

            foreach (var lst in sortMenuItems)
            {
                IEnumerable<MemberCard> lstMcs;
                if (lst.MemberRefId.HasValue)
                {
                    lstMcs =
                        rsMemberCards.Where(
                            t => t.IsActive && t.MemberRefId == lst.MemberRefId && t.CardRefId == lst.CardRefId);
                }
                else
                {
                    if (lst.EmployeeRefId.HasValue)
                    {
                        var emp = rsEmployees.FirstOrDefault(t => t.Id == lst.EmployeeRefId);
                        if (emp != null)
                        {
                            lst.MemberCode = L("Emp") + " : " + emp.EmployeeCode;
                            lst.Name = L("Emp") + " : " + emp.EmployeeName;
                            lst.EmployeeRefName = emp.EmployeeName;
                        }
                    }
                    lstMcs =
                        rsMemberCards.Where(
                            t => t.IsActive && t.MemberRefId == lst.MemberRefId && t.CardRefId == lst.CardRefId);
                }
                lst.MemberCards = lstMcs.MapTo<List<MemberCardDetailDto>>();
                //if (lst.MemberCards.Where(t => t.IsPrimaryCard == true).Count() > 1)
                //{
                //	throw new UserFriendlyException(L("MemberCanNotHaveMoreThanOnePrimaryCard", lst.MemberCode + " " + lst.Name));
                //}
                foreach (var mc in lst.MemberCards.Where(t => t.IsActive))
                {
                    var card = rsCards.FirstOrDefault(t => t.Id == mc.CardRefId);
                    if (card != null && card.ActiveStatus)
                    {
                        mc.CardNumber = card.CardNumber;
                        if (mc.IsPrimaryCard == false)
                        {
                            var primarycard = rsCards.FirstOrDefault(t => t.Id == mc.PrimaryCardRefId);
                            if (primarycard == null)
                            {
                                throw new UserFriendlyException(L("PrimaryCardDoesNotExist", mc.PrimaryCardRefId));
                            }
                            mc.PrimaryCardNumer = primarycard.CardNumber;
                        }
                        else
                        {
                            if (lst.CardRefId != null)
                            {
                                lst.CardRefId = mc.CardRefId;
                                lst.CardNumber = card.CardNumber;
                                lst.Balance = card.Balance;
                            }
                        }
                    }
                }
            }

            return new PagedResultOutput<MemberViewDto>(
                sortMenuItems.Count,
                sortMenuItems
                );
        }

        public async Task<SwipeCardLedgerListDto> GetSwipeCardLedgerDetail(IdInput input)
        {
            SwipeCardLedgerListDto output;
            var rec = await _swipeledgerRepo.FirstOrDefaultAsync(input.Id);

            output = rec.MapTo<SwipeCardLedgerListDto>();
            var users = await _userAppService.GetUsers(new
                GetUsersInput
            {
                MaxResultCount = 1000
            });

            var userList = users.Items;
            var user = userList.FirstOrDefault(t => t.Id == output.CreatorUserId);
            if (user != null)
            {
                output.UserName = user.Name;
            }

            if (rec == null)
            {
                throw new UserFriendlyException(L("NoDataFound"));
            }

            if (rec.MemberRefId == null)
            {
                if (rec.EmployeeRefId != null)
                {
                    output.EmployeeRefId = rec.EmployeeRefId;
                    var employee = await _employeeRepo.FirstOrDefaultAsync(t => t.Id == rec.EmployeeRefId);
                    if (employee != null)
                    {
                        output.EmployeeRefName = employee.EmployeeName;
                        output.Employee = employee.MapTo<EmployeeInfoListDto>();
                    }
                }
                else
                {
                    output.MemberRefName = L("Guest");
                    output.SwipeMember = null;
                    output.Employee = null;
                }
            }
            else
            {
                var member = await _swipememberRepo.FirstOrDefaultAsync(t => t.Id == rec.MemberRefId);
                output.MemberRefName = member.Name;
                output.SwipeMember = member.MapTo<SwipeMemberListDto>();
                output.Employee = null;
            }

            var card = await _swipecardRepo.FirstOrDefaultAsync(t => t.Id == rec.CardRefId);
            output.CardNumber = card.CardNumber;

            var paymentList = await (from a in _paymenttenderRepo
                .GetAll().Where(t => t.SwipeCardLedgerRefId == input.Id)
                join b in _paymenttypeRepo.GetAll()
                    on a.PaymentTypeId equals b.Id
                select new SwipePaymentTenderListDto
                {
                    PaymentTypeId = a.PaymentTypeId,
                    PaymentTypeName = b.Name,
                    SwipeCardLedgerRefId = a.SwipeCardLedgerRefId,
                    Amount = a.Amount,
                    TenderedAmount = a.TenderedAmount,
                    Id = a.Id,
                    PaymentUserName = a.PaymentUserName
                }).ToListAsync();

            output.PaymentDetailList = paymentList;

            return output;
        }

        [UnitOfWork]
        public virtual async Task<List<IdInput>> BatchUpdateMemberCard(BatchMemberCardTransactionDto input)
        {
            List<IdInput> output = new List<IdInput>();
            var pendingCount = input.CardList.Count;
            var batchPaymentTender = input.BatchPaymentTenderList.OrderBy(t=>t.PaymentTypeId);

            foreach(var lst in batchPaymentTender)
            {
                lst.PendingAmount = lst.TenderedAmount;
            }

            foreach (var lst in input.CardList)
            {
                //if (pendingCount==1)
                //{
                //    lst.MemberCard.SwipePaymentTenderList = input.BatchPaymentTenderList;
                //}
                lst.IsBatchUpdate = true;
                decimal adjustedAmount = 0;
                if (lst.TopUpAmount>0)
                {
                    adjustedAmount = lst.TopUpAmount + lst.DepositCollectedAmount;
                }
                else
                {
                    adjustedAmount = lst.RefundAmount + lst.DepositRefundedAmount;
                }
                List<SwipePaymentTenderListDto> paymentTenderList = new List<SwipePaymentTenderListDto>();
                do
                {
                    decimal amountAlloted = 0;
                    var exist = batchPaymentTender.Where(t => t.PendingAmount > 0).FirstOrDefault();
                    if (exist==null)
                    {
                        throw new UserFriendlyException(L("Tender") + " ?");
                    }
                    if (exist.PendingAmount>=adjustedAmount)
                    {
                        amountAlloted = adjustedAmount;
                        exist.PendingAmount = exist.PendingAmount - adjustedAmount;
                    }
                    else
                    {
                        amountAlloted = exist.PendingAmount;
                        exist.PendingAmount = 0;
                    }
                    adjustedAmount = adjustedAmount - amountAlloted;
                    SwipePaymentTenderListDto payDet = new SwipePaymentTenderListDto
                    {
                        PaymentTypeId = exist.PaymentTypeId,
                        TenderedAmount = amountAlloted,
                        Amount = amountAlloted,
                        PaymentUserName = exist.PaymentUserName,
                        PaymentTypeName = exist.PaymentTypeName
                    };
                    paymentTenderList.Add(payDet);
                } while (adjustedAmount > 0);

                lst.MemberCard.SwipePaymentTenderList = paymentTenderList;

                var result = await  CreateMemberCard(lst);

                if (pendingCount == 1)
                {
                    output.AddRange(result);
                }
                pendingCount--;
            }
            return output;
        }

        protected virtual async Task<List<IdInput>> CreateMemberCard(CreateOrUpdateMemberCardInput input)
        {
            var outputReturn = new List<IdInput>();

            var existCard = await _swipecardRepo.FirstOrDefaultAsync(t => t.Id == input.MemberCard.CardRefId);

            if (input.MemberCard.CardRefId == 0 || existCard == null)
            {
                #region CanIssueCardAuotmatically

                var DepositRequiredDefault = await _settingManager.GetSettingValueAsync<bool>(
                    AppSettings.SwipeSettings.DepositRequiredDefault);
                var DepositAmountDefault = await _settingManager.GetSettingValueAsync<decimal>(
                    AppSettings.SwipeSettings.DepositAmountDefault);
                var RefundAllowedDefault = await _settingManager.GetSettingValueAsync<bool>(
                    AppSettings.SwipeSettings.RefundAllowedDefault);
                //var ExpiryDaysFromIssueDefault = await _settingManager.GetSettingValueAsync<int>(
                //		  AppSettings.SwipeSettings.ExpiryDaysFromIssueDefault);

                var defaultCardTypeId = await _settingManager.GetSettingValueAsync<int>(
                    AppSettings.SwipeSettings.ExpiryDaysFromIssueDefault);

                var newCard = new SwipeCard
                {
                    CardTypeRefId = defaultCardTypeId,
                    CardNumber = input.MemberCard.CardNumber,
                    ActiveStatus = true,
                    Balance = 0,
                    DepositRequired = DepositRequiredDefault,
                    DepositAmount = DepositAmountDefault,
                    RefundAllowed = RefundAllowedDefault,
                    ExpiryDaysFromIssue = null,
                    CanIssueWithOutMember = false,
                    CanExcludeTaxAndCharges = false,
                    IsEmployeeCard = false,
                    IsAutoRechargeCard = false,
                    DailyLimit = 0,
                    WeeklyLimit = 0,
                    MonthlyLimit = 0,
                    YearlyLimit = 0,
                    MinimumTopUpAmount = 0
                };
                await _membercardManager.CardCreateSync(newCard);
                input.MemberCard.CardRefId = newCard.Id;
                existCard = newCard;

                #endregion
            }
            else
            {
                var card = await _swipecardRepo.GetAsync(existCard.Id);
                card.ActiveStatus = true;
                await _swipecardRepo.UpdateAsync(card);
            }

            var dto = input.MemberCard.MapTo<MemberCard>();

            var transactionTime = DateTime.Now;

            dto.IssueDate = transactionTime;
            if (existCard.ExpiryDaysFromIssue == null)
                dto.ExpiryDate = null;
            else
            {
                if (existCard.ExpiryDaysFromIssue.Value == 0)
                    dto.ExpiryDate = null;
                else
                    dto.ExpiryDate = transactionTime.Date.AddDays(existCard.ExpiryDaysFromIssue.Value);
            }
            dto.IsActive = true;
            if (dto.IsPrimaryCard)
            {
                dto.PrimaryCardRefId = dto.CardRefId;
            }
            CheckErrors(await _membercardManager.CreateSync(dto));

            if (dto.MemberRefId.HasValue)
            {
                var member = await _swipememberRepo.GetAsync(dto.MemberRefId.Value);
                member.IsActive = true;
                await _swipememberRepo.UpdateAsync(member);
            }

            #region Deposit-IssueCard

            if (input.DepositCollectedAmount > 0)
            {
                int? ledgerRefId = null;
                var depositDto = new SwipeCardLedger
                {
                    MemberRefId = dto.MemberRefId,
                    EmployeeRefId = dto.EmployeeRefId,
                    CardRefId = dto.CardRefId,
                    LedgerTime = transactionTime,
                    PrimaryCardRefId = dto.PrimaryCardRefId,
                    TransactionType = (int) SwipeTransaction.DEPOSIT,
                    Credit = input.DepositCollectedAmount,
                    Debit = 0
                };

                if (existCard.RefundAllowed)
                    depositDto.Description = L("RefundableDeposit");
                else
                    depositDto.Description = L("NonRefundableDeposit");

                depositDto.ShiftRefId = input.MemberCard.ShiftRefId;

                var testId = await
                    UpdateSwipeMemberCardLedger(depositDto, input.MemberCard.CanIssueWithOutMember,
                        input.MemberCard.EmployeeRefId == null ? false : true);
                ledgerRefId = depositDto.Id;

                outputReturn.Add(new IdInput {Id = ledgerRefId.Value});

                var amountTenderAdjust = input.DepositCollectedAmount;
                if (input.MemberCard.SwipePaymentTenderList != null)
                {
                    #region PaymentDenominations

                    foreach (var payment in input.MemberCard.SwipePaymentTenderList)
                    {
                        decimal tenderAmount = 0;
                        if (amountTenderAdjust <= payment.TenderedAmount)
                        {
                            tenderAmount = amountTenderAdjust;
                            payment.TenderedAmount = payment.TenderedAmount - tenderAmount;
                            payment.Amount = payment.TenderedAmount;
                            if (payment.TenderedAmount < 0)
                            {
                                throw new UserFriendlyException(L("Tender") + "?");
                            }
                        }
                        else
                        {
                            tenderAmount = payment.TenderedAmount;
                            payment.TenderedAmount = 0;
                            payment.Amount = 0;
                        }
                        amountTenderAdjust = amountTenderAdjust - tenderAmount;
                        if (tenderAmount > 0)
                        {
                            var paymentDto = new SwipePaymentTender
                            {
                                SwipeCardLedgerRefId = ledgerRefId.Value,
                                PaymentTypeId = payment.PaymentTypeId,
                                PaymentUserName = payment.PaymentUserName,
                                TenderedAmount = tenderAmount,
                                Amount = tenderAmount
                            };
                            await _paymenttenderRepo.InsertAndGetIdAsync(paymentDto);
                        }

                        if (amountTenderAdjust == 0)
                            break;
                    }

                    #endregion
                }
            }

            #endregion

            #region TopupRefund

            if (input.TopUpAmount > 0 || input.RefundAmount > 0)
            {
                int? ledgerRefId = null;
                decimal amountTenderAdjust = 0; // input.TopUpAmount == 0 ? input.RefundAmount : input.TopUpAmount;

                var ledgerDto = new SwipeCardLedger
                {
                    MemberRefId = dto.MemberRefId,
                    EmployeeRefId = dto.EmployeeRefId,
                    CardRefId = dto.CardRefId,
                    LedgerTime = transactionTime,
                    PrimaryCardRefId = dto.PrimaryCardRefId
                };

                if (input.TopUpAmount > 0 && input.RefundAmount == 0)
                {
                    ledgerDto.TransactionType = (int) SwipeTransaction.TOPUP;
                    ledgerDto.Credit = input.TopUpAmount;
                    ledgerDto.Description = L("TopUp");
                    ledgerDto.Debit = 0;
                    amountTenderAdjust = input.TopUpAmount;
                }
                else if (input.RefundAmount > 0 && input.TopUpAmount == 0)
                {
                    ledgerDto.TransactionType = (int) SwipeTransaction.REFUND;
                    ledgerDto.Debit = input.RefundAmount;
                    ledgerDto.Credit = 0;
                    ledgerDto.Description = L("Refund");
                    amountTenderAdjust = input.RefundAmount;
                }
                else
                {
                    throw new UserFriendlyException(L("TopUp") + " " + L("Refund") + "?");
                }
                ledgerDto.ShiftRefId = input.MemberCard.ShiftRefId;

                var testId = await   UpdateSwipeMemberCardLedger(ledgerDto, input.MemberCard.CanIssueWithOutMember,input.MemberCard.EmployeeRefId == null ? false : true);
                ledgerRefId = ledgerDto.Id;
                outputReturn.Add(new IdInput {Id = ledgerRefId.Value});

                #region PaymentDenominations

                foreach (var payment in input.MemberCard.SwipePaymentTenderList)
                {
                    decimal tenderAmount = 0;
                    if (amountTenderAdjust <= payment.TenderedAmount)
                    {
                        tenderAmount = amountTenderAdjust;
                        payment.TenderedAmount = payment.TenderedAmount - tenderAmount;
                        payment.Amount = payment.TenderedAmount;
                        if (payment.TenderedAmount < 0)
                        {
                            throw new UserFriendlyException(L("Tender") + "?");
                        }
                    }
                    else
                    {
                        tenderAmount = payment.TenderedAmount;
                        payment.TenderedAmount = 0;
                        payment.Amount = 0;
                    }
                    amountTenderAdjust = amountTenderAdjust - tenderAmount;

                    if (tenderAmount > 0)
                    {
                        var paymentDto = new SwipePaymentTender
                        {
                            SwipeCardLedgerRefId = ledgerRefId.Value,
                            PaymentTypeId = payment.PaymentTypeId,
                            PaymentUserName = payment.PaymentUserName,
                            TenderedAmount = tenderAmount,
                            Amount = tenderAmount
                        };
                        await _paymenttenderRepo.InsertAndGetIdAsync(paymentDto);
                    }
                    if (amountTenderAdjust == 0)
                        break;
                }

                #endregion
            }

            #endregion

            #region DepositRefund-ReturnCard

            if (input.DepositRefundedAmount > 0)
            {
                int? ledgerRefId = null;
                var depositRefundDto = new SwipeCardLedger
                {
                    MemberRefId = dto.MemberRefId,
                    EmployeeRefId = dto.EmployeeRefId,
                    CardRefId = dto.CardRefId,
                    LedgerTime = transactionTime,
                    PrimaryCardRefId = dto.PrimaryCardRefId,
                    TransactionType = (int) SwipeTransaction.DEPOSIT_REFUND,
                    Credit = 0,
                    Debit = input.DepositRefundedAmount
                };

                depositRefundDto.Description = L("Deposit") + " " + L("Refund");

                depositRefundDto.ShiftRefId = input.MemberCard.ShiftRefId;

                var testId = await
                    UpdateSwipeMemberCardLedger(depositRefundDto, input.MemberCard.CanIssueWithOutMember,
                        input.MemberCard.EmployeeRefId == null ? false : true);
                ledgerRefId = depositRefundDto.Id;
                outputReturn.Add(new IdInput {Id = ledgerRefId.Value});

                var amountTenderAdjust = input.DepositRefundedAmount;

                #region PaymentDenominations

                foreach (var payment in input.MemberCard.SwipePaymentTenderList)
                {
                    decimal tenderAmount = 0;
                    if (amountTenderAdjust <= payment.TenderedAmount)
                    {
                        tenderAmount = amountTenderAdjust;
                        payment.TenderedAmount = payment.TenderedAmount - tenderAmount;
                        payment.Amount = payment.TenderedAmount;
                        if (payment.TenderedAmount < 0)
                        {
                            throw new UserFriendlyException(L("Tender") + "?");
                        }
                    }
                    else
                    {
                        tenderAmount = payment.TenderedAmount;
                        payment.TenderedAmount = 0;
                        payment.Amount = 0;
                    }
                    amountTenderAdjust = amountTenderAdjust - tenderAmount;

                    if (tenderAmount > 0)
                    {
                        var paymentDto = new SwipePaymentTender
                        {
                            SwipeCardLedgerRefId = ledgerRefId.Value,
                            PaymentTypeId = payment.PaymentTypeId,
                            PaymentUserName = payment.PaymentUserName,
                            TenderedAmount = tenderAmount,
                            Amount = tenderAmount
                        };
                        await _paymenttenderRepo.InsertAndGetIdAsync(paymentDto);
                    }
                    if (amountTenderAdjust == 0)
                        break;
                }

                #endregion
            }

            #endregion

            return outputReturn;
        }

        public async Task<bool> ExistAllServerLevelBusinessRules(CreateOrUpdateMemberCardInput input)
        {
            var memberCard = input.MemberCard;

            var memberExists = await _swipememberRepo.FirstOrDefaultAsync(t => t.Id == input.MemberCard.MemberRefId);

            if (input.MemberCard.CanIssueWithOutMember == false && input.MemberCard.IsEmployeeCard == false)
            {
                if (memberExists == null)
                {
                    throw new UserFriendlyException(L("MemberNotFound", memberCard.MemberRefId));
                }
            }
            else
            {
                input.MemberCard.MemberRefId = null;
            }

            var cardExists = await _swipecardRepo.FirstOrDefaultAsync(t => t.Id == memberCard.CardRefId);
            if (cardExists == null)
            {
                throw new UserFriendlyException(L("SwipeCardNotFound", memberCard.CardRefId));
            }

            var alreadyIssued =
                await _membercardRepo.FirstOrDefaultAsync(t => t.CardRefId == memberCard.CardRefId && t.IsActive);
            if (alreadyIssued != null)
            {
                if (alreadyIssued.MemberRefId != input.MemberCard.MemberRefId)
                {
                    throw new UserFriendlyException(L("CardNumberAlreadyIssuedToMember_F", cardExists.CardNumber,
                        memberExists.MemberCode + " - " + memberExists.Name));
                }
                input.MemberCard.Id = alreadyIssued.Id;
            }

            if (input.TopUpAmount < 0)
            {
                throw new UserFriendlyException(L("TopUpAmountCanNotBeNegative", input.TopUpAmount));
            }

            if (input.RefundAmount < 0)
            {
                throw new UserFriendlyException(L("RefundAmountCanNotBeNegative", input.RefundAmount));
            }

            #region Check Minimum Topup Amount
            if (input.TopUpAmount>0)
            {
                if (cardExists.MinimumTopUpAmount > 0)
                {
                    if ( (input.TopUpAmount) < (cardExists.MinimumTopUpAmount) )
                    {
                        throw new UserFriendlyException(L("TopUpAmountCanNotBeLessThanMinimumTopUpAmount", input.TopUpAmount, 
                            cardExists.MinimumTopUpAmount));
                    }
                }
            }
            #endregion

            if (input.TopUpAmount == 0 && input.RefundAmount == 0 && memberCard.IsPrimaryCard &&
                input.DepositRefundedAmount == 0 && input.DepositCollectedAmount == 0)
            {
                if (cardExists.DailyLimit > 0 || cardExists.WeeklyLimit > 0 || cardExists.MonthlyLimit > 0 ||
                    cardExists.YearlyLimit > 0)
                {
                    //Do nothing
                }
                else
                {
                    throw new UserFriendlyException(L("TopUp") + " " + L("Refund") + " " + L("?"));
                }
            }

            #region DepositAmountVerify

            if (memberCard.Id.HasValue)
            {
                var exist = await _membercardRepo.FirstOrDefaultAsync(t => t.IsActive && t.Id == memberCard.Id);
                if (exist == null)
                {
                    throw new UserFriendlyException(L("MemberCardDoesNotExist"));
                }
                if (exist != null)
                {
                    if (exist.DepositAmount != memberCard.DepositAmount)
                    {
                        throw new UserFriendlyException(L("ProblemWithDeposit"));
                    }
                }
            }

            #endregion

            if (memberCard.IsPrimaryCard == false)
            {
                var isPrimaryCardExists =
                    await _swipecardRepo.FirstOrDefaultAsync(t => t.Id == memberCard.PrimaryCardRefId);
                if (isPrimaryCardExists == null)
                {
                    throw new UserFriendlyException(L("PrimaryCardReferenceMissing"));
                }
                if (isPrimaryCardExists.ActiveStatus == false)
                {
                    throw new UserFriendlyException(L("PrimaryCardIsNotInActiveStatus", isPrimaryCardExists.CardNumber));
                }
                var newSupCard = await _swipecardRepo.FirstOrDefaultAsync(t => t.Id == memberCard.CardRefId);
                if (isPrimaryCardExists.CardTypeRefId != newSupCard.CardTypeRefId)
                {
                    throw new UserFriendlyException(L("PrimaryCardAndSupplementaryCardShouldBeSameCardType",
                        isPrimaryCardExists.CardNumber));
                }
            }

            return true;
        }

        public async Task<decimal> GetCardBalance(SwipeCard card)
        {
            var cardLimitType = "";
            decimal cardBalance = 0;
            decimal cardLimit = 0;
            var fromDate = DateTime.Today;
            var toDate = DateTime.Now;

            if (card.Balance > 0)
            {
                cardBalance = card.Balance;
            }
            else if (card.DailyLimit > 0)
            {
                cardLimitType = "D";
                fromDate = DateTime.Today;
                cardLimit = card.DailyLimit;
            }
            else if (card.WeeklyLimit > 0)
            {
                cardLimitType = "W";
                fromDate = DateTime.Today.AddDays(-(int) DateTime.Today.DayOfWeek);
                cardLimit = card.WeeklyLimit;
            }
            else if (card.MonthlyLimit > 0)
            {
                cardLimitType = "M";
                fromDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                cardLimit = card.MonthlyLimit;
            }
            else if (card.YearlyLimit > 0)
            {
                cardLimitType = "Y";
                fromDate = new DateTime(DateTime.Today.Year, 1, 1);
                cardLimit = card.YearlyLimit;
            }


            if (cardLimitType != "")
            {
                var membercard =
                    await
                        _membercardRepo.GetAll()
                            .Where(
                                t =>
                                    (t.PrimaryCardRefId == card.Id || (t.IsPrimaryCard && t.CardRefId == card.Id)) &&
                                    t.IsActive)
                            .ToListAsync();

                if (membercard == null || membercard.Count == 0)
                {
                    return 0;
                }
                var memberid = membercard[0].MemberRefId;
                var cardRefIds = membercard.Select(t => t.CardRefId).ToArray();
                var transacionDays =
                    await
                        _swipeledgerRepo.GetAll()
                            .Where(
                                t =>
                                    cardRefIds.Contains(t.CardRefId) && t.MemberRefId == memberid &&
                                    t.LedgerTime >= fromDate && t.LedgerTime <= toDate)
                            .ToListAsync();

                var alreadyUsed = transacionDays.Sum(t => t.Debit);

                cardBalance = cardLimit - alreadyUsed;
                if (cardBalance < 0)
                    cardBalance = 0;
            }

            return cardBalance;
        }

        public async Task<IdInput> UpdateSwipeMemberCardLedger(SwipeCardLedger input, bool canIssueWithOutMember,
            bool isEmployeeCard)
        {
            var memberName = L("Guest");
            int? memberId = null;
            var transactionTime = input.LedgerTime;

            #region PreVerify_Rules

            var card = await _swipecardRepo.FirstOrDefaultAsync(t => t.Id == input.PrimaryCardRefId);
            if (card == null)
            {
                throw new UserFriendlyException(L("CardIdNotExist", input.PrimaryCardRefId));
            }
            if (card.ActiveStatus == false)
            {
                throw new UserFriendlyException(L("CardIsNotInActiveStatus", card.CardNumber));
            }

            if (canIssueWithOutMember == false && isEmployeeCard == false)
            {
                var member = await _swipememberRepo.FirstOrDefaultAsync(t => t.Id == input.MemberRefId);
                if (member == null)
                {
                    throw new UserFriendlyException(L("MemberIdNotExist", input.MemberRefId));
                }
                if (member.IsActive == false)
                {
                    throw new UserFriendlyException(L("MemberIsNotInActiveStatus", member.MemberCode + ' ' + member.Name));
                }
                memberName = member.Name;
                memberId = member.Id;
            }

            #endregion

            #region NewRecordAddition

            var newOpenBalance = 0.0m;

            var maxRecs =
                _swipeledgerRepo.GetAll()
                    .Where(
                        t =>
                            t.MemberRefId == input.MemberRefId && t.PrimaryCardRefId == input.PrimaryCardRefId &&
                            t.EmployeeRefId == input.EmployeeRefId);

            if (maxRecs.Count() == 0)
            {
                newOpenBalance = 0.0m;
            }
            else
            {
                if (card.DailyLimit > 0 || card.WeeklyLimit > 0 || card.MonthlyLimit > 0 || card.YearlyLimit > 0)
                {
                    if (input.TransactionType == (int) SwipeTransaction.DEPOSIT_REFUND)
                    {
                        newOpenBalance = input.Debit;
                    }
                    else
                    {
                        newOpenBalance = 0;
                        var lastId = maxRecs.Max(t => t.Id);
                        var lastDaysRecord =
                            await
                                _swipeledgerRepo.FirstOrDefaultAsync(
                                    l => l.Id == lastId && l.TransactionType != (int) SwipeTransaction.DEPOSIT);
                        if (lastDaysRecord != null)
                        {
                            newOpenBalance = lastDaysRecord.ClosingBalance;
                        }
                    }
                }
                else
                {
                    var lastId = maxRecs.Max(t => t.Id);
                    var lastDaysRecord = await _swipeledgerRepo.FirstOrDefaultAsync(l => l.Id == lastId);
                    newOpenBalance = lastDaysRecord.ClosingBalance;
                }
            }

            input.OpeningBalance = newOpenBalance;
            input.ClosingBalance = (input.OpeningBalance + input.Credit) - (input.Debit);

            #region CheckNegativeErrors   

            if (input.ClosingBalance < 0)
            {
                if (card.DailyLimit > 0 || card.WeeklyLimit > 0 || card.MonthlyLimit > 0 || card.YearlyLimit > 0)
                {
                    #region AutoTopup

                    int? ledgerRefId = null;
                    //decimal amountTenderAdjust = 0;// input.TopUpAmount == 0 ? input.RefundAmount : input.TopUpAmount;
                    //var dto = await _membercardRepo.FirstOrDefaultAsync(t=>t.CardRefId==card.Id && )
                    var ledgerDto = new SwipeCardLedger
                    {
                        MemberRefId = input.MemberRefId,
                        EmployeeRefId = input.EmployeeRefId,
                        CardRefId = card.Id,
                        LedgerTime = transactionTime,
                        PrimaryCardRefId = card.Id
                        // dto.PrimaryCardRefId == null ? dto.CardRefId : dto.PrimaryCardRefId.Value,
                    };

                    //	var autotopupmembercard = await _membercardRepo.FirstOrDefaultAsync(t => t.CardRefId == card.Id && t.IsActive == true);
                    //	if(autotopupmembercard!=null)
                    //{
                    //	if(autotopupmembercard!=null)
                    //	{
                    //		if(autotopupmembercard.PrimaryCardRefId.HasValue)
                    //		{
                    //			ledgerDto.CardRefId = autotopupmembercard.PrimaryCardRefId.Value;
                    //		}
                    //	}
                    //}

                    ledgerDto.TransactionType = (int) SwipeTransaction.TOPUP;
                    ledgerDto.Credit = input.Debit;
                    ledgerDto.Description = L("Auto") + " " + L("TopUp");
                    ledgerDto.Debit = 0;

                    await UpdateSwipeMemberCardLedger(ledgerDto, canIssueWithOutMember, isEmployeeCard);
                    ledgerRefId = ledgerDto.Id;

                    input.OpeningBalance = input.Debit;
                    input.ClosingBalance = (input.OpeningBalance + input.Credit) - (input.Debit);

                    #endregion
                }
                else
                {
                    throw new UserFriendlyException(L("CardBalanceShouldNotBeNegative", card.CardNumber, memberName));
                }
            }

            if (input.Debit < 0)
            {
                throw new UserFriendlyException(L("DebitShouldNotBeNegative", card.CardNumber, memberName));
            }

            if (input.Credit < 0)
            {
                throw new UserFriendlyException(L("CreditShouldNotBeNegative", card.CardNumber, memberName));
            }

            if (input.OpeningBalance < 0)
            {
                throw new UserFriendlyException(L("CardBalanceShouldNotBeNegative", card.CardNumber, memberName));
            }

            #endregion

            var tranId = 0;

            try
            {
                input.LedgerTime = DateTime.Now;
                tranId = await _swipeledgerRepo.InsertAndGetIdAsync(input);
            }
            catch (Exception exception)
            {
                var messa = exception.Message;
            }

            #endregion

            #region SetBalanceOfCards

            if (card.DailyLimit > 0 || card.WeeklyLimit > 0 || card.MonthlyLimit > 0 || card.YearlyLimit > 0)
            {
                //Do nothing
            }
            else
            {
                if (input.TransactionType == (int) SwipeTransaction.TOPUP ||
                    input.TransactionType == (int) SwipeTransaction.REFUND ||
                    input.TransactionType == (int) SwipeTransaction.REDEEM)
                {
                    var ret = await ApiGetSwipeCardCurrentDepositBalance(new SwipeCardListDto {Id = card.Id});
                    var depositAmount = ret.TotalDeposit;

                    var particularCard = await _swipecardRepo.GetAsync(card.Id);
                    if (input.ClosingBalance == 0)
                        particularCard.Balance = 0;
                    else
                        particularCard.Balance = input.ClosingBalance - depositAmount;

                    if (particularCard.Balance < 0)
                    {
                        throw new UserFriendlyException(L("NegativeBalanceNotAllowed"));
                    }

                    await _swipecardRepo.UpdateAsync(particularCard);
                }
            }

            #endregion

            if (input.TransactionType == (int) SwipeTransaction.DEPOSIT_REFUND)
            {
                List<MemberCard> lstMemberCards;

                #region RefundUpdation

                if (input.MemberRefId.HasValue)
                {
                    lstMemberCards =
                        await
                            _membercardRepo.GetAll()
                                .Where(
                                    t =>
                                        t.IsActive && t.MemberRefId == input.MemberRefId &&
                                        (t.PrimaryCardRefId == input.PrimaryCardRefId || t.IsPrimaryCard))
                                .ToListAsync();
                }
                else
                {
                    if (input.EmployeeRefId.HasValue)
                    {
                        lstMemberCards =
                            await
                                _membercardRepo.GetAll()
                                    .Where(
                                        t =>
                                            t.IsActive && t.MemberRefId == input.MemberRefId &&
                                            (t.CardRefId == input.CardRefId ||
                                             (t.PrimaryCardRefId == input.PrimaryCardRefId || t.IsPrimaryCard)))
                                    .ToListAsync();
                        lstMemberCards =
                            await
                                _membercardRepo.GetAll()
                                    .Where(
                                        t =>
                                            (t.PrimaryCardRefId == card.Id ||
                                             (t.IsPrimaryCard && t.CardRefId == card.Id)) && t.IsActive)
                                    .ToListAsync();
                    }
                    else
                    {
                        lstMemberCards =
                            await
                                _membercardRepo.GetAll()
                                    .Where(
                                        t =>
                                            t.IsActive && t.MemberRefId == input.MemberRefId &&
                                            t.CardRefId == input.CardRefId && t.IsPrimaryCard)
                                    .ToListAsync();
                    }
                }

                foreach (var lst in lstMemberCards)
                {
                    var refmc = await _membercardRepo.GetAsync(lst.Id);
                    refmc.IsActive = false;
                    refmc.ExpiryDate = null;
                    refmc.ReturnDate = DateTime.Now;
                    refmc.DepositAmount = 0;
                    await _membercardRepo.UpdateAsync(refmc);

                    var refCard = await _swipecardRepo.GetAsync(lst.CardRefId);
                    refCard.ActiveStatus = false;
                    await _swipecardRepo.UpdateAsync(refCard);
                }

                if (input.MemberRefId.HasValue)
                {
                    var refMember = await _swipememberRepo.GetAsync(input.MemberRefId.Value);
                    refMember.IsActive = false;
                    await _swipememberRepo.UpdateAsync(refMember);
                }

                #endregion
            }
            else
            {
                #region MemberCardUpdation

                var memberCard =
                    await
                        _membercardRepo.FirstOrDefaultAsync(
                            t => t.IsActive && t.MemberRefId == memberId && t.CardRefId == card.Id);
                if (memberCard == null)
                {
                    throw new UserFriendlyException(L("MemberCardNotInActiveStatus"));
                }
                var memberDto = await _membercardRepo.GetAsync(memberCard.Id);
                memberDto.IsActive = true;
                //memberDto.ExpiryDate = input.LedgerTime.Date.AddDays(card.ExpiryDaysFromIssue.Value);
                if (card.ExpiryDaysFromIssue == null)
                    memberDto.ExpiryDate = null;
                else
                {
                    if (card.ExpiryDaysFromIssue.Value == 0)
                        memberDto.ExpiryDate = null;
                }
                await _membercardRepo.UpdateAsync(memberDto);

                #endregion
            }


            return new IdInput {Id = tranId}; //  Non Zero is Success , 0 For Failure
        }
    }
}