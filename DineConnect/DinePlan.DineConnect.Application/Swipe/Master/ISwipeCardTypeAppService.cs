﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Swipe.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Swipe.Master
{
	public interface ISwipeCardTypeAppService : IApplicationService
	{
		Task<PagedResultOutput<SwipeCardTypeListDto>> GetAll(GetSwipeCardTypeInput inputDto);
		Task<FileDto> GetAllToExcel();
		Task<GetSwipeCardTypeForEditOutput> GetSwipeCardTypeForEdit(NullableIdInput nullableIdInput);
		Task<IdInput> CreateOrUpdateSwipeCardType(CreateOrUpdateSwipeCardTypeInput input);
		Task DeleteSwipeCardType(IdInput input);

		Task<ListResultOutput<SwipeCardTypeListDto>> GetNames();
	}
}

