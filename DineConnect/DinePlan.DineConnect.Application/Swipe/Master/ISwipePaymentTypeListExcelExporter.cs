﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Swipe.Transaction.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Swipe.Master.Dtos;

namespace DinePlan.DineConnect.Swipe.Transaction
{
    public interface ISwipePaymentTypeListExcelExporter
    {
        FileDto ExportToFile(List<SwipePaymentTypeListDto> dtos);
    }
}