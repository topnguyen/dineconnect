﻿using System;
using System.Collections.Generic;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Threading;
using DinePlan.DineConnect.Addon;
using DinePlan.DineConnect.Addons.Odoo;
using DinePlan.DineConnect.Connect.DayClose.Dto;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House;
using DinePlan.DineConnect.House.Master;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.HouseReportName;

namespace DinePlan.DineConnect.Job.House
{
    public class HouseReportBackgroundJob : BackgroundJob<HouseReportBackgroundJobIdJobArgs>, ITransientDependency
    {
        private IMaterialAppService _materialAppService;
        private IHouseReportAppService _houseReportAppService;
        private readonly IReportBackgroundAppService _reportAppService;

        public HouseReportBackgroundJob(IMaterialAppService materialAppService,
            IHouseReportAppService houseReportAppService,
            IReportBackgroundAppService reportAppService)
        {
            _materialAppService = materialAppService;
            _houseReportAppService = houseReportAppService;
            _reportAppService = reportAppService;
        }

        [UnitOfWork]
        public override void Execute(HouseReportBackgroundJobIdJobArgs args)
        {
            if (args != null)
            {
                FileDto fileDto = null;
                switch (args.ReportName)
                {
                    case HouseReportNames.CLOSINGSTOCK_GROUP_CATEGORY_MATERIAL_VALUE:

                        fileDto = AsyncHelper.RunSync(() =>
                        _houseReportAppService.GetClosingStockReportGroupOrCategoryOrMaterialWiseForGivenInputToExcel(new ClosingStockExportGroupCategoryMaterialDto
                        {
                            StartDate = args.CloseReportInput.StartDate,
                            EndDate = args.CloseReportInput.EndDate,
                            LocationGroup = args.CloseReportInput.LocationGroup,
                            GroupList = args.CloseReportInput.GroupList,
                            CategoryList = args.CloseReportInput.CategoryList,
                            MaterialList = args.CloseReportInput.MaterialList,
                            ExportMaterialAlso = args.CloseReportInput.ExportMaterialAlso,
                            RunInBackGround = args.CloseReportInput.RunInBackGround,
                            UserId = args.CloseReportInput.UserId,
                            TenantId = args.TenantId
                        })
                        );
                        break;
                    case HouseReportNames.MATERIAL_TRACK_INVOICE_INTERTRANSFER:

                        fileDto = AsyncHelper.RunSync(() =>
                        _houseReportAppService.GetMaterialTrackReceivedDetailReportMaterialWiseForGivenInputToExcel(new ClosingStockExportGroupCategoryMaterialDto
                        {
                            StartDate = args.CloseReportInput.StartDate,
                            EndDate = args.CloseReportInput.EndDate,
                            LocationGroup = args.CloseReportInput.LocationGroup,
                            GroupList = args.CloseReportInput.GroupList,
                            CategoryList = args.CloseReportInput.CategoryList,
                            MaterialList = args.CloseReportInput.MaterialList,
                            ExportMaterialAlso = args.CloseReportInput.ExportMaterialAlso,
                            RunInBackGround = args.CloseReportInput.RunInBackGround,
                            UserId = args.CloseReportInput.UserId,
                            TenantId = args.TenantId
                        })
                        );
                        break;

                    default:
                        break;
                }

                if (fileDto != null)
                {
                    AsyncHelper.RunSync(() =>
                        _reportAppService.UpdateFileOutputCompleted(fileDto, args.BackGroundId));
                }
            }
        }
    }

    [Serializable]
    public class HouseReportBackgroundJobIdJobArgs
    {
        public string ReportName { get; set; }
        public int BackGroundId { get; set; }
        public ClosingStockExportGroupCategoryMaterialDto CloseReportInput { get; set; }
        public int TenantId { get; set; }
        public long UserId { get; set; }
    }
}
