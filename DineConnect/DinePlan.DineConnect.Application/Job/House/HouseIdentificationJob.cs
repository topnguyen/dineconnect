﻿using System;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using DinePlan.DineConnect.Addon;
using DinePlan.DineConnect.Addons.Odoo;
using DinePlan.DineConnect.Addons.QuickBooks;
using DinePlan.DineConnect.Addons.Xero;
using DinePlan.DineConnect.Addons.ZohoBooks;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Engage;
using DinePlan.DineConnect.House;

namespace DinePlan.DineConnect.Job.House
{
    public class HouseIdentificationJob : BackgroundJob<HouseIdJobArgs>, ITransientDependency
    {
        private IOdooAppService _odooService;
        private IXeroAppService _xeroAppService;
        private IQuickBooksAppService _quickBooksAppService;
        private IZohoBooksAppService _zohoBooksAppService;
        private IRepository<ConnectAddOn> _addons;


        public HouseIdentificationJob(IOdooAppService odooService, 
            IXeroAppService xeroAppService,
            IQuickBooksAppService qbAppService,
            IZohoBooksAppService zohoBooksAppService,
            IRepository<ConnectAddOn> addons)
        {
            _zohoBooksAppService = zohoBooksAppService;
            _quickBooksAppService = qbAppService;
            _odooService = odooService;
            _xeroAppService = xeroAppService;
            _addons = addons;

        }

        [UnitOfWork]
        public override void Execute(HouseIdJobArgs args)
        {
            var odooAvailable = _addons.FirstOrDefault(a => a.Name.Equals("Odoo"));
            if (odooAvailable != null)
            {
                if (args.SupplierId > 0)
                    _odooService.SyncSupplier(args.SupplierId);

                if (args.MaterialId > 0)
                    _odooService.SyncMaterial(args.MaterialId);

                if (args.TaxId > 0)
                    _odooService.SyncTax(args.TaxId);

                if (args.InvoiceId > 0)
                    _odooService.SyncInvoice(args.InvoiceId);
            }


           
            var qbAvailable = _addons.FirstOrDefault(a => a.Name.Equals("QuickBooks"));

            if (qbAvailable != null)
            {
                if (args.MaterialId > 0)
                    _quickBooksAppService.SyncItem(args.MaterialId);

                /*
                 * TODO: Implement more crud
                 * 
                 * 
                if (args.SupplierId > 0)
                    _xeroAppService.SyncSuppliers();

                */
            }

            var zohoBooksAvailable = _addons.FirstOrDefault(a => a.Name.Equals("ZohoBooks"));

            if (zohoBooksAvailable != null)
            {
                if (args.MaterialId > 0)
                    _zohoBooksAppService.SyncItem(args.MaterialId);

                /*
                 * TODO: Implement more crud
                 * 
                 * 
                if (args.SupplierId > 0)
                    _xeroAppService.SyncSuppliers();

                */
            }

        }
    }

    [Serializable]
    public class HouseIdJobArgs
    {
        public int SupplierId { get; set; }
        public int MaterialId { get; set; }
        public int TaxId { get; set; }
        public int InvoiceId { get; set; }

    }
}
