﻿using System;
using System.Collections.Generic;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Threading;
using DinePlan.DineConnect.Addon;
using DinePlan.DineConnect.Addons.Odoo;
using DinePlan.DineConnect.Connect.DayClose.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.House;
using DinePlan.DineConnect.House.Master;
using DinePlan.DineConnect.House.Master.Dtos;

namespace DinePlan.DineConnect.Job.House
{
    public class HouseDayCloseJob : BackgroundJob<HouseDayCloseIdJobArgs>, ITransientDependency
    {
        private IMaterialAppService _materialAppService;


        public HouseDayCloseJob(IMaterialAppService materialAppService)
        {
            _materialAppService = materialAppService;

        }

        [UnitOfWork]
        public override void Execute(HouseDayCloseIdJobArgs args)
        {
            var output = AsyncHelper.RunSync(() =>
            _materialAppService.VerifyWipeOutStockForCloseDay(new DineConnect.Connect.DayClose.Dto.DayCloseDto
            {
                LocationRefId = args.LocationRefId,
                TransactionDate = args.TransactionDate,
                LedgerAffect = args.LedgerAffect,
                Remarks = args.Remarks,
                SkipTransactionDays = args.SkipTransactionDays,
                DayCloseWastageData = args.DayCloseWastageData,
                ResetFlag = args.ResetFlag,
                DoesDayCloseRequiredInBackGround = true,
                RunInBackGround = false,
                AllItemSalesDto = args.AllItemSalesDto,
                TicketStatsDto = args.TicketStatsDto,
                DayCloseSyncReportOutputDtos = args.DayCloseSyncReportOutputDtos,
                UnitConversionListDtos = args.UnitConversionListDtos,
                NegativeStockAccepted = args.NegativeStockAccepted,
                UserId = args.UserId,
                TenantId = args.TenantId
            })
            );
            var t = output;
        }
    }

    [Serializable]
    public class HouseDayCloseIdJobArgs
    {
        public int LocationRefId { get; set; }
        public DateTime TransactionDate { get; set; }
        public bool LedgerAffect { get; set; }
        public string Remarks { get; set; }
        public bool ResetFlag { get; set; }
        public List<MaterialSalesWithWipeOutCloseDayDto> DayCloseWastageData;
        public bool SkipTransactionDays { get; set; }
        public AllItemSalesDto AllItemSalesDto { get; set; }
        public TicketStatsDto TicketStatsDto { get; set; }
        public bool NegativeStockAccepted { get; set; }
        public int? UserId { get; set; }
        public int TenantId { get; set; }
        public List<UnitConversionListDto> UnitConversionListDtos { get; set; }
        public List<DayCloseSyncReportOutputDto> DayCloseSyncReportOutputDtos { get; set; }
    }
}
