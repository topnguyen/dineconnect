﻿using System;
using System.Collections.Generic;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Threading;
using DinePlan.DineConnect.Addon;
using DinePlan.DineConnect.Addons.Odoo;
using DinePlan.DineConnect.Connect.DayClose.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.House;
using DinePlan.DineConnect.House.Master;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.House.MaterialStock;

namespace DinePlan.DineConnect.Job.House
{
    public class HouseLocationWiseMaterialInitialiseJob : BackgroundJob<HouseLocationWiseMaterialInitialiseJobArgs>, ITransientDependency
    {
        private IMaterialStockAppService _materialStockAppService;

        public HouseLocationWiseMaterialInitialiseJob(IMaterialStockAppService materialStockAppService)
        {
            _materialStockAppService = materialStockAppService;

        }

        [UnitOfWork]
        public override void Execute(HouseLocationWiseMaterialInitialiseJobArgs args)
        {
            AsyncHelper.RunSync(() =>
            _materialStockAppService.InitialiseLocationWiseMaterials(new HouseLocationWiseMaterialInitialiseDto
            {
                LocationRefId = args.LocationRefId,
                MaterialRefId = args.MaterialRefId,
                UserId = args.UserId,
                TenantId = args.TenantId
            })
            );
        }
    }

    [Serializable]
    public class HouseLocationWiseMaterialInitialiseJobArgs
    {
        public int? LocationRefId { get; set; }
        public long? UserId { get; set; }
        public int TenantId { get; set; }
        public int? MaterialRefId { get; set; }
    }
}
