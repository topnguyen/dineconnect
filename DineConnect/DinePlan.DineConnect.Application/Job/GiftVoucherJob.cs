﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Connect;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Engage;

namespace DinePlan.DineConnect.Job
{
    public class GiftVoucherJob : BackgroundJob<GiftVoucherJobArgs>, ITransientDependency
    {
        private readonly IEngageMessageSender _emailer;

        public GiftVoucherJob(IEngageMessageSender emailer)
        {
            _emailer = emailer;
        }
        public override void Execute(GiftVoucherJobArgs args)
        {
            _emailer.SendMemberVoucher(args.MemberId, args.Voucher);
        }
    }

    [Serializable]
    public class GiftVoucherJobArgs
    {
        public int MemberId { get; set; }
        public string Voucher{ get; set; }


    }
}
