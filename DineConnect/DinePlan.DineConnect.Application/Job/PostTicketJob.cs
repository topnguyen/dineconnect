﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Threading;
using Castle.Core.Logging;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Job
{
    public class PostTicketJob : BackgroundJob<PostTicketJobInputArgs>, ITransientDependency
    {
        private readonly IPostAppService _postAppService;
        private readonly ILogger _logger;
        public PostTicketJob(IPostAppService postAppService
            , ILogger logger)
        {
            _postAppService = postAppService;
            _logger = logger;
        }
        [UnitOfWork]
        public override void Execute(PostTicketJobInputArgs args)
        {
            if (args.PostDataInput != null)
            {
                try
                {
                    args.PostDataInput.Processed = false; // Get the not processed only
                    var postData = AsyncHelper.RunSync(() => _postAppService.PullContents(args.PostDataInput));
                    foreach (var postDataItem in postData.Items)
                    {
                        _postAppService.SyncTicket(postDataItem.Id);
                    }
                }
                catch (Exception e)
                {
                    _logger.Error("Exception when process PostTicketJob", e);
                }
            }
        }
    }

    public class PostTicketJobInputArgs
    {
        public int TenantId { get; set; }
        public long UserId { get; set; }

        public PostDataInput PostDataInput;
    }

}
