﻿using System;
using System.Collections.Generic;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Threading;
using DinePlan.DineConnect.Addon;
using DinePlan.DineConnect.Addons.Odoo;
using DinePlan.DineConnect.Cluster.Master;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using DinePlan.DineConnect.Cluster.Master.Implementation;
using DinePlan.DineConnect.Connect.DayClose.Dto;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House;
using DinePlan.DineConnect.House.Master;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.HouseReportName;

namespace DinePlan.DineConnect.Job.Cluster
{
    public class DelAggItemBackgroundImport : BackgroundJob<DelAggItemBackgroundImportIdJobArgs>, ITransientDependency
    {
        private readonly IDelAggItemAppService _delAggItemAppService;
        private readonly IReportBackgroundAppService _reportAppService;

        public DelAggItemBackgroundImport(IDelAggItemAppService delAggItemAppService,
            IReportBackgroundAppService reportAppService)
        {
            _delAggItemAppService = delAggItemAppService;
            _reportAppService = reportAppService;
        }

        [UnitOfWork]
        public override void Execute(DelAggItemBackgroundImportIdJobArgs args)
        {
            if (args != null)
            {
                FileDto fileDto = new FileDto
                {
                    BackGroudId = args.BackGroundId
                };
                AsyncHelper.RunSync(() => _delAggItemAppService.BulkDelAggItemImport(new BulkDelAggItemImportDtos
                {
                    BackGroundId = args.BackGroundId,
                    TenantId = args.TenantId,
                    UserId = args.UserId,
                    CreateOrUpdateDelAggItemInputs = args.BulkList
                }));

                AsyncHelper.RunSync(() =>
                       _reportAppService.UpdateFileOutputCompleted(fileDto, args.BackGroundId));
            }


        }
    }

    [Serializable]
    public class DelAggItemBackgroundImportIdJobArgs
    {
        public int BackGroundId { get; set; }
        public int TenantId { get; set; }
        public long UserId { get; set; }
        public List<CreateOrUpdateDelAggItemInput> BulkList { get; set; }
        public DelAggItemBackgroundImportIdJobArgs()
        {
            BulkList = new List<CreateOrUpdateDelAggItemInput>();
        }
    }
}
