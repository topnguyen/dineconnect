﻿using System;
using System.Collections.Generic;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Threading;
using DinePlan.DineConnect.Addon;
using DinePlan.DineConnect.Addons.Odoo;
using DinePlan.DineConnect.Cluster.Master;
using DinePlan.DineConnect.Cluster.Master.Dtos;
using DinePlan.DineConnect.Cluster.Master.Implementation;
using DinePlan.DineConnect.Connect.DayClose.Dto;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House;
using DinePlan.DineConnect.House.Master;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.HouseReportName;

namespace DinePlan.DineConnect.Job.Cluster
{
    public class DelAggItemBackgroundClone : BackgroundJob<DelAggItemBackgroundCloneIdJobArgs>, ITransientDependency
    {
        private readonly IDelAggItemAppService _delAggItemAppService;
        private readonly IReportBackgroundAppService _reportAppService;

        public DelAggItemBackgroundClone(IDelAggItemAppService delAggItemAppService,
            IReportBackgroundAppService reportAppService)
        {
            _delAggItemAppService = delAggItemAppService;
            _reportAppService = reportAppService;
        }

        [UnitOfWork]
        public override void Execute(DelAggItemBackgroundCloneIdJobArgs args)
        {
            if (args != null)
            {
                FileDto fileDto = new FileDto
                {
                    BackGroudId = args.BackGroundId
                };
                AsyncHelper.RunSync(() => _delAggItemAppService.CloneItems(new CloneDelAggItemDtos
                {
                    BackGroundId = args.BackGroundId,
                    TenantId = args.TenantId,
                    UserId = args.UserId,
                }));

                AsyncHelper.RunSync(() =>
                       _reportAppService.SetBackGroundJobCompleted(args.BackGroundId));
            }
        }
    }

    [Serializable]
    public class DelAggItemBackgroundCloneIdJobArgs
    {
        public int BackGroundId { get; set; }
        public int TenantId { get; set; }
        public long UserId { get; set; }
        public DelAggItemBackgroundCloneIdJobArgs()
        {
        }
    }
}
