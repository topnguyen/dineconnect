﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.BackgroundJobs;
using Abp.Dependency;

namespace DinePlan.DineConnect.Job
{
    public class SendReportJob : BackgroundJob<SendReportJobArgs>, ITransientDependency
    {
        public override void Execute(SendReportJobArgs args)
        {

        }
    }

    [Serializable]
    public class SendReportJobArgs
    {
        public string Input { get; set; }
        public string[] Emails { get; set; }
        public string Report { get; set; }

    }
}
