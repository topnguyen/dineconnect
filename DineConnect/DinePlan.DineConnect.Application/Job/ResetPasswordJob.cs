﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Authorization.Users.Dto;
using DinePlan.DineConnect.Connect;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Engage;
using DinePlan.DineConnect.Template;

namespace DinePlan.DineConnect.Job
{
    public class ResetPasswordJob : BackgroundJob<ResetPasswordJobArgs>, ITransientDependency
    {
        private readonly IEngageMessageSender _emailer;

        public ResetPasswordJob(IEngageMessageSender emailer)
        {
            _emailer = emailer;
        }
        public override void Execute(ResetPasswordJobArgs args)
        {
            _emailer.SendResetPassword(args.EmailTemplate, args.UserName,args.NewPassword,args.Sender,args.EmailAddress);
        }
    }

    public class RandomPasswordJob : BackgroundJob<ResetPasswordJobArgs>, ITransientDependency
    {
        private readonly IEngageMessageSender _emailer;

        public RandomPasswordJob(IEngageMessageSender emailer)
        {
            _emailer = emailer;
        }
        public override void Execute(ResetPasswordJobArgs args)
        {
            _emailer.SendRandomPassword(args.UserName, args.NewPassword, args.EmailAddress, args.Sender, args.TenantId);
        }
    }

    [Serializable]
    public class ResetPasswordJobArgs
    {
        public EmailTemplate EmailTemplate { get; set; }
        public string UserName { get; set; }
        public string NewPassword { get; set; }
        public string Sender { get; set; }
        public string EmailAddress { get; set; }
        public int TenantId { get; set; }

    }
}

