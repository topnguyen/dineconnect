﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using DinePlan.DineConnect.Connect.UrbanPiperIntegrate;
using DinePlan.DineConnect.Connect.UrbanPiperIntegrate.Dto;

namespace DinePlan.DineConnect.Job
{
    public class IntegrateJob : BackgroundJob<PushMenuItemInput>, ITransientDependency
    {
        private readonly IUrbanPiperIntegrateAppService _integrateAppService;

        public IntegrateJob(IUrbanPiperIntegrateAppService integrateAppService)
        {
            _integrateAppService = integrateAppService;
        }

        public override void Execute(PushMenuItemInput args)
        {
            _integrateAppService.PushMenuItemsBackground(args);
        }
    }
}