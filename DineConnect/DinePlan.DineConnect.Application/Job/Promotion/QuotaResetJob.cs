﻿using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.Logging;
using Abp.Threading;
using Abp.Threading.BackgroundWorkers;
using Abp.Threading.Timers;
using Castle.Core.Logging;
using DinePlan.DineConnect.Connect.Discount;
using System;

namespace DinePlan.DineConnect.Job
{
    public class QuotaResetJob : PeriodicBackgroundWorkerBase, ISingletonDependency
    {
        private readonly ILogger _logger;
        private readonly IPromotionAppService _promotionAppService;

        public QuotaResetJob(AbpTimer timer, ILogger logger, IPromotionAppService promotionAppService)
            : base(timer)
        {
            _logger = logger;
            _promotionAppService = promotionAppService;
            Timer.Period = 60000;
        }
        [UnitOfWork]
        protected override void DoWork()
        {
            var tomorrow = DateTime.Today.AddDays(1);
            var next = DateTime.Now.AddMinutes(1);
            if (next > tomorrow)
            {
                _logger.Log(LogSeverity.Info, "Execute QuotaResetJob");
                AsyncHelper.RunSync(() => _promotionAppService.ResetQuota());
            }
        }
    }
}