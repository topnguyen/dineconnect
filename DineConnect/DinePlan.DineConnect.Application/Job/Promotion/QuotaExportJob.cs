﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.Threading;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Discount.Dtos;
using DinePlan.DineConnect.Connect.OrderTag.Dtos;
using DinePlan.DineConnect.Connect.OrderTag.Exporting;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Report;

namespace DinePlan.DineConnect.Job
{
    public class QuotaExportJob : BackgroundJob<QuotaExportInput>, ITransientDependency
    {
        private readonly IPromotionExporter _exporter;
        private readonly IReportBackgroundAppService _reportAppService;

        public QuotaExportJob(IPromotionExporter exporter, IReportBackgroundAppService reportBackgroundAppService)
        {
            _exporter = exporter;
            _reportAppService = reportBackgroundAppService;
        }

        [UnitOfWork]
        public override void Execute(QuotaExportInput args)
        {
            if (args != null)
            {
                FileDto fileDto = null;
                switch (args.ReportName)
                {
                    case ReportNames.QUOTAS:
                        fileDto = AsyncHelper.RunSync(() => _exporter.ExportQuota(args));
                        break;
                    case ReportNames.QUOTATICKETS:
                        fileDto = AsyncHelper.RunSync(() => _exporter.ExportQuotaTicket(args));
                        break;

                    default:
                        break;
                }
                if (fileDto != null)
                {
                    AsyncHelper.RunSync(() => _reportAppService.UpdateFileOutputCompleted(fileDto, args.BackGroundId));
                }
            }
        }
    }
}