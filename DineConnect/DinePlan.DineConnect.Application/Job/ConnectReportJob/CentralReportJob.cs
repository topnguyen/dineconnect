﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.Threading;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Ticket.Central;
using DinePlan.DineConnect.Connect.Ticket.Central.Dto;
using DinePlan.DineConnect.Dto;
using System;

namespace DinePlan.DineConnect.Job.ConnectReportJob
{
    public class CentralReportJob : BackgroundJob<CentralReportInputArgs>, ITransientDependency
    {
        private readonly ICentralReportAppService _centralReportAppService;
        private readonly IReportBackgroundAppService _reportAppService;
        private readonly ICentralReportExporter _exporter;

        public CentralReportJob(ICentralReportAppService centralReportAppService
            , IReportBackgroundAppService reportAppService
            , ICentralReportExporter exporter
            )
        {
            _centralReportAppService = centralReportAppService;
            _reportAppService = reportAppService;
            _exporter = exporter;
        }

        [UnitOfWork]
        public override void Execute(CentralReportInputArgs args)
        {
            if (args != null)
            {
                FileDto fileDto = null;

                fileDto = AsyncHelper.RunSync(() => _exporter.CentralDayExport(args.CentralExportInput, _centralReportAppService));

                if (fileDto != null)
                {
                    AsyncHelper.RunSync(() =>
                        _reportAppService.UpdateFileOutputCompleted(fileDto, args.BackGroundId));
                }
            }
        }
    }

    [Serializable]
    public class CentralReportInputArgs
    {
        public string ReportName { get; set; }
        public int BackGroundId { get; set; }
        public CentralExportInput CentralExportInput { get; set; }
        public int TenantId { get; set; }
        public long UserId { get; set; }
    }
}