﻿using Abp.Application.Services.Dto;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.Threading;
using DinePlan.DineConnect.Connect.FeedbackUnread;
using DinePlan.DineConnect.Connect.FeedbackUnread.Dto;
using DinePlan.DineConnect.Connect.FeedbackUnread.Exporting;
using DinePlan.DineConnect.Connect.Log;
using DinePlan.DineConnect.Connect.Log.Implementation;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.PaymentExcess;
using DinePlan.DineConnect.Connect.PaymentExcess.Exporting;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.SaleTarget;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.Ticket.CategoryReport;
using DinePlan.DineConnect.Connect.Ticket.CategoryReport.Exporting;
using DinePlan.DineConnect.Connect.Ticket.CollectionReport;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Ticket.OrderPromotion;
using DinePlan.DineConnect.Connect.Ticket.TicketPromotion;
using DinePlan.DineConnect.Connect.Ticket.TicketPromotion.Exporting;
using DinePlan.DineConnect.Connect.TillAccount;
using DinePlan.DineConnect.Connect.TillAccount.Dto;
using DinePlan.DineConnect.Connect.TillAccount.Implementation;
using DinePlan.DineConnect.Connect.User.Dtos;
using DinePlan.DineConnect.Connect.WorkPeriod;
using DinePlan.DineConnect.Connect.WorkPeriod.Dtos;
using DinePlan.DineConnect.Connect.WorkPeriod.Implementation;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter;
using DinePlan.DineConnect.Report;
using System;
using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Master.Exporter;
using DinePlan.DineConnect.Hr.Report;
using DinePlan.DineConnect.Hr.Report.Dto;
using DinePlan.DineConnect.Connect.ReturnProduct.Dtos;
using DinePlan.DineConnect.Connect.ReturnProduct.Exporting;
using DinePlan.DineConnect.Auditing;
using DinePlan.DineConnect.Auditing.Exporting;
using DinePlan.DineConnect.Connect.AbnormalEndday.Dtos;
using DinePlan.DineConnect.Connect.AbnormalEndday.Exporting;
using DinePlan.DineConnect.Connect.Ticket.Central;
using DinePlan.DineConnect.Connect.Ticket.Central.Dto;
using DinePlan.DineConnect.Connect.SpeedOfServiceReport.Dto;
using DinePlan.DineConnect.Connect.CashSummaryReport.Dto;
using DinePlan.DineConnect.Connect.CreditCardReport.Dto;
using DinePlan.DineConnect.Connect.SpeedOfServiceReport;
using DinePlan.DineConnect.Connect.SpeedOfServiceReport.Exporting;
using DinePlan.DineConnect.Connect.CashSummaryReport;
using DinePlan.DineConnect.Connect.CashSummaryReport.Exporting;
using DinePlan.DineConnect.Connect.CreditCardReport;
using DinePlan.DineConnect.Connect.CreditCardReport.Exporting;
using DinePlan.DineConnect.Connect.Alert.CashAudit.DTO;
using DinePlan.DineConnect.Connect.Alert.CashAudit.Exporting;
using DinePlan.DineConnect.Connect.Alert.CashAudit;
using DinePlan.DineConnect.Connect.ComparisionReport.Dto;
using DinePlan.DineConnect.Connect.ComparisionReport;
using DinePlan.DineConnect.Connect.ComparisionReport.Exporting;
using DinePlan.DineConnect.Connect.ComparisionReport.MonthlyStoreSalesStats.Dto;
using DinePlan.DineConnect.Connect.ComparisionReport.MonthlyStoreSalesStats.Exporting;
using DinePlan.DineConnect.Connect.ComparisionReport.MonthlyStoreSalesStats;
using DinePlan.DineConnect.Connect.CreditSalesReport.Dto;
using DinePlan.DineConnect.Connect.CreditSalesReport;
using DinePlan.DineConnect.Connect.CreditSalesReport.Exporting;
using DinePlan.DineConnect.Connect.FullTaxInvoiceReport;
using DinePlan.DineConnect.Connect.FullTaxInvoiceReport.Exporting;
using DinePlan.DineConnect.Connect.FullTaxInvoiceReport.Dto;
using DinePlan.DineConnect.Connect.TopDownSellReport.Dto;
using DinePlan.DineConnect.Connect.TopDownSellReport.Exporting;
using DinePlan.DineConnect.Connect.TopDownSellReport;
using DinePlan.DineConnect.Connect.SalesPromotionReport;
using DinePlan.DineConnect.Connect.SalesPromotionReport.Exporting;
using DinePlan.DineConnect.Connect.SalesPromotionReport.Dto;
using DinePlan.DineConnect.Connect.ProductMix.Exporting;
using DinePlan.DineConnect.Connect.ProductMix;
using DinePlan.DineConnect.Connect.ProductMix.Dto;
using DinePlan.DineConnect.Connect.ComparisionReport.MonthlySalesAdvance.Dto;
using DinePlan.DineConnect.Connect.ComparisionReport.MonthlySalesAdvance;
using DinePlan.DineConnect.Connect.ComparisionReport.MonthlySalesAdvance.Exporting;
using DinePlan.DineConnect.Connect.ComparisionReport.DailySalesBreakdown.Dto;
using DinePlan.DineConnect.Connect.ComparisionReport.DailySalesBreakdown;
using DinePlan.DineConnect.Connect.ComparisionReport.DailySalesBreakdown.Exporting;

namespace DinePlan.DineConnect.Job.ConnectReportJob
{
    public class ConnectReportJob : BackgroundJob<ConnectReportInputArgs>, ITransientDependency
    {
        private readonly IConnectReportAppService _connectReportAppService;

        private readonly ITicketListExcelExporter _exporter;
        private readonly IWorkPeriodOutputExporter _wpoe;
        private readonly IWorkPeriodExporter _wpe;
        private readonly ISaleTargetExporter _ste;
        private readonly ISaleTargetAppService _saleTargetAppService;
        private readonly IReportBackgroundAppService _reportAppService;
        private readonly IHourExporter _hourlyExporter;
        private readonly ITillAccountListExcelExporter _tillExporter;
        private readonly IPlanLoggerExporter _planLoggerExporter;
        private readonly WorkPeriodAppService _workPeriodAppService;
        private readonly TillAccountAppService _tillAccountAppService;
        private readonly PlanLoggerAppService _planLoggerAppService;
        private readonly IPaymentExcessReportExporter _paymentExcessReportExporter;
        private readonly IPaymentExcessReportAppService _paymentExcessReportAppService;
        private readonly ICategoryReportExporter _categoryReportExporter;
        private readonly ICategoryReportAppService _categoryReportAppService;
        private readonly ICollectionReportAppService _collectionReportAppService;
        private readonly IOrderPromotionReportAppService _orderPromotionReportAppService;
        private readonly ITicketPromotionReportAppService _ticketPromotionReportAppService;
        private readonly ITicketPromotionReportExporter _ticketPromotionReportExporter;
        private readonly IFeedbackUnreadAppService _feedbackUnreadAppService;
        private readonly IFeedbackUnreadReportExporter _feedbackUnreadReportExporter;
        private readonly ITopDownSellReportExporter _topDownSellReportExporter;
        private readonly ITopDownSellReportAppService _topDownSellReportAppService;
        private readonly IProductMixReportExporter _productMixReportExporter;
        private readonly IProductMixReportAppService _productMixReportAppService;
        private readonly ISalesPromotionReportAppService _salesPromotionReportAppService;
        private readonly ISalesPromotionReportExporter _salesPromotionReportExporter;
        private readonly IEmployeeInfoListExcelExporter _employeeInfoListExcelExporter;
        private readonly IEmployeeInfoAppService _employeeInfoAppService;
        private readonly IReturnProductReportExcelExporter _returnProductReportExcelExporter;
        private readonly IAuditLogAppService _auditLogAppService;
        private readonly IAuditLogListExcelExporter _auditLogListExcelExporter;
        private readonly IAbnormalEndDayReportExcelExporter _abnormalEndDayReportExcelExporter;
        private readonly ICentralReportExporter _centralReportExporter;
        private readonly ICentralReportAppService _centralReportAppService;
        private readonly ISpeedOfServiceReportExporter _speedOfServiceReportExporter;
        private readonly ICashSummaryReportExporter _cashSummaryReportExporter;
        private readonly IFullTaxInvoiceReportExporter _fullTaxInvoiceReportExporter;
        private readonly ICashSummaryReportAppService _cashSummaryReportAppService;
        private readonly IFullTaxInvoiceReportAppService _fullTaxInvoiceReportAppService;
        private readonly ICreditCardReportAppService _creditCardReportAppService;
        private readonly ICreditCardReportExporter _creditCardReportExporter;
        private readonly ISpeedOfServiceReportAppService _speedOfServiceReportAppService;
        private readonly IConsolidatedAppService _consolidatedAppService;
        private readonly IConsolidatedExporter _consolidatedExporter;
        private readonly ICashAuditReportExporter _cashAuditExporter;
        private readonly ICashAuditAppService _cashAuditAppService;
        private readonly IMonthlySalesReportAppService _monthlySalesReportAppservice;
        private readonly IMonthlySalesReportExporter _monthlySalesReportExporter;
        private readonly IMealTimeSalesReportAppService _mealTimeSaleesReportAppservice;
        private readonly IMealTimeSalesReportExporter _mealTimeSalesReportExporter;
        private readonly IMonthlySalesAdvanceReportAppService _monthlyAdvanceReportAppservice;
        private readonly IMonthlySalesAdvanceReportExporter _monthlyAdvanceReportExporter;
        private readonly IMonthlySalesStatsReportExporter _monthlySalesStatsReportExporter;
        private readonly IMonthlySalesStatsReportAppService _monthlySalesStatsReportAppservice;
        private readonly ICreditSalesReportAppService _creditSalesReportAppService;
        private readonly ICreditSalesReportExporter _creditSalesReportExporter; 
        private readonly IDailySalesBreakdownReportAppService _dailySalesBreakdownReportAppService;
        private readonly IDailySalesBreakdownReportExporter _dailySalesBreakdownReportExporter;
        public ConnectReportJob(IConnectReportAppService connectReportAppService,
            ITicketListExcelExporter exporter, IReportBackgroundAppService reportAppService, IWorkPeriodOutputExporter wpoe,
            WorkPeriodAppService workPeriodAppService, IHourExporter hourExporter, ITillAccountListExcelExporter tillExporter,
            TillAccountAppService tillAccountAppService, PlanLoggerAppService planLoggerAppService, IPlanLoggerExporter planLoggerExporter,
            IWorkPeriodExporter wpe, ISaleTargetExporter ste, ISaleTargetAppService saleTargetAppService,
            IPaymentExcessReportExporter paymentExcessReportExporter,
            IPaymentExcessReportAppService paymentExcessReportAppService,
            ICategoryReportExporter categoryReportExporter,
            ICategoryReportAppService categoryReportAppService,
            ICollectionReportAppService collectionReportAppService,
            IOrderPromotionReportAppService orderPromotionReportAppService,
            ITicketPromotionReportAppService ticketPromotionReportAppService,
            ITicketPromotionReportExporter ticketPromotionReportExporter,
            IFeedbackUnreadAppService feedbackUnreadAppService,
            IFeedbackUnreadReportExporter feedbackUnreadReportExporter,
            IEmployeeInfoListExcelExporter employeeInfoListExcelExporter,
            IEmployeeInfoAppService employeeInfoAppService,
            IReturnProductReportExcelExporter returnProductReportExcelExporter,
            IAuditLogAppService auditLogAppService,
            IAuditLogListExcelExporter auditLogListExcelExporter,
            ICentralReportExporter centralReportExporter,
            ICentralReportAppService centralReportAppService,
            IAbnormalEndDayReportExcelExporter abnormalEndDayReportExcelExporter,
           ISpeedOfServiceReportExporter speedOfServiceReportExporter,
           ISpeedOfServiceReportAppService speedOfServiceReportAppService,
           ICashSummaryReportExporter cashSummaryReportExporter,
           IFullTaxInvoiceReportExporter fullTaxInvoiceReportExporter,
           ICashSummaryReportAppService cashSummaryReportAppService,
           IFullTaxInvoiceReportAppService fullTaxInvoiceReportAppService,
           ICreditCardReportAppService creditCardReportAppService,
           ICreditCardReportExporter creditCardReportExporter,
           IConsolidatedAppService consolidatedAppService,
           IConsolidatedExporter consolidatedExporter,
           ICashAuditReportExporter cashAuditExporter,
           ICashAuditAppService cashAuditAppService,
           IMonthlySalesReportAppService monthlySalesReportAppservice,
           IMonthlySalesReportExporter monthlySalesReportExporter,
           IMealTimeSalesReportAppService mealTimeSaleesReportAppservice,
           IMealTimeSalesReportExporter mealTimeSalesReportExporter,
           IMonthlySalesStatsReportExporter monthlySalesStatsReportExporter,
           IMonthlySalesStatsReportAppService monthlySalesStatsReportAppservice,
           ICreditSalesReportAppService creditSalesReportAppService,
           ICreditSalesReportExporter creditSalesReportExporter,
           ITopDownSellReportAppService topDownSellReportAppService,
           ITopDownSellReportExporter topDownSellReportExporter,
           ISalesPromotionReportExporter salesPromotionReportExporter,
           ISalesPromotionReportAppService salesPromotionReportAppService,
           IProductMixReportExporter productMixReportExporter,
           IProductMixReportAppService productMixReportAppService,
           IMonthlySalesAdvanceReportAppService monthlyAdvanceReportAppservice,
           IMonthlySalesAdvanceReportExporter monthlyAdvanceReportExporter,
           IDailySalesBreakdownReportAppService dailySalesBreakdownReportAppService,
           IDailySalesBreakdownReportExporter dailySalesBreakdownReportExporter
        )
        {
            _connectReportAppService = connectReportAppService;
            _exporter = exporter;
            _wpoe = wpoe;
            _workPeriodAppService = workPeriodAppService;
            _reportAppService = reportAppService;
            _hourlyExporter = hourExporter;
            _tillExporter = tillExporter;
            _tillAccountAppService = tillAccountAppService;
            _planLoggerAppService = planLoggerAppService;
            _planLoggerExporter = planLoggerExporter;
            _wpe = wpe;
            _ste = ste;
            _saleTargetAppService = saleTargetAppService;
            _paymentExcessReportExporter = paymentExcessReportExporter;
            _paymentExcessReportAppService = paymentExcessReportAppService;
            _categoryReportExporter = categoryReportExporter;
            _categoryReportAppService = categoryReportAppService;
            _collectionReportAppService = collectionReportAppService;
            _orderPromotionReportAppService = orderPromotionReportAppService;
            _ticketPromotionReportAppService = ticketPromotionReportAppService;
            _feedbackUnreadAppService = feedbackUnreadAppService;
            _ticketPromotionReportExporter = ticketPromotionReportExporter;
            _feedbackUnreadReportExporter = feedbackUnreadReportExporter;
            _employeeInfoListExcelExporter = employeeInfoListExcelExporter;
            _employeeInfoAppService = employeeInfoAppService;
            _returnProductReportExcelExporter = returnProductReportExcelExporter;
            _auditLogAppService = auditLogAppService;
            _auditLogListExcelExporter = auditLogListExcelExporter;
            _centralReportExporter = centralReportExporter;
            _centralReportAppService = centralReportAppService;
            _abnormalEndDayReportExcelExporter = abnormalEndDayReportExcelExporter;
            _speedOfServiceReportExporter = speedOfServiceReportExporter;
            _speedOfServiceReportAppService = speedOfServiceReportAppService;
            _cashSummaryReportExporter = cashSummaryReportExporter;
            _fullTaxInvoiceReportExporter = fullTaxInvoiceReportExporter;
            _cashSummaryReportAppService = cashSummaryReportAppService;
            _fullTaxInvoiceReportAppService = fullTaxInvoiceReportAppService;
            _creditCardReportAppService = creditCardReportAppService;
            _creditCardReportExporter = creditCardReportExporter;
            _consolidatedExporter = consolidatedExporter;
            _consolidatedAppService = consolidatedAppService;
            _cashAuditExporter = cashAuditExporter;
            _cashAuditAppService = cashAuditAppService;
            _monthlySalesReportAppservice = monthlySalesReportAppservice;
            _monthlySalesReportExporter = monthlySalesReportExporter;
            _mealTimeSaleesReportAppservice = mealTimeSaleesReportAppservice;
            _mealTimeSalesReportExporter = mealTimeSalesReportExporter;
            _monthlySalesStatsReportAppservice = monthlySalesStatsReportAppservice;
            _monthlySalesStatsReportExporter = monthlySalesStatsReportExporter;
            _creditSalesReportAppService = creditSalesReportAppService;
            _creditSalesReportExporter = creditSalesReportExporter;
            _topDownSellReportExporter = topDownSellReportExporter;
            _topDownSellReportAppService = topDownSellReportAppService;
            _salesPromotionReportExporter = salesPromotionReportExporter;
            _salesPromotionReportAppService = salesPromotionReportAppService;
            _productMixReportAppService = productMixReportAppService;
            _productMixReportExporter = productMixReportExporter;
            _monthlyAdvanceReportAppservice = monthlyAdvanceReportAppservice;
            _monthlyAdvanceReportExporter = monthlyAdvanceReportExporter;
            _dailySalesBreakdownReportAppService = dailySalesBreakdownReportAppService;
            _dailySalesBreakdownReportExporter = dailySalesBreakdownReportExporter;
    }

        [UnitOfWork]
        public override void Execute(ConnectReportInputArgs args)
        {
            if (args != null)
            {
                FileDto fileDto = null;
                switch (args.ReportName)
                {
                    case ReportNames.CollectionReport:
                        fileDto = AsyncHelper.RunSync(() => _collectionReportAppService.GetCollectionReportForExportInternal(args.TicketInput));
                        break;

                    case ReportNames.LOCATIONCOMPARISON:
                        fileDto = _exporter.ExportLocationComparison(args.LocationComparisons);
                        break;

                    case ReportNames.REFUNDTICKETDETAIL:
                        fileDto = AsyncHelper.RunSync(() =>
                            _exporter.ExportRefundTicketDetail(args.TicketInput, _connectReportAppService));
                        break;

                    case ReportNames.ITEMANDORDERTAGSALES:
                        fileDto = AsyncHelper.RunSync(() =>
                            _exporter.ExportToFileItemAndOrderTagSales(args.ItemInput, _connectReportAppService));
                        break;

                    case ReportNames.ITEMCOMBOSALES:
                        fileDto = AsyncHelper.RunSync(() =>
                            _exporter.ExportToFileItemCombos(args.ItemInput, _connectReportAppService));
                        break;

                    case ReportNames.ITEMSALES:
                        fileDto = AsyncHelper.RunSync(() =>
                            _exporter.ExportToFileItems(args.ItemInput, _connectReportAppService));

                        break;

                    case ReportNames.CATEGORYSALES:
                        fileDto = AsyncHelper.RunSync(() =>
                            _categoryReportExporter.ExportToFileCategory(args.ItemInput, _categoryReportAppService));

                        break;

                    case ReportNames.GROUPSALES:
                        fileDto = AsyncHelper.RunSync(() =>
                            _exporter.ExportToFileGroup(args.ItemInput, _connectReportAppService));

                        break;

                    case ReportNames.ITEMSALESBYPRICE:
                        fileDto = AsyncHelper.RunSync(() =>
                            _exporter.ExportToFileItemsByPrice(args.ItemInput, _connectReportAppService));

                        break;
                    case ReportNames.ITEMTAGSALESDETAIL:
                        fileDto = AsyncHelper.RunSync(() =>
                            _exporter.ExportToFileItemTagSalesDetail(args.ItemInput, _connectReportAppService));

                        break;
                    case ReportNames.ITEMTAGSALES:
                        fileDto = AsyncHelper.RunSync(() =>
                            _exporter.ExportToFileItemTagSales(args.ItemInput, _connectReportAppService));

                        break;
                    case ReportNames.ITEMSALESBYPORTION:
                        fileDto = AsyncHelper.RunSync(() =>
                            _exporter.ExportToFileItemsSummary(args.ItemInput, _connectReportAppService));

                        break;

                    case ReportNames.HOURLYSALES:
                        {
                            if (args.TicketInput.Duration.Equals("S"))
                            {
                                fileDto = AsyncHelper.RunSync(() =>
                                    _hourlyExporter.ExportHourlySummary(args.TicketInput, _connectReportAppService));
                            }

                            if (args.TicketInput.Duration.Equals("D"))
                            {
                                fileDto = AsyncHelper.RunSync(() =>
                                    _hourlyExporter.ExportHourlySummaryByDate(args.TicketInput, _connectReportAppService));
                            }
                        }

                        break;

                    case ReportNames.TICKETSALES:
                        fileDto = AsyncHelper.RunSync(() => _exporter.ExportTickets(args.TicketReportInput.TicketInput,
                            _connectReportAppService, args.TicketReportInput.TransactionTypes,
                            args.TicketReportInput.PaymentTypes, args.TicketReportInput.TicketTags, args.TenantId));

                        break;

                    case ReportNames.TICKETSYNCS:
                        fileDto = AsyncHelper.RunSync(() => _exporter.ExportTicketSyncs(args.TicketSyncReportInput.TicketSyncInput, _connectReportAppService));
                        break;

                    case ReportNames.DEPARTMENTSALES:
                        fileDto = AsyncHelper.RunSync(() => _exporter.ExportItemSalesForDepartmentExcel(args.ItemInput, _connectReportAppService));

                        break;

                    case ReportNames.WEEKDAYSALES:
                        fileDto = AsyncHelper.RunSync(() => _wpoe.ExportWeekDayOutput(args.WorkPeriodSummaryInput,
                            _workPeriodAppService, _connectReportAppService));

                        break;

                    case ReportNames.WEEKENDSALES:
                        fileDto = AsyncHelper.RunSync(() => _wpoe.ExportWeekEndOutput(args.WorkPeriodSummaryInput,
                            _workPeriodAppService, _connectReportAppService));

                        break;

                    case ReportNames.ITEMHOURS:
                        fileDto = AsyncHelper.RunSync(() => _exporter.ExportToFileItemsHours(args.ItemInput, _connectReportAppService));

                        break;

                    case ReportNames.ITEMLOCATIONS:
                        fileDto = AsyncHelper.RunSync(() => _exporter.ExportToFileLocationSales(args.TicketInput, _connectReportAppService));

                        break;

                    case ReportNames.ITEMLOCATIONSDETAIL:
                        fileDto = AsyncHelper.RunSync(() => _exporter.ExportToFileLocationSalesDetail(args.TicketInput, _connectReportAppService));

                        break;

                    case ReportNames.ORDERS:
                        fileDto = AsyncHelper.RunSync(() => _exporter.ExportOrders(args.ItemInput, _connectReportAppService));

                        break;

                    case ReportNames.ORDERPROMOTION:
                        fileDto = AsyncHelper.RunSync(() => _orderPromotionReportAppService.ExportInBackground(args.ItemInput));

                        break;

                    case ReportNames.DEPARTMENTSUMMARY:
                        fileDto = AsyncHelper.RunSync(() => _exporter.ExportToDepartmentSummary(args.TicketInput, _connectReportAppService, args.TTList, args.PTList));

                        break;

                    case ReportNames.SALESUMMARY:
                        fileDto = AsyncHelper.RunSync(() => _exporter.ExportToSalesSummary(args.TicketInput, _connectReportAppService, args.TTList, args.PTList, args.DTList, args.ProList, args.TenantId));

                        break;
                    case ReportNames.SALESUMMARYNEW:
                        fileDto = AsyncHelper.RunSync(() => _exporter.ExportToSalesSummaryNew(args.TicketInput, _connectReportAppService, args.TTList, args.PTList, args.DTList, args.ProList, args.TenantId));

                        break;
                    case ReportNames.SALESUMMARYSECOND:
                        fileDto = AsyncHelper.RunSync(() => _exporter.ExportToSalesSecondSummary(args.TicketInput, _connectReportAppService, args.PTList, args.TenantId));

                        break;
                    case ReportNames.TILLTRANSACTION:
                        fileDto = AsyncHelper.RunSync(() => _tillExporter.ExportToFilePromotionOrders(args.TillAccountInput, _tillAccountAppService));

                        break;

                    case ReportNames.LOGGER:
                        fileDto = AsyncHelper.RunSync(() => _planLoggerExporter.ExportLogs(_planLoggerAppService, args.PlanLoggerInput));

                        break;

                    case ReportNames.DAYSUMMARY:
                        {
                            if (args.WorkPeriodSummaryInput.ExportOutputType == ExportType.Excel)
                            {
                                fileDto = AsyncHelper.RunSync(() => _wpe.ExportWorkPeriodSummary(args.WorkPeriodSummaryInput, _workPeriodAppService));
                            }
                            else
                            {
                                fileDto = AsyncHelper.RunSync(() => _wpoe.ExportWorkPeriodSummary(args.WorkPeriodSummaryInput, _workPeriodAppService));
                            }
                        }

                        break;

                    case ReportNames.DAYDETAILS:
                        {
                            if (args.WorkPeriodSummaryInput.ExportOutputType == ExportType.Excel)
                            {
                                fileDto = AsyncHelper.RunSync(() => _wpe.ExportWorkPeriodDetails(args.WorkPeriodSummaryInput, _workPeriodAppService));
                            }
                            else
                            {
                                fileDto = AsyncHelper.RunSync(() => _wpoe.ExportWorkPeriodDetails(args.WorkPeriodSummaryInput, _workPeriodAppService));
                            }
                        }

                        break;

                    case ReportNames.DAYSUMMARYEXPORT:
                        fileDto = AsyncHelper.RunSync(() => _wpoe.ExportSummaryOutput(args.WorkPeriodSummaryInput, _workPeriodAppService, _connectReportAppService));

                        break;
                    case ReportNames.MONTHLYSALEADVANCE:
                        fileDto = AsyncHelper.RunSync(() => _monthlyAdvanceReportExporter.ExportMonthlySalesAdvanceReport(args.MonthlySaleAdvanceReportInput.GetMonthlySaleAdvanceReportInput, _monthlyAdvanceReportAppservice));

                        break;
                    case ReportNames.DAILYSALEBREAK:
                        fileDto = AsyncHelper.RunSync(() => _dailySalesBreakdownReportExporter.ExportDaylySalesReport(args.DailySalesBreakdownReportInput.GetDailySalesBreakdownReportInput, _dailySalesBreakdownReportAppService));

                        break;
                    case ReportNames.SCHEDULESALES:
                        fileDto = AsyncHelper.RunSync(() => _wpoe.ExportScheduleSalesOutput(args.WorkPeriodSummaryInput, _workPeriodAppService, _connectReportAppService));

                        break;

                    case ReportNames.SALETARGET:
                        fileDto = AsyncHelper.RunSync(() => _ste.ExportSaleTargetAsync(args.SaleTargetTicketInput, _saleTargetAppService));

                        break;

                    case ReportNames.DISCOUNT:
                        fileDto = AsyncHelper.RunSync(() => _ticketPromotionReportExporter.ExportToFileDiscountDetail(args.TicketInput, _ticketPromotionReportAppService));

                        break;

                    case ReportNames.TICKETPROMOTIONDETAIL:
                        fileDto = AsyncHelper.RunSync(() => _ticketPromotionReportExporter.ExportToFileTicketPromotionDetail(args.TicketInput, _ticketPromotionReportAppService));
                        break;

                    case ReportNames.TENDER:
                        fileDto = AsyncHelper.RunSync(() => _exporter.ExportToFileTenderDetail(args.TicketInput, _connectReportAppService));

                        break;

                    case ReportNames.FRANCHISE:
                        fileDto = AsyncHelper.RunSync(() => _exporter.ExportFranchise(args.TicketInput, _connectReportAppService, args.TTList, args.PTList, args.LList, args.MenuItemList));
                        break;

                    case ReportNames.PAYMENTEXCESSSUMMARY:
                        fileDto = AsyncHelper.RunSync(() => _paymentExcessReportExporter.ExportPaymentExcessSummary(args.TicketInput, _paymentExcessReportAppService));
                        break;

                    case ReportNames.PAYMENTEXCESSDETAIL:
                        fileDto = AsyncHelper.RunSync(() => _paymentExcessReportExporter.ExportPaymentExcessDetail(args.TicketInput, _paymentExcessReportAppService));
                        break;

                    case ReportNames.TOPORBOTTOMITEMSALES:

                        fileDto = AsyncHelper.RunSync<FileDto>(() =>
                            _connectReportAppService.GetTopOrBottomItemSalesToExcelInBackground(args.ItemInput));
                        break;

                    case ReportNames.SCHEDULEREPORT:

                        fileDto = AsyncHelper.RunSync<FileDto>(() =>
                                                        _exporter.ExportScheduleReportToFile(args.ItemInput, _connectReportAppService));

                        break;
                    case ReportNames.FEEDBACKUNREAD:

                        fileDto = AsyncHelper.RunSync<FileDto>(() =>
                                                        _feedbackUnreadReportExporter.ExportToFile(args.FeedbackUnreadExportInput, _feedbackUnreadAppService));

                        break;

                    case ReportNames.TOPDOWNSELLAMOUNTPLANT:

                        fileDto = AsyncHelper.RunSync<FileDto>(() =>
                                                        _topDownSellReportExporter.ExportTopDownSellByAmountPlantReport(args.TopDownSellReportInput, _topDownSellReportAppService));

                        break;

                    case ReportNames.PRODUCTMIX:

                        fileDto = AsyncHelper.RunSync<FileDto>(() =>
                                                        _productMixReportExporter.ExportProductMixReport(args.ProductMixInput, _productMixReportAppService));

                        break;

                    case ReportNames.TOPDOWNSELLAMOUNTSUMMARY:

                        fileDto = AsyncHelper.RunSync<FileDto>(() =>
                                                        _topDownSellReportExporter.ExportTopDownSellByAmountSummaryReport(args.TopDownSellReportInput, _topDownSellReportAppService));

                        break;
                    case ReportNames.TOPDOWNSELLQUANTITYPLANT:

                        fileDto = AsyncHelper.RunSync<FileDto>(() =>
                                                        _topDownSellReportExporter.ExportTopDownSellByQuantityPlantReport(args.TopDownSellReportInput, _topDownSellReportAppService));

                        break;
                    case ReportNames.TOPDOWNSELLQUANTITYSUMMARY:

                        fileDto = AsyncHelper.RunSync<FileDto>(() =>
                                                        _topDownSellReportExporter.ExportTopDownSellByQuantitySummaryReport(args.TopDownSellReportInput, _topDownSellReportAppService));

                        break;
                    case ReportNames.SALESPROMOTIONBYPROMOTION:

                        fileDto = AsyncHelper.RunSync<FileDto>(() =>
                                                        _salesPromotionReportExporter.ExportSalePromotionByPromotionReport(args.SalesPromotionInput, _salesPromotionReportAppService));

                        break;
                    case ReportNames.SALESPROMOTIONBYPLANT:

                        fileDto = AsyncHelper.RunSync<FileDto>(() =>
                                                        _salesPromotionReportExporter.ExportSalePromotionByPlantReport(args.SalesPromotionInput, _salesPromotionReportAppService));

                        break;
                    case ReportNames.ABBREPRINT:

                        fileDto = AsyncHelper.RunSync<FileDto>(() =>
                                                        _exporter.ExportABBReprintReportToFile(args.ABBReprintReportInput, _connectReportAppService));

                        break;
                    case ReportNames.RETURNPRODUCT:

                        fileDto = AsyncHelper.RunSync<FileDto>(() =>
                                                        _returnProductReportExcelExporter.ExportReturnProduct(args.GetReturnProductReportInput, _connectReportAppService));

                        break;
                    case ReportNames.ABNORMALENDDAY:

                        fileDto = AsyncHelper.RunSync<FileDto>(() =>
                                                        _abnormalEndDayReportExcelExporter.ExportAbnormalEndDay(args.GetAbnormalEndDayReportInput, _connectReportAppService));

                        break;
                    case ReportNames.TIMEATTENDANCE:

                        fileDto = AsyncHelper.RunSync<FileDto>(() =>
                                                        _employeeInfoListExcelExporter.ExportTimeAttendanceReportToFile(args.TimeAttendanceReportInput, _employeeInfoAppService));

                        break;
                    case ReportNames.EXCHANGEREPORT:

                        fileDto = AsyncHelper.RunSync<FileDto>(() =>
                                                        _exporter.ExportExchangeReport(args.ItemInput, _connectReportAppService));

                        break;
                    case ReportNames.EXTERNALLOG:

                        fileDto = AsyncHelper.RunSync<FileDto>(() => _auditLogListExcelExporter.ExportExternalLogToFile(args.ItemInput, _connectReportAppService));

                        break;
                    //case ReportNames.DAILYRECONCILITION:

                    //	fileDto = AsyncHelper.RunSync<FileDto>(() => _centralReportExporter.DailyReconciliationExport(args.CentralShiftReportInput, _centralReportAppService));

                    //	break;
                    case ReportNames.DAILYRECONCILITIONALL:

                        fileDto = AsyncHelper.RunSync<FileDto>(() => _centralReportExporter.DailyReconciliationExportAll(args.WorkPeriodSummaryInput, _centralReportAppService));

                        break;

                    case ReportNames.CASHAUDIT:
                        fileDto = AsyncHelper.RunSync(() => _cashAuditExporter.ExportCashAudits(args.CashAuditReportInput.CashAuditInput, _cashAuditAppService));
                        break;

                    case ReportNames.SPEEDOFSERVICEREPORT:
                        fileDto = AsyncHelper.RunSync(() => _speedOfServiceReportExporter.ExportSpeedOfServiceReport(args.SpeedOfServiceReportInput.GetSpeedOfServiceReportInput, _speedOfServiceReportAppService));
                        break;

                    case ReportNames.CASHSUMMARYREPORT:
                        fileDto = AsyncHelper.RunSync(() => _cashSummaryReportExporter.ExportCashSummaryReport(args.CashSummaryReportInput.GetCashSummaryReportInput, _cashSummaryReportAppService));
                        break;

                    case ReportNames.FULLTAXINVOICEREPORT:
                        fileDto = AsyncHelper.RunSync(() => _fullTaxInvoiceReportExporter.ExportFullTaxInvoiceReport(args.FullTaxInvoiceReportInput.GetFullTaxInvoiceReportInput, _fullTaxInvoiceReportAppService));
                        break;

                    case ReportNames.CREDITCARD:
                        fileDto = AsyncHelper.RunSync(() => _creditCardReportExporter.ExportCreditCard(args.CreditCardReportInput.CreditCardInput, _creditCardReportAppService));
                        break;

                    case ReportNames.CONSOLIDATED:
                        fileDto = AsyncHelper.RunSync(() => _consolidatedExporter.ExportConsolidatedReportToFile(args.TicketInput, _consolidatedAppService));
                        break;

                    case ReportNames.PLANWORKDAY:
                        fileDto = AsyncHelper.RunSync(() => _exporter.ExportToFilePlanWorkDay(args.WorkPeriodSummaryInputWithPagingInput, _connectReportAppService));
                        break;

                    case ReportNames.MONTHLYSTORESALE:
                        fileDto = AsyncHelper.RunSync(() => _monthlySalesReportExporter.ExportMonthlyStoreSaleReport(args.MonthlyStoreSaleReportInput.GetMonthlyStoreSaleReportInput, _monthlySalesReportAppservice));
                        break;

                    case ReportNames.MEALTIMESALE:
                        fileDto = AsyncHelper.RunSync(() => _mealTimeSalesReportExporter.ExportMealTimeReport(args.MealTimeReportInput.GetMealTimeInput, _mealTimeSaleesReportAppservice));
                        break;

                    case ReportNames.MONTHLYSTORESALESTATS:
                        fileDto = AsyncHelper.RunSync(() => _monthlySalesStatsReportExporter.ExportMonthlyStoreSalesStatsReport(args.MonthlyStoreSaleStatsReportInput.GetMonthlyStoreSalesStatsReportInput, _monthlySalesStatsReportAppservice));
                        break;

                    case ReportNames.CREDITSALES:
                        fileDto = AsyncHelper.RunSync(() => _creditSalesReportExporter.ExportCreditSales(args.CreditSalesReportInput.CreditSalesInput, _creditSalesReportAppService));
                        break;


                    default:
                        break;
                }
                if (fileDto != null)
                {
                    AsyncHelper.RunSync(() =>
                        _reportAppService.UpdateFileOutputCompleted(fileDto, args.BackGroundId));
                }
            }
        }
    }

    [Serializable]
    public class ConnectReportInputArgs
    {
        public string ReportName { get; set; }
        public int BackGroundId { get; set; }
        public GetItemInput ItemInput { get; set; }
        public int TenantId { get; set; }
        public long UserId { get; set; }
        public TicketReportInput TicketReportInput { get; set; }
        public TicketSyncReportInput TicketSyncReportInput { get; set; }
        public CashAuditReportInput CashAuditReportInput { get; set; }
        public SpeedOfServiceReportInput SpeedOfServiceReportInput { get; set; }
        public CashSummaryReportInput CashSummaryReportInput { get; set; }

        public FullTaxInvoiceReportInput FullTaxInvoiceReportInput { get; set; }

        public CreditCardReportInput CreditCardReportInput { get; set; }
        public CreditSalesReportInput CreditSalesReportInput { get; set; }
        public WorkPeriodSummaryInput WorkPeriodSummaryInput { get; set; }
        public GetTicketInput TicketInput { get; set; }
        public List<ComboboxItemDto> ComboboxItemDtos { get; set; }
        public List<string> TTList { get; set; }
        public List<string> PTList { get; set; }
        public List<string> DTList { get; set; }

        public List<string> ProList { get; set; }
        public List<SharingPercentageDto> LList { get; set; }
        public GetTillAccountInput TillAccountInput { get; set; }
        public GetPlanLoggerInput PlanLoggerInput { get; set; }
        public FeedbackUnreadExportInput FeedbackUnreadExportInput { get; set; }

        public TopDownSellReportInput TopDownSellReportInput { get; set; }
        public ProductMixInput ProductMixInput { get; set; }
        public SalesPromotionInput SalesPromotionInput { get; set; }

        public ABBReprintReportInputDto ABBReprintReportInput { get; set; }
        public TimeAttendanceReportInput TimeAttendanceReportInput { get; set; }
        public GetReturnProductReportInput GetReturnProductReportInput { get; set; }
        public CentralShiftReportInput CentralShiftReportInput { get; set; }
        public GetAbnormalEndDayReportInput GetAbnormalEndDayReportInput { get; set; }

        public SaleTargetTicketInput SaleTargetTicketInput { get; set; }
        public List<MenuItemListDto> MenuItemList { get; set; }

        public List<LocationComparisonReportOuptut> LocationComparisons { get; set; }
        public WorkPeriodSummaryInputWithPaging WorkPeriodSummaryInputWithPagingInput { get; set; }
        public MonthlyStoreSaleReportInput MonthlyStoreSaleReportInput { get; set; }
        public MonthlySaleAdvanceReportInput MonthlySaleAdvanceReportInput { get; set; }
        public DailySalesBreakdownReportInput DailySalesBreakdownReportInput { get; set; }
        public MonthlyStoreSaleStatsReportInput MonthlyStoreSaleStatsReportInput { get; set; }
        public GetMealTimeReportInput MealTimeReportInput { get; set; }
    }

    public class TicketReportInput
    {
        public GetTicketInput TicketInput { get; set; }
        public List<string> PaymentTypes { get; set; }
        public List<string> TransactionTypes { get; set; }
        public List<string> TicketTags { get; set; }
    }

    public class TicketSyncReportInput
    {
        public GetTicketSyncInput TicketSyncInput { get; set; }
    }
    public class CashAuditReportInput
    {
        public CashAuditExportInput CashAuditInput { get; set; }
    }
    public class CreditCardReportInput
    {
        public GetCreditCardInput CreditCardInput { get; set; }
    }
    public class CreditSalesReportInput
    {
        public GetCreditSalesInput CreditSalesInput { get; set; }
    }
    public class SpeedOfServiceReportInput
    {
        public GetSpeedOfServiceReportInput GetSpeedOfServiceReportInput { get; set; }
    }
    public class CashSummaryReportInput
    {
        public GetCashSummaryReportInput GetCashSummaryReportInput { get; set; }
    }

    public class FullTaxInvoiceReportInput
    {
        public GetFullTaxInvoiceReportInput GetFullTaxInvoiceReportInput { get; set; }
    }

    public class MonthlyStoreSaleReportInput
    {
        public GetMonthlyStoreSaleReportInput GetMonthlyStoreSaleReportInput { get; set; }
    }
    public class MonthlySaleAdvanceReportInput
    {
        public GetTicketInput GetMonthlySaleAdvanceReportInput { get; set; }
    }
    public class DailySalesBreakdownReportInput
    {
        public GetTicketInput GetDailySalesBreakdownReportInput { get; set; }
    }
    public class MonthlyStoreSaleStatsReportInput
    {
        public GetMonthlyStoreSalesStatsReportInput GetMonthlyStoreSalesStatsReportInput { get; set; }
    }

    public class GetMealTimeReportInput
    {
        public GetMealTimeInput GetMealTimeInput { get; set; }
    }
}