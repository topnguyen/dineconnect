﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.Threading;
using DinePlan.DineConnect.Connect.OrderTag.Dtos;
using DinePlan.DineConnect.Connect.OrderTag.Exporting;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Report;

namespace DinePlan.DineConnect.Job.OrderTagReportJob
{
    public class OrderTagReportJob : BackgroundJob<GetOrderTagReportInput>, ITransientDependency
    {
        private readonly IOrderTagGroupExcelExporter _exporter;
        private readonly IReportBackgroundAppService _reportAppService;
        private readonly IConnectReportAppService _connectReportService;

        public OrderTagReportJob(IOrderTagGroupExcelExporter exporter, IReportBackgroundAppService reportBackgroundAppService, IConnectReportAppService connectReportAppService)
        {
            _exporter = exporter;
            _reportAppService = reportBackgroundAppService;
            _connectReportService = connectReportAppService;
        }

        [UnitOfWork]
        public override void Execute(GetOrderTagReportInput args)
        {
            if (args != null)
            {
                FileDto fileDto = null;
                switch (args.ReportName)
                {
                    case ReportNames.ORDERTAGS:
                        fileDto = AsyncHelper.RunSync(() => _exporter.ExportOrderTag(args, _connectReportService));
                        break;

                    default:
                        break;
                }
                if (fileDto != null)
                {
                    AsyncHelper.RunSync(() => _reportAppService.UpdateFileOutputCompleted(fileDto, args.BackGroundId));
                }
            }
        }
    }
}