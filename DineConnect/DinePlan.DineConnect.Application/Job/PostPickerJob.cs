﻿using System;
using System.Linq;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Threading;
using Abp.Threading.BackgroundWorkers;
using Abp.Threading.Timers;
using Castle.Core.Logging;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Connect.WorkPeriod;
using Nito.AsyncEx.Synchronous;
using DinePlan.DineConnect.Connect.FutureData;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Job
{
    public class PostPickerJob : PeriodicBackgroundWorkerBase, ISingletonDependency
    {
        private readonly ILogger _logger;
        private readonly IPostAppService _postAppService;
        private readonly ITicketAppService _ticketAppService;
        private readonly IFutureDataAppService _futureDataAppService;
        private readonly IRepository<PostData> _postRepository;
        private readonly IWorkPeriodAppService _workPeriodAppService;

        public PostPickerJob(AbpTimer timer, ILogger logger,
            IPostAppService postAppService, IRepository<PostData> postRepository, ITicketAppService ticketAppService, IWorkPeriodAppService workPeriodAppService,
            IFutureDataAppService futureDataAppService)
            : base(timer)
        {
            _logger = logger;
            _postAppService = postAppService;
            _ticketAppService = ticketAppService;
            _postRepository = postRepository;
            _workPeriodAppService = workPeriodAppService;
            _futureDataAppService = futureDataAppService;
            Timer.Period = 5000;
        }

        [UnitOfWork]
        protected override void DoWork()
        {
            //Timer.Period = 100000;
            #region Push Tickets
            var allOutputs = AsyncHelper.RunSync(() =>
                _postAppService.PullContents(new PostDataInput()
                {
                    ContentType = Convert.ToInt32(PostContentType.Ticket),
                    MaxResultCount = 20,
                    SkipCount = 0,
                    TotalCount = 20
                }));

            if (allOutputs.Items.Any())
            {
                foreach (var myTicket in allOutputs.Items)
                {
                    _postAppService.SyncTicket(myTicket.Id);
                }
            }

            #endregion
            #region Work Periods
            allOutputs = AsyncHelper.RunSync(() =>
                _postAppService.PullContents(new PostDataInput()
                {
                    ContentType = Convert.ToInt32(PostContentType.WorkPeriod),
                    MaxResultCount = 20,
                    SkipCount = 0,
                    TotalCount = 20
                }));

            if (allOutputs.Items.Any())
            {
                foreach (var myTicket in allOutputs.Items)
                {
                    var output = AsyncHelper.RunSync(() =>
                         _workPeriodAppService.SyncWorkPeriod(new PostInput()
                         {
                             ContentType = Convert.ToInt32(PostContentType.WorkPeriod),
                             Contents = myTicket.Contents
                         }));

                    if (output > 0)
                    {
                        var myRe = _postRepository.Get(myTicket.Id);
                        myRe.Processed = true;
                    }
                }
            }

            #endregion

            #region
            var allFutureDataToBeUpdated = AsyncHelper.RunSync(() =>
               _futureDataAppService.PullFutureDataTaxesToBeUpdated());

            if (allFutureDataToBeUpdated.Items.Any())
            {
                foreach (var dinePlanTax in allFutureDataToBeUpdated.Items)
                {
                    AsyncHelper.RunSync(() =>
                         _futureDataAppService.FutureDataUpdationInBackground(new IdInput()
                         {
                             Id = dinePlanTax.Id.Value
                         }));
                }
            }

            #endregion

        }
    }
}