﻿using System;
using Abp.BackgroundJobs;
using Abp.Dependency;
using DinePlan.DineConnect.Connect;

namespace DinePlan.DineConnect.Job.Mail
{
    public class SendEmailJob : BackgroundJob<ReceiptEmailArgs>, ITransientDependency
    {
        private readonly IConnectMessageSender _emailer;

        public SendEmailJob(IConnectMessageSender emailer)
        {
            _emailer = emailer;
        }
        public override void Execute(ReceiptEmailArgs args)
        {
            if(args.EmailType.Equals("RECEIPT"))
                _emailer.SendEmailReceipt(args.Email, args.TicketNumber, args.LocationId);
            else  if(args.EmailType.Equals("WORKPERIOD"))
                _emailer.SendEmail(args.Email, args.EmailSubject, args.EmailContents);


        }
    }
    [Serializable]
    public class ReceiptEmailArgs 
    {
        public string TicketNumber { get; set; }
        public int LocationId { get; set; }
        public string Email{ get; set; }
        public string EmailContents{ get; set; }
        public string EmailSubject{ get; set; }

        public string EmailType{ get; set; }

        public ReceiptEmailArgs()
        {
            EmailType = "RECEIPT";
        }



    }
}
