﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Connect;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Engage;

namespace DinePlan.DineConnect.Job
{
    public class ReceiptMessageJob : BackgroundJob<ReceiptMessageArgs>, ITransientDependency
    {
        private readonly IConnectMessageSender _emailer;

        public ReceiptMessageJob(IConnectMessageSender emailer)
        {
            _emailer = emailer;
        }
        public override void Execute(ReceiptMessageArgs args)
        {
            _emailer.SendMessageReceipt(args.Phone, args.TicketNumber, args.LocationId);
        }
    }
    [Serializable]
    public class ReceiptMessageArgs
    {
        public string TicketNumber { get; set; }
        public int LocationId { get; set; }
        public string Phone{ get; set; }


    }
}
