﻿using System;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Engage;
using DinePlan.DineConnect.Engage.Member;

namespace DinePlan.DineConnect.Job.Engage
{
    public class EngageJob : BackgroundJob<InputMemberPointCalculation>, ITransientDependency
    {
        private readonly IMemberAppService _memberAppService;

        public EngageJob(IMemberAppService memberAppService)
        {
            _memberAppService = memberAppService;
        }
        

        public override void Execute(InputMemberPointCalculation args)
        {
            _memberAppService.CreateMemberAndPoints(args);
        }
    }

    [Serializable]
    public class InputMemberPointCalculation
    {
        public string TicketEntities { get; set; }
        public int LocationId { get; set; }
        public int TenantId { get; set; }
        public decimal TotalAmount { get; set; }
        public string TicketNumber { get; set; }
        public DateTime CreatedTime { get; set; }
        public bool CalOnOrder { get; set; }

        public bool OnBoardMember { get; set; }
    }
}
