﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using DinePlan.DineConnect.Connect.ConnectCard.Card;
using DinePlan.DineConnect.Connect.ConnectCard.Card.Dtos;

namespace DinePlan.DineConnect.Job.Connect
{
    public class GenerateConnectCardJob : BackgroundJob<GenerateCardsInputDto>, ITransientDependency
    {
        private readonly IConnectCardAppService _connectCardAppService;

        public GenerateConnectCardJob(IConnectCardAppService connectCardAppService)
        {
            _connectCardAppService = connectCardAppService;
        }

        public override void Execute(GenerateCardsInputDto args)
        {
            _connectCardAppService.GenerateConnectCardsInBackground(args);
        }
    }
}