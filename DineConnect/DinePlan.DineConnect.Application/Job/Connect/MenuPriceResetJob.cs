﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.Threading;
using Abp.Threading.BackgroundWorkers;
using Abp.Threading.Timers;
using Castle.Core.Logging;
using DinePlan.DineConnect.Connect.FutureData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Job.Connect
{
    public class MenuPriceResetJob : IBackgroundJob<MenuPriceResetJobArgs>, ISingletonDependency
    {
        private readonly ILogger _logger;
        private readonly IFutureDataAppService _futureAppService;

        public MenuPriceResetJob(ILogger logger, IFutureDataAppService futureAppService)
        {
            _logger = logger;
            _futureAppService = futureAppService;
        }

        public void Execute(MenuPriceResetJobArgs args)
        {
            _logger.Info($"Start running MenuPriceReset job at {DateTime.Now}");
            AsyncHelper.RunSync(() => _futureAppService.UpdateItemPriceByFuture());
        }        
    }
}
