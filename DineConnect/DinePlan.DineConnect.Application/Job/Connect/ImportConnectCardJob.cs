﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Threading;
using DinePlan.DineConnect.Connect.ConnectCard.Card;
using DinePlan.DineConnect.Connect.ConnectCard.Card.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Job.Connect
{
    public class ImportConnectCardJob : BackgroundJob<ImportConnectCardJobArgs>, ITransientDependency
    {
        private readonly IConnectCardAppService _connectCardAppService;

        public ImportConnectCardJob(IConnectCardAppService connectCardAppService)
        {
            _connectCardAppService = connectCardAppService;
        }

        public override void Execute(ImportConnectCardJobArgs args)
        {
            AsyncHelper.RunSync(() =>
            _connectCardAppService.ImportConnectCardInBackGround(new ImportConnectCardsInBackgroundInputDto
            {
                ConnectCard=args.ConnectCard,
                FileName=args.FileName,
                Active=args.Active,
                TenantId=args.TenantId,
                TenantName=args.TenantName,
                JobName=args.JobName,
                ImportCardDataDetail=args.ImportCardDataDetail
            })
            );
           
        }
    }
    [Serializable]
    public class ImportConnectCardJobArgs
    {
        public List<ConnectCardEditDto> ConnectCard { get; set; }
        public string FileName { get; set; }
        public bool Active { get; set; }
        public int TenantId { get; set; }
        public string TenantName { get; set; }
        public string JobName { get; set; }
        public ImportCardDataDetailEditDto ImportCardDataDetail { get; set; }
    }
}
