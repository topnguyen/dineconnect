﻿using System;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using DinePlan.DineConnect.Addon;
using DinePlan.DineConnect.Addons.Odoo;
using DinePlan.DineConnect.Addons.Xero;
using DinePlan.DineConnect.External.Delivery.Dto;

namespace DinePlan.DineConnect.Job.Connect
{
    public class ConnectJob : BackgroundJob<ConnectJobArgs>, ITransientDependency
    {
        private readonly IOdooAppService _odooService;
        private IRepository<ConnectAddOn> _addons;
        private readonly IXeroAppService _xeroAppService;


        public ConnectJob(IOdooAppService odooService, IXeroAppService xeroAppService, IRepository<ConnectAddOn> addons)
        {
            _odooService = odooService;
            _xeroAppService = xeroAppService;

            _addons = addons;

        }

        [UnitOfWork]
        public override void Execute(ConnectJobArgs args)
        {
         
            var odooAvailable = _addons.FirstOrDefault(a => a.Name.Equals("Odoo"));
            if (odooAvailable != null)
            {
                if (args.WorkPeriodId > 0)
                    _odooService.SyncSales(args.WorkPeriodId);

            }

            var xeroAvailable = _addons.FirstOrDefault(a => a.Name.Equals("Xero") && a.TenantId == args.TenantId);
            if (xeroAvailable != null)
            {
                if (args.WorkPeriodId > 0)
                    _xeroAppService.SyncSales(new ConnectJobArgs()
                    {
                        WorkPeriodId = args.WorkPeriodId,
                        TenantId = args.TenantId
                    });

            }
        }
    }
    [Serializable]
    public class ConnectJobArgs
    {
        public int WorkPeriodId { get; set; }
        public int TenantId { get; set; }
    }
}
