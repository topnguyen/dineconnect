﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Connect.Master.Dtos;
using System;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.House.Transaction.Dtos;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Common
{
    public interface IEngageLookAppService : IApplicationService
    {
        Task<ListResultOutput<ComboboxItemDto>> GetVoucherTypes();
    }
}