﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.MultiTenancy;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Departments;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Tag;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Editions;
using DinePlan.DineConnect.House;
using DinePlan.DineConnect.House.Master;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.House.Transaction;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Card;
using DinePlan.DineConnect.Connect.Reason;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations.Dtos;

namespace DinePlan.DineConnect.Common.Impl
{
    public class CommonLookupAppService : DineConnectAppServiceBase, ICommonLookupAppService
    {
        private readonly EditionManager _editionManager;
        private readonly ICategoryAppService _categoryAppService;
        private readonly ILocationAppService _locationAppService;
        private readonly IUnitAppService _unitAppService;
        private readonly IRecipeGroupAppService _recipegroupAppService;
        private readonly IRecipeAppService _recipeAppService;
        private readonly IMaterialGroupAppService _materialgroupAppService;
        private readonly IMaterialAppService _materialAppService;
        private readonly ITaxAppService _taxAppService;
        private readonly ISalesTaxAppService _salestaxAppService;
        private readonly IRepository<Material> _materialRepo;
        private readonly IAdjustmentAppService _adjustmentAppService;
        private readonly ICompanyAppService _companyAppService;
        private readonly ITemplateAppService _templateAppService;
        private readonly IRepository<MenuItemPortion> _menuitemportionRepo;
        private readonly IRepository<MenuItem> _menuitemRepo;
        private readonly IRepository<TransactionOrderTag> _transactionOrderTagRepo;
        private readonly IRepository<MaterialGroupCategory> _materialGroupCategoryRepo;
        private readonly IBrandAppService _brandAppService;
        private readonly IDepartmentAppService _departmentAppService;
        private readonly IRepository<MaterialMenuMapping> _materialmenumappingRepo;
        private readonly IMenuItemAppService _menuAppService;
        private readonly IScreenMenuAppService _screenMenuAppService;
        private readonly IPriceTagAppService _priceTagAppService;
        private readonly IRepository<Terminal> _terminalRepository;
        private readonly IRepository<ConnectCardType> _connectCardTypeRepo;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<PaymentType> _paymentRepository;
        private readonly IRepository<ProductGroup> _productGroupRepository;
        private readonly IRepository<PlanReason> _reasonRepository;

        public CommonLookupAppService(EditionManager editionManager
            , ICategoryAppService categoryAppService
            , ILocationAppService locationAppService
            , IUnitAppService unitAppService
            , IRecipeGroupAppService recipegroupAppService
            , IRecipeAppService recipeAppService
            , IMaterialGroupAppService materialgroupAppService
            , IMaterialAppService materialAppService
            , ITaxAppService taxAppService
            , ISalesTaxAppService salestaxAppService
            , IBrandAppService brandAppService
            , IRepository<Material> materialRepo
            , IMenuItemAppService menuAppService
            , IRepository<TransactionOrderTag> transactionOrderTagRepo
            , IAdjustmentAppService adjustmentAppService
            , ICompanyAppService companyAppService
            , ITemplateAppService templateAppService
            , IRepository<MenuItemPortion> menuitemportionRepo
            , IRepository<MenuItem> menuitemRepo
            , IRepository<MaterialGroupCategory> materialGroupCategoryRepo
            , IDepartmentAppService departmentAppService
            , IRepository<MaterialMenuMapping> materialmenumappingRepo
            , IScreenMenuAppService screenMenuAppService
            , IPriceTagAppService priceTagAppService
            , IRepository<Terminal> terminalRepository
            , IRepository<User, long> userRepository
            , IRepository<ConnectCardType> connectTypeRepo
            , IRepository<PaymentType> paymentRepository
            , IRepository<ProductGroup> productGroupRepository
            , IRepository<PlanReason> reasonRepository
            )
        {
            _editionManager = editionManager;
            _categoryAppService = categoryAppService;
            _locationAppService = locationAppService;
            _unitAppService = unitAppService;
            _recipegroupAppService = recipegroupAppService;
            _recipeAppService = recipeAppService;
            _materialgroupAppService = materialgroupAppService;
            _materialAppService = materialAppService;
            _taxAppService = taxAppService;
            _salestaxAppService = salestaxAppService;
            _materialRepo = materialRepo;
            _brandAppService = brandAppService;
            _adjustmentAppService = adjustmentAppService;
            _companyAppService = companyAppService;
            _templateAppService = templateAppService;
            _menuitemportionRepo = menuitemportionRepo;
            _menuitemRepo = menuitemRepo;
            _transactionOrderTagRepo = transactionOrderTagRepo;
            _materialGroupCategoryRepo = materialGroupCategoryRepo;
            _menuAppService = menuAppService;
            _departmentAppService = departmentAppService;
            _materialmenumappingRepo = materialmenumappingRepo;
            _screenMenuAppService = screenMenuAppService;
            _priceTagAppService = priceTagAppService;
            _terminalRepository = terminalRepository;
            _userRepository = userRepository;
            _connectCardTypeRepo = connectTypeRepo;
            _paymentRepository = paymentRepository;
            _productGroupRepository = productGroupRepository;
            _reasonRepository = reasonRepository;
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetFieldTypes()
        {
            List<ComboboxItemDto> returnList = new List<ComboboxItemDto>();

            var fieldTypes =
             new[] { "String", "WideString", "Number", "Query", "Date", "ReadOnly" };

            int i = 0;
            foreach (var fieldType in fieldTypes)
            {
                returnList.Add(new ComboboxItemDto()
                {
                    DisplayText = fieldType,
                    Value = i.ToString()
                });
                i++;
            }

            return new ListResultOutput<ComboboxItemDto>(returnList);
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetEditionsForCombobox()
        {
            var editions = await _editionManager.Editions.ToListAsync();
            return new ListResultOutput<ComboboxItemDto>(
                editions.Select(e => new ComboboxItemDto(e.Id.ToString(), e.DisplayName)).ToList()
                );
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetCategoriesForCombobox(string group = "")
        {
            var roles = await _categoryAppService.GetCategories();
            var groups = await GetProductGroupCombobox();
            if (!string.IsNullOrEmpty(group))
            {
                var myGroup = groups.Items.FirstOrDefault(a => a.Value.Equals(group));
                if (myGroup != null)
                {
                    roles.Items
                        = roles.Items.Where(a => a.ProductGroupId.HasValue && a.ProductGroupId.Value.ToString() == myGroup.Value).ToList();
                }
            }

            return
                new ListResultOutput<ComboboxItemDto>(
                    roles.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).OrderBy(e => e.DisplayText).ToList());
        }

        public async Task<List<ComboboxItemDto>> GetDepartments(string departGroupCode = "")
        {
            List<ComboboxItemDto> items = new List<ComboboxItemDto>();
            var departments = await _departmentAppService.GetDepartments();
            var departmentGroups = await _departmentAppService.GetDepartmentGroups();

            var roleItems = departments.Items.ToList();
            if (!string.IsNullOrEmpty(departGroupCode))
            {
                var myGroup = departmentGroups.Items.LastOrDefault(a => a.Code.Equals(departGroupCode));
                if (myGroup != null)
                {
                    roleItems = roleItems.Where(a => a.DepartmentGroupId != null && a.DepartmentGroupId == myGroup.Id)
                        .ToList();
                }
            }
            foreach (var myDept in roleItems)
            {
                items.Add(new ComboboxItemDto(myDept.Id.ToString(), myDept.Name));
            }
            return items;
        }

        public async Task<List<ListComboItem>> GetDepartmentsFilter()
        {           
            List<ListComboItem> result = new List<ListComboItem>();
            var departments = await _departmentAppService.GetDepartments();
            var departmentGroups = await _departmentAppService.GetDepartmentGroups();
            foreach (var group in departmentGroups.Items)
            {
                List<ComboboxItemDto> items = new List<ComboboxItemDto>();
                var roleItems = departments.Items.ToList();
                if (!string.IsNullOrEmpty(group.Code))
                {
                    var myGroup = departmentGroups.Items.LastOrDefault(a => a.Code.Equals(group.Code));
                    if (myGroup != null)
                    {
                        roleItems = roleItems.Where(a => a.DepartmentGroupId != null && a.DepartmentGroupId == myGroup.Id).ToList();
                        foreach (var myDept in roleItems)
                        {
                            items.Add(new ComboboxItemDto(myDept.Id.ToString(), myDept.Name));
                        }
                        result.Add(new ListComboItem
                        {
                            DepartmentGroup = group.Code,
                            ListItem = items
                        });
                    }

                }
            }
            return result;
        }

        public async Task<List<ComboboxItemDto>> GetDepartmentGroups()
        {
            var roles = await _departmentAppService.GetDepartmentGroups();
            return
                new List<ComboboxItemDto>(
                    roles.Items.Select(e => new ComboboxItemDto(e.Code.ToString(), e.Name)).OrderBy(e => e.DisplayText).ToList());
        }



        public async Task<ListResultOutput<ComboboxItemDto>> GetMenuPortionForCombobox(IdInput input)
        {
            List<ProductListDto> menus = new List<ProductListDto>();

            if (input.Id == 0)
            {
                menus = await (from menuportion in _menuitemportionRepo.GetAll()
                               join menu in _menuitemRepo.GetAll().Where(a => a.ProductType == 1)
                               on menuportion.MenuItemId equals menu.Id
                               select new ProductListDto
                               {
                                   PortionId = menuportion.Id,
                                   PortionName = string.Concat(menu.Name, " - ", menuportion.Name),
                                   AliasCode = menu.AliasCode,
                                   AliasName = menu.AliasName
                               }).ToListAsync();
            }
            else
            {
                int[] menuMap = _materialmenumappingRepo.GetAll().Select(t => t.PosMenuPortionRefId).Distinct().ToArray();

                menus = await (from menuportion in _menuitemportionRepo.GetAll().Where(t => !menuMap.Contains(t.Id))
                               join menu in _menuitemRepo.GetAll().Where(a => a.ProductType == 1)
                               on menuportion.MenuItemId equals menu.Id
                               select new ProductListDto()
                               {
                                   PortionId = menuportion.Id,
                                   PortionName = string.Concat(menu.Name, "-", menuportion.Name),
                                   AliasCode = menu.AliasCode,
                                   AliasName = menu.AliasName
                               }).ToListAsync();
            }

            return
                new ListResultOutput<ComboboxItemDto>(
                    menus.Select(e => new ComboboxItemDto(e.PortionId.ToString(), e.PortionName)).ToList());
        }

        public async Task<ListResultOutput<SimpleMenuOutput>> GetSimplePortionsForCombobox(IdInput input)
        {
            List<ProductListDto> menus = new List<ProductListDto>();

            if (input.Id == 0)
            {
                menus = await (from menuportion in _menuitemportionRepo.GetAll()
                               join menu in _menuitemRepo.GetAll().Where(a => a.ProductType == 1)
                               on menuportion.MenuItemId equals menu.Id
                               select new ProductListDto
                               {
                                   PortionId = menuportion.Id,
                                   PortionName = string.Concat(menu.Name, " - ", menuportion.Name),
                                   AliasCode = menu.AliasCode,
                                   AliasName = menu.AliasName
                               }).ToListAsync();
            }
            else
            {
                int[] menuMap = _materialmenumappingRepo.GetAll().Select(t => t.PosMenuPortionRefId).Distinct().ToArray();

                menus = await (from menuportion in _menuitemportionRepo.GetAll().Where(t => !menuMap.Contains(t.Id))
                               join menu in _menuitemRepo.GetAll().Where(a => a.ProductType == 1)
                               on menuportion.MenuItemId equals menu.Id
                               select new ProductListDto()
                               {
                                   PortionId = menuportion.Id,
                                   PortionName = string.Concat(menu.Name, "-", menuportion.Name),
                                   AliasCode = menu.AliasCode,
                                   AliasName = menu.AliasName
                               }).ToListAsync();
            }

            return
                new ListResultOutput<SimpleMenuOutput>(
                    menus.Select(e => new SimpleMenuOutput
                    {
                        DisplayText = e.PortionName,
                        Value = e.PortionId,
                        AliasCode = e.AliasCode,
                        AlternateName = e.AliasName
                    }).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetMenuItemsForCombobox()
        {
            var roles = await (from menuportion in _menuitemRepo.GetAll()
                               select new ProductListDto
                               {
                                   PortionId = menuportion.Id,
                                   PortionName = menuportion.Name
                               }).ToListAsync();

            return
                new ListResultOutput<ComboboxItemDto>(
                    roles.Select(e => new ComboboxItemDto(e.PortionId.ToString(), e.PortionName)).ToList());
        }
        public async Task<ListResultOutput<ComboboxItemDto>> GetTransactionOrderTagsForCombobox()
        {
            var roles = await (from transactionOrderTag in _transactionOrderTagRepo.GetAll()
                               select new TransactionOrderTagDto
                               {
                                   Id = transactionOrderTag.Id,
                                   TagName = transactionOrderTag.TagName
                               }).ToListAsync();

            return
                new ListResultOutput<ComboboxItemDto>(
                    roles.Select(e => new ComboboxItemDto(e.Id.ToString(), e.TagName)).ToList());
        }
        public async Task<ListResultOutput<ComboboxItemDto>> GetMenuItemsForComboboxByLocation(int id)
        {
            var allItems = await _menuAppService.GetAllListItems(new Connect.Menu.Dtos.GetMenuItemLocationInput()
            {
                LocationId = id
            }, false);
            var roles = (from menuportion in allItems.Items
                         let menuportionId = menuportion.Id
                         where menuportionId != null
                         select new ProductListDto
                         {
                             PortionId = menuportionId,
                             PortionName = menuportion.Name
                         });
            return
                new ListResultOutput<ComboboxItemDto>(
                    roles.Select(e => new ComboboxItemDto(e.PortionId.ToString(), e.PortionName)).ToList());
        }

        public async Task<PagedResultOutput<NameValueDto>> FindUsers(FindUsersInput input)
        {
            if (AbpSession.MultiTenancySide == MultiTenancySides.Host && input.TenantId.HasValue)
            {
                CurrentUnitOfWork.SetFilterParameter(AbpDataFilters.MayHaveTenant, AbpDataFilters.Parameters.TenantId, input.TenantId.Value);
            }

            var query = UserManager.Users
                .WhereIf(
                    !input.Filter.IsNullOrWhiteSpace(),
                    u =>
                        u.Name.Contains(input.Filter) ||
                        u.Surname.Contains(input.Filter) ||
                        u.UserName.Contains(input.Filter) ||
                        u.EmailAddress.Contains(input.Filter)
                );

            var userCount = await query.CountAsync();
            var users = await query
                .OrderBy(u => u.Name)
                .ThenBy(u => u.Surname)
                .PageBy(input)
                .ToListAsync();

            return new PagedResultOutput<NameValueDto>(
                userCount,
                users.Select(u =>
                    new NameValueDto(
                        u.Name + " " + u.Surname + " (" + u.EmailAddress + ")",
                        u.Id.ToString()
                        )
                    ).ToList()
                );
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetUnitForCombobox(NullableIdInput input)
        {
            var lst = await _unitAppService.GetNames();

            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }

        //public async Task<ListResultOutput<ComboboxItemDto>> GetUnitForMaterialLinkCombobox(IdInput input)
        //{
        //   // var lst = await (from _unitAppService.GetMaterialUnitNames(input.Id.Value);

        //    var lst = await (from u in _unitRepo.GetAll()
        //                         join mu in _materialUnitLinkRepo.GetAll().Where(a => a.MaterialRefId == input.Id)
        //                         on u.Id equals mu.UnitRefId
        //                         select new ComboboxItemDto
        //                         {
        //                             Value = u.Id.ToString(),
        //                             DisplayText = u.Name
        //                         }).ToListAsync();

        //    if (lst.Count == 0)
        //    {
        //      lst=   await (from u in _unitRepo.GetAll()
        //               join mu in _materialRepo.GetAll().Where(a => a.Id == input.Id)
        //               on u.Id equals mu.DefaultUnitId
        //               select new ComboboxItemDto
        //               {
        //                   Value = u.Id.ToString(),
        //                   DisplayText = u.Name
        //               }).ToListAsync();
        //    }
        //    //return new ListResultOutput<UnitListDto>(lstUnit.MapTo<List<UnitListDto>>());

        //    return new ListResultOutput<ComboboxItemDto>(lst.MapTo<List<ComboboxItemDto>>());

        //    //return lst.MapTo<ListResultOutput<ComboboxItemDto>>();

        //    //return
        //        //new ListResultOutput<ComboboxItemDto>(
        //        //    lst.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        //}

        public async Task<ListResultOutput<ComboboxItemDto>> GetRecipeGroupForCombobox()
        {
            var lst = await _recipegroupAppService.GetRecipeGroupNames();
            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.RecipeGroupName)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetRecipeForCombobox()
        {
            var lst = await _recipeAppService.GetRecipeNames();
            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.RecipeName)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetMaterialGroupForCombobox()
        {
            var lst = await _materialgroupAppService.GetMaterialGroupNames();
            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.MaterialGroupName)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetMaterialForCombobox()
        {
            var lst = await _materialAppService.GetMaterialNames();
            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.MaterialName)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetMaterialGroupCategoryByGroupId(NullableIdInput ninput)
        {
            //List<MaterialGroupCategoryEditDto> lst;
            //lst = await (from matgroupcategory in  _materialGroupCategoryRepo.GetAll()
            //        .WhereIf(!ninput.Id.HasValue, g => g.MaterialGroupRefId == ninput.Id.Value)
            //         join matgroup in _materialgroupRepo.GetAll().Where(t => t.TenantId == AbpSession.TenantId)
            //         on matgroupcategory.MaterialGroupRefId equals matgroup.Id
            //             select new MaterialGroupCategoryEditDto
            //                {
            //                    Id = matgroupcategory.Id,
            //                    MaterialGroupRefId = matgroupcategory.MaterialGroupRefId,
            //                    MaterialGroupCategoryName = matgroupcategory.MaterialGroupCategoryName,
            //                }
            //        ).ToListAsync();

            var lst = _materialGroupCategoryRepo.GetAll();
            if (ninput.Id.HasValue && ninput.Id != 0)
            {
                lst = lst.Where(t => t.MaterialGroupRefId == ninput.Id.Value);
            }
            var output = await lst.ToListAsync();
            return
                new ListResultOutput<ComboboxItemDto>(
                    output.Select(e => new ComboboxItemDto(e.Id.ToString(), e.MaterialGroupCategoryName)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetMaterialByGroupCategory(NullableIdInput ninput)
        {
            var lst = _materialRepo.GetAll();

            if (ninput.Id.HasValue && ninput.Id.Value != 0)
            {
                lst = lst.Where(t => t.MaterialGroupCategoryRefId == ninput.Id.Value);
            }
            //.WhereIf(!ninput.Id.HasValue, g => g.MaterialGroupCategoryRefId == ninput.Id.Value).ToListAsync();

            var output = await lst.ToListAsync();
            return
                new ListResultOutput<ComboboxItemDto>(
                    output.Select(e => new ComboboxItemDto(e.Id.ToString(), e.MaterialName)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetMaterialForCombobox(NullableIdInput ninput)
        {
            var lst = await _materialAppService.GetMaterialNames();
            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.MaterialName)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetMaterialExcludedFromParticularRecipeForCombobox(NullableIdInput input)
        {
            var lst = await _materialAppService.GetMaterialNames();
            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.MaterialName)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetLocationForCombobox()
        {
            var lst = await _locationAppService.GetLocations();
            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }
        public async Task<ListResultOutput<ComboboxItemDto>> GetLocationCodeForCombobox()
        {
            var lst = await _locationAppService.GetLocations();
            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Code)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetProductGroupCombobox()
        {
            var lst = await _productGroupRepository.GetAll().Select(x => new ComboboxItemDto
            {
                DisplayText = x.Name,
                Value = x.Id.ToString()
            }).ToListAsync();
            return new ListResultOutput<ComboboxItemDto>(lst);
        }
        public async Task<ListResultOutput<ComboboxItemDto>> GetPaymentTypeCombobox()
        {
            var lst = await _paymentRepository.GetAll().Select(x => new ComboboxItemDto
            {
                DisplayText = x.Name,
                Value = x.Id.ToString()
            }).ToListAsync();
            return new ListResultOutput<ComboboxItemDto>(lst);
        }
        public async Task<ListResultOutput<ComboboxItemDto>> GetReasonExchangeCombobox()
        {
            var lst = await _reasonRepository.GetAll()
                .Where(x => x.ReasonGroup == "Exchange")
                .Select(x => new ComboboxItemDto
                {
                    DisplayText = x.Reason,
                    Value = x.Id.ToString()
                }).ToListAsync();
            return new ListResultOutput<ComboboxItemDto>(lst);
        }


        public async Task<ListResultOutput<ComboboxItemDto>> GetTaxForCombobox()
        {
            var lst = await _taxAppService.GetTaxNames();
            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.TaxName)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetSalesTaxForCombobox()
        {
            var lst = await _salestaxAppService.GetSalesTaxNames();
            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.TaxName)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetAdjustmentForCombobox()
        {
            var lst = await _adjustmentAppService.GetIds();
            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Id.ToString())).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetCompanyForCombobox()
        {
            var lst = await _companyAppService.GetNames();
            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetTemplateForCombobox(GetTemplateList input)
        {
            var lst = await _templateAppService.GetTemplateDetail(input);
            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }



        public async Task<ListResultOutput<ComboboxItemDto>> GetBrandForCombobox()
        {
            var lst = await _brandAppService.GetNames();
            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetTicketTagTypes()
        {
            List<ComboboxItemDto> returnList = new List<ComboboxItemDto>();

            int i = 0;
            foreach (string name in Enum.GetNames(typeof(TicketTagGroupType)))
            {
                returnList.Add(new ComboboxItemDto()
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetLocationsForLoginUser()
        {
            var lst = await _locationAppService.GetLocationsForLoginUser();
            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetMenuItemForCombobox(string category = "")
        {
            var list = await _menuAppService.GetMenuItemNames();
            var categories = await GetCategoriesForCombobox();
            if (!string.IsNullOrEmpty(category))
            {
                var cate = categories.Items.FirstOrDefault(x => x.Value == category);
                if (cate != null)
                {
                    list.Items =
                        list.Items.Where(a => a.CategoryId.HasValue && a.CategoryId.Value.ToString() == cate.Value).ToList();
                }
            }
            return
                new ListResultOutput<ComboboxItemDto>(
                    list.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetPriceTagForCombobox(FilterInputDto input)
        {
            var list = await _priceTagAppService.GetPriceTagNames(input.FutureData);
            return
                new ListResultOutput<ComboboxItemDto>(
                    list.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetScreenMenuForCombobox(FilterInputDto input)
        {
            var list = await _screenMenuAppService.GetScreenMenuNames(input.FutureData);
            return
                new ListResultOutput<ComboboxItemDto>(
                    list.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetTerminalForCombobox()
        {
            var list = await _terminalRepository.GetAllListAsync();
            return
                new ListResultOutput<ComboboxItemDto>(
                    list.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).OrderBy(e => e.DisplayText).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetUserForCombobox()
        {
            var list = await _userRepository.GetAllListAsync();
            return
                new ListResultOutput<ComboboxItemDto>(
                    list.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).OrderBy(e => e.DisplayText).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetMenuTagForCombobox()
        {
            var list = await _menuitemRepo.GetAll().Select(m => m.Tag).ToListAsync();
            if (list.Any())
            {
                var result = list.SelectMany(l => l.Split(",")).Distinct().ToList();
                return
                    new ListResultOutput<ComboboxItemDto>(
                        result.Select(e => new ComboboxItemDto(e, e)).OrderBy(e => e.DisplayText).ToList());
            }

            return null;
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetCardTypes()
        {
            var list = await _connectCardTypeRepo.GetAllListAsync();
            return
                new ListResultOutput<ComboboxItemDto>(
                    list.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).OrderBy(e => e.DisplayText).ToList());
        }
    }
}