﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using DinePlan.DineConnect.Connect.Departments;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Till;
using DinePlan.DineConnect.Connect.TillAccount;
using DinePlan.DineConnect.Connect.User;
using DinePlan.DineConnect.Engage.MemberLoyaltyProgram.Dtos;
using DinePlan.DineConnect.General;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Report;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.ConnectCard.Card;
using DinePlan.DineConnect.Connect.Processor;
using DinePlan.DineConnect.Connect.Prints.DinePlan.DineConnect.Connect.Processor;
using DinePlan.DineConnect.Hr.Master.Dtos;
using DinePlan.DineConnect.Connect.Departments.Dtos;

namespace DinePlan.DineConnect.Common.Impl
{
    public class ConnectLookupAppService : DineConnectAppServiceBase, IConnectLookupAppService
    {
        private readonly IDepartmentAppService _dpService;
        private readonly IMenuItemAppService _menuItemAppService;
        private readonly IScreenMenuAppService _screenMenuAppService;
        private readonly IPaymentTypeAppService _paytService;
        private readonly IPromotionAppService _promotionService;
        private readonly IPriceTagAppService _ptService;
        private readonly IDinePlanUserRoleAppService _roleAppService;
        private readonly ITicketTagGroupAppService _tagService;
        private readonly ITillAccountAppService _tillAccountService;
        private readonly ITransactionTypeAppService _ttService;
        private readonly IConnectCardAppService _connectCardService;
        private readonly INumeratorAppService _numeratorAppService;
        private readonly ITicketTypeConfigurationAppService _ticketTypeConfigurationAppService;
        

        public ConnectLookupAppService(IDinePlanUserRoleAppService roleAppService, 
            IPromotionAppService promotionService,
            IPriceTagAppService ptService,
            IPaymentTypeAppService paytService,
            ITransactionTypeAppService ttService, 
            ITillAccountAppService tillAccountService,
            IMenuItemAppService menuItemAppService,
            IDepartmentAppService dpService, 
            ITicketTagGroupAppService tagService,
            IConnectCardAppService connectCardService,
            INumeratorAppService numeratorAppService,
            IScreenMenuAppService screenMenuAppService,
            ITicketTypeConfigurationAppService ticketTypeConfigurationAppService)
        {
            _roleAppService = roleAppService;
            _ttService = ttService;
            _ptService = ptService;
            _paytService = paytService;
            _dpService = dpService;
            _promotionService = promotionService;
            _tillAccountService = tillAccountService;
            _menuItemAppService = menuItemAppService;
            _tagService = tagService;
            _connectCardService = connectCardService;
            _numeratorAppService = numeratorAppService;
            _screenMenuAppService = screenMenuAppService;
            _ticketTypeConfigurationAppService = ticketTypeConfigurationAppService;
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetUserRoles()
        {
            var lst = await _roleAppService.GetIds();

            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetTagTypes()
        {
            var returnList = new List<ComboboxItemDto>();

            var fieldTypes =
                new[] { "Normal", "Date" };

            var i = 0;
            foreach (var fieldType in fieldTypes)
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = fieldType,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetTransactionTypesForCombobox()
        {
            var roles = await _ttService.GetAllItems();
            return
                new ListResultOutput<ComboboxItemDto>(
                    roles.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetPaymentTypes()
        {
            var roles = await _paytService.GetAllItems();
            return
                new ListResultOutput<ComboboxItemDto>(
                    roles.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetDepartments()
        {
            var roles = await _dpService.GetIds();
            return
                new ListResultOutput<ComboboxItemDto>(
                    roles.Items.OrderBy(x=>x.SortOrder).Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }  
        
        public async Task<ListResultOutput<ComboboxItemDto>> GetDepartmentGroups()
        {
            var input = new GetDepartmentGroupInput()
            {
                IsDeleted = false,
                MaxResultCount = 1000,
                SkipCount = 0
            };
            var departmentGroup = await _dpService.GetAllDepartmentGroup(input);
            var result = departmentGroup.Items.Select(x => new ComboboxItemDto()
            {
                Value = x.Id.ToString(),
                DisplayText = x.Name
            }).ToList();
            return new ListResultOutput<ComboboxItemDto>(result);
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetTicketTags()
        {
            var roles = await _tagService.GetAllTags();
            return
                new ListResultOutput<ComboboxItemDto>(
                    roles.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetPromotionTypes()
        {
            var returnList = new List<ComboboxItemDto>();

            var i = 1;
            foreach (var name in Enum.GetNames(typeof(PromotionTypes)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }


        public async Task<ListResultOutput<ComboboxItemDto>> GetDays()
        {
            return new ListResultOutput<ComboboxItemDto>(new List<ComboboxItemDto>
            {
                new ComboboxItemDto{DisplayText = DayOfWeek.Monday.ToString(), Value = ((int)DayOfWeek.Monday).ToString()},
                new ComboboxItemDto{DisplayText = DayOfWeek.Tuesday.ToString(), Value = ((int)DayOfWeek.Tuesday).ToString()},
                new ComboboxItemDto{DisplayText = DayOfWeek.Wednesday.ToString(), Value = ((int)DayOfWeek.Wednesday).ToString()},
                new ComboboxItemDto{DisplayText = DayOfWeek.Thursday.ToString(), Value = ((int)DayOfWeek.Thursday).ToString()},
                new ComboboxItemDto{DisplayText = DayOfWeek.Friday.ToString(), Value = ((int)DayOfWeek.Friday).ToString()},
                new ComboboxItemDto{DisplayText = DayOfWeek.Saturday.ToString(), Value = ((int)DayOfWeek.Saturday).ToString()},
                new ComboboxItemDto{DisplayText = DayOfWeek.Sunday.ToString(), Value = ((int)DayOfWeek.Sunday).ToString()},
            });
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetMonthDays()
        {
            var returnList = new List<ComboboxItemDto>();

            for (var i = 1; i <= 31; i++)
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = i.ToString(),
                    Value = i.ToString()
                });
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetPaymentProcessors()
        {
            var returnList = new List<ComboboxItemDto>();
            var i = 1;
            foreach (var name in PaymentTypeProcessors.Processors)
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetNumerators()
        {
            var returnList = new List<ComboboxItemDto>();
            var i = 1;
            var numerators = _numeratorAppService.GetAll(new Connect.Master.Dtos.NumeratorInput() { IsDeleted = false });
            foreach (var item in numerators.Items)
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = item.Name,
                    Value = item.Id.ToString()
                });
                i++;
            }
           return new ListResultOutput<ComboboxItemDto>(returnList);
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetTicketTypes()
        {
            var returnList = new List<ComboboxItemDto>();
            var i = 1;
            var ticketTypeConfigurations = _ticketTypeConfigurationAppService.GetAll(new Connect.Master.Dtos.GetTicketTypeConfigurationInput() { IsDeleted = false });
            foreach (var item in ticketTypeConfigurations.Items)
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = item.Name,
                    Value = item.Id.ToString()
                });
                i++;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }

        public async Task<List<FormlyType>> GetProcessorSetting(IdInput input)
        {
            var myKey = PaymentTypeProcessors.Processors[input.Id-1];
            return PaymentTypeProcessors.AllProcessors[myKey];
        }
        public async Task<ListResultOutput<ComboboxItemDto>> GetPrinterProcessors()
        {
            var returnList = new List<ComboboxItemDto>();
            foreach (var name in CustomerPrinterProcessors.Printers)
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = name.ToString()
                });
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }

       

        public async Task<List<FormlyType>> GetPrinterProcessorSetting(StringInput input)
        {
            return CustomerPrinterProcessors.AllProcessors[input.StringId];
        }
        public async Task<ListResultOutput<ComboboxItemDto>> GetTillAccountTypes()
        {
            var returnList = new List<ComboboxItemDto>();

            var i = 0;
            foreach (var name in Enum.GetNames(typeof(TillAccounTypes)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetPromotions()
        {
            var lst = await _promotionService.GetIds();
            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetTillAccounts()
        {
            var lst = await _tillAccountService.GetTillAccounts();
            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetProductTypes()
        {
            var returnList = new List<ComboboxItemDto>();

            var i = 1;
            foreach (var name in Enum.GetNames(typeof(ProductType)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetLogTypes()
        {
            var returnList = new List<ComboboxItemDto>();
            var i = 1;

            foreach (var name in DineConnectConsts.LogConsts)
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetPromotionValueTypes()
        {
            var returnList = new List<ComboboxItemDto>();

            var i = 1;
            foreach (var name in Enum.GetNames(typeof(PromotionValueTypes)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetPriceTags()
        {
            var roles = await _ptService.GetIds();
            return
                new ListResultOutput<ComboboxItemDto>(
                    roles.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }

        public async Task<ListResultOutput<IdNameDto>> GetMenuItemsForLocation(int id)
        {
            var allItems = await _menuItemAppService.GetAllListItems(new GetMenuItemLocationInput
            {
                LocationId = id
            },false);
            var roles = (from menuportion in allItems.Items
                let menuportionId = menuportion.Id
                where menuportionId != null
                select new ProductListDto
                         {
                             PortionId = menuportionId,
                             PortionName = menuportion.Name
                         });
            return
                new ListResultOutput<IdNameDto>(
                    roles.Select(e => new IdNameDto(e.PortionId, e.PortionName)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetScreenMenus()
        {
            var returnList = new List<ComboboxItemDto>();
            var i = 1;
            var menus = await _screenMenuAppService.GetAll(new GetScreenInput() { IsDeleted = false }) ;
            foreach (var item in menus.Items)
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = item.Name,
                    Value = item.Id.ToString()
                });
                i++;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }
        public async Task<ListResultOutput<ComboboxItemDto>> GetPointAccumulationTypes()
        {
            var returnList = new List<ComboboxItemDto>();

            var i = 1;
            foreach (var name in Enum.GetNames(typeof(PointAccumulationTypes)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetCalculationTypes()
        {
            List<ComboboxItemDto> returnList = new List<ComboboxItemDto>();

            int i = 0;
            foreach (string name in Enum.GetNames(typeof(CalculationType)))
            {
                returnList.Add(new ComboboxItemDto()
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i += 1;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetScreenMenuCategoryKeyBoardTypes()
        {
            List<ComboboxItemDto> returnList = new List<ComboboxItemDto>();

            int i = 0;
            foreach (string name in Enum.GetNames(typeof(ScreenMenuNumerator)))
            {
                returnList.Add(new ComboboxItemDto()
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i += 1;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetConnectCardTypes()
        {
            var lst = await _connectCardService.GetConnectCardTypes();
            return
                new ListResultOutput<ComboboxItemDto>(
                    lst.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());

        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetConfirmationTypes()
        {
            List<ComboboxItemDto> returnList = new List<ComboboxItemDto>();

            int i = 0;
            foreach (string name in Enum.GetNames(typeof(ConfirmationType)))
            {
                returnList.Add(new ComboboxItemDto()
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i += 1;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetReportNames()
        {
            List<ComboboxItemDto> returnList = new List<ComboboxItemDto>();

            foreach (var allReports in ReportNames.AllReports)
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = allReports,
                    Value = allReports
                });
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetPlanReasonGroups()
        {
            List<ComboboxItemDto> returnList = new List<ComboboxItemDto>();

            foreach (var reasonGroup in DineConnectConsts.ReasonGroups)
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = reasonGroup,
                    Value = reasonGroup
                });
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }
    }
}