﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Configuration.Host.Dto;
using DinePlan.DineConnect.Connect.Departments;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.User;
using DinePlan.DineConnect.Connect.Users;

namespace DinePlan.DineConnect.Common.Impl
{
    public class HostLookupAppService : DineConnectAppServiceBase, IHostLookupAppService
    {
     

        public HostLookupAppService()
        {
            
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetShortMessageTypes()
        {
            List<ComboboxItemDto> returnList = new List<ComboboxItemDto>();
            int i = 1;
            foreach (string name in Enum.GetNames(typeof(ShortMessageTypes)))
            {
                returnList.Add(new ComboboxItemDto()
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }
    }
}