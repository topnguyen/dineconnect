﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;

namespace DinePlan.DineConnect.Common.Impl
{
    public class EngageLookAppService : DineConnectAppServiceBase, IEngageLookAppService
    {
        public async Task<ListResultOutput<ComboboxItemDto>> GetVoucherTypes()
        {
            List<ComboboxItemDto> returnList = new List<ComboboxItemDto>();

            var fieldTypes =
             new[] { "Percentage", "Value"};

            int i = 0;
            foreach (var fieldType in fieldTypes)
            {
                returnList.Add(new ComboboxItemDto()
                {
                    DisplayText = fieldType,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }
    }
}