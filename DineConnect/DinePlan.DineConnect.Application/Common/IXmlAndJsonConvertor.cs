﻿
namespace DinePlan.DineConnect.Common
{
    public interface IXmlAndJsonConvertor
    {
        string Serialize(object dataToSerialize);
        string SerializeToJSON(object dataToSerialize);
        T DeSerializeFromJSON<T>(string jsonText);
        T Deserialize<T>(string xmlText);
		string GetAmountInWords(decimal inputAmount);


	}

}