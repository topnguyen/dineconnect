﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.ConnectCard.Card;
using DinePlan.DineConnect.Connect.Master.Dtos;
using FluentValidation.Results;

namespace DinePlan.DineConnect.Common
{
    public class FluentValidatoryExtension
    {
        public static string ValidationResult(IList<ValidationFailure> allResults)
        {
            StringBuilder builder = new StringBuilder();

            foreach (var validationFailure in allResults)
            {
                if(builder.Length>0)
                    builder.Append(",");
                builder.Append(validationFailure.ErrorMessage);
            }

            return builder.ToString();

        }
        
    }
}
