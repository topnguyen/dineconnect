﻿using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Common.Dto
{
    public class FindMenuItemInput : PagedAndFilteredInputDto
    {
        public int? TenantId { get; set; }
    }
}