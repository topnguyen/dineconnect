﻿using DinePlan.DineConnect.Dto;
using System;

namespace DinePlan.DineConnect.Common.Dto
{
    public class FindUsersInput : PagedAndFilteredInputDto
    {
        public int? TenantId { get; set; }

        public int SupplierRefId { get; set; }
    }

    public class InputDateRangeWithSupplierId 
    {
        public bool AllTimeDcs { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public int SupplierRefId { get; set; }

        public int LocationRefId { get; set; }
    }

    public class InputDateRangeWithCustomerId
    {
        public bool AllTimeDcs { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public int CustomerRefId { get; set; }

        public int LocationRefId { get; set; }
    }

}