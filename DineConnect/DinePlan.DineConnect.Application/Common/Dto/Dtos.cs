﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Common.Dto
{


    public class PullTimeInput : IInputDto
    {
        public int LocationId { get; set; }
        public int TenantId { get; set; }
        public string IpAddress { get; set; }
        public string DbVersion { get; set; }
    }

    public class TenantLocationPageDto : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public int LocationId { get; set; }
        public DateTime LastPullTime { get; set; }
        public DateTime LastPushTime { get; set; }

        public string Status { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
    public class ListComboItem
    {
        public string DepartmentGroup { get; set; }
        public List<ComboboxItemDto> ListItem { get; set; }
    }
}