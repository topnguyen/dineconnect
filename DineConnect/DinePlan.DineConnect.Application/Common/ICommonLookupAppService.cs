﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations.Dtos;
using DinePlan.DineConnect.House.Master.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Common
{
    public interface ICommonLookupAppService : IApplicationService
    {
        Task<ListResultOutput<ComboboxItemDto>> GetFieldTypes();

        Task<ListResultOutput<ComboboxItemDto>> GetEditionsForCombobox();

        Task<ListResultOutput<ComboboxItemDto>> GetCategoriesForCombobox(string group = "");

        Task<ListResultOutput<ComboboxItemDto>> GetLocationForCombobox();
        Task<ListResultOutput<ComboboxItemDto>> GetLocationCodeForCombobox();

        Task<ListResultOutput<ComboboxItemDto>> GetMenuPortionForCombobox(IdInput input);

        Task<ListResultOutput<SimpleMenuOutput>> GetSimplePortionsForCombobox(IdInput input);

        Task<ListResultOutput<ComboboxItemDto>> GetMenuItemsForCombobox();
        Task<ListResultOutput<ComboboxItemDto>> GetTransactionOrderTagsForCombobox();
        Task<ListResultOutput<ComboboxItemDto>> GetMenuItemsForComboboxByLocation(int id);
        Task<List<ListComboItem>> GetDepartmentsFilter();
        Task<PagedResultOutput<NameValueDto>> FindUsers(FindUsersInput input);

        //Task<ListResultOutput<ComboboxItemDto>> GetUnitForMaterialLinkCombobox(IdInput input);

        Task<ListResultOutput<ComboboxItemDto>> GetMaterialExcludedFromParticularRecipeForCombobox(NullableIdInput input);

        Task<ListResultOutput<ComboboxItemDto>> GetSalesTaxForCombobox();

        Task<ListResultOutput<ComboboxItemDto>> GetMaterialGroupForCombobox();

        Task<ListResultOutput<ComboboxItemDto>> GetMaterialForCombobox();

        Task<ListResultOutput<ComboboxItemDto>> GetMaterialForCombobox(NullableIdInput ninput);

        Task<ListResultOutput<ComboboxItemDto>> GetTaxForCombobox();

        Task<ListResultOutput<ComboboxItemDto>> GetTemplateForCombobox(GetTemplateList input);

        Task<ListResultOutput<ComboboxItemDto>> GetBrandForCombobox();


        Task<ListResultOutput<ComboboxItemDto>> GetMaterialByGroupCategory(NullableIdInput ninput);

        Task<ListResultOutput<ComboboxItemDto>> GetMaterialGroupCategoryByGroupId(NullableIdInput ninput);

        Task<List<ComboboxItemDto>> GetDepartments(string groupCode="");
        Task<List<ComboboxItemDto>> GetDepartmentGroups();

        Task<ListResultOutput<ComboboxItemDto>> GetTicketTagTypes();

        Task<ListResultOutput<ComboboxItemDto>> GetLocationsForLoginUser();

        Task<ListResultOutput<ComboboxItemDto>> GetUnitForCombobox(NullableIdInput input);

        Task<ListResultOutput<ComboboxItemDto>> GetMenuItemForCombobox(string category = "");

        Task<ListResultOutput<ComboboxItemDto>> GetPriceTagForCombobox(FilterInputDto input);

        Task<ListResultOutput<ComboboxItemDto>> GetScreenMenuForCombobox(FilterInputDto input);
        Task<ListResultOutput<ComboboxItemDto>> GetTerminalForCombobox();
        Task<ListResultOutput<ComboboxItemDto>> GetUserForCombobox();
        Task<ListResultOutput<ComboboxItemDto>> GetMenuTagForCombobox();

        Task<ListResultOutput<ComboboxItemDto>> GetCardTypes();

        Task<ListResultOutput<ComboboxItemDto>> GetPaymentTypeCombobox();
        Task<ListResultOutput<ComboboxItemDto>> GetProductGroupCombobox();
        Task<ListResultOutput<ComboboxItemDto>> GetReasonExchangeCombobox();

    }
}