﻿using System.Collections.Generic;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.General;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Processor;
using DinePlan.DineConnect.Hr.Master.Dtos;

namespace DinePlan.DineConnect.Common
{
    public interface IConnectLookupAppService : IApplicationService
    {

        Task<ListResultOutput<ComboboxItemDto>> GetScreenMenus();

        Task<ListResultOutput<ComboboxItemDto>> GetUserRoles();

        Task<ListResultOutput<ComboboxItemDto>> GetTagTypes();

        Task<ListResultOutput<ComboboxItemDto>> GetTransactionTypesForCombobox();

        Task<ListResultOutput<ComboboxItemDto>> GetPaymentTypes();

        Task<ListResultOutput<ComboboxItemDto>> GetDepartments();
        Task<ListResultOutput<ComboboxItemDto>> GetDepartmentGroups();

        Task<ListResultOutput<ComboboxItemDto>> GetTicketTags();

        Task<ListResultOutput<ComboboxItemDto>> GetPromotionTypes();


        Task<ListResultOutput<ComboboxItemDto>> GetPromotionValueTypes();

        Task<ListResultOutput<ComboboxItemDto>> GetPriceTags();

        Task<ListResultOutput<ComboboxItemDto>> GetProductTypes();

        Task<ListResultOutput<ComboboxItemDto>> GetLogTypes();

        Task<ListResultOutput<ComboboxItemDto>> GetDays();

        Task<ListResultOutput<ComboboxItemDto>> GetMonthDays();

        Task<ListResultOutput<ComboboxItemDto>> GetPaymentProcessors();

        Task<ListResultOutput<ComboboxItemDto>> GetNumerators();

        Task<ListResultOutput<ComboboxItemDto>> GetTicketTypes();

        Task<List<FormlyType>> GetProcessorSetting(IdInput input);
        Task<ListResultOutput<ComboboxItemDto>> GetPrinterProcessors();
        Task<List<FormlyType>> GetPrinterProcessorSetting(StringInput input);

        Task<ListResultOutput<ComboboxItemDto>> GetTillAccountTypes();

        Task<ListResultOutput<ComboboxItemDto>> GetPromotions();

        Task<ListResultOutput<ComboboxItemDto>> GetTillAccounts();

        Task<ListResultOutput<IdNameDto>> GetMenuItemsForLocation(int id);

        Task<ListResultOutput<ComboboxItemDto>> GetPointAccumulationTypes();

        Task<ListResultOutput<ComboboxItemDto>> GetCalculationTypes();

        Task<ListResultOutput<ComboboxItemDto>> GetConfirmationTypes();

        Task<ListResultOutput<ComboboxItemDto>> GetReportNames();

        Task<ListResultOutput<ComboboxItemDto>> GetPlanReasonGroups();

        Task<ListResultOutput<ComboboxItemDto>> GetScreenMenuCategoryKeyBoardTypes();

        Task<ListResultOutput<ComboboxItemDto>> GetConnectCardTypes();

    }
}