﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Exporter;
using OpenHtmlToPdf;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.AbnormalEndday.Dtos
{
    public class AbnormalEndDayReportDto
    {
        public string LocationCode { get; set; }
        public string LocationName { get; set; }
        public string Date { get; set; }
        public string StartWorkDate { get; set; }
        public string StartWorkDayBy { get; set; }
        public string SellerName { get; set; }
        public string EndWorkDate { get; set; }
        public string EndWorkDayBy { get; set; }
    }

    public class AbnormalEndDayReportOutput
    {
        public PagedResultOutput<AbnormalEndDayReportDto> Data { get; set; }
    }

    [Serializable]
    public class GetAbnormalEndDayReportInput : IInputDto, IFileExport
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
       // public List<LocationListDto> Locations { get; set; }
        public List<SimpleLocationDto> Locations { get; set; }
        public int LocationId { get; set; }
        public ExportType ExportOutputType { get; set; }
        public bool RunInBackground { get; set; }
        public int? TenantId { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public int UserId { get; set; }
        public PaperSize PaperSize { get; set; }
        public bool Portrait { get; set; }
        public string ReportName { get; set; }
        public string ReportDescription { get; set; }
        public string DynamicFilter { get; set; }
        public string DateFormat { get; set; }
        public string DatetimeFormat { get; set; }

    }
}
