﻿using Abp.Application.Services.Dto;
using Abp.BackgroundJobs;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Castle.Core.Logging;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.AbnormalEndday.Dtos;
using DinePlan.DineConnect.Connect.AbnormalEndday.Exporting;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.Users;
using DinePlan.DineConnect.Connect.WorkPeriod.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Report;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.AbnormalEndday.Implementation
{
    public class AbnormalEndDayReportAppService : DineConnectAppServiceBase, IAbnormalEndDayReportAppService
    {
        private readonly IBackgroundJobManager _bgm;
        private readonly IConnectReportAppService _cService;
        private readonly ILogger _logger;
        private readonly IAbnormalEndDayReportExcelExporter _abnormalEndDayReportExcelExporter;
        private readonly IReportBackgroundAppService _rbas;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<MenuItem> _menuitemRepo;
        private readonly ITenantSettingsAppService _tenantSettingsService;
        private readonly ILocationAppService _locService;
        private readonly IRepository<Master.Location> _locationRepo;
        private readonly IRepository<DinePlanUser> _dinePlanUserRepo;
        private readonly ISettingManager _settingManager;

       

        public AbnormalEndDayReportAppService(IAbnormalEndDayReportExcelExporter abnormalEndDayReportExcelExporter,
            IUnitOfWorkManager unitOfWorkManager,
            IConnectReportAppService cService,
            ILogger logger,
            IReportBackgroundAppService reportBackgroundAppService,
            IBackgroundJobManager backgroundJobManager,
            IRepository<MenuItem> menuitemRepo,
            ITenantSettingsAppService tenantSettingsService,
            ILocationAppService locService,
            IRepository<Master.Location> locationRepo,
            IRepository<DinePlanUser> dinePlanUserRepo,
            ISettingManager settingManager)
        {
            _abnormalEndDayReportExcelExporter = abnormalEndDayReportExcelExporter;
            _unitOfWorkManager = unitOfWorkManager;
            _cService = cService;
            _logger = logger;
            _rbas = reportBackgroundAppService;
            _bgm = backgroundJobManager;
            _menuitemRepo = menuitemRepo;
            _tenantSettingsService = tenantSettingsService;
            _locService = locService;
            _locationRepo = locationRepo;
            _dinePlanUserRepo = dinePlanUserRepo;
            _settingManager = settingManager;
            
        }

        public async Task<AbnormalEndDayReportOutput> GetAbnormalEndDayReport(GetAbnormalEndDayReportInput input)
        {
            var workPeriods = await _cService.GetWorkPeriodByLocationForDates(new WorkPeriodSummaryInput
            {
                StartDate = input.StartDate,
                EndDate = input.EndDate,
                LocationGroup = input.LocationGroup
            });
            var dtos = new List<AbnormalEndDayReportDto>();
            try
            {
                foreach (var workPeriod in workPeriods.Where(x => x.AutoClosed))
                {
                    dtos.Add(new AbnormalEndDayReportDto()
                    {
                        LocationCode = workPeriod.LocationCode,
                        LocationName = workPeriod.LocationName,
                        Date = workPeriod.StartTime.Date.ToString(GetDateFormat()),
                        StartWorkDate = workPeriod.StartTime.ToString(GetDateTimeFormat()),
                        EndWorkDate = workPeriod.EndTime.ToString(GetDateTimeFormat()),
                        SellerName = workPeriod.StartUser,
                        StartWorkDayBy = workPeriod.StartUser,
                        EndWorkDayBy = L("System"),
                    });
                }
                var dataAsQueryable = dtos.AsQueryable();
                if (!string.IsNullOrEmpty(input.DynamicFilter))
                {
                    var jsonSerializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver =
                            new CamelCasePropertyNamesContractResolver()
                    };
                    var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                    if (filRule?.Rules != null)
                    {
                        dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                    }
                }

                var total = dataAsQueryable.Count();
                var result = dataAsQueryable.ToList();
                return new AbnormalEndDayReportOutput
                {
                    Data = new PagedResultOutput<AbnormalEndDayReportDto>(total, result)
                };
            }
            catch (Exception ex)
            {
                _logger.Error("GetAbnormalEndDayReport", ex);
            }

            return new AbnormalEndDayReportOutput
            {
                Data = new PagedResultOutput<AbnormalEndDayReportDto>(0, new List<AbnormalEndDayReportDto>()),
            };
        }

        public async Task<FileDto> GetAbnormalEndDayReportExcel(GetAbnormalEndDayReportInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                var setting = await _tenantSettingsService.GetAllSettings();
                input.DatetimeFormat = setting.Connect.DateTimeFormat;
                input.DateFormat = setting.Connect.SimpleDateFormat;

                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.ABNORMALENDDAY,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                    {
                        if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
                        && !input.LocationGroup.Locations.Any())
                        {
                            var locationIds = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                            {
                                Locations = input.LocationGroup.Locations,
                                Group = false
                            });

                            input.Locations = new List<Master.Dtos.SimpleLocationDto>();
                            foreach (var locationId in locationIds)
                            {
                                var loc = await _locationRepo.FirstOrDefaultAsync(locationId);

                                input.Locations.Add(new Master.Dtos.SimpleLocationDto()
                                {
                                    Id = loc.Id
                                });
                            }
                        }

                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.ABNORMALENDDAY,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value,
                            GetAbnormalEndDayReportInput = input
                        });
                    }
                }
                else
                {
                    return await _abnormalEndDayReportExcelExporter.ExportAbnormalEndDay(input, _cService);
                }
            }

            return null;
        }
    }
}
