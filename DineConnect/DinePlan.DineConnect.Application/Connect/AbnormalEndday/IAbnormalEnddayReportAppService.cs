﻿using Abp.Application.Services;
using DinePlan.DineConnect.Connect.AbnormalEndday.Dtos;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.AbnormalEndday
{
    public interface IAbnormalEndDayReportAppService : IApplicationService
    {
        Task<AbnormalEndDayReportOutput> GetAbnormalEndDayReport(GetAbnormalEndDayReportInput input);
        Task<FileDto> GetAbnormalEndDayReportExcel(GetAbnormalEndDayReportInput input);
    }
}
