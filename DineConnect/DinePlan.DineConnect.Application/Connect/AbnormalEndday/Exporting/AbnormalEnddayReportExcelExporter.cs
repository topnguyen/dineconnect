﻿using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.AbnormalEndday.Dtos;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.WorkPeriod.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.AbnormalEndday.Exporting
{
    public class AbnormalEndDayReportExcelExporter : FileExporterBase, IAbnormalEndDayReportExcelExporter
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IConnectReportAppService _cService;
        private readonly IRepository<MenuItem> _menuitemRepo;
        public AbnormalEndDayReportExcelExporter(
            IUnitOfWorkManager unitOfWorkManager,
            IConnectReportAppService cService,
            IRepository<MenuItem> menuitemRepo,
            SettingManager settingManager)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _cService = cService;
            _menuitemRepo = menuitemRepo;
        }

        public async Task<FileDto> ExportAbnormalEndDay(GetAbnormalEndDayReportInput input, IConnectReportAppService connectReportService)
        {
            var start = input.StartDate.ToString(input.DateFormat);
            var end = input.EndDate.ToString(input.DateFormat);
            var file = new FileDto($"Abnormal_End_Day_Report_{start}-{end}.xlsx", MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add($"Abnormal End Day Report");
                sheet.OutLineApplyStyle = true;

                AddHeader(sheet,
                        L("LocationCode"),
                        L("LocationName"),
                        L("Date"),
                        L("StartWorkDate"),
                        L("StartWorkDayBy"),
                        L("SellerName"),
                        L("EndWorkDate"),
                        L("EndWorkDayBy")
                        );

                var dtos = new List<AbnormalEndDayReportDto>();
                var workPeriods = await _cService.GetWorkPeriodByLocationForDates(new WorkPeriodSummaryInput
                {
                    StartDate = input.StartDate,
                    EndDate = input.EndDate,
                    LocationGroup = input.LocationGroup
                });

                foreach (var workPeriod in workPeriods.Where(x => x.AutoClosed))
                {
                    dtos.Add(new AbnormalEndDayReportDto()
                    {
                        LocationCode = workPeriod.LocationCode,
                        LocationName = workPeriod.LocationName,
                        Date = workPeriod.StartTime.Date.ToString(input.DateFormat),
                        StartWorkDate = workPeriod.StartTime.ToString(input.DatetimeFormat),
                        EndWorkDate = workPeriod.EndTime.ToString(input.DatetimeFormat),
                        SellerName = workPeriod.StartUser,
                        StartWorkDayBy = workPeriod.StartUser,
                        EndWorkDayBy = L("System"),
                    });
                }
                var dataAsQueryable = dtos.AsQueryable();
                if (!string.IsNullOrEmpty(input.DynamicFilter))
                {
                    var jsonSerializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver =
                            new CamelCasePropertyNamesContractResolver()
                    };
                    var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                    if (filRule?.Rules != null)
                    {
                        dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                    }
                }

                var result = dataAsQueryable.ToList();

                AddObjects(sheet, 2, result,
                    _ => _.LocationCode,
                    _ => _.LocationName,
                            _ => _.Date,
                            _ => _.StartWorkDate,
                            _ => _.StartWorkDayBy,
                            _ => _.SellerName,
                            _ => _.EndWorkDate,
                            _ => _.EndWorkDayBy
                            );

                //Formatting cells
                for (var i = 1; i <= 8; i++)                
                    sheet.Column(i).AutoFit();

                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }
    }
}
