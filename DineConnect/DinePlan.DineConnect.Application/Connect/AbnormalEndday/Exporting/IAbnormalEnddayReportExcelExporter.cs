﻿using DinePlan.DineConnect.Connect.AbnormalEndday.Dtos;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.AbnormalEndday.Exporting
{
    public interface IAbnormalEndDayReportExcelExporter
    {
        Task<FileDto> ExportAbnormalEndDay(GetAbnormalEndDayReportInput input, IConnectReportAppService connectReportService);
    }
}
