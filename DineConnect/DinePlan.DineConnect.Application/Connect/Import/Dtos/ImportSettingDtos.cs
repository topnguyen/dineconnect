﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Import.Dtos
{
    [AutoMapTo(typeof(ImportSetting))]
    public class ImportSettingEditDto : ConnectEditDto
    {
        public int? Id { get; set; }
        public string FilePath { get; set; }
        public bool IsImported { get; set; }
    }

    public class CreateOrUpdateImportSettingInput : IInputDto
    {
        [Required]
        public ImportSettingEditDto ImportSetting { get; set; }

        public LocationGroupDto LocationGroup { get; set; }

        public CreateOrUpdateImportSettingInput()
        {
            LocationGroup = new LocationGroupDto();
        }
    }
    public class GetImportSettingInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Location { get; set; }
        public GetImportSettingInput()
        {
            LocationGroup = new LocationGroupDto();
        }

        public LocationGroupDto LocationGroup { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
}