﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Import.Dtos;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Import
{
    public interface IImportSettingAppService : IApplicationService
    {
        Task CreateOrUpdateImportSetting(CreateOrUpdateImportSettingInput input);
        Task<PagedResultOutput<ImportSettingEditDto>> GetAll(GetImportSettingInput input);
        Task DeleteImportSetting(IdInput input);
    }
}