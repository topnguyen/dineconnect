﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Import.Dtos;
using DinePlan.DineConnect.Connect.Master;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Import.Implementation
{
    public class ImportSettingAppService : DineConnectAppServiceBase, IImportSettingAppService
    {
        private readonly IRepository<ImportSetting> _importRepo;
        private readonly ILocationAppService _locAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public ImportSettingAppService(IRepository<ImportSetting> importRepo,
                                       ILocationAppService locAppService,
                                       IUnitOfWorkManager unitOfWorkManager)
        {
            _importRepo = importRepo;
            _locAppService = locAppService;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task<PagedResultOutput<ImportSettingEditDto>> GetAll(GetImportSettingInput input)
        { 
            var allItems = _importRepo.GetAll();
            var allMyEnItems = SearchLocation(allItems, input.LocationGroup).OfType<ImportSetting>();

            var allItemCount = allMyEnItems.Count();

            var sortMenuItems = allMyEnItems.AsQueryable()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList();

            var allListDtos = sortMenuItems.MapTo<List<ImportSettingEditDto>>();
            return new PagedResultOutput<ImportSettingEditDto>(allItemCount, allListDtos);
        }

        public async Task CreateOrUpdateImportSetting(CreateOrUpdateImportSettingInput input)
        {
            if (input.ImportSetting.Id.HasValue)
            {
                await UpdateImportSetting(input);
            }
            else
            {
                await CreateImportSetting(input);
            }
        }

        protected virtual async Task UpdateImportSetting(CreateOrUpdateImportSettingInput input)
        {
            var item = await _importRepo.GetAsync(input.ImportSetting.Id.Value);
            var dto = input.ImportSetting;
            item.FilePath = dto.FilePath;

            UpdateLocationAndNonLocation(item, input.LocationGroup);
            await _importRepo.UpdateAsync(item);
        }

        protected virtual async Task CreateImportSetting(CreateOrUpdateImportSettingInput input)
        {
            var dto = input.ImportSetting.MapTo<ImportSetting>();
            UpdateLocationAndNonLocation(dto, input.LocationGroup);
            await _importRepo.InsertAsync(dto);
        }

        public async Task DeleteImportSetting(IdInput input)
        {
            await _importRepo.DeleteAsync(input.Id);
        }
    }
}