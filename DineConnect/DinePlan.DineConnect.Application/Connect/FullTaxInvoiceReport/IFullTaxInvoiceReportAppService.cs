﻿using Abp.Application.Services;
using DinePlan.DineConnect.Connect.FullTaxInvoiceReport.Dto;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace DinePlan.DineConnect.Connect.FullTaxInvoiceReport
{
    public interface IFullTaxInvoiceReportAppService: IApplicationService
    {
        Task<GetFullTaxInvoiceReportOutput> GetFullTaxInvoiceReport(GetFullTaxInvoiceReportInput input);
        Task<FileDto> GetFullTaxInvoiceReportToExport(GetFullTaxInvoiceReportInput input);
    }
}
