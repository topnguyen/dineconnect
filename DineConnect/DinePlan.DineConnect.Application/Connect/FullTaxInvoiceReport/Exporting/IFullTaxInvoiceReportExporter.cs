﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.FullTaxInvoiceReport.Dto;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.FullTaxInvoiceReport.Exporting
{
    public interface IFullTaxInvoiceReportExporter
    {
        Task<FileDto> ExportFullTaxInvoiceReport(GetFullTaxInvoiceReportInput input, IFullTaxInvoiceReportAppService appService);
    }
}
