﻿using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.FullTaxInvoiceReport.Dto;
using DinePlan.DineConnect.Connect.Custom.Dto;
using DinePlan.DineConnect.Connect.Customize.Dto;
using DinePlan.DineConnect.Connect.SpeedOfServiceReport.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace DinePlan.DineConnect.Connect.FullTaxInvoiceReport.Exporting
{
    public class FullTaxInvoiceReportExporter : FileExporterBase, IFullTaxInvoiceReportExporter
    {
        public async Task<FileDto> ExportFullTaxInvoiceReport(GetFullTaxInvoiceReportInput input, IFullTaxInvoiceReportAppService appService)
        {
            var builder = new StringBuilder();
            builder.Append(L("FullTaxInvoiceReport"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append("_");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            if (input.ExportOutputType == 0)
            {
                using (var excelPackage = new ExcelPackage())
                {
                    await GetFullTaxInvoiceReportExportExcel(excelPackage, input, appService);
                    Save(excelPackage, file);
                }
                return ProcessFile(input, file);
            }
            else
            {
                var contentHTML = await ExportDatatableToHtml(input, appService);
                var result = ProcessFileHTML(file, contentHTML);
                return result;
            }
        }
        private FileDto ProcessFileHTML(FileDto file, string fileHTML)
        {
            File.WriteAllText(Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken), fileHTML);
            file.FileType = MimeTypeNames.ApplicationXhtmlXml;
            file.FileName = Path.GetFileNameWithoutExtension(file.FileName) + ".html";
            return file;
        }

        private async Task GetFullTaxInvoiceReportExportExcel(ExcelPackage package, GetFullTaxInvoiceReportInput ticketInput, IFullTaxInvoiceReportAppService appService)
        {
            var formatDateSetting = await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.SimpleDateFormat);
            var worksheet = package.Workbook.Worksheets.Add(L("FullTaxInvoiceReport"));
            worksheet.OutLineApplyStyle = true;
            var dtos = await appService.GetFullTaxInvoiceReport(ticketInput);

            //Add  report data
            var row = 1;
            worksheet.Cells[row, 1, row, 12].Merge = true;
            worksheet.Cells[row, 1, row, 12].Value = L("FullTaxInvoiceReport");
            worksheet.Cells[row, 1, row, 12].Style.Font.Name = "Tahoma";
            worksheet.Cells[row, 1, row, 12].Style.Font.Size = 14;
            worksheet.Cells[row, 1, row, 12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            row++;
            worksheet.Cells[row, 1, row, 12].Merge = true;
            worksheet.Cells[row, 1, row, 12].Value = L("Date") + " " + dtos.StartDate.ToString(formatDateSetting) + " " + L("to") + " " + dtos.EndDate.ToString(formatDateSetting);
            worksheet.Cells[row, 1, row, 12].Style.Font.Name = "Tahoma";
            worksheet.Cells[row, 1, row, 12].Style.Font.Size = 12;
            worksheet.Cells[row, 1, row, 12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            row++;
            if (!dtos.HidenHeader)
            {
                worksheet.Cells[row, 1, row, 12].Merge = true;
                worksheet.Cells[row, 1, row, 12].Value = L("Name") + ": " + dtos.PlantName + " " + L("Code") + ": " + dtos.PlantCode;
                worksheet.Cells[row, 1, row, 12].Style.Font.Name = "Tahoma";
                worksheet.Cells[row, 1, row, 12].Style.Font.Size = 12;
                row++;
            }
            row++;
            var listHeader = new List<string> {
               L("SNo"),
               L("Date"),
               L("FullTaxInvoiceNumber"),
               L("CustomerName"),
               L("TaxId"),
               L("PlantId"),
               L("PlantName"),
               L("Service"),
               L("AmountExcludeVAT"),
               L("VatAmount"),
               L("TotalAmount"),
               L("Remark")};
            //location
            foreach (var dataByStatus in dtos.ListItem)
            {
                int No = 1;
                worksheet.Cells[row, 1, row, 12].Merge = true;
                worksheet.Cells[row, 1, row, 12].Value = L("Status") + ": " + dataByStatus.Status;
                worksheet.Cells[row, 1, row, 12].Style.Font.Name = "Tahoma";
                worksheet.Cells[row, 1, row, 12].Style.Font.Size = 14;
                worksheet.Cells[row, 1, row, 12].Style.Font.Color.SetColor(Color.Black);

                try
                {
                    Color color = System.Drawing.ColorTranslator.FromHtml("#808080");
                    worksheet.Cells[$"A{row}:L{row}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[$"A{row}:L{row}"].Style.Fill.BackgroundColor.SetColor(color);
                }
                catch (Exception e)
                {
                    throw e;
                }

                row++;
                int colHeader = 1;
                foreach (var header in listHeader)
                {
                    worksheet.Cells[row, colHeader].Value = header;
                    worksheet.Cells[row, colHeader].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    colHeader++;
                }
                worksheet.Cells[$"A{row}:L{row}"].Style.Font.Name = "Tahoma";
                worksheet.Cells[$"A{row}:L{row}"].Style.Font.Size = 14;
                row++;
                foreach (var dataItem in dataByStatus.ListItem)
                {
                    worksheet.Cells[row, 1].Value = No;
                    worksheet.Cells[row, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, 2].Value = dataItem.Date.ToString(formatDateSetting);
                    worksheet.Cells[row, 3].Value = dataItem.FullTaxInvoiceNumber;
                    worksheet.Cells[row, 4].Value = dataItem.CustomerName;
                    worksheet.Cells[row, 5].Value = dataItem.TaxId;
                    worksheet.Cells[row, 6].Value = dataItem.PlantId;
                    worksheet.Cells[row, 7].Value = dataItem.PlantName;
                    worksheet.Cells[row, 8].Value = dataItem.Service;
                    worksheet.Cells[row, 9].Value = dataItem.AmountExcludeVAT.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    worksheet.Cells[row, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    worksheet.Cells[row, 10].Value = dataItem.Vat.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    worksheet.Cells[row, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    worksheet.Cells[row, 11].Value = dataItem.TotalAmount.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    worksheet.Cells[row, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    worksheet.Cells[row, 12].Value = dataItem.Remark;
                    worksheet.Cells[$"A{row}:L{row}"].Style.Font.Name = "Tahoma";
                    worksheet.Cells[$"A{row}:L{row}"].Style.Font.Size = 12;
                    row++;
                    No++;
                }
                worksheet.Cells[row, 1, row, 3].Merge = true;
                worksheet.Cells[row, 1, row, 3].Value = L("Quantity") + ": " + dataByStatus.ListItem.Count + " " + L("Items");
                worksheet.Cells[row, 8].Value = L("AllTotal");
                worksheet.Cells[row, 9].Value = dataByStatus.SumAmountExcludeVAT.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                worksheet.Cells[row, 10].Value = dataByStatus.SumVAT.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                worksheet.Cells[row, 11].Value = dataByStatus.SumTotalAmount.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                worksheet.Cells[row, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                worksheet.Cells[row, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                worksheet.Cells[row, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                worksheet.Cells[$"A{row}:L{row}"].Style.Font.Name = "Tahoma";
                worksheet.Cells[$"A{row}:L{row}"].Style.Font.Size = 14;
                row++;
                row++;
            }
            for (var i = 1; i <= 12; i++)
            {
                worksheet.Column(i).AutoFit();
            }
        }
        private async Task<string> ExportDatatableToHtml(GetFullTaxInvoiceReportInput ticketInput, IFullTaxInvoiceReportAppService appService)
        {
            var dtos = await appService.GetFullTaxInvoiceReport(ticketInput);
            var formatDateSetting = await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.SimpleDateFormat);
            StringBuilder strHTMLBuilder = new StringBuilder();
            strHTMLBuilder.Append("<html>");
            strHTMLBuilder.Append("<head>");
            strHTMLBuilder.Append("</head>");

            strHTMLBuilder.Append(@"<style> 
                  body  { 
                   font:400 12px 'Tahoma';
                   font-size: 12px;
                   padding:10px;
                 }
                 table  { 
                     margin-top: 10px;
                     border-collapse: collapse;
                     width: 100%;
                     margin-bottom: 10px;
                 }

                 table td, table th {
                     border: 1px solid #ddd;
                     padding: 8px;
                 }
                 table th {
                     padding-top: 12px;
                     padding-bottom: 12px;
                     text-align: left;
                     background-color: #364150;
                     color: white;
                 }
                 thead {
                     display: table-header-group;
                 }
                 tfoot {
                     display: table-row-group;
                 }
                 tr{
                     page-break-inside: avoid;
                 }
                 blockquote {
                   color:white;
                   text-align:center;
                 }

                 h3{
                   text-align:center;
                   text-transform: capitalize;
                   font-size: medium;
                 }

        .margin_top_10{
           margin-top: -10px;
            }
        .margin_bottom_0 {
        	margin-bottom: 0 !important;
        }
                 .margin_top_0 {
        	margin-top: 0 !important;
        }

        .border_bottom_none {
        	border-bottom: none !important;
        }

        .custom_table_date {
        	margin-bottom: 0px !important;
        }

        .display_flex {
        	display: flex;
        }
   .w-2 {
        width: 2%;
    }
    .w-3 {
        width: 3%;
    }
    .w-5 {
        width: 5%;
    }
    .w-10 {
        width: 10%;
    }

        .w-15 {
        	width: 15%;
        }

        .w-25 {
        	width: 25%;
        }

        .w-30 {
        	width: 30%;
        }

        .w-35 {
        	width: 35%;
        }
        .w-50 {
        	width: 50%;
        }

        .w-85 {
        	width: 85%;
        }


        .w-100 {
        	width: 100%;
        }


        .float-right {
        	float: right;
        }

        .ml-10 {
        	margin-left: 10px;
        }

        .mt-50 {
        	margin-top: 50px;
        }

        .zom-15 {
        	zoom: 1.5;
        }

        .pl-44 {
        	padding-left: 44px;
        }

        .thead_title {
        	font-weight: bold;
            background-color: #008000;
            color: white;
        }

        .thead_content {
        	background-color: #808080;
        	color: white;
        }
                 .soldTo{
                 background-color: #b4c6e7;
                 } 
                 .b_date{
                     background-color: #ddebf7;
                 }

        .footer_item {
        	background-color: #d6dce5;
        	font-weight: bold;
        }
        .footer_str {
        	display: flex;
        	padding: 10px;
        }

        .footer {
        	background-color: #e2f0d9;
        	font-weight: bold;
        }

        .color_header {
        	background-color: #808080;
        	color: white;
        }

        .color_posName {
        	background-color: #f0f0f0;
        }

        .color_total {
        	background-color: #f0f0f0;
        }

        .font_weight_bold {
        	font-weight: bold;
        }

        .text-center{
           text-align: center;
        }
                  .font_16{
        	font-size: 16;
                  }
                 .font_14{
        	font-size: 14;
                  }
                  .font_12{
        	font-size: 12;
                  }
            .header_status{
                background-color: #008000;
                color: white;
            }
  
                 </style>");

            strHTMLBuilder.Append("<body>");

            strHTMLBuilder.Append("<div class='portlet-body mt-50'>");
            strHTMLBuilder.Append("<div class='page-head'>");
            strHTMLBuilder.Append("<div class='page-title'>");
            strHTMLBuilder.Append($"<h1 class='text-center'><span class='caption-subject bold uppercase'>{L("FullTaxInvoiceReport")}</span></h1>");
            strHTMLBuilder.Append($"<h4 class='text-center'> {L("Date")} {dtos.StartDate.ToString(formatDateSetting)} {L("To")} {dtos.EndDate.ToString(formatDateSetting)}</span></h4>");
            strHTMLBuilder.Append("</div>");
            strHTMLBuilder.Append("</div>");
            if (!dtos.HidenHeader)
            {
                strHTMLBuilder.Append($"<div>{L("Name")}: {dtos.PlantName}   {L("Code")}: {dtos.PlantCode}</div>");
            }
            strHTMLBuilder.Append("</div>");

            foreach (var dataByStatus in dtos.ListItem.OrderBy(s => s.Status))
            {
                strHTMLBuilder.Append("<div>");
                strHTMLBuilder.Append("<table class='table table-bordered custom_table_date border_bottom_none'>");
                strHTMLBuilder.Append("<tr class='thead_title'>");
                strHTMLBuilder.Append($"<td colspan = '12' class='w-100 border_bottom_none text- eft  font_16'>{L("Status")}: { dataByStatus.Status}</td>");
                strHTMLBuilder.Append("</tr>");
                strHTMLBuilder.Append("<tr class='thead_content font_14'>");
                strHTMLBuilder.Append($"<td class='w-5 border_bottom_none text-center font_weight_bold'>{L("SNo")}</td>");
                strHTMLBuilder.Append($"<td class='w-10 border_bottom_none text-center font_weight_bold'>{L("Date")}</td>");
                strHTMLBuilder.Append($"<td class='w-10 border_bottom_none text-center font_weight_bold'>{L("FullTaxInvoiceNumber")}</td>");
                strHTMLBuilder.Append($"<td class='w-10 border_bottom_none text-center font_weight_bold'>{L("FullTaxInvoiceNumber")}</td>");
                strHTMLBuilder.Append($"<td class='w-10 border_bottom_none text-center font_weight_bold'>{L("TaxId")}</td>");
                strHTMLBuilder.Append($"<td class='w-5 border_bottom_none text-center font_weight_bold'>{L("PlantId")}</td>");
                strHTMLBuilder.Append($"<td class='w-15 border_bottom_none text-center font_weight_bold'>{L("PlantName")}</td>");
                strHTMLBuilder.Append($"<td class='w-5 border_bottom_none text-center font_weight_bold'>{L("Service")}</td>");
                strHTMLBuilder.Append($"<td class='w-10 border_bottom_none text-center font_weight_bold'>{L("AmountExcludeVAT")}</td>");
                strHTMLBuilder.Append($"<td class='w-5 border_bottom_none text-center font_weight_bold'>{L("VAT")}</td>");
                strHTMLBuilder.Append($"<td class='w-5 border_bottom_none text-center font_weight_bold'>{L("TotalAmount")}</td>");
                strHTMLBuilder.Append($"<td class='w-10 border_bottom_none text-center font_weight_bold'>{L("Remark")}</td>");
                strHTMLBuilder.Append("</tr>");
                strHTMLBuilder.Append("</table>");
                strHTMLBuilder.Append("</div>");
                strHTMLBuilder.Append("<div class='content_str'>");
                var indexByShift = 1;
                foreach (var dataItem in dataByStatus.ListItem)
                {
                    strHTMLBuilder.Append("<div class='display_flex w-100 color-blue margin_top_10'>");
                    strHTMLBuilder.Append("<table class='table table-bordered custom_table_date border_bottom_none'>");
                    strHTMLBuilder.Append("<tr class='color_posName margin_top_10'>");
                    strHTMLBuilder.Append($"<td class='w-5 text-center' >{indexByShift}</td>");
                    strHTMLBuilder.Append($"<td class='w-10' >{dataItem.Date.ToString(formatDateSetting)}</td>");
                    strHTMLBuilder.Append($"<td class='w-10' >{dataItem.FullTaxInvoiceNumber}</td>");
                    strHTMLBuilder.Append($"<td class='w-10' >{dataItem.CustomerName}</td>");
                    strHTMLBuilder.Append($"<td class='w-10' >{dataItem.TaxId}</td>");
                    strHTMLBuilder.Append($"<td class='w-5' >{dataItem.PlantId}</td>");
                    strHTMLBuilder.Append($"<td class='w-15' >{dataItem.PlantName}</td>");
                    strHTMLBuilder.Append($"<td class='w-5' >{dataItem.Service}</td>");
                    strHTMLBuilder.Append($"<td class='w-10 text-center' >{dataItem.AmountExcludeVAT.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td>");
                    strHTMLBuilder.Append($"<td class='w-5 text-center' >{dataItem.Vat.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td>");
                    strHTMLBuilder.Append($"<td class='w-5 text-center' >{dataItem.TotalAmount.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td>");
                    strHTMLBuilder.Append($"<td class='w-10' >{dataItem.Remark}</td>");
                    strHTMLBuilder.Append("</tr>");
                    strHTMLBuilder.Append("</table>");
                    strHTMLBuilder.Append("</div>");
                    indexByShift++;
                }
                strHTMLBuilder.Append("</div>");
                strHTMLBuilder.Append("<table class='table table-bordered custom_table_date border_bottom_none color_total margin_top_0'>");
                strHTMLBuilder.Append("<tr> ");
                strHTMLBuilder.Append($"<td colspan='3' class='w-30 font_weight_bold'>{L("Quantity")} : {dataByStatus.ListItem.Count} {L("Items")} </td>");
                strHTMLBuilder.Append("<td class='w-10'></td>");
                strHTMLBuilder.Append("<td class='w-10'></td>");
                strHTMLBuilder.Append("<td class='w-5'></td>");
                strHTMLBuilder.Append("<td class='w-10'></td>");
                strHTMLBuilder.Append($"<td class='w-5 font_weight_bold'>{L("AllTotal")}</td>");
                strHTMLBuilder.Append($"<td class='w-10 font_weight_bold text-center'> {dataByStatus.SumAmountExcludeVAT.ToString(ConnectConsts.ConnectConsts.NumberFormat)} </td>");
                strHTMLBuilder.Append($"<td class='w-5 font_weight_bold text-center'> {dataByStatus.SumVAT.ToString(ConnectConsts.ConnectConsts.NumberFormat)} </td>");
                strHTMLBuilder.Append($"<td class='w-5 font_weight_bold text-center'> {dataByStatus.SumTotalAmount.ToString(ConnectConsts.ConnectConsts.NumberFormat)} </td>");
                strHTMLBuilder.Append("<td></td>");
                strHTMLBuilder.Append("</tr>");
                strHTMLBuilder.Append("</table>");
            }
            strHTMLBuilder.Append("</body>");
            strHTMLBuilder.Append("</html>");

            string Htmltext = strHTMLBuilder.ToString();
            return Htmltext;
        }
    }
}
