﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.FullTaxInvoiceReport.Dto
{
    public class GetFullTaxInvoiceReportOutput
    {
        public List<GetFullTaxInvoiceItemByStatus> ListItem { get; set; }
        public GetFullTaxInvoiceReportOutput()
        {
            ListItem = new List<GetFullTaxInvoiceItemByStatus>();
        }
        public decimal SumAmountExcludeVAT {get;set;}
        public decimal SumVAT { get; set; }
        public decimal SumTotalAmount { get; set; }
        public string PlantCode { get; set; }
        public string PlantName { get; set; }
        public string PlantPhone { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool HidenHeader { get; set; } = true;
    }
    public class GetFullTaxInvoiceItemByStatus
    {
        public string Status { get; set; }
        public List<GetFullTaxInvoiceItem> ListItem { get; set; }
        public GetFullTaxInvoiceItemByStatus()
        {
            ListItem = new List<GetFullTaxInvoiceItem>();
        }
        public decimal SumAmountExcludeVAT { get; set; }
        public decimal SumVAT { get; set; }
        public decimal SumTotalAmount { get; set; }
    }

    public class GetFullTaxInvoiceItem
    {
        public DateTime Date { get; set; }
        public string FullTaxInvoiceNumber { get; set; }
        public string CustomerName { get; set; }
        public string TaxId { get; set; }
        public string PlantId { get; set; }
        public string PlantName { get; set; }
        public string Service { get; set; }
        public decimal AmountExcludeVAT { get; set; }
        public decimal Vat { get; set; }
        public decimal TotalAmount => AmountExcludeVAT + Vat;
        public string Remark { get; set; }
        public string Status { get; set; }
    }
}
