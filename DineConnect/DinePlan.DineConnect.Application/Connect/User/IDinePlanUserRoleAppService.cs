﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.User.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.User
{
    public interface IDinePlanUserRoleAppService : IApplicationService
    {
        Task<PagedResultOutput<DinePlanUserRoleListDto>> GetAll(GetDinePlanUserRoleInput inputDto);

        Task<FileDto> GetAllToExcel();

        Task<GetDinePlanUserRoleForEditOutput> GetDinePlanUserRoleForEdit(NullableIdInput nullableIdInput);

        Task CreateOrUpdateDinePlanUserRole(CreateOrUpdateDinePlanUserRoleInput input);

        Task DeleteDinePlanUserRole(IdInput input);

        Task<ListResultOutput<DinePlanUserRoleListDto>> GetIds();

        Task<GetDinePlanUserRoleOutputList> GetUserRoles(GetUserRolesInput input);

        Task<GetDinePlanUserRoleOutputList> ApiGetUserRoles(GetUserRolesNameInput input);

        Task ActivateItem(IdInput input);
    }
}