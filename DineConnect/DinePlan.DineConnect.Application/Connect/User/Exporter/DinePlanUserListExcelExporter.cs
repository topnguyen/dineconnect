﻿using DinePlan.DineConnect.Connect.User.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.User.Exporter
{
    public class DinePlanUserListExcelExporter : FileExporterBase, IDinePlanUserListExcelExporter
    {
        public FileDto ExportToFile(List<DinePlanUserListDto> dtos)
        {
            return CreateExcelPackage(
                "DinePlanUserList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("DinePlanUser"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Code"),
                        L("Name"),
                        L("Role"),
                        L("PinCode"),
                        L("CreationTime")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.Code,
                        _ => _.Name,
                        _ => _.DinePlanUserRoleName,
                        _ => _.PinCode,
                        _ => _.CreationTime
                        );

                    var creationTimeColumn = sheet.Column(5);
                    creationTimeColumn.Style.Numberformat.Format = "dd-mm-yyyy hh:mm:ss";

                    for (var i = 1; i <= 5; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}