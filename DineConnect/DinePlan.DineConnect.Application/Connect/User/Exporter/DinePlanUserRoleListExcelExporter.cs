﻿using DinePlan.DineConnect.Connect.User.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.User.Exporter
{
    public class DinePlanUserRoleListExcelExporter : FileExporterBase, IDinePlanUserRoleListExcelExporter
    {
        public FileDto ExportToFile(List<DinePlanUserRoleListDto> dtos)
        {
            return CreateExcelPackage(
                "DinePlanUserRoleList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("DinePlanUserRole"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Name"),
                        L("IsAdmin"),
                        L("CreationTime"),
                        L("Permissions")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.Name,
                        _ => _.IsAdmin,
                        _ => _.CreationTime,
                        _ => _.Permission
                        );

                    var creationTimeColumn = sheet.Column(4);
                    creationTimeColumn.Style.Numberformat.Format = "dd-mm-yyyy hh:mm:ss";

                    var permissionCol = sheet.Column(5);
                    permissionCol.Width = 100;
                    permissionCol.Style.WrapText = true;

                    for (var i = 1; i <= 4; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}