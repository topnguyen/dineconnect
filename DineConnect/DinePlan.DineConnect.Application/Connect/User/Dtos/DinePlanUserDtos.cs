﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Users;
using DinePlan.DineConnect.Dto;
using System;
using System.ComponentModel.DataAnnotations;
using Abp.Extensions;
using DinePlan.DineConnect.Filter;

namespace DinePlan.DineConnect.Connect.User.Dtos
{
    [AutoMapFrom(typeof(DinePlanUser))]
    public class DinePlanUserListDto
    {
        public virtual int Id { get; set; }
        public virtual string Code { get; set; }
        public virtual string PinCode { get; set; }
        public virtual string Name { get; set; }
        public virtual string SecurityCode { get; set; }
       

        public int DinePlanUserRoleId { get; set; }
        public string DinePlanUserRoleName { get; set; }
        public DateTime CreationTime { get; set; }
    }

    [AutoMapFrom(typeof(DinePlanUser))]
    public class ApiDinePlanUserListDto
    {
        public virtual int Id { get; set; }
        public virtual string Code { get; set; }
        public virtual string PinCode { get; set; }
        public virtual string Name { get; set; }
        public virtual string SecurityCode { get; set; }
        public virtual string ConnectUser { get; set; }
        public virtual string ConnectPassword { get; set; }
        public virtual string LanguageCode { get; set; }
        public virtual string AlternateLanguageCode { get; set; }


        public virtual bool IsActive { get; set; }
        public int DinePlanUserRoleId { get; set; }
        public string DinePlanUserRoleName { get; set; }
        public DateTime CreationTime { get; set; }
    }

    [AutoMapTo(typeof(DinePlanUser))]
    public class DinePlanUserEditDto : ConnectEditDto
    {
        public int? Id { get; set; }
        public virtual string PinCode { get; set; }
        public virtual string SecurityCode { get; set; }
        public virtual string Code { get; set; }

        public virtual string Name { get; set; }
        public int DinePlanUserRoleId { get; set; }
        public string LanguageCode { get; set; }
        public string AlternateLanguageCode { get; set; }

        public long? UserId { get; set; }


        public DinePlanUserEditDto()
        {
            PinCode = new Random().Next().ToString();
            LanguageCode = "en";
        }
    }

    public class GetDinePlanUserInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }

        public GetDinePlanUserInput()
        {
            LocationGroup = new LocationGroupDto();
        }

        public LocationGroupDto LocationGroup { get; set; }
    }

    public class GetDinePlanUserForEditOutput : IOutputDto
    {
        public DinePlanUserEditDto DinePlanUser { get; set; }
        public LocationGroupDto LocationGroup { get; set; }

        public GetDinePlanUserForEditOutput()
        {
            LocationGroup = new LocationGroupDto();
            DinePlanUser = new DinePlanUserEditDto();
        }
    }

    public class CreateOrUpdateDinePlanUserInput : IInputDto
    {
        [Required]
        public DinePlanUserEditDto DinePlanUser { get; set; }

        public LocationGroupDto LocationGroup { get; set; }

        public CreateOrUpdateDinePlanUserInput()
        {
            LocationGroup = new LocationGroupDto();
            DinePlanUser = new DinePlanUserEditDto();
        }
    }

    public class GetUsersByLocationInput : IInputDto
    {
        [Required]
        public int LocationId { get; set; }

        public int TenantId { get; set; }
    }
}