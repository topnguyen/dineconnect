﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter;
using DinePlan.DineConnect.Exporter.Util;
using OpenHtmlToPdf;

namespace DinePlan.DineConnect.Connect.User.Dtos
{
    [AutoMapFrom(typeof (PlanLogger))]
    public class PlanLoggerListDto : ExportDataObject
    {
        public virtual int? Id { get; set; }
        public virtual string Terminal { get; set; }
        public virtual int TicketId { get; set; }
        public virtual string TicketNo { get; set; }
        public virtual decimal TicketTotal { get; set; }
        public virtual string UserName { get; set; }
        public virtual string EventLog { get; set; }
        public virtual string EventName { get; set; }
        public virtual DateTime EventTime { get; set; }
        public virtual int TenantId { get; set; }
        public virtual int LocationId { get; set; }
        public virtual string LocationName { get; set; }
    }

    [AutoMapTo(typeof (PlanLogger))]
    public class PlanLoggerEditDto
    {
        public virtual int? Id { get; set; }
        public virtual string Terminal { get; set; }
        public virtual int TicketId { get; set; }
        public virtual string TicketNo { get; set; }
        public virtual decimal TicketTotal { get; set; }
        public virtual string UserName { get; set; }
        public virtual string EventLog { get; set; }
        public virtual string EventName { get; set; }
        public virtual DateTime EventTime { get; set; }
        public virtual int TenantId { get; set; }
        public virtual int LocationId { get; set; }
    }
    public class GetPlanLoggerInput : PagedAndSortedInputDto, IShouldNormalize, IDateInput,IFileExport, IBackgroundExport
    {
        public bool Refund { get; set; }
        public string TicketNo { get; set; }
        public bool Group { get; set; }
        public bool Unfinished { get; set; }
        public List<ComboboxItemDto> Events { get; set; }
        public string Filter { get; set; }
        public string Operation { get; set; }
        public ExportType ExportType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<SimpleLocationDto> Locations { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public bool Credit { get; set; }
        public int Location { get; set; }
        public long UserId { get; set; }
        public bool NotCorrectDate { get; set; }
        public bool RunInBackground { get; set; }
        public string ReportDescription { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "EventTime";
            }
        }

        public ExportType ExportOutputType { get; set; }
        public PaperSize PaperSize { get; set; }
        public bool Portrait { get; set; }
        public string DynamicFilter { get; set; }
        public string DateFormat { get; set; }
        public string DatetimeFormat { get; set; }
    }

    public class GetPlanLoggerOutput : IOutputDto
    {
        public PlanLoggerEditDto PlanLogger { get; set; }
    }

    public class CreateOrUpdatePlanLoggerInput : IInputDto
    {
        [Required]
        public PlanLoggerEditDto PlanLogger { get; set; }
    }

    public class CreateOrUpdatePlanLoggerOutput : IOutputDto
    {
        [Required]
        public int LogId { get; set; }
    }

    public class EventOutput : IOutputDto
    {
        public string BadgeClass { get; set; }
        public string BadgeIconClass { get; set; }
        public string Title { get; set; }
        public string When { get; set; }
        public string Content { get; set; }
    }

    public class TimeLineOutput : IOutputDto
    {
        public TimeLineOutput()
        {
            EventOutputs = new List<EventOutput>();
            Descriptions = new List<NameValueDto>();
        }

        public List<EventOutput> EventOutputs { get; set; }
        public List<NameValueDto> Descriptions { get; set; }
    }
}