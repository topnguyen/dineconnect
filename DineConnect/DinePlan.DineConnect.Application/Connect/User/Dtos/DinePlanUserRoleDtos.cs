﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Authorization.Dto;
using DinePlan.DineConnect.Connect.Users;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.User.Dtos
{
    [AutoMapFrom(typeof(DinePlanUserRole))]
    public class DinePlanUserRoleListDto : FullAuditedEntityDto
    {
        public virtual string Name { get; set; }
        public virtual bool IsAdmin { get; set; }
        public virtual string Permission { get; set; }
    }

    [AutoMapTo(typeof(DinePlanUserRole))]
    public class DinePlanUserRoleEditDto
    {
        public int? Id { get; set; }
        public virtual string Name { get; set; }
        public virtual bool IsAdmin { get; set; }
        public int? DepartmentId { get; set; }
    }

    public class GetDinePlanUserRoleInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }
        public string Location { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    public class GetDinePlanUserRoleForEditOutput : IOutputDto
    {
        public DinePlanUserRoleEditDto DinePlanUserRole { get; set; }
        public List<FlatPermissionDto> Permissions { get; set; }
        public List<string> GrantedPermissionNames { get; set; }
    }

    public class GetDinePlanUserRoleOutputList : IOutputDto
    {
        public List<GetDinePlanUserRoleForEditOutput> Items { get; set; }
    }

    public class CreateOrUpdateDinePlanUserRoleInput : IInputDto
    {
        [Required]
        public DinePlanUserRoleEditDto DinePlanUserRole { get; set; }

        [Required]
        public List<string> GrantedPermissionNames { get; set; }
    }

    public class GetUserRolesInput : IInputDto
    {
        public int TenantId { get; set; }
        public int UserId { get; set; }
    }

    public class GetUserRolesNameInput : IInputDto
    {
        public int TenantId { get; set; }
        public string Name { get; set; }
    }
}