﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Connect.Departments.Dtos;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Connect.User.Dtos;
using DinePlan.DineConnect.Connect.Users;
using DinePlan.DineConnect.Dto;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Connect.User.Implementation
{
    public class DinePlanUserAppService : DineConnectAppServiceBase, IDinePlanUserAppService
    {
        private readonly IDinePlanUserListExcelExporter _dineplanuserExporter;
        private readonly IDinePlanUserManager _dineplanuserManager;
        private readonly IRepository<DinePlanUser> _dineplanuserRepo;
        private readonly ILocationAppService _locAppService;
        private readonly ISyncAppService _syncAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IUserAppService _userAppService;

        public DinePlanUserAppService(IDinePlanUserManager dineplanuserManager,
            IRepository<DinePlanUser> dinePlanUserRepo, ILocationAppService locAppService,
            IDinePlanUserListExcelExporter dineplanuserExporter, ISyncAppService syncAppService
            , IUnitOfWorkManager unitOfWorkManager, IUserAppService userAppService
        )
        {
            _dineplanuserManager = dineplanuserManager;
            _dineplanuserRepo = dinePlanUserRepo;
            _dineplanuserExporter = dineplanuserExporter;
            _syncAppService = syncAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _locAppService = locAppService;
            _userAppService = userAppService;
        }

        public async Task<PagedResultOutput<DinePlanUserListDto>> GetAll(GetDinePlanUserInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var allItems = _dineplanuserRepo.GetAll()
                    .Where(i => i.IsDeleted == input.IsDeleted)
                   
                    .WhereIf(
                        !input.Filter.IsNullOrEmpty(),
                        p => p.Name != null && p.Name.Contains(input.Filter) ||
                             p.PinCode != null && p.PinCode.Contains(input.Filter) ||
                             p.DinePlanUserRole != null && p.DinePlanUserRole.Name.Contains(input.Filter)
                    );
                var allMyEnItems = SearchLocation(allItems, input.LocationGroup).OfType<DinePlanUser>();

                var allItemCount = allMyEnItems.Count();

                var sortMenuItems = allMyEnItems.AsQueryable()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList();

                var allListDtos = sortMenuItems.MapTo<List<DinePlanUserListDto>>();

                return new PagedResultOutput<DinePlanUserListDto>(
                    allItemCount,
                    allListDtos
                );
            }
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _dineplanuserRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<DinePlanUserListDto>>();
            return _dineplanuserExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDinePlanUserForEditOutput> GetDinePlanUserForEdit(NullableIdInput input)
        {
            DinePlanUserEditDto editDto;
            var output = new GetDinePlanUserForEditOutput();

            if (input.Id.HasValue)
            {
                var hDto = await _dineplanuserRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<DinePlanUserEditDto>();
            }
            else
            {
                editDto = new DinePlanUserEditDto();
            }

            UpdateLocationAndNonLocationForEdit(editDto, output.LocationGroup);

            output.DinePlanUser = editDto;

            return output;
        }

        public async Task CreateOrUpdateDinePlanUser(CreateOrUpdateDinePlanUserInput input)
        {
            if (input?.LocationGroup == null)
                return;

            if (input.DinePlanUser.Id.HasValue)
                await UpdateDinePlanUser(input);
            else
                await CreateDinePlanUser(input);
            await _syncAppService.UpdateSync(SyncConsts.USER);
        }

        public async Task DeleteDinePlanUser(IdInput input)
        {
            await _dineplanuserRepo.DeleteAsync(input.Id);
        }

        public async Task<ListResultOutput<DinePlanUserListDto>> GetIds()
        {
            var lstDinePlanUser = await _dineplanuserRepo.GetAll().ToListAsync();
            return new ListResultOutput<DinePlanUserListDto>(lstDinePlanUser.MapTo<List<DinePlanUserListDto>>());
        }

        public async Task<ListResultOutput<ApiDinePlanUserListDto>> ApiUsersByLocation(ApiLocationInput input)
        {
            var outputUsers = new List<DinePlanUser>();

            try
            {

                var allUsers = await _dineplanuserRepo.GetAllListAsync(a => a.TenantId == input.TenantId);

                if (!string.IsNullOrEmpty(input.Tag))
                {
                    var allLocations = await _locAppService.GetLocationsByLocationGroupCode(input.Tag);
                    if (allLocations != null && allLocations.Items.Any())
                        foreach (var dinePlanUser in allUsers)
                            if (!dinePlanUser.Group && !string.IsNullOrEmpty(dinePlanUser.Locations) &&
                                !dinePlanUser.LocationTag)
                            {
                                var scLocations =
                                    JsonConvert.DeserializeObject<List<SimpleLocationGroupDto>>(dinePlanUser.Locations);
                                if (scLocations.Any())
                                    foreach (var i in scLocations.Select(a => a.Id))
                                        if (allLocations.Items.Select(a => a.Value).Contains(i.ToString()))
                                            outputUsers.Add(dinePlanUser);
                            }
                            else
                            {
                                
                                outputUsers.Add(dinePlanUser);
                            }
                }
                else if (input.LocationId > 0)
                {
                    foreach (var dinePlanUser in allUsers)
                        if (await _locAppService.IsLocationExists(new CheckLocationInput
                        {
                            LocationId = input.LocationId,
                            Locations = dinePlanUser.Locations,
                            Group = dinePlanUser.Group,
                            LocationTag = dinePlanUser.LocationTag,
                            NonLocations = dinePlanUser.NonLocations
                        }))
                            outputUsers.Add(dinePlanUser);
                }
                else
                {
                    foreach (var dinePlanUser in allUsers) outputUsers.Add(dinePlanUser);
                }
            }
            catch (Exception exception)
            {
                var mess = exception.Message;
            }

            List<ApiDinePlanUserListDto> allPlanUsers = new List<ApiDinePlanUserListDto>();

            foreach (var dinePlanUser in outputUsers)
            {
                var apiDi = dinePlanUser.MapTo<ApiDinePlanUserListDto>();
                if (dinePlanUser.UserId.HasValue)
                {
                    var output = await 
                        _userAppService.ApiGetUser(new ApiLocationInput()
                        {
                            TenantId = input.TenantId,
                            UserId = dinePlanUser.UserId.Value
                        });

                    if (output != null)
                    {
                        apiDi.IsActive = output.IsActive;
                        apiDi.ConnectUser = output.UserName;
                        apiDi.ConnectPassword = output.Password;
                        apiDi.Name = output.Name;
                    }
                }
                allPlanUsers.Add(apiDi);
            }
            return new ListResultOutput<ApiDinePlanUserListDto>(allPlanUsers);
        }

        public async Task<ListResultOutput<ApiDinePlanUserListDto>> GetUsersByLocation(ApiLocationInput input)
        {
            return await ApiUsersByLocation(input);
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _dineplanuserRepo.GetAsync(input.Id);
                item.IsDeleted = false;

                await _dineplanuserRepo.UpdateAsync(item);
            }
        }

        protected virtual async Task UpdateDinePlanUser(CreateOrUpdateDinePlanUserInput input)
        {
            var dto = input.DinePlanUser;
            if (input.DinePlanUser.Id != null)
            {
                var item = await _dineplanuserRepo.GetAsync(input.DinePlanUser.Id.Value);
                dto.MapTo(item);
                UpdateLocationAndNonLocation(item, input.LocationGroup);
                CheckErrors(await _dineplanuserManager.CreateSync(item));
            }
        }

        protected virtual async Task CreateDinePlanUser(CreateOrUpdateDinePlanUserInput input)
        {
            var dto = input.DinePlanUser.MapTo<DinePlanUser>();
            UpdateLocationAndNonLocation(dto, input.LocationGroup);
            CheckErrors(await _dineplanuserManager.CreateSync(dto));
        }
    }
}