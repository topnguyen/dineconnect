﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Authorization.Dto;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Connect.User.Dtos;
using DinePlan.DineConnect.Connect.Users;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.User.Implementation
{
    public class DinePlanUserRoleAppService : DineConnectAppServiceBase, IDinePlanUserRoleAppService
    {
        private readonly IRepository<Department> _departmentRepo;
        private readonly IRepository<DinePlanPermission> _dinePlanPermission;
        private readonly IDinePlanUserRoleListExcelExporter _dineplanuserroleExporter;
        private readonly IDinePlanUserRoleManager _dineplanuserroleManager;
        private readonly IRepository<DinePlanUserRole> _dineplanuserroleRepo;
        private readonly string _dPermission = "Department Permissions";

        private readonly string[] _permissions =
        {
            "#Navigation Permissions",
            "Open Navigation",
            "Open FrontDesk",
            "Open BackOffice",
            "Open WorkPeriods",
            "Open Tickets",
            "Open POS",
            "Open Quick Reports",
            "Open Till Transactions",
            "Open Custom Reports",
            "Open Reserve",
            "Open Message",
            "Open Cash Audit",
            "Open PayInSlip",

            "#WorkDay Permissions",
            "Start WorkPeriods",
            "End WorkPeriods",
            "End WorkShift",
            "Reset WorkShift",
            "Display Old Work Periods",
            "Display Old Work Shifts",
            "Pos Training",
            "Change WorkShift Float Amount",
            "Can PreOrder",
            "Can Non-PreOrder",
            "Change Float",

            "#Ticket Permissions",
            "Add Items To Locked Tickets",
            "Move Orders",
            "Change Combo Item",
            "Move Unlocked Orders",
            "Change Item Price",
            "Display Old Tickets",
            "Display Other Waiters Tickets",
            "Display Ticket",
            "Display Total in Ticket Explorer",
            "Re-Open Non Current Period Tickets",
            "Change Entity",
            "Print Ticket",
            "Merge Tickets",
            "Refund Ticket",
            "Return Ticket",
            "Generate Invoice",

            "#Reporting Permissions",

            "Abnormal End Day Report",
            "Active Promotions Report",
            "Cash Audit Report",
            "Cash Summary Report",
            "Change Report Date",
            "Combo Sales Report",
            "Comp Report",
            "Credit Card Report",
            "Credit Sales Report",
            "Customer Total Summary Report",
            "Daily Reconciliation Report",
            "Daily Sales Summary Report",
            "Daily Terminal Sales Summary Report",
            "Day Summary Report",
            "Deliverer Report",
            "Deliverer Summary Report",
            "Deliverer Tag Summary Report",
            "DelivererSummaryReport",
            "Detail Discount Report",
            "Display Today Date",
            "Down Sell By Amount Report",
            "Down Sell By Quantity Report",
            "Exchange Report",
            "Full Tax Invoice Report",
            "Gift Report",
            "Internal Sales Meeting Report",
            "Item Category Sales By Date",
            "Items Category Date Sales Report",
            "Item Sales By Category Report",
            "Items Category Sales Report",
            "Items Department Sales Report",
            "Items Discount Report",
            "Items Hourly Sales Report",
            "Items Order Sales Report",
            "Items Portion Sales Report",
            "Items Sales Report",
            "Items Tag Sales Report",
            "Items User Sales Report",
            "Log Report",
            "Meal Time Sales Report",
            "Member Open Tickets Report",
            "Member Report",
            "Member Summary Open Tickets Report",
            "Member Summary Report",
            "Member Total Summary Report",
            "Menu Items Sync Report",
            "Non-Sales Tax Report",
            "Order State Report",
            "Order Tag Report",
            "Payment Summary Report",
            "Print Till Transactions Report",
            "Print WorkDay Report",
            "Print WorkShift Report",
            "Product List Report",
            "Refund Report",
            "Reprint Receipt Report",
            "Return Product Report",
            "Sales By Period Report",
            "Sales Promotion Report",
            "Sales Tax Report",
            "Speed Of Service Report",
            "Tender Report",
            "Ticket and Orders Report",
            "Ticket Table Report",
            "Ticket Tag Group Sales Report",
            "Ticket Tag Items Sales Report",
            "Tickets Hourly Sales Report",
            "Tickets Report",
            "Tickets Tax Report",
            "Till Transactions Report",
            "Time Attendance Report",
            "Top Sell By Amount Report",
            "Top Sell By Quantity Report",
            "Payment Summary By Terminal Report",
            "Void Report",
            "Work Day Report",
            "Work Shift Report",


            "#Department Permissions",
            "Change Department",
            "Use Department"
        };

        private readonly ISyncAppService _syncService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IDinePlanUserAppService _userAppService;

        public DinePlanUserRoleAppService(ISyncAppService syncService, IDinePlanUserRoleManager dineplanuserroleManager,
            IRepository<DinePlanUserRole> dinePlanUserRoleRepo,
            IDinePlanUserRoleListExcelExporter dineplanuserroleExporter,
            IRepository<DinePlanPermission> dinePlanPermission, IDinePlanUserAppService userAppService,
            IRepository<Department> dRepo
            , IUnitOfWorkManager unitOfWorkManager
        )
        {
            _syncService = syncService;
            _dineplanuserroleManager = dineplanuserroleManager;
            _dineplanuserroleRepo = dinePlanUserRoleRepo;
            _dineplanuserroleExporter = dineplanuserroleExporter;
            _dinePlanPermission = dinePlanPermission;
            _userAppService = userAppService;
            _departmentRepo = dRepo;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task<PagedResultOutput<DinePlanUserRoleListDto>> GetAll(GetDinePlanUserRoleInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var allItems = _dineplanuserroleRepo
                    .GetAll()
                    .Where(i => i.IsDeleted == input.IsDeleted)
                    .WhereIf(
                        !input.Filter.IsNullOrEmpty(),
                        p => p.Id.Equals(input.Filter)
                    );
                if (!input.Location.IsNullOrEmpty())
                    allItems = allItems.Where(r => r.Department.Locations.Contains(input.Location));

                var allDtos = from a in allItems
                    select new DinePlanUserRoleListDto
                    {
                        Name = a.Name,
                        IsAdmin = a.IsAdmin,
                        CreationTime = a.CreationTime,
                        Id = a.Id
                    };

                var sortMenuItems = await allDtos
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var allListDtos = sortMenuItems.MapTo<List<DinePlanUserRoleListDto>>();

                var allItemCount = await allItems.CountAsync();

                return new PagedResultOutput<DinePlanUserRoleListDto>(
                    allItemCount,
                    allListDtos
                );
            }
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _dineplanuserroleRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<DinePlanUserRoleListDto>>();

            foreach (var item in allListDtos)
            {
                var listPermission = GetGrantedPermission(new NullableIdInput(item.Id));

                item.Permission = listPermission.JoinAsString(", ");
            }

            return _dineplanuserroleExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDinePlanUserRoleForEditOutput> GetDinePlanUserRoleForEdit(NullableIdInput input)
        {
            DinePlanUserRoleEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _dineplanuserroleRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<DinePlanUserRoleEditDto>();
            }
            else
            {
                editDto = new DinePlanUserRoleEditDto();
            }

            return new GetDinePlanUserRoleForEditOutput
            {
                DinePlanUserRole = editDto,
                Permissions = GetPermissions(),
                GrantedPermissionNames = GetGrantedPermission(input)
            };
        }

        public async Task CreateOrUpdateDinePlanUserRole(CreateOrUpdateDinePlanUserRoleInput input)
        {
            if (input.DinePlanUserRole.Id.HasValue)
                await UpdateDinePlanUserRole(input);
            else
                await CreateDinePlanUserRole(input);
            await _syncService.UpdateSync(SyncConsts.USER);
        }

        public async Task DeleteDinePlanUserRole(IdInput input)
        {
            await _dineplanuserroleRepo.DeleteAsync(input.Id);
        }

        public async Task<ListResultOutput<DinePlanUserRoleListDto>> GetIds()
        {
            var lstDinePlanUserRole = await _dineplanuserroleRepo.GetAll().ToListAsync();
            return
                new ListResultOutput<DinePlanUserRoleListDto>(
                    lstDinePlanUserRole.MapTo<List<DinePlanUserRoleListDto>>());
        }

        public async Task<GetDinePlanUserRoleOutputList> GetUserRoles(GetUserRolesInput input)
        {
            if (input.TenantId <= 0) return null;
            GetDinePlanUserForEditOutput user = null;
            var lstDinePlanUserRole = await _dineplanuserroleRepo.GetAllListAsync(a => a.TenantId == input.TenantId);
            if (input.UserId > 0)
                user = await _userAppService.GetDinePlanUserForEdit(new NullableIdInput(input.UserId));

            if (user != null)
                lstDinePlanUserRole =
                    lstDinePlanUserRole.Where(a => a.Id == user.DinePlanUser.DinePlanUserRoleId).ToList();
            var lst = lstDinePlanUserRole.Select(dto => new GetDinePlanUserRoleForEditOutput
            {
                DinePlanUserRole = dto.MapTo<DinePlanUserRoleEditDto>(),
                GrantedPermissionNames = GetGrantedPermission(new NullableIdInput(dto.Id))
            }).ToList();

            return new GetDinePlanUserRoleOutputList
            {
                Items = lst
            };
        }


        public async Task<GetDinePlanUserRoleOutputList> ApiGetUserRoles(GetUserRolesNameInput input)
        {
            if (input.TenantId <= 0) return null;
            var lstDinePlanUserRole =
                await
                    _dineplanuserroleRepo.GetAllListAsync(
                        a => a.TenantId == input.TenantId && a.Name.Equals(input.Name));

            var lst = lstDinePlanUserRole.Select(dto => new GetDinePlanUserRoleForEditOutput
            {
                DinePlanUserRole = dto.MapTo<DinePlanUserRoleEditDto>(),
                GrantedPermissionNames = GetGrantedPermission(new NullableIdInput(dto.Id))
            }).ToList();

            return new GetDinePlanUserRoleOutputList
            {
                Items = lst
            };
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _dineplanuserroleRepo.GetAsync(input.Id);
                item.IsDeleted = false;

                await _dineplanuserroleRepo.UpdateAsync(item);
            }
        }

        private List<string> GetGrantedPermission(NullableIdInput input)
        {
            var allRolesS = new List<string>();
            if (input.Id.HasValue)
            {
                var allRoles =
                    _dinePlanPermission.GetAll()
                        .Where(
                            a =>
                                a.DinePlanUserRoleId != null && a.DinePlanUserRoleId == input.Id.Value &&
                                a.IsGrantedByDefault);
                if (allRoles.Any())
                    allRolesS.AddRange(allRoles.ToList().Select(dinePlanPermission => dinePlanPermission.Name));
            }

            return allRolesS;
        }

        private List<FlatPermissionDto> GetPermissions()
        {
            var permissions = new List<FlatPermissionDto>();

            permissions.Add(new FlatPermissionDto
            {
                Description = null,
                DisplayName = "Permissions",
                IsGrantedByDefault = false,
                Name = "Permissions",
                ParentName = null
            });

            var category = "";
            foreach (var item in _permissions)
            {
                if (string.IsNullOrWhiteSpace(item)) continue;
                if (item.StartsWith("#"))
                {
                    category = item.Substring(1);
                    permissions.Add(new FlatPermissionDto
                    {
                        ParentName = "Permissions",
                        Name = category,
                        DisplayName = category,
                        IsGrantedByDefault = false,
                        Description = null
                    });
                }
                else if (!string.IsNullOrEmpty(category))
                {
                    permissions.Add(new FlatPermissionDto
                    {
                        ParentName = category,
                        Name = item,
                        DisplayName = item
                    });
                }
            }

            foreach (var department in _departmentRepo.GetAll())
                permissions.Add(new FlatPermissionDto
                {
                    ParentName = _dPermission,
                    Name = "UseDepartment_" + department.Id,
                    DisplayName = department.Name
                });
            return permissions;
        }

        protected virtual async Task UpdateDinePlanUserRole(CreateOrUpdateDinePlanUserRoleInput input)
        {
            var item = await _dineplanuserroleRepo.GetAsync(input.DinePlanUserRole.Id.Value);
            var dto = input.DinePlanUserRole;
            item.Name = dto.Name;
            item.IsAdmin = dto.IsAdmin;
            item.DepartmentId = dto.DepartmentId;
            await SetGrantedPermissionsAsync(dto.Id.Value, input.GrantedPermissionNames);
        }

        protected virtual async Task CreateDinePlanUserRole(CreateOrUpdateDinePlanUserRoleInput input)
        {
            var dto = input.DinePlanUserRole.MapTo<DinePlanUserRole>();
            dto.TenantId = AbpSession.TenantId ?? 0;
            CheckErrors(await _dineplanuserroleManager.CreateSync(dto));
            await CurrentUnitOfWork.SaveChangesAsync(); //It's done to get Id of the role.
            await SetGrantedPermissionsAsync(dto.Id, input.GrantedPermissionNames);
        }

        public virtual async Task SetGrantedPermissionsAsync(int role, IEnumerable<string> permissions)
        {
            var oldPermissions = await GetGrantedPermissionsAsync(role);
            var oldPermissionsNames = new List<string>();
            if (oldPermissions != null) oldPermissionsNames = oldPermissions.Select(a => a.Name).ToList();
            var newPermissions = permissions.ToArray();
            var addList = newPermissions.Except(oldPermissionsNames).ToList();
            var removeList = oldPermissionsNames.Except(newPermissions).ToList();

            foreach (var myAddList in addList)
            {
                var perm =
                    await
                        _dinePlanPermission.FirstOrDefaultAsync(
                            a => a.Name.Equals(myAddList) && a.DinePlanUserRoleId == role);
                if (perm != null)
                    perm.IsGrantedByDefault = true;
                else
                    await _dinePlanPermission.InsertAsync(new DinePlanPermission
                    {
                        TenantId = AbpSession.TenantId ?? 0,
                        DinePlanUserRoleId = role,
                        IsGrantedByDefault = true,
                        Name = myAddList
                    });
            }

            foreach (var myAddList in removeList)
            {
                var perm =
                    await
                        _dinePlanPermission.FirstOrDefaultAsync(
                            a => a.Name.Equals(myAddList) && a.DinePlanUserRoleId == role);
                perm.IsGrantedByDefault = false;
                _dinePlanPermission.Update(perm);
            }
        }

        public virtual async Task<IReadOnlyList<DinePlanPermission>> GetGrantedPermissionsAsync(int roleId)
        {
            if (_dinePlanPermission.GetAll().Any())
                return await _dinePlanPermission.GetAllListAsync(a => a.DinePlanUserRoleId != null
                                                                      && a.DinePlanUserRoleId == roleId &&
                                                                      a.IsGrantedByDefault);
            return null;
        }
    }
}