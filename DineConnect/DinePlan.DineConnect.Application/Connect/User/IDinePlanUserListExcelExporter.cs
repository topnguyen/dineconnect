﻿using System.Collections.Generic;
using DinePlan.DineConnect.Connect.User.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.User
{
    public interface IDinePlanUserListExcelExporter
    {
        FileDto ExportToFile(List<DinePlanUserListDto> dtos);
    }
}