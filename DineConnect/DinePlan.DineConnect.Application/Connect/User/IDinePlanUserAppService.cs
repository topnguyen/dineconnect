﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.User.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.Dtos;

namespace DinePlan.DineConnect.Connect.User
{
    public interface IDinePlanUserAppService : IApplicationService
    {
        Task<PagedResultOutput<DinePlanUserListDto>> GetAll(GetDinePlanUserInput inputDto);

        Task<FileDto> GetAllToExcel();

        Task<GetDinePlanUserForEditOutput> GetDinePlanUserForEdit(NullableIdInput nullableIdInput);

        Task CreateOrUpdateDinePlanUser(CreateOrUpdateDinePlanUserInput input);

        Task DeleteDinePlanUser(IdInput input);

        Task<ListResultOutput<DinePlanUserListDto>> GetIds();


        Task ActivateItem(IdInput input);
        Task<ListResultOutput<ApiDinePlanUserListDto>> GetUsersByLocation(ApiLocationInput input);

        Task<ListResultOutput<ApiDinePlanUserListDto>> ApiUsersByLocation(ApiLocationInput input);

    }
}