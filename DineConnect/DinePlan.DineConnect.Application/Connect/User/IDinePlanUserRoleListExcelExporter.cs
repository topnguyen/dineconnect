﻿using System.Collections.Generic;
using DinePlan.DineConnect.Connect.User.Dtos;
using DinePlan.DineConnect.Dto;


namespace DinePlan.DineConnect.Connect.User
{
    public interface IDinePlanUserRoleListExcelExporter
    {
        FileDto ExportToFile(List<DinePlanUserRoleListDto> dtos);
    }
}
