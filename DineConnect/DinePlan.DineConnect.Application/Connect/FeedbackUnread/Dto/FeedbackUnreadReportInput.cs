﻿using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.FeedbackUnread.Dto
{
    public class FeedbackUnreadReportInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public bool Unread { get; set; }
        public bool Acknowledgement { get; set; }
        public bool Replies { get; set; }
        
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int Location { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public List<SimpleLocationDto> Locations { get; set; }
        public long UserId { get; set ; }
        public string DynamicFilter { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
}
