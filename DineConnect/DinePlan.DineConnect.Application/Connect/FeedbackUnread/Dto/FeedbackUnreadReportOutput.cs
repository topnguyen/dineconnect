﻿using DinePlan.DineConnect.Connect.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.FeedbackUnread.Dto
{
    public class FeedbackUnreadReportOutput
    {
        public int Id { get; set; }
        public string MessageHeader { get; set; }
        public string Type { get; set; }
        public int PlantId { get; set; }
        public string Location { get; set; }
        public bool Unread => !Acknowledgement && !Replies;
        public bool Acknowledgement { get; set; }
        public bool Reply { get; set; }
        public bool Replies { get; set; }
        public string Content { get; set; }
        public bool IsTextContent => Type == TickMessageType.Text.ToString();
    }
}
