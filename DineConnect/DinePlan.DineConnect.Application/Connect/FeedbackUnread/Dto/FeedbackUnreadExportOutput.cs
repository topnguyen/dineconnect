﻿using DinePlan.DineConnect.Connect.Messages;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Exporter;
using OpenHtmlToPdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.FeedbackUnread.Dto
{
    public class FeedbackUnreadExportOutput 
    {
        public int PlantId { get; set; }
        public string PlantCode { get; set; }
        public string Location { get; set; }
        public string MessageHeader { get; set; }
        public string Type { get; set; }
        public bool IsTextContent => Type == TickMessageType.Text.ToString();
        public bool Unread { get; set; }
        public bool Acknowledgement { get; set; }
        public bool Replied { get; set; }
        public string Reply { get; set; }
        public string Content { get; set; }
        public string Terminal { get; set; }
        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public string ResponsedsName { get; set; }
        public string RepliedTime { get; set; }
        public string CreationTime { get; set; }
    }
}
