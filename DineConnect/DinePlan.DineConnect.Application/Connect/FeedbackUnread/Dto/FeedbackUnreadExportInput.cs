﻿using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Exporter;
using OpenHtmlToPdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.FeedbackUnread.Dto
{
    public class FeedbackUnreadExportInput : FeedbackUnreadReportInput, IFileExport, IBackgroundExport
    {
        public string OutputType { get; set; }
        public ExportType ExportOutputType { get; set; }
        public PaperSize PaperSize { get; set; }
        public bool Portrait { get; set; }
        public bool RunInBackground { get; set; }
        public string ReportDescription { get; set; }

        public int TenantId { get; set; }
    }
}
