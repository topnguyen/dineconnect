﻿using DinePlan.DineConnect.Connect.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.FeedbackUnread.Dto
{
    public class FeedbackUnreadDetail
    {
        public string MessageHeader { get; set; }
        public string Type { get; set; }
        public string CreatedOn { get; set; }
        public string Content { get; set; }
        public bool IsTextContent => Type == TickMessageType.Text.ToString();
        public string User { get; set; }
        public string ResponsedOn { get; set; }
        public int TickMessageId { get; set; }
        public bool Acknowledgement { get; set; }
        public bool Reply { get; set; }



    }
}
