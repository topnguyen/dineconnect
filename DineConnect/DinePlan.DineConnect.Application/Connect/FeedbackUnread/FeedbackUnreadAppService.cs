﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Linq.Dynamic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.BackgroundJobs;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.FeedbackUnread.Dto;
using DinePlan.DineConnect.Connect.FeedbackUnread.Exporting;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Message;
using DinePlan.DineConnect.Connect.Messages;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Users;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Helper;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Report;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace DinePlan.DineConnect.Connect.FeedbackUnread
{
    public class FeedbackUnreadAppService : DineConnectAppServiceBase, IFeedbackUnreadAppService
    {
        private readonly IRepository<TickMessageReply> _tickMessageReplyRepo;
        private readonly IRepository<TickMessage> _tickMessageRepo;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ILocationAppService _locationService;
        private readonly IBackgroundJobManager _backgroundJobManager;
        private readonly IReportBackgroundAppService _reportBackgroundService;
        private readonly IFeedbackUnreadReportExporter _exporter;
        private readonly ITickMessageAppService _tickMessageAppService;
        private IRepository<Master.Location> _locationRepository;
        private readonly UserManager _userManager;
        private readonly IRepository<DinePlanUser> _dinePlanUserRepository;


        public FeedbackUnreadAppService(IRepository<TickMessageReply> tickMessageReplyRepo,
                                        IRepository<TickMessage> tickMessageRepo,
                                        IUnitOfWorkManager unitOfWorkManager,
                                        ILocationAppService locationService,
                                        IBackgroundJobManager backgroundJobManager,
                                        IReportBackgroundAppService reportBackgroundService,
                                        IFeedbackUnreadReportExporter exporter,
                                        ITickMessageAppService tickMessageAppService,
                                        IRepository<Master.Location> locationRepository,
                                        UserManager userManager,
                                        IRepository<DinePlanUser> dinePlanUserRepository)
        {
            _tickMessageRepo = tickMessageRepo;
            _tickMessageReplyRepo = tickMessageReplyRepo;
            _unitOfWorkManager = unitOfWorkManager;
            _locationService = locationService;
            _backgroundJobManager = backgroundJobManager;
            _reportBackgroundService = reportBackgroundService;
            _exporter = exporter;
            _tickMessageAppService = tickMessageAppService;
            _locationRepository = locationRepository;
            _userManager = userManager;
            _dinePlanUserRepository = dinePlanUserRepository;
        }

        public async Task<PagedResultOutput<FeedbackUnreadReportOutput>> GetAllReport(FeedbackUnreadReportInput input)
        {
            using (_unitOfWorkManager.Current.EnableFilter(AbpDataFilters.SoftDelete))
            {
                if (!input.Filter.IsNullOrEmpty())
                    input.Filter = Regex.Replace(input.Filter.Trim(), @"\s+", " ");

                var query = _tickMessageRepo.GetAll()
                    .WhereIf(!input.Filter.IsNullOrEmpty(),
                        p => p.MessageHeading.ToLower().Contains(input.Filter?.ToLower())
                        || p.MessageContent.ToLower().Contains(input.Filter?.ToLower()));

                query = FilterLocation(query, input);
                if (input.StartDate != null)
                    query = query.Where(x => x.CreationTime.Date >= (input.StartDate?.Date ?? new DateTime()));
                if (input.EndDate != null)
                    query = query.Where(x => x.CreationTime.Date <= input.EndDate?.Date);

                var report = query.Select(x => new FeedbackUnreadReportOutput
                {
                    Id = x.Id,
                    MessageHeader = x.MessageHeading,
                    Type = x.MessageType.ToString(),
                    Acknowledgement = x.Acknowledgement,
                    Content = x.MessageContent,
                    Reply = x.Reply
                });

                var dataAsQueryable = report.AsQueryable();

                if (!string.IsNullOrEmpty(input.DynamicFilter))
                {
                    var jsonSerializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver =
                            new CamelCasePropertyNamesContractResolver()
                    };
                    var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                    if (filRule?.Rules != null)
                    {
                        dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                    }
                }


                var result = dataAsQueryable
                    .OrderBy(input.Sorting)
                    .ToList();
                var qr = new PagedResultOutput<FeedbackUnreadReportOutput>(result.Count, result);
                return new PagedResultOutput<FeedbackUnreadReportOutput>(result.Count, result);
            }
        }

        public async Task<List<FeedbackUnreadExportOutput>> GetDetailExport(FeedbackUnreadExportInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.MustHaveTenant, AppConsts.ConnectFilter))
            {
                var dateTimeFormat = await SettingManager.GetSettingValueForTenantAsync(AppSettings.ConnectSettings.DateTimeFormat, input.TenantId);
                var dateTimeFormat24 = dateTimeFormat.Replace("hh", "HH");
                var report = new List<FeedbackUnreadExportOutput>();
                if (!input.Filter.IsNullOrEmpty())
                    input.Filter = Regex.Replace(input.Filter.Trim(), @"\s+", " ");

                var query = _tickMessageRepo.GetAll()
                    .WhereIf(!input.Filter.IsNullOrEmpty(),
                        p => p.MessageHeading.ToLower().Contains(input.Filter?.ToLower())
                        || p.MessageContent.ToLower().Contains(input.Filter?.ToLower()));

                query = FilterLocation(query, input);
                if (input.StartDate != null)
                    query = query.Where(x => x.CreationTime.Date >= input.StartDate?.Date);
                if (input.EndDate != null)
                    query = query.Where(x => x.CreationTime.Date <= input.EndDate?.Date);

                List<Master.Location> allLocations = GetAllLocations(input);

                foreach (var tickMessage in query.ToList())
                {
                    var locMess = JsonConvert.DeserializeObject<List<Master.Location>>(tickMessage.Locations);
                    var locResult = new List<Master.Location>();
                    if (locMess != null)
                    {
                        foreach (var loc in locMess)
                        {
                            var locExist = allLocations.FirstOrDefault(s => s.Id == loc.Id);
                            if (locExist != null)
                            {
                                locResult.Add(loc);
                            }
                        }
                        var resultByLoc = GetItemByLocations(locResult, tickMessage, input, dateTimeFormat24);
                        report.AddRange(resultByLoc);
                    }
                    else
                    {
                        var resultByLoc = GetItemByLocations(allLocations, tickMessage, input, dateTimeFormat24);
                        report.AddRange(resultByLoc);
                    }

                }
                var dataAsQueryable = report.AsQueryable();
                if (!string.IsNullOrEmpty(input.DynamicFilter))
                {
                    var jsonSerializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver =
                            new CamelCasePropertyNamesContractResolver()
                    };
                    var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                    if (filRule?.Rules != null)
                    {
                        dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                    }
                }
                var result = dataAsQueryable.OrderBy(x => x.MessageHeader).ThenBy(x => x.PlantId).ThenBy(x => x.RepliedTime).ThenBy(x => x.Terminal).ToList();
                return result;
            }
        }


        private List<FeedbackUnreadExportOutput> GetItemByLocations(List<Master.Location> locations, TickMessage tickMessage, FeedbackUnreadExportInput input, string dateTimeFormat24)
        {
            var report = new List<FeedbackUnreadExportOutput>();
            foreach (var location in locations)
            {
                var tickMessageReply = _tickMessageReplyRepo.GetAll().Include(x => x.Terminal).FirstOrDefault(t => t.TickMessageId == tickMessage.Id && t.LocationId == location.Id);
                //var employeeCreateMessage =await UserManager.GetUserByIdAsync(tickMessage.CreatorUserId ?? 1);
                if (tickMessageReply != null)
                {
                    //var employeeReply = _dinePlanUserRepository.GetAll().FirstOrDefault(t => t.Name == tickMessageReply.User && t.TenantId == input.TenantId);
                    report.Add(new FeedbackUnreadExportOutput()
                    {
                        PlantId = location.Id,
                        PlantCode = location.Code,
                        Location = location.Name,
                        Acknowledgement = true,
                        Content = tickMessageReply.Reply,
                        Unread = false,
                        MessageHeader = tickMessage.MessageHeading,
                        Reply = tickMessageReply.Reply,
                        Type = tickMessage.MessageType.ToString(),
                        RepliedTime = tickMessageReply.LastModificationTime == null
                                ? tickMessageReply.CreationTime.ToString(dateTimeFormat24)
                                : tickMessageReply.LastModificationTime.Value.ToString(dateTimeFormat24),
                        Terminal = tickMessageReply.Terminal?.Name,
                        ResponsedsName = tickMessageReply.User,
                        CreationTime = tickMessage.CreationTime.ToString(dateTimeFormat24),
                        Replied = true,
                        EmployeeCode = tickMessageReply.UserId,
                        EmployeeName = tickMessageReply.User,
                    });
                }
                else
                {
                    report.Add(new FeedbackUnreadExportOutput()
                    {
                        PlantId = location.Id,
                        PlantCode = location.Code,
                        Location = location.Name,
                        Acknowledgement = false,
                        Content = "",
                        Unread = true,
                        MessageHeader = tickMessage.MessageHeading,
                        Reply = null,
                        Type = tickMessage.MessageType.ToString(),
                        RepliedTime = "",
                        ResponsedsName = "",
                        Terminal = "",
                        CreationTime = tickMessage.CreationTime.ToString(dateTimeFormat24),
                        Replied = false,
                        EmployeeCode = "",
                        EmployeeName = "",
                    });
                }
            }
            return report;
        }


        private List<Master.Location> GetAllLocations(FeedbackUnreadExportInput input)
        {
            var allLocations = new List<Master.Location>();

            var locationIDs = new List<int>();

            if (input.Location > 0)
            {
                locationIDs.Add(input.Location);
            }
            else if (input.Locations != null && input.Locations.Any())
            {
                locationIDs = input.Locations.Select(a => a.Id).ToList();
            }
            else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
                     && !input.LocationGroup.Locations.Any())
            {
                locationIDs = _locationService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
            }
            else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                locationIDs = _locationService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
            }
            else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                locationIDs = _locationService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true
                });
            }
            if (input.LocationGroup != null)
            {
                if (input.LocationGroup.NonLocations != null && input.LocationGroup.NonLocations.Any())
                {
                    locationIDs = input.LocationGroup.NonLocations.Select(a => a.Id).ToList();
                }
            }

            else if (input.LocationGroup == null)
            {
                locationIDs = _locationService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = new List<SimpleLocationDto>(),
                    Group = false,
                    UserId = input.UserId
                });
            }


            if (locationIDs.Count > 0)
            {
                allLocations = _locationRepository.GetAll().Where(x => locationIDs.Any(a => a == x.Id) && x.TenantId == input.TenantId).ToList();
            }
            else
            {
                allLocations = _locationRepository.GetAll().Where(x => x.TenantId == input.TenantId).ToList();
            }

            return allLocations;
        }

        public async Task<FeedbackUnreadDetail> GetDetail(NullableIdInput input)
        {
            var output = new FeedbackUnreadDetail();
            if (input.Id.HasValue)
            {
                var message = _tickMessageRepo.FirstOrDefault(y => y.Id == input.Id);
                DateTime latestResponsedOn = DateTime.MinValue;
                if (message.Replies != null && message.Replies.Count > 0)
                {
                    message.Replies = message.Replies.OrderByDescending(r => r.CreationTime).ToList();
                    latestResponsedOn = message.Replies.First().LastModificationTime ?? message.Replies.First().CreationTime;
                }
                var user = message.CreatorUserId != null
                    ? await _userManager.FindByIdAsync(message.CreatorUserId.Value)
                    : null;
                output.TickMessageId = message.Id;
                output.MessageHeader = message.MessageHeading;
                output.Type = message.MessageType.ToString();
                output.CreatedOn = message.CreationTime.ToString("dd MMMM yyyy hh:mm tt",
                    CultureInfo.CreateSpecificCulture("en-US"));
                output.Content = message.MessageContent;
                output.User = user != null ? user.Name : "";
                output.Acknowledgement = message.Acknowledgement;
                output.Reply = message.Reply;
                output.ResponsedOn = latestResponsedOn != DateTime.MinValue
                    ? latestResponsedOn.ToString("dd MMMM yyyy hh:mm tt",
                        CultureInfo.CreateSpecificCulture("en-US"))
                    : "";
            }
            return output;
        }

        private IEnumerable<TickMessage> FilterLocation(IEnumerable<TickMessage> query, FeedbackUnreadReportInput input)
        {
            var result = query.ToList();
            var resultExport = new List<TickMessage>();
            var allLocation = _locationRepository.GetAll();
            var allTickMsgReplies = _tickMessageReplyRepo.GetAll();
            var matchedReplies = new List<TickMessageReply>();
            var locations = new List<int>();
            if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                locations = _locationService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
            }
            else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                locations = _locationService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true
                });
            }

            foreach (var tickMessage in result)
            {
                if (!string.IsNullOrEmpty(tickMessage.Locations) && locations.Count > 0)
                {
                    bool existLoc = false;
                    var locationTickMess = JsonHelper.Deserialize<List<SimpleLocationDto>>(tickMessage.Locations).Select(s => s.Id).ToList();

                    foreach (var locMes in locations)
                    {
                        if (locationTickMess.Contains(locMes))
                        {
                            existLoc = true;
                        }
                    }
                    if (existLoc)
                    {
                        resultExport.Add(tickMessage);
                    }
                }
                else
                {
                    resultExport.Add(tickMessage);
                }
            }

            foreach (var tickMessage in resultExport.ToList())
            {
                if (input.Unread)
                {
                    var unreadLocations = _locationRepository.GetAll().Where(l => !l.TickMessageReplies.Where(t => t.TickMessageId == tickMessage.Id).Any());
                    if (unreadLocations == null || unreadLocations.Count() == 0)
                    {
                        result.Remove(tickMessage); // Remove item
                    }
                }
                else
                {
                    if (input.Replies)
                    {
                        var replyLocations = _locationRepository.GetAll().Where(l => l.TickMessageReplies.Where(t => t.TickMessageId == tickMessage.Id && t.Reply != null).Any());
                        if (replyLocations == null || replyLocations.Count() == 0)
                        {
                            result.Remove(tickMessage); // Remove item
                        }
                    }
                    else if (input.Acknowledgement)
                    {
                        var accknowledgementLocations = _locationRepository.GetAll().Where(l => l.TickMessageReplies.Where(t => t.TickMessageId == tickMessage.Id && t.Acknowledged == true).Any());
                        if (accknowledgementLocations == null || accknowledgementLocations.Count() == 0)
                        {
                            result.Remove(tickMessage); // Remove item
                        }
                    }
                }
            }

            return resultExport;
        }

        public async Task<FileDto> GetReportToFile(FeedbackUnreadExportInput input)
        {
            input.OutputType = "EXPORT";
            input.TenantId = AbpSession.TenantId.Value;
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _reportBackgroundService.CreateOrUpdate(new Report.Dtos.CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.FEEDBACKUNREAD,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                    {
                        var feedbackUnreadExportInput = input;
                        feedbackUnreadExportInput.UserId = AbpSession.UserId.Value;

                        await _backgroundJobManager.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.FEEDBACKUNREAD,
                            FeedbackUnreadExportInput = feedbackUnreadExportInput,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                    }
                }
                else
                {
                    return await _exporter.ExportToFile(input, this);
                }
            }
            return null;
        }
    }
}
