﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using Abp.Collections.Extensions;
using Abp.Configuration;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.FeedbackUnread.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using Microsoft.Office.Interop.Excel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Spire.Xls;

namespace DinePlan.DineConnect.Connect.FeedbackUnread.Exporting
{
	public class FeedbackUnreadReportExporter : FileExporterBase, IFeedbackUnreadReportExporter
	{
		private readonly IRepository<Master.Location> _locRepo;
		public FeedbackUnreadReportExporter(IRepository<Master.Location> locRepo)
		{
			_locRepo = locRepo;
		}
		public async Task<FileDto> ExportToFile(FeedbackUnreadExportInput input,
			IFeedbackUnreadAppService appService)
		{
			var file = new FileDto("FeedbackUnread_" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".xlsx",
				MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
			if (input.ExportOutputType == 0)
			{
				using (var excelPackage = new ExcelPackage())
				{
					await GetFeedbackUnreadReportExportExcel(excelPackage, input, appService);
					Save(excelPackage, file);
				}
				return ProcessFile(input, file);
			}
			else
			{
				var contentHTML = await ExportDatatableToHtml(input, appService);
				var result = ProcessFileHTML(file, contentHTML);
				return result;
			}
		}
		private async Task GetFeedbackUnreadReportExportExcel(ExcelPackage excelPackage, FeedbackUnreadExportInput input, IFeedbackUnreadAppService appService)
        {
			var sheet = excelPackage.Workbook.Worksheets.Add(L("FeedbackUnread"));
			sheet.OutLineApplyStyle = true;


			sheet.Cells[1, 1].Value = "รายงาน Feedback and Unread Report";
			sheet.Cells[1, 1, 1, 13].Merge = true;
			sheet.Cells[1, 1, 1, 13].Style.Font.Bold = true;
			sheet.Cells[1, 1, 1, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

			var formatDateSetting = await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.SimpleDateFormat);
			var dataRange = "ของวันที่"+ " " + input.StartDate?.ToString(formatDateSetting).Replace("-","/") + " " + "ถึง" + " " + input.EndDate?.ToString(formatDateSetting).Replace("-", "/");
			sheet.Cells[2, 1].Value = dataRange;
			sheet.Cells[2, 1, 2, 13].Merge = true;
			sheet.Cells[2, 1, 2, 13].Style.Font.Bold = true;
			sheet.Cells[2, 1, 2, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

			//var locationName = GetLoacationName(input);

			//sheet.Cells[3, 1].Value = locationName;
			//sheet.Cells[3, 1, 3, 13].Merge = true;
			//sheet.Cells[3, 1, 3, 13].Style.Font.Bold = true;
			//sheet.Cells[3, 1, 3, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

			var headers = new List<string>
					{
						L("MessageHeader"),
						L("Type"),
						L("CreatedOn"),
						L("PlantID"),
						L("PlantName"),
						L("Unread"),
						L("Acknowledgement"),
						L("Replies"),
						L("Content"),
						L("POSNo.(TerminalName)"),
						L("EmployeeNo."),
						L("Responsed`sName"),
						L("ResponsedOn")
					};

			var rowCount = 4;
			var cellHeader = 1;
			foreach (var item in headers.ToArray())
			{
				sheet.Cells[rowCount, cellHeader].Value = item;
				sheet.Cells[rowCount, cellHeader].Style.Border.BorderAround(ExcelBorderStyle.Thin);
				sheet.Cells[rowCount, cellHeader].Style.Font.Bold = true;
				sheet.Cells[rowCount, cellHeader].Style.Font.Color.SetColor(System.Drawing.Color.White);
				cellHeader += 1;
			}
			//set color location
			Color colorHeader = System.Drawing.ColorTranslator.FromHtml("#c0bebe");
			sheet.Cells[$"A{rowCount}:M{rowCount}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
			sheet.Cells[$"A{rowCount}:M{rowCount}"].Style.Fill.BackgroundColor.SetColor(colorHeader);

			rowCount++;
			var colCount = 1;
			input.MaxResultCount = 1000;
			var feedbackUnreadDtos = await appService.GetDetailExport(input);

			if (DynamicQueryable.Any(feedbackUnreadDtos))
			{
				foreach (var feedbackUnread in feedbackUnreadDtos)
				{
					colCount = 1;
					sheet.Cells[rowCount, colCount].Style.Border.BorderAround(ExcelBorderStyle.Thin);
					sheet.Cells[rowCount, colCount++].Value = feedbackUnread.MessageHeader;
					sheet.Cells[rowCount, colCount].Style.Border.BorderAround(ExcelBorderStyle.Thin);
					sheet.Cells[rowCount, colCount++].Value = feedbackUnread.Type;
					sheet.Cells[rowCount, colCount].Style.Border.BorderAround(ExcelBorderStyle.Thin);
					sheet.Cells[rowCount, colCount++].Value = feedbackUnread.CreationTime;
					sheet.Cells[rowCount, colCount].Style.Border.BorderAround(ExcelBorderStyle.Thin);
					sheet.Cells[rowCount, colCount++].Value = feedbackUnread.PlantCode;
					sheet.Cells[rowCount, colCount].Style.Border.BorderAround(ExcelBorderStyle.Thin);
					sheet.Cells[rowCount, colCount++].Value = feedbackUnread.Location;
					sheet.Cells[rowCount, colCount].Style.Border.BorderAround(ExcelBorderStyle.Thin);
					sheet.Cells[rowCount, colCount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
					sheet.Cells[rowCount, colCount++].Value = feedbackUnread.Unread ? "X" : "";
					sheet.Cells[rowCount, colCount].Style.Border.BorderAround(ExcelBorderStyle.Thin);
					sheet.Cells[rowCount, colCount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
					sheet.Cells[rowCount, colCount++].Value = feedbackUnread.Acknowledgement ? "X" : "";
					sheet.Cells[rowCount, colCount].Style.Border.BorderAround(ExcelBorderStyle.Thin);
					sheet.Cells[rowCount, colCount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
					sheet.Cells[rowCount, colCount++].Value = feedbackUnread.Replied ? "X" : "";
					sheet.Cells[rowCount, colCount].Style.Border.BorderAround(ExcelBorderStyle.Thin);
					sheet.Cells[rowCount, colCount++].Value = feedbackUnread.Content;
					sheet.Cells[rowCount, colCount].Style.Border.BorderAround(ExcelBorderStyle.Thin);
					sheet.Cells[rowCount, colCount++].Value = feedbackUnread.Terminal;
					sheet.Cells[rowCount, colCount].Style.Border.BorderAround(ExcelBorderStyle.Thin);
					sheet.Cells[rowCount, colCount++].Value = feedbackUnread.EmployeeCode;
					sheet.Cells[rowCount, colCount].Style.Border.BorderAround(ExcelBorderStyle.Thin);
					sheet.Cells[rowCount, colCount++].Value = feedbackUnread.EmployeeName;
					sheet.Cells[rowCount, colCount].Style.Border.BorderAround(ExcelBorderStyle.Thin);
					sheet.Cells[rowCount, colCount++].Value = feedbackUnread.RepliedTime;
					rowCount++;
				}
			}
			for (var i = 1; i <= colCount + 2; i++) sheet.Column(i).AutoFit();
		}
		private FileDto ProcessFileHTML(FileDto file, string fileHTML)
		{
			File.WriteAllText(Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken), fileHTML);
			file.FileType = MimeTypeNames.ApplicationXhtmlXml;
			file.FileName = Path.GetFileNameWithoutExtension(file.FileName) + ".html";
			return file;
		}
		private async Task<string> ExportDatatableToHtml(FeedbackUnreadExportInput input, IFeedbackUnreadAppService appService)
		{
				var dtos = await appService.GetDetailExport(input);
			StringBuilder strHTMLBuilder = new StringBuilder();
			strHTMLBuilder.Append("<html>");
			strHTMLBuilder.Append("<head>");
			strHTMLBuilder.Append("</head>");

			strHTMLBuilder.Append(@"<style> 
             body  { 
              font:400 12px 'Tahoma';
              font-size: 12px;
              padding:10px;
            }
            table  { 
                margin-top: 10px;
                border-collapse: collapse;
                width: 100%;
                margin-bottom: 10px;
            }

            table td, table th {
                border: 1px solid #ddd;
                padding: 8px;
            }
            table th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #c0bebe;
                color: white;
            }
            thead {
                display: table-header-group;
            }
            tfoot {
                display: table-row-group;
            }
            tr{
                page-break-inside: avoid;
            }
            .text-right{
              text-align:right;
            }

			.margin_bottom_0 {
				margin-bottom: 0 !important;
			}
            .margin_top_0 {
				margin-top: 0 !important;
			}

			.border_bottom_none {
				border-bottom: none !important;
			}

			.custom_table_date {
				margin-bottom: 0px !important;
			}

			.display_flex {
				display: flex;
			}
			.w-100 {
				width: 100%;
			}
	
			.float-right {
				float: right;
			}

			.font_weight_bold {
				font-weight: bold;
			}
            .font_14{
				font-size: 14;
             }
            .text-center{
               text-align: center;          
            }

            table tr:hover {background-color: #ddd;}
            </style>");


			strHTMLBuilder.Append("<body>");
			strHTMLBuilder.Append("<div>");

			var locationName = GetLoacationName(input);
			var formatDateSetting = await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.SimpleDateFormat);
			var dataRange = "ของวันที่"+ " " + input.StartDate?.ToString(formatDateSetting).Replace("-", "/") + " " + "ถึง" + " " + input.EndDate?.ToString(formatDateSetting).Replace("-", "/");

			// add header
			strHTMLBuilder.Append("<table class='table table-bordered custom_table_date border_bottom_none'>");
			
			strHTMLBuilder.Append("<tr class='font_14'>");
			strHTMLBuilder.Append($"<td colspan='13' class='text-center font_weight_bold'> {"รายงาน Feedback and Unread Report"}</td> ");
			strHTMLBuilder.Append("</tr>");

			strHTMLBuilder.Append("<tr class='font_14'>");
			strHTMLBuilder.Append($"<td colspan='13'class='text-center font_weight_bold' > {dataRange}</td> ");
			strHTMLBuilder.Append("</tr>");

			//strHTMLBuilder.Append("<tr class='font_14'>");
			//strHTMLBuilder.Append($"<td colspan='13' class='text-center font_weight_bold'> {locationName}</td> ");
			//strHTMLBuilder.Append("</tr>");

			strHTMLBuilder.Append("<tr class=' font_14'>");
			strHTMLBuilder.Append("</tr>");

			strHTMLBuilder.Append("<tr class=' font_14'>");
			strHTMLBuilder.Append($"<th class=' border_bottom_none text-center font_weight_bold'>{L("MessageHeader")}</th>");
			strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("Type")}</th>");
			strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("CreatedOn")}</th>");
			strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("PlantID")}</th>");
			strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("PlantName")}</th>");
			strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("Unread")}</th>");
			strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("Acknowledgement")}</th>");
			strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("Replies")}</th>");
			strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("Content")}</th>");
			strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("POSNo.(TerminalName)")}</th>");
			strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("EmployeeNo.")}</th>");
			strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("Responsed`sName")}</th>");
			strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("ResponsedOn")}</th>");
			strHTMLBuilder.Append("</tr>");

			// add content
			foreach (var item in dtos)
			{
				strHTMLBuilder.Append("<tr class='font_14'>");
				strHTMLBuilder.Append($"<td> { item.MessageHeader}</td> ");
				strHTMLBuilder.Append($"<td> { item.Type}</td> ");
				strHTMLBuilder.Append($"<td> { item.CreationTime}</td>");
				strHTMLBuilder.Append($"<td> { item.PlantCode}</td> ");
				strHTMLBuilder.Append($"<td> { item.Location}</td> ");
				strHTMLBuilder.Append($"<td class='text-center'> { GetActive(item.Unread)}</td> ");
				strHTMLBuilder.Append($"<td class='text-center'> { GetActive(item.Acknowledgement)}</td> ");
				strHTMLBuilder.Append($"<td class='text-center'> { GetActive(item.Replied)}</td> ");
				strHTMLBuilder.Append($"<td> { item.Content}</td> ");
				strHTMLBuilder.Append($"<td> { item.Terminal}</td> ");
				strHTMLBuilder.Append($"<td> { item.EmployeeCode}</td> ");
				strHTMLBuilder.Append($"<td> { item.EmployeeName}</td> ");
				strHTMLBuilder.Append($"<td> { item.RepliedTime}</td> ");
				strHTMLBuilder.Append("</tr>");
			}
			strHTMLBuilder.Append("</table>");
			strHTMLBuilder.Append("</div>");
			string Htmltext = strHTMLBuilder.ToString();
			return Htmltext;
		}
		private string GetActive(bool input)
        {
			return input ? "X" : "";
        }


		private string GetLoacationName(FeedbackUnreadExportInput input)
		{
			var locationName = "";
			var isAllLocation = (input.LocationGroup?.Locations?.Count() + input.LocationGroup?.Groups?.Count() + input.LocationGroup?.LocationTags?.Count()) == _locRepo.GetAll().Count();

			if (isAllLocation)
			{
				locationName = "";
			}
			else
			{
				if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
				{
					locationName = input.LocationGroup.Locations.Select(l => l.Name).JoinAsString(", ");
				}
				if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
				{
					locationName = input.LocationGroup.Groups.Select(l => l.Name).JoinAsString(", ");
				}
				if (input.LocationGroup?.LocationTags != null && input.LocationGroup.LocationTags.Any())
				{
					locationName = input.LocationGroup.LocationTags.Select(l => l.Name).JoinAsString(", ");
				}
			}

			return locationName;
		}
	}
}