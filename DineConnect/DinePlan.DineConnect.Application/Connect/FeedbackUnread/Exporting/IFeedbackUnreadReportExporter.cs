﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.FeedbackUnread.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.FeedbackUnread.Exporting
{
    public interface IFeedbackUnreadReportExporter
    {
        Task<FileDto> ExportToFile(FeedbackUnreadExportInput input, IFeedbackUnreadAppService appService);
    }
}