﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.FeedbackUnread.Dto;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.FeedbackUnread
{
    public interface IFeedbackUnreadAppService : IApplicationService
    {
        Task<PagedResultOutput<FeedbackUnreadReportOutput>> GetAllReport(FeedbackUnreadReportInput input);
        Task<List<FeedbackUnreadExportOutput>> GetDetailExport(FeedbackUnreadExportInput input);
        Task<FeedbackUnreadDetail> GetDetail(NullableIdInput input);
        Task<FileDto> GetReportToFile(FeedbackUnreadExportInput input);
    }
}
