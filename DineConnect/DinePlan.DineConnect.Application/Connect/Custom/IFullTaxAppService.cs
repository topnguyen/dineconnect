﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Custom.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Custom
{
    public interface IFullTaxAppService : IApplicationService
    {
        Task<IndividualFullTaxOutput> GetInvidualFullTax(IndividualFullTaxInput input);

        Task<PagedResultDto<FullTaxMemberListDto>> GetTaxMemberList(GetFullTaxMemberInput input);

        Task<FileDto> GetTaxMemberListToExcel();

        Task<CreateOrUpdateFullTaxMemberInput> GetFullTaxMemberForEdit(NullableIdInput input);

        Task CreateOrUpdateFullTaxMember(CreateOrUpdateFullTaxMemberInput input);

        Task ValidateForCreateOrUpdateMember(FullTaxMemberEditDto input);

        Task DeleteTaxMember(NullableIdInput input);

        Task CreateTaxAddress(FullTaxAddressEditDto input);

        Task<PagedResultDto<FullTaxInvoiceListDto>> GetTaxInvoiceList(GetFullTaxInvoiceInput input);

        Task<FileDto> GetTaxInvoiceListToExcel();

        Task<CreateOrUpdateFullTaxInvoiceInput> GetFullTaxInvoiceForEdit(NullableIdInput input);

        Task CreateOrUpdateFullTaxInvoice(CreateOrUpdateFullTaxInvoiceInput input);

        Task DeleteTaxInvoice(NullableIdInput input);

        Task CreateOrUpdateFullTaxInvoiceCopyCount(FullTaxInvoiceCopyCountEditDto input);

        Task ActivateItem(IdInput input);
        Task<List<FullTaxMemberListDto>> APIGetAllTaxMembers(ApiGetFullTaxMemberInput input);
        Task DeleteAddress(NullableIdInput input);
        Task<CreateOrUpdateTicketOutput> ApiSyncTaxInvoice(FullTaxInvoiceDto input);
    }
}