﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Connect.Custom.Dto;
using DinePlan.DineConnect.Connect.Custom.Exporting;
using DinePlan.DineConnect.Connect.Invoice;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Session;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Custom.Implementation
{
    public class FullTaxAppService : DineConnectAppServiceBase, IFullTaxAppService
    {
        private readonly IRepository<Transaction.Ticket> _ticketRepo;
        private readonly IRepository<Master.Location> _locRepo;
        private readonly IRepository<Master.Company> _companyRepo;
        private readonly IRepository<FullTaxInvoice> _taxInvoiceRepository;
        private readonly IRepository<FullTaxMember> _taxMemberRepository;
        private readonly IRepository<FullTaxAddress> _taxAddressRepository;
        private readonly IRepository<FullTaxInvoiceCopyCount> _taxInvoiceCopyCountRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IFullTaxExporter _fullTaxExporter;
        private readonly ISyncAppService _syncAppService;

        public FullTaxAppService(IRepository<Transaction.Ticket> ticketRepo,
                                IUnitOfWorkManager unitOfWorkManager,
                                IRepository<Master.Company> companyRepo,
                                IRepository<Master.Location> locRepo,
                                IRepository<FullTaxInvoice> taxInvoiceRepository,
                                IRepository<FullTaxMember> taxMemberRepository,
                                IRepository<FullTaxAddress> taxAddressRepository,
                                IRepository<FullTaxInvoiceCopyCount> taxInvoiceCopyCountRepository,
                                IFullTaxExporter fullTaxExporter,
                                ISyncAppService syncAppService)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _ticketRepo = ticketRepo;
            _locRepo = locRepo;
            _taxInvoiceRepository = taxInvoiceRepository;
            _taxMemberRepository = taxMemberRepository;
            _taxAddressRepository = taxAddressRepository;
            _taxInvoiceCopyCountRepository = taxInvoiceCopyCountRepository;
            _fullTaxExporter = fullTaxExporter;
            _companyRepo = companyRepo;
            _syncAppService = syncAppService;
        }

        public async Task<IndividualFullTaxOutput> GetInvidualFullTax(IndividualFullTaxInput input)
        {
            IndividualFullTaxOutput output = new IndividualFullTaxOutput();
            var locationId = 0;
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                if (input != null && !string.IsNullOrEmpty(input.CheckNo) && !string.IsNullOrEmpty(input.BranchId))
                {
                    var allLocations = _locRepo.GetAll();
                    if (!allLocations.Any())
                    {
                        throw new UserFriendlyException("Branches are not available");
                    }

                    var myLocation = allLocations.FirstOrDefault(a => a.Code.Equals(input.BranchId));

                    if (myLocation == null)
                    {
                        throw new UserFriendlyException("Branch Id is wrong");
                    }
                    locationId = myLocation.Id;
                    var myTicket = _ticketRepo.GetAll().FirstOrDefault(a =>
                        a.QueueNumber.Equals(input.CheckNo) && a.LocationId.Equals(locationId) &&
                        a.LastPaymentTimeTruc.Equals(input.DateToSearch));

                    if (myTicket == null)
                    {
                        myTicket = _ticketRepo.GetAll().FirstOrDefault(a =>
                            a.TicketNumber.Equals(input.CheckNo) && a.LocationId.Equals(locationId) &&
                            a.LastPaymentTimeTruc.Equals(input.DateToSearch));
                        if (myTicket == null)
                        {
                            throw new UserFriendlyException("No Check is Available with the Number");
                        }
                    }

                    int vatVoucherCount = 0;
                    int nonVatVoucherCount = 0;

                    decimal vatVoucherAmount = 0M;
                    decimal nonVatVoucherAmount = 0M;

                    output.BussinessDate = myTicket.LastPaymentTimeTruc.ToShortDateString();
                    output.CheckNo = myTicket.QueueNumber;
                    output.DateTime = DateTime.Now.ToString();
                    output.ExclusiveTax = "100";
                    output.IsCreditTerm = false;
                    output.PCName = myTicket.TerminalName;
                    output.ThaiRD = myTicket.TerminalName;
                    output.RCVNumber = myTicket.TicketNumber;
                    if (myLocation?.Company != null)
                        output.ThaiTaxID = myLocation.Company.GovtApprovalId;
                    else
                    {
                        var company = _companyRepo.Get(myLocation.CompanyRefId);
                        if (company != null)
                        {
                            output.ThaiTaxID = company.GovtApprovalId;
                        }
                    }
                    output.Total = myTicket.TotalAmount.ToString();
                    output.BussinessDate = myTicket.LastPaymentTimeTruc.ToShortDateString();
                    foreach (var ticketOrder in myTicket.Orders)
                    {
                        if (ticketOrder.CalculatePrice && ticketOrder.DecreaseInventory)
                        {
                            var totalAmount = ticketOrder.Quantity * ticketOrder.Price;
                            var taxItem = new FullTaxItem
                            {
                                ID = ticketOrder.MenuItemId.ToString(),
                                Description = ticketOrder.MenuItemName.ToString(),
                                IsVoid = false,
                                Price = ticketOrder.Price.ToString(),
                                Unit = ticketOrder.Quantity.ToString(),
                                PriceSum = Math.Round(totalAmount, 2, MidpointRounding.AwayFromZero).ToString()
                            };

                            if (ticketOrder.Taxes.Any())
                            {
                                vatVoucherCount += 1;
                            }
                            else
                            {
                                nonVatVoucherCount += 1;
                            }

                            output.Item.Add(taxItem);
                        }
                    }

                    foreach (var payment in myTicket.Payments)
                    {
                        var taxItem = new FullTaxTender()
                        {
                            ID = payment.PaymentTypeId.ToString(),
                            Description = payment.PaymentType.Name,
                            IsVoid = false,
                            Price = payment.Amount.ToString(),
                            Unit = "1",
                            PriceSum = payment.Amount.ToString()
                        };
                        output.Tender.Add(taxItem);
                    }

                    if (myTicket.Transactions.Any())
                    {
                        var totalTax = myTicket.Transactions.Where(a => a.TransactionType.Tax);
                        output.Vat = totalTax.Sum(a => a.Amount).ToString();

                        var totalDiscount = myTicket.Transactions.Where(a => a.TransactionType.Discount);
                        output.Discount = totalDiscount.Sum(a => a.Amount).ToString();
                    }

                    output.Tips = "";
                    output.Charge = "";

                    output.VatCount = vatVoucherCount.ToString();
                    output.VatVoucher = nonVatVoucherCount.ToString();

                    output.NoVatCount = vatVoucherCount > 1 ? "100" : "";
                    output.NoVatVoucher = nonVatVoucherCount > 1 ? "100" : "";
                }
                else
                {
                    throw new UserFriendlyException("Input is in Wrong Format");
                }
            }
            output.Code = "1";
            return output;
        }

        #region Tax Member

        public async Task<PagedResultDto<FullTaxMemberListDto>> GetTaxMemberList(GetFullTaxMemberInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                if (!input.Filter.IsNullOrEmpty())
                {
                    input.Filter = Regex.Replace(input.Filter.Trim(), @"\s+", " ");
                }

                var query = _taxMemberRepository.GetAll()
                    .Where(i => i.IsDeleted == input.IsDeleted)
                    .WhereIf(!input.Filter.IsNullOrEmpty(), t => t.Name.Contains(input.Filter))
                    .WhereIf(!input.BranchId.IsNullOrEmpty(), t => t.BranchId.Contains(input.BranchId))
                    .WhereIf(!input.TaxId.IsNullOrEmpty(), t => t.TaxId.Contains(input.TaxId));

                var allMyEnItems = SearchLocation(query, input.LocationGroup).OfType<FullTaxMember>();

                var allItemCount = allMyEnItems.Count();

                var sortMenuItems = allMyEnItems.AsQueryable()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList();

                var allListDtos = sortMenuItems.MapTo<List<FullTaxMemberListDto>>();
                
                return new PagedResultOutput<FullTaxMemberListDto>(
                    allItemCount,
                    allListDtos
                    );
            }
        }

        public async Task<FileDto> GetTaxMemberListToExcel()
        {
            var allList = await _taxMemberRepository.GetAll().OrderBy(a => a.Name).ToListAsync();

            return _fullTaxExporter.ExportMembersToFile(allList.MapTo<List<FullTaxMemberListDto>>());
        }

        public async Task<CreateOrUpdateFullTaxMemberInput> GetFullTaxMemberForEdit(NullableIdInput input)
        {
            var output = new CreateOrUpdateFullTaxMemberInput();

            var editDto = new FullTaxMemberEditDto();

            if (input.Id.HasValue)
            {
                var member = await _taxMemberRepository.GetAsync(input.Id.Value);
                editDto = member.MapTo<FullTaxMemberEditDto>();
                output.Addresses = member.Addresses.ToList().MapTo<List<FullTaxAddressEditDto>>();
            }
            output.TaxMember = editDto;
            UpdateLocationAndNonLocationForEdit(editDto, output.LocationGroup);

            return output;
        }

        public async Task CreateOrUpdateFullTaxMember(CreateOrUpdateFullTaxMemberInput input)
        {
            await ValidateForCreateOrUpdateMember(input.TaxMember);

            if (input.TaxMember.Id.HasValue)
            {
                await UpdateTaxMember(input);
            }
            else
            {
                await CreateTaxMember(input);
            }
        }

        public async Task DeleteTaxMember(NullableIdInput input)
        {
            if (input.Id.HasValue)
            {
                var taxMember = await _taxMemberRepository.GetAsync(input.Id.Value);

                taxMember.Addresses.ToList().ForEach(a => _taxAddressRepository.Delete(a));

                await _taxMemberRepository.DeleteAsync(input.Id.Value);
            }
        }
        public async Task DeleteAddress(NullableIdInput input)
        {
            if (input.Id.HasValue)
            {
                await _taxAddressRepository.DeleteAsync(input.Id.Value);
            }
        }

        private async Task CreateTaxMember(CreateOrUpdateFullTaxMemberInput input)
        {
            var taxMember = input.TaxMember.MapTo<FullTaxMember>();
            if (taxMember.Addresses == null) taxMember.Addresses = new List<FullTaxAddress>();

            UpdateLocationAndNonLocation(taxMember, input.LocationGroup);
            if(input.Addresses != null)
            {
                foreach (var address in input.Addresses)
                {
                    taxMember.Addresses.Add(address.MapTo<FullTaxAddress>());
                }
            }
            var memberid = await _taxMemberRepository.InsertAndGetIdAsync(taxMember);
        }

        private async Task UpdateTaxMember(CreateOrUpdateFullTaxMemberInput input)
        {
            var dto = input.TaxMember;
            var item = await _taxMemberRepository.GetAsync(input.TaxMember.Id.Value);

            dto.MapTo(item);

            UpdateLocationAndNonLocation(item, input.LocationGroup);

            await _taxMemberRepository.UpdateAsync(item);
        }

        public async Task ValidateForCreateOrUpdateMember(FullTaxMemberEditDto input)
        {
            var exists = _taxMemberRepository.GetAll()
                .WhereIf(input.Id.HasValue, m => m.Id != input.Id);

            if (exists.Where(m => m.Name.Equals(input.Name)).Any())
            {
                throw new UserFriendlyException(L("NameAlreadyExists", input.Name));
            }

            if (exists.Where(m => m.TaxId.Equals(input.TaxId)).Any())
            {
                throw new UserFriendlyException(L("TaxAlreadyExists", input.TaxId));
            }

            if (exists.Where(m => m.BranchId.Equals(input.BranchId)).Any())
            {
                throw new UserFriendlyException(L("BranchAlreadyExists", input.BranchId));
            }
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _taxMemberRepository.GetAsync(input.Id);
                item.IsDeleted = false;

                await _taxMemberRepository.UpdateAsync(item);
            }
        }

        public async Task<List<FullTaxMemberListDto>> APIGetAllTaxMembers(ApiGetFullTaxMemberInput input)
        {
            var query = await _taxMemberRepository.GetAll()
                .WhereIf(!input.Name.IsNullOrEmpty(), t => t.Name.ToLower().Contains(input.Name.ToLower()))
                .WhereIf(!input.BranchId.IsNullOrEmpty(), t => t.BranchId.ToLower().Contains(input.BranchId.ToLower()))
                .WhereIf(!input.TaxId.IsNullOrEmpty(), t => t.TaxId.ToLower().Contains(input.TaxId.ToLower()))
                .WhereIf(!input.Phone.IsNullOrEmpty(), t => t.Phone.ToLower().Contains(input.Phone.ToLower()))
                .WhereIf(!input.BranchName.IsNullOrEmpty(), t => t.BranchName.ToLower().Contains(input.BranchName.ToLower()))
                .ToListAsync();

            return query.MapTo<List<FullTaxMemberListDto>>();
        }

        #endregion Tax Member

        #region Tax Address

        public async Task CreateTaxAddress(FullTaxAddressEditDto input)
        {
            ValidateForCreateTaxAddress(input);

            var taxAddress = input.MapTo<FullTaxAddress>();

            await _taxAddressRepository.InsertAndGetIdAsync(taxAddress);
        }

        private void ValidateForCreateTaxAddress(FullTaxAddressEditDto input)
        {
            var exists = _taxAddressRepository.GetAll()
                .Where(m => m.FullMemberId.Equals(input.FullMemberId))
                .Where(m => m.Address1.Equals(input.Address1));

            if (exists.Any())
            {
                throw new UserFriendlyException(L("Address1AlreadyExists", input.Address1));
            }
        }

        #endregion Tax Address

        #region Full Tax Invoices

        public async Task<PagedResultDto<FullTaxInvoiceListDto>> GetTaxInvoiceList(GetFullTaxInvoiceInput input)
        {
            if (!input.Filter.IsNullOrEmpty())
            {
                input.Filter = Regex.Replace(input.Filter.Trim(), @"\s+", " ");
            }

            var query = _taxInvoiceRepository.GetAll()
                .Include(i => i.FullInvoiceNumber)
                .Include(i => i.Location)
                .Include(i => i.Ticket)
                .WhereIf(!input.Filter.IsNullOrEmpty(), t => t.FullInvoiceNumber.Contains(input.Filter));

            var result = await query
              .OrderBy(input.Sorting)
              .PageBy(input)
              .ToListAsync();

            return new PagedResultOutput<FullTaxInvoiceListDto>(
                await query.CountAsync(),
                result.MapTo<List<FullTaxInvoiceListDto>>()
                );
        }

        public async Task<FileDto> GetTaxInvoiceListToExcel()
        {
            var allList = await _taxInvoiceRepository.GetAll().OrderBy(a => a.FullInvoiceNumber).ToListAsync();

            return _fullTaxExporter.ExportInvoicesToFile(allList.MapTo<List<FullTaxInvoiceListDto>>());
        }

        public async Task<CreateOrUpdateFullTaxInvoiceInput> GetFullTaxInvoiceForEdit(NullableIdInput input)
        {
            var output = new CreateOrUpdateFullTaxInvoiceInput();

            var editDto = new FullTaxInvoiceEditDto();

            if (input.Id.HasValue)
            {
                var invoice = await _taxInvoiceRepository.GetAsync(input.Id.Value);
                editDto = invoice.MapTo<FullTaxInvoiceEditDto>();
            }
            output.TaxInvoice = editDto;
            UpdateLocationAndNonLocationForEdit(editDto, output.LocationGroup);

            return output;
        }

        public async Task CreateOrUpdateFullTaxInvoice(CreateOrUpdateFullTaxInvoiceInput input)
        {
            ValidateForCreateOrUpdateInvoice(input.TaxInvoice);

            if (input.TaxInvoice.Id.HasValue)
            {
                await UpdateTaxInvoice(input);
            }
            else
            {
                await CreateTaxInvoice(input);
            }
        }
        public async Task<CreateOrUpdateTicketOutput> ApiSyncTaxInvoice(FullTaxInvoiceDto input)
        {
            try
            {
                if (input.SyncId > 0)
                {
                    var copys = _taxInvoiceCopyCountRepository.GetAll().Where(x => x.FullTaxInvoiceId == input.SyncId).Select(x => x.Id).ToList();
                    foreach (var id in copys)
                    {
                        await _taxInvoiceCopyCountRepository.DeleteAsync(id);
                    }
                    foreach (var cc in input.CopyCounts)
                    {
                        var newCC = cc.MapTo<FullTaxInvoiceCopyCount>();
                        newCC.FullTaxInvoiceId = input.SyncId;
                        _taxInvoiceCopyCountRepository.InsertAsync(newCC);
                    }

                    //Member
                    int memberId = 0;
                    if (input.FullTaxMember != null)
                    {
                        var mem = input.FullTaxMember;
                        var mb = _taxMemberRepository.GetAll().FirstOrDefault(x => x.Name == mem.Name && x.TaxId == mem.TaxId && x.BranchId == mem.BranchId);
                        if (mb == null)
                        {
                            var newMb = mem.MapTo<FullTaxMember>();
                            memberId = await _taxMemberRepository.InsertAndGetIdAsync(newMb);
                        }
                        else
                        {
                            var address = _taxAddressRepository.GetAll().Where(x => x.FullMemberId == mb.Id).Select(x => x.Id).ToList();
                            foreach (var id in address)
                            {
                                await _taxAddressRepository.DeleteAsync(id);
                            }

                            foreach (var adr in mem.Addresses)
                            {
                                var newAdr = adr.MapTo<FullTaxAddress>();
                                newAdr.FullMemberId = mb.Id;
                                await _taxAddressRepository.InsertAsync(newAdr);
                            }

                            mb.BranchId = mem.BranchId;
                            mb.BranchName = mem.BranchName;
                            mb.Name = mem.Name;
                            mb.Phone = mem.Phone;
                            mb.TaxId = mem.TaxId;
                            await _taxMemberRepository.UpdateAsync(mb);
                            memberId = mb.Id;
                        }
                    }

                    var invoice = await _taxInvoiceRepository.GetAsync(input.SyncId);
                    invoice.CopyCount = input.CopyCount;
                    invoice.FullInvoiceNumber = input.FullInvoiceNumber;
                    invoice.PreviousInvoiceNumber = input.PreviousInvoiceNumber;
                    invoice.PrintTime = input.PrintTime;
                    invoice.Reason = input.Reason;
                    invoice.Status = input.Status;
                    await _taxInvoiceRepository.UpdateAsync(invoice);

                    await _syncAppService.UpdatePushTime(new ApiLocationInput
                    {
                        LocationId = input.LocationId,
                        TenantId = input.TenantId
                    });

                    return new CreateOrUpdateTicketOutput
                    {
                        Ticket = invoice.Id
                    };
                }
                else
                {
                    var invoice = input.MapTo<FullTaxInvoice>();

                    var mb = _taxMemberRepository.GetAll().FirstOrDefault(x => x.Name == invoice.FullTaxMember.Name
                                                                               && x.TaxId == invoice.FullTaxMember.TaxId
                                                                               && x.BranchId == invoice.FullTaxMember.BranchId);
                    if (mb != null)
                    {
                        invoice.FullMemberId = mb.Id;
                        invoice.FullTaxMember = null;
                    }
                    var id = await _taxInvoiceRepository.InsertAndGetIdAsync(invoice);

                    await _syncAppService.UpdatePushTime(new ApiLocationInput
                    {
                        LocationId = input.LocationId,
                        TenantId = input.TenantId
                    });

                    return new CreateOrUpdateTicketOutput
                    {
                        Ticket = id
                    };
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task DeleteTaxInvoice(NullableIdInput input)
        {
            if (input.Id.HasValue)
            {
                await _taxInvoiceRepository.DeleteAsync(input.Id.Value);
            }
        }

        private async Task CreateTaxInvoice(CreateOrUpdateFullTaxInvoiceInput input)
        {
            var taxInvoice = input.TaxInvoice.MapTo<FullTaxInvoice>();

            UpdateLocationAndNonLocation(taxInvoice, input.LocationGroup);

            var invoiceid = await _taxInvoiceRepository.InsertAndGetIdAsync(taxInvoice);
        }

        private async Task UpdateTaxInvoice(CreateOrUpdateFullTaxInvoiceInput input)
        {
            var dto = input.TaxInvoice;
            var item = await _taxInvoiceRepository.GetAsync(input.TaxInvoice.Id.Value);

            dto.MapTo(item);

            UpdateLocationAndNonLocation(item, input.LocationGroup);

            await _taxInvoiceRepository.UpdateAsync(item);
        }

        private void ValidateForCreateOrUpdateInvoice(FullTaxInvoiceEditDto input)
        {
            var exists = _taxInvoiceRepository.GetAll()
                .Where(m => m.FullInvoiceNumber.Equals(input.FullInvoiceNumber))
                .WhereIf(input.Id.HasValue, m => m.Id != input.Id);

            if (exists.Any())
            {
                throw new UserFriendlyException(L("NumberAlreadyExists", input.FullInvoiceNumber));
            }
        }

        #endregion Full Tax Invoices

        #region Copy Count

        public async Task CreateOrUpdateFullTaxInvoiceCopyCount(FullTaxInvoiceCopyCountEditDto input)
        {
            if (input.Id.HasValue)
            {
                await UpdateCopyCount(input);
            }
            else
            {
                await CreateCopyCount(input);
            }
        }

        private async Task CreateCopyCount(FullTaxInvoiceCopyCountEditDto input)
        {
            var copyCount = input.MapTo<FullTaxInvoiceCopyCount>();

            await _taxInvoiceCopyCountRepository.InsertOrUpdateAsync(copyCount);
        }

        private Task UpdateCopyCount(FullTaxInvoiceCopyCountEditDto input)
        {
            throw new NotImplementedException();
        }

        #endregion Copy Count
    }
}