﻿using System;
using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Invoice;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Custom.Dto
{
    public class IndividualFullTaxInput : IInputDto
    {
        public string CheckNo { get; set; }
        public DateTime DateToSearch { get; set; }
        public string BranchId { get; set; }
        public string BrandId { get; set; }
    }


    public class FullTaxItem
    {
        public string Description { get; set; }
        public string ID { get; set; }
        public bool IsVoid { get; set; }
        public string Price { get; set; }
        public string PriceSum { get; set; }
        public string Unit { get; set; }
    }

    public class FullTaxTender
    {
        public string Description { get; set; }
        public string ID { get; set; }
        public bool IsVoid { get; set; }
        public string Price { get; set; }
        public string PriceSum { get; set; }
        public string Unit { get; set; }
    }

    public class IndividualFullTaxOutput : IOutputDto
    {

        public IndividualFullTaxOutput()
        {
            Tender = new List<FullTaxTender>();
            Item = new List<FullTaxItem>();
            Code = "0";
        }

        public string BussinessDate { get; set; }
        public string Charge { get; set; }
        public string CheckNo { get; set; }
        public string Code { get; set; }
        public string DateTime { get; set; }
        public string Description { get; set; }
        public string Discount { get; set; }
        public string ExclusiveTax { get; set; }
        public bool IsCreditTerm { get; set; }
        public List<FullTaxItem> Item { get; set; }
        public string NoVatCount { get; set; }
        public string NoVatVoucher { get; set; }
        public string PCName { get; set; }
        public string RCVNumber { get; set; }
        public List<FullTaxTender> Tender { get; set; }
        public string ThaiRD { get; set; }
        public string ThaiTaxID { get; set; }
        public string Tips { get; set; }
        public string Total { get; set; }
        public string Vat { get; set; }
        public string VatCount { get; set; }
        public string VatVoucher { get; set; }
    }

    public class GetSettingsInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public string Location { get; set; }
        public int LocationId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }
}