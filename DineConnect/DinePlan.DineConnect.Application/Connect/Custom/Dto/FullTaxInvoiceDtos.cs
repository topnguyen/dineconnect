﻿using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Invoice;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Custom.Dto
{
    [AutoMapFrom(typeof(FullTaxInvoice))]
    public class FullTaxInvoiceListDto : FullAuditedEntity<int?>
    {
        public virtual string FullMemberName { get; set; }
        public virtual string LocationNamePhone { get; set; }
        public virtual FullTaxMember FullTaxMember { get; set; }
        public virtual int FullMemberId { get; set; }
        public virtual Master.Location Location { get; set; }
        public virtual int LocationId { get; set; }
        public virtual Transaction.Ticket Ticket { get; set; }
        public string FullInvoiceNumber { get; set; }
        public string Status { get; set; }
        public int CopyCount { get; set; }
        public string User { get; set; }
        public DateTime PrintTime { get; set; }
        public string PreviousInvoiceNumber { get; set; }
    }

    public class GetFullTaxInvoiceInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    [AutoMapTo(typeof(FullTaxInvoice))]
    public class FullTaxInvoiceEditDto : ConnectEditDto
    {
        public int? Id { get; set; }
        public virtual int? FullMemberId { get; set; }
        public virtual int? LocationId { get; set; }
        public string FullInvoiceNumber { get; set; }
        public string Status { get; set; }
        public int CopyCount { get; set; }
        public string User { get; set; }
        public DateTime PrintTime { get; set; }
        public string PreviousInvoiceNumber { get; set; }
    }

    public class CreateOrUpdateFullTaxInvoiceInput
    {
        public CreateOrUpdateFullTaxInvoiceInput()
        {
            TaxInvoice = new FullTaxInvoiceEditDto();
            LocationGroup = new LocationGroupDto();
        }

        public FullTaxInvoiceEditDto TaxInvoice { get; set; }

        public LocationGroupDto LocationGroup { get; set; }
    }

    [AutoMapTo(typeof(FullTaxInvoiceCopyCount))]
    public class FullTaxInvoiceCopyCountEditDto
    {
        public int? Id { get; set; }
        public virtual int FullTaxInvoiceId { get; set; }
    }

    [AutoMapTo(typeof(FullTaxInvoice))]
    public class FullTaxInvoiceDto
    {
        public FullTaxInvoiceDto()
        {
            CopyCounts = new List<TaxInvoiceCopyCountDto>();
        }
        public int SyncId { get; set; }
        public string FullMemberName { get; set; }
        public FullTaxMemberDto FullTaxMember { get; set; }
        public string FullInvoiceNumber { get; set; }
        public string Status { get; set; }
        public int CopyCount { get; set; }
        public string User { get; set; }
        public DateTime PrintTime { get; set; }
        public string PreviousInvoiceNumber { get; set; }
        public string Reason { get; set; }
        public int LocationId { get; set; }
        public int TenantId { get; set; }
        public int TicketId { get; set; }
        public IList<TaxInvoiceCopyCountDto> CopyCounts { get; set; }
    }

    [AutoMapTo(typeof(FullTaxMember))]
    public class FullTaxMemberDto
    {
        public FullTaxMemberDto()
        {
            Addresses = new List<FullTaxAddressDto>();
        }
        public string TaxId { get; set; }

        public string BranchId { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }
        public string BranchName { get; set; }
        public IList<FullTaxAddressDto> Addresses { get; set; }
    }


    [AutoMapTo(typeof(FullTaxAddress))]
    public class FullTaxAddressDto
    {
        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }
        public string PostCode { get; set; }
    }

    [AutoMapTo(typeof(FullTaxInvoiceCopyCount))]
    public class TaxInvoiceCopyCountDto
    {
        public DateTime CopyPrintTime { get; set; }
        public string CopyPrintUser { get; set; }
        public string Reason { get; set; }
        public bool IsCopy { get; set; }
        public string Name { get; set; }
    }
}