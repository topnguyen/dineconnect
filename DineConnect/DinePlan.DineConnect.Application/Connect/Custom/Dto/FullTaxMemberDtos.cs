﻿using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Invoice;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Custom.Dto
{
    [AutoMapFrom(typeof(FullTaxMember))]
    public class FullTaxMemberListDto : FullAuditedEntity<int?>
    {
        public virtual string TaxId { get; set; }

        public virtual string BranchId { get; set; }

        public virtual string Name { get; set; }

        public virtual string Phone { get; set; }
        public string BranchName { get; set; }
        public IList<FullTaxAddressEditDto> Addresses { get; set; }
    }

    public class GetFullTaxMemberInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string TaxId { get; set; }

        public string BranchId { get; set; }

        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
        public GetFullTaxMemberInput()
        {
            LocationGroup = new LocationGroupDto();
        }

        public LocationGroupDto LocationGroup { get; set; }
    }

    [AutoMapTo(typeof(FullTaxMember))]
    public class FullTaxMemberEditDto : ConnectEditDto
    {
        public int? Id { get; set; }

        public virtual string TaxId { get; set; }

        public virtual string BranchId { get; set; }

        public virtual string Name { get; set; }

        public virtual string Phone { get; set; }
        public virtual string BranchName { get; set; }
    }

    public class CreateOrUpdateFullTaxMemberInput
    {
        public CreateOrUpdateFullTaxMemberInput()
        {
            TaxMember = new FullTaxMemberEditDto();
            LocationGroup = new LocationGroupDto();
            Addresses = new List<FullTaxAddressEditDto>();
        }

        public FullTaxMemberEditDto TaxMember { get; set; }

        public LocationGroupDto LocationGroup { get; set; }

        public List<FullTaxAddressEditDto> Addresses { get; set; }
    }
    public class ApiGetFullTaxMemberInput
    {
        public string TaxId { get; set; }

        public string BranchId { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }
        public string BranchName { get; set; }
    }
}