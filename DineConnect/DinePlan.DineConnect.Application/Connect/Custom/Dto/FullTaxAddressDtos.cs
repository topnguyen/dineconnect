﻿using Abp.AutoMapper;
using DinePlan.DineConnect.Connect.Invoice;

namespace DinePlan.DineConnect.Connect.Custom.Dto
{
    [AutoMapTo(typeof(FullTaxAddress))]
    public class FullTaxAddressEditDto
    {
        public int? Id { get; set; }

        public virtual int FullMemberId { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }
        public string PostCode { get; set; }
    }
}