﻿using DinePlan.DineConnect.Connect.Custom.Dto;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Custom.Exporting
{
    public interface IFullTaxExporter
    {
        FileDto ExportMembersToFile(List<FullTaxMemberListDto> dtos);

        FileDto ExportInvoicesToFile(List<FullTaxInvoiceListDto> dtos);
    }
}