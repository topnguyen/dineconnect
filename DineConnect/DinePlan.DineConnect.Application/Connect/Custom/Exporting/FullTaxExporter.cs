﻿using DinePlan.DineConnect.Connect.Custom.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Custom.Exporting
{
    public class FullTaxExporter : FileExporterBase, IFullTaxExporter
    {
        public FileDto ExportMembersToFile(List<FullTaxMemberListDto> dtos)
        {
            return CreateExcelPackage(
                "FullTaxMemberList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("FullTaxMemberList"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("TaxId"),
                        L("BranchId"),
                        L("Name"),
                        L("Phone")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.TaxId,
                        _ => _.BranchId,
                        _ => _.Name,
                        _ => _.Phone

                        );

                    //var creationTimeColumn = sheet.Column(5);
                    //creationTimeColumn.Style.Numberformat.Format = "yyyy-mm-dd hh:mm";

                    for (var i = 1; i <= 5; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }

        public FileDto ExportInvoicesToFile(List<FullTaxInvoiceListDto> dtos)
        {
            return CreateExcelPackage(
                "FullTaxInvoiceList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("FullTaxInvoiceList"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("FullInvoiceNumber"),
                        L("MemberName"),
                        L("Location"),
                        L("TicketNumber"),
                        L("Status"),
                        L("CopyCount"),
                        L("User"),
                        L("PrintTime"),
                        L("PreviousInvoiceNumber")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.FullInvoiceNumber,
                        _ => _.FullTaxMember?.Name,
                        _ => _.Location?.Name,
                        _ => _.Ticket?.TicketNumber,
                        _ => _.Status,
                        _ => _.CopyCount,
                        _ => _.User,
                        _ => _.PrintTime,
                        _ => _.PreviousInvoiceNumber

                        );

                    var printTimeColumn = sheet.Column(9);
                    printTimeColumn.Style.Numberformat.Format = "yyyy-mm-dd hh:mm";

                    for (var i = 1; i <= 10; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}