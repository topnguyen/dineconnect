﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Connect.LanguageDescriptions.Dtos;
using DinePlan.DineConnect.Connect.MultiLingual;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace DinePlan.DineConnect.Connect.LanguageDescriptions.Implementation
{

    public class LanguageDescriptionAppService : DineConnectAppServiceBase, ILanguageDescriptionAppService
    {
        private readonly IRepository<LanguageDescription> _languageDescriptionRepo;
        private readonly ILanguageDescriptionManager _languageDescriptionManager;

        public LanguageDescriptionAppService(
            IRepository<LanguageDescription> languageDescriptionRepo,
            ILanguageDescriptionManager languageDescriptionManager)
        {
            _languageDescriptionRepo = languageDescriptionRepo;
            _languageDescriptionManager = languageDescriptionManager;
        }

        public async Task<PagedResultOutput<LanguageDescriptionListDto>> GetAll(GetLanguageDescriptionInput input)
        {
            var allItems = _languageDescriptionRepo.GetAll().Where(t => t.ReferenceId == input.Id && t.LanguageDescriptionType == input.LanguageDescriptionType).WhereIf(
                                !input.Filter.IsNullOrEmpty(),
                                p => p.Name.Contains(input.Filter));

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<LanguageDescriptionListDto>>();
            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<LanguageDescriptionListDto>(
                allItemCount,
                allListDtos
                );
        }



        public async Task<GetLanguageDescriptionForEditOutput> GetLanguageDescriptionForEdit(NullableIdInput input)
        {
            LanguageDescriptionEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _languageDescriptionRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<LanguageDescriptionEditDto>();
            }
            else
            {
                editDto = new LanguageDescriptionEditDto();
            }

            return new GetLanguageDescriptionForEditOutput
            {
                LanguageDescription = editDto
            };
        }

        public async Task<IdInput> CreateOrUpdateLanguageDescription(CreateOrUpdateLanguageDescriptionInput input)
        {
            input.LanguageDescription.Name = input.LanguageDescription.Name.Trim().ToUpper();
            bool updateDesc = input.LanguageDescription.Id.HasValue;

            var myDesc =
            await _languageDescriptionRepo.FirstOrDefaultAsync(t => t.LanguageCode.Equals(input.LanguageDescription.LanguageCode) && t.ReferenceId == input.LanguageDescription.ReferenceId && t.LanguageDescriptionType == input.LanguageDescription.LanguageDescriptionType);
            if (myDesc != null)
            {
                input.LanguageDescription.Id = myDesc.Id;
                updateDesc = true;
            }

            if (updateDesc)
                return await UpdateLanguageDescription(input);
            return await CreateLanguageDescription(input);

        }

        public async Task DeleteLanguageDescription(IdInput input)
        {
            await _languageDescriptionRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task<IdInput> UpdateLanguageDescription(CreateOrUpdateLanguageDescriptionInput input)
        {
            var item = await _languageDescriptionRepo.GetAsync(input.LanguageDescription.Id.Value);
            var dto = input.LanguageDescription;
            item.LanguageCode = dto.LanguageCode;
            item.LanguageDescriptionType = dto.LanguageDescriptionType;
            item.Name = dto.Name;
            item.Description = dto.Description;
            item.ReferenceId = dto.ReferenceId;
            return new IdInput { Id = item.Id };
        }

        protected virtual async Task<IdInput> CreateLanguageDescription(CreateOrUpdateLanguageDescriptionInput input)
        {
            var dto = input.LanguageDescription.MapTo<LanguageDescription>();

            CheckErrors(await _languageDescriptionManager.CreateOrUpdateSync(dto));
            return new IdInput { Id = dto.Id };
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetLanguageDescriptionTypes()
        {
            var returnList = new List<ComboboxItemDto>();

            var i = 1;
            foreach (var name in Enum.GetNames(typeof(LanguageDescriptionType)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);

        }
    }
}
