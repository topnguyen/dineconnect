﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.LanguageDescriptions.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.LanguageDescriptions.Implementation
{
    public interface ILanguageDescriptionAppService : IApplicationService
    {
        Task<PagedResultOutput<LanguageDescriptionListDto>> GetAll(GetLanguageDescriptionInput inputDto);
        Task<GetLanguageDescriptionForEditOutput> GetLanguageDescriptionForEdit(NullableIdInput nullableIdInput);
        Task<IdInput> CreateOrUpdateLanguageDescription(CreateOrUpdateLanguageDescriptionInput input);
        Task DeleteLanguageDescription(IdInput input);

        Task<ListResultDto<ComboboxItemDto>> GetLanguageDescriptionTypes();
       
    }
}
