﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.MultiLingual;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.LanguageDescriptions.Dtos
{
    [AutoMapFrom(typeof(LanguageDescription))]
    public class LanguageDescriptionListDto : FullAuditedEntityDto
    {
        public virtual string LanguageCode { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public LanguageDescriptionType LanguageDescriptionType { get; set; }
        public int ReferenceId { get; set; }
    }
    [AutoMapTo(typeof(LanguageDescription))]
    public class LanguageDescriptionEditDto
    {
        public int? Id { get; set; }
        public virtual string LanguageCode { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public LanguageDescriptionType LanguageDescriptionType { get; set; }
        public int ReferenceId { get; set; }
    }

    public class GetLanguageDescriptionInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public int Id { get; set; }
        public string Operation { get; set; }
        public LanguageDescriptionType LanguageDescriptionType { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id Desc";
            }
        }
    }
    public class GetLanguageDescriptionForEditOutput : IOutputDto
    {
        public LanguageDescriptionEditDto LanguageDescription { get; set; }
    }
    public class CreateOrUpdateLanguageDescriptionInput : IInputDto
    {
        [Required]
        public LanguageDescriptionEditDto LanguageDescription { get; set; }
    }
}
