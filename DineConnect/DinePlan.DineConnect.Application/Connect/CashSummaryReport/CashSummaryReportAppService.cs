﻿using Abp.Application.Services.Dto;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Auditing.Exporting;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Audit;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Connect.Tag;
using DinePlan.DineConnect.Connect.Ticket.CategoryReport.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Connect.WorkPeriod.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.Handler.Mapping;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.House.Transaction.Implementation;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Report;
using DinePlan.Infrastructure.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System;
using Abp.Configuration;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Customize.Dto;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using DinePlan.DineConnect.Connect.Custom.Exporting;
using DinePlan.DineConnect.Connect.SpeedOfServiceReport.Dto;
using DinePlan.DineConnect.Connect.SpeedOfServiceReport.Exporting;
using DinePlan.DineConnect.Connect.CashSummaryReport.Dto;
using DinePlan.DineConnect.Connect.CashSummaryReport.Exporting;
using DinePlan.DineConnect.Connect.Ticket;

namespace DinePlan.DineConnect.Connect.CashSummaryReport
{
	public class CashSummaryReportAppService : DineConnectAppServiceBase, ICashSummaryReportAppService
	{
		private readonly IRepository<Company> _companyRe;
		private readonly IRepository<Master.Department> _dRepo;
		private readonly ILocationAppService _locService;
		private readonly IRepository<Master.Location> _lRepo;
		private readonly IRepository<Transaction.Ticket> _ticketManager;
		private readonly IRepository<Transaction.Order> _orderManager;
		private readonly IUnitOfWorkManager _unitOfWorkManager;
		private ICustomizeExporter _customizeExporter;
		private readonly IReportBackgroundAppService _rbas;
		private readonly IBackgroundJobManager _bgm;
		private readonly ICashSummaryReportExporter _exporter;
		private readonly IRepository<Period.WorkPeriod> _workPeriod;
		private readonly IRepository<PaymentType> _payRe;
		private readonly IConnectReportAppService _connectReportAppService;
		public CashSummaryReportAppService(
			IRepository<Transaction.Ticket> ticketManager,
			ILocationAppService locService,
			IRepository<Company> comR,
			IUnitOfWorkManager unitOfWorkManager,
			IRepository<Master.Department> drepo,
			IRepository<Master.Location> lRepo,
			IRepository<Transaction.Order> orderManager,
			ICustomizeExporter customizeExporter,
			 IReportBackgroundAppService rbas,
			 IBackgroundJobManager bgm,
			 ICashSummaryReportExporter exporter,
			 IRepository<Period.WorkPeriod> workPeriod,
			 IRepository<PaymentType> payRe,
			  IConnectReportAppService connectReportAppService
			)
		{
			_ticketManager = ticketManager;
			_companyRe = comR;
			_unitOfWorkManager = unitOfWorkManager;
			_dRepo = drepo;
			_locService = locService;
			_lRepo = lRepo;
			_orderManager = orderManager;
			_customizeExporter = customizeExporter;
			_rbas = rbas;
			_bgm = bgm;
			_exporter = exporter;
			_workPeriod = workPeriod;
			_payRe = payRe;
			_connectReportAppService = connectReportAppService;
		}



		#region CashSummaryReport

		public async Task<GetCashSummaryReportOutput> GetCashSummaryReport(GetCashSummaryReportInput input)
		{
			var formatDateSetting = await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.SimpleDateFormat);
			var result = new GetCashSummaryReportOutput();
			var listTickets = new List<GetCashSummaryReportByItem>();

			var fromDate = input.StartDate.Date;
			var endDate = input.EndDate.Date.AddDays(1).AddMilliseconds(-1);

			var cash = await _payRe.FirstOrDefaultAsync(x => x.Name.ToUpper() == ConnectConsts.ConnectConsts.Cash.ToUpper());
			var wht = _payRe.GetAll().Where(x => x.ProcessorName.ToUpper().Contains(ConnectConsts.ConnectConsts.WithHoldTax.ToUpper()));
			var _cashPaymentTypeId = cash?.Id ?? 0;
			var _whtPaymentTypeId = wht.Select(a => a.Id).ToList();

			while (fromDate < endDate)
			{
				var lstWP = await GetWorkTimessOnWorkPeriod(fromDate);
				foreach (var wp in lstWP)
				{
					if (wp.StartDate.Date == fromDate.Date)
					{
						var listGroupsOfPayments = await GetWorkTimeTickets(wp, input);
						if (listGroupsOfPayments == null)
							continue;

						foreach (var myTicket in listGroupsOfPayments)
						{

							foreach (var grpPayment in myTicket.Payments)
							{
								if (_whtPaymentTypeId.Any() && _whtPaymentTypeId.Contains(grpPayment.PaymentTypeId))
								{
									var allCashPayments = myTicket.Payments
										.Where(a => a.PaymentTypeId == _cashPaymentTypeId).Sum(a => a.Amount);

									if (allCashPayments > 0M)
									{
										string taxId = GetTaxId(myTicket.TicketTags);
										listTickets.Add(new GetCashSummaryReportByItem

										{
											Detail = L(ConnectConsts.ConnectConsts.SalesWithHoldingTax),
											TaxId = taxId,
											Amount = allCashPayments,
											WithTWH = true,
											AddOn = myTicket.Location.AddOn,
											PlantCode = myTicket.Location.Code,
											PlantName = myTicket.Location.Name,
											Date = fromDate,
											StartUser = wp.StartUser,
											StopUser = wp.StopUser,
											StartShift = wp.StartDate.ToString("HH:mm"),
											EndShift = wp.EndDate.ToString("HH:mm"),
											LocationID = myTicket.LocationId,
										});
										break;
									}

								}
								else
								{
									if (grpPayment.PaymentTypeId == _cashPaymentTypeId)
									{
										var ticket = listTickets.FirstOrDefault(l => !l.WithTWH && l.Date == fromDate && l.LocationID == wp.LocationId);
										if (ticket == null)
										{
											ticket = new GetCashSummaryReportByItem
											{
												Detail = L(ConnectConsts.ConnectConsts.CashSales),
												TaxId = null,
												WithTWH = false,
												AddOn = myTicket.Location.AddOn,
												PlantCode = myTicket.Location.Code,
												PlantName = myTicket.Location.Name,
												Date = fromDate,
												StartUser = wp.StartUser,
												StopUser = wp.StopUser,
												StartShift = wp.StartDate.ToString("HH:mm"),
												EndShift = wp.EndDate.ToString("HH:mm"),
												LocationID = wp.LocationId,
											};
											listTickets.Add(ticket);
										}
										ticket.Amount += grpPayment.Amount;
									}
								}
							}

						}
					}
				}
				fromDate = fromDate.AddDays(1);
			}



			var mapdataAsQueryable = listTickets.AsQueryable();
			if (!string.IsNullOrEmpty(input.DynamicFilter))
			{
				var jsonSerializerSettings = new JsonSerializerSettings
				{
					ContractResolver =
						new CamelCasePropertyNamesContractResolver()
				};
				var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
				if (filRule?.Rules != null)
				{
					mapdataAsQueryable = mapdataAsQueryable.BuildQuery(filRule);
				}
			}

			var getCashSummaryBy_Plant_Date_Shift = mapdataAsQueryable.OrderBy(x=>x.Date).GroupBy(x => new
			{
				x.PlantCode,
				x.PlantName,
				x.SoldToID,
				x.Date,
				x.StartShift,
				x.EndShift,
				x.StartUser
			}).Select(x => new GetCashSummaryReportByShift
			{
				PlantCode = x.Key.PlantCode,
				PlantName = x.Key.PlantName,
				Date = x.Key.Date,
				EndShift = x.Key.EndShift,
				StartShift = x.Key.StartShift,
				StartUser = x.Key.StartUser,
				SoldToID = x.Key.SoldToID,
				ListItem = x.ToList()
			}).OrderBy(x => x.Date).ToList();
			var getCashSummaryBy_Plant_Date = getCashSummaryBy_Plant_Date_Shift.GroupBy(x => new
			{
				x.PlantCode,
				x.PlantName,
				x.Date,
				x.SoldToID,
				x.StartUser
			}).Select(x => new GetCashSummaryReportByDate
			{
				PlantCode = x.Key.PlantCode,
				PlantName = x.Key.PlantName,
				Date = x.Key.Date,
				SoldToID = x.Key.SoldToID,
				ListItem = x.ToList()
			}).OrderBy(x => x.Date).ToList();

			var getCashSummaryBy_Plant = getCashSummaryBy_Plant_Date.GroupBy(x => new
			{
				x.PlantCode,
				x.PlantName,
				x.SoldToID
			}).Select(x => new GetCashSummaryReportByPlant
			{
				PlantCode = x.Key.PlantCode,
				PlantName = x.Key.PlantName,
				SoldToID = x.Key.SoldToID,

				ListItem = x.ToList()
			}).ToList();
			result.ListItem = getCashSummaryBy_Plant;
			return result;
		}

		private string GetTaxId(string ticketTags)
		{
			var result = "";
			if (!string.IsNullOrEmpty(ticketTags))
			{
				var listTag = JsonConvert.DeserializeObject<List<TicketTagValue>>(ticketTags);
				var tax = listTag.Find(x => x.TagName == ConnectConsts.ConnectConsts.TaxIds);
				if(tax != null)
				{
					result = tax.TagValue;
				}
			}
			return result;
		}
		private async Task<List<Transaction.Ticket>> GetWorkTimeTickets(WorkShiftByLocationDto wt, GetCashSummaryReportInput input)
		{
			using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
			{
				var defWorkTime = wt;
				if (defWorkTime == null)
					return null;
				if (wt.StartDate.Equals(wt.EndDate))
				{
					var ticketByLocation = GetTicketForConditionLocation(input);
					var tickets = await ticketByLocation.Where(x => x.LastPaymentTime == defWorkTime.StartDate && !x.PreOrder && !x.Credit && x.LocationId == wt.LocationId).ToListAsync();
					return tickets;
				}
				else
				{
					var ticketByLocation = GetTicketForConditionLocation(input);
					var tickets = await ticketByLocation.Where(x => x.LastPaymentTime >= defWorkTime.StartDate && x.LastPaymentTime <= defWorkTime.EndDate && !x.PreOrder && !x.Credit && x.LocationId == wt.LocationId).ToListAsync();
					return tickets;
				}
			}
		}
		private async Task<List<WorkShiftByLocationDto>> GetWorkTimessOnWorkPeriod(DateTime startDate)
		{
			var lstShiftDto = new List<WorkShiftByLocationDto>();

			var lstwt = await _workPeriod.GetAll().Where(x => x.StartTime.Year == startDate.Year &&
								x.StartTime.Month == startDate.Month &&
								x.StartTime.Day == startDate.Day).ToListAsync();

			foreach (var wt in lstwt)
			{
				if (wt != null && !string.IsNullOrEmpty(wt.WorkPeriodInformations))
				{
			 	var	wf = JsonConvert.DeserializeObject<List<WorkShiftByLocationDto>>(wt.WorkPeriodInformations);
				
				// TODO: add locationID	
			    foreach (var item in wf)
				{
					item.LocationId = wt.LocationId;
				}
					lstShiftDto.AddRange(wf);
				}
			}

		
		 return lstShiftDto;
		}
		public async Task<FileDto> GetCashSummaryReportToExport(GetCashSummaryReportInput input)
		{
			if (AbpSession.UserId != null && AbpSession.TenantId != null)
			{
				input.MaxResultCount = 10000;

				if (input.RunInBackground)
				{
					var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
					{
						ReportName = ReportNames.CASHSUMMARYREPORT,
						Completed = false,
						UserId = AbpSession.UserId.Value,
						TenantId = AbpSession.TenantId.Value,
						ReportDescription = input.ReportDescription
					});

					if (backGroundId > 0)
					{
						var tInput = new CashSummaryReportInput
						{
							GetCashSummaryReportInput = input,
						};
						await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
						{
							BackGroundId = backGroundId,
							ReportName = ReportNames.CASHSUMMARYREPORT,
							CashSummaryReportInput = tInput,
							UserId = AbpSession.UserId.Value,
							TenantId = AbpSession.TenantId.Value
						});
					}
				}
				else
				{
					var output = await _exporter.ExportCashSummaryReport(input, this);
					return output;
				}
			}

			return null;



		}

		private IQueryable<Transaction.Ticket> GetTicketForConditionLocation(GetCashSummaryReportInput input)
		{
			IQueryable<Transaction.Ticket> output = _ticketManager.GetAll();

			if (input.Location > 0)
			{
				output = output.Where(a => a.LocationId == input.Location);
			}
			else if (input.Locations != null && input.Locations.Any())
			{
				var locations = input.Locations.Select(a => a.Id).ToList();
				output = output.Where(a => locations.Contains(a.LocationId));
			}
			else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
					 && !input.LocationGroup.Locations.Any())
			{
				var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
				{
					Locations = input.LocationGroup.Locations,
					Group = false
				});
				if (locations.Any()) output = output.Where(a => locations.Contains(a.LocationId));
			}
			else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
			{
				var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
				{
					Locations = input.LocationGroup.Locations,
					Group = false
				});
				output = output.Where(a => locations.Contains(a.LocationId));
			}
			else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
			{
				var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
				{
					Groups = input.LocationGroup.Groups,
					Group = true
				});
				output = output.Where(a => locations.Contains(a.LocationId));
			}
			if (input.LocationGroup != null)
			{
				if (input.LocationGroup.NonLocations != null && input.LocationGroup.NonLocations.Any())
				{
					var nonlocations = input.LocationGroup.NonLocations.Select(a => a.Id).ToList();
					output = output.Where(a => !nonlocations.Contains(a.LocationId));
				}
			}

			else if (input.LocationGroup == null)
			{
				var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
				{
					Locations = new List<SimpleLocationDto>(),
					Group = false,
					UserId = input.UserId
				});
				if (input.UserId > 0)
					output = output.Where(a => locations.Contains(a.LocationId));
			}
			return output;
		}
		#endregion  CashSummaryReport
	}
}
