﻿using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.CashSummaryReport.Dto;
using DinePlan.DineConnect.Connect.Custom.Dto;
using DinePlan.DineConnect.Connect.Customize.Dto;
using DinePlan.DineConnect.Connect.SpeedOfServiceReport.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.CashSummaryReport.Exporting
{
    public class CashSummaryReportExporter : FileExporterBase, ICashSummaryReportExporter
    {
        private readonly ITenantSettingsAppService _tenantSettingsService;

        public CashSummaryReportExporter(ITenantSettingsAppService tenantSettingsService)
        {
            _tenantSettingsService = tenantSettingsService;
        }
        public async Task<FileDto> ExportCashSummaryReport(GetCashSummaryReportInput input, ICashSummaryReportAppService appService)
        {
            var builder = new StringBuilder();
            builder.Append(L("CashSummaryReport"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append("_");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            if (input.ExportOutputType == 0)
            {
                using (var excelPackage = new ExcelPackage())
                {
                    await GetCashSummaryReportExportExcel(excelPackage, input, appService);
                    Save(excelPackage, file);
                }
                return ProcessFile(input, file);
            }
            else
            {
                var contentHTML = await ExportDatatableToHtml(input, appService);
                var result = ProcessFileHTML(file, contentHTML);
                return result;
            }
        }

        private FileDto ProcessFileHTML(FileDto file, string fileHTML)
        {
            File.WriteAllText(Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken), fileHTML);
            file.FileType = MimeTypeNames.ApplicationXhtmlXml;
            file.FileName = Path.GetFileNameWithoutExtension(file.FileName) + ".html";
            return file;
        }

        private async Task GetCashSummaryReportExportExcel(ExcelPackage package, GetCashSummaryReportInput ticketInput, ICashSummaryReportAppService appService)
        {
            var worksheet = package.Workbook.Worksheets.Add(L("CashSummaryReport"));
            worksheet.OutLineApplyStyle = true;
            var setting = await _tenantSettingsService.GetAllSettings();
            var dateTimeFormat = setting.Connect.DateTimeFormat;
            var dateFormat = setting.Connect.SimpleDateFormat;
            AddHeader(
                worksheet,
                L("Plant"),
                L("PlantName"),
                L("SoldTo"),
                L("DateOfSale"),
                L("Shift"),
                L("CloseShiftBy"),
                L("StartTimeOfEachShift"),
                L("EndTimeOfEachShift"),
                L("SaleDetail"),
                L("SaleAmount(ExcludedWHLDTaxAmount)"),
                L("TaxID")
                );

            worksheet.Cells[$"A{1}:K{1}"].Style.Font.Name = "Tahoma";
            worksheet.Cells[$"A{1}:K{1}"].Style.Font.Size = 14;

            var dtos = await appService.GetCashSummaryReport(ticketInput);

            //Add  report data
            var row = 2;
            //location
            foreach (var dataByPlant in dtos.ListItem)
            {
                foreach (var dataByDate in dataByPlant.ListItem)
                {
                    var indexShift = 1;
                    foreach (var dataByShift in dataByDate.ListItem)
                    {
                        foreach (var item in dataByShift.ListItem)
                        {
                            worksheet.Cells[row, 1].Value = dataByPlant.PlantCode;
                            worksheet.Cells[row, 2].Value = dataByPlant.PlantName;
                            worksheet.Cells[row, 3].Value = dataByPlant.SoldToID;
                            worksheet.Cells[row, 4].Value = dataByDate.Date.ToString(dateFormat);
                            worksheet.Cells[row, 5].Value = L("Shift") + " " + indexShift + $"( {dataByShift.StartUser} )";
                            worksheet.Cells[row, 6].Value = item.StopUser;
                            worksheet.Cells[row, 7].Value = dataByShift.StartShift;
                            worksheet.Cells[row, 8].Value = dataByShift.EndShift;
                            worksheet.Cells[row, 9].Value = item.Detail;
                            worksheet.Cells[row, 10].Value = item.Amount.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                            worksheet.Cells[row, 11].Value = item.TaxId;
                            worksheet.Cells[$"A{row}:K{row}"].Style.Font.Name = "Tahoma";
                            worksheet.Cells[$"A{row}:K{row}"].Style.Font.Size = 12;
                            row++;
                        }
                        indexShift++;
                    }
                }
            }
            for (var i = 1; i <= 11; i++)
            {
                worksheet.Column(i).AutoFit();
            }
        }
        private async Task<string> ExportDatatableToHtml(GetCashSummaryReportInput ticketInput, ICashSummaryReportAppService appService)
        {
            var dtos = await appService.GetCashSummaryReport(ticketInput);
            var setting = await _tenantSettingsService.GetAllSettings();
            var dateTimeFormat = setting.Connect.DateTimeFormat;
            var dateFormat = setting.Connect.SimpleDateFormat;
            StringBuilder strHTMLBuilder = new StringBuilder();
            strHTMLBuilder.Append("<html>");
            strHTMLBuilder.Append("<head>");
            strHTMLBuilder.Append("</head>");

            strHTMLBuilder.Append(@"<style> 
             body  { 
              font:400 12px 'Tahoma';
              font-size: 12px;
              padding:10px;
            }
            table  { 
                margin-top: 10px;
                border-collapse: collapse;
                width: 100%;
                margin-bottom: 10px;
            }

            table td, table th {
                border: 1px solid #ddd;
                padding: 8px;
            }
            table th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #364150;
                color: white;
            }
            thead {
                display: table-header-group;
            }
            tfoot {
                display: table-row-group;
            }
            tr{
                page-break-inside: avoid;
            }
            blockquote {
              color:white;
              text-align:center;
            }

            h3{
              text-align:center;
              text-transform: capitalize;
              font-size: medium;
            }

			.margin_bottom_0 {
				margin-bottom: 0 !important;
			}
            .margin_top_0 {
				margin-top: 0 !important;
			}

			.border_bottom_none {
				border-bottom: none !important;
			}

			.custom_table_date {
				margin-bottom: 0px !important;
			}

			.display_flex {
				display: flex;
			}

	
			.w-15 {
				width: 15%;
			}

			.w-25 {
				width: 25%;
			}

			.w-30 {
				width: 30%;
			}

			.w-35 {
				width: 35%;
			}
			.w-50 {
				width: 50%;
			}

			.w-85 {
				width: 85%;
			}


			.w-100 {
				width: 100%;
			}

	
			.float-right {
				float: right;
			}

			.ml-10 {
				margin-left: 10px;
			}

			.mt-50 {
				margin-top: 50px;
			}

			.zom-15 {
				zoom: 1.5;
			}

			.pl-44 {
				padding-left: 44px;
			}

			.thead_title {
				background-color: #f0f0f0;
				font-weight: bold;
			}

			.thead_content {
				background-color: #2f75b5;
				color: white;
			}
            .soldTo{
            background-color: #b4c6e7;
            } 
            .b_date{
                background-color: #ddebf7;
            }

			.footer_item {
				background-color: #d6dce5;
				font-weight: bold;
			}
			.footer_str {
				display: flex;
				padding: 10px;
			}

			.footer {
				background-color: #e2f0d9;
				font-weight: bold;
			}

			.color_header {
				background-color: #808080;
				color: white;
			}

			.color_posName {
				background-color: #f0f0f0;
			}

			.color_total {
				background-color: #f0f0f0;
			}

			.font_weight_bold {
				font-weight: bold;
			}

			.text-center{
			   text-align: center;
			}
             .font_16{
				font-size: 16;
             }
            .font_14{
				font-size: 14;
             }
             .font_12{
				font-size: 12;
             }
            </style>");

            strHTMLBuilder.Append("<body>");

            var titleDateRange = $"{ticketInput.StartDate.ToString(dateFormat)} - {ticketInput.EndDate.ToString(dateFormat)}";
            var lstPlantCode = "";
            foreach (var item in dtos.ListItem)
            {
                lstPlantCode = lstPlantCode + item.PlantCode + ",";
            }
            if (lstPlantCode.Length > 0)
            {
                lstPlantCode = lstPlantCode.Substring(0, lstPlantCode.Length - 1);
            }

            strHTMLBuilder.Append("<div>");
            strHTMLBuilder.Append("<table class='table table-bordered custom_table_date border_bottom_none'>");
            strHTMLBuilder.Append("<tr class='thead_title'>");
            strHTMLBuilder.Append($"<td colspan = '4' class='border_bottom_none text-center font_16'>" +
                $"{L("CashSummaryReport")} </br> " +
                $"{L("Plant")} : {lstPlantCode}  </br> " +
                $"{titleDateRange} " +
                $" </td>");
            strHTMLBuilder.Append("</tr>");
            strHTMLBuilder.Append("<tr class='thead_content font_14'>");
            strHTMLBuilder.Append($"<td class='w-15 border_bottom_none text-center font_weight_bold'>{L("SNo")}</td>");
            strHTMLBuilder.Append($"<td class='w-35 border_bottom_none text-center font_weight_bold'>{L("Description")}</td>");
            strHTMLBuilder.Append($"<td class='w-25 border_bottom_none text-center font_weight_bold'>{L("Amount")}</td>");
            strHTMLBuilder.Append($"<td class='w-25 border_bottom_none text-center font_weight_bold'>{L("CustomerTAXID")}</td>");
            strHTMLBuilder.Append("</tr>");
            strHTMLBuilder.Append("</table>");
            strHTMLBuilder.Append("</div>");

            foreach (var dataByPlant in dtos.ListItem.OrderBy(s => s.PlantName))
            {
                strHTMLBuilder.Append("<div class='content_str'>");

                strHTMLBuilder.Append("<div class='w-100 display_flex color_header'>");

                strHTMLBuilder.Append("<table class='table table-bordered custom_table_date border_bottom_none margin_top_0 soldTo'>");
                strHTMLBuilder.Append("<tr>");
                strHTMLBuilder.Append($"<td colspan = '4' class='w-85 border_bottom_none font_14'>{L("Plant")} : {  dataByPlant.PlantCode } - {dataByPlant.PlantName}  {L("SoldTo")}:  { dataByPlant.SoldToID }</td> ");
                strHTMLBuilder.Append("</tr>");
                strHTMLBuilder.Append("</table>");

                strHTMLBuilder.Append("</div>");
                foreach (var dataByDate in dataByPlant.ListItem.OrderBy(s => s.Date))
                {
                    strHTMLBuilder.Append("<div class='display_flex w-100 color_header'>");
                    strHTMLBuilder.Append("<table class='table table-bordered custom_table_date border_bottom_none margin_top_0 font_14 b_date'>");
                    strHTMLBuilder.Append("<tr>");
                    strHTMLBuilder.Append($"<td class='w-15 border_bottom_none text-center margin_top_0'>{L("Date")}</td>");
                    strHTMLBuilder.Append($"<td colspan = '5' class='w-85 border_bottom_none'> {dataByDate.Date.ToString(dateFormat)}</td>");
                    strHTMLBuilder.Append("</tr>");
                    strHTMLBuilder.Append("</table>");
                    strHTMLBuilder.Append("</div>");


                    strHTMLBuilder.Append("<div class='w-100'>");
                    var indexByShift = 1;
                    foreach (var dataByShift in dataByDate.ListItem)
                    {
                        strHTMLBuilder.Append("<table class='table table-bordered margin_bottom_0 margin_top_0'>");
                        strHTMLBuilder.Append("<tr class='color_posName font_14'>");
                        strHTMLBuilder.Append($"<td class='w-15 text-center'> { L("Shift")}  { indexByShift } ( {dataByShift.StartUser } )</td>");

                        strHTMLBuilder.Append($"<td colspan = '4' class= 'w-85' > { dataByShift.StartShift} -  { dataByShift.EndShift} </td>");
                        strHTMLBuilder.Append("</tr>");

                        //filter by item
                        var index = 1;
                        foreach (var item in dataByShift.ListItem)
                        {
                            strHTMLBuilder.Append("<tr class='font_12'>");
                            strHTMLBuilder.Append($"<td class='w-15 text-center'>{index}</td>");
                            strHTMLBuilder.Append($"<td class= 'w-35' >{ item.Detail}</td> ");
                            strHTMLBuilder.Append($"<td class= 'w-25 text-center' > { item.Amount.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td>");
                            strHTMLBuilder.Append($"<td class= 'w-25'> { item.TaxId }</td>");
                            strHTMLBuilder.Append("</tr>");
                            index++;
                        }

                        // sum by shift
                        strHTMLBuilder.Append("<tr class='color_total'>");
                        strHTMLBuilder.Append($"<td colspan = '2' style='text-align: right;' class='font_14'>{L("TotalShift")} {indexByShift}</td>");
                        strHTMLBuilder.Append($"<td class='text-center font_14'>{dataByShift.TotalAmount.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td>");
                        strHTMLBuilder.Append("<td ></td>");
                        strHTMLBuilder.Append("</tr>");
                        strHTMLBuilder.Append("</table>");
                        indexByShift++;
                    }
                    strHTMLBuilder.Append("</div>");
                    // sum by dataByDate
                    strHTMLBuilder.Append("<table class='table table-bordered custom_table_date border_bottom_none color_total margin_top_0'>");
                    strHTMLBuilder.Append("<tr>");
                    strHTMLBuilder.Append($"<td colspan = '2' style='text-align: right;' class='w-50 font_14'>{L("TotalDate")} {dataByDate.Date.ToString(dateFormat)}</td>");
                    strHTMLBuilder.Append($"<td class='text-center w-25 font_14'>{dataByDate.TotalAmount.ToString(ConnectConsts.ConnectConsts.NumberFormat) }</td> ");
                    strHTMLBuilder.Append("<td></td>");
                    strHTMLBuilder.Append("</tr>");
                    strHTMLBuilder.Append("</table>");
                }
                strHTMLBuilder.Append("</div>");

                // total by plant
                strHTMLBuilder.Append("<table class='table table-bordered custom_table_date border_bottom_none color_total margin_top_0'>");
                strHTMLBuilder.Append("<tr>");
                strHTMLBuilder.Append($"<td colspan = '2' style='text-align: right;' class='w-50 font_14 font_weight_bold'>{L("TotalPlant")}   {dataByPlant.PlantCode}</td>");
                strHTMLBuilder.Append($"<td class='text-center w-25 font_14 font_weight_bold'> {dataByPlant.TotalAmount.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</ td >");
                strHTMLBuilder.Append("<td></td>");
                strHTMLBuilder.Append("</tr> ");
                strHTMLBuilder.Append("</table >");

                strHTMLBuilder.Append("</div>");
            }

            // total by plant
            strHTMLBuilder.Append("<table class='table table-bordered custom_table_date border_bottom_none color_total margin_top_0 font_14'>");
            strHTMLBuilder.Append("<tr>");
            strHTMLBuilder.Append($"<td colspan = '2' style='text-align: right;' class='w-50 font_weight_bold'>{L("AllTotal")}</td>");
            strHTMLBuilder.Append($"<td class='text-center w-25 font_weight_bold'> {dtos.TotalAmount.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</ td >");
            strHTMLBuilder.Append("<td></td>");
            strHTMLBuilder.Append("</tr>");
            strHTMLBuilder.Append("</table >");

            //Close tags.  
            strHTMLBuilder.Append("</table>");
            strHTMLBuilder.Append("</body>");
            strHTMLBuilder.Append("</html>");

            string Htmltext = strHTMLBuilder.ToString();
            return Htmltext;
        }

    }
}