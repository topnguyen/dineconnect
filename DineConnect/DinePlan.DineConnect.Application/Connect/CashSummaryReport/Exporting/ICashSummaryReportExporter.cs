﻿using DinePlan.DineConnect.Connect.CashSummaryReport.Dto;
using DinePlan.DineConnect.Connect.Custom.Dto;
using DinePlan.DineConnect.Connect.Customize.Dto;
using DinePlan.DineConnect.Connect.SpeedOfServiceReport.Dto;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.CashSummaryReport.Exporting
{
    public interface ICashSummaryReportExporter
    {
        Task<FileDto> ExportCashSummaryReport(GetCashSummaryReportInput input, ICashSummaryReportAppService appService);
    }
}