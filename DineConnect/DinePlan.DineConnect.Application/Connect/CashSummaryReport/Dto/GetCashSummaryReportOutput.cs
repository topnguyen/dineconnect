﻿using Abp.Domain.Entities.Auditing;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.CashSummaryReport.Dto
{
	public class GetCashSummaryReportOutput
	{
		public List<GetCashSummaryReportByPlant> ListItem { get; set; }
		public decimal TotalAmount
		{
			get
			{
				return ListItem.Sum(x => x.TotalAmount);
			}
		}
		public GetCashSummaryReportOutput()
		{
			ListItem = new List<GetCashSummaryReportByPlant>();
		}

	}
	public class GetCashSummaryReportByPlant
	{
		public string PlantCode { get; set; }
		public string PlantName { get; set; }
		public string SoldToID { get; set; }
		public List<GetCashSummaryReportByDate> ListItem { get; set; }
		public decimal TotalAmount
		{
			get
			{
				return ListItem.Sum(x => x.TotalAmount);
			}
		}
		public GetCashSummaryReportByPlant()
		{
			ListItem = new List<GetCashSummaryReportByDate>();
		}
	}
	public class GetCashSummaryReportByDate
	{
		public string PlantCode { get; set; }
		public string PlantName { get; set; }
		public string SoldToID { get; set; }
		public DateTime Date { get; set; }
		public DateTime DateSort { get; set; }
		public List<GetCashSummaryReportByShift> ListItem { get; set; }
		public decimal TotalAmount
		{
			get
			{
				return ListItem.Sum(x => x.TotalAmount);
			}
		}
		public GetCashSummaryReportByDate()
		{
			ListItem = new List<GetCashSummaryReportByShift>();
		}
	}

	public class GetCashSummaryReportByShift
	{
		public string PlantCode { get; set; }
		public string PlantName { get; set; }
		public DateTime Date { get; set; }
		public string SoldToID { get; set; }
		public string StartUser { get; set; }
		public string EndShift { get; set; }
		public string StartShift { get; set; }
		public List<GetCashSummaryReportByItem> ListItem { get; set; }
		public decimal TotalAmount
		{
			get
			{
				return ListItem.Sum(x => x.Amount);
			}
		}
		public GetCashSummaryReportByShift()
		{
			ListItem = new List<GetCashSummaryReportByItem>();
		}
	}
	public class GetCashSummaryReportByItem
	{
		public int LocationID { get; set; }
		public string PlantCode { get; set; }
		public string PlantName { get; set; }
		public DateTime Date { get; set; }
		public string StartUser { get; set; }
		public string StopUser { get; set; }
		public string EndShift { get; set; }
		public string StartShift { get; set; }
		public decimal Amount { get; set; }
		public string TaxId { get; set; }
		public string AddOn { get; set; }
		public string SoldToID
		{
			get
			{
				if (!string.IsNullOrEmpty(AddOn))
				{
					var result = JsonConvert.DeserializeObject<AddOn>(AddOn);
					return result.SoldTo;
				}
				return null;
			}
		}

		public string Detail { get; set; }
		public bool WithTWH { get; set; }
	}

	public class AddOn
	{
		public string SoldTo { get; set; }

	}
}
