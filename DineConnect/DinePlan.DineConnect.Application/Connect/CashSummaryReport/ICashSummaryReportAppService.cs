﻿
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.CashSummaryReport.Dto;
using DinePlan.DineConnect.Connect.Customize.Dto;
using DinePlan.DineConnect.Connect.SpeedOfServiceReport.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.CashSummaryReport
{
	public interface ICashSummaryReportAppService : IApplicationService
	{
		Task<GetCashSummaryReportOutput> GetCashSummaryReport(GetCashSummaryReportInput input);
		Task<FileDto> GetCashSummaryReportToExport(GetCashSummaryReportInput input);

	}
}
