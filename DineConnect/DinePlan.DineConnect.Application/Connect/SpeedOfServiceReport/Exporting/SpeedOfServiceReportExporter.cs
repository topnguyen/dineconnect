﻿using Abp.Configuration;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Custom.Dto;
using DinePlan.DineConnect.Connect.Customize.Dto;
using DinePlan.DineConnect.Connect.SpeedOfServiceReport.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.SpeedOfServiceReport.Exporting
{
	public class SpeedOfServiceReportExporter : FileExporterBase, ISpeedOfServiceReportExporter
	{
        private string timeSpanFormat = "hh\\:mm\\:ss";
        private readonly SettingManager _settingManager;
        public SpeedOfServiceReportExporter(SettingManager settingManager)
        {
            _settingManager = settingManager;
        }
        public async Task<FileDto> ExportSpeedOfServiceReport(GetSpeedOfServiceReportInput input, ISpeedOfServiceReportAppService appService)
        {
            var builder = new StringBuilder();
            builder.Append(L("SpeedOfServiceReport"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append("_");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                await GetSpeedOfServiceReport(excelPackage, input, appService);
                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }
        private async Task GetSpeedOfServiceReport(ExcelPackage package, GetSpeedOfServiceReportInput input, ISpeedOfServiceReportAppService appService)
        {
            var worksheet = package.Workbook.Worksheets.Add(L("SpeedOfServiceReport"));
            worksheet.OutLineApplyStyle = true;

            AddHeader(
                worksheet,
                L("POSName"),
                L("Plant"),
                L("PlantName"),
                L("Staff"),
                L("TicketNumber"),
                L("DateTime"),
                L("OrderTime"),
                L("PackingTime"),
                L("TotalTime")
                );

            //Add  report data
            var row = 2;
            var dtos = await appService.GetSpeedOfServiceReport(input);
			foreach (var databyLocation in dtos.ListItem)
			{
				foreach (var dataByPos in databyLocation.ListItem)
				{
                    foreach(var item in dataByPos.ListItem)
					{
                        worksheet.Cells[row, 1].Value = item.POS;
                        worksheet.Cells[row, 2].Value = item.Plant;
                        worksheet.Cells[row, 3].Value = item.PlantName;
                        worksheet.Cells[row, 4].Value = item.Staff;
                        worksheet.Cells[row, 5].Value = item.TicketNumber;
                        worksheet.Cells[row, 6].Value = item.DateTime.ToString(input.DatetimeFormat);
                        worksheet.Cells[row, 7].Value = item.OrderTime.ToString(timeSpanFormat);
                        worksheet.Cells[row, 8].Value = item.PackingTime.ToString(timeSpanFormat);
                        worksheet.Cells[row, 9].Value = item.TotalTime.ToString(timeSpanFormat);
                        row++;
                    }
                    worksheet.Cells[row, 6].Value = "AVG " + dataByPos.POS;
                    worksheet.Cells[row, 7].Value = dataByPos.AVGOrderTime.ToString(timeSpanFormat);
                    worksheet.Cells[row, 8].Value = dataByPos.AVGPackingTime.ToString(timeSpanFormat);
                    worksheet.Cells[row, 9].Value = dataByPos.AVGTotalTime.ToString(timeSpanFormat);
                    row++;
                }
				worksheet.Cells[row, 6].Value = "Total AVG " + databyLocation.Plant;
				worksheet.Cells[row, 7].Value = databyLocation.TotalOrderTime.ToString(timeSpanFormat);
				worksheet.Cells[row, 8].Value = databyLocation.TotalPackingTime.ToString(timeSpanFormat);
				worksheet.Cells[row, 9].Value = databyLocation.TotalTotalTime.ToString(timeSpanFormat);
				row++;
                worksheet.Cells[row, 8].Value = "Met% ";
                worksheet.Cells[row, 9].Value = databyLocation.TotalMet.ToString(ConnectConsts.ConnectConsts.NumberFormat) + " %";
                row++;
            }

			worksheet.Cells[row, 6].Value = "TotalAVG";
			worksheet.Cells[row, 7].Value = dtos.TotalOrderTime.ToString(timeSpanFormat);
			worksheet.Cells[row, 8].Value = dtos.TotalPackingTime.ToString(timeSpanFormat);
			worksheet.Cells[row, 9].Value = dtos.TotalTotalTime.ToString(timeSpanFormat);

            worksheet.Cells[row + 1,8].Value = L("TotalMet%");
            worksheet.Cells[row + 1, 9].Value = dtos.TotalMet.ToString(ConnectConsts.ConnectConsts.NumberFormat) + " %";

            for (var i = 1; i <= 9; i++)
			{
				worksheet.Column(i).AutoFit();
			}
		}
    }
}