﻿using DinePlan.DineConnect.Connect.Custom.Dto;
using DinePlan.DineConnect.Connect.Customize.Dto;
using DinePlan.DineConnect.Connect.SpeedOfServiceReport.Dto;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.SpeedOfServiceReport.Exporting
{
    public interface ISpeedOfServiceReportExporter
    {
        Task<FileDto> ExportSpeedOfServiceReport(GetSpeedOfServiceReportInput input, ISpeedOfServiceReportAppService appService);
    }
}