﻿
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Customize.Dto;
using DinePlan.DineConnect.Connect.SpeedOfServiceReport.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.SpeedOfServiceReport
{
	public interface ISpeedOfServiceReportAppService : IApplicationService
	{
		Task<GetSpeedOfServiceReportOutput> GetSpeedOfServiceReport(GetSpeedOfServiceReportInput input);
		Task<FileDto> GetSpeedOfServiceReportToExport(GetSpeedOfServiceReportInput input);

	}
}
