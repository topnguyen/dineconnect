﻿using Abp.Application.Services.Dto;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Auditing.Exporting;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Audit;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Connect.Tag;
using DinePlan.DineConnect.Connect.Ticket.CategoryReport.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Connect.WorkPeriod.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.Handler.Mapping;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.House.Transaction.Implementation;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Report;
using DinePlan.Infrastructure.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System;
using Abp.Configuration;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Customize.Dto;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using DinePlan.DineConnect.Connect.Custom.Exporting;
using DinePlan.DineConnect.Connect.SpeedOfServiceReport.Dto;
using DinePlan.DineConnect.Connect.SpeedOfServiceReport.Exporting;
using DinePlan.DineConnect.Connect.ProgramSettings;

namespace DinePlan.DineConnect.Connect.SpeedOfServiceReport
{
	public class SpeedOfServiceReportAppService : DineConnectAppServiceBase, ISpeedOfServiceReportAppService
    {
        private readonly IRepository<Company> _companyRe;
        private readonly IRepository<Master.Department> _dRepo;
        private readonly ILocationAppService _locService;
        private readonly IRepository<Master.Location> _lRepo;
        private readonly IRepository<Transaction.Ticket> _ticketManager;
        private readonly IRepository<Transaction.Order> _orderManager;
        private readonly IRepository<Master.Terminal> _terminalRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private  ICustomizeExporter _customizeExporter;
        private readonly IReportBackgroundAppService _rbas;
        private readonly IBackgroundJobManager _bgm;
        private readonly ISpeedOfServiceReportExporter _exporter;
        private readonly IProgramSettingTemplateAppService _programSettingTemplateAppService;
        private readonly ITenantSettingsAppService _tenantSettingsService;
        public SpeedOfServiceReportAppService(
            IRepository<Transaction.Ticket> ticketManager, 
            ILocationAppService locService,
            IRepository<Company> comR,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<Master.Department> drepo, 
            IRepository<Master.Location> lRepo ,
            IRepository<Transaction.Order> orderManager,
            ICustomizeExporter customizeExporter ,
             IReportBackgroundAppService rbas  ,
             IBackgroundJobManager bgm     ,
             ISpeedOfServiceReportExporter exporter  ,
             IRepository<Master.Terminal> terminalRepository,
             IProgramSettingTemplateAppService programSettingTemplateAppService,
              ITenantSettingsAppService tenantSettingsService
            )
		{
            _ticketManager = ticketManager;
            _companyRe = comR;
            _unitOfWorkManager = unitOfWorkManager;
            _dRepo = drepo;
            _locService = locService;
            _lRepo = lRepo;
            _orderManager = orderManager;
            _customizeExporter = customizeExporter;
            _rbas = rbas;
            _bgm = bgm;
            _exporter = exporter;
            _terminalRepository = terminalRepository;
            _tenantSettingsService = tenantSettingsService;
            _programSettingTemplateAppService = programSettingTemplateAppService;
        }



        public async Task<GetSpeedOfServiceReportOutput> GetSpeedOfServiceReport(GetSpeedOfServiceReportInput input)
        {
            var result = new GetSpeedOfServiceReportOutput();
            try
            {
                result.Title = $"Speed Of Service ( {input.StartDate.ToString("MM/dd/yyy")} - {input.EndDate.ToString("MM/dd/yyy")} )";
                var defaultMaxOrderTime = "00:00:30";
                var defaultMaxPackingTime = "00:01:00";
                var maxOrderTimeFromPST = await _programSettingTemplateAppService.GetProgramSettingByName(ConnectConsts.ConnectConsts.MaxOrderTime);
                var maxPackingTimeFromPST = await _programSettingTemplateAppService.GetProgramSettingByName(ConnectConsts.ConnectConsts.MaxPackingTime);
				var maxOrderTime = GetTimeSpanFromString(maxOrderTimeFromPST != null ?  maxOrderTimeFromPST.DefaultValue : defaultMaxOrderTime);
				var maxPackingTime = GetTimeSpanFromString(maxPackingTimeFromPST != null ? maxPackingTimeFromPST.DefaultValue : defaultMaxPackingTime);
				var maxProcessTime = maxOrderTime + maxPackingTime;
				
                var salesTaxReportViewDtos = GetSpeedOfServiceReportForCondition(input).OrderByDescending(x=>x.TicketCreatedTime).ToList();
                var totalMet = 0;

                var dataItem = salesTaxReportViewDtos.Select(x => new GetSpeedOfServiceReportItemOutput
                {
                    Department = x.DepartmentName,
                    Staff = x.LastModifiedUserName,
                    TicketNumber = Int64.Parse(x.TicketNumber),
                    DateTime = x.TicketCreatedTime,
                    OrderTime = new TimeSpan(),
                    PackingTime = new TimeSpan(),
                    TotalTime = new TimeSpan(),
                    Plant = x.Location.Code,
                    PlantName = x.Location.Name,
                    POS = x.TerminalName,
                    TicketStatus = x.TicketStatus ,
                    MaxProcessTime = maxProcessTime,
                }).ToList();

                foreach (var item in dataItem)
                {
                    if (!string.IsNullOrEmpty(item.TicketStatus))
                    {
                        var ticketStatus = JsonConvert.DeserializeObject<TicketStatus>(item.TicketStatus);
                        if (ticketStatus != null)
                        {
                            if (!string.IsNullOrEmpty(ticketStatus.OrderTime))
                                item.OrderTime = GetTimeSpanFromString(ticketStatus.OrderTime);
                            if (!string.IsNullOrEmpty(ticketStatus.PackingTime))
                                item.PackingTime = GetTimeSpanFromString(ticketStatus.PackingTime);
                            item.TotalTime = item.OrderTime + item.PackingTime;
                        }
                    }
                }

                

                // filter builder 
                var dataAsQueryable = dataItem.AsQueryable();
                if (!string.IsNullOrEmpty(input.DynamicFilter))
                {
                    var jsonSerializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver =
                            new CamelCasePropertyNamesContractResolver()
                    };
                    var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                    if (filRule?.Rules != null)
                    {
                        dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                    }
                }

                foreach(var item in dataAsQueryable.ToList())
				{
                    if (item.TotalTime <= maxProcessTime)
                        totalMet += 1;
                }

                var dataBy_Plant_POS = dataAsQueryable.ToList().GroupBy(x =>new { x.POS, x.Plant,x.PlantName })
                    .Select(x => new GetSpeedOfServiceReporByPOSOutput
                    {
                        POS = x.Key.POS,
                        Plant = x.Key.Plant,
                        PlantName = x.Key.PlantName,
                        ListItem = x.OrderBy(a=>a.TicketNumber).ToList()
                    }).ToList();

                //TODO: Get POS ID and sort POS ID
                foreach(var item in dataBy_Plant_POS)
				{
					if (!string.IsNullOrEmpty(item.POS))
					{
                        item.POSID = await GetTerminalID(item.POS);
                    }
				}

                var dataBy_Plant = dataBy_Plant_POS.GroupBy(x => new { x.Plant, x.PlantName })
                 .Select(x => new GetSpeedOfServiceReportByLocationOutput
                 {
                     Plant = x.Key.Plant,
                     PlantName = x.Key.PlantName,
                     ListItem = x.OrderBy(a=>a.POSID).ToList()
                 }).ToList();

                result.ListItem = dataBy_Plant;

                return await Task.FromResult(result);
            }
            catch (Exception ex)
             {
                throw ex;
            }
        }

        private async Task<int> GetTerminalID(string terminalName)
		{
            var terminal =await _terminalRepository.FirstOrDefaultAsync(x => x.Name == terminalName);
            return terminal != null ? terminal.Id : 0;
        }

        public async Task<FileDto> GetSpeedOfServiceReportToExport(GetSpeedOfServiceReportInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                var setting = await _tenantSettingsService.GetAllSettings();
                input.DatetimeFormat = setting.Connect.DateTimeFormat;
                input.DateFormat = setting.Connect.SimpleDateFormat;

                input.MaxResultCount = 10000;

                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.SPEEDOFSERVICEREPORT,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });

                    if (backGroundId > 0)
                    {
                        var tInput = new SpeedOfServiceReportInput
                        {
                            GetSpeedOfServiceReportInput = input,
                        };
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.SPEEDOFSERVICEREPORT,
                            SpeedOfServiceReportInput = tInput,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                    }
                }
                else
                {
                    var output = await _exporter.ExportSpeedOfServiceReport(input, this);
                    return output;
                }
            }

            return null;
        }

        private TimeSpan GetTimeSpanFromString(string timeString)
        {
            var timeSpan = new TimeSpan();
            var times = timeString.Split(':');
            if (times != null && times.Length == 3)
            {
                // format time is hh:mm:ss
                timeSpan = TimeSpan.FromHours(double.Parse(times[0]));
                timeSpan += TimeSpan.FromMinutes(double.Parse(times[1]));
                timeSpan += TimeSpan.FromSeconds(double.Parse(times[2]));
            }
			else
			{
                // format time is mm:ss
                timeSpan += TimeSpan.FromMinutes(double.Parse(times[0]));
                timeSpan += TimeSpan.FromSeconds(double.Parse(times[1]));
            }
            return timeSpan;
        }

        public IQueryable<Transaction.Ticket> GetSpeedOfServiceReportForCondition(GetSpeedOfServiceReportInput input)
        {
          
            IQueryable<Transaction.Ticket> output;

            var correctDate = CorrectInputDate(input);
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                output = _ticketManager.GetAll();
                if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
                {
                    if (correctDate)
                    {
                        output =
                            output.Where(
                                a =>
                                    a.LastPaymentTime >=
                                    input.StartDate
                                    &&
                                    a.LastPaymentTime <=
                                    input.EndDate);
                    }
                    else
                    {
                        var mySt = input.StartDate;
                        var myEn = input.EndDate;

                        output =
                            output.Where(
                                a =>
                                    a.LastPaymentTime >= mySt
                                    &&
                                    a.LastPaymentTime <= myEn);
                    }
                }
            }

            if (input.Location > 0)
            {
                output = output.Where(a => a.LocationId == input.Location);
            }
            else if (input.Locations != null && input.Locations.Any())
            {
                var locations = input.Locations.Select(a => a.Id).ToList();
                output = output.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
                     && !input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                if (locations.Any()) output = output.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                output = output.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true
                });
                output = output.Where(a => locations.Contains(a.LocationId));
            }
            if (input.LocationGroup != null)
            {
                if (input.LocationGroup.NonLocations != null && input.LocationGroup.NonLocations.Any())
                {
                    var nonlocations = input.LocationGroup.NonLocations.Select(a => a.Id).ToList();
                    output = output.Where(a => !nonlocations.Contains(a.LocationId));
                }
            }

            else if (input.LocationGroup == null)
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = new List<SimpleLocationDto>(),
                    Group = false,
                    UserId = input.UserId
                });
                if (input.UserId > 0)
                    output = output.Where(a => locations.Contains(a.LocationId));
            }

            output = output.Where(x =>  !x.PreOrder && !x.Credit);
      
            return output;
        }

        public bool CorrectInputDate(IDateInput input)
        {
            if (input.NotCorrectDate) return true;
            var returnStatus = true;

            var operateHours = 0M;

            if (input.Locations != null && input.Locations.Any() && input.Locations.Count() == 1)
                operateHours = _lRepo.Get(input.Locations.First().Id).ExtendedBusinessHours;

            if (operateHours == 0M && input.Location > 0)
                operateHours = _lRepo.Get(input.Location).ExtendedBusinessHours;
            if (operateHours == 0M && input.LocationGroup != null && input.LocationGroup.Locations.Any() &&
                input.LocationGroup.Locations.Count() == 1)
                operateHours = _lRepo.Get(input.LocationGroup.Locations.First().Id).ExtendedBusinessHours;
            if (operateHours == 0M)
                operateHours = SettingManager.GetSettingValue<decimal>(AppSettings.ConnectSettings.OperateHours);

            if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
            {
                if (operateHours == 0M) returnStatus = false;
                //input.EndDate = input.EndDate.AddDays(1);
                input.StartDate = input.StartDate.AddHours(Convert.ToDouble(operateHours));
                //input.EndDate = input.EndDate.AddHours(Convert.ToDouble(operateHours)).AddMinutes(-1);
                input.EndDate = input.EndDate.AddHours(Convert.ToDouble(operateHours));
            }
            else
            {
                returnStatus = false;
            }

            return returnStatus;
        }

		
	}
}
