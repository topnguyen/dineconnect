﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DinePlan.DineConnect.Connect.SpeedOfServiceReport.Dto
{
	public class GetSpeedOfServiceReportOutput : AuditedEntity
	{
		public string Title { get; set; }
		public decimal TotalMet
		{
			get
			{
				var result = 0M;
				if(ListItem.Count > 0)
				{
					result =  ListItem.Average(x => x.TotalMet);
				}
				return result;
			}
		}
		public List<GetSpeedOfServiceReportByLocationOutput> ListItem { get; set; }

		public TimeSpan TotalOrderTime
		{
			get
			{
				var result = new TimeSpan(00, 00, 00);
				if (ListItem.Count > 0)
				{
					result = new TimeSpan(Convert.ToInt64(ListItem.Select(x => x.TotalOrderTime)?.Average(t => t.Ticks)));
				}
				return result;
			}
		}
		public TimeSpan TotalPackingTime
		{
			get
			{
				var result = new TimeSpan(00, 00, 00);
				if (ListItem.Count > 0)
				{
					result = new TimeSpan(Convert.ToInt64(ListItem.Select(x => x.TotalPackingTime).Average(t => t.Ticks)));
				}
				return result;
			}
		}
		public TimeSpan TotalTotalTime
		{
			get
			{
				var result = new TimeSpan(00, 00, 00);
				if (ListItem.Count > 0)
				{
					result = new TimeSpan(Convert.ToInt64(ListItem.Select(x => x.TotalTotalTime).Average(t => t.Ticks)));
				}
				return result;
			}
		}
		public GetSpeedOfServiceReportOutput()
		{
			ListItem = new List<GetSpeedOfServiceReportByLocationOutput>();

		}
	}

	public class GetSpeedOfServiceReportByLocationOutput : AuditedEntity
	{
		public string Plant { get; set; }
		public string PlantName { get; set; }
		public decimal TotalMet
		{
			get
			{
				var totalMet = ListItem.Sum(x => x.TotalMet);
				var totalCountItem = ListItem.Sum(x=>x.TotalCount);
				return ((decimal)totalMet / (decimal)totalCountItem) * 100;
			}
		}
		public List<GetSpeedOfServiceReporByPOSOutput> ListItem { get; set; }

		public TimeSpan TotalOrderTime
		{
			get
			{
				var result = new TimeSpan(Convert.ToInt64(ListItem.Select(x => x.AVGOrderTime).Average(t => t.Ticks)));
				return result;
			}
		}
		public TimeSpan TotalPackingTime
		{
			get
			{
				var result = new TimeSpan(Convert.ToInt64(ListItem.Select(x => x.AVGPackingTime).Average(t => t.Ticks)));
				return result;
			}
		}
		public TimeSpan TotalTotalTime
		{
			get
			{
				var result = new TimeSpan(Convert.ToInt64(ListItem.Select(x => x.AVGTotalTime).Average(t => t.Ticks)));
				return result;
			}
		}
		public GetSpeedOfServiceReportByLocationOutput()
		{
			ListItem = new List<GetSpeedOfServiceReporByPOSOutput>();

		}
	}
	public class GetSpeedOfServiceReporByPOSOutput : AuditedEntity
	{
		public string POS { get; set; }
		public int POSID { get; set; }
		public string Plant { get; set; }
		public string PlantName { get; set; }
		public int TotalMet
		{
			get
			{
				return ListItem.Where(x => x.IsMet == true).Count();
			}
		}

		public int TotalCount
		{
			get
			{
				return ListItem.Count();
			}
		}

		public TimeSpan AVGOrderTime
		{
			get
			{
				var average = new TimeSpan(Convert.ToInt64(ListItem.Select(x => x.OrderTime).Average(t => t.Ticks)));
				return average;
			}
		}
		public TimeSpan AVGPackingTime
		{
			get
			{
				var average = new TimeSpan(Convert.ToInt64(ListItem.Select(x => x.PackingTime).Average(t => t.Ticks)));
				return average;
			}
		}
		public TimeSpan AVGTotalTime
		{
			get
			{
				var average = new TimeSpan(Convert.ToInt64(ListItem.Select(x => x.TotalTime).Average(t => t.Ticks)));
				return average;
			}
		}
		public List<GetSpeedOfServiceReportItemOutput> ListItem { get; set; }
		public GetSpeedOfServiceReporByPOSOutput()
		{
			ListItem = new List<GetSpeedOfServiceReportItemOutput>();
		}

	}

	public class GetSpeedOfServiceReportItemOutput
	{
		public string Plant { get; set; }
		public string POS { get; set; }
		public string PlantName { get; set; }
		public string Department { get; set; }
		public string SalesChannel { get; set; }
		public string TerminalId { get; set; }
		public string Staff { get; set; }
		public long? TicketNumber { get; set; }
		public DateTime DateTime { get; set; }
		public TimeSpan OrderTime { get; set; }
		public TimeSpan PackingTime { get; set; }
		public TimeSpan TotalTime { get; set; }
		public string TicketStatus { get; set; }
		public TimeSpan MaxProcessTime { get; set; }
		public bool IsMet
		{
			get
			{
				return TotalTime <= MaxProcessTime;
			}
		}

	}
}
