﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.SpeedOfServiceReport.Dto
{

    public class GetSpeedOfSeriveReporExcelListDto : AuditedEntity
    {
        public string POS { get; set; }
        public List<GetSpeedOfServiceReportItemOutput> ListItems { get; set; }
        public GetSpeedOfSeriveReporExcelListDto()
        {
            ListItems = new List<GetSpeedOfServiceReportItemOutput>();
        }
    }
}
