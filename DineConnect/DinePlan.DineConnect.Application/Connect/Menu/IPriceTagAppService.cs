﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Menu
{
    public interface IPriceTagAppService : IApplicationService
    {
        Task<PagedResultOutput<PriceTagListDto>> GetAll(GetPriceTagInput inputDto);

        Task<FileDto> GetAllToExcel(GetPriceTagInput inputDto);

        Task<GetPriceTagForEditOutput> GetPriceTagForEdit(NullableIdInput nullableIdInput);

        Task CreateOrUpdatePriceTag(CreateOrUpdatePriceTagInput input);

        Task DeletePriceTag(IdInput input);

        Task<ListResultOutput<PriceTagListDto>> GetIds();

        Task<TagPriceDto> ApiGetTagByLocationAndId(ApiLocationInput locationInput);

        Task<PagedResultOutput<TagMenuItemPortionDto>> GetMenuPricesForTag(GetPriceTagInput inputDto);

        Task UpdateTagPrice(TagPriceInput input);

        Task<TagPriceOutputDto> GetTagForLocation(ApiLocationInput locationId);
        Task<TagPriceOutputDto> ApiGetTags(ApiLocationInput input);

        Task<TagPriceDto> GetTagsById(int priceTagId);

        Task<FileDto> GetImportTemplate(string name);

        Task<ListResultOutput<PriceTagListDto>> GetPriceTagNames(bool futureData = false);

        Task ActivateItem(IdInput input);
        Task<PagedResultOutput<PriceTagLocationPriceListDto>> GetAllPriceTagLocationPrice(GetPriceTagInput input);
        Task<GetPriceTagLocationPriceForEditOutput> GetPriceTagLocationPriceForEdit(NullableIdInput input);
        Task CreatePriceTagLocationPrice(CreatePriceTagLocationPrice input);
        Task DeletePriceTagLocationPrice(IdInput input);
        Task<int> UpdateTagPriceInPricetagDefinition(TagPriceInput input);
    }
}