﻿using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Menu
{
    public interface ICategoryListExcelExporter
    {
        FileDto ExportToFile(List<CategoryListDto> dtos);
    }
}