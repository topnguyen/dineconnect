﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Menu
{
    public interface IProductGroupAppService : IApplicationService
    {
        Task<ListResultOutput<ProductGroupListDto>> GetProductGroups(GetProductGroupInput input);
       Task<PagedResultOutput<CategoryListDto>> GetProductGroupCategories(GeProductGroupCategoriesInput input);
        Task<PagedResultOutput<ProductGroupListDto>> GetAll(GetProductGroupInput inputDto);

        Task<FileDto> GetAllToExcel(GetProductGroupInput input);
        Task<ProductGroupEditDto> GetProductGroupForEdit(NullableIdInput nullableIdInput);

        Task<ProductGroupListDto> CreateProductGroup(CreateProductGroupInput input);
        Task<ProductGroupListDto> UpdateProductGroup(UpdateProductGroupInput input);

        Task DeleteProductGroup(IdInput input);
        Task<ProductGroupListDto> MoveProductGroup(MoveProductGroupInput input);
        Task<ListResultOutput<ProductGroupListDto>> GetProductGroupNames();

        Task<ProductGroupEditDto> GetOrCreateByName(string groupname, string code);
        Task UpdateCategoryProductgroupId(UpdateProductgroupCategoryInput input);
    }
}