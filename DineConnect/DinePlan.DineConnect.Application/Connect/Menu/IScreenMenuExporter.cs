﻿
using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Menu.Implementation;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Menu
{
    public interface IScreenMenuExporter
    {
        Task<FileDto> ExportTemplateFile(IList<MenuItemEditDto> dtos);
    }
}