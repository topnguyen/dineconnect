﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Extensions;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Discount.Dtos;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter;
using OpenHtmlToPdf;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Menu.Dtos
{
    [AutoMap(typeof(GetItemInput))]
    public class GetMenuEngineeringReportInput : PagedAndSortedInputDto, IShouldNormalize, IDateInput, IFileExport, IBackgroundExport
    {
        public bool IncludeTax { get; set; }
        public List<string> FilterByUser { get; set; }
        public List<string> FilterByTerminal { get; set; }
        public List<string> FilterByDepartment { get; set; }
        public List<int?> FilterByCategory { get; set; }
        public List<int?> FilterByGroup { get; set; }
        public List<string> FilterByTag { get; set; }
        public bool IsItemSalesReport { get; set; }
        public bool IsSummary { get; set; }
        public bool IsMenuItems { get; set; }
        public string LastModifiedUserName { get; set; }
        public bool Gift { get; set; }
        public bool Void { get; set; }
        public bool Comp { get; set; }
        public bool Sales { get; set; }
        public bool Exchange { get; set; }
        public bool NoSales { get; set; }
        public bool Stats { get; set; }
        public bool ByLocation { get; set; }
        public string Duration { get; set; }
        public string ExportOutput { get; set; }
        public int WorkPeriodId { get; set; }
        public string OutputType { get; set; }
        public List<PromotionListDto> Promotions { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Location { get; set; }
        public long UserId { get; set; }
        public bool NotCorrectDate { get; set; }
        public List<SimpleLocationDto> Locations { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public bool Credit { get; set; }
        public bool Refund { get; set; }
        public string TicketNo { get; set; }
        public ExportType ExportOutputType { get; set; }
        public PaperSize PaperSize { get; set; }
        public bool Portrait { get; set; }
        public List<int> MenuItemIds { get; set; }
        public List<int> MenuItemPortionIds { get; set; }
        public void Normalize()
        {
            if (Sorting.IsNullOrWhiteSpace())
            {
                Sorting = "Id";
            }
        }

        public bool RunInBackground { get; set; }
        public bool IsExport { get; set; }
        public string ReportDescription { get; set; }
        public int? TenantId { get; set; }

        public int TakeNumber { get; set; }
        public string TakeType { get; set; }
        public string Ranking { get; set; }

        public string TakeString
        {
            get { return $"{Ranking} {TakeType}"; }
        }
        public string DynamicFilter { get; set; }
        public List<ComboboxItemDto> AuditTypes { get; set; }
        public string TerminalName { get; set; }
        public bool TaxIncluded { get; set; }
        public string DateFormat { get; set; }
        public string DatetimeFormat { get; set; }

        public GetMenuEngineeringReportInput()
        {
            FilterByCategory = new List<int?>();
            FilterByGroup = new List<int?>();
            FilterByDepartment = new List<string>();
            Locations = new List<SimpleLocationDto>();
            FilterByTerminal = new List<string>();
            FilterByUser = new List<string>();
            MenuItemIds = new List<int>();
            MenuItemPortionIds = new List<int>();
        }
    }
}
