﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Location;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Menu.Dtos
{
    [AutoMapFrom(typeof(PriceTag))]
    public class PriceTagListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
        public string Locations { get; set; }
    }

    [AutoMapTo(typeof(PriceTag))]
    public class PriceTagEditDto : ConnectEditDto
    {
        public PriceTagEditDto()
        {
            Type = 1;
        }

        public int? Id { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public bool FutureData { get; set; }
    }

    public class GetPriceTagInput : IInputDto, IPagedResultRequest, ISortedResultRequest, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }

        public int TagId { get; set; }
        public int TagDefinitionId { get; set; }
        public int MaxResultCount { get; set; }
        public int SkipCount { get; set; }
        public string Sorting { get; set; }
        public string Location { get; set; }
        public int? FutureDataId { get; set; }
        public GetPriceTagInput()
        {
            LocationGroup = new LocationGroupDto();
        }

        public LocationGroupDto LocationGroup { get; set; }
    }

    public class TagMenuItemPortionDto
    {
        public int? Id { get; set; }
        public string MenuName { get; set; }
        public string PortionName { get; set; }
        public int MultiPlier { get; set; }
        public decimal Price { get; set; }
        public int MenuItemId { get; set; }
        public int TagId { get; set; }
        public decimal TagPrice { get; set; }
        public string DisplayName => MenuName + ":" + PortionName;
        public int PriceTagDefinitionId { get; set; }
    }

    public class TagPriceInput : IInputDto
    {
        [Required]
        public int TagId { get; set; }

        public decimal Price { get; set; }

        [Required]
        public int PortionId { get; set; }
        public int? FutureDataId { get; set; }
    }

    public class GetPriceTagForEditOutput : IOutputDto
    {
        public GetPriceTagForEditOutput()
        {
            LocationGroup = new LocationGroupDto();
            PriceTag = new PriceTagEditDto();
        }

        public PriceTagEditDto PriceTag { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
    }

    public class CreateOrUpdatePriceTagInput : IInputDto
    {
        [Required]
        public PriceTagEditDto PriceTag { get; set; }

        public LocationGroupDto LocationGroup { get; set; }

        public CreateOrUpdatePriceTagInput()
        {
            LocationGroup = new LocationGroupDto();
        }
    }

    public class TagPriceOutputDto
    {
        public List<TagPriceDto> Tags { get; set; }
    }

    public class TagPriceDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<TagDefinitionDto> Definitions { get; set; }
        public int Type { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

    public class TagDefinitionDto
    {
        public int TagId { get; set; }
        public string TagName { get; set; }
        public int MenuItemPortionId { get; set; }
        public decimal Price { get; set; }
    }
    public class GetPriceTagLocationPriceForEditOutput
    {
        public PriceTagLocationPriceEditDto PriceTagLocationPrice { get; set; }
    }
    [AutoMapTo(typeof(PriceTagLocationPrice))]
    public class PriceTagLocationPriceEditDto : CreationAuditedEntity
    {
       
        public int? Id { get; set; }
        public int PriceTagDefinitionId { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public decimal Price { get; set; }
    }
    public class CreatePriceTagLocationPrice : IInputDto
    {
        [Required] public PriceTagLocationPriceEditDto PriceTagLocationPrice { get; set; }
    }
    [AutoMapTo(typeof(PriceTagLocationPrice))]
    public class PriceTagLocationPriceListDto : CreationAuditedEntity
    {

        public int PriceTagDefinitionId { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public decimal Price { get; set; }
    }
}