﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.General;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace DinePlan.DineConnect.Connect.Menu.Dtos
{
    [AutoMapFrom(typeof(ScreenMenu))]
    public class ScreenMenuListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
        public string Locations { get; set; }
        public int CategoryColumnCount { get; set; }
        public int CategoryColumnWidthRate { get; set; }
        public int CategoryCount { get; set; }
        public int RefLocationId { get; set; }
        public bool Group { get; set; }
    }

    [AutoMapTo(typeof(ScreenMenu))]
    public class ScreenMenuEditDto : ConnectEditDto
    {
        public ScreenMenuEditDto()
        {
            LocationGroup = new LocationGroupDto();
        }
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Departments { get; set; }
        public int CategoryColumnCount { get; set; }
        public int CategoryColumnWidthRate { get; set; }
        public LocationGroupDto LocationGroup { get; set; }

        public bool FutureData { get; set; }

    }

    public class GetScreenMenuInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int LocationId { get; set; }
        public int MenuId { get; set; }
        public int CategoryId { get; set; }
        public string Filter { get; set; }
        public string Operation { get; set; }
        public string CurrentTag { get; set; }
        public int? FutureDataId { get; set; }
        public bool DateFilterApplied { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "SortOrder,Id";
            }
        }
    }

    public class GetScreenInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int MenuId { get; set; }
        public int CategoryId { get; set; }
        public string Filter { get; set; }
        public string Operation { get; set; }
        public string Location { get; set; }
        public bool IsDeleted { get; set; }
        public LocationGroupDto LocationGroup { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    public class GetScreenMenuForEditOutput : IOutputDto
    {
        public GetScreenMenuForEditOutput()
        {
            LocationGroup = new LocationGroupDto();
        }

        public ScreenMenuEditDto ScreenMenu { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public List<ComboboxItemDto> Departments { get; set; }
        public List<ScreenMenuCategoryEditDto> ScreenMenuCategory { get; set; }
    }

    public class CreateOrUpdateScreenMenuInput : IInputDto
    {
        public CreateOrUpdateScreenMenuInput()
        {
            LocationGroup = new LocationGroupDto();
            Departments = new List<ComboboxItemDto>();
        }

        [Required]
        public ScreenMenuEditDto ScreenMenu { get; set; }

        public LocationGroupDto LocationGroup { get; set; }
        public List<ComboboxItemDto> Departments { get; set; }
        public int? FutureDataId { get; set; }
    }

    public class CreateCategoryInput : IInputDto
    {
        public string Name { get; set; }
        public int MenuId { get; set; }
        public Guid DownloadImage { get; set; }
        public string Files { get; set; }
    }

    public class CreateScreenMenuItemInput : IInputDto
    {
        public string Name { get; set; }
        public int MenuItemId { get; set; }
        public int CategoryId { get; set; }
        public List<IdNameDto> Items { get; set; }

        public CreateScreenMenuItemInput()
        {
            Items = new List<IdNameDto>();
        }

        public ScreenMenuItemEditDto MenuItem { get; set; }
        public string Files { get; set; }
        public Guid DownloadImage { get; set; }
    }

    [AutoMapFrom(typeof(ScreenMenuCategory), typeof(ScreenMenuCategoryEditDto))]
    public class ScreenMenuCategoryListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
        public bool MostUsedItemsCategory { get; set; }
        public string MenuItemButtonColor { get; set; }
        public string MainButtonColor { get; set; }
        public double MainFontSize { get; set; }
        public int ColumnCount { get; set; }
        public int MainButtonHeight { get; set; }
        public bool WrapText { get; set; }
        public int MenuItemButtonHeight { get; set; }
        public int SubButtonHeight { get; set; }
        public int SubButtonRows { get; set; }
        public string SubButtonColorDef { get; set; }
        public string ScreenMenuCategorySchedule { get; set; }
        public List<ScreenMenuCategoryScheduleEditDto> ScreenMenuCategorySchedules { get; set; }
    }

    public class GetScreenCategoryForEditOutput : IOutputDto
    {
        public ScreenMenuCategoryEditDto Category { get; set; }
    }

    public class CreateOrUpdateCategory : IInputDto
    {
        [Required]
        public ScreenMenuCategoryEditDto Category { get; set; }

        public bool CopyToAll { get; set; }
        public bool CopyColor { get; set; }
        public int MenuId { get; set; }
    }

    [AutoMapTo(typeof(ScreenMenuCategory))]
    public class ScreenMenuCategoryEditDto: CreationAuditedEntity
    {
        public ScreenMenuCategoryEditDto()
        {
            ScreenMenuCategorySchedules = new Collection<ScreenMenuCategoryScheduleEditDto>();
        }
        public int? Id { get; set; }
        public string Name { get; set; }
        public bool MostUsedItemsCategory { get; set; }
        public string ImagePath { get; set; }
        public int MainButtonHeight { get; set; }
        public int MenuItemButtonHeight { get; set; }
        public string MainButtonColor { get; set; }
        public double MainFontSize { get; set; }
        public int ColumnCount { get; set; }
        public int MaxItems { get; set; }
        public int PageCount { get; set; }
        public bool WrapText { get; set; }
        public int SubButtonHeight { get; set; }
        public int SubButtonRows { get; set; }
        public string SubButtonColorDef { get; set; }
        public int NumeratorType { get; set; }
        public string Files { get; set; }
        public virtual Guid? DownloadImage { get; set; }
        public bool RefreshImage { get; set; }
        public string ScreenMenuCategorySchedule { get; set; }
        public int FromHour { get; set; }
        public int FromMinute { get; set; }
        public int ToHour { get; set; }
        public int ToMinute { get; set; }
        public List<ScreenMenuItemEditDto> ScreenMenuItem { get; set; }
        public Collection<ScreenMenuCategoryScheduleEditDto> ScreenMenuCategorySchedules { get; set; }
        public void AddSchedule(int startHour, int startMinute, int endHour, int endMinute)
        {
            ScreenMenuCategorySchedules.Add(new ScreenMenuCategoryScheduleEditDto
            {
                StartHour = startHour,
                StartMinute = startMinute,
                EndHour = endHour,
                EndMinute = endMinute
            });
        }

    }

    [AutoMapFrom(typeof(ScreenMenuItem),typeof(ScreenMenuItemEditDto))]
    public class ScreenMenuItemListDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ButtonColor { get; set; }
        public bool AutoSelect { get; set; }
        public bool CategoryName { get; set; }
        public string SubMenuTag { get; set; }
        public double FontSize { get; set; }
        public bool ShowAliasName { get; set; }
        public string MenuItemAliasName { get; set; }
        public int ConfirmationType { get; set; }
        public string ConfirmationTypeName => ((ScreenMenuItemConfirmationTypes)ConfirmationType).ToString();
        public bool WeightedItem { get; set; }
    }

    public class GetScreenMenuItemBySubMenuOuput
    {
        public GetScreenMenuItemBySubMenuOuput()
        {
            MenuItems = new List<ScreenMenuItemListDto>();
            CategoryTags = new List<string>();
        }

        public List<ScreenMenuItemListDto> MenuItems { get; set; }
        public IEnumerable<string> CategoryTags { get; set; }
        //public string SubMenuTag { get; set; }
    }

    public class GetScreenMenuItemForEditOutput : IOutputDto
    {
        public ScreenMenuItemEditDto MenuItem { get; set; }
        public int MenuItemId { get; set; }
    }

    public class CreateOrUpdateScreenMenuItem : IInputDto
    {
        [Required]
        public ScreenMenuItemEditDto MenuItem { get; set; }

        public bool CopyToAll { get; set; }
        public int MenuItemId { get; set; }
        public int CategoryId { get; set; }
    }

    [AutoMapTo(typeof(ScreenMenuItem))]
    public class ScreenMenuItemEditDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public bool ShowAliasName { get; set; }
        public bool AutoSelect { get; set; }
        public string ButtonColor { get; set; }
        public double FontSize { get; set; }
        public string SubMenuTag { get; set; }
        public string OrderTags { get; set; }
        public string ItemPortion { get; set; }
        public Guid DownloadImage { get; set; }
        public bool RefreshImage { get; set; }
        public bool VoucherItem { get; set; }
        public int? ConnectCardTypeId { get; set; }
        public string Files { get; set; }
        public string ConfirmationType { get; set; }
        public bool WeightedItem { get; set; }
    }

    public class ApiMenu
    {
        public ApiMenu()
        {
            ScreenMenu = new ApiScreenMenuOutput();
            Menu = new List<ApiMenuItemOutput>();
        }

        public ApiScreenMenuOutput ScreenMenu { get; set; }
        public List<ApiMenuItemOutput> Menu { get; set; }
    }

    public class ApiScreenMenuOutput
    {
        public ApiScreenMenuOutput()
        {
            Menu = new List<ApiScreenMenu>();
        }

        public List<ApiScreenMenu> Menu { get; set; }
    }

    [AutoMapFrom(typeof(ScreenMenu))]
    public class ApiScreenMenu
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Departments { get; set; }
        public int CategoryColumnCount { get; set; }
        public int CategoryColumnWidthRate { get; set; }
        public virtual ICollection<ApiScreenCategory> Categories { get; set; }

        public ApiScreenMenu()
        {
            Categories = new List<ApiScreenCategory>();
        }
    }

    [AutoMapFrom(typeof(ScreenMenuCategory))]
    public class ApiScreenCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool MostUsedItemsCategory { get; set; }
        public string ImagePath { get; set; }
        public int MainButtonHeight { get; set; }
        public string MainButtonColor { get; set; }
        public double MainFontSize { get; set; }
        public int ColumnCount { get; set; }
        public int MenuItemButtonHeight { get; set; }
        public int MaxItems { get; set; }
        public int PageCount { get; set; }
        public bool WrapText { get; set; }
        public int SortOrder { get; set; }
        public int NumeratorType { get; set; }
        public int SubButtonHeight { get; set; }
        public int SubButtonRows { get; set; }

        public string ScreenMenuCategorySchedule { get; set; }

        public string SubButtonColorDef { get; set; }
        public virtual ICollection<ApiScreenMenuItem> MenuItems { get; set; }

        public string Files { get; set; }
        public virtual Guid DownloadImage { get; set; }
    }

    [AutoMapFrom(typeof(ScreenMenuItem))]
    public class ApiScreenMenuItem
    {
        public int Id { get; set; }
        public int MenuItemId { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public bool ShowAliasName { get; set; }
        public bool AutoSelect { get; set; }
        public string ButtonColor { get; set; }
        public double FontSize { get; set; }
        public string SubMenuTag { get; set; }
        public string OrderTags { get; set; }
        public string ItemPortion { get; set; }
        public int SortOrder { get; set; }
        public int ConfirmationType { get; set; }
        public bool WeightedItem { get; set; }

        public string Files { get; set; }
        public bool VoucherItem { get; set; }
        public int? ConnectCardTypeId { get; set; }

        public virtual Guid DownloadImage { get; set; }
    }

    public class GetScreenItemsOutput
    {
        public GetScreenItemsOutput()
        {
            Items = new List<IdNameDto>();
            SelectedItems = new List<IdNameDto>();
        }

        public List<IdNameDto> Items { get; set; }
        public List<IdNameDto> SelectedItems { get; set; }
    }
    
    public class ScreenMenuCategoryScheduleEditDto
    {
        private string _Days;
        private string _MonthDays;

        public ScreenMenuCategoryScheduleEditDto()
        {
            _Days = null;
            _MonthDays = null;
        }

        public int? Id { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int StartHour { get; set; }
        public int StartMinute { get; set; }
        public int EndHour { get; set; }
        public int EndMinute { get; set; }
        public List<ComboboxItemDto> AllDays { get; set; }
        public List<ComboboxItemDto> AllMonthDays { get; set; }

        public string Days
        {
            get
            {
                if (AllDays == null || !AllDays.Any())
                    return _Days;
                return string.Join(",", AllDays.Select(a => a.Value).ToArray());
            }
            set { _Days = value; }
        }

        public string MonthDays
        {
            get
            {
                if (AllMonthDays == null || !AllMonthDays.Any())
                {
                    return _MonthDays;
                }
                return string.Join(",", AllMonthDays.Select(a => a.Value).ToArray());
            }
            set { _MonthDays = value; }
        }
    }
}