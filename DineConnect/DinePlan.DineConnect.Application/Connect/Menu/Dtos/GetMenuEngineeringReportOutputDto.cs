﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DinePlan.DineConnect.Connect.Menu.Dtos
{
	public class GetMenuEngineeringReportOutputDto
	{
		public List<MenuEngineerReportListDto> ListItem { get; set; }
		public decimal TotalMenuCost => ListItem.Sum(x => x.MenuCost);
		public decimal TotalMenuRevenue => ListItem.Sum(x => x.MenuRevenue);
		public decimal TotalMenuCM => ListItem.Sum(x => x.MenuCM);
		public decimal TotalQuantity => ListItem.Sum(x => x.Quantity);
		public int CountMenuItem => ListItem.Count();

		public decimal MenuFoodCost => TotalMenuRevenue == 0 ? 0 : TotalMenuCost * 100 / TotalMenuRevenue;
		public decimal AverageCM
		{
			get
			{
				var result = 0M;
				if (ListItem.Count > 0)
				{
					result = ListItem.Average(x => x.ItemCM);
				}
				return result;
			}
		}
		public decimal PercentageMenuMix => CountMenuItem == 0 ? 0 : 1M / CountMenuItem * 0.7M * 100;
		public GetMenuEngineeringReportForDashboardDto Dashboard { get; set; }

		public GetMenuEngineeringReportOutputDto()
		{
			ListItem = new List<MenuEngineerReportListDto>();
		}

	}
	public class MenuEngineerReportListDto
	{
		public string LocationName { get; set; }
		public string GroupName { get; set; }
		public string CategoryName { get; set; }
		public string AliasCode { get; set; }
		public int CategoryId { get; set; }
		public int MenuItemId { get; set; }
		public string MenuItemName { get; set; }
		public int MenuItemPortionId { get; set; }
		public int GroupId { get; set; }
		public string MenuItemPortionName { get; set; }
		public decimal Quantity { get; set; }
		public decimal CostPrice { get; set; }
		public decimal TotalQuantity { get; set; }
		public decimal MenuMix
	    {
            get
            {
				if(TotalQuantity != 0 )
                {
					return (Quantity / TotalQuantity) * 100;
                }					
				else
                {
					return 0;
                }					

            }
        }
		public decimal Price { get; set; }
		public decimal ItemCM => Price - CostPrice;
		public decimal MenuCost => Quantity * CostPrice;
		public decimal MenuRevenue => Quantity * Price;
		public decimal MenuCM => Quantity * ItemCM;
		public decimal AverageCM { get; set; }
		public string MenuItemAndPortion => $"{MenuItemName} ( {MenuItemPortionName} )";
		public string CMCAT
		{
			get
			{
				if (ItemCM > AverageCM)
				{
					return ConnectConsts.ConnectConsts.Hight;
				}
				else
				{
					return ConnectConsts.ConnectConsts.Low;
				}
			}
		}
		public decimal PercentageMenuMix { get; set; }
		public string MMCAT
		{
			get
			{
				if (MenuMix > PercentageMenuMix)
				{
					return ConnectConsts.ConnectConsts.Hight;
				}
				else
				{
					return ConnectConsts.ConnectConsts.Low;
				}
			}
		}
		public string MenuItemClass
		{
			get
			{
				if (CMCAT == ConnectConsts.ConnectConsts.Hight && MMCAT == ConnectConsts.ConnectConsts.Hight)
				{
					return ConnectConsts.ConnectConsts.Star;
				}
				else if (CMCAT == ConnectConsts.ConnectConsts.Hight && MMCAT == ConnectConsts.ConnectConsts.Low)
				{
					return ConnectConsts.ConnectConsts.Puzzle;
				}
				else if (CMCAT == ConnectConsts.ConnectConsts.Low && MMCAT == ConnectConsts.ConnectConsts.Hight)
				{
					return ConnectConsts.ConnectConsts.Horse;
				}
				else
				{
					return ConnectConsts.ConnectConsts.Dog;
				}
			}
		}

	}

	public class GetMenuEngineeringReportForDashboardDto
	{
		public string Horse { get; set; }
		public string Puzzle { get; set; }
		public string Star { get; set; }
		public string Dog { get; set; }
	}
}
