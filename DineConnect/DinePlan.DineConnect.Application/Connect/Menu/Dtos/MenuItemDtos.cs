﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Addons.Dto;
using DinePlan.DineConnect.Connect.FutureData.Dtos;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace DinePlan.DineConnect.Connect.Menu.Dtos
{
    [AutoMapFrom(typeof(MenuItem))]
    public class MenuItemListDto : FullAuditedEntityDto
    {
        public string BarCode { get; set; }
        public string AliasCode { get; set; }
        public string AliasName { get; set; }
        public string ItemDescription { get; set; }
        public bool ForceQuantity { get; set; }
        public bool ForceChangePrice { get; set; }
        public int? CategoryId { get; set; }
        public string Name { get; set; }
        public string CategoryName { get; set; }
        public string CategoryProductGroupName { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryProductGroupCode { get; set; }

        public Collection<MenuItemPortionEditDto> Portions { get; set; }
        public string Locations { get; set; }
        public bool RestrictPromotion { get; set; }
        public string Tag { get; set; }
        public bool NoTax { get; set; }
        public int FoodType { get; set; }
        public string FoodTypeName => ((ProductFoodType)FoodType).ToString();
        public decimal Quantity { get; set; }

        public string FirstPortionPrice
        {
            get
            {
                if (Portions != null && Portions.Any())
                {
                    return Portions.First().Price.ToString("N2");
                }

                return "0.00";
            }
        }
    }

    public class MenuItemLocationPriceDto
    {
        public MenuItemLocationPriceDto()
        {
            MenuItemPortions = new List<MenuItemPortionDto>();
        }

        public int MenuItemId { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public List<MenuItemPortionDto> MenuItemPortions { get; set; }
    }

    public class MenuItemPortionDto
    {
        public int? Id { get; set; }
        public string MenuName { get; set; }
        public string PortionName { get; set; }
        public int MultiPlier { get; set; }
        public decimal Price { get; set; }
        public int MenuItemId { get; set; }
        public int LocationId { get; set; }
        public decimal LocationPrice { get; set; }
        public string DisplayName => MenuName + ":" + PortionName;
        public string LocationName { get; set; }
        public string AliasName { get; set; }
    }

    public class SimpleMenuOutput
    {
        public int Value { get; set; }
        public string DisplayText { get; set; }
        public string AlternateName { get; set; }
        public string AliasCode { get; set; }
    }

    [AutoMapTo(typeof(MenuItem))]
    public class MenuItemEditDto : ConnectEditDto
    {
        public MenuItemEditDto()
        {
            Portions = new Collection<MenuItemPortionEditDto>();
            Barcodes = new Collection<MenuBarCodeEditDto>();
            UpMenuItems = new Collection<UpMenuItemEditDto>();
            UpSingleMenuItems = new Collection<UpMenuItemEditDto>();
            MenuItemSchedules = new Collection<MenuItemScheduleEditDto>();
            MenuItemDescriptions = new Collection<MenuItemDescriptionEditDto>();
            FoodType = 4;
        }

        public int? Id { get; set; }
        public string BarCode { get; set; }

        public string AliasCode { get; set; }
        public string AliasName { get; set; }
        public string ItemDescription { get; set; }

        public bool ForceQuantity { get; set; }
        public bool ForceChangePrice { get; set; }
        public bool RestrictPromotion { get; set; }
        public int? CategoryId { get; set; }
        public bool NoTax { get; set; }

        public string CategoryCode { get; set; }
        public string CategoryName { get; set; }
        public string GroupCode { get; set; }
        public string GroupName { get; set; }

        // For Importing Template
        public string TPName { get; set; }

        public decimal TPPrice { get; set; }

        public int TPNumberOfPax { get; set; }
        public int? TransactionTypeId { get; set; }
        public string TransactionTypeName { get; set; }
        public TransactionType TransactionType { get; set; }

        public string Name { get; set; }
        public int ProductType { get; set; }

        public string Tag { get; set; }
        public string Files { get; set; }
        public Guid DownloadImage { get; set; }

        public string HsnCode { get; set; }

        public Collection<MenuBarCodeEditDto> Barcodes { get; set; }
        public Collection<MenuItemPortionEditDto> Portions { get; set; }
        public Collection<UpMenuItemEditDto> UpMenuItems { get; set; }
        public Collection<UpMenuItemEditDto> UpSingleMenuItems { get; set; }
        public Collection<MenuItemScheduleEditDto> MenuItemSchedules { get; set; }
        public Collection<MenuItemDescriptionEditDto> MenuItemDescriptions { get; set; }

        public int Oid { get; set; }
        public string Uom { get; set; }
        public int RowIndex { get; set; }
        public int FoodType { get; set; }
        public string LocationName { get; set; }
        public string PortionName { get; set; }

        public void AddPortion(string itemName, decimal price)
        {
            Portions.Add(new MenuItemPortionEditDto
            {
                Name = itemName,
                Price = price,
                MultiPlier = 1,
                Barcode = ""
            });
        }

        public string AddOns { get; set; }

        public CategoryOutputEditDto Category { get; set; }
    }

    [AutoMap(typeof(UpMenuItem))]
    public class UpMenuItemEditDto
    {
        public int? Id { get; set; }
        public int MenuItemId { get; set; }
        public string Name { get; set; }
        public int RefMenuItemId { get; set; }
        public bool AddBaseProductPrice { get; set; }
        public decimal Price { get; set; }
        public int ProductType { get; set; }
        public int MinimumQuantity { get; set; }
        public int MaxQty { get; set; }
        public bool AddAuto { get; set; }
        public int AddQuantity { get; set; }
    }

    [AutoMap(typeof(MenuItemSchedule))]
    public class MenuItemScheduleEditDto
    {
        private string _Days;

        public MenuItemScheduleEditDto()
        {
            _Days = null;
        }

        public int? Id { get; set; }
        public int StartHour { get; set; }
        public int StartMinute { get; set; }
        public int EndHour { get; set; }
        public int EndMinute { get; set; }

        public string Days
        {
            get
            {
                if (AllDays == null || !AllDays.Any())
                    return _Days;
                return string.Join(",", AllDays.Select(a => a.Value).ToArray());
            }
            set { _Days = value; }
        }

        public List<ComboboxItemDto> AllDays { get; set; }
    }

    public class MenuitemImportDto
    {
        public MenuitemImportDto()
        {
            Portions = new Collection<MenuItemPortionEditDto>();
        }

        public int? Id { get; set; }
        public string BarCode { get; set; }

        public string AliasCode { get; set; }
        public string AliasName { get; set; }
        public string ItemDescription { get; set; }
        public bool ForceQuantity { get; set; }
        public bool ForceChangePrice { get; set; }
        public bool RestrictPromotion { get; set; }
        public bool NoTax { get; set; }

        public string CategoryName { get; set; }
        public string GroupName { get; set; }

        // For Importing Template
        public string TPName { get; set; }

        public decimal TPPrice { get; set; }

        public int? CategoryId { get; set; }
        public string Name { get; set; }
        public int ProductType { get; set; }
        public string Location { get; set; }
        public string Tag { get; set; }

        public int RowIndex { get; set; }

        public Collection<MenuItemPortionEditDto> Portions { get; set; }

        public void AddPortion(string itemName, decimal price,int preparation, int? numberOfPax)
        {
            Portions.Add(new MenuItemPortionEditDto
            {
                Name = itemName,
                Price = price,
                MultiPlier = 1,
                Barcode = "",
                PreparationTime=preparation,
                NumberOfPax = numberOfPax
            });
        }
    }

    [AutoMap(typeof(MenuBarCode))]
    public class MenuBarCodeEditDto
    {
        public int? Id { get; set; }
        public string Barcode { get; set; }
    }

    [AutoMap(typeof(MenuItemPortion))]
    public class MenuItemPortionEditDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int MultiPlier { get; set; }
        public decimal Price { get; set; }
        public decimal CostPrice { get; set; }
        public string Barcode { get; set; }
        public int MenuItemId { get; set; }
        public bool ChangePrice { get; set; }
        public int? PreparationTime { get; set; }
        public string AliasName { get; set; }

        public int? NumberOfPax { get; set; }
    }

    public class GetMenuItemInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public string CategoryName { get; set; }
        public int MenuItemType { get; set; }
        public int ReferenceId { get; set; }
        public bool IsDeleted { get; set; }
        public int LocationId { get; set; }
        public int CategoryId { get; set; }
        public int MenuItemId { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }

        public GetMenuItemInput()
        {
            LocationGroup = new LocationGroupDto();
        }

        public LocationGroupDto LocationGroup { get; set; }

    }

    public class GetMenuItemLocationInput : IInputDto, IPagedResultRequest, ISortedResultRequest, IShouldNormalize
    {
        public int PortionId { get; set; }

        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }

        public int LocationId { get; set; }

        [Range(0, int.MaxValue)]
        public int MaxResultCount { get; set; } //0: Unlimited.

        [Range(0, int.MaxValue)]
        public int SkipCount { get; set; }

        public string Sorting { get; set; }
    }

    public class GetMenuPortionPriceInput : IInputDto, IPagedResultRequest, ISortedResultRequest, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "MenuName";
            }
        }

        public int LocationId { get; set; }
        public int? MenuItemId { get; set; }

        [Range(0, int.MaxValue)]
        public int MaxResultCount { get; set; } //0: Unlimited.

        [Range(0, int.MaxValue)]
        public int SkipCount { get; set; }

        public string Sorting { get; set; }
    }

    public class LocationPriceInput : IInputDto
    {
        [Required]
        public int LocationId { get; set; }

        public decimal Price { get; set; }

        [Required]
        public int PortionId { get; set; }
    }

    public class LocationPriceOutput
    {
        public int MenuPortionId { get; set; }
        public decimal Price { get; set; }
        public string PortionName { get; set; }
        public string LocationName { get; set; }
        public int LocationId { get; set; }
    }

    public class GetMenuItemForEditOutput : IOutputDto
    {
        public GetMenuItemForEditOutput()
        {
            Combo = new List<ProductComboGroupEditDto>();
            LocationGroup = new LocationGroupDto();
        }

        public MenuItemEditDto MenuItem { get; set; }
        public List<ProductComboGroupEditDto> Combo { get; set; }
        public int ProductType { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public UrbanPiperMenuAddOn UrbanPiper { get; set; }
    }

    public class CreateOrUpdateMenuItemInput : IInputDto
    {
        [Required]
        public MenuItemEditDto MenuItem { get; set; }

        public int ProductType { get; set; }
        public List<ProductComboGroupEditDto> Combo { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public bool RefreshImage { get; set; }
        public bool NotOverride { get; set; }
        public bool UpdateItem { get; set; }

        public List<MenuItemLocationPriceDto> MenuItemLocationPrices { get; set; }

        public CreateOrUpdateMenuItemInput()
        {
            LocationGroup = new LocationGroupDto();
            Combo = new List<ProductComboGroupEditDto>();
            MenuItem = new MenuItemEditDto();
            MenuItemLocationPrices = new List<MenuItemLocationPriceDto>();
        }

        public UrbanPiperMenuAddOn UrbanPiper { get; set; }
    }

    public class MenuItemLocationInput : IInputDto
    {
        public int LocationId { get; set; }
        public int MenuItemId { get; set; }

        public List<int> MenuItems
        {
            get;
            set;
        }

        public bool DeleteMenuItems { get; set; }
    }

    [AutoMapFrom(typeof(LocationMenuItem))]
    public class LocationMenuItemListDto : EntityDto<int>
    {
        public string MenuItemName { get; set; }
        public int MenuItemId { get; set; }
    }

    public class ApiMenuOutput
    {
        public ApiMenuOutput()
        {
            Categories = new List<ApiCategoryOutput>();
            OrderTagGroups = new List<ApiOrderTagGroupOutput>();
        }

        public IList<ApiCategoryOutput> Categories { get; set; }
        public IList<ApiOrderTagGroupOutput> OrderTagGroups { get; set; }
    }
    public class ApiLocationListOutput
    {
        public ApiLocationListOutput()
        {
            LocationMenus = new List<ApiLocationMenuOutput>();
        }

        public List<ApiLocationMenuOutput> LocationMenus { get; set; }
    }

    public class ApiLocationMenuItemListOutput
    {
        public int LocationId { get; set; }

        public ApiLocationMenuItemListOutput()
        {
            Menus = new List<ApiLocationMenuItemPriceOutput>();
        }

        public List<ApiLocationMenuItemPriceOutput> Menus { get; set; }
    }

    public class ApiLocationMenuItemPriceOutput
    {
        public string Name { get; set; }
        public string AliasCode { get; set; }
        public string AliasName { get; set; }
        public decimal Price { get; set; }
        public int PortionId { get; set; }
        public int MenuItemId { get; set; }
        public string PortionName { get; set; }
        public string UserString => $"{Name}-{PortionName}";

        public List<TaxForMenu> TaxForMenu { get; set; }

        public List<ApplicableTaxesForMenu> ApplicableTaxes { get; set; }
    }

    public class TaxForMenu
    {
        public int MenuItemId { get; set; }
        public int PortionId { get; set; }
        public int DinePlanTaxRefId { get; set; }
        public string DinePlanTaxName { get; set; }
        public decimal DinePlanTaxRate { get; set; }
        public decimal DinePlanTaxValue { get; set; }
        public int SortOrder { get; set; }
        public int Rounding { get; set; }
        public string TaxCalculationMethod { get; set; }
    }

    public class ApplicableTaxesForMenu
    {
        public int DinePlanTaxRefId { get; set; }
        public string DinePlanTaxName { get; set; }
        public decimal DinePlanTaxRate { get; set; }
        public int SortOrder { get; set; }
        public int Rounding { get; set; }
        public string TaxCalculationMethod { get; set; }
    }

    public class ApiLocationMenuOutput
    {
        public ApiLocationMenuOutput()
        {
            Categories = new List<ApiCategoryOutput>();
            OrderTagGroups = new List<ApiOrderTagGroupOutput>();
        }

        public int LocationId { get; set; }
        public IList<ApiCategoryOutput> Categories { get; set; }
        public IList<ApiOrderTagGroupOutput> OrderTagGroups { get; set; }
    }

    public class ApiCategoryOutput
    {
        public ApiCategoryOutput()
        {
            MenuItems = new List<ApiMenuItemOutput>();
            OrderTagGroups = new List<ApiOrderTagGroupOutput>();
            ProductGroup = new ApiProductGroup();
        }
        public int? Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public IList<ApiOrderTagGroupOutput> OrderTagGroups { get; set; }
        public IList<ApiMenuItemOutput> MenuItems { get; set; }
        public int SortOrder { get; set; }
        public string ImagePath { get; set; }

        public ApiProductGroup ProductGroup { get; set; }

        public int? ProductGroupId { get; set; }
    }

    public class ApiProductGroup
    {
        public ApiProductGroup()
        {
        }
        public virtual int Id { get; set; }

        public virtual string Code { get; set; }

        [MaxLength(50)]
        public virtual string Name { get; set; }

        public virtual ApiProductGroup Parent { get; set; }
        public virtual int? ParentId { get; set; }
    }

    public class ApiOrderTagGroupOutput
    {
        public ApiOrderTagGroupOutput()
        {
            OrderTags = new List<ApiOrderTagOutput>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public int MinSelectItems { get; set; }
        public int MaxSelectItems { get; set; }
        public IList<ApiOrderTagOutput> OrderTags { get; set; }
        public bool AddPriceToOrder { get; set; }
        public bool SaveFreeTags { get; set; }
        public bool FreeTagging { get; set; }
        public bool TaxFree { get; set; }

        public int SortOrder { get; set; }
    }

    public class ApiOrderTagOutput
    {
        public int SortOrder { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
        public decimal Price { get; set; }
        public int MenuItemId { get; set; }
    }

    public class ApiMenuItemImageOutput
    {
        public ApiMenuItemImageOutput()
        {
            ImagesList = new List<ApiMenuItemImage>();
        }

        public IList<ApiMenuItemImage> ImagesList { get; set; }
    }

    public class ApiMenuItemImage
    {
        public int Id { get; set; }
        public Guid DownloadImage { get; set; }
        public string Files { get; set; }
    }

    public class ApiMenuItemOutput
    {
        public ApiMenuItemOutput()
        {
            MenuPortions = new List<ApiMenuItemPortionOutput>();
            OrderTagGroups = new List<ApiOrderTagGroupOutput>();
            Barcodes = new List<ApiMenuBarcodeOutput>();
            UpMenuItems = new List<ApiUpMenuItemOutput>();
            MenuItemSchedules = new List<ApiMenuItemSchedule>();
            MenuItemDescriptions = new List<ApiMenuItemDescription>();
        }

        public int Id { get; set; }
        public int ProductType { get; set; }

        public string Name { get; set; }
        public string AliasCode { get; set; }
        public string AliasName { get; set; }
        public string ImagePath { get; set; }
        public string Description { get; set; }
        public bool IsFavorite { get; set; }
        public IList<ApiMenuItemPortionOutput> MenuPortions { get; set; }
        public IList<ApiMenuBarcodeOutput> Barcodes { get; set; }
        public IList<ApiUpMenuItemOutput> UpMenuItems { get; set; }
        public IList<ApiMenuItemSchedule> MenuItemSchedules { get; set; }
        public IList<ApiMenuItemDescription> MenuItemDescriptions { get; set; }

        public int SortOrder { get; set; }
        public IList<ApiOrderTagGroupOutput> OrderTagGroups { get; set; }
        public string BarCode { get; set; }
        public string Files { get; set; }

        public bool ForceQuantity { get; set; }
        public bool RestrictPromotion { get; set; }
        public bool NoTax { get; set; }

        public bool ForceChangePrice { get; set; }
        public string Tag { get; set; }
        public int? TransactionTypeId { get; set; }
        public string TransactionTypeName { get; set; }
        public ApiProductComboOutput Combo { get; set; }
        public Guid DownloadImage { get; set; }
        public string Uom { get; set; }
        public int LocationId { get; set; }
    }

    [AutoMap(typeof(ProductCombo))]
    public class ApiProductComboOutput
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int MenuItemId { get; set; }

        public ApiProductComboOutput()
        {
            ComboGroups = new Collection<ApiProductComboGroupOutput>();
        }

        public Collection<ApiProductComboGroupOutput> ComboGroups { get; set; }
        public string Sorting { get; set; }

    }

    [AutoMap(typeof(MenuBarCode))]
    public class ApiMenuBarcodeOutput
    {
        public int Id { get; set; }
        public string Barcode { get; set; }
    }

    [AutoMap(typeof(UpMenuItem))]
    public class ApiUpMenuItemOutput
    {
        public int Id { get; set; }
        public int MenuItemId { get; set; }
        public bool AddBaseProductPrice { get; set; }
        public int RefMenuItemId { get; set; }
        public decimal Price { get; set; }
        public int MinimumQuantity { get; set; }
        public int MaxQty { get; set; }
        public bool AddAuto { get; set; }
        public int AddQuantity { get; set; }
        public int ProductType { get; set; }
    }

    [AutoMap(typeof(MenuItemSchedule))]
    public class ApiMenuItemSchedule
    {
        public int Id { get; set; }
        public int StartHour { get; set; }
        public int StartMinute { get; set; }
        public int EndHour { get; set; }
        public int EndMinute { get; set; }
        public string Days { get; set; }
        public int MenuItemId { get; set; }
    }

    [AutoMap(typeof(MenuItemDescription))]
    public class ApiMenuItemDescription
    {
        public string LanguageCode { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public int MenuItemId { get; set; }
    }

    [AutoMap(typeof(ProductComboGroup))]
    public class ApiProductComboGroupOutput
    {
        public ApiProductComboGroupOutput()
        {
            ComboItems = new Collection<ApiProductComboItemOutput>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public int Minimum { get; set; }
        public int Maximum { get; set; }

        public Collection<ApiProductComboItemOutput> ComboItems { get; set; }
    }

    [AutoMap(typeof(ProductComboItem))]
    public class ApiProductComboItemOutput
    {
        public ApiProductComboItemOutput()
        {
            Count = 1;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int MenuItemId { get; set; }
        public int MenuItemPortionId { get; set; }

        public bool AutoSelect { get; set; }
        public decimal Price { get; set; }
        public int SortOrder { get; set; }
        public int Count { get; set; }
        public bool AddSeperately { get; set; }
        public string GroupTag { get; set; }
        public string ButtonColor { get; set; }
        public string Files { get; set; }
        public Guid DownloadImage { get; set; }
    }

    public class ApiMenuItemPortionOutput
    {
        public int Id { get; set; }
        public string PortionName { get; set; }
        public decimal Price { get; set; }
        public int Multiplier { get; set; }
        public string Barcode { get; set; }
        public bool ChangePrice { get; set; }
        public int? PreparationTime { get; set; }
        public string AliasName { get; set; }

        public int? NumberOfPax { get; set; }
    }

    [AutoMap(typeof(MenuItemDescription))]
    public class MenuItemDescriptionEditDto
    {
        public int? Id { get; set; }
        public int MenuItemId { get; set; }
        public string LanguageCode { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public string MenuItemName { get; set; }
        public string LangugeName { get; set; }
    }

 
    public class GetMenuItemDescriptionExportInput
    {
        public int MenuItemId { get; set; }

        public bool IsExport { get; set; }
    }
    public class MenuItemSortingOutputDto
    {
        public int[] combogroupItems { get; set; }
        public int MenuItemId { get; set; }
    }

    public class GetUpMenuItemLocationPriceForEditOutput
    {
        public UpMenuItemLocationPriceEditDto UpMenuItemLocationPrice { get; set; }
    }
    [AutoMapTo(typeof(UpMenuItemLocationPrice))]
    public class UpMenuItemLocationPriceEditDto : CreationAuditedEntity
    {

        public int? Id { get; set; }
        public int UpMenuItemId { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public decimal Price { get; set; }
    }
    public class CreateUpMenuItemLocationPrice : IInputDto
    {
        [Required] public UpMenuItemLocationPriceEditDto UpMenuItemLocationPrice { get; set; }
    }
    [AutoMapTo(typeof(UpMenuItemLocationPrice))]
    public class UpMenuItemLocationPriceListDto : CreationAuditedEntity
    {
        public int UpMenuItemId { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public decimal Price { get; set; }
    }
    public class GetUpMenuItemLocationPriceInput : IInputDto, IPagedResultRequest, ISortedResultRequest, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }

        public int TagId { get; set; }
        public int UpMenuItemId { get; set; }
        public int MaxResultCount { get; set; }
        public int SkipCount { get; set; }
        public string Sorting { get; set; }
        public string Location { get; set; }

    }
}