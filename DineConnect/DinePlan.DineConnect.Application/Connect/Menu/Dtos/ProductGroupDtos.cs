﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Menu.Dtos
{
    [AutoMapFrom(typeof(ProductGroup))]
    public class ProductGroupListDto : FullAuditedEntityDto
    {
        public ProductGroupListDto()
        {
            Categories = new HashSet<CategoryListDto>();
        }
        public virtual string Code { get; set; }
        public virtual string Name { get; set; }
        public int TenantId { get; set; }
        public int Oid { get; set; }
        public int? ParentId { get; set; }
        public int MemberCount { get; set; }
        public ICollection<CategoryListDto> Categories { get; set; }
    }

    [AutoMapTo(typeof(ProductGroup))]
    public class ProductGroupEditDto
    {
        public int? Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int Oid { get; set; }
    }

    public class GetProductGroupInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }

    public class GetProductGroupForEditOutput : IOutputDto
    {
        public ProductGroupEditDto ProductGroup { get; set; }
    }

 
    public class CreateProductGroupInput : IInputDto
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int? ParentId { get; set; }
    }
    public class UpdateProductGroupInput : IInputDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
    public class MoveProductGroupInput : IInputDto
    {
        public int Id { get; set; }

        public int? NewParentId { get; set; }
    }
    public class CreateOrUpdateProductGroupInput : IInputDto
    {
        [Required]
        public ProductGroupEditDto ProductGroup { get; set; }
    }
    public class GeProductGroupCategoriesInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public long Id { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }
    public class UpdateProductgroupCategoryInput : IInputDto
    {
       public int ProductGroupId { get; set; }
        public List<int> CategoryIds { get; set; }
    }

    public class GetLinkedProductGroupIdsDto
    {
        public int LinkedId { get; set; }
        public List<int> LinkedIds { get; set; }
    }

    public class LinkedListDto
    {
        public int Id { get; set; }
        public bool CheckedFlag { get; set; }
    }
}