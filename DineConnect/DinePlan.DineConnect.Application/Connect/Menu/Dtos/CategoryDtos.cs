﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Dto;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Menu.Dtos
{
    [AutoMapFrom(typeof(Category))]
    public class CategoryListDto : FullAuditedEntityDto
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int? ProductGroupId { get; set; }
        public string GroupName { get; set; }
    }

    [AutoMapTo(typeof(Category))]
    public class CategoryEditDto
    {
        public int? Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int Oid { get; set; }
        public int? ProductGroupId { get; set; }
        public string GroupCode { get; set; }
        public string GroupName { get; set; }
        public Collection<LocationCategoryShareDto> Locations { get; set; }

        public void AddLocation(int locationId, int categoryId, decimal percentage, bool tax)
        {
            Locations.Add(new LocationCategoryShareDto
            {
                LocationId = locationId,
                CategoryId = categoryId,
                SharingPercentage = percentage,
                InclusiveOfTax = tax
            });
        }
    }

    [AutoMapTo(typeof(Category))]
    public class CategoryOutputEditDto
    {
        public int? Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int? ProductGroupId { get; set; }
        public ProductGroupOutputEditDto ProductGroup { get; set; }
    }

    [AutoMapTo(typeof(ProductGroup))]
    public class ProductGroupOutputEditDto
    {
        public int? Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public virtual ProductGroupOutputEditDto Parent { get; set; }
        public virtual int? ParentId { get; set; }
    }

    [AutoMapTo(typeof(LocationCategoryShare))]
    public class LocationCategoryShareDto
    {
        public int? Id { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }

        public int CategoryId { get; set; }

        public decimal SharingPercentage { get; set; }
        public bool InclusiveOfTax { get; set; }
    }

    public class GetCategoryInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public bool IsDeleted { get; set; }
        public int ProductGroupId { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }

    public class GetCategoryForEditOutput : IOutputDto
    {
        public CategoryEditDto Category { get; set; }
    }

    public class CreateOrUpdateCategoryInput : IInputDto
    {
        [Required]
        public CategoryEditDto Category { get; set; }
    }
}