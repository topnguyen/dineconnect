﻿using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Filter;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace DinePlan.DineConnect.Connect.Menu.Dtos
{
    public class GetProductComboGroupIntput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public List<int> ComboGroupSelectionIds { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime Desc";
            }
        }
    }

    [AutoMapTo(typeof(ProductComboGroup))]
    public class ProductComboGroupEditDto:ConnectMultiTenantEntity
    {
        public ProductComboGroupEditDto()
        {
            ComboItems = new List<ProductComboItemEditDto>();
            CustomSortOrder = -1;
        }

        public int? Id { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
        public int SortOrder { get; set; }
        public int Minimum { get; set; }
        public int Maximum { get; set; }
        public int CustomSortOrder { get; set; }

        public List<ProductComboItemEditDto> ComboItems { get; set; }
        public List<ProductComboEditDto> ProductCombos { get; set; }
        public List<MenuItemListDto> MenuItems { get; set; }
    }
    public class MenuItemComboGroup
    {
        public string Name { get; set; }
        public string Color { get; set; }
    }
    [AutoMapTo(typeof(ProductComboItem))]
    public class ProductComboItemEditDto
    {
        public ProductComboItemEditDto()
        {
            Count = 1;
        }

        public int? Id { get; set; }
        public string Name { get; set; }
        public int MenuItemId { get; set; }
        public int MenuItemPortionId { get; set; }
        public bool AutoSelect { get; set; }
        public decimal Price { get; set; }
        public int SortOrder { get; set; }
        public int Count { get; set; }
        public bool AddSeperately { get; set; }
        public bool IsSelected { get; set; }
        public bool IsOrdered { get; set; }
        public bool IsRemoved { get; set; }
        public string GroupTag { get; set; }
        public string ButtonColor { get; set; }
        public string Files { get; set; }
        public virtual Guid DownloadImage { get; set; }
    }
    [AutoMapTo(typeof(ProductCombo))]
    public class ProductComboEditDto
    {
        public int? Id { get; set; }
        public int MenuItemId { get; set; }
        public string Name { get; set; }
        public bool AddPriceToOrderPrice { get; set; }
        public string Sorting { get; set; }
    }
}