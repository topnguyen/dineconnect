﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Menu.Implementation
{
    public class ComboGroupAppService : DineConnectAppServiceBase, IComboGroupAppService
    {
        private readonly IRepository<ProductComboGroup> _comboGroupRepository;
        private readonly IRepository<ProductComboItem> _comboItemRepository;
        private readonly IRepository<ProductCombo> _comboRepository;
        private readonly IRepository<MenuItemPortion> _menuItemPortionManager;
        private readonly IMenuItemAppService _menuItemManager;
        public ComboGroupAppService(IRepository<ProductComboGroup> comboGroupRepository
            , IRepository<ProductComboItem> comboItemRepository
            , IRepository<ProductCombo> comboRepository
            , IRepository<MenuItemPortion> menuItemPortionManager
            , IMenuItemAppService menuItemManager
            )
        {
            _comboGroupRepository = comboGroupRepository;
            _comboItemRepository = comboItemRepository;
            _comboRepository = comboRepository;
            _menuItemPortionManager = menuItemPortionManager;
            _menuItemManager = menuItemManager;
        }

        public async Task<PagedResultOutput<ProductComboGroupEditDto>> GetAllComboGroups(GetProductComboGroupIntput input)
        {
            var allItems = _comboGroupRepository.GetAll();
            var allMenuItems = await _menuItemManager.GetAll(new GetMenuItemInput { MaxResultCount = 10000,IsDeleted = false});
            var allMenu = allMenuItems.Items;
            if (input.ComboGroupSelectionIds != null&&input.ComboGroupSelectionIds.Count>0)
            {
                allItems = allItems.Where(t => input.ComboGroupSelectionIds.Contains(t.Id));
            }
            var list = allItems
                .WhereIf(!input.Filter.IsNullOrEmpty(), c => c.Name.Contains(input.Filter));

            var pagedList = await list.OrderBy(input.Sorting).PageBy(input).ToListAsync();
            var resultItem = pagedList.MapTo<List<ProductComboGroupEditDto>>();
            foreach(var item in resultItem)
            {
                var comboGroupEditDto = item.ComboItems.OrderBy(s=>s.SortOrder).ToList();
                var productCombo = item.ProductCombos.OrderBy(s => s.Id).ToList();
                item.ComboItems = comboGroupEditDto;
                var listMenu = new List<MenuItemListDto>();
                foreach (var pro in productCombo)
                {
                    var findMenuItem = allMenu.FirstOrDefault(s => s.Id == pro.MenuItemId);
                    if (findMenuItem != null)
                    {
                        listMenu.Add(findMenuItem);
                    }
                }
                item.MenuItems = listMenu;
            }
            return new PagedResultOutput<ProductComboGroupEditDto>(await list.CountAsync(), resultItem);
        }

        public async Task<ProductComboGroupEditDto> GetComboGroupForEdit(NullableIdInput input)
        {
            var result = new ProductComboGroupEditDto();
            if (input.Id.HasValue)
            {
                var comboGroup = await _comboGroupRepository.GetAsync(input.Id.Value);

                result = comboGroup.MapTo<ProductComboGroupEditDto>();
            }

            return result;
        }

        public async Task CreateOrUpdateComboGroup(ProductComboGroupEditDto input)
        {
            ValidateForDuplicateName(input);
            if (input.Id.HasValue)
            {
                await UpdateComboGroup(input);
            }
            else
            {
                await CreateComboGroup(input);
            }
        }

        public async Task Delete(IdInput input)
        {
            await _comboGroupRepository.DeleteAsync(input.Id);
        }

        public async Task CreateComboGroup(ProductComboGroupEditDto input)
        {           
            foreach (var pdi in input.ComboItems)
            {
                if (pdi.MenuItemPortionId > 0)
                {
                    var myPortion = _menuItemPortionManager.Get(pdi.MenuItemPortionId);
                    if (myPortion != null)
                    {
                        pdi.MenuItemId = myPortion.MenuItemId;
                        pdi.MenuItemPortionId = myPortion.Id;
                    }
                }
                if(!string.IsNullOrEmpty(input.Color))
                {
                    pdi.ButtonColor = input.Color;
                }
            }

            var comboGroup = input.MapTo<ProductComboGroup>();

            await _comboGroupRepository.InsertAsync(comboGroup);
        }

        public async Task UpdateComboGroup(ProductComboGroupEditDto input)
        {
            var comboGroup = await _comboGroupRepository.GetAsync(input.Id.Value);

            comboGroup.Name = input.Name;
            comboGroup.SortOrder = input.SortOrder;
            comboGroup.Maximum = input.Maximum;
            comboGroup.Minimum = input.Minimum;
            comboGroup.Color = input.Color;
            if(input.ComboItems!=null && input.ComboItems.Count>0)
            {
                var checkColor = !string.IsNullOrEmpty(input.Color);
                foreach (var cIdItem in input.ComboItems.Where(a => a.Id != null && a.Id > 0))
                {
                    var ci = _comboItemRepository.Get(cIdItem.Id.Value);
                    ci.Name = cIdItem.Name;
                    ci.AutoSelect = cIdItem.AutoSelect;
                    ci.MenuItemId = cIdItem.MenuItemId;
                    ci.MenuItemPortionId = cIdItem.MenuItemPortionId;
                    ci.SortOrder = cIdItem.SortOrder;
                    ci.AddSeperately = cIdItem.AddSeperately;
                    ci.ButtonColor = checkColor? input.Color: cIdItem.ButtonColor;
                    ci.GroupTag = cIdItem.GroupTag;
                    ci.Price = cIdItem.Price;
                    ci.Count = cIdItem.Count == 0 && cIdItem.AutoSelect ? 1 : cIdItem.Count;
                    ci.Files = cIdItem.Files;
                    ci.DownloadImage= Guid.NewGuid();
                    await _comboItemRepository.UpdateAsync(ci);
                }
            }
            
            var cItems = input.ComboItems.Where(a => a.Id == null || a.Id == 0).ToList();
            foreach (var ci in cItems) comboGroup.ComboItems.Add(ci.MapTo<ProductComboItem>());
            await _comboGroupRepository.UpdateAsync(comboGroup);
            var combiIIds =
                input.ComboItems.Where(a => a.Id != null && a.Id > 0).Select(a => a.Id.Value).ToList();
            
            var removeIds =
                comboGroup.ComboItems.Where(a => a.Id != 0 && !combiIIds.Contains(a.Id))
                    .Select(a => a.Id)
                    .ToList();
            foreach (var i in removeIds) await _comboItemRepository.DeleteAsync(i);
        }

        public void ValidateForDuplicateName(ProductComboGroupEditDto input)
        {
            var exists = _comboGroupRepository.GetAll().Where(t => t.Name.Equals(input.Name)).WhereIf(input.Id.HasValue, t => t.Id != input.Id);
            if (exists.Any())
                throw new UserFriendlyException(string.Format(L("DuplicateExists_f"), L("Group"), L("Combo")));
        }
        public async Task<ProductComboItemEditDto> GetComboGroupItemForEdit(NullableIdInput input)
        {
            var result = new ProductComboItemEditDto();
            if (input.Id.HasValue)
            {
                var comboGroupItem = await _comboItemRepository.GetAsync(input.Id.Value);

                result = comboGroupItem.MapTo<ProductComboItemEditDto>();
            }

            return result;
        }
    }
}