﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using Castle.Core.Internal;
using DinePlan.DineConnect.Connect.Future;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations.Dtos;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.General;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Utility.Exporter;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Connect.Menu.Implementation
{
    public class ScreenMenuAppService : DineConnectAppServiceBase, IScreenMenuAppService
    {
        private readonly IRepository<Category> _catRepo;
        private readonly IExcelExporter _exporter;
        private readonly ILocationAppService _locAppService;
        private readonly IRepository<Master.Location> _lRepo;
        private readonly IRepository<MenuItem> _mRepo;
        private readonly IMenuItemAppService _mService;
        private readonly IRepository<ScreenMenuCategory> _screenCatRepo;
        private readonly IRepository<ScreenMenuItem> _screenItemRepo;
        private readonly IScreenMenuManager _screenmenuManager;
        private readonly IRepository<ScreenMenu> _screenmenuRepo;
        private readonly ISyncAppService _syncAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IScreenMenuExporter _screenMenuExporter;
        private readonly IRepository<FutureDateInformation> _futureDateRepository;

        public ScreenMenuAppService(IScreenMenuManager screenmenuManager,
            IRepository<ScreenMenu> screenMenuRepo, IRepository<Category> catRepo, IRepository<MenuItem> menuItem,
            IRepository<ScreenMenuCategory> smc, IRepository<Master.Location> lRepo, ILocationAppService locAppService,
            IRepository<ScreenMenuItem> sm,
            IExcelExporter exporter,
            ISyncAppService syncAppService,
            IMenuItemAppService mService,
            IUnitOfWorkManager unitOfWorkManager,
            IScreenMenuExporter screenMenuExporter,
            IRepository<FutureDateInformation> futureDateRepository
            )
        {
            _screenmenuManager = screenmenuManager;
            _screenmenuRepo = screenMenuRepo;
            _screenCatRepo = smc;
            _screenItemRepo = sm;
            _catRepo = catRepo;
            _lRepo = lRepo;
            _locAppService = locAppService;
            _mService = mService;
            _unitOfWorkManager = unitOfWorkManager;
            _mRepo = menuItem;
            _exporter = exporter;
            _syncAppService = syncAppService;
            _screenMenuExporter = screenMenuExporter;
            _futureDateRepository = futureDateRepository;
        }

        public async Task<PagedResultOutput<ScreenMenuListDto>> GetAll(GetScreenInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var allItems = _screenmenuRepo.GetAll()
                    .Where(i => i.IsDeleted == input.IsDeleted)
                    .WhereIf(
                            !input.Filter.IsNullOrWhiteSpace(),
                            p => p.Name.Contains(input.Filter)
                        );

                var sortMenuItems = allItems
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList();
                
                var allItemCount = allItems.Count();

                if(input.LocationGroup!=null && (input.LocationGroup.Locations.Any() || input.LocationGroup.LocationTags.Any() || input.LocationGroup.Groups.Any()|| input.LocationGroup.NonLocations.Any()))
                {
                    var allMyEnItems = SearchLocation(allItems, input.LocationGroup).OfType<ScreenMenu>();
                    allItemCount = allMyEnItems.Count();
                    sortMenuItems = allMyEnItems.AsQueryable()
                        .OrderBy(input.Sorting)
                        .PageBy(input)
                        .ToList();
                }
         
                var allListDtos = sortMenuItems.MapTo<List<ScreenMenuListDto>>();

                foreach (var ltd in allListDtos)
                {
                    if (!string.IsNullOrEmpty(ltd.Locations))
                    {
                        if (!ltd.Group)
                        {
                            var myL = JsonConvert.DeserializeObject<List<SimpleLocationDto>>(ltd.Locations);

                            if (myL.Any() && myL.Count() == 1)
                            {
                                ltd.RefLocationId = Convert.ToInt32(myL[0].Id);
                            }
                        }
                        else
                        {
                            ltd.RefLocationId = 0;
                        }
                    }
                }
                return new PagedResultOutput<ScreenMenuListDto>(
                    allItemCount,
                    allListDtos
                    );
            }
        }

        public async Task<PagedResultOutput<ScreenMenuCategoryListDto>> GetCategories(GetScreenMenuInput input)
        {
            DateTime? StartDate, EndDate;
            StartDate = input.StartDate;
            EndDate = input.EndDate;
            var allItems = _screenCatRepo
            .GetAll()
            .WhereIf(
                input.MenuId > 0,
                p => p.ScreenMenuId == input.MenuId);

            if (!string.IsNullOrEmpty(input.Filter))
            {
                allItems = allItems.WhereIf(!string.IsNullOrEmpty(input.Filter), p => p.Name.Contains(input.Filter));
            }
            List<int> screenmenuCategoryRefIds = new List<int>();
            if (input.DateFilterApplied)
            {
                foreach (var lst in allItems)
                {
                    List<ScreenMenuCategoryScheduleEditDto> screenMenuCategorySchedule = new List<ScreenMenuCategoryScheduleEditDto>();

                    if (!string.IsNullOrEmpty(lst.ScreenMenuCategorySchedule))
                    {
                        screenMenuCategorySchedule = JsonConvert.DeserializeObject<List<ScreenMenuCategoryScheduleEditDto>>(lst.ScreenMenuCategorySchedule);

                        var rsscreenmenucategorySchedules = screenMenuCategorySchedule.Where(t => t.StartDate >= StartDate
                      && t.EndDate <= EndDate).ToList();

                        if (rsscreenmenucategorySchedules != null && rsscreenmenucategorySchedules.Count > 0)
                        {
                            screenmenuCategoryRefIds.Add(lst.Id);
                        }
                    }
                }
                if (screenmenuCategoryRefIds != null && screenmenuCategoryRefIds.Count > 0)
                {
                    allItems = allItems.Where(t => screenmenuCategoryRefIds.Contains(t.Id));
                }
            }
            if (!string.IsNullOrEmpty(input.Operation) && input.Operation.Equals("All"))
            {
                allItems = allItems.OrderBy(input.Sorting);
            }
            else
            {
                allItems = allItems
                    .OrderBy(input.Sorting);

                if (input.MaxResultCount > 1)
                {
                    allItems = allItems
                    .PageBy(input);
                }
            }

            var cats = await allItems.ToListAsync();

            var allListDtos = cats.MapTo<List<ScreenMenuCategoryListDto>>();
            // var screenmenucategorySchedules=await _screenmenucategorySchedule.GetAllListAsync(t => DbFunctions.TruncateTime(StartDate) >= DbFunctions.TruncateTime(t.StartDate)
            //&& DbFunctions.TruncateTime(EndDate) <= DbFunctions.TruncateTime(t.EndDate));
            //foreach (var lst in allListDtos)
            //{
            //    foreach(var sublst in lst.ScreenMenuCategorySchedules)
            //    {
            //        if (StartDate != null)
            //            sublst = lst.ScreenMenuCategorySchedules.Where(t => DbFunctions.TruncateTime(StartDate) >= DbFunctions.TruncateTime(t.StartDate));
            //        if (EndDate != null)
            //            allListDtos = lst.ScreenMenuCategorySchedules.Where(t => DbFunctions.TruncateTime(EndDate) <= DbFunctions.TruncateTime(t.EndDate));
            //    }
            //}

            var allItemCount = await allItems.CountAsync();
            return new PagedResultOutput<ScreenMenuCategoryListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task SaveSortCategories(int[] categories)
        {
            var i = 1;
            foreach (var category in categories)
            {
                var cate = await _screenCatRepo.GetAsync(category);
                cate.SortOrder = i++;
                await _screenCatRepo.UpdateAsync(cate);
            }
        }

        public async Task SaveSortSortItems(int[] menuItems)
        {
            var i = 1;
            foreach (var item in menuItems)
            {
                var mItem = await _screenItemRepo.GetAsync(item);
                mItem.SortOrder = i++;
                await _screenItemRepo.UpdateAsync(mItem);
            }
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _screenmenuRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<ScreenMenuListDto>>();
            var baseE = new BaseExportObject
            {
                ExportObject = allListDtos,
                ColumnNames = new[] { "Id" }
            };
            return _exporter.ExportToFile(baseE, L("ScreenMenu"));
        }

        public async Task<GetScreenMenuForEditOutput> GetScreenMenuForEdit(FilterInputDto input)
        {
            var output = new GetScreenMenuForEditOutput();
            ScreenMenuEditDto editDto;
            List<ComboboxItemDto> allDepartments = new List<ComboboxItemDto>();
            if (input.Id.HasValue)
            {
                var hDto = await _screenmenuRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<ScreenMenuEditDto>();
                if (!string.IsNullOrEmpty(editDto.Departments))
                {
                    allDepartments = JsonConvert.DeserializeObject<List<ComboboxItemDto>>(editDto.Departments);
                }
                UpdateLocationAndNonLocationForEdit(editDto, editDto.LocationGroup);
            }
            else
            {
                editDto = new ScreenMenuEditDto();
            }
            return new GetScreenMenuForEditOutput
            {
                ScreenMenu = editDto,
                Departments = allDepartments
            };
        }

        public async Task<GetScreenCategoryForEditOutput> GetCategoryForEdit(NullableIdInput input)
        {
            ScreenMenuCategoryEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _screenCatRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<ScreenMenuCategoryEditDto>();
                if (!editDto.ScreenMenuCategorySchedule.IsNullOrEmpty())
                {
                    Collection<ScreenMenuCategoryScheduleEditDto> screenMenuCategoryScheduleJson = JsonConvert.DeserializeObject<Collection<ScreenMenuCategoryScheduleEditDto>>(editDto.ScreenMenuCategorySchedule);
                    editDto.ScreenMenuCategorySchedules = screenMenuCategoryScheduleJson;
                }
                else
                {
                    editDto.ScreenMenuCategorySchedules = new Collection<ScreenMenuCategoryScheduleEditDto>();
                }
            }
            else
            {
                editDto = new ScreenMenuCategoryEditDto();
            }
            return new GetScreenCategoryForEditOutput
            {
                Category = editDto
            };
        }

        public async Task CreateOrUpdateCategory(CreateOrUpdateCategory input)
        {
            if (input.Category.Id.HasValue)
            {
                await UpdateScreenCategory(input);
            }
            else
            {
                await CreateScreenCategory(input);
            }
            await _syncAppService.UpdateSync(SyncConsts.SCREENMENU);
        }

        public async Task<IdInput> CreateOrUpdateScreenMenu(CreateOrUpdateScreenMenuInput input)
        {
            IdInput output = null;
            if (input == null)
                return output;

            var allDepartments = "";

            if (input.Departments.Any())
            {
                allDepartments = JsonConvert.SerializeObject(input.Departments);
            }

            if (input.ScreenMenu.Id.HasValue)
            {
                output = await UpdateScreenMenu(input, allDepartments);
            }
            else
            {
                output = await CreateScreenMenu(input, allDepartments);
            }
            await _syncAppService.UpdateSync(SyncConsts.SCREENMENU);
            return output;
        }

        public async Task DeleteScreenMenu(IdInput input)
        {
            await _screenmenuRepo.DeleteAsync(input.Id);
            await _syncAppService.UpdateSync(SyncConsts.SCREENMENU);
        }

        public async Task CloneMenu(IdInput input)
        {
            if (input.Id > 0)
            {
                var obMenu = _screenmenuRepo.Get(input.Id);

                var menu = new ScreenMenu
                {
                    Name = obMenu.Name,
                    Departments = obMenu.Departments,
                    Locations = obMenu.Locations,
                    CategoryColumnCount = obMenu.CategoryColumnCount,
                    CategoryColumnWidthRate = obMenu.CategoryColumnWidthRate
                };

                menu.Categories = new Collection<ScreenMenuCategory>();

                foreach (var smc in obMenu.Categories)
                {
                    var smcNew = new ScreenMenuCategory
                    {
                        Name = smc.Name,
                        MostUsedItemsCategory = smc.MostUsedItemsCategory,
                        ImagePath = smc.ImagePath,
                        MainButtonHeight = smc.MainButtonHeight,
                        MainButtonColor = smc.MainButtonColor,
                        MainFontSize = smc.MainFontSize,
                        ColumnCount = smc.ColumnCount,
                        MenuItemButtonHeight = smc.MenuItemButtonHeight,
                        SubButtonHeight = smc.SubButtonHeight,
                        SubButtonRows = smc.SubButtonRows,
                        SubButtonColorDef = smc.SubButtonColorDef,
                        MaxItems = smc.MaxItems,
                        PageCount = smc.PageCount,
                        WrapText = smc.WrapText,
                        SortOrder = smc.SortOrder,
                        NumeratorType = smc.NumeratorType
                    };
                    menu.Categories.Add(smcNew);
                    smcNew.MenuItems = new Collection<ScreenMenuItem>();

                    foreach (var smi in smc.MenuItems)
                    {
                        var mi = new ScreenMenuItem
                        {
                            Name = smi.Name,
                            ImagePath = smi.ImagePath,
                            ShowAliasName = smi.ShowAliasName,
                            AutoSelect = smi.AutoSelect,
                            ButtonColor = smi.ButtonColor,
                            FontSize = smi.FontSize,
                            SubMenuTag = smi.SubMenuTag,
                            OrderTags = smi.OrderTags,
                            ItemPortion = smi.ItemPortion,
                            MenuItemId = smi.MenuItemId,
                            SortOrder = smi.SortOrder
                        };
                        smcNew.MenuItems.Add(mi);
                    }
                }

                if (menu.Categories.Any())
                {
                    await _screenmenuRepo.InsertAndGetIdAsync(menu);
                }
            }
        }

        public async Task DeleteScreenCategory(IdInput input)
        {
            await _screenCatRepo.DeleteAsync(input.Id);
            await _syncAppService.UpdateSync(SyncConsts.SCREENMENU);
        }

        public async Task<GetScreenMenuItemBySubMenuOuput> GetMenuItemForVisualScreen(GetScreenMenuInput input)
        {
            var allItems = await _screenItemRepo.GetAll()
                                                .WhereIf(input.CategoryId > 0, x => x.ScreenCategoryId == input.CategoryId)
                                                .ToListAsync();

            //Get MenuItems
            var tag = input.CurrentTag;
            if (tag.Contains(','))
            {
                tag = tag.Split(',').Last().Trim();
            }
            var menuItems = allItems.Where(x =>
            {
                if (x.SubMenuTag == tag || (string.IsNullOrEmpty(tag) && string.IsNullOrEmpty(x.SubMenuTag)))
                {
                    return true;
                }
                var tags = x.SubMenuTag.Split(',').Select(y => y.Trim()).ToList();
                if (tags.Any(z => z.Equals(tag)) && tags.IndexOf(tag) == tags.Count - 1)
                {
                    return true;
                }
                return false;
            });

            //Get Tags
            var parentTag = input.CurrentTag;
            var tagOrderComparer = new TagComparer(allItems.ToList());
            var cateTags = allItems.Where(x => !string.IsNullOrEmpty(x.SubMenuTag)).Select(x => x.SubMenuTag).Distinct()
                                .Where(x => string.IsNullOrEmpty(parentTag) || (x.StartsWith(parentTag) && x != parentTag))
                                .Select(x => Regex.Replace(x, "^" + parentTag + ",", ""))
                                .Select(x =>
                                {
                                    if (x.Contains(','))
                                    {
                                        return x.Split(',').First();
                                    }
                                    return x;
                                })
                                .Select(x => !string.IsNullOrEmpty(parentTag) ? parentTag + "," + x : x).Distinct().OrderBy(x => x, tagOrderComparer);

            return new GetScreenMenuItemBySubMenuOuput { CategoryTags = cateTags, MenuItems = menuItems.MapTo<List<ScreenMenuItemListDto>>() };
        }

        private class TagComparer : IComparer<string>
        {
            private List<ScreenMenuItem> _menuItems;

            public TagComparer(List<ScreenMenuItem> menuItems)
            {
                _menuItems = menuItems.OrderBy(t => t.SortOrder).ToList();
            }

            public int Compare(string x, string y)
            {
                var menuItemX = _menuItems.FirstOrDefault(m => x != null && m.SubMenuTag.Contains(x));
                var menuItemY = _menuItems.FirstOrDefault(m => y != null && m.SubMenuTag.Contains(y));

                if (menuItemY != null && menuItemX != null)
                    return menuItemX.SortOrder.CompareTo(menuItemY.SortOrder);
                return 1;
            }
        }

        public async Task<PagedResultOutput<ScreenMenuItemListDto>> GetMenuItems(GetScreenMenuInput input)
        {
            var allItems = _screenItemRepo
            .GetAll()
            .WhereIf(
                input.CategoryId > 0,
                p => p.ScreenCategoryId == input.CategoryId)
            .WhereIf(!string.IsNullOrEmpty(input.Filter), p => p.Name.Contains(input.Filter));

            List<ScreenMenuItem> menuItems = null;
            if (!string.IsNullOrEmpty(input.Operation) && input.Operation.Equals("All"))
            {
                menuItems = await allItems.OrderBy(input.Sorting).ToListAsync();
            }
            else
            {
                menuItems = await allItems
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();
            }
            var allListDtos = menuItems.MapTo<List<ScreenMenuItemListDto>>();
            var allItemCount = await allItems.CountAsync();
            return new PagedResultOutput<ScreenMenuItemListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<GetScreenMenuItemForEditOutput> GetMenuItemForEdit(NullableIdInput input)
        {
            ScreenMenuItemEditDto editDto;
            var menuIt = 0;
            if (input.Id.HasValue)
            {
                var hDto = await _screenItemRepo.GetAsync(input.Id.Value);
                menuIt = hDto.MenuItemId;
                editDto = hDto.MapTo<ScreenMenuItemEditDto>();
            }
            else
            {
                editDto = new ScreenMenuItemEditDto();
            }
            return new GetScreenMenuItemForEditOutput
            {
                MenuItem = editDto,
                MenuItemId = menuIt
            };
        }

        public async Task CreateMenuItem(CreateScreenMenuItemInput input)
        {
            if (input.MenuItemId > 0)
            {
                var menuName = _mRepo.Get(input.MenuItemId).Name;
                if (string.IsNullOrEmpty(input.Name))
                {
                    input.Name = menuName;
                }
                if (!string.IsNullOrEmpty(input.Name) && input.MenuItemId > 0)
                {
                    ScreenMenuItem item = new ScreenMenuItem
                    {
                        Name = input.Name,
                        ScreenCategoryId = input.CategoryId,
                        MenuItemId = input.MenuItemId
                    };

                    if (input.MenuItem != null)
                    {
                        if (!string.IsNullOrEmpty(input.MenuItem.SubMenuTag))
                        {
                            item.SubMenuTag = input.MenuItem.SubMenuTag;
                        }
                        if (!string.IsNullOrEmpty(input.MenuItem.OrderTags))
                        {
                            item.SubMenuTag = input.MenuItem.OrderTags;
                        }
                        if (!string.IsNullOrEmpty(input.MenuItem.ButtonColor))
                        {
                            item.ButtonColor = input.MenuItem.ButtonColor;
                        }
                        if (input.MenuItem.FontSize > 0)
                        {
                            item.FontSize = input.MenuItem.FontSize;
                        }
                    }

                    input.Files = input.Files;
                    input.DownloadImage = input.DownloadImage;

                    await _screenItemRepo.InsertAsync(item);
                }
            }
            else if (input.Items.Any())
            {
                var allIds = input.Items.Select(a => a.Id).ToList();
                var removalList = _screenItemRepo.GetAll().Where(a => a.ScreenCategoryId == input.CategoryId
                                                                      && !allIds.Contains(a.MenuItemId)).ToList();

                foreach (var myItem in removalList)
                {
                    _screenItemRepo.Delete(myItem.Id);
                }

                foreach (var myItem in input.Items)
                {
                    if (_screenItemRepo.FirstOrDefault(a => a.MenuItemId == myItem.Id && a.ScreenCategoryId == input.CategoryId) == null)
                    {
                        var myName = myItem.Name;
                        if (myName.Contains("@"))
                        {
                            var splitStr = myName.Split("@");
                            if (splitStr != null && splitStr.Any())
                            {
                                myName = splitStr[0];
                            }
                        }
                        ScreenMenuItem item = new ScreenMenuItem
                        {
                            Name = myName,
                            ScreenCategoryId = input.CategoryId,
                            MenuItemId = myItem.Id
                        };

                        if (input.MenuItem != null)
                        {
                            if (!string.IsNullOrEmpty(input.MenuItem.SubMenuTag))
                            {
                                item.SubMenuTag = input.MenuItem.SubMenuTag;
                            }
                            if (!string.IsNullOrEmpty(input.MenuItem.OrderTags))
                            {
                                item.SubMenuTag = input.MenuItem.OrderTags;
                            }
                            if (!string.IsNullOrEmpty(input.MenuItem.ButtonColor))
                            {
                                item.ButtonColor = input.MenuItem.ButtonColor;
                            }
                            if (input.MenuItem.FontSize > 0)
                            {
                                item.FontSize = input.MenuItem.FontSize;
                            }

                            input.Files = input.Files;
                            input.DownloadImage = input.DownloadImage;
                        }
                        await _screenItemRepo.InsertAsync(item);
                    }
                }
            }
        }

        public async Task CreateOrUpdateMenuItem(CreateOrUpdateScreenMenuItem input)
        {
            if (input.MenuItem.Id.HasValue)
            {
                await UpdateScreenMenuItem(input);
            }
            else
            {
                await CreateScreenMenuItem(input);
            }
            await _syncAppService.UpdateSync(SyncConsts.SCREENMENU);
        }

        public async Task DeleteScreenMenuItem(IdInput input)
        {
            await _screenItemRepo.DeleteAsync(input.Id);
            await _syncAppService.UpdateSync(SyncConsts.SCREENMENU);
        }

        public async Task<ListResultOutput<ScreenMenuListDto>> GetIds()
        {
            var lstScreenMenu = await _screenmenuRepo.GetAll().ToListAsync();
            return new ListResultOutput<ScreenMenuListDto>(lstScreenMenu.MapTo<List<ScreenMenuListDto>>());
        }

        public async Task<ApiMenu> ApiMenu(ApiLocationInput locationInput)
        {
            var output = new ApiMenu { ScreenMenu = await ApiScreenMenu(locationInput) };
            var mItems =
                output.ScreenMenu.Menu.SelectMany(a => a.Categories)
                    .SelectMany(a => a.MenuItems).Select(a => a.MenuItemId).ToList();

            var mOutput = await _mService.ApiItemsForLocation(locationInput);
            var menuItems = mOutput.Categories.SelectMany(a => a.MenuItems).Where(a => mItems.Contains(a.Id)).ToList();
            output.Menu = menuItems;
            return output;
        }

        public async Task<GetScreenItemsOutput> GetScreenItemsForLocation(GetScreenMenuInput input)
        {
            var output = new GetScreenItemsOutput();

            var allItems = await _mService.GetAllListItems(new GetMenuItemLocationInput
            {
                LocationId = input.LocationId
            });

            var allCatItems =
                _screenItemRepo.GetAll()
                    .Where(a => a.ScreenCategoryId == input.CategoryId)
                    .Select(a => a.MenuItemId)
                    .ToList();

            var allI = allItems.Items.Where(a => a?.Id != null && !allCatItems.Contains(a.Id));
            var allList = allI.ToList();
            foreach (var allM in allList)
            {
                output.Items.Add(new IdNameDto(allM.Id, allM.Name + "@" + allM.CategoryName));
            }
            output.SelectedItems =
              _screenItemRepo.GetAll().Where(a => a.ScreenCategoryId == input.CategoryId).Select(a => new IdNameDto
              {
                  Id = a.MenuItemId,
                  Name = a.Name + "@" + a.ScreenMenuCategory.Name
              }).ToList();

            return output;
        }

        public async Task<ApiScreenMenuOutput> ApiScreenMenu(ApiLocationInput locationInput)
        {
            var orgId = await _locAppService.GetOrgnizationIdForLocationId(locationInput.LocationId);
            if (orgId > 0)
            {
                CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);
            }

            var output = new ApiScreenMenuOutput();

            var allMenu = _screenmenuRepo.GetAll().Where(a => a.TenantId == locationInput.TenantId && a.FutureData == locationInput.IsFuturedata);

            var myMenu = new List<ScreenMenu>();
            if (!string.IsNullOrEmpty(locationInput.Tag))
            {
                var allLocations = await _locAppService.GetLocationsByLocationGroupCode(locationInput.Tag);
                if (allLocations != null && allLocations.Items.Any())
                {
                    foreach (var screenMenu in allMenu)
                    {
                        if (!screenMenu.Group && !string.IsNullOrEmpty(screenMenu.Locations) && !screenMenu.LocationTag)
                        {
                            var scLocations =
                                JsonConvert.DeserializeObject<List<SimpleLocationGroupDto>>(screenMenu.Locations);
                            if (scLocations.Any())
                            {
                                foreach (var i in scLocations.Select(a => a.Id))
                                {
                                    if (allLocations.Items.Select(a => a.Value).Contains(i.ToString()))
                                    {
                                        myMenu.Add(screenMenu);
                                    }
                                }
                            }
                            else
                            {
                                myMenu.Add(screenMenu);
                            }
                        }
                    }
                }
            }
            else if (locationInput.LocationId != 0)
            {
                foreach (var screenMenu in allMenu)
                {
                    if (await _locAppService.IsLocationExists(new CheckLocationInput
                    {
                        LocationId = locationInput.LocationId,
                        Locations = screenMenu.Locations,
                        Group = screenMenu.Group,
                        NonLocations = screenMenu.NonLocations
                    }))
                    {
                        myMenu.Add(screenMenu);
                    }
                }
            }
            else
            {
                myMenu = allMenu.ToList();
            }

            if (!string.IsNullOrEmpty(locationInput.Tag))
            {
                output.Menu.Clear();

                ApiScreenMenu oScMenu = new ApiScreenMenu();

                foreach (var screenMenu in myMenu)
                {
                    var menu = screenMenu.MapTo<ApiScreenMenu>();
                    foreach (var apiScreenCategory in menu.Categories)
                    {
                        if (oScMenu.Categories.Any())
                        {
                            var lasCat = oScMenu.Categories.LastOrDefault(a =>
                                a.Name.Equals(apiScreenCategory.Name));
                            if (lasCat != null)
                            {
                                foreach (var apiScreenMenuItem in apiScreenCategory.MenuItems)
                                {
                                    lasCat.MenuItems.Add(apiScreenMenuItem);
                                }
                            }
                            else
                            {
                                oScMenu.Categories.Add(apiScreenCategory);
                            }
                        }
                        else
                        {
                            oScMenu.Categories.Add(apiScreenCategory);
                        }
                    }
                    oScMenu.Id = screenMenu.Id;
                }

                var myMenuIdea = myMenu.LastOrDefault();
                if (myMenuIdea != null)
                {
                    oScMenu.CategoryColumnCount = myMenuIdea.CategoryColumnCount;
                    oScMenu.CategoryColumnWidthRate = myMenuIdea.CategoryColumnWidthRate;
                }

                output.Menu.Add(oScMenu);
                oScMenu.Name = "Menu";
            }
            else
            {
                foreach (var screenMenu in myMenu)
                {
                    var menu = screenMenu.MapTo<ApiScreenMenu>();
                    if (!string.IsNullOrEmpty(screenMenu.Departments))
                    {
                        var allDep = JsonConvert.DeserializeObject<List<ComboboxItemDto>>(menu.Departments);
                        menu.Departments = string.Join(",", allDep.Select(a => a.DisplayText));
                    }

                    output.Menu.Add(menu);
                }
            }
            return output;
        }

        public async Task<ApiScreenMenu> ApiSingleScreenMenu(ApiLocationInput locationInput)
        {
            var orgId = await _locAppService.GetOrgnizationIdForLocationId(locationInput.LocationId);
            if (orgId > 0)
            {
                CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);
            }

            var output = new ApiScreenMenu();
            var lastMenu = _screenmenuRepo.GetAll().Where(a => a.Id == locationInput.Id).ToList();
            if (lastMenu.Any())
            {
                var myScreen = lastMenu.LastOrDefault();
                return myScreen.MapTo<ApiScreenMenu>();
            }
            return output;
        }

        public async Task ClearScreenMenu(IdInput input)
        {
            if (input.Id > 0)
            {
                var menu = _screenmenuRepo.Get(input.Id);
                try
                {
                    if (menu != null && menu?.Categories.Count >= 0)
                    {
                        foreach (var smc in menu.Categories)
                        {
                            await _screenItemRepo.DeleteAsync(a => a.ScreenCategoryId == smc.Id);
                        }
                        await _screenCatRepo.DeleteAsync(a => a.ScreenMenuId == input.Id);
                    }
                }
                catch (Exception exception)
                {
                    var mess = exception.Message;
                }
            }
        }

        public async Task GenerateScreenMenu(IdInput input, bool locationInput)
        {
            var locationId = 0;
            bool locationGroup = false;
            ScreenMenu menu = null;
            if (input != null && input.Id > 0)
            {
                if (!locationInput)
                {
                    menu = _screenmenuRepo.Get(input.Id);
                    if (menu.Group)
                    {
                        locationGroup = true;
                    }
                    if (!string.IsNullOrEmpty(menu?.Locations))
                    {
                        var simList = JsonConvert.DeserializeObject<List<SimpleLocationDto>>(menu.Locations);
                        if (simList.Any() && simList.Count() == 1)
                        {
                            locationId = simList.First().Id;
                        }
                    }
                }
                else
                {
                    locationId = input.Id;
                }
            }
            if (locationId == 0 && !locationGroup)
            {
                if (menu != null && menu?.Categories.Count == 0)
                {
                    menu?.Categories.Clear();
                    var allCate = _catRepo.GetAll();
                    foreach (var category in allCate)
                    {
                        var cate = new ScreenMenuCategory
                        {
                            Name = category.Name,
                            MenuItems = new List<ScreenMenuItem>()
                        };

                        foreach (var menuItem in category.MenuItems)
                        {
                            cate.MenuItems.Add(new ScreenMenuItem
                            {
                                Name = menuItem.Name,
                                ShowAliasName = true,
                                MenuItemId = menuItem.Id,
                                SubMenuTag = menuItem.Tag
                            });
                        }
                        menu.Categories.Add(cate);
                    }
                }
                await _screenmenuRepo.UpdateAsync(menu);
            }
            else if (locationId > 0 && !locationGroup)
            {
                var allLo = await _lRepo.FirstOrDefaultAsync(a => a.Id == locationId);
                if (await CheckScreenMenuExists(allLo))
                {
                    return;
                }

                var allList = await _mService.GetAllListItems(new GetMenuItemLocationInput
                {
                    LocationId = locationId
                });
                if (locationInput)
                {
                    var comL = new List<SimpleLocationDto>
                    {
                        new SimpleLocationDto
                        {
                            Name = allLo.Name,
                            Id = allLo.Id,
                            Code = allLo.Code
                        }
                    };

                    menu = new ScreenMenu
                    {
                        Locations = JsonConvert.SerializeObject(comL),
                        Name = allLo.Name,
                        Categories = new Collection<ScreenMenuCategory>()
                    };
                }

                if (menu == null)
                    return;

                foreach (var ci in allList.Items.GroupBy(a => a.CategoryId))
                {
                    var cat = await _catRepo.FirstOrDefaultAsync(a => a.Id == ci.Key);
                    if (cat != null)
                    {
                        var cate = new ScreenMenuCategory
                        {
                            Name = cat.Name,
                            MenuItems = new List<ScreenMenuItem>()
                        };
                        foreach (var menuItem in ci)
                        {
                            if (menuItem.Id != null)
                                cate.MenuItems.Add(new ScreenMenuItem
                                {
                                    Name = menuItem.Name,
                                    ShowAliasName = true,
                                    MenuItemId = menuItem.Id,
                                    SubMenuTag = menuItem.Tag
                                });
                        }
                        menu.Categories.Add(cate);
                    }
                }
                await _screenmenuRepo.InsertOrUpdateAndGetIdAsync(menu);
            }
            else if (locationGroup)
            {
                var allLocations = await _locAppService.GetLocationsForGivenLocationGroupId(new GetLocationGroupInput()
                {
                    LocationGroupId = locationId
                });

                menu?.Categories.Clear();

                if (allLocations != null && allLocations.Items.Any())
                {
                    foreach (var allLocationsItem in allLocations.Items)
                    {
                        var output = await _mService.GetAllListItems(new GetMenuItemLocationInput
                        {
                            LocationId = Convert.ToInt32(allLocationsItem.Id)
                        }, true);
                        FillScreenMenuIems(menu, output);
                    }

                    var myOutput = await _mService.GetAllListItems(new GetMenuItemLocationInput
                    {
                        LocationId = 0
                    });

                    FillScreenMenuIems(menu, myOutput);
                }

                if (menu.Categories.Any())
                {
                    await _screenmenuRepo.InsertOrUpdateAndGetIdAsync(menu);
                }
            }
        }

        private void FillScreenMenuIems(ScreenMenu menu, PagedResultOutput<MenuItemListDto> output)
        {
            if (menu.Categories == null)
                menu.Categories = new List<ScreenMenuCategory>();

            foreach (var myMenuItem in output.Items)
            {
                ScreenMenuCategory category = null;
                if (menu.Categories != null && menu.Categories.Any())
                {
                    var lastCategory = menu.Categories.LastOrDefault(a => a.Name.Equals(myMenuItem.CategoryName));

                    if (lastCategory != null)
                    {
                        category = lastCategory;
                    }
                }
                if (category == null)
                {
                    category = new ScreenMenuCategory()
                    {
                        Name = myMenuItem.CategoryName,
                        MenuItems = new List<ScreenMenuItem>()
                    };
                    menu.Categories.Add(category);
                }
                ScreenMenuItem myItem = null;
                if (category.MenuItems != null && category.MenuItems.Any())
                {
                    var lastMenuItem = category.MenuItems.LastOrDefault(a => a.Name.Equals(myMenuItem.Name));
                    if (lastMenuItem != null)
                    {
                        myItem = lastMenuItem;
                    }
                }
                if (myItem == null && myMenuItem.Id != null)
                {
                    myItem = new ScreenMenuItem
                    {
                        Name = myMenuItem.Name,
                        ShowAliasName = true,
                        MenuItemId = myMenuItem.Id,
                    };
                    category.MenuItems?.Add(myItem);
                }
            }
        }

        public async Task CreateCategory(CreateCategoryInput input)
        {
            if (!string.IsNullOrEmpty(input.Name))
            {
                var allCate = await _catRepo.FirstOrDefaultAsync(a => a.Name.Equals(input.Name));
                var allItems = new List<ScreenMenuItem>();
                if (allCate != null && allCate.MenuItems.Any())
                {
                    allItems.AddRange(allCate.MenuItems.Select(mi => new ScreenMenuItem
                    {
                        MenuItemId = mi.Id,
                        Name = mi.Name,
                        ShowAliasName = true
                    }));
                }
                await _screenCatRepo.InsertAsync(new ScreenMenuCategory
                {
                    Name = input.Name,
                    ScreenMenuId = input.MenuId,
                    MenuItems = allItems,
                    Files = input.Files,
                    DownloadImage = input.DownloadImage
                });
            }
        }

        public async Task<ListResultOutput<ScreenMenuListDto>> GetScreenMenuNames(bool futureData = false)
        {
            var returnList = await _screenmenuRepo.GetAll().Where(t => t.FutureData == futureData)
                .ToListAsync();

            return new ListResultOutput<ScreenMenuListDto>(returnList.MapTo<List<ScreenMenuListDto>>());
        }

        private async Task<bool> CheckScreenMenuExists(Master.Location allLo)
        {
            var returnStatus = false;

            foreach (var screenMenu in _screenmenuRepo.GetAll())
            {
                if (!string.IsNullOrEmpty(screenMenu.Locations))
                {
                    var locations = JsonConvert.DeserializeObject<List<SimpleLocationDto>>(screenMenu.Locations);
                    if (locations.Find(a => a.Id.Equals(allLo.Id)) != null)
                    {
                        var totalCount = screenMenu.Categories.Count();
                        if (totalCount != 0)
                        {
                            return true;
                        }
                    }
                }
            }
            return returnStatus;
        }

        protected virtual async Task<IdInput> UpdateScreenMenu(CreateOrUpdateScreenMenuInput input, string allDepartments)
        {
            var dto = input.ScreenMenu.MapTo<ScreenMenu>();
            var item = await _screenmenuRepo.GetAsync(input.ScreenMenu.Id.Value);
            List<ComboboxItemDto> allDepartment = new List<ComboboxItemDto>();
            ScreenMenuEditDto editDto = new ScreenMenuEditDto();
            item.Departments = allDepartments;
            item.CategoryColumnCount = dto.CategoryColumnCount;
            item.CategoryColumnWidthRate = dto.CategoryColumnWidthRate;
            item.Name = dto.Name;
            item.FutureData = dto.FutureData;
            UpdateLocationAndNonLocation(item, input.LocationGroup);

            CheckErrors(await _screenmenuManager.CreateOrUpdateSync(item));


            return new IdInput { Id = item.Id };
        }

        protected virtual async Task<IdInput> CreateScreenMenu(CreateOrUpdateScreenMenuInput input, string allDepartments)
        {
            var dto = input.ScreenMenu.MapTo<ScreenMenu>();
            dto.Departments = allDepartments;
            UpdateLocationAndNonLocation(dto, input.LocationGroup);
            CheckErrors(await _screenmenuManager.CreateOrUpdateSync(dto));
            return new IdInput { Id = dto.Id };
        }

        private async Task CreateScreenCategory(CreateOrUpdateCategory input)
        {
            await CreateCategory(new CreateCategoryInput
            {
                Name = input.Category.Name,
                MenuId = input.MenuId,
                Files = input.Category.Files,
                DownloadImage = Guid.NewGuid()
            });
            await _syncAppService.UpdateSync(SyncConsts.SCREENMENU);
        }

        private async Task UpdateScreenCategory(CreateOrUpdateCategory input)
        {
            var from = input.Category;
            var myCategoryLocal = _screenCatRepo.Get(input.Category.Id.Value);
            myCategoryLocal.Name = from.Name;
            myCategoryLocal.MostUsedItemsCategory = from.MostUsedItemsCategory;
            myCategoryLocal.ImagePath = from.ImagePath;
            myCategoryLocal.MainButtonHeight = from.MainButtonHeight;
            myCategoryLocal.MainButtonColor = from.MainButtonColor;
            myCategoryLocal.MainFontSize = from.MainFontSize;
            myCategoryLocal.ColumnCount = from.ColumnCount;
            myCategoryLocal.MenuItemButtonHeight = from.MenuItemButtonHeight;
            myCategoryLocal.MaxItems = from.MaxItems;
            myCategoryLocal.PageCount = from.PageCount;
            myCategoryLocal.WrapText = from.WrapText;
            myCategoryLocal.SubButtonColorDef = from.SubButtonColorDef;
            myCategoryLocal.SubButtonHeight = from.SubButtonHeight;
            myCategoryLocal.SubButtonRows = from.SubButtonRows;
            myCategoryLocal.NumeratorType = from.NumeratorType;
            myCategoryLocal.Files = from.Files;
            myCategoryLocal.ScreenMenuCategorySchedule = from.ScreenMenuCategorySchedule;
            if (!string.IsNullOrEmpty(myCategoryLocal.Files) && !myCategoryLocal.Files.Equals(from.Files))
            {
                myCategoryLocal.DownloadImage = Guid.NewGuid();
            }

            if (input.Category.RefreshImage)
            {
                myCategoryLocal.DownloadImage = Guid.NewGuid();
            }
            var findCate = await _screenCatRepo.GetAllListAsync(a => a.ScreenMenuId == input.MenuId);
            var checkCopyColor = input.CopyColor;
            if (input.CopyToAll)
            {               
                foreach (var smc in findCate)
                {
                    smc.MostUsedItemsCategory = !@from.MostUsedItemsCategory && @from.MostUsedItemsCategory;
                    smc.MainButtonHeight = from.MainButtonHeight;
                    smc.MainButtonColor = checkCopyColor ? from.MainButtonColor : smc.MainButtonColor;
                    smc.MainFontSize = from.MainFontSize;
                    smc.ColumnCount = from.ColumnCount;
                    smc.MenuItemButtonHeight = from.MenuItemButtonHeight;
                    smc.MaxItems = from.MaxItems;
                    smc.PageCount = from.PageCount;
                    smc.WrapText = from.WrapText;
                    smc.SubButtonColorDef = checkCopyColor ? from.SubButtonColorDef : smc.SubButtonColorDef;
                    smc.SubButtonHeight = from.SubButtonHeight;
                    smc.SubButtonRows = from.SubButtonRows;
                    smc.NumeratorType = from.NumeratorType;
                    smc.ScreenMenuCategorySchedule = from.ScreenMenuCategorySchedule;
                    await _screenCatRepo.UpdateAsync(smc);
                }
            }
            if(input.CopyColor)
            {
                foreach (var smc in findCate)
                {
                    smc.MainButtonColor = from.MainButtonColor ;
                    smc.SubButtonColorDef = from.SubButtonColorDef;
                    await _screenCatRepo.UpdateAsync(smc);
                }
            }
        }

        private async Task CreateScreenMenuItem(CreateOrUpdateScreenMenuItem input)
        {
            await CreateMenuItem(new CreateScreenMenuItemInput
            {
                Name = input.MenuItem.Name,
                MenuItemId = input.MenuItemId,
                CategoryId = input.CategoryId,
                Files = input.MenuItem.Files,
                DownloadImage = Guid.NewGuid()
            });
            await _syncAppService.UpdateSync(SyncConsts.SCREENMENU);
        }

        private async Task UpdateScreenMenuItem(CreateOrUpdateScreenMenuItem input)
        {
            var fromItem = input.MenuItem;
            if (input.MenuItem.Id != null)
            {
                var myMenuItem = _screenItemRepo.Get(input.MenuItem.Id.Value);
                if (!string.IsNullOrEmpty(myMenuItem.Files) && !myMenuItem.Files.Equals(fromItem.Files))
                {
                    fromItem.DownloadImage = Guid.NewGuid();
                }

                if (input.MenuItem.RefreshImage)
                {
                    fromItem.DownloadImage = Guid.NewGuid();
                }

                fromItem.MapTo(myMenuItem);
                myMenuItem.MenuItemId = input.MenuItemId;

                await _screenItemRepo.UpdateAsync(myMenuItem);
            }

            if (input.CopyToAll)
            {
                foreach (var smc in await _screenItemRepo.GetAllListAsync(a => a.ScreenCategoryId == input.CategoryId))
                {
                    smc.ScreenCategoryId = input.CategoryId;
                    smc.ImagePath = fromItem.ImagePath;
                    smc.ShowAliasName = fromItem.ShowAliasName;
                    smc.AutoSelect = fromItem.AutoSelect;
                    smc.ButtonColor = fromItem.ButtonColor;
                    smc.FontSize = fromItem.FontSize;
                    smc.SubMenuTag = fromItem.SubMenuTag;
                    smc.OrderTags = fromItem.OrderTags;
                    smc.ItemPortion = fromItem.ItemPortion;
                    await _screenItemRepo.UpdateAsync(smc);
                }
            }
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _screenmenuRepo.GetAsync(input.Id);
                item.IsDeleted = false;

                await _screenmenuRepo.UpdateAsync(item);
            }
        }

        public ListResultDto<ComboboxItemDto> GetConfirmationTypes()
        {
            var returnList = new List<ComboboxItemDto>();
            var i = 0;
            foreach (var value in Enum.GetValues(typeof(ScreenMenuItemConfirmationTypes)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = value.ToString(),
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultDto<ComboboxItemDto>(returnList);
        }

        public async Task<ScreenMenuEditDto> GetScreenMenuByNameAndLocation(string name, int locationId)
        {
            var allSm = await _screenmenuRepo.GetAllListAsync(a => a.Name.Equals(name));
            var sm = allSm.FirstOrDefault(a => JsonConvert.DeserializeObject<List<SimpleLocationDto>>(a.Locations).Any(x => x.Id == locationId));
            if (sm == null)
                return null;

            return sm.MapTo<ScreenMenuEditDto>();
        }

        public async Task<FileDto> GetImportTemplate()
        {
            try
            {
                if (AbpSession.UserId != null && AbpSession.TenantId != null)
                {
                    var location = (await _lRepo.GetAllListAsync()).FirstOrDefault()?.Name;
                    var menus = new List<MenuItemEditDto>();

                    if (!menus.Any())
                    {
                        menus.Add(new MenuItemEditDto
                        {
                            LocationName = location,
                            CategoryName = "DRINKS",
                            AliasCode = "AC1",
                            Name = "BLACK COFFEE",
                            AliasName = "咖啡乌",
                            ItemDescription = "Black coffee",
                            BarCode = "1234567890",
                            PortionName = "NORMAL",
                            TPPrice = 4,
                            Tag = "DRINKS"
                        });
                        menus.Add(new MenuItemEditDto
                        {
                            LocationName = location,
                            CategoryName = "MAINCOURSE",
                            AliasCode = "MC1",
                            Name = "MOO PING",
                            AliasName = "หมูปิ้ง",
                            ItemDescription = "Moo Ping",
                            BarCode = "1234567890",
                            PortionName = "NORMAL",
                            TPPrice = 30,
                            Tag = "FOOD"
                        });
                    }

                    return await _screenMenuExporter.ExportTemplateFile(menus);
                }

                return null;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}