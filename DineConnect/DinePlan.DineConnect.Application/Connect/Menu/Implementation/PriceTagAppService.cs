﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Utility.Exporter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Future;

namespace DinePlan.DineConnect.Connect.Menu.Implementation
{
    public class PriceTagAppService : DineConnectAppServiceBase, IPriceTagAppService
    {
        private readonly IPriceTagExcelExporter _exExporter;
        private readonly IExcelExporter _exporter;
        private readonly ILocationAppService _locAppService;
        private readonly IRepository<MenuItemPortion> _menuItemPortionManager;
        private readonly IRepository<PriceTagDefinition> _priceTagDef;
        private readonly IRepository<PriceTag> _pricetagRepo;
        private readonly ISyncAppService _syncAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<MenuItem> _miManager;
        private readonly IRepository<FutureDateInformation> _futureDateRepository;
        private readonly IRepository<PriceTagLocationPrice> _pricetagLocationPriceRepo;
        private readonly IRepository<Master.Location> _locationRepo;
        private readonly IRepository<PriceTagLocationPrice> _tagLocationPrice;

        public PriceTagAppService(IRepository<PriceTag> priceTagRepo, ILocationAppService locAppService,
            IExcelExporter exporter, IRepository<MenuItemPortion> mipm, IRepository<PriceTagDefinition> priceDef,
            ISyncAppService syncAppService, IPriceTagExcelExporter exExporter, IRepository<MenuItem> miManager,
            IUnitOfWorkManager unitOfWorkManager,IRepository<PriceTagLocationPrice> tagLocationPrice,
             IRepository<FutureDateInformation> futureDateRepository,
             IRepository<PriceTagLocationPrice> pricetagLocationPriceRepo,
             IRepository<Master.Location> locationRepo)
        {
            _locAppService = locAppService;
            _pricetagRepo = priceTagRepo;
            _exporter = exporter;
            _exExporter = exExporter;
            _menuItemPortionManager = mipm;
            _priceTagDef = priceDef;
            _syncAppService = syncAppService;
            _miManager = miManager;
            _unitOfWorkManager = unitOfWorkManager;
            _futureDateRepository = futureDateRepository;
            _pricetagLocationPriceRepo = pricetagLocationPriceRepo;
            _locationRepo = locationRepo;
            _tagLocationPrice = tagLocationPrice;
        }

        public async Task<PagedResultOutput<PriceTagListDto>> GetAll(GetPriceTagInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var allItems = _pricetagRepo.GetAll();
                allItems = _pricetagRepo
                    .GetAll()
                    .Where(i => i.IsDeleted == input.IsDeleted)
                    .WhereIf(
                        !input.Location.IsNullOrEmpty(),
                        p => p.Locations.Contains(input.Location))
                    .WhereIf(
                        !input.Filter.IsNullOrWhiteSpace(),
                        p => p != null && (p.Name.Contains(input.Filter))
                    );
                var allMyEnItems = SearchLocation(allItems, input.LocationGroup).OfType<PriceTag>();

                var allItemCount = allMyEnItems.Count();

                var sortMenuItems = allMyEnItems.AsQueryable()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList();

                var allListDtos = sortMenuItems.MapTo<List<PriceTagListDto>>();

                foreach (var pdtl in allListDtos)
                    if (!string.IsNullOrEmpty(pdtl.Locations))
                    {
                        var tL = JsonConvert.DeserializeObject<List<LocationListDto>>(pdtl.Locations);
                        var builder = new StringBuilder();
                        foreach (var ldt in tL)
                        {
                            if (builder.Length > 0) builder.Append(",");
                            builder.Append(ldt.Name);
                        }

                        pdtl.Locations = builder.ToString();
                    }
                    else
                    {
                        pdtl.Locations = "ALL";
                    }


                return new PagedResultOutput<PriceTagListDto>(
                    allItemCount,
                    allListDtos
                );
            }
        }

        public async Task<FileDto> GetAllToExcel(GetPriceTagInput input)
        {
            var allList = await _pricetagRepo
                .GetAll()
                .ToListAsync();

            var allListDtos = allList.MapTo<List<PriceTagListDto>>();
            var baseE = new BaseExportObject
            {
                ExportObject = allListDtos,
                ColumnNames = new[] { "Id", "Name" }
            };
            return _exporter.ExportToFile(baseE, L("PriceTag"));
        }

        public async Task UpdateTagPrice(TagPriceInput input)
        {
            var portion = await _menuItemPortionManager.GetAsync(input.PortionId);
            var locationPrice = portion.PriceTags.SingleOrDefault(a => a.TagId.Equals(input.TagId));

            if (locationPrice != null)
                locationPrice.Price = input.Price;
            else
                portion.PriceTags.Add(new PriceTagDefinition
                {
                    TagId = input.TagId,
                    Price = input.Price,
                });
            await _syncAppService.UpdateSync(SyncConsts.PRICETAG);
        }

        public async Task<TagPriceOutputDto> GetTagForLocation(ApiLocationInput input)
        {
            return await ApiGetTags(input);
        }

        public async Task<TagPriceOutputDto> ApiGetTags(ApiLocationInput input)
        {
            var orgId = await _locAppService.GetOrgnizationIdForLocationId(input.LocationId);
            if (orgId > 0) CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);

            var output = new List<TagPriceDto>();

            var rOutput = new TagPriceOutputDto
            {
                Tags = output
            };

            var listP = await _pricetagRepo.GetAllListAsync(a => a.TenantId == input.TenantId && a.FutureData == false);

            if (!string.IsNullOrEmpty(input.Tag))
            {
                var allLocations = await _locAppService.GetLocationsByLocationGroupCode(input.Tag);
                if (allLocations != null && allLocations.Items.Any())
                    foreach (var repoTag in listP)
                        if (!repoTag.Group && !string.IsNullOrEmpty(repoTag.Locations) && !repoTag.LocationTag)
                        {
                            var scLocations =
                                JsonConvert.DeserializeObject<List<SimpleLocationGroupDto>>(repoTag.Locations);
                            if (scLocations.Any())
                            {
                                foreach (var i in scLocations.Select(a => a.Id))
                                    if (allLocations.Items.Select(a => a.Value).Contains(i.ToString()))
                                    {
                                        var tPrice = new TagPriceDto
                                        {
                                            Id = repoTag.Id,
                                            Name = repoTag.Name,
                                            Type = repoTag.Type,
                                            StartDate = repoTag.StartDate,
                                            EndDate = repoTag.EndDate
                                        };
                                        var pDefinitions = await _priceTagDef.GetAllListAsync(a => a.TagId == repoTag.Id);
                                        if (pDefinitions.Any())
                                        {
                                            tPrice.Definitions = pDefinitions.Select(a => new TagDefinitionDto
                                            {
                                                TagName = a.PriceTag.Name,
                                                MenuItemPortionId = a.MenuPortionId,
                                                Price = a.Price,
                                                TagId = a.Id
                                            }).ToList();
                                            output.Add(tPrice);
                                        }
                                    }
                            }
                            else
                            {
                                var tPrice = new TagPriceDto
                                {
                                    Id = repoTag.Id,
                                    Name = repoTag.Name,
                                    Type = repoTag.Type,
                                    StartDate = repoTag.StartDate,
                                    EndDate = repoTag.EndDate
                                };
                                var pDefinitions = await _priceTagDef.GetAllListAsync(a => a.TagId == repoTag.Id);
                                if (pDefinitions.Any())
                                {
                                    tPrice.Definitions = pDefinitions.Select(a => new TagDefinitionDto
                                    {
                                        TagName = a.PriceTag.Name,
                                        MenuItemPortionId = a.MenuPortionId,
                                        Price = a.Price,
                                        TagId = a.Id
                                    }).ToList();
                                    output.Add(tPrice);
                                }
                            }
                        }
            }
            else
            {
                foreach (var priceTag in listP)
                {
                    var fetch = await _locAppService.IsLocationExists(new CheckLocationInput
                    {
                        LocationId = input.LocationId,
                        Locations = priceTag.Locations,
                        Group = priceTag.Group,
                        NonLocations = priceTag.NonLocations,
                        LocationTag = priceTag.LocationTag

                    });
                    if (fetch)
                    {
                        var tPrice = new TagPriceDto
                        {
                            Id = priceTag.Id,
                            Name = priceTag.Name,
                            Type = priceTag.Type,
                            StartDate = priceTag.StartDate,
                            EndDate = priceTag.EndDate
                        };
                        var pDefinitions = await _priceTagDef.GetAllListAsync(a => a.TagId == priceTag.Id);
                        if (pDefinitions.Any())
                        {
                            tPrice.Definitions = pDefinitions.Select(a => new TagDefinitionDto
                            {
                                TagName = a.PriceTag.Name,
                                MenuItemPortionId = a.MenuPortionId,
                                Price = a.Price,
                                TagId = a.Id
                            }).ToList();
                            output.Add(tPrice);
                        }
                    }
                }
            }


            var myLocationTags = _tagLocationPrice.GetAll()
                .Where(a=>a.LocationId == input.LocationId);
            var myTagPriceCount= myLocationTags.Count(a => a.LocationId == input.LocationId);
            if (myTagPriceCount > 0)
            {
                foreach (var tagDto in rOutput.Tags.SelectMany(a=>a.Definitions))
                {
                    var myTag = myLocationTags.Where(a => a.PriceTagDefinitionId == tagDto.TagId);
                    if (myTag.Any())
                    {

                        var myTagList = myTag.ToList();
                        if (myTagList.Any())
                            tagDto.Price = myTagList.Last().Price;
                    }
                }
            }

            return rOutput;
        }

        public async Task<TagPriceDto> ApiGetTagByLocationAndId(ApiLocationInput locationInput)
        {
            var orgId = await _locAppService.GetOrgnizationIdForLocationId(locationInput.LocationId);
            if (orgId > 0) CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);

            var output = new TagPriceDto();
            if (locationInput.PriceTagId > 0)
            {
                var tagInput = await _pricetagRepo.FirstOrDefaultAsync(a => a.Id == locationInput.PriceTagId);
                if (tagInput == null)
                    return output;

                output.Name = tagInput.Name;
                output.Id = tagInput.Id;
                var defi = await _priceTagDef.GetAllListAsync(a => a.TagId == tagInput.Id);
                if (defi.Any())
                    output.Definitions = defi.Select(a => new TagDefinitionDto
                    {
                        TagName = a.PriceTag.Name,
                        MenuItemPortionId = a.MenuPortionId,
                        Price = a.Price
                    }).ToList();
            }

            return output;
        }

        public async Task<TagPriceDto> GetTagsById(int priceTagId)
        {
            var output = new TagPriceDto();
            if (priceTagId > 0)
            {
                var tagInput = await _pricetagRepo.FirstOrDefaultAsync(a => a.Id == priceTagId);
                if (tagInput == null)
                    return output;

                output.Name = tagInput.Name;
                output.Id = tagInput.Id;
                var defi = await _priceTagDef.GetAllListAsync(a => a.TagId == tagInput.Id);
                if (defi.Any())
                    output.Definitions = defi.Select(a => new TagDefinitionDto
                    {
                        TagName = a.PriceTag.Name,
                        MenuItemPortionId = a.MenuPortionId,
                        Price = a.Price
                    }).ToList();
            }

            return output;
        }

        public async Task<FileDto> GetImportTemplate(string name)
        {
            var myId = await GetTagId(name);
            if (myId > 0)
            {
                var input = new GetPriceTagInput
                {
                    TagId = myId
                };
                return await _exExporter.ExportImportTemplate(input, this);
            }

            return null;
        }

        public async Task<GetPriceTagForEditOutput> GetPriceTagForEdit(NullableIdInput input)
        {
            var output = new GetPriceTagForEditOutput();
            PriceTagEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _pricetagRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<PriceTagEditDto>();
            }
            else
            {
                editDto = new PriceTagEditDto();
            }

            UpdateLocationAndNonLocationForEdit(editDto, output.LocationGroup);

            output.PriceTag = editDto;
            return output;
        }

        public async Task CreateOrUpdatePriceTag(CreateOrUpdatePriceTagInput input)
        {
            if (input?.LocationGroup == null)
                return;

            if (input.PriceTag.Id.HasValue)
                await UpdatePriceTag(input);
            else
                await CreatePriceTag(input);
            await _syncAppService.UpdateSync(SyncConsts.PRICETAG);
        }

        public async Task DeletePriceTag(IdInput input)
        {
            await _pricetagRepo.DeleteAsync(input.Id);
        }

        public async Task<ListResultOutput<PriceTagListDto>> GetIds()
        {
            var lstPriceTag = await _pricetagRepo.GetAll().ToListAsync();
            return new ListResultOutput<PriceTagListDto>(lstPriceTag.MapTo<List<PriceTagListDto>>());
        }

        public async Task<PagedResultOutput<TagMenuItemPortionDto>> GetMenuPricesForTag(GetPriceTagInput input)
        {
            if (input.TagId > 0)
            {
                var allItems = _miManager
                    .GetAll()
                    .WhereIf(
                        !input.Filter.IsNullOrEmpty(),
                        p => p.Name.Contains(input.Filter)
                 );


                var returnList = allItems.SelectMany(a => a.Portions).Select(mip => new TagMenuItemPortionDto
                {
                    Id = mip.Id,
                    MenuName = mip.MenuItem.Name,
                    PortionName = mip.Name,
                    MenuItemId = mip.MenuItemId,
                    MultiPlier = mip.MultiPlier,
                    TagId = input.TagId,
                    Price = mip.Price,
                    TagPrice =
                        mip.PriceTags.Any(a => a.TagId.Equals(input.TagId))
                            ? mip.PriceTags.FirstOrDefault(a => a.TagId.Equals(input.TagId)).Price
                            : 0M
                }).AsQueryable();

                var allItemCount = await allItems.CountAsync();


                if (!input.Sorting.IsNullOrEmpty()) returnList = returnList.OrderBy(input.Sorting);
                if (input.SkipCount > 0) returnList = returnList.Skip(input.SkipCount);

                if (input.MaxResultCount > 0) returnList = returnList.Take(input.MaxResultCount);


                var listToBeReturned = returnList.MapTo<List<TagMenuItemPortionDto>>();
                var rspricetagdefinition=await _priceTagDef.GetAllListAsync();
                foreach (var lst in listToBeReturned)
                {
                    var rspricetagdef=rspricetagdefinition.FirstOrDefault(t=>t.TagId==lst.TagId&&t.Price==lst.TagPrice&&t.MenuPortionId==lst.Id);
                    if (rspricetagdef != null)
                    {
                        lst.PriceTagDefinitionId = rspricetagdef.Id;
                    }
                }                
                return new PagedResultOutput<TagMenuItemPortionDto>(
                    allItemCount,
                    listToBeReturned
                );
            }

            return null;
        }

        public async Task<ListResultOutput<PriceTagListDto>> GetPriceTagNames(bool futureData = false)
        {
            var returnList = await _pricetagRepo.GetAll().Where(t => t.FutureData == futureData)
                .ToListAsync();

            return new ListResultOutput<PriceTagListDto>(returnList.MapTo<List<PriceTagListDto>>());
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _pricetagRepo.GetAsync(input.Id);
                item.IsDeleted = false;

                await _pricetagRepo.UpdateAsync(item);
            }
        }

        public async Task<int> GetTagId(string tagName)
        {
            var allList = await _pricetagRepo.GetAllListAsync(a => a.Name.Equals(tagName));
            if (allList.Any()) return allList.First().Id;
            return 0;
        }

        protected virtual async Task UpdatePriceTag(CreateOrUpdatePriceTagInput input)
        {
            var item = await _pricetagRepo.GetAsync(input.PriceTag.Id.Value);
            var dto = input.PriceTag;
            item.Name = dto.Name;
            item.Type = 1;
            item.StartDate = dto.StartDate;
            item.EndDate = dto.EndDate;
            item.FutureData = dto.FutureData;
            UpdateLocationAndNonLocation(item, input.LocationGroup);
        }

        protected virtual async Task CreatePriceTag(CreateOrUpdatePriceTagInput input)
        {
            var dto = input.PriceTag.MapTo<PriceTag>();

            if (dto.StartDate == DateTime.MinValue)
                dto.StartDate = DateTime.Now;

            if (dto.EndDate == DateTime.MinValue)
                dto.EndDate = DateTime.Now;
            UpdateLocationAndNonLocation(dto, input.LocationGroup);
            await _pricetagRepo.InsertAndGetIdAsync(dto);
        }
        public async Task<PagedResultOutput<PriceTagLocationPriceListDto>> GetAllPriceTagLocationPrice(GetPriceTagInput input)
        {

            var allItems = await _pricetagLocationPriceRepo.GetAllListAsync(t=>t.PriceTagDefinitionId==input.TagDefinitionId);
            var allItemCount = allItems.Count();

            var sortMenuItems = allItems.AsQueryable()
                    .PageBy(input)
                    .ToList();

            var allListDtos = sortMenuItems.MapTo<List<PriceTagLocationPriceListDto>>();

            var rslocation=await _locationRepo.GetAllListAsync();
            foreach (var lst in allListDtos)
            {
                if (lst.LocationId > 0)
                {
                    var location=rslocation.FirstOrDefault(t=>t.Id==lst.LocationId);
                    if (location != null)
                    {
                        lst.LocationName = location.Name;
                    }
                }
            }
            allListDtos = allListDtos.OrderBy(input.Sorting).ToList();
            return new PagedResultOutput<PriceTagLocationPriceListDto>(
                allItemCount,
                allListDtos
            );
        }
        public async Task<GetPriceTagLocationPriceForEditOutput> GetPriceTagLocationPriceForEdit(NullableIdInput input)
        {
            var output = new GetPriceTagLocationPriceForEditOutput();
            PriceTagLocationPriceEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _pricetagLocationPriceRepo.GetAllListAsync(t=>t.PriceTagDefinitionId==input.Id.Value);
                editDto = hDto.MapTo<PriceTagLocationPriceEditDto>();

            }
            else
            {
                editDto = new PriceTagLocationPriceEditDto();
            }
            output.PriceTagLocationPrice = editDto;
            return output;
        }

        public async Task CreatePriceTagLocationPrice(CreatePriceTagLocationPrice input)
        {
            if (input.PriceTagLocationPrice.Id.HasValue)
            {
                var pricetaglocationprice = input.PriceTagLocationPrice;
                var existEntry = await _pricetagLocationPriceRepo.FirstOrDefaultAsync(t => t.Id == pricetaglocationprice.Id.Value);
                pricetaglocationprice.MapTo(existEntry);
                await _pricetagLocationPriceRepo.UpdateAsync(existEntry);
            }
            else
            {
                var dto = input.PriceTagLocationPrice.MapTo<PriceTagLocationPrice>();
                await _pricetagLocationPriceRepo.InsertAndGetIdAsync(dto);
            }
        }
        public async Task DeletePriceTagLocationPrice(IdInput input)
        {
            await _pricetagLocationPriceRepo.DeleteAsync(input.Id);
        }

        public async Task<int> UpdateTagPriceInPricetagDefinition(TagPriceInput input)
        {
            int Id;
            var rspricetagdefinition=await _priceTagDef.FirstOrDefaultAsync(t=>t.TagId==input.TagId&& t.Price==input.Price&&t.MenuPortionId==input.PortionId);
            if (rspricetagdefinition != null)
            {
                rspricetagdefinition.TagId = input.TagId;
                rspricetagdefinition.Price = input.Price;
                rspricetagdefinition.MenuPortionId = input.PortionId;
                await _priceTagDef.UpdateAsync(rspricetagdefinition);
                Id = rspricetagdefinition.Id;
            }
            else
            {
                PriceTagDefinition pricetagDefinition=new PriceTagDefinition
                {
                    TagId = input.TagId,
                    Price = input.Price,
                    MenuPortionId=input.PortionId
                };
               var retId= await _priceTagDef.InsertAndGetIdAsync(pricetagDefinition);
                Id = retId;
            }

            return Id;
        }

    }
}