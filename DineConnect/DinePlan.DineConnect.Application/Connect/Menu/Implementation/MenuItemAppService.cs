﻿using Abp.Application.Features;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Localization;
using Abp.UI;
using AutoMapper;
using Castle.Core.Logging;
using DinePlan.DineConnect.Addons.Dto;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.OrderTag;
using DinePlan.DineConnect.Connect.OrderTag.Dtos;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Connect.Taxes;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Features;
using DinePlan.DineConnect.General;
using DinePlan.DineConnect.House.Master;
using DinePlan.DineConnect.Session;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Utility.Exporter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using AutoMapper.Internal;
using static DinePlan.DineConnect.Connect.Ticket.Implementation.ConnectReportAppService;

namespace DinePlan.DineConnect.Connect.Menu.Implementation
{
    /// <summary>
    ///     MenuItemAppService --> For Creating the Managing the MenuItems
    /// </summary>

    public class MenuItemAppService : DineConnectAppServiceBase, IMenuItemAppService
    {
        #region Constructor

        private readonly IRepository<ProductComboGroup> _comboGManager;
        private readonly IRepository<ProductComboItem> _comboIManager;
        private readonly IRepository<ProductCombo> _comboManager;
        private readonly IExcelExporter _excelExporter;
        private readonly ILocationAppService _locationAppService;
        private readonly IRepository<LocationMenuItem> _locationItemManager;
        private readonly IRepository<LocationMenuItemPrice> _locationPriceManager;
        private readonly ILogger _logger;
        private readonly IMaterialAppService _materialAppService;
        private readonly IRepository<MenuBarCode> _mbarRepo;
        private readonly IRepository<MenuItemDescription> _menuItemDescriptionRepository;
        private readonly IMenuItemListExcelExporter _menuitemExporter;
        private readonly IRepository<MenuItem> _menuitemManager;
        private readonly IRepository<MenuItemPortion> _menuItemPortionManager;
        private readonly IRepository<MenuItemSchedule> _menuItemSchedule;
        private readonly IMenuItemManager _miManager;
        private readonly IOrderTagGroupAppService _orderTagGroupService;
        private readonly IRepository<ScreenMenuItem> _screenItemManager;
        private readonly ISyncAppService _syncAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<UpMenuItem> _upMenuItem;
        private readonly FeatureChecker _featureChecker;
        private readonly ILocalizationManager _localizationManager;
        private readonly IRepository<UpMenuItemLocationPrice> _upMenuItemLocationPriceRepo;
        private readonly IRepository<Master.Location> _locationRepo;
        private readonly IRepository<Order> _orderRepository;
        private readonly IConnectReportAppService _connectReportAppService;
        public ConnectSession ConnectSession { get; set; }

        /// <summary>
        ///     Initializes a new instance of the <see cref="MenuItemAppService" /> class.
        /// </summary>
        /// 
		public MenuItemAppService(IRepository<MenuItem> menuitemManager, IRepository<ScreenMenuItem> screenItemManager,
            IRepository<MenuItemPortion> menuItemPortionManager, FeatureChecker featureChecker,
            IRepository<LocationMenuItemPrice> locationPriceManager,

            IRepository<ProductCombo> comboManager,
            IRepository<ProductComboGroup> comboGManager,
            IRepository<ProductComboItem> comboIManager,
            IRepository<LocationMenuItem> locationManager,
            IRepository<MenuBarCode> barRepo,
            IRepository<UpMenuItem> upMenuItem,
            IRepository<MenuItemSchedule> menuItemSchdule,
            ISyncAppService syncAppService,
            IOrderTagGroupAppService orderTagGroupService,
            ILocationAppService locationAppService,
            IMenuItemListExcelExporter menuitemExporter,
            IExcelExporter exporter,
            IMenuItemManager miManager,
            ILogger logger,
            IMaterialAppService materialAppService,
            IRepository<MenuItemDescription> menuItemDescriptionRepository,
            IUnitOfWorkManager unitOfWorkManager,
            ILocalizationManager localizationManager,
            IRepository<UpMenuItemLocationPrice> upMenuItemLocationPriceRepo,
            IRepository<Master.Location> locationRepo,
            IRepository<Order> orderRepository,
            IConnectReportAppService connectReportAppService
        )
        {
            _menuitemManager = menuitemManager;
            _menuitemExporter = menuitemExporter;
            _menuItemPortionManager = menuItemPortionManager;
            _comboManager = comboManager;
            _comboGManager = comboGManager;
            _comboIManager = comboIManager;
            _locationItemManager = locationManager;
            _locationPriceManager = locationPriceManager;
            _syncAppService = syncAppService;
            _orderTagGroupService = orderTagGroupService;
            _locationAppService = locationAppService;
            _excelExporter = exporter;
            _logger = logger;
            _miManager = miManager;
            _upMenuItem = upMenuItem;
            _mbarRepo = barRepo;
            _materialAppService = materialAppService;
            _menuItemDescriptionRepository = menuItemDescriptionRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _localizationManager = localizationManager;
            _menuItemSchedule = menuItemSchdule;
            _screenItemManager = screenItemManager;
            _featureChecker = featureChecker;
            _upMenuItemLocationPriceRepo = upMenuItemLocationPriceRepo;
            _locationRepo = locationRepo;
            _orderRepository = orderRepository;
            _connectReportAppService = connectReportAppService;
        }
        #endregion

        #region ForSync
        public async Task<List<MenuItem>> ApiGetAllMenuItemsForSync(ApiLocationInput input)
        {
            var orgId = await _locationAppService.GetOrgnizationIdForLocationId(input.LocationId);
            if (orgId > 0) CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);

            return await _menuitemManager.GetAll().Where(a => a.TenantId == input.TenantId)
                .Include(a => a.Portions.Select(av => av.LocationPrices))
                .Include(a => a.Portions.Select(av => av.PriceTags))

                .Include(a => a.MenuItemDescriptions)
                .Include(a => a.MenuItemSchedules)
                .Include(a => a.UpMenuItems)
                .Include(a => a.Barcodes)
                .Include(a => a.Category)
                .Include(a => a.Category.ProductGroup)

                .ToListAsync();
        }

        public async Task<List<ProductCombo>> ApiGetAllComboesForSync(ApiLocationInput input)
        {
            var orgId = await _locationAppService.GetOrgnizationIdForLocationId(input.LocationId);
            if (orgId > 0) CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);
            return await _comboManager.GetAll().Include(a => a.ComboGroups).ToListAsync();
        }
        #endregion
        /// <summary>
        ///     Method to get all the MenuItems for Index View <see cref="MenuItemAppService" /> class.
        /// </summary>

        public async Task<PagedResultOutput<MenuItemListDto>> GetAll(GetMenuItemInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var allItems = _menuitemManager
                    .GetAll()
                    .Where(i => i.IsDeleted == input.IsDeleted)
                    .WhereIf(
                        !input.Filter.IsNullOrWhiteSpace(),
                        p => p != null && (p.Name.Contains(input.Filter) ||
                                           p.AliasName != null && p.AliasName.Contains(input.Filter) ||
                                           p.AliasCode != null && p.AliasCode.Contains(input.Filter) ||
                                           p.Category != null && p.Category.Name.Contains(input.Filter))
                    );


                if (!string.IsNullOrEmpty(input.CategoryName))
                    allItems = allItems.Where(a => a.Category.Name.Equals(input.CategoryName));
                if (input.CategoryId > 0)
                {
                    allItems = allItems.Where(a => a.CategoryId == input.CategoryId);
                }
                if (input.MenuItemType > 0) allItems = allItems.Where(a => a.ProductType == input.MenuItemType);


                var sortMenuItems = allItems
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList();
                var allItemCount = allItems.Count();


                if (input.LocationGroup != null && (input.LocationGroup.Locations.Any() || input.LocationGroup.LocationTags.Any() || input.LocationGroup.Groups.Any() || input.LocationGroup.NonLocations.Any()))
                {
                    var allMyEnItems = SearchLocation(allItems, input.LocationGroup).OfType<MenuItem>();
                    allItemCount = allMyEnItems.Count();
                    sortMenuItems = allMyEnItems.AsQueryable()
                       .OrderBy(input.Sorting)
                       .PageBy(input)
                       .ToList();
                }

                var allItemDtos = sortMenuItems.MapTo<List<MenuItemListDto>>();

                return new PagedResultOutput<MenuItemListDto>(
                    allItemCount,
                    allItemDtos
                );
            }
        }

        /// <summary>
        ///     Method to get all Non Combo Items <see cref="MenuItemAppService" /> class.
        /// </summary>
        /// 
		public async Task<PagedResultOutput<IdNameDto>> GetNonComboItems(GetMenuItemInput input)
        {
            var allItems = _menuitemManager
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrWhiteSpace(),
                    p => p != null && (p.Name.Contains(input.Filter) ||
                                       p.AliasName != null && p.AliasName.Contains(input.Filter) ||
                                       p.AliasCode != null && p.AliasCode.Contains(input.Filter) ||
                                       p.Category != null && p.Category.Name.Contains(input.Filter))
                );

            if (!string.IsNullOrEmpty(input.CategoryName))
                allItems = allItems.Where(a => a.Category.Name.Equals(input.CategoryName));
            allItems = allItems.Where(a => a.ProductType == 1);

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var namesDto = sortMenuItems.Select(sortItems => new IdNameDto
            {
                Id = sortItems.Id,
                Name = sortItems.Name
            }).ToList();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<IdNameDto>(
                allItemCount,
                namesDto
            );
        }

        /// <summary>
        ///     Method to get all the MenuItems based on Category Id <see cref="MenuItemAppService" /> class.
        /// </summary>
        public async Task<PagedResultOutput<IdNameDto>> GetAllMenuItemsBasedOnCatgeoryId(GetMenuItemInput input)
        {
            var allItems = _menuitemManager
                .GetAll()
                .Where(t => t.CategoryId == input.CategoryId)
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Name.Contains(input.Filter)
                ).Select(c => new IdNameDto
                {
                    Id = c.Id,
                    Name = c.Name
                });

            var allMyItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<IdNameDto>(
                allItemCount,
                allMyItems
            );
        }

        public async Task<PagedResultOutput<IdNameDto>> GetAllMenuItems(GetMenuItemInput input)
        {
            if (input.LocationId > 0)
            {
                var orgId = await _locationAppService.GetOrgnizationIdForLocationId(input.LocationId);
                if (orgId > 0) CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);
            }

            var allItems = _menuitemManager
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrWhiteSpace(),
                    p => p != null && (p.Name.Contains(input.Filter) ||
                                       p.AliasName != null && p.AliasName.Contains(input.Filter) ||
                                       p.AliasCode != null && p.AliasCode.Contains(input.Filter) ||
                                       p.Category != null && p.Category.Name.Contains(input.Filter))
                );

            if (!string.IsNullOrEmpty(input.CategoryName))
                allItems = allItems.Where(a => a.Category.Name.Equals(input.CategoryName));

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var namesDto = sortMenuItems.Select(sortItems => new IdNameDto
            {
                Id = sortItems.Id,
                Name = sortItems.Name
            }).ToList();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<IdNameDto>(
                allItemCount,
                namesDto
            );
        }
        public async Task<PagedResultOutput<IdNameDto>> GetAllMenuItemPortions(GetMenuItemInput input)
        {
            var allItems = _menuitemManager
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrWhiteSpace(),
                    p => p != null && (p.Name.Contains(input.Filter) ||
                                       p.AliasName != null && p.AliasName.Contains(input.Filter) ||
                                       p.AliasCode != null && p.AliasCode.Contains(input.Filter) ||
                                       p.Category != null && p.Category.Name.Contains(input.Filter))
                );

            if (!string.IsNullOrEmpty(input.CategoryName))
                allItems = allItems.Where(a => a.Category.Name.Equals(input.CategoryName));

            if (input.CategoryId > 0)
            {
                allItems = allItems.Where(a => a.CategoryId == input.CategoryId);
            }


            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var namesDto = sortMenuItems.SelectMany(a => a.Portions).Select(sortItems => new IdNameDto
            {
                Id = sortItems.Id,
                Name = sortItems.MenuItem.Name + "(" + sortItems.Name + ")",
                MenuItemId = sortItems.MenuItemId
            }).ToList();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<IdNameDto>(
                allItemCount,
                namesDto
            );
        }
        public async Task<PagedResultOutput<IdNameDto>> GetAllMenuItemPortionsBasedOnMenuItemId(GetMenuItemInput input)
        {
            var allItems = _menuItemPortionManager
                .GetAll()
                .Where(t => t.MenuItemId == input.MenuItemId)
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Name.Contains(input.Filter)
                ).Select(c => new IdNameDto
                {
                    Id = c.Id,
                    Name = c.MenuItem.Name + "(" + c.Name + ")"
                });

            var allMyItems = await allItems
               .OrderBy(input.Sorting)
               .PageBy(input)
               .ToListAsync();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<IdNameDto>(
                allItemCount,
                allMyItems
                );
        }
        public async Task<PagedResultOutput<MenuItemListDto>> GetLocationMenuItems(GetMenuItemLocationInput input)
        {
            var retuDto = new PagedResultOutput<MenuItemListDto>();

            var returnList = await _locationItemManager.GetAllListAsync(a => a.LocationId.Equals(input.LocationId));

            var menuItems = returnList.Select(a => a.MenuItem);
            var allItems = menuItems.AsQueryable()
                .WhereIf(
                        !input.Filter.IsNullOrWhiteSpace(),
                        p => p != null && (p.Name.IndexOf(input.Filter, StringComparison.OrdinalIgnoreCase) >= 0 ||
                                           p.AliasName != null && p.AliasName.IndexOf(input.Filter, StringComparison.OrdinalIgnoreCase) >= 0 ||
                                           p.AliasCode != null && p.AliasCode.IndexOf(input.Filter, StringComparison.OrdinalIgnoreCase) >= 0 ||
                                           p.Category != null && p.Category.Name.IndexOf(input.Filter, StringComparison.OrdinalIgnoreCase) >= 0)
                    );

            retuDto.Items = allItems.MapTo<List<MenuItemListDto>>();
            retuDto.TotalCount = allItems.Count();
            return retuDto;
        }

        public async Task<PagedResultOutput<MenuItemPortionDto>> GetLocationMenuItemPrices(GetMenuPortionPriceInput input)
        {
            if (input.LocationId > 0)
            {
                var orgId = await _locationAppService.GetOrgnizationIdForLocationId(input.LocationId);
                if (orgId > 0) CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);

                var menuItems = await GetAllListItems(new GetMenuItemLocationInput
                {
                    LocationId = input.LocationId,
                    Filter = input.Filter,
                    MaxResultCount = input.MaxResultCount,
                    SkipCount = input.SkipCount,
                    Sorting = input.Sorting
                });

                var returnList = menuItems.Items
                    .SelectMany(a => a.Portions).Select(mip => new MenuItemPortionDto
                    {
                        Id = mip.Id,
                        MenuName = _menuitemManager.FirstOrDefault(mip.MenuItemId).Name,
                        PortionName = mip.Name,
                        MenuItemId = mip.MenuItemId,
                        MultiPlier = mip.MultiPlier,
                        LocationId = input.LocationId,
                        Price = mip.Price,
                        LocationPrice =
                            _locationPriceManager.FirstOrDefault(
                                a => a.MenuPortionId == mip.Id && a.LocationId == input.LocationId)
                            != null
                                ? _locationPriceManager.FirstOrDefault(
                                    a => a.MenuPortionId == mip.Id && a.LocationId == input.LocationId).Price
                                : 0M
                    });

                var sortMenuItems = returnList.AsQueryable()
                    .PageBy(input)
                    .ToList();
                var allItemCount = returnList.Count();

                return new PagedResultOutput<MenuItemPortionDto>(allItemCount, sortMenuItems);
            }

            return null;
        }

        public async Task UpdateLocationPrice(List<LocationPriceInput> inputs)
        {
            foreach (var input in inputs)
            {
                var portion = await _menuItemPortionManager.GetAsync(input.PortionId);
                var locationPrice = portion.LocationPrices.SingleOrDefault(a => a.LocationId.Equals(input.LocationId));

                if (locationPrice != null)
                    locationPrice.Price = input.Price;
                else
                    portion.LocationPrices.Add(new LocationMenuItemPrice
                    {
                        LocationId = input.LocationId,
                        Price = input.Price
                    });
            }
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _menuitemManager.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<MenuItemListDto>>();
            return _menuitemExporter.ExportToFile(allListDtos.OrderBy(a => a.CategoryName).ToList());
        }

        public async Task<GetMenuItemForEditOutput> GetMenuItemForEdit(NullableIdInput input)
        {
            var output = new GetMenuItemForEditOutput();

            var editDto = new MenuItemEditDto();
            var productType = 1;
            var combs = new List<ProductComboGroupEditDto>();

            if (input.Id.HasValue)
            {
                var menuItem = await _menuitemManager.GetAll()
                    .Include(e => e.MenuItemDescriptions)
                    .Include(e => e.MenuItemSchedules)
                    .Include(e => e.UpMenuItems)
                    .Include(e => e.Portions)
                    .Include(e => e.Barcodes)
                    .FirstOrDefaultAsync(e => e.Id == input.Id.Value);
                editDto = menuItem.MapTo<MenuItemEditDto>();
                productType = menuItem.ProductType;

                if (editDto.UpMenuItems.Any())
                {
                    var comboItems =
                        new Collection<UpMenuItemEditDto>(editDto.UpMenuItems.Where(a => a.ProductType == 2).ToList());
                    var singleItems =
                        new Collection<UpMenuItemEditDto>(editDto.UpMenuItems.Where(a => a.ProductType == 1).ToList());

                    editDto.UpMenuItems = comboItems;
                    editDto.UpSingleMenuItems = singleItems;

                    foreach (var single in editDto.UpSingleMenuItems)
                        if (single.RefMenuItemId > 0)
                        {
                            var mItem = _menuitemManager.Get(single.RefMenuItemId);
                            if (mItem != null)
                                single.Name = mItem.Name;
                        }
                }

                if (productType == 2)
                {
                    var productCombo = await _comboManager.FirstOrDefaultAsync(a => a.MenuItemId == menuItem.Id);
                    if (productCombo != null)
                    {
                        combs = productCombo.ComboGroups.OrderBy(t => t.SortOrder).MapTo<List<ProductComboGroupEditDto>>();
                        if (!string.IsNullOrEmpty(productCombo.Sorting))
                        {
                            var sortedIds = productCombo.Sorting.Split(',').ToList();
                            if (sortedIds.Any())
                            {
                                foreach (var id in sortedIds)
                                {
                                    var idInt = Convert.ToInt32(id);
                                    var g = combs.FirstOrDefault(x => x.Id == idInt);
                                    if (g != null)
                                    {
                                        g.CustomSortOrder = sortedIds.IndexOf(id);
                                    }
                                }
                                combs = combs.OrderBy(x => x.CustomSortOrder).ToList();
                            }
                        }
                    }

                }
            }

            foreach (var edi in editDto.MenuItemSchedules)
            {
                edi.AllDays = new List<ComboboxItemDto>();

                if (!string.IsNullOrEmpty(edi.Days))
                    foreach (var day in edi.Days.Split(","))
                    {
                        var myday = (DayOfWeek)Convert.ToInt32(day);
                        edi.AllDays.Add(new ComboboxItemDto
                        {
                            DisplayText = myday.ToString(),
                            Value = day
                        });
                    }
            }

            UpdateLocationAndNonLocationForEdit(editDto, output.LocationGroup);

            output.MenuItem = editDto;
            output.ProductType = productType;
            output.Combo = combs;

            if (AbpSession.TenantId.HasValue)
            {
                if (await _featureChecker.IsEnabledAsync(AbpSession.TenantId ?? 0,
                    AppFeatures.UrbanPiper))
                {
                    UrbanPiperMenuAddOn uAddOn = null;
                    AddOnOutput aAddOn = null;

                    aAddOn = new AddOnOutput();
                    if (string.IsNullOrEmpty(editDto.AddOns))
                    {
                        uAddOn = new UrbanPiperMenuAddOn();
                        aAddOn.UrbanPiperMenuAddOn = uAddOn;
                    }
                    else
                    {
                        aAddOn = JsonConvert.DeserializeObject<AddOnOutput>(editDto.AddOns);
                        uAddOn = aAddOn.UrbanPiperMenuAddOn;
                    }
                    editDto.AddOns = JsonConvert.SerializeObject(aAddOn);
                    output.UrbanPiper = uAddOn;
                }
            }

            return output;
        }

        public async Task CreateOrUpdateMenuItem(CreateOrUpdateMenuItemInput input)
        {
            if (ConnectSession.OrgId > 0)
                CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", ConnectSession.OrgId);

            var output = 0;
            if (input.ProductType == 2 && input.MenuItem.Portions.Any() && input.MenuItem.Portions.Count() > 1)
                input.MenuItem.Portions = new Collection<MenuItemPortionEditDto> { input.MenuItem.Portions[0] };

            if (input.ProductType == 1 && input.MenuItem.UpMenuItems.Any())
                input.MenuItem.UpMenuItems =
                    new Collection<UpMenuItemEditDto>(
                        input.MenuItem.UpMenuItems.Where(a => a.RefMenuItemId > 0).ToList());
            if (input.ProductType == 1 && input.MenuItem.UpSingleMenuItems.Any())
                input.MenuItem.UpSingleMenuItems =
                    new Collection<UpMenuItemEditDto>(
                        input.MenuItem.UpSingleMenuItems.Where(a => a.RefMenuItemId > 0).ToList());

            input.MenuItem.ProductType = input.ProductType;

            if (input.MenuItem.Id.HasValue)
            {
                if (await _featureChecker.IsEnabledAsync(AbpSession.TenantId ?? 0, AppFeatures.UrbanPiper))
                {
                    if (input.UrbanPiper != null)
                    {
                        AddOnOutput addOnOutput = new AddOnOutput { UrbanPiperMenuAddOn = input.UrbanPiper };
                        input.MenuItem.AddOns = JsonConvert.SerializeObject(addOnOutput);
                    }
                }

                await UpdateMenuItem(input);
                output = input.MenuItem.Id.Value;
            }
            else
            {
                var menuName = input.MenuItem.Name.ToUpper();
                var manager =
                    _menuitemManager.GetAll().Where(a => a.Name.ToUpper().Equals(menuName));

                if (input.NotOverride && manager.Any())
                {
                    var firstItem = manager.First();
                    input.MenuItem.Id = firstItem.Id;
                    return;
                }
                output = await CreateMenuItem(input);
            }

            if (input.MenuItemLocationPrices.Any())
                await UpdateLocationPriceForMenu(input.MenuItemLocationPrices, input.MenuItem.Id);

            if (input.ProductType == 2) await CreateOrUpdateMenuCombo(input.Combo, output, input.MenuItem.Name);
            await _syncAppService.UpdateSync(SyncConsts.MENU);
        }

        public async Task DeleteMenuItem(IdInput input)
        {
            var mIem = await _menuitemManager.GetAsync(input.Id);

            var bars = await _mbarRepo.GetAllListAsync(a => a.MenuItemId == input.Id);
            foreach (var bar in bars) await _mbarRepo.DeleteAsync(bar.Id);
            var mis = await _comboManager.GetAllListAsync(a => a.MenuItemId == input.Id);
            if (mis.Any())
            {
                var mm = mis.First();
                await _comboManager.DeleteAsync(mm.Id);
            }

            var allComboItems = await _comboIManager.GetAllListAsync(a => a.MenuItemId == input.Id);
            if (allComboItems.Any())
            {
                foreach (var productComboItem in allComboItems)
                {
                    await _comboIManager.DeleteAsync(productComboItem.Id);
                }
            }

            var allLocationMenuItems = await _locationItemManager.GetAllListAsync(a => a.MenuItemId == input.Id);
            foreach (var bar in allLocationMenuItems) await _locationItemManager.DeleteAsync(bar.Id);
            if (mIem != null && mIem.Id > 0 && mIem.Portions.Any())
            {
                var portionIds = mIem.Portions.Select(a => a.Id).ToList();
                var allPriceItems =
                    await _locationPriceManager.GetAllListAsync(a => portionIds.Contains(a.MenuPortionId));
                foreach (var bar in allPriceItems) await _locationPriceManager.DeleteAsync(bar.Id);
            }

            await _menuitemManager.DeleteAsync(input.Id);
            await _syncAppService.UpdateSync(SyncConsts.MENU);
        }

        public async Task<bool> CloneMenuItemAsMaterial(IdInput input)
        {
            var menu = await _menuitemManager.FirstOrDefaultAsync(input.Id);
            if (menu == null)
                return false;
            var output = await _materialAppService.CloneMenuItemAsMaterial(menu);
            return true;
        }

        public async Task<NameValueDto> GetMenuItemById(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                var mItem = await _menuitemManager.FirstOrDefaultAsync(a => a.Name.ToUpper().Equals(input.ToUpper()));
                if (mItem != null)
                    return new NameValueDto
                    {
                        Name = mItem.Name,
                        Value = mItem.Id.ToString()
                    };
            }

            return null;
        }

        public async Task<PagedResultOutput<MenuItemEditDto>> GetAllItems(GetMenuItemLocationInput input, bool restrictLocation = false)
        {
            List<MenuItemEditDto> outPut;
            var count = 0;

            if (input.LocationId > 0 && await _locationItemManager.CountAsync(a => a.LocationId.Equals(input.LocationId)) > 0)
            {
                var returnList = await _locationItemManager.GetAllListAsync(a => a.LocationId.Equals(input.LocationId));
                if (!string.IsNullOrEmpty(input.Filter))
                {
                    var menuItems = returnList.Select(a => a.MenuItem);
                    var allItems = menuItems
                        .WhereIf(
                            !input.Filter.IsNullOrWhiteSpace(),
                            p => p != null && (p.Name.IndexOf(input.Filter, StringComparison.OrdinalIgnoreCase) >= 0 ||
                                           p.AliasName != null && p.AliasName.IndexOf(input.Filter, StringComparison.OrdinalIgnoreCase) >= 0 ||
                                           p.AliasCode != null && p.AliasCode.IndexOf(input.Filter, StringComparison.OrdinalIgnoreCase) >= 0 ||
                                           p.Category != null && p.Category.Name.IndexOf(input.Filter, StringComparison.OrdinalIgnoreCase) >= 0)
                        );
                    outPut = allItems.MapTo<List<MenuItemEditDto>>();
                }
                else
                {
                    outPut = returnList.Select(a => a.MenuItem).MapTo<List<MenuItemEditDto>>();
                }
            }
            else
            {
                var allItems = new List<MenuItem>();
                if (!restrictLocation)
                {
                    allItems = await _menuitemManager.GetAllListAsync(a => string.IsNullOrEmpty(a.Locations));
                }

                if (input.LocationId > 0)
                {
                    var allMyLocations =
                        await _menuitemManager.GetAllListAsync(a => a.Locations != null && !a.Locations.Equals(""));

                    foreach (var allLoc in allMyLocations)
                        if (await _locationAppService.IsLocationExists(new CheckLocationInput
                        {
                            LocationId = input.LocationId,
                            Locations = allLoc.Locations,
                            Group = allLoc.Group,
                            NonLocations = allLoc.NonLocations,
                            LocationTag = allLoc.LocationTag

                        }))
                            allItems.Add(allLoc);
                }

                var myAllItems = allItems.AsEnumerable();
                if (!string.IsNullOrEmpty(input.Filter))
                    myAllItems = myAllItems
                        .WhereIf(
                            !input.Filter.IsNullOrWhiteSpace(),
                            p => p != null && (p.Name.IndexOf(input.Filter, StringComparison.OrdinalIgnoreCase) >= 0 ||
                                           p.AliasName != null && p.AliasName.IndexOf(input.Filter, StringComparison.OrdinalIgnoreCase) >= 0 ||
                                           p.AliasCode != null && p.AliasCode.IndexOf(input.Filter, StringComparison.OrdinalIgnoreCase) >= 0 ||
                                           p.Category != null && p.Category.Name.IndexOf(input.Filter, StringComparison.OrdinalIgnoreCase) >= 0)
                        );
                outPut = myAllItems.MapTo<List<MenuItemEditDto>>();
                count = allItems.Count();
            }

            var allPortions = outPut.SelectMany(a => a.Portions);
            var poritonIds = allPortions.Select(a => a.Id);

            if (await _locationPriceManager.CountAsync(a => a.LocationId.Equals(input.LocationId) && a.Price > 0M) > 0)
                foreach (
                    var lportion in
                    _locationPriceManager.GetAll()
                        .Where(
                            a =>
                                poritonIds.Contains(a.MenuPortionId) && a.LocationId.Equals(input.LocationId) &&
                                a.Price > 0M))
                    if (lportion.Price > 0M)
                    {
                        var singPors = allPortions.Where(a => a.Id == lportion.MenuPortionId);
                        if (singPors.Any())
                        {
                            foreach (var mied in singPors)
                            {
                                mied.Price = lportion.Price;
                                mied.ChangePrice = true;
                            }
                        }
                    }

            return new PagedResultOutput<MenuItemEditDto>(count, outPut);
        }
        public async Task<PagedResultOutput<MenuItemListDto>> GetAllListItems(GetMenuItemLocationInput input, bool restrictLocation = false)
        {
            List<MenuItemListDto> outPut;
            var count = 0;

            if (input.LocationId > 0 && await _locationItemManager.CountAsync(a => a.LocationId.Equals(input.LocationId)) > 0)
            {
                var returnList = await _locationItemManager.GetAllListAsync(a => a.LocationId.Equals(input.LocationId));
                if (!string.IsNullOrEmpty(input.Filter))
                {
                    var menuItems = returnList.Select(a => a.MenuItem);
                    var allItems = menuItems
                        .WhereIf(
                            !input.Filter.IsNullOrWhiteSpace(),
                            p => p != null && (p.Name.IndexOf(input.Filter, StringComparison.OrdinalIgnoreCase) >= 0 ||
                                           p.AliasName != null && p.AliasName.IndexOf(input.Filter, StringComparison.OrdinalIgnoreCase) >= 0 ||
                                           p.AliasCode != null && p.AliasCode.IndexOf(input.Filter, StringComparison.OrdinalIgnoreCase) >= 0 ||
                                           p.Category != null && p.Category.Name.IndexOf(input.Filter, StringComparison.OrdinalIgnoreCase) >= 0)
                        );
                    outPut = allItems.MapTo<List<MenuItemListDto>>();
                }
                else
                {
                    outPut = returnList.Select(a => a.MenuItem).MapTo<List<MenuItemListDto>>();
                }
            }
            else
            {
                var allItems = new List<MenuItem>();
                if (!restrictLocation)
                {
                    allItems = await _menuitemManager.GetAllListAsync(a => string.IsNullOrEmpty(a.Locations));
                }

                if (input.LocationId > 0)
                {
                    var allMyLocations =
                        await _menuitemManager.GetAllListAsync(a => a.Locations != null && !a.Locations.Equals(""));

                    foreach (var allLoc in allMyLocations)
                        if (await _locationAppService.IsLocationExists(new CheckLocationInput
                        {
                            LocationId = input.LocationId,
                            Locations = allLoc.Locations,
                            Group = allLoc.Group,
                            NonLocations = allLoc.NonLocations,
                            LocationTag = allLoc.LocationTag

                        }))
                            allItems.Add(allLoc);
                }

                var myAllItems = allItems.AsEnumerable();
                if (!string.IsNullOrEmpty(input.Filter))
                    myAllItems = myAllItems
                        .WhereIf(
                            !input.Filter.IsNullOrWhiteSpace(),
                            p => p != null && (p.Name.IndexOf(input.Filter, StringComparison.OrdinalIgnoreCase) >= 0 ||
                                           p.AliasName != null && p.AliasName.IndexOf(input.Filter, StringComparison.OrdinalIgnoreCase) >= 0 ||
                                           p.AliasCode != null && p.AliasCode.IndexOf(input.Filter, StringComparison.OrdinalIgnoreCase) >= 0 ||
                                           p.Category != null && p.Category.Name.IndexOf(input.Filter, StringComparison.OrdinalIgnoreCase) >= 0)
                        );
                outPut = myAllItems.MapTo<List<MenuItemListDto>>();
                count = allItems.Count();
            }

            var allPortions = outPut.SelectMany(a => a.Portions);
            var poritonIds = allPortions.Select(a => a.Id);

            if (await _locationPriceManager.CountAsync(a => a.LocationId.Equals(input.LocationId) && a.Price > 0M) > 0)
                foreach (
                    var lportion in
                    _locationPriceManager.GetAll()
                        .Where(
                            a =>
                                poritonIds.Contains(a.MenuPortionId) && a.LocationId.Equals(input.LocationId) &&
                                a.Price > 0M))
                    if (lportion.Price > 0M)
                    {
                        var singPors = allPortions.Where(a => a.Id == lportion.MenuPortionId);
                        if (singPors.Any())
                        {
                            foreach (var mied in singPors)
                            {
                                //mied.Price = lportion.Price;
                                mied.ChangePrice = true;
                            }
                        }
                    }

            return new PagedResultOutput<MenuItemListDto>(count, outPut);
        }

        public async Task<ApiMenuItemImageOutput> ApiMenuItemImageForLocation(ApiLocationInput locationInput)
        {
            return new ApiMenuItemImageOutput();
        }

        public async Task CreateMenuItemToLocation(MenuItemLocationInput input)
        {
            if (input.MenuItemId > 0)
            {
                var checkItem = await
                    _locationItemManager.CountAsync(
                        a => a.LocationId.Equals(input.LocationId) && a.MenuItemId.Equals(input.MenuItemId));

                if (checkItem <= 0)
                    await _locationItemManager.InsertAsync(new LocationMenuItem
                    {
                        MenuItemId = input.MenuItemId,
                        LocationId = input.LocationId
                    });
            }

            if (input.MenuItems != null && input.MenuItems.Any())
            {
                input.DeleteMenuItems = true;
                await DeleteMenuItemFromLocation(input);
                foreach (var menuItem in input.MenuItems)
                    await _locationItemManager.InsertAsync(new LocationMenuItem
                    {
                        MenuItemId = menuItem,
                        LocationId = input.LocationId
                    });
            }
        }

        public async Task DeleteMenuItemFromLocation(MenuItemLocationInput input)
        {
            await _locationItemManager.DeleteAsync(
                a => a.LocationId.Equals(input.LocationId) && a.MenuItemId.Equals(input.MenuItemId));
            if (input.DeleteMenuItems) _locationItemManager.Delete(a => a.LocationId == input.LocationId);
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetMenuItemForCategoryCombobox(NullableIdInput input)
        {
            List<MenuItem> lstMenu;
            if (input.Id.HasValue)
                lstMenu = await _menuitemManager.GetAll().Where(t => t.CategoryId == input.Id).ToListAsync();
            else
                lstMenu = await _menuitemManager.GetAll().ToListAsync();

            return
                new ListResultOutput<ComboboxItemDto>(
                    lstMenu.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }

        public async Task<ListResultOutput<SimpleMenuOutput>> GetSimpleMenuItemForCategory(NullableIdInput input)
        {
            List<MenuItem> lstMenu = await _menuitemManager.GetAll().WhereIf(input.Id.HasValue, t => t.CategoryId == input.Id).ToListAsync();

            return
                new ListResultOutput<SimpleMenuOutput>(
                    lstMenu.Select(e => new SimpleMenuOutput
                    {
                        DisplayText = e.Name,
                        AliasCode = e.AliasCode,
                        AlternateName = e.AliasName,
                        Value = e.Id
                    }).ToList());
        }

        public async Task<PagedResultOutput<LocationPriceOutput>> GetLocationPricesForPortion(GetMenuItemLocationInput input)
        {
            var allListDtos = new List<LocationPriceOutput>();
            var allItemCount = 0;

            if (input.PortionId > 0)
            {
                var allItems = _locationPriceManager
                    .GetAll().Where(a => a.MenuPortionId == input.PortionId);
                var sortMenuItems = await allItems
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();
                allListDtos = sortMenuItems.MapTo<List<LocationPriceOutput>>();
                allItemCount = await allItems.CountAsync();
            }

            return new PagedResultOutput<LocationPriceOutput>(
                allItemCount,
                allListDtos
            );
        }


        public async Task<ListResultOutput<ComboboxItemDto>> GetSinglePortionMenuItems()
        {
            var lstMenu = await _menuitemManager.GetAll().Where(a => a.Portions.Count() == 1).ToListAsync();

            return
                new ListResultOutput<ComboboxItemDto>(
                    lstMenu.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetComboItems()
        {
            var lstMenu = await _menuitemManager.GetAll().Where(a => a.ProductType == 2).ToListAsync();

            return
                new ListResultOutput<ComboboxItemDto>(
                    lstMenu.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }

        public async Task<FileDto> GetMenuItemsTemplate(int locationId, bool isExport)
        {
            var orgId = await _locationAppService.GetOrgnizationIdForLocationId(locationId);
            if (orgId > 0) CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);

            var allMenuItems = await _menuitemManager.GetAllListAsync();

            if (isExport)
                allMenuItems = await _locationItemManager.GetAll()
                    .Where(a => a.LocationId == locationId)
                    .Where(a => a.MenuItem != null)
                    .Select(a => a.MenuItem)
                    .ToListAsync();

            var allListDtos = allMenuItems.MapTo<List<MenuItemListDto>>();
            var baseE = new BaseExportObject
            {
                ExportObject = allListDtos,
                ColumnNames = new[] { "Id", "CategoryName", "Name" }
            };
            return _excelExporter.ExportToFile(baseE, L("MenuItems"));
        }

        public async Task<FileDto> GetMenuPriceTemplate(int locationId)
        {
            return await _menuitemExporter.ExportPriceForLocation(this, locationId);
        }


        public async Task<ApiMenuOutput> ApiItemsForLocation(ApiLocationInput locationInput)
        {
            var orgId = await _locationAppService.GetOrgnizationIdForLocationId(locationInput.LocationId);
            if (orgId > 0) CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);
            var apiOutput = new ApiMenuOutput();

            if (!string.IsNullOrEmpty(locationInput.Tag))
            {
                var allLocations = await _locationAppService.GetLocationsByLocationGroupCode(locationInput.Tag);
                if (allLocations != null && allLocations.Items.Any())
                {
                    foreach (var allLocationsItem in allLocations.Items)
                    {
                        var output = await GetAllItems(new GetMenuItemLocationInput
                        {
                            LocationId = Convert.ToInt32(allLocationsItem.Value)
                        });
                        FillMenuIems(output, apiOutput, Convert.ToInt32(allLocationsItem.Value));
                    }

                    if (!apiOutput.Categories.Any())
                    {
                        var myOutput = await GetAllItems(new GetMenuItemLocationInput
                        {
                            LocationId = 0
                        });
                        FillMenuIems(myOutput, apiOutput, 0);
                    }
                }
            }
            else
            {
                var output = await GetAllItems(new GetMenuItemLocationInput
                {
                    LocationId = locationInput.LocationId
                });
                FillMenuIems(output, apiOutput);
            }

            var myLocationTags = _upMenuItemLocationPriceRepo.GetAll()
                .Where(a => a.LocationId == locationInput.LocationId);
            var myTagPriceCount = myLocationTags.Count(a => a.LocationId == locationInput.LocationId);
            if (myTagPriceCount > 0)
            {
                foreach (var tagDto in apiOutput.Categories.SelectMany(a => a.MenuItems))
                {
                    if (tagDto.UpMenuItems.Any())
                    {
                        foreach (var upItem in tagDto.UpMenuItems)
                        {
                            var myTag = myLocationTags.Where(a => a.UpMenuItemId == upItem.Id).ToList();
                            if (myTag.Any())
                            {
                                upItem.Price = myTag.Last().Price;
                            }
                        }
                    }
                }
            }
            return apiOutput;
        }

        private void FillMenuIems(PagedResultOutput<MenuItemEditDto> output, ApiMenuOutput apiOutput, int locationId = 0)
        {
            foreach (var ou in output.Items)
            {
                if (ou?.CategoryId == null)
                    continue;
                var cat = apiOutput.Categories.SingleOrDefault(a => a.Id == ou.CategoryId);
                if (cat == null)
                {
                    cat = new ApiCategoryOutput
                    {
                        Name = ou.CategoryName,
                        Id = ou.CategoryId,
                        Code = ou.CategoryCode
                    };
                    if (ou.Category != null && ou.Category.ProductGroup != null)
                    {
                        cat.ProductGroup = new ApiProductGroup();
                        FillParentGroup(cat.ProductGroup, ou.Category.ProductGroup);
                    }

                    apiOutput.Categories.Add(cat);
                }

                var menuItem = new ApiMenuItemOutput
                {
                    Id = ou.Id ?? 0,
                    Name = ou.Name,
                    Files = ou.Files,
                    DownloadImage = ou.DownloadImage,
                    AliasName = ou.AliasName,
                    AliasCode = ou.AliasCode,
                    BarCode = ou.BarCode,
                    Description = ou.ItemDescription,
                    ForceQuantity = ou.ForceQuantity,
                    RestrictPromotion = ou.RestrictPromotion,
                    NoTax = ou.NoTax,
                    ForceChangePrice = ou.ForceChangePrice,
                    TransactionTypeId = ou.TransactionTypeId,
                    TransactionTypeName = ou.TransactionType != null ? ou.TransactionType.Name : "",
                    Tag = ou.Tag,
                    Uom = ou.Uom,
                    ProductType = ou.ProductType,
                    LocationId = locationId
                };

                foreach (var po in ou.Portions)
                    menuItem.MenuPortions.Add(new ApiMenuItemPortionOutput
                    {
                        Id = po.Id ?? 0,
                        PortionName = po.Name,
                        Barcode = po.Barcode,
                        Price = po.Price,
                        Multiplier = po.MultiPlier,
                        ChangePrice = po.ChangePrice,
                        PreparationTime = po.PreparationTime,
                        AliasName = po.AliasName,
                        NumberOfPax = po.NumberOfPax

                    });
                foreach (var po in ou.Barcodes)
                    menuItem.Barcodes.Add(new ApiMenuBarcodeOutput
                    {
                        Id = po.Id ?? 0,
                        Barcode = po.Barcode
                    });
                foreach (var po in ou.UpMenuItems)
                    menuItem.UpMenuItems.Add(new ApiUpMenuItemOutput
                    {
                        Id = po.Id ?? 0,
                        AddBaseProductPrice = po.AddBaseProductPrice,
                        MenuItemId = po.MenuItemId,
                        RefMenuItemId = po.RefMenuItemId,
                        Price = po.Price,
                        MinimumQuantity = po.MinimumQuantity,
                        MaxQty = po.MaxQty,
                        AddAuto = po.AddAuto,
                        AddQuantity = po.AddQuantity,
                        ProductType = po.ProductType
                    });
                foreach (var po in ou.MenuItemSchedules)
                    menuItem.MenuItemSchedules.Add(new ApiMenuItemSchedule
                    {
                        Id = po.Id ?? 0,
                        StartHour = po.StartHour,
                        EndHour = po.EndHour,
                        StartMinute = po.StartMinute,
                        EndMinute = po.EndMinute,
                        Days = po.Days,
                        MenuItemId = menuItem.Id
                    });

                foreach (var po in ou.MenuItemDescriptions)
                    menuItem.MenuItemDescriptions.Add(new ApiMenuItemDescription
                    {
                        MenuItemId = menuItem.Id,
                        LanguageCode = po.LanguageCode,
                        ProductName = po.ProductName,
                        Description = po.Description
                    });

                cat.MenuItems.Add(menuItem);

                if (ou.Id > 0 && ou.ProductType == 2)
                {
                    var cItem = _comboManager.FirstOrDefault(a => a.MenuItemId == ou.Id);
                    if (cItem != null)
                        try
                        {
                            menuItem.Combo = cItem.MapTo<ApiProductComboOutput>();
                        }
                        catch (Exception)
                        {
                            // ignored
                        }
                }
            }
        }
        private async Task<ApiLocationListOutput> InternalGetMenuForAllLocation()
        {
            var apiOutput = new ApiLocationListOutput();
            var allLocations = await _locationAppService.GetLocationsByLoginUser();
            var defaultL = new List<int>();
            foreach (var loc in allLocations.Items)
                if (await _locationItemManager.CountAsync(a => a.LocationId == loc.Id) > 0)
                {
                    var output = await ApiGetMenuForLocation(new ApiLocationInput
                    {
                        LocationId = loc.Id
                    });

                    apiOutput.LocationMenus.Add(new ApiLocationMenuOutput
                    {
                        LocationId = loc.Id,
                        Categories = output.Categories,
                        OrderTagGroups = output.OrderTagGroups
                    });
                }
                else if (await _locationPriceManager.CountAsync(a => a.LocationId == loc.Id) > 0)
                {
                    var output = await ApiGetMenuForLocation(new ApiLocationInput
                    {
                        LocationId = loc.Id
                    });

                    apiOutput.LocationMenus.Add(new ApiLocationMenuOutput
                    {
                        LocationId = loc.Id,
                        Categories = output.Categories,
                        OrderTagGroups = output.OrderTagGroups
                    });
                }
                else
                {
                    defaultL.Add(loc.Id);
                }

            if (defaultL.Any())
                foreach (var defL in defaultL)
                {
                    var output = await ApiGetMenuForLocation(new ApiLocationInput
                    {
                        LocationId = defL
                    });
                    if (output.Categories != null && output.Categories.Any())
                    {
                        //We are setting as ZERO for all the Location
                        apiOutput.LocationMenus.Add(new ApiLocationMenuOutput
                        {
                            LocationId = 0,
                            Categories = output.Categories,
                            OrderTagGroups = output.OrderTagGroups
                        });
                        break;
                    }
                }

            return apiOutput;
        }

        private void FillParentGroup(ApiProductGroup catProductGroup, ProductGroupOutputEditDto editDto)
        {
            if (editDto.Id.HasValue)
            {
                catProductGroup.Name = editDto.Name;
                catProductGroup.Code = editDto.Code;
                catProductGroup.Id = editDto.Id.Value;
                catProductGroup.ParentId = editDto.ParentId;
            }

            if (editDto.Parent != null)
            {
                catProductGroup.Parent = new ApiProductGroup();
                FillParentGroup(catProductGroup.Parent, editDto.Parent);
            }

        }


        public async Task<List<MenuItemLocationPriceDto>> GetLocationPricesForMenu(GetMenuPortionPriceInput input)
        {
            //var menuPrice = await GetLocationMenuItemPrices(locationInput);

            var result = await _locationPriceManager.GetAll()
                .Where(m => m.MenuPortion.MenuItemId == input.MenuItemId)
                .Select(m => new MenuItemPortionDto
                {
                    LocationId = m.LocationId,
                    MenuItemId = m.MenuPortion.MenuItemId,
                    Id = m.MenuPortionId,
                    PortionName = m.MenuPortion.Name,
                    Price = m.MenuPortion.Price,
                    MultiPlier = m.MenuPortion.MultiPlier,
                    LocationPrice = m.Price,
                    MenuName = m.MenuPortion.MenuItem.Name,
                    LocationName = m.Location.Name,
                    AliasName = m.MenuPortion.AliasName
                })
                .GroupBy(t => new { t.LocationId, t.MenuItemId })
                .Select(g => new MenuItemLocationPriceDto
                {
                    LocationId = g.Key.LocationId,
                    MenuItemId = g.Key.MenuItemId,
                    LocationName = g.FirstOrDefault().LocationName,
                    MenuItemPortions = g.ToList()
                })
                .ToListAsync();

            return result;
        }

        /**
         * DineTouch Service for Menu
         */

        public async Task<ApiMenuOutput> ApiGetMenuForLocation(ApiLocationInput locationInput)
        {
            var orgId = await _locationAppService.GetOrgnizationIdForLocationId(locationInput.LocationId);
            if (orgId > 0) CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);

            var apiOutput = new ApiMenuOutput();
            var output = await GetAllItems(new GetMenuItemLocationInput
            {
                LocationId = locationInput.LocationId
            });

            apiOutput.OrderTagGroups = await GetTag(null, null);
            try
            {
                var allSorting = output.Items.OrderBy(a => a.CategoryId);
                foreach (var ou in allSorting)
                {
                    var cat = apiOutput.Categories.SingleOrDefault(a => a.Id == ou.CategoryId);
                    if (cat == null)
                    {
                        cat = new ApiCategoryOutput
                        {
                            Name = ou.CategoryName,
                            Id = ou.CategoryId
                        };
                        apiOutput.Categories.Add(cat);
                        cat.OrderTagGroups = await GetTag(cat.Id, null);
                    }

                    var menuItem = new ApiMenuItemOutput
                    {
                        Id = ou.Id ?? 0,
                        Name = ou.Name,
                        AliasName = ou.AliasName,
                        AliasCode = ou.AliasCode,
                        Tag = ou.Tag,
                        BarCode = ou.BarCode,
                        Description = ou.ItemDescription,
                        NoTax = ou.NoTax,
                        RestrictPromotion = ou.RestrictPromotion,
                        ForceQuantity = ou.ForceQuantity,
                        ForceChangePrice = ou.ForceChangePrice
                    };
                    foreach (var po in ou.Barcodes)
                        menuItem.Barcodes.Add(new ApiMenuBarcodeOutput
                        {
                            Id = po.Id ?? 0,
                            Barcode = po.Barcode
                        });
                    if (menuItem.OrderTagGroups.IsNullOrEmpty())
                        menuItem.OrderTagGroups = await GetTag(null, menuItem.Id);

                    foreach (var po in ou.Portions)
                        menuItem.MenuPortions.Add(new ApiMenuItemPortionOutput
                        {
                            Id = po.Id ?? 0,
                            PortionName = po.Name,
                            Price = po.Price,
                            Multiplier = po.MultiPlier
                        });
                    cat.MenuItems.Add(menuItem);
                }
            }
            catch (Exception)
            {
                //
            }

            return apiOutput;
        }

        public async Task<List<ApiLocationMenuItemListOutput>> ApiItemsForSpecificLocation(int locationId)
        {
            var retOutput = new List<ApiLocationMenuItemListOutput>();

            //var orgId = await _locationAppService.GetOrgnizationIdForLocationId(locationId);
            //if (orgId > 0) CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);

            //var outP = await InternalGetMenuForAllLocation();

            //var dpTaxes = await _dineplantaxRepo.GetAll().ToListAsync();
            //var dpLocationWiseTaxes = await _dineplantaxlocationRepo.GetAll().ToListAsync();
            //var dpTaxMappings = await _dineplantaxtemplatemappingRepo.GetAll().ToListAsync();

            //foreach (var apLm in outP.LocationMenus)
            //{
            //    var inP = new ApiLocationMenuItemListOutput
            //    {
            //        LocationId = apLm.LocationId
            //    };

            //    var selM = apLm.Categories.SelectMany(a => a.MenuItems);
            //    var taxToBeAdded = new List<TaxForMenu>();
            //    foreach (var apiMenuItemOutput in selM)
            //    {
            //        var categoryId = apiMenuItemOutput.Id;
            //        foreach (var poR in apiMenuItemOutput.MenuPortions)
            //        {
            //            var dislocationTaxes =
            //                dpLocationWiseTaxes.Where(t => t.LocationRefId == apLm.LocationId).ToList();

            //            List<TaxForMenu> sortedTaxToAdded;

            //            if (dislocationTaxes.Count > 0)
            //                foreach (var dislocationTax in dislocationTaxes)
            //                {
            //                    var taxTemplates = dpTaxMappings.Where(
            //                        t =>
            //                            t.DinePlanTaxLocationRefId == dislocationTax.Id &&
            //                            t.MenuItemId == apiMenuItemOutput.Id).ToList();

            //                    if (!taxTemplates.Any())
            //                        taxTemplates =
            //                            dpTaxMappings.Where(
            //                                t =>
            //                                    t.DinePlanTaxLocationRefId == dislocationTax.Id &&
            //                                    t.CategoryId == categoryId && t.MenuItemId == null).ToList();

            //                    if (!taxTemplates.Any())
            //                        // If not specific for all above need to find the default any
            //                        taxTemplates =
            //                            dpTaxMappings.Where(
            //                                t =>
            //                                    t.DinePlanTaxLocationRefId == dislocationTax.Id &&
            //                                    t.CategoryId == null &&
            //                                    t.MenuItemId == null).ToList();

            //                    if (taxTemplates.Any())
            //                        foreach (var unused in taxTemplates)
            //                        {
            //                            var tax = dpTaxes.Single(t => t.Id == dislocationTax.DinePlanTaxRefId);
            //                            taxToBeAdded.Add(new TaxForMenu
            //                            {
            //                                MenuItemId = apiMenuItemOutput.Id,
            //                                PortionId = poR.Id,
            //                                DinePlanTaxName = tax.TaxName,
            //                                DinePlanTaxRefId = tax.Id,
            //                                DinePlanTaxRate = dislocationTax.TaxPercentage,
            //                                SortOrder = 1,
            //                                Rounding = 2,
            //                                TaxCalculationMethod = "GT"
            //                            }
            //                            );
            //                        }
            //                }

            //            var applicabletaxes = new List<ApplicableTaxesForMenu>();
            //            sortedTaxToAdded = taxToBeAdded.OrderBy(t => t.SortOrder).ToList();

            //            foreach (var tax in sortedTaxToAdded)
            //                applicabletaxes.Add(new ApplicableTaxesForMenu
            //                {
            //                    DinePlanTaxName = tax.DinePlanTaxName,
            //                    DinePlanTaxRefId = tax.DinePlanTaxRefId,
            //                    DinePlanTaxRate = tax.DinePlanTaxRate,
            //                    SortOrder = tax.SortOrder,
            //                    Rounding = tax.Rounding,
            //                    TaxCalculationMethod = tax.TaxCalculationMethod
            //                });

            //            inP.Menus.Add(new ApiLocationMenuItemPriceOutput
            //            {
            //                PortionId = poR.Id,
            //                MenuItemId = apiMenuItemOutput.Id,
            //                AliasCode = apiMenuItemOutput.AliasCode,
            //                AliasName = apiMenuItemOutput.AliasName,
            //                Name = apiMenuItemOutput.Name,
            //                PortionName = poR.PortionName,
            //                Price = poR.Price,
            //                TaxForMenu = sortedTaxToAdded,
            //                ApplicableTaxes = applicabletaxes
            //            });
            //        }
            //    }

            //    retOutput.Add(inP);
            //}
            return retOutput;
        }

        public async Task<bool> IsBarcodeExists(string barcode)
        {
            if (!string.IsNullOrEmpty(barcode))
            {
                var count = await _mbarRepo.CountAsync(a => a.Barcode.Equals(barcode));
                return count > 0;
            }

            return false;
        }

        public async Task DeleteBarcode(string barcode)
        {
            if (!string.IsNullOrEmpty(barcode)) await _mbarRepo.DeleteAsync(a => a.Barcode.Equals(barcode));
        }

        public async Task<ListResultOutput<MenuItemListDto>> GetMenuItemNames()
        {
            var returnList = await _menuitemManager.GetAll()
                .ToListAsync();

            return new ListResultOutput<MenuItemListDto>(returnList.MapTo<List<MenuItemListDto>>());
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _menuitemManager.GetAsync(input.Id);
                item.IsDeleted = false;

                await _menuitemManager.UpdateAsync(item);
            }
        }

        private async Task CreateOrUpdateMenuCombo(List<ProductComboGroupEditDto> comboGs, int mId, string name)
        {
            try
            {
                foreach (var pdi in comboGs.SelectMany(a => a.ComboItems))
                {
                    if (pdi.MenuItemPortionId > 0)
                    {
                        var myPortion = _menuItemPortionManager.Get(pdi.MenuItemPortionId);
                        if (myPortion != null)
                        {
                            pdi.MenuItemId = myPortion.MenuItemId;
                            pdi.MenuItemPortionId = myPortion.Id;
                        }
                    }
                }
                foreach (var pgEdit in comboGs)
                {
                    var checkColor = !string.IsNullOrEmpty(pgEdit.Color);
                    foreach (var comboItem in pgEdit.ComboItems)
                    {
                        comboItem.ButtonColor = checkColor ? pgEdit.Color : comboItem.ButtonColor;
                    }
                }
                var groupedList = comboGs.GroupBy(x => x.Name)
                .Select(x => new
                {
                    ShortName = x.Key,
                    Count = x.Count()
                });

                var onlyDuplicates = groupedList.Where(x => x.Count > 1);
                if (onlyDuplicates.Any())
                    throw new UserFriendlyException(string.Format(L("DuplicateExists_f"), L("Group"), L("Combo")));
                var exists = await _comboManager.GetAllListAsync(a => a.MenuItemId == mId);
                if (exists != null && exists.Any())
                {
                    var firstCom = exists[0];
                    firstCom.Name = name;
                    var alreadyExists = firstCom.ComboGroups.Where(a => a.Id != 0)
                            .Select(a => a.Id)
                            .ToList();
                    foreach (var pgEdit in comboGs)
                    {
                        if (pgEdit.Id != null && pgEdit.Id > 0)
                        {
                            if (alreadyExists.Contains(pgEdit.Id.Value))
                            {
                                var pGroup = _comboGManager.Get(pgEdit.Id.Value);

                                pGroup.Name = pgEdit.Name;
                                pGroup.Minimum = pgEdit.Minimum;
                                pGroup.Maximum = pgEdit.Maximum;
                                pGroup.SortOrder = pgEdit.SortOrder;
                                pGroup.Color = pgEdit.Color;
                                var cItems = pgEdit.ComboItems.Where(a => a.Id == null || a.Id == 0).ToList();

                                foreach (var ci in cItems) pGroup.ComboItems.Add(ci.MapTo<ProductComboItem>());
                                await _comboGManager.UpdateAsync(pGroup);

                                var combiIIds =
                                    pgEdit.ComboItems.Where(a => a.Id != null && a.Id > 0).Select(a => a.Id.Value).ToList();
                                var removeIds =
                                    pGroup.ComboItems.Where(a => a.Id != 0 && !combiIIds.Contains(a.Id))
                                        .Select(a => a.Id)
                                        .ToList();
                                foreach (var i in removeIds) await _comboIManager.DeleteAsync(i);
                                foreach (var cIdItem in pgEdit.ComboItems.Where(a => a.Id != null && a.Id > 0))
                                {
                                    var ci = _comboIManager.Get(cIdItem.Id.Value);
                                    ci.Name = cIdItem.Name;
                                    ci.AutoSelect = cIdItem.AutoSelect;
                                    ci.MenuItemId = cIdItem.MenuItemId;
                                    ci.MenuItemPortionId = cIdItem.MenuItemPortionId;
                                    ci.SortOrder = cIdItem.SortOrder;
                                    ci.AddSeperately = cIdItem.AddSeperately;
                                    ci.ButtonColor = cIdItem.ButtonColor;
                                    ci.GroupTag = cIdItem.GroupTag;
                                    ci.Price = cIdItem.Price;
                                    ci.Count = cIdItem.Count == 0 && cIdItem.AutoSelect ? 1 : cIdItem.Count;
                                    await _comboIManager.UpdateAsync(ci);
                                }
                            }
                            else
                            {
                                var alreadyExistProductGroup = await _comboGManager.FirstOrDefaultAsync(t => t.Id == pgEdit.Id);
                                if (alreadyExistProductGroup != null)
                                {
                                    var productComboTobeAdd = await _comboManager.FirstOrDefaultAsync(t => t.MenuItemId == mId);
                                    alreadyExistProductGroup.ProductCombos.Add(productComboTobeAdd);
                                    //alreadyExistProductGroup.ProductCombos.Add(new ProductCombo { })
                                }
                                else
                                {
                                    firstCom.ComboGroups.Add(pgEdit.MapTo<ProductComboGroup>());
                                }
                            }
                        }
                        else
                        {
                            firstCom.ComboGroups.Add(pgEdit.MapTo<ProductComboGroup>());
                        }

                    }

                    var combiGIds = comboGs.Where(a => a.Id != null && a.Id > 0).Select(a => a.Id.Value).ToList();
                    var removeGro =
                        firstCom.ComboGroups.Where(a => a.Id != 0 && !combiGIds.Contains(a.Id))
                            .ToList();
                    var productComboTobeDeleted = await _comboManager.FirstOrDefaultAsync(t => t.MenuItemId == mId);
                    foreach (var i in removeGro)
                    {
                        var productComboGroup = await _comboGManager.FirstOrDefaultAsync(t => t.Id == i.Id);
                        productComboGroup.ProductCombos.Remove(productComboTobeDeleted);
                        await _comboGManager.UpdateAsync(productComboGroup);
                    }
                    await _comboManager.UpdateAsync(firstCom);
                }
                else
                {
                    var newCg = comboGs.Where(t => t.Id == 0 || t.Id.HasValue == false).ToList();
                    var productComboTobeAdd = await _comboManager.FirstOrDefaultAsync(t => t.MenuItemId == mId);
                    if (productComboTobeAdd == null)
                    {
                        var pCombo = new ProductCombo
                        {
                            Name = name,
                            MenuItemId = mId,
                            ComboGroups = newCg.MapTo<List<ProductComboGroup>>()
                        };
                        _comboManager.InsertAndGetId(pCombo);
                    }
                    else
                    {
                        foreach (var detCg in newCg)
                        {
                            productComboTobeAdd.ComboGroups.Add(detCg.MapTo<ProductComboGroup>());
                        }
                        await _comboManager.UpdateAsync(productComboTobeAdd);
                    }

                    foreach (var lstComboGroup in comboGs.Where(t => t.Id > 0))
                    {
                        if (lstComboGroup.Id > 0)
                        {
                            var alreadyExistProductGroup = await _comboGManager.FirstOrDefaultAsync(t => t.Id == lstComboGroup.Id);
                            if (alreadyExistProductGroup != null)
                            {
                                productComboTobeAdd = await _comboManager.FirstOrDefaultAsync(t => t.MenuItemId == mId);
                                alreadyExistProductGroup.ProductCombos.Add(productComboTobeAdd);
                                await _comboGManager.UpdateAsync(alreadyExistProductGroup);
                            }
                        }
                    }

                }
            }
            catch (Exception exception)
            {
                _logger.Fatal("CREATE OR UPDATE COMBO", exception);
            }
        }

        private async Task<IList<ApiOrderTagGroupOutput>> GetTag(int? categoryId, int? menuId)
        {
            var output = new List<ApiOrderTagGroupOutput>();

            var gOuput = await _orderTagGroupService.GetTagForCategoryAndItem(new OrderTagInput
            {
                CategoryId = categoryId,
                MenuItemId = menuId
            });

            foreach (var go in gOuput.Items)
            {
                var to = new ApiOrderTagGroupOutput
                {
                    Id = go.Id ?? 0,
                    MinSelectItems = go.MinSelectedItems,
                    MaxSelectItems = go.MaxSelectedItems,
                    AddPriceToOrder = go.AddTagPriceToOrderPrice,
                    SaveFreeTags = go.SaveFreeTags,
                    FreeTagging = go.FreeTagging,
                    TaxFree = go.TaxFree,
                    Name = go.Name,
                    SortOrder = 0
                };

                foreach (var otag in go.Tags)
                    to.OrderTags.Add(new ApiOrderTagOutput
                    {
                        Id = otag.Id ?? 0,
                        Name = otag.Name,
                        Price = otag.Price,
                        MenuItemId = otag.MenuItemId ?? 0,
                        SortOrder = 0
                    });
                output.Add(to);
            }
            return output;
        }

        protected virtual async Task UpdateMenuItem(CreateOrUpdateMenuItemInput input)
        {
            var dto = input.MenuItem;
            var item = await _menuitemManager.GetAsync(input.MenuItem.Id.Value);
            UpdateLocationAndNonLocation(item, input.LocationGroup);
            if (input.MenuItemLocationPrices.Count == 0)
            {
                var allPortions = await _menuItemPortionManager.GetAllListAsync(a => a.MenuItemId == input.MenuItem.Id.Value);
                foreach (var portions in allPortions)
                {
                    var locationPrices = await _locationPriceManager.GetAllListAsync(a => a.MenuPortionId == portions.Id);
                    foreach (var itemprices in locationPrices)
                    {
                        await _locationPriceManager.DeleteAsync(itemprices.Id);
                    }
                }
            }
            item.Name = input.MenuItem.Name;
            item.BarCode = input.MenuItem.BarCode;
            item.AliasCode = input.MenuItem.AliasCode;
            item.AliasName = input.MenuItem.AliasName;
            item.ItemDescription = input.MenuItem.ItemDescription;
            item.CategoryId = input.MenuItem.CategoryId;
            item.ForceQuantity = input.MenuItem.ForceQuantity;
            item.RestrictPromotion = input.MenuItem.RestrictPromotion;
            item.NoTax = input.MenuItem.NoTax;
            item.ForceChangePrice = input.MenuItem.ForceChangePrice;
            item.Tag = input.MenuItem.Tag;
            item.Files = input.MenuItem.Files;
            item.FoodType = input.MenuItem.FoodType;
            item.AddOns = input.MenuItem.AddOns;
            if (input.UpdateItem)
            {
                if (!item.Name.Equals(input.MenuItem.Name))
                    foreach (var sitem in _screenItemManager.GetAll().Where(a => a.MenuItemId == item.Id))
                    {
                        sitem.Name = input.MenuItem.Name;
                        await _screenItemManager.UpdateAsync(sitem);
                    }

                if (dto?.Portions != null && dto.Portions.Any())
                {
                    var oids = new List<int>();

                    foreach (var portion in dto.Portions)
                    {
                        var fromUpdation = item.Portions.SingleOrDefault(a => a.Name.Equals(portion.Name));
                        if (fromUpdation != null)
                        {
                            fromUpdation.Price = portion.Price;
                            fromUpdation.NumberOfPax = portion.NumberOfPax;
                            oids.Add(fromUpdation.Id);
                        }
                        else
                        {
                            item.Portions.Add(Mapper.Map<MenuItemPortionEditDto, MenuItemPortion>(portion));
                        }
                    }

                    var tobeRemoved = item.Portions.Where(a => !a.Id.Equals(0) && !oids.Contains(a.Id)).ToList();
                    foreach (var menuItemPortion in tobeRemoved)
                        try
                        {
                            await _menuItemPortionManager.DeleteAsync(menuItemPortion.Id);
                        }
                        catch (Exception exception)
                        {
                            throw new UserFriendlyException(exception.Message);
                        }
                }
            }
            else
            {
                if (!item.Name.Equals(input.MenuItem.Name))
                    foreach (var sitem in _screenItemManager.GetAll().Where(a => a.MenuItemId == item.Id))
                    {
                        sitem.Name = input.MenuItem.Name;
                        await _screenItemManager.UpdateAsync(sitem);
                    }

                item.TransactionTypeId = input.MenuItem.TransactionTypeId;
                item.ProductType = input.ProductType == 0 ? 1 : input.ProductType;
                item.Uom = input.MenuItem.Uom;
                item.HsnCode = input.MenuItem.HsnCode;

                if (input.RefreshImage) item.DownloadImage = Guid.NewGuid();
                if (dto?.Portions != null && dto.Portions.Any())
                {
                    var oids = new List<int>();
                    foreach (var otdto in dto.Portions)
                    {
                        if (otdto.Id != null && otdto.Id > 0)
                        {
                            oids.Add(otdto.Id.Value);
                            var fromUpdation = item.Portions.SingleOrDefault(a => a.Id.Equals(otdto.Id));
                            UpdateMenuItemPortion(fromUpdation, otdto);
                        }
                        else
                        {
                            if (input.ProductType == 2)
                            {

                                var findPor = item.Portions.FirstOrDefault(s => s.Name == otdto.Name);
                                if (findPor != null)
                                {
                                    UpdateMenuItemPortion(findPor, otdto);
                                    oids.Add(findPor.Id);
                                }
                                else
                                {
                                    item.Portions.Clear();
                                    item.Portions.Add(Mapper.Map<MenuItemPortionEditDto, MenuItemPortion>(otdto));
                                }
                            }
                            else
                            {
                                item.Portions.Add(Mapper.Map<MenuItemPortionEditDto, MenuItemPortion>(otdto));
                            }
                        }
                    }

                    var tobeRemoved = item.Portions.Where(a => !a.Id.Equals(0) && !oids.Contains(a.Id)).ToList();
                    foreach (var menuItemPortion in tobeRemoved)
                        try
                        {
                            await _menuItemPortionManager.DeleteAsync(menuItemPortion.Id);
                        }
                        catch (Exception exception)
                        {
                            throw new UserFriendlyException(exception.Message);
                        }

                }

                if (dto?.Barcodes != null && dto.Barcodes.Any())
                {
                    var oids = new List<int>();
                    foreach (var otdto in dto.Barcodes)
                        if (otdto.Id != null && otdto.Id > 0)
                        {
                            oids.Add(otdto.Id.Value);
                            var fromUpdation = item.Barcodes.SingleOrDefault(a => a.Id.Equals(otdto.Id));
                            UpdateMenuItemBarcode(fromUpdation, otdto);
                        }
                        else
                        {
                            item.Barcodes.Add(Mapper.Map<MenuBarCodeEditDto, MenuBarCode>(otdto));
                        }

                    var tobeRemoved = item.Barcodes.Where(a => !a.Id.Equals(0) && !oids.Contains(a.Id)).ToList();
                    foreach (var menuItemPortion in tobeRemoved) await _mbarRepo.DeleteAsync(menuItemPortion.Id);
                }
                else if (item.Barcodes.Any())
                {
                    var tobeRemoved = item.Barcodes.Select(a => a.Id).ToList();
                    foreach (var upItem in tobeRemoved) await _mbarRepo.DeleteAsync(upItem);
                }

                var upSchduleItems = new List<int>();
                if (dto?.MenuItemSchedules != null && dto.MenuItemSchedules.Any())
                    foreach (var otdto in dto.MenuItemSchedules)
                        if (otdto.Id != null && otdto.Id > 0)
                        {
                            upSchduleItems.Add(otdto.Id.Value);
                            var fromUpdation = item.MenuItemSchedules.SingleOrDefault(a => a.Id.Equals(otdto.Id));
                            if (fromUpdation != null)
                            {
                                fromUpdation.MenuItemId = item.Id;
                                UpdateMenuItemSchdules(fromUpdation, otdto);
                            }
                        }
                        else
                        {
                            var obj = Mapper.Map<MenuItemScheduleEditDto, MenuItemSchedule>(otdto);
                            item.MenuItemSchedules.Add(obj);
                        }

                var toRemoveSchdules =
                    item.MenuItemSchedules.Where(a => !a.Id.Equals(0) && !upSchduleItems.Contains(a.Id)).ToList();
                foreach (var schdule in toRemoveSchdules) await _menuItemSchedule.DeleteAsync(schdule.Id);

                var upItemsInts = new List<int>();
                var deleteUp = true;
                if (dto?.UpMenuItems != null && dto.UpMenuItems.Any())
                {
                    deleteUp = false;
                    foreach (var otdto in dto.UpMenuItems)
                        if (otdto.Id != null && otdto.Id > 0)
                        {
                            upItemsInts.Add(otdto.Id.Value);
                            var fromUpdation = item.UpMenuItems.SingleOrDefault(a => a.Id.Equals(otdto.Id));
                            if (fromUpdation != null)
                            {
                                fromUpdation.MenuItemId = item.Id;
                                fromUpdation.ProductType = 2;
                                UpdateUpMenuItems(fromUpdation, otdto);
                            }
                        }
                        else
                        {
                            if (dto.Id != null) otdto.MenuItemId = dto.Id.Value;
                            var obj = Mapper.Map<UpMenuItemEditDto, UpMenuItem>(otdto);
                            obj.ProductType = 2;
                            obj.MenuItemId = item.Id;
                            item.UpMenuItems.Add(obj);
                        }
                }

                var deleteSUp = true;

                if (dto?.UpSingleMenuItems != null && dto.UpSingleMenuItems.Any())
                {
                    deleteSUp = false;
                    foreach (var otdto in dto.UpSingleMenuItems)
                        if (otdto.RefMenuItemId != dto.Id)
                        {
                            if (otdto.Id != null && otdto.Id > 0)
                            {
                                upItemsInts.Add(otdto.Id.Value);
                                var fromUpdation = item.UpMenuItems.SingleOrDefault(a => a.Id.Equals(otdto.Id));
                                if (fromUpdation != null)
                                {
                                    fromUpdation.MenuItemId = item.Id;
                                    fromUpdation.ProductType = 1;
                                    fromUpdation.Price = otdto.Price;
                                    UpdateUpMenuItems(fromUpdation, otdto);
                                }
                            }
                            else
                            {
                                if (dto.Id != null) otdto.MenuItemId = dto.Id.Value;
                                var fromUpdation =
                                    item.UpMenuItems.Any(
                                        a => a.RefMenuItemId.Equals(otdto.RefMenuItemId) && a.ProductType == 1);
                                if (!fromUpdation)
                                {
                                    var obj = Mapper.Map<UpMenuItemEditDto, UpMenuItem>(otdto);
                                    obj.ProductType = 1;
                                    obj.MenuItemId = item.Id;
                                    obj.Price = otdto.Price;
                                    obj.MinimumQuantity = otdto.MinimumQuantity;
                                    obj.AddAuto = otdto.AddAuto;
                                    obj.AddQuantity = otdto.AddQuantity;

                                    item.UpMenuItems.Add(obj);
                                }
                            }
                        }
                }

                var toRemoveUps = item.UpMenuItems.Where(a => !a.Id.Equals(0) && !upItemsInts.Contains(a.Id)).ToList();
                foreach (var upItem in toRemoveUps) await _upMenuItem.DeleteAsync(upItem.Id);

                if (deleteSUp && deleteUp && item.UpMenuItems.Any())
                {
                    var tobeRemoved = item.UpMenuItems.Select(a => a.Id).ToList();
                    foreach (var upItem in tobeRemoved) await _upMenuItem.DeleteAsync(upItem);
                }

                foreach (var des in dto.MenuItemDescriptions)
                {
                    var description = des.MapTo<MenuItemDescription>();
                    if (des.Id == 0)
                        await _menuItemDescriptionRepository.InsertAsync(description);
                    else
                        await _menuItemDescriptionRepository.UpdateAsync(description);
                }

                item.MenuItemDescriptions
                    .Where(m => !dto.MenuItemDescriptions.Select(d => d.Id).Contains(m.Id))
                    .ToList()
                    .ForEach(m => _menuItemDescriptionRepository.DeleteAsync(m));



                CheckErrors(await _miManager.CreateOrUpdateSync(item));
            }
        }

        private void UpdateUpMenuItems(UpMenuItem fromUpdation, UpMenuItemEditDto otdto)
        {
            fromUpdation.MenuItemId = otdto.MenuItemId;
            fromUpdation.AddBaseProductPrice = otdto.AddBaseProductPrice;
            fromUpdation.MinimumQuantity = otdto.MinimumQuantity;
            fromUpdation.MaxQty = otdto.MaxQty;
            fromUpdation.AddAuto = otdto.AddAuto;
            fromUpdation.AddQuantity = otdto.AddQuantity;

            fromUpdation.RefMenuItemId = otdto.RefMenuItemId;
            fromUpdation.Price = otdto.Price;
        }

        private void UpdateMenuItemSchdules(MenuItemSchedule fromUpdation, MenuItemScheduleEditDto otdto)
        {
            fromUpdation.StartMinute = otdto.StartMinute;
            fromUpdation.StartHour = otdto.StartHour;
            fromUpdation.EndMinute = otdto.EndMinute;
            fromUpdation.EndHour = otdto.EndHour;
            fromUpdation.Days = otdto.Days;
        }

        private void UpdateMenuItemBarcode(MenuBarCode fromUpdation, MenuBarCodeEditDto otdto)
        {
            fromUpdation.Barcode = otdto.Barcode;
        }

        private void UpdateMenuItemPortion(MenuItemPortion fromUpdation, MenuItemPortionEditDto otdto)
        {
            fromUpdation.Name = otdto.Name;
            fromUpdation.MultiPlier = otdto.MultiPlier;
            fromUpdation.Price = otdto.Price;
            fromUpdation.Barcode = otdto.Barcode;
            fromUpdation.CostPrice = otdto.CostPrice;
            fromUpdation.PreparationTime = otdto.PreparationTime;
            fromUpdation.AliasName = otdto.AliasName;
            fromUpdation.NumberOfPax = otdto.NumberOfPax;
        }

        protected virtual async Task<int> CreateMenuItem(CreateOrUpdateMenuItemInput input)
        {
            try
            {
                var menuItem = input.MenuItem.MapTo<MenuItem>();
                UpdateLocationAndNonLocation(menuItem, input.LocationGroup);
                menuItem.ProductType = input.ProductType;
                menuItem.DownloadImage = Guid.NewGuid();

                if (menuItem.ProductType == 0)
                    menuItem.ProductType = 1;
                var dto = input.MenuItem;

                if (dto?.UpMenuItems != null && dto.UpMenuItems.Any())
                    foreach (var otdto in dto.UpMenuItems)
                        if (menuItem.UpMenuItems.Any(a => a.RefMenuItemId != otdto.RefMenuItemId && a.ProductType == 2))
                        {
                            var obj = Mapper.Map<UpMenuItemEditDto, UpMenuItem>(otdto);
                            obj.ProductType = 2;
                            menuItem.UpMenuItems.Add(obj);
                        }

                if (dto?.UpSingleMenuItems != null && dto.UpSingleMenuItems.Any())
                    foreach (var otdto in dto.UpSingleMenuItems)
                        if (menuItem.UpMenuItems.Any(a => a.RefMenuItemId != otdto.RefMenuItemId && a.ProductType == 1))
                        {
                            var obj = Mapper.Map<UpMenuItemEditDto, UpMenuItem>(otdto);
                            obj.ProductType = 1;
                            menuItem.UpMenuItems.Add(obj);
                        }

                CheckErrors(await _miManager.CreateOrUpdateSync(menuItem));
                var item = await _menuitemManager.InsertOrUpdateAndGetIdAsync(menuItem);
                return item;
            }
            catch (Exception exception)
            {
                _logger.Error("CreateMenuItem", exception);
                throw new UserFriendlyException(exception.Message);
            }
        }

        private async Task UpdateLocationPriceForMenu(List<MenuItemLocationPriceDto> inputs, int? menuItemId)
        {
            var locationIds = inputs.Select(l => l.LocationId).Distinct().ToList();

            var deleteList = await _locationPriceManager
                .GetAll()
                .Where(l => !locationIds.Contains(l.LocationId) && l.MenuPortion.MenuItemId == menuItemId)
                .ToListAsync();

            deleteList.ForEach(l => _locationPriceManager.DeleteAsync(l));

            var locationPriceInputs = inputs
                .SelectMany(m => m.MenuItemPortions)
                .Select(l => new LocationPriceInput
                {
                    LocationId = l.LocationId,
                    PortionId = l.Id.Value,
                    Price = l.LocationPrice
                }).ToList();

            await UpdateLocationPrice(locationPriceInputs);
        }

        public ListResultDto<ComboboxItemDto> GetFoodTypes()
        {
            var returnList = new List<ComboboxItemDto>();
            var i = 1;
            foreach (var name in Enum.GetNames(typeof(ProductFoodType)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultDto<ComboboxItemDto>(returnList);
        }

        public async Task<FileDto> GetMenuItemDescriptionForExport(GetMenuItemDescriptionExportInput input)
        {
            var menu = await _menuitemManager.GetAsync(input.MenuItemId);
            var descriptions = await _menuItemDescriptionRepository.GetAllListAsync(e => e.MenuItemId == input.MenuItemId);

            var dtos = descriptions.MapTo<List<MenuItemDescriptionEditDto>>();

            if (!input.IsExport)
            {
                dtos = new List<MenuItemDescriptionEditDto>();
                var allLanguages = _localizationManager.GetAllLanguages();

                foreach (var item in allLanguages.ToList())
                {
                    dtos.Add(new MenuItemDescriptionEditDto { MenuItemId = input.MenuItemId, MenuItemName = menu.Name, LanguageCode = item.DisplayName });
                }
            }

            return _menuitemExporter.ExportMenuItemDescription(dtos);
        }

        public async Task<List<ProductComboGroupEditDto>> GetProductComboGroupsBasedOnMenuItem(IdInput input)
        {
            var cb = await _comboManager.FirstOrDefaultAsync(a => a.MenuItemId == input.Id);
            if (cb == null) return new List<ProductComboGroupEditDto>();

            var combs = cb.ComboGroups.OrderBy(t => t.SortOrder).MapTo<List<ProductComboGroupEditDto>>();
            if (string.IsNullOrEmpty(cb.Sorting))
                return combs;

            var sortedIds = cb.Sorting.Split(',').ToList();
            if (!sortedIds.Any()) return combs;

            foreach (var id in sortedIds)
            {
                var idInt = Convert.ToInt32(id);
                var g = combs.FirstOrDefault(x => x.Id == idInt);
                if (g != null)
                {
                    g.CustomSortOrder = sortedIds.IndexOf(id);
                }
            }
            return combs.OrderBy(x => x.CustomSortOrder).ToList();
        }
        public async Task SaveSortCombogroupItems(MenuItemSortingOutputDto input)
        {
            var mItem = await _comboManager.FirstOrDefaultAsync(t => t.MenuItemId == input.MenuItemId);
            var sortOrder = "";
            foreach (var item in input.combogroupItems)
            {
                sortOrder = sortOrder + item + ",";
            }
            if (sortOrder.Length > 0)
                sortOrder = sortOrder.Left(sortOrder.Length - 1);
            mItem.Sorting = sortOrder;
            await _comboManager.UpdateAsync(mItem);
        }

        public async Task<PagedResultOutput<UpMenuItemLocationPriceListDto>> GetAllUpMenuItemLocationPrice(GetUpMenuItemLocationPriceInput input)
        {

            var allItems = await _upMenuItemLocationPriceRepo.GetAllListAsync(t => t.UpMenuItemId == input.UpMenuItemId);
            var allItemCount = allItems.Count();

            var sortMenuItems = allItems.AsQueryable()
                    .PageBy(input)
                    .ToList();

            var allListDtos = sortMenuItems.MapTo<List<UpMenuItemLocationPriceListDto>>();

            var rslocation = await _locationRepo.GetAllListAsync();
            foreach (var lst in allListDtos)
            {
                if (lst.LocationId > 0)
                {
                    var location = rslocation.FirstOrDefault(t => t.Id == lst.LocationId);
                    if (location != null)
                    {
                        lst.LocationName = location.Name;
                    }
                }
            }
            allListDtos = allListDtos.OrderBy(input.Sorting).ToList();
            return new PagedResultOutput<UpMenuItemLocationPriceListDto>(
                allItemCount,
                allListDtos
            );
        }
        public async Task<GetUpMenuItemLocationPriceForEditOutput> GetUpMenuItemLocationPriceForEdit(NullableIdInput input)
        {
            var output = new GetUpMenuItemLocationPriceForEditOutput();
            UpMenuItemLocationPriceEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _upMenuItemLocationPriceRepo.GetAllListAsync(t => t.UpMenuItemId == input.Id.Value);
                editDto = hDto.MapTo<UpMenuItemLocationPriceEditDto>();

            }
            else
            {
                editDto = new UpMenuItemLocationPriceEditDto();
            }
            output.UpMenuItemLocationPrice = editDto;
            return output;
        }

        public async Task CreateUpMenuItemLocationPrice(CreateUpMenuItemLocationPrice input)
        {
            if (input.UpMenuItemLocationPrice.Id.HasValue)
            {
                var menuitemlocationprice = input.UpMenuItemLocationPrice;
                var existEntry = await _upMenuItemLocationPriceRepo.FirstOrDefaultAsync(t => t.Id == menuitemlocationprice.Id.Value);
                menuitemlocationprice.MapTo(existEntry);
                await _upMenuItemLocationPriceRepo.UpdateAsync(existEntry);
            }
            else
            {
                var dto = input.UpMenuItemLocationPrice.MapTo<UpMenuItemLocationPrice>();
                await _upMenuItemLocationPriceRepo.InsertAndGetIdAsync(dto);
            }

        }
        public async Task DeleteUpMenuItemLocationPrice(IdInput input)
        {
            await _upMenuItemLocationPriceRepo.DeleteAsync(input.Id);
        }
        public async Task<GetMenuEngineeringReportOutputDto> GetMenuEngineeringReport(GetMenuEngineeringReportInput input)
        {
            var result = await GetMenuEngineeringReportByLocation(input);

            // Get data for dashboard
            var horse = result.ListItem.Where(x => x.MenuItemClass == ConnectConsts.ConnectConsts.Horse);
            var star = result.ListItem.Where(x => x.MenuItemClass == ConnectConsts.ConnectConsts.Star);
            var dog = result.ListItem.Where(x => x.MenuItemClass == ConnectConsts.ConnectConsts.Dog);
            var puzzle = result.ListItem.Where(x => x.MenuItemClass == ConnectConsts.ConnectConsts.Puzzle);

            var dashboard = new GetMenuEngineeringReportForDashboardDto();
            dashboard.Horse = string.Join("</br>", horse.Select(x => x.MenuItemAndPortion));
            dashboard.Star = string.Join("</br>", star.Select(x => x.MenuItemAndPortion));
            dashboard.Dog = string.Join("</br>", dog.Select(x => x.MenuItemAndPortion));
            dashboard.Puzzle = string.Join("</br>", puzzle.Select(x => x.MenuItemAndPortion));

            result.Dashboard = dashboard;
            return await Task.FromResult(result);
        }

        public async Task<List<MenuEngineerReportListDto>> GetListMenuEngineer(GetMenuEngineeringReportInput input)
        {
            var result = new List<MenuEngineerReportListDto>();
            var inputItem = input.MapTo<GetItemInput>();
            var orders = _connectReportAppService.GetAllOrdersNotFilterByDynamicFilter(inputItem);
            var output = (from c in orders
                          group c by new { c.MenuItemPortionId, Location = c.Location_Id }
                    into g
                          select new { g.Key.Location, g.Key.MenuItemPortionId, Items = g });
            List<OrderTemp> outputOrderTemps = new List<OrderTemp>();

            foreach (var item in output.ToList())
            {
                List<OrderList2Dto> list2Dtos = new List<OrderList2Dto>();
                foreach (var itemList in item.Items.ToList())
                {
                    list2Dtos.Add(new OrderList2Dto
                    {
                        Quantity = itemList.Quantity,
                        MenuItemId = itemList.MenuItemId,
                        MenuItemName = itemList.MenuItemName,
                        PortionName = itemList.PortionName,
                    });
                }
                outputOrderTemps.Add(new OrderTemp
                {
                    Location = item.Location,
                    MenuItemPortion = item.MenuItemPortionId,
                    Items = list2Dtos
                });
            }
            var dicLocations = new Dictionary<int, string>();
            var dicCategory = new Dictionary<int, string>();
            var dicGroups = new Dictionary<int, string>();
            var aliasCodes = new Dictionary<int, string>();
            var portionIds = new Dictionary<int, string>();

            foreach (var dto in outputOrderTemps)
            {
                var firObj = dto.Items.First();
                if (firObj != null)
                    try
                    {
                        var sumQuantity = dto.Items.Sum(b => b.Quantity);
                        var lname = "";
                        if (dicLocations.Any() && dicLocations.ContainsKey(dto.Location))
                        {
                            lname = dicLocations[dto.Location];
                        }
                        else
                        {
                            var lSer = _locationRepo.Get(dto.Location);
                            if (lSer == null) continue;

                            lname = lSer.Name;
                            dicLocations.Add(dto.Location, lname);
                        }

                        var menuItem = firObj.MenuItemId;
                        var cateDto = GetCategoryDto(dicCategory, dicGroups, aliasCodes, menuItem);
                        var portion = await _menuItemPortionManager.FirstOrDefaultAsync(x => x.Id == dto.MenuItemPortion);
                        var menL = new MenuEngineerReportListDto
                        {
                            AliasCode = cateDto.AliasCode,
                            LocationName = lname,
                            MenuItemPortionId = dto.MenuItemPortion,
                            MenuItemPortionName = firObj.PortionName,
                            MenuItemId = firObj.MenuItemId,
                            MenuItemName = firObj.MenuItemName,
                            CategoryId = cateDto.CatId,
                            CategoryName = cateDto.CatgoryName,
                            GroupId = cateDto.GroupId,
                            GroupName = cateDto.GroupName,
                            Quantity = dto.Items.Sum(a => a.Quantity),
                            Price = portion != null ? portion.Price : 0,
                            CostPrice = portion != null ? portion.CostPrice : 0
                        };

                        result.Add(menL);
                    }
                    catch (Exception exception)
                    {
                        var mess = exception.Message;
                    }
            }

            var menuListOutputDots = result.GroupBy(x => new { x.MenuItemName, x.CategoryName, x.MenuItemPortionName }).Select(x => new MenuEngineerReportListDto
            {
                MenuItemPortionName = x.Key.MenuItemPortionName,
                CategoryName = x.Key.CategoryName,
                MenuItemName = x.Key.MenuItemName,
                AliasCode = x.FirstOrDefault().AliasCode,
                AverageCM = x.FirstOrDefault().AverageCM,
                CategoryId = x.FirstOrDefault().CategoryId,
                CostPrice = x.FirstOrDefault().CostPrice,
                GroupId = x.FirstOrDefault().GroupId,
                GroupName = x.FirstOrDefault().GroupName,
                MenuItemId = x.FirstOrDefault().MenuItemId,
                MenuItemPortionId = x.FirstOrDefault().MenuItemPortionId,
                Price = x.FirstOrDefault().Price,
                Quantity = x.Sum(i => i.Quantity),
                LocationName = x.FirstOrDefault().LocationName,

            }); ;
            if (input.IsMenuItems)
            {
                menuListOutputDots = menuListOutputDots.GroupBy(x => new { x.MenuItemName, x.CategoryName }).Select(x => new MenuEngineerReportListDto
                {
                    MenuItemPortionName = "",
                    CategoryName = x.Key.CategoryName,
                    MenuItemName = x.Key.MenuItemName,
                    AliasCode = x.FirstOrDefault().AliasCode,
                    AverageCM = x.FirstOrDefault().AverageCM,
                    CategoryId = x.FirstOrDefault().CategoryId,
                    CostPrice = x.Sum(i => i.Quantity) > 0 ? x.Sum(i => i.MenuCost) / x.Sum(i => i.Quantity) : 0,
                    GroupId = x.FirstOrDefault().GroupId,
                    GroupName = x.FirstOrDefault().GroupName,
                    MenuItemId = x.FirstOrDefault().MenuItemId,
                    Price = x.Sum(i => i.Quantity) > 0 ? x.Sum(i => i.MenuRevenue) / x.Sum(i => i.Quantity) : 0,
                    Quantity = x.Sum(i => i.Quantity),
                    LocationName = x.FirstOrDefault().LocationName,
                });
            }

            result = menuListOutputDots.AsQueryable().PageBy(input).ToList();

            return result;
        }
        private TCategoryDto GetCategoryDto(Dictionary<int, string> dicCategory, Dictionary<int, string> dicGroups,
           Dictionary<int, string> aliasCodes, int menuItem)
        {
            var returnD = new TCategoryDto();

            if (dicCategory.Any() && dicCategory.ContainsKey(menuItem))
            {
                var catSplitname = dicCategory[menuItem];
                var splitName = catSplitname.Split('@');
                if (splitName.Any())
                {
                    returnD.CatId = Convert.ToInt32(splitName[0]);
                    returnD.CatgoryName = splitName[1];
                }
            }
            else
            {
                var lSer = _menuitemManager.Get(menuItem);
                if (lSer != null)
                {
                    returnD.CatId = lSer.CategoryId ?? 0;
                    returnD.CatgoryName = lSer.Category.Name;
                    dicCategory.Add(menuItem, returnD.CatId + "@" + returnD.CatgoryName);
                }
            }

            if (dicGroups.Any() && dicGroups.ContainsKey(menuItem))
            {
                var catSplitname = dicGroups[menuItem];
                var splitName = catSplitname.Split('@');
                if (splitName.Any())
                {
                    returnD.GroupId = Convert.ToInt32(splitName[0]);
                    returnD.GroupName = splitName[1];
                }
            }
            else
            {
                var lSer = _menuitemManager.Get(menuItem);
                if (lSer != null)
                {
                    returnD.GroupId = lSer.Category?.ProductGroupId ?? 0;
                    returnD.GroupName = lSer.Category?.ProductGroup?.Name;
                    dicGroups.Add(menuItem, returnD.GroupId + "@" + returnD.GroupName);
                }
            }

            if (aliasCodes.Any() && aliasCodes.ContainsKey(menuItem))
            {
                returnD.AliasCode = aliasCodes[menuItem];
            }
            else
            {
                var alisCodeSet = _menuitemManager.Get(menuItem);
                aliasCodes.Add(menuItem, alisCodeSet.AliasCode);
                returnD.AliasCode = alisCodeSet.AliasCode;
            }

            return returnD;
        }


        public async Task<FileDto> GetMenuEngineeringForExport(GetMenuEngineeringReportInput input)
        {
            return await _menuitemExporter.ExportMenuEngineering(input, this);
        }

        public async Task<FileDto> GetMenuEngineeringByLocationForExport(GetMenuEngineeringReportInput input)
        {
            return await _menuitemExporter.ExportMenuEngineeringByLocation(input, this);
        }

        public async Task<FileDto> GetMenuEngineeringByMonthForExport(GetMenuEngineeringReportInput input)
        {
            return await _menuitemExporter.ExportMenuEngineeringByMonth(input, this);
        }

        public async Task<GetMenuEngineeringReportOutputDto> GetMenuEngineeringReportByLocation(GetMenuEngineeringReportInput input)
        {
            var result = new GetMenuEngineeringReportOutputDto();
            result.ListItem = await GetListMenuEngineer(input);
            decimal totalQuantity = result.ListItem.Sum(s => s.Quantity);
            foreach (var item in result.ListItem)
            {
                item.AverageCM = result.AverageCM;
                item.PercentageMenuMix = result.PercentageMenuMix;
                item.TotalQuantity = totalQuantity;
            }
            return await Task.FromResult(result);
        }
    }
}