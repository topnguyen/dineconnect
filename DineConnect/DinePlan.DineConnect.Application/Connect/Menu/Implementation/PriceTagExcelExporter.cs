﻿using System.Collections.Generic;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.AutoMapper;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;

namespace DinePlan.DineConnect.Connect.Menu.Implementation
{
    public class PriceTagExcelExporter : FileExporterBase, IPriceTagExcelExporter
    {
        

        public async Task<FileDto> ExportImportTemplate(GetPriceTagInput input, PriceTagAppService appService)
        {
            var file = new FileDto("Import-PriceTag-"+ input.TagId+".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(input.TagId.ToString());
                sheet.OutLineApplyStyle = true;
                AddHeader(
                    sheet,
                    L("Id"),
                    L("TagId"),
                    L("MenuName"),
                    L("PortionName"),
                    L("Price"),
                    L("TagPrice")
                  );
                int rowCount = 2;
                var outPut = await appService.GetMenuPricesForTag(input);
                if (DynamicQueryable.Any(outPut.Items))
                {
                    AddObjects(
                        sheet, rowCount, outPut.Items.MapTo<IList<TagMenuItemPortionDto>>(),
                        _ => _.Id,
                        _ => _.TagId,
                        _ => _.MenuName,
                        _ => _.PortionName,
                        _ => _.Price,
                        _ => _.TagPrice
                        );
                }
                for (var i = 1; i <= 1; i++)
                {
                    sheet.Column(i).AutoFit();
                }
                Save(excelPackage, file);
            }

            return file;
        }
    }
}