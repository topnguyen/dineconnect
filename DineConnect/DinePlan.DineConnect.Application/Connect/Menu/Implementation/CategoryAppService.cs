﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using AutoMapper;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.General;
using DinePlan.DineConnect.Session;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Menu.Implementation
{
    public class CategoryAppService : DineConnectAppServiceBase, ICategoryAppService
    {
        public ConnectSession ConnectSession { get; set; }

        private readonly ICategoryListExcelExporter _categoryExporter;
        private readonly IRepository<Category> _categoryRepository;
        private readonly ICategoryManager _categoryManager;
        private readonly IRepository<LocationCategoryShare> _loShareManager;

        private readonly ISyncAppService _syncAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IProductGroupAppService _productGroupAppService;
        private readonly IRepository<ProductGroup> _productGroupRepository;

        public CategoryAppService(IRepository<Category> categoryRepository,
            ICategoryManager categoryManager,
            IRepository<LocationCategoryShare> loShareManager,
            ICategoryListExcelExporter categoryExporter,
            ISyncAppService syncAppService,
            IUnitOfWorkManager unitOfWorkManager,
            IProductGroupAppService productGroupAppService,
            IRepository<ProductGroup> productGroupRepository)
        {
            _categoryRepository = categoryRepository;
            _categoryManager = categoryManager;
            _loShareManager = loShareManager;
            _categoryExporter = categoryExporter;
            _syncAppService = syncAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _productGroupAppService = productGroupAppService;
            _productGroupRepository = productGroupRepository;
        }

        public async Task<PagedResultOutput<CategoryListDto>> GetAll(GetCategoryInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var allItems = _categoryRepository
                .GetAll()
                .Where(i => i.IsDeleted == input.IsDeleted)
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Name.Contains(input.Filter)
                )
                .Select(c => new CategoryListDto
                {
                    Id = c.Id,
                    Name = c.Name,
                    Code = c.Code,
                    ProductGroupId = c.ProductGroupId,
                    GroupName = c.ProductGroup != null ? c.ProductGroup.Name : null,
                    CreationTime = c.CreationTime,
                    IsDeleted = c.IsDeleted
                });

                var allMyItems = await allItems
                   .OrderBy(input.Sorting)
                   .PageBy(input)
                   .ToListAsync();

                var allItemCount = await allItems.CountAsync();

                return new PagedResultOutput<CategoryListDto>(
                    allItemCount,
                    allMyItems
                    );
            }
        }

        public async Task<ListResultOutput<CategoryListDto>> GetCategories()
        {
            var roles = await _categoryRepository.GetAll().ToListAsync();
            return new ListResultOutput<CategoryListDto>(roles.MapTo<List<CategoryListDto>>());
        }
        public async Task<PagedResultOutput<CategoryListDto>> GetAllCategoyItems(GetCategoryInput input)
        {
           var allItems = _categoryRepository
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Name.Contains(input.Filter)
                ).Select(c => new CategoryListDto
                {
                    Id = c.Id,
                    Name = c.Name,
                    Code = c.Code,
                    ProductGroupId = c.ProductGroupId,
                    GroupName = c.ProductGroup != null ? c.ProductGroup.Name : null,
                    CreationTime = c.CreationTime,
                    IsDeleted = c.IsDeleted
                });

            var allMyItems = await allItems
               .OrderBy(input.Sorting)
               .PageBy(input)
               .ToListAsync();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<CategoryListDto>(
                allItemCount,
                allMyItems
                );
        }
        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _categoryRepository.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<CategoryListDto>>();
            return _categoryExporter.ExportToFile(allListDtos);
        }

        public async Task<GetCategoryForEditOutput> GetCategoryForEdit(NullableIdInput input)
        {
            CategoryEditDto editDto;

            if (input.Id.HasValue)
            {
                var category = await _categoryRepository.GetAsync(input.Id.Value);
                editDto = category.MapTo<CategoryEditDto>();
                if(editDto.ProductGroupId.HasValue)
                {
                    var productGroup = await _productGroupRepository.GetAsync(editDto.ProductGroupId.Value);
                    editDto.GroupName = productGroup.Name;
                    editDto.GroupCode = productGroup.Code;
                }
            }
            else
            {
                editDto = new CategoryEditDto();
            }

            return new GetCategoryForEditOutput
            {
                Category = editDto
            };
        }

        public async Task<CategoryEditDto> GetOrCreateByName(string name)
        {
            if (ConnectSession.OrgId > 0)
            {
                CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", ConnectSession.OrgId);
            }
            CategoryEditDto editDto;

            var allItems = _categoryRepository
               .GetAll()
               .WhereIf(
                   !name.IsNullOrEmpty(),
                   p => p.Name.Equals(name)
               );
            List<Category> sortMenuItems = new List<Category>(allItems);

            if (DynamicQueryable.Any(sortMenuItems))
            {
                editDto = sortMenuItems.First().MapTo<CategoryEditDto>();
            }
            else
            {
                editDto = new CategoryEditDto()
                {
                    Name = name
                };
                var output = await CreateCategory(new CreateOrUpdateCategoryInput()
                {
                    Category = editDto
                });
                editDto.Id = output;
            }
            return editDto;
        }

        public async Task<CategoryEditDto> GetOrCreateCategory(CategoryEditDto input)
        {
            if (ConnectSession.OrgId > 0)
            {
                CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", ConnectSession.OrgId);
            }

            var allItems = _categoryRepository
               .GetAll()
               .WhereIf(
                   !input.Name.IsNullOrEmpty(),
                   p => p.Name.Equals(input.Name)
               );
            List<Category> sortMenuItems = new List<Category>(allItems);

            if (DynamicQueryable.Any(sortMenuItems))
            {
                var cateDto = sortMenuItems.First().MapTo<CategoryEditDto>();
                cateDto.Code = input.Code;
                cateDto.GroupName = input.GroupName;
                cateDto.GroupCode = input.GroupCode;

                await GetOrCreateGroup(cateDto);
                await CreateOrUpdateCategory(new CreateOrUpdateCategoryInput()
                {
                    Category = cateDto
                });
                input = cateDto;
            }
            else
            {
                await GetOrCreateGroup(input);
                var output = await CreateOrUpdateCategory(new CreateOrUpdateCategoryInput()
                {
                    Category = input
                });
                input.Id = output;
            }

            return input;
        }

        private async Task GetOrCreateGroup(CategoryEditDto input)
        {
            ProductGroupEditDto groupeditDto;
            var group = _productGroupRepository.GetAll().Where(t => t.Name.Equals(input.GroupName));

            List<ProductGroup> grouplst = new List<ProductGroup>(group);

            if (DynamicQueryable.Any(grouplst))
            {
                groupeditDto = grouplst.First().MapTo<ProductGroupEditDto>();
            }
            else
            {
                groupeditDto = await _productGroupAppService.GetOrCreateByName(input.GroupName, input.GroupCode);
            }

            input.ProductGroupId = groupeditDto.Id;
        }

        public async Task<int> CreateOrUpdateCategory(CreateOrUpdateCategoryInput input)
        {
            int returnOutput = 0;

            if (input.Category.Id.HasValue)
            {
                await UpdateCategory(input);
                returnOutput = input.Category.Id.Value;
            }
            else
            {
                returnOutput = await CreateCategory(input);
            }
            await _syncAppService.UpdateSync(SyncConsts.MENU);
            return returnOutput;
        }

        public async Task DeleteCategory(IdInput input)
        {
            await _categoryRepository.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateCategory(CreateOrUpdateCategoryInput input)
        {
            var item = await _categoryRepository.GetAsync(input.Category.Id.Value);
            var dto = input.Category;
            dto.MapTo(item);

            if (dto?.Locations != null && dto.Locations.Any())
            {
                var oids = new List<int>();
                foreach (var otdto in dto.Locations)
                {
                    otdto.CategoryId = item.Id;

                    if (otdto.Id == null)
                    {
                        item.Locations.Add(Mapper.Map<LocationCategoryShareDto, LocationCategoryShare>(otdto));
                    }
                    else
                    {
                        oids.Add(otdto.Id.Value);
                        var fromUpdation = item.Locations.SingleOrDefault(a => a.Id.Equals(otdto.Id));
                        UpdateLocationShare(fromUpdation, otdto);
                    }
                }
                var tobeRemoved = item.Locations.Where(a => !a.Id.Equals(0) && !oids.Contains(a.Id)).ToList();
                foreach (var tor in tobeRemoved)
                {
                    await _loShareManager.DeleteAsync(tor.Id);
                }
            }
            try
            {
                CheckErrors(await _categoryManager.CreateSync(item));
            }
            catch (Exception exception)
            {
                var mess = exception.Message;
            }
        }

        private void UpdateLocationShare(LocationCategoryShare fromUpdation, LocationCategoryShareDto otdto)
        {
            fromUpdation.LocationId = otdto.LocationId;
            fromUpdation.InclusiveOfTax = otdto.InclusiveOfTax;
            fromUpdation.SharingPercentage = otdto.SharingPercentage;
        }

        protected virtual async Task<int> CreateCategory(CreateOrUpdateCategoryInput input)
        {
            var dto = input.Category.MapTo<Category>();

            CheckErrors(await _categoryManager.CreateSync(dto));

            return dto.Id;
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _categoryRepository.GetAsync(input.Id);
                item.IsDeleted = false;

                await _categoryRepository.UpdateAsync(item);
            }
        }
        public async Task<PagedResultOutput<IdNameDto>> GetAllCatgeoriesBasedOnProductGroupId(GetCategoryInput input)
        {
            var allItems = _categoryRepository
                 .GetAll()
                 .Where(t => t.ProductGroupId == input.ProductGroupId)
                 .WhereIf(
                     !input.Filter.IsNullOrEmpty(),
                     p => p.Name.Contains(input.Filter)
                 ).Select(c => new IdNameDto
                 {
                     Id = c.Id,
                     Name = c.Name
                 });

            var allMyItems = await allItems
               .OrderBy(input.Sorting)
               .PageBy(input)
               .ToListAsync();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<IdNameDto>(
                allItemCount,
                allMyItems
                );
        }
    }
}