﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Authorization;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Utility.Exporter;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Menu.Implementation
{
    public class ProductGroupAppService : DineConnectAppServiceBase, IProductGroupAppService
    {
        private readonly IProductGroupManager _productGroupManager;
        private readonly IRepository<ProductGroup> _productGroupRepo;
        private readonly IRepository<Category> _categoryRepo;
        private readonly IExcelExporter _exporter;

        public ProductGroupAppService(
            IProductGroupManager ProductgroupManager,
            IRepository<ProductGroup> productGroupRepo,
            IRepository<Category> categoryRepository,
            IExcelExporter exporter)
        {
            _productGroupManager = ProductgroupManager;
            _productGroupRepo = productGroupRepo;
            _categoryRepo = categoryRepository;
            _exporter = exporter;
        }

        public async Task<ListResultOutput<ProductGroupListDto>> GetProductGroups(GetProductGroupInput input)
        {
            var allItems =
                _productGroupRepo.GetAll().Include(lg => lg.Categories);

            var items = await allItems.OrderBy(input.Sorting).ToListAsync();

            return new ListResultOutput<ProductGroupListDto>(
                items.Select(item =>
                {
                    var dto = item.MapTo<ProductGroupListDto>();
                    return dto;
                }).ToList());
        }
        public async Task<PagedResultOutput<CategoryListDto>> GetProductGroupCategories(GeProductGroupCategoriesInput input)
        {
            var query = from cat in _categoryRepo.GetAll()
                        where cat.ProductGroupId == input.Id
                        orderby input.Sorting
                        select new { cat };
            var totalCount = await query.CountAsync();
            var items = await query.PageBy(input).ToListAsync();

            return new PagedResultOutput<CategoryListDto>(
                totalCount,
                items.Select(item =>
                {
                    var dto = item.cat.MapTo<CategoryListDto>();
                    return dto;
                }).ToList());
        }
        public async Task<PagedResultOutput<ProductGroupListDto>> GetAll(GetProductGroupInput input)
        {
            var allItems = _productGroupRepo.GetAll();
            if (!string.IsNullOrEmpty(input.Operation) && input.Operation.Equals("SEARCH"))
            {
                allItems = _productGroupRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Name.Contains(input.Filter) || p.Code.Contains(input.Filter)
               );
            }
            else
            {
                allItems = _productGroupRepo
               .GetAll()
               .WhereIf(
                   !input.Filter.IsNullOrEmpty(),
                   p => p.Name.Contains(input.Filter)
               );
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<ProductGroupListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<ProductGroupListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetProductGroupInput input)
        {
            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<ProductGroupListDto>>();
            var baseE = new BaseExportObject
            {
                ExportObject = allListDtos,
                ColumnNames = new string[] { "Id", "Code", "Name" },
                ColumnDisplayNames = new string[] { "Id", "Code", "Name" }
            };
            return _exporter.ExportToFile(baseE, L("ProductGroup"));
        }

        public async Task<ProductGroupEditDto> GetProductGroupForEdit(NullableIdInput input)
        {
            ProductGroupEditDto editDto = new ProductGroupEditDto();

            if (input.Id.HasValue)
            {
                var hDto = await _productGroupRepo.GetAsync(input.Id.Value);
                hDto.MapTo(editDto);
            }

            return editDto;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Connect_Menu_ProductGroup_ManageProductGroup)]
        public async Task<ProductGroupListDto> CreateProductGroup(CreateProductGroupInput input)
        {
            var productGroup = new ProductGroup
            {
                TenantId = AbpSession.TenantId.Value,
                Name = input.Name,
                Code = input.Code,
                ParentId = input.ParentId
            };
            if (input.ParentId.HasValue)
            {
                var productGrp = new ProductGroup
                {
                    TenantId = AbpSession.TenantId.Value,
                    Name = input.Name,
                    Code = input.Code,
                    ParentId = input.ParentId
                };
                await _productGroupManager.CreateSync(productGroup);
                await CurrentUnitOfWork.SaveChangesAsync();
            }
            else
            {
                try
                {
                    productGroup = new ProductGroup
                    {
                        TenantId = AbpSession.TenantId.Value,
                        Name = input.Name,
                        Code = input.Code,
                    };
                    var oum = _productGroupRepo.InsertAndGetId(productGroup);
                }
                catch (Exception exception)
                {
                    throw exception;
                }
            }

            return productGroup.MapTo<ProductGroupListDto>();
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Connect_Menu_ProductGroup_ManageProductGroup)]
        public async Task<ProductGroupListDto> UpdateProductGroup(UpdateProductGroupInput input)
        {
            var productGroup = await _productGroupRepo.GetAsync(input.Id);

            productGroup.Code = input.Code;
            productGroup.Name = input.Name;

            await _productGroupManager.CreateSync(productGroup);

            return productGroup.MapTo<ProductGroupListDto>();
        }

        public async Task<ProductGroupListDto> MoveProductGroup(MoveProductGroupInput input)
        {
            var productGroup = await _productGroupRepo.GetAsync(input.Id);
            //if(productGroup.ParentId.HasValue)
            //{
            //    var rsparent = await _productGroupRepo.FirstOrDefaultAsync(t => t.ParentId == productGroup.ParentId);
            //    productGroup.Code = rsparent.Name;
            //}
            productGroup.ParentId = input.NewParentId.Value;
            await _productGroupManager.CreateSync(productGroup);

            return productGroup.MapTo<ProductGroupListDto>();
        }

        public async Task DeleteProductGroup(IdInput input)
        {
            GetLinkedProductGroupIdsDto detDto = new GetLinkedProductGroupIdsDto();
            detDto.LinkedId = input.Id;
            detDto.LinkedIds = new List<int>();
            
            List<LinkedListDto> linkedList = new List<LinkedListDto>();
            linkedList.Add(new LinkedListDto { Id = input.Id, CheckedFlag = false });
            var output = await GetLinkedProductGroupIds(new IdInput { Id = input.Id }, linkedList);
            foreach(var lst in output)
            {
                await _productGroupRepo.DeleteAsync(t => t.Id==lst.Id);
            }
            
        }

        public async Task<List<LinkedListDto>> GetLinkedProductGroupIds(IdInput input, List<LinkedListDto> linkedListDtos)
        {
            var output = await GetLinkedParendIds(input, linkedListDtos);
            return output;
        }

        public async Task<List<LinkedListDto>> GetLinkedParendIds(IdInput input, List<LinkedListDto> linkedListDto)
        {
            var linkedProductGroup = await _productGroupRepo.GetAllListAsync(t => t.ParentId.HasValue && t.ParentId == input.Id);
            if (linkedProductGroup.Count == 0)
            {
                var checkedList = linkedListDto.FirstOrDefault(t => t.Id == input.Id);
                if (checkedList != null)
                    checkedList.CheckedFlag = true;
                return linkedListDto;
            }
            foreach (var lst in linkedProductGroup.Where(t => t.ParentId.HasValue))
            {
                linkedListDto.Add(new LinkedListDto { Id = lst.Id, CheckedFlag = false });
                await GetLinkedParendIds(new IdInput { Id = lst.Id}, linkedListDto);
            }
            return linkedListDto;
        }

        public async Task<IdInput> CreateOrUpdateProductGroup(CreateOrUpdateProductGroupInput input)
        {
            if (input.ProductGroup.Id.HasValue)
            {
                return await UpdateProductGroup(input);
            }
            else
            {
                return await CreateProductGroup(input);
            }
        }
        protected virtual async Task<IdInput> UpdateProductGroup(CreateOrUpdateProductGroupInput input)
        {
            var item = await _productGroupRepo.GetAsync(input.ProductGroup.Id.Value);
            var dto = input.ProductGroup;
            dto.MapTo(item);

            CheckErrors(await _productGroupManager.CreateSync(item));

            return new IdInput
            {
                Id = input.ProductGroup.Id.Value
            };
        }

        protected virtual async Task<IdInput> CreateProductGroup(CreateOrUpdateProductGroupInput input)
        {
            var dto = input.ProductGroup.MapTo<ProductGroup>();

            CheckErrors(await _productGroupManager.CreateSync(dto));

            var returnId = dto.Id;

            return new IdInput
            {
                Id = returnId
            };
        }
        public async Task<ProductGroupEditDto> GetOrCreateByName(string groupname, string code)
        {
            ProductGroupEditDto editDto;

            var allItems = _productGroupRepo
               .GetAll()
               .WhereIf(
                   !groupname.IsNullOrEmpty(),
                   p => p.Name.Contains(groupname)
               );

            List<ProductGroup> sortMenuItems = new List<ProductGroup>(allItems);

            if (DynamicQueryable.Any(sortMenuItems))
            {
                editDto = sortMenuItems.FirstOrDefault().MapTo<ProductGroupEditDto>();
            }
            else
            {
                editDto = new ProductGroupEditDto()
                {
                    Name = groupname,
                    Code = code
                };
                var output = await CreateOrUpdateProductGroup(new CreateOrUpdateProductGroupInput()
                {
                    ProductGroup = editDto
                });

                editDto.Id = output.Id;
            }
            return editDto;
        }

        public async Task<ListResultOutput<ProductGroupListDto>> GetProductGroupNames()
        {
            var lstProductGroup = await _productGroupRepo.GetAll().OrderBy(e => e.Name).ToListAsync();
            return new ListResultOutput<ProductGroupListDto>(lstProductGroup.MapTo<List<ProductGroupListDto>>());
        }

        public async Task UpdateCategoryProductgroupId(UpdateProductgroupCategoryInput input)
        {
            if (input.CategoryIds != null && input.CategoryIds.Count > 0)
            {
                foreach (var catId in input.CategoryIds)
                {
                    var category = await _categoryRepo.GetAsync(catId);
                    if (category != null)
                    {
                        category.ProductGroupId = input.ProductGroupId;
                        await _categoryRepo.UpdateAsync(category);
                    }
                }
            }
        }
    }
}