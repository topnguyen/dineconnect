﻿using Abp.AutoMapper;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Menu.Implementation;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Menu.Exporter
{
    public class ScreenMenuExporter : FileExporterBase, IScreenMenuExporter
    {
        public async Task<FileDto> ExportTemplateFile(IList<MenuItemEditDto> dtos)
        {
            var file = new FileDto("ScreenMenuTemplate.xlsx", MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                var tempSheet = excelPackage.Workbook.Worksheets.Add("TEMPLATE");
                AddHeader(tempSheet,
                            L("LocationNameTarget"),
                            L("Category"),
                            L("AliasCode"),
                            L("MenuName"),
                            L("AliasName"),
                            L("Description"),
                            L("Barcode"),
                            L("Portion"),
                            L("Price"),
                            L("Tag"));
                for (var i = 1; i <= 10; i++)
                {
                    tempSheet.Column(i).AutoFit();
                }


                var sheet = excelPackage.Workbook.Worksheets.Add("SAMPLE");
                sheet.OutLineApplyStyle = true;

                AddHeader(sheet,
                            L("LocationNameTarget"),
                            L("Category"),
                            L("AliasCode"),
                            L("MenuName"),
                            L("AliasName"),
                            L("Description"),
                            L("Barcode"),
                            L("Portion"),
                            L("Price"),
                            L("Tag"));

                AddObjects(sheet, 2, dtos,
                            _ => _.LocationName,
                            _ => _.CategoryName,
                            _ => _.AliasCode,
                            _ => _.Name,
                            _ => _.AliasName,
                            _ => _.ItemDescription,
                            _ => _.BarCode,
                            _ => _.PortionName,
                            _ => _.TPPrice,
                            _ => _.Tag
                            );

                //Formatting cells
                for (var i = 1; i <= 10; i++)
                {
                    sheet.Column(i).AutoFit();
                }

                Save(excelPackage, file);
            }

            return file;
        }
    }
}