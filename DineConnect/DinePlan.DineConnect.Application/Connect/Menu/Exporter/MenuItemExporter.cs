﻿using Abp.AutoMapper;
using DinePlan.DineConnect.Connect.ComparisionReport.Dto;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Menu.Implementation;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Menu.Exporter
{
	public class MenuItemListExcelExporter : FileExporterBase, IMenuItemListExcelExporter
	{
		public FileDto ExportToFile(List<MenuItemListDto> dtos)
		{
			var file = new FileDto(@L("MenuItem") + "-" + DateTime.Now.ToString("yyyy-MMMM-dd") + ".xlsx",
				MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

			using (var excelPackage = new ExcelPackage())
			{
				var sheet = excelPackage.Workbook.Worksheets.Add(L("MenuItem"));
				sheet.OutLineApplyStyle = true;

				List<string> headers = new List<string>
				{
					"Id",
					"GroupCode",
					"Group",
					"CategoryCode",
					"Category",
					"AliasCode",
					"MenuName",
					"AliasName",
					"Tag",
					"Portion",
					"Price",
					"Barcode",
					"Description",
					"ForceQuantity",
					"ForceChangePrice",
					"RestrictPromotion",
					"NoTax",
					"NumberOfPax"
				};

				AddHeader(
					sheet,
					headers.ToArray()
					);

				int rowCount = 2;
				var colCount = 1;

				foreach (MenuItemListDto t in dtos)
				{
					foreach (var mportion in t.Portions)
					{
						colCount = 1;
						sheet.Cells[rowCount, colCount++].Value = t.Id;
						sheet.Cells[rowCount, colCount++].Value = t.CategoryProductGroupCode;
						sheet.Cells[rowCount, colCount++].Value = t.CategoryProductGroupName;
						sheet.Cells[rowCount, colCount++].Value = t.CategoryCode;
						sheet.Cells[rowCount, colCount++].Value = t.CategoryName;
						sheet.Cells[rowCount, colCount++].Value = t.AliasCode;
						sheet.Cells[rowCount, colCount++].Value = t.Name;
						sheet.Cells[rowCount, colCount++].Value = t.AliasName;
						sheet.Cells[rowCount, colCount++].Value = t.Tag;
						sheet.Cells[rowCount, colCount++].Value = mportion.Name;
						sheet.Cells[rowCount, colCount++].Value = mportion.Price;
						sheet.Cells[rowCount, colCount++].Value = t.BarCode;
						sheet.Cells[rowCount, colCount++].Value = t.ItemDescription;
						sheet.Cells[rowCount, colCount++].Value = t.ForceQuantity ? "1" : "0";
						sheet.Cells[rowCount, colCount++].Value = t.ForceChangePrice ? "1" : "0";
						sheet.Cells[rowCount, colCount++].Value = t.RestrictPromotion ? "1" : "0";
						sheet.Cells[rowCount, colCount++].Value = t.NoTax ? "1" : "0";
						sheet.Cells[rowCount, colCount++].Value = mportion?.NumberOfPax ; 
						rowCount++;
					}
				}
				for (int i = 1; i <= colCount; i++)
				{
					sheet.Column(i).AutoFit();
				}
				Save(excelPackage, file);
			}

			return file;
		}

		public async Task<FileDto> ExportPriceForLocation(MenuItemAppService appService, int locationId)
		{
			var file = new FileDto("Price-" + DateTime.Now.ToString("yyyy-MMMM-dd") + ".xlsx",
				MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

			using (var excelPackage = new ExcelPackage())
			{
				var sheet = excelPackage.Workbook.Worksheets.Add(L("Price"));
				sheet.OutLineApplyStyle = true;

				AddHeader(
					sheet,
					L("Id"),
					L("MenuName"),
					L("Portion"),
					L("Price"),
					L("LocationPrice")
					);
				GetMenuPortionPriceInput input = new GetMenuPortionPriceInput();
				input.SkipCount = 0;
				input.LocationId = locationId;
				input.Sorting = "Id";
				var rowCount = 2;
				while (true)
				{
					input.MaxResultCount = 100;
					var outPut = await appService.GetLocationMenuItemPrices(input);

					if (DynamicQueryable.Any(outPut.Items))
					{
						AddObjects(
							sheet, rowCount, outPut.Items.MapTo<IList<MenuItemPortionDto>>(),
							_ => _.Id,
							_ => _.MenuName,
							_ => _.PortionName,
							_ => _.Price,
							_ => _.LocationPrice
							);
						input.SkipCount = input.SkipCount + input.MaxResultCount;
						rowCount = input.SkipCount + 2;
					}
					else
					{
						break;
					}
				}

				for (var i = 1; i <= 1; i++)
				{
					sheet.Column(i).AutoFit();
				}

				Save(excelPackage, file);
			}

			return file;
		}

		public FileDto ExportMenuItemDescription(List<MenuItemDescriptionEditDto> dtos)
		{
			var file = new FileDto(@L("MenuItemDescription") + "-" + DateTime.Now.ToString("yyyy-MMMM-dd") + ".xlsx",
			   MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

			using (var excelPackage = new ExcelPackage())
			{
				var sheet = excelPackage.Workbook.Worksheets.Add(L("MenuItemDescription"));
				sheet.OutLineApplyStyle = true;

				AddHeader(
					sheet,
					"MenuItemId",
					"MenuItemName",
					"Language",
					"Name",
					"Description"
					);

				AddObjects(
					sheet, 2, dtos,
					_ => _.MenuItemId,
					_ => _.MenuItemName,
					_ => _.LanguageCode,
					_ => _.ProductName,
					_ => _.Description

					);

				for (var i = 1; i <= 5; i++)
				{
					sheet.Column(i).AutoFit();
				}

				Save(excelPackage, file);
			}

			return file;
		}

		public async Task<FileDto> ExportMenuEngineering(GetMenuEngineeringReportInput input, IMenuItemAppService appService)
		{
			var builder = new StringBuilder();
			builder.Append(L("MenuEngineeringExport"));
			builder.Append(L("-"));
			builder.Append(!input.StartDate.Equals(DateTime.MinValue)
				? input.StartDate.ToString(_simpleDateFormat)
				: DateTime.Now.ToString(_simpleDateFormat));
			builder.Append("_");
			builder.Append(!input.EndDate.Equals(DateTime.MinValue)
				? input.EndDate.ToString(_simpleDateFormat)
				: DateTime.Now.ToString(_simpleDateFormat));

			var file = new FileDto(builder + ".xlsx",
				MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

			using (var excelPackage = new ExcelPackage())
			{
				await GetMenuEngineeringReportExportExcel(excelPackage, input, appService);
				Save(excelPackage, file);
			}
			return  ProcessFile(input, file);
		}
		private async Task GetMenuEngineeringReportExportExcel(ExcelPackage package, GetMenuEngineeringReportInput input, IMenuItemAppService appService)
		{
			var worksheet = package.Workbook.Worksheets.Add(L("MenuEngineeringExport"));
			worksheet.OutLineApplyStyle = true;
			AddHeader(
				worksheet,
				L("Category"),
				L("MenuItem"),
				L("Portion"),
				L("Quantity"),
				L("MenuMix%(QTY)"),
				L("FoodCost"),
				L("Price"),
				L("ItemCM"),
				L("MenuCost"),
				L("MenuRevenue"),
				L("MenuCM"),
				L("CMCAT"),
				L("MMCAT"),
				L("MenuItemClass")
				);

			input.SkipCount = 0;
			input.MaxResultCount = 10000;
			var dtos = await appService.GetMenuEngineeringReport(input);

			//Add  report data
			var row = 2;
			//location
			foreach (var item in dtos.ListItem)
			{
				worksheet.Cells[row, 1].Value = item.CategoryName;
				worksheet.Cells[row, 2].Value = item.MenuItemName;
				worksheet.Cells[row, 3].Value = item.MenuItemPortionName;
				worksheet.Cells[row, 4].Value = item.Quantity.ToString(ConnectConsts.ConnectConsts.NumberFormat);
				worksheet.Cells[row, 5].Value = item.MenuMix.ToString(ConnectConsts.ConnectConsts.NumberFormat) + " %";
				worksheet.Cells[row, 6].Value = item.CostPrice.ToString(ConnectConsts.ConnectConsts.NumberFormat);
				worksheet.Cells[row, 7].Value = item.Price.ToString(ConnectConsts.ConnectConsts.NumberFormat);
				worksheet.Cells[row, 8].Value = item.ItemCM.ToString(ConnectConsts.ConnectConsts.NumberFormat);
				worksheet.Cells[row, 9].Value = item.MenuCost.ToString(ConnectConsts.ConnectConsts.NumberFormat);
				worksheet.Cells[row, 10].Value = item.MenuRevenue.ToString(ConnectConsts.ConnectConsts.NumberFormat);
				worksheet.Cells[row, 11].Value = item.MenuCM.ToString(ConnectConsts.ConnectConsts.NumberFormat);
				worksheet.Cells[row, 12].Value = item.CMCAT;
				worksheet.Cells[row, 13].Value = item.MMCAT;
				worksheet.Cells[row, 14].Value = item.MenuItemClass;
				row++;
			}		
			worksheet.Cells[row, 4].Value = dtos.TotalQuantity.ToString(ConnectConsts.ConnectConsts.NumberFormat);
			worksheet.Cells[row, 9].Value = dtos.TotalMenuCost.ToString(ConnectConsts.ConnectConsts.NumberFormat);
			worksheet.Cells[row, 10].Value = dtos.TotalMenuRevenue.ToString(ConnectConsts.ConnectConsts.NumberFormat);
			worksheet.Cells[row, 11].Value = dtos.TotalMenuCM.ToString(ConnectConsts.ConnectConsts.NumberFormat);
			if (input.IsMenuItems)
			{
				worksheet.DeleteColumn(3);
			}
			row++;
			worksheet.Cells[row, 2].Value = L("MenuItems");
			worksheet.Cells[row, 3].Value = dtos.CountMenuItem.ToString(ConnectConsts.ConnectConsts.NumberFormat);

			row++;
			worksheet.Cells[row, 2].Value = L("MenuFoodCost");
			worksheet.Cells[row, 3].Value = dtos.MenuFoodCost.ToString(ConnectConsts.ConnectConsts.NumberFormat) + " %";

			row++;
			worksheet.Cells[row, 2].Value = L("AverageCM");
			worksheet.Cells[row, 3].Value = dtos.AverageCM.ToString(ConnectConsts.ConnectConsts.NumberFormat);

			row++;
			worksheet.Cells[row, 2].Value = L("PercentageMenuMix");
			worksheet.Cells[row, 3].Value = dtos.PercentageMenuMix.ToString(ConnectConsts.ConnectConsts.NumberFormat) + " %";

			for (var i = 1; i <= 14; i++)
			{
				worksheet.Column(i).AutoFit();
			}			
		}

		public async Task<FileDto> ExportMenuEngineeringByLocation(GetMenuEngineeringReportInput input, IMenuItemAppService appService)
		{
			var builder = new StringBuilder();
			builder.Append(L("MenuEngineeringExport"));
			builder.Append(L("-"));
			builder.Append(!input.StartDate.Equals(DateTime.MinValue)
				? input.StartDate.ToString(_simpleDateFormat)
				: DateTime.Now.ToString(_simpleDateFormat));
			builder.Append("_");
			builder.Append(!input.EndDate.Equals(DateTime.MinValue)
				? input.EndDate.ToString(_simpleDateFormat)
				: DateTime.Now.ToString(_simpleDateFormat));

			var file = new FileDto(builder + ".xlsx",
				MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

			using (var excelPackage = new ExcelPackage())
			{
				await GetMenuEngineeringByLocationExportExcel(excelPackage, input, appService);
				Save(excelPackage, file);
			}
			return ProcessFile(input, file);
		}

		private async Task GetMenuEngineeringByLocationExportExcel(ExcelPackage package, GetMenuEngineeringReportInput input, IMenuItemAppService appService)
		{
			input.SkipCount = 0;
			input.MaxResultCount = 10000;
			var dtos = await appService.GetMenuEngineeringReportByLocation(input);
			var dtosGroupByLocation = dtos.ListItem.GroupBy(a => a.LocationName).Select(x => new
			{
				Location = x.Key,
				ListData = x
			});
			var headers = new List<string>
			{
				L("Category"),
				L("MenuItem"),
				L("Portion"),
				L("Quantity"),
				L("MenuMix%(QTY)"),
				L("FoodCost"),
				L("Price"),
				L("ItemCM"),
				L("MenuCost"),
				L("MenuRevenue"),
				L("MenuCM"),
				L("CMCAT"),
				L("MMCAT"),
				L("MenuItemClass")
			};

			foreach (var location in dtosGroupByLocation)
			{
				var row = 1;
				var worksheet = package.Workbook.Worksheets.Add(location.Location);
				worksheet.OutLineApplyStyle = true;

				worksheet.Cells[row, 1, row, 14].Style.Font.Bold = true;
				worksheet.Cells[row, 1, row, 14].Merge = true;
				worksheet.Cells[row, 1, row, 14].Value =L("Location") + " : " + location.Location;

				row++;
				// add header
				var cellHeader = 1;
				foreach (var header in headers)
				{
					worksheet.Cells[row, cellHeader].Value = header;
					cellHeader++;
				}
				row++;
		
				//location
				foreach (var item in location.ListData)
				{
					worksheet.Cells[row, 1].Value = item.CategoryName;
					worksheet.Cells[row, 2].Value = item.MenuItemName;
					worksheet.Cells[row, 3].Value = item.MenuItemPortionName;
					worksheet.Cells[row, 4].Value = item.Quantity.ToString(ConnectConsts.ConnectConsts.NumberFormat);
					worksheet.Cells[row, 5].Value = item.MenuMix.ToString(ConnectConsts.ConnectConsts.NumberFormat) + " %";
					worksheet.Cells[row, 6].Value = item.CostPrice.ToString(ConnectConsts.ConnectConsts.NumberFormat);
					worksheet.Cells[row, 7].Value = item.Price.ToString(ConnectConsts.ConnectConsts.NumberFormat);
					worksheet.Cells[row, 8].Value = item.ItemCM.ToString(ConnectConsts.ConnectConsts.NumberFormat);
					worksheet.Cells[row, 9].Value = item.MenuCost.ToString(ConnectConsts.ConnectConsts.NumberFormat);
					worksheet.Cells[row, 10].Value = item.MenuRevenue.ToString(ConnectConsts.ConnectConsts.NumberFormat);
					worksheet.Cells[row, 11].Value = item.MenuCM.ToString(ConnectConsts.ConnectConsts.NumberFormat);
					worksheet.Cells[row, 12].Value = item.CMCAT;
					worksheet.Cells[row, 13].Value = item.MMCAT;
					worksheet.Cells[row, 14].Value = item.MenuItemClass;
					row++;
				}
				
				worksheet.Cells[row, 4].Value = dtos.TotalQuantity.ToString(ConnectConsts.ConnectConsts.NumberFormat);
				worksheet.Cells[row, 9].Value = dtos.TotalMenuCost.ToString(ConnectConsts.ConnectConsts.NumberFormat);
				worksheet.Cells[row, 10].Value = dtos.TotalMenuRevenue.ToString(ConnectConsts.ConnectConsts.NumberFormat);
				worksheet.Cells[row, 11].Value = dtos.TotalMenuCM.ToString(ConnectConsts.ConnectConsts.NumberFormat);
				if (input.IsMenuItems)
				{
					worksheet.DeleteColumn(3);
				}
				row++;
				worksheet.Cells[row, 2].Value = L("MenuItems");
				worksheet.Cells[row, 3].Value = dtos.CountMenuItem.ToString(ConnectConsts.ConnectConsts.NumberFormat);

				row++;
				worksheet.Cells[row, 2].Value = L("MenuFoodCost");
				worksheet.Cells[row, 3].Value = dtos.MenuFoodCost.ToString(ConnectConsts.ConnectConsts.NumberFormat) + " %";

				row++;
				worksheet.Cells[row, 2].Value = L("AverageCM");
				worksheet.Cells[row, 3].Value = dtos.AverageCM.ToString(ConnectConsts.ConnectConsts.NumberFormat);

				row++;
				worksheet.Cells[row, 2].Value = L("PercentageMenuMix");
				worksheet.Cells[row, 3].Value = dtos.PercentageMenuMix.ToString(ConnectConsts.ConnectConsts.NumberFormat) + " %";

				for (var i = 1; i <= 14; i++)
				{
					worksheet.Column(i).AutoFit();
				}	
			}
		}

		public async Task<FileDto> ExportMenuEngineeringByMonth(GetMenuEngineeringReportInput input, IMenuItemAppService appService)
		{
			var builder = new StringBuilder();
			builder.Append(L("MenuEngineeringExport"));
			builder.Append(L("-"));
			builder.Append(!input.StartDate.Equals(DateTime.MinValue)
				? input.StartDate.ToString(_simpleDateFormat)
				: DateTime.Now.ToString(_simpleDateFormat));
			builder.Append("_");
			builder.Append(!input.EndDate.Equals(DateTime.MinValue)
				? input.EndDate.ToString(_simpleDateFormat)
				: DateTime.Now.ToString(_simpleDateFormat));

			var file = new FileDto(builder + ".xlsx",
				MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

			using (var excelPackage = new ExcelPackage())
			{
				await GetMenuEngineerByMonthReportExportExcel(excelPackage, input, appService);
				Save(excelPackage, file);
			}
			return ProcessFile(input, file);
		}
		private async Task GetMenuEngineerByMonthReportExportExcel(ExcelPackage package, GetMenuEngineeringReportInput input, IMenuItemAppService appService)
		{
			var startDate = input.StartDate.Date;
			var endDate = input.EndDate.Date;
			while (startDate.Month <= endDate.Month)
			{
				var worksheet = package.Workbook.Worksheets.Add(L("MenuEngineeringExport")+ L("Month") + ":" + startDate.Month);
				worksheet.OutLineApplyStyle = true;
				AddHeader(
					worksheet,
					L("Category"),
					L("MenuItem"),
					L("Portion"),
					L("Quantity"),
					L("MenuMix%(QTY)"),
					L("FoodCost"),
					L("Price"),
					L("ItemCM"),
					L("MenuCost"),
					L("MenuRevenue"),
					L("MenuCM"),
					L("CMCAT"),
					L("MMCAT"),
					L("MenuItemClass")
					);

				input.SkipCount = 0;
				input.MaxResultCount = 10000;
				var dateRange = GetStartAndEndMonth(startDate);
				input.StartDate = dateRange.StartDate;
				input.EndDate = dateRange.EndDate;
				var dtos = await appService.GetMenuEngineeringReport(input);

				//Add  report data
				var row = 2;
				//location
				foreach (var item in dtos.ListItem)
				{
					worksheet.Cells[row, 1].Value = item.CategoryName;
					worksheet.Cells[row, 2].Value = item.MenuItemName;
					worksheet.Cells[row, 3].Value = item.MenuItemPortionName;
					worksheet.Cells[row, 4].Value = item.Quantity.ToString(ConnectConsts.ConnectConsts.NumberFormat);
					worksheet.Cells[row, 5].Value = item.MenuMix.ToString(ConnectConsts.ConnectConsts.NumberFormat) + " %";
					worksheet.Cells[row, 6].Value = item.CostPrice.ToString(ConnectConsts.ConnectConsts.NumberFormat);
					worksheet.Cells[row, 7].Value = item.Price.ToString(ConnectConsts.ConnectConsts.NumberFormat);
					worksheet.Cells[row, 8].Value = item.ItemCM.ToString(ConnectConsts.ConnectConsts.NumberFormat);
					worksheet.Cells[row, 9].Value = item.MenuCost.ToString(ConnectConsts.ConnectConsts.NumberFormat);
					worksheet.Cells[row, 10].Value = item.MenuRevenue.ToString(ConnectConsts.ConnectConsts.NumberFormat);
					worksheet.Cells[row, 11].Value = item.MenuCM.ToString(ConnectConsts.ConnectConsts.NumberFormat);
					worksheet.Cells[row, 12].Value = item.CMCAT;
					worksheet.Cells[row, 13].Value = item.MMCAT;
					worksheet.Cells[row, 14].Value = item.MenuItemClass;
					row++;
				}
				
				worksheet.Cells[row, 4].Value = dtos.TotalQuantity.ToString(ConnectConsts.ConnectConsts.NumberFormat);
				worksheet.Cells[row, 9].Value = dtos.TotalMenuCost.ToString(ConnectConsts.ConnectConsts.NumberFormat);
				worksheet.Cells[row, 10].Value = dtos.TotalMenuRevenue.ToString(ConnectConsts.ConnectConsts.NumberFormat);
				worksheet.Cells[row, 11].Value = dtos.TotalMenuCM.ToString(ConnectConsts.ConnectConsts.NumberFormat);
				if (input.IsMenuItems)
				{
					worksheet.DeleteColumn(3);
				}
				row++;
				worksheet.Cells[row, 2].Value = L("MenuItems");
				worksheet.Cells[row, 3].Value = dtos.CountMenuItem.ToString(ConnectConsts.ConnectConsts.NumberFormat);

				row++;
				worksheet.Cells[row, 2].Value = L("MenuFoodCost");
				worksheet.Cells[row, 3].Value = dtos.MenuFoodCost.ToString(ConnectConsts.ConnectConsts.NumberFormat) + " %";

				row++;
				worksheet.Cells[row, 2].Value = L("AverageCM");
				worksheet.Cells[row, 3].Value = dtos.AverageCM.ToString(ConnectConsts.ConnectConsts.NumberFormat);

				row++;
				worksheet.Cells[row, 2].Value = L("PercentageMenuMix");
				worksheet.Cells[row, 3].Value = dtos.PercentageMenuMix.ToString(ConnectConsts.ConnectConsts.NumberFormat) + " %";

				for (var i = 1; i <= 14; i++)
				{
					worksheet.Column(i).AutoFit();
				}
				startDate = startDate.AddMonths(1);	
			}
		}

		private StartAndEndMonthlyDto GetStartAndEndMonth(DateTime dateTime)
		{
			var startDate = new DateTime(dateTime.Year, dateTime.Month, 1);
			var endDate = startDate.AddMonths(1).AddMinutes(-1);

			return new StartAndEndMonthlyDto()
			{
				StartDate = startDate,
				EndDate = endDate
			};
		}


	}
}
