﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Menu.Exporter
{
    public class CategoryListExcelExporter : FileExporterBase, ICategoryListExcelExporter
    {
        public FileDto ExportToFile(List<CategoryListDto> dtos)
        {
            return CreateExcelPackage(
                "CategoryList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Category"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Code"),
                        L("Name"),
                        L("ProductGroup")


                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.Code,
                        _ => _.Name,
                        _ => _.GroupName
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
