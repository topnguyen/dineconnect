﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations.Dtos;

namespace DinePlan.DineConnect.Connect.Menu
{
    public interface IScreenMenuAppService : IApplicationService
    {
        Task<PagedResultOutput<ScreenMenuListDto>> GetAll(GetScreenInput inputDto);

        Task<GetScreenMenuForEditOutput> GetScreenMenuForEdit(FilterInputDto input);

        Task<IdInput> CreateOrUpdateScreenMenu(CreateOrUpdateScreenMenuInput input);

        Task DeleteScreenMenu(IdInput input);

        Task CloneMenu(IdInput input);

        Task SaveSortCategories(int[] categories);

        Task SaveSortSortItems(int[] menuItems);

        Task<FileDto> GetAllToExcel();

        Task<PagedResultOutput<ScreenMenuCategoryListDto>> GetCategories(GetScreenMenuInput input);

        Task<GetScreenCategoryForEditOutput> GetCategoryForEdit(NullableIdInput input);

        Task CreateCategory(CreateCategoryInput input);

        Task CreateOrUpdateCategory(CreateOrUpdateCategory input);

        Task DeleteScreenCategory(IdInput input);

        Task<PagedResultOutput<ScreenMenuItemListDto>> GetMenuItems(GetScreenMenuInput input);

        Task<GetScreenMenuItemForEditOutput> GetMenuItemForEdit(NullableIdInput input);

        Task CreateMenuItem(CreateScreenMenuItemInput input);

        Task CreateOrUpdateMenuItem(CreateOrUpdateScreenMenuItem input);

        Task DeleteScreenMenuItem(IdInput input);

        Task<ListResultOutput<ScreenMenuListDto>> GetIds();

        Task<ApiScreenMenuOutput> ApiScreenMenu(ApiLocationInput locationInput);
        Task<ApiScreenMenu> ApiSingleScreenMenu(ApiLocationInput locationInput);

        Task GenerateScreenMenu(IdInput input, bool locationInput);

        Task ClearScreenMenu(IdInput input);

        Task<ApiMenu> ApiMenu(ApiLocationInput locationInput);

        Task<GetScreenItemsOutput> GetScreenItemsForLocation(GetScreenMenuInput input);

        Task<ListResultOutput<ScreenMenuListDto>> GetScreenMenuNames(bool futureData = false);

        Task ActivateItem(IdInput input);
        Task<GetScreenMenuItemBySubMenuOuput> GetMenuItemForVisualScreen(GetScreenMenuInput input);
        ListResultDto<ComboboxItemDto> GetConfirmationTypes();
        Task<ScreenMenuEditDto> GetScreenMenuByNameAndLocation(string name, int locationId);
        Task<FileDto> GetImportTemplate();
    }
}