﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.General;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Menu
{
    public interface IMenuItemAppService : IApplicationService
    {
        Task<PagedResultOutput<MenuItemListDto>> GetAll(GetMenuItemInput inputDto);

        Task<PagedResultOutput<IdNameDto>> GetNonComboItems(GetMenuItemInput inputDto);

        Task<PagedResultOutput<IdNameDto>> GetAllMenuItems(GetMenuItemInput input);

        Task<PagedResultOutput<MenuItemListDto>> GetAllListItems(GetMenuItemLocationInput input, bool restrictLocation = false);
        Task<PagedResultOutput<MenuItemEditDto>> GetAllItems(GetMenuItemLocationInput input, bool restrictLocation = false);

        Task<PagedResultOutput<IdNameDto>> GetAllMenuItemPortions(GetMenuItemInput inputDto);
        Task<PagedResultOutput<IdNameDto>> GetAllMenuItemPortionsBasedOnMenuItemId(GetMenuItemInput input);

        Task<PagedResultOutput<MenuItemPortionDto>> GetLocationMenuItemPrices(GetMenuPortionPriceInput inputDto);

        Task<PagedResultOutput<MenuItemListDto>> GetLocationMenuItems(GetMenuItemLocationInput inputDto);

        Task UpdateLocationPrice(List<LocationPriceInput> input);

        Task<FileDto> GetAllToExcel();

        Task<GetMenuItemForEditOutput> GetMenuItemForEdit(NullableIdInput nullableIdInput);

        Task CreateOrUpdateMenuItem(CreateOrUpdateMenuItemInput input);

        Task DeleteMenuItem(IdInput input);

        Task<ApiMenuItemImageOutput> ApiMenuItemImageForLocation(ApiLocationInput locationInput);

        Task CreateMenuItemToLocation(MenuItemLocationInput input);

        Task DeleteMenuItemFromLocation(MenuItemLocationInput input);

        Task<ListResultOutput<ComboboxItemDto>> GetMenuItemForCategoryCombobox(NullableIdInput input);

        Task<ListResultOutput<SimpleMenuOutput>> GetSimpleMenuItemForCategory(NullableIdInput input);

        Task<PagedResultOutput<LocationPriceOutput>> GetLocationPricesForPortion(GetMenuItemLocationInput input);

        Task<ListResultOutput<ComboboxItemDto>> GetSinglePortionMenuItems();

        Task<ListResultOutput<ComboboxItemDto>> GetComboItems();


        Task<FileDto> GetMenuItemsTemplate(int locationId, bool isExport);

        Task<FileDto> GetMenuPriceTemplate(int locationId);

        Task<ApiMenuOutput> ApiItemsForLocation(ApiLocationInput locationInput);

        Task<List<ApiLocationMenuItemListOutput>> ApiItemsForSpecificLocation(int locationId);

        Task<ApiMenuOutput> ApiGetMenuForLocation(ApiLocationInput locationInput);

        Task<bool> IsBarcodeExists(string barcode);

        Task DeleteBarcode(string barcode);

        Task<bool> CloneMenuItemAsMaterial(IdInput input);

        Task<NameValueDto> GetMenuItemById(string input);

        Task<List<MenuItemLocationPriceDto>> GetLocationPricesForMenu(GetMenuPortionPriceInput input);

        Task<ListResultOutput<MenuItemListDto>> GetMenuItemNames();

        Task ActivateItem(IdInput input);

        ListResultDto<ComboboxItemDto> GetFoodTypes();
        Task<FileDto> GetMenuItemDescriptionForExport(GetMenuItemDescriptionExportInput input);
        Task<PagedResultOutput<IdNameDto>> GetAllMenuItemsBasedOnCatgeoryId(GetMenuItemInput input);
        Task<List<ProductComboGroupEditDto>> GetProductComboGroupsBasedOnMenuItem(IdInput input);
        Task SaveSortCombogroupItems(MenuItemSortingOutputDto input);
        Task<PagedResultOutput<UpMenuItemLocationPriceListDto>> GetAllUpMenuItemLocationPrice(GetUpMenuItemLocationPriceInput input);
        Task<GetUpMenuItemLocationPriceForEditOutput> GetUpMenuItemLocationPriceForEdit(NullableIdInput input);
        Task CreateUpMenuItemLocationPrice(CreateUpMenuItemLocationPrice input);
        Task DeleteUpMenuItemLocationPrice(IdInput input);
        Task<GetMenuEngineeringReportOutputDto> GetMenuEngineeringReport(GetMenuEngineeringReportInput input);

        Task<FileDto> GetMenuEngineeringForExport(GetMenuEngineeringReportInput input);
        Task<FileDto> GetMenuEngineeringByLocationForExport(GetMenuEngineeringReportInput input);
        Task<FileDto> GetMenuEngineeringByMonthForExport(GetMenuEngineeringReportInput input);
        Task<GetMenuEngineeringReportOutputDto> GetMenuEngineeringReportByLocation(GetMenuEngineeringReportInput input);


        Task<List<MenuItem>> ApiGetAllMenuItemsForSync(ApiLocationInput input);
        Task<List<ProductCombo>> ApiGetAllComboesForSync(ApiLocationInput input);

    }
}