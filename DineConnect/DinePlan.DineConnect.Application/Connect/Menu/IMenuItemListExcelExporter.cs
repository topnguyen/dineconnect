﻿
using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Menu.Implementation;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Menu
{
    public interface IMenuItemListExcelExporter
    {
        FileDto ExportToFile(List<MenuItemListDto> dtos);
        Task<FileDto> ExportPriceForLocation(MenuItemAppService menuItemAppService, int locationId);
        FileDto ExportMenuItemDescription(List<MenuItemDescriptionEditDto> dtos);
        Task<FileDto> ExportMenuEngineering(GetMenuEngineeringReportInput input, IMenuItemAppService appService);
        Task<FileDto> ExportMenuEngineeringByLocation(GetMenuEngineeringReportInput input, IMenuItemAppService appService);
        Task<FileDto> ExportMenuEngineeringByMonth(GetMenuEngineeringReportInput input, IMenuItemAppService appService);
    }
}