﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.General;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Menu
{
    public interface ICategoryAppService : IApplicationService
    {
        Task<PagedResultOutput<CategoryListDto>> GetAll(GetCategoryInput inputDto);

        Task<ListResultOutput<CategoryListDto>> GetCategories();
        Task<PagedResultOutput<CategoryListDto>> GetAllCategoyItems(GetCategoryInput input);

        Task<FileDto> GetAllToExcel();

        Task<GetCategoryForEditOutput> GetCategoryForEdit(NullableIdInput nullableIdInput);

        Task<int> CreateOrUpdateCategory(CreateOrUpdateCategoryInput input);

        Task DeleteCategory(IdInput input);

        Task<CategoryEditDto> GetOrCreateByName(string input);

        Task<CategoryEditDto> GetOrCreateCategory(CategoryEditDto input);

        Task ActivateItem(IdInput input);
        Task<PagedResultOutput<IdNameDto>> GetAllCatgeoriesBasedOnProductGroupId(GetCategoryInput input);
    }
}