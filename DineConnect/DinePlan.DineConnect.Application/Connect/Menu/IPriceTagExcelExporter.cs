﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Menu.Implementation;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Menu
{
    public interface IPriceTagExcelExporter
    {
        Task<FileDto> ExportImportTemplate(GetPriceTagInput name, PriceTagAppService priceTagAppService);
    }
}