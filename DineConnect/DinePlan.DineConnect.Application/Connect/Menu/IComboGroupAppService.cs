﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Menu
{
    public interface IComboGroupAppService : IApplicationService
    {
        Task CreateOrUpdateComboGroup(ProductComboGroupEditDto input);

        Task Delete(IdInput input);

        Task<PagedResultOutput<ProductComboGroupEditDto>> GetAllComboGroups(GetProductComboGroupIntput input);

        Task<ProductComboGroupEditDto> GetComboGroupForEdit(NullableIdInput input);
        Task<ProductComboItemEditDto> GetComboGroupItemForEdit(NullableIdInput input);
    }
}