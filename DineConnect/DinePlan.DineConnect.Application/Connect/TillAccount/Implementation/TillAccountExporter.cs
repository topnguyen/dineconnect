﻿using System;
using System.Collections.Generic;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.TillAccount.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;

namespace DinePlan.DineConnect.Connect.TillAccount.Implementation
{
    public class TillAccountListExcelExporter : FileExporterBase, ITillAccountListExcelExporter
    {
        private readonly ITenantSettingsAppService _tenantSettingsService;
        public TillAccountListExcelExporter(ITenantSettingsAppService tenantSettingsService)
        {
            _tenantSettingsService = tenantSettingsService;
        }
        public FileDto ExportToFile(List<TillAccountListDto> dtos)
        {
            return CreateExcelPackage(
                "TillAccountList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("TillAccount"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }

        public async Task<FileDto> ExportToFilePromotionOrders(GetTillAccountInput input, TillAccountAppService appService)
        {
            var file = new FileDto("Till-" + DateTime.Now.ToString("yyyy-MMMM-dd") + ".xlsx",
             MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                var setting = await _tenantSettingsService.GetAllSettings();
                var dateTimeFormat = setting.Connect.DateTimeFormat;
                var dateFormat = setting.Connect.SimpleDateFormat;
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Till"));
                sheet.OutLineApplyStyle = true;

                AddHeader(
                    sheet,
                    L("Location"),
                    L("Date"),
                    L("Time"),
                    L("Account"),
                    L("Remarks"),
                    L("User"),
                    L("Amount")
                    );

                var rowCount = 2;
                var secondTime = false;
                var allTot = 0M;


                while (true)
                {
                    input.MaxResultCount = 100;
                    if (secondTime)
                    {
                        input.NotCorrectDate = true;
                    }
                    var outPut = await appService.GetTillTransactionReport(input);

                    if (!secondTime)
                    {
                        secondTime = true;
                    }


                    if (DynamicQueryable.Any(outPut.Items))
                    {
                        rowCount = input.SkipCount + 2;

                        foreach (var old in outPut.Items)
                        {
                             allTot += old.TotalAmount;

                            var colCount = 1;
                            sheet.Cells[rowCount, colCount++].Value = old.LocationName;
                            sheet.Cells[rowCount, colCount++].Value = old.TransactionTime.ToString(dateFormat);
                            sheet.Cells[rowCount, colCount++].Value = old.TransactionTime.ToShortTimeString();
                            sheet.Cells[rowCount, colCount++].Value = old.TillAccountName;
                            sheet.Cells[rowCount, colCount++].Value = old.Remarks;
                            sheet.Cells[rowCount, colCount++].Value = old.User;
                            sheet.Cells[rowCount, colCount++].Value = old.TotalAmount;
                            rowCount++;
                        }

                        input.SkipCount = input.SkipCount + input.MaxResultCount;
                    }
                    else
                    {
                        break;
                    }
                }
                rowCount++;
                sheet.Cells[rowCount, 1].Value = L("Total");
                sheet.Cells[rowCount, 7].Value = allTot;

                for (var i = 1; i <= 1; i++)
                {
                    sheet.Column(i).AutoFit();
                }

                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }
    }
}