﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Castle.Core.Logging;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Connect.Departments.Dtos;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Connect.Till;
using DinePlan.DineConnect.Connect.TillAccount.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Report;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.TillAccount.Implementation
{
    public class TillAccountAppService : DineConnectAppServiceBase, ITillAccountAppService
    {
        private readonly ILogger _logger;
        private readonly IReportBackgroundAppService _rbas;
        private readonly IBackgroundJobManager _bgm;
        private readonly ISyncAppService _syncAppService;
        private readonly ITillAccountListExcelExporter _tillaccountExporter;
        private readonly IRepository<Till.TillAccount> _tillaccountManager;
        private readonly IRepository<TillTransaction> _transManager;
        private readonly ITillAccountListExcelExporter _exporter;
        private readonly ILocationAppService _locAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public TillAccountAppService(IRepository<Till.TillAccount> tillaccountManager, ILocationAppService locAppService,
            ITillAccountListExcelExporter exporter,
            IRepository<TillTransaction> transManager,
            ITillAccountListExcelExporter tillaccountExporter, ISyncAppService syncAppService, ILogger logger,
            IReportBackgroundAppService rbas, IBackgroundJobManager bgm
            , IUnitOfWorkManager unitOfWorkManager
            )
        {
            _exporter = exporter;
            _locAppService = locAppService;
            _tillaccountManager = tillaccountManager;
            _transManager = transManager;
            _logger = logger;
            _rbas = rbas;
            _bgm = bgm;
            _unitOfWorkManager = unitOfWorkManager;
            _tillaccountExporter = tillaccountExporter;
            _syncAppService = syncAppService;
        }

        public async Task<PagedResultOutput<TillAccountListDto>> GetAll(GetTillAccountInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var allItems = _tillaccountManager
                    .GetAll()
                    .Where(i => i.IsDeleted == input.IsDeleted)
                    .WhereIf(
                        !input.Filter.IsNullOrEmpty(),
                        p => p.Name.Contains(input.Filter)
                    )
                    .WhereIf(
                        input.AccountType.HasValue,
                        p => p.TillAccountTypeId.Equals(input.AccountType.Value)
                    );
                var allMyEnItems = SearchLocation(allItems, input.LocationGroup).OfType<Till.TillAccount>();

                var allItemCount = allMyEnItems.Count();

                var sortMenuItems = allMyEnItems.AsQueryable()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList();

                var allListDtos = sortMenuItems.MapTo<List<TillAccountListDto>>();


                foreach (var loca in allListDtos)
                {
                    if (string.IsNullOrEmpty(loca.Locations)) continue;
                    var allLo = JsonConvert.DeserializeObject<List<ComboboxItemDto>>(loca.Locations);
                    loca.DisplayLocation = string.Join(", ", allLo.Select(a => a.DisplayText));
                }



                return new PagedResultOutput<TillAccountListDto>(
                    allItemCount,
                    allListDtos
                    );
            }
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _tillaccountManager.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<TillAccountListDto>>();
            return _tillaccountExporter.ExportToFile(allListDtos);
        }

        public async Task<GetTillAccountForEditOutput> GetTillAccountForEdit(NullableIdInput input)
        {
            GetTillAccountForEditOutput output = new GetTillAccountForEditOutput();
            TillAccountEditDto editDto = new Dto.TillAccountEditDto();

            if (input.Id.HasValue)
            {
                var hDto = await _tillaccountManager.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<TillAccountEditDto>();
            }
            else
            {
                editDto = new TillAccountEditDto();
            }
            UpdateLocationAndNonLocationForEdit(editDto, output.LocationGroup);

            output.TillAccount = editDto;
            return output;
        }

        public async Task CreateOrUpdateTillAccount(CreateOrUpdateTillAccountInput input)
        {
            if (input?.LocationGroup == null)
                return;

            if (input.TillAccount.Id.HasValue)
            {
                await UpdateTillAccount(input);
            }
            else
            {
                await CreateTillAccount(input);
            }
            await _syncAppService.UpdateSync(SyncConsts.TILLACCOUNT);
        }

        public async Task DeleteTillAccount(IdInput input)
        {
            await _tillaccountManager.DeleteAsync(input.Id);
        }

        public async Task<ListResultOutput<ApiTillAccountOutput>> ApiGetAll(ApiLocationInput input)
        {
            //var lastSyncTime = input.TimeZone != null
            //  ? TimeZoneInfo.ConvertTime(input.LastSyncTime, input.TimeZone, TimeZoneInfo.Local)
            //  : input.LastSyncTime;

            //var tillAccounts = await _tillaccountManager.GetAllListAsync(i => 
            //(i.LastModificationTime != null && i.LastModificationTime > lastSyncTime) ||
            //(i.LastModificationTime == null && i.CreationTime > lastSyncTime));

            var tillAccounts = await _tillaccountManager.GetAllListAsync(a => a.TenantId == input.TenantId);


            var apiOuputs = new List<ApiTillAccountOutput>();
            if (input.LocationId > 0)
            {
                foreach (var myaccount in tillAccounts)
                {
                    if (await _locAppService.IsLocationExists(new CheckLocationInput
                    {
                        LocationId = input.LocationId,
                        Locations = myaccount.Locations,
                        Group = myaccount.Group,
                        LocationTag = myaccount.LocationTag,
                        NonLocations = myaccount.NonLocations
                    }))
                    {
                        apiOuputs.Add(new ApiTillAccountOutput
                        {
                            Id = myaccount.Id,
                            Name = myaccount.Name,
                            TillAccountTypeId = myaccount.TillAccountTypeId
                        });
                    }
                }
            }
            else
            {
                apiOuputs = tillAccounts.Select(dept => new ApiTillAccountOutput
                {
                    Id = dept.Id,
                    Name = dept.Name,
                    TillAccountTypeId = dept.TillAccountTypeId
                }).ToList();
            }
            return new ListResultOutput<ApiTillAccountOutput>(apiOuputs);
        }

        public async Task<CreateOrUpdateTillOutput> CreateOrUpdateTillTrans(CreateOrUpdateTillInput input)
        {
            var output = new CreateOrUpdateTillOutput();
            try
            {
                var updateTrans = false;
                TillTransaction trans = null;
                if (input.Transaction.Id.HasValue && input.Transaction.Id > 0)
                {
                    updateTrans = true;
                    trans = await _transManager.GetAsync(input.Transaction.Id.Value);
                }
                else
                {
                    trans =
                        await
                            _transManager.FirstOrDefaultAsync(
                                a =>
                                    a.LocalId == input.Transaction.LocalId &&
                                    a.LocationId == input.Transaction.LocationId);

                    if (trans != null)
                    {
                        updateTrans = true;
                    }
                }

                if (trans == null)
                    trans = new TillTransaction();

                trans.TillAccountId = input.Transaction.AccountId;
                trans.TotalAmount = input.Transaction.TotalAmount;
                trans.TransactionTime = input.Transaction.TransactionTime;
                trans.WorkPeriodId = input.Transaction.WorkPeriodId;
                trans.LocalWorkPeriodId = input.Transaction.LocalWorkPeriodId;
                trans.LocationId = input.Transaction.LocationId;
                trans.User = input.Transaction.User;
                trans.Status = input.Transaction.Status;
                trans.LocalId = input.Transaction.LocalId;
                trans.Remarks = input.Transaction.Remarks;

                var id = 0;
                if (updateTrans)
                {
                    id = trans.Id;
                }
                else
                {
                    id = await _transManager.InsertAndGetIdAsync(trans);
                }
                output.Trans = id;
            }
            catch (Exception exception)
            {
                _logger.Fatal("TILL TRANSACTION SYNC FAILED");
                _logger.Fatal("-------------------");
                _logger.Fatal("LOCATION : " + input.Transaction.LocationId);
                _logger.Fatal(exception.StackTrace);
                output.Trans = 0;
            }
            return output;
        }

        public async Task<FileDto> GetTillTransactionReportExcel(GetTillAccountInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new Report.Dtos.CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.TILLTRANSACTION,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                    });
                    if (backGroundId > 0)
                    {
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.TILLTRANSACTION,
                            TillAccountInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value,
                        });
                    }
                }
                else
                {
                    return await _exporter.ExportToFilePromotionOrders(input, this);
                }
            }
            return null;
        }

        public async Task<PagedResultOutput<TillTransactionListDto>> GetTillTransactionReport(GetTillAccountInput input)
        {
            if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue) && !input.NotCorrectDate)
            {
                input.EndDate = input.EndDate.AddDays(1);
            }
            IQueryable<TillTransaction> trans = _transManager.GetAll()
                .WhereIf(!input.UserName.IsNullOrEmpty(), a => a.User.Contains(input.UserName));

            if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
            {
                trans =
                        trans.Where(
                            a =>
                                DbFunctions.TruncateTime(a.TransactionTime) >=
                                DbFunctions.TruncateTime(input.StartDate)
                                &&
                                DbFunctions.TruncateTime(a.TransactionTime) <=
                                DbFunctions.TruncateTime(input.EndDate));
            }

            var locations = new List<int>();
            if (input.Locations != null && input.Locations.Any())
            {
                locations = input.Locations.Select(a => Convert.ToInt32(a.Id)).ToList();
            }
            if (locations.Any())
            {
                trans = trans.Where(a => locations.Contains(a.LocationId));
            }

            var accounts = new List<int>();
            if (input.Accounts != null && input.Accounts.Any())
            {
                accounts = input.Accounts.Select(a => Convert.ToInt32(a.Value)).ToList();
            }
            if (accounts.Any())
            {
                trans = trans.Where(a => accounts.Contains(a.TillAccountId));
            }

            if (input.AccountType > 0)
            {
                trans = trans.Where(a => a.TillAccount.TillAccountTypeId == input.AccountType);
            }

            // filter by dynamic filter
            if (!string.IsNullOrEmpty(input.DynamicFilter))
            {
                var jsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver =
                        new CamelCasePropertyNamesContractResolver()
                };
                var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                if (filRule?.Rules != null)
                {
                    trans = trans.BuildQuery(filRule);
                }
            }


            List<TillTransactionListDto> outputDtos = null;

            var sortTickets = await trans.OrderBy(input.Sorting)
                  .PageBy(input)
                  .ToListAsync();
            outputDtos = sortTickets.MapTo<List<TillTransactionListDto>>();
            var menuItemCount = await trans.CountAsync();
            return new PagedResultOutput<TillTransactionListDto>(
                menuItemCount,
                outputDtos
                );
        }

        protected virtual async Task UpdateTillAccount(CreateOrUpdateTillAccountInput input)
        {
            var item = await _tillaccountManager.GetAsync(input.TillAccount.Id.Value);
            var dto = input.TillAccount;

            item.Name = dto.Name;
            item.TillAccountTypeId = dto.TillAccountTypeId;
            UpdateLocationAndNonLocation(item, input.LocationGroup);
        }

        protected virtual async Task CreateTillAccount(CreateOrUpdateTillAccountInput input)
        {
            var dto = input.TillAccount.MapTo<Till.TillAccount>();
            UpdateLocationAndNonLocation(dto, input.LocationGroup);
            await _tillaccountManager.InsertAsync(dto);
        }

        public virtual async Task<List<TillAccountListDto>> GetTillAccounts()
        {
            var lst = await _tillaccountManager.GetAllListAsync();
            var allListDtos = lst.MapTo<List<TillAccountListDto>>();
            return allListDtos;
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _tillaccountManager.GetAsync(input.Id);
                item.IsDeleted = false;

                await _tillaccountManager.UpdateAsync(item);
            }
        }
    }
}