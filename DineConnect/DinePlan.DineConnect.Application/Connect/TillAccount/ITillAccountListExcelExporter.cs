﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.TillAccount.Dto;
using DinePlan.DineConnect.Connect.TillAccount.Implementation;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.TillAccount
{
    public interface ITillAccountListExcelExporter
    {
        FileDto ExportToFile(List<TillAccountListDto> dtos);
        Task<FileDto> ExportToFilePromotionOrders(GetTillAccountInput input, TillAccountAppService tillAccountAppService);
    }
}