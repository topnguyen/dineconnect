﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Departments.Dtos;
using DinePlan.DineConnect.Connect.TillAccount.Dto;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.Dtos;

namespace DinePlan.DineConnect.Connect.TillAccount
{
    public interface ITillAccountAppService : IApplicationService
    {
        Task<PagedResultOutput<TillAccountListDto>> GetAll(GetTillAccountInput inputDto);

        Task<FileDto> GetAllToExcel();

        Task<GetTillAccountForEditOutput> GetTillAccountForEdit(NullableIdInput nullableIdInput);

        Task CreateOrUpdateTillAccount(CreateOrUpdateTillAccountInput input);

        Task DeleteTillAccount(IdInput input);

        Task<ListResultOutput<ApiTillAccountOutput>> ApiGetAll(ApiLocationInput inputDto);

        Task<CreateOrUpdateTillOutput> CreateOrUpdateTillTrans(CreateOrUpdateTillInput input);

        Task<PagedResultOutput<TillTransactionListDto>> GetTillTransactionReport(GetTillAccountInput inputDto);

        Task<FileDto> GetTillTransactionReportExcel(GetTillAccountInput input);

        Task<List<TillAccountListDto>> GetTillAccounts();

        Task ActivateItem(IdInput input);
    }
}