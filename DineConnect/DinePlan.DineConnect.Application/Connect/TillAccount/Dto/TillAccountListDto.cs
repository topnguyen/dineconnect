﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Till;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter;
using OpenHtmlToPdf;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.TillAccount.Dto
{
    [AutoMapFrom(typeof(Till.TillAccount))]
    public class TillAccountListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
        public string Locations { get; set; }
        public int TillAccountTypeId { get; set; }
        public string DisplayLocation { get; set; }
        public string AccountType => ((TillAccounTypes)TillAccountTypeId).ToString();
    }

    [AutoMapTo(typeof(Till.TillAccount))]
    public class TillAccountEditDto : ConnectEditDto
    {
        public int? Id { get; set; }
        public int TillAccountTypeId { get; set; }
        public string Name { get; set; }
    }

    [AutoMapFrom(typeof(Till.TillTransaction))]
    public class TillTransactionListDto : FullAuditedEntityDto
    {
        public virtual string LocationName { get; set; }
        public virtual int LocationId { get; set; }
        public virtual string Remarks { get; set; }
        public virtual string User { get; set; }
        public virtual DateTime TransactionTime { get; set; }
        public virtual bool Status { get; set; }
        public virtual decimal TotalAmount { get; set; }
        public virtual int TillAccountId { get; set; }
        public virtual string TillAccountName { get; set; }
    }

    public class GetTillAccountInput : PagedAndSortedInputDto, IShouldNormalize, IFileExport, IBackgroundExport
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<LocationListDto> Locations { get; set; }

        public List<ComboboxItemDto> Accounts { get; set; }
        public int? AccountType { get; set; }

        public string Filter { get; set; }
        public bool NotCorrectDate { get; set; }
        public bool IsDeleted { get; set; }
        public string DynamicFilter { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }

        public ExportType ExportOutputType { get; set; }
        public PaperSize PaperSize { get; set; }
        public bool Portrait { get; set; }
        public bool RunInBackground { get; set; }
        public string ReportDescription { get; set; }
        public string UserName { get; set; }
        public GetTillAccountInput()
        {
            LocationGroup = new LocationGroupDto();
        }

        public LocationGroupDto LocationGroup { get; set; }

    }

    public class GetTillAccountForEditOutput : IOutputDto
    {
        public TillAccountEditDto TillAccount { get; set; }
        public LocationGroupDto LocationGroup { get; set; }

        public GetTillAccountForEditOutput()
        {
            LocationGroup = new LocationGroupDto();
        }
    }

    public class CreateOrUpdateTillAccountInput : IInputDto
    {
        [Required]
        public TillAccountEditDto TillAccount { get; set; }

        public LocationGroupDto LocationGroup { get; set; }

        public CreateOrUpdateTillAccountInput()
        {
            LocationGroup = new LocationGroupDto();
        }
    }

    public class ApiTillAccountOutput
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TillAccountTypeId { get; set; }
    }

    public class CreateOrUpdateTillInput
    {
        [Required]
        public TillTransactionInput Transaction { get; set; }
    }

    public class TillTransactionInput
    {
        public int? Id { get; set; }
        public int LocalId { get; set; }
        public int LocationId { get; set; }
        public int AccountId { get; set; }
        public decimal TotalAmount { get; set; }

        public int AccountType { get; set; }
        public string User { get; set; }
        public string Remarks { get; set; }
        public DateTime TransactionTime { get; set; }
        public int LocalWorkPeriodId { get; set; }
        public int WorkPeriodId { get; set; }
        public bool Status { get; set; }
    }

    public class CreateOrUpdateTillOutput
    {
        public int Trans { get; set; }
    }
}