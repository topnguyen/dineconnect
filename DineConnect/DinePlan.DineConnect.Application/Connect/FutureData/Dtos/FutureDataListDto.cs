﻿using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Future;
using System;

namespace DinePlan.DineConnect.Connect.FutureData.Dtos
{
    [AutoMapFrom(typeof(FutureDateInformation))]
    public class FutureDataListDto : CreationAuditedEntity<int?>
    {
        public string Name { get; set; }
        public DateTime FutureDate { get; set; }
        public DateTime? FutureDateTo { get; set; }
        public int FutureDateType { get; set; }
        public string Contents { get; set; }
        public string ObjectContents { get; set; }
        public int Oid { get; set; }
        public string Status { get; set; }
        public int EntityId { get; set; }
    }
}