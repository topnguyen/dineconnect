﻿namespace DinePlan.DineConnect.Connect.FutureData.Dtos
{
    public class FuturePriceDto
    {
        public int? FutureDataId { get; set; }

        public decimal FuturePrice { get; set; }
        public int MenuPortionId { get; set; }
    }
}