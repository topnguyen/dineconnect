﻿using Abp.AutoMapper;
using DinePlan.DineConnect.Connect.Future;
using System;

namespace DinePlan.DineConnect.Connect.FutureData.Dtos
{
    [AutoMapTo(typeof(FutureDateInformation))]
    public class FutureDataEditDto : ConnectEditDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public DateTime FutureDate { get; set; }
        public DateTime? FutureDateTo { get; set; }
        public int FutureDateType { get; set; }
        public string Contents { get; set; }
        public string ObjectContents { get; set; }
        public int Oid { get; set; }
        public string Status { get; set; }
        public int EntityId { get; set; }
        public decimal Value { get; set; }
        public bool IsIncrease { get; set; }
        public bool Applied { get; set; }
    }

    public class MenuItemPriceDto
    {
        public int? menuItemId { get; set; }

        public string menuName { get; set; }

        public string portionName { get; set; }

        public decimal price { get; set; }

        public decimal locationPrice { get; set; }
    }
    public class MenuItemPriceContent
    {
        public int ChangeType { get; set; }
        public decimal Value { get; set; }
        public bool IsIncrease { get; set; }
        public bool Applied { get; set; }
    }
}