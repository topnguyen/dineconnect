﻿using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.FutureData.Dtos
{
    public class GetFutureDataPriceInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public int? FutureId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "MenuName";
            }
        }
    }
}