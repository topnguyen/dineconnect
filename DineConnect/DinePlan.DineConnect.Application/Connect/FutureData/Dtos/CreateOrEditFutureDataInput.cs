﻿using DinePlan.DineConnect.Connect.Location;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.FutureData.Dtos
{
    public class CreateOrEditFutureDataInput
    {
        public CreateOrEditFutureDataInput()
        {
            FutureData = new FutureDataEditDto();
            LocationGroup = new LocationGroupDto();
        }

        [Required]
        public FutureDataEditDto FutureData { get; set; }

        public LocationGroupDto LocationGroup { get; set; }
    }
}