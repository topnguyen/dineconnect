﻿using Abp.AutoMapper;
using DinePlan.DineConnect.Connect.Future;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.FutureData.Dtos
{
    public class ApiFutureDateInformationOutput
    {
        public List<ApiFutureDateInformation> FutureDateInformations { get; set; }

        public ApiFutureDateInformationOutput()
        {
            FutureDateInformations = new List<ApiFutureDateInformation>();
        }
    }

    [AutoMapTo(typeof(FutureDateInformation))]
    public class ApiFutureDateInformation
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public DateTime FutureDate { get; set; }
        public DateTime? FutureDateTo { get; set; }

        public int FutureDateType { get; set; }
        public string Contents { get; set; }
        public string ObjectContents { get; set; }
        public int Oid { get; set; }
        public int EntityId { get; set; }

        public string Locations { get; set; }

        public virtual int TenantId { get; set; }
    }

    public class FutureDatePricesContentsType12
    {
        public List<MenuItemPortionPriceUpdateItem> Items { get; set; }
    }

    public class MenuItemPortionPriceUpdateItem
    {
        public string MenuItemPortionId { get; set; }

        public decimal Price { get; set; }
    }

    public class FutureDatePricesContentsType3
    {
        public List<TaxUpdateItem> Items { get; set; }
    }

    public class TaxUpdateItem
    {
        public string TaxName { get; set; }

        public decimal TaxPercentage { get; set; }
    }
}
