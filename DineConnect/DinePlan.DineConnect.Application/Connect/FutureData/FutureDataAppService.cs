﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Future;
using DinePlan.DineConnect.Connect.FutureData.Dtos;
using DinePlan.DineConnect.Connect.FutureData.Exporting;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Connect.Taxes;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.FutureData
{
    public class FutureDataAppService : DineConnectAppServiceBase, IFutureDataAppService
    {
        private readonly IRepository<FutureDateInformation> _futureDateRepository;
        private readonly IFutureDataExporter _futureDataExporter;
        private readonly IMenuItemAppService _menuItemAppService;
        private readonly IRepository<LocationMenuItemPrice> _locationPriceRepo;
        private readonly IRepository<MenuItem> _menuitemManager;
        private readonly IRepository<Master.Location> _locationRepo;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IDinePlanTaxAppService _dinePlanTaxAppService;
        private readonly IRepository<DinePlanTax> _dineplantaxRepo;
        private readonly IRepository<MenuItemPortion> _menuItemPortionRepo;
        private readonly IRepository<DinePlanTaxLocation> _dineplantaxlocationRepo;
        private readonly IRepository<ScreenMenu> _screenmenuRepo;
        private readonly IScreenMenuAppService _screenMenuAppService;
        private readonly IPriceTagAppService _priceTagAppService;
        private readonly IRepository<ScreenMenuCategory> _screenCatRepo;
        private readonly IRepository<ScreenMenuItem> _screenItemRepo;
        private readonly ILocationAppService _locationAppService;
        private readonly ISyncAppService _syncAppService;
        public FutureDataAppService(
            IRepository<FutureDateInformation> futureDateRepository
            , IFutureDataExporter futureDataExporter
            , IMenuItemAppService menuItemAppService
            , IRepository<LocationMenuItemPrice> locationPriceRepo
            , IRepository<MenuItem> menuitemManager
            , IUnitOfWorkManager unitOfWorkManager,
            IDinePlanTaxAppService dinePlanTaxAppService,
            IRepository<DinePlanTax> dinePlanTaxesRepo,
            IRepository<DinePlanTaxLocation> dineplantaxlocationRepo,
            IRepository<ScreenMenu> screenmenuRepo,
            IScreenMenuAppService screenMenuAppService,
          IPriceTagAppService priceTagAppService,
          IRepository<ScreenMenuCategory> screenCatRepo,
            IRepository<ScreenMenuItem> screenItemRepo,
            ILocationAppService locationAppService,
            IRepository<MenuItemPortion> menuItemPortionRepo,
            IRepository<Master.Location> locationRepo,
        ISyncAppService syncAppService)
        {
            _futureDateRepository = futureDateRepository;
            _futureDataExporter = futureDataExporter;
            _menuItemAppService = menuItemAppService;
            _locationPriceRepo = locationPriceRepo;
            _menuitemManager = menuitemManager;
            _unitOfWorkManager = unitOfWorkManager;
            _dinePlanTaxAppService = dinePlanTaxAppService;
            _dineplantaxRepo = dinePlanTaxesRepo;
            _dineplantaxlocationRepo = dineplantaxlocationRepo;
            _screenmenuRepo = screenmenuRepo;
            _screenMenuAppService = screenMenuAppService;
            _priceTagAppService = priceTagAppService;
            _screenCatRepo = screenCatRepo;
            _screenItemRepo = screenItemRepo;
            _locationAppService = locationAppService;
            _menuItemPortionRepo = menuItemPortionRepo;
            _locationRepo = locationRepo;
            _syncAppService = syncAppService;
        }

        public async Task<PagedResultOutput<FutureDataListDto>> GetFutureDatas(GetFutureDatasInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                if (!input.Filter.IsNullOrEmpty())
                {
                    input.Filter = Regex.Replace(input.Filter.Trim(), @"\s+", " ");
                }

                var query = _futureDateRepository
                   .GetAll()
                   .Where(i => i.IsDeleted == input.IsDeleted)
                   .WhereIf(!input.Filter.IsNullOrEmpty(), p => p.Name.Contains(input.Filter))
                   .WhereIf(input.Type.HasValue, p => p.FutureDateType.Equals(input.Type.Value))
                   .WhereIf(input.FutureDate.HasValue, p => p.FutureDate == input.FutureDate);
                var allMyEnItems = SearchLocation(query, input.LocationGroup).OfType<FutureDateInformation>();

                var allItemCount = allMyEnItems.Count();

                var result = allMyEnItems.AsQueryable()
                   .OrderBy(input.Sorting)
                   .PageBy(input)
                   .ToList();
                var allListDtos = result.MapTo<List<FutureDataListDto>>();
                foreach (var lst in allListDtos)
                {
                    if (lst.EntityId > 0)
                    {
                        lst.Status = "Updated";
                    }
                    else
                    {
                        lst.Status = "";
                    }
                }
                return new PagedResultOutput<FutureDataListDto>(await query.CountAsync(), allListDtos);
            }
        }

        public async Task<ApiFutureDateInformationOutput> ApiFutureDateInformations(ApiLocationInput locationInput)
        {
            var output = new ApiFutureDateInformationOutput();
            locationInput.IsFuturedata = true;
            try
            {
                var orgId = await _locationAppService.GetOrgnizationIdForLocationId(locationInput.LocationId);
                if (orgId > 0)
                {
                    CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);
                }

                var allItems = await _futureDateRepository.GetAllListAsync(a => a.TenantId == locationInput.TenantId && a.FutureDate >= DateTime.Now);


                var returnDto = new List<ApiFutureDateInformation>();
                if (locationInput.LocationId > 0)
                {
                    foreach (var myDto in allItems)
                    {
                        if (await _locationAppService.IsLocationExists(new CheckLocationInput
                        {
                            LocationId = locationInput.LocationId,
                            Locations = myDto.Locations,
                            Group = myDto.Group,
                            NonLocations = myDto.NonLocations,
                            LocationTag = myDto.LocationTag,

                        }))
                        {
                            ApiFutureDateInformation dto = await ConvertOutput(locationInput, myDto);
                            if (dto != null) returnDto.Add(dto);
                        }
                    }
                }
                else
                {
                    returnDto = allItems.MapTo<List<ApiFutureDateInformation>>();
                }
                output.FutureDateInformations = returnDto;
            }
            catch (Exception exception)
            {
                var mess = exception.Message;
            }
            return output;
        }

        private async Task<ApiFutureDateInformation> ConvertOutput(ApiLocationInput locationInput, FutureDateInformation myDto)
        {
            var dto = myDto.MapTo<ApiFutureDateInformation>();
            if (dto.FutureDateType == (int)FutureDateType.PriceTag)
            {
                var priceTagId = Convert.ToInt32(dto.Contents);
                var priceTag = await _priceTagAppService.GetPriceTagForEdit(new NullableIdInput() { Id = priceTagId });
                var menuPrices = (await _priceTagAppService.GetMenuPricesForTag(new GetPriceTagInput()
                {
                    TagId = priceTagId
                })).Items.ToList();

                var items = new List<MenuItemPortionPriceUpdateItem>();
                foreach (var mn in menuPrices)
                {
                    var locationPrices = (await _priceTagAppService.GetAllPriceTagLocationPrice(new GetPriceTagInput()
                    {
                        MaxResultCount = int.MaxValue,
                        TagDefinitionId = mn.PriceTagDefinitionId
                    })).Items.ToList();
                    locationPrices.Where(l => l.LocationId == locationInput.LocationId).ToList().ForEach(l =>
                    {
                        items.Add(new MenuItemPortionPriceUpdateItem()
                        {
                            MenuItemPortionId = mn.Id.ToString(),
                            Price = l.Price
                        });
                    });

                }
                var result = new FutureDatePricesContentsType12()
                {
                    Items = items
                };

                dto.Name = priceTag.PriceTag.Name;
                dto.ObjectContents = JsonConvert.SerializeObject(result);
            }
            else if (dto.FutureDateType == (int)FutureDateType.Tax)
            {
                var taxObject = await GetTaxInfo(myDto);

                var location = taxObject.DinePlanTax.DinePlanTaxLocations.FirstOrDefault(t =>
                    t.LocationGroup.Locations.Any(l => l.Id == locationInput.LocationId)
                    || t.LocationObjects.Any(l => l.Id == locationInput.LocationId));

                if (location == null) return null;

                var result = new FutureDatePricesContentsType3()
                {
                    Items = new List<TaxUpdateItem>{new TaxUpdateItem()
                            {
                                TaxName = taxObject.DinePlanTax.TaxName,
                                TaxPercentage = (decimal)location.TaxPercentage
                            }
                        }
                };
                dto.ObjectContents = JsonConvert.SerializeObject(result);
            }
            else if (dto.FutureDateType == (int)FutureDateType.ScreenMenu)
            {
                var screenId = Convert.ToInt32(dto.Contents);
                var allScreenMenu = await _screenMenuAppService.ApiScreenMenu(locationInput);
                var screenMenu = allScreenMenu.Menu.FirstOrDefault(s => s.Id == screenId);
                dto.ObjectContents = JsonConvert.SerializeObject(screenMenu);
            }
            return dto;
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _futureDateRepository.GetAll().OrderBy(a => a.Name).ToListAsync();

            return _futureDataExporter.ExportToFile(allList.MapTo<List<FutureDataListDto>>());
        }

        public async Task<CreateOrEditFutureDataInput> GetFutureDataForEdit(NullableIdInput input)
        {
            var output = new CreateOrEditFutureDataInput();

            var editDto = new FutureDataEditDto();

            if (input.Id.HasValue)
            {
                var message = await _futureDateRepository.GetAsync(input.Id.Value);
                editDto = message.MapTo<FutureDataEditDto>();
            }
            output.FutureData = editDto;
            UpdateLocationAndNonLocationForEdit(editDto, output.LocationGroup);

            return output;
        }
        public ListResultDto<ComboboxItemDto> GetEnumChangeTypes()
        {
            var returnList = new List<ComboboxItemDto>();

            var i = 1;
            var lst = Enum.GetValues(typeof(EnumChangeType)).Cast<EnumChangeType>();
            foreach (var name in lst)
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name.ToString(),
                    Value = ((int)name).ToString()
                });
                i++;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }
        public async Task<int> CreateOrUpdateFutureData(CreateOrEditFutureDataInput input)
        {
            var output = 0;
            if (input.FutureData.Id.HasValue)
            {
                await UpdateFutureData(input);
                output = input.FutureData.Id.Value;
            }
            else
            {
                output = await CreateFutureData(input);
            }
            return output;
        }

        public async Task DeleteFutureData(NullableIdInput input)
        {
            if (input.Id.HasValue)
            {
                await _futureDateRepository.DeleteAsync(input.Id.Value);
                await _syncAppService.UpdateSync(SyncConsts.FUTUREDATEINFORMATIONS);
            }
        }

        private async Task<int> CreateFutureData(CreateOrEditFutureDataInput input)
        {
            var futureDate = input.FutureData.MapTo<FutureDateInformation>();
            if (input.FutureData.FutureDateType == (int)FutureDateType.MenuItemPrice)
            {
                var changeType = Convert.ToInt32(input.FutureData.Contents);
                futureDate.ObjectContents = JsonConvert.SerializeObject(new MenuItemPriceContent { IsIncrease = input.FutureData.IsIncrease, Value = input.FutureData.Value, ChangeType = changeType, Applied = false });
            }
            UpdateLocationAndNonLocation(futureDate, input.LocationGroup);
            var item = await _futureDateRepository.InsertOrUpdateAndGetIdAsync(futureDate);
            await _syncAppService.UpdateSync(SyncConsts.FUTUREDATEINFORMATIONS);
            return item;
        }

        private async Task<GetDinePlanTaxForEditOutput> GetTaxInfo(FutureDateInformation myDto)
        {
            var taxId = Convert.ToInt32(myDto.Contents);

            var hDto = await _dineplantaxRepo.FirstOrDefaultAsync(t => t.Id == taxId);
            var editDto = hDto.MapTo<DinePlanTaxEditDto>();
            var dineplantaxlocation = await _dineplantaxlocationRepo.GetAllListAsync(t => t.DinePlanTaxRefId == editDto.Id);
            var tempValuesFilter = dineplantaxlocation.MapTo<Collection<DinePlanTaxLocationEditDto>>();
            int sno = 1;
            foreach (var lst in tempValuesFilter)
            {
                lst.FutureDataSno = sno++;
                UpdateLocationAndNonLocationForEdit(lst, lst.LocationGroup);
            }
            editDto.DinePlanTaxLocations = tempValuesFilter;

            return new GetDinePlanTaxForEditOutput
            {
                DinePlanTax = editDto,
                DinePlanTaxLocation = new DinePlanTaxLocationEditDto()
            };
        }

        private async Task UpdateFutureData(CreateOrEditFutureDataInput input)
        {
            var dto = input.FutureData;
            dto.Applied = false;
            if (input.FutureData.FutureDateType == (int)FutureDateType.MenuItemPrice)
            {
                var changeType = Convert.ToInt32(input.FutureData.Contents);
                dto.ObjectContents = JsonConvert.SerializeObject(new MenuItemPriceContent { IsIncrease = input.FutureData.IsIncrease, Value = input.FutureData.Value, ChangeType = changeType, Applied = false });
            }
            var item = await _futureDateRepository.GetAsync(input.FutureData.Id.Value);
            dto.MapTo(item);

            UpdateLocationAndNonLocation(item, input.LocationGroup);

            await _futureDateRepository.UpdateAsync(item);

            await _syncAppService.UpdateSync(SyncConsts.FUTUREDATEINFORMATIONS);

        }
        public async Task UpdateItemPriceByFuture()
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.MustHaveTenant, AppConsts.ConnectFilter))
            {
                var allFuture = _futureDateRepository.GetAll().Where(s => s.FutureDateType == (int)FutureDateType.MenuItemPrice
                                                                        && s.FutureDate == DateTime.Today
                                                                        && !s.Applied);
                var allLocationMenuitem = _locationPriceRepo.GetAll();
                var allPortion = _menuItemPortionRepo.GetAll();
                var allLoc = _locationRepo.GetAll();
                foreach (var future in allFuture)
                {
                    var allMenuitem = _menuitemManager.GetAll().Where(s => s.TenantId == future.TenantId && s.Oid == future.Oid);
                    var content = JsonConvert.DeserializeObject<MenuItemPriceContent>(future.ObjectContents);
                    var allLoction = JsonConvert.DeserializeObject<List<SimpleLocationDto>>(future.Locations);
                    if (allLoction != null)
                    {
                        foreach (var loc in allLoction)
                        {
                            await InsertOrUpdateLocItemPrice(loc.Id, content, allMenuitem);
                        }
                    }
                    else
                    {
                        foreach (var loc in allLoc)
                        {
                            await InsertOrUpdateLocItemPrice(loc.Id, content, allMenuitem);
                        }
                        foreach (var menuitem in allMenuitem)
                        {
                            foreach (var portion in menuitem.Portions)
                            {
                                portion.Price = ReturnPrice(portion.Price, content);
                            }
                            await _menuitemManager.InsertOrUpdateAsync(menuitem);
                        }                        
                    }
                    future.Applied = true;
                    await _futureDateRepository.InsertOrUpdateAsync(future);
                }
            }
        }
        private async Task InsertOrUpdateLocItemPrice(int idLocation, MenuItemPriceContent content, IQueryable<MenuItem> allMenuItems)
        {
            var allPortion = allMenuItems.SelectMany(s => s.Portions);
            var allLocMenuItem = _locationPriceRepo.GetAll();
            var allLocitem = await _menuItemAppService.GetLocationMenuItemPrices(new GetMenuPortionPriceInput()
            {
                LocationId = idLocation,
                MaxResultCount = int.MaxValue
            });

            var inputs = new List<LocationPriceInput>();
            foreach (var item in allLocitem.Items)
            {
                var findLoc = allLocMenuItem.FirstOrDefault(s => s.MenuPortionId == item.Id && s.LocationId == idLocation);
                var price = findLoc == null ? ReturnPrice(item.Price, content) : ReturnPrice(item.LocationPrice != 0 ? item.LocationPrice : item.Price, content);
                inputs.Add(new LocationPriceInput()
                {
                    LocationId = idLocation,
                    Price = price,
                    PortionId = item.Id.Value
                });
            }

            await _menuItemAppService.UpdateLocationPrice(inputs);
        }
        private decimal ReturnPrice(decimal price, MenuItemPriceContent content)
        {
            var flag = content.IsIncrease ? 1 : -1;
            if (content.ChangeType == (int)EnumChangeType.Percentage)
            {
                return price + price * (content.Value / 100) * flag;
            }
            else
            {
                return price + content.Value * flag;
            }
        }
        public ListResultDto<ComboboxItemDto> GetFutureDataTypes()
        {
            var returnList = new List<ComboboxItemDto>();

            var i = 1;
            var lst = Enum.GetValues(typeof(FutureDateType)).Cast<FutureDateType>();
            foreach (var name in lst)
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name.ToString(),
                    Value = ((int)name).ToString()
                });
                i++;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }

        public async Task<PagedResultDto<MenuItemPortionDto>> GetMenuItemPricesForFutureData(GetFutureDataPriceInput input)
        {
            if (!input.Filter.IsNullOrEmpty())
            {
                input.Filter = Regex.Replace(input.Filter.Trim(), @"\s+", " ");
            }

            var futureData = await GetFutureDataForEdit(new NullableIdInput(input.FutureId));

            var locationIds = futureData.LocationGroup.Locations.Select(l => l.Id).ToList();

            var listMenuItems = await GetMenuItems(locationIds);

            var result = listMenuItems
                .SelectMany(a => a.Portions)
                .Select(mip => new MenuItemPortionDto
                {
                    Id = mip.Id,
                    MenuName = _menuitemManager.FirstOrDefault(mip.MenuItemId).Name,
                    PortionName = mip.Name,
                    MenuItemId = mip.MenuItemId,
                    MultiPlier = mip.MultiPlier,
                    Price = mip.Price,
                    LocationPrice = 0M
                })
                .WhereIf(!input.Filter.IsNullOrEmpty(), p => p.MenuName.Contains(input.Filter) || p.PortionName.Contains(input.Filter))
                .AsQueryable();

            var finalList = result.OrderBy(input.Sorting).PageBy(input).ToList();

            var futurePrices = new List<FuturePriceDto>();
            if (!futureData.FutureData.Contents.IsNullOrEmpty())
            {
                futurePrices = JsonConvert.DeserializeObject<List<FuturePriceDto>>(futureData.FutureData.Contents);
            }

            foreach (var item in finalList)
            {
                var price = futurePrices.FirstOrDefault(p => p.MenuPortionId.Equals(item.Id));

                item.LocationPrice = price != null ? price.FuturePrice : 0M;
            }

            return new PagedResultDto<MenuItemPortionDto>(result.Count(), finalList);
        }

        private async Task<List<MenuItemListDto>> GetMenuItems(List<int> locationIds)
        {
            var listMenuItems = new List<MenuItemListDto>();

            if (!locationIds.Any())
            {
                var list = await _menuItemAppService.GetAllListItems(new GetMenuItemLocationInput(), false);
                listMenuItems = list.Items.ToList();
            }

            foreach (var item in locationIds)
            {
                var list = await _menuItemAppService.GetAllListItems(new GetMenuItemLocationInput { LocationId = item }, false);
                if (list.Items.Any())
                {
                    listMenuItems.AddRange(list.Items);
                }
            }

            return listMenuItems;
        }

        public async Task UpdateFuturePrice(FuturePriceDto input)
        {
            if (input.FutureDataId.HasValue)
            {
                var futureData = await _futureDateRepository.GetAsync(input.FutureDataId.Value);

                var futurePrices = new List<FuturePriceDto>();
                if (!futureData.Contents.IsNullOrEmpty())
                {
                    futurePrices = JsonConvert.DeserializeObject<List<FuturePriceDto>>(futureData.Contents);
                }

                var i = 0;
                foreach (var item in futurePrices)
                {
                    if (item.MenuPortionId == input.MenuPortionId)
                    {
                        item.FuturePrice = input.FuturePrice;
                        i++;
                    }
                }

                if (i == 0)
                {
                    futurePrices.Add(input);
                }

                futureData.Contents = JsonConvert.SerializeObject(futurePrices);

                await _futureDateRepository.UpdateAsync(futureData);
            }
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _futureDateRepository.GetAsync(input.Id);
                item.IsDeleted = false;

                await _futureDateRepository.UpdateAsync(item);
            }
        }


        public async Task<PagedResultOutput<FutureDataListDto>> PullFutureDataTaxesToBeUpdated()
        {

            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.MustHaveTenant,
        AppConsts.ConnectFilter))
            {
                var allItems = _futureDateRepository
                .GetAll()
                .Where(i => i.EntityId == 0 && DateTime.Now >= i.FutureDate);

                var sortMenuItems = await allItems.ToListAsync();

                var allItemCount = await allItems.CountAsync();
                var allListPostDataOutputs = sortMenuItems.MapTo<List<FutureDataListDto>>();

                return new PagedResultOutput<FutureDataListDto>(
                    allItemCount,
                    allListPostDataOutputs
                );
            }

        }


        [UnitOfWork]
        public async Task FutureDataUpdationInBackground(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.MustHaveTenant,
       AppConsts.ConnectFilter))
            {
                var futureData = await _futureDateRepository.FirstOrDefaultAsync(t => t.Id == input.Id);
                var tenId = AbpSession.TenantId.HasValue ? AbpSession.TenantId.Value : 0;
                if (futureData != null)
                {
                    if (futureData.EntityId == 0)
                    {
                        if (futureData.FutureDateType == (int)FutureDateType.Tax)
                        {
                            var jsonOutput = JsonConvert.DeserializeObject<GetDinePlanTaxForEditOutput>(futureData.ObjectContents);
                            CreateOrUpdateDinePlanTaxLocationInput createOrUpdateDinePlanTaxLocationInput = new CreateOrUpdateDinePlanTaxLocationInput()
                            {
                                FutureDataId = null,
                                DinePlanTax = jsonOutput.DinePlanTax
                            };
                            var retTaxId = await _dinePlanTaxAppService.CreateOrUpdateDinePlanTax(createOrUpdateDinePlanTaxLocationInput);

                            foreach (var lst in jsonOutput.DinePlanTax.DinePlanTaxLocations)
                            {
                                lst.DinePlanTaxRefId = retTaxId.Id;
                                if (lst.TenantId == 0)
                                    lst.TenantId = futureData.TenantId;
                                await _dinePlanTaxAppService.AddOrEditDinePlanTaxLocation(
                                    new CreateOrUpdateDinePlanTaxLocationInput
                                    { DinePlanTaxLocation = lst });
                            }
                            futureData.EntityId = retTaxId.Id;
                            //await _futureDateRepository.UpdateAsync(futureData);
                        }
                        else if (futureData.FutureDateType == (int)FutureDateType.ScreenMenu)
                        {
                            var jsonOutput = JsonConvert.DeserializeObject<GetScreenMenuForEditOutput>(futureData.ObjectContents);
                            ScreenMenuEditDto editDto = jsonOutput.ScreenMenu.MapTo<ScreenMenuEditDto>();
                            UpdateLocationAndNonLocationForEdit(editDto, editDto.LocationGroup);
                            CreateOrUpdateScreenMenuInput createOrUpdateScreenMenuInput = new CreateOrUpdateScreenMenuInput()
                            {
                                FutureDataId = null,
                                ScreenMenu = editDto,
                                Departments = jsonOutput.Departments,
                                LocationGroup = editDto.LocationGroup
                            };
                            var retScreenMenuId = await _screenMenuAppService.CreateOrUpdateScreenMenu(createOrUpdateScreenMenuInput);
                            futureData.EntityId = retScreenMenuId.Id;
                        }
                        else if (futureData.FutureDateType == (int)FutureDateType.PriceTag)
                        {
                            var jsonOutput = JsonConvert.DeserializeObject<List<TagPriceInput>>(futureData.ObjectContents);
                            if (jsonOutput != null || jsonOutput.Count > 0)
                            {
                                foreach (var lst in jsonOutput)
                                {
                                    TagPriceInput tagPriceInput = new TagPriceInput()
                                    {
                                        Price = lst.Price,
                                        TagId = lst.TagId,
                                        PortionId = lst.PortionId,
                                        FutureDataId = null
                                    };
                                    await _priceTagAppService.UpdateTagPrice(tagPriceInput);
                                }
                                futureData.EntityId = jsonOutput[0].TagId;
                            }
                        }
                        else
                        {

                        }
                    }
                }
            }
        }
    }
}