﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.FutureData.Dtos;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.FutureData
{
    public interface IFutureDataAppService : IApplicationService
    {
        Task<PagedResultOutput<FutureDataListDto>> GetFutureDatas(GetFutureDatasInput input);

        Task<CreateOrEditFutureDataInput> GetFutureDataForEdit(NullableIdInput input);
        Task UpdateItemPriceByFuture();
        Task<int> CreateOrUpdateFutureData(CreateOrEditFutureDataInput input);

        Task DeleteFutureData(NullableIdInput input);

        ListResultDto<ComboboxItemDto> GetFutureDataTypes();
        ListResultDto<ComboboxItemDto> GetEnumChangeTypes();
        Task<FileDto> GetAllToExcel();

        Task<PagedResultDto<MenuItemPortionDto>> GetMenuItemPricesForFutureData(GetFutureDataPriceInput input);

        Task UpdateFuturePrice(FuturePriceDto input);

        Task ActivateItem(IdInput input);
        Task FutureDataUpdationInBackground(IdInput input);

        Task<PagedResultOutput<FutureDataListDto>> PullFutureDataTaxesToBeUpdated();

        Task<ApiFutureDateInformationOutput> ApiFutureDateInformations(ApiLocationInput locationInput);

    }
}