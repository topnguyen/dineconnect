﻿using DinePlan.DineConnect.Connect.FutureData.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.FutureData.Exporting
{
    public interface IFutureDataExporter
    {
        FileDto ExportToFile(List<FutureDataListDto> dtos);
    }
}