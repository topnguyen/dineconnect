﻿using DinePlan.DineConnect.Connect.FutureData.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.FutureData.Exporting
{
    public class FutureDataExporter : FileExporterBase, IFutureDataExporter
    {
        public FileDto ExportToFile(List<FutureDataListDto> dtos)
        {
            return CreateExcelPackage(
                "FutureDataList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(base.L("FutureData"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        base.L("Id"),
                        base.L("Name"),
                        base.L("Type"),
                        base.L("FutureDate")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.Name,
                        _ => MapFutureType(_.FutureDateType),
                        _ => _.FutureDate
                        );

                    var creationTimeColumn = sheet.Column(4);
                    creationTimeColumn.Style.Numberformat.Format = "yyyy-mm-dd hh:mm";

                    for (var i = 1; i <= 5; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }

        private string MapFutureType(int type)
        {
            switch (type)
            {
                case 1:
                    return base.L("MenuItem");

                case 2:
                    return base.L("PriceTag");

                case 3:
                    return base.L("Tax");

                case 4:
                    return base.L("ScreenMenu");

                default:
                    return "";
            }
        }
    }
}