﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Entity;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Connect.Table.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Utility.Exporter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Table.Implementation
{
    public class ConnectTableGroupAppService : DineConnectAppServiceBase, IConnectTableGroupAppService
    {
        private readonly IConnectTableGroupManager _connectTableGroupManager;
        private readonly IRepository<ConnectTableGroup> _connectTableGroupRepo;
        private readonly IExcelExporter _exporter;
        private readonly ISyncAppService _syncAppService;
        private readonly IRepository<ConnectTable> _tableRepo;
        private readonly ILocationAppService _locAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public ConnectTableGroupAppService(IConnectTableGroupManager connectTableGroupManager,
            IRepository<Entity.ConnectTableGroup> connectTableGroupRepo, IRepository<ConnectTable> tableRepo,
        ILocationAppService locAppService,
            IExcelExporter exporter,
            ISyncAppService syncAppService,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _connectTableGroupManager = connectTableGroupManager;
            _connectTableGroupRepo = connectTableGroupRepo;
            _exporter = exporter;
            _syncAppService = syncAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _tableRepo = tableRepo;
            _locAppService = locAppService;
        }

        public async Task<ListResultOutput<ConnectTableGroupEditDto>> ApiGetAll(ApiConnectTableGroupInput input)
        {
            var lastSyncTime = input.TimeZone != null
                 ? TimeZoneInfo.ConvertTime(input.LastSyncTime, input.TimeZone, TimeZoneInfo.Local)
                 : input.LastSyncTime;

            //var lstDept = _connectTableGroupRepo.GetAll().Where(i=> 
            //(i.LastModificationTime != null && i.LastModificationTime > lastSyncTime) ||
            //(i.LastModificationTime == null && i.CreationTime > lastSyncTime)).Include(a => a.ConnectTables);

            var lstDept = _connectTableGroupRepo.GetAll().Where(a=>a.TenantId == input.TenantId).Include(a => a.ConnectTables);


            List<ConnectTableGroupEditDto> deptOutput = new List<ConnectTableGroupEditDto>();
            if (input.LocationId > 0)
            {
                foreach (var mydept in lstDept)
                {
                    if (await _locAppService.IsLocationExists(new CheckLocationInput
                    {
                        LocationId = input.LocationId,
                        Locations = mydept.Locations,
                        Group = mydept.Group,
                        LocationTag= mydept.LocationTag,
                        NonLocations = mydept.NonLocations
                    }))
                    {
                        deptOutput.Add(mydept.MapTo<ConnectTableGroupEditDto>());
                    }
                }
            }
            else
            {
                deptOutput = lstDept.MapTo<List<ConnectTableGroupEditDto>>();
            }
            return new ListResultOutput<ConnectTableGroupEditDto>(deptOutput);
        }

        public async Task<PagedResultOutput<ConnectTableGroupListDto>> GetAll(GetConnectTableGroupInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var allItems = _connectTableGroupRepo.GetAll()
                    .Where(i => i.IsDeleted == input.IsDeleted);

                if (input.Operation == "SEARCH")
                {
                    allItems = allItems
                        .WhereIf(
                            !input.Filter.IsNullOrEmpty(),
                            p => p.Name.Equals(input.Filter)
                        );
                }
                else
                {
                    allItems = allItems
                        .WhereIf(
                            !input.Filter.IsNullOrEmpty(),
                            p => p.Name.Contains(input.Filter)
                        );
                }
                var allMyEnItems = SearchLocation(allItems, input.LocationGroup).OfType<ConnectTableGroup>();

                var allItemCount = allMyEnItems.Count();

                var sortMenuItems = allMyEnItems.AsQueryable()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList();

                var allListDtos = sortMenuItems.MapTo<List<ConnectTableGroupListDto>>();
                

                foreach (var loca in allListDtos)
                {
                    if (string.IsNullOrEmpty(loca.Locations)) continue;
                    var allLo = JsonConvert.DeserializeObject<List<ComboboxItemDto>>(loca.Locations);
                    loca.DisplayLocation = string.Join(", ", allLo.Select(a => a.DisplayText));
                }

                
                return new PagedResultOutput<ConnectTableGroupListDto>(
                    allItemCount,
                    allListDtos
                    );
            }
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _connectTableGroupRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<ConnectTableGroupListDto>>();
            var baseE = new BaseExportObject
            {
                ExportObject = allListDtos,
                ColumnNames = new[] { "Id" }
            };
            return _exporter.ExportToFile(baseE, L("ConnectTableGroup"));
        }

        public async Task<GetConnectTableGroupForEditOutput> GetConnectTableGroupForEdit(NullableIdInput input)
        {
            ConnectTableGroupEditDto editDto;
            GetConnectTableGroupForEditOutput output = new GetConnectTableGroupForEditOutput();
            var allTables = _tableRepo.GetAll().MapTo<List<ConnectTableEditDto>>();

            if (input.Id.HasValue && input.Id.Value > 0)
            {
                var hDto =
                    await
                        _connectTableGroupRepo.GetAll()
                            .Include(a => a.ConnectTables)
                            .FirstAsync(a => a.Id == input.Id.Value);
                editDto = hDto.MapTo<ConnectTableGroupEditDto>();
                var myTaIds = editDto.ConnectTables.Select(a => a.Id).ToList();
                editDto.AllTables = allTables.Where(a => !myTaIds.Contains(a.Id)).ToList();
            }
            else
            {
                editDto = new ConnectTableGroupEditDto { AllTables = allTables };
            }
            UpdateLocationAndNonLocationForEdit(editDto, output.LocationGroup);
            output.ConnectTableGroup = editDto;
            return output;
        }

        public async Task CreateOrUpdateConnectTableGroup(CreateOrUpdateConnectTableGroupInput input)
        {
            if (input?.LocationGroup == null)
                return;
            if (input.ConnectTableGroup.Id.HasValue)
            {
                await UpdateConnectTableGroup(input);
            }
            else
            {
                await CreateConnectTableGroup(input);
            }
            await _syncAppService.UpdateSync(SyncConsts.TABLE);
        }

        public async Task DeleteConnectTableGroup(IdInput input)
        {
            await _connectTableGroupRepo.DeleteAsync(input.Id);
        }

        public async Task<ListResultOutput<ConnectTableGroupListDto>> GetIds()
        {
            var lstConnectTableGroup = await _connectTableGroupRepo.GetAll().ToListAsync();
            return new ListResultOutput<ConnectTableGroupListDto>(lstConnectTableGroup.MapTo<List<ConnectTableGroupListDto>>());
        }

        public async Task<ListResultOutput<ConnectTableGroupListDto>> GetConnectTableGroups()
        {
            var roles = await _connectTableGroupRepo.GetAll().ToListAsync();
            return new ListResultOutput<ConnectTableGroupListDto>(roles.MapTo<List<ConnectTableGroupListDto>>());
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetConnectTableGroupsForCombobox()
        {
            var lstLocation = await _connectTableGroupRepo.GetAllListAsync(a => !a.IsDeleted);

            return
                new ListResultOutput<ComboboxItemDto>(
                    lstLocation.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }

        public async Task<PagedResultOutput<ConnectTableListDto>> GetAllTables(GetConnectTableGroupInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var allItems = _tableRepo.GetAll()
                    .Where(i => i.IsDeleted == input.IsDeleted);

                if (input.Operation == "SEARCH")
                {
                    allItems = allItems
                        .WhereIf(
                            !input.Filter.IsNullOrEmpty(),
                            p => p.Name.Equals(input.Filter)
                        );
                }
                else
                {
                    allItems = allItems
                       .WhereIf(
                           !input.Filter.IsNullOrEmpty(),
                           p => p.Name.Equals(input.Filter)
                       );
                }
                var sortMenuItems = await allItems
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var allListDtos = sortMenuItems.MapTo<List<ConnectTableListDto>>();

                var allItemCount = await allItems.CountAsync();

                return new PagedResultOutput<ConnectTableListDto>(
                    allItemCount,
                    allListDtos
                    );
            }
        }

        protected virtual async Task UpdateConnectTableGroup(CreateOrUpdateConnectTableGroupInput input)
        {
            var dto = input.ConnectTableGroup;

            var groupObject =
                   await
                       _connectTableGroupRepo.GetAll()
                           .Include(a => a.ConnectTables)
                           .FirstAsync(a => a.Id == dto.Id.Value);

            List<int> otherIds = dto.ConnectTables.Where(a => a.Id.HasValue).Select(a => a.Id.Value).ToList();

            List<int> tableIds =
                groupObject.ConnectTables.Where(a => !otherIds.Contains(a.Id)).Select(a => a.Id).ToList();

            foreach (var objec in tableIds.Select(allMyIds => groupObject.ConnectTables.SingleOrDefault(a => a.Id == allMyIds)).Where(objec => objec != null))
            {
                groupObject.ConnectTables.Remove(objec);
            }

            foreach (var allTab in dto.ConnectTables)
            {
                if (allTab.Id.HasValue)
                {
                    if (groupObject.ConnectTables.Any(a => a.Id != allTab.Id))
                    {
                        var tableRe = _tableRepo.Get(allTab.Id.Value);
                        if (tableRe != null)
                        {
                            groupObject.ConnectTables.Add(tableRe);
                        }
                    }
                }
            }
            groupObject.Name = dto.Name;
            UpdateLocationAndNonLocation(groupObject, input.LocationGroup);
            CheckErrors(await _connectTableGroupManager.CreateOrUpdateSync(groupObject));
        }

        protected virtual async Task CreateConnectTableGroup(CreateOrUpdateConnectTableGroupInput input)
        {
            var dto = input.ConnectTableGroup.MapTo<Entity.ConnectTableGroup>();
            dto.ConnectTables.Clear();

            foreach (var table in input.ConnectTableGroup.ConnectTables)
            {
                if (table.Id.HasValue)
                {
                    var tableRe = _tableRepo.Get(table.Id.Value);
                    if (tableRe != null)
                    {
                        dto.ConnectTables.Add(tableRe);
                    }
                }
            }
            UpdateLocationAndNonLocation(dto, input.LocationGroup);
            CheckErrors(await _connectTableGroupManager.CreateOrUpdateSync(dto));
        }

        public async Task<int> CreateOrUpdateConnectTable(CreateOrUpdateConnectTableInput input)
        {
            if (input.Table.Id.HasValue)
            {
                await UpdateConnectTable(input);
                await _syncAppService.UpdateSync(SyncConsts.TABLE);
                return input.Table.Id.Value;
            }
            else
            {
                var output = await CreateConnectTable(input);
                if (output > 0)
                {
                    await _syncAppService.UpdateSync(SyncConsts.TABLE);
                    return output;
                }
            }
            return 0;
        }

        public async Task DeleteConnectTable(IdInput input)
        {
            await _tableRepo.DeleteAsync(input.Id);
        }

        public async Task<GetConnectTableForEditOutput> GetConnectTableForEdit(NullableIdInput input)
        {
            ConnectTableEditDto editDto;
            var allLocations = new List<ComboboxItemDto>();

            if (input.Id.HasValue)
            {
                var hDto = await _tableRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<ConnectTableEditDto>();
            }
            else
            {
                editDto = new ConnectTableEditDto();
            }
            return new GetConnectTableForEditOutput
            {
                Table = editDto
            };
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _tableRepo.GetAsync(input.Id);
                item.IsDeleted = false;

                await _tableRepo.UpdateAsync(item);
            }
        }

        private async Task<int> CreateConnectTable(CreateOrUpdateConnectTableInput input)
        {
            var dto = input.Table.MapTo<Entity.ConnectTable>();
            CheckErrors(await _connectTableGroupManager.CreateOrUpdateSyncTable(dto));
            return dto.Id;
        }

        private async Task UpdateConnectTable(CreateOrUpdateConnectTableInput input)
        {
            var item = await _tableRepo.GetAsync(input.Table.Id.Value);
            var dto = input.Table;
            item.Name = dto.Name;
            item.Pax = dto.Pax;
            CheckErrors(await _connectTableGroupManager.CreateOrUpdateSyncTable(item));
        }
    }
}