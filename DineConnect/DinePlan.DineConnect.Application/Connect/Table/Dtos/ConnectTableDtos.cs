﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Table.Dtos
{
    [AutoMapFrom(typeof(Entity.ConnectTable))]
    public class ConnectTableListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
        public int Pax { get; set; }
    }

    [AutoMapTo(typeof(Entity.ConnectTable))]
    public class ConnectTableEditDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int Pax { get; set; }
    }

    public class CreateOrUpdateConnectTableInput : IInputDto
    {
        [Required]
        public ConnectTableEditDto Table { get; set; }
    }

    public class GetConnectTableForEditOutput : IOutputDto
    {
        public ConnectTableEditDto Table { get; set; }
    }

    [AutoMapFrom(typeof(Entity.ConnectTableGroup))]
    public class ConnectTableGroupListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
        public string DisplayLocation { get; set; }
        public string Locations { get; set; }
    }

    [AutoMapTo(typeof(Entity.ConnectTableGroup))]
    public class ConnectTableGroupEditDto : ConnectEditDto
    {
        public ConnectTableGroupEditDto()
        {
            ConnectTables = new List<ConnectTableEditDto>();
            AllTables = new List<ConnectTableEditDto>();
        }

        public int? Id { get; set; }
        public string Name { get; set; }
        public List<ConnectTableEditDto> ConnectTables { get; set; }
        public List<ConnectTableEditDto> AllTables { get; set; }
    }

    public class GetConnectTableGroupInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public GetConnectTableGroupInput()
        {
            LocationGroup = new LocationGroupDto();
        }
        public LocationGroupDto LocationGroup { get; set; }
        public string Filter { get; set; }

        public string Operation { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    public class GetConnectTableGroupForEditOutput : IOutputDto
    {
        public ConnectTableGroupEditDto ConnectTableGroup { get; set; }
        public LocationGroupDto LocationGroup { get; set; }

        public GetConnectTableGroupForEditOutput()
        {
            ConnectTableGroup = new ConnectTableGroupEditDto();
            LocationGroup = new LocationGroupDto();
        }
    }

    public class CreateOrUpdateConnectTableGroupInput : IInputDto
    {
        [Required]
        public ConnectTableGroupEditDto ConnectTableGroup { get; set; }

        public LocationGroupDto LocationGroup { get; set; }

        public CreateOrUpdateConnectTableGroupInput()
        {
            ConnectTableGroup = new ConnectTableGroupEditDto();
            LocationGroup = new LocationGroupDto();
        }
    }

    public class ApiConnectTableGroupInput
    {
        public int TenantId { get; set; }
        public int LocationId { get; set; }
        public DateTime LastSyncTime { get; set; }
        public TimeZoneInfo TimeZone { get; set; }
    }

    public class ApiConnectTableGroupOutput
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}