﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Table.Dtos
{
    public class ImportTableDto
    {
        public string Group { get; set; }
        public string TableName { get; set; }
        public int Pax { get; set; }

    }

}
