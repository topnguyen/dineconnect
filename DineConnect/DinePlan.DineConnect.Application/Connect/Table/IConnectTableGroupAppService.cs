﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Table.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Table
{
    public interface IConnectTableGroupAppService : IApplicationService
    {
        Task<PagedResultOutput<ConnectTableGroupListDto>> GetAll(GetConnectTableGroupInput inputDto);

        Task<FileDto> GetAllToExcel();

        Task<GetConnectTableGroupForEditOutput> GetConnectTableGroupForEdit(NullableIdInput nullableIdInput);

        Task CreateOrUpdateConnectTableGroup(CreateOrUpdateConnectTableGroupInput input);

        Task DeleteConnectTableGroup(IdInput input);

        Task<ListResultOutput<ConnectTableGroupListDto>> GetIds();

        Task<ListResultOutput<ConnectTableGroupEditDto>> ApiGetAll(ApiConnectTableGroupInput inputDto);

        Task<ListResultOutput<ConnectTableGroupListDto>> GetConnectTableGroups();

        Task<ListResultOutput<ComboboxItemDto>> GetConnectTableGroupsForCombobox();

        Task<PagedResultOutput<ConnectTableListDto>> GetAllTables(GetConnectTableGroupInput input);

        Task<int> CreateOrUpdateConnectTable(CreateOrUpdateConnectTableInput input);

        Task DeleteConnectTable(IdInput input);

        Task<GetConnectTableForEditOutput> GetConnectTableForEdit(NullableIdInput nullableIdInput);

        Task ActivateItem(IdInput input);
    }
}