﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Device;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Dto;
namespace DinePlan.DineConnect.Connect.Prints.DineDevices.Dtos
{
    [AutoMapFrom(typeof(DineDevice))]
    public class DineDeviceListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
        public string Settings { get; set; }
        public int DeviceType { get; set; } // 0-> DineChef 1->DineQueue
        public string DineDeviceTypeName
        {
            get
            {
                return ((DineDeviceType)DeviceType).ToString();
            }
        }
        public string Locations { get; set; }
    }
    [AutoMapTo(typeof(DineDevice))]
    public class DineDeviceEditDto : ConnectEditDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Settings { get; set; }
        public int DeviceType { get; set; } // 0-> DineChef 1->DineQueue
        public string DineDeviceTypeName { get; set; }
    }

    public class GetDineDeviceInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }
    public class GetDineDeviceForEditOutput : IOutputDto
    {
        public DineDeviceEditDto DineDevice { get; set; }
    }
    public class CreateOrUpdateDineDeviceInput : IInputDto
    {
        public CreateOrUpdateDineDeviceInput()
        {
            DineDevice = new DineDeviceEditDto();
            LocationGroup = new LocationGroupDto();
        }
        [Required]
        public DineDeviceEditDto DineDevice { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
    }
}

