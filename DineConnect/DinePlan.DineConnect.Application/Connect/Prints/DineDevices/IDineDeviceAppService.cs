﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Prints.DineDevices.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.General;

namespace DinePlan.DineConnect.Connect.Prints
{
    public interface IDineDeviceAppService : IApplicationService
    {
        Task<PagedResultOutput<DineDeviceListDto>> GetAll(GetDineDeviceInput inputDto);
        Task<FileDto> GetAllToExcel(GetDineDeviceInput input);
        Task<CreateOrUpdateDineDeviceInput> GetDineDeviceForEdit(NullableIdInput input);
        Task CreateOrUpdateDineDevice(CreateOrUpdateDineDeviceInput input);
        Task DeleteDineDevice(IdInput input);
        ListResultDto<ComboboxItemDto> GetDineDeviceTypes();
        Task<List<FormlyType>> GetDineDeviceTypeSetting(IdInput input);

        Task<PagedResultOutput<DineDeviceListDto>> ApiGetAll(ApiLocationInput input);
    }
}