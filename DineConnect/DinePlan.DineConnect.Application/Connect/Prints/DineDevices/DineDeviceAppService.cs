﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Device;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Prints.DineDevices;
using DinePlan.DineConnect.Connect.Prints.DineDevices.Dtos;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Filter;
using DinePlan.DineConnect.General;

namespace DinePlan.DineConnect.Connect.Prints.Implementation
{
    public class DineDeviceAppService : DineConnectAppServiceBase, IDineDeviceAppService
    {
        private readonly IDineDeviceListExcelExporter _dinedeviceExporter;
        private readonly IRepository<DineDevice> _dinedeviceManager;
        private readonly ISyncAppService _syncAppService;

        public DineDeviceAppService(IRepository<DineDevice> dinedeviceManager,
            IDineDeviceListExcelExporter dinedeviceExporter, ISyncAppService syncAppService)
        {
            _dinedeviceManager = dinedeviceManager;
            _dinedeviceExporter = dinedeviceExporter;
            _syncAppService = syncAppService;
        }

        public async Task<PagedResultOutput<DineDeviceListDto>> ApiGetAll(ApiLocationInput input)
        {
            var allItems = _dinedeviceManager
                .GetAll()
                .Where(s => s.Id > 0);

            var allMyEnItems = SearchLocation(allItems, input.LocationId).OfType<DineDevice>();

            var sortMenuItems = allMyEnItems.AsQueryable().ToList();

            var allListDtos = sortMenuItems.MapTo<List<DineDeviceListDto>>();
            var allItemCount = allMyEnItems.Count();

            return new PagedResultOutput<DineDeviceListDto>(
                allItemCount,
                allListDtos
                );
        }

        private IEnumerable<ConnectFullMultiTenantAuditEntity> SearchLocation(IQueryable<ConnectFullMultiTenantAuditEntity> allEntities, int locationId)
        {
            var allCodes = new List<int>() { locationId };
            return allEntities.ToList().Where(a => (string.IsNullOrEmpty(a.Locations) && !a.LocationTag && !a.Group) || CompareList(a.Locations, allCodes)).ToList();
        }


        public async Task<FileDto> GetAllToExcel(GetDineDeviceInput input)
        {
            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<DineDeviceListDto>>();
            return _dinedeviceExporter.ExportToFile(allListDtos);
        }

        public async Task<PagedResultOutput<DineDeviceListDto>> GetAll(GetDineDeviceInput input)
        {
            var allItems = _dinedeviceManager
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Name.Contains(input.Filter)
                ).Where(s => s.Id > 0);
            var allMyEnItems = SearchLocation(allItems, input.LocationGroup).OfType<DineDevice>();

            var sortMenuItems = allMyEnItems.AsQueryable()
           .OrderBy(input.Sorting)
           .PageBy(input)
           .ToList();

            var allListDtos = sortMenuItems.MapTo<List<DineDeviceListDto>>();
            var allItemCount = allMyEnItems.Count();

            return new PagedResultOutput<DineDeviceListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<CreateOrUpdateDineDeviceInput> GetDineDeviceForEdit(NullableIdInput input)
        {
            var output = new CreateOrUpdateDineDeviceInput();

            var editDto = new DineDeviceEditDto();

            if (input.Id.HasValue)
            {
                var hDto = await _dinedeviceManager.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<DineDeviceEditDto>();
            }
            output.DineDevice = editDto;
            UpdateLocationAndNonLocationForEdit(editDto, output.LocationGroup);

            return output;
        }

        public async Task CreateOrUpdateDineDevice(CreateOrUpdateDineDeviceInput input)
        {
            if (input.DineDevice.Id.HasValue)
            {
                await UpdateDineDevice(input);
            }
            else
            {
                await CreateDineDevice(input);
            }
            await _syncAppService.UpdateSync(SyncConsts.DINEDEVICE);
        }

        public async Task DeleteDineDevice(IdInput input)
        {
            await _dinedeviceManager.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateDineDevice(CreateOrUpdateDineDeviceInput input)
        {
            var dto = input.DineDevice;
            var item = await _dinedeviceManager.GetAsync(input.DineDevice.Id.Value);
            dto.MapTo(item);
            UpdateLocationAndNonLocation(item, input.LocationGroup);
            await _dinedeviceManager.UpdateAsync(item);
        }

        protected virtual async Task CreateDineDevice(CreateOrUpdateDineDeviceInput input)
        {
            var dto = input.DineDevice.MapTo<DineDevice>();
            UpdateLocationAndNonLocation(dto, input.LocationGroup);
            await _dinedeviceManager.InsertAsync(dto);
        }
        public ListResultDto<ComboboxItemDto> GetDineDeviceTypes()
        {
            var returnList = new List<ComboboxItemDto>();

            var i = 0;
            foreach (var name in Enum.GetNames(typeof(DineDeviceType)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }
        public async Task<List<FormlyType>> GetDineDeviceTypeSetting(IdInput input)
        {
            var myKey = DineDeviceTypes.DeviceTypes[input.Id];
            var output = DineDeviceTypes.AllDeviceTypes[myKey];
            return output;

        }
    }
}