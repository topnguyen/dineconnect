﻿using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Prints.DineDevices.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Prints
{
    public interface IDineDeviceListExcelExporter
    {
        FileDto ExportToFile(List<DineDeviceListDto> dtos);
    }
}