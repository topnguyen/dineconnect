﻿using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Prints.DineDevices.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Prints.Exporter
{
    public class DineDeviceListExcelExporter : FileExporterBase, IDineDeviceListExcelExporter
    {
        public FileDto ExportToFile(List<DineDeviceListDto> dtos)
        {
            return CreateExcelPackage(
                "DineDeviceList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("DineDevice"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("Type")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Name,
                        _ => _.DineDeviceTypeName
                        );

                    for (var i = 1; i <= 10; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
