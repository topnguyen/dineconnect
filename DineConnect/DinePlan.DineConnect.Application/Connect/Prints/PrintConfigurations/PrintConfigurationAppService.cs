﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Connect.Display;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations.Dtos;

namespace DinePlan.DineConnect.Connect.Prints.PrintConfigurations
{
    public class PrintConfigurationAppService : DineConnectAppServiceBase, IPrintConfigurationAppService
    {
        private readonly IRepository<PrintConfiguration> _printConfigurationRepository;
        public PrintConfigurationAppService(IRepository<PrintConfiguration> printConfigurationRepository)
        {
            _printConfigurationRepository = printConfigurationRepository;
        }

        public async Task<IdInput> CreateOrUpdatePrintConfiguration(CreateOrUpdatePrintConfigurationInput input)
        {
            IdInput returnId;
            if (input.PrintConfiguration.Id.HasValue)
            {
                returnId = await UpdatePrintConfiguration(input);
            }
            else
            {
                returnId = await CreatePrintConfiguration(input);
            }
            return returnId;
        }
        private async Task<IdInput> CreatePrintConfiguration(CreateOrUpdatePrintConfigurationInput input)
        {
            var printConfiguration = input.PrintConfiguration.MapTo<PrintConfiguration>();
            var retId = await _printConfigurationRepository.InsertAndGetIdAsync(printConfiguration);
            return new IdInput { Id = retId };
        }

        private async Task<IdInput> UpdatePrintConfiguration(CreateOrUpdatePrintConfigurationInput input)
        {
            var dto = input.PrintConfiguration;
            var item = await _printConfigurationRepository.GetAsync(input.PrintConfiguration.Id.Value);

            dto.MapTo(item);

            await _printConfigurationRepository.UpdateAsync(item);
            return new IdInput { Id = dto.Id.Value };
        }
    }
}
