﻿using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using DinePlan.DineConnect.Connect.Display;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Prints.Printers.Dtos;
using DinePlan.DineConnect.Connect.Prints.PrintJobs.Dtos;
using DinePlan.DineConnect.Connect.Prints.PrintTemplates.Dtos;
using DinePlan.DineConnect.Connect.PrintTemplateConditions.Dtos;
using DinePlan.DineConnect.Connect.PrintTemplates.Dtos;

namespace DinePlan.DineConnect.Connect.Prints.PrintConfigurations.Dtos
{
    
    [AutoMapFrom(typeof(PrintConfiguration))]
    public class PrintConfigurationListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
        public PrintConfigurationType PrintConfigurationType { get; set; }
    }

    [AutoMapTo(typeof(PrintConfiguration))]
    public class PrintConfigurationEditDto : FullAuditedEntityDto
    {
        public PrintConfigurationEditDto()
        {
            Printers = new Collection<PrinterEditDto>();
            PrintTemplates = new Collection<PrintTemplateEditDto>();
            PrintJobs = new Collection<PrintJobEditDto>();
            PrintTemplateConditions= new Collection<PrintTemplateConditionEditDto>();
        }
        public int? Id { get; set; }
        public string Name { get; set; }
        public PrintConfigurationType PrintConfigurationType { get; set; }
        public virtual Collection<PrinterEditDto> Printers { get; set; }
        public virtual Collection<PrintTemplateEditDto> PrintTemplates { get; set; }
        public virtual Collection<PrintJobEditDto> PrintJobs { get; set; }
        public virtual Collection<PrintTemplateConditionEditDto> PrintTemplateConditions { get; set; }
    }
    public class CreateOrUpdatePrintConfigurationInput : IInputDto
    {
        [Required]
        public PrintConfigurationEditDto PrintConfiguration { get; set; }

    }
    public class FilterInputDto
    {
        public int? Id { get; set; }

        public int? FutureDataId { get; set; }

        public bool FutureData { get; set; } = false;
        public LocationGroupDto LocationGroupToBeFiltered { get; set; }
    }
}
