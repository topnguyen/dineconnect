﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations.Dtos;

namespace DinePlan.DineConnect.Connect.Prints.PrintConfigurations
{
    public interface IPrintConfigurationAppService: IApplicationService
    {
        Task<IdInput> CreateOrUpdatePrintConfiguration(CreateOrUpdatePrintConfigurationInput input);
    }
}
