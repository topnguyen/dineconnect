﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations;
using DinePlan.DineConnect.Connect.Prints.Printers.Dtos;
using DinePlan.DineConnect.Connect.PrintTemplates.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations.Dtos;
using DinePlan.DineConnect.Connect.Prints.PrintTemplates.Dtos;

namespace DinePlan.DineConnect.Connect.PrintTemplates
{
    public interface IPrintTemplateAppService : IApplicationService
    {
        Task<PagedResultOutput<PrintConfigurationListDto>> GetPrintTemplates(GetPrintTemplatesInput input);

        Task<GetPrinterTemplateOutputDto> GetPrintTemplateForEdit(FilterInputDto input);
        Task DeletePrintConfigForPrintTemplate(NullableIdInput input);

        ListResultDto<ComboboxItemDto> GetPrintTemplateTypes();

        Task<FileDto> GetAllToExcel(GetPrintTemplatesInput input);

        Task ActivateItem(IdInput input);
        Task<ApiPrinterTemplateOutput> ApiPrinterTemplate(ApiLocationInput locationInput);
        Task<IdInput> AddOrEditPrinterTemplate(CreateOrUpdatePrinterTemplateInput input);
        Task DeletePrinterTemplate(IdInput input);
    }
}