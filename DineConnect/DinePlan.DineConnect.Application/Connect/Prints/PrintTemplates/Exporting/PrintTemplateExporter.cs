﻿using DinePlan.DineConnect.Connect.Prints.PrintConfigurations;
using DinePlan.DineConnect.Connect.PrintTemplates.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations.Dtos;

namespace DinePlan.DineConnect.Connect.PrintTemplates.Exporting
{
    public class PrintTemplateExporter : FileExporterBase, IPrintTemplateExporter
    {
        public FileDto ExportToFile(List<PrintConfigurationListDto> dtos)
        {
            return CreateExcelPackage(
                "PrintTemplateList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(base.L("PrintTemplate"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        base.L("Name"),
                        base.L("CreationTime")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Name,
                        _ => _.CreationTime
                        );

                    var creationTimeColumn = sheet.Column(2);
                    creationTimeColumn.Style.Numberformat.Format = "yyyy-mm-dd hh:mm";

                    for (var i = 1; i <= 5; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }

        private string MapFutureType(int type)
        {
            switch (type)
            {
                case 1:
                    return base.L("MenuItem");

                case 2:
                    return base.L("PriceTag");

                case 3:
                    return base.L("Tax");

                case 4:
                    return base.L("ScreenMenu");

                default:
                    return "";
            }
        }
    }
}