﻿using DinePlan.DineConnect.Connect.Prints.PrintConfigurations;
using DinePlan.DineConnect.Connect.PrintTemplates.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations.Dtos;

namespace DinePlan.DineConnect.Connect.PrintTemplates.Exporting
{
    public interface IPrintTemplateExporter
    {
        FileDto ExportToFile(List<PrintConfigurationListDto> dtos);
    }
}