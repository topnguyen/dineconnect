﻿using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;

namespace DinePlan.DineConnect.Connect.PrintTemplates.Dtos
{
    public class GetPrintTemplatesInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public virtual int PrintConfigurationId { get; set; }
        public string Filter { get; set; }

        public string Operation { get; set; }

        public DateTime? FutureDate { get; set; }

        public string Location { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime Desc";
            }
        }
    }
}