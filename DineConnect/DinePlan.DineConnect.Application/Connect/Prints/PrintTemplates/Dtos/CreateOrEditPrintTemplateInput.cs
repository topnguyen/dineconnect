﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations;
using System.ComponentModel.DataAnnotations;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations.Dtos;
using DinePlan.DineConnect.Connect.Prints.PrintTemplates.Dtos;

namespace DinePlan.DineConnect.Connect.PrintTemplates.Dtos
{
    public class CreateOrEditPrintTemplateInput
    {
        public CreateOrEditPrintTemplateInput()
        {
            PrintTemplate = new PrintTemplateEditDto();
            LocationGroup = new LocationGroupDto();
        }

        [Required]
        public PrintTemplateEditDto PrintTemplate { get; set; }

        public LocationGroupDto LocationGroup { get; set; }
    }
    public class CreateOrUpdatePrinterTemplateInput : IInputDto
    {
        public PrintTemplateEditDto PrintTemplate { get; set; }
    }
    public class GetPrinterTemplateOutputDto : IOutputDto
    {
        public PrintConfigurationEditDto PrintConfiguration { get; set; }
        public PrintTemplateEditDto PrintTemplate { get; set; }

    }
}