﻿using System;
using System.Collections.Generic;
using Abp.AutoMapper;
using DinePlan.DineConnect.Connect.Display;

namespace DinePlan.DineConnect.Connect.Prints.PrintTemplates.Dtos
{
    public class ApiPrinterTemplateOutput
    {
        public ApiPrinterTemplateOutput()
        {
            PrinterTemplates = new List<ApiPrinterTemplateDto>();
        }

        public IList<ApiPrinterTemplateDto> PrinterTemplates { get; set; }
    }

    [AutoMapTo(typeof(PrintTemplate))]
    public class ApiPrinterTemplateDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool MergeLines { get; set; }
        public string Contents { get; set; }
        public string Files { get; set; }
    }


    public class ApiPrinterConditionOutput
    {
        public ApiPrinterConditionOutput()
        {
            PrintTemplateConditions = new List<ApiPrinterTemplateConditionDto>();
        }

        public IList<ApiPrinterTemplateConditionDto> PrintTemplateConditions { get; set; }
    }

    [AutoMapTo(typeof(PrintTemplateCondition))]
    public class ApiPrinterTemplateConditionDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public string ConditionSchedules { get; set; }
        public string Filter { get; set; }
        public string Contents { get; set; }
    }
}