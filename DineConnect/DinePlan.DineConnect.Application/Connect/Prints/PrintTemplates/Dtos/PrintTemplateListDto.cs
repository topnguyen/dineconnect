﻿using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Display;

namespace DinePlan.DineConnect.Connect.PrintTemplates.Dtos
{
    [AutoMapFrom(typeof(Display.PrintTemplate))]
    public class PrintTemplateListDto : CreationAuditedEntity<int?>
    {
        public string Name { get; set; }
        public PrintTemplateType PrintTemplateType { get; set; }
        public string Contents { get; set; }
        public bool MergeLines { get; set; }
    }
}