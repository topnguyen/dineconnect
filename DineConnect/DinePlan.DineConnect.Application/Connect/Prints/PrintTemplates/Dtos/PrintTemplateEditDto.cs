﻿using Abp.AutoMapper;
using DinePlan.DineConnect.Connect.Location;

namespace DinePlan.DineConnect.Connect.Prints.PrintTemplates.Dtos
{
    [AutoMapTo(typeof(Display.PrintTemplate))]
    public class PrintTemplateEditDto : ConnectEditDto
    {
        public int? Id { get; set; }
        public virtual int PrintConfigurationId { get; set; }
        public string AliasName { get; set; }
        public bool MergeLines { get; set; }
        public string Contents { get; set; }
        public string Files { get; set; }

        public LocationGroupDto LocationGroup { get; set; }
        public PrintTemplateEditDto()
        {
            LocationGroup = new LocationGroupDto();
        }
    }
}