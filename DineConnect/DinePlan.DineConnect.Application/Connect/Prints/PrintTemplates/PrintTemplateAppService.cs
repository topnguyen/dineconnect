﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Connect.Display;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations.Dtos;
using DinePlan.DineConnect.Connect.Prints.PrintTemplates.Dtos;
using DinePlan.DineConnect.Connect.PrintTemplates;
using DinePlan.DineConnect.Connect.PrintTemplates.Dtos;
using DinePlan.DineConnect.Connect.PrintTemplates.Exporting;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Prints.PrintTemplates
{
    public class PrintTemplateAppService : DineConnectAppServiceBase, IPrintTemplateAppService
    {
        private readonly IRepository<Display.PrintTemplate> _printTemplateRepository;
        private readonly IPrintTemplateExporter _printTemplateExporter;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ILocationAppService _locAppService;
        private readonly IRepository<PrintConfiguration> _printConfigurationRepository;
        private readonly ISyncAppService _syncAppService;

        public PrintTemplateAppService(IRepository<Display.PrintTemplate> printTemplateRepository,
                                       IPrintTemplateExporter printTemplateExporter,
                                       IUnitOfWorkManager unitOfWorkManager,
                                       ILocationAppService locationAppService, ISyncAppService syncAppService,
                                       IRepository<PrintConfiguration> printConfigurationRepository)
        {
            _printTemplateRepository = printTemplateRepository;
            _printTemplateExporter = printTemplateExporter;
            _unitOfWorkManager = unitOfWorkManager;
            _locAppService = locationAppService;
            _printConfigurationRepository = printConfigurationRepository;
            _syncAppService = syncAppService;
        }

        public async Task<PagedResultOutput<PrintConfigurationListDto>> GetPrintTemplates(GetPrintTemplatesInput input)
        {
            var allItems = _printConfigurationRepository
                            .GetAll()
                            .Where(i => i.IsDeleted == input.IsDeleted)
                           .WhereIf(
                                !input.Filter.IsNullOrEmpty(),
                                p => p.Name.Contains(input.Filter));
            if (!input.Location.IsNullOrWhiteSpace())
            {
                var printtemplate = _printTemplateRepository.GetAll().Where(i => i.IsDeleted == input.IsDeleted).WhereIf(
                    !input.Location.IsNullOrWhiteSpace(),
                    p => p.Locations.Contains(input.Location));
                var printtemplateIds = printtemplate.Select(t => t.PrintConfigurationId).ToList();
                allItems = allItems.Where(t => printtemplateIds.Contains(t.Id));
            }
            allItems = allItems.Where(t => t.PrintConfigurationType == PrintConfigurationType.PrintTemplate);
            var sortMenuItems = await allItems.OrderBy(input.Sorting).PageBy(input).ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<PrintConfigurationListDto>>();
            var totalCount = await allItems.CountAsync();

            return new PagedResultOutput<PrintConfigurationListDto>(totalCount, allListDtos);
        }

        public async Task<FileDto> GetAllToExcel(GetPrintTemplatesInput input)
        {
            var allList = await GetPrintTemplates(input);
            var allListDtos = allList.Items.MapTo<List<PrintConfigurationListDto>>();
            return _printTemplateExporter.ExportToFile(allListDtos);
        }

        public async Task<GetPrinterTemplateOutputDto> GetPrintTemplateForEdit(FilterInputDto input)
        {
            PrintConfigurationEditDto editDto;

            if (input.Id.HasValue)
            {
                List<SimpleLocationDto> filterLocation = new List<SimpleLocationDto>();
                List<SimpleLocationGroupDto> filterLocationGroup = new List<SimpleLocationGroupDto>();
                List<int> filterLocationRefIds = new List<int>();
                List<int> filterLocationGroupRefIds = new List<int>();
                List<int> filterLocationTagRefIds = new List<int>();
                if (input.LocationGroupToBeFiltered != null)
                {
                    filterLocationRefIds = input.LocationGroupToBeFiltered.Locations.Select(t => t.Id).ToList();
                    filterLocationGroupRefIds = input.LocationGroupToBeFiltered.Groups.Select(t => t.Id).ToList();
                    filterLocationTagRefIds = input.LocationGroupToBeFiltered.LocationTags.Select(t => t.Id).ToList();
                }

                var printConfiguration = await _printConfigurationRepository.FirstOrDefaultAsync(e => e.Id == input.Id.Value);
                editDto = printConfiguration.MapTo<PrintConfigurationEditDto>();

                var printtemplate = await _printTemplateRepository.GetAllListAsync(t => t.PrintConfigurationId == input.Id.Value);
                var tempValuesFilter = printtemplate.MapTo<Collection<PrintTemplateEditDto>>();
                Collection<PrintTemplateEditDto> valuesOutput = new Collection<PrintTemplateEditDto>();
                foreach (var lst in tempValuesFilter)
                {
                    UpdateLocationAndNonLocationForEdit(lst, lst.LocationGroup);
                    if (filterLocationRefIds.Count > 0 || filterLocationGroupRefIds.Count > 0||filterLocationTagRefIds.Count > 0)
                    {
                        if (lst.LocationGroup.Locations.Select(t => t.Id).Intersect(filterLocationRefIds).ToList().Count > 0)
                            valuesOutput.Add(lst);
                        else if (lst.LocationGroup.Groups.Select(t => t.Id).Intersect(filterLocationGroupRefIds).ToList().Count > 0)
                            valuesOutput.Add(lst);
                        else if (lst.LocationGroup.LocationTags.Select(t => t.Id).Intersect(filterLocationTagRefIds).ToList().Count > 0)
                            valuesOutput.Add(lst);
                    }
                    else
                    {
                        valuesOutput.Add(lst);
                    }
                }
                editDto.PrintTemplates = valuesOutput;
            }
            else
            {
                editDto = new PrintConfigurationEditDto();
            }

            return new GetPrinterTemplateOutputDto
            {
                PrintConfiguration = editDto,
                PrintTemplate = new PrintTemplateEditDto()
            };
        }

       

        public async Task DeletePrintConfigForPrintTemplate(NullableIdInput input)
        {

            await _printTemplateRepository.DeleteAsync(t => t.PrintConfigurationId == input.Id);
            await _printConfigurationRepository.DeleteAsync(input.Id.Value);
        }

        

        public ListResultDto<ComboboxItemDto> GetPrintTemplateTypes()
        {
            var returnList = new List<ComboboxItemDto>();

            var i = 0;
            foreach (var name in Enum.GetNames(typeof(PrintTemplateType)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _printTemplateRepository.GetAsync(input.Id);
                item.IsDeleted = false;

                await _printTemplateRepository.UpdateAsync(item);
            }
        }

        public async Task<ApiPrinterTemplateOutput> ApiPrinterTemplate(ApiLocationInput locationInput)
        {
            var orgId = await _locAppService.GetOrgnizationIdForLocationId(locationInput.LocationId);
            if (orgId > 0)
            {
                CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);
            }

            var output = new ApiPrinterTemplateOutput();
            var printTemplates = await _printTemplateRepository.GetAllListAsync(a => a.TenantId == locationInput.TenantId);
            var myPTs = new List<Display.PrintTemplate>();
            if (locationInput.LocationId != 0)
            {
                foreach (var pt in printTemplates)
                {
                    if (await _locAppService.IsLocationExists(new CheckLocationInput
                    {
                        LocationId = locationInput.LocationId,
                        Locations = pt.Locations,
                        Group = pt.Group,
                        LocationTag = pt.LocationTag,
                        NonLocations = pt.NonLocations
                    }))
                    {
                        myPTs.Add(pt);
                    }
                }
            }
            else
            {
                myPTs = printTemplates.ToList();
            }

            foreach (var pt in myPTs)
            {
                var myPrinter = pt.MapTo<ApiPrinterTemplateDto>();
                var printConfigurations = await _printConfigurationRepository.GetAll().Where(a=>a.Id.Equals(pt.PrintConfigurationId)).ToListAsync();
                if (printConfigurations.Any())
                {
                    var pC = printConfigurations.LastOrDefault();
                    if (pC != null)
                    {
                        myPrinter.Id = pt.PrintConfigurationId;
                        myPrinter.Name = pC.Name;
                        output.PrinterTemplates.Add(myPrinter);
                    }
                }
            }

            return output;
        }
        public async Task<IdInput> AddOrEditPrinterTemplate(CreateOrUpdatePrinterTemplateInput input)
        {
            await BusinessRulesForPrinter(input);
            var printtemplate = input.PrintTemplate;
            await _syncAppService.UpdateSync(SyncConsts.PRINTERTEMPLATE);

            if (printtemplate.Id.HasValue)
            {
                var existEntry = await _printTemplateRepository.FirstOrDefaultAsync(t => t.Id == printtemplate.Id.Value);
                printtemplate.MapTo(existEntry);
                UpdateLocationAndNonLocation(existEntry, printtemplate.LocationGroup);
                await _printTemplateRepository.UpdateAsync(existEntry);
                return new IdInput { Id = existEntry.Id };
            }
            else
            {
                var dto = printtemplate.MapTo<PrintTemplate>();
                UpdateLocationAndNonLocation(dto, printtemplate.LocationGroup);
                await _printTemplateRepository.InsertOrUpdateAndGetIdAsync(dto);
                return new IdInput { Id = dto.Id };
            }
        }

        public async Task BusinessRulesForPrinter(CreateOrUpdatePrinterTemplateInput input)
        {
            List<SimpleLocationDto> newLocationToBeAdded = new List<SimpleLocationDto>();
            List<SimpleLocationGroupDto> newLocationGroupToBeAdded = new List<SimpleLocationGroupDto>();
            List<SimpleLocationTagDto> newLocationTagToBeAdded = new List<SimpleLocationTagDto>();
            var locationGroupDto = input.PrintTemplate.LocationGroup;
            if (locationGroupDto.Locations.Count > 0)
            {
                newLocationToBeAdded.AddRange(locationGroupDto.Locations);
            }
            if (locationGroupDto.Groups.Count > 0)
            {
                newLocationGroupToBeAdded.AddRange(locationGroupDto.Groups);
            }
            if (locationGroupDto.LocationTags.Count > 0)
            {
                newLocationTagToBeAdded.AddRange(locationGroupDto.LocationTags);
            }

            var iQPv = _printTemplateRepository.GetAll().Where(t => t.PrintConfigurationId == input.PrintTemplate.PrintConfigurationId);
            if (input.PrintTemplate.Id.HasValue)
                iQPv = iQPv.Where(t => t.Id != input.PrintTemplate.Id);
            var otherProgramValues = await iQPv.ToListAsync();
            var printtemplateDtos = otherProgramValues.MapTo<Collection<PrintTemplateEditDto>>();
            List<SimpleLocationDto> locationAlreadyMapped = new List<SimpleLocationDto>();
            List<SimpleLocationGroupDto> locationGroupAlreadyMapped = new List<SimpleLocationGroupDto>();
            List<SimpleLocationTagDto> locationTagAlreadyMapped = new List<SimpleLocationTagDto>();
            foreach (var lst in printtemplateDtos)
            {
                UpdateLocationAndNonLocationForEdit(lst, lst.LocationGroup);
                if (lst.LocationGroup.Locations.Count > 0)
                    locationAlreadyMapped.AddRange(lst.LocationGroup.Locations);
                if (lst.LocationGroup.Groups.Count > 0)
                    locationGroupAlreadyMapped.AddRange(lst.LocationGroup.Groups);
                if (lst.LocationGroup.LocationTags.Count > 0)
                    locationTagAlreadyMapped.AddRange(lst.LocationGroup.LocationTags);

            }

            if (locationAlreadyMapped.Count > 0)
            {
                var intersectLocations = locationAlreadyMapped.Select(t => t.Id).ToList().Intersect(newLocationToBeAdded.Select(t => t.Id).ToList()).ToList();
                if (intersectLocations.Count > 0)
                {
                    var locationAlreadyExists = locationAlreadyMapped.FindAll(t => intersectLocations.Contains(t.Id)).ToList();
                    string locationAlreadExists = string.Join(",", locationAlreadyExists.Select(t => t.Name));
                    throw new UserFriendlyException("This Location(s) :" + locationAlreadExists + " Already mapped with Another Values");
                }
            }
            if (locationGroupAlreadyMapped.Count > 0)
            {
                var intersectLocationGroups = locationGroupAlreadyMapped.Select(t => t.Id).ToList().Intersect(newLocationGroupToBeAdded.Select(t => t.Id).ToList()).ToList();
                if (intersectLocationGroups.Count > 0)
                {
                    var locationGroupAlreadyExists = locationGroupAlreadyMapped.FindAll(t => intersectLocationGroups.Contains(t.Id)).ToList();
                    string locationgroupAlreadyExists = string.Join(",", locationGroupAlreadyExists.Select(t => t.Name));
                    throw new UserFriendlyException("This Location Group(s) :" + locationgroupAlreadyExists + " Already mapped with Another Values");
                }
            }
            if (locationTagAlreadyMapped.Count > 0)
            {
                var intersectLocationTags = locationTagAlreadyMapped.Select(t => t.Id).ToList().Intersect(newLocationTagToBeAdded.Select(t => t.Id).ToList()).ToList();
                if (intersectLocationTags.Count > 0)
                {
                    var locationTagAlreadyExists = locationGroupAlreadyMapped.FindAll(t => intersectLocationTags.Contains(t.Id)).ToList();
                    string locationtagAlreadyExists = string.Join(",", locationTagAlreadyExists.Select(t => t.Name));
                    throw new UserFriendlyException("This Location Tag(s) :" + locationtagAlreadyExists + " Already mapped with Another Values");
                }
            }
        }

        public async Task DeletePrinterTemplate(IdInput input)
        {
            await _printTemplateRepository.DeleteAsync(t => t.Id == input.Id);
        }
    }
}