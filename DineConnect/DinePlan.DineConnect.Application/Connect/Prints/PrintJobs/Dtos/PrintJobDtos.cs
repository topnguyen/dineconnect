﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Display;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations.Dtos;

namespace DinePlan.DineConnect.Connect.Prints.PrintJobs.Dtos
{
    [AutoMapFrom(typeof(PrintJob))]
    public class PrintJobListDto : FullAuditedEntityDto
    {
        public virtual int PrintConfigurationId { get; set; }
        public string Name { get; set; }
        public int WhatToPrint { get; set; }
        public bool Negative { get; set; }
        public bool ExcludeTax { get; set; }
        public string JobGroup { get; set; }
        public string WhatToPrintType => ((WhatToPrintTypes)WhatToPrint).ToString();
    }

    [AutoMapTo(typeof(PrintJob))]
    public class PrintJobEditDto : ConnectEditDto
    {
        public PrintJobEditDto()
        {
            PrinterMaps = new Collection<PrinterMapEditDto>();
            LocationGroup = new LocationGroupDto();
        }
        public virtual int PrintConfigurationId { get; set; }
        public string AliasName { get; set; }
        public int? Id { get; set; }
        public string Name { get; set; }
        public int WhatToPrint { get; set; }
        public bool Negative { get; set; }
        public bool ExcludeTax { get; set; }
        public string JobGroup { get; set; }
        public string WhatToPrintType => ((WhatToPrintTypes)WhatToPrint).ToString();
        public Collection<PrinterMapEditDto> PrinterMaps { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
    }

    public class GetPrintJobInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public virtual int PrintConfigurationId { get; set; }
        public string Filter { get; set; }

        public string Operation { get; set; }

        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime Desc";
            }
        }

        public string Location { get; set; }
    }

    public class GetPrintJobForEditOutput : IOutputDto
    {
        public GetPrintJobForEditOutput()
        {
            LocationGroup = new LocationGroupDto();
            PrintJob = new PrintJobEditDto();
        }

        public PrintJobEditDto PrintJob { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
    }

    public class CreateOrUpdatePrintJobInput : IInputDto
    {
        public PrintJobEditDto PrintJob { get; set; }
    }

    [AutoMapTo(typeof(PrinterMap))]
    public class PrinterMapEditDto
    {
        public int? Id { get; set; }
        public string MenuItemGroupCode { get; set; }
        public int MenuItemId { get; set; }
        public string MenuItemName { get; set; }
        public int PrinterId { get; set; }
        public string PrinterName { get; set; }
        public int PrintTemplateId { get; set; }
        public string PrintTemplateName { get; set; }
        public PrintTemplateType PrintTemplatePrintTemplateType { get; set; }
        public int? PrintJobId { get; set; }
        public string PrintJobName { get; set; }
        public int CategoryId { get; set; }
        public virtual string CategoryName { get; set; }
        public virtual int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
    }

    public class ApiPrintJobOutput
    {
        public ApiPrintJobOutput()
        {
            PrintJobs = new List<ApiPrintJob>();
        }
        public IList<ApiPrintJob> PrintJobs { get; set; }
    }

    [AutoMapTo(typeof(PrintJob))]
    public class ApiPrintJob
    {
        public ApiPrintJob()
        {
            PrinterMaps = new List<ApiPrinterMap>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public int WhatToPrint { get; set; }
        public bool Negative { get; set; }
        public bool ExcludeTax { get; set; }
        public string JobGroup { get; set; }
        public IList<ApiPrinterMap> PrinterMaps { get; set; }
    }

    [AutoMapTo(typeof(PrinterMap))]
    public class ApiPrinterMap
    {
        public int Id { get; set; }
        public string MenuItemGroupCode { get; set; }
        public int PrintJobId { get; set; }
        public int MenuItemId { get; set; }
        public int PrinterId { get; set; }
        public int PrintTemplateId { get; set; }
        public string CategoryName { get; set; }
        public int DepartmentId { get; set; }

    }
    public class GetPrintJobOutputDto : IOutputDto
    {
        public PrintConfigurationEditDto PrintConfiguration { get; set; }
        public PrintJobEditDto PrintJob { get; set; }

    }
}