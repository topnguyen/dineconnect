﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using AutoMapper;
using DinePlan.DineConnect.Connect.Display;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations;
using DinePlan.DineConnect.Connect.Prints.PrintJobs.Dtos;
using DinePlan.DineConnect.Connect.Prints.PrintJobs.Exporting;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations.Dtos;
using DinePlan.DineConnect.Connect.Sync;

namespace DinePlan.DineConnect.Connect.Prints.PrintJobs
{
    public class PrintJobAppService : DineConnectAppServiceBase, IPrintJobAppService
    {
        private readonly IRepository<PrintJob> _printJobRepository;
        private readonly IRepository<MenuItem> _menuItemRepository;
        private readonly IRepository<PrinterMap> _printerMapRepository;
        private readonly ILocationAppService _locAppService;
        private readonly IPrintJobExporter _printJobExporter;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<PrintConfiguration> _printConfigurationRepository;
        private readonly ISyncAppService _syncAppService;
        private readonly IRepository<Master.Department> _departmentRepo;

        public PrintJobAppService(IRepository<PrintJob> printJobRepository,
                                  IRepository<PrinterMap> printerMapRepository,
                                  IRepository<MenuItem> menuItemRepository,
                                  IPrintJobExporter printJobExporter,ISyncAppService syncAppService,
                                  IUnitOfWorkManager unitOfWorkManager,
                                  ILocationAppService locationAppService,
                                  IRepository<PrintConfiguration> printConfigurationRepository,
                                  IRepository<Master.Department> departmentRepo)
        {
            _printJobRepository = printJobRepository;
            _printerMapRepository = printerMapRepository;
            _menuItemRepository = menuItemRepository;
            _printJobExporter = printJobExporter;
            _unitOfWorkManager = unitOfWorkManager;
            _locAppService = locationAppService;
            _printConfigurationRepository = printConfigurationRepository;
            _syncAppService = syncAppService;
            _departmentRepo = departmentRepo;
        }

        public async Task<PagedResultOutput<PrintConfigurationListDto>> GetPrintJobs(GetPrintJobInput input)
        {
            var allItems = _printConfigurationRepository
                            .GetAll()
                            .Where(i => i.IsDeleted == input.IsDeleted)
                            .WhereIf(
                                !input.Filter.IsNullOrEmpty(),
                                p => p.Name.Contains(input.Filter));
            if (!input.Location.IsNullOrWhiteSpace())
            {
                var printer = _printJobRepository.GetAll().Where(i => i.IsDeleted == input.IsDeleted).WhereIf(
                    !input.Location.IsNullOrWhiteSpace(),
                    p => p.Locations.Contains(input.Location));
                var printerIds = printer.Select(t => t.PrintConfigurationId).ToList();
                allItems = allItems.Where(t => printerIds.Contains(t.Id));
            }
            allItems = allItems.Where(t => t.PrintConfigurationType == PrintConfigurationType.PrintJob);
            var sortMenuItems = await allItems.OrderBy(input.Sorting).PageBy(input).ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<PrintConfigurationListDto>>();
            var totalCount = await allItems.CountAsync();

            return new PagedResultOutput<PrintConfigurationListDto>(totalCount, allListDtos);
        }

        public async Task<FileDto> GetAllToExcel(GetPrintJobInput input)
        {
            var allList = await GetPrintJobs(input);
            var allListDtos = allList.Items.MapTo<List<PrintConfigurationListDto>>();
            return _printJobExporter.ExportToFile(allListDtos);
        }

        public async Task<GetPrintJobOutputDto> GetPrintJobForEdit(FilterInputDto input)
        {
            PrintConfigurationEditDto editDto;

            if (input.Id.HasValue)
            {
                List<SimpleLocationDto> filterLocation = new List<SimpleLocationDto>();
                List<SimpleLocationGroupDto> filterLocationGroup = new List<SimpleLocationGroupDto>();
                List<int> filterLocationRefIds = new List<int>();
                List<int> filterLocationGroupRefIds = new List<int>();
                List<int> filterLocationTagRefIds = new List<int>();
                if (input.LocationGroupToBeFiltered != null)
                {
                    filterLocationRefIds = input.LocationGroupToBeFiltered.Locations.Select(t => t.Id).ToList();
                    filterLocationGroupRefIds = input.LocationGroupToBeFiltered.Groups.Select(t => t.Id).ToList();
                    filterLocationTagRefIds = input.LocationGroupToBeFiltered.LocationTags.Select(t => t.Id).ToList();
                }

                var printConfiguration = await _printConfigurationRepository.FirstOrDefaultAsync(e => e.Id == input.Id.Value);
                editDto = printConfiguration.MapTo<PrintConfigurationEditDto>();

                var printJob = await _printJobRepository.GetAllListAsync(t => t.PrintConfigurationId == input.Id.Value);
                var tempValuesFilter = printJob.MapTo<Collection<PrintJobEditDto>>();
                var rsdepartment=await _departmentRepo.GetAllListAsync();
                Collection<PrintJobEditDto> valuesOutput = new Collection<PrintJobEditDto>();
                foreach (var lst in tempValuesFilter)
                {
                    UpdateLocationAndNonLocationForEdit(lst, lst.LocationGroup);
                    if (filterLocationRefIds.Count > 0 || filterLocationGroupRefIds.Count > 0 || filterLocationTagRefIds.Count > 0)
                    {
                        if (lst.LocationGroup.Locations.Select(t => t.Id).Intersect(filterLocationRefIds).ToList().Count > 0)
                            valuesOutput.Add(lst);
                        else if (lst.LocationGroup.Groups.Select(t => t.Id).Intersect(filterLocationGroupRefIds).ToList().Count > 0)
                            valuesOutput.Add(lst);
                        else if (lst.LocationGroup.LocationTags.Select(t => t.Id).Intersect(filterLocationTagRefIds).ToList().Count > 0)
                            valuesOutput.Add(lst);
                    }
                    else
                    {
                        valuesOutput.Add(lst);
                    }
                    foreach(var sublst in lst.PrinterMaps)
                    {
                        if(sublst.DepartmentId>0)
                        {
                            var department=rsdepartment.FirstOrDefault(t=>t.Id==sublst.DepartmentId);
                            if(department!=null)
                            {
                                sublst.DepartmentName = department.Name;
                            }
                        }
                    }
                }
                editDto.PrintJobs = valuesOutput;
            }
            else
            {
                editDto = new PrintConfigurationEditDto();
            }

            return new GetPrintJobOutputDto
            {
                PrintConfiguration = editDto,
                PrintJob = new PrintJobEditDto()
            };
        }



        public async Task DeletePrintConfigForPrintJob(NullableIdInput input)
        {
            await _printJobRepository.DeleteAsync(t => t.PrintConfigurationId == input.Id);
            await _printConfigurationRepository.DeleteAsync(input.Id.Value);
        }

        public ListResultDto<ComboboxItemDto> GetWhatToPrints()
        {
            var returnList = new List<ComboboxItemDto>();

            var i = 0;
            foreach (var name in Enum.GetNames(typeof(WhatToPrintTypes)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _printJobRepository.GetAsync(input.Id);
                item.IsDeleted = false;

                await _printJobRepository.UpdateAsync(item);
            }
        }

        public async Task<ApiPrintJobOutput> ApiPrintJob(ApiLocationInput locationInput)
        {
            var orgId = await _locAppService.GetOrgnizationIdForLocationId(locationInput.LocationId);
            if (orgId > 0)
            {
                CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);
            }

            var output = new ApiPrintJobOutput();
           
            var printJobs = await _printJobRepository.GetAllListAsync(a => a.TenantId == locationInput.TenantId);

            var myPJs = new List<PrintJob>();
            if (locationInput.LocationId != 0)
            {
                foreach (var pj in printJobs)
                {
                    if (await _locAppService.IsLocationExists(new CheckLocationInput
                    {
                        LocationId = locationInput.LocationId,
                        Locations = pj.Locations,
                        Group = pj.Group,
                        LocationTag = pj.LocationTag,
                        NonLocations = pj.NonLocations
                    }))
                    {
                        myPJs.Add(pj);
                    }
                }
            }
            else
            {
                myPJs = printJobs.ToList();
            }

            foreach (var pt in myPJs)
            {
                var myPrinter = pt.MapTo<ApiPrintJob>();
                var printConfigurations = await _printConfigurationRepository.GetAll().Where(a=>a.Id.Equals(pt.PrintConfigurationId)).ToListAsync();
                if (printConfigurations.Any())
                {
                    var pC = printConfigurations.LastOrDefault();
                    if (pC != null)
                    {
                        myPrinter.Id = pC.Id;
                        myPrinter.Name = pC.Name;
                        output.PrintJobs.Add(myPrinter);
                    }
                }
            }

            return output;
        }
        public async Task<IdInput> AddOrEditPrintJob(CreateOrUpdatePrintJobInput input)
        {
            await BusinessRulesForPrinter(input);
            var printJob = input.PrintJob;

            await _syncAppService.UpdateSync(SyncConsts.PRINTERJOB);

            if (printJob.Id.HasValue)
            {
                var existEntry = await _printJobRepository.FirstOrDefaultAsync(t => t.Id == printJob.Id.Value);
                existEntry.AliasName = printJob.AliasName;
                existEntry.WhatToPrint = printJob.WhatToPrint;
                existEntry.JobGroup = printJob.JobGroup;
                existEntry.ExcludeTax = printJob.ExcludeTax;
                existEntry.Negative = printJob.Negative;
                UpdateLocationAndNonLocation(existEntry, printJob.LocationGroup);
               
                if (printJob.PrinterMaps != null && printJob.PrinterMaps.Any())
                {
                    var oids = new List<int>();
                    foreach (var otdto in printJob.PrinterMaps)
                    {
                        if (otdto.Id != null && otdto.Id > 0)
                        {
                            oids.Add(otdto.Id.Value);
                            var fromUpdation = existEntry.PrinterMaps.SingleOrDefault(a => a.Id.Equals(otdto.Id));
                            UpdatePrinterMapsPortion(fromUpdation, otdto);
                        }
                        else
                        {
                            existEntry.PrinterMaps.Add(Mapper.Map<PrinterMapEditDto, PrinterMap>(otdto));
                        }
                    }
                    var tobeRemoved = existEntry.PrinterMaps.Where(a => !a.Id.Equals(0) && !oids.Contains(a.Id)).ToList();
                    foreach (var printerMaps in tobeRemoved)
                        try
                        {
                            await _printerMapRepository.DeleteAsync(printerMaps.Id);
                        }
                        catch (Exception exception)
                        {
                            throw new UserFriendlyException(exception.Message);
                        }
                }
                else
                {
                    foreach (var printerMaps in existEntry.PrinterMaps) await _printerMapRepository.DeleteAsync(printerMaps.Id);
                }

                
                return new IdInput { Id = existEntry.Id };
            }
            else
            {
                var dto = printJob.MapTo<PrintJob>();
                UpdateLocationAndNonLocation(dto, printJob.LocationGroup);
                await _printJobRepository.InsertOrUpdateAndGetIdAsync(dto);
                return new IdInput { Id = dto.Id };
            }
        }

        private void UpdatePrinterMapsPortion(PrinterMap fromUpdation, PrinterMapEditDto otdto)
        {
            fromUpdation.PrinterId = otdto.PrinterId;
            fromUpdation.PrintTemplateId = otdto.PrintTemplateId;
            fromUpdation.CategoryId = otdto.CategoryId;
            fromUpdation.MenuItemId = otdto.MenuItemId;
            fromUpdation.MenuItemGroupCode = otdto.MenuItemGroupCode;
            fromUpdation.CategoryName = otdto.CategoryName;
            fromUpdation.DepartmentId = otdto.DepartmentId;
        }

        public async Task BusinessRulesForPrinter(CreateOrUpdatePrintJobInput input)
        {
            List<SimpleLocationDto> newLocationToBeAdded = new List<SimpleLocationDto>();
            List<SimpleLocationGroupDto> newLocationGroupToBeAdded = new List<SimpleLocationGroupDto>();
            List<SimpleLocationTagDto> newLocationTagToBeAdded = new List<SimpleLocationTagDto>();
            var locationGroupDto = input.PrintJob.LocationGroup;
            if (locationGroupDto.Locations.Count > 0)
            {
                newLocationToBeAdded.AddRange(locationGroupDto.Locations);
            }
            if (locationGroupDto.Groups.Count > 0)
            {
                newLocationGroupToBeAdded.AddRange(locationGroupDto.Groups);
            }
            if (locationGroupDto.LocationTags.Count > 0)
            {
                newLocationTagToBeAdded.AddRange(locationGroupDto.LocationTags);
            }

            var iQPv = _printJobRepository.GetAll().Where(t => t.PrintConfigurationId == input.PrintJob.PrintConfigurationId);
            if (input.PrintJob.Id.HasValue)
                iQPv = iQPv.Where(t => t.Id != input.PrintJob.Id);
            var otherProgramValues = await iQPv.ToListAsync();
            var printerDtos = otherProgramValues.MapTo<Collection<PrintJobEditDto>>();
            List<SimpleLocationDto> locationAlreadyMapped = new List<SimpleLocationDto>();
            List<SimpleLocationGroupDto> locationGroupAlreadyMapped = new List<SimpleLocationGroupDto>();
            List<SimpleLocationTagDto> locationTagAlreadyMapped = new List<SimpleLocationTagDto>();
            foreach (var lst in printerDtos)
            {
                UpdateLocationAndNonLocationForEdit(lst, lst.LocationGroup);
                if (lst.LocationGroup.Locations.Count > 0)
                    locationAlreadyMapped.AddRange(lst.LocationGroup.Locations);
                if (lst.LocationGroup.Groups.Count > 0)
                    locationGroupAlreadyMapped.AddRange(lst.LocationGroup.Groups);
                if (lst.LocationGroup.LocationTags.Count > 0)
                    locationTagAlreadyMapped.AddRange(lst.LocationGroup.LocationTags);

            }

            if (locationAlreadyMapped.Count > 0)
            {
                var intersectLocations = locationAlreadyMapped.Select(t => t.Id).ToList().Intersect(newLocationToBeAdded.Select(t => t.Id).ToList()).ToList();
                if (intersectLocations.Count > 0)
                {
                    var locationAlreadyExists = locationAlreadyMapped.FindAll(t => intersectLocations.Contains(t.Id)).ToList();
                    string locationAlreadExists = string.Join(",", locationAlreadyExists.Select(t => t.Name));
                    throw new UserFriendlyException("This Location(s) :" + locationAlreadExists + " Already mapped with Another Values");
                }
            }
            if (locationGroupAlreadyMapped.Count > 0)
            {
                var intersectLocationGroups = locationGroupAlreadyMapped.Select(t => t.Id).ToList().Intersect(newLocationGroupToBeAdded.Select(t => t.Id).ToList()).ToList();
                if (intersectLocationGroups.Count > 0)
                {
                    var locationGroupAlreadyExists = locationGroupAlreadyMapped.FindAll(t => intersectLocationGroups.Contains(t.Id)).ToList();
                    string locationgroupAlreadyExists = string.Join(",", locationGroupAlreadyExists.Select(t => t.Name));
                    throw new UserFriendlyException("This Location Group(s) :" + locationgroupAlreadyExists + " Already mapped with Another Values");
                }
            }
            if (locationTagAlreadyMapped.Count > 0)
            {
                var intersectLocationTags = locationTagAlreadyMapped.Select(t => t.Id).ToList().Intersect(newLocationTagToBeAdded.Select(t => t.Id).ToList()).ToList();
                if (intersectLocationTags.Count > 0)
                {
                    var locationTagAlreadyExists = locationGroupAlreadyMapped.FindAll(t => intersectLocationTags.Contains(t.Id)).ToList();
                    string locationtagAlreadyExists = string.Join(",", locationTagAlreadyExists.Select(t => t.Name));
                    throw new UserFriendlyException("This Location Tag(s) :" + locationtagAlreadyExists + " Already mapped with Another Values");
                }
            }
        }

        public async Task DeletePrintJob(IdInput input)
        {
            await _printJobRepository.DeleteAsync(t => t.Id == input.Id);
        }
    }
}