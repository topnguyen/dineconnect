﻿using DinePlan.DineConnect.Connect.Prints.PrintConfigurations;
using DinePlan.DineConnect.Connect.Prints.PrintJobs.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations.Dtos;

namespace DinePlan.DineConnect.Connect.Prints.PrintJobs.Exporting
{
    public class PrintJobExporter : FileExporterBase, IPrintJobExporter
    {
        public FileDto ExportToFile(List<PrintConfigurationListDto> dtos)
        {
            return CreateExcelPackage(
                "PrintJobList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(base.L("PrintJob"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        base.L("Name"),
                        base.L("CreationTime")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                       _ => _.Name,
                        _ => _.CreationTime
                        );

                    var creationTimeColumn = sheet.Column(2);
                    creationTimeColumn.Style.Numberformat.Format = "yyyy-mm-dd hh:mm";

                    for (var i = 1; i <= 5; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}