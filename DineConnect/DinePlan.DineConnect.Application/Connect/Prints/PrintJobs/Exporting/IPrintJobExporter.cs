﻿using DinePlan.DineConnect.Connect.Prints.PrintConfigurations;
using DinePlan.DineConnect.Connect.Prints.PrintJobs.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations.Dtos;

namespace DinePlan.DineConnect.Connect.Prints.PrintJobs.Exporting
{
    public interface IPrintJobExporter
    {
        FileDto ExportToFile(List<PrintConfigurationListDto> dtos);
    }
}