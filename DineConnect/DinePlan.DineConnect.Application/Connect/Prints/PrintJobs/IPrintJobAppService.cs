﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations;
using DinePlan.DineConnect.Connect.Prints.PrintJobs.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations.Dtos;

namespace DinePlan.DineConnect.Connect.Prints.PrintJobs
{
    public interface IPrintJobAppService : IApplicationService
    {
        Task ActivateItem(IdInput input);


        Task DeletePrintConfigForPrintJob(NullableIdInput input);

        Task<FileDto> GetAllToExcel(GetPrintJobInput input);

        Task<GetPrintJobOutputDto> GetPrintJobForEdit(FilterInputDto input);

        Task<PagedResultOutput<PrintConfigurationListDto>> GetPrintJobs(GetPrintJobInput input);

        ListResultDto<ComboboxItemDto> GetWhatToPrints();
        Task<ApiPrintJobOutput> ApiPrintJob(ApiLocationInput locationInput);
        Task<IdInput> AddOrEditPrintJob(CreateOrUpdatePrintJobInput input);
        Task DeletePrintJob(IdInput input);
    }
}