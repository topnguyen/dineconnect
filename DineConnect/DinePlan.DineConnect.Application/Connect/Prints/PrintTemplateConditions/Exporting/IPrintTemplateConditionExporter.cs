﻿using DinePlan.DineConnect.Connect.Prints.PrintConfigurations;
using DinePlan.DineConnect.Connect.PrintTemplateConditions.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations.Dtos;

namespace DinePlan.DineConnect.Connect.PrintTemplateConditions.Exporting
{
    public interface IPrintTemplateConditionExporter
    {
        FileDto ExportToFile(List<PrintConfigurationListDto> dtos);
    }
}