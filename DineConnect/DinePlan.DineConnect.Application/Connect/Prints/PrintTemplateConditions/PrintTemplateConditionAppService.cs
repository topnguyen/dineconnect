﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Connect.Display;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations.Dtos;
using DinePlan.DineConnect.Connect.Prints.PrintJobs.Dtos;
using DinePlan.DineConnect.Connect.Prints.PrintTemplates.Dtos;
using DinePlan.DineConnect.Connect.PrintTemplateConditions;
using DinePlan.DineConnect.Connect.PrintTemplateConditions.Dtos;
using DinePlan.DineConnect.Connect.PrintTemplateConditions.Exporting;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Prints.PrintTemplateConditions
{
    public class PrintTemplateConditionAppService : DineConnectAppServiceBase, IPrintTemplateConditionAppService
    {
        private readonly IRepository<Display.PrintTemplateCondition> _printTemplateRepository;
        private readonly IPrintTemplateConditionExporter _printTemplateExporter;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<PrintConfiguration> _printConfigurationRepository;
        private readonly ILocationAppService _locationAppService;
        private readonly ISyncAppService _syncAppService;

        public PrintTemplateConditionAppService(
            IRepository<Display.PrintTemplateCondition> printTemplateRepository
            , IPrintTemplateConditionExporter printTemplateExporter
            , IUnitOfWorkManager unitOfWorkManager,ISyncAppService syncAppService
            , IRepository<PrintConfiguration> printConfigurationRepository, ILocationAppService locationAppService)
        {
            _printTemplateRepository = printTemplateRepository;
            _printTemplateExporter = printTemplateExporter;
            _unitOfWorkManager = unitOfWorkManager;
            _printConfigurationRepository = printConfigurationRepository;
            _locationAppService = locationAppService;
            _syncAppService = syncAppService;
        }

        public async Task<PagedResultOutput<PrintConfigurationListDto>> GetPrintTemplateConditions(GetPrintTemplateConditionsInput input)
        {
            var allItems = _printConfigurationRepository
                            .GetAll()
                            .Where(i => i.IsDeleted == input.IsDeleted)
                            .WhereIf(
                                !input.Filter.IsNullOrEmpty(),
                                p => p.Name.Contains(input.Filter));
            if (!input.Location.IsNullOrWhiteSpace())
            {
                var printtemplateCondition = _printTemplateRepository.GetAll().Where(i => i.IsDeleted == input.IsDeleted).WhereIf(
                    !input.Location.IsNullOrWhiteSpace(),
                    p => p.Locations.Contains(input.Location));
                var printtemplateConditionIds = printtemplateCondition.Select(t => t.PrintConfigurationId).ToList();
                allItems = allItems.Where(t => printtemplateConditionIds.Contains(t.Id));
            }
            allItems = allItems.Where(t => t.PrintConfigurationType == PrintConfigurationType.PrintCondition);
            var sortMenuItems = await allItems.OrderBy(input.Sorting).PageBy(input).ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<PrintConfigurationListDto>>();
            var totalCount = await allItems.CountAsync();

            return new PagedResultOutput<PrintConfigurationListDto>(totalCount, allListDtos);
        }

        public async Task<FileDto> GetAllToExcel(GetPrintTemplateConditionsInput input)
        {
            var allList = await GetPrintTemplateConditions(input);
            var allListDtos = allList.Items.MapTo<List<PrintConfigurationListDto>>();
            return _printTemplateExporter.ExportToFile(allListDtos);
        }

        public async Task<GetPrintTemplateConditionOutputDto> GetPrintTemplateConditionForEdit(FilterInputDto input)
        {
            PrintConfigurationEditDto editDto;

            if (input.Id.HasValue)
            {
                List<SimpleLocationDto> filterLocation = new List<SimpleLocationDto>();
                List<SimpleLocationGroupDto> filterLocationGroup = new List<SimpleLocationGroupDto>();
                List<int> filterLocationRefIds = new List<int>();
                List<int> filterLocationGroupRefIds = new List<int>();
                List<int> filterLocationTagRefIds = new List<int>();
                if (input.LocationGroupToBeFiltered != null)
                {
                    filterLocationRefIds = input.LocationGroupToBeFiltered.Locations.Select(t => t.Id).ToList();
                    filterLocationGroupRefIds = input.LocationGroupToBeFiltered.Groups.Select(t => t.Id).ToList();
                    filterLocationTagRefIds = input.LocationGroupToBeFiltered.LocationTags.Select(t => t.Id).ToList();
                }

                var printConfiguration = await _printConfigurationRepository.FirstOrDefaultAsync(e => e.Id == input.Id.Value);
                editDto = printConfiguration.MapTo<PrintConfigurationEditDto>();

                var printtemplatecondition = await _printTemplateRepository.GetAllListAsync(t => t.PrintConfigurationId == input.Id.Value);
                var tempValuesFilter = printtemplatecondition.MapTo<Collection<PrintTemplateConditionEditDto>>();

                Collection<PrintTemplateConditionEditDto> valuesOutput = new Collection<PrintTemplateConditionEditDto>();
                foreach (var lst in tempValuesFilter)
                {
                    UpdateLocationAndNonLocationForEdit(lst, lst.LocationGroup);
                    if (filterLocationRefIds.Count > 0 || filterLocationGroupRefIds.Count > 0 || filterLocationTagRefIds.Count > 0)
                    {
                        if (lst.LocationGroup.Locations.Select(t => t.Id).Intersect(filterLocationRefIds).ToList().Count > 0)
                            valuesOutput.Add(lst);
                        else if (lst.LocationGroup.Groups.Select(t => t.Id).Intersect(filterLocationGroupRefIds).ToList().Count > 0)
                            valuesOutput.Add(lst);
                        else if (lst.LocationGroup.LocationTags.Select(t => t.Id).Intersect(filterLocationTagRefIds).ToList().Count > 0)
                            valuesOutput.Add(lst);
                    }
                    else
                    {
                        valuesOutput.Add(lst);
                    }
                }
                editDto.PrintTemplateConditions = valuesOutput;
            }
            else
            {
                editDto = new PrintConfigurationEditDto();
            }

            return new GetPrintTemplateConditionOutputDto
            {
                PrintConfiguration = editDto,
                PrintTemplateCondition = new PrintTemplateConditionEditDto()
            };
        }


        public async Task DeletePrintConfigForPrintTemplateCondition(NullableIdInput input)
        {
            await _printTemplateRepository.DeleteAsync(t => t.PrintConfigurationId == input.Id);
            await _printConfigurationRepository.DeleteAsync(input.Id.Value);
        }

        public ListResultDto<ComboboxItemDto> GetPrintTemplateTypes()
        {
            var returnList = new List<ComboboxItemDto>();

            var i = 0;
            foreach (var name in Enum.GetNames(typeof(PrintTemplateType)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _printTemplateRepository.GetAsync(input.Id);
                item.IsDeleted = false;

                await _printTemplateRepository.UpdateAsync(item);
            }
        }
        public async Task<IdInput> AddOrEditPrintTemplateCondition(CreateOrEditPrintTemplateConditionInput input)
        {
            await BusinessRulesForPrintTemplateCondition(input);
            var printtemplatecondition = input.ConditionEditDto;
            await _syncAppService.UpdateSync(SyncConsts.PRINTERTEMPLATECONDITION);

            if (printtemplatecondition.Id.HasValue)
            {
                var existEntry = await _printTemplateRepository.FirstOrDefaultAsync(t => t.Id == printtemplatecondition.Id.Value);
                printtemplatecondition.MapTo(existEntry);
                UpdateLocationAndNonLocation(existEntry, printtemplatecondition.LocationGroup);
                await _printTemplateRepository.UpdateAsync(existEntry);
                return new IdInput { Id = existEntry.Id };
            }
            else
            {
                var dto = printtemplatecondition.MapTo<PrintTemplateCondition>();
                UpdateLocationAndNonLocation(dto, printtemplatecondition.LocationGroup);
                await _printTemplateRepository.InsertOrUpdateAndGetIdAsync(dto);
                return new IdInput { Id = dto.Id };
            }
        }

        public async Task BusinessRulesForPrintTemplateCondition(CreateOrEditPrintTemplateConditionInput input)
        {
            List<SimpleLocationDto> newLocationToBeAdded = new List<SimpleLocationDto>();
            List<SimpleLocationGroupDto> newLocationGroupToBeAdded = new List<SimpleLocationGroupDto>();
            List<SimpleLocationTagDto> newLocationTagToBeAdded = new List<SimpleLocationTagDto>();
            var locationGroupDto = input.ConditionEditDto.LocationGroup;
            if (locationGroupDto.Locations.Count > 0)
            {
                newLocationToBeAdded.AddRange(locationGroupDto.Locations);
            }
            if (locationGroupDto.Groups.Count > 0)
            {
                newLocationGroupToBeAdded.AddRange(locationGroupDto.Groups);
            }
            if (locationGroupDto.LocationTags.Count > 0)
            {
                newLocationTagToBeAdded.AddRange(locationGroupDto.LocationTags);
            }

            var iQPv = _printTemplateRepository.GetAll().Where(t => t.PrintConfigurationId == input.ConditionEditDto.PrintConfigurationId);
            if (input.ConditionEditDto.Id.HasValue)
                iQPv = iQPv.Where(t => t.Id != input.ConditionEditDto.Id);
            var otherProgramValues = await iQPv.ToListAsync();
            var printerDtos = otherProgramValues.MapTo<Collection<PrintTemplateConditionEditDto>>();
            List<SimpleLocationDto> locationAlreadyMapped = new List<SimpleLocationDto>();
            List<SimpleLocationGroupDto> locationGroupAlreadyMapped = new List<SimpleLocationGroupDto>();
            List<SimpleLocationTagDto> locationTagAlreadyMapped = new List<SimpleLocationTagDto>();
            foreach (var lst in printerDtos)
            {
                UpdateLocationAndNonLocationForEdit(lst, lst.LocationGroup);
                if (lst.LocationGroup.Locations.Count > 0)
                    locationAlreadyMapped.AddRange(lst.LocationGroup.Locations);
                if (lst.LocationGroup.Groups.Count > 0)
                    locationGroupAlreadyMapped.AddRange(lst.LocationGroup.Groups);
                if (lst.LocationGroup.LocationTags.Count > 0)
                    locationTagAlreadyMapped.AddRange(lst.LocationGroup.LocationTags);

            }

            if (locationAlreadyMapped.Count > 0)
            {
                var intersectLocations = locationAlreadyMapped.Select(t => t.Id).ToList().Intersect(newLocationToBeAdded.Select(t => t.Id).ToList()).ToList();
                if (intersectLocations.Count > 0)
                {
                    var locationAlreadyExists = locationAlreadyMapped.FindAll(t => intersectLocations.Contains(t.Id)).ToList();
                    string locationAlreadExists = string.Join(",", locationAlreadyExists.Select(t => t.Name));
                    throw new UserFriendlyException("This Location(s) :" + locationAlreadExists + " Already mapped with Another Values");
                }
            }
            if (locationGroupAlreadyMapped.Count > 0)
            {
                var intersectLocationGroups = locationGroupAlreadyMapped.Select(t => t.Id).ToList().Intersect(newLocationGroupToBeAdded.Select(t => t.Id).ToList()).ToList();
                if (intersectLocationGroups.Count > 0)
                {
                    var locationGroupAlreadyExists = locationGroupAlreadyMapped.FindAll(t => intersectLocationGroups.Contains(t.Id)).ToList();
                    string locationgroupAlreadyExists = string.Join(",", locationGroupAlreadyExists.Select(t => t.Name));
                    throw new UserFriendlyException("This Location Group(s) :" + locationgroupAlreadyExists + " Already mapped with Another Values");
                }
            }
            if (locationTagAlreadyMapped.Count > 0)
            {
                var intersectLocationTags = locationTagAlreadyMapped.Select(t => t.Id).ToList().Intersect(newLocationTagToBeAdded.Select(t => t.Id).ToList()).ToList();
                if (intersectLocationTags.Count > 0)
                {
                    var locationTagAlreadyExists = locationGroupAlreadyMapped.FindAll(t => intersectLocationTags.Contains(t.Id)).ToList();
                    string locationtagAlreadyExists = string.Join(",", locationTagAlreadyExists.Select(t => t.Name));
                    throw new UserFriendlyException("This Location Tag(s) :" + locationtagAlreadyExists + " Already mapped with Another Values");
                }
            }
        }

        public async Task DeletePrintTemplateCondition(IdInput input)
        {
            await _printTemplateRepository.DeleteAsync(t => t.Id == input.Id);
        }
        

        public async Task<ApiPrinterConditionOutput> ApiPrinterTemplateCondition(ApiLocationInput locationInput)
        {
            var output = new ApiPrinterConditionOutput();

            var orgId = await _locationAppService.GetOrgnizationIdForLocationId(locationInput.LocationId);
            if (orgId > 0)
            {
                CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);
            }


            var printTemplateConditions = await _printTemplateRepository.GetAllListAsync(a => 
                a.TenantId == locationInput.TenantId && a.EndDate>=DateTime.Now);

            var myPJs = new List<PrintTemplateCondition>();
            if (locationInput.LocationId != 0)
            {
                foreach (var pj in printTemplateConditions)
                {
                    if (await _locationAppService.IsLocationExists(new CheckLocationInput
                    {
                        LocationId = locationInput.LocationId,
                        Locations = pj.Locations,
                        Group = pj.Group,
                        LocationTag = pj.LocationTag,
                        NonLocations = pj.NonLocations
                    }))
                    {
                        myPJs.Add(pj);
                    }
                }
            }
            else
            {
                myPJs = printTemplateConditions.ToList();
            }

            foreach (var pt in myPJs)
            {
                var myPrinter = pt.MapTo<ApiPrinterTemplateConditionDto>();
                var printConfigurations = await _printConfigurationRepository.GetAll().Where(a => a.Id.Equals(pt.PrintConfigurationId)).ToListAsync();
                if (printConfigurations.Any())
                {
                    var pC = printConfigurations.LastOrDefault();
                    if (pC != null)
                    {
                        myPrinter.Id = pC.Id;
                        myPrinter.Name = pC.Name;
                        output.PrintTemplateConditions.Add(myPrinter);
                    }
                }
            }

            return output;
        }
    }
}