﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations;
using System.ComponentModel.DataAnnotations;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations.Dtos;

namespace DinePlan.DineConnect.Connect.PrintTemplateConditions.Dtos
{
    public class CreateOrEditPrintTemplateConditionInput
    {
        public PrintTemplateConditionEditDto ConditionEditDto { get; set; }
    }
    public class GetPrintTemplateConditionOutputDto : IOutputDto
    {
        public PrintConfigurationEditDto PrintConfiguration { get; set; }
        public PrintTemplateConditionEditDto PrintTemplateCondition { get; set; }

    }
}