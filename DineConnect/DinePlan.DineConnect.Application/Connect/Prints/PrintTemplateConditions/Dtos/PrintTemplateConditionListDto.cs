﻿using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using System;

namespace DinePlan.DineConnect.Connect.PrintTemplateConditions.Dtos
{
    [AutoMapFrom(typeof(Display.PrintTemplateCondition))]
    public class PrintTemplateConditionListDto : CreationAuditedEntity<int?>
    {
        public virtual int PrintConfigurationId { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ConditionSchedules { get; set; }
        public string Filter { get; set; }
        public string Contents { get; set; }
    }
}