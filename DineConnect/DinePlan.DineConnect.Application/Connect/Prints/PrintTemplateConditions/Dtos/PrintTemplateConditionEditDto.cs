﻿using Abp.AutoMapper;
using DinePlan.DineConnect.Connect.Location;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.PrintTemplateConditions.Dtos
{
    [AutoMapTo(typeof(Display.PrintTemplateCondition))]
    public class PrintTemplateConditionEditDto : ConnectEditDto
    {
        public PrintTemplateConditionEditDto()
        {
            ConditionScheduleList = new List<PromotionScheduleEditDto>();
            LocationGroup = new LocationGroupDto();
        }

        public int? Id { get; set; }
        public string Name { get; set; }
        public string AliasName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int PromotionTypeId { get; set; }
        public bool Active { get; set; }
        public string Filter { get; set; }
        public string ConditionSchedules { get; set; }
        public string Contents { get; set; }
        public virtual int PrintConfigurationId { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public List<PromotionScheduleEditDto> ConditionScheduleList { get; set; }
        //    get
        //    {
        //        var list = new List<PromotionScheduleEditDto>();
        //        if (!ConditionSchedules.IsNullOrEmpty())
        //        {
        //            list = JsonConvert.DeserializeObject<List<PromotionScheduleEditDto>>(ConditionSchedules);
        //        }
        //        return list;
        //    }
        //}
    }
}