﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace DinePlan.DineConnect.Connect.PrintTemplateConditions.Dtos
{
    [AutoMapFrom(typeof(Promotion))]
    public class PromotionListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreateDateTime { get; set; }
        public int PromotionTypeId { get; set; }
        public string PromotionType { get; set; }
    }

    [AutoMapTo(typeof(Promotion))]
    public class PromotionEditDto : ConnectEditDto
    {
        public PromotionEditDto()
        {
            PromotionSchedules = new Collection<PromotionScheduleEditDto>();
            PromotionRestrictItems = new Collection<PromotionRestrictItemEditDto>();
        }

        public int? Id { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int PromotionTypeId { get; set; }
        public bool Active { get; set; }
        public string Filter { get; set; }
        public int SortOrder { get; set; }

        public Collection<PromotionScheduleEditDto> PromotionSchedules { get; set; }
        public Collection<PromotionRestrictItemEditDto> PromotionRestrictItems { get; set; }

        public void AddSchedule(int startHour, int startMinute, int endHour, int endMinute)
        {
            PromotionSchedules.Add(new PromotionScheduleEditDto
            {
                StartHour = startHour,
                StartMinute = startMinute,
                EndHour = endHour,
                EndMinute = endMinute
            });
        }
    }

    [AutoMapTo(typeof(PromotionRestrictItem))]
    public class PromotionRestrictItemEditDto
    {
        public PromotionRestrictItemEditDto()
        {
        }

        public int? Id { get; set; }
        public int MenuItemId { get; set; }
        public string Name { get; set; }
    }

    [AutoMapTo(typeof(PromotionSchedule))]
    public class PromotionScheduleEditDto
    {
        private string _Days;
        private string _MonthDays;

        public PromotionScheduleEditDto()
        {
            _Days = null;
            _MonthDays = null;
        }

        public int? Id { get; set; }
        public int StartHour { get; set; }
        public int StartMinute { get; set; }
        public int EndHour { get; set; }
        public int EndMinute { get; set; }
        public List<ComboboxItemDto> AllDays { get; set; }
        public List<ComboboxItemDto> AllMonthDays { get; set; }

        public string Days
        {
            get
            {
                if (AllDays == null || !AllDays.Any())
                    return _Days;
                return string.Join(",", AllDays.Select(a => a.Value).ToArray());
            }
            set { _Days = value; }
        }

        public string MonthDays
        {
            get
            {
                if (AllMonthDays == null || !AllMonthDays.Any())
                {
                    return _MonthDays;
                }
                return string.Join(",", AllMonthDays.Select(a => a.Value).ToArray());
            }
            set { _MonthDays = value; }
        }
    }

    [AutoMapTo(typeof(TimerPromotion))]
    public class TimerPromotionEditDto
    {
        public int? Id { get; set; }
        public int PriceTagId { get; set; }
    }

    [AutoMapTo(typeof(FixedPromotion))]
    public class FixedPromotionEditDto
    {
        public int? Id { get; set; }
        public int CategoryId { get; set; }
        public int MenuItemId { get; set; }
        public int PromotionValueType { get; set; }
        public decimal PromotionValue { get; set; }
    }

    [AutoMapTo(typeof(FreePromotion))]
    public class FreePromotionEditDto
    {
        public int? Id { get; set; }
        public bool AllFrom { get; set; }
        public int FromCount { get; set; }
        public int ToCount { get; set; }
        public Collection<FreePromotionExecutionEditDto> FreePromotionExecutions { get; set; }
        public Collection<FreePromotionExecutionEditDto> From { get; set; }
        public Collection<FreePromotionExecutionEditDto> To { get; set; }

        public FreePromotionEditDto()
        {
            FreePromotionExecutions = new Collection<FreePromotionExecutionEditDto>();
            From = new Collection<FreePromotionExecutionEditDto>();
            To = new Collection<FreePromotionExecutionEditDto>();
            FromCount = 1;
            ToCount = 1;
        }
    }

    [AutoMapTo(typeof(FreePromotionExecution))]
    public class FreePromotionExecutionEditDto
    {
        public int? Id { get; set; }
        public int MenuItemId { get; set; }
        public string PortionName { get; set; }
        public int MenuItemPortionId { get; set; }
        public bool From { get; set; }
    }

    [AutoMapTo(typeof(FreeValuePromotion))]
    public class FreeValuePromotionEditDto
    {
        public int? Id { get; set; }
        public bool AllFrom { get; set; }
        public int FromCount { get; set; }
        public Collection<FreeValuePromotionExecutionEditDto> FreeValuePromotionExecutions { get; set; }
        public Collection<FreeValuePromotionExecutionEditDto> From { get; set; }
        public Collection<FreeValuePromotionExecutionEditDto> To { get; set; }

        public FreeValuePromotionEditDto()
        {
            FreeValuePromotionExecutions = new Collection<FreeValuePromotionExecutionEditDto>();
            From = new Collection<FreeValuePromotionExecutionEditDto>();
            To = new Collection<FreeValuePromotionExecutionEditDto>();
            FromCount = 1;
        }
    }

    [AutoMapTo(typeof(FreeValuePromotionExecution))]
    public class FreeValuePromotionExecutionEditDto
    {
        public int? Id { get; set; }
        public int MenuItemId { get; set; }
        public string PortionName { get; set; }
        public int MenuItemPortionId { get; set; }
        public bool From { get; set; }
        public virtual int ValueType { get; set; }
        public virtual decimal Value { get; set; }
    }

    [AutoMapTo(typeof(FreeItemPromotion))]
    public class FreeItemPromotionEditDto
    {
        public int? Id { get; set; }
        public int ItemCount { get; set; }
        public bool Confirmation { get; set; }
        public decimal TotalAmount { get; set; }
        public bool MultipleTimes { get; set; }
        public Collection<FreeItemPromotionExecutionEditDto> Executions { get; set; }

        public FreeItemPromotionEditDto()
        {
            Executions = new Collection<FreeItemPromotionExecutionEditDto>();
        }
    }

    [AutoMapTo(typeof(FreeItemPromotionExecution))]
    public class FreeItemPromotionExecutionEditDto
    {
        public int? Id { get; set; }
        public int MenuItemId { get; set; }
        public string PortionName { get; set; }
        public int MenuItemPortionId { get; set; }
        public decimal Price { get; set; }
    }

    public class GetPromotionInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public string Operation { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<ComboboxItemDto> PromotionTypes { get; set; }
        public string Location { get; set; }

        public bool Deleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "SortOrder";
            }
        }
    }

    public class GetPromotionForEditOutput : IOutputDto
    {
        public GetPromotionForEditOutput()
        {
            Promotion = new PromotionEditDto();

            FixPromotions = new List<FixedPromotionEditDto>();
            FixPromotionsPer = new List<FixedPromotionEditDto>();
            TimerPromotion = new TimerPromotionEditDto();
            FreePromotion = new FreePromotionEditDto();
            DemandPromotion = new DemandPromotionEditDto();
            FreeItemPromotion = new FreeItemPromotionEditDto();
            TicketDiscountPromotion = new TicketDiscountPromotionDto();
            LocationGroup = new LocationGroupDto();
        }

        public PromotionEditDto Promotion { get; set; }
        public TimerPromotionEditDto TimerPromotion { get; set; }
        public List<FixedPromotionEditDto> FixPromotions { get; set; }
        public List<FixedPromotionEditDto> FixPromotionsPer { get; set; }
        public FreePromotionEditDto FreePromotion { get; set; }
        public FreeValuePromotionEditDto FreeValuePromotion { get; set; }
        public LocationGroupDto LocationGroup { get; set; }

        public DemandPromotionEditDto DemandPromotion { get; set; }
        public FreeItemPromotionEditDto FreeItemPromotion { get; set; }

        public TicketDiscountPromotionDto TicketDiscountPromotion { get; set; }

        public string Filter { get; set; }
    }

    public class CreateOrUpdatePromotionInput : IInputDto
    {
        [Required]
        public PromotionEditDto Promotion { get; set; }

        public TimerPromotionEditDto TimerPromotion { get; set; }
        public List<FixedPromotionEditDto> FixPromotions { get; set; }
        public List<FixedPromotionEditDto> FixPromotionsPer { get; set; }
        public FreePromotionEditDto FreePromotion { get; set; }
        public FreeValuePromotionEditDto FreeValuePromotion { get; set; }
        public DemandPromotionEditDto DemandPromotion { get; set; }
        public FreeItemPromotionEditDto FreeItemPromotion { get; set; }
        public TicketDiscountPromotionDto TicketDiscountPromotion { get; set; }

        public LocationGroupDto LocationGroup { get; set; }
        public string Filter { get; set; }
    }

    [AutoMapTo(typeof(TicketDiscountPromotion))]
    public class TicketDiscountPromotionDto
    {
        public TicketDiscountPromotionDto()
        {
            PromotionValueType = 1;
        }

        public int? Id { get; set; }
        public int PromotionValueType { get; set; }
        public string ButtonCaption { get; set; }
        public decimal PromotionValue { get; set; }
        public bool AskReference { get; set; }
        public bool AuthenticationRequired { get; set; }
    }

    [AutoMapTo(typeof(DemandDiscountPromotion))]
    public class DemandPromotionEditDto
    {
        public DemandPromotionEditDto()
        {
            PromotionValueType = 1;
            DemandPromotionExecutions = new Collection<DemandPromotionExecutionEditDto>();
        }

        public int? Id { get; set; }
        public int PromotionValueType { get; set; }
        public string ButtonCaption { get; set; }
        public string ButtonColor { get; set; }
        public decimal PromotionValue { get; set; }
        public bool PromotionOverride { get; set; }
        public bool AuthenticationRequired { get; set; }
        public bool AskReference { get; set; }
        public Collection<DemandPromotionExecutionEditDto> DemandPromotionExecutions { get; set; }
    }

    [AutoMapTo(typeof(DemandPromotionExecution))]
    public class DemandPromotionExecutionEditDto
    {
        public int? Id { get; set; }
        public int CategoryId { get; set; }
        public int MenuItemId { get; set; }
        public string CategoryName { get; set; }
    }

    public class PromotionFilter
    {
        public string Id { get; set; }
        public string Label { get; set; }
        public string Type { get; set; }
        public string Input { get; set; }
        public string Operator { get; set; }
        public string Operators { get; set; }
        public string Values { get; set; }
        public decimal Value { get; set; }
    }

    public class ApiPromotionDefinitionInput : IInputDto
    {
        public int TenantId { get; set; }
        public int PromotionId { get; set; }
        public int LocationId { get; set; }
    }

    public class ApiPromotionOutput : IOutputDto
    {
        public List<PromotionEditDto> Promotions { get; set; }
    }

    public class ApiTimerPromotionOutput : IOutputDto
    {
        public string PriceTag { get; set; }
    }

    public class ApiFixedPromotionOutput : IOutputDto
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int MenuItemId { get; set; }
        public int ValueType { get; set; }
        public decimal Value { get; set; }
    }
}