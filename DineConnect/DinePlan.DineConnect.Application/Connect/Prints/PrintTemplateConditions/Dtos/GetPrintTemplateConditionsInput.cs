﻿using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.PrintTemplateConditions.Dtos
{
    public class GetPrintTemplateConditionsInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public virtual int PrintConfigurationId { get; set; }
        public string Filter { get; set; }

        public string Location { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime Desc";
            }
        }
    }
}