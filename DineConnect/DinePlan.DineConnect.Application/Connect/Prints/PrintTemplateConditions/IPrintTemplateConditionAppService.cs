﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations.Dtos;
using DinePlan.DineConnect.Connect.Prints.PrintTemplates.Dtos;
using DinePlan.DineConnect.Connect.PrintTemplateConditions.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Prints.PrintTemplateConditions
{
    public interface IPrintTemplateConditionAppService : IApplicationService
    {
        Task<PagedResultOutput<PrintConfigurationListDto>> GetPrintTemplateConditions(GetPrintTemplateConditionsInput input);

        Task<GetPrintTemplateConditionOutputDto> GetPrintTemplateConditionForEdit(FilterInputDto input);

        Task DeletePrintConfigForPrintTemplateCondition(NullableIdInput input);

        ListResultDto<ComboboxItemDto> GetPrintTemplateTypes();

        Task<FileDto> GetAllToExcel(GetPrintTemplateConditionsInput input);

        Task ActivateItem(IdInput input);
        Task<IdInput> AddOrEditPrintTemplateCondition(CreateOrEditPrintTemplateConditionInput input);
        Task DeletePrintTemplateCondition(IdInput input);

        Task<ApiPrinterConditionOutput> ApiPrinterTemplateCondition(ApiLocationInput locationInput);

    }
}