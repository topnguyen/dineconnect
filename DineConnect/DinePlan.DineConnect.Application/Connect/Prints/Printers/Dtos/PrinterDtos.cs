﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Display;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations.Dtos;

namespace DinePlan.DineConnect.Connect.Prints.Printers.Dtos
{
    [AutoMapFrom(typeof(Printer))]
    public class PrinterListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
        public int PrinterType { get; set; }

        public string PrinterTypeName
        {
            get
            {
                return ((PrinterTypes)PrinterType).ToString();
            }
        }
        public virtual int PrintConfigurationId { get; set; }
        public string AliasName { get; set; }
        public int CodePage { get; set; }
        public int CharsPerLine { get; set; }
        public int NumberOfLines { get; set; }

        public int FallBackPrinter { get; set; }
        public string FallBackPrinterName { get; set; }
        public string CharReplacement { get; set; }

        public string CustomPrinterName { get; set; }
        public string CustomPrinterData { get; set; }
        public string Locations { get; set; }
    }

    [AutoMapTo(typeof(Printer))]
    public class PrinterEditDto : ConnectEditDto
    {
        public int? Id { get; set; }
        public virtual int PrintConfigurationId { get; set; }
        public string AliasName { get; set; }
        public int PrinterType { get; set; }
        public string PrinterTypeName { get; set; }
        public int CodePage { get; set; }
        public int CharsPerLine { get; set; }
        public int NumberOfLines { get; set; }

        public int FallBackPrinter { get; set; }
        public string FallBackPrinterName { get; set; }
        public string CharReplacement { get; set; }

        public string CustomPrinterName { get; set; }
        public string CustomPrinterData { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public PrinterEditDto()
        {
            LocationGroup = new LocationGroupDto();
        }
    }

    public class GetPrinterInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public virtual int PrintConfigurationId { get; set; }
        public string Filter { get; set; }

        public string Operation { get; set; }

        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime Desc";
            }
        }

        public string Location { get; set; }
    }

    public class GetPrinterForEditOutput : IOutputDto
    {
        public GetPrinterForEditOutput()
        {
            LocationGroup = new LocationGroupDto();
            Printer = new PrinterEditDto();
        }

        public PrinterEditDto Printer { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
    }



    public class ApiPrinterOutput
    {
        public ApiPrinterOutput()
        {
            Printers = new List<ApiPrinterDto>();
        }
        public IList<ApiPrinterDto> Printers { get; set; }
    }

    [AutoMapTo(typeof(Printer))]
    public class ApiPrinterDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int PrinterType { get; set; }
        public int CodePage { get; set; }
        public int CharsPerLine { get; set; }
        public int NumberOfLines { get; set; }
        public string CustomPrinterName { get; set; }
        public string CustomPrinterData { get; set; }
        public string CharReplacement { get; set; }
        public int FallBackPrinter { get; set; }
    }
    public class CreateOrUpdatePrinterInput : IInputDto
    {
        public PrinterEditDto Printer { get; set; }
    }
    public class GetPrinterOutputDto : IOutputDto
    {
        public PrintConfigurationEditDto PrintConfiguration { get; set; }
        public PrinterEditDto Printer { get; set; }

    }

}