﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations;
using DinePlan.DineConnect.Connect.Prints.Printers.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations.Dtos;

namespace DinePlan.DineConnect.Connect.Prints.Printers
{
    public interface IPrinterAppService : IApplicationService
    {
        Task ActivateItem(IdInput input);

        Task DeletePrintConfigForPrinter(NullableIdInput input);

        Task<FileDto> GetAllToExcel(GetPrinterInput input);

        Task<ListResultDto<ComboboxItemDto>> GetFallBackPrinters(NullableIdInput input);

        Task<GetPrinterOutputDto> GetPrinterForEdit(FilterInputDto input);

        Task<PagedResultOutput<PrintConfigurationListDto>> GetPrinters(GetPrinterInput input);

        Task<ListResultDto<ComboboxItemDto>> GetPrintersForCombobox();

        Task<ListResultDto<ComboboxItemDto>> GetPrinterTemplates();

        ListResultDto<ComboboxItemDto> GetPrinterTypes();
        Task<ApiPrinterOutput> ApiPrinter(ApiLocationInput locationInput);
        Task<IdInput> AddOrEditPrinter(CreateOrUpdatePrinterInput input);
        Task DeletePrinter(IdInput input);
    }
}