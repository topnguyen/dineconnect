﻿using DinePlan.DineConnect.Connect.Prints.PrintConfigurations;
using DinePlan.DineConnect.Connect.Prints.Printers.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations.Dtos;

namespace DinePlan.DineConnect.Connect.Prints.Printers.Exporting
{
    public interface IPrinterExporter
    {
        FileDto ExportToFile(List<PrintConfigurationListDto> dtos);
    }
}