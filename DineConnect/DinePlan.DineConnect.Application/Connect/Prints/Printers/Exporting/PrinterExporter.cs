﻿using DinePlan.DineConnect.Connect.Prints.PrintConfigurations;
using DinePlan.DineConnect.Connect.Prints.Printers.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations.Dtos;

namespace DinePlan.DineConnect.Connect.Prints.Printers.Exporting
{
    public class PrinterExporter : FileExporterBase, IPrinterExporter
    {
        public FileDto ExportToFile(List<PrintConfigurationListDto> dtos)
        {
            return CreateExcelPackage(
                "PrinterList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(base.L("PrintTemplate"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        base.L("Name"),
                        base.L("CreationTime")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Name,
                        _ => _.CreationTime
                        );

                    var creationTimeColumn = sheet.Column(2);
                    creationTimeColumn.Style.Numberformat.Format = "yyyy-mm-dd hh:mm";

                    for (var i = 1; i <= 5; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}