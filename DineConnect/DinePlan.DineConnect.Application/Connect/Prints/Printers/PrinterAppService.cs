﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Connect.Display;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations;
using DinePlan.DineConnect.Connect.Prints.Printers.Dtos;
using DinePlan.DineConnect.Connect.Prints.Printers.Exporting;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations.Dtos;
using DinePlan.DineConnect.Connect.Sync;

namespace DinePlan.DineConnect.Connect.Prints.Printers
{
    public class PrinterAppService : DineConnectAppServiceBase, IPrinterAppService
    {
        private readonly IRepository<Printer> _printerRepository;
        private readonly IRepository<PrintConfiguration> _printConfigurationRepository;
        private readonly IRepository<Display.PrintTemplate> _printerTemplateRepository;
        private readonly ILocationAppService _locAppService;
        private readonly IPrinterExporter _printerExporter;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ISyncAppService _syncAppService;

        public PrinterAppService(IRepository<Printer> printerRepository,
            IRepository<PrintConfiguration> printConfigurationRepository,
                                 IRepository<Display.PrintTemplate> printerTemplateRepository,
                                 IPrinterExporter printerExporter,
                                 IUnitOfWorkManager unitOfWorkManager,ISyncAppService syncAppService,
                                 ILocationAppService locationAppService)
        {
            _printerRepository = printerRepository;
            _printConfigurationRepository = printConfigurationRepository;
            _printerTemplateRepository = printerTemplateRepository;
            _printerExporter = printerExporter;
            _unitOfWorkManager = unitOfWorkManager;
            _locAppService = locationAppService;
            _syncAppService = syncAppService;
        }

        public async Task<PagedResultOutput<PrintConfigurationListDto>> GetPrinters(GetPrinterInput input)
        {
            var allItems = _printConfigurationRepository
                            .GetAll()
                            .Where(i => i.IsDeleted == input.IsDeleted)
                            .WhereIf(
                                !input.Filter.IsNullOrEmpty(),
                                p => p.Name.Contains(input.Filter));
            if (!input.Location.IsNullOrWhiteSpace())
            {
                var printer = _printerRepository.GetAll().Where(i => i.IsDeleted == input.IsDeleted).WhereIf(
                    !input.Location.IsNullOrWhiteSpace(),
                    p => p.Locations.Contains(input.Location));
                var printerIds = printer.Select(t => t.PrintConfigurationId).ToList();
                allItems = allItems.Where(t => printerIds.Contains(t.Id));
            }
            allItems = allItems.Where(t => t.PrintConfigurationType == PrintConfigurationType.Printer);
            var sortMenuItems = await allItems.OrderBy(input.Sorting).PageBy(input).ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<PrintConfigurationListDto>>();
            var totalCount = await allItems.CountAsync();

            return new PagedResultOutput<PrintConfigurationListDto>(totalCount, allListDtos);

        }

        public async Task<FileDto> GetAllToExcel(GetPrinterInput input)
        {
            var allList = await GetPrinters(input);
            var allListDtos = allList.Items.MapTo<List<PrintConfigurationListDto>>();
            return _printerExporter.ExportToFile(allListDtos);
        }

        public async Task<GetPrinterOutputDto> GetPrinterForEdit(FilterInputDto input)
        {
            PrintConfigurationEditDto editDto;

            try
            {
                if (input.Id.HasValue)
                {
                    List<SimpleLocationDto> filterLocation = new List<SimpleLocationDto>();
                    List<SimpleLocationGroupDto> filterLocationGroup = new List<SimpleLocationGroupDto>();
                    List<int> filterLocationRefIds = new List<int>();
                    List<int> filterLocationGroupRefIds = new List<int>();
                    List<int> filterLocationTagRefIds = new List<int>();
                    if (input.LocationGroupToBeFiltered != null)
                    {
                        filterLocationRefIds = input.LocationGroupToBeFiltered.Locations.Select(t => t.Id).ToList();
                        filterLocationGroupRefIds = input.LocationGroupToBeFiltered.Groups.Select(t => t.Id).ToList();
                        filterLocationTagRefIds = input.LocationGroupToBeFiltered.LocationTags.Select(t => t.Id).ToList();
                    }

                    var printConfiguration = await _printConfigurationRepository.FirstOrDefaultAsync(e => e.Id == input.Id.Value);
                    editDto = printConfiguration.MapTo<PrintConfigurationEditDto>();

                    var printer = await _printerRepository.GetAllListAsync(t => t.PrintConfigurationId == input.Id.Value);
                    var tempValuesFilter = printer.MapTo<Collection<PrinterEditDto>>();
                    ListResultDto<ComboboxItemDto> rsPrinterTypes = new ListResultDto<ComboboxItemDto>();
                    rsPrinterTypes = GetPrinterTypes();
                    var rsprintConfigurationlist = await _printConfigurationRepository.GetAllListAsync();
                    foreach (var lst in tempValuesFilter)
                    {
                        var printertypename = rsPrinterTypes.Items.FirstOrDefault(t => t.Value == lst.PrinterType.ToString());
                        var fallbackprinter = rsprintConfigurationlist.FirstOrDefault(t => t.Id == lst.FallBackPrinter);
                        lst.PrinterTypeName = printertypename.DisplayText;
                        if (fallbackprinter != null)
                        {
                            lst.FallBackPrinterName = fallbackprinter.Name;
                        }
                    }


                    Collection<PrinterEditDto> valuesOutput = new Collection<PrinterEditDto>();
                    foreach (var lst in tempValuesFilter)
                    {
                        UpdateLocationAndNonLocationForEdit(lst, lst.LocationGroup);
                        if (filterLocationRefIds.Count > 0 || filterLocationGroupRefIds.Count > 0 || filterLocationTagRefIds.Count > 0)
                        {
                            if (lst.LocationGroup.Locations.Select(t => t.Id).Intersect(filterLocationRefIds).ToList().Count > 0)
                                valuesOutput.Add(lst);
                            else if (lst.LocationGroup.Groups.Select(t => t.Id).Intersect(filterLocationGroupRefIds).ToList().Count > 0)
                                valuesOutput.Add(lst);
                            else if (lst.LocationGroup.LocationTags.Select(t => t.Id).Intersect(filterLocationTagRefIds).ToList().Count > 0)
                                valuesOutput.Add(lst);
                        }
                        else
                        {
                            valuesOutput.Add(lst);
                        }
                    }
                    editDto.Printers = valuesOutput;
                }
                else
                {
                    editDto = new PrintConfigurationEditDto();
                }

                return new GetPrinterOutputDto
                {
                    PrintConfiguration = editDto,
                    Printer = new PrinterEditDto()
                };
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Cannot get Printer");
            }
            
        }

      
        public async Task DeletePrintConfigForPrinter(NullableIdInput input)
        {
            await _printerRepository.DeleteAsync(t => t.PrintConfigurationId == input.Id);
            await _printConfigurationRepository.DeleteAsync(input.Id.Value);
            
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetPrinterTemplates()
        {
            var returnList = await _printConfigurationRepository.GetAll().Where(t => t.PrintConfigurationType == PrintConfigurationType.PrintTemplate)
                .Select(e => new ComboboxItemDto
                {
                    Value = e.Id.ToString(),
                    DisplayText = e.Name.ToString()
                })
                .ToListAsync();
            return new ListResultOutput<ComboboxItemDto>(returnList);

        }

        public ListResultDto<ComboboxItemDto> GetPrinterTypes()
        {
            var returnList = new List<ComboboxItemDto>();

            var i = 0;
            foreach (var name in Enum.GetNames(typeof(PrinterTypes)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetPrintersForCombobox()
        {
            var returnList = await _printConfigurationRepository.GetAll().Where(t => t.PrintConfigurationType == PrintConfigurationType.Printer)
                .Select(e => new ComboboxItemDto
                {
                    Value = e.Id.ToString(),
                    DisplayText = e.Name.ToString()
                })
                .ToListAsync();
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetFallBackPrinters(NullableIdInput input)
        {
            var returnList = await _printConfigurationRepository.GetAll()
                .WhereIf(input.Id.HasValue, e => e.Id != input.Id&&e.PrintConfigurationType==PrintConfigurationType.Printer)
                .Select(e => new ComboboxItemDto
                {
                    Value = e.Id.ToString(),
                    DisplayText = e.Name
                })
                .ToListAsync();
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _printerRepository.GetAsync(input.Id);
                item.IsDeleted = false;

                await _printerRepository.UpdateAsync(item);
            }
        }

       
        public async Task<ApiPrinterOutput> ApiPrinter(ApiLocationInput locationInput)
        {
            var orgId = await _locAppService.GetOrgnizationIdForLocationId(locationInput.LocationId);
            if (orgId > 0)
            {
                CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);
            }

            var output = new ApiPrinterOutput();
            var printers = await _printerRepository.GetAllListAsync(a => a.TenantId == locationInput.TenantId);
            var myPrinters = new List<Printer>();
            if (locationInput.LocationId != 0)
            {
                foreach (var printer in printers)
                {
                    if (await _locAppService.IsLocationExists(new CheckLocationInput
                    {
                        LocationId = locationInput.LocationId,
                        Locations = printer.Locations,
                        Group = printer.Group,
                        LocationTag = printer.LocationTag,
                        NonLocations = printer.NonLocations
                    }))
                    {
                        myPrinters.Add(printer);
                    }
                }
            }
            else
            {
                myPrinters = printers.ToList();
            }

            foreach (var printer in myPrinters)
            {
                var myPrinter = printer.MapTo<ApiPrinterDto>();
                var printConfigurations = await _printConfigurationRepository.GetAll().Where(a=>a.Id.Equals(printer.PrintConfigurationId)).ToListAsync();
                if (printConfigurations.Any())
                {
                    var pC = printConfigurations.LastOrDefault();
                    if (pC != null)
                    {
                        myPrinter.Id = printer.PrintConfigurationId;
                        myPrinter.Name = pC.Name;
                        output.Printers.Add(myPrinter);
                    }
                }
                
            }

            return output;
        }
        public async Task<IdInput> AddOrEditPrinter(CreateOrUpdatePrinterInput input)
        {
            await BusinessRulesForPrinter(input);
            var printer = input.Printer;
            await _syncAppService.UpdateSync(SyncConsts.PRINTER);

            if (printer.Id.HasValue)
            {
                var existEntry = await _printerRepository.FirstOrDefaultAsync(t => t.Id == printer.Id.Value);
                printer.MapTo(existEntry);
                UpdateLocationAndNonLocation(existEntry, printer.LocationGroup);
                await _printerRepository.UpdateAsync(existEntry);
                return new IdInput { Id = existEntry.Id };
            }
            else
            {
                var dto = printer.MapTo<Printer>();
                UpdateLocationAndNonLocation(dto, printer.LocationGroup);
                await _printerRepository.InsertOrUpdateAndGetIdAsync(dto);
                return new IdInput { Id = dto.Id };
            }


        }

        public async Task BusinessRulesForPrinter(CreateOrUpdatePrinterInput input)
        {
            List<SimpleLocationDto> newLocationToBeAdded = new List<SimpleLocationDto>();
            List<SimpleLocationGroupDto> newLocationGroupToBeAdded = new List<SimpleLocationGroupDto>();
            List<SimpleLocationTagDto> newLocationTagToBeAdded = new List<SimpleLocationTagDto>();
            var locationGroupDto = input.Printer.LocationGroup;
            if (locationGroupDto.Locations.Count > 0)
            {
                newLocationToBeAdded.AddRange(locationGroupDto.Locations);
            }
            if (locationGroupDto.Groups.Count > 0)
            {
                newLocationGroupToBeAdded.AddRange(locationGroupDto.Groups);
            }
            if (locationGroupDto.LocationTags.Count > 0)
            {
                newLocationTagToBeAdded.AddRange(locationGroupDto.LocationTags);
            }

            var iQPv = _printerRepository.GetAll().Where(t => t.PrintConfigurationId == input.Printer.PrintConfigurationId);
            if (input.Printer.Id.HasValue)
                iQPv = iQPv.Where(t => t.Id != input.Printer.Id);
            var otherProgramValues = await iQPv.ToListAsync();
            var printerDtos = otherProgramValues.MapTo<Collection<PrinterEditDto>>();
            List<SimpleLocationDto> locationAlreadyMapped = new List<SimpleLocationDto>();
            List<SimpleLocationGroupDto> locationGroupAlreadyMapped = new List<SimpleLocationGroupDto>();
            List<SimpleLocationTagDto> locationTagAlreadyMapped = new List<SimpleLocationTagDto>();
            foreach (var lst in printerDtos)
            {
                UpdateLocationAndNonLocationForEdit(lst, lst.LocationGroup);
                if (lst.LocationGroup.Locations.Count > 0)
                    locationAlreadyMapped.AddRange(lst.LocationGroup.Locations);
                if (lst.LocationGroup.Groups.Count > 0)
                    locationGroupAlreadyMapped.AddRange(lst.LocationGroup.Groups);
                if (lst.LocationGroup.LocationTags.Count > 0)
                    locationTagAlreadyMapped.AddRange(lst.LocationGroup.LocationTags);
               
            }

            if (locationAlreadyMapped.Count > 0)
            {
                var intersectLocations = locationAlreadyMapped.Select(t => t.Id).ToList().Intersect(newLocationToBeAdded.Select(t => t.Id).ToList()).ToList();
                if (intersectLocations.Count > 0)
                {
                    var locationAlreadyExists = locationAlreadyMapped.FindAll(t => intersectLocations.Contains(t.Id)).ToList();
                    string locationAlreadExists = string.Join(",", locationAlreadyExists.Select(t => t.Name));
                    throw new UserFriendlyException("This Location(s) :" + locationAlreadExists + " Already mapped with Another Values");
                }
            }
            if (locationGroupAlreadyMapped.Count > 0)
            {
                var intersectLocationGroups = locationGroupAlreadyMapped.Select(t => t.Id).ToList().Intersect(newLocationGroupToBeAdded.Select(t => t.Id).ToList()).ToList();
                if (intersectLocationGroups.Count > 0)
                {
                    var locationGroupAlreadyExists = locationGroupAlreadyMapped.FindAll(t => intersectLocationGroups.Contains(t.Id)).ToList();
                    string locationgroupAlreadyExists = string.Join(",", locationGroupAlreadyExists.Select(t => t.Name));
                    throw new UserFriendlyException("This Location Group(s) :" + locationgroupAlreadyExists + " Already mapped with Another Values");
                }
            }
            if (locationTagAlreadyMapped.Count > 0)
            {
                var intersectLocationTags = locationTagAlreadyMapped.Select(t => t.Id).ToList().Intersect(newLocationTagToBeAdded.Select(t => t.Id).ToList()).ToList();
                if (intersectLocationTags.Count > 0)
                {
                    var locationTagAlreadyExists = locationGroupAlreadyMapped.FindAll(t => intersectLocationTags.Contains(t.Id)).ToList();
                    string locationtagAlreadyExists = string.Join(",", locationTagAlreadyExists.Select(t => t.Name));
                    throw new UserFriendlyException("This Location Tag(s) :" + locationtagAlreadyExists + " Already mapped with Another Values");
                }
            }
        }

        public async Task DeletePrinter(IdInput input)
        {
            await _printerRepository.DeleteAsync(t => t.Id == input.Id);
        }
    }
}