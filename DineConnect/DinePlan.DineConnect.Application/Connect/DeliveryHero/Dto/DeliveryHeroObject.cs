﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace DinePlan.DineConnect.Connect.DeliveryHero.Dto
{

	[XmlRoot(ElementName="Menu", Namespace="https://foodintegrations.com/Integration/Catalog")]
	public class DeHeMenu {
		[XmlElement(ElementName="Id", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string Id { get; set; }
		[XmlElement(ElementName="Title", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string Title { get; set; }
		[XmlElement(ElementName="Description", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string Description { get; set; }
		[XmlElement(ElementName="StartHour", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string StartHour { get; set; }
		[XmlElement(ElementName="EndHour", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string EndHour { get; set; }
		[XmlElement(ElementName="MenuType", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string MenuType { get; set; }
	}

	[XmlRoot(ElementName="Menus", Namespace="https://foodintegrations.com/Integration/Catalog")]
	public class DeHeMenus {
		[XmlElement(ElementName="Menu", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public List<DeHeMenu> Menu { get; set; }
	}

	[XmlRoot(ElementName="ProductVariation", Namespace="https://foodintegrations.com/Integration/Catalog")]
	public class DeHeProductVariation {
		[XmlElement(ElementName="Id", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string Id { get; set; }
		[XmlElement(ElementName="Title", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string Title { get; set; }
		[XmlElement(ElementName="Price", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string Price { get; set; }
		[XmlElement(ElementName="ContainerPrice", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string ContainerPrice { get; set; }
	}

	[XmlRoot(ElementName="ProductVariations", Namespace="https://foodintegrations.com/Integration/Catalog")]
	public class DeHeProductVariations {
		[XmlElement(ElementName="ProductVariation", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public List<DeHeProductVariation> ProductVariation { get; set; }
	}

	[XmlRoot(ElementName="Product", Namespace="https://foodintegrations.com/Integration/Catalog")]
	public class DeHeProduct {
		[XmlElement(ElementName="Id", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string Id { get; set; }
		[XmlElement(ElementName="Title", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string Title { get; set; }
		[XmlElement(ElementName="Description", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string Description { get; set; }
		[XmlElement(ElementName="Image", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string Image { get; set; }
		[XmlElement(ElementName="ProductVariations", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public DeHeProductVariations ProductVariations { get; set; }
	}

	[XmlRoot(ElementName="Products", Namespace="https://foodintegrations.com/Integration/Catalog")]
	public class DeHeProducts {
		[XmlElement(ElementName="Product", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public List<DeHeProduct> Product { get; set; }
	}

	[XmlRoot(ElementName="MenuCategory", Namespace="https://foodintegrations.com/Integration/Catalog")]
	public class DeHeMenuCategory {
		[XmlElement(ElementName="Id", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string Id { get; set; }
		[XmlElement(ElementName="Title", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string Title { get; set; }
		[XmlElement(ElementName="Description", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string Description { get; set; }
		[XmlElement(ElementName="Products", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public DeHeProducts Products { get; set; }
	}

	[XmlRoot(ElementName="MenuCategories", Namespace="https://foodintegrations.com/Integration/Catalog")]
	public class DeHeMenuCategories {
		[XmlElement(ElementName="MenuCategory", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public List<DeHeMenuCategory> MenuCategory { get; set; }
	}

	[XmlRoot(ElementName="MenuProduct", Namespace="https://foodintegrations.com/Integration/Catalog")]
	public class DeHeMenuProduct {
		[XmlElement(ElementName="MenuId", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string MenuId { get; set; }
		[XmlElement(ElementName="ProductId", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string ProductId { get; set; }
	}

	[XmlRoot(ElementName="MenuProducts", Namespace="https://foodintegrations.com/Integration/Catalog")]
	public class DeHeMenuProducts {
		[XmlElement(ElementName="MenuProduct", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public List<DeHeMenuProduct> MenuProduct { get; set; }
	}

	[XmlRoot(ElementName="ToppingTemplate", Namespace="https://foodintegrations.com/Integration/Catalog")]
	public class DeHeToppingTemplate {
		[XmlElement(ElementName="Id", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string Id { get; set; }
		[XmlElement(ElementName="Title", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string Title { get; set; }
		[XmlElement(ElementName="IsHalfHalf", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string IsHalfHalf { get; set; }
		[XmlElement(ElementName="QuantityMin", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string QuantityMin { get; set; }
		[XmlElement(ElementName="QuantityMax", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string QuantityMax { get; set; }
	}

	[XmlRoot(ElementName="ToppingTemplates", Namespace="https://foodintegrations.com/Integration/Catalog")]
	public class DeHeToppingTemplates {
		[XmlElement(ElementName="ToppingTemplate", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public List<DeHeToppingTemplate> ToppingTemplate { get; set; }
	}

	[XmlRoot(ElementName="ToppingTemplateProduct", Namespace="https://foodintegrations.com/Integration/Catalog")]
	public class DeHeToppingTemplateProduct {
		[XmlElement(ElementName="ToppingTemplateId", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string ToppingTemplateId { get; set; }
		[XmlElement(ElementName="ProductId", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string ProductId { get; set; }
		[XmlElement(ElementName="Price", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string Price { get; set; }
	}

	[XmlRoot(ElementName="ToppingTemplateProducts", Namespace="https://foodintegrations.com/Integration/Catalog")]
	public class DeHeToppingTemplateProducts {
		[XmlElement(ElementName="ToppingTemplateProduct", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public List<DeHeToppingTemplateProduct> ToppingTemplateProduct { get; set; }
	}

	[XmlRoot(ElementName="ProductVariationToppingTemplate", Namespace="https://foodintegrations.com/Integration/Catalog")]
	public class DeHeProductVariationToppingTemplate {
		[XmlElement(ElementName="ProductVariationId", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string ProductVariationId { get; set; }
		[XmlElement(ElementName="ToppingTemplateId", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string ToppingTemplateId { get; set; }
	}

	[XmlRoot(ElementName="ProductVariationToppingTemplates", Namespace="https://foodintegrations.com/Integration/Catalog")]
	public class DeHeProductVariationToppingTemplates {
		[XmlElement(ElementName="ProductVariationToppingTemplate", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public DeHeProductVariationToppingTemplate ProductVariationToppingTemplate { get; set; }
	}

	[XmlRoot(ElementName="MenuCategoryTranslation", Namespace="https://foodintegrations.com/Integration/Catalog")]
	public class DeHeMenuCategoryTranslation {
		[XmlElement(ElementName="Id", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string Id { get; set; }
		[XmlElement(ElementName="Title", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string Title { get; set; }
		[XmlElement(ElementName="Description", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string Description { get; set; }
	}

	[XmlRoot(ElementName="MenuCategoryTranslations", Namespace="https://foodintegrations.com/Integration/Catalog")]
	public class DeHeMenuCategoryTranslations {
		[XmlElement(ElementName="MenuCategoryTranslation", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public List<DeHeMenuCategoryTranslation> MenuCategoryTranslation { get; set; }
	}

	[XmlRoot(ElementName="ProductTranslation", Namespace="https://foodintegrations.com/Integration/Catalog")]
	public class DeHeProductTranslation {
		[XmlElement(ElementName="Id", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string Id { get; set; }
		[XmlElement(ElementName="Title", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string Title { get; set; }
		[XmlElement(ElementName="Description", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string Description { get; set; }
	}

	[XmlRoot(ElementName="ProductTranslations", Namespace="https://foodintegrations.com/Integration/Catalog")]
	public class DeHeProductTranslations {
		[XmlElement(ElementName="ProductTranslation", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public List<DeHeProductTranslation> ProductTranslation { get; set; }
	}

	[XmlRoot(ElementName="Language", Namespace="https://foodintegrations.com/Integration/Catalog")]
	public class DeHeLanguage {
		[XmlElement(ElementName="Code", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string Code { get; set; }
		[XmlElement(ElementName="MenuCategoryTranslations", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public DeHeMenuCategoryTranslations MenuCategoryTranslations { get; set; }
		[XmlElement(ElementName="ProductTranslations", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public DeHeProductTranslations ProductTranslations { get; set; }
		[XmlElement(ElementName="ProductVariationTranslations", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public DeHeProductVariationTranslations ProductVariationTranslations { get; set; }
		[XmlElement(ElementName="ToppingTemplateTranslations", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public DeHeToppingTemplateTranslations ToppingTemplateTranslations { get; set; }
	}

	[XmlRoot(ElementName="ProductVariationTranslation", Namespace="https://foodintegrations.com/Integration/Catalog")]
	public class DeHeProductVariationTranslation {
		[XmlElement(ElementName="Id", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string Id { get; set; }
		[XmlElement(ElementName="Title", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string Title { get; set; }
	}

	[XmlRoot(ElementName="ProductVariationTranslations", Namespace="https://foodintegrations.com/Integration/Catalog")]
	public class DeHeProductVariationTranslations {
		[XmlElement(ElementName="ProductVariationTranslation", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public DeHeProductVariationTranslation ProductVariationTranslation { get; set; }
	}

	[XmlRoot(ElementName="ToppingTemplateTranslation", Namespace="https://foodintegrations.com/Integration/Catalog")]
	public class DeHeToppingTemplateTranslation {
		[XmlElement(ElementName="Id", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string Id { get; set; }
		[XmlElement(ElementName="Title", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string Title { get; set; }
	}

	[XmlRoot(ElementName="ToppingTemplateTranslations", Namespace="https://foodintegrations.com/Integration/Catalog")]
	public class DeHeToppingTemplateTranslations {
		[XmlElement(ElementName="ToppingTemplateTranslation", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public DeHeToppingTemplateTranslation ToppingTemplateTranslation { get; set; }
	}

	[XmlRoot(ElementName="Translations", Namespace="https://foodintegrations.com/Integration/Catalog")]
	public class DeHeTranslations {
		[XmlElement(ElementName="Language", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public List<DeHeLanguage> Language { get; set; }
	}

	[XmlRoot(ElementName="Vendor", Namespace="https://foodintegrations.com/Integration/Catalog")]
	public class DeHeVendor {
		[XmlElement(ElementName="Id", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public string Id { get; set; }
		[XmlElement(ElementName="Menus", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public DeHeMenus Menus { get; set; }
		[XmlElement(ElementName="MenuCategories", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public DeHeMenuCategories MenuCategories { get; set; }
		[XmlElement(ElementName="MenuProducts", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public DeHeMenuProducts MenuProducts { get; set; }
		[XmlElement(ElementName="ToppingTemplates", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public DeHeToppingTemplates ToppingTemplates { get; set; }
		[XmlElement(ElementName="ToppingTemplateProducts", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public DeHeToppingTemplateProducts ToppingTemplateProducts { get; set; }
		[XmlElement(ElementName="ProductVariationToppingTemplates", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public DeHeProductVariationToppingTemplates ProductVariationToppingTemplates { get; set; }
		[XmlElement(ElementName="Translations", Namespace="https://foodintegrations.com/Integration/Catalog")]
		public DeHeTranslations Translations { get; set; }
		[XmlAttribute(AttributeName="xmlns")]
		public string Xmlns { get; set; }

        public DeHeVendor()
        {
            Translations = new DeHeTranslations();
        }
	}

}

