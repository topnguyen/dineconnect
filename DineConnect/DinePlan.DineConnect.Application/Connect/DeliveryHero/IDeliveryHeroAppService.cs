﻿using System.Threading.Tasks;
using Abp.Application.Services;
using DinePlan.DineConnect.Connect.UrbanPiperIntegrate.Dto;

namespace DinePlan.DineConnect.Connect.DeliveryHero
{
    public interface IDeliveryHeroAppService : IApplicationService
    {
        Task Push(PushMenuItemInput input);

    }
}