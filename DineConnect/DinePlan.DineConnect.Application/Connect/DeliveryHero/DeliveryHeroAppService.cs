﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Abp.BackgroundJobs;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Addons.Dto;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.OrderTags;
using DinePlan.DineConnect.Connect.UrbanPiperIntegrate;
using DinePlan.DineConnect.Connect.UrbanPiperIntegrate.Dto;
using DinePlan.DineConnect.Dto;
using Newtonsoft.Json;
using Category = DinePlan.DineConnect.Connect.Master.Category;

namespace DinePlan.DineConnect.Connect.DeliveryHero
{
    public class DeliveryHeroAppService : DineConnectAppServiceBase, IDeliveryHeroAppService
    {
        private readonly IBackgroundJobManager _backgroundJobManager;
        private readonly IRepository<Category> _categoryRepository;
        private readonly IRepository<Master.Location> _locRepo;
        private readonly IRepository<MenuItemPortion> _menuItemPortionRepository;
        private readonly IRepository<MenuItem> _menuItemRepository;
        private readonly IRepository<OrderTagGroup> _orderGroupRepo;
        private readonly IRepository<PriceTag> _priceTagRepo;
        private readonly ISettingManager _settingManager;
        private readonly IScreenMenuAppService _screenAppService;

        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public DeliveryHeroAppService(IRepository<MenuItem> menuItemRepository, ISettingManager settingManager,
            IRepository<Category> categoryRepository, IRepository<OrderTagGroup> orderGroupRepo,
            IRepository<PriceTag> priceTagRepo, IRepository<Master.Location> locRepo,
            IRepository<MenuItemPortion> menuItemPortionRepository, IScreenMenuAppService screenAppService,
            IUnitOfWorkManager unitOfWorkManager, IBackgroundJobManager backgroundJobManager)
        {
            _menuItemRepository = menuItemRepository;
            _categoryRepository = categoryRepository;
            _priceTagRepo = priceTagRepo;
            _menuItemPortionRepository = menuItemPortionRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _backgroundJobManager = backgroundJobManager;
            _settingManager = settingManager;
            _orderGroupRepo = orderGroupRepo;
            _locRepo = locRepo;
            _screenAppService = screenAppService;
        }

        public async Task Push(PushMenuItemInput input)
        {
           
            var uPriceTag = _settingManager.GetSettingValue(AppSettings.ConnectSettings.DeliveryHeroPriceTag);
            if (string.IsNullOrEmpty(uPriceTag)) throw new UserFriendlyException("No Price Tag Configured");

            if(string.IsNullOrEmpty(input.Location))
                throw new UserFriendlyException("Location is not defined in Input");

            if (input.TenantId == null)
            {
                if(AbpSession.TenantId == null)
                    throw new UserFriendlyException("Tenant Id is empty");
                input.TenantId = AbpSession.TenantId;
            }

            Master.Location myLocation = null;


            var allInCludedPlatforms = new List<string>();

            myLocation = await _locRepo.FirstOrDefaultAsync(a => a.Code.Equals(input.Location));
            if (myLocation == null)
                throw new UserFriendlyException("Location is not available");



            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.MustHaveTenant, AppConsts.ConnectFilter))
            {
                var allPrice =
                    await _priceTagRepo.FirstOrDefaultAsync(a => a.TenantId == input.TenantId && a.Name.Equals(uPriceTag));

                if (allPrice == null) throw new UserFriendlyException("No Price Definition for " + uPriceTag);

                if (input.TenantId != null)
                {
                    var allMenu = await _screenAppService.ApiScreenMenu(new ApiLocationInput()
                    {
                        LocationId = myLocation.Id,
                        TenantId =input.TenantId.Value
                    });
                    if (allMenu != null && allMenu.Menu.Any())
                        await PushBasedOnMenu(allMenu, input, allPrice, allInCludedPlatforms);
                }
            }
        }


    
        private async Task PushBasedOnMenu(ApiScreenMenuOutput allMenu,
            PushMenuItemInput input, PriceTag allPrice,
            List<string> allInCludedPlatforms)
        {
            var priceTagPortionItems = allPrice.PriceTagDefinitions.OrderBy(a => a.MenuPortionId);

            var uBaseAddress = _settingManager.GetSettingValue(AppSettings.ConnectSettings.DeliveryHeroUrl);
            var userId = _settingManager.GetSettingValue(AppSettings.ConnectSettings.DeliveryHeroUser);
            var uapiKey = _settingManager.GetSettingValue(AppSettings.ConnectSettings.DeliveryHeroPassword);
            var uPriceTag = _settingManager.GetSettingValue(AppSettings.ConnectSettings.DeliveryHeroPriceTag);




         
        }

        private string GetFileName(string categoryFiles)
        {
            if (!string.IsNullOrEmpty(categoryFiles))
            {
                var allFi = JsonConvert.DeserializeObject<List<FileDto>>(categoryFiles);
                if (allFi != null && allFi.Any()) return allFi.First().FileSystemName;
            }

            return "";
        }

    
    }
}