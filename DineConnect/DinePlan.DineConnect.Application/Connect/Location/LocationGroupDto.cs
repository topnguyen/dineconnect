﻿using DinePlan.DineConnect.Connect.Master.Dtos;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Location
{
    public class LocationGroupDto
    {
        public LocationGroupDto()
        {
            Locations = new List<SimpleLocationDto>();
            Groups = new List<SimpleLocationGroupDto>();
            Group = false;
            LocationTags = new List<SimpleLocationTagDto>();
            LocationTag = false;
            NonLocations = new List<SimpleLocationDto>();
        }

        public List<SimpleLocationDto> Locations { get; set; }
        public List<SimpleLocationGroupDto> Groups { get; set; }
        public List<SimpleLocationTagDto> LocationTags { get; set; }
        public List<SimpleLocationDto> NonLocations { get; set; }

        public bool Group { get; set; }
        public bool LocationTag { get; set; }
        public long UserId { get; set; }
    }
}