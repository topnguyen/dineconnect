﻿using DinePlan.DineConnect.Connect.CreditSalesReport.Dto;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.CreditSalesReport.Exporting
{
    public interface ICreditSalesReportExporter
    {
        Task<FileDto> ExportCreditSales(GetCreditSalesInput input, ICreditSalesReportAppService appService);
    }
}