﻿using Abp.Configuration;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.CreditSalesReport.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.CreditSalesReport.Exporting
{
	public class CreditSalesReportExporter : FileExporterBase, ICreditSalesReportExporter
    {
        public async Task<FileDto> ExportCreditSales(GetCreditSalesInput input, ICreditSalesReportAppService appService)
        {
            var builder = new StringBuilder();
            builder.Append(L("CreditSales"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append("_");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                await GetCreditSales(excelPackage, input, appService);


                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }
        private async Task GetCreditSales(ExcelPackage package, GetCreditSalesInput creditSalesInput, ICreditSalesReportAppService appService)
        {
            var sheet = package.Workbook.Worksheets.Add(L("CreditSales"));
            sheet.OutLineApplyStyle = true;

            var headers = new List<string>
            {
                L("Plant"),
                L("PlantDescription"),
                L("DateTime"),
                L("SalesOrderNo"),
                L("BillStatus"),
                L("BillNumber"),
                L("FullTaxInvoiceNumber"),
                L("RefInvoiceNo"),
                L("CashierName"),
                L("CustomerCode"),
                L("CustomerName"),
                L("CustomerAddress"),
                L("TaxIdenticationNumber"),
                L("PaymentTerm"),
                L("CustomerPhoneNumber"),
                L("RecipientName"),
                L("SaleMode"),
                L("SaleChannel"),
                L("ProductCostExcludeVAT"),
                L("ProductCostIncludeVAT"),
                L("ServiceCostExcludeVAT"),
                L("ServiceCostIncludeVAT"),
                L("WithholdingTaxAmountFromSAP"),
                L("TotalCostIncludeVAT"),
                L("WithholdingTaxDescriptionFromSAP"),
                L("Note")
            };

            int rowCount = 1;

            AddHeader(
                sheet, rowCount, headers.ToArray()
            );
            rowCount++;

            creditSalesInput.SkipCount = 0;
            creditSalesInput.MaxResultCount = 10000;
            var creditSales = await appService.GetCreditSalesReport(creditSalesInput);

            if (creditSales != null)
            {
                foreach (var item in creditSales.Items)
                {
                    sheet.Cells[rowCount, 1].Value = item.Plant;
                    sheet.Cells[rowCount, 2].Value = item.PlantDescription;
                    sheet.Cells[rowCount, 3].Value = item.DateTime.ToString(creditSalesInput.DatetimeFormat);
                    sheet.Cells[rowCount, 4].Value = item.SalesOrderNo;
                    sheet.Cells[rowCount, 5].Value = item.BillStatus;
                    sheet.Cells[rowCount, 6].Value = item.BillNumber;
                    sheet.Cells[rowCount, 7].Value = item.FullTaxInvoiceNumber;
                    sheet.Cells[rowCount, 8].Value = item.RefInvoiceNo;
                    sheet.Cells[rowCount, 9].Value = item.CashierName;
                    sheet.Cells[rowCount, 10].Value = item.CustomerCode;
                    sheet.Cells[rowCount, 11].Value = item.CustomerName;
                    sheet.Cells[rowCount, 12].Value = item.CustomerAddress;
                    sheet.Cells[rowCount, 13].Value = item.TaxIdenticationNumber;
                    sheet.Cells[rowCount, 14].Value = item.CreditTerm;
                    sheet.Cells[rowCount, 15].Value = item.CustomerPhoneNumber;
                    sheet.Cells[rowCount, 16].Value = item.RecipientName;
                    sheet.Cells[rowCount, 17].Value = item.SaleMode;
                    sheet.Cells[rowCount, 18].Value = item.SaleChannel;
                    sheet.Cells[rowCount, 19].Value = item.ProductCostExcludeVAT.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    sheet.Cells[rowCount, 20].Value = item.ProductCostIncludeVAT.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    sheet.Cells[rowCount, 21].Value = item.ServiceCostExcludeVAT.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    sheet.Cells[rowCount, 22].Value = item.ServiceCostIncludeVAT.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    sheet.Cells[rowCount, 23].Value = item.WithholdingTaxAmountFromSAP.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    sheet.Cells[rowCount, 24].Value = item.TotalCostIncludeVAT.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    sheet.Cells[rowCount, 25].Value = item.WithholdingTaxDescriptionFromSAP;
                    sheet.Cells[rowCount, 26].Value = item.Note;
                    rowCount++;
                }
            }

            // Add footer
            sheet.Cells[rowCount, 18].Value = "Total";
            sheet.Cells[rowCount, 19].Value = creditSales.Items.Sum(x => x.ProductCostExcludeVAT).ToString(ConnectConsts.ConnectConsts.NumberFormat);
            sheet.Cells[rowCount, 20].Value = creditSales.Items.Sum(x => x.ProductCostIncludeVAT).ToString(ConnectConsts.ConnectConsts.NumberFormat);
            sheet.Cells[rowCount, 21].Value = creditSales.Items.Sum(x => x.ServiceCostExcludeVAT).ToString(ConnectConsts.ConnectConsts.NumberFormat);
            sheet.Cells[rowCount, 22].Value = creditSales.Items.Sum(x => x.ServiceCostIncludeVAT).ToString(ConnectConsts.ConnectConsts.NumberFormat);
            sheet.Cells[rowCount, 23].Value = creditSales.Items.Sum(x => x.TotalCostIncludeVAT).ToString(ConnectConsts.ConnectConsts.NumberFormat);
            sheet.Cells[rowCount, 24].Value = creditSales.Items.Sum(x => x.WithholdingTaxAmountFromSAP).ToString(ConnectConsts.ConnectConsts.NumberFormat);
            sheet.Cells[$"R{rowCount}:X{rowCount}"].Style.Font.Bold = true;
            for (int i = 1; i <= 26; i++)
            {
                sheet.Column(i).AutoFit();
            }
        }

    }
}