﻿using Abp.Application.Services.Dto;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Report;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System;
using Abp.Configuration;
using Abp.Linq.Extensions;
using System.Data.Entity;
using DinePlan.DineConnect.Connect.Custom.Exporting;
using DinePlan.DineConnect.Connect.CreditSalesReport.Exporting;
using DinePlan.DineConnect.Connect.CreditSalesReport.Dto;
using DinePlan.DineConnect.Connect.Setting;
using DinePlan.DineConnect.Configuration.Tenants;
namespace DinePlan.DineConnect.Connect.CreditSalesReport
{
	public class CreditSalesReportAppService : DineConnectAppServiceBase, ICreditSalesReportAppService
	{
		private readonly IRepository<Company> _companyRe;
		private readonly IRepository<Master.Department> _dRepo;
		private readonly ILocationAppService _locService;
		private readonly IRepository<Master.Location> _lRepo;
		private readonly IRepository<Transaction.Ticket> _ticketManager;
		private readonly IRepository<Transaction.Order> _orderManager;
		private readonly IUnitOfWorkManager _unitOfWorkManager;
		private ICustomizeExporter _customizeExporter;
		private readonly IReportBackgroundAppService _rbas;
		private readonly IBackgroundJobManager _bgm;
		private readonly ICreditSalesReportExporter _exporter;
		private readonly IRepository<Period.WorkPeriod> _workPeriod;
		private readonly IRepository<PaymentType> _payRe;
		private readonly IRepository<Payment> _paymentRepository;
		private readonly IRepository<Master.Terminal> _terminalRepository;
		private readonly IRepository<ProgramSettingTemplate> _programSettingTemplateRepository;
		private readonly ITenantSettingsAppService _tenantSettingsService;

		public CreditSalesReportAppService(
			IRepository<Transaction.Ticket> ticketManager,
			ILocationAppService locService,
			IRepository<Company> comR,
			IUnitOfWorkManager unitOfWorkManager,
			IRepository<Master.Department> drepo,
			IRepository<Master.Location> lRepo,
			IRepository<Transaction.Order> orderManager,
			ICustomizeExporter customizeExporter,
			 IReportBackgroundAppService rbas,
			 IBackgroundJobManager bgm,
			 ICreditSalesReportExporter exporter,
			 IRepository<Period.WorkPeriod> workPeriod,
			 IRepository<PaymentType> payRe,
			 IRepository<Payment> paymentRepository,
			 IRepository<Master.Terminal> terminalRepository,
			 IRepository<ProgramSettingTemplate> programSettingTemplateRepository, 
			 ITenantSettingsAppService tenantSettingsService
			)
		{
			_ticketManager = ticketManager;
			_companyRe = comR;
			_unitOfWorkManager = unitOfWorkManager;
			_dRepo = drepo;
			_locService = locService;
			_lRepo = lRepo;
			_orderManager = orderManager;
			_customizeExporter = customizeExporter;
			_rbas = rbas;
			_bgm = bgm;
			_exporter = exporter;
			_workPeriod = workPeriod;
			_payRe = payRe;
			_paymentRepository = paymentRepository;
			_terminalRepository = terminalRepository; 
			_tenantSettingsService = tenantSettingsService;
			_programSettingTemplateRepository = programSettingTemplateRepository;
		}


		public async Task<PagedResultOutput<GetCreditSalesOutput>> GetCreditSalesReport(GetCreditSalesInput input)
		{
			var result = new PagedResultOutput<GetCreditSalesOutput>();
			try
			{
				var tickets = GetCreditSalesForCondition(input);
				if (input.Sorting == null)
				{
					input.Sorting = "Plant";
				}
				var pttCreditSaleDepartmentsSetting = await _programSettingTemplateRepository.FirstOrDefaultAsync(x => x.Name == "CreditSaleReport");
				if (pttCreditSaleDepartmentsSetting == null) return result;

				var dptIds = pttCreditSaleDepartmentsSetting.DefaultValue.Split(',');
				var dpts = await _dRepo.GetAll().Where(x => dptIds.Any(a => a == x.Id.ToString())).Select(x => x.Name).ToListAsync();
				if (dpts == null) return result;

				var listData = tickets.Include(x => x.Location).Where(x => dpts.Any(d => x.DepartmentName == d)).ToList();
				var creditSales = new List<GetCreditSalesOutput>();
				foreach (var item in listData)
				{
					var tagHANA = item.GetTicketTagValue(ConnectConsts.ConnectConsts.S4HANA);
					if (tagHANA == null) continue;
					var hana = GetHANAData(tagHANA);
					if (hana == null) continue;
			   
					var dto = new GetCreditSalesOutput()
					{
						Id = item.Id,
						Plant = item.Location.Code,
						PlantDescription = item.Location.Name,
						DateTime = item.LastPaymentTime,
						FullTaxInvoiceNumber = item.InvoiceNo,
						CashierName = item.LastModifiedUserName,
						SaleMode = item.DepartmentGroup,
						SaleChannel = item.DepartmentName,
						Note = item.Note,
						BillNumber = item.TicketNumber,
						TicketLogs = item.TicketLogs
					};

					if (hana.S4HanaSaleOrder != null)
					{
						var so = hana.S4HanaSaleOrder;
						dto.SalesOrderNo = so.SalesOrderNo;
						if (so.HeaderDetail != null)
						{
							dto.BillStatus = GetBillStatus(so.HeaderDetail.DocCat);
							dto.RefInvoiceNo = GetRefInvoiceNo(so.HeaderDetail.DocCat);
							dto.CreditTerm = so.HeaderDetail.CreditTermDesc;
							dto.WithholdingTaxAmountFromSAP = so.HeaderDetail.TotalVAT;

						}
						if (so.SoldDetail != null)
						{
							dto.CustomerCode = so.SoldDetail.CustomerNo;
							dto.CustomerName = so.SoldDetail.CustomerName;
							dto.TaxIdenticationNumber = so.SoldDetail.TaxID;
						}
						if (so.ShipDetail != null)
						{
							var sd = so.ShipDetail;
							var adr = sd.Street_SH;
							GetAddress(ref adr, sd.District_SH, ", ");
							GetAddress(ref adr, sd.City_SH, ", ");
							GetAddress(ref adr, sd.PostalCode_SH.ToString(), ", ");
							dto.CustomerAddress = adr;
							dto.RecipientName = sd.SameSoldto;
						}

						if (so.ItemDetail != null && so.ItemDetail.Any())
						{
							dto.WithholdingTaxDescriptionFromSAP = so.ItemDetail[0].MaterialDesc;
							dto.ProductCostExcludeVAT = so.ItemDetail.Where(x => !x.IsMaterial45).Sum(x => x.UnitPriceExclVAT);
							dto.ProductCostIncludeVAT = so.ItemDetail.Where(x => !x.IsMaterial45).Sum(x => x.UnitPriceInclVAT);
							dto.ServiceCostExcludeVAT = so.ItemDetail.Where(x => x.IsMaterial45).Sum(x => x.UnitPriceExclVAT);
							dto.ServiceCostIncludeVAT = so.ItemDetail.Where(x => x.IsMaterial45).Sum(x => x.UnitPriceInclVAT);
						}

					}
					if (hana.HanaCustomer != null)
					{
						dto.CustomerPhoneNumber = hana.HanaCustomer.Telephone_SP;
					}
					else if (hana.OneTimeCustomer != null)
					{
						dto.CustomerPhoneNumber = hana.OneTimeCustomer.Telephone;
					}
					creditSales.Add(dto);
				}

				// filter by query builder 
				var dataAsQueryable = creditSales.AsQueryable();
				if (!string.IsNullOrEmpty(input.DynamicFilter))
				{
					var jsonSerializerSettings = new JsonSerializerSettings
					{
						ContractResolver =
							new CamelCasePropertyNamesContractResolver()
					};
					var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
					if (filRule?.Rules != null)
					{
						dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
					}
				}
				var totalCount = dataAsQueryable.Count();


				var pageBy = dataAsQueryable.OrderBy(input.Sorting).PageBy(input).ToList();
				result = new PagedResultOutput<GetCreditSalesOutput>()
				{
					Items = pageBy,
					TotalCount = totalCount
				};

				return result;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		private static HanaData GetHANAData(string ticket)
		{
			if (ticket == null || string.IsNullOrEmpty(ticket)) return null;
			return JsonConvert.DeserializeObject<HanaData>(ticket);
		}
	

		private static void GetAddress(ref string current, string next, string concat = " - ")
		{
			if (string.IsNullOrEmpty(current))
				current = next;
			else if (!string.IsNullOrEmpty(next))
				current = $"{current}{concat}{next}";
		}

		private static string GetBillStatus(string key)
		{
			if (ConnectConsts.ConnectConsts.BillStatuses.ContainsKey(key))
				return ConnectConsts.ConnectConsts.BillStatuses[key];
			return "";
		}
		public static string GetRefInvoiceNo(string cat)
		{
			if (cat == "K" || cat == "L" || cat == "H")
				return "";
			return "";
		}
		public IQueryable<Transaction.Ticket> GetCreditSalesForCondition(GetCreditSalesInput input)
		{
			IQueryable<Transaction.Ticket> output;

			var correctDate = CorrectInputDate(input);
			using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
			{
				output = _ticketManager.GetAll();
				if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
				{
					if (correctDate)
					{
						output =
							output.Where(
								a =>
									a.LastPaymentTime >=
									input.StartDate
									&&
									a.LastPaymentTime <=
									input.EndDate);
					}
					else
					{
						var mySt = input.StartDate.Date;
						var myEn = input.EndDate.Date;

						output =
							output.Where(
								a =>
									a.LastPaymentTime >= mySt
									&&
									a.LastPaymentTime <= myEn);
					}
				}
			}

			if (input.Location > 0)
			{
				output = output.Where(a => a.LocationId == input.Location);
			}
			else if (input.Locations != null && input.Locations.Any())
			{
				var locations = input.Locations.Select(a => a.Id).ToList();
				output = output.Where(a => locations.Contains(a.LocationId));
			}
			else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
					 && !input.LocationGroup.Locations.Any())
			{
				var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
				{
					Locations = input.LocationGroup.Locations,
					Group = false
				});
				if (locations.Any()) output = output.Where(a => locations.Contains(a.LocationId));
			}
			else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
			{
				var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
				{
					Locations = input.LocationGroup.Locations,
					Group = false
				});
				output = output.Where(a => locations.Contains(a.LocationId));
			}
			else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
			{
				var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
				{
					Groups = input.LocationGroup.Groups,
					Group = true
				});
				output = output.Where(a => locations.Contains(a.LocationId));
			}
			if (input.LocationGroup != null)
			{
				if (input.LocationGroup.NonLocations != null && input.LocationGroup.NonLocations.Any())
				{
					var nonlocations = input.LocationGroup.NonLocations.Select(a => a.Id).ToList();
					output = output.Where(a => !nonlocations.Contains(a.LocationId));
				}
			}

			else if (input.LocationGroup == null)
			{
				var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
				{
					Locations = new List<SimpleLocationDto>(),
					Group = false,
					UserId = input.UserId
				});
				if (input.UserId > 0)
					output = output.Where(a => locations.Contains(a.LocationId));
			}
			return output;
		}

		public async Task<FileDto> GetCreditSalesToExcel(GetCreditSalesInput input)
		{
			if (AbpSession.UserId != null && AbpSession.TenantId != null)
			{
				var setting = await _tenantSettingsService.GetAllSettings();
				input.DatetimeFormat = setting.Connect.DateTimeFormat;
				input.DateFormat = setting.Connect.SimpleDateFormat;

				input.MaxResultCount = 10000;

				if (input.RunInBackground)
				{
					var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
					{
						ReportName = ReportNames.CREDITSALES,
						Completed = false,
						UserId = AbpSession.UserId.Value,
						TenantId = AbpSession.TenantId.Value,
						ReportDescription = input.ReportDescription
					});

					if (backGroundId > 0)
					{
						var tInput = new CreditSalesReportInput
						{
							CreditSalesInput = input,
						};
						await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
						{
							BackGroundId = backGroundId,
							ReportName = ReportNames.CREDITSALES,
							CreditSalesReportInput = tInput,
							UserId = AbpSession.UserId.Value,
							TenantId = AbpSession.TenantId.Value
						});
					}
				}
				else
				{
					var output = await _exporter.ExportCreditSales(input, this);
					return output;
				}
			}

			return null;
		}
		public bool CorrectInputDate(IDateInput input)
		{
			if (input.NotCorrectDate) return true;
			var returnStatus = true;

			var operateHours = 0M;

			if (input.Locations != null && input.Locations.Any() && input.Locations.Count() == 1)
				operateHours = _lRepo.Get(input.Locations.First().Id).ExtendedBusinessHours;

			if (operateHours == 0M && input.Location > 0)
				operateHours = _lRepo.Get(input.Location).ExtendedBusinessHours;
			if (operateHours == 0M && input.LocationGroup != null && input.LocationGroup.Locations.Any() &&
				input.LocationGroup.Locations.Count() == 1)
				operateHours = _lRepo.Get(input.LocationGroup.Locations.First().Id).ExtendedBusinessHours;
			if (operateHours == 0M)
				operateHours = SettingManager.GetSettingValue<decimal>(AppSettings.ConnectSettings.OperateHours);

			if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
			{
				if (operateHours == 0M) returnStatus = false;
				input.EndDate = input.EndDate.AddDays(1);
				input.StartDate = input.StartDate.AddHours(Convert.ToDouble(operateHours));
				input.EndDate = input.EndDate.AddHours(Convert.ToDouble(operateHours)).AddMinutes(-1);
			}
			else
			{
				returnStatus = false;
			}

			return returnStatus;
		}


	}
}
