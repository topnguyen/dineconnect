﻿
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.CreditSalesReport.Dto;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.CreditSalesReport
{
	public interface ICreditSalesReportAppService : IApplicationService
	{
		Task<PagedResultOutput<GetCreditSalesOutput>> GetCreditSalesReport(GetCreditSalesInput input);
		Task<FileDto> GetCreditSalesToExcel(GetCreditSalesInput input);

	}
}
