﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.CreditSalesReport.Dto
{
    public class GetCreditSalesOutput : AuditedEntity
    {
        public string Plant { get; set; }
        public string PlantDescription { get; set; }
        public DateTime DateTime { get; set; }
        public string SalesOrderNo { get; set; }
        public string BillStatus { get; set; }
        public string BillNumber { get; set; }
        public string FullTaxInvoiceNumber { get; set; }
        public string RefInvoiceNo { get; set; }
        public string CashierName { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public string TaxIdenticationNumber { get; set; }
        public string CreditTerm { get; set; }
        public string CustomerPhoneNumber { get; set; }
        public string RecipientName { get; set; }
        public string SaleMode { get; set; }
        public string SaleChannel { get; set; }
        public decimal ProductCostExcludeVAT { get; set; }
        public decimal ProductCostIncludeVAT { get; set; }
        public decimal ServiceCostExcludeVAT { get; set; }
        public decimal ServiceCostIncludeVAT { get; set; }
        public decimal TotalCostIncludeVAT => ProductCostIncludeVAT + ServiceCostIncludeVAT;
        public string WithholdingTaxDescriptionFromSAP { get; set; }
        public decimal WithholdingTaxAmountFromSAP { get; set; }
        public string Note { get; set; }
        public string TicketLogs { get; set; }
    }
}
