﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.CreditSalesReport.Dto
{
	public class HanaData
	{
		public HanaCustomerInfo HanaCustomer { get; set; }
		public OneTimeCustomer OneTimeCustomer { get; set; }
		public S4HanaSaleOrderResponse2 S4HanaSaleOrder { get; set; }
		public Wht Wht { get; set; }
	}
    public class HanaCustomerInfo
    {
        public HanaCustomerInfo()
        {
            ContactSoldInfo = new HanaCustomerSoldInfo();
            ShipToInfo = new List<HanaCustomerShipToInfo>();
        }
        public string CompanyCode { get; set; }
        public string CustomerNo { get; set; }
        public string CustomerName { get; set; }
        public string TaxID { get; set; }
        public string BranchCode_SP { get; set; }
        public string WHTCode { get; set; }
        public decimal WHTPercent { get; set; }
        public string Street_SP { get; set; }
        public string Street2_SP { get; set; }
        public string Street3_SP { get; set; }
        public string Street4_SP { get; set; }
        public string Street5_SP { get; set; }
        public string District_SP { get; set; }
        public string DifferenceCity_SP { get; set; }
        public string City_SP { get; set; }
        public string CountryKey_SP { get; set; }
        public string CountryDesc_SP { get; set; }
        public string PostalCode_SP { get; set; }
        public string Telephone_SP { get; set; }

        public HanaCustomerSoldInfo ContactSoldInfo { get; set; }
        public List<HanaCustomerShipToInfo> ShipToInfo { get; set; }
    }
    public class HanaCustomerSoldInfo
    {
        public string ContactIDCard_SP { get; set; }
        public string ContactName_SP { get; set; }
        public string ContactNo_SP { get; set; }
    }
    public class HanaCustomerShipToInfo
    {
        public string ShipToParty_SH { get; set; }
        public string BranchCode_SH { get; set; }
        public string Street_SH { get; set; }
        public string Street2_SH { get; set; }
        public string Street3_SH { get; set; }
        public string Street4_SH { get; set; }
        public string Street5_SH { get; set; }
        public string District_SH { get; set; }
        public string DifferenceCity_SH { get; set; }
        public string City_SH { get; set; }
        public string CountryKey_SH { get; set; }
        public string PostalCode_SH { get; set; }
        public string Telephone_SH { get; set; }
    }
    public class OneTimeCustomer
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Telephone { get; set; }
        public string TaxId { get; set; }
    }
    public class S4HanaSaleOrderResponse2 : S4HanaSaleOrderResponseBase
    {
        public IList<ItemDetail> ItemDetail { get; set; }
    }
    public abstract class S4HanaSaleOrderResponseBase
    {
        public string SalesOrderNo { get; set; }
        public string STATUS { get; set; }
        public string MESSAGE { get; set; }
        public SoldDetail SoldDetail { get; set; }
        public ShipDetail ShipDetail { get; set; }
        public HeaderDetail HeaderDetail { get; set; }
    }
    public class SoldDetail
    {
        public string CustomerNo { get; set; }
        public string CustomerName { get; set; }
        public string TaxID { get; set; }
        public string Street_SP { get; set; }
        public string Street2_SP { get; set; }
        public string Street3_SP { get; set; }
        public string Street4_SP { get; set; }
        public string Street5_SP { get; set; }
        public string District_SP { get; set; }
        public string DifferenceCity_SP { get; set; }
        public string City_SP { get; set; }
        public int PostalCode_SP { get; set; }
        public string BranchCode { get; set; }
    }
    public class ShipDetail
    {
        public string SameSoldto { get; set; }
        public string ShipToNo { get; set; }
        public string Street_SH { get; set; }
        public string Street2_SH { get; set; }
        public string Street3_SH { get; set; }
        public string Street4_SH { get; set; }
        public string Street5_SH { get; set; }
        public string District_SH { get; set; }
        public string DifferenceCity_SH { get; set; }
        public string City_SH { get; set; }
        public int PostalCode_SH { get; set; }
    }
    public class HeaderDetail
    {
        public string DocCat { get; set; }
        public string CreditTermCode { get; set; }
        public string CreditTermDesc { get; set; }
        public decimal TotalPriceExclVAT { get; set; }
        public decimal TotalPriceInclVAT { get; set; }
        public decimal TotalVAT { get; set; }
        public string DocCurr { get; set; }
    }

    public class ItemDetail
    {
        public string LineNo { get; set; }
        public string MaterialCode { get; set; }
        public string MaterialDesc { get; set; }
        public int Quantity { get; set; }
        public string SalesUOM { get; set; }
        public decimal UnitPriceExclVAT { get; set; }
        public decimal UnitPriceInclVAT { get; set; }
        public decimal ItemTotalVAT { get; set; }
        public decimal ItemTotalPrice { get; set; }
        public bool IsMaterial45
		{
            get
			{
                var result = false;
				if (!string.IsNullOrEmpty(MaterialCode))
				{
                    var input = MaterialCode.Substring(0, 2);
                    if (input == "45")
					{
                        result = true;
                    }
                }
                return result;
            }
        }
    }
    public class Wht
    {
        public Wht()
        {
            Date = DateTime.Now;
        }

        public string Certificate { get; set; }
        public DateTime Date { get; set; }
        public string Remark { get; set; }
    }

}
