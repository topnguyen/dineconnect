﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities.Auditing;
using Abp.Extensions;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Discount.Dtos;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter;
using OpenHtmlToPdf;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.CreditSalesReport.Dto
{
    public class GetCreditSalesInput : PagedAndSortedInputDto, IFileExport, IDateInput, IBackgroundExport
    {
        public GetCreditSalesInput()
        {
            Locations = new List<SimpleLocationDto>();
            LocationGroup = new LocationGroupDto();
        }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Location { get; set; }
        public List<SimpleLocationDto> Locations { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public ExportType ExportOutputType { get; set; }
        public PaperSize PaperSize { get; set; }
        public bool Portrait { get; set; }
        public long UserId { get; set; }
        public bool NotCorrectDate { get; set; }
        public bool Credit { get; set; }
        public bool Refund { get; set; }
        public string TicketNo { get; set; }
        public bool RunInBackground { get; set; }
        public string ReportDescription { get; set; }
        public string DynamicFilter { get; set; }
        public string DateFormat { get; set; }
        public string DatetimeFormat { get; set; }

    }
}
