﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Connect.Departments;
using DinePlan.DineConnect.Connect.Departments.Dtos;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.ProgramSettings.Dtos;
using DinePlan.DineConnect.Connect.ProgramSettings.Exporting;
using DinePlan.DineConnect.Connect.Setting;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Dto;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Connect.ProgramSettings
{
    public class ProgramSettingTemplateAppService : DineConnectAppServiceBase, IProgramSettingTemplateAppService

    {
        private readonly ILocationAppService _locAppService;
        private readonly IProgramSettingTemplateExporter _programSettingTemplateExporter;

        private readonly IRepository<ProgramSettingTemplate> _programSettingTemplateRepository;
        private readonly IRepository<ProgramSettingValue> _programSettingValueRepo;
        private readonly IRepository<Master.Location> _locRepo;
        private readonly IRepository<Department> _departmentRepo;

        private readonly ISyncAppService _syncAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        private const string LMS = "LMS";
        private const string LMSPaymentDepartments = "LMSPaymentDepartments";
        private const string InternalSale = "InternalSale";
        private const string HanaSearch = "HanaSearch";
        private const string FullTax = "FullTax";
        private const string FTICreditSale = "FTICreditSale";
        private const string ECCCreditLimit = "ECCCreditLimit";
        private const string PettyCashControl = "PettyCashControl";
        private readonly List<string> pttOrSettingTemplateName = new List<string>();

        public ProgramSettingTemplateAppService(
            ILocationAppService locAppService ,
            IRepository<ProgramSettingTemplate> programSettingTemplateRepository ,
            IRepository<Master.Location> locRepo ,
            IProgramSettingTemplateExporter programSettingTemplateExporter ,
            ISyncAppService syncAppService ,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<ProgramSettingValue> programSettingValueRepo,
            IRepository<Department> departmentRepo
        )
        {
            _locAppService = locAppService;
            _programSettingTemplateRepository = programSettingTemplateRepository;
            _programSettingTemplateExporter = programSettingTemplateExporter;
            _syncAppService = syncAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _programSettingValueRepo = programSettingValueRepo;
            _locRepo = locRepo;
            _departmentRepo = departmentRepo;

            pttOrSettingTemplateName.Add(LMS);
            pttOrSettingTemplateName.Add(LMSPaymentDepartments);
            pttOrSettingTemplateName.Add(InternalSale);
            pttOrSettingTemplateName.Add(HanaSearch);
            pttOrSettingTemplateName.Add(FullTax);
            pttOrSettingTemplateName.Add(FTICreditSale);
            pttOrSettingTemplateName.Add(ECCCreditLimit);
            pttOrSettingTemplateName.Add(PettyCashControl);
        }

        public async Task<ListResultOutput<ApiProgramSettingValue>> ApiGetProgramSettings(ApiLocationInput input)
        {
            var loc = await  _locRepo.GetAsync(input.LocationId);
            if (loc != null)
            {
                CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", loc.CompanyRefId);
            }

            var deptOutput = new List<ApiProgramSettingValue>();

            var lstTemplates =
                await _programSettingTemplateRepository.GetAllListAsync(i => i.TenantId == loc.TenantId);


            foreach (var oneTemplate in lstTemplates)
            {
                var nullSettings =  _programSettingValueRepo
                    .GetAll().Where(a => a.ProgramSettingTemplateId == oneTemplate.Id && a.Locations == null);

                if (nullSettings.Any())
                {
                    var nullSetting = nullSettings.ToList().LastOrDefault();
                    if (nullSetting != null)
                    {
                        deptOutput.Add(new ApiProgramSettingValue
                        {
                            Name = oneTemplate.Name,
                            DefaultValue = nullSetting.ActualValue
                        });
                        continue;
                    }
                }


                var notNullLocations =  _programSettingValueRepo
                    .GetAll().Where(a => a.ProgramSettingTemplateId == oneTemplate.Id && (a.Locations != null || a.NonLocations != null));

                bool add = false;
                foreach (var myValue in notNullLocations)
                {
                    if (await _locAppService.IsLocationExists(new CheckLocationInput
                    {
                        LocationId = input.LocationId,
                        Locations = myValue.Locations,
                        Group = myValue.Group,
                        NonLocations = myValue.NonLocations,
                        LocationTag = myValue.LocationTag
                    }))
                    {
                        add = true;
                        deptOutput.Add(new ApiProgramSettingValue
                        {
                            Name = oneTemplate.Name,
                            DefaultValue = myValue.ActualValue
                        });
                        break;
                    }
                }

                if (add)
                {
                    continue;
                }
                deptOutput.Add(new ApiProgramSettingValue
                {
                    Name = oneTemplate.Name,
                    DefaultValue = oneTemplate.DefaultValue
                });

            }

            return new ListResultOutput<ApiProgramSettingValue>(deptOutput);
        }

        public async Task<Dictionary<string, int>> GetPttOrSettingsIds()
        {
            Dictionary<string, int> pttOrSettingIds = new Dictionary<string, int>();

            foreach (var templateName in pttOrSettingTemplateName)
            {
                var templateId = 0;

                var template = await _programSettingTemplateRepository.GetAll().Where(x => x.Name == templateName).FirstOrDefaultAsync();

                if (template == null)
                {
                    template = new ProgramSettingTemplate
                    {
                        Name = templateName,
                        DefaultValue = string.Empty
                    };

                    templateId = await _programSettingTemplateRepository.InsertAndGetIdAsync(template);
                }

                templateId = template.Id;

                pttOrSettingIds.Add(templateName, templateId);
            }

            return pttOrSettingIds;
        }

        public async Task<PagedResultOutput<ProgramSettingTemplateListDto>> GetAllTemplates(
            GetProgramSettingTemplateInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var lastSyncTime = input.TimeZone != null
                    ? TimeZoneInfo.ConvertTime(input.LastSyncTime, input.TimeZone, TimeZoneInfo.Local)
                    : input.LastSyncTime;
                var allItems = _programSettingTemplateRepository.GetAll()
                    .Where(i => 
                        !pttOrSettingTemplateName.Contains(i.Name) &&
                        i.IsDeleted == input.IsDeleted && (
                        i.LastModificationTime != null && i.LastModificationTime > lastSyncTime ||
                        i.LastModificationTime == null && i.CreationTime > lastSyncTime))
                    .WhereIf(!input.Filter.IsNullOrEmpty(),
                        p => p.Name.Contains(input.Filter) || p.DefaultValue.Contains(input.Filter));

                if (!input.Location.IsNullOrWhiteSpace())
                {
                    var programSettingValue = _programSettingValueRepo.GetAll().ToList()
                        .Where(i => i.IsDeleted == input.IsDeleted)
                        .Where(p =>
                        {
                            var locations =
                                JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(p.Locations);
                            return locations.Exists(x => x["Id"] == input.Location);
                        })
                        .ToList();
                    if (programSettingValue?.Count() > 0)
                    {
                        var programsettingtemplateIds =
                            programSettingValue.Select(t => t.ProgramSettingTemplateId).ToList();

                        var programsettingmap = allItems.ToDictionary(x => x.Id);
                        foreach (var id in programsettingtemplateIds)
                            if (programsettingmap.TryGetValue(id, out var found))
                            {
                                found.DefaultValue = programSettingValue
                                    .FirstOrDefault(x => x.ProgramSettingTemplateId == id)?.ActualValue;
                                break;
                            }
                    }
                }

                var sortMenuItems = await allItems.OrderBy(input.Sorting).PageBy(input).ToListAsync();

                var allListDtos = sortMenuItems.MapTo<List<ProgramSettingTemplateListDto>>();
                var totalCount = await allItems.CountAsync();

                return new PagedResultOutput<ProgramSettingTemplateListDto>(totalCount, allListDtos);
            }
        }

        public async Task<GetProgramSettingTemplateForEditOutput> GetProgramSettingTemplateForEdit(
            ProgramSettingTemplateInputIdWithFilter input)
        {
            var editDto = new ProgramSettingTemplateEditDto();

            if (input.Id.HasValue)
            {
                var filterLocation = new List<SimpleLocationDto>();
                var filterLocationGroup = new List<SimpleLocationGroupDto>();
                var filterLocationRefIds = new List<int>();
                var filterLocationGroupRefIds = new List<int>();
                var filterLocationTagRefIds = new List<int>();
                if (input.LocationGroupToBeFiltered != null)
                {
                    filterLocationRefIds = input.LocationGroupToBeFiltered.Locations.Select(t => t.Id).ToList();
                    filterLocationGroupRefIds = input.LocationGroupToBeFiltered.Groups.Select(t => t.Id).ToList();
                    filterLocationTagRefIds = input.LocationGroupToBeFiltered.LocationTags.Select(t => t.Id).ToList();
                }

                var tempProgramSettingTemplate = await _programSettingTemplateRepository.GetAll()
                    .FirstOrDefaultAsync(e => e.Id == input.Id.Value);
                if(tempProgramSettingTemplate != null)
                    editDto = tempProgramSettingTemplate.MapTo<ProgramSettingTemplateEditDto>();

                var tempProgramSettingValues =
                    await _programSettingValueRepo.GetAllListAsync(t => t.ProgramSettingTemplateId == input.Id.Value);
                var tempValuesFilter = tempProgramSettingValues.MapTo<Collection<ProgramSettingValueEditDto>>();
                var valuesOutput = new Collection<ProgramSettingValueEditDto>();
                foreach (var lst in tempValuesFilter)
                {
                    lst.ProgramSettingTemplateName = editDto.Name;
                    UpdateLocationAndNonLocationForEdit(lst, lst.LocationGroup);
                    if (filterLocationRefIds.Count > 0 || filterLocationGroupRefIds.Count > 0 ||
                        filterLocationTagRefIds.Count > 0)
                    {
                        if (lst.LocationGroup.Locations.Select(t => t.Id).Intersect(filterLocationRefIds).ToList()
                            .Count > 0)
                            valuesOutput.Add(lst);
                        else if (lst.LocationGroup.Groups.Select(t => t.Id).Intersect(filterLocationGroupRefIds)
                            .ToList().Count > 0)
                            valuesOutput.Add(lst);
                        else if (lst.LocationGroup.LocationTags.Select(t => t.Id).Intersect(filterLocationTagRefIds)
                            .ToList().Count > 0)
                            valuesOutput.Add(lst);
                    }
                    else
                    {
                        valuesOutput.Add(lst);
                    }
                }

                editDto.ProgramSettingValues = valuesOutput;
            }

            return new GetProgramSettingTemplateForEditOutput
            {
                ProgramSettingTemplate = editDto,
                ProgramSettingValueEditDto = new ProgramSettingValueEditDto()
            };
        }
        public async Task<ProgramSettingValueEditDto> GetDepartmentList(ProgramSettingValueEditDto dto)
        {
            try
            {
                var dInput = new GetDepartmentInput
                {
                    MaxResultCount = 999,
                    LocationGroup = dto.LocationGroup
                };
                dto.PaymentDepartments.Clear();

                var departments = await _departmentRepo.GetAllListAsync();
                if (string.IsNullOrEmpty(dto.ActualValue))
                {
                    if (dto.ProgramSettingTemplateName == LMSPaymentDepartments || dto.ProgramSettingTemplateName == HanaSearch)
                    {
                        GetDepartments(dto, departments);
                    }
                }
                else if (dto.ProgramSettingTemplateName == LMSPaymentDepartments)
                {
                    var lms = JsonConvert.DeserializeObject<LmsPaymentSetting>(dto.ActualValue);
                    if (lms == null)
                    {
                        GetDepartments(dto, departments);
                    }
                    else
                    {
                        if (lms.lmsPaymentDepartments == null)
                        {
                            GetDepartments(dto, departments);
                        }
                        else
                        {
                            var entriesInt = new List<int>();
                            var entries = lms.lmsPaymentDepartments.ToString().Split(",").ToList();
                            foreach (var entry in entries)
                            {
                                if (int.TryParse(entry, out int val))
                                {
                                    if (val > 0)
                                        entriesInt.Add(val);
                                }
                            }

                            GetDepartments(dto, departments, entriesInt);
                        }
                    }
                }
                else if(dto.ProgramSettingTemplateName == HanaSearch)
                {
                    var hana = JsonConvert.DeserializeObject<HanaSetting>(dto.ActualValue);
                    if (hana == null)
                    {
                        GetDepartments(dto, departments);
                    }
                    else
                    {
                        if (hana.hanaDepartments == null)
                        {
                            GetDepartments(dto, departments);
                        }
                        else
                        {
                            var entriesInt = new List<int>();
                            var entries = hana.hanaDepartments.ToString().Split(",").ToList();
                            foreach (var entry in entries)
                            {
                                if (int.TryParse(entry, out int val))
                                {
                                    if (val > 0)
                                        entriesInt.Add(val);
                                }
                            }
                            GetDepartments(dto, departments, entriesInt);
                        }
                    }
                }

                return dto;
            }
            catch (Exception ex)
            {
                return dto;
            }
            
        }

        private void GetDepartments(ProgramSettingValueEditDto dto, IList<Department> departments, List<int> ids = null)
        {
            foreach (var d in departments)
            {
                dto.PaymentDepartments.Add(new DepartmentSelectDto
                {
                    Id = d.Id,
                    UIId = "PM" + d.Id,
                    Name = d.Name,
                    IsSelected = ids != null && ids.Contains(d.Id)
                });
            }
        }

        public async Task<int> CreateOrUpdateProgramSettingTemplate(CreateOrUpdateProgramSettingTemplateInput input)
        {
            ValidateForCreateOrUpdate(input.ProgramSettingTemplate);

            try
            {
                if(input.ProgramSettingTemplate.Name == LMSPaymentDepartments)
                {
                    var lms = JsonConvert.DeserializeObject<LmsPaymentSetting>(input.ProgramSettingTemplate.DefaultValue);
                    if (lms != null && input.ProgramSettingTemplate.PaymentDepartments != null)
                    {
                        lms.lmsPaymentDepartments = string.Join(",", input.ProgramSettingTemplate.PaymentDepartments.Where(x => x.IsSelected).Select(x => x.Id).ToArray());
                        input.ProgramSettingTemplate.DefaultValue = JsonConvert.SerializeObject(lms);
                    }
                }
                else if(input.ProgramSettingTemplate.Name == HanaSearch)
                {
                    var lms = JsonConvert.DeserializeObject<HanaSetting>(input.ProgramSettingTemplate.DefaultValue);
                    if (lms != null && input.ProgramSettingTemplate.PaymentDepartments != null)
                    {
                        lms.hanaDepartments = string.Join(",", input.ProgramSettingTemplate.PaymentDepartments.Where(x => x.IsSelected).Select(x => x.Id).ToArray());
                        input.ProgramSettingTemplate.DefaultValue = JsonConvert.SerializeObject(lms);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Cannot update Setting");
            }

            await _syncAppService.UpdateSync(SyncConsts.PROGRAMSETTINGS);

            if (input.ProgramSettingTemplate.Id.HasValue)
            {
                await UpdateProgramSettingTemplate(input);
                return input.ProgramSettingTemplate.Id.Value;
            }

            return await CreateProgramSettingTemplate(input);
        }

        public async Task DeleteProgramSettingTemplate(IdInput input)
        {
            await _programSettingValueRepo.DeleteAsync(t => t.ProgramSettingTemplateId == input.Id);
            await _programSettingTemplateRepository.DeleteAsync(input.Id);
        }

        public async Task<IdInput> AddOrEditProgramSettingValues(CreateProgramSettingValue input)
        {
            try
            {
                if(input.ProgramSettingValueEditDto.ProgramSettingTemplateName == LMSPaymentDepartments)
                {
                    var lms = JsonConvert.DeserializeObject<LmsPaymentSetting>(input.ProgramSettingValueEditDto.ActualValue);
                    if (lms != null)
                    {
                        lms.lmsPaymentDepartments = string.Join(",", input.ProgramSettingValueEditDto.PaymentDepartments.Where(x => x.IsSelected).Select(x => x.Id).ToArray());
                        input.ProgramSettingValueEditDto.ActualValue = JsonConvert.SerializeObject(lms);
                    }
                }
                else if(input.ProgramSettingValueEditDto.ProgramSettingTemplateName == HanaSearch)
                {
                    var lms = JsonConvert.DeserializeObject<HanaSetting>(input.ProgramSettingValueEditDto.ActualValue);
                    if (lms != null)
                    {
                        lms.hanaDepartments = string.Join(",", input.ProgramSettingValueEditDto.PaymentDepartments.Where(x => x.IsSelected).Select(x => x.Id).ToArray());
                        input.ProgramSettingValueEditDto.ActualValue = JsonConvert.SerializeObject(lms);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Cannot update Setting");
            }

            await BusinessRulesForProgramSettingTemplate(input);
            var programSettingValue = input.ProgramSettingValueEditDto;
            if (programSettingValue.Id.HasValue)
            {
                var existEntry =
                    await _programSettingValueRepo.FirstOrDefaultAsync(t => t.Id == programSettingValue.Id.Value);
                existEntry.ActualValue = programSettingValue.ActualValue;
                UpdateLocationAndNonLocation(existEntry, programSettingValue.LocationGroup);
                await _programSettingValueRepo.UpdateAsync(existEntry);
                return new IdInput {Id = existEntry.Id};
            }

            var dto = programSettingValue.MapTo<ProgramSettingValue>();
            UpdateLocationAndNonLocation(dto, programSettingValue.LocationGroup);
            await _programSettingValueRepo.InsertOrUpdateAndGetIdAsync(dto);
            return new IdInput {Id = dto.Id};
        }

        public async Task DeleteProgramSettingValues(IdInput input)
        {
            await _programSettingValueRepo.DeleteAsync(t => t.Id == input.Id);
        }


        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _programSettingTemplateRepository.GetAll().Where(i => !pttOrSettingTemplateName.Contains(i.Name)).OrderBy(a => a.Id).ToListAsync();

            return _programSettingTemplateExporter.ExportToFile(allList.MapTo<List<ProgramSettingTemplateListDto>>());
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _programSettingTemplateRepository.GetAsync(input.Id);
                item.IsDeleted = false;

                await _programSettingTemplateRepository.UpdateAsync(item);
            }
        }

        private async Task<int> CreateProgramSettingTemplate(CreateOrUpdateProgramSettingTemplateInput input)
        {
            var template = input.ProgramSettingTemplate.MapTo<ProgramSettingTemplate>();

            var retId = await _programSettingTemplateRepository.InsertAndGetIdAsync(template);

            return retId;
        }

        private async Task UpdateProgramSettingTemplate(CreateOrUpdateProgramSettingTemplateInput input)
        {
            var dto = input.ProgramSettingTemplate;
            var template = await _programSettingTemplateRepository.GetAsync(input.ProgramSettingTemplate.Id.Value);

            dto.MapTo(template);

            await _programSettingTemplateRepository.UpdateAsync(template);
        }

        public async Task BusinessRulesForProgramSettingTemplate(CreateProgramSettingValue input)
        {
            var newLocationToBeAdded = new List<SimpleLocationDto>();
            var newLocationGroupToBeAdded = new List<SimpleLocationGroupDto>();
            var newLocationTagToBeAdded = new List<SimpleLocationTagDto>();
            var locationGroupDto = input.ProgramSettingValueEditDto.LocationGroup;
            if (locationGroupDto.Locations.Count > 0) newLocationToBeAdded.AddRange(locationGroupDto.Locations);
            if (locationGroupDto.Groups.Count > 0) newLocationGroupToBeAdded.AddRange(locationGroupDto.Groups);
            if (locationGroupDto.LocationTags.Count > 0)
                newLocationTagToBeAdded.AddRange(locationGroupDto.LocationTags);

            var iQPv = _programSettingValueRepo.GetAll().Where(t =>
                t.ProgramSettingTemplateId == input.ProgramSettingValueEditDto.ProgramSettingTemplateId);
            if (input.ProgramSettingValueEditDto.Id.HasValue)
                iQPv = iQPv.Where(t => t.Id != input.ProgramSettingValueEditDto.Id);
            var otherProgramValues = await iQPv.ToListAsync();
            var programSettingValuesDtos = otherProgramValues.MapTo<Collection<ProgramSettingValueEditDto>>();
            var locationAlreadyMapped = new List<SimpleLocationDto>();
            var locationGroupAlreadyMapped = new List<SimpleLocationGroupDto>();
            var locationTagAlreadyMapped = new List<SimpleLocationTagDto>();
            foreach (var lst in programSettingValuesDtos)
            {
                UpdateLocationAndNonLocationForEdit(lst, lst.LocationGroup);
                if (lst.LocationGroup.Locations.Count > 0)
                    locationAlreadyMapped.AddRange(lst.LocationGroup.Locations);
                if (lst.LocationGroup.Groups.Count > 0)
                    locationGroupAlreadyMapped.AddRange(lst.LocationGroup.Groups);
                if (lst.LocationGroup.LocationTags.Count > 0)
                    locationTagAlreadyMapped.AddRange(lst.LocationGroup.LocationTags);
                if (lst.ActualValue == input.ProgramSettingValueEditDto.ActualValue)
                    throw new UserFriendlyException("The Same Value " + lst.ActualValue +
                                                    " Already exists in another record");
            }

            if (locationAlreadyMapped.Count > 0)
            {
                var intersectLocations = locationAlreadyMapped.Select(t => t.Id).ToList()
                    .Intersect(newLocationToBeAdded.Select(t => t.Id).ToList()).ToList();
                if (intersectLocations.Count > 0)
                {
                    var locationAlreadyExists =
                        locationAlreadyMapped.FindAll(t => intersectLocations.Contains(t.Id)).ToList();
                    var locationAlreadExists = string.Join(",", locationAlreadyExists.Select(t => t.Name));
                    throw new UserFriendlyException("This Location(s) :" + locationAlreadExists +
                                                    " Already mapped with Another Values");
                }
            }

            if (locationGroupAlreadyMapped.Count > 0)
            {
                var intersectLocationGroups = locationGroupAlreadyMapped.Select(t => t.Id).ToList()
                    .Intersect(newLocationGroupToBeAdded.Select(t => t.Id).ToList()).ToList();
                if (intersectLocationGroups.Count > 0)
                {
                    var locationGroupAlreadyExists = locationGroupAlreadyMapped
                        .FindAll(t => intersectLocationGroups.Contains(t.Id)).ToList();
                    var locationgroupAlreadyExists = string.Join(",", locationGroupAlreadyExists.Select(t => t.Name));
                    throw new UserFriendlyException("This Location Group(s) :" + locationgroupAlreadyExists +
                                                    " Already mapped with Another Values");
                }
            }

            if (locationTagAlreadyMapped.Count > 0)
            {
                var intersectLocationTags = locationTagAlreadyMapped.Select(t => t.Id).ToList()
                    .Intersect(newLocationTagToBeAdded.Select(t => t.Id).ToList()).ToList();
                if (intersectLocationTags.Count > 0)
                {
                    var locationTagAlreadyExists = locationGroupAlreadyMapped
                        .FindAll(t => intersectLocationTags.Contains(t.Id)).ToList();
                    var locationtagAlreadyExists = string.Join(",", locationTagAlreadyExists.Select(t => t.Name));
                    throw new UserFriendlyException("This Location Tag(s) :" + locationtagAlreadyExists +
                                                    " Already mapped with Another Values");
                }
            }
        }

        private void ValidateForCreateOrUpdate(ProgramSettingTemplateEditDto input)
        {
            var exists = _programSettingTemplateRepository.GetAll()
                .WhereIf(input.Id.HasValue, m => m.Id != input.Id);

            if (exists.Where(m => m.Name.Equals(input.Name)).Any())
                throw new UserFriendlyException(L("NameAlreadyExists", input.Name));
        }

		public async Task<ProgramSettingTemplateEditDto> GetProgramSettingByName(string input)
		{
            var program = await _programSettingTemplateRepository.FirstOrDefaultAsync(x => x.Name.ToUpper().Equals(input.ToUpper()));
            var result = program.MapTo<ProgramSettingTemplateEditDto>();
            return result;
        }
	}
}