﻿using DinePlan.DineConnect.Connect.ProgramSettings.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.ProgramSettings.Exporting
{
    public class ProgramSettingTemplateExporter : FileExporterBase, IProgramSettingTemplateExporter
    {
        public FileDto ExportToFile(List<ProgramSettingTemplateListDto> dtos)
        {
            return CreateExcelPackage(
                "ProgramSettingList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(base.L("ProgramSetting"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        base.L("Id"),
                        base.L("Name"),
                        base.L("DefaultValue"),
                        base.L("CreationTime")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.Name,
                         _ => _.DefaultValue,
                        _ => _.CreationTime
                        );

                    var creationTimeColumn = sheet.Column(4);
                    creationTimeColumn.Style.Numberformat.Format = "yyyy-mm-dd hh:mm";

                    for (var i = 1; i <= 5; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}