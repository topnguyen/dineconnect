﻿using DinePlan.DineConnect.Connect.ProgramSettings.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.ProgramSettings.Exporting
{
    public interface IProgramSettingTemplateExporter
    {
        FileDto ExportToFile(List<ProgramSettingTemplateListDto> dtos);
    }
}