﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.ProgramSettings.Dtos;
using DinePlan.DineConnect.Connect.ProgramSettings.Exporting;
using DinePlan.DineConnect.Connect.Setting;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ProgramSettings
{
    public class ProgramSettingAppService : DineConnectAppServiceBase, IProgramSettingAppService
    {
    //    private readonly IRepository<ProgramSettingTemplate> _programSettingTemplateRepository;

    //    private readonly IProgramSettingExporter _programSettingExporter;
    //    private readonly IUnitOfWorkManager _unitOfWorkManager;

    //    public ProgramSettingAppService(IRepository<ProgramSettingTemplate> programSettingTemplateRepository
    //        , IProgramSettingExporter programSettingExporter
    //        , IUnitOfWorkManager unitOfWorkManager
    //        )
    //    {
    //        _programSettingTemplateRepository = programSettingTemplateRepository;
    //        _programSettingExporter = programSettingExporter;
    //        _unitOfWorkManager = unitOfWorkManager;
    //    }

    //    public async Task<PagedResultOutput<ProgramSettingListDto>> GetProgramSettings(GetProgramSettingInput input)
    //    {
    //        //using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
    //        //{
    //        //    if (!input.Filter.IsNullOrEmpty())
    //        //    {
    //        //        input.Filter = Regex.Replace(input.Filter.Trim(), @"\s+", " ");
    //        //    }

    //        //    var query = _programSettingRepository
    //        //        .GetAll()
    //        //        .Include(e => e.ProgramSettingTemplate);
    //        //       //.Where(i => i.IsDeleted == input.IsDeleted)
    //        //       //.WhereIf(!input.Filter.IsNullOrEmpty(), p => p.ProgramSettingTemplate.Name.Contains(input.Filter) || p.ActualValue.Contains(input.Filter))
    //        //       //.WhereIf(!input.Location.IsNullOrEmpty(), p => p.Locations.Contains(input.Location));

    //        //    var result = await query
    //        //       .OrderBy(input.Sorting)
    //        //       .PageBy(input)
    //        //       .ToListAsync();

    //        //    return new PagedResultOutput<ProgramSettingListDto>(
    //        //        await query.CountAsync(),
    //        //        result.MapTo<List<ProgramSettingListDto>>()
    //        //        );
    //        //}

    //        return null;
    //    }

    //    public async Task<FileDto> GetAllToExcel()
    //    {
    //        //var allList = await _programSettingRepository.GetAll().OrderBy(a => a.Id).ToListAsync();

    //        //return _programSettingExporter.ExportToFile(allList.MapTo<List<ProgramSettingListDto>>());
    //        return null;
    //    }

    //    public async Task<CreateOrUpdateProgramSettingInput> GetProgramSettingForEdit(NullableIdInput input)
    //    {
    //        var output = new CreateOrUpdateProgramSettingInput();

    //        //var editDto = new ProgramSettingEditDto();

    //        //if (input.Id.HasValue)
    //        //{
    //        //    var message = await _programSettingRepository.GetAsync(input.Id.Value);
    //        //    editDto = message.MapTo<ProgramSettingEditDto>();
    //        //}
    //        //output.ProgramSetting = editDto;
    //        //UpdateLocationAndNonLocationForEdit(editDto, output.LocationGroup);

    //        return output;
    //    }

    //    public async Task<int> CreateOrUpdateProgramSetting(CreateOrUpdateProgramSettingInput input)
    //    {
    //        var output = 0;

    //        if (input.ProgramSetting.Id.HasValue)
    //        {
    //            await UpdateProgramSetting(input);
    //            output = input.ProgramSetting.Id.Value;
    //        }
    //        else
    //        {
    //            output = await CreateProgramSetting(input);
    //        }
    //        return output;
    //    }

    //    public async Task DeleteProgramSetting(NullableIdInput input)
    //    {
    //        //if (input.Id.HasValue)
    //        //    await _programSettingRepository.DeleteAsync(input.Id.Value);
    //    }

    //    private async Task<int> CreateProgramSetting(CreateOrUpdateProgramSettingInput input)
    //    {
    //        //var programSetting = input.ProgramSetting.MapTo<ProgramSetting>();

    //        ////UpdateLocationAndNonLocation(programSetting, input.LocationGroup);
    //        //var item = await _programSettingRepository.InsertOrUpdateAndGetIdAsync(programSetting);
    //        //return item;
    //        return 1;
    //    }

    //    private async Task UpdateProgramSetting(CreateOrUpdateProgramSettingInput input)
    //    {
    //        //var dto = input.ProgramSetting;
    //        //var item = await _programSettingRepository.GetAsync(input.ProgramSetting.Id.Value);

    //        //dto.MapTo(item);

    //        ////UpdateLocationAndNonLocation(item, input.LocationGroup);

    //        //await _programSettingRepository.UpdateAsync(item);
    //    }

    //    public async Task<ListResultDto<ComboboxItemDto>> GetProgramSettingTemplates()
    //    {
    //        var returnList = await _programSettingTemplateRepository.GetAll()
    //            .Select(e => new ComboboxItemDto
    //            {
    //                Value = e.Id.ToString(),
    //                DisplayText = e.Name
    //            })
    //            .ToListAsync();
    //        return new ListResultOutput<ComboboxItemDto>(returnList);
    //    }

    //    public async Task ActivateItem(IdInput input)
    //    {
    //        //using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
    //        //{
    //        //    var item = await _programSettingRepository.GetAsync(input.Id);
    //        //    item.IsDeleted = false;

    //        //    await _programSettingRepository.UpdateAsync(item);
    //        //}
    //    }
    }
}