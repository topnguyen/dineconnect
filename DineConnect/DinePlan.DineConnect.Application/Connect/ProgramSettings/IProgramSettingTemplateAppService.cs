﻿using System.Collections.Generic;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.ProgramSettings.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.Dtos;

namespace DinePlan.DineConnect.Connect.ProgramSettings
{
    public interface IProgramSettingTemplateAppService : IApplicationService
    {
        Task<int> CreateOrUpdateProgramSettingTemplate(CreateOrUpdateProgramSettingTemplateInput input);

        Task DeleteProgramSettingTemplate(IdInput input);

        Task<PagedResultOutput<ProgramSettingTemplateListDto>> GetAllTemplates(GetProgramSettingTemplateInput input);

        Task<FileDto> GetAllToExcel();

        Task<GetProgramSettingTemplateForEditOutput> GetProgramSettingTemplateForEdit(ProgramSettingTemplateInputIdWithFilter input);
        Task<IdInput> AddOrEditProgramSettingValues(CreateProgramSettingValue input);
        Task DeleteProgramSettingValues(IdInput input);
        Task ActivateItem(IdInput input);

        Task<ListResultOutput<ApiProgramSettingValue>> ApiGetProgramSettings(ApiLocationInput input);

        Task<Dictionary<string, int>> GetPttOrSettingsIds();
        Task<ProgramSettingTemplateEditDto> GetProgramSettingByName(string input);
        Task<ProgramSettingValueEditDto> GetDepartmentList(ProgramSettingValueEditDto dto);
    }
}