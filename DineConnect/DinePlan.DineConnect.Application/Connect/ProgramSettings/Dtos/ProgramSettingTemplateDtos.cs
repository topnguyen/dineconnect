﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Departments.Dtos;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Setting;
using DinePlan.DineConnect.Dto;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Connect.ProgramSettings.Dtos
{
    //[AutoMapFrom(typeof(ProgramSetting))]
    //public class ProgramSettingListDto
    //{
    //    public virtual int ProgramSettingTemplateId { get; set; }
    //}


    [AutoMapTo(typeof(ProgramSettingValue))]
    public class ProgramSettingValueListDto
    {
        public int Id { get; set; }
        public virtual int ProgramSettingTemplateId { get; set; }
        public virtual string ActualValue { get; set; }

        public LocationGroupDto LocationGroup { get; set; }
    }

    [AutoMapTo(typeof(ProgramSettingValue))]
    public class ProgramSettingValueEditDto : ConnectEditDto
    {
        public ProgramSettingValueEditDto()
        {
            LocationGroup = new LocationGroupDto();
            PaymentDepartments = new Collection<DepartmentSelectDto>();
        }

        public int? Id { get; set; }
        public int ProgramSettingTemplateId { get; set; }
        public string ProgramSettingTemplateName { get; set; }
        public string ActualValue { get; set; }

        public Collection<DepartmentSelectDto> PaymentDepartments { get; set; }

        public LocationGroupDto LocationGroup { get; set; }
    }

    [AutoMapFrom(typeof(ProgramSettingTemplate))]
    public class ProgramSettingTemplateListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
        public string DefaultValue { get; set; }
    }
    public class ApiProgramSettingValue 
    {
        public string Name { get; set; }
        public string DefaultValue { get; set; }
    }
    [AutoMapTo(typeof(ProgramSettingTemplate))]
    public class ProgramSettingTemplateEditDto
    {
        public ProgramSettingTemplateEditDto()
        {
            ProgramSettingValues = new Collection<ProgramSettingValueEditDto>();
        }

        public int? Id { get; set; }
        public string Name { get; set; }
        public string DefaultValue { get; set; }
        public Collection<DepartmentSelectDto> PaymentDepartments { get; set; }
        public virtual Collection<ProgramSettingValueEditDto> ProgramSettingValues { get; set; }
    }

    public class GetProgramSettingTemplateInput : PagedAndSortedInputDto, ISyncTimeDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public bool IsDeleted { get; set; }
        public string Location { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting)) Sorting = "Id Desc";
        }

        public DateTime LastSyncTime { get; set; }
        public TimeZoneInfo TimeZone { get; set; }
    }

    public class ProgramSettingTemplateInputIdWithFilter
    {
        public int? Id { get; set; }
        public LocationGroupDto LocationGroupToBeFiltered { get; set; }
    }


    public class GetProgramSettingTemplateForEditOutput : IOutputDto
    {
        public ProgramSettingTemplateEditDto ProgramSettingTemplate { get; set; }

        public ProgramSettingValueEditDto ProgramSettingValueEditDto { get; set; }
    }

    public class CreateOrUpdateProgramSettingTemplateInput : IInputDto
    {
        [Required] public ProgramSettingTemplateEditDto ProgramSettingTemplate { get; set; }
    }

    public class CreateProgramSettingValue : IInputDto
    {
        [Required] public ProgramSettingValueEditDto ProgramSettingValueEditDto { get; set; }
    }

    public class LmsSetting
    {
        public string url { get; set; }

        public string storeForwardAutoSync { get; set; }

        public long timeout { get; set; }

        public string name { get; set; }

        public string code { get; set; }

        public bool allowRedeemByPhoneNo { get; set; }

        public bool autoBlueCardEntryScreen { get; set; }

        public bool blueCardManualKeyIn { get; set; }
    }
    public class LmsPaymentSetting
    {
        public object lmsPaymentDepartments { get; set; }
    }
    public class HanaSetting
    {
        public string userName { get; set; }

        public string password { get; set; }

        public string companyCode { get; set; }

        public string purchasingOrg { get; set; }

        public string hanaSearchUrl { get; set; }

        public string hanaSaleOrderUrl { get; set; }

        public string hanaVendorSearchUrl { get; set; }

        public string reimbursementDocumentTransactionUrl { get; set; }

        public object hanaDepartments { get; set; }
    }
    public class pettyCashSetting
    {
        public bool pettyCashControl { get; set; }
    }
}