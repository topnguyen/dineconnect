﻿using DinePlan.DineConnect.Connect.SaleTarget.Dtos;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Ticket.Dto;

namespace DinePlan.DineConnect.Connect.SaleTarget
{
    public interface ISaleTargetExporter
    {
        Task<FileDto> ExportSaleTargetTemplateAsync(SaleTargetTemplateInput input);
        Task<FileDto> ExportSaleTargetAsync(SaleTargetTicketInput input, ISaleTargetAppService saleTargetAppService);

        FileDto DowloadExistedTemplate(string path, string fileName);
    }
}
