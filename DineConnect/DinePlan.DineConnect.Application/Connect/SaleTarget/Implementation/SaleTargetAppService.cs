﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.BackgroundJobs;
using Abp.Linq.Extensions;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.SaleTarget.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Ticket.Implementation;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Net.MimeTypes;
using DinePlan.DineConnect.Report;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using OfficeOpenXml;

namespace DinePlan.DineConnect.Connect.SaleTarget.Implementation
{
    public class SaleTargetAppService : DineConnectAppServiceBase, ISaleTargetAppService
    {
        private readonly ISaleTargetExporter _exporter;
        private readonly IAppFolders _appFolders;
        private readonly ConnectReportAppService _connectReport;
        private readonly IReportBackgroundAppService _rbas;
        private readonly IBackgroundJobManager _bgm;
        private readonly ITenantSettingsAppService _tenantSettingsService;
        public SaleTargetAppService(ISaleTargetExporter exporter, IAppFolders appFolders,
            ConnectReportAppService connectReport, IReportBackgroundAppService rbas, IBackgroundJobManager bgm, ITenantSettingsAppService tenantSettingsService)
        {
            _exporter = exporter;
            _appFolders = appFolders;
            _connectReport = connectReport;
            _rbas = rbas;
            _bgm = bgm;
            _tenantSettingsService = tenantSettingsService;
        }

        public async Task<PagedResultOutput<SaleTargetViewDto>> GetAllAsync(SaleTargetTicketInput input)
        {
            // Read file excel
            var fileName = input.FileName;

            var fullPath = Path.Combine(_appFolders.UploadFolder, fileName);
            var saleTargetTable = ReadFromExcelFile(fullPath, "");
            if (saleTargetTable == null) return null;
            
            input.PaymentName = true;
            
            var saleSummary = await _connectReport.GetSaleSummary(input);
            var result = new List<SaleTargetViewDto>();

            foreach (var item in saleSummary.Sale.Items)
            {
                var locationCodeData = item.LocationCode;
                for (var i = 0; i < saleTargetTable.Rows.Count; i++)
                {
                    var locationCodeExcel = saleTargetTable.Rows[i].ItemArray[1].ToString();
                    if (locationCodeData == locationCodeExcel)
                    {
                        var targetView = new SaleTargetViewDto();
                        targetView.Date = item.Date;
                        targetView.LocationCode = item.LocationCode;
                        targetView.LocationName = item.Location;

                        var indexDay = ((int)item.Date.DayOfWeek == 0) ? 7 : (int)item.Date.DayOfWeek;

                        targetView.Day = item.Date.DayOfWeek.ToString();

                        if (decimal.TryParse(saleTargetTable.Rows[i].ItemArray[indexDay + 2].ToString(), out var saleTargetDecimal))
                        {
                            targetView.SaleTarget = saleTargetDecimal;
                        }
                        targetView.SaleValue = item.Total;

                        result.Add(targetView);
                    }
                }
            }


            // filter by dynamic filter
            var dataAsQueryable = result.AsQueryable();

            if (!string.IsNullOrEmpty(input.DynamicFilter))
            {
                var jsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver =
                        new CamelCasePropertyNamesContractResolver()
                };
                var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                if (filRule?.Rules != null)
                {
                    dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                }
            }
            result = dataAsQueryable.ToList();
            var totalCount = result.Count;

            if (!input.Sorting.Equals("Id"))
                result = result
                    .OrderBy(input.Sorting)
                    .ToList();

            var outputPageDtos = result
                .AsQueryable()
                .PageBy(input)
                .ToList();
            return new PagedResultOutput<SaleTargetViewDto>(
                totalCount,
                outputPageDtos
            );
        }
        private string CheckTemplateExist()
        {
            var myFileNameSub = "Sales_Target.xlsx";
            var fullPathSub = Path.Combine(_appFolders.ImportFolder, myFileNameSub);
            if (System.IO.File.Exists(fullPathSub))
            {
                return fullPathSub;
            }
            return null;
        }
        public async Task<FileDto> GetSaleTargetTemplateAsync(SaleTargetTemplateInput input)
        {
            var existTemplate = CheckTemplateExist();
            if (!string.IsNullOrEmpty(existTemplate))
            {
                return _exporter.DowloadExistedTemplate(existTemplate, "Target_Dates.xlsx");
            }
            return await _exporter.ExportSaleTargetTemplateAsync(input);
        }

        private DataTable ReadFromExcelFile(string path, string sheetName)
        {
            var dt = new DataTable();
            using (ExcelPackage package = new ExcelPackage(new FileInfo(path)))
            {
                if (package.Workbook.Worksheets.Count < 1)
                {
                    return null;
                }

                var workSheet = package.Workbook.Worksheets.FirstOrDefault(x => x.Name == sheetName) ??
                                           package.Workbook.Worksheets.FirstOrDefault();
                foreach (var firstRowCell in workSheet.Cells[1, 1, 1, workSheet.Dimension.End.Column])
                {
                    dt.Columns.Add(firstRowCell.Text);
                }

                for (var rowNumber = 2; rowNumber <= workSheet.Dimension.End.Row; rowNumber++)
                {
                    var row = workSheet.Cells[rowNumber, 1, rowNumber, workSheet.Dimension.End.Column];
                    var newRow = dt.NewRow();
                    foreach (var cell in row)
                    {
                        newRow[cell.Start.Column - 1] = cell.Text;

                    }

                    dt.Rows.Add(newRow);
                }
            }

            var columnNameList = new List<string>();
            for (var i = 0; i < dt.Columns.Count; i++)
            {
                columnNameList.Add(dt.Columns[i].ColumnName);

            }
            var targetColumnName = new List<string>(new[] {"LOCATION ID", "LOCATION CODE", "LOCATION NAME",
            "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY", "SUNDAY"});

            return !columnNameList.SequenceEqual(targetColumnName) ? null : dt;
        }

        public async Task<FileDto> GetSaleTargetExcelAsync(SaleTargetTicketInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                var setting = await _tenantSettingsService.GetAllSettings();
                input.DatetimeFormat = setting.Connect.DateTimeFormat;
                input.DateFormat = setting.Connect.SimpleDateFormat;

                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new Report.Dtos.CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.SALETARGET,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                    {
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.SALETARGET,
                            SaleTargetTicketInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value,
                        });
                    }
                }
                else
                {
                    return await _exporter.ExportSaleTargetAsync(input, this);
                }
            }
            return null;
        }
    }
}
