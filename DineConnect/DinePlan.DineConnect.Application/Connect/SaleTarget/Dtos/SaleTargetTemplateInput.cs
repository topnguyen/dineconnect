﻿using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Exporter;
using OpenHtmlToPdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Location;

namespace DinePlan.DineConnect.Connect.SaleTarget.Dtos
{
    public class SaleTargetTemplateInput : IFileExport
    {
        public GetLocationInput LocationInput { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public ExportType ExportOutputType { get; set; }
        public PaperSize PaperSize { get; set; }
        public bool Portrait { get; set; }
    }
}
