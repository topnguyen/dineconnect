﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.SaleTarget.Dtos
{
    public class SaleTargetViewDto
    {
        public DateTime Date { get; set; }
        public string LocationCode { get; set; }
        public string LocationName { get; set; }
        public string Day { get; set; }
        public decimal SaleTarget { get; set; }
        public decimal SaleValue { get; set; }
        public decimal Difference => SaleTarget - SaleValue;

    }
}
