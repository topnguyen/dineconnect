﻿using Abp.Application.Services;
using DinePlan.DineConnect.Connect.SaleTarget.Dtos;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.SaleTarget
{
    public interface ISaleTargetAppService : IApplicationService
    {
        Task<FileDto> GetSaleTargetTemplateAsync(SaleTargetTemplateInput input);
        Task<FileDto> GetSaleTargetExcelAsync(SaleTargetTicketInput input);
        Task<PagedResultOutput<SaleTargetViewDto>> GetAllAsync(SaleTargetTicketInput input);
    }
}
