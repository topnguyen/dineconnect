﻿using Abp.Configuration;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.SaleTarget.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Location;

namespace DinePlan.DineConnect.Connect.SaleTarget.Exporter
{
    public class SaleTargetExporter : FileExporterBase, ISaleTargetExporter
    {
        private readonly ILocationAppService _locationAppService;
        private readonly int _roundDecimals = 2;
        private readonly SettingManager _settingManager;

        public SaleTargetExporter(ILocationAppService locationAppService, SettingManager settingManager)
        {
            _locationAppService = locationAppService;
            _settingManager = settingManager;
            _roundDecimals = _settingManager.GetSettingValue<int>(AppSettings.ConnectSettings.Decimals);
        }

        public async Task<FileDto> ExportSaleTargetTemplateAsync(SaleTargetTemplateInput input)
        {
            var builder = new StringBuilder();
            builder.Append(L("Target_Dates"));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);


            var allLocation = await _locationAppService.GetSimpleAll(input.LocationInput);

            var listLocationId = new List<int>();
            if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
                    && !input.LocationGroup.Locations.Any())
            {
                listLocationId = _locationAppService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });

            }
            else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                listLocationId = _locationAppService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
            }
            else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                listLocationId = _locationAppService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true
                });
            }

            var locations = listLocationId.Count > 0 ? allLocation.Items.Where(location => listLocationId.Contains(location.Id)).ToList() : allLocation.Items.ToList();


            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Sheet"));
                sheet.OutLineApplyStyle = true;

                var headers = new List<string>
                {
                    L("LocationId"),
                    L("LocationCode"),
                    L("LocationNameTarget"),
                    L("Monday"),
                    L("Tuesday"),
                    L("Wednesday"),
                    L("Thursday"),
                    L("Friday"),
                    L("Saturday"),
                    L("Sunday")
                };
                AddHeader(
                    sheet,
                    true, headers.ToArray()
                );


                var rowCount = 2;

                foreach (var location in locations)
                {
                    var colCount = 1;
                    sheet.Cells[rowCount, colCount++].Value = location.Id;
                    sheet.Cells[rowCount, colCount++].Value = location.Code;
                    sheet.Cells[rowCount, colCount++].Value = location.Name;

                    rowCount++;
                }

                var lastRow = rowCount;
                // sheet.Cells[1, 1, lastRow + 1, 10].Style.WrapText = true;
                for (var i = 1; i <= 10; i++) sheet.Column(i).AutoFit();
                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

        public FileDto DowloadExistedTemplate(string path, string fileName)
        {
            return CopyFile(path, fileName);
        }

        public async Task<FileDto> ExportSaleTargetAsync(SaleTargetTicketInput input, ISaleTargetAppService saleTargetAppService)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(L("SalesTarget"));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            var saleTargets = await saleTargetAppService.GetAllAsync(input);

            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Sheet"));
                sheet.OutLineApplyStyle = true;

                var headers = new List<string>
                {
                    L("Date"),
                    L("LocationCode"),
                    L("LocationNameTarget"),
                    L("Day"),
                    L("SalesTarget"),
                    L("SalesValue"),
                    L("Difference")
                };
                AddHeader(
                    sheet,
                    true, headers.ToArray()
                );


                var rowCount = 2;
                var colCount = 1;
                foreach (var saleTarget in saleTargets.Items)
                {
                    colCount = 1;
                    sheet.Cells[rowCount, colCount++].Value = saleTarget.Date.ToString(input.DateFormat);
                    sheet.Cells[rowCount, colCount++].Value = saleTarget.LocationCode;
                    sheet.Cells[rowCount, colCount++].Value = saleTarget.LocationName;
                    sheet.Cells[rowCount, colCount++].Value = saleTarget.Day;
                    sheet.Cells[rowCount, colCount++].Value = Math.Round(saleTarget.SaleTarget, _roundDecimals, MidpointRounding.AwayFromZero);
                    sheet.Cells[rowCount, colCount++].Value = Math.Round(saleTarget.SaleValue, _roundDecimals, MidpointRounding.AwayFromZero);
                    sheet.Cells[rowCount, colCount++].Value = Math.Round(saleTarget.Difference, _roundDecimals, MidpointRounding.AwayFromZero);

                    rowCount++;
                }

                sheet.Cells[rowCount, 1].Value = L("Total");
                sheet.Cells[rowCount, 1, rowCount, 7].Style.Font.Bold = true;
                colCount = 2;
                sheet.Cells[rowCount, colCount++].Value = "";
                sheet.Cells[rowCount, colCount++].Value = "";
                sheet.Cells[rowCount, colCount++].Value = "";
                sheet.Cells[rowCount, colCount++].Value = saleTargets.Items.Sum(c => Math.Round(c.SaleTarget, _roundDecimals, MidpointRounding.AwayFromZero));
                sheet.Cells[rowCount, colCount++].Value = saleTargets.Items.Sum(c => Math.Round(c.SaleValue, _roundDecimals, MidpointRounding.AwayFromZero));
                sheet.Cells[rowCount, colCount++].Value = saleTargets.Items.Sum(c => Math.Round(c.Difference, _roundDecimals, MidpointRounding.AwayFromZero));
                var lastRow = rowCount;
                // sheet.Cells[1, 1, lastRow + 1, 10].Style.WrapText = true;
                for (var i = 1; i <= 7; i++) sheet.Column(i).AutoFit();
                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }
    }
}