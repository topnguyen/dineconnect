﻿using System.Collections.Generic;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.OrderTag.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.OrderTags;
using static DinePlan.DineConnect.Connect.OrderTag.Dtos.GetOrderExchangeReportInput;

namespace DinePlan.DineConnect.Connect.OrderTag
{
    public interface IOrderTagGroupAppService : IApplicationService
    {
        Task<PagedResultOutput<OrderTagGroupListDto>> GetAll(GetOrderTagGroupInput inputDto);

        Task<FileDto> GetAllToExcel(bool isTemplate = false);

        Task<GetOrderTagGroupForEditOutput> GetTagForEdit(NullableIdInput nullableIdInput);

        Task CreateOrUpdateOrderTagGroup(CreateOrUpdateOrderTagGroupInput input);

        Task DeleteOrderTagGroup(IdInput input);

        Task<ListResultOutput<OrderTagGroupListDto>> GetIds();

        Task<ListResultOutput<OrderTagGroupEditDto>> GetTagForCategoryAndItem(OrderTagInput input);

        Task<ListResultOutput<OrderTagGroupEditDto>> ApiGetTags(ApiLocationInput locationInput);

        Task ActivateItem(IdInput input);
        Task<OrderTagReportOutput> GetOrderTagReport(GetOrderTagReportInput input);
        Task<FileDto> GetOrderTagsExcel(GetOrderTagReportInput input);
        Task<OrderExchangeReportOutput> GetOrderExchangeReport(GetOrderExchangeReportInput input);
        Task<FileDto> GetOrderExchangesExcel(GetOrderExchangeReportInput input);
        Task<PagedResultOutput<OrderTagLocationPriceListDto>> GetAllOrderTagLocationPrice(GetOrderTagInput input);
        Task<GetOrderTagLocationPriceForEditOutput> GetOrderTagLocationPriceForEdit(NullableIdInput input);
        Task CreateOrderTagLocationPrice(CreateOrderTagLocationPrice input);
        Task DeleteOrderTagLocationPrice(IdInput input);
        Task<OrderTagDto> GetOrdertagForEdit(NullableIdInput input);
        Task<ListResultOutput<OrderTagGroupEditDto>> GetOrderTagGroupNames();
        Task<ListResultOutput<OrderTagDto>> GetOrderTagNames();
        Task<List<OrderTagGroup>> ApiGetAllOrderTagGroupsForSync(ApiLocationInput input);


    }
}