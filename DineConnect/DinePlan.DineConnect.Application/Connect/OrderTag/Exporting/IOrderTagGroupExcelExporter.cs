﻿using Abp.Domain.Repositories;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.OrderTag.Dtos;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.OrderTag.Exporting
{
    public interface IOrderTagGroupExcelExporter
    {
        FileDto ExportToFile(List<OrderTagGroupListDto> dtos);
        Task<FileDto> ExportOrderTag(GetOrderTagReportInput input, IConnectReportAppService connectReportService);
        Task<FileDto> ExportOrderExchange(GetOrderExchangeReportInput input, IConnectReportAppService connectReportService);
    }
}