﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.OrderTag.Dtos;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.Handler.Mapping;
using DinePlan.DineConnect.Helper;
using DinePlan.DineConnect.Net.MimeTypes;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using static DinePlan.DineConnect.Connect.OrderTag.Dtos.GetOrderExchangeReportInput;

namespace DinePlan.DineConnect.Connect.OrderTag.Exporting
{
    public class OrderTagGroupExcelExporter : FileExporterBase, IOrderTagGroupExcelExporter
    {
        private readonly IRepository<MenuItem> _menuitemRepo;
        public OrderTagGroupExcelExporter(IRepository<MenuItem> menuitemRepo)
        {
            _menuitemRepo = menuitemRepo;
        }
        public FileDto ExportToFile(List<OrderTagGroupListDto> dtos)
        {
            var allMenuItem = _menuitemRepo.GetAll();
            return CreateExcelPackage(
                "OrderTagGroupList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("OrderTagGroups"));
                    sheet.OutLineApplyStyle = true;
                    var listExport = new List<ImportOrderTagGroupDto>();
                    foreach (var group in dtos)
                    {
                        var allMap = group.Maps;
                        var allTag = group.Tags;
                        if (!allMap.Any() && !allTag.Any())
                        {
                            listExport.Add(new ImportOrderTagGroupDto
                            {
                                Id = group?.Id,
                                TagGroupName = group.Name,
                                Departments = group.Departments,
                                MinimumEx = group?.MinSelectedItems,
                                MaximumEx = group?.MaxSelectedItems,
                                FreeTagging = group.FreeTagging,
                                SaveFreeTags = group.SaveFreeTags,
                                AddTagPriceToOrder = group.AddTagPriceToOrderPrice,
                                TaxFree = group.TaxFree
                            });
                        }
                        var listExportMaps = new List<ImportOrderTagGroupDto>();
                        var idMap = 0;
                        foreach (var map in group.Maps)
                        {                         
                            listExportMaps.Add(new ImportOrderTagGroupDto
                            {
                                IndexEx = idMap,
                                Category = map.CategoryName,
                                Menu = allMenuItem.FirstOrDefault(s => s.Id == map.MenuItemId)?.Name
                            });
                            idMap++;
                        }
                        var listExportTags = new List<ImportOrderTagGroupDto>();
                        var idTag = 0;
                        foreach (var tag in group.Tags)
                        {
                           
                            listExportTags.Add(new ImportOrderTagGroupDto
                            {
                                IndexEx = idTag,
                                Tag = tag.Name,
                                TagAliasName = tag.AlternateName, 
                                TagPrice = tag.Price,
                                TagQuantity = tag.MaxQuantity,
                                MenuItem = allMenuItem.FirstOrDefault(s => s.Id == tag.MenuItemId)?.Name
                            });
                            idTag++;
                        }
                        var listMerge = new List<ImportOrderTagGroupDto>();
                        var depart = !string.IsNullOrEmpty(group.Departments) ? JsonConvert.DeserializeObject<List<ComboboxItemDto>>(group.Departments): null;
                        var count = listExportMaps.Count >= listExportTags.Count ? listExportMaps.Count : listExportTags.Count;
                        for (int i = 0; i < count; i++)
                        {
                            var findMap = listExportMaps.Find(s => s.IndexEx == i);
                            var findTag = listExportTags.Find(s => s.IndexEx == i);

                            listMerge.Add(new ImportOrderTagGroupDto
                            {
                                Id = group?.Id,
                                TagGroupName = group.Name,
                                Departments = depart != null ? (depart.FirstOrDefault()?.DisplayText ?? "*") : "*",
                                MinimumEx = group?.MinSelectedItems,
                                MaximumEx = group?.MaxSelectedItems,
                                FreeTagging = group.FreeTagging,
                                SaveFreeTags = group.SaveFreeTags,
                                AddTagPriceToOrder = group.AddTagPriceToOrderPrice,
                                TaxFree = group.TaxFree,
                                Tag = findTag != null ? findTag.Tag : "",
                                TagAliasName = findTag != null ? findTag.TagAliasName : "",
                                TagPrice = findTag != null ? findTag.TagPrice : 0,
                                TagQuantity = findTag != null ? findTag.TagQuantity : 0,
                                MenuItem = findTag != null ? findTag.MenuItem : "",
                                Category = findMap != null ? findMap.Category : "",
                                Menu = findMap != null ? findMap.Menu : "",
                            }) ;
                        }
                        listExport.AddRange(listMerge);
                    }
                    listExport = listExport.OrderBy(x => x.Id).ToList();
                    AddHeader(
                        sheet,
                        "Id",
                        "Tag Group Name",
                        "Departments",
                        "Minimum",
                        "Maximum",
                        "Free Tagging",
                        "Save Free Tags",
                        "Add Tag Price To Order",
                        "Tax Free",

                        "Tag",
                        "Tag Alias Name",
                        "Menu Item",
                        "Tag Quantity",
                        "Tag Price",

                        "Category",
                        "Menu"
                        );
                    AddObjects(
                        sheet,
                        2,
                        listExport,
                        _ =>_.Id,
                        _ => _.TagGroupName,
                        _ => _.Departments,
                        _ => _.MinimumEx,
                        _ => _.MaximumEx,
                        _ => _.FreeTagging == false ? "0" : "1",
                        _ => _.SaveFreeTags == false ? "0" : "1",
                        _ => _.AddTagPriceToOrder == false ? "0" : "1",
                        _ => _.TaxFree == false ? "0" : "1",
                        _ => _.Tag,
                        _ => _.TagAliasName,
                        _ => _.MenuItem,
                        _ => _.TagQuantity,
                        _ => _.TagPrice,
                        _ => _.Category,
                        _ => _.Menu

                    );

                    //Formatting cells

                    var creationTimeColumn = sheet.Column(16);
                    creationTimeColumn.Style.Numberformat.Format = "mm-dd-yyyy hh:mm:ss";

                    for (var i = 1; i <= 16; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
        public async Task<FileDto> ExportOrderTag(GetOrderTagReportInput input, IConnectReportAppService connectReportService)
        {
            var start = input.StartDate.ToString("ddMMyyyy");
            var end = input.EndDate.ToString("ddMMyyyy");
            var file = new FileDto($"Order_Tag_Report_{start}-{end}.xlsx", MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add($"Order Tag Report_{start}-{end}");
                sheet.OutLineApplyStyle = true;

                AddHeader(sheet,
                        L("TicketNo"),
                        L("LocationCode"),
                        L("OrderId"),
                        L("OrderTagGroup"),
                        L("OrderTag"),
                        L("Quantity"),
                        L("Price"),
                        L("Total"),
                        L("User"));

                var cInput = new GetChartInput
                {
                    StartDate = input.StartDate,
                    EndDate = input.EndDate,
                    LocationGroup = input.LocationGroup
                };
                var tickets = connectReportService.GetAllTickets(cInput).SelectMany(x => x.Orders.SelectMany(o => o.TransactionOrderTags));
                var dtos = tickets.OrderBy(input.Sorting).Select(tag => new OrderTagReportDto
                {
                    TicketId = tag.Order.TicketId,
                    TicketNo = tag.Order.Ticket.TicketNumber,
                    LocationCode = tag.Order.Ticket.Location.Code,
                    OrderId = tag.Order.OrderId,
                    OrderTagGroup = tag.TagName,
                    OrderTagName = tag.TagValue,
                    Quantity = tag.Quantity,
                    Price = tag.Price,
                    Total = tag.Price * tag.Quantity,
                    User = tag.Order.Ticket.LastModifiedUserName
                }).ToList();

                AddObjects(sheet, 2, dtos,
                            _ => _.TicketNo,
                            _ => _.LocationCode,
                            _ => _.OrderId,
                            _ => _.OrderTagGroup,
                            _ => _.OrderTagName,
                            _ => _.Quantity,
                            _ => _.Price,
                            _ => _.Total,
                            _ => _.User
                            );

                //Formatting cells

                for (var i = 1; i <= 10; i++)
                {
                    sheet.Column(i).AutoFit();
                }

                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

        public async Task<FileDto> ExportOrderExchange(GetOrderExchangeReportInput input, IConnectReportAppService connectReportService)
        {
            var start = input.StartDate.ToString("ddMMyyyy");
            var end = input.EndDate.ToString("ddMMyyyy");
            var file = new FileDto($"Order_Exchange_Report_{start}-{end}.xlsx", MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add($"Order Exchange Report_{start}-{end}");
                sheet.OutLineApplyStyle = true;

                AddHeader(sheet,
                        L("TicketNo"),
                        L("LocationCode"),
                        L("OrderId"),
                        L("MenuItemName"),
                        L("OrderExchange"),
                        L("Quantity"),
                        L("Price"),
                        L("Total"),
                        L("User"));

                var cInput = new GetChartInput
                {
                    StartDate = input.StartDate,
                    EndDate = input.EndDate,
                    LocationGroup = input.LocationGroup
                };
                var tickets = connectReportService.GetAllTickets(cInput).SelectMany(x => x.Orders).Where(x => x.OrderStates.Contains("Exchange"));
                var dtos = tickets.OrderBy(input.Sorting).Select(order => new OrderExchangeReportDto
                {
                    TicketId = order.TicketId,
                    TicketNo = order.Ticket.TicketNumber,
                    LocationCode = order.Ticket.Location.Code,
                    OrderId = order.OrderId,
                    MenuItemName = order.MenuItemName,
                    OrderExchange = order.OrderLog.Replace("\r\n", ", "),
                    Quantity = order.Quantity,
                    Price = order.Price,
                    Total = order.Price * order.Quantity,
                    User = order.Ticket.LastModifiedUserName
                }).ToList();

                AddObjects(sheet, 2, dtos,
                            _ => _.TicketNo,
                            _ => _.LocationCode,
                            _ => _.OrderId,
                            _ => _.MenuItemName,
                            _ => _.OrderExchange,
                            _ => _.Quantity,
                            _ => _.Price,
                            _ => _.Total,
                            _ => _.User
                            );

                //Formatting cells

                for (var i = 1; i <= 10; i++)
                {
                    sheet.Column(i).AutoFit();
                }

                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }
    }
}