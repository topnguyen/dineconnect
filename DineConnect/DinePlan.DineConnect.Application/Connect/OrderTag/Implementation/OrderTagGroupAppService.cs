﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using System.Windows;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using AutoMapper;
using Castle.Core.Logging;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.OrderTag.Dtos;
using DinePlan.DineConnect.Connect.OrderTag.Exporting;
using DinePlan.DineConnect.Connect.OrderTags;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.Handler.Mapping;
using DinePlan.DineConnect.Helper;
using DinePlan.DineConnect.Job.OrderTagReportJob;
using DinePlan.DineConnect.Report;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using static DinePlan.DineConnect.Connect.OrderTag.Dtos.GetOrderExchangeReportInput;

namespace DinePlan.DineConnect.Connect.OrderTag.Implementation
{
    public class OrderTagGroupAppService : DineConnectAppServiceBase, IOrderTagGroupAppService
    {
        private readonly IBackgroundJobManager _bgm;
        private readonly IRepository<Category> _cateRepository;
        private readonly IConnectReportAppService _cService;
        private readonly ILocationAppService _locAppService;
        private readonly ILogger _logger;
        private readonly IRepository<OrderTagMap> _orderMapManager;
        private readonly IOrderTagGroupExcelExporter _orderTagGroupExporter;
        private readonly IOrderTagGroupManager _ordertaggroupManager;
        private readonly IRepository<OrderTagGroup> _ordertaggroupRepo;
        private readonly IRepository<OrderTags.OrderTag> _orderTagManager;
        private readonly IReportBackgroundAppService _rbas;
        private readonly ISyncAppService _syncAppService;
        private readonly IRepository<Transaction.Ticket> _ticketRepo;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<MenuItem> _menuitemRepo;
        private readonly IRepository<OrderTagLocationPrice> _ordertagLocationPriceRepo;
        private readonly IRepository<Master.Location> _locationRepo;
        private readonly IRepository<OrderTagLocationPrice> _tagLocationPrice;
        private readonly string DISCOUNT = "DISCOUNT";
        public OrderTagGroupAppService(IOrderTagGroupManager ordertaggroupManager, ILocationAppService locAppService,
            IRepository<OrderTagGroup> orderTagGroupRepo, IRepository<OrderTagLocationPrice> tagLocationPrice,
            IRepository<OrderTags.OrderTag> orderTag,
            IRepository<OrderTagMap> orderTagMap, IRepository<Category> cateRepository,
            IOrderTagGroupExcelExporter orderTagGroupExporter,
            ISyncAppService syncAppService,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<Transaction.Ticket> ticketRepo,
            IConnectReportAppService cService,
            ILogger logger,
            IReportBackgroundAppService reportBackgroundAppService,
            IBackgroundJobManager backgroundJobManager,
            IRepository<MenuItem> menuitemRepo,
            IRepository<OrderTagLocationPrice> ordertagLocationPriceRepo,
            IRepository<Master.Location> locationRepo)
        {
            _ordertaggroupManager = ordertaggroupManager;
            _ordertaggroupRepo = orderTagGroupRepo;
            _orderMapManager = orderTagMap;
            _orderTagManager = orderTag;
            _cateRepository = cateRepository;
            _orderTagGroupExporter = orderTagGroupExporter;
            _syncAppService = syncAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _locAppService = locAppService;
            _ticketRepo = ticketRepo;
            _cService = cService;
            _logger = logger;
            _rbas = reportBackgroundAppService;
            _bgm = backgroundJobManager;
            _menuitemRepo = menuitemRepo;
            _ordertagLocationPriceRepo = ordertagLocationPriceRepo;
            _locationRepo = locationRepo;
            _tagLocationPrice = tagLocationPrice;
        }

        public async Task<PagedResultOutput<OrderTagGroupListDto>> GetAll(GetOrderTagGroupInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                IQueryable<OrderTagGroup> allItems;
                if (input.Operation == "SEARCH")
                    allItems = _ordertaggroupRepo
                        .GetAll()
                        .Where(i => i.IsDeleted == input.IsDeleted)
                        .WhereIf(
                            !input.Filter.IsNullOrEmpty(),
                            p => p.Id.Equals(input.Filter)
                        );
                else
                    allItems = _ordertaggroupRepo
                        .GetAll()
                        .Where(i => i.IsDeleted == input.IsDeleted)
                        .WhereIf(
                            !input.Filter.IsNullOrEmpty(),
                            p => p.Name.Contains(input.Filter)
                        );

                var allMyEnItems = SearchLocation(allItems, input.LocationGroup).OfType<OrderTagGroup>();

                var allItemCount = allMyEnItems.Count();

                var sortMenuItems = allMyEnItems.AsQueryable()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList();

                var allListDtos = sortMenuItems.MapTo<List<OrderTagGroupListDto>>();
                var allListOrder = allListDtos.OrderBy(s => s.SortOrder).ThenBy(s => s.Id).ToList();
                return new PagedResultOutput<OrderTagGroupListDto>(
                    allItemCount,
                    allListOrder
                );
            }
        }

        public async Task<FileDto> GetAllToExcel(bool isTemplate = false)
        {
            if (isTemplate)
            {
                return _orderTagGroupExporter.ExportToFile(new List<OrderTagGroupListDto>());
            }

            var allList = await _ordertaggroupRepo.GetAll()
                .OrderBy(t => t.Id)
                .ToListAsync();

            var allListDtos = allList.MapTo<List<OrderTagGroupListDto>>();

            foreach (var loca in allListDtos)
            {
                if (string.IsNullOrEmpty(loca.Departments) || loca.Departments.ToUpper().Equals("NULL"))
                {
                    loca.DisplayDepartments = "";
                }
                else
                {
                    var allLo = JsonConvert.DeserializeObject<List<ComboboxItemDto>>(loca.Departments);
                    loca.DisplayDepartments = allLo.Select(a => a.DisplayText).JoinAsString(", ");
                }
            }

            return _orderTagGroupExporter.ExportToFile(allListDtos);
        }

        public async Task<GetOrderTagGroupForEditOutput> GetTagForEdit(NullableIdInput input)
        {
            var output = new GetOrderTagGroupForEditOutput();

            OrderTagGroupEditDto editDto;
            if (input.Id.HasValue)
            {
                var hDto = await _ordertaggroupRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<OrderTagGroupEditDto>();
                var rscategory = await _cateRepository.GetAllListAsync();
                var rsmenuItem = await _menuitemRepo.GetAllListAsync();
                if (editDto.Tags != null && editDto.Tags.Count > 0)
                {
                    foreach (var lst in editDto.Tags)
                    {
                        if (lst.MenuItemId != null)
                        {
                            var menuItem = rsmenuItem.FirstOrDefault(t => t.Id == lst.MenuItemId);
                            if (menuItem != null)
                            {
                                lst.MenuItemGroupCode = menuItem.Name;
                            }
                        }
                    }
                }
                if (editDto.Maps != null && editDto.Maps.Count > 0)
                {
                    foreach (var lst in editDto.Maps)
                    {
                        if (lst.CategoryId != null)
                        {
                            var category = rscategory.FirstOrDefault(t => t.Id == lst.CategoryId);
                            if (category != null)
                            {
                                lst.CategoryName = category.Name;
                            }
                        }
                        if (lst.MenuItemId != null)
                        {
                            var menuItem = rsmenuItem.FirstOrDefault(t => t.Id == lst.MenuItemId);
                            if (menuItem != null)
                            {
                                lst.MenuItemGroupCode = menuItem.Name;
                            }
                        }
                    }
                }
            }
            else
            {
                editDto = new OrderTagGroupEditDto();
            }

            UpdateLocationAndNonLocationForEdit(editDto, output.LocationGroup);

            output.OrderTagGroup = editDto;
            if (!string.IsNullOrEmpty(editDto.Departments))
                output.Departments = JsonConvert.DeserializeObject<List<ComboboxItemDto>>(editDto.Departments);
            return output;
        }

        public async Task CreateOrUpdateOrderTagGroup(CreateOrUpdateOrderTagGroupInput input)
        {
            if (input?.LocationGroup == null)
                return;

            if (input.OrderTagGroup.Id.HasValue)
                await UpdateOrderTagGroup(input);
            else
                await CreateOrderTagGroup(input);
            await _syncAppService.UpdateSync(SyncConsts.ORDERTAG);
        }

        public async Task DeleteOrderTagGroup(IdInput input)
        {
            await _ordertaggroupRepo.DeleteAsync(input.Id);
            await _syncAppService.UpdateSync(SyncConsts.ORDERTAG);
        }

        public async Task<ListResultOutput<OrderTagGroupListDto>> GetIds()
        {
            var lstOrderTagGroup = await _ordertaggroupRepo.GetAll().OrderBy(x => x.Name).ToListAsync();
            return new ListResultOutput<OrderTagGroupListDto>(lstOrderTagGroup.MapTo<List<OrderTagGroupListDto>>());
        }

        public async Task<ListResultOutput<OrderTagGroupEditDto>> GetTagForCategoryAndItem(OrderTagInput input)
        {
            var allItems = await (from mas in _ordertaggroupRepo.GetAll()
                                  where mas.Maps.Any(a => a.CategoryId == input.CategoryId && a.MenuItemId == input.MenuItemId)
                                  select mas).ToListAsync();

            var allListDtos = allItems.MapTo<List<OrderTagGroupEditDto>>();
            return new ListResultOutput<OrderTagGroupEditDto>(allListDtos);
        }

        public async Task<ListResultOutput<OrderTagGroupEditDto>> ApiGetTags(ApiLocationInput locationInput)
        {
            var returnDto = new List<OrderTagGroupEditDto>();
            var orgId = await _locAppService.GetOrgnizationIdForLocationId(locationInput.LocationId);

            if (locationInput.TenantId == 0)
            {
                var myLocation = await _locationRepo.GetAsync(locationInput.LocationId);
                if (myLocation != null)
                {
                    locationInput.TenantId = myLocation.TenantId;
                }
            }

            if (orgId > 0) CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);

            var allItems = await (from mas in _ordertaggroupRepo.GetAll().Where(a => a.TenantId == locationInput.TenantId)
                                  select mas).ToListAsync();

            var allListOutputs = allItems.MapTo<List<OrderTagGroupEditDto>>();

            foreach (var map in allListOutputs.SelectMany(a => a.Maps))
                if (map.CategoryId != null)
                    using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                    {
                        map.CategoryName = _cateRepository.Get(map.CategoryId.Value).Name;
                    }

            if (!string.IsNullOrEmpty(locationInput.Tag))
            {
                var allLocations = await _locAppService.GetLocationsByLocationGroupCode(locationInput.Tag);
                if (allLocations != null && allLocations.Items.Any())
                    foreach (var orderTagList in allListOutputs)
                        if (!orderTagList.Group && !string.IsNullOrEmpty(orderTagList.Locations) &&
                            !orderTagList.LocationTag)
                        {
                            var scLocations =
                                JsonConvert.DeserializeObject<List<SimpleLocationGroupDto>>(orderTagList.Locations);
                            if (scLocations.Any())
                                foreach (var i in scLocations.Select(a => a.Id))
                                    if (allLocations.Items.Select(a => a.Value).Contains(i.ToString()))
                                        returnDto.Add(orderTagList);
                        }
                        else
                        {
                            returnDto.Add(orderTagList);
                        }
            }
            else if (locationInput.LocationId > 0)
            {
                foreach (var myDto in allListOutputs)
                    if (await _locAppService.IsLocationExists(new CheckLocationInput
                    {
                        LocationId = locationInput.LocationId,
                        Locations = myDto.Locations,
                        Group = myDto.Group,
                        LocationTag = myDto.LocationTag,
                        NonLocations = myDto.NonLocations
                    }))
                        returnDto.Add(myDto);
            }
            else
            {
                returnDto = allListOutputs;
            }

            var myLocationTags = _tagLocationPrice.GetAll()
                .Where(a => a.LocationId == locationInput.LocationId);
            var myTagPriceCount = myLocationTags.Count(a => a.LocationId == locationInput.LocationId);
            if (myTagPriceCount > 0)
            {
                foreach (var orderTagDto in allListOutputs.SelectMany(a => a.Tags))
                {
                    var myTag = myLocationTags.Where(a => a.OrderTagId == orderTagDto.Id).ToList();
                    if (myTag.Any())
                    {
                        orderTagDto.Price = myTag.Last().Price;
                    }
                }
            }

            return new ListResultOutput<OrderTagGroupEditDto>(returnDto);
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _ordertaggroupRepo.GetAsync(input.Id);
                item.IsDeleted = false;

                await _ordertaggroupRepo.UpdateAsync(item);
            }
        }

        public async Task<OrderTagReportOutput> GetOrderTagReport(GetOrderTagReportInput input)
        {
            var cInput = new GetChartInput
            {
                StartDate = input.StartDate,
                EndDate = input.EndDate,
                LocationGroup = input.LocationGroup
            };
            var tickets = _cService.GetAllTickets(cInput);
            try
            {
                var ordersTags = tickets.SelectMany(x => x.Orders.SelectMany(o => o.TransactionOrderTags))
                    .WhereIf(input.OrderTagGroupIds != null && input.OrderTagGroupIds.Any(),
                        x => input.OrderTagGroupIds.Contains(x.OrderTagGroupId));
                //if (!string.IsNullOrEmpty(input.DynamicFilter))
                //{
                //    var jsonSerializerSettings = new JsonSerializerSettings
                //    {
                //        ContractResolver =
                //            new CamelCasePropertyNamesContractResolver()
                //    };
                //    var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                //    if (filRule?.Rules != null)
                //    {
                //        foreach (var lst in filRule.Rules)
                //        {
                //            if (lst.Field == "TicketNo")
                //            {
                //                ordersTags = ordersTags.Where(t => t.Order.Ticket.TicketNumber == lst.Value);
                //            }
                //            if (lst.Field == "Quantity")
                //            {
                //                var quantity = decimal.Parse(lst.Value);
                //                ordersTags = ordersTags.Where(t => t.Quantity == quantity);
                //            }
                //            if (lst.Field == "Price")
                //            {
                //                var price = decimal.Parse(lst.Value);
                //                ordersTags = ordersTags.Where(t => t.Price == price);
                //            }
                //            if (lst.Field == "TotalAmount")
                //            {
                //                var totalAmount = decimal.Parse(lst.Value);
                //                ordersTags = ordersTags.Where(t => t.Order.Ticket.TotalAmount == totalAmount);
                //            }
                //        }
                //        //filRule.Rules = filRule.Rules.Where(t => t.Field != "TicketNo" && t.Field != "Quantity" && t.Field != "Price" && t.Field!= "TotalAmount").ToList();
                //        //if (filRule.Rules != null && filRule.Rules.Count > 0)
                //        //    ordersTags = ordersTags.BuildQuery(filRule);
                //    }
                //}

                var orderTagGroup = _ordertaggroupRepo.GetAll().Select(s => s.Name).ToList();
                var allTags = await ordersTags.Where(s => orderTagGroup.Contains(s.TagName)).OrderBy(input.Sorting).ToListAsync();
                var dtos = new List<OrderTagReportDto>();
                var orderTags = new List<OrderTagReportDto>();
                foreach (var tag in allTags)
                {
                    if (!orderTags.Any())
                    {
                        orderTags.Add(new OrderTagReportDto
                        {
                            TicketId = tag.Order.TicketId,
                            TicketNo = tag.Order.Ticket.TicketNumber,
                            LocationCode = tag.Order.Ticket.Location.Code,
                            OrderId = tag.Order.OrderId,
                            OrderTagGroup = tag.TagName,
                            OrderTagName = tag.TagValue,
                            Quantity = tag.Quantity,
                            Price = tag.Price,
                            Total = tag.Price * tag.Quantity,
                            User = tag.Order.Ticket.LastModifiedUserName
                        });
                        continue;
                    }

                    var existTag = orderTags.FirstOrDefault(x =>
                        x.OrderId == tag.Order.OrderId && x.OrderTagGroup == tag.TagName &&
                        x.OrderTagName == tag.TagValue && x.Price == tag.Price);
                    if (existTag != null)
                    {
                        existTag.Quantity += tag.Quantity;
                    }
                    else
                    {
                        dtos.AddRange(orderTags);
                        orderTags = new List<OrderTagReportDto>
                        {
                            new OrderTagReportDto
                            {
                                TicketId = tag.Order.TicketId,
                                TicketNo = tag.Order.Ticket.TicketNumber,
                                LocationCode = tag.Order.Ticket.Location.Code,
                                OrderId = tag.Order.OrderId,
                                OrderTagGroup = tag.TagName,
                                OrderTagName = tag.TagValue,
                                Quantity = tag.Quantity,
                                Price = tag.Price,
                                Total = tag.Price * tag.Quantity,
                                User = tag.Order.Ticket.LastModifiedUserName
                            }
                        };
                    }
                }
                dtos.AddRange(orderTags);

                // filter by query builder
                var dataAsQueryable = dtos.AsQueryable();
                if (!string.IsNullOrEmpty(input.DynamicFilter))
                {
                    var jsonSerializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver =
                            new CamelCasePropertyNamesContractResolver()
                    };
                    var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                    if (filRule?.Rules != null)
                    {
                        dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                    }
                }
                var total = dataAsQueryable.Count();
                var pageBy = dataAsQueryable.PageBy(input).ToList();

                return new OrderTagReportOutput
                {
                    Data = new PagedResultOutput<OrderTagReportDto>(total, pageBy),
                    DashBoardOrderDto = new DashboardOrderDto
                    {
                        TotalAmount = ordersTags.Sum(x => x.Price * x.Quantity),
                        TotalOrderCount = ordersTags.Select(x => x.Order.Id).Distinct().Count(),
                        TotalItemSold = ordersTags.Count()
                    }
                };
            }
            catch (Exception ex)
            {
                _logger.Error("GetOrderTagReport", ex);
            }

            return new OrderTagReportOutput
            {
                Data = new PagedResultOutput<OrderTagReportDto>(0, new List<OrderTagReportDto>()),
                DashBoardOrderDto = new DashboardOrderDto()
            };
        }

        public async Task<FileDto> GetOrderTagsExcel(GetOrderTagReportInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.ORDERTAGS,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                    {
                        input.BackGroundId = backGroundId;
                        input.ReportName = ReportNames.ORDERTAGS;
                        await _bgm.EnqueueAsync<OrderTagReportJob, GetOrderTagReportInput>(input);
                    }
                }
                else
                {
                    return await _orderTagGroupExporter.ExportOrderTag(input, _cService);
                }
            }

            return null;
        }

        protected virtual async Task UpdateOrderTagGroup(CreateOrUpdateOrderTagGroupInput input)
        {
            var dto = input.OrderTagGroup;
            var item = await _ordertaggroupRepo.GetAsync(input.OrderTagGroup.Id.Value);

            item.Name = dto.Name;
            item.Prefix = dto.Prefix;
            item.MaxSelectedItems = dto.MaxSelectedItems;
            item.MinSelectedItems = dto.MinSelectedItems;
            item.SortOrder = dto.SortOrder;
            item.AddTagPriceToOrderPrice = dto.AddTagPriceToOrderPrice;
            item.SaveFreeTags = dto.SaveFreeTags;
            item.FreeTagging = dto.FreeTagging;
            item.TaxFree = dto.TaxFree;
            item.Files = dto.Files;
            item.DownloadImage = Guid.NewGuid();
            item.Departments = JsonConvert.SerializeObject(input.Departments);

            UpdateLocationAndNonLocation(item, input.LocationGroup);
            if (dto?.Tags != null && dto.Tags.Any())
            {
                var oids = new List<int>();
                foreach (var otdto in dto.Tags)
                    if (otdto.Id == null)
                    {
                        item.Tags.Add(Mapper.Map<OrderTagDto, OrderTags.OrderTag>(otdto));
                    }
                    else
                    {
                        oids.Add(otdto.Id.Value);
                        var fromUpdation = item.Tags.SingleOrDefault(a => a.Id.Equals(otdto.Id));
                        UpdateTag(fromUpdation, otdto);
                    }

                var tobeRemoved = item.Tags.Where(a => !a.Id.Equals(0) && !oids.Contains(a.Id)).ToList();
                foreach (var tagItem in tobeRemoved) await _orderTagManager.DeleteAsync(tagItem.Id);
            }

            if (dto?.Maps != null && dto.Maps.Any())
            {
                var oids = new List<int>();
                foreach (var otdto in dto.Maps)
                    if (otdto.Id == null)
                    {
                        item.Maps.Add(Mapper.Map<OrderMapDto, OrderTagMap>(otdto));
                    }
                    else
                    {
                        oids.Add(otdto.Id.Value);
                        var fromUpdation = item.Maps.SingleOrDefault(a => a.Id.Equals(otdto.Id));
                        UpdateMap(fromUpdation, otdto);
                    }

                var tobeRemoved =
                    item.Maps.Where(a => a.Id != null && !a.Id.Equals(0) && !oids.Contains(a.Id)).ToList();
                foreach (var mapItem in tobeRemoved) await _orderMapManager.DeleteAsync(mapItem.Id);
            }
            else
            {
                var allList = _orderMapManager.GetAll().Where(a => a.OrderTagGroupId == dto.Id).ToList();
                foreach (var orderMap in allList) await _orderMapManager.DeleteAsync(orderMap.Id);
            }

            CheckErrors(await _ordertaggroupManager.CreateSync(item));
        }

        private void UpdateMap(OrderTagMap fromUpdation, OrderMapDto otdto)
        {
            fromUpdation.CategoryId = otdto.CategoryId;
            fromUpdation.MenuItemId = otdto.MenuItemId;
        }

        private void UpdateTag(OrderTags.OrderTag fromUpdation, OrderTagDto otdto)
        {
            fromUpdation.Name = otdto.Name;
            fromUpdation.AlternateName = otdto.AlternateName;
            fromUpdation.MenuItemId = otdto.MenuItemId;
            fromUpdation.MaxQuantity = otdto.MaxQuantity;
            fromUpdation.Price = otdto.Price;
            fromUpdation.SortOrder = otdto.SortOrder;
            fromUpdation.Files = otdto.Files;
            fromUpdation.DownloadImage = Guid.NewGuid();
        }

        protected virtual async Task CreateOrderTagGroup(CreateOrUpdateOrderTagGroupInput input)
        {
            var dto = input.OrderTagGroup.MapTo<OrderTagGroup>();
            if (input.Departments != null && input.Departments.Any())
                dto.Departments = JsonConvert.SerializeObject(input.Departments);

            UpdateLocationAndNonLocation(dto, input.LocationGroup);

            CheckErrors(await _ordertaggroupManager.CreateSync(dto));
        }

        public async Task<OrderExchangeReportOutput> GetOrderExchangeReport(GetOrderExchangeReportInput input)
        {
            var cInput = new GetChartInput
            {
                StartDate = input.StartDate,
                EndDate = input.EndDate,
                LocationGroup = input.LocationGroup
            };
            var tickets = _cService.GetAllTickets(cInput);
            try
            {
                var exchangeOrders = tickets.SelectMany(x => x.Orders).Where(x => x.OrderStates.Contains("Exchange"));
                var filterOrders = await exchangeOrders.OrderBy(input.Sorting).ToListAsync();
                var dtos = new List<OrderExchangeReportDto>();
                foreach (var order in filterOrders)
                {
                    dtos.Add(new OrderExchangeReportDto
                    {
                        TicketId = order.TicketId,
                        TicketNo = order.Ticket?.TicketNumber,
                        LocationCode = order.Ticket?.Location.Code,
                        OrderId = order.OrderId,
                        MenuItemName = order.MenuItemName,
                        OrderExchange = order.OrderLog?.Replace("\r\n", ", "),
                        OrderExchange2 = order.OrderLog,
                        Quantity = order.Quantity,
                        Price = order.Price,
                        Total = order.Price * order.Quantity,
                        User = order.Ticket?.LastModifiedUserName
                    });
                }

                // filter by query builder
                var dataAsQueryable = dtos.AsQueryable();
                if (!string.IsNullOrEmpty(input.DynamicFilter))
                {
                    var jsonSerializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver =
                            new CamelCasePropertyNamesContractResolver()
                    };
                    var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                    if (filRule?.Rules != null)
                    {
                        dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                    }
                }
                var total = dataAsQueryable.Count();
                var result = dataAsQueryable.PageBy(input).ToList();

                return new OrderExchangeReportOutput
                {
                    Data = new PagedResultOutput<OrderExchangeReportDto>(total, result),
                    DashBoardOrderDto = new DashboardOrderDto
                    {
                        TotalAmount = exchangeOrders.Sum(x => x.Price * x.Quantity),
                        TotalOrderCount = exchangeOrders.Select(x => x.Id).Distinct().Count(),
                        TotalItemSold = exchangeOrders.Count()
                    }
                };
            }
            catch (Exception ex)
            {
                _logger.Error("GetOrderExchangeReport", ex);
            }

            return new OrderExchangeReportOutput
            {
                Data = new PagedResultOutput<OrderExchangeReportDto>(0, new List<OrderExchangeReportDto>()),
                DashBoardOrderDto = new DashboardOrderDto()
            };
        }



        public async Task<FileDto> GetOrderExchangesExcel(GetOrderExchangeReportInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.ORDEREXCHANGE,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                    {
                        input.BackGroundId = backGroundId;
                        input.ReportName = ReportNames.ORDEREXCHANGE;
                        await _bgm.EnqueueAsync<OrderExchangeReportJob, GetOrderExchangeReportInput>(input);
                    }
                }
                else
                {
                    return await _orderTagGroupExporter.ExportOrderExchange(input, _cService);
                }
            }

            return null;
        }
        public async Task<PagedResultOutput<OrderTagLocationPriceListDto>> GetAllOrderTagLocationPrice(GetOrderTagInput input)
        {

            var allItems = await _ordertagLocationPriceRepo.GetAllListAsync(t => t.OrderTagId == input.OrderTagId);
            var allItemCount = allItems.Count();

            var sortMenuItems = allItems.AsQueryable()
                    .PageBy(input)
                    .ToList();

            var allListDtos = sortMenuItems.MapTo<List<OrderTagLocationPriceListDto>>();

            var rslocation = await _locationRepo.GetAllListAsync();
            foreach (var lst in allListDtos)
            {
                if (lst.LocationId > 0)
                {
                    var location = rslocation.FirstOrDefault(t => t.Id == lst.LocationId);
                    if (location != null)
                    {
                        lst.LocationName = location.Name;
                    }
                }
            }
            allListDtos = allListDtos.OrderBy(input.Sorting).ToList();
            return new PagedResultOutput<OrderTagLocationPriceListDto>(
                allItemCount,
                allListDtos
            );
        }
        public async Task<GetOrderTagLocationPriceForEditOutput> GetOrderTagLocationPriceForEdit(NullableIdInput input)
        {
            var output = new GetOrderTagLocationPriceForEditOutput();
            OrderTagLocationPriceEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _ordertagLocationPriceRepo.GetAllListAsync(t => t.OrderTagId == input.Id.Value);
                editDto = hDto.MapTo<OrderTagLocationPriceEditDto>();

            }
            else
            {
                editDto = new OrderTagLocationPriceEditDto();
            }
            output.OrderTagLocationPrice = editDto;
            return output;
        }

        public async Task CreateOrderTagLocationPrice(CreateOrderTagLocationPrice input)
        {
            if (input.OrderTagLocationPrice.Id.HasValue)
            {
                var ordertaglocationprice = input.OrderTagLocationPrice;
                var existEntry = await _ordertagLocationPriceRepo.FirstOrDefaultAsync(t => t.Id == ordertaglocationprice.Id.Value);
                ordertaglocationprice.MapTo(existEntry);
                await _ordertagLocationPriceRepo.UpdateAsync(existEntry);
            }
            else
            {
                var dto = input.OrderTagLocationPrice.MapTo<OrderTagLocationPrice>();
                await _ordertagLocationPriceRepo.InsertAndGetIdAsync(dto);
            }
        }
        public async Task DeleteOrderTagLocationPrice(IdInput input)
        {
            await _ordertagLocationPriceRepo.DeleteAsync(input.Id);
        }
        public async Task<OrderTagDto> GetOrdertagForEdit(NullableIdInput input)
        {
            var result = new OrderTagDto();
            if (input.Id.HasValue)
            {
                var orderTag = await _orderTagManager.GetAsync(input.Id.Value);

                result = orderTag.MapTo<OrderTagDto>();
            }

            return result;
        }
        public async Task<ListResultOutput<OrderTagGroupEditDto>> GetOrderTagGroupNames()
        {
            var lstOrderTagGroup = await _ordertaggroupRepo.GetAll().ToListAsync();
            return new ListResultOutput<OrderTagGroupEditDto>(lstOrderTagGroup.MapTo<List<OrderTagGroupEditDto>>());
        }
        public async Task<ListResultOutput<OrderTagDto>> GetOrderTagNames()
        {
            var lstOrderTag = await _orderTagManager.GetAll().ToListAsync();
            return new ListResultOutput<OrderTagDto>(lstOrderTag.MapTo<List<OrderTagDto>>());
        }

        public async Task<List<OrderTagGroup>> ApiGetAllOrderTagGroupsForSync(ApiLocationInput input)
        {
            var orgId = await _locAppService.GetOrgnizationIdForLocationId(input.LocationId);
            if (orgId > 0) CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);
            return await _ordertaggroupRepo.GetAll().Where(a => a.TenantId == input.TenantId).Include(a => a.Tags).Include(a => a.Maps).ToListAsync();

        }

    }
}