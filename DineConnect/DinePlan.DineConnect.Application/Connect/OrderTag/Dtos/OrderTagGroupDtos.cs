﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using Abp.Extensions;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.OrderTags;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter;
using DinePlan.DineConnect.Report;
using OpenHtmlToPdf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.OrderTag.Dtos
{
    [AutoMapFrom(typeof(OrderTagGroup))]
    public class OrderTagGroupListDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int? MaxSelectedItems { get; set; }
        public int? MinSelectedItems { get; set; }
        public bool SaveFreeTags { get; set; }
        public bool FreeTagging { get; set; }
        public bool TaxFree { get; set; }
        public string DisplayLocation { get; set; }
        public string Locations { get; set; }
        public DateTime? CreationTime { get; set; }
        public string Departments { get; set; }
        public bool AddTagPriceToOrderPrice { get; set; }
        public string DisplayDepartments { get; set; }
        public int SortOrder { get; set; }
        public string Tag { get; set; }
        public decimal Price { get; set; }
        public Collection<OrderTagDto> Tags { get; set; }
        public Collection<OrderMapDto> Maps { get; set; }
    }

    [AutoMapTo(typeof(OrderTagGroup))]
    public class OrderTagGroupEditDto : ConnectEditDto
    {
        public OrderTagGroupEditDto()
        {
            Tags = new Collection<OrderTagDto>();
            Maps = new Collection<OrderMapDto>();
        }

        public int? Id { get; set; }
        public string Name { get; set; }
        public string Prefix { get; set; }
        public int MaxSelectedItems { get; set; }
        public int MinSelectedItems { get; set; }
        public int SortOrder { get; set; }
        public bool AddTagPriceToOrderPrice { get; set; }
        public bool SaveFreeTags { get; set; }
        public bool FreeTagging { get; set; }
        public bool TaxFree { get; set; }
        public string Departments { get; set; }

        public Collection<OrderTagDto> Tags { get; set; }
        public Collection<OrderMapDto> Maps { get; set; }

        public void AddTag(string name, decimal price, int menuItemId, int maxQuantity)
        {
            Tags.Add(new OrderTagDto
            {
                Name = name,
                Price = price,
                MenuItemId = 1,
                MaxQuantity = maxQuantity
            });
        }

        public void AddMap(int categoryId, int menuId)
        {
            Maps.Add(new OrderMapDto
            {
                CategoryId = categoryId,
                MenuItemId = menuId,
            });
        }

        public string Files { get; set; }
        public Guid DownloadImage { get; set; }
    }

    [AutoMapTo(typeof(OrderTags.OrderTagMap))]
    public class OrderMapDto
    {
        public int? Id { get; set; }
        public int? CategoryId { get; set; }
        public String CategoryName { get; set; }
        public int? MenuItemId { get; set; }
        public string MenuItemGroupCode { get; set; }
    }

    [AutoMapTo(typeof(OrderTags.OrderTag))]
    public class OrderTagDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string AlternateName { get; set; }
        public decimal Price { get; set; }
        public int? MenuItemId { get; set; }
        public int MaxQuantity { get; set; }
        public int SortOrder { get; set; }
        public string MenuItemGroupCode { get; set; }
        public int? CategoryId { get; set; }

        public string Files { get; set; }
        public Guid DownloadImage { get; set; }
    }

    public class GetOrderTagGroupInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }
        public string Location { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
        public GetOrderTagGroupInput()
        {
            LocationGroup = new LocationGroupDto();
        }

        public LocationGroupDto LocationGroup { get; set; }
    }

    public class GetOrderTagGroupForEditOutput : IOutputDto
    {
        public GetOrderTagGroupForEditOutput()
        {
            LocationGroup = new LocationGroupDto();
        }

        public OrderTagGroupEditDto OrderTagGroup { get; set; }
        public List<ComboboxItemDto> Departments { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
    }

    public class CreateOrUpdateOrderTagGroupInput : IInputDto
    {
        public CreateOrUpdateOrderTagGroupInput()
        {
            LocationGroup = new LocationGroupDto();
        }

        [Required]
        public OrderTagGroupEditDto OrderTagGroup { get; set; }

        public LocationGroupDto LocationGroup { get; set; }
        public List<ComboboxItemDto> Departments { get; set; }
    }

    public class OrderTagInput
    {
        public int? CategoryId { get; set; }
        public int? MenuItemId { get; set; }
    }

    public class OrderTagReportOutput
    {
        public PagedResultOutput<OrderTagReportDto> Data { get; set; }
        public DashboardOrderDto DashBoardOrderDto { get; set; }
    }

    public class OrderExchangeReportOutput
    {
        public PagedResultOutput<OrderExchangeReportDto> Data { get; set; }
        public DashboardOrderDto DashBoardOrderDto { get; set; }
    }

    public class OrderTagReportDto
    {
        public int TicketId { get; set; }
        public string TicketNo { get; set; }
        public string LocationCode { get; set; }
        public int OrderId { get; set; }
        public string OrderTagGroup { get; set; }
        public string OrderTagName { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Total { get; set; }
        public string User { get; set; }
    }

    public class OrderExchangeReportDto
    {
        public int TicketId { get; set; }
        public string TicketNo { get; set; }
        public string LocationCode { get; set; }
        public int OrderId { get; set; }
        public string MenuItemName { get; set; }
        public string OrderExchange { get; set; }
        public string OrderExchange2 { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Total { get; set; }
        public string User { get; set; }
    }



    [Serializable]
    public class GetOrderTagReportInput : PagedAndSortedInputDto, IShouldNormalize, IFileExport
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool RunInBackground { get; set; }
        public string ReportDescription { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public string ReportName { get; set; }
        public int BackGroundId { get; set; }
        public ExportType ExportOutputType { get; set; }
        public PaperSize PaperSize { get; set; }
        public bool Portrait { get; set; }
        public List<int> OrderTagGroupIds { get; set; }
        public string DynamicFilter { get; set; }
        public void Normalize()
        {
            if (Sorting.IsNullOrWhiteSpace())
            {
                Sorting = "Order.Ticket.TicketNumber";
            }
            if (Sorting.Contains("ticketNo"))
            {
                Sorting = Sorting.Replace("ticketNo", "Order.Ticket.TicketNumber");
            }
            if (Sorting.Contains("locationCode"))
            {
                Sorting = Sorting.Replace("locationCode", "Order.Ticket.Ticket.Location.Code");
            }
            if (Sorting.Contains("orderId"))
            {
                Sorting = Sorting.Replace("orderId", "Order.Id");
            }
            if (Sorting.Contains("orderTagGroup"))
            {
                Sorting = Sorting.Replace("orderTagGroup", "TagName");
            }
            if (Sorting.Contains("orderTagName"))
            {
                Sorting = Sorting.Replace("orderTagName", "TagValue");
            }
            if (Sorting.Contains("user"))
            {
                Sorting = Sorting.Replace("user", "Order.Ticket.LastModifiedUserName");
            }
        }
    }

    [Serializable]
    public class GetOrderExchangeReportInput : PagedAndSortedInputDto, IShouldNormalize, IFileExport
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool RunInBackground { get; set; }
        public string ReportDescription { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public string ReportName { get; set; }
        public int BackGroundId { get; set; }
        public ExportType ExportOutputType { get; set; }
        public PaperSize PaperSize { get; set; }
        public bool Portrait { get; set; }
        public List<int> OrderTagGroupIds { get; set; }
        public string DynamicFilter { get; set; }
        public void Normalize()
        {
            if (Sorting.IsNullOrWhiteSpace())
            {
                Sorting = "Ticket.TicketNumber";
            }
            if (Sorting.Contains("ticketNo"))
            {
                Sorting = Sorting.Replace("ticketNo", "Ticket.TicketNumber");
            }
            if (Sorting.Contains("locationCode"))
            {
                Sorting = Sorting.Replace("locationCode", "Ticket.Ticket.Location.Code");
            }
            if (Sorting.Contains("orderId"))
            {
                Sorting = Sorting.Replace("orderId", "Id");
            }
            if (Sorting.Contains("orderTagName"))
            {
                Sorting = Sorting.Replace("orderTagName", "OrderLog");
            }
            if (Sorting.Contains("user"))
            {
                Sorting = Sorting.Replace("user", "Ticket.LastModifiedUserName");
            }
        }

        public class GetOrderTagLocationPriceForEditOutput
        {
            public OrderTagLocationPriceEditDto OrderTagLocationPrice { get; set; }
        }
        [AutoMapTo(typeof(OrderTagLocationPrice))]
        public class OrderTagLocationPriceEditDto : CreationAuditedEntity
        {

            public int? Id { get; set; }
            public int OrderTagId { get; set; }
            public int LocationId { get; set; }
            public string LocationName { get; set; }
            public decimal Price { get; set; }
        }
        public class CreateOrderTagLocationPrice : IInputDto
        {
            [Required] public OrderTagLocationPriceEditDto OrderTagLocationPrice { get; set; }
        }
        [AutoMapTo(typeof(OrderTagLocationPrice))]
        public class OrderTagLocationPriceListDto : CreationAuditedEntity
        {
            public int OrderTagId { get; set; }
            public int LocationId { get; set; }
            public string LocationName { get; set; }
            public decimal Price { get; set; }
        }
        public class GetOrderTagInput : IInputDto, IPagedResultRequest, ISortedResultRequest, IShouldNormalize
        {
            public string Filter { get; set; }

            public string Operation { get; set; }

            public bool IsDeleted { get; set; }

            public void Normalize()
            {
                if (string.IsNullOrEmpty(Sorting))
                {
                    Sorting = "Id";
                }
            }

            public int TagId { get; set; }
            public int OrderTagId { get; set; }
            public int MaxResultCount { get; set; }
            public int SkipCount { get; set; }
            public string Sorting { get; set; }
            public string Location { get; set; }

        }
        public class ImportOrderTagGroupDto
        {
            public decimal TagPrice { get; set; }
            public int TagQuantity { get; set; }
            public string MenuItem { get; set; }
            public string TagAliasName { get; set; }
            public string Tag { get; set; }
            public bool TaxFree { get; set; }
            public bool AddTagPriceToOrder { get; set; }
            public bool SaveFreeTags { get; set; }
            public bool FreeTagging { get; set; }
            public string Menu { get; set; }
            public string Category { get; set; }
            public int Maximum { get; set; }
            public int Minimum { get; set; }
            public int? MaximumEx { get; set; }
            public int? MinimumEx { get; set; }
            public string Departments { get; set; }
            public string TagGroupName { get; set; }
            public int? Id { get; set; }

            public int IndexEx { get; set; }
        }
    }
}