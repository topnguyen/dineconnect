﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.TopDownSellReport.Dto
{
    public class TopDownSellBySummaryOutput
    {
        public string LocationCode { get; set; }
        public string LocationName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public List<TopDownSellItemDto> ListItem { get; set; }
        public decimal TotalQuantity => ListItem.Sum(x => x.Quantity);
        public decimal TotalAmount => ListItem.Sum(x => x.Amount);
        public decimal TotalPercentQuantity => ListItem.Sum(x => x.QuantityPercent);
        public decimal TotalPercentAmount => ListItem.Sum(x => x.AmountPercent);
        public List<ChartOutputDto> DataChart { get; set; }
        public PagedResultOutput<TopDownSellItemDto> DataTable { get; set; }
        public List<ConditionShift> ListShift { get; set; }
        public TopDownSellBySummaryOutput()
        {
            ListItem = new List<TopDownSellItemDto>();
            DataChart = new List<ChartOutputDto>();
            ListShift = new List<ConditionShift>();
        }
        
    }
    public class TopDownSellItemDto
    {
        public string PlantCode { get; set; }
        public string PlantName { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Quantity { get; set; }
        public decimal Amount { get; set; }
        //public decimal Amount => UnitPrice * Quantity;
        public decimal TotalQuantity { get; set; }
        public decimal TotalAmount { get; set; }

        public decimal QuantityPercent
        {
            get
            {
                var result = 0M;
                if(TotalQuantity != 0M)
                {
                    result = (Quantity / TotalQuantity )*100;
                }
                return result;
            }
        }

        public decimal AmountPercent
        {
            get
            {
                var result = 0M;
                if (TotalAmount != 0M)
                {
                    result = (Amount / TotalAmount) * 100;
                }
                return result;
            }
        }   
        public decimal AmountShift { get; set; }
        public decimal QuantityShift { get; set; }
        public List<ItemByShiftDto> ListItemShift = new List<ItemByShiftDto>(); 

    }

    public class ItemByShiftDto
    {
        public virtual int StartHour { get; set; }
        public virtual int StartMinute { get; set; }
        public virtual int EndHour { get; set; }
        public virtual int EndMinute { get; set; }
        public double Start_Number => Convert.ToDouble(StartHour + StartMinute / 60);
        public double End_Number => Convert.ToDouble(EndHour + EndMinute / 60);
        public string Start_String => StartHour + ":" + StartMinute;
        public string End_String => EndHour + ":" + EndMinute;

        public decimal TotalQuantityShift { get; set; }
        public decimal TotalAmountShift { get; set; }
        public decimal AmountShift { get; set; }
        public decimal QuantityShift { get; set; }

        public decimal QuantityShiftPercent
        {
            get
            {
                var result = 0M;
                if (TotalQuantityShift != 0M)
                {
                    result = (QuantityShift / TotalQuantityShift) * 100;
                }
                return result;
            }
        }

        public decimal AmountShiftPercent
        {
            get
            {
                var result = 0M;
                if (TotalAmountShift != 0M)
                {
                    result = (AmountShift / TotalAmountShift) * 100;
                }
                return result;
            }
        }
    }


    public class TopDownSellByPlantOutput
    {
        public string Location { get; set; }
        public string LocationCode { get; set; }
        public TopDownSellBySummaryOutput ListItem { get; set; }
        public TopDownSellByPlantOutput()
        {
            ListItem = new TopDownSellBySummaryOutput();
        }
    }
    public class AllTopDownSellByPlantOutput
    {
        public string LocationCode { get; set; }
        public string LocationName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public decimal AllTotalAmount => ListItem.Sum(x => x.ListItem.TotalAmount);
        public decimal AllTotalQuantity => ListItem.Sum(x => x.ListItem.TotalQuantity);
        public decimal AllTotalAmountPercent => 100;
        public decimal AllTotalQuantityPercent => 100;
        public List<TopDownSellByPlantOutput> ListItem { get; set; }
        public AllTopDownSellByPlantOutput()
        {
            ListItem = new List<TopDownSellByPlantOutput>();
        }
    }
    public class ConditionShift
    {
        public virtual int StartHour { get; set; }
        public virtual int StartMinute { get; set; }
        public virtual int EndHour { get; set; }
        public virtual int EndMinute { get; set; }
        public double Start_Number => Convert.ToDouble(StartHour + StartMinute / 60);
        public double End_Number => Convert.ToDouble(EndHour + EndMinute / 60);
        public string Start_String 
        {
            get
            {
                var result = "";
                if(StartMinute > 10) {
                    result = StartHour + ":" + StartMinute;
                } else
                {
                    result = StartHour + ":" + "0" + StartMinute;
                }
                return result;
            }
        }

        public string End_String
        {
            get
            {
                var result = "";
                if (EndMinute > 10)
                {
                    result = EndHour + ":" + EndMinute;
                }
                else
                {
                    result = EndHour + ":" + "0" + EndMinute;
                }
                return result;
            }
        }
    }
}
