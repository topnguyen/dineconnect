﻿using Abp.Application.Services.Dto;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System;
using Abp.Configuration;
using System.Data.Entity;
using DinePlan.DineConnect.Connect.Custom.Exporting;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.TopDownSellReport.Exporting;
using DinePlan.DineConnect.Connect.TopDownSellReport.Dto;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using System.Data.Entity.Core.Objects;
using DinePlan.DineConnect.Report;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Configuration.Tenants;

namespace DinePlan.DineConnect.Connect.TopDownSellReport
{
    public class TopDownSellReportAppService : DineConnectAppServiceBase, ITopDownSellReportAppService
    {
        private readonly IRepository<Company> _companyRe;
        private readonly IRepository<Master.Department> _dRepo;
        private readonly ILocationAppService _locService;
        private readonly IRepository<Master.Location> _lRepo;
        private readonly IRepository<Transaction.Ticket> _ticketRepository;
        private readonly IRepository<Transaction.Order> _orderManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IReportBackgroundAppService _rbas;
        private readonly IBackgroundJobManager _bgm;
        private readonly ITopDownSellReportExporter _exporter;
        private readonly IRepository<Period.WorkPeriod> _workPeriod;
        private readonly IRepository<PaymentType> _payRe;
        private readonly IRepository<MenuItem> _menuItemRepository;
        private readonly IReportBackgroundAppService _reportBackgroundService;
        private readonly IBackgroundJobManager _backgroundJobManager;
        private readonly SettingManager _settingManager;
        private readonly ITenantSettingsAppService _tenantSettingsService;
        
        public TopDownSellReportAppService(
            IRepository<Transaction.Ticket> ticketRepository,
            ILocationAppService locService,
            IRepository<Company> comR,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<Master.Department> drepo,
            IRepository<Master.Location> lRepo,
            IRepository<Transaction.Order> orderManager,
             IReportBackgroundAppService rbas,
             IBackgroundJobManager bgm,
             ITopDownSellReportExporter exporter,
             IRepository<Period.WorkPeriod> workPeriod,
             IRepository<PaymentType> payRe,
             IBackgroundJobManager backgroundJobManager,
             IReportBackgroundAppService reportBackgroundService,
             IRepository<MenuItem> menuItemRepository,
             SettingManager settingManager,
             ITenantSettingsAppService tenantSettingsService
            )
        {
            _ticketRepository = ticketRepository;
            _companyRe = comR;
            _unitOfWorkManager = unitOfWorkManager;
            _dRepo = drepo;
            _locService = locService;
            _lRepo = lRepo;
            _orderManager = orderManager;
            _rbas = rbas;
            _bgm = bgm;
            _exporter = exporter;
            _workPeriod = workPeriod;
            _payRe = payRe;
            _menuItemRepository = menuItemRepository;
            _reportBackgroundService = reportBackgroundService;
            _backgroundJobManager = backgroundJobManager;
            ShiftListTotal = new List<ItemByShiftDto>();
            _settingManager = settingManager;
            _tenantSettingsService = tenantSettingsService;
        }
        const string Amount = "Amount";
        const string Quantity = "Quantity";
        private List<ItemByShiftDto> ShiftListTotal { get; set; }
        #region Top Down Sell By Quantity
        [Obsolete]
        public async Task<TopDownSellBySummaryOutput> GetTopDownSellReportByQuantity(TopDownSellReportInput input)
        {
            var reuslt = await GetTopDownSellReportCommon(input, Quantity);
            return reuslt;
        }
        public async Task<FileDto> GetTopDownSellReportByQuantityReportToExport(TopDownSellReportInput input)
        {
            input.TenantId = AbpSession.TenantId.Value;
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _reportBackgroundService.CreateOrUpdate(new Report.Dtos.CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.TOPDOWNSELLQUANTITYSUMMARY,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                    {
                        var topDownExportInput = input;
                        topDownExportInput.UserId = AbpSession.UserId.Value;

                        await _backgroundJobManager.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.TOPDOWNSELLQUANTITYSUMMARY,
                            TopDownSellReportInput = topDownExportInput,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                    }
                }
                else
                {
                    return await _exporter.ExportTopDownSellByQuantitySummaryReport(input, this);
                }
            }
            return null;
        }


        #endregion

        #region Top Down By amount summary
        [Obsolete]
        public async Task<TopDownSellBySummaryOutput> GetTopDownSellReportByAmount(TopDownSellReportInput input)
        {
            var reuslt = await GetTopDownSellReportCommon(input, Amount);
            return reuslt;
        }

        public async Task<FileDto> GetTopDownSellReportByAmountReportToExport(TopDownSellReportInput input)
        {
            input.TenantId = AbpSession.TenantId.Value;
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _reportBackgroundService.CreateOrUpdate(new Report.Dtos.CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.TOPDOWNSELLAMOUNTSUMMARY,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                    {
                        var topDownExportInput = input;
                        topDownExportInput.UserId = AbpSession.UserId.Value;

                        await _backgroundJobManager.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.TOPDOWNSELLAMOUNTSUMMARY,
                            TopDownSellReportInput = topDownExportInput,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                    }
                }
                else
                {
                    return await _exporter.ExportTopDownSellByAmountSummaryReport(input, this);
                }
            }
            return null;
        }
        #endregion

        [Obsolete]
        public async Task<List<TopDownSellItemDto>> GetListTopDownSellItem(TopDownSellReportInput input)
        {
            var result = new List<TopDownSellItemDto>();
         
            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                var listShift = GetListShift(input);
                var orders = GetAllOrdersByCondition(input);
                var output = from c in orders
                             group c by new { c.MenuItemPortionId, c.MenuItemId }
                             into g
                             select new { MenuItem = g.Key.MenuItemId, MenuItemPortion = g.Key.MenuItemPortionId, Items = g };

                foreach (var item in output)
                {
                    decimal amount = 0;
                    decimal quantity = 0;
                    var firObj = item.Items.FirstOrDefault();

                    if (firObj != null)
                    {
                        foreach (var order in item.Items.ToList())
                        {
                            amount += order.Price * order.Quantity;
                            quantity += order.Quantity;
                        }
                        var menuItem = await _menuItemRepository.FirstOrDefaultAsync(item.MenuItem);
                        if (menuItem != null)
                        {
                            if (input.IsAddOns == true)
                            {
                                if (!string.IsNullOrEmpty(menuItem.AddOns))
                                {
                                    result.Add(new TopDownSellItemDto
                                    {
                                        ProductCode = menuItem.AliasCode,
                                        ProductName = !string.IsNullOrEmpty(menuItem.Name) ? menuItem.Name : menuItem.AliasName,
                                        UnitPrice = firObj.Price,
                                        Quantity = item.Items.Sum(a => a.Quantity),
                                        Amount = amount,
                                        ListItemShift = GetListItemShift(listShift, item.Items )
                                    }) ;
                                }
                            }
                            else
                            {
                                result.Add(new TopDownSellItemDto
                                {
                                    ProductCode = menuItem.AliasCode,
                                    ProductName = !string.IsNullOrEmpty(menuItem.Name) ? menuItem.Name : menuItem.AliasName,
                                    UnitPrice = firObj.Price,
                                    Quantity = item.Items.Sum(a => a.Quantity),
                                    Amount = amount,
                                    ListItemShift = GetListItemShift(listShift, item.Items )
                                });
                            }

                        }
                    }
                }
                var dataAsQueryable = result.AsQueryable();
                if (!string.IsNullOrEmpty(input.DynamicFilter))
                {
                    var jsonSerializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver =
                            new CamelCasePropertyNamesContractResolver()
                    };
                    var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                    if (filRule?.Rules != null) dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                }
                result = dataAsQueryable.ToList();
            }
            return result;
        }
        [Obsolete]
        public async Task<TopDownSellBySummaryOutput> GetTopDownSellReportCommon(TopDownSellReportInput input, string order)
        {
            var reuslt = new TopDownSellBySummaryOutput();
            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                var setting = await _tenantSettingsService.GetAllSettings();
                input.DatetimeFormat = setting.Connect.DateTimeFormat;
                input.DateFormat = setting.Connect.SimpleDateFormat;
                var listDate = await GetListTopDownSellItem(input);
                reuslt.ListItem = listDate;
                var sortingChart = input.View == "Top" ? order + " " + "desc" : order + " " + "asc";
                // Data for table 
                var dataOutputTable = listDate.AsQueryable().OrderBy(sortingChart).PageBy(input).Take(input.ViewNumber).ToList();
                var totalQuantity = dataOutputTable.Sum(x => x.Quantity);
                var totalAmount = dataOutputTable.Sum(x => x.Amount);
                var totalCount = dataOutputTable.Count;
                foreach (var item in dataOutputTable)
                {
                    item.TotalQuantity = totalQuantity;
                    item.TotalAmount = totalAmount;
                }
                reuslt.DataTable = new PagedResultOutput<TopDownSellItemDto>(
                    totalCount,
                    dataOutputTable
                 );

                // Build chart 

                var listItemChart = listDate.OrderBy(sortingChart).Take(input.ViewNumber).ToList();
                var listDataChart = new List<ChartOutputDto>();
                if (order == Amount)
                {
                    foreach (var item in listItemChart)
                    {
                        listDataChart.Add(new ChartOutputDto
                        {
                            name = item.ProductName,
                            y = item.Amount
                        });
                    }
                    reuslt.DataChart = listDataChart;
                }
                else if (order == Quantity)
                {
                    foreach (var item in listItemChart)
                    {
                        listDataChart.Add(new ChartOutputDto
                        {
                            name = item.ProductName,
                            y = item.Quantity
                        });
                    }
                    reuslt.DataChart = listDataChart;
                }
            }
            reuslt.StartDate = input.StartDate.ToString(input.DateFormat);
            reuslt.EndDate = input.EndDate.ToString(input.DateFormat);
            if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                if (locations.Count == 1)
                {
                    foreach (var loc in locations)
                    {
                        var findLoc = await _locService.GetLocationById(new EntityDto { Id = loc });
                        if (findLoc != null)
                        {
                            reuslt.LocationCode = findLoc.Code;
                            reuslt.LocationName = findLoc.Name;
                        }
                    }
                }
            }          
            return reuslt;
        }

        #region Top Down By amount plant
        [Obsolete]
        public async Task<AllTopDownSellByPlantOutput> GetTopDownSellReportByAmountPlant(TopDownSellReportInput input)
        {
            var setting = await _tenantSettingsService.GetAllSettings();
            input.DatetimeFormat = setting.Connect.DateTimeFormat;
            input.DateFormat = setting.Connect.SimpleDateFormat;
            var resultItem = await GetListTopDownSellItemPlant(input, Amount);
            var resultAll = new AllTopDownSellByPlantOutput();
            resultAll.ListItem = resultItem;
            resultAll.StartDate = input.StartDate.ToString(input.DateFormat);
            resultAll.EndDate = input.EndDate.ToString(input.DateFormat);
            if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                if (locations.Count == 1)
                {
                    foreach (var loc in locations)
                    {
                        var findLoc = await _locService.GetLocationById(new EntityDto { Id = loc });
                        if (findLoc != null)
                        {
                            resultAll.LocationCode = findLoc.Code;
                            resultAll.LocationName = findLoc.Name;
                        }
                    }
                }
            }          
            return resultAll;
        }

        public async Task<FileDto> GetTopDownSellReportByAmountPlantReportToExport(TopDownSellReportInput input)
        {
            input.TenantId = AbpSession.TenantId.Value;
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _reportBackgroundService.CreateOrUpdate(new Report.Dtos.CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.TOPDOWNSELLAMOUNTPLANT,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                    {
                        var topDownExportInput = input;
                        topDownExportInput.UserId = AbpSession.UserId.Value;

                        await _backgroundJobManager.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.TOPDOWNSELLAMOUNTPLANT,
                            TopDownSellReportInput = topDownExportInput,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                    }
                }
                else
                {
                    return await _exporter.ExportTopDownSellByAmountPlantReport(input, this);
                }
            }
            return null;
        }
        #endregion
        #region Top Down By quantity plant
        [Obsolete]
        public async Task<AllTopDownSellByPlantOutput> GetTopDownSellReportByQuantityPlant(TopDownSellReportInput input)
        {
            var setting = await _tenantSettingsService.GetAllSettings();
            input.DatetimeFormat = setting.Connect.DateTimeFormat;
            input.DateFormat = setting.Connect.SimpleDateFormat;
            var resultItem = await GetListTopDownSellItemPlant(input, Quantity);
            var resultAll = new AllTopDownSellByPlantOutput();
            resultAll.ListItem = resultItem;
            resultAll.StartDate = input.StartDate.ToString(input.DateFormat);
            resultAll.EndDate = input.EndDate.ToString(input.DateFormat);
            if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                if (locations.Count == 1)
                {
                    foreach (var loc in locations)
                    {
                        var findLoc = await _locService.GetLocationById(new EntityDto { Id = loc });
                        if (findLoc != null)
                        {
                            resultAll.LocationCode = findLoc.Code;
                            resultAll.LocationName = findLoc.Name;
                        }
                    }
                }
            }           
            return resultAll;
        }

        public async Task<FileDto> GetTopDownSellReportByQuantityPlantReportToExport(TopDownSellReportInput input)
        {
            input.TenantId = AbpSession.TenantId.Value;
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _reportBackgroundService.CreateOrUpdate(new Report.Dtos.CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.TOPDOWNSELLQUANTITYPLANT,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                    {
                        var topDownExportInput = input;
                        topDownExportInput.UserId = AbpSession.UserId.Value;

                        await _backgroundJobManager.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.TOPDOWNSELLQUANTITYPLANT,
                            TopDownSellReportInput = topDownExportInput,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                    }
                }
                else
                {
                    return await _exporter.ExportTopDownSellByQuantityPlantReport(input, this);
                }
            }
            return null;
        }
        #endregion

        [Obsolete]
        public async Task<List<TopDownSellByPlantOutput>> GetListTopDownSellItemPlant(TopDownSellReportInput input, string orderby)
        {
            var listAllPlant = new List<TopDownSellByPlantOutput>();
            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                var listShift = GetListShift(input);
                var orders = GetAllOrdersByCondition(input);
                var orderLoc = orders.GroupBy(s => s.Location_Id);
                var sortingChart = input.View == "Top" ? orderby + " " + "desc" : orderby + " " + "asc";
                foreach (var loc in orderLoc)
                {
                    var listItemInPlant = new TopDownSellBySummaryOutput();
                    var result = new List<TopDownSellItemDto>();
                    var findLoc = await _locService.GetLocationById(new EntityDto { Id = loc.Key });
                    if (findLoc != null)
                    {
                        var output = from c in loc
                                     group c by new { c.MenuItemId, c.MenuItemPortionId }
                                     into g
                                     select new { MenuItem = g.Key.MenuItemId, MenuItemPortion = g.Key.MenuItemPortionId, Items = g };

                        foreach (var item in output)
                        {
                            decimal amount = 0;
                            var firObj = item.Items.FirstOrDefault();
                            if (firObj != null)
                            {
                                foreach (var order in item.Items.ToList())
                                {
                                    amount += order.Price * order.Quantity;
                                }
                                var menuItem = await _menuItemRepository.FirstOrDefaultAsync(item.MenuItem);
                                if (menuItem != null)
                                {
                                    if (input.IsAddOns == true)
                                    {
                                        if (!string.IsNullOrEmpty(menuItem.AddOns))
                                        {
                                            result.Add(new TopDownSellItemDto
                                            {
                                                PlantCode = findLoc.Code,
                                                PlantName = findLoc.Name,
                                                ProductCode = menuItem.AliasCode,
                                                ProductName = !string.IsNullOrEmpty(menuItem.Name) ? menuItem.Name : menuItem.AliasName,
                                                UnitPrice = firObj.Price,
                                                Quantity = item.Items.Sum(a => a.Quantity),
                                                Amount = amount,
                                                ListItemShift = GetListItemShift(listShift, item.Items)
                                            }) ;
                                        }
                                    }
                                    else
                                    {
                                        result.Add(new TopDownSellItemDto
                                        {
                                            PlantCode = findLoc.Code,
                                            PlantName = findLoc.Name,
                                            ProductCode = menuItem.AliasCode,
                                            ProductName = !string.IsNullOrEmpty(menuItem.Name) ? menuItem.Name : menuItem.AliasName,
                                            UnitPrice = firObj.Price,
                                            Quantity = item.Items.Sum(a => a.Quantity),
                                            Amount = amount,
                                            ListItemShift = GetListItemShift(listShift, item.Items)
                                        });
                                    }
                                }
                            }
                        }
                        var dataAsQueryable = result.AsQueryable();
                        if (!string.IsNullOrEmpty(input.DynamicFilter))
                        {
                            var jsonSerializerSettings = new JsonSerializerSettings
                            {
                                ContractResolver =
                                    new CamelCasePropertyNamesContractResolver()
                            };
                            var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                            if (filRule?.Rules != null) dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                        }
                        if(input.ViewNumber == 0 )
                        {
                            result = dataAsQueryable.OrderBy(sortingChart).ToList();
                            var listDownTop = await GetTopDownSellReportPlantItem(input, orderby, result);
                            listDownTop.ListItem.OrderBy(sortingChart);
                            listAllPlant.Add(new TopDownSellByPlantOutput
                            {
                                LocationCode = findLoc.Code,
                                Location = findLoc.Name,
                                ListItem = listDownTop
                            });
                        }
                        else
                        {
                            result = dataAsQueryable.OrderBy(sortingChart).Take<TopDownSellItemDto>(input.ViewNumber).ToList();
                            var listDownTop = await GetTopDownSellReportPlantItem(input, orderby, result);
                            listDownTop.ListItem.OrderBy(sortingChart).Take(input.ViewNumber);
                            listAllPlant.Add(new TopDownSellByPlantOutput
                            {
                                LocationCode = findLoc.Code,
                                Location = findLoc.Name,
                                ListItem = listDownTop
                            });
                        }                      
                    }
                }
            }
            return listAllPlant;
        }
        [Obsolete]
        public Task<TopDownSellBySummaryOutput> GetTopDownSellReportPlantItem(TopDownSellReportInput input, string order, List<TopDownSellItemDto> listDownTop)
        {
            var reuslt = new TopDownSellBySummaryOutput();
            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                var listDate = listDownTop;
                reuslt.ListItem = listDate;
                // Data for table 
                var dataOutputTable = new List<TopDownSellItemDto>();
                if (input.ViewNumber == 0)
                {
                    dataOutputTable = listDate.AsQueryable().ToList();
                }
                else
                {
                    dataOutputTable = listDate.AsQueryable().Take(input.ViewNumber).ToList();
                }               
                var totalCount = dataOutputTable.Count;
                var totalQuantity = dataOutputTable.Sum(x => x.Quantity);
                var totalAmount = dataOutputTable.Sum(x => x.Amount);
                foreach (var item in dataOutputTable)
                {
                    item.TotalQuantity = totalQuantity;
                    item.TotalAmount = totalAmount;
                }
                reuslt.DataTable = new PagedResultOutput<TopDownSellItemDto>(
                    totalCount,
                    dataOutputTable
                 );

                // Build chart 
                var sortingChart = input.View == "Top" ? order + " " + "desc" : order + " " + "asc";
                var listItemChart = new List<TopDownSellItemDto>();
                if (input.ViewNumber == 0)
                {
                    listItemChart = listDate.OrderBy(sortingChart).ToList();
                }
                else
                {
                    listItemChart = listDate.OrderBy(sortingChart).Take(input.ViewNumber).ToList();
                }
                var listDataChart = new List<ChartOutputDto>();
                if (order == Amount)
                {
                    foreach (var item in listItemChart)
                    {
                        listDataChart.Add(new ChartOutputDto
                        {
                            name = item.ProductName,
                            y = item.Amount
                        });
                    }
                    reuslt.DataChart = listDataChart;
                }
                else if (order == Quantity)
                {
                    foreach (var item in listItemChart)
                    {
                        listDataChart.Add(new ChartOutputDto
                        {
                            name = item.ProductName,
                            y = item.Quantity
                        });
                    }
                    reuslt.DataChart = listDataChart;
                }
            }
            return Task.FromResult(reuslt);
        }

        [Obsolete]
        public IQueryable<Order> GetAllOrdersByCondition(TopDownSellReportInput input)
        {
            DateTime endDate = input.EndDate.AddDays(1);
            var tickets = _ticketRepository.GetAll();
            tickets = tickets.Where(a => a.LastPaymentTime >= input.StartDate && a.LastPaymentTime <= endDate);

            tickets = tickets.Where(x => (x.LastPaymentTime.Hour * 60 + x.LastPaymentTime.Minute) >= (input.StartHour * 60 + input.StartMinute)
                                            && (x.LastPaymentTime.Hour * 60 + x.LastPaymentTime.Minute) <= (input.EndHour * 60 + input.EndMinute));

            if (!string.IsNullOrEmpty(input.Days))
            {
                DateTime firstSunday = new DateTime(1753, 1, 7);
                tickets = tickets.Where(s => (input.Days == "1" && EntityFunctions.DiffDays(firstSunday, s.LastPaymentTime) % 7 == 1)
                || (input.Days == "2" && EntityFunctions.DiffDays(firstSunday, s.LastPaymentTime) % 7 == 2)
                 || (input.Days == "3" && EntityFunctions.DiffDays(firstSunday, s.LastPaymentTime) % 7 == 3)
                  || (input.Days == "4" && EntityFunctions.DiffDays(firstSunday, s.LastPaymentTime) % 7 == 4)
                   || (input.Days == "5" && EntityFunctions.DiffDays(firstSunday, s.LastPaymentTime) % 7 == 5)
                    || (input.Days == "6" && EntityFunctions.DiffDays(firstSunday, s.LastPaymentTime) % 7 == 6)
                     || (input.Days == "0" && EntityFunctions.DiffDays(firstSunday, s.LastPaymentTime) % 7 == 0));
            }

            if (input.Location > 0)
            {
                tickets = tickets.Where(a => a.LocationId == input.Location);
            }
            else if (input.Locations != null && input.Locations.Any())
            {
                var locations = input.Locations.Select(a => a.Id).ToList();
                tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
                     && !input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                if (locations.Any()) tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true
                });
                tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }

            if (input.LocationGroup != null)
            {
                if (input.LocationGroup.NonLocations != null && input.LocationGroup.NonLocations.Any())
                {
                    var nonlocations = input.LocationGroup.NonLocations.Select(a => a.Id).ToList();
                    tickets = tickets.Where(a => !nonlocations.Contains(a.LocationId));
                }
            }

            else if (input.LocationGroup == null)
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = new List<SimpleLocationDto>(),
                    Group = false,
                    UserId = input.UserId
                });
                if (input.UserId > 0)
                    tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }

            var orders = tickets.SelectMany(a => a.Orders);
            if (input.IsCombo == false)
            {
                orders = orders.Where(s => s.MenuItemType == 0);
            }
            if (!string.IsNullOrEmpty(input.DepartmentSelect))
            {
                int x = 0;
                if (int.TryParse(input.DepartmentSelect, out x))
                {
                    var findDepartment = _dRepo.GetAll().FirstOrDefault(s => s.Id == x);
                    if (findDepartment != null)
                        orders = orders.Where(s => s.DepartmentName == findDepartment.Name);
                }
            }          
            if (input.Locations.Any())
            {
                var locationIds = input.Locations.Select(l => l.Id).ToList();
                orders = orders.Where(o => locationIds.Contains(o.Location_Id));
            }

            if (input.MenuItemIds != null && input.MenuItemIds.Any())
                orders = orders
                    .WhereIf(input.MenuItemIds.Any(), o => input.MenuItemIds.Contains(o.MenuItemId));

            if (input.ListMaterial != null && input.ListMaterial.Any())
                orders = orders
                    .WhereIf(input.ListMaterial.Any(), o => input.ListMaterial.Contains(o.MenuItem.Category.ProductGroupId ?? -1));

            if (!string.IsNullOrEmpty(input.LastModifiedUserName))
                orders = orders.Where(a => a.CreatingUserName.Contains(input.LastModifiedUserName));
            return orders;
        }
        public List<ItemByShiftDto> GetListItemShift(List<ConditionShift> listShift, IEnumerable<Order> orders)
        {          
            var allOrderShift = new List<ItemByShiftDto>();
            var subresult = new List<ItemByShiftDto>();
            var result = new List<ItemByShiftDto>();
            foreach (var shift in listShift)
            {            
                decimal amountShift = 0;
                decimal quantityShift = 0;
                foreach(var order in orders)
                {
                    var ticket =  _ticketRepository.FirstOrDefault(s => s.Id == order.TicketId);
                    if (ticket != null)
                    {
                        var endMinute = shift.EndMinute + 1;
                        bool check = (ticket.LastPaymentTime.Hour * 60 + ticket.LastPaymentTime.Minute) >= (shift.StartHour * 60 + shift.StartMinute)
                                            && (ticket.LastPaymentTime.Hour * 60 + ticket.LastPaymentTime.Minute) <= (shift.EndHour * 60 + endMinute);
                        if (check)
                        {
                            amountShift += order.Quantity * order.Price;
                            quantityShift += order.Quantity;
                        }
                    }
                }
                subresult.Add(new ItemByShiftDto
                {
                    AmountShift = amountShift,
                    QuantityShift = quantityShift,   
                    StartHour = shift.StartHour,
                    EndHour = shift.EndHour,
                    StartMinute = shift.StartMinute,
                    EndMinute = shift.EndMinute, 
                }) ;              
            }         
            return subresult;
        }
        public List<ConditionShift> GetListShift(TopDownSellReportInput input)
        {
            var listShift = new List<ConditionShift>();
            var startHour = input.StartHour;
            var endHour = input.EndHour;
            var diffMinute = input.EndMinute - input.StartMinute;
            var startMinute = input.StartMinute;
            var endMinute = input.EndMinute;
            do
            {
                var subStartHour = startHour;
                subStartHour++;
                var qr = (subStartHour == endHour && diffMinute < 0) ? (startMinute + diffMinute) : (59 - startMinute);             
                listShift.Add(new ConditionShift
                {
                    StartHour = startHour,
                    EndHour = startMinute == 0 ? startHour : startHour + 1,
                    StartMinute = startMinute,
                    EndMinute = (subStartHour++ == endHour && diffMinute < 0) ? (startMinute + diffMinute) : (59 - startMinute)
                });
                startHour++;
            }
            while (startHour < endHour);
            if (diffMinute > 0)
            {
                listShift.Add(new ConditionShift
                {
                    StartHour = startHour,
                    EndHour = startHour,
                    StartMinute = startMinute,
                    EndMinute = startMinute + diffMinute
                });
            }
            return listShift;
        }
    }
}
