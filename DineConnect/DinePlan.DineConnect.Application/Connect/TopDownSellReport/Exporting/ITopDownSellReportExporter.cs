﻿using DinePlan.DineConnect.Connect.CashSummaryReport.Dto;
using DinePlan.DineConnect.Connect.TopDownSellReport.Dto;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.TopDownSellReport.Exporting
{
    public interface ITopDownSellReportExporter
    {
        Task<FileDto> ExportTopDownSellByQuantitySummaryReport(TopDownSellReportInput input, ITopDownSellReportAppService appService);

        Task<FileDto> ExportTopDownSellByAmountSummaryReport(TopDownSellReportInput input, ITopDownSellReportAppService appService);

        Task<FileDto> ExportTopDownSellByQuantityPlantReport(TopDownSellReportInput input, ITopDownSellReportAppService appService);

        Task<FileDto> ExportTopDownSellByAmountPlantReport(TopDownSellReportInput input, ITopDownSellReportAppService appService);

    }
}