﻿using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.CashSummaryReport.Dto;
using DinePlan.DineConnect.Connect.Custom.Dto;
using DinePlan.DineConnect.Connect.Customize.Dto;
using DinePlan.DineConnect.Connect.SpeedOfServiceReport.Dto;
using DinePlan.DineConnect.Connect.TopDownSellReport.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.TopDownSellReport.Exporting
{
    public class TopDownSellReportExporter : FileExporterBase, ITopDownSellReportExporter
    {
        #region Quantity Summary
        public async Task<FileDto> ExportTopDownSellByQuantitySummaryReport(TopDownSellReportInput input, ITopDownSellReportAppService appService)
        {
            var builder = new StringBuilder();
            builder.Append(L("TopDownSellReportByQuantitySummary"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append("_");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            if (input.ExportOutputType == 0)
            {
                using (var excelPackage = new ExcelPackage())
                {
                    await GetTopDownSellByQuantitySummaryReportExportExcel(excelPackage, input, appService);
                    Save(excelPackage, file);
                }
                return ProcessFile(input, file);
            }
            else
            {
                var contentHTML = await ExportDatatableToHtml(input, appService);
                var result = ProcessFileHTML(file, contentHTML);
                return result;
            }
        }

        private async Task GetTopDownSellByQuantitySummaryReportExportExcel(ExcelPackage package, TopDownSellReportInput input, ITopDownSellReportAppService appService)
        {
            var worksheet = package.Workbook.Worksheets.Add(L("TopDownSellReportByQuantitySummary"));
            worksheet.OutLineApplyStyle = true;
            input.MaxResultCount = 10000;
            var dtos = await appService.GetTopDownSellReportByQuantity(input);
            var lstData = dtos.DataTable.Items;
            var listShift = GetListShift(input);
            // Add header
            var textTitle = L("TopDownSellReportByQuantitySummary") + "<br/>";
            textTitle += (dtos.StartDate != dtos.EndDate ? L("Date") + " " + dtos.StartDate + " " + L("To") + " " + dtos.EndDate : L("Date") + " " + dtos.StartDate);
            if (!string.IsNullOrEmpty(dtos.LocationName))
            {
                textTitle = textTitle + "<br/>" + dtos.LocationName;
            }

            string textTitleWithNewLine = textTitle.Replace("<br/>", Environment.NewLine);
            int headerCol = 6 + listShift.Count * 2;
            worksheet.Cells[1, 1].Value = textTitleWithNewLine;
            worksheet.Cells[1, 1, 4, headerCol].Merge = true;
            worksheet.Cells[1, 1, 4, headerCol].Style.Font.Bold = true;
            worksheet.Cells[1, 1, 4, headerCol].Style.Font.Size = 11;
            worksheet.Cells[1, 1, 4, headerCol].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 1].Style.Border.BorderAround(ExcelBorderStyle.None);
            worksheet.Cells[1, 1, 4, headerCol].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            worksheet.Cells[1, 1, 4, headerCol].Style.WrapText = true;

            var listheader = new List<string> { L("No."),
                L("ProductCode"),
                L("ProductName"),
                L("UnitPrice"),
                L("TotalQuantity"),
                L("QuantityPercent") };

            var colhead = 1;
            Color colorHeader = System.Drawing.ColorTranslator.FromHtml("#99CCFF");
            foreach (var header in listheader)
            {
                worksheet.Column(colhead).AutoFit();
                worksheet.Cells[5, colhead, 6, colhead].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[5, colhead, 6, colhead].Merge = true;
                worksheet.Cells[5, colhead, 6, colhead].Value = header;
                colhead++;
            }
            for (var i = 1; i < headerCol; i++)
            {
                worksheet.Cells[5, i].Style.WrapText = true;
            }
            int colShift = 7;
            int colHeadShift = 7;
            int colShiftMerge = 8;
            foreach (var shift in listShift.OrderBy(s => s.Start_Number))
            {
                worksheet.Cells[5, colShift, 5, colShiftMerge].Merge = true;
                worksheet.Cells[5, colShift, 5, colShiftMerge].Value = shift.Start_String + " - " + shift.End_String;
                worksheet.Cells[5, colShift, 5, colShiftMerge].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[6, colHeadShift].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[6, colHeadShift++].Value = L("TotalQuantity");
                worksheet.Cells[6, colHeadShift].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[6, colHeadShift++].Value = L("QuantityPercent");
                colShiftMerge += 2;
                colShift += 2;
            }
            var colHeadColor = colShiftMerge - 2;
            worksheet.Cells[5, 1, 6, colHeadColor].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            worksheet.Cells[5, 1, 6, colHeadColor].Style.Font.Bold = true;
            worksheet.Cells[5, 1, 6, colHeadColor].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells[5, 1, 6, colHeadColor].Style.Fill.BackgroundColor.SetColor(colorHeader);
            worksheet.Cells[5, 1, 6, colHeadColor].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells[5, 1, 6, colHeadColor].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            var row = 7;
            var index = 1;
            var listShiftTotal = new List<ItemByShiftDto>();
            foreach (var item in lstData)
            {
                foreach (var itemShift in item.ListItemShift.OrderBy(s => s.Start_Number))
                {
                    listShiftTotal.Add(new ItemByShiftDto
                    {
                        StartHour = itemShift.StartHour,
                        EndHour = itemShift.EndHour,
                        StartMinute = itemShift.StartMinute,
                        EndMinute = itemShift.EndMinute,
                        QuantityShift = itemShift.QuantityShift,
                        AmountShift = itemShift.AmountShift,
                    });
                }
            }
            var colData = 1;
            foreach (var itemDto in lstData)
            {
                worksheet.Cells[row, colData].Value = index;
                worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                colData++;
                worksheet.Cells[row, colData].Value = itemDto.ProductCode;
                worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                colData++;
                worksheet.Cells[row, colData].Value = itemDto.ProductName;
                worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                colData++;
                worksheet.Cells[row, colData].Value = itemDto.UnitPrice.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                colData++;
                worksheet.Cells[row, colData].Value = itemDto.Quantity.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                colData++;
                worksheet.Cells[row, colData].Value = itemDto.QuantityPercent.ToString(ConnectConsts.ConnectConsts.NumberFormat) + "%";
                worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                colData++;
                var colShiftData = 7;

                foreach (var itemShift in itemDto.ListItemShift.OrderBy(s => s.Start_Number))
                {
                    var findTotalShif = listShiftTotal.Where(s => s.Start_Number == itemShift.Start_Number);
                    var totalQuantity = findTotalShif.Sum(s => s.QuantityShift);
                    worksheet.Cells[row, colShiftData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[row, colShiftData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colShiftData++].Value = itemShift.QuantityShift.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    worksheet.Cells[row, colShiftData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[row, colShiftData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colShiftData++].Value = ((totalQuantity != 0 ? (itemShift.QuantityShift / totalQuantity) : 0) * 100).ToString(ConnectConsts.ConnectConsts.NumberFormat) + "%";                 
                }
                colData = 1;
                index++;
                row++;
            }
            // Add footer

            worksheet.Cells[row, 1, row, 4].Merge = true;
            worksheet.Cells[row, 1, row, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            worksheet.Cells[row, 1, row, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            worksheet.Cells[row, 1, row, 4].Value = L("Total");

            worksheet.Cells[row, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            worksheet.Cells[row, 5].Value = lstData.Sum(x => x.Quantity).ToString(ConnectConsts.ConnectConsts.NumberFormat);
            worksheet.Cells[row, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            worksheet.Cells[row, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            worksheet.Cells[row, 6].Value = "100%";
            worksheet.Cells[row, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            int colSumTotal = 7;
            foreach (var shift in listShift.OrderBy(s => s.Start_Number))
            {
                var findTotalShif = listShiftTotal.Where(s => s.Start_Number == shift.Start_Number);
                var totalQuantity = findTotalShif.Sum(s => s.QuantityShift);
                worksheet.Cells[row, colSumTotal].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, colSumTotal].Value = totalQuantity.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                worksheet.Cells[row, colSumTotal].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colSumTotal++;
                worksheet.Cells[row, colSumTotal].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, colSumTotal].Value = (totalQuantity != 0 ? 100 : 0).ToString(ConnectConsts.ConnectConsts.NumberFormat) + "%";
                worksheet.Cells[row, colSumTotal].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colSumTotal++;
            }
            int colSumTotalShift = colSumTotal - 1;
            Color colorFooter = System.Drawing.ColorTranslator.FromHtml("#cccccc");
            worksheet.Cells[row, 1, row, colSumTotalShift].Style.Font.Bold = true;
            worksheet.Cells[row, 1, row, colSumTotalShift].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells[row, 1, row, colSumTotalShift].Style.Fill.BackgroundColor.SetColor(colorFooter);
            worksheet.Cells[4, 1, row, colSumTotalShift].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            for (var i = 1; i <= (6 + listShift.Count * 2); i++)
            {
                worksheet.Column(i).AutoFit();
            }
        }
        private async Task<string> ExportDatatableToHtml(TopDownSellReportInput input, ITopDownSellReportAppService appService)
        {
            input.MaxResultCount = 10000;
            var dtos = await appService.GetTopDownSellReportByQuantity(input);
            var lstData = dtos.DataTable.Items;
            var listShiftTotal = new List<ItemByShiftDto>();
            foreach (var item in lstData)
            {
                foreach (var itemShift in item.ListItemShift.OrderBy(s => s.Start_Number))
                {
                    listShiftTotal.Add(new ItemByShiftDto
                    {
                        StartHour = itemShift.StartHour,
                        EndHour = itemShift.EndHour,
                        StartMinute = itemShift.StartMinute,
                        EndMinute = itemShift.EndMinute,
                        QuantityShift = itemShift.QuantityShift,
                        AmountShift = itemShift.AmountShift,
                    });
                }
            }
            var listShift = GetListShift(input);
            var totalCol = 6 + (listShift.Count * 2);
            var businessDate = dtos.StartDate != dtos.EndDate ? L("Date") + " " + dtos.StartDate + " " + L("To") + " " + dtos.EndDate : L("Date") + " " + dtos.StartDate;
            StringBuilder strHTMLBuilder = new StringBuilder();
            strHTMLBuilder.Append("<html>");
            strHTMLBuilder.Append("<head>");
            strHTMLBuilder.Append("</head>");
            var styleTable = GetStyleForTable();
            strHTMLBuilder.Append(styleTable);
            strHTMLBuilder.Append("<body>");
            strHTMLBuilder.Append("<div>");

            //TODO: start add header
            var titleHeader = L("TopDownSellReportByQuantitySummary") + "<br>" + businessDate;
            if (!string.IsNullOrEmpty(dtos.LocationCode) && !string.IsNullOrEmpty(dtos.LocationName))
            {
                titleHeader += "<br>" + "(" + dtos.LocationCode + ")" + dtos.LocationName;
            }
            strHTMLBuilder.Append("<table class='table table-bordered custom_table_date border-bottom-none'>");
            strHTMLBuilder.Append("<tr class='font_14'>");
            strHTMLBuilder.Append($"<td colspan='{totalCol}' class='border-bottom-none text-center font-weight-bold' style='height: 80px;'>{titleHeader}</td>");
            strHTMLBuilder.Append("</tr>");
            strHTMLBuilder.Append("<tr class=' font_14'>");
            strHTMLBuilder.Append($"<th class=' border-bottom-none text-center font-weight-bold'>{L("No.")}</th>");
            strHTMLBuilder.Append($"<th class='border-bottom-none text-center font-weight-bold'>{L("ProductCode")}</th>");
            strHTMLBuilder.Append($"<th class='border-bottom-none text-center font-weight-bold'>{L("ProductName")}</th>");
            strHTMLBuilder.Append($"<th class='border-bottom-none text-center font-weight-bold'>{L("UnitPrice")}</th>");
            strHTMLBuilder.Append($"<th class='border-bottom-none text-center font-weight-bold'>{L("TotalQuantity")}</th>");
            strHTMLBuilder.Append($"<th class='border-bottom-none text-center font-weight-bold'>{L("QuantityPercent")}</th>");

            foreach (var shift in listShift.OrderBy(s => s.Start_Number))
            {
                strHTMLBuilder.Append($"<th class='border-bottom-none text-center font-weight-bold padding-0' colspan='2'>");
                strHTMLBuilder.Append($"<p class='padding-top-15'>{shift.Start_String + '-' + shift.End_String }</p>");
                strHTMLBuilder.Append($"<table class='margin-0'>");
                strHTMLBuilder.Append($"<tr class='border-bottom-none text-center font-weight-bold'>");
                strHTMLBuilder.Append($"<th class='border-bottom-none text-center font-weight-bold border-left-none'>{L("TotalQuantity")}</th>");
                strHTMLBuilder.Append($"<th class='border-bottom-none text-center font-weight-bold border-right-none'>{L("QuantityPercent")}</th>");
                strHTMLBuilder.Append("</tr>");
                strHTMLBuilder.Append("</table>");
                strHTMLBuilder.Append("</th>");
            }
            //end add header
     
            // add content
            var index = 1;
            foreach (var item in lstData)
            {
                strHTMLBuilder.Append("<tr class='font_14'>");
                strHTMLBuilder.Append($"<td class='text-center'> { index}</td> ");
                strHTMLBuilder.Append($"<td class='text-center'> { item.ProductCode}</td> ");
                strHTMLBuilder.Append($"<td class='text-center'> { item.ProductName}</td> ");
                strHTMLBuilder.Append($"<td class='text-center'> { item.UnitPrice.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td> ");
                strHTMLBuilder.Append($"<td class='text-center'> { item.Quantity.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td> ");
                strHTMLBuilder.Append($"<td class='text-center'> { item.QuantityPercent.ToString(ConnectConsts.ConnectConsts.NumberFormat)} %</td> ");

                foreach (var itemShift in item.ListItemShift.OrderBy(s => s.Start_Number))
                {
                    var findTotalShif = listShiftTotal.Where(s => s.Start_Number == itemShift.Start_Number);
                    var totalQuantity = findTotalShif.Sum(s => s.QuantityShift);
                    strHTMLBuilder.Append($"<td class='text-center'> { itemShift.QuantityShift.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td> ");
                    strHTMLBuilder.Append($"<td class='text-center'> { ((totalQuantity != 0 ? (itemShift.QuantityShift / totalQuantity) : 0) * 100).ToString(ConnectConsts.ConnectConsts.NumberFormat)}%</td> ");
                }
                strHTMLBuilder.Append("</tr>");
                index++;
            }
            // end content

            strHTMLBuilder.Append("<tr class='font_14 background-footer'>");
            strHTMLBuilder.Append($"<td colspan='4' class='text-right font-weight-bold'> {L("Total")}</td> ");
            strHTMLBuilder.Append($"<td class = 'font-weight-bold text-center' > { dtos.DataTable.Items.Sum(x => x.Quantity).ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td> ");
            strHTMLBuilder.Append($"<td class = 'font-weight-bold text-center'> {( dtos.DataTable.Items.Sum(x => x.Quantity) != 0 ? 100 : 0).ToString(ConnectConsts.ConnectConsts.NumberFormat)}%</td> ");

            foreach (var shift in listShift.OrderBy(s => s.Start_Number))
            {
                var findTotalShif = listShiftTotal.Where(s => s.Start_Number == shift.Start_Number);
                var totalQuantity = findTotalShif.Sum(s => s.QuantityShift);
                strHTMLBuilder.Append($"<td class='text-center font-weight-bold'> {totalQuantity.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td> ");
                strHTMLBuilder.Append($"<td class='text-center font-weight-bold'> {(totalQuantity != 0 ? 100 : 0).ToString(ConnectConsts.ConnectConsts.NumberFormat)}%</td> ");
            }

            strHTMLBuilder.Append("</tr>");
            strHTMLBuilder.Append("</table>");
            strHTMLBuilder.Append("</div>");
            string Htmltext = strHTMLBuilder.ToString();
            return Htmltext;
        }

        #endregion

        #region Amount Summary
        public async Task<FileDto> ExportTopDownSellByAmountSummaryReport(TopDownSellReportInput input, ITopDownSellReportAppService appService)
        {
            var builder = new StringBuilder();
            builder.Append(L("TopDownSellReportByAmountSummary"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append("_");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            if (input.ExportOutputType == 0)
            {
                using (var excelPackage = new ExcelPackage())
                {
                    await GetTopDownSellByAmountSummaryReportExportExcel(excelPackage, input, appService);
                    Save(excelPackage, file);
                }
                return ProcessFile(input, file);
            }
            else
            {
                var contentHTML = await ExportDatatableAmountToHtml(input, appService);
                var result = ProcessFileHTML(file, contentHTML);
                return result;
            }
        }

        private async Task GetTopDownSellByAmountSummaryReportExportExcel(ExcelPackage package, TopDownSellReportInput input, ITopDownSellReportAppService appService)
        {          
            var worksheet = package.Workbook.Worksheets.Add(L("TopDownSellReportByAmountSummary"));
            worksheet.OutLineApplyStyle = true;
            input.MaxResultCount = 10000;
            var dtos = await appService.GetTopDownSellReportByAmount(input);
            var lstData = dtos.DataTable.Items;
            var listShift = GetListShift(input);
            // Add header
            var textTitle = L("TopDownSellReportByAmountSummary") + "<br/>";
            textTitle += (dtos.StartDate != dtos.EndDate ? L("Date") + " " + dtos.StartDate + " " + L("To") + " " + dtos.EndDate : L("Date") + " " + dtos.StartDate);
            if (!string.IsNullOrEmpty(dtos.LocationName))
            {
                textTitle = textTitle + "<br/>" + dtos.LocationName;
            }

            string textTitleWithNewLine = textTitle.Replace("<br/>", Environment.NewLine);
            int headerCol = 6 + listShift.Count * 2;
            worksheet.Cells[1, 1].Value = textTitleWithNewLine;
            worksheet.Cells[1, 1, 4, headerCol].Merge = true;
            worksheet.Cells[1, 1, 4, headerCol].Style.Font.Bold = true;
            worksheet.Cells[1, 1, 4, headerCol].Style.Font.Size = 11;
            worksheet.Cells[1, 1, 4, headerCol].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 1].Style.Border.BorderAround(ExcelBorderStyle.None);
            worksheet.Cells[1, 1, 4, headerCol].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            worksheet.Cells[1, 1, 4, headerCol].Style.WrapText = true;

            var listheader = new List<string> { L("No."),
                L("ProductCode"),
                L("ProductName"),
                L("UnitPrice"),
                L("TotalAmount"),
                L("AmountPercent") };

            var colhead = 1;
            Color colorHeader = System.Drawing.ColorTranslator.FromHtml("#99CCFF");
            foreach (var header in listheader)
            {
                worksheet.Column(colhead).AutoFit();
                worksheet.Cells[5, colhead, 6, colhead].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[5, colhead, 6, colhead].Merge = true;
                worksheet.Cells[5, colhead, 6, colhead].Value = header;
                colhead++;
            }
            for (var i = 1; i < headerCol; i++)
            {
                worksheet.Cells[5, i].Style.WrapText = true;
            }
            int colShift = 7;
            int colHeadShift = 7;
            int colShiftMerge = 8;
            foreach (var shift in listShift.OrderBy(s => s.Start_Number))
            {
                worksheet.Cells[5, colShift, 5, colShiftMerge].Merge = true;
                worksheet.Cells[5, colShift, 5, colShiftMerge].Value = shift.Start_String + " - " + shift.End_String;
                worksheet.Cells[5, colShift, 5, colShiftMerge].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[6, colHeadShift].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[6, colHeadShift++].Value = L("TotalAmount");
                worksheet.Cells[6, colHeadShift].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[6, colHeadShift++].Value = L("AmountPercent");
                colShiftMerge += 2;
                colShift += 2;
            }
            var colHeadColor = colShiftMerge - 2;
            worksheet.Cells[5, 1, 6, colHeadColor].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            worksheet.Cells[5, 1, 6, colHeadColor].Style.Font.Bold = true;
            worksheet.Cells[5, 1, 6, colHeadColor].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells[5, 1, 6, colHeadColor].Style.Fill.BackgroundColor.SetColor(colorHeader);
            worksheet.Cells[5, 1, 6, colHeadColor].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells[5, 1, 6, colHeadColor].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            var row = 7;
            var index = 1;
            var listShiftTotal = new List<ItemByShiftDto>();
            foreach (var item in lstData)
            {
                foreach (var itemShift in item.ListItemShift.OrderBy(s => s.Start_Number))
                {
                    listShiftTotal.Add(new ItemByShiftDto
                    {
                        StartHour = itemShift.StartHour,
                        EndHour = itemShift.EndHour,
                        StartMinute = itemShift.StartMinute,
                        EndMinute = itemShift.EndMinute,
                        QuantityShift = itemShift.QuantityShift,
                        AmountShift = itemShift.AmountShift,
                    });
                }
            }
            var colData = 1;
            foreach (var itemDto in lstData)
            {
                worksheet.Cells[row, colData].Value = index;
                worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                colData++;
                worksheet.Cells[row, colData].Value = itemDto.ProductCode;
                worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                colData++;
                worksheet.Cells[row, colData].Value = itemDto.ProductName;
                worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                colData++;
                worksheet.Cells[row, colData].Value = itemDto.UnitPrice.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                colData++;
                worksheet.Cells[row, colData].Value = itemDto.Amount.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                colData++;
                worksheet.Cells[row, colData].Value = itemDto.AmountPercent.ToString(ConnectConsts.ConnectConsts.NumberFormat) + "%";
                worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                colData++;
                var colShiftData = 7;

                foreach (var itemShift in itemDto.ListItemShift.OrderBy(s => s.Start_Number))
                {
                    var findTotalShif = listShiftTotal.Where(s => s.StartHour == itemShift.Start_Number);
                    var totalAmount = findTotalShif.Sum(s => s.AmountShift);

                    worksheet.Cells[row, colShiftData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[row, colShiftData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colShiftData++].Value = itemShift.AmountShift.ToString(ConnectConsts.ConnectConsts.NumberFormat);

                    worksheet.Cells[row, colShiftData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[row, colShiftData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colShiftData++].Value = ((totalAmount != 0 ? (itemShift.AmountShift / totalAmount) : 0) * 100).ToString(ConnectConsts.ConnectConsts.NumberFormat) + "%";
                }
                colData = 1;
                index++;
                row++;
            }
            // Add footer

            worksheet.Cells[row, 1, row, 4].Merge = true;
            worksheet.Cells[row, 1, row, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            worksheet.Cells[row, 1, row, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            worksheet.Cells[row, 1, row, 4].Value = L("Total");

            worksheet.Cells[row, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            worksheet.Cells[row, 5].Value = lstData.Sum(x => x.Amount).ToString(ConnectConsts.ConnectConsts.NumberFormat);
            worksheet.Cells[row, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            worksheet.Cells[row, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            worksheet.Cells[row, 6].Value = "100%";
            worksheet.Cells[row, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            int colSumTotal = 7;
            foreach (var shift in listShift.OrderBy(s => s.Start_Number))
            {
                var findTotalShif = listShiftTotal.Where(s => s.Start_Number == shift.Start_Number);
                var totalAmount = findTotalShif.Sum(s => s.AmountShift);
                worksheet.Cells[row, colSumTotal].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, colSumTotal].Value = totalAmount.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                worksheet.Cells[row, colSumTotal].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colSumTotal++;
                worksheet.Cells[row, colSumTotal].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, colSumTotal].Value = (totalAmount != 0 ? 100 : 0 ).ToString(ConnectConsts.ConnectConsts.NumberFormat) + "%";
                worksheet.Cells[row, colSumTotal].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colSumTotal++;
            }
            int colSumTotalShift = colSumTotal - 1;
            Color colorFooter = System.Drawing.ColorTranslator.FromHtml("#cccccc");
            worksheet.Cells[row, 1, row, colSumTotalShift].Style.Font.Bold = true;
            worksheet.Cells[row, 1, row, colSumTotalShift].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells[row, 1, row, colSumTotalShift].Style.Fill.BackgroundColor.SetColor(colorFooter);
            worksheet.Cells[4, 1, row, colSumTotalShift].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            for (var i = 1; i <= (6 + listShift.Count * 2); i++)
            {
                worksheet.Column(i).AutoFit();
            }
        }
        private async Task<string> ExportDatatableAmountToHtml(TopDownSellReportInput input, ITopDownSellReportAppService appService)
        {
            input.MaxResultCount = 10000;
            var dtos = await appService.GetTopDownSellReportByAmount(input);
            var lstData = dtos.DataTable.Items;
            var listShiftTotal = new List<ItemByShiftDto>();
            foreach (var item in lstData)
            {
                foreach (var itemShift in item.ListItemShift.OrderBy(s => s.Start_Number))
                {
                    listShiftTotal.Add(new ItemByShiftDto
                    {
                        StartHour = itemShift.StartHour,
                        EndHour = itemShift.EndHour,
                        StartMinute = itemShift.StartMinute,
                        EndMinute = itemShift.EndMinute,
                        QuantityShift = itemShift.QuantityShift,
                        AmountShift = itemShift.AmountShift,
                    });
                }
            }
            var listShift = GetListShift(input);
            var totalCol = 6 + (listShift.Count * 2);
            var businessDate = dtos.StartDate != dtos.EndDate ? L("Date") + " " + dtos.StartDate + " " + L("To") + " " + dtos.EndDate : L("Date") + " " + dtos.StartDate;
            StringBuilder strHTMLBuilder = new StringBuilder();
            strHTMLBuilder.Append("<html>");
            strHTMLBuilder.Append("<head>");
            strHTMLBuilder.Append("</head>");
            var styleTable = GetStyleForTable();
            strHTMLBuilder.Append(styleTable);
            strHTMLBuilder.Append("<body>");
            strHTMLBuilder.Append("<div>");

            //TODO: start add header
            var titleHeader = L("TopDownSellReportByAmountSummary") + "<br>" + businessDate;
            if (!string.IsNullOrEmpty(dtos.LocationCode) && !string.IsNullOrEmpty(dtos.LocationName))
            {
                titleHeader += "<br>" + "(" + dtos.LocationCode + ")" + dtos.LocationName;
            }
            strHTMLBuilder.Append("<table class='table table-bordered custom_table_date border-bottom-none'>");
            strHTMLBuilder.Append("<tr class='font_14'>");
            strHTMLBuilder.Append($"<td colspan='{totalCol}' class='border-bottom-none text-center font-weight-bold' style='height: 80px;'>{titleHeader}</td>");
            strHTMLBuilder.Append("</tr>");
            strHTMLBuilder.Append("<tr class=' font_14'>");
            strHTMLBuilder.Append($"<th class=' border-bottom-none text-center font-weight-bold'>{L("No.")}</th>");
            strHTMLBuilder.Append($"<th class='border-bottom-none text-center font-weight-bold'>{L("ProductCode")}</th>");
            strHTMLBuilder.Append($"<th class='border-bottom-none text-center font-weight-bold'>{L("ProductName")}</th>");
            strHTMLBuilder.Append($"<th class='border-bottom-none text-center font-weight-bold'>{L("UnitPrice")}</th>");
            strHTMLBuilder.Append($"<th class='border-bottom-none text-center font-weight-bold'>{L("TotalAmount")}</th>");
            strHTMLBuilder.Append($"<th class='border-bottom-none text-center font-weight-bold'>{L("AmountPercent")}</th>");

            foreach (var shift in listShift.OrderBy(s => s.Start_Number))
            {
                strHTMLBuilder.Append($"<th class='border-bottom-none text-center font-weight-bold padding-0' colspan='2'>");
                strHTMLBuilder.Append($"<p class='padding-top-15'>{shift.Start_String + '-' + shift.End_String }</p>");
                strHTMLBuilder.Append($"<table class='margin-0'>");
                strHTMLBuilder.Append($"<tr class='border-bottom-none text-center font-weight-bold'>");
                strHTMLBuilder.Append($"<th class='border-bottom-none text-center font-weight-bold border-left-none'>{L("TotalAmount")}</th>");
                strHTMLBuilder.Append($"<th class='border-bottom-none text-center font-weight-bold border-right-none'>{L("AmountPercent")}</th>");
                strHTMLBuilder.Append("</tr>");
                strHTMLBuilder.Append("</table>");
                strHTMLBuilder.Append("</th>");
            }
            //end add header

            // add content
            var index = 1;
            foreach (var item in lstData)
            {
                strHTMLBuilder.Append("<tr class='font_14'>");
                strHTMLBuilder.Append($"<td class='text-center'> { index}</td> ");
                strHTMLBuilder.Append($"<td class='text-center'> { item.ProductCode}</td> ");
                strHTMLBuilder.Append($"<td class='text-center'> { item.ProductName}</td> ");
                strHTMLBuilder.Append($"<td class='text-center'> { item.UnitPrice.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td> ");
                strHTMLBuilder.Append($"<td class='text-center'> { item.Amount.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td> ");
                strHTMLBuilder.Append($"<td class='text-center'> { item.AmountPercent.ToString(ConnectConsts.ConnectConsts.NumberFormat)} %</td> ");

                foreach (var itemShift in item.ListItemShift.OrderBy(s => s.Start_Number))
                {
                    var findTotalShif = listShiftTotal.Where(s => s.Start_Number == itemShift.Start_Number);
                    var totalAmount = findTotalShif.Sum(s => s.AmountShift);
                    strHTMLBuilder.Append($"<td class='text-center'> { itemShift.AmountShift.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td> ");
                    strHTMLBuilder.Append($"<td class='text-center'> { ((totalAmount != 0 ? (itemShift.QuantityShift / totalAmount) : 0) * 100).ToString(ConnectConsts.ConnectConsts.NumberFormat)}%</td> ");
                }
                strHTMLBuilder.Append("</tr>");
                index++;
            }
            // end content

            strHTMLBuilder.Append("<tr class='font_14 background-footer'>");
            strHTMLBuilder.Append($"<td colspan='4' class='text-right font-weight-bold'> {L("Total")}</td> ");
            strHTMLBuilder.Append($"<td class = 'font-weight-bold text-center' > { dtos.DataTable.Items.Sum(x => x.Amount).ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td> ");
            strHTMLBuilder.Append($"<td class = 'font-weight-bold text-center'> {(dtos.DataTable.Items.Sum(x => x.Amount) != 0 ? 100 : 0).ToString(ConnectConsts.ConnectConsts.NumberFormat)}%</td> ");

            foreach (var shift in listShift.OrderBy(s => s.Start_Number))
            {
                var findTotalShif = listShiftTotal.Where(s => s.Start_Number == shift.Start_Number);
                var totalAmount = findTotalShif.Sum(s => s.AmountShift);
                strHTMLBuilder.Append($"<td class='text-center font-weight-bold'> {totalAmount.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td> ");
                strHTMLBuilder.Append($"<td class='text-center font-weight-bold'> {(totalAmount != 0 ? 100 : 0).ToString(ConnectConsts.ConnectConsts.NumberFormat)}%</td> ");
            }

            strHTMLBuilder.Append("</tr>");
            strHTMLBuilder.Append("</table>");
            strHTMLBuilder.Append("</div>");
            string Htmltext = strHTMLBuilder.ToString();
            return Htmltext;
        }
        #endregion


        #region Quantity Plant
        public async Task<FileDto> ExportTopDownSellByQuantityPlantReport(TopDownSellReportInput input, ITopDownSellReportAppService appService)
        {
            var builder = new StringBuilder();
            builder.Append(L("TopDownSellReportByQuantityPlant"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append("_");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            if (input.ExportOutputType == 0)
            {
                using (var excelPackage = new ExcelPackage())
                {
                    await GetTopDownSellByQuantityPlantReportExportExcel(excelPackage, input, appService);
                    Save(excelPackage, file);
                }
                return ProcessFile(input, file);
            }
            else
            {
                var contentHTML = await ExportDatatableQuantityPlantToHtml(input, appService);
                var result = ProcessFileHTML(file, contentHTML);
                return result;
            }
        }

        private async Task GetTopDownSellByQuantityPlantReportExportExcel(ExcelPackage package, TopDownSellReportInput input, ITopDownSellReportAppService appService)
        {
            var worksheet = package.Workbook.Worksheets.Add(L("TopDownSellReportByQuantityPlant"));
            var listShift = GetListShift(input);
            input.MaxResultCount = 10000;
            var dtos = await appService.GetTopDownSellReportByQuantityPlant(input);
            var lstData = dtos;
            worksheet.OutLineApplyStyle = true;
            var listHeader = new List<string> {
                L("No."),
                L("PlantCode"),
                L("PlantName"),
                L("ProductCode"),
                L("ProductName"),
                L("UnitPrice"),
                L("TotalQuantity"),
                L("QuantityPercent") };          
            Color colorHeader = System.Drawing.ColorTranslator.FromHtml("#99CCFF");
            Color colorFooter = System.Drawing.ColorTranslator.FromHtml("#cccccc");          
            var textTitle = L("TopDownSellReportByQuantityPlant") + "<br/>";
            textTitle += (dtos.StartDate != dtos.EndDate ? L("Date") + " " + dtos.StartDate + " " + L("To") + " " + dtos.EndDate : L("Date") + " " + dtos.StartDate);
            if (!string.IsNullOrEmpty(dtos.LocationName))
            {
                textTitle = textTitle + "<br/>" + dtos.LocationName;
            }
            string textTitleWithNewLine = textTitle.Replace("<br/>", Environment.NewLine);
            int headerCol = 8 + listShift.Count * 2;
            worksheet.Cells[1, 1].Value = textTitleWithNewLine;
            worksheet.Cells[1, 1, 4, headerCol].Merge = true;
            worksheet.Cells[1, 1, 4, headerCol].Style.Font.Bold = true;
            worksheet.Cells[1, 1, 4, headerCol].Style.Font.Size = 11;
            worksheet.Cells[1, 1, 4, headerCol].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 1].Style.Border.BorderAround(ExcelBorderStyle.None);
            worksheet.Cells[1, 1, 4, headerCol].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            worksheet.Cells[1, 1, 4, headerCol].Style.WrapText = true;
            var colhead = 1;
           
            foreach (var header in listHeader)
            {
                worksheet.Column(colhead).AutoFit();
                worksheet.Cells[5, colhead, 6, colhead].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[5, colhead, 6, colhead].Merge = true;
                worksheet.Cells[5, colhead, 6, colhead].Value = header;
                colhead++;
            }
            for(var i = 1; i < headerCol; i++)
            {
                worksheet.Cells[5,i].Style.WrapText = true;
            }
            int colShift = 9;
            int colHeadShift = 9;
            int colShiftMerge = 10;
            foreach (var shift in listShift.OrderBy(s => s.Start_Number))
            {
                worksheet.Cells[5, colShift, 5, colShiftMerge].Merge = true;
                worksheet.Cells[5, colShift, 5, colShiftMerge].Value = shift.Start_String + " - " + shift.End_String;
                worksheet.Cells[5, colShift, 5, colShiftMerge].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[6, colHeadShift].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[6, colHeadShift++].Value = L("TotalQuantity");
                worksheet.Cells[6, colHeadShift].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[6, colHeadShift++].Value = L("QuantityPercent");
                colShiftMerge += 2;
                colShift += 2;
            }
            var colHeadColor = colShiftMerge - 2;
            worksheet.Cells[5, 1, 6, colHeadColor].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            worksheet.Cells[5, 1, 6, colHeadColor].Style.Font.Bold = true;
            worksheet.Cells[5, 1, 6, colHeadColor].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells[5, 1, 6, colHeadColor].Style.Fill.BackgroundColor.SetColor(colorHeader);
            worksheet.Cells[5, 1, 6, colHeadColor].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells[5, 1, 6, colHeadColor].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            var row = 7;
            var totalShiftAllPlant = new List<ItemByShiftDto>();
            foreach (var itemDto in lstData.ListItem.OrderBy(s => s.Location))
            {
                var listShiftTotal = new List<ItemByShiftDto>();
                foreach (var item in itemDto.ListItem.ListItem)
                {
                    foreach (var itemShift in item.ListItemShift.OrderBy(s => s.Start_Number))
                    {
                        listShiftTotal.Add(new ItemByShiftDto
                        {
                            StartHour = itemShift.StartHour,
                            EndHour = itemShift.EndHour,
                            StartMinute = itemShift.StartMinute,
                            EndMinute = itemShift.EndMinute,
                            QuantityShift = itemShift.QuantityShift,
                            AmountShift = itemShift.AmountShift,
                        });
                    }
                }
                totalShiftAllPlant.AddRange(listShiftTotal);
                var index = 1;
                var colData = 1;
                foreach (var item in itemDto.ListItem.ListItem)
                {
                    worksheet.Cells[row, colData].Value = index;
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = itemDto.LocationCode;
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = itemDto.Location;
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = item.ProductCode;
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = item.ProductName;
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = item.UnitPrice.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = item.Quantity.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = item.QuantityPercent.ToString(ConnectConsts.ConnectConsts.NumberFormat) + "%";
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    var colShiftData = 9;

                    foreach (var itemShift in item.ListItemShift.OrderBy(s => s.Start_Number))
                    {
                        var findTotalShif = listShiftTotal.Where(s => s.StartHour == itemShift.Start_Number);
                        var totalQuantity = findTotalShif.Sum(s => s.QuantityShift);

                        worksheet.Cells[row, colShiftData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        worksheet.Cells[row, colShiftData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[row, colShiftData++].Value = itemShift.QuantityShift.ToString(ConnectConsts.ConnectConsts.NumberFormat);

                        worksheet.Cells[row, colShiftData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        worksheet.Cells[row, colShiftData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[row, colShiftData++].Value = ((totalQuantity != 0 ? (itemShift.QuantityShift / totalQuantity) : 0) * 100).ToString(ConnectConsts.ConnectConsts.NumberFormat) + "%";
                    }
                    colData = 1;
                    row++;
                    index++;
                }
                worksheet.Cells[row, 1, row, headerCol].Style.Font.Bold = true;
                worksheet.Cells[row, 1, row, headerCol].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[row, 1, row, headerCol].Style.Fill.BackgroundColor.SetColor(colorFooter);
                worksheet.Cells[row, 1, row, 6].Merge = true;
                worksheet.Cells[row, 1, row, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                worksheet.Cells[row, 1, row, 6].Value = L("Total");
                worksheet.Cells[row, 1, row, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                worksheet.Cells[row, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 7].Value = itemDto.ListItem.TotalQuantity.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                worksheet.Cells[row, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells[row, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 8].Value = 100.ToString(ConnectConsts.ConnectConsts.NumberFormat) + "%";
                worksheet.Cells[row, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                int colSumTotal = 9;
                foreach (var shift in listShift.OrderBy(s => s.Start_Number))
                {
                    var findTotalShif = listShiftTotal.Where(s => s.Start_Number == shift.Start_Number);
                    var totalQuantity = findTotalShif.Sum(s => s.QuantityShift);
                    worksheet.Cells[row, colSumTotal].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[row, colSumTotal].Value = totalQuantity.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    worksheet.Cells[row, colSumTotal].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    colSumTotal++;
                    worksheet.Cells[row, colSumTotal].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[row, colSumTotal].Value = (totalQuantity != 0 ? 100 : 0).ToString(ConnectConsts.ConnectConsts.NumberFormat) + "%";
                    worksheet.Cells[row, colSumTotal].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    colSumTotal++;
                }
                row++;
            }
            if (dtos.ListItem.Count > 1)
            {
                worksheet.Cells[row, 1, row, headerCol].Style.Font.Bold = true;
                worksheet.Cells[row, 1, row, headerCol].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[row, 1, row, headerCol].Style.Fill.BackgroundColor.SetColor(colorFooter);
                worksheet.Cells[row, 1, row, 6].Merge = true;
                worksheet.Cells[row, 1, row, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                worksheet.Cells[row, 1, row, 6].Value = L("Total");
                worksheet.Cells[row, 1, row, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 7].Value = lstData.AllTotalQuantity.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                worksheet.Cells[row, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells[row, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 8].Value = 100.ToString(ConnectConsts.ConnectConsts.NumberFormat) + "%"; 
                worksheet.Cells[row, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                int colSumTotal = 9;
                foreach (var shift in listShift.OrderBy(s => s.Start_Number))
                {
                    var findTotalShif = totalShiftAllPlant.Where(s => s.Start_Number == shift.Start_Number);
                    var totalQuantity = findTotalShif.Sum(s => s.QuantityShift);
                    worksheet.Cells[row, colSumTotal].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[row, colSumTotal].Value = totalQuantity.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    worksheet.Cells[row, colSumTotal].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    colSumTotal++;
                    worksheet.Cells[row, colSumTotal].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[row, colSumTotal].Value = (totalQuantity != 0 ? 100 : 0).ToString(ConnectConsts.ConnectConsts.NumberFormat) + "%";
                    worksheet.Cells[row, colSumTotal].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    colSumTotal++;
                }
                row++;
            }
            for (var i = 1; i <= headerCol; i++)
            {
                worksheet.Column(i).AutoFit();
            }
        }
        private async Task<string> ExportDatatableQuantityPlantToHtml(TopDownSellReportInput input, ITopDownSellReportAppService appService)
        {
            input.MaxResultCount = 10000;
            var dtos = await appService.GetTopDownSellReportByQuantityPlant(input);
            var businessDate = dtos.StartDate != dtos.EndDate ? L("Date") + " " + dtos.StartDate + " " + L("To") + " " + dtos.EndDate : L("Date") + " " + dtos.StartDate;
            StringBuilder strHTMLBuilder = new StringBuilder();
            strHTMLBuilder.Append("<html>");
            strHTMLBuilder.Append("<head>");
            strHTMLBuilder.Append("</head>");
            var styleTable = GetStyleForTable();
            strHTMLBuilder.Append(styleTable);
            var titleHeader = L("TopDownSellReportByQuantityPlant") + "<br>" + businessDate;
            if (!string.IsNullOrEmpty(dtos.LocationCode) && !string.IsNullOrEmpty(dtos.LocationName))
            {
                titleHeader += "<br>" + "(" + dtos.LocationCode + ")" + " " + dtos.LocationName;
            }
            var listShift = GetListShift(input);
            var shiftColTotal = listShift.Count() * 2;
            var totalShiftAllPlant = new List<ItemByShiftDto>();
            strHTMLBuilder.Append("<body>");
            strHTMLBuilder.Append("<div class='font_14' style='height: 70px;'>");
            strHTMLBuilder.Append($"<div class='border-bottom-none text-center font-weight-bold'>{titleHeader}</div>");
            strHTMLBuilder.Append("</div>"); 
            strHTMLBuilder.Append("<table class='table table-bordered table-condensed margin-0'>");
            strHTMLBuilder.Append("<thead>");
            strHTMLBuilder.Append("<tr class='thead_content'>");
            strHTMLBuilder.Append($"<th class='text-center' rowspan='2'>{L("No.")}</th>");
            strHTMLBuilder.Append($"<th class='text-center' rowspan='2'>{L("PlantCode")}</th>");
            strHTMLBuilder.Append($"<th class='text-center' rowspan='2'>{L("PlantName")}</th>");
            strHTMLBuilder.Append($"<th class='text-center' rowspan='2'>{L("ProductCode")}</th>");
            strHTMLBuilder.Append($"<th class='text-center' rowspan='2'>{L("ProductName")}</th>");
            strHTMLBuilder.Append($"<th class='text-center' rowspan='2'>{L("UnitPrice")}</th>");
            strHTMLBuilder.Append($"<th class='text-center' rowspan='2'>{L("TotalQuantity")}</th>");
            strHTMLBuilder.Append($"<th class='text-center' rowspan='2'>{L("QuantityPercent")}</th>");
            foreach(var shift in listShift.OrderBy(s=>s.Start_Number))
            {
                strHTMLBuilder.Append($"<th class='text-center' colspan='2'>{shift.Start_String} - {shift.End_String}</th>");
            }    
            strHTMLBuilder.Append("</tr>");
            strHTMLBuilder.Append("<tr class='thead_content'>");
            foreach (var shift in listShift.OrderBy(s => s.Start_Number))
            {
                strHTMLBuilder.Append($"<th class='text-center'>{L("TotalQuantity")}</th>");
                strHTMLBuilder.Append($"<th class='text-center'>{L("QuantityPercent")}</th>");
            }
            strHTMLBuilder.Append("</tr>");

            strHTMLBuilder.Append("</thead>");
            foreach (var location in dtos.ListItem.OrderBy(s=>s.Location))
            {
                var listShiftTotal = new List<ItemByShiftDto>();
                foreach (var item in location.ListItem.ListItem)
                {
                    foreach (var itemShift in item.ListItemShift.OrderBy(s => s.Start_Number))
                    {
                        listShiftTotal.Add(new ItemByShiftDto
                        {
                            StartHour = itemShift.StartHour,
                            EndHour = itemShift.EndHour,
                            StartMinute = itemShift.StartMinute,
                            EndMinute = itemShift.EndMinute,
                            QuantityShift = itemShift.QuantityShift,
                            AmountShift = itemShift.AmountShift,
                        });
                    }
                }
                totalShiftAllPlant.AddRange(listShiftTotal);


                var count = 1;
                // content
                foreach (var item in location.ListItem.ListItem)
                {
                    strHTMLBuilder.Append($"<tr>");
                    strHTMLBuilder.Append($"<td class='item border-bottom-none text-center'>{count++}</td>");
                    strHTMLBuilder.Append($"<td class='item border-bottom-none text-center'>{location.LocationCode}</td>");
                    strHTMLBuilder.Append($"<td class='item border-bottom-none text-center'>{location.Location}</td>");
                    strHTMLBuilder.Append($"<td class='item border-bottom-none text-center'>{item.ProductCode}</td>");
                    strHTMLBuilder.Append($"<td class='item border-bottom-none text-center'>{item.ProductName}</td>");
                    strHTMLBuilder.Append($"<td class='item border-bottom-none text-center'>{item.UnitPrice.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td>");
                    strHTMLBuilder.Append($"<td class='item border-bottom-none text-center'>{item.Quantity.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td>");
                    strHTMLBuilder.Append($"<td class='item border-bottom-none text-center'>{item.QuantityPercent.ToString(ConnectConsts.ConnectConsts.NumberFormat)} %</td>");
                    

                    foreach (var itemShift in item.ListItemShift.OrderBy(s => s.Start_Number))
                    {
                        var findTotalShif = listShiftTotal.Where(s => s.Start_Number == itemShift.Start_Number);
                        var totalQuantity = findTotalShif.Sum(s => s.QuantityShift);
                        strHTMLBuilder.Append($"<td class='item border-bottom-none text-center'>{itemShift.QuantityShift.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td>");
                        strHTMLBuilder.Append($"<td class='item border-bottom-none text-center'>{((totalQuantity != 0 ? (itemShift.QuantityShift / totalQuantity) : 0) * 100).ToString(ConnectConsts.ConnectConsts.NumberFormat) + "%"}</td>");
                    }
                    strHTMLBuilder.Append("</tr>");
                }
                strHTMLBuilder.Append($"<tfoot>");
                strHTMLBuilder.Append($"<tr class='background-footer'> ");
                strHTMLBuilder.Append($"<td class='item text-center'></td>");
                strHTMLBuilder.Append($"<td class='item text-center'></td>");
                strHTMLBuilder.Append($"<td class='item text-center'></td>");
                strHTMLBuilder.Append($"<td class='item text-center'></td>");
                strHTMLBuilder.Append($"<td class='item text-center'></td>");
                strHTMLBuilder.Append($"<td class='item font-weight-bold text-center font-weight-bold'>{L("Total")}</td>");
                strHTMLBuilder.Append($"<td class='item font-weight-bold text-center font-weight-bold'>{location.ListItem.TotalQuantity}</td>");
                strHTMLBuilder.Append($"<td class='item font-weight-bold  text-center'>{(location.ListItem.TotalQuantity != 0 ? 100 : 0).ToString(ConnectConsts.ConnectConsts.NumberFormat) + "%"}</td>");
                foreach (var shift in listShift.OrderBy(s => s.Start_Number))
                {
                    var findTotalShif = listShiftTotal.Where(s => s.Start_Number == shift.Start_Number);
                    var totalQuantity = findTotalShif.Sum(s => s.QuantityShift);
                    strHTMLBuilder.Append($"<td class='item font-weight-bold text-center font-weight-bold'>{ totalQuantity.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td>");
                    strHTMLBuilder.Append($"<td class='item font-weight-bold text-center font-weight-bold'>{ (totalQuantity != 0 ? 100 : 0).ToString(ConnectConsts.ConnectConsts.NumberFormat) + "%"}</td>");
                }
                strHTMLBuilder.Append("</tr>");
                strHTMLBuilder.Append("</tfoot>");
            }
            if (dtos.ListItem.Count > 1)
            {
                strHTMLBuilder.Append($"<tfoot>");
                strHTMLBuilder.Append($"<tr class='background-footer'>");
                strHTMLBuilder.Append($"<td class='item text-center' colspan='5'></td>");
                strHTMLBuilder.Append($"<td class='item font-weight-bold text-center'>{L("Total")}</td>");
                strHTMLBuilder.Append($"<td class='item font-weight-bold text-center'>{dtos.AllTotalQuantity.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td>");
                strHTMLBuilder.Append($"<td class='item font-weight-bold text-center'>{(dtos.AllTotalQuantity != 0 ? 100 : 0).ToString(ConnectConsts.ConnectConsts.NumberFormat) + "%"}</td>");
                foreach (var shift in listShift.OrderBy(s => s.Start_Number))
                {
                    var findTotalShif = totalShiftAllPlant.Where(s => s.Start_Number == shift.Start_Number);
                    var totalQuantity = findTotalShif.Sum(s => s.QuantityShift);
                    strHTMLBuilder.Append($"<td class='item font-weight-bold text-center'>{ totalQuantity.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td>");
                    strHTMLBuilder.Append($"<td class='item font-weight-bold text-center'>{(totalQuantity != 0 ? 100 : 0).ToString(ConnectConsts.ConnectConsts.NumberFormat) + "%"}</td>");
                }
                strHTMLBuilder.Append("</tr>");
                strHTMLBuilder.Append($"</tfoot>");
            }
            strHTMLBuilder.Append("</body>");
            strHTMLBuilder.Append("</html>");
            string Htmltext = strHTMLBuilder.ToString();
            return Htmltext;
        }
        #endregion

        #region Amount Plant
        public async Task<FileDto> ExportTopDownSellByAmountPlantReport(TopDownSellReportInput input, ITopDownSellReportAppService appService)
        {
            var builder = new StringBuilder();
            builder.Append(L("TopDownSellReportByAmountPlant"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append("_");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            if (input.ExportOutputType == 0)
            {
                using (var excelPackage = new ExcelPackage())
                {
                    await GetTopDownSellByAmountPlantReportExportExcel(excelPackage, input, appService);
                    Save(excelPackage, file);
                }
                return ProcessFile(input, file);
            }
            else
            {
                var contentHTML = await ExportDatatableAmountPlantToHtml(input, appService);
                var result = ProcessFileHTML(file, contentHTML);
                return result;
            }
        }

        private async Task GetTopDownSellByAmountPlantReportExportExcel(ExcelPackage package, TopDownSellReportInput input, ITopDownSellReportAppService appService)
        {
            var worksheet = package.Workbook.Worksheets.Add(L("TopDownSellReportByAmountPlant"));
            var listShift = GetListShift(input);
            input.MaxResultCount = 10000;
            var dtos = await appService.GetTopDownSellReportByAmountPlant(input);
            var lstData = dtos;
            worksheet.OutLineApplyStyle = true;
            var listHeader = new List<string> {
                L("No."),
                L("PlantCode"),
                L("PlantName"),
                L("ProductCode"),
                L("ProductName"),
                L("UnitPrice"),
                L("TotalAmount"),
                L("AmountPercent") };
            Color colorHeader = System.Drawing.ColorTranslator.FromHtml("#99CCFF");
            Color colorFooter = System.Drawing.ColorTranslator.FromHtml("#cccccc");
            var textTitle = L("TopDownSellReportByAmountPlant") + "<br/>";
            textTitle += (dtos.StartDate != dtos.EndDate ? L("Date") + " " + dtos.StartDate + " " + L("To") + " " + dtos.EndDate : L("Date") + " " + dtos.StartDate);
            if (!string.IsNullOrEmpty(dtos.LocationName))
            {
                textTitle = textTitle + "<br/>" + dtos.LocationName;
            }
            string textTitleWithNewLine = textTitle.Replace("<br/>", Environment.NewLine);
            int headerCol = 8 + listShift.Count * 2;
            worksheet.Cells[1, 1].Value = textTitleWithNewLine;
            worksheet.Cells[1, 1, 4, headerCol].Merge = true;
            worksheet.Cells[1, 1, 4, headerCol].Style.Font.Bold = true;
            worksheet.Cells[1, 1, 4, headerCol].Style.Font.Size = 11;
            worksheet.Cells[1, 1, 4, headerCol].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 1].Style.Border.BorderAround(ExcelBorderStyle.None);
            worksheet.Cells[1, 1, 4, headerCol].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            worksheet.Cells[1, 1, 4, headerCol].Style.WrapText = true;
            var colhead = 1;

            foreach (var header in listHeader)
            {
                worksheet.Column(colhead).AutoFit();
                worksheet.Cells[5, colhead, 6, colhead].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[5, colhead, 6, colhead].Merge = true;
                worksheet.Cells[5, colhead, 6, colhead].Value = header;
                colhead++;
            }
            for (var i = 1; i < headerCol; i++)
            {
                worksheet.Cells[5, i].Style.WrapText = true;
            }
            int colShift = 9;
            int colHeadShift = 9;
            int colShiftMerge = 10;
            foreach (var shift in listShift.OrderBy(s => s.Start_Number))
            {
                worksheet.Cells[5, colShift, 5, colShiftMerge].Merge = true;
                worksheet.Cells[5, colShift, 5, colShiftMerge].Value = shift.Start_String + " - " + shift.End_String;
                worksheet.Cells[5, colShift, 5, colShiftMerge].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[6, colHeadShift].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[6, colHeadShift++].Value = L("TotalAmount");
                worksheet.Cells[6, colHeadShift].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[6, colHeadShift++].Value = L("AmountPercent");
                colShiftMerge += 2;
                colShift += 2;
            }
            var colHeadColor = colShiftMerge - 2;
            worksheet.Cells[5, 1, 6, colHeadColor].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            worksheet.Cells[5, 1, 6, colHeadColor].Style.Font.Bold = true;
            worksheet.Cells[5, 1, 6, colHeadColor].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells[5, 1, 6, colHeadColor].Style.Fill.BackgroundColor.SetColor(colorHeader);
            worksheet.Cells[5, 1, 6, colHeadColor].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells[5, 1, 6, colHeadColor].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            var row = 7;
            var totalShiftAllPlant = new List<ItemByShiftDto>();
            foreach (var itemDto in lstData.ListItem.OrderBy(s => s.Location))
            {
                var listShiftTotal = new List<ItemByShiftDto>();
                foreach (var item in itemDto.ListItem.ListItem)
                {
                    foreach (var itemShift in item.ListItemShift.OrderBy(s => s.Start_Number))
                    {
                        listShiftTotal.Add(new ItemByShiftDto
                        {
                            StartHour = itemShift.StartHour,
                            EndHour = itemShift.EndHour,
                            StartMinute = itemShift.StartMinute,
                            EndMinute = itemShift.EndMinute,
                            QuantityShift = itemShift.QuantityShift,
                            AmountShift = itemShift.AmountShift,
                        });
                    }
                }
                totalShiftAllPlant.AddRange(listShiftTotal);
                var index = 1;
                var colData = 1;
                foreach (var item in itemDto.ListItem.ListItem)
                {
                    worksheet.Cells[row, colData].Value = index;
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = itemDto.LocationCode;
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = itemDto.Location;
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = item.ProductCode;
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = item.ProductName;
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = item.UnitPrice.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = item.Amount.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = item.AmountPercent.ToString(ConnectConsts.ConnectConsts.NumberFormat) + "%";
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    var colShiftData = 9;

                    foreach (var itemShift in item.ListItemShift.OrderBy(s => s.Start_Number))
                    {
                        var findTotalShif = listShiftTotal.Where(s => s.StartHour == itemShift.Start_Number);
                        var totalAmount = findTotalShif.Sum(s => s.AmountShift);

                        worksheet.Cells[row, colShiftData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        worksheet.Cells[row, colShiftData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[row, colShiftData++].Value = itemShift.AmountShift.ToString(ConnectConsts.ConnectConsts.NumberFormat);

                        worksheet.Cells[row, colShiftData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        worksheet.Cells[row, colShiftData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[row, colShiftData++].Value = ((totalAmount != 0 ? (itemShift.AmountShift / totalAmount) : 0) * 100).ToString(ConnectConsts.ConnectConsts.NumberFormat) + "%";
                    }
                    colData = 1;
                    row++;
                    index++;
                }
                worksheet.Cells[row, 1, row, headerCol].Style.Font.Bold = true;
                worksheet.Cells[row, 1, row, headerCol].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[row, 1, row, headerCol].Style.Fill.BackgroundColor.SetColor(colorFooter);
                worksheet.Cells[row, 1, row, 6].Merge = true;
                worksheet.Cells[row, 1, row, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                worksheet.Cells[row, 1, row, 6].Value = L("Total");
                worksheet.Cells[row, 1, row, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                worksheet.Cells[row, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 7].Value = itemDto.ListItem.TotalAmount.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                worksheet.Cells[row, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells[row, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 8].Value = 100.ToString(ConnectConsts.ConnectConsts.NumberFormat) + "%";
                worksheet.Cells[row, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                int colSumTotal = 9;
                foreach (var shift in listShift.OrderBy(s => s.Start_Number))
                {
                    var findTotalShif = listShiftTotal.Where(s => s.Start_Number == shift.Start_Number);
                    var totalAmount = findTotalShif.Sum(s => s.AmountShift);
                    worksheet.Cells[row, colSumTotal].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[row, colSumTotal].Value = totalAmount.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    worksheet.Cells[row, colSumTotal].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    colSumTotal++;
                    worksheet.Cells[row, colSumTotal].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[row, colSumTotal].Value = (totalAmount != 0 ? 100 : 0).ToString(ConnectConsts.ConnectConsts.NumberFormat) + "%";
                    worksheet.Cells[row, colSumTotal].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    colSumTotal++;
                }
                row++;
            }
            if (dtos.ListItem.Count > 1)
            {
                worksheet.Cells[row, 1, row, headerCol].Style.Font.Bold = true;
                worksheet.Cells[row, 1, row, headerCol].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[row, 1, row, headerCol].Style.Fill.BackgroundColor.SetColor(colorFooter);
                worksheet.Cells[row, 1, row, 6].Merge = true;
                worksheet.Cells[row, 1, row, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                worksheet.Cells[row, 1, row, 6].Value = L("Total");
                worksheet.Cells[row, 1, row, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 7].Value = lstData.AllTotalAmount.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                worksheet.Cells[row, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells[row, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 8].Value = 100.ToString(ConnectConsts.ConnectConsts.NumberFormat) + "%";
                worksheet.Cells[row, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                int colSumTotal = 9;
                foreach (var shift in listShift.OrderBy(s => s.Start_Number))
                {
                    var findTotalShif = totalShiftAllPlant.Where(s => s.Start_Number == shift.Start_Number);
                    var totalAmount = findTotalShif.Sum(s => s.AmountShift);
                    worksheet.Cells[row, colSumTotal].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[row, colSumTotal].Value = totalAmount.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    worksheet.Cells[row, colSumTotal].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    colSumTotal++;
                    worksheet.Cells[row, colSumTotal].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    worksheet.Cells[row, colSumTotal].Value = (totalAmount != 0 ? 100 : 0).ToString(ConnectConsts.ConnectConsts.NumberFormat) + "%";
                    worksheet.Cells[row, colSumTotal].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    colSumTotal++;
                }
                row++;
            }
            for (var i = 1; i <= headerCol; i++)
            {
                worksheet.Column(i).AutoFit();
            }
        }
        private async Task<string> ExportDatatableAmountPlantToHtml(TopDownSellReportInput input, ITopDownSellReportAppService appService)
        {
            input.MaxResultCount = 10000;
            var dtos = await appService.GetTopDownSellReportByAmountPlant(input);
            var businessDate = dtos.StartDate != dtos.EndDate ? L("Date") + " " + dtos.StartDate + " " + L("To") + " " + dtos.EndDate : L("Date") + " " + dtos.StartDate;
            StringBuilder strHTMLBuilder = new StringBuilder();
            strHTMLBuilder.Append("<html>");
            strHTMLBuilder.Append("<head>");
            strHTMLBuilder.Append("</head>");
            var styleTable = GetStyleForTable();
            strHTMLBuilder.Append(styleTable);
            var titleHeader = L("TopDownSellReportByAmountPlant") + "<br>" + businessDate;
            if (!string.IsNullOrEmpty(dtos.LocationCode) && !string.IsNullOrEmpty(dtos.LocationName))
            {
                titleHeader += "<br>" + "(" + dtos.LocationCode + ")" + " " + dtos.LocationName;
            }
            var listShift = GetListShift(input);
            var shiftColTotal = listShift.Count() * 2;
            var totalShiftAllPlant = new List<ItemByShiftDto>();
            strHTMLBuilder.Append("<body>");
            strHTMLBuilder.Append("<div class='font_14' style='height: 70px;'>");
            strHTMLBuilder.Append($"<div class='border-bottom-none text-center font-weight-bold'>{titleHeader}</div>");
            strHTMLBuilder.Append("</div>");
            strHTMLBuilder.Append("<table class='table table-bordered table-condensed margin-0'>");
            strHTMLBuilder.Append("<thead>");
            strHTMLBuilder.Append("<tr class='thead_content'>");
            strHTMLBuilder.Append($"<th class='text-center' rowspan='2'>{L("No.")}</th>");
            strHTMLBuilder.Append($"<th class='text-center' rowspan='2'>{L("PlantCode")}</th>");
            strHTMLBuilder.Append($"<th class='text-center' rowspan='2'>{L("PlantName")}</th>");
            strHTMLBuilder.Append($"<th class='text-center' rowspan='2'>{L("ProductCode")}</th>");
            strHTMLBuilder.Append($"<th class='text-center' rowspan='2'>{L("ProductName")}</th>");
            strHTMLBuilder.Append($"<th class='text-center' rowspan='2'>{L("UnitPrice")}</th>");
            strHTMLBuilder.Append($"<th class='text-center' rowspan='2'>{L("TotalAmount")}</th>");
            strHTMLBuilder.Append($"<th class='text-center' rowspan='2'>{L("AmountPercent")}</th>");
            foreach (var shift in listShift.OrderBy(s => s.Start_Number))
            {
                strHTMLBuilder.Append($"<th class='text-center' colspan='2'>{shift.Start_String} - {shift.End_String}</th>");
            }
            strHTMLBuilder.Append("</tr>");
            strHTMLBuilder.Append("<tr class='thead_content'>");
            foreach (var shift in listShift.OrderBy(s => s.Start_Number))
            {
                strHTMLBuilder.Append($"<th class='text-center'>{L("TotalAmount")}</th>");
                strHTMLBuilder.Append($"<th class='text-center'>{L("AmountPercent")}</th>");
            }
            strHTMLBuilder.Append("</tr>");

            strHTMLBuilder.Append("</thead>");
            foreach (var location in dtos.ListItem.OrderBy(s => s.Location))
            {
                var listShiftTotal = new List<ItemByShiftDto>();
                foreach (var item in location.ListItem.ListItem)
                {
                    foreach (var itemShift in item.ListItemShift.OrderBy(s => s.Start_Number))
                    {
                        listShiftTotal.Add(new ItemByShiftDto
                        {
                            StartHour = itemShift.StartHour,
                            EndHour = itemShift.EndHour,
                            StartMinute = itemShift.StartMinute,
                            EndMinute = itemShift.EndMinute,
                            QuantityShift = itemShift.QuantityShift,
                            AmountShift = itemShift.AmountShift,
                        });
                    }
                }
                totalShiftAllPlant.AddRange(listShiftTotal);


                var count = 1;
                // content
                foreach (var item in location.ListItem.ListItem)
                {
                    strHTMLBuilder.Append($"<tr>");
                    strHTMLBuilder.Append($"<td class='item border-bottom-none text-center'>{count++}</td>");
                    strHTMLBuilder.Append($"<td class='item border-bottom-none text-center'>{location.LocationCode}</td>");
                    strHTMLBuilder.Append($"<td class='item border-bottom-none text-center'>{location.Location}</td>");
                    strHTMLBuilder.Append($"<td class='item border-bottom-none text-center'>{item.ProductCode}</td>");
                    strHTMLBuilder.Append($"<td class='item border-bottom-none text-center'>{item.ProductName}</td>");
                    strHTMLBuilder.Append($"<td class='item border-bottom-none text-center'>{item.UnitPrice.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td>");
                    strHTMLBuilder.Append($"<td class='item border-bottom-none text-center'>{item.Amount.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td>");
                    strHTMLBuilder.Append($"<td class='item border-bottom-none text-center'>{item.AmountPercent.ToString(ConnectConsts.ConnectConsts.NumberFormat)} %</td>");


                    foreach (var itemShift in item.ListItemShift.OrderBy(s => s.Start_Number))
                    {
                        var findTotalShif = listShiftTotal.Where(s => s.Start_Number == itemShift.Start_Number);
                        var totalAmount = findTotalShif.Sum(s => s.AmountShift);
                        strHTMLBuilder.Append($"<td class='item border-bottom-none text-center'>{itemShift.AmountShift.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td>");
                        strHTMLBuilder.Append($"<td class='item border-bottom-none text-center'>{((totalAmount != 0 ? (itemShift.AmountShift / totalAmount) : 0) * 100).ToString(ConnectConsts.ConnectConsts.NumberFormat) + "%"}</td>");
                    }
                    strHTMLBuilder.Append("</tr>");
                }
                strHTMLBuilder.Append($"<tfoot>");
                strHTMLBuilder.Append($"<tr class='background-footer'> ");
                strHTMLBuilder.Append($"<td class='item text-center'></td>");
                strHTMLBuilder.Append($"<td class='item text-center'></td>");
                strHTMLBuilder.Append($"<td class='item text-center'></td>");
                strHTMLBuilder.Append($"<td class='item text-center'></td>");
                strHTMLBuilder.Append($"<td class='item text-center'></td>");
                strHTMLBuilder.Append($"<td class='item font-weight-bold text-center font-weight-bold'>{L("Total")}</td>");
                strHTMLBuilder.Append($"<td class='item font-weight-bold text-center font-weight-bold'>{location.ListItem.TotalAmount}</td>");
                strHTMLBuilder.Append($"<td class='item font-weight-bold  text-center'>{(location.ListItem.TotalAmount != 0 ? 100 : 0).ToString(ConnectConsts.ConnectConsts.NumberFormat) + "%"}</td>");
                foreach (var shift in listShift.OrderBy(s => s.Start_Number))
                {
                    var findTotalShif = listShiftTotal.Where(s => s.Start_Number == shift.Start_Number);
                    var totalAmount = findTotalShif.Sum(s => s.AmountShift);
                    strHTMLBuilder.Append($"<td class='item font-weight-bold text-center font-weight-bold'>{ totalAmount.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td>");
                    strHTMLBuilder.Append($"<td class='item font-weight-bold text-center font-weight-bold'>{ (totalAmount != 0 ? 100 : 0).ToString(ConnectConsts.ConnectConsts.NumberFormat) + "%"}</td>");
                }
                strHTMLBuilder.Append("</tr>");
                strHTMLBuilder.Append("</tfoot>");
            }
            if (dtos.ListItem.Count > 1)
            {
                strHTMLBuilder.Append($"<tfoot>");
                strHTMLBuilder.Append($"<tr class='background-footer'>");
                strHTMLBuilder.Append($"<td class='item text-center' colspan='5'></td>");
                strHTMLBuilder.Append($"<td class='item font-weight-bold text-center'>{L("Total")}</td>");
                strHTMLBuilder.Append($"<td class='item font-weight-bold text-center'>{dtos.AllTotalAmount.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td>");
                strHTMLBuilder.Append($"<td class='item font-weight-bold text-center'>{ (dtos.AllTotalAmount != 0 ? 100 : 0).ToString(ConnectConsts.ConnectConsts.NumberFormat) + "%"}</td>");
                foreach (var shift in listShift.OrderBy(s => s.Start_Number))
                {
                    var findTotalShif = totalShiftAllPlant.Where(s => s.Start_Number == shift.Start_Number);
                    var totalAmount = findTotalShif.Sum(s => s.AmountShift);
                    strHTMLBuilder.Append($"<td class='item font-weight-bold text-center'>{ totalAmount.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td>");
                    strHTMLBuilder.Append($"<td class='item font-weight-bold text-center'>{(totalAmount != 0 ? 100 : 0).ToString(ConnectConsts.ConnectConsts.NumberFormat) + "%"}</td>");
                }
                strHTMLBuilder.Append("</tr>");
                strHTMLBuilder.Append($"</tfoot>");
            }
            strHTMLBuilder.Append("</body>");
            strHTMLBuilder.Append("</html>");
            string Htmltext = strHTMLBuilder.ToString();
            return Htmltext;
        }

        private string GetStyleForTable()
        {
            var stype = @"<style> 
             body  { 
              font:400 12px 'Tahoma';
              font-size: 12px;
              padding:10px;
            }
            table  { 
                margin-top: 10px;
                border-collapse: collapse;
                width: 100%;
                margin-bottom: 10px;
            }

            table td, table th {
                border: 1px solid #ddd;
                padding: 8px;
            }
            table th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #99CCFF;
                color: white;
            }
            thead {
                display: table-header-group;
            }
            tfoot {
                display: table-row-group;
            }
            tr{
                page-break-inside: avoid;
            }
            .text-right{
              text-align:right;
            }

			.margin_bottom_0 {
				margin-bottom: 0 !important;
			}
            .margin_top_0 {
				margin-top: 0 !important;
			}

			.border-bottom-none {
				border-bottom: none !important;
			}

			.custom_table_date {
				margin-bottom: 0px !important;
			}

			.display_flex {
				display: flex;
			}
			.w-100 {
				width: 100%;
			}
	
			.float-right {
				float: right;
			}

			.font-weight-bold {
				font-weight: bold;
			}
            .font_14{
				font-size: 14;
             }
            .text-center{
               text-align: center;          
            }
            .text-right{
               text-align: right;          
            }
            .w-18 {
                width: 18%;
            }

            .w-20 {
                width: 20%;
            }

            .w-5 {
                width: 5%;
            }

            .w-15 {
                width: 15%;
            }

            .w-25 {
                width: 25%;
            }

            .w-30 {
                width: 30%;
            }

            .w-35 {
                width: 35%;
            }

            .w-50 {
                width: 50%;
            }

            .w-85 {
                width: 85%;
            }
            .margin-0{
               margin: 0;
            }
            //table tr:hover {background-color: #ddd;}
           
            .padding-0{
                padding: 0px;
            }
            .padding-top-15{
                padding: 15px;
            }        
            .border-left-none{
              border-left: none;
            }
            .border-right-none{
               border-right: none;
            }
            .background-footer
            {
              background-color: #cccccc;
            }

            </style>";
            return stype;
        }
        #endregion

        private FileDto ProcessFileHTML(FileDto file, string fileHTML)
        {
            File.WriteAllText(Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken), fileHTML);
            file.FileType = MimeTypeNames.ApplicationXhtmlXml;
            file.FileName = Path.GetFileNameWithoutExtension(file.FileName) + ".html";
            return file;
        }
        public List<ConditionShift> GetListShift(TopDownSellReportInput input)
        {
            var listShift = new List<ConditionShift>();
            var startHour = input.StartHour;
            var endHour = input.EndHour;
            var diffMinute = input.EndMinute - input.StartMinute;
            var startMinute = input.StartMinute;
            var endMinute = input.EndMinute;
            do
            {
                var subStartHour = startHour;
                subStartHour++;
                var qr = (subStartHour == endHour && diffMinute < 0) ? (startMinute + diffMinute) : (59 - startMinute);
                listShift.Add(new ConditionShift
                {
                    StartHour = startHour,
                    EndHour = startMinute == 0 ? startHour : startHour + 1,
                    StartMinute = startMinute,
                    EndMinute = (subStartHour++ == endHour && diffMinute < 0) ? (startMinute + diffMinute) : (59 - startMinute)
                });
                startHour++;
            }
            while (startHour < endHour);
            if (diffMinute > 0)
            {
                listShift.Add(new ConditionShift
                {
                    StartHour = startHour,
                    EndHour = startHour,
                    StartMinute = startMinute,
                    EndMinute = startMinute + diffMinute
                });
            }
            return listShift;
        }
    }
}