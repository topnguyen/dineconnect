﻿
using Abp.Application.Services;
using DinePlan.DineConnect.Connect.CashSummaryReport.Dto;
using DinePlan.DineConnect.Connect.TopDownSellReport.Dto;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.TopDownSellReport
{
	public interface ITopDownSellReportAppService : IApplicationService
	{
		Task<TopDownSellBySummaryOutput> GetTopDownSellReportByQuantity(TopDownSellReportInput input);
		Task<FileDto> GetTopDownSellReportByQuantityReportToExport(TopDownSellReportInput input);


		Task<TopDownSellBySummaryOutput> GetTopDownSellReportByAmount(TopDownSellReportInput input);
		Task<FileDto> GetTopDownSellReportByAmountReportToExport(TopDownSellReportInput input);

		Task<AllTopDownSellByPlantOutput> GetTopDownSellReportByQuantityPlant(TopDownSellReportInput input);
		Task<FileDto> GetTopDownSellReportByQuantityPlantReportToExport(TopDownSellReportInput input);


		Task<AllTopDownSellByPlantOutput> GetTopDownSellReportByAmountPlant(TopDownSellReportInput input);
		Task<FileDto> GetTopDownSellReportByAmountPlantReportToExport(TopDownSellReportInput input);
	}
}
