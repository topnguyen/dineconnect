﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Connect.SalesByPeriod.Dto;
using DinePlan.DineConnect.Connect.SalesByPeriod.Exporting;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Report;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.SalesByPeriod
{
    public class SalesByPeriodReportAppService : DineConnectAppServiceBase, ISalesByPeriodReportAppService
    {
        private readonly IRepository<Transaction.Ticket> _ticketRepository;
        private readonly IRepository<Master.Location> _lRepo;
        private readonly IReportBackgroundAppService _rbas;
        private readonly IBackgroundJobManager _bgm;
        private readonly ISalesByPeriodReportExporter _exporter;
        private readonly IConnectReportAppService _connectReportAppService;
        private readonly ILocationAppService _locService;
        public SalesByPeriodReportAppService(
            ILocationAppService locService,
            IRepository<Transaction.Ticket> ticketRepository,
            IRepository<Master.Location> lRepo,
            IReportBackgroundAppService rbas,
            IBackgroundJobManager bgm,
            ISalesByPeriodReportExporter exporter,
            IConnectReportAppService connectReportAppService
            )
        {
            _locService = locService;
            _ticketRepository = ticketRepository;
            _lRepo = lRepo;
            _rbas = rbas;
            _bgm = bgm;
            _exporter = exporter;
            _connectReportAppService = connectReportAppService;
        }

        [Obsolete]
        public async Task<ListSalesOutput> GetSalesReports(SalesPeriodInput input)
        {
            var result = new ListSalesOutput();
            var scheduleFromSetting = await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.Schedules);
            var templateSeting = (JsonConvert.DeserializeObject<List<LocationSchdule>>(scheduleFromSetting));
            var scheduleSetting = new List<LocationSchdule>();
            var listSalesItems = new List<ListSalesItems>();
            var listSalesByLocOutput = new List<ListSalesByLocOutput>();
            if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                foreach (var loc in locations)
                {
                    var findLoc = await _locService.GetLocationById(new EntityDto { Id = loc });
                    var locationDetail = await _lRepo.GetAll().Include(x => x.Schedules).Where(x => x.Name == findLoc.Name).FirstOrDefaultAsync();
                    if (locationDetail != null && locationDetail.Schedules.Count > 0)
                    {
                        scheduleSetting = locationDetail.Schedules.ToList();
                    }
                    else
                    {
                        scheduleSetting = templateSeting;
                    }

                    var listSalesByLoc = ListSalesByLoc(input, scheduleSetting, findLoc.Name);
                    listSalesByLocOutput.AddRange(listSalesByLoc);
                }
            }
            else
            {
                scheduleSetting = templateSeting;
                var listSalesByLoc = ListSalesByLoc(input, scheduleSetting, "");
                listSalesByLocOutput.AddRange(listSalesByLoc);
            }
            result.ListItems.AddRange(listSalesByLocOutput);
            return result;
        }

        [Obsolete]
        private List<ListSalesByLocOutput> ListSalesByLoc(SalesPeriodInput input, List<LocationSchdule> locationSchdules, string locName)
        {
            var listSalesItems = new List<ListSalesItems>();
            var listSalesByLocOutput = new List<ListSalesByLocOutput>();
            var listShift = locationSchdules.MapTo<List<ShiftDto>>();
            foreach (var shift in listShift)
            {
                input.StartHour = shift.StartHour;
                input.EndHour = shift.EndHour;
                input.StartMinute = shift.StartMinute;
                input.EndMinute = shift.EndMinute;
                var ticketsShift = GetAllTicketByCondition(input);
                var orderShift = GetAllOrderByTicket(input, ticketsShift);
                decimal netSales = 0;
                decimal gst = 0;
                decimal serviceCharge = 0;
                int ticketCount = ticketsShift.Count();
                decimal at = 0;
                decimal pax = 0;
                decimal ac = 0;
                decimal totalAmount = 0;
                var toaalll = ticketsShift.Where(a => !string.IsNullOrEmpty(a.TicketTags)).Select(a => a.TicketTags);
                foreach (var tag in toaalll)
                    if (!string.IsNullOrEmpty(tag))
                    {
                        var myTag = JsonConvert.DeserializeObject<List<TicketTagValue>>(tag);
                        var lastTag = myTag?.LastOrDefault(a => a.TagName.Equals("COVERS") || a.TagName.Equals("PAX"));
                        if (lastTag != null) pax += int.Parse(lastTag.TagValue);
                    }

                if (pax == 0)
                {
                    pax = orderShift.Sum(o => o.NumberOfPax);
                }
                var promotions = new List<PromotionDetailValue>();
                foreach (var ticket in ticketsShift)
                {
                    netSales += ticket.GetPlainSum();
                    gst += ticket.CalculateTax(Math.Abs(ticket.GetPlainSum()), ticket.GetPreTaxServicesTotal());
                    serviceCharge += ticket.GetPreTaxServicesTotal();
                    totalAmount += ticket.TotalAmount;
                    promotions.AddRange(ticket.GetTicketPromotionList());
                }
                foreach (var order in orderShift)
                {
                    promotions.AddRange(order.GetOrderPromotionList());
                }
                at = ticketCount != 0 ? totalAmount / ticketCount : 0;
                ac = pax != 0 ? totalAmount / pax : 0;
                var allPromotion = new List<DiscountPromotion>();
                var promotionGroups = promotions.GroupBy(p => p.PromotionName);
                foreach (var pro in promotionGroups)
                {
                    allPromotion.Add(new DiscountPromotion
                    {
                        Name = pro.Key,
                        Used = pro.Count(),
                        Amount = pro.Sum(s => s.PromotionAmount),
                    });
                }

                listSalesItems.Add(new ListSalesItems
                {
                    NetSales = netSales,
                    GST = gst,
                    ServiceCharge = serviceCharge,
                    Ticket = ticketCount,
                    AT = at,
                    Pax = pax,
                    AC = ac,
                    Promotions = allPromotion,
                    StartHour = shift.StartHour,
                    EndHour = shift.EndHour,
                    StartMinute = shift.StartMinute,
                    EndMinute = shift.EndMinute,
                    ScheduleName = shift.Name
                });
            }
            listSalesByLocOutput.Add(new ListSalesByLocOutput
            {
                LocationName = locName,
                ListItems = listSalesItems,
                DiscountPromotion = GetPromotionTotalString(listSalesItems)
            });

            return listSalesByLocOutput;
        }
        private List<DiscountPromotion> promotionTotal = new List<DiscountPromotion>();
        private string GetPromotionTotalString(List<ListSalesItems> items)
        {
            var allPromotion = items.SelectMany(t => t.Promotions).ToList();
            var allPromotionGroups = allPromotion.GroupBy(t => t.Name);
            foreach (var proTotal in allPromotionGroups)
            {
                promotionTotal.Add(new DiscountPromotion
                {
                    Name = proTotal.Key,
                    Used = proTotal.Sum(s => s.Used),
                    Amount = proTotal.Sum(s => s.Amount),
                });
            }
            var promotionTotalString = " ";
            foreach (var pro in promotionTotal)
            {
                promotionTotalString += pro.Name + "," + pro.Used + "," + pro.Amount + "\r\n";
            }
            return promotionTotalString;
        }
        public async Task<FileDto> GetSalesReportToExport(SalesPeriodInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                input.MaxResultCount = 10000;
                var output = await _exporter.ExportSalesPeriodReport(input, this);
                return output;
            }
            return null;
        }
        [Obsolete]
        public IQueryable<Transaction.Ticket> GetAllTicketByCondition(SalesPeriodInput input)
        {
            DateTime endDate = input.EndDate.AddDays(1);
            var tickets = _ticketRepository.GetAll();
            tickets = tickets.Where(a => a.LastPaymentTime >= input.StartDate && a.LastPaymentTime <= endDate);
            tickets = tickets.Where(x => (x.LastPaymentTime.Hour * 60 + x.LastPaymentTime.Minute) >= (input.StartHour * 60 + input.StartMinute)
                                           && (x.LastPaymentTime.Hour * 60 + x.LastPaymentTime.Minute) <= (input.EndHour * 60 + input.EndMinute));
            if (input.Location > 0)
            {
                tickets = tickets.Where(a => a.LocationId == input.Location);
            }
            else if (input.Locations != null && input.Locations.Any())
            {
                var locations = input.Locations.Select(a => a.Id).ToList();
                tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
                     && !input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                if (locations.Any()) tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true
                });
                tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }

            if (input.LocationGroup != null)
            {
                if (input.LocationGroup.NonLocations != null && input.LocationGroup.NonLocations.Any())
                {
                    var nonlocations = input.LocationGroup.NonLocations.Select(a => a.Id).ToList();
                    tickets = tickets.Where(a => !nonlocations.Contains(a.LocationId));
                }
            }

            else if (input.LocationGroup == null)
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = new List<SimpleLocationDto>(),
                    Group = false,
                    UserId = input.UserId
                });
                if (input.UserId > 0)
                    tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }

            return tickets;
        }
        [Obsolete]
        public IEnumerable<Order> GetAllOrderByTicket(SalesPeriodInput input, IEnumerable<Transaction.Ticket> tickets)
        {
            var orders = tickets.SelectMany(a => a.Orders);

            if (input.Locations.Any())
            {
                var locationIds = input.Locations.Select(l => l.Id).ToList();
                orders = orders.Where(o => locationIds.Contains(o.Location_Id));
            }
            return orders;
        }

    }
}
