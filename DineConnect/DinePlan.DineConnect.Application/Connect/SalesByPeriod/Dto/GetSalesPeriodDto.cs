﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.SalesByPeriod.Dto
{
    public class ListSalesOutput
    {
        public List<ListSalesByLocOutput> ListItems { get; set; } = new List<ListSalesByLocOutput>();
    }
    public class ListSalesByLocOutput
    {
        public string LocationName { get; set; }
        public List<ListSalesItems> ListItems { get; set; } = new List<ListSalesItems>();
        public string DiscountPromotion { get; set; }
        public decimal NetSales => ListItems.Sum(s => s.NetSales);
        public decimal GST => ListItems.Sum(s => s.GST);
        public decimal ServiceCharge => ListItems.Sum(s => s.ServiceCharge);
        public int Ticket => ListItems.Sum(s => s.Ticket);
        public decimal AT => ListItems.Sum(s => s.AT);
        public decimal Pax => ListItems.Sum(s => s.Pax);
        public decimal AC => ListItems.Sum(s => s.AC);
    }

    public class ListSalesItems
    {
        public string ScheduleName { get; set; }
        public virtual int StartHour { get; set; }
        public virtual int StartMinute { get; set; }
        public virtual int EndHour { get; set; }
        public virtual int EndMinute { get; set; }
        public double Start_Number => Convert.ToDouble(StartHour + StartMinute / 60);
        public double End_Number => Convert.ToDouble(EndHour + EndMinute / 60);
        public string Start_String => StartHour + ":" + StartMinute;
        public string End_String => EndHour + ":" + EndMinute;

        public string DiscountPromotion
        {
            get
            {
                string discountString = " ";
                foreach (var pro in Promotions)
                {
                    discountString += pro.Name + "," + pro.Used + "," + pro.Amount + "\r\n";
                }
                return discountString;
            }
        }
        public decimal NetSales { get; set; }
        public decimal GST { get; set; }
        public decimal ServiceCharge { get; set; }
        public int Ticket { get; set; }
        public decimal AT { get; set; }
        public decimal Pax { get; set; }
        public decimal AC { get; set; }

        public List<DiscountPromotion> Promotions { get; set; } = new List<DiscountPromotion>();
    }
    public class DiscountPromotion
    {
        public string Name { get; set; }
        public int Used { get; set; }
        public decimal Amount { get; set; }
    }
}
