﻿using Abp.Application.Services;
using DinePlan.DineConnect.Connect.SalesByPeriod.Dto;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.SalesByPeriod
{
    public interface ISalesByPeriodReportAppService : IApplicationService
    {
        Task<ListSalesOutput> GetSalesReports(SalesPeriodInput input);
        Task<FileDto> GetSalesReportToExport(SalesPeriodInput input);
    }
}
