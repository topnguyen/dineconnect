﻿using DinePlan.DineConnect.Connect.SalesByPeriod.Dto;
using Abp.Domain.Repositories;
using Abp.Extensions;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.SalesByPeriod.Exporting
{
    public class SalesByPeriodReportExporter : FileExporterBase, ISalesByPeriodReportExporter
    {
        public async Task<FileDto> ExportSalesPeriodReport(SalesPeriodInput input, ISalesByPeriodReportAppService appService)
        {
            var formatDateSetting = await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.SimpleDateFormat);
            var builder = new StringBuilder();
            builder.Append(L("SalesByPeriod"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append("_");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            if (input.ExportOutputType == 0)
            {
                using (var excelPackage = new ExcelPackage())
                {
                    await GetSalesReportExportExcel(excelPackage, input, appService);
                    Save(excelPackage, file);
                }
                return ProcessFile(input, file);
            }
            else
            {
                var contentHTML = await ExportDatatableToHtml(input, appService);
                var result = ProcessFileHTML(file, contentHTML);
                return result;
            }
        }
        private FileDto ProcessFileHTML(FileDto file, string fileHTML)
        {
            File.WriteAllText(Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken), fileHTML);
            file.FileType = MimeTypeNames.ApplicationXhtmlXml;
            file.FileName = Path.GetFileNameWithoutExtension(file.FileName) + ".html";
            return file;
        }


        private async Task GetSalesReportExportExcel(ExcelPackage package, SalesPeriodInput input, ISalesByPeriodReportAppService appService)
        {
            var sheet = package.Workbook.Worksheets.Add(L("SalesByReport"));
            input.MaxResultCount = 10000;
            sheet.OutLineApplyStyle = true;
            var data = await appService.GetSalesReports(input);
            var formatDateSetting = await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.SimpleDateFormat);
            List<string> listRowName = new List<string> { L("NetSales"), L("GST"), L("ServiceCharge"), L("Ticket"), L("AT"), L("Pax"), L("ACSales"), L("DiscountPromotion") };
            var allCell = 0;
            if (data != null)
            {
                int rowCount = 4;
                foreach (var location in data.ListItems)
                {
                    var totalCell = 1 + location.ListItems.Count;
                    allCell = totalCell;
                    if (!string.IsNullOrEmpty(location.LocationName))
                    {
                        sheet.Cells[rowCount, 1, rowCount, totalCell].Merge = true;
                        sheet.Cells[rowCount, 1, rowCount, totalCell].Value = location.LocationName;
                        Color colorHeader = System.Drawing.ColorTranslator.FromHtml("#99CCFF");
                        sheet.Cells[$"A{rowCount}:B{rowCount}"].Style.Font.Bold = true;
                        sheet.Cells[$"A{rowCount}:B{rowCount}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        sheet.Cells[$"A{rowCount}:B{rowCount}"].Style.Fill.BackgroundColor.SetColor(colorHeader);
                        rowCount++;
                    }

                    int rowItem = rowCount;
                    foreach (var head in listRowName)
                    {
                        sheet.Cells[rowCount, 1].Value = head;
                        sheet.Cells[rowCount, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        rowCount++;
                    }
                    rowCount -= listRowName.Count();
                    int colItem = 2;
                    foreach (var item in location.ListItems)
                    {

                        sheet.Cells[rowItem++, colItem].Value = item.NetSales.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                        sheet.Cells[rowItem++, colItem].Value = item.GST.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                        sheet.Cells[rowItem++, colItem].Value = item.ServiceCharge.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                        sheet.Cells[rowItem++, colItem].Value = item.Ticket;
                        sheet.Cells[rowItem++, colItem].Value = item.AT.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                        sheet.Cells[rowItem++, colItem].Value = item.Pax.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                        sheet.Cells[rowItem++, colItem].Value = item.AC.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                        sheet.Cells[rowItem, colItem].Style.WrapText = true;
                        sheet.Cells[rowItem, colItem].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        sheet.Cells[rowItem++, colItem].Value = item.DiscountPromotion;
                        colItem++;
                        rowItem = rowCount;
                    }
                    rowCount += 8;
                }
            }
            sheet.Cells[$"A{1}:Z{1}"].Style.Font.Bold = true;
            sheet.Cells[1, 1, 1, allCell].Merge = true;
            sheet.Cells[1, 1, 1, allCell].Value = L("SalesByPeriod");
            sheet.Cells[1, 1, 1, allCell].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            sheet.Cells[2, 1, 2, allCell].Merge = true;
            sheet.Cells[2, 1, 2, allCell].Value = L("Date") + " " + input.StartDate.ToString(formatDateSetting) + " " + L("To") + " " + input.EndDate.ToString(formatDateSetting);
            sheet.Cells[2, 1, 2, allCell].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            for (int i = 1; i <= 10; i++)
            {
                sheet.Column(i).AutoFit();
            }
        }

        private async Task<string> ExportDatatableToHtml(SalesPeriodInput input, ISalesByPeriodReportAppService appService)
        {
            var dtos = await appService.GetSalesReports(input);
            var formatDateSetting = await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.SimpleDateFormat);
            StringBuilder strHTMLBuilder = new StringBuilder();
            strHTMLBuilder.Append("<html>");
            strHTMLBuilder.Append("<head>");
            strHTMLBuilder.Append("</head>");
            var businessDate = input.StartDate != input.EndDate ? L("Date") + " " + input.StartDate.ToString(formatDateSetting) + " " + L("To") + " " + input.EndDate.ToString(formatDateSetting) : L("Date") + " " + input.StartDate.ToString(formatDateSetting);
            strHTMLBuilder.Append(@"<style> 
             body  { 
              font:400 12px 'Tahoma';
              font-size: 12px;
              padding:10px;
            }
            table  { 
                margin-top: 10px;
                border-collapse: collapse;
                width: 100%;
                margin-bottom: 10px;
            }

            table td, table th {
                border: 1px solid #ddd;
                padding: 8px;
            }
            table th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                color: white;
            }
            thead {
                display: table-header-group;
            }
            tfoot {
                display: table-row-group;
            }
            tr{
                page-break-inside: avoid;
            }
            blockquote {
              color:white;
              text-align:center;
            }

            h3{
              text-align:center;
              text-transform: capitalize;
              font-size: medium;
            }

			 .thead_title {
				background-color: #deebf7;
				font-weight: bold;
			}

			.thead_content {
				background-color: #fff2cc;
			}

			.footer_item {
				background-color: #d6dce5;
				font-weight: bold;
			}

			.footer {
				background-color: #e2f0d9;
				font-weight: bold;
			}

			.margin_bottom_0 {
				margin-bottom: 0 !important;
			}

			.border_bottom_none {
				border-bottom: none !important;
			}

			.custom_table_date {
				margin-bottom: 0px !important;
			}

			.display_flex {
				display: flex;
			}

			.item {
				max-width: 116px;
			}

			.w-100 {
				width: 100%;
			}


			.float-right {
				float: right;
			}

			.thead_title {
				background-color: #008000;
				font-weight: bold;
				color: white;
			}

			.thead_content {
				background-color: #808080;
				color: white;
			}

			.footer_item {
				background-color: #d6dce5;
				font-weight: bold;
			}

			.footer_str {
				display: flex;
				padding: 10px;
			}

			.footer {
				background-color: #e2f0d9;
				font-weight: bold;
			}

			.color_header {
				background-color: #808080;
				color: white;
			}

			.font_weight_bold {
				font-weight: bold;
			}

			.nodata {
				background: navajowhite;
				color: red;
			}

			.table_Item {
				border: none !important;
				margin: 0px !important;
				padding: 0px !important;
			}

			.table_Item_Element {
				border-left: none !important;
				border-bottom: none !important;
			}

			.max_width_50 {
				max-width: 50px;
			}

			.max_width_20 {
				max-width: 20px;
			}
            .text-center{
			  text-align: center;           
			}
            </style>");
            var titleHeader = L("SalesByPeriod") + "<br>" + businessDate;

            strHTMLBuilder.Append("<body>");
            strHTMLBuilder.Append("<div class='font_14'>");
            strHTMLBuilder.Append($"<h3  class='border_bottom_none text-center font_weight_bold'>{titleHeader}</h3>");
            strHTMLBuilder.Append("</div>");
            foreach (var location in dtos.ListItems)
            {
                strHTMLBuilder.Append("<table class='table table-bordered table-condensed'>");
                strHTMLBuilder.Append("<thead> <tr class='thead_title'>");
                strHTMLBuilder.Append($"<th colspan = '22' class='border_bottom_none font_weight_bold'>{location.LocationName}</th> </thead> ");

                strHTMLBuilder.Append("<tr class='thead_content'>");
                strHTMLBuilder.Append("<th></th>");
                foreach (var schedule in location.ListItems)
                {
                    strHTMLBuilder.Append($"<th class='text-center font_weight_bold'>{schedule.ScheduleName} <br />  {schedule.Start_String} - {schedule.End_String}  </th>");
                }
                strHTMLBuilder.Append("</tr>");

                strHTMLBuilder.Append("<tr>");
                strHTMLBuilder.Append($"<td class='item border_bottom_none text-center max_width_50'>{L("NetSales")}</td>");
                foreach (var item in location.ListItems)
                {
                    strHTMLBuilder.Append(" <td class='item border_bottom_none text-center max_width_50'> ");
                    strHTMLBuilder.Append($"{item.NetSales.ToString(ConnectConsts.ConnectConsts.NumberFormat)}");
                    strHTMLBuilder.Append("</td>");
                }
                strHTMLBuilder.Append("</tr>");

                strHTMLBuilder.Append("<tr>");
                strHTMLBuilder.Append($"<td class='item border_bottom_none text-center max_width_50'>{L("GST")}</td>");
                foreach (var item in location.ListItems)
                {
                    strHTMLBuilder.Append(" <td class='item border_bottom_none text-center max_width_50'> ");
                    strHTMLBuilder.Append($"{item.GST.ToString(ConnectConsts.ConnectConsts.NumberFormat)}");
                    strHTMLBuilder.Append("</td>");
                }
                strHTMLBuilder.Append("</tr>");

                strHTMLBuilder.Append("<tr>");
                strHTMLBuilder.Append($"<td class='item border_bottom_none text-center max_width_50'>{L("ServiceCharge")}</td>");
                foreach (var item in location.ListItems)
                {
                    strHTMLBuilder.Append(" <td class='item border_bottom_none text-center max_width_50'> ");
                    strHTMLBuilder.Append($"{item.ServiceCharge.ToString(ConnectConsts.ConnectConsts.NumberFormat)}");
                    strHTMLBuilder.Append("</td>");
                }
                strHTMLBuilder.Append("</tr>");

                strHTMLBuilder.Append("<tr>");
                strHTMLBuilder.Append($"<td class='item border_bottom_none text-center max_width_50'>{L("Ticket")}</td>");
                foreach (var item in location.ListItems)
                {
                    strHTMLBuilder.Append(" <td class='item border_bottom_none text-center max_width_50'> ");
                    strHTMLBuilder.Append($"{item.Ticket.ToString()}");
                    strHTMLBuilder.Append("</td>");
                }
                strHTMLBuilder.Append("</tr>");

                strHTMLBuilder.Append("<tr>");
                strHTMLBuilder.Append($"<td class='item border_bottom_none text-center max_width_50'>{L("AT")}</td>");
                foreach (var item in location.ListItems)
                {
                    strHTMLBuilder.Append(" <td class='item border_bottom_none text-center max_width_50'> ");
                    strHTMLBuilder.Append($"{item.AT.ToString(ConnectConsts.ConnectConsts.NumberFormat)}");
                    strHTMLBuilder.Append("</td>");
                }
                strHTMLBuilder.Append("</tr>");

                strHTMLBuilder.Append("<tr>");
                strHTMLBuilder.Append($"<td class='item border_bottom_none text-center max_width_50'>{L("Pax")}</td>");
                foreach (var item in location.ListItems)
                {
                    strHTMLBuilder.Append(" <td class='item border_bottom_none text-center max_width_50'> ");
                    strHTMLBuilder.Append($"{item.Pax.ToString(ConnectConsts.ConnectConsts.NumberFormat)}");
                    strHTMLBuilder.Append("</td>");
                }
                strHTMLBuilder.Append("</tr>");

                strHTMLBuilder.Append("<tr>");
                strHTMLBuilder.Append($"<td class='item border_bottom_none text-center max_width_50'>{L("ACSales")}</td>");
                foreach (var item in location.ListItems)
                {
                    strHTMLBuilder.Append(" <td class='item border_bottom_none text-center max_width_50'> ");
                    strHTMLBuilder.Append($"{item.AC.ToString(ConnectConsts.ConnectConsts.NumberFormat)}");
                    strHTMLBuilder.Append("</td>");
                }
                strHTMLBuilder.Append("</tr>");


                strHTMLBuilder.Append("<tr>");
                strHTMLBuilder.Append($"<td class='item border_bottom_none text-center max_width_50'>{L("DiscountPromotion")}</td>");
                foreach (var item in location.ListItems)
                {
                    strHTMLBuilder.Append(" <td class='item border_bottom_none text-center max_width_50'> ");
                    foreach(var promo in item.Promotions)
                    {
                        strHTMLBuilder.Append($"<span>{promo.Name} , {promo.Used} , {promo.Amount.ToString(ConnectConsts.ConnectConsts.NumberFormat)} <br></span> ");
                    }
               
                    strHTMLBuilder.Append("</td>");
                }
                strHTMLBuilder.Append("</tr>");
           
                //Close tags.  
                strHTMLBuilder.Append("</table>");
            }

            strHTMLBuilder.Append("</body>");
            strHTMLBuilder.Append("</html>");

            string Htmltext = strHTMLBuilder.ToString();
            return Htmltext;
        }
    }
}
