﻿using DinePlan.DineConnect.Connect.SalesByPeriod.Dto;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.SalesByPeriod.Exporting
{
    public interface ISalesByPeriodReportExporter
    {
        Task<FileDto> ExportSalesPeriodReport(SalesPeriodInput input, ISalesByPeriodReportAppService appService);

    }
}
