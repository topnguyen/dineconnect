﻿
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.CashSummaryReport.Dto;
using DinePlan.DineConnect.Connect.CreditCardReport.Dto;
using DinePlan.DineConnect.Connect.Customize.Dto;
using DinePlan.DineConnect.Connect.SpeedOfServiceReport.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.CreditCardReport
{
	public interface ICreditCardReportAppService : IApplicationService
	{
		Task<PagedResultOutput<GetCreditCardOutput>> GetCreditCardReport(GetCreditCardInput input);
		Task<FileDto> GetCreditCardToExcel(GetCreditCardInput input);

	}
}
