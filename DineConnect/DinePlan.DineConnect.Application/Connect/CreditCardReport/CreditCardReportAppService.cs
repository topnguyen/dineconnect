﻿using Abp.Application.Services.Dto;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Auditing.Exporting;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Audit;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Connect.Tag;
using DinePlan.DineConnect.Connect.Ticket.CategoryReport.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Connect.WorkPeriod.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.Handler.Mapping;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.House.Transaction.Implementation;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Report;
using DinePlan.Infrastructure.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System;
using Abp.Configuration;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Customize.Dto;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using DinePlan.DineConnect.Connect.Custom.Exporting;
using DinePlan.DineConnect.Connect.SpeedOfServiceReport.Dto;
using DinePlan.DineConnect.Connect.SpeedOfServiceReport.Exporting;
using DinePlan.DineConnect.Connect.CashSummaryReport.Dto;
using DinePlan.DineConnect.Connect.CashSummaryReport.Exporting;
using DinePlan.DineConnect.Connect.CreditCardReport.Dto;
using DinePlan.DineConnect.Connect.CreditCardReport.Exporting;

namespace DinePlan.DineConnect.Connect.CreditCardReport
{
	public class CreditCardReportAppService : DineConnectAppServiceBase, ICreditCardReportAppService
	{
		private readonly IRepository<Company> _companyRe;
		private readonly IRepository<Master.Department> _dRepo;
		private readonly ILocationAppService _locService;
		private readonly IRepository<Master.Location> _lRepo;
		private readonly IRepository<Transaction.Ticket> _ticketManager;
		private readonly IRepository<Transaction.Order> _orderManager;
		private readonly IUnitOfWorkManager _unitOfWorkManager;
		private ICustomizeExporter _customizeExporter;
		private readonly IReportBackgroundAppService _rbas;
		private readonly IBackgroundJobManager _bgm;
		private readonly ICreditCardReportExporter _exporter;
		private readonly IRepository<Period.WorkPeriod> _workPeriod;
		private readonly IRepository<PaymentType> _payRe;
		private readonly IRepository<Payment> _paymentRepository;
		private readonly IRepository<Master.Terminal> _terminalRepository; 
		private readonly ITenantSettingsAppService _tenantSettingsService;
		public CreditCardReportAppService(
			IRepository<Transaction.Ticket> ticketManager,
			ILocationAppService locService,
			IRepository<Company> comR,
			IUnitOfWorkManager unitOfWorkManager,
			IRepository<Master.Department> drepo,
			IRepository<Master.Location> lRepo,
			IRepository<Transaction.Order> orderManager,
			ICustomizeExporter customizeExporter,
			 IReportBackgroundAppService rbas,
			 IBackgroundJobManager bgm,
			 ICreditCardReportExporter exporter,
			 IRepository<Period.WorkPeriod> workPeriod,
			 IRepository<PaymentType> payRe,
			 IRepository<Payment> paymentRepository,
			 IRepository<Master.Terminal> terminalRepository,
			 ITenantSettingsAppService tenantSettingsService
			)
		{
			_ticketManager = ticketManager;
			_companyRe = comR;
			_unitOfWorkManager = unitOfWorkManager;
			_dRepo = drepo;
			_locService = locService;
			_lRepo = lRepo;
			_orderManager = orderManager;
			_customizeExporter = customizeExporter;
			_rbas = rbas;
			_bgm = bgm;
			_exporter = exporter;
			_workPeriod = workPeriod;
			_payRe = payRe;
			_paymentRepository = paymentRepository;
			_terminalRepository = terminalRepository;
			_tenantSettingsService = tenantSettingsService;
		}


		public async Task<PagedResultOutput<GetCreditCardOutput>> GetCreditCardReport(GetCreditCardInput input)
		{
			var result = new PagedResultOutput<GetCreditCardOutput>();
			try
			{
				var tickets = GetCreditCardForCondition(input);
				if (input.Sorting == null)
				{
					input.Sorting = "DateWithTime";
				}
				var paymenTypeTagCreditCard = "CREDIT CARD";
				var creditCard = from ticket in tickets
							  join payment in _paymentRepository.GetAll()
							  on ticket.Id equals payment.TicketId
							  join paymentType in _payRe.GetAll()
							  on payment.PaymentTypeId equals paymentType.Id
							  join location in _lRepo.GetAll()
							  on ticket.LocationId equals location.Id
							  where paymentType.PaymentTag.Contains(paymenTypeTagCreditCard)
							  select new GetCreditCardOutput
							  {
								  LocationCode = location.Code,
								  Id = ticket.Id,
								  TicketNo = ticket.TicketNumber,
								  DeftGroup = ticket.DepartmentGroup,
								  Deft = ticket.DepartmentName,
								  DateWithTime = ticket.CreationTime,
								  TotalAmount = ticket.TotalAmount,
								  PaidAmount = payment.Amount,
								  PaymentType = paymentType.Name,
								  AddOn = location.AddOn,
								  Cashier = ticket.LastModifiedUserName,
								  TerminalName = ticket.TerminalName
							  };
				var ouput = await creditCard.OrderBy(input.Sorting).ToListAsync();
				var totalCount = await creditCard.CountAsync();
				foreach (var item in ouput)
				{
					item.MID = GetMIDFromAddOn(item.AddOn);
					item.TID = await GetTIDFromTerminal(item.TerminalName);
				}

				// filter by query builder 
				var dataAsQueryable = ouput.AsQueryable();
				if (!string.IsNullOrEmpty(input.DynamicFilter))
				{
					var jsonSerializerSettings = new JsonSerializerSettings
					{
						ContractResolver =
							new CamelCasePropertyNamesContractResolver()
					};
					var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
					if (filRule?.Rules != null)
					{
						dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
					}
				}

				var pageBy = dataAsQueryable.PageBy(input).ToList();
				result = new PagedResultOutput<GetCreditCardOutput>()
				{
					Items = pageBy,
					TotalCount = totalCount
				};

				return result;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		private string GetMIDFromAddOn(string input)
		{
			if (!string.IsNullOrEmpty(input))
			{
				var result = JsonConvert.DeserializeObject<GetMIDOutput>(input);
				return result?.Mid;
			}
			else
			{
				return "";
			}

		}

		private async Task<string> GetTIDFromTerminal(string input)
		{
			if (!string.IsNullOrEmpty(input))
			{
				var result =await _terminalRepository.FirstOrDefaultAsync(x => x.Name == input);
				return result?.Tid;
			}
			else
			{
				return "";
			}

		}


		public IQueryable<Transaction.Ticket> GetCreditCardForCondition(GetCreditCardInput input)
		{
			IQueryable<Transaction.Ticket> output;

			var correctDate = CorrectInputDate(input);
			using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
			{
				output = _ticketManager.GetAll();
				if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
				{
					if (correctDate)
					{
						output =
							output.Where(
								a =>
									a.LastPaymentTime >=
									input.StartDate
									&&
									a.LastPaymentTime <=
									input.EndDate);
					}
					else
					{
						var mySt = input.StartDate.Date;
						var myEn = input.EndDate.Date;

						output =
							output.Where(
								a =>
									a.LastPaymentTime >= mySt
									&&
									a.LastPaymentTime <= myEn);
					}
				}
			}

			if (input.Location > 0)
			{
				output = output.Where(a => a.LocationId == input.Location);
			}
			else if (input.Locations != null && input.Locations.Any())
			{
				var locations = input.Locations.Select(a => a.Id).ToList();
				output = output.Where(a => locations.Contains(a.LocationId));
			}
			else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
					 && !input.LocationGroup.Locations.Any())
			{
				var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
				{
					Locations = input.LocationGroup.Locations,
					Group = false
				});
				if (locations.Any()) output = output.Where(a => locations.Contains(a.LocationId));
			}
			else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
			{
				var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
				{
					Locations = input.LocationGroup.Locations,
					Group = false
				});
				output = output.Where(a => locations.Contains(a.LocationId));
			}
			else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
			{
				var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
				{
					Groups = input.LocationGroup.Groups,
					Group = true
				});
				output = output.Where(a => locations.Contains(a.LocationId));
			}
			if (input.LocationGroup != null)
			{
				if (input.LocationGroup.NonLocations != null && input.LocationGroup.NonLocations.Any())
				{
					var nonlocations = input.LocationGroup.NonLocations.Select(a => a.Id).ToList();
					output = output.Where(a => !nonlocations.Contains(a.LocationId));
				}
			}

			else if (input.LocationGroup == null)
			{
				var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
				{
					Locations = new List<SimpleLocationDto>(),
					Group = false,
					UserId = input.UserId
				});
				if (input.UserId > 0)
					output = output.Where(a => locations.Contains(a.LocationId));
			}
			return output;
		}

		public async Task<FileDto> GetCreditCardToExcel(GetCreditCardInput input)
		{
			if (AbpSession.UserId != null && AbpSession.TenantId != null)
			{
				input.MaxResultCount = 10000;

				var setting = await _tenantSettingsService.GetAllSettings();
				input.DatetimeFormat = setting.Connect.DateTimeFormat;
				input.DateFormat = setting.Connect.SimpleDateFormat;

				if (input.RunInBackground)
				{
					var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
					{
						ReportName = ReportNames.CREDITCARD,
						Completed = false,
						UserId = AbpSession.UserId.Value,
						TenantId = AbpSession.TenantId.Value,
						ReportDescription = input.ReportDescription
					});

					if (backGroundId > 0)
					{
						var tInput = new CreditCardReportInput
						{
							CreditCardInput = input,
						};
						await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
						{
							BackGroundId = backGroundId,
							ReportName = ReportNames.CREDITCARD,
							CreditCardReportInput = tInput,
							UserId = AbpSession.UserId.Value,
							TenantId = AbpSession.TenantId.Value
						});
					}
				}
				else
				{
					var output = await _exporter.ExportCreditCard(input, this);
					return output;
				}
			}

			return null;
		}
		public bool CorrectInputDate(IDateInput input)
		{
			if (input.NotCorrectDate) return true;
			var returnStatus = true;

			var operateHours = 0M;

			if (input.Locations != null && input.Locations.Any() && input.Locations.Count() == 1)
				operateHours = _lRepo.Get(input.Locations.First().Id).ExtendedBusinessHours;

			if (operateHours == 0M && input.Location > 0)
				operateHours = _lRepo.Get(input.Location).ExtendedBusinessHours;
			if (operateHours == 0M && input.LocationGroup != null && input.LocationGroup.Locations.Any() &&
				input.LocationGroup.Locations.Count() == 1)
				operateHours = _lRepo.Get(input.LocationGroup.Locations.First().Id).ExtendedBusinessHours;
			if (operateHours == 0M)
				operateHours = SettingManager.GetSettingValue<decimal>(AppSettings.ConnectSettings.OperateHours);

			if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
			{
				if (operateHours == 0M) returnStatus = false;
				input.EndDate = input.EndDate.AddDays(1);
				input.StartDate = input.StartDate.AddHours(Convert.ToDouble(operateHours));
				input.EndDate = input.EndDate.AddHours(Convert.ToDouble(operateHours)).AddMinutes(-1);
			}
			else
			{
				returnStatus = false;
			}

			return returnStatus;
		}


	}
}
