﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.CreditCardReport.Dto
{
	public class GetCreditCardOutput : AuditedEntity
	{
		public string LocationCode { get; set; }
		public string MID { get; set; }
		public string TID { get; set; }
		public string TicketNo { get; set; }
		public string DeftGroup { get; set; }
		public string Deft { get; set; }
		public DateTime DateWithTime { get; set; }
		public decimal TotalAmount { get; set; }
		public decimal PaidAmount { get; set; }
		public string PaymentType { get; set; }
		public string CardIssuer { get; set; }
		public string CardNo { get; set; }
		public string TransactonID { get; set; }
		public string BlueCardNo { get; set; }
		public string Cashier { get; set; }
		public string AddOn { get; set; }
		public string TerminalName { get; set; }
		public DateTime FormatDateWithTime
		{
			get
			{
				return new DateTime(DateWithTime.Year, DateWithTime.Month, DateWithTime.Day);
			}
		}

	}
}
