﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.CreditCardReport.Dto
{
	public class GetMIDOutput
	{
		public string Mid { get; set; }
	}
}
