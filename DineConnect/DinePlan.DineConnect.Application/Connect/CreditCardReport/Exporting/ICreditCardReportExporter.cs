﻿using DinePlan.DineConnect.Connect.CashSummaryReport.Dto;
using DinePlan.DineConnect.Connect.CreditCardReport.Dto;
using DinePlan.DineConnect.Connect.Custom.Dto;
using DinePlan.DineConnect.Connect.Customize.Dto;
using DinePlan.DineConnect.Connect.SpeedOfServiceReport.Dto;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.CreditCardReport.Exporting
{
    public interface ICreditCardReportExporter
    {
        Task<FileDto> ExportCreditCard(GetCreditCardInput input, ICreditCardReportAppService appService);
    }
}