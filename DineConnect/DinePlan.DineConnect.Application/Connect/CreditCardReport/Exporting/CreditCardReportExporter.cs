﻿using Abp.Configuration;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.CashSummaryReport.Dto;
using DinePlan.DineConnect.Connect.CreditCardReport.Dto;
using DinePlan.DineConnect.Connect.Custom.Dto;
using DinePlan.DineConnect.Connect.Customize.Dto;
using DinePlan.DineConnect.Connect.SpeedOfServiceReport.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.CreditCardReport.Exporting
{
	public class CreditCardReportExporter : FileExporterBase, ICreditCardReportExporter
    {
        public async Task<FileDto> ExportCreditCard(GetCreditCardInput input, ICreditCardReportAppService appService)
        {
            var builder = new StringBuilder();
            builder.Append(L("CreditCard"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append("_");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                await GetCreditCard(excelPackage, input, appService);


                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }
        private async Task GetCreditCard(ExcelPackage package, GetCreditCardInput creditCardInput, ICreditCardReportAppService appService)
        {
            var sheet = package.Workbook.Worksheets.Add(L("CreditCard"));
            sheet.OutLineApplyStyle = true;

            var headers = new List<string>
            {
                L("LocationCode"),
                L("MID"),
                L("TID"),
                L("TicketNo"),
                L("DepartmentGroup"),
                L("Deft"),
                L("DateWithTime"),
                L("TotalAmount"),
                L("PaidAmount"),
                L("PaymentType"),
                L("CardIssuer"),
                L("CardNo"),
                L("TransactonID"),
                L("BlueCardNo"),
                L("Cashier"),
            };

            int rowCount = 1;

            AddHeader(
                sheet, rowCount, headers.ToArray()
            );
            rowCount++;


            var creditCards = await appService.GetCreditCardReport(creditCardInput);

            if (creditCards != null)
            {
                foreach (var item in creditCards.Items)
                {
                    sheet.Cells[rowCount, 1].Value = item.LocationCode;
                    sheet.Cells[rowCount, 2].Value = item.MID;
                    sheet.Cells[rowCount, 3].Value = item.TID;
                    sheet.Cells[rowCount, 4].Value = item.TicketNo;
                    sheet.Cells[rowCount, 5].Value = item.DeftGroup;
                    sheet.Cells[rowCount, 6].Value = item.Deft;
                    sheet.Cells[rowCount, 7].Value = item.DateWithTime.ToString(creditCardInput.DatetimeFormat);
                    sheet.Cells[rowCount, 8].Value = item.TotalAmount.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    sheet.Cells[rowCount, 9].Value = item.PaidAmount.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    sheet.Cells[rowCount, 10].Value = item.PaymentType;
                    sheet.Cells[rowCount, 11].Value = item.CardIssuer;
                    sheet.Cells[rowCount, 12].Value = item.CardNo;
                    sheet.Cells[rowCount, 13].Value = item.TransactonID;
                    sheet.Cells[rowCount, 14].Value = item.BlueCardNo;
                    sheet.Cells[rowCount, 15].Value = item.Cashier;
                    rowCount++;
                }
            }
        }

    }
}