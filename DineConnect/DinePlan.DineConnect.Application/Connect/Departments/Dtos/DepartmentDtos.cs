﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Dto;
using System;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Departments.Dtos
{
    [AutoMapFrom(typeof(Master.Department))]
    public class DepartmentListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
        public string Tag { get; set; }
        public string DisplayLocation { get; set; }
        public string Locations { get; set; }
        public virtual int? TagId { get; set; }
        public string PriceTag { get; set; }
        public virtual string Code { get; set; }
        public virtual int SortOrder { get; set; }
        public virtual string AddOns { get; set; }
        public int? DepartmentGroupId { get; set; }
        public string DepartmentGroupName { get; set; }
    }

    [AutoMapTo(typeof(Master.Department))]
    public class DepartmentEditDto : ConnectEditDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Tag { get; set; }
        public virtual int? TagId { get; set; }
        public virtual string Code { get; set; }
        public virtual int SortOrder { get; set; }
        public virtual string AddOns { get; set; }
        public int? DepartmentGroupId { get; set; }
        public int? TicketTypeId { get; set; }

    }

    public class GetDepartmentInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public GetDepartmentInput()
        {
            LocationGroup = new LocationGroupDto();
        }

        public string Filter { get; set; }

        public string Operation { get; set; }
        public string Location { get; set; }
        public bool IsDeleted { get; set; }
        public LocationGroupDto LocationGroup { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "SortOrder";
            }
        }
    }

    public class GetDepartmentForEditOutput : IOutputDto
    {
        public DepartmentEditDto Department { get; set; }
        public LocationGroupDto LocationGroup { get; set; }

        public GetDepartmentForEditOutput()
        {
            LocationGroup = new LocationGroupDto();
            Department = new DepartmentEditDto();
        }
    }

    public class CreateOrUpdateDepartmentInput : IInputDto
    {
        [Required]
        public DepartmentEditDto Department { get; set; }

        public LocationGroupDto LocationGroup { get; set; }

        public CreateOrUpdateDepartmentInput()
        {
            LocationGroup = new LocationGroupDto();
        }
    }

  

    public class ApiDepartmentOutput
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Tag { get; set; }
        public int TagId { get; set; }
        public int SortOrder { get; set; }
        public string DepartmentGroup { get; set; }
        public string DepartmentGroupCode { get; set; }
        public int? TicketTypeId { get; set; }
        public string Code { get; set; }
    }

    
    [AutoMapFrom(typeof(DepartmentGroup))]
    public class DepartmentGroupListDto : FullAuditedEntityDto
    {
        public string Code { get; set; }
        public string Name { get; set; }

    }
    [AutoMapTo(typeof(DepartmentGroup))]
    public class DepartmentGroupEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO DepartmentGroup Properties Missing
        public string Code { get; set; }
        public string Name { get; set; }
        public string AddOns { get; set; }
    }

    public class GetDepartmentGroupInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }
        public bool IsDeleted { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
    public class GetDepartmentGroupForEditOutput : IOutputDto
    {
        public DepartmentGroupEditDto DepartmentGroup { get; set; }
    }
    public class CreateOrUpdateDepartmentGroupInput : IInputDto
    {
        [Required]
        public DepartmentGroupEditDto DepartmentGroup { get; set; }
    }

    [AutoMapTo(typeof(Department))]
    public class DepartmentSelectDto
    {
        public int Id { get; set; }
        public string UIId { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
    }
}