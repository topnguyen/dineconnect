﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Departments.Dtos;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Departments
{
    public interface IDepartmentAppService : IApplicationService
    {
        Task<PagedResultOutput<DepartmentListDto>> GetAll(GetDepartmentInput inputDto);

        Task<FileDto> GetAllToExcel(GetDepartmentInput input);

        Task<GetDepartmentForEditOutput> GetDepartmentForEdit(NullableIdInput nullableIdInput);

        Task CreateOrUpdateDepartment(CreateOrUpdateDepartmentInput input);

        Task DeleteDepartment(IdInput input);

        Task<ListResultOutput<DepartmentListDto>> GetIds();

        Task<ListResultOutput<ApiDepartmentOutput>> ApiGetAll(ApiLocationInput inputDto);
        Task<ApiDepartmentOutput> ApiGetDepartment(ApiLocationInput inputDto);


        Task<ListResultOutput<DepartmentListDto>> GetDepartments();

        Task<ListResultOutput<ComboboxItemDto>> GetDepartmentsForCombobox();

        Task SaveSortSortItems(int[] menuItems);

        Task ActivateItem(IdInput input);

        Task<PagedResultOutput<DepartmentGroupListDto>> GetAllDepartmentGroup(GetDepartmentGroupInput inputDto);
        Task<FileDto> GetAllDepartmentGroupToExcel();
        Task<GetDepartmentGroupForEditOutput> GetDepartmentGroupForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateDepartmentGroup(CreateOrUpdateDepartmentGroupInput input);
        Task<bool> DeleteDepartmentGroup(IdInput input);

        Task<ListResultOutput<DepartmentGroupListDto>> GetDepartmentGroupIds();
        Task ActivateDepartmentGroupItem(IdInput input);
        Task<ListResultOutput<DepartmentGroupListDto>> GetDepartmentGroups();
        Task<ListResultOutput<ComboboxItemDto>> GetComboBoxDepartmentGroups();
    }
}