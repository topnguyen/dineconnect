﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Departments.Dtos;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Utility.Exporter;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Connect.Departments.Implementation
{
    public class DepartmentAppService : DineConnectAppServiceBase, IDepartmentAppService
    {
        private readonly IDepartmentManager _departmentManager;
        private readonly IRepository<Master.Department> _departmentRepo;
        private readonly IExcelExporter _exporter;
        private readonly ISyncAppService _syncAppService;
        private readonly ILocationAppService _locAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<DepartmentGroup> _departmentgroupRepo;
        private readonly IRepository<PriceTag> _pricetagRepo;

        public DepartmentAppService(IDepartmentManager departmentManager,
            IRepository<Master.Department> departmentRepo,
            ILocationAppService locAppService,
            IExcelExporter exporter,
            ISyncAppService syncAppService,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<DepartmentGroup> departmentgroupRep,
            IRepository<PriceTag> priceTagRepo)
        {
            _departmentManager = departmentManager;
            _departmentRepo = departmentRepo;
            _exporter = exporter;
            _syncAppService = syncAppService;
            _unitOfWorkManager = unitOfWorkManager;
            _locAppService = locAppService;
            _departmentgroupRepo = departmentgroupRep;
            _pricetagRepo = priceTagRepo;
        }

        public async Task<ListResultOutput<ApiDepartmentOutput>> ApiGetAll(ApiLocationInput input)
        {
            
            var lstDept = await _departmentRepo.GetAllListAsync(a => a.TenantId == input.TenantId);

            List<ApiDepartmentOutput> deptOutput = new List<ApiDepartmentOutput>();
            if (input.LocationId > 0)
            {
                foreach (var mydept in lstDept)
                {
                    if (await _locAppService.IsLocationExists(new CheckLocationInput
                    {
                        LocationId = input.LocationId,
                        Locations = mydept.Locations,
                        Group = mydept.Group,
                        NonLocations = mydept.NonLocations,
                        LocationTag = mydept.LocationTag
                    }))
                    {
                        var deGroup = mydept.DepartmentGroup;
                        if (deGroup == null && mydept.DepartmentGroupId!=null)
                        {
                            deGroup = await _departmentgroupRepo.GetAsync(mydept.DepartmentGroupId.Value);
                        }
                        deptOutput.Add(new ApiDepartmentOutput
                        {
                            Id = mydept.Id,
                            Tag = mydept.Tag,
                            Name = mydept.Name,
                            TagId = mydept.TagId ?? 0,
                            SortOrder = mydept.SortOrder,
                            DepartmentGroupCode =deGroup!=null?deGroup.Code:"",
                            DepartmentGroup = deGroup!=null?deGroup.Name:"",
                            TicketTypeId = mydept.TicketTypeId,
                            Code = mydept.Code
                        });
                    }
                }
            }
            else
            {

                foreach (var mydept in lstDept)
                {
                    var deGroup = mydept.DepartmentGroup;
                    if (deGroup == null && mydept.DepartmentGroupId!=null)
                    {
                        deGroup = await _departmentgroupRepo.GetAsync(mydept.DepartmentGroupId.Value);
                    }
                    deptOutput.Add(new ApiDepartmentOutput
                    {
                        Id = mydept.Id,
                        Name = mydept.Name,
                        Tag = mydept.Tag,
                        TagId = mydept.TagId ?? 0,
                        SortOrder = mydept.SortOrder,
                        DepartmentGroupCode =deGroup!=null?deGroup.Code:"",
                        DepartmentGroup = deGroup!=null?deGroup.Name:"",
                        TicketTypeId = mydept.TicketTypeId,
                        Code = mydept.Code
                    });
                }
            }
            return new ListResultOutput<ApiDepartmentOutput>(deptOutput);
        }

        public async Task<ApiDepartmentOutput> ApiGetDepartment(ApiLocationInput locationInput)
        {
            var orgId = await _locAppService.GetOrgnizationIdForLocationId(locationInput.LocationId);
            if (orgId > 0)
            {
                CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);
            }

            var output = new ApiDepartmentOutput();
            var lastMenu = _departmentRepo.GetAll().Where(a => a.Id == locationInput.Id).ToList();
            if (lastMenu.Any())
            {
                var myScreen = lastMenu.LastOrDefault();
                if (myScreen != null)
                {
                    output.Id = myScreen.Id;
                    output.Name = myScreen.Name;
                    output.TagId = myScreen.TagId ?? 0;
                    return output;
                }

            }
            return output;
        }

        public async Task<PagedResultOutput<DepartmentListDto>> GetAll(GetDepartmentInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var allItems = _departmentRepo.GetAll()
                    .Where(i => i.IsDeleted == input.IsDeleted);

                if (input.Operation == "SEARCH")
                {
                    allItems = allItems
                        .WhereIf(
                            !input.Filter.IsNullOrWhiteSpace(),
                            p => p != null && (p.Name.Contains(input.Filter) ||
                                               p.Code != null && p.Code.Contains(input.Filter) ||
                                               p.DepartmentGroup != null && p.DepartmentGroup.Code.Contains(input.Filter) ||
                                               p.DepartmentGroup != null && p.DepartmentGroup.Name.Contains(input.Filter) ));

                }
                else
                {
                    allItems = allItems
                        .WhereIf(
                            !input.Filter.IsNullOrEmpty(),
                            p => p != null && (p.Name.Contains(input.Filter) ||
                                               p.Code != null && p.Code.Contains(input.Filter) ||
                                               p.DepartmentGroup != null &&
                                               p.DepartmentGroup.Code.Contains(input.Filter) ||
                                               p.DepartmentGroup != null &&
                                               p.DepartmentGroup.Name.Contains(input.Filter)));

                }
                var allMyEnItems= SearchLocation(allItems, input.LocationGroup).OfType<Master.Department>();

                var sortMenuItems = allMyEnItems.AsQueryable()
                     .PageBy(input)
                     .ToList();

                var allListDtos = sortMenuItems.MapTo<List<DepartmentListDto>>();
                var rsdepartmentGroup = await _departmentgroupRepo.GetAllListAsync();
                var rsPriceTags = await _pricetagRepo.GetAllListAsync();
            
                foreach (var locationDto in allListDtos)
                {
                    if(locationDto.TagId.HasValue)
                    {
                        var priceTag = rsPriceTags.FirstOrDefault(t => t.Id == locationDto.TagId);
                        if(priceTag!=null)
                        {
                            locationDto.PriceTag = priceTag.Name;
                        }
                       
                    }
                    if (locationDto.DepartmentGroupId.HasValue)
                    {
                        var departmentGroup = rsdepartmentGroup.FirstOrDefault(t => 
                            t.Id == locationDto.DepartmentGroupId);
                        if(departmentGroup!=null)
                        {
                            locationDto.DepartmentGroupName = departmentGroup.Name;
                        }
                        
                    }
                }
                allListDtos = allListDtos.OrderBy(input.Sorting).ToList();
                var allItemCount = await allItems.CountAsync();

                return new PagedResultOutput<DepartmentListDto>(
                    allItemCount,
                    allListDtos
                    );
            }
        }

        public async Task<FileDto> GetAllToExcel(GetDepartmentInput input)
        {
            input.MaxResultCount = 1000;
            input.SkipCount = 0;

            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<DepartmentListDto>>();
            var baseE = new BaseExportObject
            {
                ExportObject = allListDtos,
                ColumnNames = new string[] { "Id", "Name", "PriceTag", "DepartmentGroupName" },
                ColumnDisplayNames = new string[] { "Id", "Name", "PriceTag", "DepartmentGroupName" }
            };
            return _exporter.ExportToFile(baseE, L("Department"));
        }

        public async Task<GetDepartmentForEditOutput> GetDepartmentForEdit(NullableIdInput input)
        {
            GetDepartmentForEditOutput output = new GetDepartmentForEditOutput();

            DepartmentEditDto editDto;
            if (input.Id.HasValue)
            {
                var hDto = await _departmentRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<DepartmentEditDto>();
                if(editDto.TicketTypeId==null)
                {
                    editDto.TicketTypeId = 0;
                }
            }
            else
            {
                editDto = new DepartmentEditDto();
            }

            UpdateLocationAndNonLocationForEdit(editDto, output.LocationGroup);

            output.Department = editDto;
            return output;
        }

        public async Task CreateOrUpdateDepartment(CreateOrUpdateDepartmentInput input)
        {
            input.Department.TicketTypeId = input.Department.TicketTypeId != 0 ? input.Department.TicketTypeId : null;

            if (input.Department.Id.HasValue)
            {
                await UpdateDepartment(input);
            }
            else
            {
                await CreateDepartment(input);
            }
            await _syncAppService.UpdateSync(SyncConsts.DEPARTMENT);
        }

        public async Task DeleteDepartment(IdInput input)
        {
            await _departmentRepo.DeleteAsync(input.Id);
        }

        public async Task<ListResultOutput<DepartmentListDto>> GetIds()
        {
            var lstDepartment = await _departmentRepo.GetAll().ToListAsync();
            return new ListResultOutput<DepartmentListDto>(lstDepartment.MapTo<List<DepartmentListDto>>());
        }

        public async Task<ListResultOutput<DepartmentListDto>> GetDepartments()
        {
            var roles = _departmentRepo.GetAll().OrderBy(a => a.SortOrder);
            return new ListResultOutput<DepartmentListDto>(roles.MapTo<List<DepartmentListDto>>());
        }

        public async Task SaveSortSortItems(int[] menuItems)
        {
            var i = 1;
            foreach (var item in menuItems)
            {
                var mItem = await _departmentRepo.GetAsync(item);
                mItem.SortOrder = i++;
                await _departmentRepo.UpdateAsync(mItem);
            }
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetDepartmentsForCombobox()
        {
            var lstLocation = await _departmentRepo.GetAllListAsync(a => !a.IsDeleted);

            return
                new ListResultOutput<ComboboxItemDto>(
                    lstLocation.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _departmentRepo.GetAsync(input.Id);
                item.IsDeleted = false;

                await _departmentRepo.UpdateAsync(item);
            }
        }

        protected virtual async Task UpdateDepartment(CreateOrUpdateDepartmentInput input)
        {
            var item = await _departmentRepo.GetAsync(input.Department.Id.Value);
            var dto = input.Department;
            item.Code = dto.Code;
            item.Tag = dto.Tag;
            item.Name = dto.Name;
            item.TagId = dto.TagId;
            item.DepartmentGroupId = dto.DepartmentGroupId;
            item.TicketTypeId = dto.TicketTypeId;
            UpdateLocationAndNonLocation(item, input.LocationGroup);
            CheckErrors(await _departmentManager.CreateOrUpdateSync(item));
        }

        protected virtual async Task CreateDepartment(CreateOrUpdateDepartmentInput input)
        {
            var dto = input.Department.MapTo<Master.Department>();
            UpdateLocationAndNonLocation(dto, input.LocationGroup);
            CheckErrors(await _departmentManager.CreateOrUpdateSync(dto));
        }

        public async Task<PagedResultOutput<DepartmentGroupListDto>> GetAllDepartmentGroup(GetDepartmentGroupInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var allItems = _departmentgroupRepo.GetAll()
                    .Where(i => i.IsDeleted == input.IsDeleted);

               
                allItems = allItems
                        .WhereIf(
                            !input.Filter.IsNullOrEmpty(),
                            p => p.Name.Contains(input.Filter) ||
                                 p.Code != null && p.Code.Contains(input.Filter));

                var sortMenuItems = await allItems
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var allListDtos = sortMenuItems.MapTo<List<DepartmentGroupListDto>>();
                var allItemCount = await allItems.CountAsync();

                return new PagedResultOutput<DepartmentGroupListDto>(
                    allItemCount,
                    allListDtos
                    );
            }
        }

        public async Task<FileDto> GetAllDepartmentGroupToExcel()
        {

            var allList = await _departmentgroupRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<DepartmentGroupListDto>>();
            var baseE = new BaseExportObject()
            {
                ExportObject = allListDtos,
                ColumnNames = new string[] { "Id","Name","Code" }
            };
            return _exporter.ExportToFile(baseE, L("DepartmentGroup"));

        }

        public async Task<GetDepartmentGroupForEditOutput> GetDepartmentGroupForEdit(NullableIdInput input)
        {
            DepartmentGroupEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _departmentgroupRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<DepartmentGroupEditDto>();
            }
            else
            {
                editDto = new DepartmentGroupEditDto();
            }

            return new GetDepartmentGroupForEditOutput
            {
                DepartmentGroup = editDto
            };
        }

        public async Task CreateOrUpdateDepartmentGroup(CreateOrUpdateDepartmentGroupInput input)
        {
            if (input.DepartmentGroup.Id.HasValue)
            {
                await UpdateDepartmentGroup(input);
            }
            else
            {
                await CreateDepartmentGroup(input);
            }
        }

        public async Task<bool> DeleteDepartmentGroup(IdInput input)
        {
            bool isExists = false;
            var recordExists = await _departmentRepo.GetAllListAsync(t => t.DepartmentGroupId == input.Id);
            if(recordExists!=null&&recordExists.Count>0)
            {
                isExists = true;
                return isExists;
            }
            else
            {
                await _departmentgroupRepo.DeleteAsync(input.Id);
                return isExists;
            }
            
        }

        protected virtual async Task UpdateDepartmentGroup(CreateOrUpdateDepartmentGroupInput input)
        {
            var item = await _departmentgroupRepo.GetAsync(input.DepartmentGroup.Id.Value);
            var dto = input.DepartmentGroup;
            item.Name = dto.Name;
            item.Code = dto.Code;
            item.AddOns = dto.AddOns;
            //TODO: SERVICE DepartmentGroup Update Individually

            CheckErrors(await _departmentManager.CreateDepartmentGroupSync(item));
        }

        protected virtual async Task CreateDepartmentGroup(CreateOrUpdateDepartmentGroupInput input)
        {
            var dto = input.DepartmentGroup.MapTo<DepartmentGroup>();

            CheckErrors(await _departmentManager.CreateDepartmentGroupSync(dto));
        }

        public async Task<ListResultOutput<DepartmentGroupListDto>> GetDepartmentGroupIds()
        {
            var lstDepartmentGroup = await _departmentgroupRepo.GetAll().ToListAsync();
            return new ListResultOutput<DepartmentGroupListDto>(lstDepartmentGroup.MapTo<List<DepartmentGroupListDto>>());
        }
        public async Task ActivateDepartmentGroupItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _departmentgroupRepo.GetAsync(input.Id);
                item.IsDeleted = false;

                await _departmentgroupRepo.UpdateAsync(item);
            }
        }
        public async Task<ListResultOutput<DepartmentGroupListDto>> GetDepartmentGroups()
        {
            var departmentGroup = _departmentgroupRepo.GetAll();
            return new ListResultOutput<DepartmentGroupListDto>(departmentGroup.MapTo<List<DepartmentGroupListDto>>());
        }
        public async Task<ListResultOutput<ComboboxItemDto>> GetComboBoxDepartmentGroups()
        {
            var groups = await _departmentgroupRepo.GetAllListAsync();
            return new ListResultOutput<ComboboxItemDto>(
                    groups.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }
    }
}