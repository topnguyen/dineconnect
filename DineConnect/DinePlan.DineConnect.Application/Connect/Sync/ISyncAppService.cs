﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Configuration.Tenants.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Sync.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Sync
{
    public interface ISyncAppService : IApplicationService
    {
        Task<ListResultOutput<Syncer>> GetAll(TenantInput tenant);

        Task<List<SyncerOutput>> GetAllList();

        Task<ListResultOutput<ApiSyncerOutput>> ApiGetAll(ApiLocationInput tenant);

        Task UpdateSyncer(UpdateSyncerInput tenant);

        Task UpdateSync(string input);

        Task UpdatePushTime(ApiLocationInput input);

        Task UpdatePullTime(ApiLocationInput input);

        Task<PagedResultOutput<LastSyncList>> GetAllLastSyncs(TenantLocationPageDto tenant);

        Task<ListResultOutput<LocationSyncerOutput>> GetLocationSyncer(ApiLocationInput tenant);

        Task ApiUpdateAllSyncer(ApiLocationInput input);

        Task<FileDto> GetAllToExcel();

        Task<List<LastSyncStatusDto>> GetSyncStatusDtos();
    }
}