﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Sync.Dtos
{
    public class PostDataTotalOutput
    {
        public int TotalTicket { get; set; }
        public int SyncedTicket { get; set; }
        public decimal TotalAmount { get; set; }
    }
}
