﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.ObjectModel;

namespace DinePlan.DineConnect.Connect.Sync.Dtos
{
    public class TenantInput
    {
        public int TenantId { get; set; }
    }

    public class ApiSyncerOutput
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Guid SyncerGuid { get; set; }
    }

    [AutoMapFrom(typeof(Syncer))]
    public class SyncerOutput : IOutputDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Synced { get; set; }

        public Guid SyncerGuid { get; set; }
        public Collection<LocationSyncerOutput> LocationSyncers { get; set; }
    }

    [AutoMap(typeof(LocationSyncer))]
    public class LocationSyncerOutput : IOutputDto
    {
        public string SyncerName { get; set; }
        public Guid SyncerGuid { get; set; }
        public DateTime LastSyncTime { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public bool Synced { get; set; }
    }

    public class UpdateSyncerInput : IInputDto
    {
        public int LocationId { get; set; }
        public int TenantId { get; set; }
        public string Name { get; set; }
        public Guid SyncerGuid { get; set; }
    }

    [AutoMap(typeof(LastSync))]
    public class LastSyncList : CreationAuditedEntityDto
    {
        public string LocationName { get; set; }
        public int LocationId { get; set; }
        public DateTime LastPush { get; set; }
        public DateTime LastPull { get; set; }
        public string Version { get; set; }
        public string SystemIp { get; set; }
    }
}