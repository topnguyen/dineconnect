﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Sync.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Sync
{
    public interface ISyncDashboardAppService : IApplicationService
    {
        Task<PagedResultOutput<PostDataOutput>> GetAll(PostDataInput input);
        Task<PostDataTotalOutput> GetTotalTicket(PostDataInput input);
        void Process(PostDataInput input);
        Task<FileDto> GetAllToExcel(PostDataInput input);
        Task<ReprocessOutput> GetReProcess(int Id);
        void ReProcess(int Id);
    }
}
