﻿using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Sync.Dtos;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Sync.Exporting
{
    public class SyncDashboardExporter : FileExporterBase, ISyncDashboardExporter
    {
        public  FileDto ExportToFile(List<PostDataOutput> input,PostDataInput inputPost)
        {         
			return CreateExcelPackage(
                "SyncDashboard.xlsx",
                excelPackage =>
                {
                    int rowCount = 1;
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("SyncDashboard"));
                    sheet.OutLineApplyStyle = true;
                    sheet.Cells[1, 1].Value = "Sync Dashboard";
                    sheet.Cells[1, 1, 1,5].Merge = true;
                    sheet.Cells[1, 1, 1,5].Style.Font.Bold = true;
                    sheet.Cells[1, 1, 1,5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    rowCount++;
                    var formatDateSetting = SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.SimpleDateFormat).GetAwaiter().GetResult();
                    var formatDecimalSetting = SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.Decimals).GetAwaiter().GetResult();
                    try
                    {
                        sheet.Cells[2, 1].Value = inputPost.StartDate?.ToString(formatDateSetting) + " to " + inputPost.EndDate?.ToString(formatDateSetting);
                        sheet.Cells[2, 1, 2, 5].Merge = true;
                        sheet.Cells[2, 1, 2, 5].Style.Font.Bold = true;
                        sheet.Cells[2, 1, 2, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                    rowCount++;
                    List<string> header = new List<string> {
                        L("Location") +" "+ L("Name"),
                        L("Ticket") + " " + L("No"),
                        L("Amount"),
                        L("Date"),
                        L("Operations") };
                    AddHeader(sheet,rowCount,header.ToArray());
                    rowCount++;
                    foreach (var item in input)
                    {
                        var colCount = 1;
                        sheet.Cells[rowCount, colCount++].Value = item.Location.Name;
                        sheet.Cells[rowCount, colCount++].Value = item.TicketItem.Ticket.TicketNumber;
                        sheet.Cells[rowCount, colCount++].Value = item.TicketItem.Ticket.TotalAmount.ToString(formatDecimalSetting);
                        sheet.Column(colCount).Style.Numberformat.Format = "yyyy-mm-dd hh:mm:ss";
                        sheet.Cells[rowCount, colCount++].Value = item.TicketItem.Ticket.TicketCreatedTime;                      
                        sheet.Cells[rowCount, colCount++].Value = item.Processed == false ? "UnProcess" : "Process";
                        rowCount++;
                    }
                    for (var i = 1; i <= rowCount; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}