﻿using DinePlan.DineConnect.Connect.Sync.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Sync.Exporting
{
    public class SyncExporter : FileExporterBase, ISyncExporter
    {
        public FileDto ExportToFile(List<LastSyncList> dtos)
        {
            return CreateExcelPackage(
                "LastSyncList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("LastSync"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Location"),
                        L("LastPush"),
                        L("LastPull")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.LocationName,
                        _ => _.LastPush,
                        _ => _.LastPull
                        );

                    //Formatting cells

                    var lastPushColumn = sheet.Column(3);
                    lastPushColumn.Style.Numberformat.Format = "yyyy-mm-dd hh:mm:ss";

                    var lastPullColumn = sheet.Column(4);
                    lastPullColumn.Style.Numberformat.Format = "yyyy-mm-dd hh:mm:ss";

                    for (var i = 1; i <= 5; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}