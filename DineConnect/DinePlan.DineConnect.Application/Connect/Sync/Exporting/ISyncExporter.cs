﻿using DinePlan.DineConnect.Connect.Sync.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Sync.Exporting
{
    public interface ISyncExporter
    {
        FileDto ExportToFile(List<LastSyncList> dtos);
    }
}