﻿using DinePlan.DineConnect.Connect.Sync.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Sync.Exporting
{
    public interface ISyncDashboardExporter
    {
        FileDto ExportToFile(List<PostDataOutput> dtos,PostDataInput input);
    }
}