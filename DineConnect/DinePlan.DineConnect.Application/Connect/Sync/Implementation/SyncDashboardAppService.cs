﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Sync.Dtos;
using DinePlan.DineConnect.Connect.Sync.Exporting;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Job;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Sync.Implementation
{
    public class SyncDashboardAppService : DineConnectAppServiceBase, ISyncDashboardAppService
    {
        private readonly IRepository<PostData> _postDataRepo;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IPostAppService _postAppService;
        private readonly IBackgroundJobManager _backgroundJobManager;
        private readonly ISyncDashboardExporter _syncDashboardExporter;
        private readonly ILocationAppService _locationService;
        public SyncDashboardAppService(IRepository<PostData> postDataRepo
            , IUnitOfWorkManager unitOfWorkManager
            , IPostAppService postAppService
            , IBackgroundJobManager backgroundJobManager
            , ILocationAppService locationService
            , ISyncDashboardExporter syncDashboardExporter)
        {
            _postDataRepo = postDataRepo;
            _unitOfWorkManager = unitOfWorkManager;
            _postAppService = postAppService;
            _backgroundJobManager = backgroundJobManager;
            _syncDashboardExporter = syncDashboardExporter;
            _locationService = locationService;
        }

        public async Task<PagedResultOutput<PostDataOutput>> GetAll(PostDataInput input)
        {
            input.TenantId = AbpSession.TenantId;
            return await _postAppService.PullContents(input);
        }
        public async Task<PostDataTotalOutput> GetTotalTicket(PostDataInput input)
        {
            input.TenantId = AbpSession.TenantId;
            var result = await _postAppService.PullContents(input);         
            var totalSynced = result.Items.Where(s => s.Processed == true && s.ContentType == (int)PostContentType.Ticket).Count();
            var totalTicket = result.Items.Where(s => s.ContentType == (int)PostContentType.Ticket);
            var totalAmount = result.Items.Sum(s => s.TicketItem.Ticket.TotalAmount);
            return new PostDataTotalOutput
            {
                TotalAmount = totalAmount,
                TotalTicket = totalTicket.Count(),
                SyncedTicket = totalSynced
            };
        }
        public async Task<ReprocessOutput> GetReProcess(int Id)
        {
            var result = _postDataRepo.Get(Id);
            var pass = result.RePass;
            var fail = result.ReFail;
            var allTran = result.ReProcessCount;
            return new ReprocessOutput
            {
                Failed = fail,
                Passed = pass,
                TotalTransaction = allTran
            };
        }
        public void Process(PostDataInput input)
        {
            input.TenantId = AbpSession.TenantId;
            _backgroundJobManager.Enqueue<PostTicketJob, PostTicketJobInputArgs>(
                new PostTicketJobInputArgs()
                {
                    TenantId = input.TenantId.Value,
                    UserId = input.UserId,
                    PostDataInput = input
                });
        }
        public void ReProcess(int Id)
        {
            _postAppService.SyncTicket(Id);
        }
        public async Task<FileDto> GetAllToExcel(PostDataInput input)
        {
            input.TenantId = AbpSession.TenantId;
            input.SkipCount = 0;
            input.TotalCount = int.MaxValue;
            var allList = await _postAppService.PullContents(input);
            return _syncDashboardExporter.ExportToFile(allList.Items.ToList(), input);
        }
    }
}
