﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Logging;
using Abp.Timing;
using Castle.Core.Logging;
using DinePlan.DineConnect.Common.Dto;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Configuration.Tenants.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Sync.Dtos;
using DinePlan.DineConnect.Connect.Sync.Exporting;
using DinePlan.DineConnect.Dto;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Sync.Implementation
{
    public class SyncAppService : DineConnectAppServiceBase, ISyncAppService
    {
        private readonly IRepository<Syncer> _syncs;
        private readonly IRepository<Master.Location> _lService;
        private readonly IRepository<LastSync> _lastSyncManager;
        private readonly IRepository<LocationSyncer> _lsyncs;
        private readonly ISyncExporter _syncExporter;
        private readonly ILogger _logger;

        private readonly ITenantSettingsAppService _tenantSettingsService;

        public SyncAppService(IRepository<Syncer> syncs, ILogger logger,
            IRepository<Master.Location> locationAppService, IRepository<LastSync> lastSyncManager,
            IRepository<LocationSyncer> lsyncs,
            ITenantSettingsAppService tenantSettingsService,
            ISyncExporter syncExporter)
        {
            _syncs = syncs;
            _lService = locationAppService;
            _lastSyncManager = lastSyncManager;
            _lsyncs = lsyncs;
            _tenantSettingsService = tenantSettingsService;
            _syncExporter = syncExporter;
            _logger = logger;
        }

        public async Task<ListResultOutput<Syncer>> GetAll(TenantInput input)
        {
            var lstLocation = await _syncs.GetAllListAsync(a => a.TenantId.Equals(input.TenantId));
            return new ListResultOutput<Syncer>(lstLocation.MapTo<List<Syncer>>());
        }

        public async Task<List<SyncerOutput>> GetAllList()
        {
            var allL = await _lService.GetAllListAsync(a => !a.IsDeleted && a.TenantId == AbpSession.TenantId);
            var lstLocation = await _syncs.GetAllListAsync(a => a.Id > 0);
            var returnL = new List<SyncerOutput>(lstLocation.MapTo<List<SyncerOutput>>());
            foreach (var rr in returnL)
            {
                foreach (var lSyc in rr.LocationSyncers)
                {
                    var singleOrDefault = allL.SingleOrDefault(a => a.Id == lSyc.LocationId);
                    if (singleOrDefault != null)
                    {
                        lSyc.LocationName = singleOrDefault.Name;
                        lSyc.Synced = lSyc.SyncerGuid == rr.SyncerGuid;
                    }
                }
                if (rr.LocationSyncers.Any())
                    rr.Synced = rr.LocationSyncers.All(a => a.SyncerGuid == rr.SyncerGuid);
            }
            return returnL.OrderBy(a => a.Id).ToList();
        }

        public async Task<ListResultOutput<ApiSyncerOutput>> ApiGetAll(ApiLocationInput input)
        {
            if (input.LocationId > 0 && input.TenantId > 0)
            {
                await UpdatePullTime(new ApiLocationInput
                {
                    LocationId = input.LocationId,
                    TenantId = input.TenantId
                });
            }

            var allSyncs = await _syncs.GetAllListAsync(a => a.TenantId == input.TenantId);
            List<ApiSyncerOutput> output = allSyncs.Select(dept => new ApiSyncerOutput()
            {
                Id = dept.Id,
                Name = dept.Name,
                SyncerGuid = dept.SyncerGuid
            }).ToList();

            return new ListResultOutput<ApiSyncerOutput>(output);
        }

        public async Task UpdateSyncer(UpdateSyncerInput input)
        {
            var lSync = await _syncs.FirstOrDefaultAsync(a => a.TenantId == input.TenantId
                            && a.Name.Equals(input.Name));

            if (lSync != null)
            {
                var locUpdate = lSync.LocationSyncers.SingleOrDefault(a => a.LocationId == input.LocationId);
                if (locUpdate != null)
                {
                    locUpdate.LastSyncTime = DateTime.Now;
                    locUpdate.SyncerGuid = input.SyncerGuid;
                }
                else
                {
                    lSync.LocationSyncers.Add(new LocationSyncer()
                    {
                        LastSyncTime = DateTime.Now,
                        LocationId = input.LocationId,
                        SyncerGuid = input.SyncerGuid
                    });
                }
                await _syncs.UpdateAsync(lSync);
            }
        }

        public async Task UpdateSync(string input)
        {
            var syncer = await _syncs.FirstOrDefaultAsync(a => a.Name.Equals(input));
            if (syncer != null)
            {
                syncer.SyncerGuid = Guid.NewGuid();
                syncer.LastModificationTime = DateTime.Now;
            }
            else
            {
                syncer = new Syncer()
                {
                    Name = input,
                    TenantId = (int)(AbpSession.TenantId ?? 0),
                    SyncerGuid = Guid.NewGuid()
                };
                _syncs.InsertAndGetId(syncer);
            }
        }

        public async Task UpdatePushTime(ApiLocationInput input)
        {
            var syncer = await _lastSyncManager.FirstOrDefaultAsync(a => a.LocationId.Equals(input.LocationId) && a.TenantId == input.TenantId);
            if (syncer != null)
            {
                syncer.LastPush = DateTime.Now;
            }
            else
            {
                syncer = new LastSync()
                {
                    TenantId = input.TenantId,
                    LocationId = input.LocationId,
                    CreationTime = DateTime.Now,
                    CreatorUserId = 1,
                    LastPull = DateTime.Now,
                    LastPush = DateTime.Now,
                };
                _lastSyncManager.InsertAndGetId(syncer);
            }
        }

        public async Task UpdatePullTime(ApiLocationInput input)
        {
            if (input.LocationId > 0 && input.TenantId > 0)
            {
                try
                {
                    var mySyncer = await _lastSyncManager.FirstOrDefaultAsync(a =>
                        a.LocationId == input.LocationId && a.TenantId == input.TenantId);

                    if (mySyncer != null)
                    {
                        mySyncer.LastPull = DateTime.Now;
                    }
                    else
                    {
                        mySyncer = new LastSync()
                        {
                            TenantId = input.TenantId,
                            LocationId = input.LocationId,
                            CreationTime = DateTime.Now,
                            CreatorUserId = 1,
                            LastPull = DateTime.Now,
                            LastPush = DateTime.Now,
                        };
                        _lastSyncManager.InsertAndGetId(mySyncer);
                    }
                }
                catch (Exception e)
                {
                    _logger.Log(LogSeverity.Fatal, "Update Pull Time : Location " + input.LocationId + "Tenant : " + input.TenantId);
                }
            }
        }

        public async Task<PagedResultOutput<LastSyncList>> GetAllLastSyncs(TenantLocationPageDto input)
        {
            var allItems = GetAllList(input);

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<LastSyncList>>();
            var allItemCount = await allItems.CountAsync();

            foreach (var allL in allListDtos)
            {
                allL.LocationName = _lService.Get(allL.LocationId).Name;
            }

            return new PagedResultOutput<LastSyncList>(allItemCount, allListDtos);
        }

        public async Task<ListResultOutput<LocationSyncerOutput>> GetLocationSyncer(ApiLocationInput tenant)
        {
            var returnList = new List<LocationSyncerOutput>();
            var allDefaultSyncer = _syncs.GetAll();
            var allLocationSyncer = _lsyncs.GetAll().Where(a => a.LocationId == tenant.LocationId);

            foreach (var rr in allDefaultSyncer)
            {
                LocationSyncerOutput lso = new LocationSyncerOutput
                {
                    SyncerName = rr.Name,
                };
                var myLocationSyncer = await allLocationSyncer.FirstOrDefaultAsync(a => a.Syncer_Id == rr.Id);
                if (myLocationSyncer == null)
                {
                    lso.Synced = false;
                    lso.LastSyncTime = DateTime.Now;
                }
                else
                {
                    lso.Synced = rr.SyncerGuid == myLocationSyncer.SyncerGuid;
                    lso.LastSyncTime = myLocationSyncer.LastSyncTime;
                }
                returnList.Add(lso);
            }
            return new ListResultOutput<LocationSyncerOutput>(returnList);
        }

        public async Task ApiUpdateAllSyncer(ApiLocationInput input)
        {
            var myDate = DateTime.Now;
            var allDefaultSyncer = _syncs.GetAll();
            var allLocationSyncer = _lsyncs.GetAll().Where(a => a.LocationId == input.LocationId);

            foreach (var rr in allDefaultSyncer)
            {
                var myLocationSyncer = await allLocationSyncer.FirstOrDefaultAsync(a => a.Syncer_Id == rr.Id);
                if (myLocationSyncer == null)
                {
                    LocationSyncer lsy = new LocationSyncer()
                    {
                        LocationId = input.LocationId,
                        LastSyncTime = myDate,
                        SyncerGuid = rr.SyncerGuid,
                        Syncer_Id = rr.Id
                    };
                    await _lsyncs.InsertAndGetIdAsync(lsy);
                }
                else
                {
                    myLocationSyncer.LastSyncTime = myDate;
                    myLocationSyncer.Syncer_Id = rr.Id;
                    myLocationSyncer.SyncerGuid = rr.SyncerGuid;

                    await _lsyncs.UpdateAsync(myLocationSyncer);
                }
            }
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _lastSyncManager.GetAll()
                .Include(l => l.Location)
                .ToListAsync();
            var allListDtos = allList.MapTo<List<LastSyncList>>();
            return _syncExporter.ExportToFile(allListDtos);
        }

        public async Task<List<LastSyncStatusDto>> GetSyncStatusDtos()
        {
            var result = new List<LastSyncStatusDto>();

            var setting = await _tenantSettingsService.GetAllSettings();

            if (!setting.Connect.LastSyncStatuses.IsNullOrEmpty())
            {
                result = JsonConvert.DeserializeObject<List<LastSyncStatusDto>>(setting.Connect.LastSyncStatuses);
                result = result.Select(s =>
                {
                    var dto = s;
                    dto.Count = GetLastSyncCount(JsonConvert.SerializeObject(s));
                    return dto;
                }).ToList();
            }

            return result;
        }

        private IQueryable<LastSync> GetAllList(TenantLocationPageDto input)
        {
            var allItems = _lastSyncManager.GetAll()
                 .Include(l => l.Location)
                 .WhereIf(input.LocationId > 0, a => a.LocationId == input.LocationId);

            var setting = new LastSyncStatusDto();

            if (!input.Status.IsNullOrEmpty())
            {
                setting = JsonConvert.DeserializeObject<LastSyncStatusDto>(input.Status);

                var currentTime = Clock.Now;
                var fromMin = setting.StartMinute + setting.StartHour * 60; ;
                var toMin = setting.EndMinute + setting.EndHour * 60;

                allItems = allItems
                    .WhereIf(fromMin == 0 && toMin == 0, a => DbFunctions.DiffDays(a.LastPush, currentTime) >= 1)
                    .WhereIf(fromMin > 0 || toMin > 0,
                                a => (DbFunctions.DiffMinutes(a.LastPush, currentTime) >= fromMin && DbFunctions.DiffMinutes(a.LastPush, currentTime) <= toMin));
            }

            return allItems;
        }

        private int GetLastSyncCount(string status)
        {
            var input = new TenantLocationPageDto
            {
                Status = status,
                SkipCount = 0,
                MaxResultCount = 100000
            };

            return GetAllList(input).Count();
        }
    }
}