﻿using Abp.Application.Services.Dto;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System;
using Abp.Configuration;
using System.Data.Entity;
using DinePlan.DineConnect.Connect.Custom.Exporting;
using DinePlan.DineConnect.Connect.Ticket;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using System.Data.Entity.Core.Objects;
using DinePlan.DineConnect.Report;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Connect.SalesPromotionReport.Dto;
using DinePlan.DineConnect.Connect.SalesPromotionReport.Exporting;
using Abp.AutoMapper;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Configuration.Tenants;

namespace DinePlan.DineConnect.Connect.SalesPromotionReport
{
    public class SalesPromotionReportAppService : DineConnectAppServiceBase, ISalesPromotionReportAppService
    {
        private readonly IConnectReportAppService _connectReportAppService;
        private readonly IRepository<Company> _companyRe;
        private readonly IRepository<Master.Department> _dRepo;
        private readonly ILocationAppService _locService;
        private readonly IRepository<Master.Location> _lRepo;
        private readonly IRepository<Transaction.Ticket> _ticketRepository;
        private readonly IRepository<Transaction.Order> _orderManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IReportBackgroundAppService _rbas;
        private readonly IBackgroundJobManager _bgm;
        private readonly ISalesPromotionReportExporter _exporter;
        private readonly IRepository<Period.WorkPeriod> _workPeriod;
        private readonly IRepository<PaymentType> _payRe;
        private readonly IRepository<MenuItem> _menuItemRepository;
        private readonly IReportBackgroundAppService _reportBackgroundService;
        private readonly IBackgroundJobManager _backgroundJobManager;
        private readonly IRepository<Promotion> _promotionRepository;
        private readonly IRepository<PromotionCategory> _promotionCateRepo;
        private readonly SettingManager _settingManager;
        private readonly ITenantSettingsAppService _tenantSettingsService;
        public SalesPromotionReportAppService(
            IRepository<Transaction.Ticket> ticketRepository,
            ILocationAppService locService,
            IRepository<Company> comR,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<Master.Department> drepo,
            IRepository<Master.Location> lRepo,
            IRepository<Transaction.Order> orderManager,
             IReportBackgroundAppService rbas,
             IBackgroundJobManager bgm,
             ISalesPromotionReportExporter exporter,
             IRepository<Period.WorkPeriod> workPeriod,
             IRepository<PaymentType> payRe,
             IBackgroundJobManager backgroundJobManager,
             IReportBackgroundAppService reportBackgroundService,
             IRepository<MenuItem> menuItemRepository,
             IConnectReportAppService connectReportAppService,
             IRepository<Promotion> promotionRepository,
             IRepository<PromotionCategory> promotionCateRepo,
             SettingManager settingManager, 
             ITenantSettingsAppService tenantSettingsService
            )
        {
            _connectReportAppService = connectReportAppService;
            _ticketRepository = ticketRepository;
            _companyRe = comR;
            _unitOfWorkManager = unitOfWorkManager;
            _dRepo = drepo;
            _locService = locService;
            _lRepo = lRepo;
            _orderManager = orderManager;
            _rbas = rbas;
            _bgm = bgm;
            _exporter = exporter;
            _workPeriod = workPeriod;
            _payRe = payRe;
            _menuItemRepository = menuItemRepository;
            _reportBackgroundService = reportBackgroundService;
            _backgroundJobManager = backgroundJobManager;
            _promotionRepository = promotionRepository;
            _promotionCateRepo = promotionCateRepo;
            _settingManager = settingManager;
            _tenantSettingsService = tenantSettingsService;
        }

        #region by promotion
        [Obsolete]
        public async Task<SalesByPromotionList> GetPromotionByPromotion(SalesPromotionInput input)
        {
            var setting = await _tenantSettingsService.GetAllSettings();
            input.DatetimeFormat = setting.Connect.DateTimeFormat;
            input.DateFormat = setting.Connect.SimpleDateFormat;

            var returnOutput = new SalesByPromotionList();
            var proItemForOneTicket = new List<SalesByPromotionItems>();

            var proByPromo = new List<SalesByPromotionListGroupByPromo>();
            var allTicket = GetAllTicketByCondition(input);

            foreach (var ticket in allTicket)
            {
                var proItem = new List<SalesByPromotionItems>();
                var findLoc = await _locService.GetLocationById(new EntityDto { Id = ticket.LocationId });
                if (findLoc != null)
                {
                    if (!string.IsNullOrEmpty(ticket.TicketPromotionDetails))
                    {
                        var ticketPromo = JsonConvert.DeserializeObject<List<PromotionDetailValue>>(ticket.TicketPromotionDetails);
                        ticketPromo.ToList().ForEach(s => proItem.Add(new SalesByPromotionItems
                        {
                            TicketId = ticket.Id,
                            BillValue = ticket.GetPlainSum(),
                            DiscountValue = Math.Abs(s.PromotionAmount),
                            PromotionName = s.PromotionName,
                        }));
                    }
                    var orders = GetAllOrdersByCondition(input, ticket);
                    foreach (var order in orders)
                    {
                        if (!string.IsNullOrEmpty(order.OrderPromotionDetails))
                        {
                            var oderPromo = order.GetOrderPromotionList();
                            oderPromo.ToList().ForEach(or => proItem.Add(
                                new SalesByPromotionItems
                                {
                                    TicketId = ticket.Id,
                                    BillValue = ticket.GetPlainSum(),
                                    DiscountValue = Math.Abs(or.PromotionAmount),
                                    PromotionName = or.PromotionName,
                                }));
                        }
                    }

                    if (proItem.Any())
                    {
                        var proItemByProId = from d in proItem
                                             group d by new { d.PromotionName }
                                         into g
                                             select new { PromotionName = g.Key.PromotionName, Items = g };
                        foreach (var pr in proItemByProId)
                        {
                            proItemForOneTicket.Add(new SalesByPromotionItems
                            {
                                PlantCode = findLoc.Code,
                                PlantName = findLoc.Name,
                                DiscountValue = pr.Items.Sum(s => s.DiscountValue),
                                PromotionName = pr.PromotionName,
                                NumberItemQuantity = pr.Items.Count(),
                                BillAmount = pr.Items.GroupBy(t => new { t.TicketId, t.BillValue }).Count(),
                                BillValue = pr.Items.GroupBy(t => new { t.TicketId, t.BillValue }).Sum(s => s.Key.BillValue),
                            });
                        }
                    }
                }
            }
            var itemGroupByPromo = from d in proItemForOneTicket
                                   group d by new { d.PromotionName }
                                         into g
                                   select new { PromotionName = g.Key.PromotionName, Items = g };
            foreach (var groupPromo in itemGroupByPromo)
            {
                var proByPlant = new List<SalesByPromotionListGroupByPlant>();
                var groupByPlant = from d in groupPromo.Items
                                   group d by new { d.PlantCode, d.PlantName }
                                         into g
                                   select new { PlantCode = g.Key.PlantCode, PlantName = g.Key.PlantName, Items = g };
                foreach (var groupItem in groupByPlant)
                {
                    proByPlant.Add(new SalesByPromotionListGroupByPlant
                    {
                        PromotionName = groupPromo.PromotionName,
                        PlantCode = groupItem.PlantCode,
                        PlantName = groupItem.PlantName,
                        ListItem = groupItem.Items.ToList(),
                        TotalDiscountValue = groupPromo.Items.ToList().Sum(s => s.DiscountValue)
                    });
                }
                // Filter dynamic
                var mapdataAsQueryable = proByPlant.AsQueryable();
                if (!string.IsNullOrEmpty(input.DynamicFilter))
                {
                    var jsonSerializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver =
                            new CamelCasePropertyNamesContractResolver()
                    };
                    var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                    if (filRule?.Rules != null)
                    {
                        mapdataAsQueryable = mapdataAsQueryable.BuildQuery(filRule);
                    }
                }
                proByPlant = mapdataAsQueryable.ToList();
                if (proByPlant.Count > 0)
                {
                    proByPromo.Add(new SalesByPromotionListGroupByPromo
                    {
                        PromotionName = groupPromo.PromotionName,
                        TotalDiscountValue = proByPlant.Sum(s => s.DiscountValue),
                        ListItem = proByPlant
                    });
                }

            }
            returnOutput.StartDate = input.StartDate.ToString(input.DateFormat);
            returnOutput.EndDate = input.EndDate.ToString(input.DateFormat);
            if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                if (locations.Count == 1)
                {
                    foreach (var loc in locations)
                    {
                        var findLoc = await _locService.GetLocationById(new EntityDto { Id = loc });
                        if (findLoc != null)
                        {
                            returnOutput.LocationCode = findLoc.Code;
                            returnOutput.LocationName = findLoc.Name;
                        }
                    }
                }
            }
            returnOutput.ListItem.AddRange(proByPromo);
            return returnOutput;
        }
        public async Task<FileDto> GetPromotionByPromotionToExport(SalesPromotionInput input)
        {
            input.TenantId = AbpSession.TenantId.Value;
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _reportBackgroundService.CreateOrUpdate(new Report.Dtos.CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.SALESPROMOTIONBYPROMOTION,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                    {
                        var salesPromotionInput = input;
                        salesPromotionInput.UserId = AbpSession.UserId.Value;

                        await _backgroundJobManager.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.SALESPROMOTIONBYPROMOTION,
                            SalesPromotionInput = salesPromotionInput,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                    }
                }
                else
                {
                    return await _exporter.ExportSalePromotionByPromotionReport(input, this);
                }
            }
            return null;
        }
        #endregion
        #region by plant
        [Obsolete]
        public async Task<SalesByPlantListTotal> GetPromotionByPlant(SalesPromotionInput input)
        {
            var returnOutput = new SalesByPlantListTotal();
            var proItemForOneTicket = new List<SalesByPlantItems>();
            var allProCate = _promotionCateRepo.GetAll();
            var proByPlant = new List<SalesByPlantList>();
            var allTicket = GetAllTicketByCondition(input);

            foreach (var ticket in allTicket)
            {
                var proItem = new List<SalesByPlantItems>();
                var findLoc = await _locService.GetLocationById(new EntityDto { Id = ticket.LocationId });
                if (findLoc != null)
                {
                    var orders = GetAllOrdersByCondition(input, ticket);
                    foreach (var order in orders)
                    {
                        if (!string.IsNullOrEmpty(order.OrderPromotionDetails))
                        {
                            var oderPromo = order.GetOrderPromotionList();
                            foreach (var or in oderPromo)
                            {
                                proItem.Add(new SalesByPlantItems
                                {
                                    BillValue = ticket.GetPlainSum(),
                                    TicketId = ticket.Id,
                                    PromotionType = or.PromotionType,
                                    Date = ticket.LastPaymentTime,
                                    DateString = ticket.LastPaymentTime.ToString("yyyy-MM-dd"),
                                    DiscountValue = Math.Abs(or.PromotionAmount),
                                    PromotionName = or.PromotionName,
                                });
                            }
                        }

                    }
                    if (!string.IsNullOrEmpty(ticket.TicketPromotionDetails))
                    {
                        var ticketPromo = JsonConvert.DeserializeObject<List<PromotionDetailValue>>(ticket.TicketPromotionDetails);
                        foreach (var promo in ticketPromo)
                        {
                            proItem.Add(new SalesByPlantItems
                            {
                                BillValue = ticket.GetPlainSum(),
                                TicketId = ticket.Id,
                                PromotionType = promo.PromotionType,
                                Date = ticket.LastPaymentTime,
                                DateString = ticket.LastPaymentTime.ToString("yyyy-MM-dd"),
                                DiscountValue = Math.Abs(promo.PromotionAmount),
                                PromotionName = promo.PromotionName,
                            });
                        }
                    }
                    if (proItem.Any())
                    {
                        var proItemByProId = from d in proItem
                                             group d by new { d.PromotionName, d.DateString, d.PromotionType }
                                         into g
                                             select new { DateString = g.Key.DateString, PromotionName = g.Key.PromotionName, PromotionType = g.Key.PromotionType, Items = g };
                        foreach (var pr in proItemByProId)
                        {
                            var findPromotion = _promotionRepository.FirstOrDefault(s => s.Name == pr.PromotionName);
                            var finProCate = new PromotionCategory();
                            if (findPromotion != null)
                            {
                                finProCate = _promotionCateRepo.FirstOrDefault(s => s.Id == findPromotion.PromotionCategoryId);
                            }

                            proItemForOneTicket.Add(new SalesByPlantItems
                            {
                                PlantCode = findLoc.Code,
                                PlantName = findLoc.Name,
                                Date = Convert.ToDateTime(pr.DateString),
                                DateString = pr.DateString,
                                PromotionType = pr.PromotionType,
                                PromotionCategory = finProCate != null ? finProCate.Name : "",
                                ConditionType = finProCate != null ? finProCate.Code : "",
                                DiscountValue = pr.Items.Sum(s => s.DiscountValue),
                                PromotionName = pr.PromotionName,
                                Quantity = pr.Items.Count(),
                                BillAmount = pr.Items.GroupBy(t => new { t.TicketId, t.BillValue }).Count(),
                            });
                        }
                    }
                }
            }
            var itemGroupByPlant = from d in proItemForOneTicket
                                   group d by new { d.PlantCode, d.PlantName }
                                         into g
                                   select new { PlantCode = g.Key.PlantCode, PlantName = g.Key.PlantName, Items = g };
            foreach (var itemGroup in itemGroupByPlant)
            {
                var itemGroupPro = from d in itemGroup.Items
                                   group d by new { d.DateString, d.PromotionName }
                                          into g
                                   select new { DateString = g.Key.DateString, Items = g, PromotionName = g.Key.PromotionName };
                var listAddTotal = new List<SalesByPlantItems>();
                foreach (var item in itemGroupPro)
                {
                    var first = item.Items.FirstOrDefault();
                    listAddTotal.Add(new SalesByPlantItems
                    {
                        PlantCode = first.PlantCode,
                        PlantName = first.PlantName,
                        Date = first.Date,
                        DateString = first.DateString,
                        PromotionType = first.PromotionType,
                        PromotionCategory = first.PromotionCategory,
                        ConditionType = first.ConditionType,
                        DiscountValue = item.Items.Sum(s => s.DiscountValue),
                        PromotionName = first.PromotionName,
                        Quantity = item.Items.Sum(s => s.Quantity),
                        BillAmount = item.Items.Sum(s => s.BillAmount),
                        TotalDiscountValue = itemGroup.Items.Sum(s => s.DiscountValue)
                    });
                }
                var listAddTotalOrder = listAddTotal.OrderBy(s => s.Date);
                var mapdataAsQueryable = listAddTotal.AsQueryable();
                if (!string.IsNullOrEmpty(input.DynamicFilter))
                {
                    var jsonSerializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver =
                            new CamelCasePropertyNamesContractResolver()
                    };
                    var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                    if (filRule?.Rules != null)
                    {
                        mapdataAsQueryable = mapdataAsQueryable.BuildQuery(filRule);
                    }
                }
                listAddTotal = mapdataAsQueryable.ToList();
                if (listAddTotal.Count > 0)
                {
                    proByPlant.Add(new SalesByPlantList
                    {
                        PlantCode = itemGroup.PlantCode,
                        PlantName = itemGroup.PlantName,
                        ListItem = listAddTotal
                    });
                }
            }
            returnOutput.StartDate = input.StartDate.ToString(input.DateFormat);
            returnOutput.EndDate = input.EndDate.ToString(input.DateFormat);
            if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                if (locations.Count == 1)
                {
                    foreach (var loc in locations)
                    {
                        var findLoc = await _locService.GetLocationById(new EntityDto { Id = loc });
                        if (findLoc != null)
                        {
                            returnOutput.LocationCode = findLoc.Code;
                            returnOutput.LocationName = findLoc.Name;
                        }
                    }
                }
            }
            returnOutput.ListItem.AddRange(proByPlant);
            return returnOutput;
        }
        public async Task<FileDto> GetPromotionByPlantToExport(SalesPromotionInput input)
        {
            input.TenantId = AbpSession.TenantId.Value;
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _reportBackgroundService.CreateOrUpdate(new Report.Dtos.CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.SALESPROMOTIONBYPLANT,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                    {

                        var salesPromotionInput = input;
                        salesPromotionInput.UserId = AbpSession.UserId.Value;

                        await _backgroundJobManager.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.SALESPROMOTIONBYPLANT,
                            SalesPromotionInput = salesPromotionInput,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                    }
                }
                else
                {
                    return await _exporter.ExportSalePromotionByPlantReport(input, this);
                }
            }
            return null;
        }
        #endregion
        [Obsolete]
        public IQueryable<Transaction.Ticket> GetAllTicketByCondition(SalesPromotionInput input)
        {
            DateTime endDate = input.EndDate.AddDays(1);
            var tickets = _ticketRepository.GetAll();
            tickets = tickets.Where(a => a.LastPaymentTime >= input.StartDate && a.LastPaymentTime <= endDate);

            tickets = tickets.Where(x => (x.LastPaymentTime.Hour * 60 + x.LastPaymentTime.Minute) >= (input.StartHour * 60 + input.StartMinute)
                                            && (x.LastPaymentTime.Hour * 60 + x.LastPaymentTime.Minute) <= (input.EndHour * 60 + input.EndMinute));
            if (input.Location > 0)
            {
                tickets = tickets.Where(a => a.LocationId == input.Location);
            }
            else if (input.Locations != null && input.Locations.Any())
            {
                var locations = input.Locations.Select(a => a.Id).ToList();
                tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
                     && !input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                if (locations.Any()) tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true
                });
                tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }

            if (input.LocationGroup != null)
            {
                if (input.LocationGroup.NonLocations != null && input.LocationGroup.NonLocations.Any())
                {
                    var nonlocations = input.LocationGroup.NonLocations.Select(a => a.Id).ToList();
                    tickets = tickets.Where(a => !nonlocations.Contains(a.LocationId));
                }
            }

            else if (input.LocationGroup == null)
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = new List<SimpleLocationDto>(),
                    Group = false,
                    UserId = input.UserId
                });
                if (input.UserId > 0)
                    tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }

            if (!string.IsNullOrEmpty(input.DepartmentSelect) && input.DepartmentSelect != "All")
                tickets = tickets.Where(a => a.DepartmentName.Contains(input.DepartmentSelect));

            return tickets;
        }
        [Obsolete]
        public IQueryable<Order> GetAllOrdersByCondition(SalesPromotionInput input, Transaction.Ticket tickets)
        {
            var allOders = _orderManager.GetAll();
            var orders = allOders.Where(a => a.TicketId == tickets.Id);

            if (input.Locations.Any())
            {
                var locationIds = input.Locations.Select(l => l.Id).ToList();
                orders = orders.Where(o => locationIds.Contains(o.Location_Id));
            }

            if (input.ListMaterial != null && input.ListMaterial.Any())
                orders = orders
                    .WhereIf(input.ListMaterial.Any(), o => input.ListMaterial.Contains(o.MenuItem.Category.ProductGroupId ?? -1));

            if (!string.IsNullOrEmpty(input.LastModifiedUserName))
                orders = orders.Where(a => a.CreatingUserName.Contains(input.LastModifiedUserName));
            return orders;
        }

    }
}
