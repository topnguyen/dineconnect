﻿using DinePlan.DineConnect.Connect.SalesPromotionReport.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.SalesPromotionReport.Exporting
{
    public class SalesPromotionReportExporter : FileExporterBase, ISalesPromotionReportExporter
    {
        #region Plant
        public async Task<FileDto> ExportSalePromotionByPlantReport(SalesPromotionInput input, ISalesPromotionReportAppService appService)
        {
            var builder = new StringBuilder();
            builder.Append(L("SalesPromotionReportByPlant"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append("_");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            if (input.ExportOutputType == 0)
            {
                using (var excelPackage = new ExcelPackage())
                {
                    await GetSalePromotionByPlantReportExportExcel(excelPackage, input, appService);
                    Save(excelPackage, file);
                }
                return ProcessFile(input, file);
            }
            else
            {
                var contentHTML = await ExportSalePromotionByPlantToHtml(input, appService);
                var result = ProcessFileHTML(file, contentHTML);
                return result;
            }
        }
        private async Task GetSalePromotionByPlantReportExportExcel(ExcelPackage package, SalesPromotionInput input, ISalesPromotionReportAppService appService)
        {
            var worksheet = package.Workbook.Worksheets.Add(L("SalesPromotionReportByPlant"));
            worksheet.OutLineApplyStyle = true;
            input.MaxResultCount = 10000;
            var dtos = await appService.GetPromotionByPlant(input);
            // Add header
            var textTitle = L("SalesPromotionReportByPlant") + "<br/>";
            textTitle += (dtos.StartDate != dtos.EndDate ? L("Date") + " " + dtos.StartDate + " " + L("To") + " " + dtos.EndDate : L("Date") + " " + dtos.StartDate);
            if (!string.IsNullOrEmpty(dtos.LocationName))
            {
                textTitle = textTitle + "<br/>" + dtos.LocationName;
            }

            string textTitleWithNewLine = textTitle.Replace("<br/>", Environment.NewLine);
            int headerCol = 10;
            worksheet.Cells[1, 1].Value = textTitleWithNewLine;
            worksheet.Cells[1, 1, 4, headerCol].Merge = true;
            worksheet.Cells[1, 1, 4, headerCol].Style.Font.Bold = true;
            worksheet.Cells[1, 1, 4, headerCol].Style.Font.Size = 11;
            worksheet.Cells[1, 1, 4, headerCol].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 1].Style.Border.BorderAround(ExcelBorderStyle.None);
            worksheet.Cells[1, 1, 4, headerCol].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            worksheet.Cells[1, 1, 4, headerCol].Style.WrapText = true;

            var listheader = new List<string> {
                L("PlantCode"),
                L("PlantName"),
                L("Date"),
                L("Promotion"),
                L("PromotionCategory"),
                L("ConditionType"),
                L("Quantity"),
                L("BillAmount"),
                L("DiscountValue"),
                L("DiscountValuePercent") };

            var colhead = 1;
            Color colorHeader = System.Drawing.ColorTranslator.FromHtml("#99CCFF");
            Color colorPromotion = System.Drawing.ColorTranslator.FromHtml("#00FFFF");
            Color colorFooter = System.Drawing.ColorTranslator.FromHtml("#cccccc");
            foreach (var header in listheader)
            {

                worksheet.Cells[5, colhead].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[5, colhead].Value = header;
                worksheet.Column(colhead).AutoFit();
                colhead++;
            }
            for (var i = 1; i < headerCol; i++)
            {
                worksheet.Cells[5, i].Style.WrapText = true;
            }
            var colHeadColor = 10;
            worksheet.Cells[5, 1, 5, colHeadColor].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            worksheet.Cells[5, 1, 5, colHeadColor].Style.Font.Bold = true;
            worksheet.Cells[5, 1, 5, colHeadColor].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells[5, 1, 5, colHeadColor].Style.Fill.BackgroundColor.SetColor(colorHeader);
            worksheet.Cells[5, 1, 5, colHeadColor].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells[5, 1, 5, colHeadColor].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            var row = 6;
            var index = 1;

            var colData = 1;
            foreach (var itemDto in dtos.ListItem)
            {
                foreach (var item in itemDto.ListItem)
                {
                    worksheet.Cells[row, colData].Value = item.PlantCode;
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = item.PlantName;
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = item.DateString;
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = item.PromotionName;
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = item.PromotionCategory;
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = item.ConditionType;
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = item.Quantity;
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = item.BillAmount;
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = item.DiscountValue.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = item.DiscountValuePercent.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    colData = 1;
                    row++;
                }
                worksheet.Cells[row, 1, row, 6].Merge = true;
                worksheet.Cells[row, 1, row, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 1, row, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                worksheet.Cells[row, 1, row, 6].Value = L("Total");

                worksheet.Cells[row, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 7].Value = itemDto.Quantity;
                worksheet.Cells[row, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                worksheet.Cells[row, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 8].Value = itemDto.BillAmount;
                worksheet.Cells[row, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                worksheet.Cells[row, 9].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 9].Value = itemDto.DiscountValue.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                worksheet.Cells[row, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                worksheet.Cells[row, 10].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 10].Value = itemDto.DiscountValuePercent.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                worksheet.Cells[row, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells[row, 1, row, headerCol].Style.Font.Bold = true;
                worksheet.Cells[row, 1, row, headerCol].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[row, 1, row, headerCol].Style.Fill.BackgroundColor.SetColor(colorFooter);
                worksheet.Cells[row, 1, row, headerCol].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                index++;
                row++;
            }
            // Add footer

            worksheet.Cells[row, 1, row, 6].Merge = true;
            worksheet.Cells[row, 1, row, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            worksheet.Cells[row, 1, row, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            worksheet.Cells[row, 1, row, 6].Value = L("Total");

            worksheet.Cells[row, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            worksheet.Cells[row, 7].Value = dtos.Quantity;
            worksheet.Cells[row, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            worksheet.Cells[row, 8].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            worksheet.Cells[row, 8].Value = dtos.BillAmount;
            worksheet.Cells[row, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            worksheet.Cells[row, 9].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            worksheet.Cells[row, 9].Value = dtos.DiscountValue.ToString(ConnectConsts.ConnectConsts.NumberFormat);
            worksheet.Cells[row, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            worksheet.Cells[row, 10].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            worksheet.Cells[row, 10].Value = dtos.DiscountValuePercent.ToString(ConnectConsts.ConnectConsts.NumberFormat);
            worksheet.Cells[row, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            
            worksheet.Cells[row, 1, row, headerCol].Style.Font.Bold = true;
            worksheet.Cells[row, 1, row, headerCol].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells[row, 1, row, headerCol].Style.Fill.BackgroundColor.SetColor(colorFooter);
            worksheet.Cells[row, 1, row, headerCol].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            for (var i = 1; i <= 10; i++)
            {
                worksheet.Column(i).AutoFit();
            }
        }
        private async Task<string> ExportSalePromotionByPlantToHtml(SalesPromotionInput input, ISalesPromotionReportAppService appService)
        {
            input.MaxResultCount = 10000;
            var dtos = await appService.GetPromotionByPlant(input);
            var businessDate = dtos.StartDate != dtos.EndDate ? L("Date") + " " + dtos.StartDate + " " + L("To") + " " + dtos.EndDate : L("Date") + " " + dtos.StartDate;
            StringBuilder strHTMLBuilder = new StringBuilder();
            strHTMLBuilder.Append("<html>");
            strHTMLBuilder.Append("<head>");
            strHTMLBuilder.Append("</head>");
            var styleTable = GetStyleForTable();
            strHTMLBuilder.Append(styleTable);
            strHTMLBuilder.Append("<body>");
            strHTMLBuilder.Append("<div>");

            // add header
            var titleHeader = L("SalesPromotionReportByPlant") + "<br>" + businessDate;
            if (!string.IsNullOrEmpty(dtos.LocationCode) && !string.IsNullOrEmpty(dtos.LocationName))
            {
                titleHeader += "<br>" + "(" + dtos.LocationCode + ")" + dtos.LocationName;
            }
            strHTMLBuilder.Append("<table class='table table-bordered custom_table_date border_bottom_none'>");
            strHTMLBuilder.Append("<tr class='font_14'>");
            strHTMLBuilder.Append($"<td colspan='10' class='border_bottom_none text-center font_weight_bold'>{titleHeader}</td>");
            strHTMLBuilder.Append("</tr>");
            strHTMLBuilder.Append("<tr class=' font_14'>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("PlantCode")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("PlantName")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("Date")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("Promotion")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("PromotionCategory")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("ConditionType")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("Quantity")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("BillAmount")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("DiscountValue")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("DiscountValuePercent")}</th>");
            strHTMLBuilder.Append("</tr>");

            // add content
            foreach (var item in dtos.ListItem)
            {
                foreach (var data in item.ListItem)
                {
                    strHTMLBuilder.Append("<tr class='font_14'>");
                    strHTMLBuilder.Append($"<td class='text-center'> { data.PlantCode}</td> ");
                    strHTMLBuilder.Append($"<td class='text-center'> { data.PlantName}</td> ");
                    strHTMLBuilder.Append($"<td class='text-center'> { data.DateString}</td> ");
                    strHTMLBuilder.Append($"<td class='text-center'> { data.PromotionName}</td> ");
                    strHTMLBuilder.Append($"<td class='text-center'> { data.PromotionCategory}</td> ");
                    strHTMLBuilder.Append($"<td class='text-center'> { data.ConditionType}</td> ");
                    strHTMLBuilder.Append($"<td class='text-center'> { data.Quantity}</td> ");
                    strHTMLBuilder.Append($"<td class='text-center'> { data.BillAmount}</td> ");
                    strHTMLBuilder.Append($"<td class='text-center'> { data.DiscountValue.ToString(ConnectConsts.ConnectConsts.NumberFormat)} </td> ");
                    strHTMLBuilder.Append($"<td class='text-center'> { data.DiscountValuePercent.ToString(ConnectConsts.ConnectConsts.NumberFormat)}%</td> ");
                    strHTMLBuilder.Append("</tr>");
                }
                strHTMLBuilder.Append("<tr class='font_14 background-gray font_weight_bold'>");
                strHTMLBuilder.Append($"<td class='text-right' colspan = '6'> {L("Total")}</td> ");
                strHTMLBuilder.Append($"<td class='text-center'> { item.Quantity}</td> ");
                strHTMLBuilder.Append($"<td class='text-center'> { item.BillAmount}</td> ");
                strHTMLBuilder.Append($"<td class='text-center'> { item.DiscountValue.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td> ");
                strHTMLBuilder.Append($"<td class='text-center'> { item.DiscountValuePercent.ToString(ConnectConsts.ConnectConsts.NumberFormat)}% </td> ");
                strHTMLBuilder.Append("</tr>");
            }

            strHTMLBuilder.Append("<tr class='font_14 background-gray font_weight_bold'>");
            strHTMLBuilder.Append($"<td class='text-right' colspan = '6'> {L("Total")}</td> ");
            strHTMLBuilder.Append($"<td class='text-center'> { dtos.Quantity}</td> ");
            strHTMLBuilder.Append($"<td class='text-center'> { dtos.BillAmount}</td> ");
            strHTMLBuilder.Append($"<td class='text-center'> { dtos.DiscountValue.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td> ");
            strHTMLBuilder.Append($"<td class='text-center'> { dtos.DiscountValuePercent.ToString(ConnectConsts.ConnectConsts.NumberFormat)}% </td> ");
            strHTMLBuilder.Append("</tr>");
            strHTMLBuilder.Append("</table>");
            strHTMLBuilder.Append("</div>");
            string Htmltext = strHTMLBuilder.ToString();
            return Htmltext;
        }
        #endregion
        #region Promotion
        public async Task<FileDto> ExportSalePromotionByPromotionReport(SalesPromotionInput input, ISalesPromotionReportAppService appService)
        {
            var builder = new StringBuilder();
            builder.Append(L("SalesPromotionReportByPromotion"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append("_");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            if (input.ExportOutputType == 0)
            {
                using (var excelPackage = new ExcelPackage())
                {
                    await GetSalePromotionByPromotionReportExportExcel(excelPackage, input, appService);
                    Save(excelPackage, file);
                }
                return ProcessFile(input, file);
            }
            else
            {
                var contentHTML = await ExportSalePromotionByPromotionToHtml(input, appService);
                var result = ProcessFileHTML(file, contentHTML);
                return result;
            }
        }

        private async Task GetSalePromotionByPromotionReportExportExcel(ExcelPackage package, SalesPromotionInput input, ISalesPromotionReportAppService appService)
        {
            var worksheet = package.Workbook.Worksheets.Add(L("SalesPromotionReportByPromotion"));
            worksheet.OutLineApplyStyle = true;
            input.MaxResultCount = 10000;
            var dtos = await appService.GetPromotionByPromotion(input);
            // Add header
            var textTitle = L("SalesPromotionReportByPromotion") + "<br/>";
            textTitle += (dtos.StartDate != dtos.EndDate ? L("Date") + " " + dtos.StartDate + " " + L("To") + " " + dtos.EndDate : L("Date") + " " + dtos.StartDate);
            if (!string.IsNullOrEmpty(dtos.LocationName))
            {
                textTitle = textTitle + "<br/>" + dtos.LocationName;
            }

            string textTitleWithNewLine = textTitle.Replace("<br/>", Environment.NewLine);
            int headerCol = 9;
            worksheet.Cells[1, 1].Value = textTitleWithNewLine;
            worksheet.Cells[1, 1, 4, headerCol].Merge = true;
            worksheet.Cells[1, 1, 4, headerCol].Style.Font.Bold = true;
            worksheet.Cells[1, 1, 4, headerCol].Style.Font.Size = 11;
            worksheet.Cells[1, 1, 4, headerCol].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells[1, 1].Style.Border.BorderAround(ExcelBorderStyle.None);
            worksheet.Cells[1, 1, 4, headerCol].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            worksheet.Cells[1, 1, 4, headerCol].Style.WrapText = true;

            var listheader = new List<string> {
                L("PlantCode"),
                L("PlantName"),
                L("Quantity"),
                L("NumberOfBill"),
                L("BillValue"),
                L("DiscountValue"),
                L("DiscountValuePercent"),
                L("AverageDiscountPerItem"),
                L("AverageDiscountPerBill") };

            var colhead = 1;
            Color colorHeader = System.Drawing.ColorTranslator.FromHtml("#99CCFF");
            Color colorPromotion = System.Drawing.ColorTranslator.FromHtml("#00FFFF");
            foreach (var header in listheader)
            {
                worksheet.Column(colhead).AutoFit();
                worksheet.Cells[5, colhead].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[5, colhead].Value = header;
                colhead++;
            }
            for (var i = 1; i < headerCol; i++)
            {
                worksheet.Cells[5, i].Style.WrapText = true;
            }
            var colHeadColor = 9;
            worksheet.Cells[5, 1, 5, colHeadColor].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            worksheet.Cells[5, 1, 5, colHeadColor].Style.Font.Bold = true;
            worksheet.Cells[5, 1, 5, colHeadColor].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells[5, 1, 5, colHeadColor].Style.Fill.BackgroundColor.SetColor(colorHeader);
            worksheet.Cells[5, 1, 5, colHeadColor].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells[5, 1, 5, colHeadColor].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            var row = 6;
            var index = 1;

            var colData = 1;
            foreach (var itemDto in dtos.ListItem)
            {

                worksheet.Cells[row, colData, row, headerCol].Merge = true;
                worksheet.Cells[row, colData, row, headerCol].Style.Font.Bold = true;
                worksheet.Cells[row, colData, row, headerCol].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[row, colData, row, headerCol].Style.Fill.BackgroundColor.SetColor(colorPromotion);
                worksheet.Cells[row, colData, row, headerCol].Value = L("Promotion")+"/" + itemDto.PromotionName;
                worksheet.Cells[row, colData, row, headerCol].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                worksheet.Cells[row, colData, row, headerCol].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                row++;
                foreach (var item in itemDto.ListItem)
                {
                    worksheet.Cells[row, colData].Value = item.PlantCode;
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = item.PlantName;
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = item.NumberItemQuantity;
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = item.BillAmount;
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = item.BillValue.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = item.DiscountValue.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = item.DiscountValuePercent.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = item.AverageDiscountPerItem.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    worksheet.Cells[row, colData].Value = item.AverageDiscountPerBill.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    worksheet.Cells[row, colData].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Cells[row, colData].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    colData++;
                    colData = 1;
                    row++;
                }
                worksheet.Cells[row, 1, row, 2].Merge = true;
                worksheet.Cells[row, 1, row, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 1, row, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                worksheet.Cells[row, 1, row, 2].Value = L("Total");

                worksheet.Cells[row, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 3].Value = itemDto.NumberItemQuantity;
                worksheet.Cells[row, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                worksheet.Cells[row, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 4].Value = itemDto.BillAmount;
                worksheet.Cells[row, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                worksheet.Cells[row, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 5].Value = itemDto.BillValue.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                worksheet.Cells[row, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                worksheet.Cells[row, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 6].Value = itemDto.DiscountValue.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                worksheet.Cells[row, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                worksheet.Cells[row, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 7].Value = itemDto.DiscountValuePercent.ToString(ConnectConsts.ConnectConsts.NumberFormat) + "%";
                worksheet.Cells[row, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                worksheet.Cells[row, 8, row, 9].Merge = true;
                worksheet.Cells[row, 8, row, 9].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                worksheet.Cells[row, 8, row, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                worksheet.Cells[row, 8, row, 9].Value = "";
                index++;
                row++;
            }
            // Add footer

            worksheet.Cells[row, 1, row, 2].Merge = true;
            worksheet.Cells[row, 1, row, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            worksheet.Cells[row, 1, row, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            worksheet.Cells[row, 1, row, 2].Value = L("Total");

            worksheet.Cells[row, 3].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            worksheet.Cells[row, 3].Value = dtos.TotalQuantity;
            worksheet.Cells[row, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            worksheet.Cells[row, 4].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            worksheet.Cells[row, 4].Value = dtos.TotalBillAmount;
            worksheet.Cells[row, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            worksheet.Cells[row, 5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            worksheet.Cells[row, 5].Value = dtos.TotalBillValue.ToString(ConnectConsts.ConnectConsts.NumberFormat);
            worksheet.Cells[row, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            worksheet.Cells[row, 6].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            worksheet.Cells[row, 6].Value = dtos.TotalDiscountValue.ToString(ConnectConsts.ConnectConsts.NumberFormat);
            worksheet.Cells[row, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            worksheet.Cells[row, 7].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            worksheet.Cells[row, 7].Value = 100.ToString(ConnectConsts.ConnectConsts.NumberFormat) + "%";
            worksheet.Cells[row, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            worksheet.Cells[row, 8, row, 9].Merge = true;
            worksheet.Cells[row, 8, row, 9].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            worksheet.Cells[row, 8, row, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            worksheet.Cells[row, 8, row, 9].Value = "";
            Color colorFooter = System.Drawing.ColorTranslator.FromHtml("#cccccc");
            worksheet.Cells[row, 1, row, headerCol].Style.Font.Bold = true;
            worksheet.Cells[row, 1, row, headerCol].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells[row, 1, row, headerCol].Style.Fill.BackgroundColor.SetColor(colorFooter);
            worksheet.Cells[row, 1, row, headerCol].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            for (var i = 1; i <= 9; i++)
            {
                worksheet.Column(i).AutoFit();
            }
        }
        private async Task<string> ExportSalePromotionByPromotionToHtml(SalesPromotionInput input, ISalesPromotionReportAppService appService)
        {
            input.MaxResultCount = 10000;
            var dtos = await appService.GetPromotionByPromotion(input);
            var businessDate = dtos.StartDate != dtos.EndDate ? L("Date") + " " + dtos.StartDate + " " + L("To") + " " + dtos.EndDate : L("Date") + " " + dtos.StartDate;
            StringBuilder strHTMLBuilder = new StringBuilder();
            strHTMLBuilder.Append("<html>");
            strHTMLBuilder.Append("<head>");
            strHTMLBuilder.Append("</head>");
            var styleTable = GetStyleForTable();
            strHTMLBuilder.Append(styleTable);
            strHTMLBuilder.Append("<body>");
            strHTMLBuilder.Append("<div>");

            // add header
            var titleHeader = L("SalesPromotionReportByPromotion") + "<br>" + businessDate;
            if (!string.IsNullOrEmpty(dtos.LocationCode) && !string.IsNullOrEmpty(dtos.LocationName))
            {
                titleHeader += "<br>"  + dtos.LocationName;
            }
            strHTMLBuilder.Append("<table class='table table-bordered custom_table_date border_bottom_none'>");
            strHTMLBuilder.Append("<tr class='font_14'>");
            //strHTMLBuilder.Append($"<td colspan='9' class='border_bottom_none text-center font_weight_bold'>{L("SalesPromotionReportByPromotion")}</td>");
            strHTMLBuilder.Append($"<td colspan='9' class='border_bottom_none text-center font_weight_bold'>{titleHeader}</td>");
            strHTMLBuilder.Append("</tr>");
            strHTMLBuilder.Append("<tr class=' font_14'>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("PlantCode")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("PlantName")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("Quantity")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("NumberOfBill")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("BillValue")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("DiscountValue")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("DiscountValuePercent")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("AverageDiscountPerItem")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{L("AverageDiscountPerBill")}</th>");
            strHTMLBuilder.Append("</tr>");

            // add content
            foreach (var item in dtos.ListItem)
            {
                strHTMLBuilder.Append("<tr class='font_14 background-aqua'>");
                strHTMLBuilder.Append($"<td class='text-left' colspan='9'> {L("Promotion")}/{ item.PromotionName}</td> ");
                strHTMLBuilder.Append("</tr>");
                foreach (var data in item.ListItem)
                {
                    strHTMLBuilder.Append("<tr class='font_14'>");
                    strHTMLBuilder.Append($"<td class='text-center'> { data.PlantCode}</td> ");
                    strHTMLBuilder.Append($"<td class='text-center'> { data.PlantName}</td> ");
                    strHTMLBuilder.Append($"<td class='text-center'> { data.NumberItemQuantity}</td> ");
                    strHTMLBuilder.Append($"<td class='text-center'> { data.BillAmount}</td> ");
                    strHTMLBuilder.Append($"<td class='text-center'> { data.BillValue.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td> ");
                    strHTMLBuilder.Append($"<td class='text-center'> { data.DiscountValue.ToString(ConnectConsts.ConnectConsts.NumberFormat)} </td> ");
                    strHTMLBuilder.Append($"<td class='text-center'> { data.DiscountValuePercent.ToString(ConnectConsts.ConnectConsts.NumberFormat)}%</td> ");
                    strHTMLBuilder.Append($"<td class='text-center'> { data.AverageDiscountPerItem.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td> ");
                    strHTMLBuilder.Append($"<td class='text-center'> { data.AverageDiscountPerBill.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td> ");
                    strHTMLBuilder.Append("</tr>");
                }
                strHTMLBuilder.Append("<tr class='font_14 background-gray font_weight_bold'>");
                strHTMLBuilder.Append($"<td class='text-right' colspan = '2'> {L("Total")}</td> ");
                strHTMLBuilder.Append($"<td class='text-center'> { item.NumberItemQuantity}</td> ");
                strHTMLBuilder.Append($"<td class='text-center'> { item.BillAmount}</td> ");
                strHTMLBuilder.Append($"<td class='text-center'> { item.BillValue.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td> ");
                strHTMLBuilder.Append($"<td class='text-center'> { item.DiscountValue.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td> ");
                strHTMLBuilder.Append($"<td class='text-center'> { item.DiscountValuePercent.ToString(ConnectConsts.ConnectConsts.NumberFormat)}% </td> ");
                strHTMLBuilder.Append($"<td class='text-center' colspan = '2'> </td> ");
                strHTMLBuilder.Append("</tr>");
            }

            strHTMLBuilder.Append("<tr class='font_14 background-gray font_weight_bold'>");
            strHTMLBuilder.Append($"<td class='text-right' colspan = '2'> {L("Total")}</td> ");
            strHTMLBuilder.Append($"<td class='text-center'> { dtos.TotalQuantity}</td> ");
            strHTMLBuilder.Append($"<td class='text-center'> { dtos.TotalBillAmount}</td> ");
            strHTMLBuilder.Append($"<td class='text-center'> { dtos.TotalBillValue.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td> ");
            strHTMLBuilder.Append($"<td class='text-center'> { dtos.TotalDiscountValue.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td> ");
            strHTMLBuilder.Append($"<td class='text-center'> { 100.ToString(ConnectConsts.ConnectConsts.NumberFormat)} %</td> ");
            strHTMLBuilder.Append($"<td class='text-center' colspan = '2'> </td> ");
            strHTMLBuilder.Append("</tr>");
            strHTMLBuilder.Append("</table>");
            strHTMLBuilder.Append("</div>");
            string Htmltext = strHTMLBuilder.ToString();
            return Htmltext;
        }
        #endregion
        private FileDto ProcessFileHTML(FileDto file, string fileHTML)
        {
            File.WriteAllText(Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken), fileHTML);
            file.FileType = MimeTypeNames.ApplicationXhtmlXml;
            file.FileName = Path.GetFileNameWithoutExtension(file.FileName) + ".html";
            return file;
        }
        private string GetStyleForTable()
        {
            var stype = @"<style> 
             body  { 
              font:400 12px 'Tahoma';
              font-size: 12px;
              padding:10px;
            }
            table  { 
                margin-top: 10px;
                border-collapse: collapse;
                width: 100%;
                margin-bottom: 10px;
            }

            table td, table th {
                border: 1px solid #ddd;
                padding: 8px;
            }
            table th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #364150;
                color: white;
            }
            thead {
                display: table-header-group;
            }
            tfoot {
                display: table-row-group;
            }
            tr{
                page-break-inside: avoid;
            }
            .text-right{
              text-align:right;
            }
            .background-aqua{
              background-color:aqua;
            }
            .background-gray{
              background-color:gray;
            }
			.margin_bottom_0 {
				margin-bottom: 0 !important;
			}
            .margin_top_0 {
				margin-top: 0 !important;
			}

			.border_bottom_none {
				border-bottom: none !important;
			}

			.custom_table_date {
				margin-bottom: 0px !important;
			}

			.display_flex {
				display: flex;
			}
			.w-100 {
				width: 100%;
			}
	
			.float-right {
				float: right;
			}

			.font_weight_bold {
				font-weight: bold;
			}
            .font_14{
				font-size: 14;
             }
            .text-center{
               text-align: center;          
            }
            .text-right{
               text-align: right;          
            }
            .w-18 {
                width: 18%;
            }

            .w-20 {
                width: 20%;
            }

            .w-5 {
                width: 5%;
            }

            .w-15 {
                width: 15%;
            }

            .w-25 {
                width: 25%;
            }

            .w-30 {
                width: 30%;
            }

            .w-35 {
                width: 35%;
            }

            .w-50 {
                width: 50%;
            }

            .w-85 {
                width: 85%;
            }


            .w-50 {
                width: 100%;
            }
            .margin-0{
               margin: 0;
            }
            table tr:hover {background-color: #ddd;}
            </style>";
            return stype;
        }
    }
}
