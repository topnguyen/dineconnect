﻿using DinePlan.DineConnect.Connect.SalesPromotionReport.Dto;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.SalesPromotionReport.Exporting
{
    public interface ISalesPromotionReportExporter
    {
        Task<FileDto> ExportSalePromotionByPlantReport(SalesPromotionInput input, ISalesPromotionReportAppService appService);

        Task<FileDto> ExportSalePromotionByPromotionReport(SalesPromotionInput input, ISalesPromotionReportAppService appService);
    }
}
