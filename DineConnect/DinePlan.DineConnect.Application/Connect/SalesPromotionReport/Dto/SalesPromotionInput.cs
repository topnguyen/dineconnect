﻿using Abp.Application.Services.Dto;
using Abp.Extensions;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Discount.Dtos;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter;
using OpenHtmlToPdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.SalesPromotionReport.Dto
{
    public class SalesPromotionInput : MaxPagedAndSortedInputDto, IShouldNormalize, IDateInput, IFileExport, IBackgroundExport
    {
        public SalesPromotionInput()
        {
            Locations = new List<SimpleLocationDto>();
            LocationGroup = new LocationGroupDto();
            ListMaterial = new List<int>();
        }
        public List<int> MenuItemIds { get; set; }
        public string Days { get; set; }
        public string View { get; set; }
        public int ViewNumber { get; set; }
        public string Group { get; set; }
        public string TypeOfChart { get; set; }
        public bool IsAddOns { get; set; }
        public string DepartmentSelect { get; set; }
        public bool Combo { get; set; }
        public List<int> Payments { get; set; }
        public List<ComboboxItemDto> Departments { get; set; }
        public string Department { get; set; }
        public string TerminalName { get; set; }
        public string LastModifiedUserName { get; set; }
        public string OutputType { get; set; }
        public string ExportType { get; set; }
        public bool ByLocation { get; set; }
        public string Duration { get; set; }
        public int TenantId { get; set; }
        public string ExportOutput { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Location { get; set; }
        public long UserId { get; set; }
        public List<SimpleLocationDto> Locations { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public bool NotCorrectDate { get; set; }
        public bool Credit { get; set; }
        public bool Refund { get; set; }
        public string TicketNo { get; set; }
        public ExportType ExportOutputType { get; set; }
        public PaperSize PaperSize { get; set; }
        public bool Portrait { get; set; }

        public void Normalize()
        {
            if (Sorting.IsNullOrWhiteSpace())
            {
                Sorting = "PlantCode";
            }
        }
        public int StartHour { get; set; }
        public int EndHour { get; set; }
        public int StartMinute { get; set; }
        public int EndMinute { get; set; }
        public double Start_Number => Convert.ToDouble(StartHour + StartMinute / 60);
        public double End_Number => Convert.ToDouble(EndHour + EndMinute / 60);
        public string Start_String => StartHour + ":" + StartMinute;
        public string End_String => EndHour + ":" + EndMinute;


        public bool IsCombo { get; set; }
        public bool RunInBackground { get; set; }
        public string ReportDescription { get; set; }
        public FileDto FileDto { get; set; }
        public bool ReportDisplay { get; set; }
        public List<int> ListMaterial { get; set; }
        public string DynamicFilter { get; set; }
        public string DateFormat { get; set; }
        public string DatetimeFormat { get; set; }


    }
}
