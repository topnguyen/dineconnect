﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.SalesPromotionReport.Dto
{
    public class SalesByPromotionList
    {
        public string LocationCode { get; set; }
        public string LocationName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public List<SalesByPromotionListGroupByPromo> ListItem = new List<SalesByPromotionListGroupByPromo>();
        public decimal TotalQuantity => ListItem.Sum(s => s.NumberItemQuantity);
        public decimal TotalBillAmount => ListItem.Sum(s => s.BillAmount);
        public decimal TotalDiscountValue => ListItem.Sum(s => s.DiscountValue);
        public decimal TotalBillValue => ListItem.Sum(s => s.BillValue);
    }
    public class SalesByPromotionListGroupByPromo
    {
        public int PromotionId { get; set; }
        public string PromotionName { get; set; }
        public List<SalesByPromotionListGroupByPlant> ListItem = new List<SalesByPromotionListGroupByPlant>();
        public decimal NumberItemQuantity => ListItem.Sum(s => s.NumberItemQuantity);
        public decimal BillAmount => ListItem.Sum(s => s.BillAmount);
        public decimal BillValue => ListItem.Sum(s => s.BillValue);
        public decimal DiscountValue => ListItem.Sum(s => s.DiscountValue);
        public decimal TotalDiscountValue { get; set; }

        public decimal DiscountValuePercent
        {
            get
            {
                var result = 0M;
                if (TotalDiscountValue != 0M)
                {
                    result = (DiscountValue / TotalDiscountValue) * 100;
                }
                return result;
            }
        }
    }
    public class SalesByPromotionListGroupByPlant
    {
        public string PlantName { get; set; }
        public string PromotionName { get; set; }
        public string PlantCode { get; set; }
        public List<SalesByPromotionItems> ListItem = new List<SalesByPromotionItems>();
        public decimal NumberItemQuantity => ListItem.Sum(s => s.NumberItemQuantity);
        public decimal BillAmount => ListItem.Sum(s => s.BillAmount);
        public decimal BillValue => ListItem.Sum(s => s.BillValue);
        public decimal DiscountValue => ListItem.Sum(s => s.DiscountValue);
        public decimal TotalDiscountValue { get; set; }

        public decimal DiscountValuePercent
        {
            get
            {
                var result = 0M;
                if (TotalDiscountValue != 0M)
                {
                    result = (DiscountValue / TotalDiscountValue) * 100;
                }
                return result;
            }
        }
        public decimal AverageDiscountPerItem
        {
            get
            {
                var result = 0M;
                if (NumberItemQuantity != 0M)
                {
                    result = (DiscountValue / NumberItemQuantity);
                }
                return result;
            }
        }
        public decimal AverageDiscountPerBill
        {
            get
            {
                var result = 0M;
                if (BillAmount != 0M)
                {
                    result = (DiscountValue / BillAmount);
                }
                return result;
            }
        }
    }
    public class SalesByPromotionItems
    {  
        public int TicketId { get; set; }
        public string LocationCode { get; set; }
        public string LocationName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int PromotionId { get; set; }
        public string PromotionName { get; set; }
        public string PlantCode { get; set; }
        public string PlantName { get; set; }
        public decimal NumberItemQuantity { get; set; }
        public decimal BillAmount { get; set; }
        public decimal BillValue { get; set; }
        public decimal DiscountValue { get; set; }
        public decimal DiscountValuePercent { get; set; }
        public decimal AverageDiscountPerItem { get; set; }
        public decimal AverageDiscountPerBill { get; set; }
    }



    public class SalesByPlantListTotal
    {
        public string LocationCode { get; set; }
        public string LocationName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public List<SalesByPlantList> ListItem = new List<SalesByPlantList>();
        public decimal Quantity => ListItem.Sum(s => s.Quantity);
        public decimal BillAmount => ListItem.Sum(s => s.BillAmount);
        public decimal DiscountValue => ListItem.Sum(s => s.DiscountValue);
        public decimal DiscountValuePercent => ListItem.Count > 0 ? ListItem.Average(s => s.DiscountValuePercent) : 0;
    }
    public class SalesByPlantList
    {
        public string PlantCode { get; set; }
        public string PlantName { get; set; }
        public List<SalesByPlantItems> ListItem = new List<SalesByPlantItems>();
        public decimal Quantity => ListItem.Sum(s => s.Quantity);
        public decimal BillAmount => ListItem.Sum(s => s.BillAmount);
        public decimal DiscountValue => ListItem.Sum(s => s.DiscountValue);
        public decimal DiscountValuePercent => ListItem.Sum(s => s.DiscountValuePercent);

    }
    public class SalesByPlantItems
    {
        public int TicketId { get; set; }
        public string PlantCode { get; set; }
        public string PlantName { get; set; }
        public decimal BillValue { get; set; }
        public DateTime Date { get; set; }
        public string DateString { get; set; }
        public int PromotionId{ get; set; }
        public string PromotionName { get; set; }
        public int PromotionType{ get; set; }
        public string PromotionCategory { get; set; }
        public string ConditionType { get; set; }
        public decimal Quantity { get; set; }
        public decimal BillAmount { get; set; }
        public decimal DiscountValue { get; set; }
        public decimal TotalDiscountValue { get; set; }
        public decimal DiscountValuePercent {
             get
            {
                var result = 0M;
                if (TotalDiscountValue != 0M)
                {
                    result = (DiscountValue / TotalDiscountValue) * 100;
                }
                return result;
            }
        }
    }

}
