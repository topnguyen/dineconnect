﻿using Abp.Application.Services;
using DinePlan.DineConnect.Connect.SalesPromotionReport.Dto;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.SalesPromotionReport
{
    public interface ISalesPromotionReportAppService : IApplicationService
    {
        Task<SalesByPlantListTotal> GetPromotionByPlant(SalesPromotionInput input);
        Task<SalesByPromotionList> GetPromotionByPromotion(SalesPromotionInput input);
        Task<FileDto> GetPromotionByPromotionToExport(SalesPromotionInput input);
        Task<FileDto> GetPromotionByPlantToExport(SalesPromotionInput input);
    }
}
