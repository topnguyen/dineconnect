﻿using DinePlan.DineConnect.Connect.Custom.Dto;
using DinePlan.DineConnect.Connect.Customize.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Custom.Exporting
{
	public class CustomizeExporter : FileExporterBase, ICustomizeExporter
	{

		#region salestaxreport
		public async Task<FileDto> ExportSaleTaxToFile(GetSaleTaxReportOutput dtos, GetTaxReportInputDto input)
		{
			var builder = new StringBuilder();
			builder.Append(L("SaleTaxExportReportList"));
			var file = new FileDto(builder + ".xlsx",
				MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
			using (var excelPackage = new ExcelPackage())
			{
				GetSalesTaxReport(excelPackage, dtos);


				Save(excelPackage, file);
			}
			return ProcessFile(input, file);
		}
		private void GetSalesTaxReport(ExcelPackage package, GetSaleTaxReportOutput dtos)
		{
			var worksheet = package.Workbook.Worksheets.Add(L("SaleTaxExportReportList"));
			worksheet.OutLineApplyStyle = true;

			AddHeader(
				worksheet,

				L("ABB.NO"),
				L("Description"),
				L("ValueBeforeVAT"),
				L("Vat"),
				L("Amount"),
				L("ReferenceReceiptNumber")
				);

			//set color location
			Color colorHeader = System.Drawing.ColorTranslator.FromHtml("#abd0bc");
			worksheet.Cells[$"A{1}:F{1}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
			worksheet.Cells[$"A{1}:F{1}"].Style.Fill.BackgroundColor.SetColor(colorHeader);

			//Add  report data
			var row = 2;
			Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#d0cece");
			//location
			foreach (var dataByLocation in dtos.ListDataByLocation)
			{
				worksheet.Cells[$"B{row}:F{row}"].Merge = true;
				worksheet.Cells[row, 1].Value = L("Location");
				worksheet.Cells[row, 2].Value = dataByLocation.Location;

				//set color location
				Color colorLocation = System.Drawing.ColorTranslator.FromHtml("#abd0bc");
				worksheet.Cells[$"A{row}:F{row}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
				worksheet.Cells[$"A{row}:F{row}"].Style.Fill.BackgroundColor.SetColor(colorLocation);

				row++;
				foreach (var dataByDate in dataByLocation.ListDataByDate)
				{
					worksheet.Cells[$"B{row}:F{row}"].Merge = true;
					worksheet.Cells[row, 1].Value = L("Date");
					//set color date
					Color colorDate = System.Drawing.ColorTranslator.FromHtml("#abd0bc");
					worksheet.Cells[$"A{row}:F{row}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
					worksheet.Cells[$"A{row}:F{row}"].Style.Fill.BackgroundColor.SetColor(colorDate);

					worksheet.Cells[row, 2].Value = dataByDate.Date;
					row++;

					foreach (var dataByPOS in dataByDate.ListDataByPOS)
					{
						worksheet.Cells[$"B{row}:F{row}"].Merge = true;
						worksheet.Cells[row, 1].Value = L("Terminal");
						worksheet.Cells[row, 2].Value = dataByPOS.POSName;

						//set color PosName
						Color colorPosName = System.Drawing.ColorTranslator.FromHtml("#f0f0f0");
						worksheet.Cells[$"A{row}:F{row}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
						worksheet.Cells[$"A{row}:F{row}"].Style.Fill.BackgroundColor.SetColor(colorPosName);
						row++;

						foreach (var item in dataByPOS.ListItem)
						{
							worksheet.Cells[row, 1].Value = item.ABBNO;
							worksheet.Cells[row, 2].Value = item.Description;
							worksheet.Cells[row, 3].Value = item.ValueBeforeVAT;
							worksheet.Cells[row, 4].Value = item.Vat;
							worksheet.Cells[row, 5].Value = item.Amount;
							worksheet.Cells[row, 6].Value = item.ReferenceReceiptNumber;
							row++;
						}

						worksheet.Cells[row, 2].Value = L("Total");
						worksheet.Cells[row, 3].Value = dataByPOS.TotalForPOSValueBeforeVAT;
						worksheet.Cells[row, 4].Value = dataByPOS.TotalForPOSVat;
						worksheet.Cells[row, 5].Value = dataByPOS.TotalForPOSAmount;

						//set color totalPos
						Color colorTotalPos = System.Drawing.ColorTranslator.FromHtml("#f0f0f0");
						worksheet.Cells[$"A{row}:F{row}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
						worksheet.Cells[$"A{row}:F{row}"].Style.Fill.BackgroundColor.SetColor(colorTotalPos);
						row++;
					}
					worksheet.Cells[row, 2].Value = L("TotalForDate") +" " + dataByDate.Date;
					worksheet.Cells[row, 3].Value = dataByDate.TotalForDateValueBeforeVAT;
					worksheet.Cells[row, 4].Value = dataByDate.TotalForDateVat;
					worksheet.Cells[row, 5].Value = dataByDate.TotalForDateAmount;
					//set color totalPos
					Color colorTotalForDate = System.Drawing.ColorTranslator.FromHtml("#f0f0f0");
					worksheet.Cells[$"A{row}:F{row}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
					worksheet.Cells[$"A{row}:F{row}"].Style.Fill.BackgroundColor.SetColor(colorTotalForDate);

					row++;
				}
				worksheet.Cells[row, 2].Value = L("TotalForLocation");
				worksheet.Cells[row, 3].Value = dataByLocation.TotalForLocationValueBeforeVAT;
				worksheet.Cells[row, 4].Value = dataByLocation.TotalForLocationVat;
				worksheet.Cells[row, 5].Value = dataByLocation.TotalForLocationAmount;

				//set color total all
				Color colorTotalAll = System.Drawing.ColorTranslator.FromHtml("#f0f0f0");
				worksheet.Cells[$"A{row}:F{row}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
				worksheet.Cells[$"A{row}:F{row}"].Style.Fill.BackgroundColor.SetColor(colorTotalAll);
				row++;
			}



			worksheet.Cells[row, 2].Value = "Total";
			worksheet.Cells[row, 3].Value = dtos.TotalValueBeforeVAT;
			worksheet.Cells[row, 4].Value = dtos.TotalVat;
			worksheet.Cells[row, 5].Value = dtos.TotalAmount;
			for (var i = 1; i <= 6; i++)
			{
				worksheet.Column(i).AutoFit();
			}

			//set color footer
			Color colFromHexFooter = System.Drawing.ColorTranslator.FromHtml("#548235");
			worksheet.Cells[$"A{row}:F{row}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
			worksheet.Cells[$"A{row}:F{row}"].Style.Fill.BackgroundColor.SetColor(colFromHexFooter);
		}
		#endregion


		#region nonsalestaxreport
		public async Task<FileDto> ExportNonSalesTaxReportToFile(GetNonSalesTaxReportOutputDto dtos, GetNonSalesTaxReportInput input)
		{
			var builder = new StringBuilder();
			builder.Append(L("NonSalesTaxReport"));
			var file = new FileDto(builder + ".xlsx",
				MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
			using (var excelPackage = new ExcelPackage())
			{
				GetNonSalesTaxReport(excelPackage, dtos);


				Save(excelPackage, file);
			}
			return await Task.FromResult(ProcessFile(input, file));
		}
		private void GetNonSalesTaxReport(ExcelPackage package, GetNonSalesTaxReportOutputDto dtos)
		{
			var worksheet = package.Workbook.Worksheets.Add(L("NonSaleTaxReportList"));
			worksheet.OutLineApplyStyle = true;

			AddHeader(
				worksheet,

				L("ABB.NO"),
				L("Description"),
				L("ValueBeforeVAT"),
				L("Vat"),
				L("Amount"),
				L("ReferenceReceiptNumber")
				);

			//set color location
			Color colorHeader = System.Drawing.ColorTranslator.FromHtml("#808080");
			worksheet.Cells[$"A{1}:F{1}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
			worksheet.Cells[$"A{1}:F{1}"].Style.Fill.BackgroundColor.SetColor(colorHeader);
			worksheet.Cells[$"A{1}:F{1}"].Style.Font.Color.SetColor(Color.White);

			//Add  report data
			var row = 2;
			foreach (var dataByDate in dtos.ListDataByDate)
			{
				worksheet.Cells[$"B{row}:F{row}"].Merge = true;
				worksheet.Cells[row, 1].Value = L("Date");
				//set color date
				Color colorDate = System.Drawing.ColorTranslator.FromHtml("#808080");
				worksheet.Cells[$"A{row}:F{row}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
				worksheet.Cells[$"A{row}:F{row}"].Style.Fill.BackgroundColor.SetColor(colorDate);

				worksheet.Cells[row, 2].Value = dataByDate.Date;
				worksheet.Cells[$"A{row}:F{row}"].Style.Font.Color.SetColor(Color.White);
				row++;

				foreach (var dataByPOS in dataByDate.ListDataByPOS)
				{
					worksheet.Cells[$"B{row}:F{row}"].Merge = true;
					worksheet.Cells[row, 1].Value = L("Terminal");
					worksheet.Cells[row, 2].Value = dataByPOS.POSName;

					//set color PosName
					Color colorPosName = System.Drawing.ColorTranslator.FromHtml("#f0f0f0");
					worksheet.Cells[$"A{row}:F{row}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
					worksheet.Cells[$"A{row}:F{row}"].Style.Fill.BackgroundColor.SetColor(colorPosName);
					row++;

					foreach (var item in dataByPOS.ListItem)
					{
						worksheet.Cells[row, 1].Value = item.ABBNO;
						worksheet.Cells[row, 2].Value = item.Description;
						worksheet.Cells[row, 3].Value = item.ValueBeforeVAT;
						worksheet.Cells[row, 4].Value = item.Vat;
						worksheet.Cells[row, 5].Value = item.Amount;
						worksheet.Cells[row, 6].Value = item.ReferenceReceiptNumber;
						row++;
					}

					worksheet.Cells[row, 2].Value = L("Total") +"("+dataByPOS.POSName +")";
					worksheet.Cells[row, 3].Value = dataByPOS.TotalForPOSValueBeforeVAT;
					worksheet.Cells[row, 4].Value = dataByPOS.TotalForPOSVat;
					worksheet.Cells[row, 5].Value = dataByPOS.TotalForPOSAmount;

					//set color totalPos
					Color colorTotalPos = System.Drawing.ColorTranslator.FromHtml("#808080");
					worksheet.Cells[$"A{row}:F{row}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
					worksheet.Cells[$"A{row}:F{row}"].Style.Fill.BackgroundColor.SetColor(colorTotalPos);
					worksheet.Cells[$"A{row}:F{row}"].Style.Font.Color.SetColor(Color.White);
					row++;
				}
				worksheet.Cells[row, 2].Value = L("TotalForDate") + " " + dataByDate.Date;
				worksheet.Cells[row, 3].Value = dataByDate.TotalForDateValueBeforeVAT;
				worksheet.Cells[row, 4].Value = dataByDate.TotalForDateVat;
				worksheet.Cells[row, 5].Value = dataByDate.TotalForDateAmount;
				//set color totalPos
				Color colorTotalForDate = System.Drawing.ColorTranslator.FromHtml("#808080");
				worksheet.Cells[$"A{row}:F{row}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
				worksheet.Cells[$"A{row}:F{row}"].Style.Fill.BackgroundColor.SetColor(colorTotalForDate);
				worksheet.Cells[$"A{row}:F{row}"].Style.Font.Color.SetColor(Color.White);
				row++;
			}

			worksheet.Cells[row, 2].Value = "Total";
			worksheet.Cells[row, 3].Value = dtos.TotalValueBeforeVAT;
			worksheet.Cells[row, 4].Value = dtos.TotalVat;
			worksheet.Cells[row, 5].Value = dtos.TotalAmount;
			for (var i = 1; i <= 6; i++)
			{
				worksheet.Column(i).AutoFit();
			}

			//set color footer
			Color colFromHexFooter = System.Drawing.ColorTranslator.FromHtml("#808080");
			worksheet.Cells[$"A{row}:F{row}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
			worksheet.Cells[$"A{row}:F{row}"].Style.Fill.BackgroundColor.SetColor(colFromHexFooter);
			worksheet.Cells[$"A{row}:F{row}"].Style.Font.Color.SetColor(Color.White);
		}
		#endregion


		#region dailysalestaxreport
		public async Task<FileDto> ExportDailySalesTaxReportToFile(List<GetDailySalesTaxReportExcelListDto> dtos, GetTaxReportInputDto input)
		{
			var builder = new StringBuilder();
			builder.Append(L("DailySalesTaxReport"));
			var file = new FileDto(builder + ".xlsx",
				MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
			using (var excelPackage = new ExcelPackage())
			{
				GetDailySalesTaxReport(excelPackage, dtos);
				Save(excelPackage, file);
			}
			return ProcessFile(input, file);
		}
		private void GetDailySalesTaxReport(ExcelPackage package, List<GetDailySalesTaxReportExcelListDto> dtos)
		{
			var worksheet = package.Workbook.Worksheets.Add(L("DailySalesTaxReport"));
			worksheet.OutLineApplyStyle = true;

			AddHeader(
				worksheet,
				L("Date"),
				L("POS"),
				L("TicketNumber"),
				L("ValueBeforeVAT"),
				L("Vat"),
				L("Amount"),
				L("ReferentReceiptNumber")
				);

			//Add  report data
			var row = 2;
			decimal totalValueBeforeVAT = 0;
			decimal totalVat = 0;
			decimal totalAmount = 0;

			foreach (var data in dtos)
			{
				foreach (var item in data.ListItem)
				{
					worksheet.Cells[row, 1].Value = item.Date;
					worksheet.Cells[row, 2].Value = item.POS;
					worksheet.Cells[row, 3].Value = item.TicketNumber;
					worksheet.Cells[row, 4].Value = item.ValueBeforeVAT;
					worksheet.Cells[row, 5].Value = item.Vat;
					worksheet.Cells[row, 6].Value = item.Amount;
					worksheet.Cells[row, 7].Value = item.ReferenceReceiptNumber;
					row++;
				}
				decimal sumValueBeforeVAT = data.ListItem.Sum(x => x.ValueBeforeVAT);
				decimal sumVat = data.ListItem.Sum(x => x.Vat);
				decimal sumAmount = data.ListItem.Sum(x => x.Amount);
				worksheet.Cells[row, 3].Value = data.Date;
				worksheet.Cells[row, 4].Value = sumValueBeforeVAT;
				worksheet.Cells[row, 5].Value = sumVat;
				worksheet.Cells[row, 6].Value = sumAmount;

				//set color
				Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#d0cece");
				worksheet.Cells[$"A{row}:G{row}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
				worksheet.Cells[$"A{row}:G{row}"].Style.Fill.BackgroundColor.SetColor(colFromHex);

				totalValueBeforeVAT = totalValueBeforeVAT + sumValueBeforeVAT;
				totalVat = totalVat + sumVat;
				totalAmount = totalAmount + sumAmount;
				row++;
			}

			worksheet.Cells[row, 3].Value = "Total";
			worksheet.Cells[row, 4].Value = totalValueBeforeVAT;
			worksheet.Cells[row, 5].Value = totalVat;
			worksheet.Cells[row, 6].Value = totalAmount;

			//set color footer
			Color colFromHexFooter = System.Drawing.ColorTranslator.FromHtml("#548235");
			worksheet.Cells[$"A{row}:G{row}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
			worksheet.Cells[$"A{row}:G{row}"].Style.Fill.BackgroundColor.SetColor(colFromHexFooter);

			for (var i = 1; i <= 7; i++)
			{
				worksheet.Column(i).AutoFit();
			}

		}
		#endregion


		#region monthly sales tax report
		public async Task<FileDto> ExportMonthlySalesTaxReportToFile(List<GetMonthlySalesTaxReportExcelListDto> dtos, GetTaxReportInputDto input)
		{
			var builder = new StringBuilder();
			builder.Append(L("MonthlySalesTaxReport"));
			var file = new FileDto(builder + ".xlsx",
				MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
			using (var excelPackage = new ExcelPackage())
			{
				GetMonthlySalesTaxReport(excelPackage, dtos);
				Save(excelPackage, file);
			}
			return ProcessFile(input, file);
		}
		private void GetMonthlySalesTaxReport(ExcelPackage package, List<GetMonthlySalesTaxReportExcelListDto> dtos)
		{
			var worksheet = package.Workbook.Worksheets.Add(L("MonthlySalesTaxReport"));
			worksheet.OutLineApplyStyle = true;

			AddHeader(
				worksheet,
				L("Date"),
				L("POS"),
				L("TicketNumber"),
				L("ValueBeforeVAT"),
				L("Vat"),
				L("Amount"),
				L("ReferentReceiptNumber")
				);

			//Add  report data
			var row = 2;
			decimal totalValueBeforeVAT = 0;
			decimal totalVat = 0;
			decimal totalAmount = 0;

			foreach (var data in dtos)
			{
				foreach (var item in data.ListItem)
				{
					worksheet.Cells[row, 1].Value = item.Date;
					worksheet.Cells[row, 2].Value = item.POS;
					worksheet.Cells[row, 3].Value = item.TicketNumber;
					worksheet.Cells[row, 4].Value = item.ValueBeforeVAT;
					worksheet.Cells[row, 5].Value = item.Vat;
					worksheet.Cells[row, 6].Value = item.Amount;
					worksheet.Cells[row, 7].Value = item.ReferenceReceiptNumber;
					row++;
				}
				decimal sumValueBeforeVAT = data.ListItem.Sum(x => x.ValueBeforeVAT);
				decimal sumVat = data.ListItem.Sum(x => x.Vat);
				decimal sumAmount = data.ListItem.Sum(x => x.Amount);
				worksheet.Cells[row, 3].Value = data.Month;
				worksheet.Cells[row, 4].Value = sumValueBeforeVAT;
				worksheet.Cells[row, 5].Value = sumVat;
				worksheet.Cells[row, 6].Value = sumAmount;

				//set color
				Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#d0cece");
				worksheet.Cells[$"A{row}:G{row}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
				worksheet.Cells[$"A{row}:G{row}"].Style.Fill.BackgroundColor.SetColor(colFromHex);

				totalValueBeforeVAT = totalValueBeforeVAT + sumValueBeforeVAT;
				totalVat = totalVat + sumVat;
				totalAmount = totalAmount + sumAmount;
				row++;
			}

			worksheet.Cells[row, 3].Value = "Total";
			worksheet.Cells[row, 4].Value = totalValueBeforeVAT;
			worksheet.Cells[row, 5].Value = totalVat;
			worksheet.Cells[row, 6].Value = totalAmount;

			//set color footer
			Color colFromHexFooter = System.Drawing.ColorTranslator.FromHtml("#548235");
			worksheet.Cells[$"A{row}:G{row}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
			worksheet.Cells[$"A{row}:G{row}"].Style.Fill.BackgroundColor.SetColor(colFromHexFooter);

			for (var i = 1; i <= 7; i++)
			{
				worksheet.Column(i).AutoFit();
			}

		}
		#endregion

		#region dailyNonsalestaxreport
		public async Task<FileDto> ExportDailyNonSalesTaxReportToFile(List<GetDailyNonSalesTaxReportExcelListDto> dtos, GetTaxReportInputDto input)
		{
			var builder = new StringBuilder();
			builder.Append(L("DailyNonSalesTaxReport"));
			var file = new FileDto(builder + ".xlsx",
				MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
			using (var excelPackage = new ExcelPackage())
			{
				GetDailyNonSalesTaxReport(excelPackage, dtos);
				Save(excelPackage, file);
			}
			return ProcessFile(input, file);
		}
		private void GetDailyNonSalesTaxReport(ExcelPackage package, List<GetDailyNonSalesTaxReportExcelListDto> dtos)
		{
			var worksheet = package.Workbook.Worksheets.Add(L("DailyNonSalesTaxReport"));
			worksheet.OutLineApplyStyle = true;

			AddHeader(
				worksheet,
				L("Date"),
				L("POS"),
				L("ABB.NO"),
				L("Discription"),
				L("ValueBeforeVAT"),
				L("Vat"),
				L("Amount"),
				L("ReferentReceiptNumber")
				);

			//Add  report data
			var row = 2;
			decimal totalValueBeforeVAT = 0;
			decimal totalVat = 0;
			decimal totalAmount = 0;

			foreach (var data in dtos)
			{
				worksheet.Cells[$"A{row}:H{row}"].Merge = true;
				worksheet.Cells[row, 1].Value = data.PlantCode;
				row++;
				foreach (var item in data.ListItem)
				{
					worksheet.Cells[row, 1].Value = item.Date;
					worksheet.Cells[row, 2].Value = item.POS;
					worksheet.Cells[row, 3].Value = item.TicketNumber;
					worksheet.Cells[row, 4].Value = item.Discription;
					worksheet.Cells[row, 5].Value = item.ValueBeforeVAT;
					worksheet.Cells[row, 6].Value = item.Vat;
					worksheet.Cells[row, 7].Value = item.Amount;
					worksheet.Cells[row, 8].Value = item.ReferenceReceiptNumber;
					row++;
				}
				decimal sumValueBeforeVAT = data.ListItem.Sum(x => x.ValueBeforeVAT);
				decimal sumVat = data.ListItem.Sum(x => x.Vat);
				decimal sumAmount = data.ListItem.Sum(x => x.Amount);
				worksheet.Cells[row, 1].Value = data.PlantCode;
				worksheet.Cells[row, 5].Value = sumValueBeforeVAT;
				worksheet.Cells[row, 6].Value = sumVat;
				worksheet.Cells[row, 7].Value = sumAmount;

				//set color
				Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#d0cece");
				worksheet.Cells[$"A{row}:H{row}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
				worksheet.Cells[$"A{row}:H{row}"].Style.Fill.BackgroundColor.SetColor(colFromHex);

				totalValueBeforeVAT = totalValueBeforeVAT + sumValueBeforeVAT;
				totalVat = totalVat + sumVat;
				totalAmount = totalAmount + sumAmount;
				row++;
			}

			worksheet.Cells[row, 1].Value = "Total";
			worksheet.Cells[row, 5].Value = totalValueBeforeVAT;
			worksheet.Cells[row, 6].Value = totalVat;
			worksheet.Cells[row, 7].Value = totalAmount;

			//set color footer
			Color colFromHexFooter = System.Drawing.ColorTranslator.FromHtml("#548235");
			worksheet.Cells[$"A{row}:H{row}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
			worksheet.Cells[$"A{row}:H{row}"].Style.Fill.BackgroundColor.SetColor(colFromHexFooter);

			for (var i = 1; i <= 8; i++)
			{
				worksheet.Column(i).AutoFit();
			}

		}
		#endregion

		#region monthyNonsalestaxreport
		public async Task<FileDto> ExportMonthlyNonSalesTaxReportToFile(List<GetMonthlyNonSalesTaxReportExcelListDto> dtos, GetTaxReportInputDto input)
		{
			var builder = new StringBuilder();
			builder.Append(L("MonthlyNonSalesTaxReport"));
			var file = new FileDto(builder + ".xlsx",
				MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
			using (var excelPackage = new ExcelPackage())
			{
				GetMonthlyNonSalesTaxReport(excelPackage, dtos);
				Save(excelPackage, file);
			}
			return await Task.FromResult(ProcessFile(input, file));
		}
		private void GetMonthlyNonSalesTaxReport(ExcelPackage package, List<GetMonthlyNonSalesTaxReportExcelListDto> dtos)
		{
			var worksheet = package.Workbook.Worksheets.Add(L("MonthlyNonSalesTaxReport"));
			worksheet.OutLineApplyStyle = true;

			AddHeader(
				worksheet,
				//L("Month"),
				L("PlantCode"),
				L("ValueBeforeVAT"),
				L("Vat"),
				L("Amount")
				);

			//Add  report data
			var row = 2;
			decimal totalValueBeforeVAT = 0;
			decimal totalVat = 0;
			decimal totalAmount = 0;

			foreach (var data in dtos)
			{
				worksheet.Cells[$"A{row}:H{row}"].Merge = true;
				worksheet.Cells[row, 1].Value = data.Month;
				row++;
				foreach (var item in data.ListItem)
				{
					worksheet.Cells[row, 1].Value = item.PlantCode;
					worksheet.Cells[row, 2].Value = item.ValueBeforeVAT;
					worksheet.Cells[row, 3].Value = item.Vat;
					worksheet.Cells[row, 4].Value = item.Amount;
					row++;
				}
				decimal sumValueBeforeVAT = data.ListItem.Sum(x => x.ValueBeforeVAT);
				decimal sumVat = data.ListItem.Sum(x => x.Vat);
				decimal sumAmount = data.ListItem.Sum(x => x.Amount);
				worksheet.Cells[row, 1].Value = data.Month;
				worksheet.Cells[row, 2].Value = sumValueBeforeVAT;
				worksheet.Cells[row, 3].Value = sumVat;
				worksheet.Cells[row, 4].Value = sumAmount;

				//set color
				Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#d0cece");
				worksheet.Cells[$"A{row}:D{row}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
				worksheet.Cells[$"A{row}:D{row}"].Style.Fill.BackgroundColor.SetColor(colFromHex);

				totalValueBeforeVAT = totalValueBeforeVAT + sumValueBeforeVAT;
				totalVat = totalVat + sumVat;
				totalAmount = totalAmount + sumAmount;
				row++;
			}

			worksheet.Cells[row, 1].Value = "Total";
			worksheet.Cells[row, 2].Value = totalValueBeforeVAT;
			worksheet.Cells[row, 3].Value = totalVat;
			worksheet.Cells[row, 4].Value = totalAmount;

			//set color footer
			Color colFromHexFooter = System.Drawing.ColorTranslator.FromHtml("#548235");
			worksheet.Cells[$"A{row}:D{row}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
			worksheet.Cells[$"A{row}:D{row}"].Style.Fill.BackgroundColor.SetColor(colFromHexFooter);

			for (var i = 1; i <= 4; i++)
			{
				worksheet.Column(i).AutoFit();
			}

		}
		#endregion
	}
}