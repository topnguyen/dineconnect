﻿using DinePlan.DineConnect.Connect.Custom.Dto;
using DinePlan.DineConnect.Connect.Customize.Dto;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Custom.Exporting
{
    public interface ICustomizeExporter
    {
        Task<FileDto> ExportSaleTaxToFile(GetSaleTaxReportOutput dtos, GetTaxReportInputDto input);
        Task<FileDto> ExportNonSalesTaxReportToFile(GetNonSalesTaxReportOutputDto dtos, GetNonSalesTaxReportInput input);
        Task<FileDto> ExportDailySalesTaxReportToFile(List<GetDailySalesTaxReportExcelListDto> dtos, GetTaxReportInputDto input);
        Task<FileDto> ExportMonthlySalesTaxReportToFile(List<GetMonthlySalesTaxReportExcelListDto> dtos, GetTaxReportInputDto input);
        Task<FileDto> ExportDailyNonSalesTaxReportToFile(List<GetDailyNonSalesTaxReportExcelListDto> dtos, GetTaxReportInputDto input);
        Task<FileDto> ExportMonthlyNonSalesTaxReportToFile(List<GetMonthlyNonSalesTaxReportExcelListDto> dtos, GetTaxReportInputDto input);

    }
}