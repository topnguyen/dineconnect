﻿using Abp.Application.Services.Dto;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Domain.Uow;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Auditing.Exporting;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Audit;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Connect.Tag;
using DinePlan.DineConnect.Connect.Ticket.CategoryReport.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Connect.WorkPeriod.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.Handler.Mapping;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.House.Transaction.Implementation;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Report;
using DinePlan.Infrastructure.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System;
using Abp.Configuration;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Customize.Dto;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using DinePlan.DineConnect.Connect.Custom.Exporting;
using System.Globalization;

namespace DinePlan.DineConnect.Connect.Customize
{
	public class CustomizeAppService : DineConnectAppServiceBase, ICustomizeAppService
	{
		private readonly IRepository<Company> _companyRe;
		private readonly IRepository<Master.Department> _dRepo;
		private readonly ILocationAppService _locService;
		private readonly IRepository<Master.Location> _lRepo;
		private readonly IRepository<Transaction.Ticket> _ticketManager;
		private readonly IRepository<Transaction.Order> _orderManager;
		private readonly IUnitOfWorkManager _unitOfWorkManager;
		private ICustomizeExporter _customizeExporter;

		public CustomizeAppService(
			IRepository<Transaction.Ticket> ticketManager,
			ILocationAppService locService,
			IRepository<Company> comR,
			IUnitOfWorkManager unitOfWorkManager,
			IRepository<Master.Department> drepo,
			IRepository<Master.Location> lRepo,
			IRepository<Transaction.Order> orderManager,
			ICustomizeExporter customizeExporter
			)
		{
			_ticketManager = ticketManager;
			_companyRe = comR;
			_unitOfWorkManager = unitOfWorkManager;
			_dRepo = drepo;
			_locService = locService;
			_lRepo = lRepo;
			_orderManager = orderManager;
			_customizeExporter = customizeExporter;
		}

		#region sales tax report
		public async Task<GetSaleTaxReportOutput> GetSalesTaxReport(GetTaxReportInputDto input)
		{
			var result = new GetSaleTaxReportOutput();
			try
			{
				using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
				{
					var formatDateSetting = await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.SimpleDateFormat);
					var tickets = GetAllTickets(input).OrderBy(x => x.LastPaymentTime).ToList();
					var listTicketItem = new List<SaleTaxReportItemDto>();
					foreach (var ticket in tickets)
					{
						var status = "";
						var ticketStateValues = ticket.GetTicketStateValues();
						foreach (var ticketStateValue in ticketStateValues)
						{
							if (ticketStateValue.StateName == ConnectConsts.ConnectConsts.Status)
							{
								status = ticketStateValue.State;
							}
						}

						var description = " ";
						var referenceReceiptNumber = " ";
						var vat = ticket.GetTaxTotal();

						if (status == ConnectConsts.ConnectConsts.Refund)
						{
							description = ConnectConsts.ConnectConsts.OutTaxReports_Description_HardCoded_Refund;
						}
						else
						{
							if (!String.IsNullOrWhiteSpace(ticket.ReferenceNumber) && ticket.TotalAmount < 0)
							{
								var referenceTicket = _ticketManager.FirstOrDefault(x => x.TicketNumber == ticket.ReferenceNumber);
								if (referenceTicket != null)
								{
									var referenceTicketstatus = "";
									var referenceTicketStateValues = referenceTicket.GetTicketStateValues();
									foreach (var referenceTicketStateValue in referenceTicketStateValues)
									{
										if (referenceTicketStateValue.StateName == ConnectConsts.ConnectConsts.Status)
										{
											referenceTicketstatus = referenceTicketStateValue.State;
										}
									}
									if (referenceTicketstatus == ConnectConsts.ConnectConsts.Returned)
									{
										vat = -vat;
										referenceReceiptNumber = ticket.ReferenceNumber;
										description = ConnectConsts.ConnectConsts.OutTaxReports_Description_HardCoded_Return;
									}
								}
							}
						}
						if (String.IsNullOrWhiteSpace(description))
							description = ConnectConsts.ConnectConsts.OutTaxReports_Description_HardCoded_NormalTicket;

						var valueBeforeVat = ticket.TotalAmount - vat;

						var nonSalesItem = new SaleTaxReportItemDto()
						{
							ABBNO = ticket.TicketNumber,
							Amount = ticket.TotalAmount,
							Date = ticket.LastPaymentTime.ToString(formatDateSetting),
							Description = description,
							Location = ticket.Location.Code,
							POS = ticket.TerminalName,
							ReferenceReceiptNumber = referenceReceiptNumber,
							ValueBeforeVAT = valueBeforeVat,
							Vat = vat
						};
						listTicketItem.Add(nonSalesItem);
					}

					var mapdataAsQueryable = listTicketItem.AsQueryable();
					// filter by query builder
					if (!string.IsNullOrEmpty(input.DynamicFilter))
					{
						var jsonSerializerSettings = new JsonSerializerSettings
						{
							ContractResolver =
								new CamelCasePropertyNamesContractResolver()
						};
						var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
						if (filRule?.Rules != null)
						{
							mapdataAsQueryable = mapdataAsQueryable.BuildQuery(filRule);
						}
					}

					var dataByLocation_Date_POS = mapdataAsQueryable
						 .GroupBy(c => new
						 {
							 c.POS,
							 c.Date,
							 c.Location
						 })
						.Select(p => new GetSaleTaxReprotByPOS()
						{
							POSName = p.Key.POS,
							Location = p.Key.Location,
							Date = p.Key.Date,
							ListItem = p.ToList()
						}).ToList();

					var dataByLocation_Date = dataByLocation_Date_POS.GroupBy(x => new
					{
						x.Location,
						x.Date
					}).Select(x => new GetSaleTaxReprotByDate()
					{
						Location = x.Key.Location,
						Date = x.Key.Date,
						ListDataByPOS = x.ToList()
					}).ToList();


					var dataByLocation = dataByLocation_Date.GroupBy(x => new
					{
						x.Location,
					}).Select(x => new GetSaleTaxReprotByLocation()
					{
						Location = x.Key.Location,
						ListDataByDate = x.ToList()
					}).ToList();

					result.ListDataByLocation = dataByLocation;

					return await Task.FromResult(result);
				}
			}
			catch (Exception e)
			{
				throw e;
			}

		}
		public async Task<FileDto> GetSaleTaxReportExport(GetTaxReportInputDto input)
		{
			try
			{
				var saleTaxReport = GetSalesTaxReport(input).Result;

				return await _customizeExporter.ExportSaleTaxToFile(saleTaxReport, input);
			}
			catch (Exception e)
			{
				throw e;
			}
		}
		#endregion

		#region nonsalestaxreport 

		#endregion

		#region dailysalestaxreport
		public async Task<PagedResultOutput<DailySalesTaxReportListDto>> GetDailySalesTaxReport(GetTaxReportInputDto input)
		{
			try
			{
				using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
				{
					var query = GetAllTickets(input);

					var countTotal = await query.CountAsync();
					if (input.Sorting == "Id") input.Sorting = "Id";
					var resultPage = query.AsQueryable()
						.OrderBy(input.Sorting)
						 .PageBy(input).ToList();
					var output = resultPage.Select(x => new DailySalesTaxReportListDto
					{
						TicketNumber = x.TicketNumber,
						Amount = x.TotalAmount,
						Date = x.LastPaymentTime.ToString("MM/dd/yyyy"),
						Discription = x.Note,
						Vat = -x.GetTaxTotal(),
						ValueBeforeVAT = x.TotalAmount - (-x.GetTaxTotal()),
						ReferenceReceiptNumber = x.ReferenceNumber,
						Id = x.Id,
						POS = x.TerminalName
					})
						.ToList();
					return new PagedResultOutput<DailySalesTaxReportListDto>(countTotal, output);
				}
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public async Task<FileDto> GetDailySalesReportExport(GetTaxReportInputDto input)
		{
			try
			{
				var query = GetAllTickets(input).ToList();
				var resultPage = query.AsQueryable()
							  .OrderBy(input.Sorting)
							   //.PageBy(input)
							   .ToList();
				var data = resultPage.Select(x => new DailySalesTaxReportListDto
				{
					TicketNumber = x.TicketNumber,
					Amount = x.TotalAmount,
					Date = x.LastPaymentTime.ToString("MM/dd/yyyy"),
					Discription = x.Note,
					Vat = -x.GetTaxTotal(),
					ValueBeforeVAT = x.TotalAmount - (-x.GetTaxTotal()),
					ReferenceReceiptNumber = x.ReferenceNumber,
					Id = x.Id,
					POS = x.TerminalName
				})
					   .ToList();

				var output = data.GroupBy(x => x.Date)
					.Select(x => new GetDailySalesTaxReportExcelListDto
					{
						Date = x.Key,
						ListItem = x.Select(g => new GetDailySalesTaxReportExcelItemDto
						{
							Vat = g.Vat,
							TicketNumber = g.TicketNumber,
							Amount = g.Amount,
							POS = g.POS,
							Discription = g.Discription,
							ReferenceReceiptNumber = g.ReferenceReceiptNumber,
							ValueBeforeVAT = g.ValueBeforeVAT,
							Date = g.Date

						}).ToList()
					}).ToList();
				return await _customizeExporter.ExportDailySalesTaxReportToFile(output, input);
			}
			catch (Exception e)
			{
				throw e;
			}
		}
		#endregion

		#region monthlysalestaxreport
		public async Task<PagedResultOutput<MonthlySalesTaxReportListDto>> GetMonthlySalesTaxReport(GetTaxReportInputDto input)
		{
			try
			{
				using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
				{
					var query = GetAllTickets(input);

					var countTotal = await query.CountAsync();
					if (input.Sorting == "Id") input.Sorting = "Id";
					var resultPage = query.AsQueryable()
						.OrderBy(input.Sorting)
						 .PageBy(input).ToList();
					var output = resultPage.Select(x => new MonthlySalesTaxReportListDto
					{
						TicketNumber = x.TicketNumber,
						Amount = x.TotalAmount,
						Date = x.LastPaymentTime.ToString("MM/dd/yyyy"),
						Discription = x.Note,
						Vat = -x.GetTaxTotal(),
						ValueBeforeVAT = x.TotalAmount - (-x.GetTaxTotal()),
						ReferenceReceiptNumber = x.ReferenceNumber,
						Id = x.Id,
						POS = x.TerminalName,
						Month = x.LastPaymentTime.ToString("MM/yyyy")

					})
						.ToList();
					return new PagedResultOutput<MonthlySalesTaxReportListDto>(countTotal, output);
				}
			}
			catch (Exception e)
			{
				throw e;
			}
		}
		public async Task<FileDto> GetMonthlySalesTaxReportExport(GetTaxReportInputDto input)
		{
			try
			{
				var query = GetAllTickets(input).ToList();
				var resultPage = query.AsQueryable()
							  .OrderBy(input.Sorting)
							   //.PageBy(input)
							   .ToList();
				var data = resultPage.Select(x => new MonthlySalesTaxReportListDto
				{
					TicketNumber = x.TicketNumber,
					Amount = x.TotalAmount,
					Date = x.LastPaymentTime.ToString("MM/dd/yyyy"),
					Discription = x.Note,
					Vat = -x.GetTaxTotal(),
					ValueBeforeVAT = x.TotalAmount - (-x.GetTaxTotal()),
					ReferenceReceiptNumber = x.ReferenceNumber,
					Id = x.Id,
					POS = x.TerminalName,
					Month = x.LastPaymentTime.ToString("MM/yyyy")
				})
					   .ToList();

				var output = data.GroupBy(x => x.Month)
					.Select(x => new GetMonthlySalesTaxReportExcelListDto
					{
						Month = x.Key,
						ListItem = x.Select(g => new GetMonthlySalesTaxReportExcelItemDto
						{
							Vat = g.Vat,
							TicketNumber = g.TicketNumber,
							Amount = g.Amount,
							POS = g.POS,
							Discription = g.Discription,
							ReferenceReceiptNumber = g.ReferenceReceiptNumber,
							ValueBeforeVAT = g.ValueBeforeVAT,
							Date = g.Date

						}).ToList()
					}).ToList();
				return await _customizeExporter.ExportMonthlySalesTaxReportToFile(output, input);
			}
			catch (Exception e)
			{
				throw e;
			}
		}
		#endregion


		#region dailynonlysalestaxreport
		public async Task<PagedResultOutput<DailyNonSalesTaxReportListDto>> GetDailyNonSalesTaxReport(GetTaxReportInputDto input)
		{
			try
			{
				using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
				{
					var query = GetAllTickets(input);

					var countTotal = await query.CountAsync();
					if (input.Sorting == "Id") input.Sorting = "Id";
					var resultPage = query.AsQueryable()
						.OrderBy(input.Sorting)
						 .PageBy(input).ToList();
					var output = resultPage.Select(x => new DailyNonSalesTaxReportListDto
					{
						TicketNumber = x.TicketNumber,
						Amount = x.TotalAmount,
						Date = x.LastPaymentTime.ToString("MM/dd/yyyy"),
						Discription = x.Note,
						Vat = -x.GetTaxTotal(),
						ValueBeforeVAT = x.TotalAmount - (-x.GetTaxTotal()),
						ReferenceReceiptNumber = x.ReferenceNumber,
						Id = x.Id,
						POS = x.TerminalName,
						Month = x.LastPaymentTime.ToString("MM/yyyy"),
						PlantCode = x.Location.Code
					})
						.ToList();
					return new PagedResultOutput<DailyNonSalesTaxReportListDto>(countTotal, output);
				}
			}
			catch (Exception e)
			{
				throw e;
			}
		}
		public async Task<FileDto> GetDailyNonSalesTaxReportExport(GetTaxReportInputDto input)
		{
			try
			{
				var query = GetAllTickets(input).ToList();
				var resultPage = query.AsQueryable()
							  .OrderBy(input.Sorting)
							   //.PageBy(input)
							   .ToList();
				var data = resultPage.Select(x => new DailyNonSalesTaxReportListDto
				{
					TicketNumber = x.TicketNumber,
					Amount = x.TotalAmount,
					Date = x.LastPaymentTime.ToString("MM/dd/yyyy"),
					Discription = x.Note,
					Vat = -x.GetTaxTotal(),
					ValueBeforeVAT = x.TotalAmount - (-x.GetTaxTotal()),
					ReferenceReceiptNumber = x.ReferenceNumber,
					Id = x.Id,
					POS = x.TerminalName,
					Month = x.LastPaymentTime.ToString("MM/yyyy"),
					PlantCode = x.Location.Code
				})
				 .ToList();

				var output = data.GroupBy(x => x.PlantCode)
					.Select(x => new GetDailyNonSalesTaxReportExcelListDto
					{
						PlantCode = x.Key,
						ListItem = x.Select(g => new GetDailyNonSalesTaxReportExcelItemDto
						{
							Vat = g.Vat,
							TicketNumber = g.TicketNumber,
							Amount = g.Amount,
							POS = g.POS,
							Discription = g.Discription,
							ReferenceReceiptNumber = g.ReferenceReceiptNumber,
							ValueBeforeVAT = g.ValueBeforeVAT,
							Date = g.Date
						}).ToList()
					}).ToList();
				return await _customizeExporter.ExportDailyNonSalesTaxReportToFile(output, input);
			}
			catch (Exception e)
			{
				throw e;
			}
		}
		#endregion

		#region monthlynonlysalestaxreport
		public async Task<PagedResultOutput<MonthlyNonSalesTaxReportListDto>> GetMonthlyNonSalesTaxReport(GetTaxReportInputDto input)
		{
			try
			{
				using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
				{
					var query = GetAllTickets(input).OrderBy(x => x.LastPaymentTime).ToList();

					var countTotal = query.Count();

					var dailyNonSales = query.Select(x => new DailyNonSalesTaxReportListDto
					{
						TicketNumber = x.TicketNumber,
						Amount = x.TotalAmount,
						Date = x.LastPaymentTime.ToString("MM/dd/yyyy"),
						Discription = x.Note,
						Vat = -x.GetTaxTotal(),
						ValueBeforeVAT = x.TotalAmount - (-x.GetTaxTotal()),
						ReferenceReceiptNumber = x.ReferenceNumber,
						Id = x.Id,
						POS = x.TerminalName,
						Month = x.LastPaymentTime.ToString("MM/yyyy"),
						PlantCode = x.Location.Code,
					})
						.ToList();

					var monthlySales = dailyNonSales.GroupBy(x => x.Month).Select(x => new MonthlyNonSalesTaxReportListDto
					{
						Month = x.Key,
						PlantCode = x.Select(p => p.PlantCode).FirstOrDefault(),
						Amount = x.Sum(i => i.Amount),
						ValueBeforeVAT = x.Sum(i => i.ValueBeforeVAT),
						Vat = x.Sum(i => i.Vat)
					});

					var resultPage = monthlySales.AsQueryable()
						 .PageBy(input).ToList();
					return new PagedResultOutput<MonthlyNonSalesTaxReportListDto>(countTotal, resultPage);
				}
			}
			catch (Exception e)
			{
				throw e;
			}
		}
		public async Task<FileDto> GetMonthlyNonSalesTaxReportExport(GetTaxReportInputDto input)
		{
			try
			{
				var query = GetAllTickets(input).ToList();
				var dailyNonSales = query.Select(x => new DailyNonSalesTaxReportListDto
				{
					TicketNumber = x.TicketNumber,
					Amount = x.TotalAmount,
					Date = x.LastPaymentTime.ToString("MM/dd/yyyy"),
					Discription = x.Note,
					Vat = -x.GetTaxTotal(),
					ValueBeforeVAT = x.TotalAmount - (-x.GetTaxTotal()),
					ReferenceReceiptNumber = x.ReferenceNumber,
					Id = x.Id,
					POS = x.TerminalName,
					Month = x.LastPaymentTime.ToString("MM/yyyy"),
					PlantCode = x.Location.Code,
				})
					   .ToList();

				var monthlySales = dailyNonSales.GroupBy(x => x.Month).Select(x => new MonthlyNonSalesTaxReportListDto
				{
					Month = x.Key,
					PlantCode = x.Select(p => p.PlantCode).FirstOrDefault(),
					Amount = x.Sum(i => i.Amount),
					ValueBeforeVAT = x.Sum(i => i.ValueBeforeVAT),
					Vat = x.Sum(i => i.Vat)
				});

				var output = monthlySales.GroupBy(x => x.Month)
					.Select(x => new GetMonthlyNonSalesTaxReportExcelListDto
					{
						Month = x.Key,
						ListItem = x.Select(g => new GetMonthlyNonSalesTaxReportExcelItemDto
						{
							PlantCode = x.Select(p => p.PlantCode).FirstOrDefault(),
							Amount = x.Sum(i => i.Amount),
							ValueBeforeVAT = x.Sum(i => i.ValueBeforeVAT),
							Vat = x.Sum(i => i.Vat)
						}).ToList()
					}).ToList();
				return await _customizeExporter.ExportMonthlyNonSalesTaxReportToFile(output, input);
			}
			catch (Exception e)
			{
				throw e;
			}
		}
		#endregion


		public IQueryable<Transaction.Ticket> GetAllTickets(GetTaxReportInputDto input)
		{
			IQueryable<Transaction.Ticket> tickets;

			if (!string.IsNullOrEmpty(input.TicketNo))
				using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
				{
					tickets = _ticketManager.GetAll()
						.Where(a => a.TicketNumber.Equals(input.TicketNo) || a.ReferenceNumber.Equals(input.TicketNo));
					return tickets;
				}

			var correctDate = CorrectInputDate(input);
			using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
			{
				tickets = _ticketManager.GetAll();
				if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
				{
					if (correctDate)
					{
						tickets =
							tickets.Where(
								a =>
									a.LastPaymentTime >=
									input.StartDate
									&&
									a.LastPaymentTime <=
									input.EndDate);
					}
					else
					{
						var mySt = input.StartDate.Date;
						var myEn = input.EndDate.Date.AddDays(1).AddMilliseconds(-1);

						tickets =
							tickets.Where(
								a =>
									a.LastPaymentTimeTruc >= mySt
									&&
									a.LastPaymentTimeTruc <= myEn);
					}
				}
			}

			if (input.Location > 0)
			{
				tickets = tickets.Where(a => a.LocationId == input.Location);
			}
			else if (input.Locations != null && input.Locations.Any())
			{
				var locations = input.Locations.Select(a => a.Id).ToList();
				tickets = tickets.Where(a => locations.Contains(a.LocationId));
			}
			else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
					 && !input.LocationGroup.Locations.Any())
			{
				var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
				{
					Locations = input.LocationGroup.Locations,
					Group = false
				});
				if (locations.Any()) tickets = tickets.Where(a => locations.Contains(a.LocationId));
			}
			else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
			{
				var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
				{
					Locations = input.LocationGroup.Locations,
					Group = false
				});
				tickets = tickets.Where(a => locations.Contains(a.LocationId));
			}
			else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
			{
				var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
				{
					Groups = input.LocationGroup.Groups,
					Group = true
				});
				tickets = tickets.Where(a => locations.Contains(a.LocationId));
			}
			if (input.LocationGroup != null)
			{
				if (input.LocationGroup.NonLocations != null && input.LocationGroup.NonLocations.Any())
				{
					var nonlocations = input.LocationGroup.NonLocations.Select(a => a.Id).ToList();
					tickets = tickets.Where(a => !nonlocations.Contains(a.LocationId));
				}
			}

			else if (input.LocationGroup == null)
			{
				var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
				{
					Locations = new List<SimpleLocationDto>(),
					Group = false,
					UserId = input.UserId
				});
				if (input.UserId > 0)
					tickets = tickets.Where(a => locations.Contains(a.LocationId));
			}

			tickets = tickets.Where(a => a.Credit == input.Credit);

			if (!string.IsNullOrEmpty(input.SelectedMonthAndYear))
			{
				var monthYear = input.SelectedMonthAndYear.Split('/');
				var month = int.Parse(monthYear[0]);
				var year = int.Parse(monthYear[1]);
				tickets = tickets.Where(a =>
				  EntityFunctions.TruncateTime(a.LastPaymentTime).Value.Month == month
				 && EntityFunctions.TruncateTime(a.LastPaymentTime).Value.Year == year
				); ;
			}
			if (input.POS > 0)
			{
				tickets = tickets.Where(l => l.LocationId == input.POS);
			}

			return tickets;
		}

		public bool CorrectInputDate(IDateInput input)
		{
			if (input.NotCorrectDate) return true;
			var returnStatus = true;

			var operateHours = 0M;

			if (input.Locations != null && input.Locations.Any() && input.Locations.Count() == 1)
				operateHours = _lRepo.Get(input.Locations.First().Id).ExtendedBusinessHours;

			if (operateHours == 0M && input.Location > 0)
				operateHours = _lRepo.Get(input.Location).ExtendedBusinessHours;
			if (operateHours == 0M && input.LocationGroup != null && input.LocationGroup.Locations.Any() &&
				input.LocationGroup.Locations.Count() == 1)
				operateHours = _lRepo.Get(input.LocationGroup.Locations.First().Id).ExtendedBusinessHours;
			if (operateHours == 0M)
				operateHours = SettingManager.GetSettingValue<decimal>(AppSettings.ConnectSettings.OperateHours);

			if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
			{
				if (operateHours == 0M) returnStatus = false;
				input.EndDate = input.EndDate.AddDays(1);
				input.StartDate = input.StartDate.AddHours(Convert.ToDouble(operateHours));
				input.EndDate = input.EndDate.AddHours(Convert.ToDouble(operateHours)).AddMinutes(-1);
			}
			else
			{
				returnStatus = false;
			}

			return returnStatus;
		}

		public async Task<GetNonSalesTaxReportOutputDto> GetNonSalesTaxReport(GetNonSalesTaxReportInput input)
		{
			var tickets = GetAllTicketsForNonSalesTaxReport(input).OrderBy(x=> x.LastPaymentTime).ToList();
			var formatDateSetting = await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.SimpleDateFormat);
			var refundTickets = tickets.Where(x => x.GetState(ConnectConsts.ConnectConsts.Refund)?.State != null).ToList();
			var prevTicketsNumber = tickets
				.Where(x => x.GetState(ConnectConsts.ConnectConsts.Returned)?.State != null)
				.Select(x => x.TicketNumber)
				.ToList();
			var returnedTickets = tickets
				.Where(x => prevTicketsNumber.Contains(x.ReferenceNumber))
				.ToList();

			var ticketsForNonSalesTaxReport = refundTickets.Concat(returnedTickets).ToList();

			var listTicketItem = new List<NonSalesTaxReportItem>();


			foreach (var ticket in ticketsForNonSalesTaxReport)
			{
				var status = "";
				var ticketStateValues = ticket.GetTicketStateValues();
				foreach (var ticketStateValue in ticketStateValues)
				{
					if (ticketStateValue.StateName == ConnectConsts.ConnectConsts.Status)
					{
						status = ticketStateValue.State;
					}
				}

				var description = " ";
				var referenceReceiptNumber = " ";
				var vat = ticket.GetTaxTotal();

				if (status == ConnectConsts.ConnectConsts.Refund)
				{
					description = ConnectConsts.ConnectConsts.OutTaxReports_Description_HardCoded_Refund;
				}
				else
				{
					if (!string.IsNullOrWhiteSpace(ticket.ReferenceNumber) && ticket.TotalAmount < 0)
					{
						var referenceTicket = _ticketManager.FirstOrDefault(x => x.TicketNumber == ticket.ReferenceNumber);
						if (referenceTicket != null)
						{
							var referenceTicketstatus = "";
							var referenceTicketStateValues = referenceTicket.GetTicketStateValues();
							foreach (var referenceTicketStateValue in referenceTicketStateValues)
							{
								if (referenceTicketStateValue.StateName == ConnectConsts.ConnectConsts.Status)
								{
									referenceTicketstatus = referenceTicketStateValue.State;
								}
							}
							if (referenceTicketstatus == ConnectConsts.ConnectConsts.Returned)
							{
								//vat = -vat;
								referenceReceiptNumber = ticket.ReferenceNumber;
								description = ConnectConsts.ConnectConsts.OutTaxReports_Description_HardCoded_Return;
							}
						}
					}
				}
				if (string.IsNullOrWhiteSpace(description))
					description = ConnectConsts.ConnectConsts.OutTaxReports_Description_HardCoded_NormalTicket;

				var valueBeforeVat = ticket.TotalAmount - vat;


				var nonSalesItem = new NonSalesTaxReportItem()
				{
					ABBNO = ticket.TicketNumber,
					Amount = ticket.TotalAmount,
					Date = ticket.LastPaymentTime.ToString(formatDateSetting),
					Description = description,
					Location = ticket.Location.Code,
					POS = ticket.TerminalName,
					ReferenceReceiptNumber = referenceReceiptNumber,
					ValueBeforeVAT = valueBeforeVat,
					Vat = vat
				};
				listTicketItem.Add(nonSalesItem);
			}



			var mapdataAsQueryable = listTicketItem.AsQueryable();
			// filter by query builder

			if (!string.IsNullOrEmpty(input.DynamicFilter))
			{
				var jsonSerializerSettings = new JsonSerializerSettings
				{
					ContractResolver =
						new CamelCasePropertyNamesContractResolver()
				};
				var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
				if (filRule?.Rules != null)
				{
					mapdataAsQueryable = mapdataAsQueryable.BuildQuery(filRule);
				}
			}

			var ticketBy_Date_POS = mapdataAsQueryable.GroupBy(x => new { x.POS, x.Date }).Select(x => new NonSalesTaxReportByPOS()
			{
				Date = x.Key.Date,
				POSName = x.Key.POS,
				ListItem = x.ToList()
			});
			var ticketBy_Date = ticketBy_Date_POS.GroupBy(x => new { x.Date }).Select(x => new NonSalesTaxReportByDate()
			{
				Date = x.Key.Date,
				ListDataByPOS = x.ToList()
			});

			var result = new GetNonSalesTaxReportOutputDto();
			result.ListDataByDate = ticketBy_Date.ToList();
			return await Task.FromResult(result);
		}

		public async Task<FileDto> GetNonSalesTaxReportExport(GetNonSalesTaxReportInput input)
		{
			try
			{
				var nonSalesTaxReport = GetNonSalesTaxReport(input).Result;

				return await _customizeExporter.ExportNonSalesTaxReportToFile(nonSalesTaxReport, input);
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public IQueryable<Transaction.Ticket> GetAllTicketsForNonSalesTaxReport(GetNonSalesTaxReportInput input)
		{
			IQueryable<Transaction.Ticket> tickets;

			if (!string.IsNullOrEmpty(input.TicketNo))
				using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
				{
					tickets = _ticketManager.GetAll()
						.Where(a => a.TicketNumber.Equals(input.TicketNo) || a.ReferenceNumber.Equals(input.TicketNo));
					return tickets;
				}

			var correctDate = CorrectInputDate(input);
			using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
			{
				tickets = _ticketManager.GetAll();
				if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
				{
					if (correctDate)
					{
						tickets =
							tickets.Where(
								a =>
									a.LastPaymentTime >=
									input.StartDate
									&&
									a.LastPaymentTime <=
									input.EndDate);
					}
					else
					{
						var mySt = input.StartDate.Date;
						var myEn = input.EndDate.Date.AddDays(1).AddMilliseconds(-1);

						tickets =
							tickets.Where(
								a =>
									a.LastPaymentTimeTruc >= mySt
									&&
									a.LastPaymentTimeTruc <= myEn);
					}
				}
			}

			if (input.Location > 0)
			{
				tickets = tickets.Where(a => a.LocationId == input.Location);
			}
			else if (input.Locations != null && input.Locations.Any())
			{
				var locations = input.Locations.Select(a => a.Id).ToList();
				tickets = tickets.Where(a => locations.Contains(a.LocationId));
			}
			else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
					 && !input.LocationGroup.Locations.Any())
			{
				var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
				{
					Locations = input.LocationGroup.Locations,
					Group = false
				});
				if (locations.Any()) tickets = tickets.Where(a => locations.Contains(a.LocationId));
			}
			else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
			{
				var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
				{
					Locations = input.LocationGroup.Locations,
					Group = false
				});
				tickets = tickets.Where(a => locations.Contains(a.LocationId));
			}
			else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
			{
				var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
				{
					Groups = input.LocationGroup.Groups,
					Group = true
				});
				tickets = tickets.Where(a => locations.Contains(a.LocationId));
			}
			if (input.LocationGroup != null)
			{
				if (input.LocationGroup.NonLocations != null && input.LocationGroup.NonLocations.Any())
				{
					var nonlocations = input.LocationGroup.NonLocations.Select(a => a.Id).ToList();
					tickets = tickets.Where(a => !nonlocations.Contains(a.LocationId));
				}
			}

			else if (input.LocationGroup == null)
			{
				var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
				{
					Locations = new List<SimpleLocationDto>(),
					Group = false,
					UserId = input.UserId
				});
				if (input.UserId > 0)
					tickets = tickets.Where(a => locations.Contains(a.LocationId));
			}

			tickets = tickets.Where(a => a.Credit == input.Credit);

			return tickets;
		}


	}
}
