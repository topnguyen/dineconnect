﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Customize.Dto
{
    public class GetSaleTaxReportExcelListDto
    {
        public string POS { get; set; }
        public List<GetSaleTaxReportExcelItemDto> ListItem { get; set; }

        public GetSaleTaxReportExcelListDto()
        {
            ListItem = new List<GetSaleTaxReportExcelItemDto>();
        }
    }

    public class GetSaleTaxReportExcelItemDto
    {
        public string Date { get; set; }
        public string ABBNO { get; set; }
        public string Discription { get; set; }
        public decimal ValueBeforeVAT { get; set; }
        public decimal Vat { get; set; }
        public decimal Amount { get; set; }
        public string ReferenceReceiptNumber { get; set; }
    }


}
