﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Customize.Dto
{
    public class GetDailyNonSalesTaxReportExcelListDto
    {
        public string PlantCode { get; set; }
        public List<GetDailyNonSalesTaxReportExcelItemDto> ListItem { get; set; }

        public GetDailyNonSalesTaxReportExcelListDto()
        {
            ListItem = new List<GetDailyNonSalesTaxReportExcelItemDto>();
        }
    }

    public class GetDailyNonSalesTaxReportExcelItemDto
    {
        public string Date { get; set; }
        public string POS { get; set; }
        public string TicketNumber { get; set; }
        public string Discription { get; set; }
        public decimal ValueBeforeVAT { get; set; }
        public decimal Vat { get; set; }
        public decimal Amount { get; set; }
        public string ReferenceReceiptNumber { get; set; }
    }


}
