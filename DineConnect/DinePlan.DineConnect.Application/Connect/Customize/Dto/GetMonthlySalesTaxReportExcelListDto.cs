﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Customize.Dto
{
    public class GetMonthlySalesTaxReportExcelListDto
    {
        public string Month { get; set; }
        public List<GetMonthlySalesTaxReportExcelItemDto> ListItem { get; set; }

        public GetMonthlySalesTaxReportExcelListDto()
        {
            ListItem = new List<GetMonthlySalesTaxReportExcelItemDto>();
        }
    }

    public class GetMonthlySalesTaxReportExcelItemDto
    {
        public string Date { get; set; }
        public string TicketNumber { get; set; }
        public string POS { get; set; }
        public string Discription { get; set; }
        public decimal ValueBeforeVAT { get; set; }
        public decimal Vat { get; set; }
        public decimal Amount { get; set; }
        public string ReferenceReceiptNumber { get; set; }
    }


}
