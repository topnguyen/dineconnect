﻿using Abp.Application.Services.Dto;
using Abp.Extensions;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Discount.Dtos;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Customize.Dto
{
    public class GetTaxReportInputDto : MaxPagedAndSortedInputDto, IFileExport ,IShouldNormalize, IDateInput, IBackgroundExport
    {
        public GetTaxReportInputDto()
        {
            Locations = new List<SimpleLocationDto>();
            LocationGroup = new LocationGroupDto();
            Promotions = new List<PromotionListDto>();
        }

        public List<PromotionListDto> Promotions { get; set; }
        public List<string> PaymentTags { get; set; }

        public List<int> Payments { get; set; }
        public List<ComboboxItemDto> Transactions { get; set; }
        public List<ComboboxItemDto> Departments { get; set; }
        public List<ComboboxItemDto> TicketTags { get; set; }
        public List<ComboboxItemDto> AuditTypes { get; set; }
        public string Department { get; set; }
        public string TerminalName { get; set; }
        public string LastModifiedUserName { get; set; }
        public string OutputType { get; set; }
        public string ExportType { get; set; }
        public bool ByLocation { get; set; }
        public string Duration { get; set; }
        public int TenantId { get; set; }
        public string ExportOutput { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Location { get; set; }
        public long UserId { get; set; }
        public List<SimpleLocationDto> Locations { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public bool NotCorrectDate { get; set; }
        public bool Credit { get; set; }
        public bool Refund { get; set; }
        public string TicketNo { get; set; }

        public ExportType ExportOutputType { get; set; }
        public PaperSize PaperSize { get; set; }
        public bool Portrait { get; set; }

        public void Normalize()
        {
            if (Sorting.IsNullOrWhiteSpace())
            {
                Sorting = "Id";
            }
        }

        public bool RunInBackground { get; set; }
        public string ReportDescription { get; set; }
        public FileDto FileDto { get; set; }

        public bool PaymentName { get; set; }

        public bool IncludeTax { get; set; }
        public bool ReportDisplay { get; set; }

        public string DynamicFilter { get; set; }

        public string SelectedMonthAndYear { get; set; }
        public int POS { get; set; }
		OpenHtmlToPdf.PaperSize IFileExport.PaperSize { get; set ; }
        public string DateFormat { get; set ; }
        public string DatetimeFormat { get ; set ; }
    }
}