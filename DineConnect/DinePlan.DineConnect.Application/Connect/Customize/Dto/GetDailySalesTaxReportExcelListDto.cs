﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Customize.Dto
{
    public class GetDailySalesTaxReportExcelListDto
    {
        public string Date { get; set; }
        public List<GetDailySalesTaxReportExcelItemDto> ListItem { get; set; }

        public GetDailySalesTaxReportExcelListDto()
        {
            ListItem = new List<GetDailySalesTaxReportExcelItemDto>();
        }
    }

    public class GetDailySalesTaxReportExcelItemDto
    {
        public string Date { get; set; }
        public string POS { get; set; }
        public string TicketNumber { get; set; }
        public string Discription { get; set; }
        public decimal ValueBeforeVAT { get; set; }
        public decimal Vat { get; set; }
        public decimal Amount { get; set; }
        public string ReferenceReceiptNumber { get; set; }
    }


}
