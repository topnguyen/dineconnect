﻿using DinePlan.DineConnect.Connect.Master;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Customize.Dto
{
	public class TaxtReportListDto
	{
        public TaxtReportListDto()
        {
            TenderDetails = new List<TaxtReportDetailDto>();
        }
        public PaymentType PaymentType { get; set; }
        public string PaymentTypeName => PaymentType != null ? PaymentType.Name : string.Empty;
        public List<TaxtReportDetailDto> TenderDetails { get; set; }
    }
}
