﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Customize.Dto
{
    public class TaxtReportDetailDto
    {
        public string TenderCode { get; set; }
        public string TenderName { get; set; }
        public string TicketNo { get; set; }
        public string Terminal { get; set; }
        public string InvoiceNo { get; set; }
        public DateTime DateTime { get; set; }
        public string Table { get; set; }
        public string Employee { get; set; }
        public string CardNumber { get; set; }
        public string ReferenceNo { get; set; }
        public decimal SubTotal => (Total - Vat - ServiceCharge + Discount);
        public decimal Discount { get; set; }
        public decimal ServiceCharge { get; set; }
        public decimal Vat { get; set; }
        public decimal Total { get; set; }
        public DateTime? Date { get; set; }
        public string POS { get; set; }

    }
}
