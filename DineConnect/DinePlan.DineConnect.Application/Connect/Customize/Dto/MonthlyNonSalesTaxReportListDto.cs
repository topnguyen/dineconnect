﻿using Abp.Domain.Entities;

namespace DinePlan.DineConnect.Connect.Customize.Dto
{
	public class MonthlyNonSalesTaxReportListDto : Entity<long>
	{
		public string PlantCode { get; set; }
		public string Month { get; set; }
		public string POS { get; set; }
		public string TicketNumber { get; set; }
		public string Discription { get; set; }
		public decimal ValueBeforeVAT { get; set; }
		public decimal Vat { get; set; }
		public decimal Amount { get; set; }
		public string ReferenceReceiptNumber { get; set; }
	}
}
