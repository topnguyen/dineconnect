﻿using Abp.Domain.Entities;
using System.Collections.Generic;
using System.Linq;

namespace DinePlan.DineConnect.Connect.Customize.Dto
{
	public class GetSaleTaxReportOutput
	{
		public List<GetSaleTaxReprotByLocation> ListDataByLocation { get; set; }
		public GetSaleTaxReportOutput()
		{
			ListDataByLocation = new List<GetSaleTaxReprotByLocation>();
		}
		public decimal? TotalValueBeforeVAT
		{
			get
			{
				var result = ListDataByLocation.Sum(x => x.TotalForLocationValueBeforeVAT);
				return result;
			}
		}
		public decimal? TotalVat
		{
			get
			{
				var result = ListDataByLocation.Sum(x => x.TotalForLocationVat);
				return result;
			}
		}
		public decimal? TotalAmount
		{
			get
			{
				var result = ListDataByLocation.Sum(x => x.TotalForLocationAmount);
				return result;
			}
		}

	}

	public class GetSaleTaxReprotByLocation {
		public string Location { get; set; }
		public List<GetSaleTaxReprotByDate> ListDataByDate { get; set; }
		public GetSaleTaxReprotByLocation()
		{
			ListDataByDate = new List<GetSaleTaxReprotByDate>();
		}
		public decimal? TotalForLocationValueBeforeVAT
		{
			get
			{
				var result = ListDataByDate.Sum(x => x.TotalForDateValueBeforeVAT);
				return result;
			}
		}
		public decimal? TotalForLocationVat
		{
			get
			{
				var result = ListDataByDate.Sum(x => x.TotalForDateVat);
				return result;
			}
		}
		public decimal? TotalForLocationAmount
		{
			get
			{
				var result = ListDataByDate.Sum(x => x.TotalForDateAmount);
				return result;
			}
		}
	}
	public class GetSaleTaxReprotByDate : Entity<long>
	{
	    public string Date { get; set; }
	    public string Location { get; set; }
		public List<GetSaleTaxReprotByPOS> ListDataByPOS { get; set; }
		public GetSaleTaxReprotByDate()
		{
			ListDataByPOS = new List<GetSaleTaxReprotByPOS>();
		}

		public decimal? TotalForDateValueBeforeVAT
		{
			get
			{
				var result = ListDataByPOS.Sum(x=>x.TotalForPOSValueBeforeVAT);
				return result;
			}
		}
		public decimal? TotalForDateVat
		{
			get
			{
				var result = ListDataByPOS.Sum(x => x.TotalForPOSVat);
				return result;
			}
		}
		public decimal? TotalForDateAmount
		{
			get
			{
				var result = ListDataByPOS.Sum(x => x.TotalForPOSAmount);
				return result;
			}
		}
	}
	public class GetSaleTaxReprotByPOS 
	{
		public string POSName { get; set; }
		public string Date { get; set; }
		public string Location { get; set; }
		public List<SaleTaxReportItemDto> ListItem { get; set; }
		public GetSaleTaxReprotByPOS()
		{
			ListItem = new List<SaleTaxReportItemDto>();
		}

		public decimal? TotalForPOSValueBeforeVAT
		{
			get
			{
				var result = ListItem.Sum(x => x.ValueBeforeVAT);
				return result;
			}
		}
		public decimal? TotalForPOSVat
		{
			get
			{
				var result = ListItem.Sum(x => x.Vat);
				return result;
			}
		}
		public decimal? TotalForPOSAmount
		{
			get
			{
				var result = ListItem.Sum(x => x.Amount);
				return result;
			}
		}
	}
	public class SaleTaxReportItemDto : Entity<long>
	{
		public string Location { get; set; }
		public string POS { get; set; }
		public string Date { get; set; }
		public string ABBNO { get; set; }
		public string Description { get; set; }
		public decimal ValueBeforeVAT { get; set; }
		public decimal Vat { get; set; }
		public decimal Amount { get; set; }
		public string ReferenceReceiptNumber { get; set; }
	}
}
