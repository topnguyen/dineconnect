﻿using Abp.Domain.Entities;

namespace DinePlan.DineConnect.Connect.Customize.Dto
{
	public class DailySalesTaxReportListDto : Entity<long>
	{
		public string POS { get; set; }
		public string Date { get; set; }
		public string TicketNumber { get; set; }
		public string Discription { get; set; }
		public decimal ValueBeforeVAT { get; set; }
		public decimal Vat { get; set; }
		public decimal Amount { get; set; }
		public string ReferenceReceiptNumber { get; set; }
	}
}
