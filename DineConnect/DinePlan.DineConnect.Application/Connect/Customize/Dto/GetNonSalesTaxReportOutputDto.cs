﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Customize.Dto
{
	public class GetNonSalesTaxReportOutputDto
	{
		public List<NonSalesTaxReportByDate> ListDataByDate { get; set; }
		public GetNonSalesTaxReportOutputDto()
		{
			ListDataByDate = new List<NonSalesTaxReportByDate>();
		}
		public decimal? TotalValueBeforeVAT
		{
			get
			{
				var result = ListDataByDate.Sum(x => x.TotalForDateValueBeforeVAT);
				return result;
			}
		}
		public decimal? TotalVat
		{
			get
			{
				var result = ListDataByDate.Sum(x => x.TotalForDateVat);
				return result;
			}
		}
		public decimal? TotalAmount
		{
			get
			{
				var result = ListDataByDate.Sum(x => x.TotalForDateAmount);
				return result;
			}
		}
	}
	public class NonSalesTaxReportByDate
	{
		public string Date { get; set; }
		public string Location { get; set; }
		public List<NonSalesTaxReportByPOS> ListDataByPOS { get; set; }
		public NonSalesTaxReportByDate()
		{
			ListDataByPOS = new List<NonSalesTaxReportByPOS>();
		}

		public decimal? TotalForDateValueBeforeVAT
		{
			get
			{
				var result = ListDataByPOS.Sum(x => x.TotalForPOSValueBeforeVAT);
				return result;
			}
		}
		public decimal? TotalForDateVat
		{
			get
			{
				var result = ListDataByPOS.Sum(x => x.TotalForPOSVat);
				return result;
			}
		}
		public decimal? TotalForDateAmount
		{
			get
			{
				var result = ListDataByPOS.Sum(x => x.TotalForPOSAmount);
				return result;
			}
		}
	}
	public class NonSalesTaxReportByPOS
	{
		public string POSName { get; set; }
		public string Date { get; set; }
		public string Location { get; set; }
		public List<NonSalesTaxReportItem> ListItem { get; set; }
		public NonSalesTaxReportByPOS()
		{
			ListItem = new List<NonSalesTaxReportItem>();
		}

		public decimal? TotalForPOSValueBeforeVAT
		{
			get
			{
				var result = ListItem.Sum(x => x.ValueBeforeVAT);
				return result;
			}
		}
		public decimal? TotalForPOSVat
		{
			get
			{
				var result = ListItem.Sum(x => x.Vat);
				return result;
			}
		}
		public decimal? TotalForPOSAmount
		{
			get
			{
				var result = ListItem.Sum(x => x.Amount);
				return result;
			}
		}
	}
	public class NonSalesTaxReportItem
	{
		public string Location { get; set; }
		public string POS { get; set; }
		public string Date { get; set; }
		public string ABBNO { get; set; }
		public string Description { get; set; }
		public decimal ValueBeforeVAT { get; set; }
		public decimal Vat { get; set; }
		public decimal Amount { get; set; }
		public string ReferenceReceiptNumber { get; set; }
	}
}
