﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Customize.Dto
{
    public class GetMonthlyNonSalesTaxReportExcelListDto
    {
        public string Month { get; set; }
        public List<GetMonthlyNonSalesTaxReportExcelItemDto> ListItem { get; set; }

        public GetMonthlyNonSalesTaxReportExcelListDto()
        {
            ListItem = new List<GetMonthlyNonSalesTaxReportExcelItemDto>();
        }
    }

    public class GetMonthlyNonSalesTaxReportExcelItemDto
    {
        public string PlantCode { get; set; }
        public string Month { get; set; }
        public string POS { get; set; }
        public string TicketNumber { get; set; }
        public string Discription { get; set; }
        public decimal ValueBeforeVAT { get; set; }
        public decimal Vat { get; set; }
        public decimal Amount { get; set; }
        public string ReferenceReceiptNumber { get; set; }
    }

}
