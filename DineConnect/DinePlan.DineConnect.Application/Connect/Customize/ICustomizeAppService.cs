﻿
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Customize.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Customize
{
	public interface ICustomizeAppService: IApplicationService
	{
		Task<GetSaleTaxReportOutput> GetSalesTaxReport(GetTaxReportInputDto input);
		Task<FileDto> GetSaleTaxReportExport(GetTaxReportInputDto input);

		Task<GetNonSalesTaxReportOutputDto> GetNonSalesTaxReport(GetNonSalesTaxReportInput input);
		Task<FileDto> GetNonSalesTaxReportExport(GetNonSalesTaxReportInput input);

		Task<PagedResultOutput<DailySalesTaxReportListDto>> GetDailySalesTaxReport(GetTaxReportInputDto input);
		Task<FileDto> GetDailySalesReportExport(GetTaxReportInputDto input);

		Task<PagedResultOutput<MonthlySalesTaxReportListDto>> GetMonthlySalesTaxReport(GetTaxReportInputDto input);
		Task<FileDto> GetMonthlySalesTaxReportExport(GetTaxReportInputDto input);

		Task<PagedResultOutput<DailyNonSalesTaxReportListDto>> GetDailyNonSalesTaxReport(GetTaxReportInputDto input);
		Task<FileDto> GetDailyNonSalesTaxReportExport(GetTaxReportInputDto input);

		Task<PagedResultOutput<MonthlyNonSalesTaxReportListDto>> GetMonthlyNonSalesTaxReport(GetTaxReportInputDto input);
		Task<FileDto> GetMonthlyNonSalesTaxReportExport(GetTaxReportInputDto input);


	}
}
