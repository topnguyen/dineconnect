﻿using DinePlan.DineConnect.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Terminal
{
    public class TerminalSetting
    {

        public static readonly List<Option> Resolutions = ConnectConsts.ConnectConsts.SupportedResolutions.Select(x => new Option { Value = x, Name = x }).ToList();

        public static readonly Dictionary<string, List<FormlyType>> AllTerminalSetting
            = new Dictionary<string, List<FormlyType>>
            {
                {
                    "Setting", new List<FormlyType>
                    {
                        new FormlyType
                        {
                             ClassName="col-xs-3",
                            Key = "AutoMigrate",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Auto Migrate"
                            }
                        },
                        new FormlyType
                        {
                             ClassName="col-xs-3",
                            Key = "AutoSyncConnect",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Auto Sync Connect"
                            }
                        },
                        new FormlyType
                        {
                             ClassName="col-xs-3",
                            Key = "DineCall",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Dine Call"
                            }
                        },
                        new FormlyType
                        {
                             ClassName="col-xs-3",
                            Key = "DisplayTerminalTickets",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Display Terminal Tickets"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "EntitySearchOnName",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Entity Search On Name"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "IsPosItemSearchEnabled",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Is Pos Item Search Enabled"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "NoDecimalKeyPad",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "No Decimal Key Pad"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "OverrideWindowsRegionalSettings",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Override Windows Regional Settings"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "QueueOrder",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Queue Order"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "QuickDine",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Quick Dine"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "QuickDineSync",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Quick Dine Sync"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "StartApiClient",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Start Api Client"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "StartMessagingClient",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Start Messaging Client"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "SwitchDefaultDepartment",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Switch Default Department"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "SyncAll",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Sync All"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "UseBoldFonts",
                            Type = "checkbox",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Use Bold Fonts"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "ResolutionHeight",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "number",
                                Label = "Resolution Height"
                            }
                        },
                         new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "ResolutionWidth",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "number",
                                Label = "Resolution Width"
                            }
                        },
                          new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "WindowScale",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "number",
                                Label = "Window Scale"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "DefaultRecordLimit",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "number",
                                Label = "Default Record Limit"
                            }

                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "DeviceType",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "number",
                                Label = "Device Type"
                            }

                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "FlyBillQuantity",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "number",
                                Label = "Fly Bill Quantity"
                            }

                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "RefreshEntityInSeconds",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "number",
                                Label = "Refresh Entity In Seconds"
                            }

                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "DefaultHtmlReportHeader",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "Default Html Report Header"
                            }

                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "DineMenuImagePath",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "Dine Menu Image Path"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "DownloadUrl",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "Download Url"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "FlyBillJobName",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "Fly Bill Job Name"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "FlyEntityScreens",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "Fly Entity Screens"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "LogoPath",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "Logo Path"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "LogPath",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "Log Path"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "PosItemSearchBinding",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "Pos Item Search Binding"
                            }
                        },
                         new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "PrintFontFamily",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "Print Font Family"
                            }
                        },
                         new FormlyType
                        {
                            ClassName="col-xs-3",
                            Key = "ScaleDeviceName",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "Scale Device Name"
                            }
                        }
                        }
                },
                {
                    "General", new List<FormlyType>
                    {
                        new FormlyType
                        {
                            ClassName="col-xs-12",
                            Key = "LogoPath",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "LogoPath"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-12",
                            Key = "ConnectionString",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "ConnectionString"
                            }
                        } ,
                        new FormlyType
                        {
                            ClassName="col-xs-12 ml_32",
                            Key = "MultipleDatabases",
                            Type = "checkbox",
                            DefaultValue = false,
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "MultipleDatabases"
                            }
                        },

                    }
                },
                {
                    "Display", new List<FormlyType>
                    {
                        new FormlyType
                        {
                            ClassName="col-xs-12",
                            Key = "WindowScale",
                            Type = "input",
                            DefaultValue = ConnectConsts.ConnectConsts.WindowScaleDefault,
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "number",
                                Label = "WindowScale"
                            }
                        },
                          new FormlyType
                        {
                            ClassName="col-xs-12",
                            Key = "Resolution",
                            Type = "select",
                            DefaultValue = ConnectConsts.ConnectConsts.ResolutionDefault,
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Resolution"  ,
                                Options = TerminalSetting.Resolutions
                            }
                        },
                            new FormlyType
                        {
                            ClassName="col-xs-12",
                            Key = "RefreshEntityInSeconds",
                            Type = "input",
                            DefaultValue = ConnectConsts.ConnectConsts.RefreshEntityDefault,
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "number",
                                Label = "Refresh Entity(Seconds)"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-12 ml_32",
                            Key = "UseBoldFonts",
                            Type = "checkbox",
                            DefaultValue = false,
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "BoldFont"
                            }
                        },
                        new FormlyType
                        {
                             ClassName="col-xs-12 ml_32",
                            Key = "IsPosItemSearchEnabled",
                            Type = "checkbox",
                            DefaultValue = false,
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Item Search(POS)"
                            }
                        },
                        new FormlyType
                        {
                             ClassName="col-xs-12 ml_32",
                            Key = "QueueOrder",
                            Type = "checkbox",
                            DefaultValue = false,
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "QueueOrder"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-12 ml_32",
                            Key = "SwitchDefaultDepartment",
                            Type = "checkbox",
                            DefaultValue = false,
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "SwitchDefaultDepartment"
                            }
                        }
                        }
                },
                    {
                    "Service", new List<FormlyType>
                    {
                        new FormlyType
                        {
                            ClassName="col-xs-6",
                            Key = "ApiHost",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "IP Address"
                            }
                        },
                          new FormlyType
                        {
                            ClassName="col-xs-6",
                            Key = "ApiPort",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "number",
                                Label = "Port"
                            }
                        },
                            new FormlyType
                        {
                            ClassName="col-xs-6",
                            Key = "MessagingServerName",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "MessagingServerName"
                            }
                        },
                              new FormlyType
                        {
                            ClassName="col-xs-6",
                            Key = "MessagingServerPort",
                            Type = "input",
                            DefaultValue="0",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "MessagingServerPort"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-6 ml_32",
                            Key = "StartApiClient",
                            Type = "checkbox",
                            DefaultValue = false,
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Start API Server"
                            }
                        },
                         new FormlyType
                        {
                            ClassName="col-xs-6 ml_32",
                            Key = "DineCall",
                            Type = "checkbox",
                            DefaultValue = false,
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Dine Call"
                            }
                        },
                             new FormlyType
                        {
                            ClassName="col-xs-6 ml_32",
                            Key = "QuickDine",
                            Type = "checkbox",
                            DefaultValue = false,
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Quick Dine"
                            }
                        },
                        new FormlyType
                        {
                             ClassName="col-xs-6 ml_32",
                            Key = "QuickDineSync",
                            Type = "checkbox",
                            DefaultValue = false,
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Auto Quick Dine Sync"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-6 ml_32",
                            Key = "GloriaFood",
                            Type = "checkbox",
                            DefaultValue = false,
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Online Order"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-6 ml_32",
                            Key = "AutoSyncConnect",
                            Type = "checkbox",
                            DefaultValue = false,
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Auto Sync Connect"
                            }
                        },
                        new FormlyType
                        {
                            ClassName="col-xs-6 ml_32",
                            Key = "StartMessagingClient",
                            Type = "checkbox",
                            DefaultValue = false,
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Label = "Auto Start Messaging Client"
                            }
                        }
                        }
                },
                  {
                    "DineFly", new List<FormlyType>
                    {
                        new FormlyType
                        {
                            ClassName="col-xs-12",
                            Key = "FlyEntityScreens",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "Fly Entity Screens"
                            }
                        },
                          new FormlyType
                        {
                            ClassName="col-xs-12",
                            Key = "FlyBillJobName",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "BillJob"
                            }
                        }
                    }
                },
                 {
                    "DineMenu", new List<FormlyType>
                    {
                        new FormlyType
                        {
                            ClassName="col-xs-12",
                            Key = "DineMenuImagePath",
                            Type = "input",
                            TemplateOptions = new FormlyTemplateOption
                            {
                                Type = "string",
                                Label = "DineMenuImagePath"
                            }
                        }                      
                    }
                },
            };


        public static readonly string[] TerminalSettings = AllTerminalSetting.Keys.ToArray();
    }
}
