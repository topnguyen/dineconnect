﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Terminal.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.General;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Terminal
{
    public interface ITerminalAppService : IApplicationService
    {
        Task<PagedResultOutput<TerminalListDto>> GetTerminals(GetTerminalsInput input);

        Task<CreateOrEditTerminalInput> GetTerminalForEdit(NullableIdInput input);

        Task<int> CreateOrUpdateTerminal(CreateOrEditTerminalInput input);

        Task DeleteTerminal(NullableIdInput input);

        Task<FileDto> GetAllToExcel();

        Task ActivateItem(IdInput input);

        Task<List<ApiTerminalOutput>> ApiGetTerminals(ApiLocationInput locationInput);
        Task<List<FormlyType>> GetTerminalSetting(IdInput input);
        Task<ListResultOutput<ComboboxItemDto>> GetNotifyTypes();
        String GenerationKey();
    }
}