﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Connect.Terminal.Dtos;
using DinePlan.DineConnect.Connect.Terminal.Exporting;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.General;
using DinePlan.DineConnect.Helper;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Connect.Terminal
{
    public class TerminalAppService : DineConnectAppServiceBase, ITerminalAppService
    {
        private readonly ILocationAppService _locationAppService;
        private readonly ISyncAppService _syncAppService;
        private readonly ITerminalExporter _terminalExporter;
        private readonly IRepository<Master.Terminal> _terminalRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ITerminalManager _terminalManager;
        public TerminalAppService(IRepository<Master.Terminal> TerminalRepository
            , ITerminalExporter TerminalExporter
            , IUnitOfWorkManager unitOfWorkManager
            , ILocationAppService locationAppService
            , ISyncAppService syncAppService
            , ITerminalManager terminalManager
        )
        {
            _terminalRepository = TerminalRepository;
            _terminalExporter = TerminalExporter;
            _unitOfWorkManager = unitOfWorkManager;
            _locationAppService = locationAppService;
            _syncAppService = syncAppService;
            _terminalManager = terminalManager;
        }

        public async Task<PagedResultOutput<TerminalListDto>> GetTerminals(GetTerminalsInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                if (!input.Filter.IsNullOrEmpty()) input.Filter = Regex.Replace(input.Filter.Trim(), @"\s+", " ");

                var query = _terminalRepository
                    .GetAll()
                    .Where(i => i.IsDeleted == input.IsDeleted)
                    .WhereIf(!input.Filter.IsNullOrEmpty(),
                        p => p.Name.Contains(input.Filter) || p.TerminalCode.Contains(input.Filter));

                var allMyEnItems= SearchLocation(query, input.LocationGroup).OfType<Master.Terminal>();

                var allItemCount = allMyEnItems.Count();

                var sortMenuItems = allMyEnItems.AsQueryable()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList();

                var allListDtos = sortMenuItems.MapTo<List<TerminalListDto>>();

                return new PagedResultOutput<TerminalListDto>(
                    allItemCount,
                    allListDtos
                );
            }
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _terminalRepository.GetAll().OrderBy(a => a.Id).ToListAsync();

            return _terminalExporter.ExportToFile(allList.MapTo<List<TerminalListDto>>());
        }

        public async Task<CreateOrEditTerminalInput> GetTerminalForEdit(NullableIdInput input)
        {
            var output = new CreateOrEditTerminalInput();

            var editDto = new TerminalEditDto();

            if (input.Id.HasValue)
            {
                var tmn = await _terminalRepository.GetAsync(input.Id.Value);
                editDto = tmn.MapTo<TerminalEditDto>();

                if (string.IsNullOrEmpty(tmn.AutoDayCloseSettings))
                    editDto.CloseSetting = new AutoDayCloseSetting();
                else
                {
                    try
                    {
                        editDto.CloseSetting = JsonConvert.DeserializeObject<AutoDayCloseSetting>(tmn.AutoDayCloseSettings);
                    }
                    catch (Exception ex)
                    {
                    }
                }

                if (editDto.CloseSetting == null)
                    editDto.CloseSetting = new AutoDayCloseSetting();
            }

            output.Terminal = editDto;
            UpdateLocationAndNonLocationForEdit(editDto, output.LocationGroup);

            return output;
        }

        public async Task<int> CreateOrUpdateTerminal(CreateOrEditTerminalInput input)
        {
            var output = 0;
            if(input.Terminal.AutoDayClose && input.Terminal.IsDefault)
            {
                if (input.Terminal.CloseSetting == null || input.Terminal.CloseSetting.Notifications == null || !input.Terminal.CloseSetting.Notifications.Any())
                    throw new UserFriendlyException("Invalid AutoClose setting");
            }
            else
                input.Terminal.CloseSetting = null;
            input.Terminal.TicketTypeId = input.Terminal.TicketTypeId == 0 ? null : input.Terminal.TicketTypeId;

            if (input.Terminal.Id.HasValue)
            {
                await UpdateTerminal(input);
                output = input.Terminal.Id.Value;
            }
            else
                output = await CreateTerminal(input);
            
            await _syncAppService.UpdateSync(SyncConsts.TERMINAL);

            return output;
        }

        public string GenerationKey()
		{
            var guid = Guid.NewGuid();
            return Encode(guid);
        }
        public  string Encode(Guid guid)
        {
            var base64String = Convert.ToBase64String(guid.ToByteArray());
            base64String = base64String.Replace("/", "_").Replace("+", "-");
            return base64String.Substring(0, 22);
        }

        public async Task DeleteTerminal(NullableIdInput input)
        {
            if (input.Id.HasValue)
                await _terminalRepository.DeleteAsync(input.Id.Value);
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _terminalRepository.GetAsync(input.Id);
                item.IsDeleted = false;

                await _terminalRepository.UpdateAsync(item);
            }
        }

        public async Task<List<ApiTerminalOutput>> ApiGetTerminals(ApiLocationInput locationInput)
        {
            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                var orgId = await _locationAppService.GetOrgnizationIdForLocationId(locationInput.LocationId);
                if (orgId > 0) CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);

                var repoTerminals = await _terminalRepository.GetAll().Where(a => a.TenantId == locationInput.TenantId && !a.IsDeleted).ToListAsync();

                var allTerminalOutputs = new List<ApiTerminalOutput>();

                if (!string.IsNullOrEmpty(locationInput.Tag))
                {
                    var allLocations = await _locationAppService.GetLocationsByLocationGroupCode(locationInput.Tag);
                    if (allLocations != null && allLocations.Items.Any())
                        foreach (var terminal in repoTerminals)
                            if (!terminal.Group && !string.IsNullOrEmpty(terminal.Locations) && !terminal.LocationTag)
                            {
                                var scLocations =
                                    JsonConvert.DeserializeObject<List<SimpleLocationGroupDto>>(terminal.Locations);
                                if (scLocations.Any())
                                {
                                    foreach (var i in scLocations.Select(a => a.Id))
                                        if (allLocations.Items.Select(a => a.Value).Contains(i.ToString()))
                                            allTerminalOutputs.Add(new ApiTerminalOutput
                                            {
                                                Id = terminal.Id,
                                                Name = terminal.Name,
                                                TerminalCode = terminal.TerminalCode,
                                                Tid = terminal.Tid,
                                                SerialNumber = terminal.SerialNumber,
                                                IsDefault = terminal.IsDefault,
                                                AutoLogout = terminal.AutoLogout,
                                                Float = terminal.Float,
                                                Tolerance = terminal.Tolerance,
                                                IsEndWorkTimeMoneyShown = terminal.IsEndWorkTimeMoneyShown,
                                                IsCollectionTerminal = terminal.IsCollectionTerminal,
                                                Denominations = terminal.Denominations,
                                                WorkTimeTotalInFloat = terminal.WorkTimeTotalInFloat,
                                                Settings = terminal.Settings,
                                                TicketTypeId = terminal.TicketTypeId,
                                                AutoDayClose = terminal.AutoDayClose,
                                                AutoDayCloseSettings = terminal.AutoDayCloseSettings,
                                                AcceptTolerance = terminal.AcceptTolerance
                                            });
                                }
                                else
                                {
                                    allTerminalOutputs.Add(new ApiTerminalOutput
                                    {
                                        Id = terminal.Id,
                                        Name = terminal.Name,
                                        TerminalCode = terminal.TerminalCode,
                                        IsDefault = terminal.IsDefault,
                                        Tid = terminal.Tid,
                                        SerialNumber = terminal.SerialNumber,
                                        AutoLogout = terminal.AutoLogout,
                                        Float = terminal.Float,
                                        Tolerance = terminal.Tolerance,
                                        IsEndWorkTimeMoneyShown = terminal.IsEndWorkTimeMoneyShown,
                                        IsCollectionTerminal = terminal.IsCollectionTerminal,
                                        Denominations = terminal.Denominations,
                                        WorkTimeTotalInFloat = terminal.WorkTimeTotalInFloat,
                                        Settings = terminal.Settings,
                                        TicketTypeId = terminal.TicketTypeId,
                                        AutoDayClose = terminal.AutoDayClose,
                                        AutoDayCloseSettings = terminal.AutoDayCloseSettings,
                                        AcceptTolerance = terminal.AcceptTolerance

                                    });
                                }
                            }
                }
                else if (locationInput.LocationId > 0)
                {
                    foreach (var terminal in repoTerminals)
                        if (await _locationAppService.IsLocationExists(new CheckLocationInput
                        {
                            LocationId = locationInput.LocationId,
                            Locations = terminal.Locations,
                            Group = terminal.Group,
                            LocationTag = terminal.LocationTag,
                            NonLocations = terminal.NonLocations
                        }))
                            allTerminalOutputs.Add(new ApiTerminalOutput
                            {
                                Id = terminal.Id,
                                Name = terminal.Name,
                                TerminalCode = terminal.TerminalCode,
                                IsDefault = terminal.IsDefault,
                                AutoLogout = terminal.AutoLogout,
                                Float = terminal.Float,
                                Tid = terminal.Tid,
                                SerialNumber = terminal.SerialNumber,
                                Tolerance = terminal.Tolerance,
                                IsEndWorkTimeMoneyShown = terminal.IsEndWorkTimeMoneyShown,
                                IsCollectionTerminal = terminal.IsCollectionTerminal,
                                Denominations = terminal.Denominations,
                                WorkTimeTotalInFloat = terminal.WorkTimeTotalInFloat,
                                Settings = terminal.Settings,
                                TicketTypeId = terminal.TicketTypeId,
                                AutoDayClose = terminal.AutoDayClose,
                                AutoDayCloseSettings = terminal.AutoDayCloseSettings,
                                AcceptTolerance = terminal.AcceptTolerance

                            });
                }
                return new List<ApiTerminalOutput>(allTerminalOutputs);
            }
        }

        private async Task<int> CreateTerminal(CreateOrEditTerminalInput input)
        {
            var terminal = input.Terminal.MapTo<Master.Terminal>();

            if (input.Terminal.CloseSetting != null)
                terminal.AutoDayCloseSettings = JsonConvert.SerializeObject(input.Terminal.CloseSetting);
            UpdateLocationAndNonLocation(terminal, input.LocationGroup);
            CheckErrors(await _terminalManager.CreateSync(terminal));
            var item = await _terminalRepository.InsertOrUpdateAndGetIdAsync(terminal);
            return item;
        }

        private async Task UpdateTerminal(CreateOrEditTerminalInput input)
        {
            var dto = input.Terminal;
            var item = await _terminalRepository.GetAsync(input.Terminal.Id.Value);
            dto.MapTo(item);
            if(dto.CloseSetting != null)
                item.AutoDayCloseSettings = JsonConvert.SerializeObject(dto.CloseSetting);
            else
                item.AutoDayCloseSettings = null;
            UpdateLocationAndNonLocation(item, input.LocationGroup);
            CheckErrors(await _terminalManager.CreateSync(item));
        }
        public async Task<List<FormlyType>> GetTerminalSetting(IdInput input)
        {
            var myKey = TerminalSetting.TerminalSettings[input.Id];
            var output = TerminalSetting.AllTerminalSetting[myKey];
            return output;
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetNotifyTypes()
        {
            var returnList = new List<ComboboxItemDto>
            {
                new ComboboxItemDto()
                {
                    DisplayText = "5",
                    Value = "5"
                },
                new ComboboxItemDto()
                {
                    DisplayText = "15",
                    Value = "15"
                },
                new ComboboxItemDto()
                {
                    DisplayText = "30",
                    Value = "30"
                }
            };

            return new ListResultOutput<ComboboxItemDto>(returnList);
        }
    }
}