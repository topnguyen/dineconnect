﻿using Abp.AutoMapper;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Terminal.Dtos
{
    [AutoMapTo(typeof(Master.Terminal))]
    public class TerminalEditDto : ConnectEditDto
    {
        public TerminalEditDto()
        {
            CloseSetting = new AutoDayCloseSetting();
        }
        public int? Id { get; set; }
        public string Name { get; set; }
        public string TerminalCode { get; set; }
        public string Tid { get; set; }
        public bool IsDefault { get; set; }
        public bool AutoLogout { get; set; }
        public decimal Float { get; set; }
        public bool AcceptTolerance{ get; set; }

        public decimal Tolerance { get; set; }
        public bool IsEndWorkTimeMoneyShown { get; set; }
        public bool IsCollectionTerminal { get; set; }
        public bool WorkTimeTotalInFloat { get; set; }
        public string Denominations { get; set; }
        public int Oid { get; set; }
        public int? TicketTypeId { get; set; }
        public string Settings { get; set; }
        public bool AutoDayClose { get; set; }
        public AutoDayCloseSetting CloseSetting { get; set; }
        public string SerialNumber { get; set; }

    }
    
    public class AutoDayCloseSetting
    {
        public AutoDayCloseSetting()
        {
            Notifications = new List<Notification>();
        }
        public string CloseTime { get; set; }
        public List<Notification> Notifications { get; set; }
    }
    public class Notification
    {
        public string Time { get; set; }
    }
}