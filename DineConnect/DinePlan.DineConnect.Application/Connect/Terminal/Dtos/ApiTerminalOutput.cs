﻿namespace DinePlan.DineConnect.Connect.Terminal.Dtos
{
    public class ApiTerminalOutput
    {
        public int Id { get; set; }
        public string TerminalCode { get; set; }

        public string Name { get; set; }
        public string Tid { get; set; }

        public bool IsDefault { get; set; }
        public bool AutoLogout { get; set; }
        public decimal Float { get; set; }
        public decimal Tolerance { get; set; }
        public bool IsEndWorkTimeMoneyShown { get; set; }
        public bool IsCollectionTerminal { get; set; }
        public bool WorkTimeTotalInFloat { get; set; }
        public string Denominations { get; set; }
        public string Settings { get; set; }
        public int? TicketTypeId { get; set; }
        public bool AutoDayClose { get; set; }
        public string AutoDayCloseSettings { get; set; }
        public bool AcceptTolerance { get; set; }
        public string SerialNumber { get; set; }
    }
}
