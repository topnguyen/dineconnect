﻿using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;

namespace DinePlan.DineConnect.Connect.Terminal.Dtos
{
    [AutoMapFrom(typeof(Master.Terminal))]
    public class TerminalListDto : CreationAuditedEntity<int?>
    {
        public string Name { get; set; }
        public string TerminalCode { get; set; }
        public string Tid { get; set; }
        public bool IsDefault { get; set; }
        public bool AutoLogout { get; set; }
        public decimal Float { get; set; }
        public decimal Tolerance { get; set; }
        public bool IsEndWorkTimeMoneyShown { get; set; }
        public bool IsCollectionTerminal { get; set; }
        public bool WorkTimeTotalInFloat { get; set; }
        public string Denominations { get; set; }
    }
}