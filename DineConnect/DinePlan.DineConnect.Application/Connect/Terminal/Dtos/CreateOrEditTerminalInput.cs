﻿using DinePlan.DineConnect.Connect.Location;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Terminal.Dtos
{
    public class CreateOrEditTerminalInput
    {
        public CreateOrEditTerminalInput()
        {
            Terminal = new TerminalEditDto();
            LocationGroup = new LocationGroupDto();
        }

        [Required]
        public TerminalEditDto Terminal { get; set; }

        public LocationGroupDto LocationGroup { get; set; }
    }
}