﻿using DinePlan.DineConnect.Connect.Terminal.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Terminal.Exporting
{
    public class TerminalExporter : FileExporterBase, ITerminalExporter
    {
        public FileDto ExportToFile(List<TerminalListDto> dtos)
        {
            return CreateExcelPackage(
                "TerminalList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(base.L("Terminal"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        base.L("Id"),
                        base.L("Name"),
                        base.L("Code"),
                        base.L("IsDefault"),
                        base.L("AutoLogout"),
                        base.L("Float"),
                        base.L("IsEndWorkTimeMoneyShown"),
                        base.L("IsCollectionTerminal"),
                        base.L("WorkTimeTotalInFloat"),
                        base.L("Denominations"),
                        base.L("CreationTime")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.Name,
                        _ => _.TerminalCode,
                        _ => _.IsDefault,
                        _ => _.AutoLogout,
                        _ => _.Float,
                        _ => _.IsEndWorkTimeMoneyShown,
                        _ => _.IsCollectionTerminal,
                        _ => _.WorkTimeTotalInFloat,
                        _ => _.Denominations,
                        _ => _.CreationTime
                        );

                    var creationTimeColumn = sheet.Column(11);
                    creationTimeColumn.Style.Numberformat.Format = "yyyy-mm-dd hh:mm:ss";

                    for (var i = 1; i <= 12; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}