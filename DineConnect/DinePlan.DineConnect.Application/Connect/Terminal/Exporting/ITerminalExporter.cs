﻿using DinePlan.DineConnect.Connect.Terminal.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Terminal.Exporting
{
    public interface ITerminalExporter
    {
        FileDto ExportToFile(List<TerminalListDto> dtos);
    }
}