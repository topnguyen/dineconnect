﻿using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.SalesPromotionReport.Exporting;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.ProductMix.Dto;
using System.Linq;
using DinePlan.DineConnect.Connect.Transaction;
using System;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master.Dtos;
using System.Collections.Generic;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Connect.ProductMix.Exporting;
using Abp.Application.Services.Dto;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Report;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Configuration.Tenants;

namespace DinePlan.DineConnect.Connect.ProductMix
{
    public class ProductMixReportAppService : DineConnectAppServiceBase, IProductMixReportAppService
    {
        private readonly IConnectReportAppService _connectReportAppService;
        private readonly IRepository<Company> _companyRe;
        private readonly IRepository<Master.Department> _dRepo;
        private readonly ILocationAppService _locService;
        private readonly IRepository<Master.Location> _lRepo;
        private readonly IRepository<Transaction.Ticket> _ticketRepository;
        private readonly IRepository<Transaction.Order> _orderManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IReportBackgroundAppService _rbas;
        private readonly IBackgroundJobManager _bgm;
        private readonly IProductMixReportExporter _exporter;
        private readonly IRepository<Period.WorkPeriod> _workPeriod;
        private readonly IRepository<PaymentType> _payRe;
        private readonly IRepository<MenuItem> _menuItemRepository;
        private readonly IRepository<Category> _cateRepository;
        private readonly IReportBackgroundAppService _reportBackgroundService;
        private readonly IBackgroundJobManager _backgroundJobManager;
        private readonly ITenantSettingsAppService _tenantSettingsService;
        public ProductMixReportAppService(
            IRepository<Transaction.Ticket> ticketRepository,
            ILocationAppService locService,
            IRepository<Company> comR,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<Master.Department> drepo,
            IRepository<Master.Location> lRepo,
            IRepository<Transaction.Order> orderManager,
             IReportBackgroundAppService rbas,
             IBackgroundJobManager bgm,
             IProductMixReportExporter exporter,
             IRepository<Period.WorkPeriod> workPeriod,
             IRepository<PaymentType> payRe,
             IBackgroundJobManager backgroundJobManager,
             IReportBackgroundAppService reportBackgroundService,
             IRepository<MenuItem> menuItemRepository,
             IConnectReportAppService connectReportAppService,
             IRepository<Category> cateRepository,
             ITenantSettingsAppService tenantSettingsService
            )
        {
            _connectReportAppService = connectReportAppService;
            _ticketRepository = ticketRepository;
            _companyRe = comR;
            _unitOfWorkManager = unitOfWorkManager;
            _dRepo = drepo;
            _locService = locService;
            _lRepo = lRepo;
            _orderManager = orderManager;
            _rbas = rbas;
            _bgm = bgm;
            _exporter = exporter;
            _workPeriod = workPeriod;
            _payRe = payRe;
            _menuItemRepository = menuItemRepository;
            _reportBackgroundService = reportBackgroundService;
            _backgroundJobManager = backgroundJobManager;
            _cateRepository = cateRepository;
            _tenantSettingsService = tenantSettingsService;
        }

        [Obsolete]
        public async Task<PagedResultOutput<ProductMixDto>> GetProductMix(ProductMixInput input)
        {
            var resultOutput = new List<ProductMixDto>();
            var allOrder = GetAllOrdersByCondition(input);
            var listItem = allOrder.GroupBy(o => o.MenuItemId).ToList().Select(o => 
                new ProductMixDto {
                ProductName = o.First().MenuItem.Category.ProductGroup.Name,
                CategoryName = o.First().MenuItem.Category.Name,
                ItemCode = o.First().MenuItem.AliasCode,
                ItemName = o.First().MenuItem.Name,
                TotalAmount = o.Sum(od => od.Price * od.Quantity),
                Quantity = o.Sum(od => od.Quantity)
            }).ToList();            
            var dataAsQueryable = listItem.AsQueryable();
            if (!string.IsNullOrEmpty(input.DynamicFilter))
            {
                var jsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver =
                        new CamelCasePropertyNamesContractResolver()
                };
                var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                if (filRule?.Rules != null)
                {
                    dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                }
            }
            decimal totalAmount = dataAsQueryable.Sum(s => s.TotalAmount);
            var output = dataAsQueryable.GroupBy(x => x.CategoryName);
            foreach (var item in output)
            {
                decimal totalAmountCate = item.Sum(s => s.TotalAmount);
                foreach (var mix in item)
                {
                    mix.TotalSales = totalAmount;
                    mix.TotalCateSales = totalAmountCate;
                }
            }
            var countTotal = dataAsQueryable.Count();
            return new PagedResultOutput<ProductMixDto>(countTotal, dataAsQueryable.ToList());
        }
        public async Task<FileDto> GetProductMixToExport(ProductMixInput input)
        {
            var setting = await _tenantSettingsService.GetAllSettings();
            input.DateFormat = setting.Connect.SimpleDateFormat;
            input.DatetimeFormat = setting.Connect.DateTimeFormat;
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _reportBackgroundService.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.PRODUCTMIX,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                    {
                        var productMixInput = input;
                        productMixInput.UserId = AbpSession.UserId.Value;
                        productMixInput.TenantId = AbpSession.TenantId.Value;
                        await _backgroundJobManager.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.PRODUCTMIX,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value,
                            ProductMixInput = productMixInput
                        });
                    }
                }
                else
                {
                    return await _exporter.ExportProductMixReport(input, this);
                }
            }

            return null;
        }
        [Obsolete]
        public IQueryable<Order> GetAllOrdersByCondition(ProductMixInput input)
        {
            DateTime endDate = input.EndDate.AddDays(1);
            var tickets = _ticketRepository.GetAll();
            tickets = tickets.Where(a => a.LastPaymentTime >= input.StartDate && a.LastPaymentTime <= endDate);

            if (input.Location > 0)
            {
                tickets = tickets.Where(a => a.LocationId == input.Location);
            }
            else if (input.Locations != null && input.Locations.Any())
            {
                var locations = input.Locations.Select(a => a.Id).ToList();
                tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
                     && !input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                if (locations.Any()) tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true
                });
                tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }

            if (input.LocationGroup != null)
            {
                if (input.LocationGroup.NonLocations != null && input.LocationGroup.NonLocations.Any())
                {
                    var nonlocations = input.LocationGroup.NonLocations.Select(a => a.Id).ToList();
                    tickets = tickets.Where(a => !nonlocations.Contains(a.LocationId));
                }
            }

            else if (input.LocationGroup == null)
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = new List<SimpleLocationDto>(),
                    Group = false,
                    UserId = input.UserId
                });
                if (input.UserId > 0)
                    tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }

            var orders = tickets.SelectMany(a => a.Orders);

            if (input.Locations.Any())
            {
                var locationIds = input.Locations.Select(l => l.Id).ToList();
                orders = orders.Where(o => locationIds.Contains(o.Location_Id));
            }
            return orders;
        }
    }
}
