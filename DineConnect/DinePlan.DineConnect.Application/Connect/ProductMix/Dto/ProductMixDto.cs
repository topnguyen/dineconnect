﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ProductMix.Dto
{

    //public class AllProductMixOuput
    //{
    //    public List<ProductMixByCategory> ListItem { get; set; }
    //    public AllProductMixOuput()
    //    {
    //        ListItem = new List<ProductMixByCategory>();
    //    }
    //    public decimal Quantity => ListItem.Sum(s => s.Quantity);
    //    public decimal TotalAmount
    //    {
    //        get
    //        {
    //            return ListItem.Sum(s => s.TotalAmount);
    //        }
    //    }
    //    public decimal SalesPercent
    //    {
    //        get
    //        {
    //            return ListItem.Sum(s => s.SalesPercent);
    //        }
    //    }
    //    public decimal CatSalesPercent
    //    {
    //        get
    //        {
    //            return ListItem.Sum(s => s.CatSalesPercent);
    //        }
    //    }
    //}
    public class ProductMixByCategory
    {
        public string CategoryName { get; set; }
        public List<ProductMixDto> ListItem { get; set; }
        public decimal Quantity => ListItem.Sum(s => s.Quantity);
        public decimal TotalAmount => ListItem.Sum(s => s.TotalAmount);
        public decimal SalesPercent => ListItem.Sum(s => s.SalesPercent);
        public decimal CatSalesPercent => ListItem.Sum(s => s.CatSalesPercent);
        public ProductMixByCategory()
        {
            ListItem = new List<ProductMixDto>();
        }
    }
    public class ProductMixDto
    {
        public string ProductName { get; set; }
        public string CategoryName { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public decimal Quantity { get; set; }
        public decimal AvgUnitPrice
        {
            get
            {
                return Quantity > 0 ? TotalAmount / Quantity : 0;
            }
        }
        public decimal TotalAmount { get; set; }
        public decimal TotalSales { get; set; }
        public decimal SalesPercent
        {
            get
            {
                decimal total = 0;
                if (TotalSales != 0)
                {
                    total = (TotalAmount / TotalSales) * 100;
                }
                return total;
            }
        }
        //public decimal SalesPercentStr { get { return Math.Round(SalesPercent, 2)} }
        //public string TotalAmountStr { get; set; }
        public decimal TotalCateSales { get; set; }
        public decimal CatSalesPercent
        {
            get
            {
                decimal total = 0;
                if (TotalCateSales != 0)
                {
                    total = (TotalAmount / TotalCateSales) * 100;
                }
                return total;
            }
        }

    }

}
