﻿using DinePlan.DineConnect.Connect.ProductMix.Dto;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ProductMix.Exporting
{
    public interface IProductMixReportExporter
    {
        Task<FileDto> ExportProductMixReport(ProductMixInput input, IProductMixReportAppService appService);
    }
}
