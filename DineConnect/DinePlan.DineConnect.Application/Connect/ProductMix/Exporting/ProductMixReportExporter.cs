﻿using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.ProductMix.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ProductMix.Exporting
{
    public class ProductMixReportExporter : FileExporterBase, IProductMixReportExporter
    {
        public async Task<FileDto> ExportProductMixReport(ProductMixInput input, IProductMixReportAppService appService)
        {
            var file = new FileDto("ProductMix-" + DateTime.Now.ToString("yyyy-MMMM-dd") + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            input.MaxResultCount = 10000;
            using (var excelPackage = new ExcelPackage())
            {
                Color colorHeader = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
                Color colorBg= System.Drawing.ColorTranslator.FromHtml("#6600CC");
                var sheet = excelPackage.Workbook.Worksheets.Add(L("ProductMixReport"));
                sheet.OutLineApplyStyle = true;
                var formatDateSetting = input.DateFormat;
                var formatDatetimeSetting = input.DatetimeFormat;
                var numberFormat = ConnectConsts.ConnectConsts.NumberFormat;
                sheet.Cells[2, 1].Value = L("ProductMixReport");
                sheet.Cells[2, 1, 2, 9].Merge = true;
        
                
                sheet.Cells[2, 1, 2, 9].Style.Font.Bold = true;
                sheet.Cells[2, 1, 2, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                sheet.Cells[4, 1].Value = L("SalesPeriod");
                sheet.Cells[4, 2].Value = input.StartDate.ToString(formatDateSetting) +" "+ L("To") + " " + input.EndDate.ToString(formatDateSetting);
                sheet.Cells[5, 1].Value = L("GeneratedOn");
                sheet.Cells[5, 2].Value = DateTime.Now.ToString(formatDatetimeSetting);

                sheet.Cells[2, 1, 5, 9].Style.Font.Size = 10;

                var query = await appService.GetProductMix(input);

                var headers = new List<string>
                {
                    L("Group"),
                    L("CategoryName"),
                    L("ItemCode"),
                    L("ItemName"),
                    L("Quantity"),
                    L("AvgUnitPrice"),
                    L("TotalAmount"),
                    L("SalesPercent"),
                    L("CatSalesPercent"),
                };
                var col = 1;
                var row = 7;
                var allItemGroup = from d in query.Items
                                   group d by new {d.ProductName}
                                   into g
                                   select new {ProductName = g.Key.ProductName, Items = g};
                var objectSum = new List<ProductMixDto>(); 
                foreach (var groupPro in allItemGroup)
                {
                    AddHeaderWithColor(sheet, row++, colorBg, colorHeader, headers.ToArray());
                    sheet.Cells[row, col].Value = groupPro.ProductName;
                    col++;
                    var groupByCate = from d in groupPro.Items
                                      group d by new { d.CategoryName }
                                      into g
                                      select new { CategoryName = g.Key.CategoryName, Items = g };
                    foreach (var cate in groupByCate)
                    {
                        col = 2;
                        foreach (var pro in cate.Items)
                        {                            
                            sheet.Cells[row, col].Value = pro.CategoryName;
                            col++;
                            sheet.Cells[row, col].Value = pro.ItemCode;
                            col++;
                            sheet.Cells[row, col].Value = pro.ItemName;
                            col++;
                            sheet.Cells[row, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet.Cells[row, col].Value = pro.Quantity;
                            col++;
                            sheet.Cells[row, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet.Cells[row, col].Value = pro.AvgUnitPrice.ToString(numberFormat);
                            col++;
                            sheet.Cells[row, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet.Cells[row, col].Value = pro.TotalAmount.ToString(numberFormat);
                            col++;
                            sheet.Cells[row, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet.Cells[row, col].Value = pro.SalesPercent.ToString(numberFormat) + "%";
                            col++;
                            sheet.Cells[row, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                            sheet.Cells[row, col].Value = pro.CatSalesPercent.ToString(numberFormat) + "%";
                            row++;
                            col = 2;
                        }
                        var sumQuantity = cate.Items.Sum(s => s.Quantity);
                        var sumTotalAmount = cate.Items.Sum(s => s.TotalAmount);
                        var sumSalesPercent = cate.Items.Sum(s => s.SalesPercent);
                        var sumCatSales = cate.Items.Sum(s => s.CatSalesPercent);                       
                        objectSum.Add(new ProductMixDto
                        {
                            ProductName = cate.Items.First().ProductName,
                            CategoryName = cate.CategoryName,
                            Quantity = sumQuantity,
                            TotalAmount = sumTotalAmount,
                            TotalSales = sumSalesPercent
                        });
                    }                
                    col = 1; 
                    row += 2;
                }
                sheet.Cells[row,1].Value = "**************CATEGORY SUMMARY****************"; ;
                sheet.Cells[row, 1, row, 9].Merge = true;
                sheet.Cells[row, 1, row, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                row++;
                var headersSum = new List<string>
                {  
                    L("CategoryName"),
                    " ",                   
                    " ",
                    " ",
                    L("Quantity"),
                    " ",
                    L("TotalAmount"),
                    L("SalesPercent"),
                    " ",
                };
                AddHeaderWithColor(sheet, row++, colorBg, colorHeader, headersSum.ToArray());
                foreach(var item in objectSum)
                {
                    sheet.Cells[row, col].Value = item.CategoryName;
                    col++;
                    sheet.Cells[row, col].Value = "";
                    col++;
                    sheet.Cells[row, col].Value = "";
                    col++;
                    sheet.Cells[row, col].Value = "";
                    col++;
                    sheet.Cells[row, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet.Cells[row, col].Value = item.Quantity;
                    col++;
                    sheet.Cells[row, col].Value = "";
                    col++;
                    sheet.Cells[row, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet.Cells[row, col].Value = item.TotalAmount.ToString(numberFormat);
                    col++;
                    sheet.Cells[row, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet.Cells[row, col].Value = item.TotalSales.ToString(numberFormat) + "%";
                    col++;
                    sheet.Cells[row, col].Value = "";
                    row++;
                    col = 1;
                }
                var listTotalSum = new List<string> {L("Total") + " "+L("Sales")," "," "," "
                        ,query.Items.Sum(s=>s.Quantity).ToString("0"), " "
                        , query.Items.Sum(s=>s.TotalAmount).ToString(numberFormat)
                        , query.Items.Sum(s=>s.SalesPercent).ToString(numberFormat)+"%"
                        , " "};
                foreach (var total in listTotalSum)
                {
                    if (col == 1)
                    {
                        sheet.Cells[row, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    }
                    else
                    {
                        sheet.Cells[row, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    }              
                    sheet.Cells[row, col].Value = total;
                    col++;
                }
                row += 2;
                sheet.Cells[row, 1].Value = "**************GROUP SUMMARY****************"; ;
                sheet.Cells[row, 1, row, 9].Merge = true;
                sheet.Cells[row, 1, row, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                row++;
                var headersSumGroup = new List<string>
                {
                    L("Group"),
                    " ",
                    " ",
                    " ",
                    L("Quantity"),
                    " ",
                    L("TotalAmount"),
                    L("SalesPercent"),
                    " ",
                };
                AddHeaderWithColor(sheet, row++, colorBg, colorHeader, headersSumGroup.ToArray());
                col = 1;
                foreach (var item in allItemGroup)
                {
                    sheet.Cells[row, col].Value = item.ProductName;
                    col++;
                    sheet.Cells[row, col].Value = "";
                    col++;
                    sheet.Cells[row, col].Value = "";
                    col++;
                    sheet.Cells[row, col].Value = "";
                    col++;
                    sheet.Cells[row, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet.Cells[row, col].Value = item.Items.Sum(s=>s.Quantity);
                    col++;
                    sheet.Cells[row, col].Value = "";
                    col++;
                    sheet.Cells[row, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet.Cells[row, col].Value = item.Items.Sum(s => s.TotalAmount).ToString(numberFormat);
                    col++;
                    sheet.Cells[row, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet.Cells[row, col].Value = item.Items.Sum(s => s.SalesPercent).ToString(numberFormat) + "%";
                    col++;
                    sheet.Cells[row, col].Value = "";
                    row++;
                    col = 1;
                }
                var listTotalSumGroup = new List<string> {L("Total") + " "+L("Sales")," "," "," "
                        ,query.Items.Sum(s=>s.Quantity).ToString("0"), " "
                        , query.Items.Sum(s=>s.TotalAmount).ToString(numberFormat)
                        , query.Items.Sum(s=>s.SalesPercent).ToString(numberFormat)+"%"
                        , " "};
                foreach (var total in listTotalSum)
                {
                    if (col == 1)
                    {
                        sheet.Cells[row, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    }
                    else
                    {
                        sheet.Cells[row, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    }
                    sheet.Cells[row, col].Value = total;
                    col++;
                }
                row++;
                for (var i = 1; i <= 9; i++) sheet.Column(i).AutoFit();
                Save(excelPackage, file);
            }
            return ProcessFile(input, file);
        }        
    }
}
