﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.ProductMix.Dto;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ProductMix
{
    public interface IProductMixReportAppService : IApplicationService
    {
        Task<PagedResultOutput<ProductMixDto>> GetProductMix(ProductMixInput input);
        Task<FileDto> GetProductMixToExport(ProductMixInput input);
    }
}
