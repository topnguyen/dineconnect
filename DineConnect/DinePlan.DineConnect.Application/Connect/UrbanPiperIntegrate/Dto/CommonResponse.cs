﻿namespace DinePlan.DineConnect.Connect.UrbanPiperIntegrate.Dto
{
    public class CommonResponse<T>
    {
        public string status { get; set; }

        public string message { get; set; }

        public T reference { get; set; }

        public bool Success
        {
            get { return status == "success"; }
        }
    }
}