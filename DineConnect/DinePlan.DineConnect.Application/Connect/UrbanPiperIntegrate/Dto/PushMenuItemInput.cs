﻿using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.UrbanPiperIntegrate.Dto
{
    public class PushMenuItemInput
    {
        public virtual int? TenantId { get; set; }
        public virtual long? UserId { get; set; }
        public virtual bool FlushAll { get; set; }
        public virtual string Location{ get; set; }


    }

    public class UrbanPiperStore
    {
        public string city { get; set; } 
        public string name { get; set; } 
        public int min_pickup_time { get; set; } 
        public int min_delivery_time { get; set; } 
        public string contact_phone { get; set; } 
        public string ref_id { get; set; } 
        public bool hide_from_ui { get; set; } 
        public string address { get; set; } 
        public List<string> included_platforms { get; set; }
        public bool ordering_enabled { get; set; }
    }

    public class UrbanPiperStoreInput    {
        public List<UrbanPiperStore> stores { get; set; }

        public UrbanPiperStoreInput()
        {
            stores = new List<UrbanPiperStore>();
        }
    }
    public class UrbanPiperStoreChangeInput    {
        public string location_ref_id { get; set; } 
        public List<string> platforms { get; set; } 
        public string action { get; set; } 
    }


    public class UrbanItemUpdate
    {
        public UrbanItemUpdate()
        {
            item_ref_ids = new List<string>();
            option_ref_ids = new List<string>();
        }
        public string location_ref_id { get; set; }
        public List<string> item_ref_ids { get; set; } 
        public List<string> option_ref_ids { get; set; } 
        public string action { get; set; } 

    }
}