﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.UrbanPiperIntegrate.Dto
{
    public class UrbanPiperStatusInput
    {
        public string TicketNumber { get; set; }
        public string NewStatus { get; set; }
        public string Message { get; set; }
        public int TenantId { get; set; }


    }

    public class OrderStatusInput
    {
        public string new_status { get; set; }
        public string message { get; set; }
    }
}
