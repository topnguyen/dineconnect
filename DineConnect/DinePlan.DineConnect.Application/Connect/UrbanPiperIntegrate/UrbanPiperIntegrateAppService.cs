﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Abp.BackgroundJobs;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.UI;
using Castle.Core.Logging;
using DinePlan.DineConnect.Addons.Dto;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.OrderTags;
using DinePlan.DineConnect.Connect.UrbanPiperIntegrate.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Connect.UrbanPiperIntegrate
{
    public class UrbanPiperIntegrateAppService : DineConnectAppServiceBase, IUrbanPiperIntegrateAppService
    {
        private const string AuthenticationHeaderName = "Authorization";
        private readonly IBackgroundJobManager _backgroundJobManager;
        private readonly IRepository<Category> _categoryRepository;

        private readonly HttpClient _httpClient = new HttpClient(new HttpClientHandler())
        {
            Timeout = TimeSpan.FromSeconds(600)
        };

        private readonly IRepository<LocationMenuItem> _locMenuItem;
        private readonly IRepository<Master.Location> _locRepo;
        private readonly ILogger _logger;

        private readonly IRepository<MenuItemPortion> _menuItemPortionRepository;
        private readonly IRepository<MenuItem> _menuItemRepository;
        private readonly IRepository<OrderTagGroup> _orderGroupRepo;
        private readonly IRepository<PriceTag> _priceTagRepo;
        private readonly IScreenMenuAppService _screenAppService;

        private readonly ISettingManager _settingManager;

        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public UrbanPiperIntegrateAppService(IRepository<MenuItem> menuItemRepository, ISettingManager settingManager,
            IRepository<Category> categoryRepository, IRepository<OrderTagGroup> orderGroupRepo,
            IRepository<PriceTag> priceTagRepo, IRepository<Master.Location> locRepo,
            IRepository<LocationMenuItem> locMenuItem,
            IRepository<MenuItemPortion> menuItemPortionRepository, IScreenMenuAppService screenAppService,
            IUnitOfWorkManager unitOfWorkManager, IBackgroundJobManager backgroundJobManager, ILogger logger)
        {
            _menuItemRepository = menuItemRepository;
            _categoryRepository = categoryRepository;
            _priceTagRepo = priceTagRepo;
            _menuItemPortionRepository = menuItemPortionRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _backgroundJobManager = backgroundJobManager;
            _settingManager = settingManager;
            _orderGroupRepo = orderGroupRepo;
            _screenAppService = screenAppService;
            _locRepo = locRepo;
            _locMenuItem = locMenuItem;
            _logger = logger;
        }

        public async Task PushMenuItemsBackground(PushMenuItemInput input)
        {
            input.TenantId = AbpSession.TenantId;
            input.UserId = AbpSession.UserId;
            var uPriceTag = _settingManager.GetSettingValue(AppSettings.ConnectSettings.UrbanPiperPriceTag);

            Master.Location myLocation = null;

            var allInCluedPlatforms = new List<string>();

            if (!string.IsNullOrEmpty(input.Location))
            {
                myLocation = await _locRepo.FirstOrDefaultAsync(a => a.Code.Equals(input.Location));
                if (myLocation != null)
                    if (!string.IsNullOrEmpty(myLocation.AddOn))
                    {
                        var aAddOn = JsonConvert.DeserializeObject<AddOnOutput>(myLocation.AddOn);
                        if (aAddOn?.UrbanPiper != null) allInCluedPlatforms = aAddOn.UrbanPiper.EnabledPlatforms;
                    }
            }


            if (string.IsNullOrEmpty(uPriceTag)) throw new UserFriendlyException("No Price Tag Configured");

            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.MustHaveTenant, AppConsts.ConnectFilter))
            {
                var allPrice =
                    await _priceTagRepo.FirstOrDefaultAsync(a =>
                        a.TenantId == input.TenantId && a.Name.Equals(uPriceTag));

                if (allPrice == null) throw new UserFriendlyException("No Price Definition for " + uPriceTag);

                if (myLocation != null && AbpSession.TenantId != null)
                {
                    var allMenu = await _screenAppService.ApiScreenMenu(new ApiLocationInput
                    {
                        LocationId = myLocation.Id,
                        TenantId = AbpSession.TenantId.Value
                    });
                    _logger.Fatal(" --> PushItems <-- ");
                    if (allMenu != null && allMenu.Menu.Any())
                    {
                        _logger.Fatal(" --> PushScreenMenu <-- ");
                        await PushBasedOnScreenMenu(allMenu, input, allPrice, allInCluedPlatforms);
                        _logger.Fatal(" --> PushScreenMenu End <-- ");
                    }
                    else
                    {
                        _logger.Fatal(" --> PushMenu  <-- ");
                        await PushBasedOnMenu(input, allPrice, allInCluedPlatforms);
                        _logger.Fatal(" --> PushMenu End <-- ");
                    }
                }
                else
                {
                    _logger.Fatal(" --> PushMenu  <-- ");
                    await PushBasedOnMenu(input, allPrice, allInCluedPlatforms);
                    _logger.Fatal(" --> PushMenu End <-- ");
                }
            }
        }

        public async Task PushStores()
        {
            var input = new UrbanPiperStoreInput();
            var allLocations = _locRepo.GetAll().ToList();

            foreach (var allLocation in allLocations)
                if (!string.IsNullOrEmpty(allLocation.AddOn))
                {
                    var addOnOutput = JsonConvert.DeserializeObject<AddOnOutput>(allLocation.AddOn);

                    if (addOnOutput.UrbanPiper != null)
                    {
                        var store = new UrbanPiperStore
                        {
                            ref_id = allLocation.Code,
                            city = allLocation.City,
                            included_platforms = addOnOutput.UrbanPiper.EnabledPlatforms,
                            ordering_enabled = addOnOutput.UrbanPiper.Enabled,
                            address = allLocation.Address1,
                            name = allLocation.Name,
                            contact_phone = allLocation.PhoneNumber
                        };

                        input.stores.Add(store);
                    }
                }

            if (input.stores.Any())
            {
                var userId = _settingManager.GetSettingValue(AppSettings.ConnectSettings.UrbanPiperUserName);
                var uapiKey = _settingManager.GetSettingValue(AppSettings.ConnectSettings.UrbanPiperApiKey);
                var uBaseAddress = _settingManager.GetSettingValue(AppSettings.ConnectSettings.UrbanPiperUrl);

                var apiKey = $"apikey {userId}:{uapiKey}";
                var urlCatelogue = UPApiUrls.ManagingStores;
                var result =
                    await Request<CommonResponse<string>>(urlCatelogue, apiKey, input, "POST",
                        uBaseAddress);
                if (!result.Success) throw new UserFriendlyException("Request failed");
            }
        }

        public async Task UpdateStore(int locationId)
        {
            var myLocation = await _locRepo.GetAsync(locationId);
            var addOnOutput = JsonConvert.DeserializeObject<AddOnOutput>(myLocation.AddOn);
            if (addOnOutput.UrbanPiper != null)
            {
                var store = new UrbanPiperStoreChangeInput
                {
                    location_ref_id = myLocation.Code,
                    platforms = addOnOutput.UrbanPiper.EnabledPlatforms,
                    action = addOnOutput.UrbanPiper.Enabled ? "enable" : "disable"
                };

                var userId = _settingManager.GetSettingValue(AppSettings.ConnectSettings.UrbanPiperUserName);
                var uapiKey = _settingManager.GetSettingValue(AppSettings.ConnectSettings.UrbanPiperApiKey);
                var uBaseAddress = _settingManager.GetSettingValue(AppSettings.ConnectSettings.UrbanPiperUrl);

                _logger.Fatal(" --> UpdateStore <-- ");
                _logger.Fatal("UrbanPiperUser       : " + userId);
                _logger.Fatal("UrbanPiperApiKey     : " + uapiKey);
                _logger.Fatal("UrbanPiperUrl        : " + uBaseAddress);

                var apiKey = $"apikey {userId}:{uapiKey}";
                _logger.Fatal("Api Key       : " + apiKey);
                var urlCatelogue = UPApiUrls.UpdateStore;
                _logger.Fatal("urlCatelogue    : " + urlCatelogue);
                _logger.Fatal("Input    : " + JsonConvert.SerializeObject(store));

                var result =
                    await Request<CommonResponse<string>>(urlCatelogue, apiKey, store, "POST",
                        uBaseAddress);
                if (!result.Success) throw new UserFriendlyException("Request failed");

                _logger.Fatal(" --> UpdateStore End <-- ");
            }
        }

        #region Item

        public async Task UpdateItems(int locationId)
        {
            var myLocation = await _locRepo.GetAsync(locationId);
            var addOnOutput = JsonConvert.DeserializeObject<AddOnOutput>(myLocation.AddOn);
            if (addOnOutput.UrbanPiper != null)
            {
                var enableItems = new List<int>();

                var allLocationItems = _locMenuItem.GetAll().Where(a => a.LocationId.Equals(locationId)).ToList();

                if (allLocationItems.Any())
                    foreach (var locationMenuItem in allLocationItems)
                        enableItems.Add(locationMenuItem.MenuItemId);

                var allMenu = await _screenAppService.ApiScreenMenu(new ApiLocationInput
                {
                    LocationId = myLocation.Id,
                    TenantId = AbpSession.TenantId.Value
                });
                var allItems = new List<int>();

                if (allMenu != null && allMenu.Menu.Any())
                {
                    var myMenu = allMenu.Menu.First();
                    var allMyIds = myMenu.Categories.SelectMany(a => a.MenuItems).Select(a => a.MenuItemId);
                    foreach (var allMyId in allMyIds)
                        if (enableItems.Any())
                        {
                            if (!enableItems.Contains(allMyId)) allItems.Add(allMyId);
                        }
                        else
                        {
                            allItems.Add(allMyId);
                        }
                }
                else
                {
                    var allMyItems = _menuItemRepository.GetAll().Select(a => a.Id);
                    foreach (var allMyId in allMyItems)
                        if (enableItems.Any())
                        {
                            if (!enableItems.Contains(allMyId)) allItems.Add(allMyId);
                        }
                        else
                        {
                            allItems.Add(allMyId);
                        }
                }

                UrbanItemUpdate store;
                if (enableItems.Any())
                {
                    store = new UrbanItemUpdate
                    {
                        location_ref_id = myLocation.Code,
                        item_ref_ids = enableItems.ConvertAll(i => i.ToString()),
                        action = "enable"
                    };

                    var userId = _settingManager.GetSettingValue(AppSettings.ConnectSettings.UrbanPiperUserName);
                    var uapiKey = _settingManager.GetSettingValue(AppSettings.ConnectSettings.UrbanPiperApiKey);
                    var uBaseAddress = _settingManager.GetSettingValue(AppSettings.ConnectSettings.UrbanPiperUrl);

                    var apiKey = $"apikey {userId}:{uapiKey}";
                    var urlCatelogue = UPApiUrls.ManageItems;
                    var result =
                        await Request<CommonResponse<string>>(urlCatelogue, apiKey, store, "POST",
                            uBaseAddress);
                    if (!result.Success) throw new UserFriendlyException("Request failed");


                    store = new UrbanItemUpdate
                    {
                        location_ref_id = myLocation.Code,
                        item_ref_ids = allItems.ConvertAll(i => i.ToString()),
                        action = "disable"
                    };

                    userId = _settingManager.GetSettingValue(AppSettings.ConnectSettings.UrbanPiperUserName);
                    uapiKey = _settingManager.GetSettingValue(AppSettings.ConnectSettings.UrbanPiperApiKey);
                    uBaseAddress = _settingManager.GetSettingValue(AppSettings.ConnectSettings.UrbanPiperUrl);

                    apiKey = $"apikey {userId}:{uapiKey}";
                    urlCatelogue = UPApiUrls.ManageItems;
                    result =
                        await Request<CommonResponse<string>>(urlCatelogue, apiKey, store, "POST",
                            uBaseAddress);
                    if (!result.Success) throw new UserFriendlyException("Request failed");
                }
                else
                {
                    store = new UrbanItemUpdate
                    {
                        location_ref_id = myLocation.Code,
                        item_ref_ids = allItems.ConvertAll(i => i.ToString()),
                        action = "enable"
                    };

                    var userId = _settingManager.GetSettingValue(AppSettings.ConnectSettings.UrbanPiperUserName);
                    var uapiKey = _settingManager.GetSettingValue(AppSettings.ConnectSettings.UrbanPiperApiKey);
                    var uBaseAddress = _settingManager.GetSettingValue(AppSettings.ConnectSettings.UrbanPiperUrl);

                    var apiKey = $"apikey {userId}:{uapiKey}";
                    var urlCatelogue = UPApiUrls.ManageItems;
                    var result =
                        await Request<CommonResponse<string>>(urlCatelogue, apiKey, store, "POST",
                            uBaseAddress);
                    if (!result.Success) throw new UserFriendlyException("Request failed");
                }
            }
        }

        public async Task ChangeStatus(UrbanPiperStatusInput input)
        {
            var store = new OrderStatusInput
            {
                message = input.Message,
                new_status = input.NewStatus
            };

            var userId = await _settingManager.GetSettingValueForTenantAsync(AppSettings.ConnectSettings.UrbanPiperUserName, input.TenantId);
            var uapiKey = await _settingManager.GetSettingValueForTenantAsync(AppSettings.ConnectSettings.UrbanPiperApiKey, input.TenantId);
            var uBaseAddress = await _settingManager.GetSettingValueForTenantAsync(AppSettings.ConnectSettings.UrbanPiperUrl, input.TenantId);

            var apiKey = $"apikey {userId}:{uapiKey}";
            var urlRequest = $"{UPApiUrls.ManagingOrders}{input.TicketNumber}/status/";

            _logger.Fatal(">> ChangeStatus <<");
            _logger.Fatal("UserId : " + userId);
            _logger.Fatal("ApiKey : " + uapiKey);
            _logger.Fatal("UrlRequest : " + urlRequest);
            _logger.Fatal("BaseAddress : " + uBaseAddress);
            _logger.Fatal("Input : " + JsonConvert.SerializeObject(store).ToString());
            _logger.Fatal(">> ChangeStatus End<<");

            var result =
                await Request<CommonResponse<string>>(urlRequest, apiKey, store, "PUT",
                    uBaseAddress);
            if (!result.Success)
            {
                var myStatus = input.NewStatus;
                if (input.NewStatus.Equals("Food Ready") || input.NewStatus.Equals("Completed"))
                {
                    input.NewStatus = "Acknowledged";

                    result =
                        await Request<CommonResponse<string>>(urlRequest, apiKey, store, "PUT",
                            uBaseAddress);


                    input.NewStatus = myStatus;

                    result =
                        await Request<CommonResponse<string>>(urlRequest, apiKey, store, "PUT",
                            uBaseAddress);

                }
                
            }
        }

        #endregion

        #region Menu

        private async Task PushBasedOnMenu(PushMenuItemInput input, PriceTag allPrice,
            List<string> allInCludedPlatforms)
        {
            var userId = _settingManager.GetSettingValue(AppSettings.ConnectSettings.UrbanPiperUserName);
            var uapiKey = _settingManager.GetSettingValue(AppSettings.ConnectSettings.UrbanPiperApiKey);
            var uPriceTag = _settingManager.GetSettingValue(AppSettings.ConnectSettings.UrbanPiperPriceTag);
            var uBaseAddress = _settingManager.GetSettingValue(AppSettings.ConnectSettings.UrbanPiperUrl);


            var dto = new UbSyncRootDto();
            //1. Add categories
            var categories = await _categoryRepository.GetAll().Where(t => t.TenantId == input.TenantId)
                .ToListAsync();

            dto.categories = categories.Select(x => new UbCategory
            {
                ref_id = x.Id.ToString(),
                name = x.Name,
                description = x.Name,
                active = true,
                sort_order = 1
            }).ToList();


            var priceTagPortionItems = allPrice.PriceTagDefinitions.OrderBy(a => a.MenuPortionId);

            var allMenuItems = await _menuItemRepository.GetAll().Where(t => t.TenantId == input.TenantId)
                .ToListAsync();

            var allOdTags =
                _orderGroupRepo.GetAll().Where(a => a.TenantId == input.TenantId);

            var items = new List<UbItem>();
            foreach (var myMenuItem in allMenuItems)
            {
                var firstPortion = myMenuItem.Portions.First();
                var myItem = new UbItem
                {
                    ref_id = myMenuItem.Id.ToString(),
                    title = myMenuItem.Name,
                    food_type = myMenuItem.FoodType,
                    description = myMenuItem.ItemDescription,
                    included_platforms = allInCludedPlatforms,
                    category_ref_ids = new List<string> { myMenuItem.CategoryId?.ToString() }
                        .Where(i => !i.IsNullOrEmpty())
                        .ToList()
                };

                if (myMenuItem.Portions.Count() > 1)
                {
                    myItem.price = 0;

                    foreach (var myTag in myMenuItem.Portions)
                    {
                        var option = new UbOptions
                        {
                            ref_id = myTag.Id.ToString(),
                            title = myTag.Name,
                            price = myTag.Price,
                            description = myTag.Name,
                            opt_grp_ref_ids = new[] { "OG" + myMenuItem.Id }.ToList()
                        };
                        var portionInTag = priceTagPortionItems.LastOrDefault(a => a.MenuPortionId == myTag.Id);
                        if (portionInTag != null && portionInTag.Price > 0M)
                            option.price = portionInTag.Price;
                        else
                            option.price = myTag.Price;
                        dto.options.Add(option);
                    }

                    var group = new UbOptionGroup
                    {
                        ref_id = "OG" + myMenuItem.Id,
                        title = "Portion_" + myMenuItem.Id,
                        description = "Select Portion " + myItem.title,
                        max_selectable = 1,
                        min_selectable = 1,
                        sort_order = 0,
                        item_ref_ids = new[] { myMenuItem.Id.ToString() }.ToList()
                    };
                    dto.option_groups.Add(group);
                }
                else
                {
                    var portionInTag = priceTagPortionItems.LastOrDefault(a => a.MenuPortionId == firstPortion.Id);

                    if (portionInTag != null && portionInTag.Price > 0M)
                        myItem.price = portionInTag.Price;
                    else
                        myItem.price = firstPortion.Price;
                }


                myItem.tags = new UbTags { swiggy = new string[] { }, zomato = new string[] { } };
                if (!string.IsNullOrEmpty(myMenuItem.AddOns))
                {
                    var aAddOn = JsonConvert.DeserializeObject<AddOnOutput>(myMenuItem.AddOns);
                    if (aAddOn?.UrbanPiperMenuAddOn != null)
                    {
                        myItem.available = aAddOn.UrbanPiperMenuAddOn.Available;

                        if (aAddOn.UrbanPiperMenuAddOn.SwiggyTags != null)
                            myItem.tags.swiggy = aAddOn.UrbanPiperMenuAddOn.SwiggyTags.ToArray();

                        if (aAddOn.UrbanPiperMenuAddOn.ZomatoTags != null)
                            myItem.tags.zomato = aAddOn.UrbanPiperMenuAddOn.ZomatoTags.ToArray();
                        if (aAddOn.UrbanPiperMenuAddOn.EnabledPlatforms != null &&
                            aAddOn.UrbanPiperMenuAddOn.EnabledPlatforms.Any())
                            myItem.included_platforms = aAddOn.UrbanPiperMenuAddOn.EnabledPlatforms;

                        myItem.recommended = aAddOn.UrbanPiperMenuAddOn.Recommend;
                        if (myItem.recommended)
                            if (!string.IsNullOrEmpty(aAddOn.UrbanPiperMenuAddOn.ReplyFiles))
                            {
                                var allFi = JsonConvert.DeserializeObject<List<FileDto>>(aAddOn.UrbanPiperMenuAddOn
                                    .ReplyFiles);

                                if (allFi != null && allFi.Any()) myItem.img_url = allFi.First().FileSystemName;
                            }
                    }
                }

                items.Add(myItem);
            }

            dto.items = items;
            var urlUncatalogued = UPApiUrls.ManagingCatalogs;
            if (input.FlushAll)
                urlUncatalogued = urlUncatalogued + "-1/";
            else if (!string.IsNullOrEmpty(input.Location))
                urlUncatalogued = urlUncatalogued + input.Location + "/";
            else
                return;

            if (!dto.items.Any())
                return;


            var allCateIds = new HashSet<int>();

            foreach (var cate in dto.items.SelectMany(a => a.category_ref_ids))
                if (!string.IsNullOrEmpty(cate))
                    allCateIds.Add(Convert.ToInt32(cate));

            var allMenuIds = new HashSet<int>();

            foreach (var cate in dto.items.Select(a => a.ref_id))
                if (!string.IsNullOrEmpty(cate))
                    allMenuIds.Add(Convert.ToInt32(cate));


            var apiKey = $"apikey {userId}:{uapiKey}";
            const int batchSize = 100;
            var page = 0;
            var first = true;
            while (true)
            {
                if (first)
                {
                    dto.flush_categories = true;
                    dto.flush_items = true;
                    first = false;
                }
                else
                {
                    dto.flush_categories = false;
                    dto.flush_items = false;
                }


                var itemsToPush = items.Skip(page * batchSize).Take(batchSize).ToList();
                dto.items = itemsToPush;

                _logger.Fatal("Request ---" + page);
                _logger.Fatal(JsonConvert.SerializeObject(itemsToPush));

                var result =
                    await Request<CommonResponse<string>>(urlUncatalogued, apiKey, dto, "POST",
                        uBaseAddress);
                if (!result.Success) throw new UserFriendlyException("Request failed");
                Thread.Sleep(8000);
                if (itemsToPush.Count() < batchSize) break;
                page = page + 1;
            }
        }

        private async Task PushBasedOnScreenMenu(ApiScreenMenuOutput allMenu,
            PushMenuItemInput input, PriceTag allPrice,
            List<string> allInCludedPlatforms)
        {
            var priceTagPortionItems = allPrice.PriceTagDefinitions.OrderBy(a => a.MenuPortionId);

            var userId = _settingManager.GetSettingValue(AppSettings.ConnectSettings.UrbanPiperUserName);
            var uapiKey = _settingManager.GetSettingValue(AppSettings.ConnectSettings.UrbanPiperApiKey);
            var uPriceTag = _settingManager.GetSettingValue(AppSettings.ConnectSettings.UrbanPiperPriceTag);
            var uBaseAddress = _settingManager.GetSettingValue(AppSettings.ConnectSettings.UrbanPiperUrl);


            _logger.Fatal(" --> UrbanPiper <-- ");
            _logger.Fatal("UrbanPiperUser       : " + userId);
            _logger.Fatal("UrbanPiperApiKey     : " + uapiKey);
            _logger.Fatal("UrbanPiperPriceTag   : " + uPriceTag);
            _logger.Fatal("UrbanPiperUrl        : " + uBaseAddress);
            _logger.Fatal(" --> UrbanPiper End <-- ");

            var dto = new UbSyncRootDto();
            var firstMenu = allMenu.Menu.Last();

            foreach (var category in firstMenu.Categories)
            foreach (var x in category.MenuItems)
            {
                var mItem = await _menuItemRepository.FirstOrDefaultAsync(a => a.Id == x.MenuItemId);

                if (mItem == null)
                    continue;

                if (mItem.Category != null)
                {
                    var retrieveCategory =
                        dto.categories.LastOrDefault(a => a.ref_id.Equals(mItem.Category.Id.ToString()));
                    if (retrieveCategory == null)
                        dto.categories.Add(new UbCategory
                        {
                            ref_id = mItem.Category.Id.ToString(),
                            name = mItem.Category.Name,
                            description = mItem.Category.Name,
                            active = true,
                            sort_order = category.SortOrder,
                            img_url = GetFileName(category.Files)
                        });
                }

                if (mItem.Category != null)
                {
                    var myItem = new UbItem
                    {
                        ref_id = x.Id.ToString(),
                        title = x.Name,
                        food_type = mItem.FoodType,
                        description = mItem.ItemDescription,
                        included_platforms = allInCludedPlatforms,
                        price = mItem.Portions.First().Price,
                        category_ref_ids = new List<string> { mItem.Category.Id.ToString() }.ToList(),
                        tags = new UbTags { swiggy = new string[] { }, zomato = new string[] { } }
                    };
              
                    if (allPrice.PriceTagDefinitions != null && allPrice.PriceTagDefinitions.Any())
                    {
                        var myPrice = allPrice.PriceTagDefinitions.LastOrDefault(a =>
                            a.MenuPortionId == mItem.Portions.First().Id && a.PriceTag != null &&
                            a.PriceTag.Name != null &&
                            a.PriceTag.Name.Equals(uPriceTag));

                        if (myPrice != null && myPrice.Price > 0) myItem.price = myPrice.Price;
                    }


                    if (mItem.Portions.Count() > 1)
                    {
                        myItem.price = 0;
                        _logger.Fatal(" --> Portions Inside Available <-- ");
                    
                        foreach (var myTag in mItem.Portions)
                        {
                            var option = new UbOptions
                            {
                                ref_id = myTag.Id.ToString(),
                                title = myTag.Name,
                                price = myTag.Price,
                                description = myTag.Name,
                                opt_grp_ref_ids = new[] { "OG" + x.Id }.ToList()
                            };
                            if (priceTagPortionItems != null)
                            {
                                var portionInTag = priceTagPortionItems.LastOrDefault(a => a.MenuPortionId == myTag.Id);
                                if (portionInTag != null && portionInTag.Price > 0M)
                                    option.price = portionInTag.Price;
                                else
                                    option.price = myTag.Price;
                            }

                            dto.options.Add(option);
                            _logger.Fatal(" --> Added to Option ");
                        }

                        var group = new UbOptionGroup
                        {
                            ref_id = "OG" + x.Id,
                            title = "Portion_" + mItem.Id,
                            description = "Select Portion " + myItem.title,
                            max_selectable = 1,
                            min_selectable = 1,
                            sort_order = 0,
                            item_ref_ids = new[] { x.Id.ToString() }.ToList()
                        };
                        dto.option_groups.Add(@group);
                    }
                    else
                    {
                        var portionInTag =
                            priceTagPortionItems.LastOrDefault(a => a.MenuPortionId == mItem.Portions.First().Id);

                        if (portionInTag != null && portionInTag.Price > 0M)
                            myItem.price = portionInTag.Price;
                        else
                            myItem.price = mItem.Portions.First().Price;
                    }


               

                    if (!string.IsNullOrEmpty(mItem.AddOns))
                    {
                        var aAddOn = JsonConvert.DeserializeObject<AddOnOutput>(mItem.AddOns);
                        if (aAddOn?.UrbanPiperMenuAddOn != null)
                        {
                            myItem.available = aAddOn.UrbanPiperMenuAddOn.Available;

                            if (aAddOn.UrbanPiperMenuAddOn.SwiggyTags != null)
                                myItem.tags.swiggy = aAddOn.UrbanPiperMenuAddOn.SwiggyTags.ToArray();

                            if (aAddOn.UrbanPiperMenuAddOn.ZomatoTags != null)
                                myItem.tags.zomato = aAddOn.UrbanPiperMenuAddOn.ZomatoTags.ToArray();
                            if (aAddOn.UrbanPiperMenuAddOn.EnabledPlatforms != null &&
                                aAddOn.UrbanPiperMenuAddOn.EnabledPlatforms.Any())
                                myItem.included_platforms = aAddOn.UrbanPiperMenuAddOn.EnabledPlatforms;

                            myItem.recommended = aAddOn.UrbanPiperMenuAddOn.Recommend;
                            if (!string.IsNullOrEmpty(x.Files))
                            {
                                var allFi = JsonConvert.DeserializeObject<List<FileDto>>(x.Files);
                                if (allFi != null && allFi.Any()) myItem.img_url = allFi.First().FileSystemName;
                            }
                        }
                    }

                    dto.items.Add(myItem);
                }
            }

            var urlCatalogue = UPApiUrls.ManagingCatalogs;
            if (input.FlushAll)
                urlCatalogue = urlCatalogue + "-1/";
            else if (!string.IsNullOrEmpty(input.Location))
                urlCatalogue = urlCatalogue + input.Location + "/";
            else
                return;

            if (!dto.items.Any())
                return;

            var apiKey = $"apikey {userId}:{uapiKey}";
            const int batchSize = 100;
            var page = 0;
            var first = true;

            var allItems = dto.items;

            while (true)
            {
                var itemsToPush = allItems.Skip(page * batchSize).Take(batchSize).ToList();
                if (first)
                {
                    dto.flush_categories = true;
                    dto.flush_items = true;
                    first = false;
                }
                else
                {
                    dto.flush_categories = false;
                    dto.flush_items = false;
                }

                _logger.Fatal("Request ---" + page);
                _logger.Fatal("Location Id ---" + input.Location);
                _logger.Fatal(JsonConvert.SerializeObject(dto));

                dto.items = itemsToPush;
                if (itemsToPush.Any())
                {
                    var result =
                        await Request<CommonResponse<string>>(urlCatalogue, apiKey, dto, "POST",
                            uBaseAddress);
                    if (!result.Success) throw new UserFriendlyException("Request failed");
                    Thread.Sleep(8000);
                }
                else
                {
                    break;
                }

                if (allItems.Count() < batchSize) break;
                page += 1;
            }
        }

        private string GetFileName(string categoryFiles)
        {
            if (!string.IsNullOrEmpty(categoryFiles))
            {
                var allFi = JsonConvert.DeserializeObject<List<FileDto>>(categoryFiles);
                if (allFi != null && allFi.Any()) return allFi.First().FileSystemName;
            }

            return "";
        }

        private void AddOrderTagOption(IEnumerable<int> allCateTags, List<int> allAddedGroups,
            UbSyncRootDto rootDto, HashSet<int> cateIds, HashSet<int> menuIds)
        {
            var allIds = new List<string>();

            var add = true;
            foreach (var myTagMap in allCateTags)
            {
                if (allAddedGroups.Contains(myTagMap))
                    add = false;


                var myTag = _orderGroupRepo.Get(myTagMap);

                if (myTag == null)
                    add = false;

                if (cateIds == null && menuIds == null)
                {
                    allIds = rootDto.items.Select(a => a.ref_id).ToList();
                    add = true;
                }

                if (menuIds != null && menuIds.Any())
                {
                    var allCates = myTag.Maps.Where(a => a.MenuItemId != null && menuIds.Contains(a.MenuItemId.Value));
                    if (allCates.Any()) allIds = (List<string>)allCates.Select(a => a.MenuItemId.ToString());
                }


                if (add && allIds.Any())
                {
                    allAddedGroups.Add(myTagMap);

                    var group = new UbOptionGroup
                    {
                        ref_id = myTag.Id.ToString(),
                        title = myTag.Name,
                        description = myTag.Name,
                        max_selectable = myTag.MaxSelectedItems,
                        min_selectable = myTag.MinSelectedItems,
                        sort_order = myTag.SortOrder,
                        item_ref_ids = allIds
                    };

                    rootDto.option_groups.Add(group);

                    foreach (var orderTag in myTag.Tags)
                    {
                        var option = new UbOptions
                        {
                            ref_id = orderTag.Id.ToString(),
                            title = orderTag.Name,
                            price = orderTag.Price,
                            description = orderTag.Name,
                            opt_grp_ref_ids = new[] { group.ref_id }.ToList()
                        };

                        rootDto.options.Add(option);
                    }
                }
            }
        }

        #endregion


        #region Push API

        public async Task<T> Request<T>(string url, string upApiKey, object requestObj, string method = "POST",
            string baseAddress = "https://pos-int.urbanpiper.com")
        {
            try
            {
                var request = new HttpRequestMessage(new HttpMethod(method), CreateFullUrl(url, baseAddress));
                lock (Guid.NewGuid().ToString())
                {
                    request.Headers.Add(AuthenticationHeaderName, upApiKey);
                    request.Headers.Add("Cache-Control", "no-cache");
                    request.Content = new StringContent(JsonConvert.SerializeObject(requestObj), Encoding.UTF8,
                        MimeTypeNames.ApplicationJson);
                    _httpClient.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue(MimeTypeNames.ApplicationJson));
                }

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                var response = await _httpClient.SendAsync(request);

                var success = await response.Content.ReadAsStringAsync();
                var exception = JsonConvert.DeserializeObject<RequestRespomse>(success);
                if (!response.IsSuccessStatusCode)
                {
                    _logger.Fatal(">> Error in Request <<");
                    _logger.Fatal("Reason Code : " + response.ReasonPhrase);
                    _logger.Fatal("Status Code : " + response.StatusCode);
                    HttpContent requestContent = response.Content;
                    string jsonContent = await requestContent.ReadAsStringAsync();
                    _logger.Fatal("Response Error: " + jsonContent);
                    _logger.Fatal(">> Error in Request End <<");
                }

                T result;
                lock (Guid.NewGuid().ToString())
                {
                    result = JsonConvert.DeserializeObject<T>(success);
                }

                return result;
            }
            catch (Exception ex)
            {
                _logger.Fatal(">> Error in Request Exception <<");
                _logger.Fatal(ex.StackTrace);
                _logger.Fatal(">> Error in Request Exception End <<");
                throw new UserFriendlyException(ex.Message);
            }
        }

        private Uri CreateFullUrl(string url, string baseAddress)
        {
            return new Uri(baseAddress.EnsureEndsWith('/') + url);
        }

        #endregion
    }
}