﻿namespace DinePlan.DineConnect.Connect.UrbanPiperIntegrate
{
    public class UPApiUrls
    {
        public const string ManagingStores = "external/api/v1/stores/";
        public const string UpdateStore = "hub/api/v1/location/";
        public const string ManageItems = "hub/api/v1/items/";


        public const string ManagingCatalogs = "external/api/v1/inventory/locations/";
        public const string ManagingOrders = "external/api/v1/orders/";
    }
}