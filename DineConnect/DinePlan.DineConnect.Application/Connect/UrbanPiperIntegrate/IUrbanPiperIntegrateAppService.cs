﻿using Abp.Application.Services;
using DinePlan.DineConnect.Connect.UrbanPiperIntegrate.Dto;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.UrbanPiperIntegrate
{
    public interface IUrbanPiperIntegrateAppService : IApplicationService
    {
        Task PushMenuItemsBackground(PushMenuItemInput input);
        Task PushStores();

        Task UpdateStore(int locationId);

        Task UpdateItems(int locationId);

        Task ChangeStatus(UrbanPiperStatusInput input);

    }
}