﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.DayClose.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;

namespace DinePlan.DineConnect.Connect.DayClose
{
    public interface IDayCloseAppService : IApplicationService
    {
        Task<DayCloseDto> GetDayClose(IdInput input);
        Task<AccountDateDto> GetDayCloseStatus(IdInput input);
        Task<CurrentDateDto> SetCloseDay(DayCloseDto input);
    }
}
