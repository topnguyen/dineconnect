﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.UI;
using DinePlan.DineConnect.Connect.DayClose.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.House;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.DayClose.Implementation
{
    public class DayCloseAppService : DineConnectAppServiceBase, IDayCloseAppService
    {
        private readonly IConnectReportAppService _connectReportAppService;
        private readonly IRepository<Master.Location> _locationRepo;
        private readonly IRepository<MenuItemWastage> _menuitemwastageRepo;
        private readonly IRepository<ClosingStock> _closingstockRepo;

        public DayCloseAppService(IRepository<Master.Location> locationRepo, IRepository<MenuItemWastage> menuitemwastageRepo,
            IRepository<ClosingStock> closingstockRepo,
            IConnectReportAppService connectReportAppService)
        {
            _connectReportAppService = connectReportAppService;
            _locationRepo = locationRepo;
            _closingstockRepo = closingstockRepo;
            _menuitemwastageRepo = menuitemwastageRepo;
        }

        public async Task<DayCloseDto> GetDayClose(IdInput input)
        {
            var loc = await _locationRepo.GetAll().FirstOrDefaultAsync(l => l.Id == input.Id);

            if (loc == null)
            {
                throw new UserFriendlyException(L("Location") + ' ' + L("NotExist"));
            }

            DateTime lastClosedDay;

            if (loc.HouseTransactionDate == null)
            {
                lastClosedDay = DateTime.Today.AddDays(-1);
            }
            else
            {
                lastClosedDay = (DateTime)loc.HouseTransactionDate;
            }

            var remarks = "";
            var skipTransactionDays = false;

            if (lastClosedDay < DateTime.Today.AddDays(-1))
            {
                var locationinput = await _locationRepo.GetAll().Where(t => t.Id == input.Id).ToListAsync();

                var locationlist = locationinput.MapTo<List<LocationListDto>>();

                var initialLastClosedDay = lastClosedDay;
                do
                {
                    var nextClosedDay = lastClosedDay.AddDays(1);
                    if (loc.IsDayCloseRecursiveUptoDateAllowed == false)
                    {
                        break;
                    }
                    var sales = await _connectReportAppService.GetTickets(
                        new GetTicketInput
                        {
                            StartDate = lastClosedDay,
                            EndDate = nextClosedDay,
                            Locations = locationlist.MapTo<List<SimpleLocationDto>>()
                        }, true);

                    if (sales.DashBoardDto.TotalAmount == 0)
                    {
                        //	Check Any Menu Wastage Available
                        var mas = await _menuitemwastageRepo.GetAll()
                            .Where(
                                t =>
                                    DbFunctions.TruncateTime(t.SalesDate) == DbFunctions.TruncateTime(lastClosedDay) &&
                                    t.LocationRefId == input.Id && t.AdjustmentRefId == null).ToListAsync();
                        if (mas.Count == 0)
                        {
                            //	Check Any Menu Wastage Available
                            var rsClosingStock =
                                await
                                    _closingstockRepo.GetAllListAsync(
                                        t =>
                                            DbFunctions.TruncateTime(t.StockDate) ==
                                            DbFunctions.TruncateTime(lastClosedDay));
                            if (rsClosingStock.Count == 0)
                            {
                                lastClosedDay = nextClosedDay;
                            }
                            else
                            {
                                break;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                    else
                    {
                        break;
                    }
                } while (lastClosedDay < DateTime.Today.AddDays(-1));

                if (lastClosedDay.Subtract(initialLastClosedDay).Days > 0)
                {
                    remarks = L("DayCloseRemarks", initialLastClosedDay.ToString("D"),
                        lastClosedDay.Subtract(initialLastClosedDay).Days);
                    skipTransactionDays = true;
                }
            }

            return new DayCloseDto
            {
                LocationRefId = input.Id,
                TransactionDate = lastClosedDay,
                Remarks = remarks,
                SkipTransactionDays = skipTransactionDays
            };
        }

        public async Task<AccountDateDto> GetDayCloseStatus(IdInput input)
        {
            var loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == input.Id);

            DateTime lastDate;

            if (loc.HouseTransactionDate == null)
            {
                var item = await _locationRepo.GetAsync(input.Id);
                item.HouseTransactionDate = DateTime.Today.AddDays(-1);
                lastDate = item.HouseTransactionDate.Value;
                await _locationRepo.InsertOrUpdateAndGetIdAsync(item);
            }
            else
            {
                lastDate = (DateTime)loc.HouseTransactionDate;
            }
            lastDate = lastDate.Date;
            long? currentUserId = AbpSession.UserId;
            if (lastDate == DateTime.Today || (lastDate.AddDays(1) == DateTime.Today && DateTime.Now.Hour < 6))
            {
                return new AccountDateDto {Id = 1, AccountDate = lastDate, DoesDayCloseRunInBackGround = loc.DoesDayCloseRunInBackGround, BackGroundDayCloseStartTime = loc.BackGroundStartTime, LastUpdateTime = DateTime.Now, CurrentUserId = currentUserId};
            }
            return new AccountDateDto {Id = 0, AccountDate = lastDate, DoesDayCloseRunInBackGround = loc.DoesDayCloseRunInBackGround, BackGroundDayCloseStartTime = loc.BackGroundStartTime, LastUpdateTime = DateTime.Now, CurrentUserId = currentUserId };
        }

        public async Task<CurrentDateDto> SetCloseDay(DayCloseDto input)
        {
            bool sameDayCloseAllowed = false;
            if (PermissionChecker.IsGranted("Pages.Tenant.House.Transaction.DayClose.SameDayClose"))
            {
                sameDayCloseAllowed = true;
            }

            if (input.TransactionDate.Date == DateTime.Today && sameDayCloseAllowed == false)
            //@@Pending Upto Synchranize With Sales BackUp
            {
                throw new UserFriendlyException(L("YouCanNotCloseDateForTodayAsOfNow"));
            }

            if (input.TransactionDate.Date == DateTime.Today && sameDayCloseAllowed == true)
            //@@Pending Upto Synchranize With Sales BackUp
            {
                if (DateTime.Now.Hour < 15)
                //@@Pending Upto Synchranize With Sales BackUp
                {
                    throw new UserFriendlyException(L("YouCanNotCloseDateForTodayAsOfNow"));
                }
            }

            var dayToBeAssignedAsTransactionDate = input.TransactionDate.Date.AddDays(1);

            if (dayToBeAssignedAsTransactionDate > DateTime.Today.AddDays(1))
            {
                throw new UserFriendlyException(L("YouCanNotCloseDateForFuture"));
            }

            var salesAccountDate = input.TransactionDate.Date;

            var locations = await _locationRepo.GetAll().Where(t => t.Id == input.LocationRefId).ToListAsync();

            var locList = locations.MapTo<List<LocationListDto>>();

            //var sales = await _connectReportAppService.GetTickets(
            //    new GetTicketInput
            //    {
            //        StartDate = salesAccountDate,
            //        EndDate = salesAccountDate,
            //        Locations = locList.MapTo<List<SimpleLocationDto>>()
            //    }, true);

            //var salesAmount = sales.DashBoardDto.TotalAmount;

            var item = await _locationRepo.GetAsync(input.LocationRefId);

            item.HouseTransactionDate = dayToBeAssignedAsTransactionDate;

            await _locationRepo.InsertOrUpdateAndGetIdAsync(item);

            return new CurrentDateDto { CurrentDate = dayToBeAssignedAsTransactionDate };
        }
    }
}