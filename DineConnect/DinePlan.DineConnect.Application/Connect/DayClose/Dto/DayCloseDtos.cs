﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;

namespace DinePlan.DineConnect.Connect.DayClose.Dto
{
    public class DayCloseDto
    {
        public int LocationRefId { get; set; }
        public DateTime TransactionDate { get; set; }
        public bool LedgerAffect { get; set; }
        public string Remarks { get; set; }

        public bool ResetFlag { get; set; }

        public List<MaterialSalesWithWipeOutCloseDayDto> DayCloseWastageData;
        public bool SkipTransactionDays { get; set; }

        public bool RunInBackGround { get; set; }
        public bool NegativeStockAccepted { get; set; }
        public bool DoesDayCloseRequiredInBackGround { get; set; }
        public int? UserId { get; set; } 
        public int TenantId { get; set; }
        public AllItemSalesDto AllItemSalesDto { get; set; }
        public TicketStatsDto TicketStatsDto { get; set; }
        public List<UnitConversionListDto> UnitConversionListDtos { get; set; }
        public List<DayCloseSyncReportOutputDto> DayCloseSyncReportOutputDtos { get; set; }
    }

    public class DayCloseMaterialDto
    {
        public int LocationRefId { get; set; }
        public int MaterialRefId { get; set; }
        public List<int> MaterialRefIds { get; set; }
        public DateTime TransactionStartDate { get; set; }
        public DateTime TransactionEndDate { get; set; }
    }

    public class DayCloseResultDto 
    {
        public bool DayCloseSuccessFlag { get; set; }
        public List<MaterialSalesWithWipeOutCloseDayDto> SalesData;

        public List<MaterialSalesWithWipeOutCloseDayDto> CompData;

        public List<MaterialSalesWithWipeOutCloseDayDto> MenuWastageData;

        public List<MenuItemWastageDetailEditDto> MenuWastageConsolidated;

        public bool SalesExistFlag { get; set; }
        public decimal SalesAmount { get; set; }
        public string SalesMessage { get; set; }
        public bool SalesOverRideFlag { get; set; }

        public bool LastDayCloseErrorFlag { get; set; }
        public List<DayCloseSyncReportOutputDto> DayCloseSyncErrorList { get; set; }
        public bool DoesRunInBackGround { get; set; }
   }

   public class DayCloseSyncReportDto
   {
      public int LocationRefId { get; set; }
      public DateTime StartDate { get; set; }
      public DateTime EndDate { get; set; }
   }

   public class DayCloseSyncReportOutputDto
   {
      public DateTime AccountDate { get; set; }
      public DateTime DayCloseTime { get; set; }
      public bool SyncErrorFlag { get; set; }
      public DashboardTicketDto DayCloseSalesDashBoard { get; set; }
      public DashboardTicketDto ActualSalesDashBoard { get; set; }
   }

    public class SalesMissingIntoLedgerDto
    {
        public int LocationRefId { get; set; }
        public DateTime SalesDate { get; set; }
        public bool LedgerAffect { get; set; }
        public string Remarks { get; set; }

        public bool ResetFlag { get; set; }

        public List<MaterialSalesWithWipeOutCloseDayDto> DayCloseWastageData;
        public bool SkipTransactionDays { get; set; }
    }

}
