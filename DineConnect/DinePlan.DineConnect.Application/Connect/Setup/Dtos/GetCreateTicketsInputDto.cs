﻿using System;

namespace DinePlan.DineConnect.Connect.Setup.Dtos
{
    public class GetCreateTicketsInputDto
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int TicketCount { get; set; }

    }

    public class GetCreateInwardInputDto
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int LocationRefId { get; set; }

    }
}
