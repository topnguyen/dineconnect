﻿using System.Threading.Tasks;
using Abp.Application.Services;
using DinePlan.DineConnect.Connect.Setup.Dtos;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Configuration.Host.Dto;

namespace DinePlan.DineConnect.Connect.Setup
{
    public interface ISetupAppService : IApplicationService
    {
        void ClearTransaction();
        void ClearSales();
        void ClearInventoryTransaction();

        void ClearAll();
        void ClearMenu();
        void ClearMaterials();

        void ClearAllHouseTransactions();

        Task SeedSampleData(GetCreateTicketsInputDto ticketDto);
        Task SeedAll();

        Task SyncConnect(GeneralSettingsEditDto input);

        Task UpdateMenuAndOrders();

        Task SeedTickAll(IdInput input);

        void ClearTickTransaction();

        

        

    }
}