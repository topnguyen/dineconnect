﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Connect.Setting;
using DinePlan.DineConnect.Features;

namespace DinePlan.DineConnect.Connect.Setup.Implementation
{
    public class SeedAppService : DineConnectAppServiceBase, ISeedAppService
    {
        private readonly IRepository<ProgramSettingValue> _pRepository;
        private readonly IRepository<ProgramSettingTemplate> _ptRepository;

        private readonly Dictionary<string, string> _allPSettings = new Dictionary<string, string>
        {

            {"SyncImagePath", ""},
            {"PrintWorkTimeStartSlip", "true"},

            {"FTIAllowBackDate", "true"},
            {"FTIPrintSummary", "true"},
            {"FTIBackDateDay", "1"},
            {"FTIDayUpdate", "1"},
            {"FTIMaxRePrint", "1"},
            {"FTIMaxVoid", "1"},
            {"FTICentralFolder", @"C:\Test"},
            {"FTICode", "1212"},
            {"FTILocalFolder", @"C:\Test"},
            {"FTIName", "Test"},
            {"FTIPrintForm", "Test"},
            {"FTIWebUrl", "Test"},
            {"FTIRestrictDepartments", ""},


            {"PttOrAutoEntryScreen", "true"},

            {"PttOrBatchId", "000"},
            {"PttOrStandId", "000"},
            
            {"PttOrCode", "12"},
            {"PttOrName", "12"},
            {"PttOrTenderCode", "12"},
            {"PttOrUrl", "12"},
            {"PttLmsAutoSync", "5"},
            {"PttLmsTimeout", "20"},
            {"AllowRedeemByPhoneNo", "true"},
            {"LMSDepartments", "1,2"},


            {"MaxNoOfPromotionPerBill", "1"},
            {"MaxNoOfPromotionPerItem", "1"},

       
            {"BrandId", "123"},
            {"SiteCode", "123"},

            {"CardNumberCount", "4"},

            {"PttEdcPort", "COM1"},
            {"PttEDCCCTimeout", "120"},
            {"PttEDCQRTimeout", "140"},
            {"PttEDCWalletTimeout", "150"},
            
            {"MaxOrderTime", "150"},
            {"MaxPackingTime", "150"},

            {"EnableDineChefLogin", "True"},

            {"HanaDepartments", "1,2"},
            {"HanaUsername", " "},
            {"HanaPassword", " "},
            {"HanaCompanyCode", " "},
            {"HanaSearchUrl", " "},
            {"S4HanaSaleOrderUrl", " "},

            {"IntSaleUsername", "Test"},
            {"IntSalePassword", "Test"},
            {"IntSaleCompanyCode", "Test"},
            {"IntSaleBudgetUrl", "Test"},

            {"JournalLocalFolder", "Test"},


            {"EccUsername", "Test"},
            {"EccPassword", "Test"},
            {"EccUrl", "Test"},

            {"ProductVoucherUrl","https://retail-interface-dev.pttor.com:44300/RESTAdapter/VoucherInfoSend/"},
            {"ProductVoucherUser","s_if_dineplan"},
            {"ProductVoucherPassword","Dineplan#2020"},
            {"ProductVoucherTimeOut","200"},


            {"CreditSaleReport","200"},

            {"DateTimeFormat","yyyy-MM-dd"},
            {"DateFormat","yyyy-MM-dd HH:mm:ss"}



        };

        public SeedAppService(IRepository<ProgramSettingValue> pRepository,
            IRepository<ProgramSettingTemplate> ptRepository)
        {
            _pRepository = pRepository;
            _ptRepository = ptRepository;
        }

        public async Task SeedSampleData()
        {
            await SeedPttData();
        }

        private async Task SeedPttData()
        {
            foreach (var allPSetting in _allPSettings.Keys)
            {
                var pSettingEx = _ptRepository.GetAll().Where(a => a.Name.Equals(allPSetting)).ToList();
                if (!pSettingEx.Any())
                {
                    if (AbpSession.TenantId != null)
                    {
                        var myTemplate = new ProgramSettingTemplate
                        {
                            Name = allPSetting,
                            DefaultValue = _allPSettings[allPSetting],
                            TenantId = AbpSession.TenantId.Value
                        };
                        await _ptRepository.InsertAndGetIdAsync(myTemplate);
                    }
                }
            }
        }
    }
}