﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Abp.Application.Features;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.UI;
using DinePlan.DineConnect.Configuration.Host.Dto;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Setup.Dtos;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.EntityFramework;
using DinePlan.DineConnect.Features;
using DinePlan.DineConnect.House;
using DinePlan.DineConnect.House.Master;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.House.Transaction;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.House.Transaction.Implementation;
using DinePlan.DineConnect.Hr;
using DinePlan.DineConnect.Hr.Master;
using DinePlan.DineConnect.Hr.Transaction;
using DinePlan.DineConnect.Rest;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Connect.Setup.Implementation
{
    public class SetupAppService : DineConnectAppServiceBase, ISetupAppService
    {
        private readonly ICategoryAppService _categoryAppService;
        private readonly IRepository<Category> _catManager;
        private readonly ICompanyAppService _companyAppService;
        private readonly IRepository<Company> _companyRepo;
        private readonly ICustomerAppService _customerAppService;
        private readonly IRepository<Customer> _customerRepo;
        private readonly IRepository<CustomerTagDefinition> _customertagdefinitionRepo;
        private readonly DineConnectDbContext _dbContext;
        private readonly IInwardDirectCreditAppService _inwardDirectCreditAppService;
        private readonly ILocationAppService _locationAppService;
        private readonly IRepository<Master.Location> _locationRepo;
        private readonly IMaterialAppService _materialAppService;
        private readonly IMaterialGroupAppService _materialGroupAppService;
        private readonly IMaterialGroupCategoryAppService _materialGroupCategoryAppService;
        private readonly IRepository<MaterialGroupCategory> _materialgroupcategoryRepo;
        private readonly IRepository<MaterialGroup> _materialgroupRepo;
        private readonly IRepository<Material> _materialRepo;
        private readonly IMenuItemAppService _menuItemApplicationService;
        private readonly IRepository<MenuItem> _menuItemManager;
        private readonly IRepository<Order> _orderManager;
        private readonly IPaymentTypeAppService _paymentTypeAppService;
        private readonly IRecipeAppService _recipeAppService;
        private readonly IRecipeGroupAppService _recipeGroupAppService;
        private readonly IRecipeIngredientAppService _recipeIngredientsAppService;
        private readonly ISupplierAppService _supplierAppService;
        private readonly IRepository<Supplier> _supplierRepo;
        private readonly ITaxAppService _taxAppService;
        private readonly IRepository<Tax> _taxRepo;
        private readonly ITicketAppService _ticketAppService;
        private readonly ITransactionTypeAppService _transactionTypeAppService;
        private readonly IUnitAppService _unitAppService;
        private readonly IUnitConversionAppService _unitconversionAppService;
        private readonly IRepository<UnitConversion> _unitconversionrepo;
        private readonly IRepository<Unit> _unitrepo;

        private readonly IRepository<DcGroupMaster> _dcgroupmasterRepo;
        private readonly IRepository<DcHeadMaster> _dcheadmasterRepo;
        private readonly IRepository<DcGroupMasterVsSkillSet> _dcgroupmastervsskillsetRepo;
        private readonly IRepository<DcStationMaster> _dcstationmasterRepo;
        private readonly IRepository<DcShiftMaster> _dcshiftmasterRepo;
        private readonly IRepository<LeaveRequest> _leaverequestRepo;
        private readonly IRepository<LeaveType> _leavetypeRepo;
        private readonly IRepository<EmployeeSkillSet> _employeeskillsetRepo;
        private readonly IRepository<SkillSet> _skillsetRepo;
        private readonly IIncentiveTagAppService _incentiveTagAppService;
        private readonly IPersonalInformationAppService _personalInformationAppService;
        private readonly IRepository<JobTitleMaster> _jobTitleMasterRepo;

        public SetupAppService(IMenuItemAppService applicationService, IRepository<MenuItem> menuItemManager,
            IRepository<Category> catManager,
            IPaymentTypeAppService paymentTypeAppService,
            ITransactionTypeAppService transactionTypeAppService,
            ICategoryAppService categoryAppService,
            ILocationAppService locationAppService,
            ICompanyAppService companyAppService,
            ITicketAppService ticketAppService,
            IMaterialAppService materialAppService,
            IRecipeAppService recipeAppService,
            IMaterialGroupAppService materialGroupAppService,
            IMaterialGroupCategoryAppService materialGroupCategoryAppService,
            IRecipeGroupAppService recipeGroupAppService,
            IRecipeIngredientAppService recipeIngredientsAppService,
            IProductRecipesLinkAppService productRecipeLinkAppService,
            ISupplierAppService supplierAppService,
            IUnitAppService unitAppService,
            IInwardDirectCreditAppService inwardDirectCreditAppService,
            IInvoiceAppService invoiceAppService,
            DineConnectDbContext dbContext,
            IRepository<Master.Location> locationRepo,
            IRepository<Supplier> supplierRepo,
            IRepository<Company> companyRepo,
            IRepository<Material> materialRepo,
            ICustomerAppService customerAppService,
            IRepository<Customer> customerRepo,
            IRepository<CustomerTagDefinition> customertagdefinitionRepo,
            ITaxAppService taxAppService,
            IRepository<Tax> taxRepo,
            IRepository<TaxTemplateMapping> taxtemplatemappingRepo,
            IRepository<Unit> unitrepo,
            IRepository<UnitConversion> unitconversionrepo,
            IUnitConversionAppService unitconversionAppService,
            IRepository<MaterialGroup> materialgroupRepo,
            IRepository<MaterialGroupCategory> materialgroupcategoryRepo,
            IRepository<MenuItemPortion> portionManager,
            IRepository<Order> orderManager,
            IRepository<DcGroupMaster> dcgroupmasterRepo,
        IRepository<DcHeadMaster> dcheadmasterRepo,
        IRepository<DcGroupMasterVsSkillSet> dcgroupmastervsskillsetRepo,
        IRepository<DcStationMaster> dcstationmasterRepo,
        IRepository<DcShiftMaster> dcshiftmasterRepo,
        IRepository<LeaveRequest> leaverequestRepo,
        IRepository<LeaveType> leavetypeRepo,
        IRepository<EmployeeSkillSet> employeeskillsetRepo,
        IRepository<SkillSet> skillsetRepo,
        IIncentiveTagAppService incentiveTagAppService,
            IPersonalInformationAppService personalInformationAppService,
            IRepository<JobTitleMaster> jobTitleMasterRepo
            )
        {
            _menuItemManager = menuItemManager;
            _orderManager = orderManager;
            _catManager = catManager;
            _materialgroupRepo = materialgroupRepo;
            _materialgroupcategoryRepo = materialgroupcategoryRepo;
            _menuItemApplicationService = applicationService;
            _paymentTypeAppService = paymentTypeAppService;
            _transactionTypeAppService = transactionTypeAppService;
            _categoryAppService = categoryAppService;
            _companyAppService = companyAppService;
            _locationAppService = locationAppService;
            _ticketAppService = ticketAppService;
            _materialAppService = materialAppService;
            _recipeAppService = recipeAppService;
            _materialGroupAppService = materialGroupAppService;
            _materialGroupCategoryAppService = materialGroupCategoryAppService;
            _recipeGroupAppService = recipeGroupAppService;
            _recipeIngredientsAppService = recipeIngredientsAppService;
            _unitAppService = unitAppService;
            _supplierAppService = supplierAppService;
            _inwardDirectCreditAppService = inwardDirectCreditAppService;
            _dbContext = dbContext;
            _locationRepo = locationRepo;
            _supplierRepo = supplierRepo;
            _materialRepo = materialRepo;
            _companyRepo = companyRepo;
            _customerAppService = customerAppService;
            _customerRepo = customerRepo;
            _customertagdefinitionRepo = customertagdefinitionRepo;
            _taxAppService = taxAppService;
            _taxRepo = taxRepo;
            _unitrepo = unitrepo;
            _unitconversionrepo = unitconversionrepo;
            _unitconversionAppService = unitconversionAppService;
            _jobTitleMasterRepo = jobTitleMasterRepo;
            _dcgroupmasterRepo = dcgroupmasterRepo;
            _dcheadmasterRepo = dcheadmasterRepo;
            _dcgroupmastervsskillsetRepo = dcgroupmastervsskillsetRepo;
            _dcstationmasterRepo = dcstationmasterRepo;
            _dcshiftmasterRepo = dcshiftmasterRepo;
            _leaverequestRepo = leaverequestRepo;
            _leavetypeRepo = leavetypeRepo;
            _employeeskillsetRepo = employeeskillsetRepo;
            _skillsetRepo = skillsetRepo;
            _incentiveTagAppService = incentiveTagAppService;
            _personalInformationAppService = personalInformationAppService;
        }

        public IList<MenuItemPortion> AllPortions { get; set; }
        public IList<PaymentType> AllPaymentTypes { get; set; }
        public IList<TransactionType> AllTransactionTypes { get; set; }

        public void ClearMaterials()
        {
            ClearInvoices();
            ClearInwardDirectCredit();

            var value = AbpSession.TenantId ?? 0;

            try
            {
                foreach (var sql in _clearMaterialSql)
                {
                    _dbContext.Database.ExecuteSqlCommand(string.Format(sql, value));
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message + ex.InnerException);
            }
        }

        private void ClearRecipe()
        {
            var value = AbpSession.TenantId ?? 0;
            try
            {
                foreach (var sql in _clearRecipeSql)
                {
                    _dbContext.Database.ExecuteSqlCommand(string.Format(sql, value));
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message + ex.InnerException);
            }
        }

        private void ClearInwardDirectCredit()
        {
            var value = AbpSession.TenantId ?? 0;
            try
            {
                foreach (var sql in _clearInwardDirectCreditSql)
                {
                    _dbContext.Database.ExecuteSqlCommand(string.Format(sql, value));
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message + ex.InnerException);
            }
        }

        private void ClearSupplier()
        {
            var value = AbpSession.TenantId ?? 0;

            foreach (var sql in _clearSupplierSql)
            {
                _dbContext.Database.ExecuteSqlCommand(string.Format(sql, value));
            }
        }

        private void ClearTax()
        {
            var value = AbpSession.TenantId ?? 0;

            foreach (var sql in _clearTaxSql)
            {
                _dbContext.Database.ExecuteSqlCommand(string.Format(sql, value));
            }
        }

        private void ClearInvoices()
        {
            var value = AbpSession.TenantId ?? 0;

            foreach (var sql in _clearInvoiceSql)
            {
                _dbContext.Database.ExecuteSqlCommand(string.Format(sql, value));
            }
        }

        public void ClearAllHouseTransactions()
        {
            var value = AbpSession.TenantId ?? 0;
            if (value > 0)
            {
                foreach (var sql in _clearHouseTransactionSql)
                {
                    _dbContext.Database.ExecuteSqlCommand(string.Format(sql, value));
                }
            }
        }

        private void ClearCustomer()
        {
            var value = AbpSession.TenantId ?? 0;
            foreach (var sql in ClearCustomerSQL)
            {
                _dbContext.Database.ExecuteSqlCommand(string.Format(sql, value));
            }
        }

        private async Task SeedSampleInwardDirectCredit(GetCreateInwardInputDto input)
        {
            await SeedSampleInwardDirectCreditDetails(input);
        }

        private async Task SeedSampleRecipe()
        {
            ClearRecipe();
            var status = await SeedSampleRecipe(_recipeContents);
        }

        private async Task<bool> SeedSampleProductRecipeLink(string[] values)
        {
            var returnStatus = false;
            try
            {
                var ds = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                IdInput currentProdRecipeLink = null;
                IdInput menuId = null;
                string[] recProd = null;
            }
            catch (Exception)
            {
                returnStatus = false;
            }
            return returnStatus;
        }

        private async Task<bool> SeedSampleCustomer()
        {
            ClearCustomer();
            var returnStatus = false;
            try
            {
                var taglst =
                    _customertagdefinitionRepo.GetAll().Where(a => a.TagCode.ToUpper().Equals("DEFAULT")).ToList();
                if (!taglst.Any())
                {
                    throw new UserFriendlyException("DefaultCustomrTagDefinitionMissing");
                }
                var tagid = taglst[0].Id;
                foreach (var item in CustomerContents)
                {
                    if (string.IsNullOrWhiteSpace(item)) continue;

                    if (item.StartsWith("#"))
                    {
                        var customer = new CreateOrUpdateCustomerInput
                        {
                            Customer = new CustomerEditDto
                            {
                                CustomerName = item.Trim('#', ' '),
                                CustomerTagRefId = tagid
                            }
                        };
                        await _customerAppService.CreateOrUpdateCustomer(customer);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.InnerException.ToString());
            }
            return returnStatus;
        }

        private async Task<bool> SeedSampleInwardDirectCreditDetails(GetCreateInwardInputDto input)
        {
            var returnStatus = false;

            var lstMaterial = _materialRepo.GetAll().Where(t => t.MaterialTypeId == 0).ToList();

            if (lstMaterial.Count == 0)
            {
                throw new UserFriendlyException(L("ImportMaterialFirst"));
            }

            var locList = _locationRepo.GetAll().Where(l => l.Id == input.LocationRefId).ToList();
            if (locList.Count == 0)
            {
                throw new UserFriendlyException(L("ImportLocationFirst"));
            }

            var lstSupplier = _supplierRepo.GetAll().ToList();

            if (lstSupplier.Count == 0)
            {
                throw new UserFriendlyException(L("ImportSupplierFirst"));
            }

            var rnd = new Random();

            try
            {
                var ds = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                var matId = 0;
                var prdunitid = 0;
                var supid = 0;
                var locid = 0;

                var noofdays = Math.Abs(input.EndDate.DayOfYear - input.StartDate.DayOfYear);

                foreach (var loc in locList)
                {
                    var daysInsertedCount = 0;
                    for (var date = input.StartDate; date.Date <= input.EndDate.Date; date = date.AddDays(1))
                    {
                        if (noofdays >= 6 && daysInsertedCount > 1)
                        {
                            var addCnt = rnd.Next(1, noofdays - daysInsertedCount);
                            date = date.AddDays(addCnt);
                            if (date > input.EndDate.Date)
                                date = input.EndDate.Date;
                        }

                        var inwarddate = date;
                        if (daysInsertedCount == 3)
                            inwarddate = input.EndDate.Date;
                        else if (daysInsertedCount >= 4)
                            break;

                        foreach (var sup in lstSupplier)
                        {
                            supid = sup.Id;
                            locid = loc.Id;

                            var inwardDet = new CreateOrUpdateInwardDirectCreditInput();
                            var detail = new List<InwardDirectCreditDetailViewDto>();

                            foreach (var mat in lstMaterial)
                            {
                                prdunitid = mat.DefaultUnitId;
                                matId = mat.Id;
                                detail.Add(new InwardDirectCreditDetailViewDto
                                {
                                    DcConvertedQty = 0,
                                    DcReceivedQty = rnd.Next(60, 150),
                                    MaterialRefId = matId,
                                    UnitRefId = prdunitid,
                                    DcRefId = 0,
                                    MaterialReceivedStatus = "0"
                                });
                            }

                            inwardDet = new CreateOrUpdateInwardDirectCreditInput
                            {
                                InwardDirectCredit = new InwardDirectCreditEditDto
                                {
                                    DcDate = inwarddate,
                                    DcNumberGivenBySupplier =
                                        string.Concat("DcSup/", supid.ToString(), "/", rnd.Next(1, 200).ToString()),
                                    LocationRefId = locid,
                                    SupplierRefId = supid,
                                    IsDcCompleted = false
                                },
                                InwardDirectCreditDetail = detail
                            };

                            await _inwardDirectCreditAppService.CreateOrUpdateInwardDirectCredit(inwardDet);
                        }
                        daysInsertedCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.InnerException.ToString());
            }
            return returnStatus;
        }

        private async Task<bool> SeedSampleSupplier()
        {
            ClearSupplier();
            var returnStatus = false;
            try
            {
                foreach (var item in _supplierContents)
                {
                    if (string.IsNullOrWhiteSpace(item)) continue;
                    if (item.StartsWith("#"))
                    {
                        var supplier = new CreateOrUpdateSupplierInput
                        {
                            Supplier = new SupplierEditDto
                            {
                                SupplierName = item.Trim('#', ' ')
                            }
                        };
                        await _supplierAppService.CreateOrUpdateSupplier(supplier);
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Fatal("SEED SUPPLIER", exception);
                returnStatus = false;
            }
            return returnStatus;
        }

        private async Task<bool> SeedSampleTax()
        {
            ClearTax();

            var returnStatus = false;
            try
            {
                string[] tax = null;
                foreach (var item in _taxContents)
                {
                    if (string.IsNullOrWhiteSpace(item)) continue;
                    if (item.StartsWith("#"))
                    {
                        tax = item.Split(',');
                        var taxname = tax[0].Trim('#', ' ');
                        var taxrate = decimal.Parse(tax[2]);


                        var taxdto = new TaxEditDto
                        {
                            TaxName = taxname,
                            Percentage = taxrate,
                            TaxCalculationMethod = "GT"
                        };

                        var taxMaplistdto = new List<TaxTemplateMapping>();

                        var taxmappingdto = new TaxTemplateMapping
                        {
                            TaxRefId = 0
                        };

                        taxMaplistdto.Add(taxmappingdto);

                        var taxnewdto = new CreateOrUpdateTaxInput
                        {
                            Tax = taxdto,
                            TaxTemplateMapping = taxMaplistdto
                        };

                        await _taxAppService.CreateOrUpdateTax(taxnewdto);

                        //await _taxtemplatemappingRepo.InsertAndGetIdAsync(taxmappingdto);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message + ex.InnerException);
                //returnStatus = false;
            }
            return returnStatus;
        }

        private async Task<bool> SeedSampleRecipe(string[] values)
        {
            var returnStatus = false;
            var matList = _materialRepo.GetAll().ToList();
            if (matList.Count == 0)
            {
                throw new UserFriendlyException(L("ImportMaterialFirst"));
            }

            try
            {
                var ds = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                IdInput currentRecipeGroup = null;
                IdInput currentRecipe = null;
                IdInput prdunitid = null;
                IdInput selunitid = null;
                string[] recipe = null;
                EntityDto matId = null;
                string[] recIngr = null;
                var sno = 0;
                foreach (var item in values)
                {
                    if (string.IsNullOrWhiteSpace(item)) continue;
                    if (item.StartsWith("$")) //RecipeGroup
                    {
                        var recGroup = new CreateOrUpdateRecipeGroupInput
                        {
                            RecipeGroup = new RecipeGroupEditDto
                            {
                                RecipeGroupName = item.Trim('$', ' '),
                                RecipeGroupShortName = item.Trim('$', ' ')
                            }
                        };
                        currentRecipeGroup = await _recipeGroupAppService.CreateOrUpdateRecipeGroup(recGroup);
                    }
                    else if (item.StartsWith("#")) //Recipe
                    {
                        recipe = item.Split(',');
                        //"#IDLY,47,KG,NO,60,36.50"
                        prdunitid = _unitAppService.GetUnitIdByName(recipe[2]);
                        selunitid = _unitAppService.GetUnitIdByName(recipe[3]);
                        var recp = new CreateOrUpdateRecipeInput
                        {
                            Recipe = new RecipeEditDto
                            {
                                RecipeName = recipe[0].Trim('#', ' '),
                                RecipeShortName = recipe[0].Trim('#', ' '),
                                RecipeGroupRefId = currentRecipeGroup.Id,
                                PrdBatchQty = ConvertToDecimal(recipe[1], ds),
                                PrdUnitId = prdunitid.Id,
                                SelUnitId = selunitid.Id,
                                LifeTimeInMinutes = 240, //Convert.ToInt16(recipe[4]),
                                FixedCost = ConvertToDecimal(recipe[5], ds),
                                AlarmTimeInMinutes = 0
                            }
                        };
                        currentRecipe = await _recipeAppService.CreateOrUpdateRecipe(recp);
                    }
                    else if (item.StartsWith("@"))
                    {
                        //"@URUD DHAL (WHOLE),0.3,KG",    
                        recIngr = item.Split(',');
                        matId = _materialAppService.GetMaterialIdByName(recIngr[0].Trim('@', ' '));
                        if (matId.Id == 0)
                        {
                            //throw new UserFriendlyException(L("MaterialNotFound"));
                            continue;
                        }
                        prdunitid = _unitAppService.GetUnitIdByName(recIngr[2]);
                        sno++;
                        var recipeIngr = new CreateOrUpdateRecipeIngredientInput
                        {
                            RecipeIngredient = new RecipeIngredientEditDto
                            {
                                MaterialRefId = matId.Id,
                                MaterialUsedQty = ConvertToDecimal(recIngr[1], ds),
                                RecipeRefId = currentRecipe.Id,
                                UnitRefId = prdunitid.Id,
                                UserSerialNumber = sno
                            }
                        };
                        await _recipeIngredientsAppService.CreateOrUpdateRecipeIngredient(recipeIngr);
                    }
                }
            }
            catch (Exception)
            {
                returnStatus = false;
            }
            return returnStatus;
        }




        private async Task<bool> SeedSampleMaterial()
        {
            ClearMaterials();
            var returnStatus = false;
            IdInput currentMatGroup = null;
            IdInput currentMatGroupCategory = null;
            EntityDto currentMaterial = null;
            var sno = 0;

            var locList = _locationRepo.GetAll().ToList();
            if (locList.Count == 0)
            {
                throw new UserFriendlyException(L("ImportLocationFirst"));
            }


            var lstSupplier = _supplierRepo.GetAll().ToList();

            if (lstSupplier.Count == 0)
            {
                throw new UserFriendlyException(L("ImportSupplierFirst"));
            }

            var lstCustomer = _customerRepo.GetAll().ToList();

            if (lstCustomer.Count == 0)
            {
                throw new UserFriendlyException(L("ImportCustomerFirst"));
            }

            try
            {
                foreach (var item in _materialContents)
                {
                    if (string.IsNullOrWhiteSpace(item)) continue;
                    if (item.StartsWith("$")) //MaterialGroups
                    {
                        var matGroupDto = new CreateOrUpdateMaterialGroupInput
                        {
                            MaterialGroup = new MaterialGroupEditDto
                            {
                                MaterialGroupName = item.Trim('$', ' ')
                            }
                        };
                        currentMatGroup = await _materialGroupAppService.CreateOrUpdateMaterialGroup(matGroupDto);
                    }
                    else if (item.StartsWith("#")) //MaterialGroupCategory
                    {
                        var matGroupCateDto = new CreateOrUpdateMaterialGroupCategoryInput
                        {
                            MaterialGroupCategory = new MaterialGroupCategoryEditDto
                            {
                                MaterialGroupCategoryName = item.Trim('#', ' '),
                                MaterialGroupRefId = currentMatGroup.Id
                            }
                        };
                        currentMatGroupCategory =
                            await _materialGroupCategoryAppService.CreateOrUpdateMaterialGroupCategory(matGroupCateDto);
                    }
                    else // Material 
                    {
                        sno++;
                        var rnd = new Random();
                        var rndAmount = rnd.Next(5, 15);

                        var customerMaterialDto = new List<CustomerMaterialEditDto>();
                        foreach (var cust in lstCustomer)
                        {
                            customerMaterialDto.Add(new CustomerMaterialEditDto
                            {
                                CustomerRefId = cust.Id,
                                MaterialPrice = rnd.Next(rndAmount, rndAmount + 1),
                                LastQuoteDate = DateTime.Today,
                                LastQuoteRefNo = "Refquote"
                            });
                        }

                        string[] matarray = null;
                        var defaultUnitId = 0;
                        var mattypeid = 0;
                        var ownproductionflag = false;
                        var wipeoutflag = false;
                        var materialName = "";

                        matarray = item.Split(',');
                        materialName = matarray[0];

                        MaterialRecipeTypesEditDto recTypeDto = null;
                        var igredientListDtos = new List<MaterialIngredientEditDto>();

                        if (matarray.Length > 1) //  Material With Units
                        {
                            defaultUnitId = _unitAppService.GetUnitIdByName(matarray[1]).Id;

                            if (matarray.Length > 2)
                            {
                                mattypeid = 1;
                                ownproductionflag = true;
                                wipeoutflag = true;

                                recTypeDto = new MaterialRecipeTypesEditDto();

                                recTypeDto.PrdBatchQty = decimal.Parse(matarray[2]);
                                recTypeDto.LifeTimeInMinutes = 240;
                                recTypeDto.AlarmFlag = true;
                                recTypeDto.AlarmTimeInMinutes = 180;
                                recTypeDto.FixedCost = 0;

                                var noofingredient = matarray.Length - 3;
                                var currentindex = 3;

                                string[] ingrdetailsArray;


                                for (var loopcnt = 0; loopcnt < noofingredient; loopcnt++)
                                {
                                    ingrdetailsArray = matarray[currentindex].Split('^');

                                    if (ingrdetailsArray.Length > 1)
                                    {
                                        var ingrDto = new MaterialIngredientEditDto();

                                        var matid =
                                            _materialAppService.GetMaterialIdByName(ingrdetailsArray[0].Replace("@", ""))
                                                .Id;

                                        ingrDto.MaterialRefId = matid;
                                        ingrDto.MaterialUsedQty = decimal.Parse(ingrdetailsArray[1]);
                                        ingrDto.UnitRefId = _unitAppService.GetUnitIdByName(ingrdetailsArray[2]).Id;
                                        ingrDto.VariationflagForProduction = false;
                                        ingrDto.UserSerialNumber = loopcnt + 1;


                                        igredientListDtos.Add(ingrDto);
                                    }
                                    currentindex++;
                                }
                            }
                        }
                        else
                        {
                            recTypeDto = null;
                        }


                        var supplierMaterialDto = new List<SupplierMaterialEditDto>();
                        rndAmount = rnd.Next(10, 30);
                        if (mattypeid == 0) // Rawmaterial types only purchased from Supplier
                        {
                            foreach (var sup in lstSupplier)
                            {
                                supplierMaterialDto.Add(new SupplierMaterialEditDto
                                {
                                    SupplierRefId = sup.Id,
                                    MaterialPrice = rnd.Next(rndAmount, rndAmount + 7),
                                    MinimumOrderQuantity = 0,
                                    MaximumOrderQuantity = 0,
                                    LastQuoteDate = DateTime.Today,
                                    LastQuoteRefNo = "Refquote",
                                    UnitRefId = defaultUnitId
                                });
                            }
                        }

                        var materialDto = new CreateOrUpdateMaterialInput
                        {
                            Material = new MaterialEditDto
                            {
                                BrandRefId = 0,
                                DefaultUnitId = defaultUnitId,
                                IssueUnitId = defaultUnitId,
                                GeneralLifeDays = 2,
                                MaterialGroupCategoryRefId = currentMatGroupCategory.Id,
                                MaterialName = materialName,
                                MaterialPetName = materialName,
                                PosReferencecodeifany = 1,
                                UserSerialNumber = sno,
                                MaterialTypeId = mattypeid,
                                OwnPreparation = ownproductionflag,
                                WipeOutStockOnClosingDay = wipeoutflag,
                                SyncLastModification = null,
                            },
                            SupplierMaterial = supplierMaterialDto,
                            CustomerMaterial = customerMaterialDto,
                            MaterialRecipeType = recTypeDto,
                            MaterialIngredient = igredientListDtos,
                            UnitConversionList = new List<UnitConversionEditDto>()
                        };

                        currentMaterial = await _materialAppService.CreateOrUpdateMaterial(materialDto);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message + ex.InnerException);
            }
            return returnStatus;
        }

        private static decimal ConvertToDecimal(string priceStr, string decimalSeperator)
        {
            try
            {
                priceStr = priceStr.Replace(".", decimalSeperator);
                priceStr = priceStr.Replace(",", decimalSeperator);

                var price = Convert.ToDecimal(priceStr);
                return price;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        #region Methods

        public void ClearInventoryTransaction()
        {
            ClearAllHouseTransactions();
        }

        public void ClearTickTransaction()
        {
            var value = AbpSession.TenantId ?? 0;
            if (value > 0)
            {
                foreach (var sql in _clearTickSql)
                {
                    try
                    {
                        _dbContext.Database.ExecuteSqlCommand(string.Format(sql, value));
                    }
                    catch (Exception ex)
                    {
                        //throw new UserFriendlyException(sql);
                        MethodBase m = MethodBase.GetCurrentMethod();
                        string innerExceptionMessage = ex.InnerException != null ? ex.InnerException.Message : "";
                        string prjErrorMessage = sql;
                        throw new UserFriendlyException(prjErrorMessage + " " + "Method : " + m.ReflectedType.Name + " : " + m.Name + " " + ex.Message + " " + innerExceptionMessage);
                    }

                }
            }
        }

        public void ClearAll()
        {
            ClearTransaction();
            ClearMaterials();
            ClearConnect();
            ClearSupplier();
            ClearTax();
            ClearLocation();
        }

        public void ClearTransaction()
        {
            ClearAllHouseTransactions();
            var value = AbpSession.TenantId ?? 0;
            if (value > 0)
            {
                foreach (var sql in _clearTransactions)
                {
                    _dbContext.Database.ExecuteSqlCommand(string.Format(sql, value));
                }
            }
        }

        public void ClearSales()
        {
            var value = AbpSession.TenantId ?? 0;
            if (value > 0)
            {
                foreach (var sql in _clearTransactions)
                {
                    _dbContext.Database.ExecuteSqlCommand(string.Format(sql, value));
                }
            }
        }

        public async Task SeedSampleData(GetCreateTicketsInputDto input)
        {
            ClearTransaction();

            AllPortions = null;
            AllPaymentTypes = _paymentTypeAppService.GetEntities().ToList();
            AllTransactionTypes = _transactionTypeAppService.GetEntities().ToList();

            var ticketCount = 10;
            var ticktNumber = 1;
            var orderNumber = 1;

            for (var date = input.StartDate; date.Date <= input.EndDate.Date; date = date.AddDays(1))
            {
                for (var i = 1; i <= ticketCount; i++)
                {
                    var hour = i % 23;
                    var createdDay = date.AddHours(i);

                    if (createdDay > DateTime.Now)
                        createdDay = DateTime.Now;

                    var allDto = await _locationAppService.GetAll(new GetLocationInput
                    {
                        Filter = ""
                    });

                    foreach (
                        var locationListDto in allDto.Items)
                    {
                        var ticket = new Transaction.Ticket
                        {
                            LocationId = locationListDto.Id,
                            TaxIncluded = false,
                            TicketCreatedTime = createdDay,
                            LastModifiedUserName = locationListDto.Code + "_Admin",
                            TicketId = ticktNumber,
                            TicketNumber = ticktNumber.ToString(),
                            TerminalName = locationListDto.Code + "Server",
                            TicketTypeName = "Ticket",
                            LastUpdateTime = createdDay.AddMinutes(10),
                            LastOrderTime = createdDay.AddMinutes(5),
                            IsClosed = true,
                            InvoiceNo = ticktNumber.ToString(),
                            IsLocked = false,
                            Note = "",
                            TicketTags = "",
                            TicketLogs = "",
                            TicketStates = "",
                            TicketEntities = "",
                            WorkPeriodId = 0,
                            LastPaymentTime = createdDay.AddMinutes(6),
                            DepartmentName = "Restaurant",
                            RemainingAmount = 0M,
                            TotalAmount = 0M,
                            TenantId = AbpSession.TenantId ?? 0
                        };

                        ticktNumber++;
                        var ticketTotalAmount = 0M;

                        ticket.Orders = new List<Order>();
                        var random = new Random();
                        var orderCount = random.Next(1, 4);

                        for (var oCount = 1; oCount <= orderCount; oCount++)
                        {
                            var menuCount = random.Next(1, 45);
                            var portionCount = random.Next(1, 10);

                            var dto = AllPortions[menuCount] ?? AllPortions[0];

                            var odto = new Order
                            {
                                DepartmentName = "Restaurant",
                                MenuItemId = dto.MenuItemId,
                                Location = _locationRepo.Get(locationListDto.Id),
                                MenuItemName = dto.MenuItem != null ? dto.MenuItem.Name : "",
                                PortionName = dto.Name,
                                CalculatePrice = true,
                                IncreaseInventory = false,
                                DecreaseInventory = true,
                                CostPrice = dto.Price - .5M,
                                PortionCount = 1,
                                Note = "",
                                Locked = false,
                                IsPromotionOrder = false,
                                PromotionAmount = 0M,
                                MenuItemPortionId = dto.Id,
                                PriceTag = "",
                                Taxes = "",
                                OrderTags = "",
                                OrderStates = "",
                                Price = dto.Price,
                                Quantity = portionCount,
                                OrderNumber = orderNumber.ToString(),
                                CreatingUserName = locationListDto.Code + "_Admin",
                                OrderCreatedTime = createdDay.AddMinutes(5)
                            };
                            ticket.Orders.Add(odto);
                            ticketTotalAmount += (odto.Quantity * odto.Price);
                            orderNumber++;
                        }
                        ticket.Payments = new List<Payment>();

                        var paymentCount = random.Next(1, 4);
                        var paymentValue = ticketTotalAmount / paymentCount;
                        for (var oCount = 1; oCount <= paymentCount; oCount++)
                        {
                            var payment = new Payment
                            {
                                PaymentCreatedTime = createdDay.AddMinutes(5),
                                TerminalName = locationListDto.Code + "Server",
                                PaymentUserName = locationListDto.Code + "_Admin",
                                Amount = paymentValue,
                                TenderedAmount = paymentValue,
                                PaymentTypeId = AllPaymentTypes[oCount].Id
                            };
                            ticket.Payments.Add(payment);
                        }

                        ticket.Transactions = new List<TicketTransaction>();

                        var paymentId = 1;
                        var singleOrDefault = AllTransactionTypes.FirstOrDefault(a => a.Name.Equals("PAYMENT"));
                        if (singleOrDefault != null)
                        {
                            paymentId = singleOrDefault.Id;
                        }

                        var salesId = 2;
                        singleOrDefault = AllTransactionTypes.FirstOrDefault(a => a.Name.Equals("SALE"));
                        var salesAmount = ticketTotalAmount - (ticketTotalAmount * .11M);
                        if (singleOrDefault != null)
                        {
                            salesId = singleOrDefault.Id;
                        }


                        var serviceId = 3;
                        var serviceAmount = ticketTotalAmount * .1M;
                        singleOrDefault = AllTransactionTypes.FirstOrDefault(a => a.Name.Equals("SERVICE CHARGE"));
                        if (singleOrDefault != null)
                        {
                            serviceId = singleOrDefault.Id;
                        }

                        var discountId = 5;
                        var discountAmount = ticketTotalAmount * .01M;
                        singleOrDefault = AllTransactionTypes.FirstOrDefault(a => a.Name.Equals("DISCOUNT"));
                        if (singleOrDefault != null)
                        {
                            discountId = singleOrDefault.Id;
                        }

                        var roundId = 6;
                        var roundAmount = ticketTotalAmount - (salesAmount + serviceAmount + discountAmount);
                        singleOrDefault = AllTransactionTypes.SingleOrDefault(a => a.Name.Equals("ROUND"));
                        if (singleOrDefault != null)
                        {
                            roundId = singleOrDefault.Id;
                        }

                        ticket.Transactions.Add(new TicketTransaction
                        {
                            TransactionTypeId = paymentId,
                            Amount = ticketTotalAmount
                        });

                        ticket.Transactions.Add(new TicketTransaction
                        {
                            TransactionTypeId = salesId,
                            Amount = salesAmount
                        });
                        ticket.Transactions.Add(new TicketTransaction
                        {
                            TransactionTypeId = serviceId,
                            Amount = serviceAmount
                        });
                        ticket.Transactions.Add(new TicketTransaction
                        {
                            TransactionTypeId = discountId,
                            Amount = discountAmount
                        });

                        ticket.Transactions.Add(new TicketTransaction
                        {
                            TransactionTypeId = roundId,
                            Amount = roundAmount
                        });

                        ticket.TotalAmount = ticketTotalAmount;
                        try
                        {
                            _ticketAppService.CreateTicket(ticket);
                        }
                        catch (Exception exception)
                        {
                            var test = exception.Message;
                        }
                    }
                }
            }
        }

        public async Task SeedAll()
        {
            try
            {
                if (_companyRepo.Count(a => !a.IsDeleted) <= 0)
                {
                    ClearAll();
                    var companyCreated = await SeedSampleCompany();
                    await SeedSampleLocation(_locationNames);
                    await SeedSampleMenu();
                    await SeedSampleCustomer();
                    await SeedSampleSupplier();
                    await SeedSampleMaterial();
                    await SeedSampleTax();
                    await _paymentTypeAppService.CreatePaymentForTenant(AbpSession.TenantId ?? 0);
                    await _transactionTypeAppService.CreateTransactionTypeForTenant(AbpSession.TenantId ?? 0);
                }
            }
            catch (Exception exception)
            {
                throw new UserFriendlyException(exception.Message + " " + exception.InnerException);
            }
        }

        public async Task SeedTickAll(IdInput locInput)
        {
            try
            {
                if (_skillsetRepo.Count(a => !a.IsDeleted) <= 0)
                {
                    //ClearTickTransaction();
                    var created = await SeedSkillSet();
                    await SeedDcHeader();
                    var c1 = await SeedGroupMaster();
                    await SeedGroupVsSkillSet();
                    await SeedJobTitles();
                    await SeedStationMaster(locInput);
                    await SeedShiftMaster(locInput);

                    await _incentiveTagAppService.ImportSystemIncentiveList();
                    //await _personalInformationAppService.ImportPersonalInformationSampleList(locInput);
                }
            }
            catch (Exception exception)
            {
                throw new UserFriendlyException(exception.Message + " " + exception.InnerException);
            }
        }

        private async Task<bool> SeedSkillSet()
        {
            var returnStatus = false;
            foreach (var item in _dcSkillSetNames)
            {
                if (string.IsNullOrWhiteSpace(item)) continue;
                if (item.StartsWith("$")) //Company
                {
                    var skillDto = new SkillSet
                    {
                        SkillCode = item.Trim('$', ' '),
                        Description = item.Trim('$', ' '),
                        IsActive = true
                    };

                    await _skillsetRepo.InsertOrUpdateAndGetIdAsync(skillDto);
                    returnStatus = true;
                }
            }
            return returnStatus;
        }

        private async Task<bool> SeedDcHeader()
        {
            var returnStatus = false;
            foreach (var item in _dcHeadMasterNames)
            {
                if (string.IsNullOrWhiteSpace(item)) continue;
                if (item.StartsWith("$")) //Company
                {
                    var dcHeaderDto = new DcHeadMaster
                    {
                        HeadName = item.Trim('$', ' '),
                        IsActive = true
                    };

                    await _dcheadmasterRepo.InsertOrUpdateAndGetIdAsync(dcHeaderDto);
                    returnStatus = true;
                }
            }
            return returnStatus;
        }

        private async Task<bool> SeedJobTitles()
        {
            var returnStatus = false;
            foreach (var item in _jobTitles)
            {
                if (string.IsNullOrWhiteSpace(item)) continue;
                if (item.StartsWith("$")) //Company
                {
                    var jobTitleDto = new JobTitleMaster
                    {
                        JobTitle = item.Trim('$', ' '),
                    };

                    await _jobTitleMasterRepo.InsertOrUpdateAndGetIdAsync(jobTitleDto);
                    returnStatus = true;
                }
            }
            return returnStatus;
        }

        private async Task<bool> SeedGroupMaster()
        {
            var returnStatus = false;
            var headerId = 0;
            var sno = 1;

            foreach (var item in _dcGroupMasterNames)
            {
                if (item.StartsWith("$"))
                {
                    var headerName = item.Trim('$', ' ');
                    var header = await _dcheadmasterRepo.FirstOrDefaultAsync(t => t.HeadName.ToUpper().Equals(headerName.ToUpper()));
                    headerId = header.Id;
                }
                else if (item.StartsWith("#")) //Location
                {
                    var groupname = item.Trim('#', ' ').ToUpper();
                    var groupDto = new DcGroupMaster
                    {
                        DcHeadRefId = headerId,
                        GroupName = groupname,
                        IsActive = true,
                        PriorityLevel = sno++
                    };
                    await _dcgroupmasterRepo.InsertOrUpdateAndGetIdAsync(groupDto);
                    returnStatus = true;
                }
            }

            return returnStatus;
        }

        private async Task<bool> SeedGroupVsSkillSet()
        {
            var returnStatus = false;
            var rsGroup = await _dcgroupmasterRepo.GetAllListAsync();
            var rsSkillSet = await _skillsetRepo.GetAllListAsync();

            try
            {
                var ds = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                foreach (var item in _dcGroupVsSkillNames)
                {
                    if (string.IsNullOrWhiteSpace(item)) continue;
                    int groupRefId = 0;
                    string[] arrSkillSet = null;
                    if (item.StartsWith("#")) //RecipeGroup
                    {
                        arrSkillSet = item.Split(',');

                        var groupName = arrSkillSet[0].Trim('#', ' ');
                        var group = await _dcgroupmasterRepo.FirstOrDefaultAsync(t => t.GroupName.ToUpper().Equals(groupName.ToUpper()));
                        groupRefId = group.Id;

                        for (var i = 1; i < arrSkillSet.Length - 1; i++)
                        {
                            var skillName = arrSkillSet[i].Trim();
                            var skill = rsSkillSet.FirstOrDefault(t => t.SkillCode.ToUpper().Equals(skillName.ToUpper()));
                            var det = new DcGroupMasterVsSkillSet
                            {
                                SkillSetRefId = skill.Id,
                                DcGropupRefId = groupRefId,
                            };
                            await _dcgroupmastervsskillsetRepo.InsertOrUpdateAndGetIdAsync(det);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                returnStatus = false;
                throw new UserFriendlyException(ex.Message);
            }
            return returnStatus;
        }


        private async Task<bool> SeedStationMaster(IdInput input)
        {
            var returnStatus = false;
            var rsGroup = await _dcgroupmasterRepo.GetAllListAsync();
            var rsLocation = await _locationRepo.GetAllListAsync();
            string errLine = "";

            try
            {
                var ds = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                foreach (var item in _dcStationMasters)
                {
                    if (string.IsNullOrWhiteSpace(item)) continue;
                    int groupRefId = 0;
                    int locationRefId = 0;
                    string[] arrStationList = null;

                    errLine = item;
                    string stnName = null;
                    int maxStaffReqd = 0;
                    int minStaffREqd = 0;
                    decimal priorLevel = 0.00m;
                    bool isMandate = true;
                    bool isActive = true;

                    bool defaultTimeFlag = false;
                    int timeIn;
                    bool breakFlag = false;
                    int breakOut;
                    int breakIn;
                    int timeOut;
                    string description;


                    //if (item.StartsWith("#")) //RecipeGroup
                    {
                        arrStationList = item.Split(',');

                        var groupName = arrStationList[2].Trim();
                        var group = rsGroup.FirstOrDefault(t => t.GroupName.ToUpper().Equals(groupName.ToUpper()));
                        if (group == null)
                            throw new UserFriendlyException(groupName + " GROUP NOT EXISTS");
                        groupRefId = group.Id;


                        var locName = arrStationList[0].Trim();
                        var loc = rsLocation.FirstOrDefault(t => t.Name.ToUpper().Equals(locName.ToUpper()) || t.Code.ToUpper().Equals(locName.ToUpper()));
                        if (loc == null)
                        {
                            loc = rsLocation.FirstOrDefault(t => t.Id == input.Id);
                            if (loc == null)
                            {
                                throw new UserFriendlyException("Location " + locName + " Not Exists");
                            }
                        }

                        locationRefId = loc.Id;



                        stnName = arrStationList[1].ToUpper().Trim();
                        maxStaffReqd = int.Parse(arrStationList[3].Trim());
                        minStaffREqd = int.Parse(arrStationList[4].Trim());
                        priorLevel = decimal.Parse(arrStationList[5].Trim());
                        isMandate = int.Parse(arrStationList[6].Trim()) == 0 ? false : true;
                        isActive = int.Parse(arrStationList[7].Trim()) == 0 ? false : true;

                        defaultTimeFlag = int.Parse(arrStationList[9].Trim()) == 0 ? false : true;
                        timeIn = int.Parse(arrStationList[10].Trim());
                        breakFlag = int.Parse(arrStationList[11].Trim()) == 0 ? false : true;
                        breakOut = int.Parse(arrStationList[12].Trim());
                        breakIn = int.Parse(arrStationList[13].Trim());
                        timeOut = int.Parse(arrStationList[15].Trim());
                        description = arrStationList[16].ToUpper().Trim();





                        {

                            var det = new DcStationMaster

                            {
                                LocationRefId = locationRefId,
                                DcGroupRefId = groupRefId,
                                StationName = stnName,
                                MaximumStaffRequired = maxStaffReqd,
                                MinimumStaffRequired = minStaffREqd,
                                PriorityLevel = priorLevel,
                                IsMandatory = isMandate,
                                IsActive = isActive,
                                NightFlag = false,
                                DefaultTimeFlag = defaultTimeFlag,
                                TimeIn = timeIn,
                                BreakFlag = breakFlag,
                                BreakOut = breakOut,
                                BreakIn = breakIn,
                                TimeOut = timeOut,
                                TimeDescription = description
                            };
                            await _dcstationmasterRepo.InsertOrUpdateAndGetIdAsync(det);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                returnStatus = false;
                throw new UserFriendlyException(errLine + " " + ex.Message);
            }
            return returnStatus;
        }

        private async Task<bool> SeedShiftMaster(IdInput input)
        {
            var returnStatus = false;
            var rsStation = await _dcstationmasterRepo.GetAllListAsync();
            var rsLocation = await _locationRepo.GetAllListAsync();
            string errLine = "";

            try
            {
                var ds = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                foreach (var item in _dcShiftMastersData)
                {
                    if (string.IsNullOrWhiteSpace(item)) continue;
                    int stationRefId = 0;
                    int locationRefId = 0;
                    string[] arrShiftList = null;

                    errLine = item;
                    string dutyDesc = null;
                    int maxStaffReqd = 0;
                    int minStaffREqd = 0;
                    decimal priorLevel = 0.00m;
                    bool isMandate = true;

                    int timeIn;
                    bool breakFlag = false;
                    int breakOut;
                    int breakIn;
                    int timeOut;
                    string description;

                    arrShiftList = item.Split(',');

                    var locName = arrShiftList[0].Trim();
                    var loc = rsLocation.FirstOrDefault(t => t.Name.ToUpper().Equals(locName.ToUpper()) || t.Code.ToUpper().Equals(locName.ToUpper()));
                    if (loc == null)
                    {
                        loc = rsLocation.FirstOrDefault(t => t.Id == input.Id);
                        if (loc == null)
                        {
                            throw new UserFriendlyException("Location " + locName + " Not Exists");
                        }
                    }

                    locationRefId = loc.Id;

                    var stationName = arrShiftList[2].Trim();
                    var stn = rsStation.FirstOrDefault(t => t.StationName.ToUpper().Equals(stationName.ToUpper()));
                    if (stn == null)
                        throw new UserFriendlyException(stationName + " Station Name Not Exists");
                    stationRefId = stn.Id;

                    //Location 0  | Duty Desc   1       | Station   2            | Max 3| Min 4 | Prio 5| IsMand 6| Night 7| TimeIn 8| BF 9 | BreakOut 10| BreakIn 11| BrkMins 12 | TimeOut 13 | TimeDescription 14,


                    //"BEDOK,     6:00 - 14:30,       COFFEE LEVEL 1,            1,   1,  1.00,   1,        0,        360,  0,    0,          0,          0,      870,   IN : 6:00 - OUT : 14:30"

                    dutyDesc = arrShiftList[1].ToUpper().Trim();
                    maxStaffReqd = int.Parse(arrShiftList[3].Trim());
                    minStaffREqd = int.Parse(arrShiftList[4].Trim());
                    priorLevel = decimal.Parse(arrShiftList[5].Trim());
                    isMandate = int.Parse(arrShiftList[6].Trim()) == 0 ? false : true;
                    timeIn = int.Parse(arrShiftList[8].Trim());
                    breakFlag = int.Parse(arrShiftList[9].Trim()) == 0 ? false : true;
                    breakOut = int.Parse(arrShiftList[10].Trim());
                    breakIn = int.Parse(arrShiftList[11].Trim());
                    timeOut = int.Parse(arrShiftList[13].Trim());
                    description = arrShiftList[14].ToUpper().Trim();

                    {

                        var det = new DcShiftMaster

                        {
                            LocationRefId = locationRefId,
                            StationRefId = stationRefId,
                            DutyDesc = dutyDesc,
                            MaximumStaffRequired = maxStaffReqd,
                            MinimumStaffRequired = minStaffREqd,
                            PriorityLevel = priorLevel,
                            IsMandatory = isMandate,
                            IsActive = true,
                            NightFlag = false,
                            TimeIn = timeIn,
                            BreakFlag = breakFlag,
                            BreakOut = breakOut,
                            BreakIn = breakIn,
                            TimeOut = timeOut,
                            TimeDescription = description,
                        };
                        await _dcshiftmasterRepo.InsertOrUpdateAndGetIdAsync(det);

                    }
                }
            }
            catch (Exception ex)
            {
                returnStatus = false;
                throw new UserFriendlyException(errLine + " " + ex.Message);
            }
            return returnStatus;
        }

        public async Task SyncConnect(GeneralSettingsEditDto input)
        {
            if (!string.IsNullOrEmpty(input.Syncurl) && !string.IsNullOrEmpty(input.TenantName)
                && !string.IsNullOrEmpty(input.User)
                && !string.IsNullOrEmpty(input.Password) &&
                input.TenantId > 0)
            {
                RestUtils.Url = input.Syncurl;
                RestUtils.Tenant = input.TenantName;
                RestUtils.User = input.User;
                RestUtils.Password = input.Password;

                var token = RestUtils.GetToken(true);
                if (!string.IsNullOrEmpty(token))
                {
                    var rsRemoteSupplier = new List<SupplierListDto>();

                    #region SyncSupplier

                    var responseSuppliers = RestUtils.GetResponse(ConnectRequests.GETSUPPLIERS,
                        new GetSupplierInput
                        {
                            SkipCount = 0,
                            MaxResultCount = AppConsts.MaxPageSize,
                            Sorting = "CreationTime"
                        });

                    if (responseSuppliers != null)
                    {
                        var output =
                            JsonConvert.DeserializeObject<PagedResultOutput<SupplierListDto>>(
                                responseSuppliers.ToString());
                        if (output?.Items != null && output.Items.Any())
                        {
                            var allItems = output.Items;
                            rsRemoteSupplier = (List<SupplierListDto>)allItems;

                            foreach (var remotesup in allItems)
                            {
                                var alreadyExists =
                                    await _supplierRepo.FirstOrDefaultAsync(t => t.SyncId == remotesup.Id) ??
                                    await
                                        _supplierRepo.FirstOrDefaultAsync(t => t.SupplierName == remotesup.SupplierName);


                                var supplier = new CreateOrUpdateSupplierInput
                                {
                                    Supplier = new SupplierEditDto
                                    {
                                        SupplierCode = remotesup.SupplierCode,
                                        SupplierName = remotesup.SupplierName,
                                        Address1 = remotesup.Address1,
                                        Address2 = remotesup.Address2,
                                        Address3 = remotesup.Address3,
                                        City = remotesup.City,
                                        State = remotesup.State,
                                        Country = remotesup.Country,
                                        ZipCode = remotesup.ZipCode,
                                        PhoneNumber1 = remotesup.PhoneNumber1,
                                        DefaultCreditDays = remotesup.DefaultCreditDays,
                                        OrderPlacedThrough = remotesup.OrderPlacedThrough,
                                        Email = remotesup.Email,
                                        FaxNumber = remotesup.FaxNumber,
                                        Website = remotesup.Website,
                                        IsActive = remotesup.IsActive,
                                        TaxRegistrationNumber = remotesup.TaxRegistrationNumber,
                                        LoadOnlyLinkedMaterials = remotesup.LoadOnlyLinkedMaterials,
                                        SyncId = remotesup.Id,
                                        SyncLastModification = DateTime.Now
                                    }
                                };
                                if (alreadyExists == null)
                                {
                                    supplier.Supplier.Id = null;
                                }
                                else
                                {
                                    supplier.Supplier.Id = alreadyExists.Id;
                                }
                                await _supplierAppService.CreateOrUpdateSupplier(supplier);
                            }
                        }
                    }

                    #endregion

                    var rsRemoteUnit = new List<UnitListDto>();

                    #region SyncUnits

                    var responseUnits = RestUtils.GetResponse(ConnectRequests.GETUNIT,
                        new GetUnitInput
                        {
                            SkipCount = 0,
                            MaxResultCount = AppConsts.MaxPageSize,
                            Sorting = "CreationTime"
                        });

                    if (responseUnits != null)
                    {
                        var output =
                            JsonConvert.DeserializeObject<PagedResultOutput<UnitListDto>>(responseUnits.ToString());

                        if (output?.Items != null && output.Items.Any())
                        {
                            var allItems = output.Items;
                            rsRemoteUnit = (List<UnitListDto>)allItems;
                            foreach (var remoteunit in allItems)
                            {
                                var alreadyExists =
                                    await _unitrepo.FirstOrDefaultAsync(t => t.SyncId == remoteunit.Id) ??
                                    await _unitrepo.FirstOrDefaultAsync(t => t.Name == remoteunit.Name);

                                var unit = new CreateOrUpdateUnitInput
                                {
                                    Unit = new UnitEditDto
                                    {
                                        Name = remoteunit.Name,
                                        SyncId = remoteunit.Id,
                                        SyncLastModification = DateTime.Now
                                    }
                                };
                                if (alreadyExists == null)
                                {
                                    unit.Unit.Id = null;
                                }
                                else
                                {
                                    unit.Unit.Id = alreadyExists.Id;
                                }
                                await _unitAppService.CreateOrUpdateUnit(unit);
                            }
                        }
                    }

                    #endregion

                    #region SyncUnitConversion

                    var responseUnitConversion = RestUtils.GetResponse(ConnectRequests.GETUNITCONVERSION,
                        new GetUnitConversionInput
                        {
                            SkipCount = 0,
                            MaxResultCount = AppConsts.MaxPageSize,
                            Sorting = "CreationTime"
                        });

                    if (responseUnitConversion != null)
                    {
                        var output =
                            JsonConvert.DeserializeObject<PagedResultOutput<UnitConversionListDto>>(
                                responseUnitConversion.ToString());
                        if (output?.Items != null && output.Items.Any())
                        {
                            var allItems = output.Items;
                            foreach (var remoteuc in allItems)
                            {
                                var remoteBaseunit = rsRemoteUnit.FirstOrDefault(t => t.Id == remoteuc.BaseUnitId);
                                if (remoteBaseunit == null)
                                {
                                    throw new UserFriendlyException(L("Remote") +
                                                                    L("UnitIdDoesNotExist", remoteuc.BaseUnitId));
                                }
                                var localBaseUnit =
                                    await _unitrepo.FirstOrDefaultAsync(t => t.Name.Equals(remoteBaseunit.Name));
                                if (localBaseUnit == null)
                                {
                                    throw new UserFriendlyException(L("Unit") + " " + L("Name") + " : " +
                                                                    remoteBaseunit.Name + L("DoesNotExist"));
                                }

                                var remoteRefUnit = rsRemoteUnit.FirstOrDefault(t => t.Id == remoteuc.RefUnitId);
                                if (remoteRefUnit == null)
                                {
                                    throw new UserFriendlyException(L("Remote") +
                                                                    L("UnitIdDoesNotExist", remoteuc.RefUnitId));
                                }
                                var localRefUnit =
                                    await _unitrepo.FirstOrDefaultAsync(t => t.Name.Equals(remoteRefUnit.Name));
                                if (localRefUnit == null)
                                {
                                    throw new UserFriendlyException(L("Unit") + " " + L("Name") + " : " +
                                                                    remoteRefUnit.Name + L("DoesNotExist"));
                                }

                                var alreadyExists =
                                    await
                                        _unitconversionrepo.FirstOrDefaultAsync(
                                            t => t.BaseUnitId == localBaseUnit.Id && t.RefUnitId == localRefUnit.Id);


                                var unitconerstion = new CreateOrUpdateUnitConversionInput
                                {
                                    UnitConversion = new UnitConversionEditDto
                                    {
                                        BaseUnitId = localBaseUnit.Id,
                                        RefUnitId = localRefUnit.Id,
                                        Conversion = remoteuc.Conversion,
                                        DecimalPlaceRounding = remoteuc.DecimalPlaceRounding,
                                        SyncId = remoteuc.Id,
                                        SyncLastModification = DateTime.Now
                                    },
                                    RecursiveFlag = true
                                };

                                if (alreadyExists == null)
                                {
                                    unitconerstion.UnitConversion.Id = null;
                                }
                                else
                                {
                                    unitconerstion.UnitConversion.Id = alreadyExists.Id;
                                }
                                await _unitconversionAppService.CreateOrUpdateUnitConversion(unitconerstion);
                            }
                        }
                    }

                    #endregion

                    var rsRemoteMaterialGroup = new List<MaterialGroupListDto>();

                    #region SyncMaterialGroup

                    var responseMaterialGroup = RestUtils.GetResponse(ConnectRequests.GETMATERIALGROUP,
                        new GetMaterialGroupInput
                        {
                            SkipCount = 0,
                            MaxResultCount = AppConsts.MaxPageSize,
                            Sorting = "CreationTime"
                        });

                    if (responseMaterialGroup != null)
                    {
                        var output =
                            JsonConvert.DeserializeObject<PagedResultOutput<MaterialGroupListDto>>(
                                responseMaterialGroup.ToString());

                        if (output?.Items != null && output.Items.Any())
                        {
                            var allItems = output.Items;
                            rsRemoteMaterialGroup = (List<MaterialGroupListDto>)allItems;

                            foreach (var remotemc in allItems)
                            {
                                var alreadyExists =
                                    await _materialgroupRepo.FirstOrDefaultAsync(t => t.SyncId == remotemc.Id) ?? await
                                        _materialgroupRepo.FirstOrDefaultAsync(
                                            t => t.MaterialGroupName == remotemc.MaterialGroupName);

                                var materialGroup = new CreateOrUpdateMaterialGroupInput
                                {
                                    MaterialGroup = new MaterialGroupEditDto
                                    {
                                        MaterialGroupName = remotemc.MaterialGroupName,
                                        SyncId = remotemc.Id,
                                        SyncLastModification = DateTime.Now
                                    }
                                };
                                if (alreadyExists == null)
                                {
                                    materialGroup.MaterialGroup.Id = null;
                                }
                                else
                                {
                                    materialGroup.MaterialGroup.Id = alreadyExists.Id;
                                }
                                await _materialGroupAppService.CreateOrUpdateMaterialGroup(materialGroup);
                            }
                        }
                    }

                    #endregion

                    var rsRemoteMaterialGroupCategory = new List<MaterialGroupCategoryViewDto>();

                    #region SyncMaterialGroupCategory

                    var responseMaterialGroupCategory = RestUtils.GetResponse(ConnectRequests.GETMATERIALGROUPCATEGORY,
                        new GetMaterialGroupCategoryInput
                        {
                            SkipCount = 0,
                            MaxResultCount = AppConsts.MaxPageSize,
                            Sorting = "CreationTime"
                        });

                    if (responseMaterialGroupCategory != null)
                    {
                        var output =
                            JsonConvert.DeserializeObject<PagedResultOutput<MaterialGroupCategoryViewDto>>(
                                responseMaterialGroupCategory.ToString());

                        if (output?.Items != null && output.Items.Any())
                        {
                            var allItems = output.Items;
                            rsRemoteMaterialGroupCategory = (List<MaterialGroupCategoryViewDto>)allItems;

                            foreach (var remotemcg in allItems)
                            {
                                var alreadyExists =
                                    await _materialgroupcategoryRepo.FirstOrDefaultAsync(t => t.SyncId == remotemcg.Id);
                                var materialGroup =
                                    rsRemoteMaterialGroup.FirstOrDefault(
                                        t => t.MaterialGroupName == remotemcg.MaterialGroupRefName);
                                if (materialGroup == null)
                                {
                                    throw new UserFriendlyException(L("MaterialGroup") + " " + L("Id") + " " +
                                                                    remotemcg.Id + " " + L("NotExist"));
                                }
                                var localMaterialGroup =
                                    await
                                        _materialgroupRepo.FirstOrDefaultAsync(
                                            t => t.MaterialGroupName.Equals(materialGroup.MaterialGroupName));
                                if (localMaterialGroup == null)
                                {
                                    throw new UserFriendlyException(L("MaterialGroup") + " " + L("Name") + " " +
                                                                    localMaterialGroup.MaterialGroupName + " " +
                                                                    L("NotExist"));
                                }

                                if (alreadyExists == null)
                                {
                                    alreadyExists =
                                        await
                                            _materialgroupcategoryRepo.FirstOrDefaultAsync(
                                                t => t.MaterialGroupCategoryName == remotemcg.MaterialGroupCategoryName);
                                }


                                var materialGroupCategory = new CreateOrUpdateMaterialGroupCategoryInput
                                {
                                    MaterialGroupCategory = new MaterialGroupCategoryEditDto
                                    {
                                        MaterialGroupCategoryName = remotemcg.MaterialGroupCategoryName,
                                        MaterialGroupRefId = localMaterialGroup.Id,
                                        SyncId = remotemcg.Id,
                                        SyncLastModification = DateTime.Now
                                    }
                                };
                                if (alreadyExists == null)
                                {
                                    materialGroupCategory.MaterialGroupCategory.Id = null;
                                }
                                else
                                {
                                    materialGroupCategory.MaterialGroupCategory.Id = alreadyExists.Id;
                                }
                                await
                                    _materialGroupCategoryAppService.CreateOrUpdateMaterialGroupCategory(
                                        materialGroupCategory);
                            }
                        }
                    }

                    #endregion

                    //			var testres = RestUtils.GetResponse(ConnectRequests.GetUserSerialNumberMax,"");

                    var rsRemoteMaterial = new List<MaterialListDto>();

                    var lstMaterialToModify = new List<MaterialListDto>();

                    #region SyncItems

                    var responseMaterials = RestUtils.GetResponse(ConnectRequests.GETMATERIALS,
                        new GetMaterialInput
                        {
                            SkipCount = 0,
                            MaxResultCount = AppConsts.MaxPageSize,
                            Sorting = "CreationTime"
                        });

                    if (responseMaterials != null)
                    {
                        var output =
                            JsonConvert.DeserializeObject<PagedResultOutput<MaterialListDto>>(
                                responseMaterials.ToString());
                        var reccount = 0;
                        if (output?.Items != null && output.Items.Any())
                        {
                            var allItems = output.Items;
                            rsRemoteMaterial = (List<MaterialListDto>)allItems;

                            foreach (var remotemat in allItems)
                            {
                                reccount++;
                                var alreadyExists =
                                    await _materialRepo.FirstOrDefaultAsync(t => t.SyncId == remotemat.Id);
                                if (alreadyExists == null)
                                {
                                    alreadyExists =
                                        await
                                            _materialRepo.FirstOrDefaultAsync(
                                                t => t.MaterialName == remotemat.MaterialName);
                                }

                                DateTime remoteLastModification;
                                if (remotemat.LastModificationTime == null)
                                {
                                    remoteLastModification = remotemat.CreationTime;
                                }
                                else
                                {
                                    remoteLastModification = remotemat.LastModificationTime.Value;
                                }

                                if (alreadyExists != null)
                                {
                                    if (alreadyExists.SyncLastModification != null &&
                                        alreadyExists.SyncLastModification.Value == remoteLastModification)
                                    {
                                        continue;
                                    }
                                    lstMaterialToModify.Add(new MaterialListDto
                                    {
                                        Id = remotemat.Id,
                                        SyncLastModification = remoteLastModification
                                    });
                                }

                                #region NewMaterial

                                var remoteMaterialGroupCategory =
                                    rsRemoteMaterialGroupCategory.FirstOrDefault(
                                        t => t.Id == remotemat.MaterialGroupCategoryRefId);
                                if (remoteMaterialGroupCategory == null)
                                {
                                    throw new UserFriendlyException(L("Remote") + " " + L("MaterialGroupCategory") + " " +
                                                                    L("Id") + remotemat.MaterialGroupCategoryRefId + " " +
                                                                    L("DoesNotExist"));
                                }
                                var localMaterialGroupCategory =
                                    await
                                        _materialgroupcategoryRepo.FirstOrDefaultAsync(
                                            t =>
                                                t.MaterialGroupCategoryName.Equals(
                                                    remoteMaterialGroupCategory.MaterialGroupCategoryName));
                                if (localMaterialGroupCategory == null)
                                {
                                    throw new UserFriendlyException(L("Remote") + " " + L("MaterialGroupCategory") +
                                                                    remoteMaterialGroupCategory
                                                                        .MaterialGroupCategoryName + " " +
                                                                    L("DoesNotExist") + " " + L("In") + L("Local"));
                                }

                                var remoteDefaultUnit = rsRemoteUnit.FirstOrDefault(t => t.Id == remotemat.DefaultUnitId);
                                if (remoteDefaultUnit == null)
                                {
                                    throw new UserFriendlyException(L("Remote") + " " + L("DefaultUnitName") + " " +
                                                                    L("Id") + remotemat.DefaultUnitId + " " +
                                                                    L("DoesNotExist"));
                                }
                                var localDefaultUnit =
                                    await _unitrepo.FirstOrDefaultAsync(t => t.Name.Equals(remoteDefaultUnit.Name));
                                if (localDefaultUnit == null)
                                {
                                    throw new UserFriendlyException(L("Remote") + " " + L("DefaultUnitName") +
                                                                    remoteDefaultUnit.Name + " " + L("DoesNotExist") +
                                                                    " " + L("In") + L("Local"));
                                }

                                var remoteIssueUnit = rsRemoteUnit.FirstOrDefault(t => t.Id == remotemat.IssueUnitId);
                                if (remoteIssueUnit == null)
                                {
                                    throw new UserFriendlyException(L("Remote") + " " + L("IssueUnit") + " " + L("Id") +
                                                                    remotemat.IssueUnitId + " " + L("DoesNotExist"));
                                }
                                var localIssueUnit =
                                    await _unitrepo.FirstOrDefaultAsync(t => t.Name.Equals(remoteIssueUnit.Name));
                                if (localIssueUnit == null)
                                {
                                    throw new UserFriendlyException(L("Remote") + " " + L("IssueUnit") +
                                                                    remoteIssueUnit.Name + " " + L("DoesNotExist") + " " +
                                                                    L("In") + L("Local"));
                                }

                                var barcodes = remotemat.Barcodes;
                                var barcode = remotemat.Barcodes;
                                var barcodeList = new Collection<MaterialBarCodeEditDto>();
                                if (remotemat.Barcodes.Count > 0)
                                {
                                    var a = 1;
                                    foreach (var bc in remotemat.Barcodes)
                                    {
                                        barcodeList.Add(new MaterialBarCodeEditDto { Barcode = bc.Barcode });
                                    }
                                }

                                var material = new CreateOrUpdateMaterialInput
                                {
                                    Material = new MaterialEditDto
                                    {
                                        MaterialTypeId = remotemat.MaterialTypeId,
                                        MaterialName = remotemat.MaterialName,
                                        MaterialPetName = remotemat.MaterialPetName,
                                        MaterialGroupCategoryRefId = localMaterialGroupCategory.Id,
                                        DefaultUnitId = localDefaultUnit.Id,
                                        IssueUnitId = localIssueUnit.Id,
                                        UserSerialNumber = remotemat.UserSerialNumber,
                                        IsFractional = remotemat.IsFractional,
                                        IsBranded = remotemat.IsBranded,
                                        IsQuoteNeededForPurchase = remotemat.IsQuoteNeededForPurchase,
                                        GeneralLifeDays = remotemat.GeneralLifeDays,
                                        OwnPreparation = remotemat.OwnPreparation,
                                        IsNeedtoKeptinFreezer = remotemat.IsNeedtoKeptinFreezer,
                                        MRPPriceExists = remotemat.MRPPriceExists,
                                        PosReferencecodeifany = remotemat.PosReferencecodeifany,
                                        IsMfgDateExists = remotemat.IsMfgDateExists,
                                        WipeOutStockOnClosingDay = remotemat.WipeOutStockOnClosingDay,
                                        IsHighValueItem = remotemat.IsHighValueItem,
                                        //Barcode =  //remotemat.Barcode,
                                        BrandRefId = remotemat.BrandRefId,
                                        Barcodes = remotemat.Barcodes,
                                        Hsncode = remotemat.Hsncode,
                                        IsActive = remotemat.IsActive,
                                        SyncId = remotemat.Id,
                                        SyncLastModification = remoteLastModification
                                    }
                                };
                                if (alreadyExists == null)
                                {
                                    material.Material.Id = null;
                                }
                                else
                                {
                                    material.Material.Id = alreadyExists.Id;
                                }
                                var matid = await _materialAppService.CreateOrUpdateMaterial(material);

                                lstMaterialToModify.Add(new MaterialListDto
                                {
                                    Id = remotemat.Id,
                                    SyncLastModification = remoteLastModification
                                });

                                #endregion
                            }

                            #region EditedMaterials

                            foreach (var materialid in lstMaterialToModify)
                            {
                                var responseMatDto = RestUtils.GetResponse(ConnectRequests.GETMATERIALFOREDIT,
                                    new NullableIdInput
                                    {
                                        Id = materialid.Id
                                    });
                                if (responseMatDto != null)
                                {
                                    var materialDto =
                                        JsonConvert.DeserializeObject<GetMaterialForEditOutput>(
                                            responseMatDto.ToString());
                                    var remotemat = materialDto.Material;

                                    var alreadyExists =
                                        await _materialRepo.FirstOrDefaultAsync(t => t.SyncId == remotemat.Id);
                                    if (alreadyExists == null)
                                    {
                                        alreadyExists =
                                            await
                                                _materialRepo.FirstOrDefaultAsync(
                                                    t => t.MaterialName == remotemat.MaterialName);
                                    }

                                    #region ModifyMaterial

                                    #region Material

                                    var remoteMaterialGroupCategory =
                                        rsRemoteMaterialGroupCategory.FirstOrDefault(
                                            t => t.Id == remotemat.MaterialGroupCategoryRefId);
                                    if (remoteMaterialGroupCategory == null)
                                    {
                                        throw new UserFriendlyException(L("Remote") + " " + L("MaterialGroupCategory") +
                                                                        " " + L("Id") +
                                                                        remotemat.MaterialGroupCategoryRefId + " " +
                                                                        L("DoesNotExist"));
                                    }
                                    var localMaterialGroupCategory =
                                        await
                                            _materialgroupcategoryRepo.FirstOrDefaultAsync(
                                                t =>
                                                    t.MaterialGroupCategoryName.Equals(
                                                        remoteMaterialGroupCategory.MaterialGroupCategoryName));
                                    if (localMaterialGroupCategory == null)
                                    {
                                        throw new UserFriendlyException(L("Remote") + " " + L("MaterialGroupCategory") +
                                                                        remoteMaterialGroupCategory
                                                                            .MaterialGroupCategoryName + " " +
                                                                        L("DoesNotExist") + " " + L("In") + L("Local"));
                                    }

                                    var remoteDefaultUnit =
                                        rsRemoteUnit.FirstOrDefault(t => t.Id == remotemat.DefaultUnitId);
                                    if (remoteDefaultUnit == null)
                                    {
                                        throw new UserFriendlyException(L("Remote") + " " + L("DefaultUnitName") + " " +
                                                                        L("Id") + remotemat.DefaultUnitId + " " +
                                                                        L("DoesNotExist"));
                                    }
                                    var localDefaultUnit =
                                        await _unitrepo.FirstOrDefaultAsync(t => t.Name.Equals(remoteDefaultUnit.Name));
                                    if (localDefaultUnit == null)
                                    {
                                        throw new UserFriendlyException(L("Remote") + " " + L("DefaultUnitName") +
                                                                        remoteDefaultUnit.Name + " " + L("DoesNotExist") +
                                                                        " " + L("In") + L("Local"));
                                    }

                                    var remoteIssueUnit = rsRemoteUnit.FirstOrDefault(t => t.Id == remotemat.IssueUnitId);
                                    if (remoteIssueUnit == null)
                                    {
                                        throw new UserFriendlyException(L("Remote") + " " + L("IssueUnit") + " " +
                                                                        L("Id") + remotemat.IssueUnitId + " " +
                                                                        L("DoesNotExist"));
                                    }
                                    var localIssueUnit =
                                        await _unitrepo.FirstOrDefaultAsync(t => t.Name.Equals(remoteIssueUnit.Name));
                                    if (localIssueUnit == null)
                                    {
                                        throw new UserFriendlyException(L("Remote") + " " + L("IssueUnit") +
                                                                        remoteIssueUnit.Name + " " + L("DoesNotExist") +
                                                                        " " + L("In") + L("Local"));
                                    }

                                    var barcodeList = new Collection<MaterialBarCodeEditDto>();


                                    if (remotemat.Barcodes.Count > 0)
                                    {
                                        var barcodes = remotemat.Barcodes;
                                        var barcode = remotemat.Barcodes;
                                        var a = 1;
                                        foreach (var bc in remotemat.Barcodes)
                                        {
                                            barcodeList.Add(new MaterialBarCodeEditDto { Barcode = bc.Barcode });
                                        }
                                    }
                                    var editMaterial = new MaterialEditDto
                                    {
                                        MaterialTypeId = remotemat.MaterialTypeId,
                                        MaterialName = remotemat.MaterialName,
                                        MaterialPetName = remotemat.MaterialPetName,
                                        MaterialGroupCategoryRefId = localMaterialGroupCategory.Id,
                                        DefaultUnitId = localDefaultUnit.Id,
                                        IssueUnitId = localIssueUnit.Id,
                                        UserSerialNumber = remotemat.UserSerialNumber,
                                        IsFractional = remotemat.IsFractional,
                                        IsBranded = remotemat.IsBranded,
                                        IsQuoteNeededForPurchase = remotemat.IsQuoteNeededForPurchase,
                                        GeneralLifeDays = remotemat.GeneralLifeDays,
                                        OwnPreparation = remotemat.OwnPreparation,
                                        IsNeedtoKeptinFreezer = remotemat.IsNeedtoKeptinFreezer,
                                        MRPPriceExists = remotemat.MRPPriceExists,
                                        PosReferencecodeifany = remotemat.PosReferencecodeifany,
                                        IsMfgDateExists = remotemat.IsMfgDateExists,
                                        WipeOutStockOnClosingDay = remotemat.WipeOutStockOnClosingDay,
                                        IsHighValueItem = remotemat.IsHighValueItem,
                                        //Barcode = remotemat.Barcode,
                                        BrandRefId = remotemat.BrandRefId,
                                        Barcodes = barcodeList,
                                        Hsncode = remotemat.Hsncode,
                                        IsActive = remotemat.IsActive,
                                        SyncId = remotemat.Id,
                                        SyncLastModification = materialid.SyncLastModification
                                    };

                                    #endregion

                                    #region Unit List

                                    var editUnitList = new List<ComboboxItemDto>();
                                    foreach (var ul in materialDto.UnitList)
                                    {
                                        var remoteUnit = rsRemoteUnit.FirstOrDefault(t => t.Id == int.Parse(ul.Value));
                                        if (remoteUnit == null)
                                        {
                                            throw new UserFriendlyException(L("Remote") + " " + L("UnitName") + " " +
                                                                            L("Id") + ul.Value + " " + L("DoesNotExist"));
                                        }
                                        var localUnit =
                                            await _unitrepo.FirstOrDefaultAsync(t => t.Name.Equals(remoteUnit.Name));
                                        if (localUnit == null)
                                        {
                                            throw new UserFriendlyException(L("Remote") + " " + L("UnitName") +
                                                                            remoteUnit.Name + " " + L("DoesNotExist") +
                                                                            " " + L("In") + L("Local"));
                                        }
                                        var newunit = new ComboboxItemDto
                                        {
                                            Value = localUnit.Id.ToString(),
                                            DisplayText = localUnit.Name
                                        };
                                        editUnitList.Add(newunit);
                                    }

                                    #endregion

                                    #region Supplier Material

                                    var editSupplierMaterials = new List<SupplierMaterialEditDto>();
                                    foreach (var sm in materialDto.SupplierMaterial)
                                    {
                                        var remoteSupplier =
                                            rsRemoteSupplier.FirstOrDefault(
                                                t => t.SupplierName.Equals(sm.SupplierRefName));
                                        var localSupplier =
                                            await
                                                _supplierRepo.FirstOrDefaultAsync(
                                                    t => t.SupplierName.Equals(sm.SupplierRefName));
                                        if (localSupplier == null)
                                        {
                                            throw new UserFriendlyException(L("Remote") + " " + L("Supplier") +
                                                                            sm.SupplierRefName + " " + L("DoesNotExist") +
                                                                            " " + L("In") + L("Local"));
                                        }

                                        var remoteUnit = rsRemoteUnit.FirstOrDefault(t => t.Id == sm.UnitRefId);
                                        if (remoteUnit == null)
                                        {
                                            throw new UserFriendlyException(L("Remote") + " " + L("UnitName") + " " +
                                                                            L("Id") + sm.UnitRefId + " " +
                                                                            L("DoesNotExist"));
                                        }
                                        var localUnit =
                                            await _unitrepo.FirstOrDefaultAsync(t => t.Name.Equals(remoteUnit.Name));
                                        if (localUnit == null)
                                        {
                                            throw new UserFriendlyException(L("Remote") + " " + L("UnitName") +
                                                                            remoteUnit.Name + " " + L("DoesNotExist") +
                                                                            " " + L("In") + L("Local"));
                                        }

                                        var supmatdto = new SupplierMaterialEditDto
                                        {
                                            SupplierRefId = localSupplier.Id,
                                            SupplierRefName = localSupplier.SupplierName,
                                            MaterialPrice = sm.MaterialPrice,
                                            LastQuoteDate = sm.LastQuoteDate,
                                            LastQuoteRefNo = sm.LastQuoteRefNo,
                                            MaterialRefId = remotemat.Id.Value,
                                            MaterialRefName = remotemat.MaterialName,
                                            MinimumOrderQuantity = sm.MinimumOrderQuantity,
                                            MaximumOrderQuantity = sm.MaximumOrderQuantity,
                                            UnitRefId = localUnit.Id,
                                            Uom = localUnit.Name
                                        };
                                        editSupplierMaterials.Add(supmatdto);
                                    }

                                    #endregion

                                    var editMaterialRecipeTypeDto = new MaterialRecipeTypesEditDto
                                    {
                                        AlarmFlag = materialDto.MaterialRecipeType.AlarmFlag,
                                        AlarmTimeInMinutes = materialDto.MaterialRecipeType.AlarmTimeInMinutes,
                                        FixedCost = materialDto.MaterialRecipeType.FixedCost,
                                        LifeTimeInMinutes = materialDto.MaterialRecipeType.LifeTimeInMinutes,
                                        PrdBatchQty = materialDto.MaterialRecipeType.PrdBatchQty
                                    };

                                    #region Ingredient Material

                                    var editMaterialIngredients = new List<MaterialIngredientEditDto>();
                                    foreach (var sm in materialDto.MaterialIngredient)
                                    {
                                        var remoteRecipe = rsRemoteMaterial.FirstOrDefault(t => t.Id == sm.RecipeRefId);
                                        if (remoteRecipe == null)
                                        {
                                            throw new UserFriendlyException(L("Remote") + " " + L("Recipe") +
                                                                            sm.RecipeRefId + " " + L("DoesNotExist") +
                                                                            " " + L("In") + L("Local"));
                                        }

                                        var localRecipe =
                                            await
                                                _materialRepo.FirstOrDefaultAsync(
                                                    t => t.MaterialName.Equals(remoteRecipe.MaterialName));
                                        if (localRecipe == null)
                                        {
                                            throw new UserFriendlyException(L("Remote") + " " + L("Recipe") +
                                                                            remoteRecipe.MaterialName + " " +
                                                                            L("DoesNotExist") + " " + L("In") +
                                                                            L("Local"));
                                        }

                                        var remoteMaterial =
                                            rsRemoteMaterial.FirstOrDefault(t => t.Id == sm.MaterialRefId);
                                        if (remoteMaterial == null)
                                        {
                                            throw new UserFriendlyException(L("Remote") + " " + L("Material") +
                                                                            sm.MaterialRefId + " " + L("DoesNotExist") +
                                                                            " " + L("In") + L("Local"));
                                        }

                                        var localMaterial =
                                            await
                                                _materialRepo.FirstOrDefaultAsync(
                                                    t => t.MaterialName.Equals(remoteMaterial.MaterialName));
                                        if (localMaterial == null)
                                        {
                                            throw new UserFriendlyException(L("Remote") + " " + L("Material") +
                                                                            remoteMaterial.MaterialName + " " +
                                                                            L("DoesNotExist") + " " + L("In") +
                                                                            L("Local"));
                                        }

                                        var remoteUnit = rsRemoteUnit.FirstOrDefault(t => t.Id == sm.UnitRefId);
                                        if (remoteUnit == null)
                                        {
                                            throw new UserFriendlyException(L("Remote") + " " + L("UnitName") + " " +
                                                                            L("Id") + sm.UnitRefId + " " +
                                                                            L("DoesNotExist"));
                                        }
                                        var localUnit =
                                            await _unitrepo.FirstOrDefaultAsync(t => t.Name.Equals(remoteUnit.Name));
                                        if (localUnit == null)
                                        {
                                            throw new UserFriendlyException(L("Remote") + " " + L("UnitName") +
                                                                            remoteUnit.Name + " " + L("DoesNotExist") +
                                                                            " " + L("In") + L("Local"));
                                        }

                                        var matingrdto = new MaterialIngredientEditDto
                                        {
                                            RecipeRefId = localRecipe.Id,
                                            RecipeRefName = localRecipe.MaterialName,
                                            MaterialRefId = localMaterial.Id,
                                            MaterialRefName = localMaterial.MaterialName,
                                            UnitRefId = localUnit.Id,
                                            MaterialUsedQty = sm.MaterialUsedQty,
                                            UserSerialNumber = sm.UserSerialNumber,
                                            VariationflagForProduction = sm.VariationflagForProduction
                                        };
                                        editMaterialIngredients.Add(matingrdto);
                                    }

                                    #endregion

                                    var material = new CreateOrUpdateMaterialInput
                                    {
                                        Material = editMaterial,
                                        UnitList = editUnitList,
                                        SupplierMaterial = editSupplierMaterials,
                                        MaterialRecipeType = editMaterialRecipeTypeDto,
                                        UpdateSerialNumberRequired = false,
                                        MaterialIngredient = editMaterialIngredients
                                    };

                                    material.Material.Id = alreadyExists.Id;
                                    var matid = await _materialAppService.CreateOrUpdateMaterial(material);

                                    #endregion
                                }
                            }

                            #endregion
                        }
                    }

                    #endregion

                    #region SyncTaxes

                    var responseTaxes = RestUtils.GetResponse(ConnectRequests.GETTAX,
                        new GetTaxInput
                        {
                            SkipCount = 0,
                            MaxResultCount = AppConsts.MaxPageSize,
                            Sorting = "CreationTime"
                        });

                    if (responseTaxes != null)
                    {
                        var output =
                            JsonConvert.DeserializeObject<PagedResultOutput<TaxListDto>>(responseTaxes.ToString());

                        if (output?.Items != null && output.Items.Any())
                        {
                            var allItems = output.Items;
                            foreach (var remotetax in allItems)
                            {
                                var alreadyExists = await _taxRepo.FirstOrDefaultAsync(t => t.SyncId == remotetax.Id);
                                if (alreadyExists == null)
                                {
                                    alreadyExists =
                                        await _taxRepo.FirstOrDefaultAsync(t => t.TaxName == remotetax.TaxName);
                                }
                                DateTime remoteLastModification;
                                if (remotetax.LastModificationTime == null)
                                {
                                    remoteLastModification = remotetax.CreationTime;
                                }
                                else
                                {
                                    remoteLastModification = remotetax.LastModificationTime.Value;
                                }

                                if (alreadyExists != null)
                                {
                                    if (alreadyExists.SyncLastModification != null &&
                                        alreadyExists.SyncLastModification.Value == remoteLastModification)
                                    {
                                        continue;
                                    }
                                }

                                var taxResponse = RestUtils.GetResponse(ConnectRequests.GETTAXFOREDIT,
                                    new NullableIdInput
                                    {
                                        Id = remotetax.Id
                                    });

                                var taxDtoForEdit =
                                    JsonConvert.DeserializeObject<GetTaxForEditOutput>(taxResponse.ToString());

                                var taxEditDto = taxDtoForEdit.Tax;

                                var editDetailTaxTemplate = new List<TaxTemplateMapping>();
                                foreach (var tt in taxDtoForEdit.TaxTemplateMapping)
                                {
                                    var det = new TaxTemplateMapping
                                    {
                                        CompanyRefId = null,
                                        LocationRefId = null
                                    };

                                    if (tt.MaterialGroupRefId != null)
                                    {
                                        var remoteMaterialGroup =
                                            rsRemoteMaterialGroup.FirstOrDefault(t => t.Id == tt.MaterialGroupRefId);
                                        var localmaterialGroup =
                                            await
                                                _materialgroupRepo.FirstOrDefaultAsync(
                                                    t =>
                                                        t.MaterialGroupName.Equals(remoteMaterialGroup.MaterialGroupName));
                                        if (localmaterialGroup == null)
                                        {
                                            throw new UserFriendlyException(L("Remote") + " " + L("Recipe") +
                                                                            remoteMaterialGroup.MaterialGroupName + " " +
                                                                            L("DoesNotExist") + " " + L("In") +
                                                                            L("Local"));
                                        }
                                        det.MaterialGroupRefId = localmaterialGroup.Id;
                                    }

                                    if (tt.MaterialGroupCategoryRefId != null)
                                    {
                                        var remoteMaterialGroupCategory =
                                            rsRemoteMaterialGroupCategory.FirstOrDefault(
                                                t => t.Id == tt.MaterialGroupCategoryRefId);
                                        var localmaterialGroupCategory =
                                            await
                                                _materialgroupcategoryRepo.FirstOrDefaultAsync(
                                                    t =>
                                                        t.MaterialGroupCategoryName.Equals(
                                                            remoteMaterialGroupCategory.MaterialGroupCategoryName));
                                        if (localmaterialGroupCategory == null)
                                        {
                                            throw new UserFriendlyException(L("Remote") + " " + L("Recipe") +
                                                                            remoteMaterialGroupCategory
                                                                                .MaterialGroupCategoryName + " " +
                                                                            L("DoesNotExist") + " " + L("In") +
                                                                            L("Local"));
                                        }
                                        det.MaterialGroupCategoryRefId = localmaterialGroupCategory.Id;
                                    }

                                    if (tt.MaterialRefId != null)
                                    {
                                        var remoteMaterial =
                                            rsRemoteMaterial.FirstOrDefault(t => t.Id == tt.MaterialRefId);
                                        var localmaterial =
                                            await
                                                _materialRepo.FirstOrDefaultAsync(
                                                    t => t.MaterialName.Equals(remoteMaterial.MaterialName));
                                        if (localmaterial == null)
                                        {
                                            throw new UserFriendlyException(L("Remote") + " " + L("Recipe") +
                                                                            remoteMaterial.MaterialName + " " +
                                                                            L("DoesNotExist") + " " + L("In") +
                                                                            L("Local"));
                                        }
                                        det.MaterialGroupCategoryRefId = localmaterial.Id;
                                    }
                                    editDetailTaxTemplate.Add(det);
                                }

                                var tax = new CreateOrUpdateTaxInput
                                {
                                    Tax = new TaxEditDto
                                    {
                                        TaxName = remotetax.TaxName,
                                        Percentage = remotetax.Percentage,
                                        Rounding = remotetax.Rounding,
                                        SortOrder = remotetax.SortOrder,
                                        TaxCalculationMethod = remotetax.TaxCalculationMethod,
                                        SyncId = remotetax.Id
                                    },
                                    TaxTemplateMapping = editDetailTaxTemplate
                                };

                                if (alreadyExists == null)
                                {
                                    tax.Tax.Id = null;
                                }
                                else
                                {
                                    tax.Tax.Id = alreadyExists.Id;
                                }
                                await _taxAppService.CreateOrUpdateTax(tax);
                            }
                        }
                    }

                    #endregion
                }
            }
        }

        public async Task UpdateMenuAndOrders()
        {
            var groupMenu = _menuItemManager.GetAll().GroupBy(a => a.Name).ToDictionary(x => x.Key, y => y.Count());

            foreach (var allIncreaseMenu in groupMenu.Where(a => a.Value > 1))
            {
                var allItems =
                    _menuItemManager.GetAll()
                        .Where(a => a.Name.Equals(allIncreaseMenu.Key))
                        .OrderByDescending(a => a.Id)
                        .Select(a => a.Id)
                        .ToList();

                var lastItemId = allItems.First();
                var lastItem = _menuItemManager.Get(lastItemId);
                var lastPortion = lastItem.Portions.FirstOrDefault();

                if (lastPortion != null)
                {
                    foreach (var myItem in allItems)
                    {
                        if (myItem != lastItemId)
                        {
                            _dbContext.Database.ExecuteSqlCommand(
                                $"UPDATE ORDERS SET MENUITEMID = {lastItemId} WHERE MENUITEMID = {myItem}");

                            _dbContext.Database.ExecuteSqlCommand(
                                $"UPDATE SCREENMENUITEMS SET MENUITEMID = {lastItemId} WHERE MENUITEMID = {myItem}");

                            _dbContext.Database.ExecuteSqlCommand(
                                $"UPDATE LOCATIONMENUITEMS SET MENUITEMID = {lastItemId} WHERE MENUITEMID = {myItem}");

                            _dbContext.Database.ExecuteSqlCommand(
                                $"UPDATE MENUITEMS SET ISDELETED = 1  WHERE ID = {myItem}");
                        }
                    }
                }
            }


            var cateMenu = _catManager.GetAll().GroupBy(a => a.Name).ToDictionary(x => x.Key, y => y.Count());

            foreach (var allCate in cateMenu.Where(a => a.Value > 1))
            {
                var allItems =
                    _catManager.GetAll()
                        .Where(a => a.Name.Equals(allCate.Key))
                        .OrderByDescending(a => a.Id)
                        .Select(a => a.Id)
                        .ToList();

                var lastItemId = allItems.First();
                var lastItem = _catManager.Get(lastItemId);

                if (lastItem != null)
                {
                    foreach (var myItem in allItems)
                    {
                        if (myItem != lastItemId)
                        {
                            _dbContext.Database.ExecuteSqlCommand(
                                $"UPDATE MENUITEMS SET CATEGORYID = {lastItemId}, IsDeleted=0 WHERE CATEGORYID = {myItem} ");

                            _dbContext.Database.ExecuteSqlCommand(
                                $"UPDATE OrderTagMaps SET CategoryId = {lastItemId} WHERE CategoryId = {myItem}");

                            _dbContext.Database.ExecuteSqlCommand(
                                $"UPDATE Categories SET ISDELETED = 1  WHERE ID = {myItem}");
                        }
                    }
                }
            }
        }

        #endregion

        #region Connect Private Methods

        private void ClearLocation()
        {
            var value = AbpSession.TenantId ?? 0;
            foreach (var sql in _clearLocationSql)
            {
                _dbContext.Database.ExecuteSqlCommand(string.Format(sql, value));
            }
        }

        private void ClearConnect()
        {
            var value = AbpSession.TenantId ?? 0;
            ClearMenu();
            foreach (var sql in _clearConnectOthers)
            {
                _dbContext.Database.ExecuteSqlCommand(string.Format(sql, value));
            }
        }

        public void ClearMenu()
        {
            ClearTransaction();

            var value = AbpSession.TenantId ?? 0;
            foreach (var sql in _clearMenuSql)
            {
                _dbContext.Database.ExecuteSqlCommand(string.Format(sql, value));
            }
        }

        private async Task<bool> SeedSampleCompany()
        {
            var returnStatus = false;
            foreach (var item in _companyNames)
            {
                if (string.IsNullOrWhiteSpace(item)) continue;
                if (item.StartsWith("$")) //Company
                {
                    var companyDto = new Company
                    {
                        Code = item.Trim('$', ' ').Substring(0, 3),
                        Name = item.Trim('$', ' ')
                    };

                    await _companyRepo.InsertOrUpdateAndGetIdAsync(companyDto);
                    returnStatus = true;
                }
            }
            return returnStatus;
        }

        private async Task<bool> SeedSampleLocation(string[] values)
        {
            var returnStatus = false;
            var companyid = 0;
            foreach (var item in values)
            {
                if (item.StartsWith("$"))
                {
                    var companyname = item.Trim('$', ' ');
                    var input = await _companyAppService.GetCompanyIdByName(companyname);
                    companyid = input.Id;
                }
                else if (item.StartsWith("#")) //Location
                {
                    var locationDto = new CreateOrUpdateLocationInput
                    {
                        Location = new LocationEditDto
                        {
                            Name = item.Trim('#', ' '),
                            Code = item.Trim('#', ' ').Substring(0, 3),
                            CompanyRefId = companyid
                        }
                    };
                    await _locationAppService.CreateOrUpdateLocation(locationDto);
                    returnStatus = true;
                }
            }

            return returnStatus;
        }

        private async Task<bool> SeedSampleMenu()
        {
            var countryName = FeatureChecker.GetValue(AppFeatures.ConnectCountry);
            string[] contents = null;

            if (!string.IsNullOrWhiteSpace(countryName) && countryName.Equals("IA"))
            {
                contents = _menuContents_IA;
            }
            else
            {
                contents = _menuContents;
            }
            var returnStatus = false;

            try
            {
                var ds = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                var currentCategory = 0;

                foreach (var item in contents)
                {
                    if (string.IsNullOrWhiteSpace(item)) continue;

                    if (item.StartsWith("#"))
                    {
                        var cateDto = new CreateOrUpdateCategoryInput
                        {
                            Category = new CategoryEditDto
                            {
                                Name = item.Trim('#', ' '),
                                Oid = 1
                            }
                        };
                        currentCategory = await _categoryAppService.CreateOrUpdateCategory(cateDto);
                    }
                    else if (item.Contains("@"))
                    {
                        var parts = item.Split('@');
                        var updateDto = new CreateOrUpdateMenuItemInput { MenuItem = new MenuItemEditDto() };
                        if (parts[0].Contains("|"))
                        {
                            updateDto.MenuItem.Name = parts[0].Split('|')[0].Trim();

                            updateDto.MenuItem.AliasName = parts[0].Split('|')[1].Trim();
                        }
                        else
                        {
                            updateDto.MenuItem.Name = parts[0].Trim();
                        }
                        updateDto.MenuItem.CategoryId = currentCategory;

                        for (var i = 1; i < parts.Length; i++)
                        {
                            if (parts[i] != null && parts[i].Contains(" "))
                            {
                                IList<string> itemPartList = new List<string>(parts[i].Split(' '));
                                var price = ConvertToDecimal(itemPartList[itemPartList.Count - 1], ds);
                                itemPartList.RemoveAt(itemPartList.Count - 1);
                                var itemName = string.Join(" ", itemPartList.ToArray());
                                updateDto.MenuItem.AddPortion(itemName.Trim(), price);
                            }
                        }
                        updateDto.MenuItem.Oid = 1;
                        await _menuItemApplicationService.CreateOrUpdateMenuItem(updateDto);
                    }
                }

                returnStatus = true;
            }
            catch (Exception exception)
            {
                returnStatus = false;
            }
            return returnStatus;
        }

        #endregion

        #region Constants

        private readonly string[] _clearLocationSql =
        {
            "DELETE FROM LOCATIONS WHERE TenantId={0}",
            "DELETE FROM COMPANIES WHERE TenantId={0}",
            "DELETE FROM AbpOrganizationUnits WHERE TenantId={0}"
        };


        private readonly string[] _clearMenuSql =
        {
            "DELETE FROM PRICETAGDEFINITIONS WHERE TagId in (SELECT Id From PRICETAGS where TenantId={0})",
            "DELETE FROM PRICETAGS WHERE TenantId={0}",
            "DELETE FROM LOCATIONMENUITEMS where LocationId in (SELECT Id From Locations where TenantId={0})",
            "DELETE FROM LOCATIONMENUITEMPRICES where LocationId in (SELECT Id From Locations where TenantId={0})",
            "DELETE FROM MENUITEMPORTIONS WHERE MenuItemId in (SELECT Id From MenuItems where TenantId={0})",
            "DELETE FROM MENUITEMS WHERE TenantId={0}",
            "DELETE FROM CATEGORIES WHERE TenantId={0}",
            "DELETE FROM LOCATIONSYNCERS WHERE Syncer_Id in (SELECT Id From SYNCERS where TenantId={0})",
            "DELETE FROM SYNCERS WHERE TenantId={0}",
            "DELETE FROM ScreenMenuCategories where ScreenMenuId in (select Id from ScreenMenus where TenantId ={0})",
            "DELETE FROM ScreenMenuItems where ScreenCategoryId in (select Id from ScreenMenuCategories where ScreenMenuId in (select Id from ScreenMenus where TenantId ={0}))",
            "DELETE FROM ScreenMenus where TenantId ={0} ",
            "DELETE FROM OrderTags where OrderTagGroupId in (select id from OrderTagGroups where TenantId={0})",
            "DELETE FROM OrderTagMaps where OrderTagGroupId in (select id from OrderTagGroups where TenantId={0})",
            "DELETE FROM OrderTagGroups where TenantId ={0}",
            "DELETE FROM TicketTags where TicketTagGroupId in (select id from TicketTagGroups where TenantId={0})",
            "DELETE FROM TicketTagGroups where TenantId ={0}"
        };

        private readonly string[] _clearConnectOthers =
        {
            "DELETE FROM DINEPLANTAXMAPPINGS WHERE DINEPLANTAXLOCATIONREFID=(SELECT ID FROM DINEPLANTAXLOCATIONS WHERE DINEPLANTAXREFID= (SELECT ID FROM DINEPLANTAXES WHERE TENANTID={0}))",
            "DELETE FROM DINEPLANTAXLOCATIONS WHERE DINEPLANTAXREFID=(SELECT ID FROM DINEPLANTAXES WHERE TENANTID={0})",
            "DELETE FROM DINEPLANTAXES WHERE TENANTID={0}",
            "DELETE FROM DINEPLANUSERROLES WHERE TenantId={0}",
            "DELETE FROM DINEPLANUSERS WHERE TenantId={0}"
        };

        private readonly string[] _clearTransactions =
        {
            "DELETE FROM TICKETTRANSACTIONS WHERE TicketId in (SELECT Id From dbo.Tickets where TenantId={0})",
            "DELETE FROM PAYMENTS WHERE TicketId in (SELECT Id From Tickets where TenantId={0})",
            "DELETE FROM TRANSACTIONORDERTAGS  WHERE OrderId in (SELECT Id From Orders where TicketId in (select Id from dbo.Tickets where TenantId={0}))",
            "DELETE FROM ORDERS  WHERE TicketId in (SELECT Id From Tickets where TenantId={0})",
            "DELETE FROM dbo.Tickets WHERE TenantId={0}",
            "DELETE FROM TillTransactions WHERE TenantId={0}",
            "DELETE FROM WORKPERIODS WHERE TenantId={0}"
        };


        private readonly string[] _clearMaterialSql =
        {
            "DELETE FROM MaterialIngredients  WHERE MaterialRefId IN (SELECT ID FROM  Materials WHERE TenantId={0}) ",
            "DELETE FROM CustomerTagWiseMaterialPrices  WHERE MaterialRefId IN (SELECT ID FROM  Materials WHERE TenantId={0}) ",
            "DELETE FROM MATERIALLEDGERS WHERE MaterialRefId IN (SELECT ID FROM  Materials WHERE TenantId={0}) ",
            "DELETE FROM SUPPLIERMATERIALS  WHERE MaterialRefId IN (SELECT ID FROM  Materials WHERE TenantId={0}) ",
            "DELETE FROM MaterialLocationWiseStocks  WHERE MaterialRefId IN (SELECT ID FROM  Materials WHERE TenantId={0})",
            "DELETE FROM MaterialUnitLinks WHERE MaterialRefId IN (SELECT ID FROM  Materials WHERE TenantId={0}) ",
            "DELETE FROM CUSTOMERMATERIALS WHERE MaterialRefId IN (SELECT ID FROM  Materials WHERE TenantId={0}) ",
            "DELETE FROM MATERIALMENUMAPPINGS ",
            "DELETE FROM MATERIALRECIPETYPES WHERE MaterailRefId IN (SELECT ID FROM  Materials WHERE TenantId={0})",
            "DELETE FROM Materials WHERE TenantId={0}",
            "DELETE FROM MATERIALGROUPCATEGORIES WHERE MaterialGroupRefId IN (SELECT ID FROM MATERIALGROUPS  WHERE TenantId={0}) ",
            "DELETE FROM MATERIALGROUPS  WHERE TenantId={0}"
        };

        private readonly string[] _clearMaterialTransactionSql =
        {
            "DELETE FROM MATERIALLEDGERS WHERE MaterialRefId IN (SELECT ID FROM  Materials WHERE TenantId={0}) ",
            "DELETE FROM MaterialLocationWiseStocks  WHERE MaterialRefId IN (SELECT ID FROM  Materials WHERE TenantId={0})"
        };

        private readonly string[] _clearSupplierSql =
        {
            "DELETE FROM SUPPLIERS WHERE TenantId={0}"
        };

        private readonly string[] _clearTaxSql =
        {
            "DELETE FROM TAXTEMPLATEMAPPINGS WHERE TAXREFID IN (SELECT ID FROM TAXES WHERE TenantId={0})",
            "DELETE FROM TAXES WHERE TenantId={0} "
        };

        public string[] ClearCustomerSQL =
        {
            "DELETE FROM CUSTOMERS WHERE TenantId={0}"
        };


        private readonly string[] _clearRecipeSql =
        {
            "DELETE FROM RECIPEINGREDIENTS WHERE RecipeRefId (SELECT ID FROM RECIPES WHERE RecipeGroupRefId IN (SELECT ID FROM RECIPEGROUPS WHERE TenantId={0}))",
            "DELETE FROM PRODUCTRECIPELINKS WHERE RecipeRefId (SELECT ID FROM RECIPES WHERE RecipeGroupRefId IN (SELECT ID FROM RECIPEGROUPS WHERE TenantId={0}))",
            "DELETE FROM RECIPES WHERE RecipeGroupRefId IN (SELECT ID FROM RECIPEGROUPS WHERE TenantId={0}) ",
            "DELETE FROM RECIPEGROUPS WHERE TenantId={0}"
        };


        private readonly string[] _clearInvoiceSql =
        {
            "DELETE FROM YieldInput WHERE YIELDREFID IN (SELECT ID FROM YIELD WHERE TENANTID = {0}) ",
            "DELETE FROM YieldOutput WHERE YIELDREFID IN (SELECT ID FROM YIELD WHERE TENANTID = {0}) ",
            "DELETE FROM Yield WHERE TENANTID = {0} ",
            "DELETE FROM ReturnDetails WHERE ReturnRefId IN (SELECT ID FROM Returns WHERE TENANTID = {0} ) ",
            "DELETE FROM Returns WHERE TENANTID = {0} ",
            "DELETE FROM InterTransferReceivedDetails WHERE InterTransferRefId IN (SELECT ID FROM InterTransfers WHERE TENANTID = {0}) ",
            "DELETE FROM InterTransferDetails WHERE InterTransferRefId IN (SELECT ID FROM InterTransfers WHERE TENANTID = {0}) ",
            "DELETE FROM InterTransfers WHERE TENANTID = {0} ",
            "DELETE FROM IssueDetails WHERE IssueRefId IN (SELECT ID FROM Issues WHERE TENANTID = {0}) ",
            "DELETE FROM Issues WHERE TENANTID = {0} ",
            "DELETE FROM IssueDetails WHERE IssueRefId IN (SELECT ID FROM Issues WHERE TENANTID = {0}) ",
            "DELETE FROM Issues WHERE TENANTID = {0} ",
            "DELETE FROM ProductionDetails WHERE ProductionRefId IN (SELECT ID FROM Productions WHERE TENANTID = {0}) ",
            "DELETE FROM Productions WHERE TENANTID = {0} ",
            "DELETE FROM MenuItemWastageDetails WHERE MenuItemWastageRefId IN (SELECT ID FROM MenuItemWastages WHERE TenantId = {0} )",
            "DELETE FROM MenuItemWastages WHERE TenantId = {0} ",
            "DELETE FROM AdjustmentDetails WHERE AdjustmentRefIf IN (SELECT ID FROM Adjustments WHERE TENANTID = {0}) ",
            "DELETE FROM Adjustments WHERE TENANTID = {0} ",
            "DELETE FROM PurchaseOrderTaxDetails WHERE PoRefId IN (SELECT ID FROM PurchaseOrders WHERE TENANTID = {0}) ",
            "DELETE FROM PurchaseOrderDetails WHERE PoRefId IN (SELECT ID FROM PurchaseOrders WHERE TENANTID = {0}) ",
            "DELETE FROM PurchaseOrders WHERE TENANTID = {0} ",
            "DELETE FROM InvoiceDirectCreditLink WHERE InwardDirectCreditRefId IN (SELECT ID FROM INWARDDIRECTCREDITS WHERE TenantId={0})",
            "DELETE FROM InvoiceTaxDetails WHERE InvoiceRefId IN (SELECT ID FROM InvoiceDetails  WHERE TenantId={0})",
            "DELETE FROM InvoiceDetails WHERE TenantId={0}",
            "DELETE FROM INWARDDIRECTCREDITDETAILS WHERE DcRefId IN (SELECT ID FROM INWARDDIRECTCREDITS WHERE TenantId={0})",
            "DELETE FROM INWARDDIRECTCREDITS WHERE TenantId={0}",
            "DELETE FROM INVOICETAXDETAILS WHERE InvoiceRefId IN (SELECT ID FROM INVOICES WHERE TenantId={0}) ",
            "DELETE FROM InvoiceDetails WHERE InvoiceRefId IN (SELECT ID FROM INVOICES WHERE TenantId={0}) ",
            "DELETE FROM INVOICES WHERE TenantId={0}"
        };

        private readonly string[] _clearHouseTransactionSql =
        {
            "DELETE FROM YieldInput WHERE YIELDREFID IN (SELECT ID FROM YIELD WHERE TENANTID = {0}) ",
            "DELETE FROM YieldOutput WHERE YIELDREFID IN (SELECT ID FROM YIELD WHERE TENANTID = {0}) ",
            "DELETE FROM Yield WHERE TENANTID = {0} ",

            "DELETE FROM ReturnDetails WHERE ReturnRefId IN (SELECT ID FROM Returns WHERE TENANTID = {0} ) ",
            "DELETE FROM Returns WHERE TENANTID = {0} ",

            "DELETE FROM InterTransferReceivedDetails WHERE InterTransferRefId IN (SELECT ID FROM InterTransfers WHERE TENANTID = {0}) ",
            "DELETE FROM InterTransferDetails WHERE InterTransferRefId IN (SELECT ID FROM InterTransfers WHERE TENANTID = {0}) ",
            "DELETE FROM InterTransfers WHERE TENANTID = {0} ",

            "DELETE FROM ProductionDetails WHERE ProductionRefId IN (SELECT ID FROM Productions WHERE TENANTID = {0}) ",
            "DELETE FROM Productions WHERE TENANTID = {0} ",

            "DELETE FROM IssueDetails WHERE IssueRefId IN (SELECT ID FROM Issues WHERE TENANTID = {0}) ",
            "DELETE FROM Issues WHERE TENANTID = {0} ",

            "DELETE FROM RequestDetails WHERE RequestRefId IN (SELECT ID FROM Requests WHERE TENANTID = {0}) ",
            "DELETE FROM Requests WHERE TENANTID = {0} ",

            "DELETE FROM AdjustmentDetails WHERE AdjustmentRefIf IN (SELECT ID FROM Adjustments WHERE TENANTID = {0}) ",
            "DELETE FROM Adjustments WHERE TENANTID = {0} ",

            "DELETE FROM INVOICETAXDETAILS WHERE InvoiceRefId IN (SELECT ID FROM INVOICES WHERE TenantId={0}) ",
            "DELETE FROM InvoiceDetails WHERE InvoiceRefId IN (SELECT ID FROM INVOICES WHERE TenantId={0}) ",
            "DELETE FROM InvoiceDirectCreditLink WHERE InwardDirectCreditRefId IN (SELECT ID FROM INWARDDIRECTCREDITS WHERE TenantId={0})",
            "DELETE FROM InvoiceTaxDetails WHERE InvoiceRefId IN (SELECT ID FROM InvoiceDetails  WHERE TenantId={0})",
            "DELETE FROM InvoiceDetails WHERE TenantId={0}",
            "DELETE FROM INVOICES WHERE TenantId={0}",

            "DELETE FROM INWARDDIRECTCREDITDETAILS WHERE DcRefId IN (SELECT ID FROM INWARDDIRECTCREDITS WHERE TenantId={0})",
            "DELETE FROM INWARDDIRECTCREDITS WHERE TenantId={0}",

            "DELETE FROM PurchaseOrderTaxDetails WHERE PoRefId IN (SELECT ID FROM PurchaseOrders WHERE TENANTID = {0}) ",
            "DELETE FROM PurchaseOrderDetails WHERE PoRefId IN (SELECT ID FROM PurchaseOrders WHERE TENANTID = {0}) ",
            "DELETE FROM PurchaseOrders WHERE TENANTID = {0} ",
            "DELETE FROM MATERIALLEDGERS WHERE MaterialRefId IN (SELECT ID FROM  Materials WHERE TenantId={0}) ",
            "UPDATE MaterialLocationWiseStocks SET CurrentInHand = 0 , ISORDERPLACED = 0 WHERE MaterialRefId IN (SELECT ID FROM  Materials WHERE TenantId={0})",
            "UPDATE Locations set HouseTransactionDate=Convert(date, getdate())  where TenantId={0}"
        };


        private readonly string[] _clearInwardDirectCreditSql =
        {
            "DELETE FROM InvoiceDirectCreditLink WHERE InwardDirectCreditRefId IN (SELECT ID FROM INWARDDIRECTCREDITS WHERE TenantId={0})",
            "DELETE FROM InvoiceTaxDetails WHERE InvoiceRefId IN (SELECT ID FROM InvoiceDetails  WHERE TenantId={0})",
            "DELETE FROM InvoiceDetails WHERE TenantId={0}",
            "DELETE FROM INWARDDIRECTCREDITDETAILS WHERE DcRefId IN (SELECT ID FROM INWARDDIRECTCREDITS WHERE TenantId={0})",
            "DELETE FROM INWARDDIRECTCREDITS WHERE TenantId={0}"
        };

        public string[] InwardDirectCreditContents =
        {
            "#2016-05-13,KALIFAS,SHENG SIONG PTE LTD,GRN1,@ALL PURPOSE FLOUR,250,KG,10.50,@CAYENNE PEPPER,43,KG,15.00,@SALT,15.00,KG,15.00,@BUTTER,150.00,KG,15.00,@FISH FILLETS COD,125.00,KG,15.00,@POTATO FLAKES,90.00,KG,15.00,@BLACK PEPPER,105,KG,15.00,@MOZZARELLA CHEESE,100,KG,15.00,@CHICKEN,100,KG,15.00,@BACON,100,KG,15.00,@OLIVE OIL,155,KG,15.00,@MUSHROOMS,155,KG,15.00,@COOKING OIL,155,KG,15.00,@BAKING POWDER,155,KG,15.00",
            "#2016-05-15,KALIFAS,FAIRPRICE PTE LTD,GRNCHO21,@ALL PURPOSE FLOUR,75,KG,10.50,@CAYENNE PEPPER,15,KG,15.00,@SALT,3.50,KG,15.00,@BUTTER,120.00,KG,15.00,@FISH FILLETS COD,175.00,KG,15.00,@POTATO FLAKES,75.00,KG,15.00,@BLACK PEPPER,75,KG,15.00,@MOZZARELLA CHEESE,100,KG,15.00,@CHICKEN,100,KG,15.00,@BACON,100,KG,15.00,@OLIVE OIL,155,KG,15.00,@MUSHROOMS,155,KG,15.00,@COOKING OIL,155,KG,15.00,@BAKING POWDER,155,KG,15.00",
            "#2016-05-15,KALIFAS,FAIRPRICE PTE LTD,GRNCHO1,@ALL PURPOSE FLOUR,75,KG,10.50,@BUTTER,23.00,KG,15.00,@FISH FILLETS COD,10.00,KG,15.00",
            "#2016-05-13,INDO CHILLI,SHENG SIONG PTE LTD,GRN2,@ALL PURPOSE FLOUR,275,KG,10.50,@CAYENNE PEPPER,35,KG,15.00,@SALT,15.00,KG,15.00,@BUTTER,146.00,KG,15.00,@FISH FILLETS COD,125.00,KG,15.00,@POTATO FLAKES,90.00,KG,15.00,@BLACK PEPPER,78,KG,15.00,@MOZZARELLA CHEESE,52,KG,15.00,@CHICKEN,100,KG,15.00,@BACON,100,KG,15.00,@OLIVE OIL,155,KG,15.00,@MUSHROOMS,155,KG,15.00,@COOKING OIL,155,KG,15.00,@BAKING POWDER,155,KG,15.00",
            "#2016-05-15,INDO CHILLI,FAIRPRICE PTE LTD,GRNCHO22,@ALL PURPOSE FLOUR,85,KG,10.50,@CAYENNE PEPPER,5,KG,15.00,@SALT,3.50,KG,15.00,@BUTTER,110.00,KG,15.00,@FISH FILLETS COD,175.00,KG,15.00,@POTATO FLAKES,75.00,KG,15.00,@BLACK PEPPER,98,KG,15.00,@MOZZARELLA CHEESE,85,KG,15.00,@CHICKEN,100,KG,15.00,@BACON,100,KG,15.00,@OLIVE OIL,155,KG,15.00,@MUSHROOMS,155,KG,15.00,@COOKING OIL,155,KG,15.00,@BAKING POWDER,155,KG,15.00",
            "#2016-05-15,INDO CHILLI,JASONS GROUP LIMITED,GRNCHO1,@ALL PURPOSE FLOUR,36,KG,10.50,@BUTTER,18.00,KG,15.00,@FISH FILLETS COD,10.00,KG,15.00,@BLACK PEPPER,18,KG,15.00,@MOZZARELLA CHEESE,100,KG,15.00,@CHICKEN,100,KG,15.00,@BACON,100,KG,15.00,@OLIVE OIL,155,KG,15.00,@MUSHROOMS,155,KG,15.00,@COOKING OIL,155,KG,15.00,@BAKING POWDER,155,KG,15.00",
            "#2016-05-13,YAYOI,SHENG SIONG PTE LTD,GRN3,@ALL PURPOSE FLOUR,826888,KG,10.50,@CAYENNE PEPPER,33,KG,15.00,@SALT,55.00,KG,15.00,@BUTTER,187.00,KG,15.00,@FISH FILLETS COD,125.00,KG,15.00,@POTATO FLAKES,90.00,KG,15.00,@BLACK PEPPER,100,KG,15.00,@MOZZARELLA CHEESE,100,KG,15.00,@CHICKEN,100,KG,15.00,@BACON,100,KG,15.00,@OLIVE OIL,155,KG,15.00,@MUSHROOMS,155,KG,15.00,@COOKING OIL,155,KG,15.00,@BAKING POWDER,155,KG,15.00",
            "#2016-05-15,YAYOI,FAIRPRICE PTE LTD,GRNCHO23,@ALL PURPOSE FLOUR,46,KG,10.50,@CAYENNE PEPPER,75,KG,15.00,@SALT,34.50,KG,15.00,@BUTTER,365.00,KG,15.00,@FISH FILLETS COD,175.00,KG,15.00,@POTATO FLAKES,75.00,KG,15.00,@BLACK PEPPER,100,KG,15.00,@MOZZARELLA CHEESE,100,KG,15.00,@CHICKEN,100,KG,15.00,@BACON,100,KG,15.00,@OLIVE OIL,155,KG,15.00,@MUSHROOMS,55,KG,15.00,@COOKING OIL,155,KG,15.00,@BAKING POWDER,155,KG,15.00",
            "#2016-05-15,YAYOI,JASONS GROUP LIMITED,GRNCHO1,@ALL PURPOSE FLOUR,86,KG,10.50,@BUTTER,23.00,KG,15.00,@FISH FILLETS COD,10.00,KG,15.00",
            "#2016-05-13,LESTEAK,SHENG SIONG PTE LTD,GRN4,@ALL PURPOSE FLOUR,213,KG,10.50,@CAYENNE PEPPER,33,KG,15.00,@SALT,8.00,KG,15.00,@BUTTER,148.00,KG,15.00,@FISH FILLETS COD,125.00,KG,15.00,@POTATO FLAKES,90.00,KG,15.00,@BLACK PEPPER,18,KG,15.00,@MOZZARELLA CHEESE,100,KG,15.00,@CHICKEN,100,KG,15.00,@BACON,100,KG,15.00,@OLIVE OIL,155,KG,15.00,@MUSHROOMS,45,KG,15.00,@COOKING OIL,155,KG,15.00,@BAKING POWDER,155,KG,15.00",
            "#2016-05-15,LESTEAK,FAIRPRICE PTE LTD,GRNCHO1,@ALL PURPOSE FLOUR,65,KG,10.50,@CAYENNE PEPPER,65,KG,15.00,@SALT,3.50,KG,15.00,@BUTTER,35.00,KG,15.00,@FISH FILLETS COD,175.00,KG,15.00,@POTATO FLAKES,75.00,KG,15.00,@BLACK PEPPER,103,KG,15.00,@MOZZARELLA CHEESE,100,KG,15.00,@CHICKEN,100,KG,15.00,@BACON,100,KG,15.00,@OLIVE OIL,155,KG,15.00,@MUSHROOMS,155,KG,15.00,@COOKING OIL,155,KG,15.00,@BAKING POWDER,155,KG,15.00",
            "#2016-05-15,LESTEAK,JASONS GROUP LIMITED,GRNCHO1,@ALL PURPOSE FLOUR,65,KG,10.50,@BUTTER,12.00,KG,15.00,@FISH FILLETS COD,10.00,KG,15.00,@BLACK PEPPER,105,KG,15.00,@MOZZARELLA CHEESE,100,KG,15.00,@CHICKEN,100,KG,15.00,@BACON,100,KG,15.00,@OLIVE OIL,155,KG,15.00,@MUSHROOMS,155,KG,15.00,@COOKING OIL,155,KG,15.00,@BAKING POWDER,155,KG,15.00"
        };

        public string[] InvoiceContents =
        {
            "#2016-01-13,KALIFAS,SHENG SIONG PTE LTD,CREDIT,000160,0,0,89711.00,89710.75,@PISTA,39.998,1248,49920,0,0,0,0,57158.4"
        };

        private readonly string[] _companyNames =
        {"$COFFEE CLUB", "$JAMIES"};

        private readonly string[] _locationNames =
        {
            "$JAMIES",
            "#BEDOK",
            "#OASIS TOWER",
            "#JURONG",
            "$COFFEE CLUB",
            "#DEIRA CENTER",
            "#ALQASIS"
        };

        private readonly string[] _dcHeadMasterNames = { "$FOOD PRODUCTION", "$FRONT OFFICE", "$LOGISTICS" };

        private readonly string[] _jobTitles = { "$Manager", "$Steward", "$Cook", "$Coffee Maker", "$Dosa Maker", "$Driver", "$Cashier" };

        private readonly string[] _dcSkillSetNames =
        {
            "$MANAGER", "$SUPERVISOR", "$STEWARD", "$CASHIER", "$COOK GRADE I", "$COOK GRADE II", "$COFFEE", "$DOSA", "$DRIVER" , "$BAKERY"
        };

        private readonly string[] _dcGroupMasterNames =
        {
            "$FOOD PRODUCTION",
            "#LEVEL 1 KITCHEN",
            "#LEVEL 2 KITCHEN",
            "#CONFECTIONARY",
            "#DOSA",
            "$FRONT OFFICE",
            "#MANAGER",
            "#SUPERVISION",
            "#CUSTOMER CARE",
            "#CASH HANDLING",
            "$LOGISTICS",
            "#DRIVER"
        };


        private readonly string[] _dcGroupVsSkillNames =
        {
            "#LEVEL 1 KITCHEN, COOK GRADE I, COOK GRADE II, BAKERY  ",
            "#LEVEL 2 KITCHEN, COOK GRADE I, COOK GRADE II  ",
            "#DOSA, DOSA",
            "#CONFECTIONARY, BAKERY",
            "#MANAGER, MANAGER",
            "#SUPERVISION, SUPERVISOR",
            "#CUSTOMER CARE, STEWARD, WAITER",
            "#CASH HANDLING, CASHIER, BILL CLERK",
            "#DRIVER, DRIVER"
        };

        private readonly string[] _clearTickSql =
      {
            "DELETE FROM BioMetricExcludedEntries WHERE TenantId={0} ",
            "DELETE FROM EmployeeVsSalaryTags WHERE TenantId={0} ",
            "DELETE FROM DutyChartDetails WHERE TenantId={0} ",
            "DELETE FROM DutyCharts WHERE TenantId={0} ",
            "DELETE FROM EmployeeDefaultWorkStations WHERE TenantId={0} ",
            "DELETE FROM DcShiftMasters WHERE TenantId={0}",
            "DELETE FROM DcStationMasters WHERE TenantId={0}",
            "DELETE FROM DcGroupMasterVsSkillSets WHERE TenantId={0}",
            "DELETE FROM DcGroupMasters WHERE TenantId={0}",
            "DELETE FROM DcHeadMasters WHERE TenantId={0}",
            "DELETE FROM LeaveRequests WHERE TenantId={0}",
            "DELETE FROM SalaryInfos WHERE TenantId={0}",
            "DELETE FROM LeaveExhaustedLists WHERE TenantId={0}",
            "DELETE FROM YearWiseLeaveAllowedForEmployees WHERE TenantId={0}",
            "DELETE FROM LeaveTypes WHERE TenantId={0}",
            "DELETE FROM EmployeeMailMessages WHERE TenantId={0}",
            "DELETE FROM CommonMailMessages WHERE TenantId={0}",
            "DELETE FROM UserDefaultInformations WHERE TenantId={0}",
            "DELETE FROM AttendanceLogs WHERE TenantId={0}",
            "DELETE FROM AttendanceMachineLocations WHERE TenantId={0}",
            "DELETE FROM EmployeeDocumentInfos WHERE TenantId={0}",
            "DELETE FROM DocumentForSkillSets WHERE TenantId={0}",
            "DELETE FROM DocumentInfos WHERE TenantId={0}",
            "DELETE FROM EmployeeSkillSets WHERE TenantId={0}",
            "DELETE FROM PersonalInformations WHERE TenantId={0}",
            "DELETE FROM SkillSets WHERE TenantId={0}",
            "DELETE FROM EMailLinkedWithAction ",
            "DELETE FROM JobTitleMasters Where TenantId={0}"
        };


        private readonly string[] _dcStationMasters =
        {
            //    //[LocationRefId],[StationName], [DcGroupRefId], [MaximumStaffRequired], [MinimumStaffRequired], [PriorityLevel], [IsMandatory], [IsActive], [NightFlag], [DefaultTimeFlag], [TimeIn], [BreakFlag], [BreakOut], [BreakIn], [BreakMinutes], [TimeOut], [TimeDescription]

            "BEDOK,COFFEE LEVEL 1,LEVEL 1 KITCHEN, 1, 1,1.00,1,1,0,0,360, 0, 0, 0, 0, 1350,IN : 6:00 - OUT : 22:30 ",
            "BEDOK,MEALS PRODUCTION,LEVEL 2 KITCHEN, 1, 1, 2.00, 1, 1, 0, 0, 420, 0, 0, 0, 0, 900, IN : 7:00 - OUT : 15:00 ",
            "BEDOK, TIFFIN PRODUCTION, LEVEL 1 KITCHEN, 1, 1, 3.00, 1, 1, 0, 1, 240, 1, 600, 960, 0, 1320, IN : 4:00 - BO : 10:00 - BI : 16:00 - OUT : 22:00 ",
            "BEDOK, MEALS PRODUCTION ASST, LEVEL 2 KITCHEN, 1, 1, 1.00, 1, 1, 0, 0, 360, 0, 0, 0, 0, 900, IN : 6:00 - OUT : 15:00 ",
            "BEDOK, MANAGER, MANAGER, 1, 1, 6.00, 1, 1, 0, 0, 600, 1, 900, 1140, 0, 1320, IN : 10:00 - BO : 15:00 - BI : 19:00 - OUT : 22:00, ",
            "BEDOK, DOSA, DOSA, 2, 2, 7.00, 1, 1, 0, 0, 360, 0, 0, 0, 0, 1380, IN : 6:00 - OUT : 23:00",
            "BEDOK, SUPERVISOR,CUSTOMER CARE, 1, 1, 8.00, 1, 1, 0, 0, 420, 0, 0, 0, 0, 1380, IN : 7:00 - OUT : 23:00",
            "BEDOK, STEWARD FIRST FLOOR, CUSTOMER CARE, 1, 1, 8.00 , 1, 1, 0, 0, 420, 0, 0, 0, 0, 1380, IN : 7:00 - OUT : 23:00",
            "BEDOK, STEWARD 2ND FLOOR, CUSTOMER CARE, 1, 1, 10.00 , 1, 1, 0, 0, 540, 1, 900, 1140, 0, 1380, IN : 9:00 - BO : 15:00 - BI : 19:00 - OUT : 23:00",
            "BEDOK, CASHIER 1ST FLOOR, CASH HANDLING, 1, 1, 12.00 , 1, 1, 0, 0, 420, 0, 0, 0, 0, 1380, IN : 7:00 - OUT : 23:00",
            "BEDOK, CASHIER 2ND FLOOR, CASH HANDLING, 1, 1, 13.00 , 1, 1, 0, 0, 540, 1, 900, 1140, 0, 1380, IN : 9:00 - BO : 15:00 - BI : 19:00 - OUT : 23:00",
            "BEDOK, BUS, DRIVER, 2, 2, 1.00, 1, 1, 0, 0, 300, 0, 0, 0, 0, 1380, IN : 5:00 - OUT : 23:00"

        };

        private readonly string[] _dcShiftMastersData =
{
        //Location  | Duty Desc         | Station               | Max| Min | Prio | IsMand | Night | TimeIn | BF | BreakOut | BreakIn | BrkMins | TimeOut | TimeDescription,

        "BEDOK,     6:00 - 14:30,       COFFEE LEVEL 1,            1,   1,  1.00,   1,        0,        360,  0,    0,          0,          0,      870,   IN : 6:00 - OUT : 14:30",
        "BEDOK,     14:30 - 22:30,      COFFEE LEVEL 1,            1,   1,  2.00,   1,        0,        870,  0,    0,          0,          0,      1350,  IN : 14:30 - OUT : 22:30",
        "BEDOK,     7:00 - 15:00,       MEALS PRODUCTION,          1,   1,  2.00,   1,        0,        420,  0,    0,          0,          0,      900,   IN : 7:00 - OUT : 15:00",
        "BEDOK,     4:00 - BO : 10:00 - BI : 14:00 - 20:00,      " +
                    "                   MEALS PRODUCTION,          1,   1,  2.00,   1,        0,        240,  1,    600,       840,         0,      1200,  IN : 4:00 - BO : 10:00 - BI : 14:00 - OUT : 20:00",
        "BEDOK,     6:00 - 14:30 DOSA,  DOSA,                      1,   1,  2.00,   1,        0,        360,  0,    0,          0,          0,      870,   IN : 6:00 - OUT : 14:30",
        "BEDOK,     14:30 - 22:30 DOSA, DOSA,                      1,   1,  2.00,   1,        0,        870,  0,    0,          0,          0,      1350,  IN : 14:30 - OUT : 22:30",
        "BEDOK,     10:00 - BO : 15:00 - BI : 19:00 - 23:00, MANAGER,1, 1,  2.00,   1,        0,        600,  1,  900,       1140,          0,      1380,  IN : 10:00 - BO : 15:00 - BI : 19:00 - OUT : 23:00",

        "BEDOK,     6:00 - BO : 10:00 - BI : 15:00 - 20:00,  SUPERVISOR,1, 1,  2.00, 1,     0,        360,  1,  600,        900,         0,      1200,  IN : 6:00 - BO : 10:00 - BI : 15:00 - OUT : 20:00",

        "BEDOK,  10:00 - BO : 15:00 - BI : 19:00 - 23:00,  SUPERVISOR,1, 1,  2.00, 1,    0,         600, 1, 900,         1140,          0,     1380,  IN : 10:00 - BO : 15:00 - BI : 19:00 - OUT : 23:00",

        "BEDOK,  7:01 - BO : 10:00 - BI : 15:00 - 20:00,  STEWARD FIRST FLOOR,1, 1,  2.00, 1,    0,   421, 1, 600,        900,           0,     1200,  IN : 7:01 - BO : 10:00 - BI : 15:00 - OUT : 20:00",

        "BEDOK,  10:00 - BO : 15:00 - BI : 19:00 - 23:00 STEWARD, STEWARD FIRST FLOOR,1, 1, 2.00, 1, 0, 600, 1, 900, 1140, 0, 1380,  IN : 10:00 - BO : 15:00 - BI : 19:00 - OUT : 23:00",

         "BEDOK,  9:00 - BO : 15:00 - BI : 19:00 - 23:00, STEWARD 2ND FLOOR,1, 1, 2.00, 1, 0, 540, 1, 900, 1140, 0, 1380,  IN : 9:00 - BO : 15:00 - BI : 19:00 - OUT : 23:00",
         "BEDOK,  6:00 - 14:30 CASHIER, CASHIER 1ST FLOOR,1, 1, 2.00, 1, 0, 360, 0, 0, 0, 0, 870,  IN : 6:00 - OUT : 14:30",

         "BEDOK,  14:30 - 23:00 CASHIER, CASHIER 1ST FLOOR,1, 1, 2.00, 1, 0, 870, 0, 0, 0, 0, 1380,  IN : 14:30 - OUT : 23:00",
         "BEDOK,  6:00 - 14:30 BUS, BUS,    1, 1, 2.00, 1, 0, 360, 0, 0, 0, 0, 870,  IN : 6:00 - OUT : 14:30",
         "BEDOK,  14:30 - 23:00 BUS, BUS,   1, 1, 2.00, 1, 0, 870, 0, 0, 0, 0, 1380,  IN : 14:30 - OUT : 23:00",


        };

        //private readonly string[] _dcShiftMasters =
        //{
        //    //[LocationRefId],[StationName], [DcGroupRefId], [MaximumStaffRequired], [MinimumStaffRequired], [PriorityLevel], [IsMandatory], [IsActive], [NightFlag], [DefaultTimeFlag], [TimeIn], [BreakFlag], [BreakOut], [BreakIn], [BreakMinutes], [TimeOut], [TimeDescription]

        //    "BEDOK,COFFEE LEVEL 1,LEVEL 1 KITCHEN, 1, 1,1.00,1,1,0,0,360, 0, 0, 0, 0, 1350,IN : 6:00 - OUT : 22:30 ",
        //    "BEDOK,MEALS PRODUCTION,LEVEL 2 KITCHEN, 1, 1, 2.00, 1, 1, 0, 0, 420, 0, 0, 0, 0, 900, IN : 7:00 - OUT : 15:00 ",
        //    "BEDOK, TIFFIN PRODUCTION, LEVEL 1 KITCHEN, 1, 1, 3.00, 1, 1, 0, 1, 240, 1, 600, 960, 0, 1320, IN : 4:00 - BO : 10:00 - BI : 16:00 - OUT : 22:00 ",
        //    "BEDOK, MEALS PRODUCTION ASST, LEVEL 2 KITCHEN, 1, 1, 1.00, 1, 1, 0, 0, 360, 0, 0, 0, 0, 900, IN : 6:00 - OUT : 15:00 ",
        //    "BEDOK, MANAGER, MANAGER, 1, 1, 6.00, 1, 1, 0, 0, 600, 1, 900, 1140, 0, 1320, IN : 10:00 - BO : 15:00 - BI : 19:00 - OUT : 22:00, ",
        //    "BEDOK, DOSA, DOSA, 2, 2, 7.00, 1, 1, 0, 0, 360, 0, 0, 0, 0, 1380, IN : 6:00 - OUT : 23:00",
        //    "BEDOK, SUPERVISOR,CUSTOMER CARE, 1, 1, 8.00, 1, 1, 0, 0, 420, 0, 0, 0, 0, 1380, IN : 7:00 - OUT : 23:00",
        //    "BEDOK, STEWARD FIRST FLOOR, CUSTOMER CARE, 1, 1, 8.00 , 1, 1, 0, 0, 420, 0, 0, 0, 0, 1380, IN : 7:00 - OUT : 23:00",
        //    "BEDOK, STEWARD 2ND FLOOR, CUSTOMER CARE, 1, 1, 10.00 , 1, 1, 0, 0, 540, 1, 900, 1140, 0, 1380, IN : 9:00 - BO : 15:00 - BI : 19:00 - OUT : 23:00",
        //    "BEDOK, CASHIER 1ST FLOOR, CASH HANDLING, 1, 1, 12.00 , 1, 1, 0, 0, 420, 0, 0, 0, 0, 1380, IN : 7:00 - OUT : 23:00",
        //    "BEDOK, CASHIER 2ND FLOOR, CASH HANDLING, 1, 1, 13.00 , 1, 1, 0, 0, 540, 1, 900, 1140, 0, 1380, IN : 9:00 - BO : 15:00 - BI : 19:00 - OUT : 23:00",
        //    "BEDOK, BUS, DRIVER, 2, 2, 1.00, 1, 1, 0, 0, 300, 0, 0, 0, 0, 1380, IN : 5:00 - OUT : 23:00"

        //};

        private readonly string[] _DcPersonalInformations =
        {
            //[LocationRefId],[EmployeeGivenName], [EmployeeSurName], [EmployeeName], [EmployeeCode],  [StaffType],[DateOfBirth], [Gender],[DateHired], [JobTitle], [ActiveStatus],[DefaultSkillRefId],[email]

            "1,'KADER SULAIMAN', 'SAHUL HAMEED', 'S KADER SULAIMAN','CC001','Permanent','1977-02-15 00:00:00.000','Male','2019-04-01 00:00:00.000','MANAGER',1,1,'testing@testing.com",

            "1,'CHRIS', 'SAM', 'S CHRIS', 'CC002','Permanent','2001-02-01 00:00:00.000','Male','2019-04-01 00:00:00.000','SUPERVISOR',1,2,'Testing@testing.com'",


        };

        private readonly string[] _DcshitMasters =
               {
            //[LocationRefId], [DutyDesc], [StationRefId], [MaximumStaffRequired], [MinimumStaffRequired], [PriorityLevel], [IsMandatory], [IsActive], [TimeIn], [BreakFlag], [BreakOut], [BreakIn], [BreakMinutes], [TimeOut]  [TimeDescription], 

        "1, '6:00 - 14:30', 1, 1, 1, 1.00, 1, 1,  360, 0, 0, 0, 0, 870, 'IN : 6:00 - OUT : 14:30'",
        "1,'14:30 - 22:30', 1, 1, 1, 1.00, 1, 1,  870, 0, 0, 0, 0, 1350, 'IN : 14:30 - OUT : 22:30'",
        "1, '7:00 - 15:00', 3, 1, 1, 1.00, 1, 1,  420, 0, 0, 0, 0, 900, 'IN : 7:00 - OUT : 15:00'",
        "1, '4:00 - BO : 10:00 - BI : 14:00 - 20:00', 4, 1, 1, 1.00 , 1, 1,  240, 1, 600, 840, 0, 1200, 'IN : 4:00 - BO : 10:00 - BI : 14:00 - OUT : 20:00'",
        "1, '6:00 - 14:30 DOSA', 7, 1, 1, 1.00 , 1, 1, 360, 0, 0, 0, 0, 870, 'IN : 6:00 - OUT : 14:30'",
        "1, '14:30 - 22:30 DOSA', 7, 1, 1, 2.00, 1, 1, 870, 0, 0, 0, 0, 1350, 'IN : 14:30 - OUT : 22:30'",
        "1, '10:00 - BO : 15:00 - BI : 19:00 - 23:00', 6, 1, 1, 1.00, 1, 1, 600, 1, 900, 1140, 0, 1380, 'IN : 10:00 - BO : 15:00 - BI : 19:00 - OUT : 23:00'",
        "1, '6:00 - BO : 10:00 - BI : 15:00 - 20:00', 8, 1, 1, 1.00, 1, 1, 360, 1, 600, 900, 0, 1200, 'IN : 6:00 - BO : 10:00 - BI : 15:00 - OUT : 20:00'",
        "1, '10:00 - BO : 15:00 - BI : 19:00 - 23:00 SUP', 8, 1, 1, 1.00, 1, 1,  600, 1, 900, 1140, 0, 1380, 'IN : 10:00 - BO : 15:00 - BI : 19:00 - OUT : 23:00'",
        "1,'7:01 - BO : 10:00 - BI : 15:00 - 20:00', 9, 1, 1, 1.00 , 1, 1,  421, 1, 600, 900, 0, 1200, 'IN : 7:01 - BO : 10:00 - BI : 15:00 - OUT : 20:00'",
        "1, '10:00 - BO : 15:00 - BI : 19:00 - 23:00 STEWARD', 9, 1, 1, 1.00 , 1, 1, 600, 1, 900, 1140, 0, 1380, 0, 1, N'IN : 10:00 - BO : 15:00 - BI : 19:00 - OUT : 23:00'"

        };



        private readonly string[] _supplierContents =
        {
            "#SHENG SIONG PTE LTD",
            "#FAIRPRICE PTE LTD",
            "#JASONS GROUP LIMITED"
        };

        private readonly string[] _taxContents =
        {
            "#GST,PS,7,GT"
        };

        public string[] CustomerContents =
        {
            "#EVERCON GROUP",
            "#NORFLEX PVT LTD",
            "#RAYMOND LIM AH ONG"
        };

        //$ - RecipeGroup
        //# - RecipeName
        //@ - Ingridient List

        private readonly string[] _recipeContents =
        {
            "$STARTERS",
            "#CRISPY FISH BASE,1,KG,G,0,0",
            "@ALL PURPOSE FLOUR,.120,KG",
            "@CAYENNE PEPPER,.080,KG",
            "@SALT,.040,KG",
            "@BUTTER,.040,KG",
            "@FISH FILLETS COD,.720,KG",
            "@POTATO FLAKES,.160,KG",
            "$MAINCOURSE",
            "#FLOUR TORTILLAS (LARGE),1,NO,NO,0,0",
            "@ALL PURPOSE FLOUR,.100,KG",
            "@COOKING OIL,0.010,KG",
            "@BAKING POWDER,0.010,KG",
            "#CARBONARA BASE,.350,KG,G,0,0",
            "@MUSHROOMS,0.180,KG",
            "@MOZZARELLA CHEESE,0.050,KG",
            "@CHICKEN,0.075,KG",
            "@BACON,0.070,KG",
            "@OLIVE OIL,0.010,KG",
            "@BUTTER,.010,KG",
            "@BLACK PEPPER,.010,KG",
            "@SALT,0.010,KG"
        };


        //$ - MaterialGroup
        //# - MaterialGroupCategory
        //  - Material
        private readonly string[] _materialContents =
        {
            "$GROCERRIES",
            "#FLOURS",
            "ALL PURPOSE FLOUR,KG",
            "#MASALA",
            "GARLIC POWDER,KG",
            "CAYENNE PEPPER,KG",
            "BLACK PEPPER,KG",
            "#GENERAL",
            "SALT,KG",
            "BAKING POWDER,KG",
            "$MILK PRODUCTS",
            "#CREAM",
            "BUTTER,KG",
            "MOZZARELLA CHEESE,KG",
            "$SEA FOODS",
            "#FISH",
            "FISH FILLETS COD,KG",
            "$VEGETABLE",
            "#VEGETABLE",
            "POTATO FLAKES,KG",
            "MUSHROOMS,KG",
            "$NON VEGETARIAN",
            "#CHICKEN",
            "CHICKEN,KG",
            "#MEAT",
            "BACON,KG",
            "$OIL",
            "#OIL",
            "OLIVE OIL,L",
            "COOKING OIL,L",
            "$SEMI FINISHED BASES",
            "#FISH GRAVYS",
            "CRISPY FISH BASE,KG,30,@ALL PURPOSE FLOUR^3^KG,@CAYENNE PEPPER^2^KG,@GARLIC POWDER^2.5^KG,@SALT^1^KG,@BUTTER^1^KG,@FISH FILLETS COD^18^KG,@POTATO FLAKES^4^KG,",
            "#BASE GRAVYS",
            "CARBONARA BASE,KG,10,@SALT^0.125^KG,@BLACK PEPPER^0.063^KG,@MOZZARELLA CHEESE^1.25^KG,@CHICKEN^1.89^KG,@BUTTER^0.125^KG,@BACON^1.75^KG,@OLIVE OIL^.250^L,@MUSHROOMS^4.5^KG"
        };


        private readonly string[] _menuContents_IA =
        {
            "#STARTERS",
            "CURED MEATS@NORMAL 150000",
            "CRISPY FISH@NORMAL 170000",
            "PROSECCO@NORMAL 140000",
            "MILANO@NORMAL 70000",
            "ROSSINI@NORMAL 140000",
            "BELLINI@NORMAL 140000",
            "BRUSCHETTA@NORMAL 100000",
            "NACHOS@NORMAL 70000",
            "ARANCINI@NORMAL 120000",
            "WALNUT SALAD@NORMAL 90000",
            "FRITTI@NORMAL 130000",
            "TOMATO SALAD@NORMAL 70000",
            "BAKED MASH@NORMAL 120000",
            "FUNKY CHIPS@NORMAL 60000",
            "BROCCOLI@NORMAL 70000",
            "#MAINCOURSE",
            "MOO PING|หมูปิ้ง@NORMAL 290000",
            "BBQ SQUID|ปลาหมึกย่าง@NORMAL 10000",
            "CEREAL PRAWNS|กุ้งผัดเนย+ข้าวโอ๊ต@NORMAL 200000",
            "TOM KHA GAI|แกงข่าไก@NORMAL 30000",
            "CURRY CHICKEN|แกงเขียวหวานไก่@ NORMAL 40000",
            "FRIED RICE|ข้าวผัดหนำเลี้ยบ@NORMAL 50000",
            "CAPRESE SALAD|สลัด@NORMAL 190000",
            "LAMB CHOP|เนื้อแกะสับ@NORMAL 330000",
            "BURGER|เบอร์เกอร์@NORMAL 280000",
            "BAKED BREAD|ขนมปังอบ@NORMAL 70000",
            "PANCAKES|แพนเค้ก@NORMAL 140000",
            "SPICY AVOCADO|อะโวคาโดเผ็ด@NORMAL 120000",
            "SALMON|ปลาแซลมอน@NORMAL 150000",
            "#DRINKS",
            "BLACK COFFEE|咖啡乌@NORMAL 40000",
            "SOYA MILK|热豆奶@NORMAL 50000",
            "TEH TARIK|拉茶@NORMAL 60000",
            "MILO|美禄@NORMAL 70000",
            "COKE|可乐@NORMAL 10000",
            "CHINESE TEA|中国茶@NORMAL 6000",
            "BELLINI PEACH|桃@NORMAL 180000",
            "FRUIT CUP|水果杯@NORMAL 190000",
            "ROSSINI STRAWBERRY|罗西尼草莓@NORMAL 180000",
            "RASPBERRY|覆盆子@NORMAL 190000",
            "HAZELNUT|榛子@NORMAL 190000",
            "CHERRY|樱桃@NORMAL 190000",
            "#VEGETABLES",
            "VEGETABLES 0",
            "MEALS|மீல்ஸ்@NORMAL 40000",
            "PANNER TIKKA|पनीर टिक्का@NORMAL 30000",
            "OPOR NANGKA@PIECE 10000@SMALL 30000@MEDIUM 50000",
            "PUCUK UBI@PIECE 10000@SMALL 30000@MEDIUM 50000",
            "PUCUK MANIS@PIECE 10000@SMALL 30000@MEDIUM 50000",
            "KANGKONG@PIECE 10000@SMALL 20000@MEDIUM 30000@LARGE 50000",
            "SPINACH@PIECE 10000@SMALL 20000@MEDIUM 30000@LARGE 50000",
            "LADYFINGERS@PIECE 10000@SMALL 20000@MEDIUM 30000@LARGE 50000",
            "TERONG BELADO@PIECE 10000@SMALL 20000@MEDIUM 30000@LARGE 50000",
            "TAUGE@PIECE 10000@SMALL 20000@MEDIUM 30000@LARGE 50000",
            "FRIED CABBAGE@PIECE 10000@SMALL 20000@MEDIUM 30000@LARGE 50000",
            "STIR FRIED VEGETABLES@PIECE 10000@SMALL 20000@MEDIUM 30000@LARGE 50000",
            "ACAR@PIECE 10000@SMALL 20000@MEDIUM 30000@LARGE 50000",
            "URAP@PIECE 10000@SMALL 20000@MEDIUM 40000@LARGE 50000",
            "SAYUR LODEH@PIECE 10000@SMALL 30000@MEDIUM 50000"
        };

        private readonly string[] _menuContents =
        {
            "#STARTERS",
            "CURED MEATS@NORMAL 15.50",
            "CRISPY FISH@NORMAL 17.00",
            "PROSECCO@NORMAL 14.00",
            "MILANO@NORMAL 7.50",
            "ROSSINI@NORMAL 14.00",
            "BELLINI@NORMAL 14.00",
            "BRUSCHETTA@NORMAL 10.50",
            "NACHOS@NORMAL 7.50",
            "ARANCINI@NORMAL 12.50",
            "WALNUT SALAD@NORMAL 9.50",
            "FRITTI@NORMAL 13.50",
            "TOMATO SALAD@NORMAL 7.50",
            "BAKED MASH@NORMAL 12.50",
            "FUNKY CHIPS@NORMAL 6.50",
            "BROCCOLI@NORMAL 7.50",
            "#MAINCOURSE",
            "MOO PING|หมูปิ้ง@NORMAL 29.50",
            "BBQ SQUID|ปลาหมึกย่าง@NORMAL 10",
            "CEREAL PRAWNS|กุ้งผัดเนย+ข้าวโอ๊ต@NORMAL 20",
            "TOM KHA GAI|แกงข่าไก@NORMAL 30",
            "CURRY CHICKEN|แกงเขียวหวานไก่@ NORMAL 40",
            "FRIED RICE|ข้าวผัดหนำเลี้ยบ@NORMAL 5.00",
            "CAPRESE SALAD|สลัด@NORMAL 19.50",
            "LAMB CHOP|เนื้อแกะสับ@NORMAL 33.50",
            "BURGER|เบอร์เกอร์@NORMAL 28.50",
            "BAKED BREAD|ขนมปังอบ@NORMAL 7.50",
            "PANCAKES|แพนเค้ก@NORMAL 14.90",
            "SPICY AVOCADO|อะโวคาโดเผ็ด@NORMAL 12.50",
            "SALMON|ปลาแซลมอน@NORMAL 15.50",
            "#DRINKS",
            "BLACK COFFEE|咖啡乌@NORMAL 4",
            "SOYA MILK|热豆奶@NORMAL 5",
            "TEH TARIK|拉茶@NORMAL 6",
            "MILO|美禄@NORMAL 7",
            "COKE|可乐@NORMAL 1.2",
            "CHINESE TEA|中国茶@NORMAL 6",
            "BELLINI PEACH|桃@NORMAL 18.00",
            "FRUIT CUP|水果杯@NORMAL 19.00",
            "ROSSINI STRAWBERRY|罗西尼草莓@NORMAL 18.00",
            "RASPBERRY|覆盆子@NORMAL 19.00",
            "HAZELNUT|榛子@NORMAL 19.00",
            "CHERRY|樱桃@NORMAL 19.00",
            "#VEGETABLES",
            "VEGETABLES 0",
            "MEALS|மீல்ஸ்@NORMAL 40",
            "PANNER TIKKA|पनीर टिक्का@NORMAL 30",
            "OPOR NANGKA@PIECE 1.00@SMALL 3.00@MEDIUM 5.00",
            "PUCUK UBI@PIECE 1.00@SMALL 3.00@MEDIUM 5.00",
            "PUCUK MANIS@PIECE 1.00@SMALL 3.00@MEDIUM 5.00",
            "KANGKONG@PIECE 1.00@SMALL 2.00@MEDIUM 3.00@LARGE 5.00",
            "SPINACH@PIECE 1.00@SMALL 2.00@MEDIUM 3.00@LARGE 5.00",
            "LADYFINGERS@PIECE 1.00@SMALL 2.00@MEDIUM 3.00@LARGE 5.00",
            "TERONG BELADO@PIECE 1.00@SMALL 2.00@MEDIUM 3.00@LARGE 5.00",
            "TAUGE@PIECE 1.00@SMALL 2.00@MEDIUM 3.00@LARGE 5.00",
            "FRIED CABBAGE@PIECE 1.00@SMALL 2.00@MEDIUM 3.00@LARGE 5.00",
            "STIR FRIED VEGETABLES@PIECE 1.00@SMALL 2.00@MEDIUM 3.00@LARGE 5.00",
            "ACAR@PIECE 1.00@SMALL 2.00@MEDIUM 3.00@LARGE 5.00",
            "URAP@PIECE 1.00@SMALL 2.00@MEDIUM 4.00@LARGE 5.00",
            "SAYUR LODEH@PIECE 1.00@SMALL 3.00@MEDIUM 5.00"
        };



        #endregion
    }
}