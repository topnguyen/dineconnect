﻿using System.Threading.Tasks;
using Abp.Application.Services;
using DinePlan.DineConnect.Connect.Setup.Dtos;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Configuration.Host.Dto;

namespace DinePlan.DineConnect.Connect.Setup
{
    public interface ISeedAppService : IApplicationService
    {
        Task SeedSampleData();
    }
}