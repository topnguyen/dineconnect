﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using DinePlan.DineConnect.Connect.Ticket.Dto;

namespace DinePlan.DineConnect.Connect.Ticket
{
    
    public interface IPaymentTagAppService : IApplicationService
    {
        Task<PaymentTagOutputDto> ApiGetAllPaymentTags(GetTicketInput inputDto);

    }
}
