﻿using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket
{
	public interface IConsolidatedExporter
	{
		Task<FileDto> ExportConsolidatedReportToFile(GetTicketInput input, IConsolidatedAppService appService);
	}
}
