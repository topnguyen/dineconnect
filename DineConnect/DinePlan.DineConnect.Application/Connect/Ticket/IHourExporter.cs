﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Log;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Ticket.Implementation;
using DinePlan.DineConnect.Connect.User.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Ticket
{
    public interface IHourExporter
    {
        Task<FileDto> ExportHourlySummary(GetTicketInput ticketInput, IConnectReportAppService reportAppService);

        Task<FileDto> ExportHourlySummaryByDate(GetTicketInput input, IConnectReportAppService connectReportAppService);
    }
}