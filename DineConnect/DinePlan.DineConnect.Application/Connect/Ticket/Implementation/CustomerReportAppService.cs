﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using System.Web;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Connect.WorkPeriod.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.Handler.Mapping;
using DinePlan.DineConnect.Helper;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.House.Transaction.Implementation;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Report;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Connect.Ticket.Implementation
{
    public class CustomerReportAppService : DineConnectAppServiceBase, ICustomerReportAppService
    {
        private readonly ICustomerReportExporter _cre;
        private readonly IPaymentTypeAppService _ptService;
        private readonly IConnectReportAppService _connectReportAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IReportBackgroundAppService _rbas;
        private readonly IBackgroundJobManager _bgm;
        private readonly ITransactionTypeAppService _ttService;
        private readonly IRepository<Promotion> _promoRepo;
        private readonly IRepository<Department> _dtRepo;
        private readonly ITenantSettingsAppService _tenantSettingsService;


        public CustomerReportAppService(ICustomerReportExporter reportExporter, IUnitOfWorkManager unitOfWorkManager, IBackgroundJobManager bgm,
             IConnectReportAppService connectReportAppService, IReportBackgroundAppService rbas,IRepository<Promotion> promoService,
            ITransactionTypeAppService ttService,IPaymentTypeAppService ptService,ITenantSettingsAppService tenantSettingsService, IRepository<Department> dtRepo)
        {
            _cre = reportExporter;
            _ptService = ptService;
            _unitOfWorkManager = unitOfWorkManager;
            _connectReportAppService = connectReportAppService;
            _rbas = rbas;
            _bgm = bgm;
            _ttService = ttService;
            _ptService = ptService;
            _promoRepo = promoService;
            _tenantSettingsService = tenantSettingsService;
            _dtRepo = dtRepo;
        }

        public async Task<FileDto> GetTicketDetailExport(GetTicketInput input)
        {
            return await _cre.ExportDetails(input);
        }

       

        public async Task<FileDto> GetWritersCafeForTicketToExcelInBackground(GetTicketInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                var tickets = _connectReportAppService.GetAllTicketsForTicketInput(input);

                var result = new List<WritersCafeList>();

                await tickets.ForEachAsync(o =>
                {
                    if (o != null)
                    {
                        var dto = new WritersCafeList
                        {
                            BillNo = o.TicketNumber,
                            PrintTime = o.TicketCreatedTime,
                            Amount = o.TotalAmount,
                            ModificationDone = o.LastModifiedUserName,
                            RePrintTime = o.LastPaymentTime,
                            BillGeneratedTime = o.LastUpdateTime,
                            AmountAfterModification = o.TotalAmount,
                            PaymentType = o.Payments?.FirstOrDefault()?.PaymentType?.Name,
                            Pax = "0"
                        };

                        if (!string.IsNullOrEmpty(o.TicketLogs))
                        {
                            var allLogs = JsonHelper.Deserialize<List<TicketLogValue>>(o.TicketLogs);

                            if (allLogs.Any())
                            {
                                var firstPrintTime =
                                    allLogs.FirstOrDefault(a =>
                                        a.Category != null && a.Category.Equals(DinePlanLogTypes.PRINTBILL));

                                if (firstPrintTime != null)
                                {
                                    dto.BillGeneratedTime = firstPrintTime.DateTime;

                                    if (!string.IsNullOrEmpty(firstPrintTime.Log))
                                    {
                                        var allListValue =
                                            JsonConvert.DeserializeObject<List<SimpleKeyValuePair>>(firstPrintTime.Log);

                                        if (allListValue.Any())
                                        {
                                            var firtValue =
                                                allListValue.FirstOrDefault(a => a.Key.Equals("TicketTotal"));

                                            if (firtValue != null) dto.Amount = Convert.ToDecimal(firtValue.Value);
                                        }
                                    }
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(o.TicketTags))
                        {
                            var coverorPax = JsonHelper.Deserialize<List<TicketTagValue>>(o.TicketTags)
                                .FirstOrDefault(t => t.TagName.Equals("PAX") || t.TagName.Equals("GUEST"));
                            if (coverorPax != null) dto.Pax = coverorPax.TagValue;
                        }
                        else if (o.Orders != null && o.Orders.Any())
                        {
                            dto.Pax = o.Orders.Sum(t => t.NumberOfPax).ToString();
                        }

                        result.Add(dto);
                    }
                });

                return await _cre.ExportWritersCafeForTicket(input, result);
            }
        }

        public async Task<FileDto> GetWritersCafeForTicketToExcel(GetTicketInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.WRITERSCAFEFORTICKET,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });

                    if (backGroundId > 0)
                    {
                        input.LocationGroup.UserId = AbpSession.UserId.Value;

                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.WRITERSCAFEFORTICKET,
                            TicketInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                    }
                }
                else
                {
                    return await GetWritersCafeForTicketToExcelInBackground(input);
                }
            }

            return null;
        }

        public async Task<FileDto> GetWritersCafeToExcelInBackground(GetItemInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                var orders = _connectReportAppService.GetAllOrders(input);

                var result = new List<WritersCafeList>();

                await orders.ForEachAsync(o =>
                {
                    if (o.Ticket != null)
                    {
                        var dto = new WritersCafeList
                        {
                            Location = o.Location?.Name,
                            Date = o.OrderCreatedTime,
                            BillNo = o.Ticket?.TicketNumber,
                            TransactionId = o.Ticket.InvoiceNo ?? o.Ticket.TicketNumber,
                            OrderTakenTime = o.OrderCreatedTime,
                            BillGeneratedTime = o.Ticket.LastUpdateTime,
                            TransactionClosedTime = o.Ticket.LastPaymentTime,
                            OrderType = o.Ticket.DepartmentName,
                            PaymentType = o.Ticket.Payments.FirstOrDefault()?.PaymentType?.Name,
                            MenuItemName = o.MenuItemName,
                            CategoryName = o.MenuItem?.Category?.Name,
                            Price = o.Price,
                            Quantity = o.Quantity,
                            Discount = o.PromotionAmount,
                            PackingCharge = 0,
                            Pax = "0"
                        };

                        if (!string.IsNullOrEmpty(o.Ticket.TicketLogs))
                        {
                            var allLogs = JsonHelper.Deserialize<List<TicketLogValue>>(o.Ticket.TicketLogs);

                            if (allLogs.Any())
                            {
                                var firstPrintTime =
                                    allLogs.FirstOrDefault(a =>
                                        a.Category != null && a.Category.Equals(DinePlanLogTypes.PRINTBILL));

                                if (firstPrintTime != null) dto.BillGeneratedTime = firstPrintTime.DateTime;
                            }
                        }

                        if (!string.IsNullOrEmpty(o.Ticket.TicketTags))
                        {
                            var coverorPax = JsonHelper.Deserialize<List<TicketTagValue>>(o.Ticket.TicketTags)
                                .FirstOrDefault(t => t.TagName.Equals("PAX") || t.TagName.Equals("GUEST"));
                            if (coverorPax != null) dto.Pax = coverorPax.TagValue;
                        }
                        else if (o.Ticket.Orders != null && o.Ticket.Orders.Any())
                        {
                            dto.Pax = o.Ticket.Orders.Sum(t => t.NumberOfPax).ToString();
                        }

                        if (o.Ticket.Transactions.Any())
                        {
                            var allPac =
                                o.Ticket.Transactions.Where(a =>
                                    a.TransactionType.AccountCode != null &&
                                    a.TransactionType.AccountCode.StartsWith("PACK"));

                            if (allPac.Any()) dto.PackingCharge = allPac.Sum(a => a.Amount);
                        }

                        if (o.Ticket.Transactions.Any())
                        {
                            var allPac =
                                o.Ticket.Transactions.Where(a =>
                                    a.TransactionType.AccountCode != null &&
                                    a.TransactionType.AccountCode.StartsWith("DISC"));

                            if (allPac.Any()) dto.Discount = allPac.Sum(a => a.Amount);
                        }

                        if (!string.IsNullOrEmpty(o.Ticket.TicketEntities))
                        {
                            var entities =
                                JsonConvert.DeserializeObject<IList<TicketEntityMap>>(o.Ticket.TicketEntities);
                            var myEntity = entities.FirstOrDefault(a => a.EntityTypeId == 2);
                            if (myEntity != null) dto.TableNo = myEntity.EntityName;
                        }

                        result.Add(dto);
                    }
                });

                return await _cre.ExportWritersCafe(input, result);
            }
        }

        public async Task<FileDto> GetWritersCafeToExcel(GetItemInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.WRITERSCAFE,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });

                    if (backGroundId > 0)
                    {
                        input.LocationGroup.UserId = AbpSession.UserId.Value;

                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.WRITERSCAFE,
                            ItemInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                    }
                }
                else
                {
                    return await GetWritersCafeToExcelInBackground(input);
                }
            }

            return null;
        }

        public async Task<FileDto> GetSalesSummaryForPB(GetTicketInput input)
        {
             var proList = _promoRepo.GetAll().Select(t => t.Name).ToList();
            input.ExportType = "EXPORT";
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var ttNames = await _ttService.GetAllItems();
                var ttList = ttNames.Items.Where(a=>!a.HideOnReport).Select(a => a.Name).ToList();
                var ptNames = await _ptService.GetAllItems();
                var ptList = ptNames.Items.Select(a => a.Name).ToList();
                var dtNames = await _dtRepo.GetAllListAsync();
                var dtList = dtNames.Select(a => a.Name).ToList();

                var setting = await _tenantSettingsService.GetAllSettings();
                input.DatetimeFormat = setting.Connect.DateTimeFormat;
                input.DateFormat = setting.Connect.SimpleDateFormat;
                if (AbpSession.UserId != null && AbpSession.TenantId != null)
                {
                    if (input.RunInBackground)
                    {
                        var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                        {
                            ReportName = ReportNames.SALESUMMARY,
                            Completed = false,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value,
                            ReportDescription = input.ReportDescription
                        });
                        if (backGroundId > 0)
                            await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                            {
                                BackGroundId = backGroundId,
                                ReportName = ReportNames.SALESUMMARY,
                                TicketInput = input,
                                TTList = ttList,
                                PTList = ptList,
                                DTList = dtList,
                                ProList = proList,
                                UserId = AbpSession.UserId.Value,
                                TenantId = AbpSession.TenantId.Value
                            });
                    }
                    else
                    {
                        return await _cre.ExportToSalesSummary(input,  ttList, ptList, dtList, proList,
                            (int)AbpSession.TenantId);
                    }
                }

                return null;
            }
        }

        public async Task<FileDto> GetItemSummaryForPB(GetItemInput input)
        {
            return null;
        }
    }
}