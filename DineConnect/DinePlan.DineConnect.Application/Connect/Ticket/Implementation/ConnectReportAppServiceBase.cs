﻿using Abp.Collections.Extensions;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Helper;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;

namespace DinePlan.DineConnect.Connect.Ticket.Implementation
{
    public class ConnectReportAppServiceBase : DineConnectAppServiceBase
    {
        private readonly ILocationAppService _locService;
        private readonly IRepository<Master.Location> _lRepo;
        private readonly IRepository<Transaction.Ticket> _ticketManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public ConnectReportAppServiceBase(IRepository<Transaction.Ticket> ticketManager,
            ILocationAppService locService,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<Master.Location> lRepo
        )
        {
            _ticketManager = ticketManager;
            _unitOfWorkManager = unitOfWorkManager;
            _locService = locService;
            _lRepo = lRepo;
        }

        public IQueryable<Transaction.Ticket> GetAllTicketsForTicketInput(GetTicketInput input)
        {
            try
            {
                var tickets = GetAllTickets(input);

                if (input.Payments != null && input.Payments.Any())
                    tickets = tickets.Include(a => a.Payments)
                        .WhereIf(input.Payments.Any(),
                            p => p.Payments.Any(pt => input.Payments.Contains(pt.PaymentTypeId)));

                if (input.Transactions != null && input.Transactions.Any())
                {
                    var transactionTypes = input.Transactions.Select(a => Convert.ToInt32(a.Value)).ToArray();
                    tickets = from p in tickets.Include(a => a.Transactions)
                              where p.Transactions.Any(pt => transactionTypes.Contains(pt.TransactionTypeId))
                              select p;
                }

                if (!string.IsNullOrEmpty(input.TerminalName))
                    tickets = tickets.Where(a => a.TerminalName.Contains(input.TerminalName));

                if (!string.IsNullOrEmpty(input.Department))
                    tickets = tickets.Where(a => a.DepartmentName.Contains(input.Department));
                if (input.Departments != null && input.Departments.Any())
                {
                    var allDeparments = input.Departments.Select(a => a.DisplayText).ToArray();
                    tickets = tickets.Where(a => a.DepartmentName != null && allDeparments.Contains(a.DepartmentName));
                }

                if (input.TicketTags != null && input.TicketTags.Any())
                {
                    var allDeparments = input.TicketTags.Select(a => a.DisplayText).ToArray();
                    tickets = from c in tickets
                              where allDeparments.Any(i => c.TicketTags.Contains(i))
                              select c;
                }

                if (!string.IsNullOrEmpty(input.LastModifiedUserName))
                    tickets = tickets.Where(a => a.LastModifiedUserName.Contains(input.LastModifiedUserName));

                if (input.Refund) tickets = tickets.Where(a => a.TicketStates.Contains("Refund"));

                if (!string.IsNullOrEmpty(input.DynamicFilter))
                {
                    var jsonSerializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver =
                            new CamelCasePropertyNamesContractResolver()
                    };
                    var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                    if (filRule?.Rules != null) tickets = tickets.BuildQuery(filRule);
                }

                return tickets;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public IQueryable<Transaction.Ticket> GetAllTicketsForTicketInputNoFilterByDynamicFilter(GetTicketInput input)
        {
            try
            {
                var tickets = GetAllTickets(input);

                if (input.Payments != null && input.Payments.Any())
                    tickets = tickets.Include(a => a.Payments).Include(p => p.Payments.Select(t => t.Ticket))
                        .WhereIf(input.Payments.Any(),
                            p => p.Payments.Any(pt => input.Payments.Contains(pt.PaymentTypeId)));

                if (input.Transactions != null && input.Transactions.Any())
                {
                    var transactionTypes = input.Transactions.Select(a => Convert.ToInt32(a.Value)).ToArray();
                    tickets = from p in tickets.Include(a => a.Transactions)
                              where p.Transactions.Any(pt => transactionTypes.Contains(pt.TransactionTypeId))
                              select p;
                }

                if (!string.IsNullOrEmpty(input.TerminalName))
                    tickets = tickets.Where(a => a.TerminalName.Contains(input.TerminalName));

                if (!string.IsNullOrEmpty(input.Department))
                    tickets = tickets.Where(a => a.DepartmentName.Contains(input.Department));
                if (input.Departments != null && input.Departments.Any())
                {
                    var allDeparments = input.Departments.Select(a => a.DisplayText).ToArray();
                    tickets = tickets.Where(a => a.DepartmentName != null && allDeparments.Contains(a.DepartmentName));
                }

                if (input.TicketTags != null && input.TicketTags.Any())
                {
                    var allDeparments = input.TicketTags.Select(a => a.DisplayText).ToArray();
                    tickets = from c in tickets
                              where allDeparments.Any(i => c.TicketTags.Contains(i))
                              select c;
                }

                if (!string.IsNullOrEmpty(input.LastModifiedUserName))
                    tickets = tickets.Where(a => a.LastModifiedUserName.Contains(input.LastModifiedUserName));

                if (input.Refund) tickets = tickets.Where(a => a.TicketStates.Contains("Refund"));

                return tickets;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public IQueryable<Transaction.Ticket> GetAllTickets(IDateInput input)
        {
            IQueryable<Transaction.Ticket> tickets;

            if (!string.IsNullOrEmpty(input.TicketNo))
                using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                {
                    tickets = _ticketManager.GetAll()
                        .Where(a => a.TicketNumber.Equals(input.TicketNo) || a.ReferenceNumber.Equals(input.TicketNo));
                    return tickets;
                }

            var correctDate = CorrectInputDate(input);
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                tickets = _ticketManager.GetAll();
                if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
                {
                    if (correctDate)
                    {
                        tickets =
                            tickets.Where(
                                a =>
                                    a.LastPaymentTime >=
                                    input.StartDate
                                    &&
                                    a.LastPaymentTime <=
                                    input.EndDate);
                    }
                    else
                    {
                        var mySt = input.StartDate.Date;
                        var myEn = input.EndDate.Date;

                        tickets =
                            tickets.Where(
                                a =>
                                    a.LastPaymentTimeTruc >= mySt
                                    &&
                                    a.LastPaymentTimeTruc <= myEn);
                    }
                }
            }

            if (input.Location > 0)
            {
                tickets = tickets.Where(a => a.LocationId == input.Location);
            }
            else if (input.Locations != null && input.Locations.Any())
            {
                var locations = input.Locations.Select(a => a.Id).ToList();
                tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
                     && !input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                if (locations.Any()) tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true
                });
                tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }

            if (input.LocationGroup != null)
            {
                if (input.LocationGroup.NonLocations != null && input.LocationGroup.NonLocations.Any())
                {
                    var nonlocations = input.LocationGroup.NonLocations.Select(a => a.Id).ToList();
                    tickets = tickets.Where(a => !nonlocations.Contains(a.LocationId));
                }
            }

            else if (input.LocationGroup == null)
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = new List<SimpleLocationDto>(),
                    Group = false,
                    UserId = input.UserId
                });
                if (input.UserId > 0)
                    tickets = tickets.Where(a => locations.Contains(a.LocationId));
            }

            tickets = tickets.Where(a => a.Credit == input.Credit);

            return tickets;
        }

        public bool CorrectInputDate(IDateInput input)
        {
            if (input.NotCorrectDate) return true;
            var returnStatus = true;

            var operateHours = 0M;

            if (input.Locations != null && input.Locations.Any() && input.Locations.Count() == 1)
                operateHours = _lRepo.Get(input.Locations.First().Id).ExtendedBusinessHours;

            if (operateHours == 0M && input.Location > 0)
                operateHours = _lRepo.Get(input.Location).ExtendedBusinessHours;
            if (operateHours == 0M && input.LocationGroup != null && input.LocationGroup.Locations.Any() &&
                input.LocationGroup.Locations.Count() == 1)
                operateHours = _lRepo.Get(input.LocationGroup.Locations.First().Id).ExtendedBusinessHours;
            if (operateHours == 0M)
                operateHours = SettingManager.GetSettingValue<decimal>(AppSettings.ConnectSettings.OperateHours);

            if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
            {
                if (operateHours == 0M) returnStatus = false;
                input.EndDate = input.EndDate.AddDays(1);
                input.StartDate = input.StartDate.AddHours(Convert.ToDouble(operateHours));
                input.EndDate = input.EndDate.AddHours(Convert.ToDouble(operateHours)).AddMinutes(-1);
            }
            else
            {
                returnStatus = false;
            }

            return returnStatus;
        }

        protected List<PromotionDetailValue> GetPromotionDetailValues(Transaction.Ticket ticket)
        {
            var results = new List<PromotionDetailValue>();
            if (!string.IsNullOrEmpty(ticket.TicketPromotionDetails))
                results = JsonConvert.DeserializeObject<List<PromotionDetailValue>>(ticket.TicketPromotionDetails);
            results.AddRange(GetPromotionDetailValues(ticket.Orders.ToList()));
            return results;
        }

        protected List<PromotionDetailValue> GetPromotionDetailValues(List<Order> orders)
        {
            var results = new List<PromotionDetailValue>();
            foreach (var order in orders)
            {
                if (order.IsPromotionOrder
                                && !string.IsNullOrEmpty(order.OrderPromotionDetails)
                                && order.CalculatePrice
                                && order.DecreaseInventory)
                {
                    var promotionOrders = JsonHelper.Deserialize<List<PromotionDetailValue>>(order.OrderPromotionDetails).ToList();
                    foreach (var promotionOrder in promotionOrders)
                    {
                        promotionOrder.PromotionAmount = promotionOrder.PromotionAmount * order.Quantity;
                    }
                    results.AddRange(promotionOrders);
                }
            }
            return results;
        }

        protected decimal GetNumberOfGuests(List<Transaction.Ticket> tickets)
        {
            var totalPaxCount = 0M;
            try
            {

                var toaalll = tickets.Where(a => !string.IsNullOrEmpty(a.TicketTags)).Select(a => a.TicketTags);
                foreach (var tag in toaalll)
                    if (!string.IsNullOrEmpty(tag))
                    {
                        var myTag = JsonConvert.DeserializeObject<List<TicketTagValue>>(tag);
                        var lastTag = myTag?.LastOrDefault(a =>
                            a.TagName.Equals("COVERS") || a.TagName.Equals("PAX"));
                        if (lastTag != null) totalPaxCount += int.Parse(lastTag.TagValue);
                    }
            }
            catch (Exception ex)
            {
            }

            return totalPaxCount;
        }

        protected decimal GetTotalOrderCount(List<Transaction.Ticket> tickets)
        {
            decimal totalOrderCount = 0;
            try
            {

                return GetTotalOrderCount(tickets.SelectMany(t => t.Orders).ToList());

            }
            catch (Exception ex)
            {
            }

            return totalOrderCount;
        }

        protected decimal GetTotalOrderCount(List<Transaction.Order> orders)
        {
            decimal totalOrderCount = 0;
            try
            {

                totalOrderCount = orders.Where(o => o.DecreaseInventory
                && !o.IsInState("*", "Gift")
                && !o.IsInState("*", "Comp")
                && !o.IsInState("*", "Void")
                && !o.IsInState("*", "Refund")).Sum(o => o.Quantity * (o.MenuItemPortion?.NumberOfPax ?? 0));

            }
            catch (Exception ex)
            {
            }

            return totalOrderCount;
        }

        protected decimal GetComplimentAmount(List<Transaction.Ticket> tickets)
        {
            var orders = tickets.SelectMany(t => t.Orders).ToList();
            return GetComplimentAmount(orders);
        }

        protected decimal GetComplimentAmount(List<Order> orders)
        {
            var complimentOrders = orders.Where(o => o.IsInState("*", "Gift") || o.IsInState("*", "Comp"));
            return complimentOrders.Sum(o => o.Price * o.Quantity);
        }
    }
}