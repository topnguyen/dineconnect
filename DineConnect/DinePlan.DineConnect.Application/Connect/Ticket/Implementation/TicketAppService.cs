﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using System.Transactions;
using Abp.Application.Features;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Logging;
using Abp.UI;
using AutoMapper;
using Castle.Core.Logging;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Credit.Dtos;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Log;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Connect.User.Dtos;
using DinePlan.DineConnect.Helper;
using DinePlan.DineConnect.Job;
using DinePlan.DineConnect.Job.Mail;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Connect.Ticket.Implementation
{
    public class TicketAppService : DineConnectAppServiceBase, ITicketAppService
    {
        private readonly IBackgroundJobManager _bgm;
        private readonly IConnectReportAppService _cService;
        private readonly IRepository<Master.Location> _locRepo;
        private readonly ILogger _logger;
        private readonly IRepository<MenuItem> _miRepository;
        private readonly IRepository<Order> _orRepository;
        private readonly IRepository<Payment> _paRepository;
        private readonly IPlanLoggerAppService _planLoggerAppService;

        private readonly IRepository<PlanLogger> _planLogRepo;

        private readonly IPromotionAppService _promotionAppService;
        private readonly IRepository<PaymentType> _pTypeRepo;
        private readonly SettingManager _settingManager;
        private readonly ISyncAppService _syncAppService;
        private readonly IRepository<TicketTransaction> _taRepository;
        private readonly IRepository<Transaction.Ticket> _ticketManager;
        private readonly IRepository<TransactionType> _transRepo;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public TicketAppService(IRepository<Transaction.Ticket> ticketManager, IRepository<PaymentType> pTypeRepo,
            IRepository<Master.Location> locRepo,
            IRepository<Payment> paRepository,
            IRepository<TicketTransaction> taRepository,
            IRepository<Order> orRepository,
            IRepository<TransactionType> transRepo,
            FeatureChecker featureChecker, ILogger logger,
            SettingManager settingManager,
            IConnectReportAppService cService,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<MenuItem> miRepository,
            IBackgroundJobManager backgroundJobManager,
            ISyncAppService syncAppService,
            IPromotionAppService promotionAppService,
            IRepository<PlanLogger> planLogRepo,
            IPlanLoggerAppService planLoggerAppService)
        {
            if (featureChecker == null) throw new ArgumentNullException(nameof(featureChecker));

            _cService = cService;
            _transRepo = transRepo;
            _ticketManager = ticketManager;
            _orRepository = orRepository;
            _paRepository = paRepository;
            _taRepository = taRepository;
            _logger = logger;
            _settingManager = settingManager;
            _unitOfWorkManager = unitOfWorkManager;
            _bgm = backgroundJobManager;
            _syncAppService = syncAppService;
            _miRepository = miRepository;
            _promotionAppService = promotionAppService;
            _pTypeRepo = pTypeRepo;
            _planLogRepo = planLogRepo;
            _planLoggerAppService = planLoggerAppService;
            _locRepo = locRepo;
        }

        public IAppFolders AppFolders { get; set; }

        public async Task<CreateOrUpdateTicketOutput> CreateOrUpdateTicket(CreateOrUpdateTicketInput input)
        {
            try
            {
                if (input == null || input.Ticket == null)
                    return new CreateOrUpdateTicketOutput
                    {
                        Ticket = 0
                    };


                if (input.Ticket.LocationId > 0 && input.Ticket.TenantId > 0)
                {
                    var myLocation = await _locRepo.FirstOrDefaultAsync(a =>
                        a.Id == input.Ticket.LocationId && a.TenantId == input.Ticket.TenantId);
                    if (myLocation != null)
                    {
                        if (myLocation.RestrictSync)
                            return new CreateOrUpdateTicketOutput
                            {
                                Ticket = 100
                            };
                    }
                    else
                    {
                        return new CreateOrUpdateTicketOutput
                        {
                            Ticket = 0
                        };
                    }
                }


                if (input.Ticket != null && input.Ticket.LocationId > 0 && input.Ticket.TenantId > 0)
                    await _syncAppService.UpdatePushTime(new ApiLocationInput
                    {
                        LocationId = input.Ticket.LocationId,
                        TenantId = input.Ticket.TenantId
                    });

                var updateTicket = false;
                var list = 0;
                if (input.Ticket != null && input.Ticket.Id.HasValue && input.Ticket.Id > 0) 
                    updateTicket = true;

                if (input.Ticket != null && !string.IsNullOrEmpty(input.Ticket.TicketNumber))
                {
                    var ticket =
                        await _ticketManager.FirstOrDefaultAsync(
                            a =>
                                a.TicketNumber.Equals(input.Ticket.TicketNumber) &&
                                a.LocationId.Equals(input.Ticket.LocationId) && a.TenantId == input.Ticket.TenantId);

                    if (ticket != null)
                    {
                        input.Ticket.Id = ticket.Id;
                        updateTicket = true;
                    }
                    else
                    {
                        updateTicket = false;
                    }
                }

                if (input.Ticket != null)
                {
                    var dName = input.Ticket.DepartmentName;
                    if (string.IsNullOrEmpty(dName))
                    {
                        input.Ticket.DepartmentName = "";
                        dName = "";
                    }

                    input.Ticket.LastPaymentTimeTruc = input.Ticket.LastPaymentTime.Date;

                    if (input.Ticket.Orders.Any())
                        foreach (var order in input.Ticket.Orders)
                        {
                            order.DepartmentName = dName;
                            order.Location_Id = input.Ticket.LocationId;

                            if (order.TTaxes != null && order.TTaxes.Any())
                                order.Taxes = JsonConvert.SerializeObject(order.TTaxes);
                            if (order.OTags != null && order.OTags.Any())
                                order.OrderTags = JsonConvert.SerializeObject(order.OTags);
                            if (order.OTags != null && order.OStates.Any())
                                order.OrderStates = JsonConvert.SerializeObject(order.OStates);
                        }

                    if (input.Ticket.TStates != null && input.Ticket.TStates.Any())
                        input.Ticket.TicketStates = JsonConvert.SerializeObject(input.Ticket.TStates);
                }

                if (input.Ticket != null && input.Ticket.TotalAmount == 0M)
                {
                    foreach (var paymentAmount in input.Ticket.Payments)
                        paymentAmount.Amount = 0M;
                }

                using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
                {
                    foreach (var paymentTrans in input.Ticket.Payments)
                        if (paymentTrans.PaymentTypeId > 0)
                        {
                            var myTrans = _pTypeRepo.GetAll().Where(a => a.Id == paymentTrans.PaymentTypeId &&
                                                                         a.TenantId == input.Ticket.TenantId).ToList();
                            if (!myTrans.Any())
                                paymentTrans.PaymentTypeId =
                                    (await _paRepository.FirstOrDefaultAsync(a => a.Id > 0)).Id;
                        }
                        else if (!string.IsNullOrEmpty(paymentTrans.PaymentTypeName))
                        {
                            var myTrans = _pTypeRepo.GetAll().Where(a =>
                                a.Name.ToUpper().Equals(paymentTrans.PaymentTypeName.ToUpper()) &&
                                a.TenantId == input.Ticket.TenantId).ToList();

                            paymentTrans.PaymentTypeId = myTrans.Any()
                                ? myTrans.Last().Id
                                : (await _paRepository.FirstOrDefaultAsync(a => a.Id > 0)).Id;
                        }
                        else
                        {
                            var errorMessage =
                                $"Payment Type Type is coming as Empty -> Ticket Number : {input.Ticket.TicketNumber} and Location Id : {input.Ticket.LocationId} and TenantId : {input.Ticket.TenantId}";
                            throw new UserFriendlyException(errorMessage);
                        }
                }

                using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
                {
                    foreach (var ticketTransaction in input.Ticket.Transactions.Where(a => a.TransactionTypeId == 0))
                        if (!string.IsNullOrEmpty(ticketTransaction.TransactionTypeName))
                        {
                            var myTrans = _transRepo.GetAll().Where(a =>
                                a.Name.ToUpper().Equals(ticketTransaction.TransactionTypeName.ToUpper()) &&
                                a.TenantId == input.Ticket.TenantId).ToList();

                            if (myTrans.Any())
                            {
                                ticketTransaction.TransactionTypeId = myTrans.Last().Id;
                            }
                            else
                            {
                                var errorMessage =
                                    $"Ticket Transaction Type is coming as Empty -> Ticket Number : {input.Ticket.TicketNumber} and Location Id : {input.Ticket.LocationId} and Tenantid : {input.Ticket.TenantId}";
                                throw new UserFriendlyException(errorMessage);
                            }
                        }
                        else
                        {
                            var errorMessage =
                                $"Ticket Transaction Type is coming as Empty -> Ticket Number : {input.Ticket.TicketNumber} and Location Id : {input.Ticket.LocationId} and Tenantid : {input.Ticket.TenantId}";
                            throw new UserFriendlyException(errorMessage);
                        }
                }

                if (updateTicket)
                {
                    list = await UpdateTicketAsync(input.Ticket);
                }
                else
                {
                    list = await CreateTicketAsync(input.Ticket);
                    var newTicket =
                        await _ticketManager.FirstOrDefaultAsync(x =>
                            x.Id == list && x.TenantId == input.Ticket.TenantId);
                    if (newTicket != null) await _promotionAppService.UpdateQuotaUsage(newTicket);
                }


                if (!updateTicket)
                {
                    if (!string.IsNullOrEmpty(input.Email))
                        await _bgm.EnqueueAsync<SendEmailJob, ReceiptEmailArgs>(new ReceiptEmailArgs
                        {
                            Email = input.Email,
                            LocationId = input.Ticket.LocationId,
                            TicketNumber = input.Ticket.TicketNumber,
                            EmailType = "RECEIPT"
                        });
                    if (!string.IsNullOrEmpty(input.Phone))
                        await _bgm.EnqueueAsync<ReceiptMessageJob, ReceiptMessageArgs>(new ReceiptMessageArgs
                        {
                            Phone = input.Phone,
                            LocationId = input.Ticket.LocationId,
                            TicketNumber = input.Ticket.TicketNumber
                        });
                }

                return new CreateOrUpdateTicketOutput
                {
                    Ticket = list
                };
            }
            catch (Exception exception)
            {
                _logger.Log(LogSeverity.Error, "TicketCreation", exception);
                throw new UserFriendlyException(exception.Message);
            }
        }

        public async Task UpdateMembersAndPoints()
        {
            var i = 0;
            while (true)
            {
                var input = new GetTicketInput
                {
                    SkipCount = i,
                    MaxResultCount = 100,
                    Sorting = "Id"
                };
                var tickets = await GetTickets(input);
                if (tickets != null && tickets.Items.Any())
                    foreach (var tld in tickets.Items)
                    {
                        var totalAmount = tld.TotalAmount;
                        var calcOnorder = false;
                        if (await _settingManager.GetSettingValueAsync<bool>(AppSettings.EngageSettings.CalculatePoints)
                        )
                        {
                            var tra = await _transRepo.SingleAsync(a => a.Name.ToUpper().Equals("SALE"));
                            if (tra != null)
                            {
                                var localt = tld.Transactions.SingleOrDefault(a => a.TransactionTypeId == tra.Id);
                                if (localt != null)
                                {
                                    totalAmount = localt.Amount;
                                    calcOnorder = true;
                                }
                            }
                        }

                        await CreateMemberAndPoints(tld.TicketEntities, tld.LocationId, tld.TenantId, totalAmount,
                            tld.TicketNumber, tld.TicketCreatedTime, calcOnorder);
                    }
                else
                    break;

                i += 50;
            }
        }

        public async Task<NullableIdInput> GetTicketIdOnLocation(GetTicketIdInput input)
        {
            var output = new NullableIdInput { Id = null };

            if (input != null)
            {
                if (!string.IsNullOrEmpty(input.TicketNumber))
                {
                    var ticket =
                        await
                            _ticketManager.FirstOrDefaultAsync(
                                a => a.LocationId == input.Location && a.TicketNumber.Equals(input.TicketNumber));
                    if (ticket == null)
                        throw new UserFriendlyException(L("NotExist_f", L("Ticket"), input.TicketNumber));
                    output.Id = ticket.Id;
                }
                else
                {
                    throw new UserFriendlyException(L("NotExist_f", L("Ticket"), ""));
                }
            }

            return output;
        }

        public async Task<ApiCreditOutput> ApiGetCreditTicketTotal(ApiCreditInput input)
        {
            var output = new ApiCreditOutput();

            if (string.IsNullOrEmpty(input.ReferenceNumber))
            {
                output.Log = "Reference Number is empty";
                return output;
            }

            if (input.TenantId <= 0)
            {
                output.Log = "Tenant Id is empty";
                return output;
            }

            Transaction.Ticket ticket;
            try
            {
                ticket =
                    await _ticketManager.FirstOrDefaultAsync(
                        a => a.TenantId == input.TenantId && a.ReferenceNumber != null &&
                             a.ReferenceNumber.Equals(input.ReferenceNumber));
            }
            catch (Exception)
            {
                ticket = null;
            }

            if (ticket == null)
            {
                output.Log = "Reference Number is incorrect";
                return output;
            }

            if (!ticket.IsClosed)
            {
                output.Log = "Ticket is not closed";
                return output;
            }

            if (ticket.CreditProcessed)
            {
                output.Log = "Ticket is already Processed";
                return output;
            }

            if (ticket.TotalAmount <= 0)
            {
                output.Log = "Ticket Amount is not correct";
                return output;
            }

            if (!ticket.Payments.Any())
            {
                output.Log = "Ticket has no Payments";
                return output;
            }

            output.Value = ticket.TotalAmount;
            output.TicketId = ticket.Id;
            return output;
        }

        public async Task<ApiCreditOutput> ApiApplyCreditTicketTotal(ApiCreditInput input)
        {
            var verify = await ApiGetCreditTicketTotal(input);
            if (string.IsNullOrEmpty(verify.Log))
            {
                var ticket = await _ticketManager.GetAsync(verify.TicketId);
                ticket.CreditProcessed = true;
                await _ticketManager.UpdateAsync(ticket);
            }

            return verify;
        }

        public async Task<int> SyncTicket(PostInput postInput)
        {
            if (postInput != null)
                if (!string.IsNullOrEmpty(postInput.Contents))
                {
                    var myTicket = JsonConvert.DeserializeObject<CreateOrUpdateTicketInput>(postInput.Contents);
                    if (myTicket != null)
                    {
                        var result = await CreateOrUpdateTicket(myTicket);
                        return result.Ticket;
                    }
                }

            return 0;
        }

        [AbpAuthorize]
        public async Task<TicketStatsDto> GetAll(GetTicketInput input)
        {
            return await _cService.GetTickets(input);
        }

        public async Task<TicketTransactionOutput> GetTicketTransactions(GetTicketInput input)
        {
            return await _cService.GetTicketTransactions(input);
        }

        [AbpAuthorize]
        public async Task<PaymentStatsDto> GetPayments(GetPaymentInput input)
        {
            return await _cService.GetPayments(input);
        }

        [AbpAuthorize]
        public async Task<TransactionStatsDto> GetTransactions(GetTransactionInput input)
        {
            return await _cService.GetTransactions(input);
        }

        public async Task<TicketListDto> GetInputTicket(NullableIdInput input)
        {
            TicketListDto returnList;
            if (input.Id.HasValue)
            {
                var ticket = await _ticketManager.GetAsync(input.Id.Value);
                returnList = ticket.MapTo<TicketListDto>();
                foreach (var lst in returnList.Orders)
                {
                    var PromotionDetailValuelst = new List<PromotionDetailValue>();
                    if (!string.IsNullOrEmpty(lst.OrderPromotionDetails))
                        PromotionDetailValuelst =
                            JsonHelper.Deserialize<List<PromotionDetailValue>>(lst.OrderPromotionDetails);
                    if (PromotionDetailValuelst != null && PromotionDetailValuelst.Count > 0)
                    {
                        var orderpromoList = " ";
                        foreach (var orderpromotionlst in PromotionDetailValuelst)
                            if (orderpromotionlst != null)
                                orderpromoList = orderpromoList + orderpromotionlst.PromotionName + " , ";
                        if (orderpromoList.Length > 0)
                            orderpromoList = orderpromoList.Left(orderpromoList.Length - 2);
                        lst.PromotionName = orderpromoList;
                    }
                }

                var TicketPromotionDetailValuelst = new List<PromotionDetailValue>();
                if (!string.IsNullOrEmpty(returnList.TicketPromotionDetails))
                {
                    TicketPromotionDetailValuelst =
                        JsonHelper.Deserialize<List<PromotionDetailValue>>(returnList.TicketPromotionDetails);
                    if (TicketPromotionDetailValuelst != null && TicketPromotionDetailValuelst.Count > 0)
                    {
                        decimal totalPromoAmount = 0;
                        foreach (var ticketpromotionlst in TicketPromotionDetailValuelst)
                            if (ticketpromotionlst != null)
                                totalPromoAmount = totalPromoAmount + ticketpromotionlst.PromotionAmount;
                        returnList.TicketPromotionAmount = totalPromoAmount;
                    }
                }
            }
            else
            {
                returnList = new TicketListDto();
            }

            // Payment in transaction  = sum payment
            foreach (var item in returnList.Transactions)
                if (item.TransactionTypeName.ToUpper() == "PAYMENT")
                    item.Amount = returnList.Payments.Sum(x => x.Amount);

            return returnList;
        }

        public void CreateTicket(Transaction.Ticket ticket)
        {
            try
            {
                foreach (var ord in ticket.Orders)
                    if (ord != null && !string.IsNullOrEmpty(ord.MenuItemName))
                        if (ord.MenuItemName.Length > 100)
                            ord.MenuItemName = ord.MenuItemName.Substring(0, 99);
                _ticketManager.Insert(ticket);
            }
            catch (Exception exception)
            {
                _logger.Log(LogSeverity.Error, "Insert Ticket", exception);
            }
        }

        public async Task<DashboardTicketDto> GetDashboard()
        {
            return GetStatics(_ticketManager.GetAll());
        }

        [AbpAuthorize]
        public async Task<TicketStatsDto> GetDashboardChart(GetChartInput input)
        {
            return await _cService.GetDashboardChart(input);
        }

        public List<ChartOutputDto> GetTransactionChart(GetChartInput input)
        {
            var result = GetChartTickets(input).SelectMany(a => a.Transactions)
                .GroupBy(a => a.TransactionType.Name).Select(a1 => new ChartOutputDto
                {
                    name = a1.Key,
                    y = a1.Sum(a => a.Amount)
                }).ToList();

            return result;
        }

        public List<ChartOutputDto> GetPaymentChart(GetChartInput input)
        {
            var result =
                GetChartTickets(input).SelectMany(a => a.Payments)
                    .GroupBy(a => a.PaymentType.Name).Select(a1 => new ChartOutputDto
                    {
                        name = a1.Key,
                        y = a1.Sum(a => a.Amount)
                    }).ToList();

            return result;
        }

        public List<ChartOutputDto> GetDepartmentChart(GetChartInput input)
        {
            var result = GetChartTickets(input).GroupBy(l => l.DepartmentName)
                .Select(cl => new ChartOutputDto
                {
                    name = cl.Key,
                    y = cl.Sum(c => c.TotalAmount)
                }).ToList();

            return result;
        }

        public List<ChartOutputDto> GetItemChart(GetChartInput input)
        {
            var result =
                GetChartTickets(input)
                    .SelectMany(a => a.Orders)
                    .Where(a => a.CalculatePrice && a.DecreaseInventory)
                    .GroupBy(a => a.MenuItemName).Select(a1 => new ChartOutputDto
                    {
                        name = a1.Key,
                        y = a1.Sum(a => a.Quantity)
                    }).OrderByDescending(a => a.y).Take(10).ToList();
            return result;
        }

        public async Task<PagedResultOutput<PaymentListDto>> GetPaymentForTicketId(int ticketId)
        {
            var output = _paRepository.GetAll().Where(a => a.TicketId.Equals(ticketId));
            var categoryListDtos = output.MapTo<List<PaymentListDto>>();

            var menuItemCount = await output.CountAsync();

            return new PagedResultOutput<PaymentListDto>(
                menuItemCount,
                categoryListDtos
            );
        }

        public async Task<PagedResultOutput<TicketTransactionListDto>> GetTransactionForTicketId(int ticketId)
        {
            var output = _taRepository.GetAll().Where(a => a.TicketId.Equals(ticketId));
            var categoryListDtos = output.MapTo<List<TicketTransactionListDto>>();

            var menuItemCount = await output.CountAsync();

            return new PagedResultOutput<TicketTransactionListDto>(
                menuItemCount,
                categoryListDtos
            );
        }

        public async Task<PagedResultOutput<OrderListDto>> GetOrderForTicketId(int ticketId)
        {
            var output = _orRepository.GetAll().Where(a => a.TicketId.Equals(ticketId));
            var categoryListDtos = output.MapTo<List<OrderListDto>>();

            var menuItemCount = await output.CountAsync();

            return new PagedResultOutput<OrderListDto>(
                menuItemCount,
                categoryListDtos
            );
        }

        private IQueryable<Transaction.Ticket> GetChartTickets(GetChartInput input)
        {
            var tickets = _cService.GetAllTickets(input);
            var outTickets = tickets.Where(a => a.TenantId == AbpSession.TenantId
                                                && input.Location > 0
                ? a.LocationId == input.Location
                : true && a.IsClosed);

            return outTickets;
        }

        private async Task CreateMemberAndPoints(string ticketEntities, int locationId, int tenantId,
            decimal totalAmount, string ticketNumber, DateTime createdTime, bool calOnOrder)
        {
        }

        private async Task<PagedResultOutput<TicketListDto>> GetTickets(GetTicketInput input)
        {
            var tickets = _cService.GetAllTicketsForTicketInput(input);
            var sortTickets = await tickets.OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var categoryListDtos = sortTickets.MapTo<List<TicketListDto>>();
            var menuItemCount = await tickets.CountAsync();

            return new PagedResultOutput<TicketListDto>(
                menuItemCount,
                categoryListDtos
            );
        }

        private DashboardTicketDto GetStatics(IQueryable<Transaction.Ticket> allTickets)
        {
            var ticketDto = new DashboardTicketDto();

            if (!allTickets.Any()) return ticketDto;
            try
            {
                ticketDto.TotalTicketCount = allTickets.Count();
                ticketDto.TotalAmount = allTickets.DefaultIfEmpty().Sum(a => a.TotalAmount);
                ticketDto.AverageTicketAmount = ticketDto.TotalAmount / ticketDto.TotalTicketCount;
                ticketDto.TotalOrderCount = allTickets.Sum(a => a.Orders.Count);

                var tickets = from p in allTickets.Include(a => a.Orders)
                    where p.Orders.Any(pt => pt.DecreaseInventory.Equals(true))
                    select p.Orders;
                ticketDto.TotalItemSold = tickets.Sum(a => a.Sum(b => b.Quantity));
            }
            catch (Exception)
            {
                // ignored
            }

            return ticketDto;
        }

        private async Task<int> CreateTicketAsync(TicketEditDto input)
        {
            var ticket = input.MapTo<Transaction.Ticket>();
            ticket.Id = 0;
            var availTicket =
                await _ticketManager.FirstOrDefaultAsync(
                    a =>
                        a.TicketNumber.Equals(input.TicketNumber) &&
                        a.LocationId.Equals(input.LocationId) && a.TenantId == input.TenantId);

            if (availTicket != null)
            {
                return availTicket.Id;
            }

            foreach (var order in ticket.Orders)
            {
                order.Location_Id = ticket.LocationId;
                if (!string.IsNullOrEmpty(order.OrderTags))
                {
                    var tTake = JsonConvert.DeserializeObject<List<ReOrderTagValue>>(order.OrderTags);

                    if (order.TransactionOrderTags == null)
                        order.TransactionOrderTags = new List<TransactionOrderTag>();
                    foreach (var otv in tTake) order.TransactionOrderTags.Add(await GetTransactionTag(otv));
                }
            }

            try
            {
                using (var unitOfWork = _unitOfWorkManager.Begin(TransactionScopeOption.RequiresNew))
                {
                    var ticketId = await _ticketManager.InsertAndGetIdAsync(ticket);
                    unitOfWork.Complete();
                    return ticketId;
                }
            }
            catch (Exception exception)
            {

                if (input!= null && !string.IsNullOrEmpty(input.TicketNumber) && input.TenantId>0 && input.LocationId>0)
                {
                    var turnedTicket =
                        await _ticketManager.FirstOrDefaultAsync(
                            a =>
                                a.TicketNumber.Equals(input.TicketNumber) &&
                                a.LocationId.Equals(input.LocationId) && a.TenantId == input.TenantId);

                    if (turnedTicket != null)
                    {
                        return turnedTicket.Id;

                    }
                }


                _logger.Fatal("Ticket Sync Failed");
                _logger.Fatal("-------------------");
                _logger.Fatal("Location : " + ticket.LocationId);
                _logger.Fatal("Ticket Number: " + ticket.TicketNumber);
                _logger.Fatal("Tenant Id : " + ticket.TenantId);
                _logger.Fatal("Ticket Details : " + JsonConvert.SerializeObject(input));
                _logger.Fatal("Exception : ", exception);
            }

            return 0;
        }

        private async Task<int> UpdateTicketAsync(TicketEditDto input)
        {
            if (input.Id == null)
                return 0;

            

            var inputDto = input;
            Transaction.Ticket dbTicket;
            try
            {
                dbTicket = _ticketManager.Get(input.Id.Value);
            }
            catch (Exception)
            {
                await _ticketManager.DeleteAsync(input.Id.Value);
                var returnValue = await CreateTicketAsync(input);
                return returnValue;
            }

            if (dbTicket == null) return 0;
            dbTicket.InvoiceNo = inputDto.InvoiceNo;
            dbTicket.FullTaxInvoiceDetails = inputDto.FullTaxInvoiceDetails;
            dbTicket.LocationId = inputDto.LocationId;
            dbTicket.TaxIncluded = inputDto.TaxIncluded;
            dbTicket.TicketId = inputDto.TicketId;
            dbTicket.TicketNumber = inputDto.TicketNumber;
            dbTicket.RemainingAmount = inputDto.RemainingAmount;
            dbTicket.TotalAmount = inputDto.TotalAmount;
            dbTicket.TicketTypeName = inputDto.TicketTypeName;
            dbTicket.LastModifiedUserName = inputDto.LastModifiedUserName;
            dbTicket.Note = inputDto.Note;
            dbTicket.TerminalName = inputDto.TerminalName;
            dbTicket.Credit = inputDto.Credit;
            dbTicket.ReferenceNumber = inputDto.ReferenceNumber;
            dbTicket.DepartmentName = inputDto.DepartmentName;
            dbTicket.DepartmentGroup = inputDto.DepartmentGroup;
            dbTicket.TicketTags = inputDto.TicketTags;
            dbTicket.TicketStates = inputDto.TicketStates;
            dbTicket.TicketLogs = inputDto.TicketLogs;
            dbTicket.LastUpdateTime = inputDto.LastUpdateTime;
            dbTicket.TicketCreatedTime = inputDto.TicketCreatedTime;
            dbTicket.LastOrderTime = inputDto.LastOrderTime;
            dbTicket.LastPaymentTime = inputDto.LastPaymentTime;
            dbTicket.LastPaymentTimeTruc = inputDto.LastPaymentTime.Date;
            dbTicket.IsClosed = inputDto.IsClosed;
            dbTicket.IsLocked = inputDto.IsLocked;
            dbTicket.PreOrder = inputDto.PreOrder;
            dbTicket.TicketEntities = inputDto.TicketEntities;
            dbTicket.WorkPeriodId = inputDto.WorkPeriodId;
            dbTicket.TicketPromotionDetails = inputDto.TicketPromotionDetails;
            dbTicket.TicketStatus = inputDto.TicketStatus;
            dbTicket.TicketLog1 = inputDto.TicketLog1;

            //Updating Order
            if (inputDto.Orders.Any())
            {
                var oIds = new List<int>();

                foreach (var otdto in inputDto.Orders)
                {
                    Order getOrder = null;
                    if (otdto.OrderId > 0) getOrder = dbTicket.Orders.SingleOrDefault(a => a.OrderId == otdto.OrderId);

                    if (getOrder == null && !otdto.OrderNumber.Equals("0"))
                        getOrder =
                            dbTicket.Orders.FirstOrDefault(
                                a => a.OrderNumber.Equals(otdto.OrderNumber) && a.MenuItemId == otdto.MenuItemId
                                                                             && otdto.PortionName.Equals(a.PortionName)
                                                                             && otdto.Quantity == a.Quantity
                                                                             && otdto.CalculatePrice == a.CalculatePrice
                                                                             && otdto.OrderCreatedTime ==
                                                                             a.OrderCreatedTime
                                                                             && otdto.IncreaseInventory ==
                                                                             a.IncreaseInventory
                                                                             && otdto.DecreaseInventory ==
                                                                             a.DecreaseInventory &&
                                                                             a.OrderTags.Equals(otdto.OrderTags));
                    if (getOrder == null)
                    {
                        if (string.IsNullOrEmpty(otdto.OrderTags)) otdto.OrderTags = "";

                        getOrder =
                            dbTicket.Orders.FirstOrDefault(
                                a => a.MenuItemId == otdto.MenuItemId
                                     && otdto.PortionName.Equals(a.PortionName)
                                     && otdto.Quantity == a.Quantity
                                     && otdto.CalculatePrice == a.CalculatePrice
                                     && otdto.OrderCreatedTime == a.OrderCreatedTime
                                     && otdto.IncreaseInventory == a.IncreaseInventory
                                     && otdto.DecreaseInventory == a.DecreaseInventory &&
                                     otdto.DepartmentName == a.DepartmentName &&
                                     a.OrderTags != null && a.OrderTags.Equals(otdto.OrderTags));

                        //NEED TO CHECK ORDER DATE
                    }

                    if (getOrder == null)
                    {
                        var order = Mapper.Map<OrderEditDto, Order>(otdto);
                        if (!string.IsNullOrEmpty(otdto.OrderTags))
                        {
                            var tTake = JsonConvert.DeserializeObject<List<ReOrderTagValue>>(otdto.OrderTags);
                            if (order.TransactionOrderTags == null)
                                order.TransactionOrderTags = new List<TransactionOrderTag>();
                            foreach (var otv in tTake) order.TransactionOrderTags.Add(await GetTransactionTag(otv));
                        }

                        order.Location_Id = inputDto.LocationId;
                        dbTicket.Orders.Add(order);
                    }
                    else
                    {
                        oIds.Add(getOrder.Id);
                        getOrder.Location_Id = inputDto.LocationId;
                        UpdateOrder(getOrder, otdto);
                    }
                }

                var tobeRemoved =
                    dbTicket.Orders.Where(a => !a.Id.Equals(0) && !oIds.Contains(a.Id)).Select(a => a.Id).ToArray();
                foreach (var order in tobeRemoved) await _orRepository.DeleteAsync(order);
            }
            else
            {
                var tobeRemoved = dbTicket.Orders.Where(a => a.TicketId == dbTicket.Id).Select(a => a.Id).ToArray();
                foreach (var payment in tobeRemoved) await _orRepository.DeleteAsync(payment);
            }

            //Updating Payments
            if (inputDto.Payments.Any())
            {
                var pids = new List<int>();
                foreach (var pto in inputDto.Payments)
                {
                    var getPayment =
                        dbTicket.Payments.SingleOrDefault(
                            a => a.PaymentTypeId == pto.PaymentTypeId && a.Amount == pto.Amount &&
                                 a.PaymentCreatedTime == pto.PaymentCreatedTime);

                    if (getPayment == null)
                    {
                        dbTicket.Payments.Add(Mapper.Map<PaymentEditDto, Payment>(pto));
                    }
                    else
                    {
                        pids.Add(getPayment.Id);
                        UpdatePayment(getPayment, pto);
                    }
                }

                var tobeRemoved = dbTicket.Payments.Where(a => !a.Id.Equals(0) && !pids.Contains(a.Id)).ToList();
                foreach (var order in tobeRemoved) await _paRepository.DeleteAsync(order.Id);
            }
            else
            {
                var tobeRemoved = dbTicket.Payments.Where(a => a.TicketId == dbTicket.Id).Select(a => a.Id).ToArray();
                foreach (var payment in tobeRemoved) await _paRepository.DeleteAsync(payment);
            }

            //Updating Transaction
            if (inputDto.Transactions.Any())
            {
                var tids = new List<int>();
                foreach (var pto in inputDto.Transactions)
                {
                    var getPayment =
                        dbTicket.Transactions.SingleOrDefault(
                            a => a.TransactionTypeId == pto.TransactionTypeId && a.Amount == pto.Amount);

                    if (getPayment == null)
                    {
                        dbTicket.Transactions.Add(Mapper.Map<TicketTransactionDto, TicketTransaction>(pto));
                    }
                    else
                    {
                        tids.Add(getPayment.Id);
                        UpdateTicketTransaction(getPayment, pto);

                        if (input.TotalAmount == 0M) getPayment.Amount = 0;
                    }
                }

                var tobeRemoved = dbTicket.Transactions.Where(a => !a.Id.Equals(0) && !tids.Contains(a.Id)).ToList();
                foreach (var order in tobeRemoved) await _taRepository.DeleteAsync(order.Id);
            }
            else
            {
                var tobeRemoved =
                    dbTicket.Transactions.Where(a => a.TicketId == dbTicket.Id).Select(a => a.Id).ToArray();
                foreach (var payment in tobeRemoved) await _taRepository.DeleteAsync(payment);
            }

            //TicketLog
            //var removeTicketLog = _planLogRepo.GetAll().Where(s => s.TicketNo == inputDto.TicketNumber && s.TenantId == inputDto.TenantId && s.LocationId == inputDto.LocationId);
            //foreach (var log in removeTicketLog) _planLogRepo.Delete(log);

            try
            {
                await _ticketManager.InsertOrUpdateAndGetIdAsync(dbTicket);
            }
            catch (Exception exception)
            {
                _logger.Fatal("TICKET UPDATE ", exception);
            }

            return input.Id.Value;
        }

        private void UpdateTicketTransaction(TicketTransaction to, TicketTransactionDto from)
        {
            to.TransactionTypeId = from.TransactionTypeId;
            to.Amount = from.Amount;
            to.TransactionNote = from.TransactionNote;
        }

        private async Task UpdatePlanLog(TicketEditDto inputDto)
        {
            if (string.IsNullOrEmpty(inputDto.TicketLogs)) return;
            try
            {
                var ticketLogValues = JsonHelper.Deserialize<List<TicketLogValue>>(inputDto.TicketLogs);
                var listLog = new List<PlanLogger>();
                foreach (var log in ticketLogValues)
                    if (!string.IsNullOrEmpty(log.Log))
                    {
                        var listEventLog = JsonConvert.DeserializeObject<List<SimpleKeyValuePair>>(log.Log);
                        var findNotTask = listEventLog.FirstOrDefault(s => s.Key == "IsTask");
                        if (findNotTask == null)
                        {
                            var stringRemove = new List<string> { "Terminal", "TicketNo", "TicketTotal" };
                            var itemLog = new CreateOrUpdatePlanLoggerInput();
                            var itemPlanLoger = new PlanLoggerEditDto
                            {
                                EventName = log.Category,
                                EventTime = log.DateTime,
                                TenantId = inputDto.TenantId,
                                LocationId = inputDto.LocationId,
                                TicketNo = log.TicketNo,
                                UserName = log.UserName,
                                Terminal = listEventLog.FirstOrDefault(s => s.Key == "Terminal")?.Value
                            };
                            try
                            {
                                itemPlanLoger.TicketTotal =
                                    Convert.ToDecimal(listEventLog.FirstOrDefault(s => s.Key == "TicketTotal").Value);
                            }
                            catch
                            {
                            }

                            var listEventLogToString = listEventLog.Where(item => !stringRemove.Contains(item.Key))
                                .Select(item => item.Key + "=" + item.Value);
                            itemPlanLoger.EventLog = string.Join(";", listEventLogToString.ToArray());
                            itemLog.PlanLogger = itemPlanLoger;
                            await _planLoggerAppService.CreateOrUpdateLog(itemLog);
                        }
                    }
            }
            catch (Exception e)
            {
                _logger.Fatal("ADD PLAN LOG FAILED");
                _logger.Fatal("-------------------");
                _logger.Fatal("ADD PLAN LOG FAILED: ", e);
            }
        }

        private void UpdatePayment(Payment to, PaymentEditDto from)
        {
            to.PaymentTypeId = from.PaymentTypeId;
            to.PaymentCreatedTime = from.PaymentCreatedTime;
            to.TenderedAmount = from.TenderedAmount;
            to.TerminalName = from.TerminalName;
            to.Amount = from.Amount;
            to.PaymentUserName = from.PaymentUserName;
            to.PaymentTags = from.PaymentTags;
        }

        private void UpdateOrder(Order to, OrderEditDto from)
        {
            to.OrderId = from.OrderId;
            to.DepartmentName = from.DepartmentName;
            to.MenuItemId = from.MenuItemId;
            to.MenuItemName = from.MenuItemName;
            to.PortionName = from.PortionName;
            to.Price = from.Price;
            to.CostPrice = from.CostPrice;
            to.Quantity = from.Quantity;
            to.PortionCount = from.PortionCount;
            to.Note = from.Note;
            to.Locked = from.Locked;
            to.CalculatePrice = from.CalculatePrice;
            to.IncreaseInventory = from.IncreaseInventory;
            to.DecreaseInventory = from.DecreaseInventory;
            to.OrderNumber = from.OrderNumber;
            to.CreatingUserName = from.CreatingUserName;
            to.OrderCreatedTime = from.OrderCreatedTime;
            to.PriceTag = from.PriceTag;
            to.Taxes = from.Taxes;
            to.OrderTags = from.OrderTags;
            to.OrderStates = from.OrderStates;
            to.IsPromotionOrder = from.IsPromotionOrder;
            to.PromotionSyncId = from.PromotionSyncId;
            to.PromotionAmount = from.PromotionAmount;
            to.MenuItemPortionId = from.MenuItemPortionId;
            to.OrderPromotionDetails = from.OrderPromotionDetails;

            to.MenuItemType = from.MenuItemType;
            to.OrderRef = from.OrderRef != null ? @from.OrderRef : "";
            to.IsParent = from.IsParent;
            to.IsUpSelling = from.IsUpSelling;
            to.UpSellingOrderRef = from.UpSellingOrderRef;
            to.OriginalPrice = from.OriginalPrice;

            to.OrderLog = from.OrderLog;
            to.OrderStatus = from.OrderStatus;

            to.BindState = from.BindState;
            to.BindStateValues = from.BindStateValues;
            to.BindCompleted = from.BindCompleted;
            to.NumberOfPax = from.NumberOfPax;
        }

        private async Task<TransactionOrderTag> GetTransactionTag(ReOrderTagValue otv)
        {
            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                var portionId = 0;
                if (otv.MI > 0)
                {
                    var menuItem = _miRepository.Get(otv.MI);
                    if (menuItem != null && menuItem.Portions.Any())
                    {
                        var myPortion = menuItem.Portions.First();
                        portionId = myPortion.Id;
                    }
                }

                return new TransactionOrderTag
                {
                    MenuItemPortionId = portionId,
                    OrderTagGroupId = otv.GID,
                    OrderTagId = otv.TID,
                    Quantity = otv.Q,
                    Price = otv.PR,
                    TagName = otv.TN,
                    TagNote = otv.TO,
                    TagValue = otv.TV,
                    TaxFree = otv.TF,
                    AddTagPriceToOrderPrice = otv.AP
                };
            }
        }
    }
}