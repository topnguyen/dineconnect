﻿using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.Implementation
{
	public class ConsolidatedAppService : DineConnectAppServiceBase, IConsolidatedAppService
	{
		private readonly IReportBackgroundAppService _rbas;
		private readonly IBackgroundJobManager _bgm;
		private readonly IConsolidatedExporter _exporter;
		private readonly IRepository<Transaction.Ticket> _ticketRepository;
		private readonly IConnectReportAppService _connectReportAppService;

		public ConsolidatedAppService(
			 IReportBackgroundAppService rbas,
			 IBackgroundJobManager bgm,
			 IConsolidatedExporter exporter,
			 IRepository<Transaction.Ticket> ticketRepository,
			 IConnectReportAppService connectReportAppService
			)
		{
			_rbas = rbas;
			_bgm = bgm;
			_exporter = exporter;
			_ticketRepository = ticketRepository;
			_connectReportAppService = connectReportAppService;
		}
		public async Task<ConsolidatedDto> GetConsolidated(GetTicketInput input)
		{
			var result = new ConsolidatedDto();
			var ticketsForFilter = _connectReportAppService.GetAllTicketsForTicketInput(input).ToList();

			var tickets = new List<ConsolidatedItem>();
			foreach(var ticket in ticketsForFilter)
			{
				var subTotal = ticket.GetPlainSum() + ticket.GetPreTaxServicesTotal();
				var tax = ticket.GetTaxTotal();
				if (ticket.TaxIncluded) subTotal = subTotal - tax;
				tickets.Add(new ConsolidatedItem
				{
					LocationID = ticket.LocationId,
					DepartmentName = ticket.DepartmentName,
					LocationName = ticket.Location.Name,
					WithOutTax = subTotal,
					WithTax = ticket.TotalAmount,
				});
			}

			var ticketBy_Location_Deparment = tickets.GroupBy(x => new { x.LocationID, x.LocationName, x.DepartmentName, })
				 .Select(x => new ConsolidatedBy_Location_Department
				 {
					 LocationID = x.Key.LocationID,
					 LocationName = x.Key.LocationName,
					 DepartmentName = x.Key.DepartmentName,
					 ListItem = x.ToList()
				 });
			var ticketBy_Location = ticketBy_Location_Deparment.GroupBy(x => new { x.LocationID, x.LocationName, })
			 .Select(x => new ConsolidatedBy_Location
			 {
				 LocationID = x.Key.LocationID,
				 LocationName = x.Key.LocationName,
				 ListItem = x.ToList()
			 });
			result.ListItem = ticketBy_Location.ToList();
			return await Task.FromResult(result);
		}

		public async Task<FileDto> GetConsolidatedExport(GetTicketInput input)
		{
			if (AbpSession.UserId != null && AbpSession.TenantId != null)
			{
				if (input.RunInBackground)
				{
					var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
					{
						ReportName = ReportNames.CONSOLIDATED,
						Completed = false,
						UserId = AbpSession.UserId.Value,
						TenantId = AbpSession.TenantId.Value,
						ReportDescription = input.ReportDescription
					});
					if (backGroundId > 0)
						await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
						{
							BackGroundId = backGroundId,
							ReportName = ReportNames.CONSOLIDATED,
							UserId = AbpSession.UserId.Value,
							TenantId = AbpSession.TenantId.Value,
							TicketInput = input
						});
				}
				else
				{
					return await _exporter.ExportConsolidatedReportToFile(input, this);
				}
			}

			return null;
		}
	}
}
