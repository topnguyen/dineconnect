﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Castle.Core.Logging;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Connect.Users;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Connect.Ticket.Implementation
{
    public class PaymentTagAppService : DineConnectAppServiceBase, IPaymentTagAppService
    {
        private readonly IConnectReportAppService _crService;
        private readonly IRepository<DinePlanUser> _dineUserRepo;
        private readonly ILogger _logger;

        public PaymentTagAppService(IConnectReportAppService crService, IRepository<DinePlanUser> dineUserRepo, 
            ILogger logger)
        {
            _crService = crService;
            _dineUserRepo = dineUserRepo;
            _logger = logger;

        }

        public async Task<PaymentTagOutputDto> ApiGetAllPaymentTags(GetTicketInput inputDto)
        {
            _logger.Fatal("Start Date : " + inputDto.StartDate);
            _logger.Fatal("End Date : " + inputDto.EndDate);

            PaymentTagOutputDto mro = new PaymentTagOutputDto();

            var allTickets = _crService.GetAllTickets(inputDto);

            var allPaymentTags = 
                allTickets.Where(a=>!a.TicketStates.Contains("Refund")).SelectMany(a => a.Payments).Where(a => !string.IsNullOrEmpty(a.PaymentTags));
            AppendTags(allPaymentTags, mro, false);

            allPaymentTags = 
                allTickets.Where(a=>a.TicketStates.Contains("Refund")).SelectMany(a => a.Payments).Where(a => !string.IsNullOrEmpty(a.PaymentTags));
            AppendTags(allPaymentTags, mro, true);

            return mro;
        }

        private void AppendTags(IQueryable<Payment> allPaymentTags, PaymentTagOutputDto mro, bool refund)
        {
            foreach (var paymentTag in allPaymentTags)
            {
                var myTag = new PaymentTagInfo
                {
                    InvoiceNo = paymentTag.Ticket.InvoiceNo,
                    TotalAmount = paymentTag.Amount,
                    PaymentName = paymentTag.PaymentType.Name,
                    TicketNo = paymentTag.Ticket.TicketNumber,
                    PaymentTime = paymentTag.Ticket.LastPaymentTime,
                    Terminal = paymentTag.TerminalName,
                    UserName = paymentTag.PaymentUserName,
                    UserCode = "101",
                    LocationCode = paymentTag.Ticket.Location.Code,
                    Refund =  refund
                };
                if (refund)
                    myTag.TicketNo = paymentTag.Ticket.ReferenceTicket;
                try
                {
                    myTag.PaymentFrame = JsonConvert.DeserializeObject<PaymentFrame>(paymentTag.PaymentTags);
                    var lastUser = _dineUserRepo.FirstOrDefault(a => a.Name.Equals(myTag.UserName));
                    if (lastUser != null)
                    {
                        myTag.UserCode = lastUser.Code;
                    }
                }
                catch (Exception e)
                {
                    var message = e.Message;
                    continue;
                }

                mro.Tags.Add(myTag);
            }

        }
    }
}
