﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using System.Web;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Collections.Extensions;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Auditing.Dto;
using DinePlan.DineConnect.Auditing.Exporting;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Audit;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Connect.Tag;
using DinePlan.DineConnect.Connect.Ticket.CategoryReport.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Connect.WorkPeriod.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.Handler.Mapping;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.House.Transaction.Implementation;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Migrations;
using DinePlan.DineConnect.Report;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace DinePlan.DineConnect.Connect.Ticket.Implementation
{
    public class ConnectReportAppService : ConnectReportAppServiceBase, IConnectReportAppService
    {
        private readonly IAuditLogListExcelExporter _auditLogListExcelExporter;
        private readonly IBackgroundJobManager _bgm;
        private readonly IRepository<CashAudit> _cashAuditManager;
        private readonly IRepository<Category> _catRepository;
        private readonly IRepository<ProductComboItem> _comboIManager;
        private readonly IRepository<Company> _companyRe;
        private readonly IRepository<Department> _departmentRepo;
        private readonly IRepository<Department> _dRepo;
        private readonly ITicketListExcelExporter _exporter;
        private readonly IRepository<ExternalLog> _externalLogRepo;
        private readonly IHourExporter _hourlyExporter;
        private readonly ILocationAppService _locService;
        private readonly IRepository<Master.Location> _lRepo;
        private readonly IRepository<MenuItem> _mrRepo;
        private readonly IRepository<Payment> _paymentRepo;
        private readonly IRepository<PaymentType> _payRe;
        private readonly IRepository<MenuItemPortion> _poRepository;
        private readonly IRepository<ProductGroup> _productGroupRepo;
        private readonly IRepository<Promotion> _promoRepo;
        private readonly IPaymentTypeAppService _ptService;
        private readonly IReportBackgroundAppService _rbas;
        private readonly IRepository<TicketTagGroup> _tagGroupRepo;
        private readonly ITenantSettingsAppService _tenantSettingsService;
        private readonly IRepository<Master.Terminal> _terminalRepo;
        private readonly IRepository<Transaction.Ticket> _ticketManager;
        private readonly IRepository<TransactionType> _tRe;
        private readonly ITransactionTypeAppService _ttService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Period.WorkPeriod> _workPeriod;
        private readonly IRepository<TransactionOrderTag> _rTransactionOrderTags;

        public ConnectReportAppService(IRepository<Transaction.Ticket> ticketManager,
            ILocationAppService locService,
            IRepository<TransactionType> tRepo,
            IRepository<TicketTagGroup> tagRepo,
            IRepository<PaymentType> pRepo,
            IRepository<MenuItemPortion> poRepository,
            IRepository<Category> caRepository,
            IRepository<Company> comR,
            ITicketListExcelExporter exporter,
            IPaymentTypeAppService paymentTypeAppService,
            ITransactionTypeAppService ttService,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<MenuItem> mrRepo,
            IRepository<Department> drepo,
            IRepository<Promotion> promoRepo,
            IBackgroundJobManager bgm,
            IRepository<Master.Location> lRepo,
            IRepository<Period.WorkPeriod> workPeriod,
            IHourExporter hourExporter,
            IReportBackgroundAppService rbas,
            IRepository<ProductGroup> productGroupRepo,
            IRepository<Master.Terminal> terminalRepo,
            IRepository<ProductComboItem> comboIManager,
            IRepository<Department> departmentRepo,
            IRepository<Payment> paymentRepo,
            ITenantSettingsAppService tenantSettingsService,
            IAuditLogListExcelExporter auditLogListExcelExporter,
            IRepository<ExternalLog> externalLogRepo,
            IRepository<CashAudit> cashAuditManager,
            IRepository<TransactionOrderTag> rTransactionOrderTags
        ) : base(ticketManager, locService, unitOfWorkManager, lRepo)
        {
            _rbas = rbas;
            _productGroupRepo = productGroupRepo;
            _comboIManager = comboIManager;
            _bgm = bgm;
            _tagGroupRepo = tagRepo;
            _ticketManager = ticketManager;
            _companyRe = comR;
            _payRe = pRepo;
            _tRe = tRepo;
            _poRepository = poRepository;
            _mrRepo = mrRepo;
            _catRepository = caRepository;
            _exporter = exporter;
            _ptService = paymentTypeAppService;
            _ttService = ttService;
            _unitOfWorkManager = unitOfWorkManager;
            _dRepo = drepo;
            _promoRepo = promoRepo;
            _locService = locService;
            _workPeriod = workPeriod;
            _hourlyExporter = hourExporter;
            _lRepo = lRepo;
            _terminalRepo = terminalRepo;
            _departmentRepo = departmentRepo;
            _paymentRepo = paymentRepo;
            _tenantSettingsService = tenantSettingsService;
            _auditLogListExcelExporter = auditLogListExcelExporter;
            _externalLogRepo = externalLogRepo;
            _cashAuditManager = cashAuditManager;
            _rTransactionOrderTags = rTransactionOrderTags;
        }

        public async Task<TicketStatsDto> GetTickets(GetTicketInput input, bool onlystatus = false)
        {
            var tickets = GetAllTicketsForTicketInput(input);


            var returnOutput = new TicketStatsDto();
            if (!onlystatus)
            {
                List<TicketListDto> outputDtos = null;
                List<TicketViewDto> outputViewDtos = new List<TicketViewDto>();
                var sortTickets = await tickets.OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();
                var menuItemCount = await tickets.CountAsync();

                if (!string.IsNullOrEmpty(input.OutputType) && input.OutputType.Equals("EXPORT"))
                {
                    outputDtos = sortTickets.MapTo<List<TicketListDto>>();
                    returnOutput.TicketList = new PagedResultOutput<TicketListDto>(
                        menuItemCount,
                        outputDtos
                    );
                }
                else
                {

                    foreach(var tick in sortTickets)
                    {
                        var tableName = "";
                        int pax = tick.Orders.Sum(s=>s.NumberOfPax);
                        if (!string.IsNullOrEmpty(tick.TicketEntities))
                        {
                            var allEntities =
                                JsonConvert.DeserializeObject<IList<TicketEntityMap>>(tick.TicketEntities);
                            var lastEntity = allEntities.LastOrDefault(a => a.EntityTypeId == 2);

                            if (lastEntity != null) tableName = lastEntity.EntityName;
                        }
                        
                        outputViewDtos.Add(new TicketViewDto
                        {
                            Id = tick.Id,
                            LocationName = tick.Location?.Name,
                            LastModifiedUserName = tick.LastModifiedUserName,
                            LocationId = tick.LocationId,
                            TicketId = tick.TicketId,
                            TicketNumber = tick.TicketNumber,
                            TotalAmount = tick.TotalAmount,
                            DepartmentName = tick.DepartmentName,
                            TerminalName = tick.TerminalName,
                            Pax = pax,                            
                            TableNumber = tableName,
                        });
                    }
                    returnOutput.TicketViewList = new PagedResultOutput<TicketViewDto>(
                        menuItemCount,
                        outputViewDtos
                    );
                }
            }

            returnOutput.DashBoardDto = GetStatics(tickets);
            return returnOutput;
        }

        public async Task<PagedResultOutput<GetTicketSyncOutput>> GetTicketSyncs(GetTicketSyncInput input)
        {
            var result = new PagedResultOutput<GetTicketSyncOutput>();

            var listTicketListDto = new List<GetTicketSyncOutput>();

            try
            {
                var workPeriods = new List<Period.WorkPeriod>();

                var allWorkPeriods = GetAllPeriodByCondition(input);

                foreach (var workPeriod in allWorkPeriods)
                {
                    var myLocation = await _locService.GetLocationById(new EntityDto(workPeriod.LocationId));

                    var startTime = workPeriod.StartTime.Date;
                    var endDate = workPeriod.EndTime.Date + new TimeSpan(23, 23, 59);

                    var tickets = await _ticketManager.GetAllListAsync(x =>
                        x.LastPaymentTimeTruc >= startTime && x.LastPaymentTimeTruc <= endDate);

                    if (tickets == null) continue;

                    var ticketSyncCount = tickets.Count();

                    var ticketTotalAmount = tickets.Sum(x => x.TotalAmount);

                    listTicketListDto.Add(new GetTicketSyncOutput
                    {
                        LocationCode = myLocation?.Code,
                        LocationName = myLocation?.Name,
                        TicketSyncCount = ticketSyncCount,
                        TicketTotalAmount = ticketTotalAmount,
                        WorkStartDate = workPeriod.StartTime,
                        WorkCloseDate = workPeriod.EndTime,
                        TotalTickets = workPeriod.TotalTicketCount,
                        WorkTotalAmount = workPeriod.TotalSales,
                        IsDifference = ticketSyncCount != workPeriod.TotalTicketCount ||
                                       ticketTotalAmount != workPeriod.TotalSales
                    });
                }


                if (input.Sorting == null) input.Sorting = "locationCode";
                var dataAsQueryable = listTicketListDto.AsQueryable();
                // filter
                if (!string.IsNullOrEmpty(input.DynamicFilter))
                {
                    var jsonSerializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver =
                            new CamelCasePropertyNamesContractResolver()
                    };
                    var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                    if (filRule?.Rules != null) dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                }

                var output = dataAsQueryable.OrderBy(input.Sorting).PageBy(input).ToList();
                result = new PagedResultOutput<GetTicketSyncOutput>
                {
                    Items = output,
                    TotalCount = workPeriods.Count
                };

                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }

        public IQueryable<Period.WorkPeriod> GetAllPeriodByCondition(GetTicketSyncInput input)
        {
            var workPeriods = new List<Period.WorkPeriod>();

            var allWorkPeriods = _workPeriod.GetAll()
                .Where(x => x.StartTime >= input.StartDate && x.EndTime <= input.EndDate && x.EndUser != null);
            if (input.Location > 0)
            {
                allWorkPeriods = allWorkPeriods.Where(a => a.LocationId == input.Location);
            }
            else if (input.Locations != null && input.Locations.Any())
            {
                var locations = input.Locations.Select(a => a.Id).ToList();
                allWorkPeriods = allWorkPeriods.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
                     && !input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                if (locations.Any()) allWorkPeriods = allWorkPeriods.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                allWorkPeriods = allWorkPeriods.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true
                });
                allWorkPeriods = allWorkPeriods.Where(a => locations.Contains(a.LocationId));
            }

            if (input.LocationGroup != null)
            {
                if (input.LocationGroup.NonLocations != null && input.LocationGroup.NonLocations.Any())
                {
                    var nonlocations = input.LocationGroup.NonLocations.Select(a => a.Id).ToList();
                    allWorkPeriods = allWorkPeriods.Where(a => !nonlocations.Contains(a.LocationId));
                }
            }

            else if (input.LocationGroup == null)
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = new List<SimpleLocationDto>(),
                    Group = false,
                    UserId = input.UserId
                });
                if (input.UserId > 0)
                    allWorkPeriods = allWorkPeriods.Where(a => locations.Contains(a.LocationId));
            }

            return allWorkPeriods;
        }

        public async Task<List<AllTicketListDto>> GetFullTickets(GetTicketInput input)
        {
            var returnList = new List<AllTicketListDto>();
            var allTickets = GetAllTicketsForTicketInput(input);

            var sortTickets = await allTickets.OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            if (sortTickets.Any()) returnList = sortTickets.MapTo<List<AllTicketListDto>>();
            var mItems = new List<MenuItem>();
            var pItems = new List<PaymentType>();
            var tItems = new List<TransactionType>();

            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                mItems = await _mrRepo.GetAllListAsync(a => a.Id > 0);
                pItems = await _payRe.GetAllListAsync(a => a.Id > 0);
                tItems = await _tRe.GetAllListAsync(a => a.Id > 0);
                foreach (var order in returnList.SelectMany(a => a.Orders))
                {
                    if (order.PromotionSyncId > 0)
                    {
                        var singlePromotion = _promoRepo.GetAll().SingleOrDefault(a => a.Id == order.PromotionSyncId);
                        if (singlePromotion != null) order.PromotionName = singlePromotion.Name;
                    }

                    var mItem = mItems.SingleOrDefault(a => a.Id == order.MenuItemId);
                    if (mItem != null) order.AliasCode = mItem.AliasCode;

                    if (!string.IsNullOrEmpty(order.OrderTags))
                    {
                        var myTagOrders = JsonConvert.DeserializeObject<List<ReOrderTagValue>>(order.OrderTags);
                        foreach (var mytagOder in myTagOrders)
                            if (mytagOder.MI > 0)
                            {
                                var tagItem = mItems.SingleOrDefault(a => a.Id == mytagOder.MI);
                                if (tagItem != null) mytagOder.ALIASCODE = tagItem.AliasCode;
                            }

                        order.OrderTags = JsonConvert.SerializeObject(myTagOrders);
                    }
                }

                foreach (var payment in returnList.SelectMany(a => a.Payments))
                {
                    var pItem = pItems.SingleOrDefault(a => a.Id == payment.PaymentTypeId);
                    if (pItem != null) payment.AccountCode = pItem.AccountCode;
                }

                foreach (var trans in returnList.SelectMany(a => a.Transactions))
                {
                    var tItem = tItems.SingleOrDefault(a => a.Id == trans.TransactionTypeId);
                    if (tItem != null) trans.AccountCode = tItem.AccountCode;
                }
            }

            return returnList;
        }

        public async Task<OrderStatsDto> GetOrders(GetItemInput input)
        {
            var returnOutput = new OrderStatsDto();
            var orders = GetAllOrdersNotFilterByDynamicFilter(input);

            var outputDtos = new List<OrderListDto>();
            List<OrderViewDto> orderViewDtos = null;
            var sortTickets = await orders.OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();
            if (!string.IsNullOrEmpty(input.OutputType) && input.OutputType.Equals("EXPORT"))
                foreach (var sortV in sortTickets)
                {
                    var orderListD = sortV.MapTo<OrderListDto>();
                    orderListD.TicketNumber = sortV.Ticket.TicketNumber;
                    orderListD.TerminalName = sortV.Ticket.TerminalName;

                    outputDtos.Add(orderListD);
                }
            else
                orderViewDtos = sortTickets.MapTo<List<OrderViewDto>>();

            var menuItemCount = await orders.CountAsync();
            returnOutput.OrderList = new PagedResultOutput<OrderListDto>(
                menuItemCount,
                outputDtos
            );

            returnOutput.OrderViewList = new PagedResultOutput<OrderViewDto>(
                menuItemCount,
                orderViewDtos
            );
            returnOutput.DashBoardDto = GetStatics(orders);
            return returnOutput;
        }

        public async Task<OrderExchangeDto> GetOrdersExchange(GetItemInput input)
        {
            var returnOutput = new OrderExchangeDto();
            var orders = GetAllOrdersExchange(input, false);
            //var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
            //if (filRule?.Rules != null)
            //{
            //	var descriptionFiters = filRule?.Rules.Where(x => x.Field == "Description");
            //	orders = orders.BuildQuery(filRule);
            //}
            var orderViewDtos = new List<OrderExchangeViewDto>();
            var lstOrder = new List<Order>();
            foreach (var order in orders)
            {
                var states = order.GetOrderStateValues();
                var exState = states.FirstOrDefault(x => x.State == "Exchange"); //PosConsts.Exchange
                if (exState == null) continue;

                var ticket = GetAllTickets(input).FirstOrDefault(a => a.Id.Equals(order.TicketId));
                if (ticket == null) continue;
                var ticketEntities = JsonConvert.DeserializeObject<IList<TicketEntityMap>>(ticket.TicketEntities);
                var entity = ticketEntities.SingleOrDefault(a => a.EntityTypeId.Equals(1));
                var payment = ticket.Payments.FirstOrDefault(x => x.TicketId == ticket.Id);
                if (payment == null) continue;
                lstOrder.Add(order);
                var geo = new OrderExchangeViewDto
                {
                    TicketId = ticket.Id,
                    DateTimeSort = payment.PaymentCreatedTime,
                    PlantCode = ticket.Location != null ? ticket.Location.Code : "",
                    PlantName = ticket.Location != null ? ticket.Location.Name : "",
                    ReceiptNumber = ticket.TicketNumber,
                    Detail = string.Empty,
                    MaterialName = order.MenuItemName,
                    Quantity = order.Quantity,
                    ExchangeBy = exState.UserName,
                    ExchangeDate = string.Format("{0} - {1}", exState.LastUpdateTime.ToShortDateString(),
                        exState.LastUpdateTime.ToShortTimeString()),
                    Exchange = exState.LastUpdateTime,
                    Reason = order.OrderLog != null ? order.OrderLog : "",
                    Tender = payment.PaymentType != null ? payment.PaymentType.Name : "",
                    DateTime = string.Format("{0} - {1}", payment.PaymentCreatedTime.ToShortDateString(),
                        payment.PaymentCreatedTime.ToShortTimeString()),
                    PaymentCreatedTime = payment.PaymentCreatedTime,
                    MaterialCode = order != null && order.MenuItem != null ? order.MenuItem.AliasCode : "",
                    Amount = order.Price * order.Quantity,
                    CustomerName = entity != null ? entity.EntityName : "",
                    SellerName = payment.PaymentUserName,
                    MaterialGroup = order != null && order.MenuItem != null && order.MenuItem.Category != null && order.MenuItem.Category.ProductGroup != null ? order.MenuItem.Category.ProductGroup.Name : "",
                    IsTotal = false
                };

                orderViewDtos.Add(geo);
            }
            var orderViewDtosSort = orderViewDtos.OrderBy(s => s.DateTimeSort);
            // 	filter by dymanic filter
            var orderViewAsQueryable = orderViewDtosSort.AsQueryable();
            if (!string.IsNullOrEmpty(input.DynamicFilter))
            {
                var jsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver =
                        new CamelCasePropertyNamesContractResolver()
                };
                var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                if (filRule?.Rules != null) orderViewAsQueryable = orderViewAsQueryable.BuildQuery(filRule);
            }
            var orderExchangePlant = new List<OrderExchangeByPlant>();
            var exchangeByPlant = orderViewAsQueryable.GroupBy(s => new { s.PlantCode, s.PlantName });
            var listScreen = new List<OrderExchangeViewDto>();
            foreach (var s in exchangeByPlant.ToList())
            {
                orderExchangePlant.Add(new OrderExchangeByPlant
                {
                    PlantCode = s.Key.PlantCode,
                    PlantName = s.Key.PlantName,
                    ListItem = s.ToList().MapTo<List<OrderExchangeViewDto>>()
                });
                var screenItem = s.ToList().MapTo<List<OrderExchangeViewDto>>();
                var total = new OrderExchangeViewDto();
                total.IsTotal = true;
                total.PlantCode = s.Key.PlantCode;
                total.PlantName = s.Key.PlantName;
                total.MaterialName = L("Total");
                total.Quantity = screenItem.Sum(a => a.Quantity);
                total.Amount = screenItem.Sum(a => a.Amount);
                listScreen.AddRange(screenItem);
                listScreen.Add(total);
            }

            //exchangeByPlant.ToList().ForEach(s => orderExchangePlant.Add(new OrderExchangeByPlant { 
            //PlantCode = s.Key.PlantCode,
            //PlantName = s.Key.PlantName,
            //ListItem = s.ToList().MapTo<List<OrderExchangeViewDto>>()       
            //}));
            var listScreenSort = listScreen.OrderBy(s => s.PlantCode).ThenBy(s => s.IsTotal).ThenBy(s => s.DateTimeSort);
            var orderExchangePlantOr = orderExchangePlant.OrderBy(s => s.PlantCode);
            returnOutput.ListItemScreen = listScreenSort.ToList();
            returnOutput.ListItem = orderExchangePlantOr.ToList();
            returnOutput.DashBoardDto = CreateDashBoardExchangeReport(orderViewAsQueryable.ToList());
            return await Task.FromResult(returnOutput);
        }

        public async Task<ListResultOutput<ComboboxItemDto>> GetReason()
        {
            var orders = GetAllOrdersExchange(new GetItemInput());
            orders = orders.Where(o => o.OrderLog != null);
            var allListDtos = orders.ToList();

            var allItemCount = await orders.CountAsync();
            var data = new PagedResultOutput<Order>(
                allItemCount,
                allListDtos
            );
            return
                new ListResultOutput<ComboboxItemDto>(
                    data.Items.Select(e => new ComboboxItemDto(e.Id.ToString(), e.OrderLog)).ToList());
        }

        [AbpAuthorize]
        public async Task<CategoryStatsDto> ApiGetCategorySales(GetItemInput input)
        {
            var returnOutput = new CategoryStatsDto();

            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                var categories = new List<string>();
                var chartOutput = new List<BarChartOutputDto>();
                var oDto = new List<CategoryReportDto>();
                if (input.Stats)
                {
                    var returnOrders = GetAllOrders(input);
                    if (returnOrders.Any())
                    {
                        var myItems = returnOrders.GroupBy(a => a.MenuItemId);
                        foreach (var myItem in myItems)
                        {
                            var menuItem = _mrRepo.Get(myItem.Key);
                            var myCategory = oDto.LastOrDefault(a => a.CategoryId == menuItem.CategoryId);

                            if (myCategory == null)
                            {
                                myCategory = new CategoryReportDto
                                {
                                    CategoryId = menuItem.CategoryId ?? 0,
                                    CategoryName = menuItem.Category.Name,
                                    Quantity = myItem.Sum(a => a.Quantity),
                                    Total = myItem.Sum(a => a.Quantity * a.Price)
                                };
                                oDto.Add(myCategory);
                            }
                            else
                            {
                                myCategory.Quantity += myItem.Sum(a => a.Quantity);
                                myCategory.Total += myItem.Sum(a => a.Quantity * a.Price);
                            }

                            myCategory.MenuItems.Add(new MenuReportView
                            {
                                AliasCode = menuItem.AliasCode,
                                MenuItemName = menuItem.Name,
                                Quantity = myItem.Sum(a => a.Quantity),
                                Price = myItem.Sum(a => a.Quantity * a.Price)
                            });
                        }
                    }

                    var menuItemCount = oDto.Count();
                    var items = oDto.AsQueryable().PageBy(input).ToList();
                    returnOutput.CategoryList = new PagedResultOutput<CategoryReportDto>(
                        menuItemCount,
                        items
                    );
                    returnOutput.Categories = categories;
                    returnOutput.ChartOutput = chartOutput;
                }
                else
                {
                    var returnSales = await GetItemSales(input);
                    if (returnSales.MenuList != null && returnSales.MenuList.Items.Any())
                    {
                        var outPut = from c in returnSales.MenuList.Items
                                     group c by c.CategoryId
                            into g
                                     select new { CategoryId = g.Key, Items = g };

                        foreach (var dto in outPut)
                        {
                            var cate = await _catRepository.GetAsync(dto.CategoryId);
                            categories.Add(cate.Name);
                            var quantity = dto.Items.Sum(a => a.Quantity);
                            var total = dto.Items.Sum(a => a.Quantity * a.Price);
                            chartOutput.Add(new BarChartOutputDto
                            {
                                name = cate.Name,
                                data = new List<decimal> { total }
                            });
                            oDto.Add(new CategoryReportDto
                            {
                                CategoryId = dto.CategoryId,
                                CategoryName = cate.Name,
                                Quantity = quantity,
                                Total = total,
                                TotalItems = dto.Items.ToList()
                            });
                        }

                        var menuItemCount = oDto.Count();
                        var items = oDto.AsQueryable().PageBy(input).ToList();

                        returnOutput.CategoryList = new PagedResultOutput<CategoryReportDto>(
                            menuItemCount,
                            items
                        );
                        returnOutput.Categories = categories;
                        returnOutput.ChartOutput = chartOutput;
                    }
                }
            }

            return returnOutput;
        }

        public async Task<CategoryStatsDto> GetCategorySales(GetItemInput input)
        {
            var returnOutput = new CategoryStatsDto();

            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                var returnOrders = GetAllOrders(input);

                var categoryGroupings = await returnOrders
                    .GroupBy(o => new { o.MenuItem.CategoryId, o.MenuItem.Category.Name }).ToListAsync();

                returnOutput.Categories = categoryGroupings.Select(c => c.Key.Name).ToList();

                returnOutput.ChartOutput = categoryGroupings
                    .Select(c => new BarChartOutputDto
                    {
                        name = c.Key.Name,
                        data = new List<decimal> { c.Sum(e => e.GetTotal()) }
                    }).ToList();

                var menuItemCount = categoryGroupings.Count();

                var items = categoryGroupings.Select(g => new CategoryReportDto
                {
                    Id = (int)g.Key.CategoryId,
                    CategoryId = (int)g.Key.CategoryId,
                    CategoryName = g.Key.Name,
                    Quantity = g.Sum(c => c.Quantity),
                    Total = g.Sum(e => e.GetTotal()),
                    MenuItems = g.GroupBy(c => c.MenuItem).Select(e => new MenuReportView
                    {
                        AliasCode = e.Key.AliasCode,
                        MenuItemName = e.Key.Name,
                        Quantity = e.Sum(a => a.Quantity),
                        Price = e.Sum(a => a.Quantity * a.Price)
                    }).ToList(),
                    LocationReports = g.GroupBy(c => new { c.Location_Id, c.Location.Name }).Select(e =>
                          new LocationReportDto
                          {
                              LocationId = e.Key.Location_Id,
                              LocationName = e.Key.Name,
                              Quantity = e.Sum(a => a.Quantity),
                              TotalAmount = e.Sum(a => a.GetTotal())
                          }).ToList()
                });

                var result = new List<CategoryReportDto>();
                result = input.IsExport
                    ? items.OrderBy(input.Sorting).ToList()
                    : items.AsQueryable().OrderBy(input.Sorting).PageBy(input).ToList();
                await FillDataForLoactions(input, result);
                returnOutput.CategoryList = new PagedResultOutput<CategoryReportDto>(
                    menuItemCount,
                    result
                );
                return returnOutput;
            }
        }

        [AbpAuthorize]
        public async Task<GroupStatsDto> ApiGetGroupSales(GetItemInput input)
        {
            var returnOutput = new GroupStatsDto();
            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                var groups = new List<string>();
                var chartOutput = new List<BarChartOutputDto>();
                var oDto = new List<GroupReportDto>();
                if (input.Stats)
                {
                    var returnOrders = GetAllOrders(input);
                    if (returnOrders.Any())
                    {
                        var myItems = returnOrders.GroupBy(a => a.MenuItemId);
                        foreach (var myItem in myItems)
                        {
                            var menuItem = _mrRepo.Get(myItem.Key);
                            var myGroup = oDto.LastOrDefault(a => a.GroupId == menuItem.Category.ProductGroupId);

                            if (myGroup == null)
                            {
                                myGroup = new GroupReportDto
                                {
                                    GroupId = menuItem.Category != null && menuItem.Category.ProductGroupId != null
                                        ? menuItem.Category.ProductGroupId.Value
                                        : 0,
                                    GroupName = menuItem.Category != null && menuItem.Category.ProductGroup != null
                                        ? menuItem.Category.ProductGroup.Name
                                        : "N/A",
                                    Quantity = myItem.Sum(a => a.Quantity),
                                    Total = myItem.Sum(a => a.Quantity * a.Price)
                                };
                                oDto.Add(myGroup);
                            }
                            else
                            {
                                myGroup.Quantity += myItem.Sum(a => a.Quantity);
                                myGroup.Total += myItem.Sum(a => a.Quantity * a.Price);
                            }

                            var myCategory =
                                myGroup.CategoryList.LastOrDefault(a => a.CategoryId == menuItem.Category.Id);

                            if (myCategory == null)
                            {
                                myCategory = new CategoryReportDto
                                {
                                    CategoryId = menuItem.Category != null ? menuItem.Category.Id : 0,
                                    CategoryName = menuItem.Category != null ? menuItem.Category.Name : "N/A",
                                    Quantity = myItem.Sum(a => a.Quantity),
                                    Total = myItem.Sum(a => a.Quantity * a.Price)
                                };
                                myGroup.CategoryList.Add(myCategory);
                            }
                            else
                            {
                                myCategory.Quantity += myItem.Sum(a => a.Quantity);
                                myCategory.Total += myItem.Sum(a => a.Quantity * a.Price);
                            }

                            myCategory.MenuItems.Add(new MenuReportView
                            {
                                AliasCode = menuItem.AliasCode,
                                MenuItemName = menuItem.Name,
                                Quantity = myItem.Sum(a => a.Quantity),
                                Price = myItem.Sum(a => a.Quantity * a.Price)
                            });
                        }
                    }

                    var menuItemCount = oDto.Count();
                    var items = oDto.AsQueryable().PageBy(input).ToList();
                    returnOutput.GroupList = new PagedResultOutput<GroupReportDto>(
                        menuItemCount,
                        items
                    );
                    returnOutput.Groups = groups;
                    returnOutput.ChartOutput = chartOutput;
                }
                else
                {
                    var returnSales = await GetItemSales(input);

                    if (returnSales.MenuList != null && returnSales.MenuList.Items.Any())
                    {
                        var outPut = from c in returnSales.MenuList.Items
                                     group c by c.GroupId
                            into g
                                     select new { GroupId = g.Key, Items = g };

                        foreach (var dto in outPut)
                            if (dto.GroupId != 0)
                            {
                                var group = await _productGroupRepo.GetAsync(dto.GroupId);
                                groups.Add(group.Name);
                                var quantity = dto.Items.Sum(a => a.Quantity);
                                var total = dto.Items.Sum(a => a.Quantity * a.Price);
                                chartOutput.Add(new BarChartOutputDto
                                {
                                    name = group.Name,
                                    data = new List<decimal> { total }
                                });
                                oDto.Add(new GroupReportDto
                                {
                                    GroupId = dto.GroupId,
                                    GroupName = group.Name,
                                    Quantity = quantity,
                                    Total = total,
                                    TotalItems = dto.Items.ToList()
                                });
                            }

                        var menuItemCount = oDto.Count();
                        var items = oDto.AsQueryable().PageBy(input).ToList();

                        returnOutput.GroupList = new PagedResultOutput<GroupReportDto>(
                            menuItemCount,
                            items
                        );
                        returnOutput.Groups = groups;
                        returnOutput.ChartOutput = chartOutput;
                    }
                }
            }

            return returnOutput;
        }

        public async Task<GroupStatsDto> GetGroupSales(GetItemInput input)
        {
            var returnOutput = new GroupStatsDto();
            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                var groups = new List<string>();
                var chartOutput = new List<BarChartOutputDto>();
                var oDto = new List<GroupReportDto>();
                var itemsales = (await GetItemSales(input)).MenuList.Items;

                if (itemsales.Any())
                {
                    var myItems = itemsales.GroupBy(a => a.MenuItemId);
                    foreach (var myItem in myItems)
                    {
                        var menuItem = _mrRepo.Get(myItem.Key);
                        var myCategory = oDto.LastOrDefault(a => a.GroupId == menuItem.Category.ProductGroupId);

                        if (myCategory == null)
                        {
                            myCategory = new GroupReportDto
                            {
                                GroupId = menuItem.Category.ProductGroupId ?? 0,
                                GroupName = menuItem.Category.ProductGroup.Name,
                                Quantity = myItem.Sum(a => a.Quantity),
                                Total = myItem.Sum(a => a.Total)
                            };
                            oDto.Add(myCategory);
                        }
                        else
                        {
                            myCategory.Quantity += myItem.Sum(a => a.Quantity);
                            myCategory.Total += myItem.Sum(a => a.Total);
                        }

                        var myReportView = new MenuReportView
                        {
                            AliasCode = menuItem.AliasCode,
                            MenuItemName = menuItem.Name,
                            Quantity = myItem.Sum(a => a.Quantity)
                        };

                        foreach (var myReport in myItem)
                        {
                            myReportView.Price += myReport.Total;
                        }

                        myCategory.MenuItems.Add(myReportView);


                    }
                }

                // filter by dynamic filter
                var dataAsQueryable = oDto.AsQueryable();
                if (!string.IsNullOrEmpty(input.DynamicFilter))
                {
                    var jsonSerializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver =
                            new CamelCasePropertyNamesContractResolver()
                    };
                    var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                    if (filRule?.Rules != null) dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                }

                oDto = dataAsQueryable.ToList();
                var menuItemCount = oDto.Count();
                var items = oDto.AsQueryable().PageBy(input).ToList();
                returnOutput.GroupList = new PagedResultOutput<GroupReportDto>(
                    menuItemCount,
                    items
                );

                groups = oDto.Select(x => x.GroupName).ToList();

                foreach (var item in oDto)
                    chartOutput.Add(new BarChartOutputDto
                    {
                        name = item.GroupName,
                        data = new List<decimal> { item.Total }
                    });

                returnOutput.Groups = groups;
                returnOutput.ChartOutput = chartOutput;
            }

            return returnOutput;
        }

        public async Task<FileDto> GetGroupExcel(GetItemInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.GROUPSALES,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });

                    if (backGroundId > 0)
                    {
                        input.LocationGroup.UserId = AbpSession.UserId.Value;

                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.GROUPSALES,
                            ItemInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                    }
                }
                else
                {
                    return await _exporter.ExportToFileGroup(input, this);
                }
            }

            return null;
        }

        public async Task<ItemTagStatsDto> GetItemTagSales(GetItemInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                var itemSales = await GetItemSalesNotFilterByDynamicFilter(input);

                var menuListDtos = itemSales.MenuList.Items
                    .SelectMany(
                        mi => mi.TagList.Select(t => new MenuListDto
                        {
                            MenuItemId = mi.MenuItemId,
                            AliasCode = mi.AliasCode,
                            MenuItemName = mi.MenuItemName,
                            CategoryId = mi.CategoryId,
                            DepartmentName = mi.DepartmentName,
                            GroupId = mi.GroupId,
                            MenuItemPortionId = mi.MenuItemPortionId,
                            MenuItemPortionName = mi.MenuItemPortionName,
                            Tag = t,
                            Price = mi.Price,
                            Quantity = mi.Quantity,
                            OrderTagPrice = mi.OrderTagPrice,
                            LocationName = mi.LocationName,
                            Hour = mi.Hour
                        })).Distinct()
                    .ToList();

                return GetItemByTag(input, menuListDtos);
            }
        }

        public async Task<DepartmentStatsDto> GetDepartmentSales(GetItemInput input)
        {
            var returnOutput = new DepartmentStatsDto();

            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                var tickets = GetAllTickets(input);
                var sortTickets = tickets.OrderBy(input.Sorting).GroupBy(a => a.DepartmentName);
                var reportDto = new List<DepartmentReportDto>();

                foreach (var dto in sortTickets)
                {
                    var depatId = 0;
                    var depts = _dRepo.FirstOrDefault(a => a.Name.Equals(dto.Key));
                    if (depts != null) depatId = depts.Id;
                    reportDto.Add(new DepartmentReportDto
                    {
                        Id = depatId,
                        DepartmentName = dto.Key,
                        TotalTicketCount = dto.Count(),
                        Total = dto.Sum(a => a.TotalAmount)
                    });
                }

                //filter by builder query
                var dataAsQueryable = reportDto.AsQueryable();
                if (!string.IsNullOrEmpty(input.DynamicFilter))
                {
                    var jsonSerializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver =
                            new CamelCasePropertyNamesContractResolver()
                    };
                    var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                    if (filRule?.Rules != null) dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                }

                var menuItemCount = dataAsQueryable.Count();
                returnOutput.DepartmentList = new PagedResultOutput<DepartmentReportDto>(
                    menuItemCount,
                    dataAsQueryable.ToList()
                );
            }

            return returnOutput;
        }

        [AbpAuthorize]
        public async Task<DepartmentStatsDto> ApiGetDepartmentSales(GetItemInput input)
        {
            var returnOutput = new DepartmentStatsDto();

            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                var tickets = GetAllTickets(input);
                var sortTickets = tickets.OrderBy(input.Sorting).GroupBy(a => a.DepartmentName);
                var reportDto = new List<DepartmentReportDto>();

                foreach (var dto in sortTickets)
                {
                    var depatId = 0;
                    var depts = _dRepo.FirstOrDefault(a => a.Name.Equals(dto.Key));
                    if (depts != null) depatId = depts.Id;
                    reportDto.Add(new DepartmentReportDto
                    {
                        Id = depatId,
                        DepartmentName = dto.Key,
                        TotalTicketCount = dto.Count(),
                        Total = dto.Sum(a => a.TotalAmount)
                    });
                    var menuItemCount = reportDto.Count();

                    returnOutput.DepartmentList = new PagedResultOutput<DepartmentReportDto>(
                        menuItemCount,
                        reportDto
                    );
                }
            }

            return returnOutput;
        }

        [AbpAuthorize]
        public async Task<ItemStatsDto> ApiGetItemSales(GetItemInput input)
        {
            var returnOutput = new ItemStatsDto();

            var menuILiDtos = new List<MenuListDto>();
            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                var returnOrders = GetAllOrders(input);
                if (returnOrders.Any())
                {
                    if (!input.Void && !input.Gift && !input.Comp)
                    {
                        var myItems = returnOrders.GroupBy(a => a.MenuItemName);
                        foreach (var myItem in myItems)
                        {
                            var menuIdt = new MenuListDto
                            {
                                MenuItemPortionId = myItem.First().MenuItemPortionId,
                                MenuItemPortionName = myItem.First().PortionName,
                                MenuItemId = myItem.First().MenuItemId,
                                MenuItemName = myItem.First().MenuItemName,
                                Price = myItem.Sum(a => a.Quantity * a.Price) / myItem.Sum(b => b.Quantity),
                                Quantity = myItem.Sum(a => a.Quantity),
                                Id = myItem.First().MenuItemPortionId
                            };
                            if (menuIdt.Total > 0)
                                menuILiDtos.Add(menuIdt);
                        }

                        if (input.NoSales) menuILiDtos = GetNoSales(menuILiDtos);

                        if (!string.IsNullOrEmpty(input.Sorting))
                            menuILiDtos = menuILiDtos.OrderBy(input.Sorting).ToList();

                        var menuItemCount = menuILiDtos.Count();
                        var items = menuILiDtos.AsQueryable().PageBy(input).ToList();

                        returnOutput.MenuList = new PagedResultOutput<MenuListDto>(
                            menuItemCount,
                            items
                        );
                    }
                    else
                    {
                        foreach (var myItem in returnOrders)
                        {
                            var menuIdt = new MenuListDto
                            {
                                MenuItemPortionId = myItem.MenuItemPortionId,
                                MenuItemPortionName = myItem.PortionName,
                                MenuItemId = myItem.MenuItemId,
                                MenuItemName = myItem.MenuItemName,
                                OrderNote = myItem.Note,
                                OrderTime = myItem.OrderCreatedTime,
                                OrderUser = myItem.CreatingUserName,
                                Price = myItem.Price,
                                Quantity = myItem.Quantity,
                                LocationName = myItem.Location.Name,
                                TicketNumber = myItem.Ticket.TicketNumber,
                                Id = myItem.Id
                            };
                            menuILiDtos.Add(menuIdt);
                        }

                        if (!string.IsNullOrEmpty(input.Sorting))
                            menuILiDtos = menuILiDtos.OrderBy(input.Sorting).ToList();

                        var menuItemCount = menuILiDtos.Count();
                        var items = menuILiDtos.AsQueryable().PageBy(input).ToList();

                        returnOutput.MenuList = new PagedResultOutput<MenuListDto>(
                            menuItemCount,
                            items
                        );
                    }
                }
            }

            return returnOutput;
        }

        public async Task<ItemStatsDto> GetItemSales(GetItemInput input)
        {
            var returnOutput = new ItemStatsDto();
            using (var unitOfWork = _unitOfWorkManager.Begin(new UnitOfWorkOptions()
            {
                Scope = System.Transactions.TransactionScopeOption.RequiresNew,
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted,
                IsTransactional = false
            }))
            {
                using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
                {
                    var orders = GetAllOrdersNotFilterByDynamicFilter(input);
                    var menuListOutputDots = GetMenuListDtoOutput(orders);
                    var dataAsQueryable = menuListOutputDots.AsQueryable();
                    if (!string.IsNullOrEmpty(input.DynamicFilter))
                    {
                        var jsonSerializerSettings = new JsonSerializerSettings
                        {
                            ContractResolver =
                                new CamelCasePropertyNamesContractResolver()
                        };
                        var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                        if (filRule?.Rules != null)
                        {
                            dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                        }
                    }

                    menuListOutputDots = dataAsQueryable.ToList();

                    if (input.Stats)
                    {
                        returnOutput.Quantities = GetItemByQuantity(dataAsQueryable);
                        returnOutput.Quantity = returnOutput.Quantities.Count() < 10 ? returnOutput.Quantities.Count : 10;

                        returnOutput.Totals = GetItemByTotal(dataAsQueryable);
                        returnOutput.Total = returnOutput.Totals.Count() < 10 ? returnOutput.Totals.Count : 10;
                    }

                    //if (input.NoSales) menuListOutputDots = GetNoSales(menuListOutputDots);

                    if (!input.Sorting.Equals("Id"))
                        menuListOutputDots = menuListOutputDots.OrderBy(input.Sorting).ToList();

                    var totalCount = menuListOutputDots.Count();

                    //if (input.IsSummary)
                    //    menuListOutputDots = menuListOutputDots.AsQueryable().PageBy(input).ToList();

                    returnOutput.MenuList = new PagedResultOutput<MenuListDto>(
                        totalCount,
                        menuListOutputDots
                    );
                }
            }

            return returnOutput;
        }
        private List<MenuListDto> GetMenuListDtoOutput(IQueryable<Order> orders)
        {
            var output = (from c in orders
                          group c by new { c.MenuItemPortionId, Location = c.Location_Id }
                       into g
                          select new { g.Key.Location, g.Key.MenuItemPortionId, Items = g });

            var outputOrderTemps = new List<OrderTemp>();
            foreach (var outputOrder in output)
            {
                var list2Dto = new List<OrderList2Dto>();
                foreach (var item in outputOrder.Items)
                {
                    var o = item.MapTo<OrderList2Dto>();
                    o.TicketNumber = item.Ticket.TicketNumber;
                    o.TaxIncluded = item.Ticket.TaxIncluded;
                    o.Tags = item.MenuItem?.Tag;
                    o.LocationName = item.Location?.Name;
                    o.LocationCode = item.Location?.Code;
                    o.CostPrice = item.MenuItemPortion?.CostPrice ?? 0;
                    list2Dto.Add(o);

                    if (item.TransactionOrderTags != null && item.TransactionOrderTags.Any())
                    {
                        foreach (var itemTransactionOrderTag in item.TransactionOrderTags)
                        {
                            if (itemTransactionOrderTag.MenuItemPortionId > 0)
                            {
                                var myDto = new OrderList2Dto()
                                {
                                    TicketNumber = item.Ticket.TicketNumber,
                                    TaxIncluded = item.Ticket.TaxIncluded,
                                    Tags = item.MenuItem?.Tag,
                                    LocationName = item.Location?.Name,
                                    LocationCode = item.Location?.Code,
                                    CostPrice = item.MenuItemPortion?.CostPrice ?? 0,
                                    MenuItemPortionId = itemTransactionOrderTag.MenuItemPortionId,
                                    Price = itemTransactionOrderTag.Price,
                                    Quantity = itemTransactionOrderTag.Quantity * item.Quantity,
                                };

                                AddToList(outputOrderTemps, myDto, outputOrder.Location);
                            }
                        }
                    }

                }
                outputOrderTemps.Add(new OrderTemp
                {
                    Location = outputOrder.Location,
                    MenuItemPortion = outputOrder.MenuItemPortionId,
                    Items = list2Dto
                });
            }

            var menuListOutputDots = new List<MenuListDto>();

            var dicCategory = new Dictionary<int, string>();
            var dicGroups = new Dictionary<int, string>();
            var aliasCodes = new Dictionary<int, string>();
            foreach (var dto in outputOrderTemps)
            {
                var firObj = dto.Items.First(); 
                var cateDto = GetCategoryDto(dicCategory, dicGroups, aliasCodes, dto.Items.First().MenuItemId);
                var menL = dto.Items.Aggregate(new MenuListDto
                {
                    LocationId = dto.Location,
                    LocationCode = firObj.LocationCode,
                    LocationName = firObj.LocationName,
                    AliasCode = cateDto.AliasCode,
                    MenuItemPortionId = dto.MenuItemPortion,
                    MenuItemPortionName = firObj.PortionName,
                    MenuItemId = firObj.MenuItemId,
                    MenuItemName = firObj.MenuItemName,
                    CategoryId = cateDto.CatId,
                    CategoryName = cateDto.CatgoryName,
                    CategoryCode = cateDto.CatgoryCode,
                    GroupId = cateDto.GroupId,
                    GroupName = cateDto.GroupName,
                    //Quantity = dto.Items.Sum(a => a.Quantity),
                    CostPrice = firObj.CostPrice,
                    Tags = firObj.Tags,
                    IncludedTax = dto.Items.Any(i => i.TaxIncluded),
                    ActualTax = dto.Items.Sum(o => o.GetOrderTaxTotal())
                }, (item, next) =>
                {
                    item.Price = (item.Quantity + next.Quantity) != 0 ? (item.Price * item.Quantity + next.GetProductPrice() * next.Quantity) / (item.Quantity + next.Quantity) : 0;
                    item.TaxPrice = (item.Quantity + next.Quantity) != 0 ? (item.TaxPrice * item.Quantity + next.GetOrderPriceNoTaxWithTags() * next.Quantity) / (item.Quantity + next.Quantity) : 0;
                    item.Quantity += next.Quantity;
                    item.OrderTagValue =  next.OrderTagValues.Sum(s=>s.Quantity * s.Price);
                    item.OrderTags = next.OrderTags;
                    return item;
                }, item => item);

                menuListOutputDots.Add(menL);
            }

            return menuListOutputDots;
        }

        private  void AddToList( List<OrderTemp> orderTemp, OrderList2Dto myDto, int location)
        {
            if (myDto.MenuItemPortionId > 0)
            {
                var myDtp = orderTemp.LastOrDefault(a =>
                    a.MenuItemPortion == myDto.MenuItemPortionId && a.Location == location);

                if (myDtp != null)
                {
                    myDtp.Items.Add(myDto);
                }
                else
                {
                    try
                    {
                        var mPortion =  _poRepository.Get(myDto.MenuItemPortionId);
                        if (mPortion != null && mPortion.MenuItem != null)
                        {
                            myDto.MenuItemId = mPortion.MenuItem.Id;
                            myDto.MenuItemName = mPortion.MenuItem.Name;

                        }
                    }
                    catch (Exception)
                    {
                        // ignored
                    }


                    orderTemp.Add(new OrderTemp
                    {
                        Items = new List<OrderList2Dto> { myDto },
                        Location = location,
                        MenuItemPortion = myDto.MenuItemPortionId
                    });
                }
            }
        }

        public class OrderTemp
        {
            public int MenuItemPortion { get; set; }

            public int Location { get; set; }

            public List<OrderList2Dto> Items { get; set; }
        }

        public async Task<ItemStatsDto> GetItemSalesForBackground(GetItemInput input)
        {
            var returnOutput = new ItemStatsDto();
            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                var orders = GetAllOrders(input);
                if (input.Stats)
                {
                    returnOutput.Quantities = GetItemByQuantity(orders);
                    returnOutput.Quantity = returnOutput.Quantities.Count() < 10 ? returnOutput.Quantities.Count : 10;

                    returnOutput.Totals = GetItemByTotal(orders);
                    returnOutput.Total = returnOutput.Totals.Count() < 10 ? returnOutput.Totals.Count : 10;
                }

                var output = from c in orders
                             group c by new { c.MenuItemPortionId, Location = c.Location_Id }
                    into g
                             select new { g.Key.Location, MenuItemPortion = g.Key.MenuItemPortionId, Items = g };

                //var rsMenuItems = await _mrRepo.GetAllListAsync();
                //var rsCategory = await _catRepository.GetAllListAsync();

                var outputDtos = new List<MenuListDto>();

                var dicLocations = new Dictionary<int, string>();
                var dicCategory = new Dictionary<int, string>();
                var dicGroups = new Dictionary<int, string>();

                var totRecordCount = output.Count();
                var loopCnt = 0;

                foreach (var dto in output)
                {
                    loopCnt++;
                    var firObj = dto.Items.First();

                    if (firObj != null)
                        try
                        {
                            var lname = "";
                            if (dicLocations.Any() && dicLocations.ContainsKey(dto.Location))
                            {
                                lname = dicLocations[dto.Location];
                            }
                            else
                            {
                                var lSer = _lRepo.Get(dto.Location);
                                if (lSer != null)
                                {
                                    lname = lSer.Name;
                                    dicLocations.Add(dto.Location, lname);
                                }
                            }

                            var catId = 0;
                            var catgoryName = "";

                            var groupId = 0;
                            var groupName = "";

                            var menuItem = firObj.MenuItemId;
                            if (dicCategory.Any() && dicCategory.ContainsKey(menuItem))
                            {
                                var catSplitname = dicCategory[menuItem];
                                var splitName = catSplitname.Split('@');
                                if (splitName != null && splitName.Any())
                                {
                                    catId = Convert.ToInt32(splitName[0]);
                                    catgoryName = splitName[1];
                                }
                            }
                            else
                            {
                                var lSer = _mrRepo.Get(menuItem);
                                if (lSer != null)
                                {
                                    catId = lSer.CategoryId ?? 0;
                                    catgoryName = lSer.Category.Name;
                                    dicCategory.Add(menuItem, catId + "@" + catgoryName);
                                }
                            }

                            if (dicGroups.Any() && dicGroups.ContainsKey(menuItem))
                            {
                                var catSplitname = dicGroups[menuItem];
                                var splitName = catSplitname.Split('@');
                                if (splitName != null && splitName.Any())
                                {
                                    groupId = Convert.ToInt32(splitName[0]);
                                    groupName = splitName[1];
                                }
                            }
                            else
                            {
                                var lSer = _mrRepo.Get(menuItem);
                                if (lSer != null)
                                {
                                    groupId = lSer.Category?.ProductGroupId ?? 0;
                                    groupName = lSer.Category?.ProductGroup?.Name;
                                    dicGroups.Add(menuItem, groupId + "@" + groupName);
                                }
                            }

                            var menL = new MenuListDto
                            {
                                LocationName = lname,
                                MenuItemPortionId = dto.MenuItemPortion,
                                MenuItemPortionName = firObj.PortionName,
                                MenuItemId = firObj.MenuItemId,
                                MenuItemName = firObj.MenuItemName,
                                CategoryId = catId,
                                CategoryName = catgoryName,
                                GroupId = groupId,
                                GroupName = groupName,
                                Quantity = dto.Items.Sum(a => a.Quantity),
                                Tags = firObj.MenuItem?.Tag
                            };
                            menL.Price = dto.Items.Sum(a => a.Quantity * a.Price) / dto.Items.Sum(b => b.Quantity);
                            var allTags = dto.Items.SelectMany(a => a.TransactionOrderTags)
                                .Where(b => !b.AddTagPriceToOrderPrice);
                            if (allTags.Any())
                                menL.OrderTagPrice =
                                    allTags.Sum(a => a.Price * (a.Order != null ? a.Order.Quantity : 1)) /
                                    dto.Items.Sum(b => b.Quantity);

                            var finalPrice = menL.Price + menL.OrderTagPrice;
                            menL.Price = finalPrice;
                            outputDtos.Add(menL);
                        }
                        catch (Exception exception)
                        {
                            var mess = exception.Message;
                        }
                }

                if (input.NoSales) outputDtos = GetNoSales(outputDtos);

                if (!input.Sorting.Equals("Id"))
                    outputDtos = outputDtos.OrderBy(input.Sorting).ToList();

                var totalCount = outputDtos.Count();

                if (input.IsSummary)
                    outputDtos = outputDtos.AsQueryable().PageBy(input).ToList();

                returnOutput.MenuList = new PagedResultOutput<MenuListDto>(
                    totalCount,
                    outputDtos
                );
            }

            return returnOutput;
        }

        public async Task<ItemStatsDto> GetItemHourlySales(GetItemInput input)
        {
            var returnOutput = new ItemStatsDto();
            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                var orders = GetAllOrders(input);

                if (input.Stats)
                {
                    returnOutput.Quantities = GetItemByQuantity(orders);
                    returnOutput.Quantity = returnOutput.Quantities.Count() < 10 ? returnOutput.Quantities.Count : 10;

                    returnOutput.Totals = GetItemByTotal(orders);
                    returnOutput.Total = returnOutput.Totals.Count() < 10 ? returnOutput.Totals.Count : 10;
                }

                var output = (from c in orders
                              group c by new
                              {
                                  c.Ticket.LastPaymentTime.Hour,
                                  c.MenuItemPortionId,
                                  Location = c.Location_Id,
                                  c.MenuItemId
                              }
                        into g
                              select new { g.Key.Location, MenuItemPortion = g.Key.MenuItemPortionId, Items = g })
                    .ToList();

                var outputDtos = new List<MenuListDto>();

                var dicLocations = new Dictionary<int, string>();
                var dicCategory = new Dictionary<int, string>();

                foreach (var dto in output)
                {
                    var firObj = dto.Items.First();

                    if (firObj != null)
                        try
                        {
                            var lname = "";
                            if (dicLocations.Any() && dicLocations.ContainsKey(dto.Location))
                            {
                                lname = dicLocations[dto.Location];
                            }
                            else
                            {
                                var lSer = _lRepo.Get(dto.Location);
                                if (lSer != null)
                                {
                                    lname = lSer.Name;
                                    dicLocations.Add(dto.Location, lname);
                                }
                            }

                            var catId = 0;
                            var catgoryName = "";

                            var menuItem = firObj.MenuItemId;
                            if (dicCategory.Any() && dicCategory.ContainsKey(menuItem))
                            {
                                var catSplitname = dicCategory[menuItem];
                                var splitName = catSplitname.Split('@');
                                if (splitName != null && splitName.Any())
                                {
                                    catId = Convert.ToInt32(splitName[0]);
                                    catgoryName = splitName[1];
                                }
                            }
                            else
                            {
                                var lSer = _mrRepo.Get(menuItem);
                                if (lSer != null)
                                {
                                    catId = lSer.CategoryId ?? 0;
                                    catgoryName = lSer.Category.Name;
                                    dicCategory.Add(menuItem, catId + "@" + catgoryName);
                                }
                            }

                            outputDtos.Add(new MenuListDto
                            {
                                LocationName = lname,
                                MenuItemPortionId = dto.MenuItemPortion,
                                MenuItemPortionName = firObj.PortionName,
                                MenuItemId = firObj.MenuItemId,
                                MenuItemName = firObj.MenuItemName,
                                CategoryId = catId,
                                CategoryName = catgoryName,
                                Quantity = dto.Items.Sum(a => a.Quantity),
                                Price = dto.Items.Sum(a => a.Quantity * a.Price) / dto.Items.Sum(b => b.Quantity),
                                Hour = dto.Items.Key.Hour
                            });
                        }
                        catch (Exception exception)
                        {
                            var mess = exception.Message;
                        }
                }

                if (input.NoSales) outputDtos = GetNoSales(outputDtos);

                if (!input.Sorting.Equals("Id"))
                    outputDtos = outputDtos.OrderBy(input.Sorting).ToList();

                // var outputDtosNew = outputDtos.GroupBy(c => c.Hour).ToList();

                var outputPageDtos = outputDtos
                    .Skip(input.SkipCount)
                    .Take(input.MaxResultCount)
                    .ToList();

                returnOutput.MenuList = new PagedResultOutput<MenuListDto>(
                    outputDtos.Count(),
                    outputPageDtos
                );
            }

            return returnOutput;
        }

        // TODO: Get order no filter dynamicFilter
        public IQueryable<Order> GetAllOrdersNotFilterByDynamicFilter(GetItemInput input)
        {
            if (AbpSession.UserId.HasValue && AbpSession.TenantId.HasValue)
            {
                input.UserId = AbpSession.UserId.Value;
                input.TenantId = AbpSession.TenantId.Value;
            }

            var tickets = GetAllTickets(input)
                .WhereIf(!string.IsNullOrEmpty(input.TerminalName), x => x.TerminalName.Contains(input.TerminalName));
            var orders = tickets.SelectMany(a => a.Orders)
                .WhereIf(input.FilterByDepartment.Any(), e => input.FilterByDepartment.Contains(e.DepartmentName))
                .WhereIf(input.FilterByGroup.Any(),
                    e => e.MenuItem.Category.ProductGroup != null &&
                         input.FilterByGroup.Contains(e.MenuItem.Category.ProductGroup.Id))
                .WhereIf(input.FilterByCategory.Any(),
                    e => e.MenuItem.Category != null && input.FilterByCategory.Contains(e.MenuItem.Category.Id))
                .WhereIf(!input.FilterByTag.IsNullOrEmpty(),
                    e => input.FilterByTag.Any(t => e.MenuItem.Tag.Contains(t)));

            if (input.Locations.Any())
            {
                var locationIds = input.Locations.Select(l => l.Id).ToList();
                orders = orders.Where(o => locationIds.Contains(o.Location_Id));
            }

            if (input.MenuItemIds != null && input.MenuItemIds.Any())
                orders = orders
                    .WhereIf(input.MenuItemIds.Any(), o => input.MenuItemIds.Contains(o.MenuItemId));
            if (input.MenuItemPortionIds != null && input.MenuItemPortionIds.Any())
                orders = orders
                    .WhereIf(input.MenuItemPortionIds.Any(),
                        o => input.MenuItemPortionIds.Contains(o.MenuItemPortionId));

            if (input.Void || input.Gift || input.Comp || input.Refund)
            {
                orders = orders.Where(x =>
                !x.CalculatePrice && !x.IncreaseInventory &&
                (!string.IsNullOrEmpty(x.OrderStates)
                && ((input.Void && x.OrderStates.Contains("Void") && !x.DecreaseInventory) ||
                (input.Gift && x.OrderStates.Contains("Gift") && x.DecreaseInventory) ||
                (input.Comp && x.OrderStates.Contains("Comp") && x.DecreaseInventory) ||
                (input.Refund && x.OrderStates.Contains("Refund") && !x.DecreaseInventory)
                )));
            }
            else if (!input.IncludeAll) // Exclude the void, gift, comp, refun order. TODO: Now 'Include all' always False
            {
                orders = orders.Where(x => x.CalculatePrice
                || x.IncreaseInventory
                || string.IsNullOrEmpty(x.OrderStates)
                || (!x.OrderStates.Contains("Void")
                    && !x.OrderStates.Contains("Gift")
                    && !x.OrderStates.Contains("Comp")
                    && !x.OrderStates.Contains("Refund")));

            }

            if (!string.IsNullOrEmpty(input.LastModifiedUserName))
                orders = orders.Where(a => a.CreatingUserName.Contains(input.LastModifiedUserName));
            if (input.FilterByTerminal.Any())
                orders = orders.Where(a => input.FilterByTerminal.Contains(a.Ticket.TerminalName));
            if (input.FilterByUser.Any())
                orders = orders.Where(a => input.FilterByUser.Contains(a.CreatingUserName));
            return orders;
        }


        public async Task<ItemStatsDto> GetItemHourlySalesForBackground(GetItemInput input)
        {
            var returnOutput = new ItemStatsDto();
            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                var orders = GetAllOrders(input);

                if (input.Stats)
                {
                    returnOutput.Quantities = GetItemByQuantity(orders);
                    returnOutput.Quantity = returnOutput.Quantities.Count() < 10 ? returnOutput.Quantities.Count : 10;

                    returnOutput.Totals = GetItemByTotal(orders);
                    returnOutput.Total = returnOutput.Totals.Count() < 10 ? returnOutput.Totals.Count : 10;
                }

                var output = (from c in orders
                              group c by new
                              {
                                  c.Ticket.LastPaymentTime.Hour,
                                  c.MenuItemPortionId,
                                  Location = c.Location_Id,
                                  c.MenuItemId
                              }
                        into g
                              select new { g.Key.Location, MenuItemPortion = g.Key.MenuItemPortionId, Items = g })
                    .ToList();

                var outputDtos = new List<MenuListDto>();

                var dicLocations = new Dictionary<int, string>();
                var dicCategory = new Dictionary<int, string>();

                foreach (var dto in output)
                {
                    var firObj = dto.Items.First();

                    if (firObj != null)
                        try
                        {
                            var lname = "";
                            if (dicLocations.Any() && dicLocations.ContainsKey(dto.Location))
                            {
                                lname = dicLocations[dto.Location];
                            }
                            else
                            {
                                var lSer = _lRepo.Get(dto.Location);
                                if (lSer != null)
                                {
                                    lname = lSer.Name;
                                    dicLocations.Add(dto.Location, lname);
                                }
                            }

                            var catId = 0;
                            var catgoryName = "";

                            var menuItem = firObj.MenuItemId;
                            if (dicCategory.Any() && dicCategory.ContainsKey(menuItem))
                            {
                                var catSplitname = dicCategory[menuItem];
                                var splitName = catSplitname.Split('@');
                                if (splitName != null && splitName.Any())
                                {
                                    catId = Convert.ToInt32(splitName[0]);
                                    catgoryName = splitName[1];
                                }
                            }
                            else
                            {
                                var lSer = _mrRepo.Get(menuItem);
                                if (lSer != null)
                                {
                                    catId = lSer.CategoryId ?? 0;
                                    catgoryName = lSer.Category.Name;
                                    dicCategory.Add(menuItem, catId + "@" + catgoryName);
                                }
                            }

                            outputDtos.Add(new MenuListDto
                            {
                                LocationName = lname,
                                MenuItemPortionId = dto.MenuItemPortion,
                                MenuItemPortionName = firObj.PortionName,
                                MenuItemId = firObj.MenuItemId,
                                MenuItemName = firObj.MenuItemName,
                                CategoryId = catId,
                                CategoryName = catgoryName,
                                Quantity = dto.Items.Sum(a => a.Quantity),
                                Price = dto.Items.Sum(a => a.Quantity * a.Price) / dto.Items.Sum(b => b.Quantity),
                                Hour = dto.Items.Key.Hour
                            });
                        }
                        catch (Exception exception)
                        {
                            var mess = exception.Message;
                        }
                }

                if (input.NoSales) outputDtos = GetNoSales(outputDtos);

                if (!input.Sorting.Equals("Id"))
                    outputDtos = outputDtos.OrderBy(input.Sorting).ToList();

                // var outputDtosNew = outputDtos.GroupBy(c => c.Hour).ToList();

                var outputPageDtos = outputDtos
                    .Skip(input.SkipCount)
                    .Take(input.MaxResultCount)
                    .ToList();

                returnOutput.MenuList = new PagedResultOutput<MenuListDto>(
                    outputDtos.Count(),
                    outputPageDtos
                );
            }

            return returnOutput;
        }

        public async Task<FileDto> GetItemHourlySalesExcel(GetItemInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.ITEMHOURS,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.ITEMHOURS,
                            ItemInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                }
                else
                {
                    return await _exporter.ExportToFileItemsHours(input, this);
                }
            }

            return null;
        }

        public async Task<FileDto> GetLocationSalesExcel(GetTicketInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.ITEMLOCATIONS,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.ITEMLOCATIONS,
                            TicketInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                }
                else
                {
                    return await _exporter.ExportToFileLocationSales(input, this);
                }
            }

            return null;
        }

        public async Task<FileDto> GetLocationSalesDetailExcel(GetTicketInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                var setting = await _tenantSettingsService.GetAllSettings();
                input.DatetimeFormat = setting.Connect.DateTimeFormat;
                input.DateFormat = setting.Connect.SimpleDateFormat;
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.ITEMLOCATIONSDETAIL,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.ITEMLOCATIONSDETAIL,
                            TicketInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                }
                else
                {
                    return await _exporter.ExportToFileLocationSalesDetail(input, this);
                }
            }

            return null;
        }

        public async Task<ItemStatsDto> GetItemSalesByPrice(GetItemInput input)
        {
            var returnOutput = new ItemStatsDto();
            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                var orders = GetAllOrdersNotFilterByDynamicFilter(input);
                var output = from c in orders
                             group c by new { c.MenuItemPortionId, c.Price }
                    into g
                             select new { MenuItemPortion = g.Key.MenuItemPortionId, g.Key.Price, Items = g };

                var outputDtos = new List<MenuListDto>();

                var dicLocations = new Dictionary<int, string>();
                var dicCategory = new Dictionary<int, string>();
                var dicGroups = new Dictionary<int, string>();

                foreach (var dto in output)
                {
                    var firObj = dto.Items.First();

                    if (firObj != null)
                        try
                        {
                            var catId = 0;
                            var catgoryName = "";
                            var groupName = "";
                            var groupId = 0;
                            var menuItem = firObj.MenuItemId;
                            if (dicCategory.Any() && dicCategory.ContainsKey(menuItem))
                            {
                                var catSplitname = dicCategory[menuItem];
                                var splitName = catSplitname.Split('@');
                                if (splitName != null && splitName.Any())
                                {
                                    catId = Convert.ToInt32(splitName[0]);
                                    catgoryName = splitName[1];
                                }
                            }
                            else
                            {
                                var lSer = _mrRepo.Get(menuItem);
                                if (lSer != null)
                                {
                                    catId = lSer.CategoryId ?? 0;
                                    catgoryName = lSer.Category.Name;
                                    dicCategory.Add(menuItem, catId + "@" + catgoryName);
                                }
                            }

                            if (dicGroups.Any() && dicGroups.ContainsKey(menuItem))
                            {
                                var catSplitname = dicGroups[menuItem];
                                var splitName = catSplitname.Split('@');
                                if (splitName != null && splitName.Any())
                                {
                                    groupId = Convert.ToInt32(splitName[0]);
                                    groupName = splitName[1];
                                }
                            }
                            else
                            {
                                var lSer = _mrRepo.Get(menuItem);
                                if (lSer != null)
                                {
                                    groupId = lSer.Category?.ProductGroupId ?? 0;
                                    groupName = lSer.Category?.ProductGroup?.Name;
                                    dicGroups.Add(menuItem, groupId + "@" + groupName);
                                }
                            }


                            outputDtos.Add(new MenuListDto
                            {
                                MenuItemPortionId = dto.MenuItemPortion,
                                MenuItemPortionName = firObj.PortionName,
                                MenuItemId = firObj.MenuItemId,
                                MenuItemName = firObj.MenuItemName,
                                CategoryId = catId,
                                CategoryName = catgoryName,
                                Quantity = dto.Items.Sum(a => a.Quantity),
                                Price = dto.Items.Sum(a => a.Quantity * a.Price) / dto.Items.Sum(b => b.Quantity),
                                GroupName = groupName
                            });
                        }
                        catch (Exception exception)
                        {
                            var mess = exception.Message;
                        }
                }

                // filter by dynamic 
                var dataAsQueryable = outputDtos.AsQueryable();
                if (!string.IsNullOrEmpty(input.DynamicFilter))
                {
                    var jsonSerializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver =
                            new CamelCasePropertyNamesContractResolver()
                    };
                    var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                    if (filRule?.Rules != null) dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                }

                outputDtos = dataAsQueryable.ToList();

                if (input.Stats)
                {
                    returnOutput.Quantities = GetItemByQuantity(dataAsQueryable);
                    returnOutput.Quantity = returnOutput.Quantities.Count() < 10 ? returnOutput.Quantities.Count : 10;

                    returnOutput.Totals = GetItemByTotal(dataAsQueryable);
                    returnOutput.Total = returnOutput.Totals.Count() < 10 ? returnOutput.Totals.Count : 10;
                }

                if (!input.Sorting.Equals("Id"))
                    outputDtos = outputDtos.OrderBy(input.Sorting).ToList();

                returnOutput.MenuList = new PagedResultOutput<MenuListDto>(
                    outputDtos.Count(),
                    outputDtos
                );
            }

            return returnOutput;
        }

        public async Task<List<ItemSale>> GetItemComboSales(GetItemInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                var orders = GetAllOrdersNotFilterByDynamicFilter(input);
                var comboOrders = orders.Where(a => a.MenuItemType == 1 && a.OrderRef != null);
                var comboOrdersWithItems = await orders
                    .Where(t => t.OrderRef != null && comboOrders.Select(o => o.OrderRef).Contains(t.OrderRef))
                    .ToListAsync();

                var result = comboOrders.ToList().GroupBy(o => new { o.MenuItemId, o.MenuItemName })
                    .Select(g => new ItemSale
                    {
                        Location = g.FirstOrDefault().Location.Name,
                        LocationCode = g.FirstOrDefault().Location.Code,
                        Name = g.Key.MenuItemName,
                        Quantity = g.Sum(o => o.Quantity),
                        Amount = g.Sum(o => o.Quantity * o.Price),
                        Price = g.FirstOrDefault().Price,
                        OrderItems = g.SelectMany(o => GetOrderItems(o, comboOrdersWithItems))
                            .GroupBy(t => new { t.Id, t.Name }).Select(g2 => new ItemSale
                            {
                                Location = g2.FirstOrDefault().Location,
                                LocationCode = g2.FirstOrDefault().LocationCode,
                                GroupName = g2.FirstOrDefault().GroupName,
                                Name = g2.Key.Name,
                                Id = g2.Key.Id,
                                Quantity = g2.Sum(i => i.Quantity),
                                Price = g2.FirstOrDefault().Price,
                                Amount = g2.Sum(i => i.Amount)
                            }).OrderBy(t => t.Name).ToList(),
                        OrderTags = g.SelectMany(o => GetOrderTags(o)).GroupBy(t => new { t.Id, t.Name }).Select(g2 =>
                              new ItemSale
                              {
                                  Location = g2.FirstOrDefault().Location,
                                  LocationCode = g2.FirstOrDefault().LocationCode,
                                  Name = g2.Key.Name,
                                  Id = g2.Key.Id,
                                  Quantity = g2.Sum(i => i.Quantity),
                                  Price = g2.FirstOrDefault().Price,
                                  Amount = g2.Sum(i => i.Amount)
                              }).OrderBy(t => t.Name).ToList()
                    }).OrderBy(t => t.Name)
                    .ToList();
                return result;
            }
        }

        public async Task<AllItemSalesDto> GetAllItemSales(GetItemInput input)
        {
            var allS = new AllItemSalesDto();
            var tickets = GetAllTickets(input);
            var orders = tickets.SelectMany(a => a.Orders);
            if (input.MenuItemIds != null && input.MenuItemIds.Any())
                orders = orders
                    .WhereIf(input.MenuItemIds.Any(), o => input.MenuItemIds.Contains(o.MenuItemId));
            if (input.MenuItemPortionIds != null && input.MenuItemPortionIds.Any())
                orders = orders
                    .WhereIf(input.MenuItemPortionIds.Any(),
                        o => input.MenuItemPortionIds.Contains(o.MenuItemPortionId));

            IQueryable<Order> allSales = null;
            if (input.Sales)
            {
                allSales = orders.Where(a => a.CalculatePrice && a.DecreaseInventory
                                                              && !DineConnectConsts.OrderStates
                                                                  .Contains(a.OrderStates));

                allS.Sales = await GetMLD(allSales);
            }

            if (input.Void)
            {
                allSales = orders.Where(a =>
                    !a.CalculatePrice && !a.IncreaseInventory && !a.DecreaseInventory &&
                    !string.IsNullOrEmpty(a.OrderStates) && a.OrderStates.Contains("Void"));
                allS.Void = await GetMLD(allSales);
            }

            if (input.Gift)
            {
                allSales =
                    orders.Where(
                        a =>
                            !a.CalculatePrice && !a.IncreaseInventory && a.DecreaseInventory &&
                            !string.IsNullOrEmpty(a.OrderStates) && a.OrderStates.Contains("Gift"));
                allS.Gift = await GetMLD(allSales);
            }

            if (input.Comp)
            {
                allSales =
                    orders.Where(
                        a =>
                            !a.CalculatePrice && !a.IncreaseInventory && a.DecreaseInventory &&
                            !string.IsNullOrEmpty(a.OrderStates) && a.OrderStates.Contains("Comp"));
                allS.Comp = await GetMLD(allSales);
            }

            return allS;
        }

        public async Task<ItemStatsDto> GetItemSalesSummary(GetItemInput input)
        {
            var orders = GetAllOrders(input);
            var returnOutput = new ItemStatsDto();

            returnOutput.Quantities = GetItemByQuantity(orders);
            returnOutput.Quantity = returnOutput.Quantities.Count() < 10 ? returnOutput.Quantities.Count : 10;

            returnOutput.Totals = GetItemByTotal(orders);
            returnOutput.Total = returnOutput.Totals.Count() < 10 ? returnOutput.Totals.Count : 10;

            var sortTickets = await orders.OrderBy(input.Sorting)
                .ToListAsync();

            var categoryListDtos = sortTickets.MapTo<List<OrderListDto>>();
            var output = from c in categoryListDtos
                         group c by new { c.MenuItemPortionId }
                into g
                         select new { MenuItemPortion = g.Key.MenuItemPortionId, Items = g };

            var outputDtos = new List<MenuListDto>();

            foreach (var dto in output)
            {
                var portion = await _poRepository.GetAsync(dto.MenuItemPortion);
                if (portion != null)
                    try
                    {
                        outputDtos.Add(new MenuListDto
                        {
                            LocationName = "All",
                            MenuItemPortionId = dto.MenuItemPortion,
                            MenuItemPortionName = portion.Name,
                            MenuItemId = portion.MenuItemId,
                            MenuItemName = portion.MenuItem.Name,
                            AliasCode = portion.MenuItem.AliasCode,
                            CategoryId = portion.MenuItem.CategoryId ?? 0,
                            CategoryName = portion.MenuItem.Category.Name ?? "",
                            Quantity = dto.Items.Sum(a => a.Quantity),
                            Price = dto.Items.Sum(a => a.Quantity * a.Price) / dto.Items.Sum(b => b.Quantity)
                        });
                    }
                    catch (Exception exception)
                    {
                        var mess = exception.Message;
                    }
            }

            if (input.NoSales) outputDtos = GetNoSales(outputDtos);

            var allPor = await _poRepository.GetAllListAsync();

            foreach (var por in allPor)
            {
                var p = outputDtos.SingleOrDefault(a => a.MenuItemPortionId == por.Id);
                if (p == null)
                    outputDtos.Add(new MenuListDto
                    {
                        LocationName = L("All"),
                        MenuItemPortionId = por.Id,
                        MenuItemPortionName = por.Name,
                        AliasCode = por.MenuItem.AliasCode,
                        MenuItemId = por.MenuItemId,
                        MenuItemName = por.MenuItem.Name,
                        CategoryId = por.MenuItem.CategoryId ?? 0,
                        CategoryName = por.MenuItem.Category.Name ?? "",
                        Quantity = 0,
                        Price = 0
                    });
            }

            if (outputDtos.Any())
                outputDtos = outputDtos.OrderBy(a => a.MenuItemName).ThenBy(a => a.MenuItemPortionName).ToList();
            returnOutput.MenuList = new PagedResultOutput<MenuListDto>(
                outputDtos.Count(),
                outputDtos
            );
            return returnOutput;
        }

        public async Task<PortionSalesOutput> GetPortionSales(GetItemInput input)
        {
            var rOutput = new PortionSalesOutput();
            var output = rOutput.Portions;

            foreach (var cat in _catRepository.GetAll().ToList().OrderBy(a => a.Id))
                output.AddRange(cat.MenuItems.OrderBy(a => a.Id).Select(mi => new PortionSales
                {
                    CategoryId = cat.Id,
                    Category = cat.Name,
                    AliasCode = mi.AliasCode,
                    AliasName = mi.AliasName,
                    MenuItemId = mi.Id,
                    MenuItemName = mi.Name
                }));
            var getAllItemSales = input;
            getAllItemSales.MaxResultCount = 0;

            var itemSales = await GetItemSales(getAllItemSales);
            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                foreach (var iSale in itemSales.MenuList.Items.GroupBy(a => new { a.MenuItemPortionId }))
                {
                    var mportion = _poRepository.Get(iSale.Key.MenuItemPortionId);
                    var menuId = mportion.MenuItemId;
                    var menuItem = mportion.MenuItem;

                    var cObje = output.FirstOrDefault(a => a.MenuItemId == menuId);
                    if (cObje == null)
                    {
                        cObje = new PortionSales
                        {
                            AliasCode = menuItem.AliasCode,
                            AliasName = menuItem.AliasName,
                            Category = menuItem.Category.Name,
                            MenuItemId = menuItem.Id,
                            MenuItemName = menuItem.Name
                        };
                        output.Add(cObje);
                    }

                    cObje.AddToPortion(mportion.Id, iSale.Sum(a => a.Quantity),
                       iSale.Sum(a => a.Quantity) == 0 ? 0 : iSale.Sum(a => a.Price * a.Quantity) / iSale.Sum(a => a.Quantity), mportion.Price, mportion.Name);
                    rOutput.PortionNames.Add(mportion.Name);
                }
            }

            rOutput.Portions = output.OrderBy(a => a.Category).ToList();
            rOutput.PortionNames = _poRepository.GetAll().ToList().Select(a => a.Name).Distinct().ToList();
            return rOutput;
        }

        public async Task<PrintOutIn40Cols> GetSaleSummaryPrint(GetTicketInput input)
        {
            var output = new PrintOutIn40Cols();
            var setting = await _tenantSettingsService.GetAllSettings();
            var dateTimeFormat = setting.Connect.DateTimeFormat;
            var dateFormat = setting.Connect.SimpleDateFormat;
            var fp = new TextFileWriter();
            var subPath = "~/Report/";
            var exists = Directory.Exists(HttpContext.Current.Server.MapPath(subPath));

            if (!exists)
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(subPath));

            var fileName =
                HttpContext.Current.Server.MapPath("~/Report/" + AbpSession.UserId + "_SalesSummary.txt");
            var PageLenth = 35;
            var com = await _companyRe.FirstOrDefaultAsync(a => a.Id > 0);
            var salesSummary = await GetSaleSummary(input);
            if (salesSummary?.Sale?.Items != null && salesSummary.Sale.Items.Any() && com != null)
            {
                var allItems = salesSummary.Sale.Items;
                fp.FileOpen(fileName, "O");
                fp.PrintTextLine(fp.PrintFormatCenter(com.Name.ToUpper(), PageLenth));
                fp.PrintTextLine(fp.PrintFormatCenter(L("SalesSummary").ToUpper(), PageLenth));
                fp.PrintTextLine("");
                fp.PrintSepLine('-', PageLenth);
                foreach (var dateVa in allItems.GroupBy(a => a.DateStr))
                {
                    fp.PrintText(fp.PrintFormatLeft(L("Date"), 18));
                    fp.PrintText(fp.PrintFormatRight(dateVa.First().Date.ToString(dateFormat), 18));
                    fp.PrintTextLine("");
                    fp.PrintSepLine('-', PageLenth);
                    foreach (var ssd in dateVa)
                    {
                        fp.PrintText(fp.PrintFormatLeft(ssd.Location, 18));
                        fp.PrintText(fp.PrintFormatRight(ssd.Total.ToString("###0.00"), 18));
                        fp.PrintTextLine("");
                    }

                    fp.PrintTextLine("");
                    fp.PrintSepLine('-', PageLenth);
                    fp.PrintText(fp.PrintFormatLeft(L("TicketCount").ToUpper(), 18));
                    fp.PrintText(fp.PrintFormatRight(dateVa.Sum(a => a.TotalTicketCount).ToString("###0.00"), 18));
                    fp.PrintTextLine("");

                    fp.PrintText(fp.PrintFormatLeft(L("Total").ToUpper(), 18));
                    fp.PrintText(fp.PrintFormatRight(dateVa.Sum(a => a.Total).ToString("###0.00"), 18));
                    fp.PrintTextLine("");
                    fp.PrintSepLine('-', PageLenth);
                }

                var retText = File.ReadAllText(fileName);
                retText = retText.Replace("\r\n", "<br/>");
                retText = retText.Replace(" ", "&nbsp;");

                output.BodyText = retText;
                output.BoldBodyText = "<STRONG>" + retText + "</STRONG>";
            }

            return output;
        }

        [AbpAuthorize]
        public async Task<SaleSummaryStatsDto> GetSaleSummary(GetTicketInput input)
        {
            var setting = await _tenantSettingsService.GetAllSettings();
            var dateTimeFormat = setting.Connect.DateTimeFormat;
            var dateFormat = setting.Connect.SimpleDateFormat;
            var returnOutput = new SaleSummaryStatsDto();
            var mySDt = new List<SaleSummaryDto>();
            var diffe = (input.EndDate - input.StartDate).Days;
            for (var i = 0; i <= diffe; i++)
            {
                input.EndDate = input.StartDate.Date;
                var tickets = GetAllTicketsForTicketInput(input);
                var output = tickets.GroupBy(t => t.LocationId);
                foreach (var dDto in output)
                {
                    var tiO = dDto.ToList();

                    var summaryDto = new SaleSummaryDto
                    {
                        Date = input.StartDate,
                        LocationId = dDto.Key,
                        Total = tiO.Sum(a => a.TotalAmount),
                        TotalTicketCount = tiO.Count(),
                        TotalCovers = 0
                    };
                    mySDt.Add(summaryDto);

                    if (input.ReportDisplay)
                        continue;

                    summaryDto.TotalCovers = GetNumberOfGuests(tiO);

                    summaryDto.TotalOrderCount = GetTotalOrderCount(tiO);

                    var tioP = tiO.SelectMany(a => a.Payments).GroupBy(a => a.PaymentTypeId);

                    var pTypes = new Dictionary<int, string>();
                    using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
                    {
                        foreach (var pName in tioP)
                        {
                            var paymentName = "";
                            if (pTypes.ContainsKey(pName.Key))
                            {
                                paymentName = pTypes[pName.Key];
                            }
                            else
                            {
                                var pType = _payRe.Get(pName.Key);
                                if (pType != null)
                                {
                                    if (input.PaymentName)
                                    {
                                        paymentName = pType.Name;
                                    }
                                    else
                                    {
                                        paymentName = pType.AccountCode;
                                        if (string.IsNullOrEmpty(paymentName))
                                            paymentName = pType.Name;
                                    }

                                    pTypes.Add(pName.Key, paymentName);
                                }
                            }

                            if (summaryDto.Payments.ContainsKey(paymentName))
                            {
                                summaryDto.Payments[paymentName] += pName.Sum(a => a.Amount);
                                summaryDto.PaymentAccess[paymentName] += pName.Sum(a => a.TenderedAmount - a.Amount);
                            }
                            else
                            {
                                summaryDto.Payments[paymentName] = pName.Sum(a => a.Amount);
                                summaryDto.PaymentAccess[paymentName] = pName.Sum(a => a.TenderedAmount - a.Amount);
                            }
                        }
                    }

                    var tioT = tiO.SelectMany(a => a.Transactions).GroupBy(a => a.TransactionTypeId);

                    var tTypes = new Dictionary<int, string>();
                    using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
                    {
                        foreach (var pName in tioT)
                        {
                            var tName = "";
                            if (tTypes.ContainsKey(pName.Key))
                            {
                                tName = tTypes[pName.Key];
                            }
                            else
                            {
                                var pType = _tRe.Get(pName.Key);
                                if (pType != null)
                                {
                                    tName = pType.Name;
                                    tTypes.Add(pName.Key, tName);
                                }
                            }

                            if (summaryDto.Transactions.ContainsKey(tName))
                                summaryDto.Transactions[tName] += pName.Sum(a => a.Amount);
                            else
                                summaryDto.Transactions[tName] = pName.Sum(a => a.Amount);
                        }
                    }

                    try
                    {
                        // Promotion Discount                    
                        var tioPromotion = tiO.SelectMany(t => GetPromotionDetailValues(t)).ToList();

                        if (tioPromotion.Any())
                        {
                            summaryDto.Promotions = tioPromotion
                                .GroupBy(t => new { t.PromotionId, t.PromotionName })
                                .Select(t => new { t.Key.PromotionName, Amount = t.Sum(p => p.PromotionAmount) })
                                .ToDictionary(t => t.PromotionName, t => t.Amount);
                        }

                        //
                    }
                    catch (Exception e)
                    {

                    }



                    var departmentDtos = new List<DepartmentReportDto>();
                    summaryDto.Departments = departmentDtos;

                    foreach (var depDto in tiO.GroupBy(a => a.DepartmentName))
                        departmentDtos.Add(new DepartmentReportDto
                        {
                            DepartmentName = depDto.Key,
                            Total = depDto.Sum(a => a.TotalAmount),
                            TotalTicketCount = depDto.Count()
                        });
                }

                input.StartDate = input.StartDate.Date.AddDays(1);
            }

            foreach (var mySd in mySDt)
                if (mySd.LocationId > 0)
                {
                    var loc = await _locService.GetLocationById(new EntityDto
                    {
                        Id = mySd.LocationId
                    });

                    if (loc != null)
                    {
                        mySd.Location = loc.Name;
                        mySd.LocationCode = loc.Code;
                    }
                }

            var menuItemCount = mySDt.Count();
            var dashDto = new DashboardSaleDto();

            dashDto.Dates.AddRange(mySDt.Select(key => key.Date.ToString(dateFormat)));
            dashDto.Totals.AddRange(mySDt.Select(key => key.Total));
            dashDto.TicketCounts.AddRange(mySDt.Select(key => key.TotalTicketCount));

            returnOutput.Dashboard = dashDto;
            returnOutput.Sale = new PagedResultOutput<SaleSummaryDto>(
                menuItemCount,
                mySDt.OrderBy(a => a.Date).ToList()
            );
            return returnOutput;
        }

        public async Task<TicketHourListDto> GetHourlySalesSummary(GetTicketInput input)
        {
            var tickets = GetAllTicketsForTicketInput(input);
            var returnOutput = new TicketHourListDto { HourList = new List<TicketHourDto>() };
            var startDate = input.StartDate.Date;
            var myHourList = new TicketHourDto
            {
                Date = startDate
            };
            BuildHour(myHourList, tickets);
            returnOutput.HourList.Add(myHourList);
            if (returnOutput.HourList.Any())
            {
                returnOutput.ChartTotal = returnOutput.HourList[0].Hours != null
                    ? returnOutput.HourList[0].Hours.Select(a => a.Total).ToList()
                    : null;
                returnOutput.ChartTicket = returnOutput.HourList[0].Hours != null
                    ? returnOutput.HourList[0].Hours.Select(a => a.Tickets).ToList()
                    : null;
            }

            return returnOutput;
        }

        public async Task<TicketHourListDto> GetHourlySalesByDate(GetTicketInput input)
        {
            var tickets = GetAllTicketsForTicketInput(input);
            var returnOutput = new TicketHourListDto { HourList = new List<TicketHourDto>() };
            var startDate = input.StartDate.Date;
            var endDate = input.EndDate.Date;
            while (true)
            {
                var myHourList = new TicketHourDto
                {
                    Date = startDate
                };
                var temDa = startDate.AddDays(1);
                var myTickets = tickets.Where(
                    a =>
                        a.TicketCreatedTime >=
                        startDate
                        &&
                        a.TicketCreatedTime < temDa);
                returnOutput.HourList.Add(myHourList);
                BuildHour(myHourList, myTickets);
                startDate = temDa;
                if (startDate >= endDate) break;
            }

            return returnOutput;
        }

        public async Task<FileDto> GetHourlySalesExport(GetTicketInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.HOURLYSALES,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.HOURLYSALES,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value,
                            TicketInput = input
                        });
                }
                else
                {
                    if (input.Duration.Equals("S")) return await _hourlyExporter.ExportHourlySummary(input, this);

                    if (input.Duration.Equals("D")) return await _hourlyExporter.ExportHourlySummaryByDate(input, this);
                }
            }

            return null;
        }

        [AbpAuthorize]
        public async Task<HourDto> GetHourlySales(GetTicketInput input)
        {
            var tickets = GetAllTicketsForTicketInput(input);
            var hourDto = new HourDto();
            BuildHour(hourDto, tickets);
            return hourDto;
        }

        [AbpAuthorize]
        public async Task<IdHourListDto> GetLocationHourySales(GetTicketInput input)
        {
            var returnOutput = new IdHourListDto { HourList = new List<IdHourDto>() };
            var tickets = GetAllTicketsForTicketInput(input);
            var lc = _locService.GetLocationForUserAndGroup(new LocationGroupDto
            {
                Locations = input.LocationGroup.Locations,
                Group = false
            });

            foreach (var location in lc)
            {
                var localTickets = tickets.Where(a => a.LocationId == location);
                if (localTickets.Any())
                {
                    var myHourList = new IdHourDto
                    {
                        Id = location
                    };
                    returnOutput.HourList.Add(myHourList);
                    BuildHour(myHourList, localTickets);
                }
            }

            foreach (var tlh in returnOutput.HourList)
            {
                var l = lc.FirstOrDefault(a => a == tlh.Id);
                if (l > 0)
                    try
                    {
                        tlh.Name = _lRepo.Get(l).Name;
                    }
                    catch (Exception)
                    {
                        // ignored
                    }
            }

            return returnOutput;
        }

        [AbpAuthorize]
        public async Task<IdHourListDto> GetDayOfWeekHourlySales(GetTicketInput input)
        {
            var returnOutput = new IdHourListDto();
            returnOutput.HourList = new List<IdHourDto>();
            var tickets = GetAllTicketsForTicketInput(input);

            var data = tickets.GroupBy(o => SqlFunctions.DatePart("weekday", o.TicketCreatedTime))
                .Select(g => new { DayOfWeek = g.Key, Tickets = g });

            foreach (var allTi in data)
            {
                var myHourList = new IdHourDto
                {
                    Id = allTi.DayOfWeek.Value
                };
                returnOutput.HourList.Add(myHourList);

                if (allTi.Tickets.Any()) BuildHour(myHourList, allTi.Tickets);
            }

            foreach (var tlh in returnOutput.HourList)
            {
                var foo = (DayOfWeek)tlh.Id;
                tlh.Name = foo.ToString();
            }

            return returnOutput;
        }

        [AbpAuthorize]
        public async Task<List<IdLocationHourListDto>> GetLocationDayOfWeekHourlySales(GetTicketInput input)
        {
            var returnOutput = new List<IdLocationHourListDto>();
            var tickets = GetAllTicketsForTicketInput(input);

            var data = tickets.GroupBy(
                    o => new { DatePart = SqlFunctions.DatePart("weekday", o.TicketCreatedTime), o.LocationId })
                .Select(g => new { DayOfWeek = g.Key.DatePart, g.Key.LocationId, Tickets = g });

            foreach (var allTi in data)
            {
                var oo = new IdLocationHourListDto { Id = allTi.LocationId };

                var myHourList = new IdHourDto
                {
                    Id = allTi.DayOfWeek.Value
                };
                oo.HourList.Add(myHourList);

                if (allTi.Tickets.Any()) BuildHour(myHourList, allTi.Tickets);
                returnOutput.Add(oo);
                foreach (var tlh in oo.HourList)
                {
                    var foo = (DayOfWeek)tlh.Id;
                    tlh.Name = foo.ToString();
                }
            }

            var lc = await _lRepo.GetAllListAsync();

            foreach (var rO in returnOutput) rO.Name = lc.First(a => a.Id == rO.Id).Name;

            return returnOutput;
        }

        [AbpAuthorize]
        public async Task<IdHourListDto> GetDayOfMonthHourlySales(GetTicketInput input)
        {
            var returnOutput = new IdHourListDto();
            returnOutput.HourList = new List<IdHourDto>();
            var tickets = GetAllTicketsForTicketInput(input);

            var data = tickets.GroupBy(o => SqlFunctions.DatePart("day", o.TicketCreatedTime))
                .Select(g => new { Day = g.Key, Tickets = g });

            foreach (var allTi in data)
            {
                var myHourList = new IdHourDto
                {
                    Id = allTi.Day.Value
                };
                returnOutput.HourList.Add(myHourList);

                if (allTi.Tickets.Any()) BuildHour(myHourList, allTi.Tickets);
            }

            return returnOutput;
        }

        public async Task<List<IdLocationHourListDto>> GetLocationDayOfMonthHourlySales(GetTicketInput input)
        {
            var returnOutput = new List<IdLocationHourListDto>();
            var tickets = GetAllTicketsForTicketInput(input);

            var data = tickets.GroupBy(
                    o => new { Day = SqlFunctions.DatePart("day", o.TicketCreatedTime), Location = o.LocationId })
                .Select(g => new { g.Key.Day, g.Key.Location, Tickets = g });

            foreach (var allTi in data)
            {
                var oo = new IdLocationHourListDto();
                oo.Id = allTi.Location;

                var myHourList = new IdHourDto
                {
                    Id = allTi.Day.Value
                };
                oo.HourList.Add(myHourList);

                if (allTi.Tickets.Any()) BuildHour(myHourList, allTi.Tickets);
                returnOutput.Add(oo);
            }

            var lc = await _lRepo.GetAllListAsync();

            foreach (var rO in returnOutput) rO.Name = lc.First(a => a.Id == rO.Id).Name;

            return returnOutput;
        }

        public async Task<IdHourListDto> GetHourlySalesByRange(GetTicketInput input)
        {
            var returnOutput = new IdHourListDto { HourList = new List<IdHourDto>() };
            var tickets = GetAllTicketsForTicketInput(input);

            var data = tickets.GroupBy(o => o.LastPaymentTimeTruc)
                .Select(g => new { Day = g.Key, Tickets = g });

            foreach (var allTi in data)
            {
                var myHourList = new IdHourDto
                {
                    Name = allTi.Day.ToString()
                };
                returnOutput.HourList.Add(myHourList);

                if (allTi.Tickets.Any()) BuildHour(myHourList, allTi.Tickets);
            }

            return returnOutput;
        }

        public async Task<List<IdLocationHourListDto>> GetLocationHourlySalesByRange(GetTicketInput input)
        {
            var returnOutput = new List<IdLocationHourListDto>();
            var tickets = GetAllTicketsForTicketInput(input);

            var data = tickets.GroupBy(
                    o => new { o.LastPaymentTimeTruc, o.LocationId })
                .Select(g => new { g.Key.LastPaymentTimeTruc, g.Key.LocationId, Tickets = g });

            foreach (var allTi in data)
            {
                var oo = new IdLocationHourListDto { Id = allTi.LocationId };
                var myHourList = new IdHourDto
                {
                    Name = allTi.LastPaymentTimeTruc.ToString()
                };
                oo.HourList.Add(myHourList);

                if (allTi.Tickets.Any()) BuildHour(myHourList, allTi.Tickets);
                returnOutput.Add(oo);
            }

            foreach (var rO in returnOutput) rO.Name = _lRepo.Get(rO.Id).Name;

            return returnOutput;
        }

        public async Task<IdDayItemOutput> GetDaysItemSales(GetDayItemInput input)
        {
            var returnOutput = new IdDayItemOutput();
            var setting = await _tenantSettingsService.GetAllSettings();
            var dateTimeFormat = setting.Connect.DateTimeFormat;
            var dateFormat = setting.Connect.SimpleDateFormat;
            var retrDates = new List<DateTime>();
            var output = new List<IdDayItemOutputDto>();
            returnOutput.Items = output;

            if (input.AllDates != null && input.AllDates.Any())
            {
                retrDates.AddRange(input.AllDates);
            }
            else if (input.StartDate > DateTime.MinValue)
            {
                var sDate = input.StartDate;
                var eDate = input.EndDate;
                while (true)
                {
                    retrDates.Add(sDate);
                    sDate = sDate.AddDays(1);
                    if (sDate > eDate) break;
                }
            }

            if (retrDates.Any())
                foreach (var retrDate in retrDates)
                {
                    var getitemInput = new GetItemInput
                    {
                        StartDate = retrDate,
                        EndDate = retrDate,
                        Comp = input.Comp,
                        Void = input.Void,
                        Gift = input.Gift,
                        Locations = input.Locations,
                        Location = input.Location,
                        Sorting = "Id",
                        Stats = input.Stats,
                        TenantId = input.TenantId,
                        UserId = input.UserId,
                        FilterByCategory = input.FilterByCategory,
                        FilterByDepartment = input.FilterByDepartment,
                        FilterByGroup = input.FilterByGroup,
                        LocationGroup = input.LocationGroup,
                        FilterByTag = input.FilterByTag,
                        MenuItemIds = input.MenuItemIds,
                        MenuItemPortionIds = input.MenuItemPortionIds,
                        DynamicFilter = input.DynamicFilter
                    };
                    var allSales = await GetItemSales(getitemInput);

                    ProcessSales(output, input, allSales, retrDate.ToString(dateFormat));
                }

            foreach (var retrDate in retrDates) returnOutput.AllDates.Add(retrDate.ToString(dateFormat));

            if (input.ByLocation && input.LocationsByUser != null)
                input.LocationsByUser.ForEach(e => { returnOutput.AllLocations.Add(e.Code, e.Name); });
            else
                foreach (var loc in _lRepo.GetAllList())
                    returnOutput.AllLocations.Add(loc.Code, loc.Name);

            foreach (var allI in returnOutput.Items)
                foreach (var datee in returnOutput.AllDates)
                {
                    var aa = allI.Items.SingleOrDefault(a => a.Date.Equals(datee));
                    if (aa == null)
                    {
                        aa = new DayWiseItemDto
                        {
                            Date = datee,
                            Price = 0M,
                            Quantity = 0M
                        };
                        allI.Items.Add(aa);
                    }
                }

            if (returnOutput.Items.Any())
                returnOutput.Items = returnOutput.Items
                    .WhereIf(input.MenuItemIds.Any(), a => input.MenuItemIds.Contains(a.MenuItemId))
                    .OrderBy(a => a.MenuItemId).ToList();
            return returnOutput;
        }

        public async Task<TicketTransactionOutput> GetTicketTransactions(GetTicketInput input)
        {
            var output = new TicketTransactionOutput();
            var allTickets = GetAllTicketsForTicketInput(input);

            var allTra = allTickets.SelectMany(a => a.Transactions);
            var sortTickets = await allTra.OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();
            output.Transactions = sortTickets.MapTo<List<TicketTransactionListDto>>();
            return output;
        }

        public async Task<List<DepartmentSummaryDto>> GetDepartmentSalesSummary(GetTicketInput input)
        {
            var summary = new List<DepartmentSummaryDto>();
            var tickets = GetAllTicketsForTicketInput(input);

            if (tickets.Any())
            {
                var deptS = tickets.GroupBy(a => new { a.Location.Name, a.DepartmentName });
                foreach (var dept in deptS)
                {
                    var su = new DepartmentSummaryDto
                    {
                        DepartmentName = dept.Key.DepartmentName,
                        Location = dept.Key.Name,
                        AllPayments = dept.SelectMany(a => a.Payments).GroupBy(a => a.PaymentType.Name),
                        AllTransactions = dept.SelectMany(a => a.Transactions).GroupBy(a => a.TransactionType.Name)
                    };
                    summary.Add(su);
                }

                foreach (var sum in summary)
                {
                    foreach (var payment in sum.AllPayments) sum.Payments[payment.Key] = payment.Sum(a => a.Amount);
                    foreach (var trns in sum.AllTransactions) sum.Transactions[trns.Key] = trns.Sum(a => a.Amount);
                }
            }

            return summary;
        }

        public async Task<FileDto> GetSummaryOutput(GetTicketInput input)
        {
            input.OutputType = "EXPORT";
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var ttNames = await _ttService.GetAllItems();
                var ttList = ttNames.Items.Select(a => a.Name).ToList();
                var ptNames = await _ptService.GetAllItems();
                var ptList = ptNames.Items.Select(a => a.Name).ToList();
                var dtNames = await _dRepo.GetAllListAsync();
                var dtList = dtNames.Select(a => a.Name).ToList();
                var proList = _promoRepo.GetAll().Select(t => t.Name).ToList();
                input.Portrait = false;
                return await _exporter.ExportToSalesSummary(input, this, ttList, ptList, dtList, proList,
                    (int)AbpSession.TenantId);
            }
        }

        public async Task<List<DepartmentTallySummaryDto>> GetDeparmentSalesSummaryForTally(GetTicketInput input)
        {
            var summary = new List<DepartmentTallySummaryDto>();
            var tickets = GetAllTicketsForTicketInput(input);

            if (tickets.Any())
            {
                var deptS = tickets.GroupBy(a => new { a.Location.Code, a.DepartmentName });
                foreach (var dept in deptS)
                {
                    var su = new DepartmentTallySummaryDto
                    {
                        DepartmentName = dept.Key.DepartmentName,
                        Location = dept.Key.Code,
                        AllPayments = dept.SelectMany(a => a.Payments).GroupBy(a => a.PaymentTypeId),
                        AllTransactions = dept.SelectMany(a => a.Transactions).GroupBy(a => a.TransactionType.Id)
                    };
                    summary.Add(su);
                }

                foreach (var sum in summary)
                {
                    foreach (var payment in sum.AllPayments) sum.Payments[payment.Key] = payment.Sum(a => a.Amount);
                    foreach (var trns in sum.AllTransactions) sum.Transactions[trns.Key] = trns.Sum(a => a.Amount);
                }
            }

            return summary;
        }

        public async Task<FileDto> GetDepartmentTallyExcel(GetTicketInput input)
        {
            if (input.Locations == null)
            {
                var allLocations = await _locService.GetSimpleLocations();
                input.Locations = allLocations.Items.ToList();
            }

            var setting = await _tenantSettingsService.GetAllSettings();
            input.DatetimeFormat = setting.Connect.DateTimeFormat;
            input.DateFormat = setting.Connect.SimpleDateFormat;
            return await _exporter.ExportToTallyForDepartment(input, this);
        }

        public async Task<FileDto> GetOrdersExcel(GetItemInput input)
        {
            input.OutputType = "EXPORT";
            input.Portrait = false;
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                var setting = await _tenantSettingsService.GetAllSettings();
                input.DatetimeFormat = setting.Connect.DateTimeFormat;
                input.DateFormat = setting.Connect.SimpleDateFormat;

                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.ORDERS,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.ORDERS,
                            ItemInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                }
                else
                {
                    return await _exporter.ExportOrders(input, this);
                }
            }

            return null;
        }

        public async Task<FileDto> GetExchangeReportExcel(GetItemInput input)
        {
            input.OutputType = "EXPORT";
            input.Portrait = false;
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                var setting = await _tenantSettingsService.GetAllSettings();
                input.DatetimeFormat = setting.Connect.DateTimeFormat;
                input.DateFormat = setting.Connect.SimpleDateFormat;

                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.EXCHANGEREPORT,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.EXCHANGEREPORT,
                            ItemInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                }
                else
                {
                    return await _exporter.ExportExchangeReport(input, this);
                }
            }

            return null;
        }

        public async Task<FileDto> GetExternalLogExcel(GetItemInput input)
        {
            input.OutputType = "EXPORT";
            input.Portrait = false;
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.EXTERNALLOG,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.EXTERNALLOG,
                            ItemInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                }
                else
                {
                    return await _auditLogListExcelExporter.ExportExternalLogToFile(input, this);
                }
            }

            return null;
        }

        public async Task<FileDto> GetItemComboSalesExcel(GetItemInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.ITEMCOMBOSALES,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.ITEMCOMBOSALES,
                            ItemInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                }
                else
                {
                    return await _exporter.ExportToFileItemCombos(input, this);
                }
            }

            return null;
        }

        public async Task<FileDto> GetItemSalesByPriceExcel(GetItemInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.ITEMSALESBYPRICE,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.ITEMSALESBYPRICE,
                            ItemInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                }
                else
                {
                    return await _exporter.ExportToFileItemsByPrice(input, this);
                }
            }

            return null;
        }

        public async Task<FileDto> GetItemsExcel(GetItemInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                input.UserId = AbpSession.UserId.Value;
                input.TenantId = AbpSession.TenantId.Value;

                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.ITEMSALES,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.ITEMSALES,
                            ItemInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                }
                else
                {
                    return await _exporter.ExportToFileItems(input, this);
                }
            }

            return null;
        }

        public async Task<FileDto> GetItemSalesByDepartmentExcel(GetItemInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.DEPARTMENTSALES,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                    {
                        var itemInput = new GetItemInput();
                        itemInput = input;
                        itemInput.TenantId = AbpSession.TenantId.Value;
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.DEPARTMENTSALES,
                            ItemInput = itemInput,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                    }
                }
                else
                {
                    return await _exporter.ExportItemSalesForDepartmentExcel(input, this);
                }
            }

            return null;
        }

        public async Task<FileDto> GetItemsSummaryExcel(GetItemInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.ITEMSALESBYPORTION,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.ITEMSALESBYPORTION,
                            ItemInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                }
                else
                {
                    return await _exporter.ExportToFileItemsSummary(input, this);
                }
            }

            return null;
        }

        public async Task<FileDto> GetItemsAndOrderTagExcel(GetItemInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                input.IsItemSalesReport = true;
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.ITEMANDORDERTAGSALES,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.ITEMANDORDERTAGSALES,
                            ItemInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                }
                else
                {
                    return await _exporter.ExportToFileItemAndOrderTagSales(input, this);
                }
            }

            return null;
        }

        public async Task<FileDto> GetSummaryExcel(GetTicketInput input)
        {
            var proList = _promoRepo.GetAll().Select(t => t.Name).ToList();
            input.ExportType = "EXPORT";
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var ttNames = await _ttService.GetAllItems();
                var ttList = ttNames.Items.Select(a => a.Name).ToList();
                var ptNames = await _ptService.GetAllItems();
                var ptList = ptNames.Items.Select(a => a.Name).ToList();
                var dtNames = await _dRepo.GetAllListAsync();
                var dtList = dtNames.Select(a => a.Name).ToList();

                var setting = await _tenantSettingsService.GetAllSettings();
                input.DatetimeFormat = setting.Connect.DateTimeFormat;
                input.DateFormat = setting.Connect.SimpleDateFormat;
                if (AbpSession.UserId != null && AbpSession.TenantId != null)
                {
                    if (input.RunInBackground)
                    {
                        var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                        {
                            ReportName = ReportNames.SALESUMMARY,
                            Completed = false,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value,
                            ReportDescription = input.ReportDescription
                        });
                        if (backGroundId > 0)
                            await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                            {
                                BackGroundId = backGroundId,
                                ReportName = ReportNames.SALESUMMARY,
                                TicketInput = input,
                                TTList = ttList,
                                PTList = ptList,
                                DTList = dtList,
                                ProList = proList,
                                UserId = AbpSession.UserId.Value,
                                TenantId = AbpSession.TenantId.Value
                            });
                    }
                    else
                    {
                        return await _exporter.ExportToSalesSummary(input, this, ttList, ptList, dtList, proList,
                            (int)AbpSession.TenantId);
                    }
                }

                return null;
            }
        }
        public async Task<FileDto> GetSummaryNewExcel(GetTicketInput input)
        {
            var ttNames = await _ttService.GetAllItems();
            var ttList = ttNames.Items.Select(a => a.Name).ToList();
            var ptNames = await _ptService.GetAllItems();
            var ptList = ptNames.Items.Select(a => a.Name).ToList();
            var dtNames = await _dRepo.GetAllListAsync();
            var dtList = dtNames.Select(a => a.Name).ToList();
            var proList = _promoRepo.GetAll().Select(t => t.Name).ToList();
            input.ExportType = "EXPORT";
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var setting = await _tenantSettingsService.GetAllSettings();
                input.DatetimeFormat = setting.Connect.DateTimeFormat;
                input.DateFormat = setting.Connect.SimpleDateFormat;
                if (AbpSession.UserId != null && AbpSession.TenantId != null)
                {
                    if (input.RunInBackground)
                    {
                        var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                        {
                            ReportName = ReportNames.SALESUMMARYNEW,
                            Completed = false,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value,
                            ReportDescription = input.ReportDescription
                        });
                        if (backGroundId > 0)
                            await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                            {
                                BackGroundId = backGroundId,
                                ReportName = ReportNames.SALESUMMARYNEW,
                                TicketInput = input,
                                TTList = ttList,
                                PTList = ptList,
                                DTList = dtList,
                                ProList = proList,
                                UserId = AbpSession.UserId.Value,
                                TenantId = AbpSession.TenantId.Value
                            });
                    }
                    else
                    {
                        return await _exporter.ExportToSalesSummaryNew(input, this, ttList, ptList, dtList, proList,
                            (int)AbpSession.TenantId);
                    }
                }

                return null;
            }
        }
        public async Task<FileDto> GetSummarySecondExcel(GetTicketInput input)
        {
            var ttNames = await _ttService.GetAllItems();
            var ttList = ttNames.Items.Select(a => a.Name).ToList();
            var ptNames = await _ptService.GetAllItems();
            var ptList = ptNames.Items.Select(a => a.Name).ToList();
            var dtNames = await _dRepo.GetAllListAsync();
            var dtList = dtNames.Select(a => a.Name).ToList();
            var proList = _promoRepo.GetAll().Select(t => t.Name).ToList();
            input.ExportType = "EXPORT";
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var setting = await _tenantSettingsService.GetAllSettings();
                input.DatetimeFormat = setting.Connect.DateTimeFormat;
                input.DateFormat = setting.Connect.SimpleDateFormat;
                if (AbpSession.UserId != null && AbpSession.TenantId != null)
                {
                    if (input.RunInBackground)
                    {
                        var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                        {
                            ReportName = ReportNames.SALESUMMARYSECOND,
                            Completed = false,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value,
                            ReportDescription = input.ReportDescription
                        });
                        if (backGroundId > 0)
                            await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                            {
                                BackGroundId = backGroundId,
                                ReportName = ReportNames.SALESUMMARYSECOND,
                                TicketInput = input,
                                TTList = ttList,
                                PTList = ptList,
                                DTList = dtList,
                                ProList = proList,
                                UserId = AbpSession.UserId.Value,
                                TenantId = AbpSession.TenantId.Value
                            });
                    }
                    else
                    {
                        return await _exporter.ExportToSalesSecondSummary(input, this, ptList, (int)AbpSession.TenantId);
                    }
                }

                return null;
            }
        }

        public async Task<List<WorkPeriodItemSales>> GetItemSalesForWorkPeriod(GetItemInput input)
        {
            var output = new List<WorkPeriodItemSales>();

            if (input.WorkPeriodId > 0)
            {
                var wp = _workPeriod.Get(input.WorkPeriodId);

                var allTickets =
                    _ticketManager.GetAll()
                        .Where(
                            a =>
                                a.WorkPeriodId == wp.Wid && a.LocationId == wp.LocationId &&
                                a.CreationTime >= wp.StartTime);
                var allorders = allTickets.SelectMany(a => a.Orders);
                var orders =
                    allorders.Where(a => a.CalculatePrice && a.DecreaseInventory).GroupBy(a => a.MenuItemPortionId);

                foreach (var order in orders)
                {
                    var itemS = new WorkPeriodItemSales
                    {
                        MenuItemPortionId = order.Key,
                        Quantity = order.Sum(a => a.Quantity),
                        Price = order.Sum(a => a.Quantity * a.Price) / order.Sum(b => b.Quantity)
                    };
                    output.Add(itemS);
                }

                foreach (var item in output)
                    using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                    {
                        var mP = _poRepository.Get(item.MenuItemPortionId);
                        item.PortionName = mP.Name;
                        item.MenuItemName = mP.MenuItem.Name;
                        item.Category = mP.MenuItem.Category.Name;
                    }
            }

            return output;
        }

        public async Task<FileDto> GetItemSalesForWorkPeriodExcel(GetItemInput input)
        {
            var myLocation = await _locService.GetLocationById(new EntityDto(input.Location));
            var fileOutput = await _exporter.ExportItemSalesForWorkPeriodExcel(input, this, myLocation.Code);
            return fileOutput;
        }

        public async Task<IList<WorkPeriodListDto>> GetWorkPeriodByLocationForDates(WorkPeriodSummaryInput input)
        {
            var allW = _workPeriod.GetAll();

            var locations = new List<int>();
            if (input.Locations != null && input.Locations.Any())
                locations = input.Locations.Select(a => a.Id).ToList();
            if (locations.Any())
            {
                allW = allW.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any() &&
                     !input.LocationGroup.Locations.Any())
            {
                locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                allW = allW.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                allW = allW.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true
                });
                allW = allW.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup != null)
            {
                if (input.LocationGroup.NonLocations != null && input.LocationGroup.NonLocations.Any())
                {
                    var nonlocations = input.LocationGroup.NonLocations.Select(a => a.Id).ToList();
                    allW = allW.Where(a => !nonlocations.Contains(a.LocationId));
                }
            }
            else if (input.LocationGroup == null)
            {
                locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = new List<SimpleLocationDto>(),
                    Group = false,
                    UserId = input.UserId
                });
                if (input.UserId > 0)
                    allW = allW.Where(a => locations.Contains(a.LocationId));
            }

            var nextDay = input.EndDate.AddDays(1);
            var result = await allW.Where(a =>
                DbFunctions.TruncateTime(a.StartTime) >= DbFunctions.TruncateTime(input.StartDate)
                && DbFunctions.TruncateTime(a.EndTime) < DbFunctions.TruncateTime(nextDay)
            ).ToListAsync();

            return result.MapTo<List<WorkPeriodListDto>>();
        }

        public async Task<FileDto> GetDepartmentExcel(GetTicketInput input)
        {
            input.OutputType = "EXPORT";
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var ttNames = await _ttService.GetAllItems();
                var ttList = ttNames.Items.Select(a => a.Name).ToList();
                var ptNames = await _ptService.GetAllItems();
                var ptList = ptNames.Items.Select(a => a.Name).ToList();
                if (AbpSession.UserId != null && AbpSession.TenantId != null)
                {
                    if (input.RunInBackground)
                    {
                        var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                        {
                            ReportName = ReportNames.DEPARTMENTSUMMARY,
                            Completed = false,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value,
                            ReportDescription = input.ReportDescription
                        });
                        if (backGroundId > 0)
                            await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                            {
                                BackGroundId = backGroundId,
                                ReportName = ReportNames.DEPARTMENTSUMMARY,
                                TicketInput = input,
                                TTList = ttList,
                                PTList = ptList,
                                UserId = AbpSession.UserId.Value,
                                TenantId = AbpSession.TenantId.Value
                            });
                    }
                    else
                    {
                        return await _exporter.ExportToDepartmentSummary(input, this, ttList, ptList);
                    }
                }

                return null;
            }
        }

        public async Task<ItemStatsDto> GetItemSalesByDepartment(GetItemInput input)
        {
            var orders = GetAllOrdersNotFilterByDynamicFilter(input);

            var returnOutput = new ItemStatsDto();

            returnOutput.Quantities = GetItemByQuantity(orders);
            returnOutput.Quantity = returnOutput.Quantities.Count() < 10 ? returnOutput.Quantities.Count : 10;

            returnOutput.Totals = GetItemByTotal(orders);
            returnOutput.Total = returnOutput.Totals.Count() < 10 ? returnOutput.Totals.Count : 10;

            // var sortTickets = await orders.OrderBy(input.Sorting)
            //    .ToListAsync();

            var sortTickets = await orders.ToListAsync();
            var categoryListDtos = sortTickets.MapTo<List<OrderListDto>>();
            var output = from c in categoryListDtos
                         group c by new { c.MenuItemPortionId, Location = c.LocationName, Department = c.DepartmentName }
                into g
                         select new { g.Key.Location, MenuItemPortion = g.Key.MenuItemPortionId, g.Key.Department, Items = g };

            var outputDtos = new List<MenuListDto>();

            foreach (var dto in output)
            {
                var portion = new MenuItemPortion();

                if (input.TenantId.HasValue)
                    using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.MustHaveTenant,
                        AppConsts.ConnectFilter))
                    {
                        var tenantId = input.TenantId.Value;

                        portion = await _poRepository
                            .GetAll()
                            .Include(menu => menu.MenuItem)
                            .Include(category => category.MenuItem.Category)
                            .Where(i => i.Id == dto.MenuItemPortion
                                        && i.MenuItem.TenantId == tenantId
                                        && i.MenuItem.Category.TenantId == tenantId)
                            .FirstOrDefaultAsync();
                    }
                else
                    portion = await _poRepository.GetAsync(dto.MenuItemPortion);

                if (portion != null)
                    try
                    {
                        outputDtos.Add(new MenuListDto
                        {
                            DepartmentName = dto.Department,
                            LocationName = dto.Location,
                            MenuItemPortionId = dto.MenuItemPortion,
                            MenuItemPortionName = portion.Name,
                            MenuItemId = portion.MenuItemId,
                            MenuItemName = portion.MenuItem.Name,
                            CategoryId = portion.MenuItem.CategoryId ?? 0,
                            CategoryName = portion.MenuItem.Category.Name ?? "",
                            Quantity = dto.Items.Sum(a => a.Quantity),
                            Price = dto.Items.Sum(a => a.Quantity * a.Price) / dto.Items.Sum(b => b.Quantity)
                        });
                    }
                    catch (Exception exception)
                    {
                        var mess = exception.Message;
                    }
            }

            if (input.NoSales) outputDtos = GetNoSales(outputDtos);

            // add filter by query builder
            var dataAsQueryable = outputDtos.AsQueryable();
            if (!string.IsNullOrEmpty(input.DynamicFilter))
            {
                var jsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver =
                        new CamelCasePropertyNamesContractResolver()
                };
                var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                if (filRule?.Rules != null) dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
            }

            outputDtos = dataAsQueryable.ToList();

            outputDtos = input.Sorting == "Id"
                ? outputDtos.OrderBy("CategoryId").ToList()
                : outputDtos.OrderBy(input.Sorting).ToList();
            var outputPageDtos = outputDtos
                .Skip(input.SkipCount)
                .Take(input.MaxResultCount)
                .ToList();

            returnOutput.MenuList = new PagedResultOutput<MenuListDto>(
                outputDtos.Count(),
                outputPageDtos
            );
            returnOutput.TotalQuantity = 0;
            returnOutput.TotalResult = 0;
            foreach (var item in outputDtos)
            {
                returnOutput.TotalQuantity += item.Quantity;
                returnOutput.TotalResult += item.Total;
            }

            returnOutput.TotalQuantity = returnOutput.TotalQuantity;
            returnOutput.TotalResult = returnOutput.TotalResult;

            return returnOutput;
        }

        public async Task<ItemStatsDto> GetTopOrBottomItemSales(GetItemInput input)
        {
            var returnOutput = new ItemStatsDto();
            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                var orders = GetAllOrdersNotFilterByDynamicFilter(input);
                if (input.Stats)
                {
                    returnOutput.Quantities = GetItemByQuantity(orders);
                    returnOutput.Quantity = returnOutput.Quantities.Count() < 10 ? returnOutput.Quantities.Count : 10;

                    returnOutput.Totals = GetItemByTotal(orders);
                    returnOutput.Total = returnOutput.Totals.Count() < 10 ? returnOutput.Totals.Count : 10;
                }

                var output = from c in orders
                             group c by new { c.MenuItemPortionId, Location = c.Location_Id }
                    into g
                             select new { g.Key.Location, MenuItemPortion = g.Key.MenuItemPortionId, Items = g };

                //var rsMenuItems = await _mrRepo.GetAllListAsync();
                //var rsCategory = await _catRepository.GetAllListAsync();

                var outputDtos = new List<MenuListDto>();

                var dicLocations = new Dictionary<int, string>();
                var dicCategory = new Dictionary<int, string>();

                var totRecordCount = output.Count();
                var loopCnt = 0;

                foreach (var dto in output)
                {
                    loopCnt++;
                    var firObj = dto.Items.First();

                    if (firObj != null)
                        try
                        {
                            var lname = "";
                            if (dicLocations.Any() && dicLocations.ContainsKey(dto.Location))
                            {
                                lname = dicLocations[dto.Location];
                            }
                            else
                            {
                                var lSer = _lRepo.Get(dto.Location);
                                if (lSer != null)
                                {
                                    lname = lSer.Name;
                                    dicLocations.Add(dto.Location, lname);
                                }
                            }

                            var catId = 0;
                            var catgoryName = "";

                            var menuItem = firObj.MenuItemId;
                            if (dicCategory.Any() && dicCategory.ContainsKey(menuItem))
                            {
                                var catSplitname = dicCategory[menuItem];
                                var splitName = catSplitname.Split('@');
                                if (splitName != null && splitName.Any())
                                {
                                    catId = Convert.ToInt32(splitName[0]);
                                    catgoryName = splitName[1];
                                }
                            }
                            else
                            {
                                var lSer = _mrRepo.Get(menuItem);
                                if (lSer != null)
                                {
                                    catId = lSer.CategoryId ?? 0;
                                    catgoryName = lSer.Category.Name;
                                    dicCategory.Add(menuItem, catId + "@" + catgoryName);
                                }
                            }

                            var menL = new MenuListDto
                            {
                                LocationName = lname,
                                MenuItemPortionId = dto.MenuItemPortion,
                                MenuItemPortionName = firObj.PortionName,
                                MenuItemId = firObj.MenuItemId,
                                MenuItemName = firObj.MenuItemName,
                                CategoryId = catId,
                                CategoryName = catgoryName,
                                Quantity = dto.Items.Sum(a => a.Quantity)
                            };
                            menL.Price = dto.Items.Sum(a => a.Quantity * a.Price) / dto.Items.Sum(b => b.Quantity);
                            var allTags = dto.Items.SelectMany(a => a.TransactionOrderTags)
                                .Where(b => !b.AddTagPriceToOrderPrice);
                            if (allTags.Any())
                                menL.OrderTagPrice =
                                    allTags.Sum(a => a.Price * (a.Order != null ? a.Order.Quantity : 1)) /
                                    dto.Items.Sum(b => b.Quantity);

                            var finalPrice = menL.Price + menL.OrderTagPrice;
                            menL.Price = finalPrice;
                            outputDtos.Add(menL);
                        }
                        catch (Exception exception)
                        {
                            var mess = exception.Message;
                        }
                }

                if (input.NoSales) outputDtos = GetNoSales(outputDtos);

                // filter by DynamicFilter

                var dataAsQueryable = outputDtos.AsQueryable();
                if (!string.IsNullOrEmpty(input.DynamicFilter))
                {
                    var jsonSerializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver =
                            new CamelCasePropertyNamesContractResolver()
                    };
                    var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                    if (filRule?.Rules != null) dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                }

                outputDtos = dataAsQueryable.OrderBy(input.TakeString).Take(input.TakeNumber).ToList();

                if (!input.Sorting.Equals("Id"))
                    outputDtos = outputDtos.OrderBy(input.Sorting).ToList();

                var outputPage = outputDtos.ToList();
                if (!input.IsExport)
                    outputPage = outputDtos.AsQueryable().PageBy(input).ToList();

                returnOutput.MenuList = new PagedResultOutput<MenuListDto>(
                    outputDtos.Count(),
                    outputPage
                );
            }

            return returnOutput;
        }

        public async Task<PaymentStatsDto> GetPayments(GetPaymentInput input)
        {
            var returnOutput = new PaymentStatsDto();
            var output = await GetDashboardChart(new GetChartInput
            {
                StartDate = input.StartDate,
                EndDate = input.EndDate,
                Location = input.Location,
                Locations = input.Locations
            });
            returnOutput.DashBoardDto = output.DashBoardDto;
            var payments = GetAllTickets(input).SelectMany(s => s.Payments)
                .GroupBy(ss => new { Id = ss.PaymentTypeId, Payment = ss.PaymentType.Name })
                .Select(g => new GetPaymentOutput
                {
                    Name = g.Key.Payment,
                    Id = g.Key.Id,
                    Amount = g.Sum(a => a.Amount)
                });
            var categoryListDtos = payments.MapTo<List<GetPaymentOutput>>();
            var menuItemCount = payments.Count();

            returnOutput.Payments = new PagedResultOutput<GetPaymentOutput>(
                menuItemCount,
                categoryListDtos
            );

            return returnOutput;
        }

        public async Task<TransactionStatsDto> GetTransactions(GetTransactionInput input)
        {
            var tickets = GetAllTickets(input);
            var returnOutput = new TransactionStatsDto();

            var locations = new List<int>();
            if (input.Locations != null && input.Locations.Any())
                locations = input.Locations.Select(a => a.Id).ToList();

            var userId = AbpSession.UserId;
            if (userId.HasValue && locations.Count == 0)
            {
                var allOrgani = await _locService.GetLocationBasedOnUser(new GetLocationInputBasedOnUser
                {
                    UserId = userId.Value
                });

                if (allOrgani.Items.Any()) locations = allOrgani.Items.Select(a => a.Id).ToList();
            }

            if (locations.Any()) tickets = tickets.Where(a => locations.Contains(a.LocationId));
            returnOutput.DashBoardDto = GetStatics(tickets);
            var trasactions = tickets.SelectMany(s => s.Transactions)
                .GroupBy(ss => new { Id = ss.TransactionTypeId, Payment = ss.TransactionType.Name })
                .Select(g => new GetTransactionOutput
                {
                    Name = g.Key.Payment,
                    Id = g.Key.Id,
                    Amount = g.Sum(a => a.Amount)
                }).OrderBy(a => a.Name);

            var categoryListDtos = trasactions.MapTo<List<GetTransactionOutput>>();
            var menuItemCount = trasactions.Count();

            returnOutput.Transactions = new PagedResultOutput<GetTransactionOutput>(
                menuItemCount,
                categoryListDtos
            );

            return returnOutput;
        }

        [AbpAuthorize]
        public async Task<TicketStatsDto> GetDashboardChart(GetChartInput input)
        {
            if (input.Location > 0)
            {
                var loc = _lRepo.Get(input.Location);
                if (loc != null)
                {
                    var myLd = loc.MapTo<SimpleLocationDto>();
                    input.Locations = new List<SimpleLocationDto> { myLd };
                }
            }

            return await GetTickets(new GetTicketInput
            {
                StartDate = input.StartDate,
                EndDate = input.EndDate,
                Locations = input.Locations
            }, true);
        }

        public async Task<PagedResultOutput<TicketViewTax>> GetLocationSales(GetTicketInput input)
        {
            var vouTypeIds =
                _payRe.GetAll().Where(a => a.PaymentTag.Equals("Voucher")).Select(a => a.Id).ToList();

            var allTicketViewTaxs = new List<TicketViewTax>();

            var dto = GetAllTickets(input).Where(a => a.TotalAmount > 0M)
                .GroupBy(c => new { c.LastPaymentTimeTruc, c.TerminalName });

            foreach (var c in dto)
            {
                var taxE = new TicketViewTax
                {
                    Date = c.Key.LastPaymentTimeTruc,
                    RegNo = c.Key.TerminalName,
                    TaxInvoiceNo = c.Min(i => i.InvoiceNo) + "-" + c.Max(i => i.InvoiceNo),
                    SaleAmt = c.Sum(i => i.TotalAmount),
                    SaleNonVat = 0,
                    Service = 0,
                    Tip = 0,
                    Vat = c.Sum(i => i.TotalAmount) * 7 / 107
                };

                allTicketViewTaxs.Add(taxE);

                var allPromo = c.Select(a => a.TicketPromotionDetails);
                foreach (var myPromo in allPromo)
                    if (!string.IsNullOrEmpty(myPromo))
                    {
                        var allPromJs = JsonConvert.DeserializeObject<List<PromotionDetailValue>>(myPromo);
                        var totalAmount = allPromJs.Sum(a => a.PromotionAmount);
                        taxE.Discount += totalAmount;
                    }

                var allPayments = c.SelectMany(a => a.Payments);
                var allVouPayments = allPayments.Where(a => vouTypeIds.Contains(a.PaymentTypeId));
                var onlyDiffer = allVouPayments.Where(a => a.TenderedAmount - a.Amount > 0);
                taxE.VoucherExcess = onlyDiffer.Sum(a => a.TenderedAmount - a.Amount);
            }

            var termDictionary = new Dictionary<string, string>();

            foreach (var item in allTicketViewTaxs)
            {
                var lastTerminal = "";
                if (!string.IsNullOrEmpty(item.RegNo))
                {
                    lastTerminal = item.RegNo;
                    if (termDictionary.ContainsKey(item.RegNo))
                    {
                        lastTerminal = termDictionary[item.RegNo];
                    }
                    else
                    {
                        var lastTerm = _terminalRepo.GetAll().FirstOrDefault(a => a.Name.Equals(item.RegNo));
                        if (lastTerm != null)
                            if (!string.IsNullOrEmpty(lastTerm.TerminalCode))
                            {
                                lastTerminal = lastTerm.TerminalCode;
                                termDictionary.Add(item.RegNo, lastTerm.TerminalCode);
                            }
                    }
                }

                if (!string.IsNullOrEmpty(lastTerminal)) item.RegNo = lastTerminal;
            }

            var countTotal = allTicketViewTaxs.Count();
            if (input.Sorting == "Id") input.Sorting = "Date";
            var finalResult = allTicketViewTaxs
                .OrderBy(input.Sorting)
                .Skip(input.SkipCount)
                .Take(input.MaxResultCount)
                .ToList();

            return new PagedResultOutput<TicketViewTax>(countTotal, finalResult);
        }

        public async Task<PagedResultOutput<TicketViewTaxDetail>> GetLocationSalesDetail(GetTicketInput input)
        {
            var query = GetAllTickets(input).Where(a => a.TotalAmount > 0M);
            var result = new List<TicketViewTaxDetail>();

            var termDictionary = new Dictionary<string, string>();

            foreach (var item in query)
            {
                var lastTerminal = "";

                if (!string.IsNullOrEmpty(item.TerminalName))
                {
                    lastTerminal = item.TerminalName;

                    if (termDictionary.ContainsKey(item.TerminalName))
                    {
                        lastTerminal = termDictionary[item.TerminalName];
                    }
                    else
                    {
                        var lastTerm = _terminalRepo.GetAll().FirstOrDefault(a => a.Name.Equals(item.TerminalName));
                        if (lastTerm != null)
                            if (!string.IsNullOrEmpty(lastTerm.TerminalCode))
                            {
                                lastTerminal = lastTerm.TerminalCode;
                                termDictionary.Add(item.TerminalName, lastTerm.TerminalCode);
                            }
                    }
                }

                var ticketView = new TicketViewTaxDetail
                {
                    Date = item.LastPaymentTime,
                    RegNo = lastTerminal,
                    TaxInvoiceNo = item.InvoiceNo,
                    Employee = item.LastModifiedUserName,
                    Detail = "Food & Beverage",
                    SalesAmount = item.TotalAmount,
                    Service = item.Transactions.Where(a => a.TransactionTypeId == 3).Sum(a => a.Amount),
                    Vat = item.GetIncludedAmount(),
                    Payment = ""
                };

                if (item.Payments != null && item.Payments.Count > 0)
                {
                    var paymentName = item.Payments.Select(c => c.PaymentType.Name).Distinct().ToList();
                    foreach (var name in paymentName)
                        ticketView.Payment = name + " " + ticketView.Payment;
                }

                result.Add(ticketView);
            }

            var countTotal = result.Count;

            if (input.Sorting == "Id") input.Sorting = "Date";

            var resultPage = result.AsQueryable()
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToList();
            return new PagedResultOutput<TicketViewTaxDetail>(countTotal, resultPage);
        }

        public async Task<PagedResultOutput<TenderDetail>> GetTenderDetails(GetTicketInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var tenderViewDtos = await GetTenderDetailsForBackground(input);

                var result = tenderViewDtos.SelectMany(c => c.TenderDetails).ToList();

                var countTotal = result.Count;
                if (input.Sorting == "Id") input.Sorting = "TenderCode";
                var resultPage = result.AsQueryable()
                    .OrderBy(input.Sorting)
                    // .PageBy(input)
                    .ToList();

                return new PagedResultOutput<TenderDetail>(countTotal, resultPage);
            }
        }

        public async Task<List<TenderViewDto>> GetTenderDetailsForBackground(GetTicketInput input)
        {
            var allReturnList = new List<TenderViewDto>();
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var currentTickets = GetAllTickets(input)
                    .Where(a => a.TotalAmount > 0M);

                var allPayments = currentTickets.SelectMany(x => x.Payments).GroupBy(o => o.PaymentTypeId)
                    .WhereIf(input.Payments.Any(), p => input.Payments.Contains(p.Key));

                foreach (var pay in allPayments)
                {
                    var allTicke = currentTickets
                        .Where(t => t.Payments.Any(x => x.PaymentTypeId == pay.Key));

                    var tenderView = new TenderViewDto
                    {
                        PaymentType = pay.FirstOrDefault()?.PaymentType
                    };
                    allReturnList.Add(tenderView);
                    foreach (var c in allTicke)
                    {
                        var payments = c.Payments.Where(a => a.PaymentTypeId == pay.Key);

                        if (payments != null)
                        {
                            var paymentDic = new Dictionary<string, List<Payment>>();
                            foreach (var payment in payments)
                            {
                                if (!string.IsNullOrEmpty(payment.PaymentTags))
                                {
                                    try
                                    {
                                        var conFrame =
                                            JsonConvert.DeserializeObject<PaymentFrame>(payment.PaymentTags);
                                        if (conFrame != null)
                                        {
                                            if (!paymentDic.ContainsKey(conFrame.Description))
                                            {
                                                paymentDic.Add(conFrame.Description, new List<Payment>());
                                            }
                                            paymentDic[conFrame.Description].Add(payment);
                                        }
                                        else if (!paymentDic.ContainsKey(""))
                                        {
                                            paymentDic.Add("", new List<Payment>() { payment });
                                        }
                                        else
                                        {
                                            paymentDic[""].Add(payment);
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        //
                                    }
                                }
                                else
                                {
                                    if (!paymentDic.ContainsKey(""))
                                    {
                                        paymentDic.Add("", new List<Payment>() { payment });
                                    }
                                    else
                                    {
                                        paymentDic[""].Add(payment);
                                    }
                                }

                            }
                            foreach (var payment in paymentDic)
                            {
                                var detail = new TenderDetail
                                {
                                    DateTime = c.LastPaymentTime,
                                    TicketNo = c.TicketNumber,
                                    Terminal = c.TerminalName,
                                    InvoiceNo = c.InvoiceNo,
                                    Table = "",
                                    Employee = payment.Value.Last().PaymentUserName,
                                    CardNumber = "",
                                    ReferenceNo = "",
                                    Discount = 0M,
                                    ServiceCharge = 0,
                                    Vat = payment.Value.Sum(a => a.Amount) * 7 / 107,
                                    Total = payment.Value.Sum(a => a.Amount),
                                    TenderCode = tenderView.PaymentType?.AccountCode,
                                    TenderName = tenderView.PaymentType?.Name
                                };

                                if (!string.IsNullOrEmpty(c.TicketEntities))
                                {
                                    var allEntities =
                                        JsonConvert.DeserializeObject<IList<TicketEntityMap>>(c.TicketEntities);
                                    var lastEntity = allEntities.LastOrDefault(a => a.EntityTypeId == 2);

                                    if (lastEntity != null) detail.Table = lastEntity.EntityName;
                                }

                                if (!string.IsNullOrEmpty(c.TicketPromotionDetails))
                                {
                                    var allPromoDetails = JsonConvert
                                        .DeserializeObject<List<PromotionDetailValue>>(c.TicketPromotionDetails);

                                    if (allPromoDetails.Any())
                                    {
                                        detail.Discount = allPromoDetails.Sum(a => a.PromotionAmount);
                                        ;
                                    }
                                }

                                if (!string.IsNullOrEmpty(payment.Key))
                                    try
                                    {
                                        var conFrame =
                                            JsonConvert.DeserializeObject<PaymentFrame>(payment.Value.Last().PaymentTags);
                                        if (conFrame != null)
                                        {
                                            detail.CardNumber = conFrame.Last4DigitsCard;
                                            detail.ReferenceNo = conFrame.Description;
                                        }

                                    }
                                    catch (Exception)
                                    {
                                        //
                                    }

                                tenderView.TenderDetails.Add(detail);
                            }


                        }
                        // filter by dynamic filter
                    }

                    var dataAsQueryable = tenderView.TenderDetails.AsQueryable();
                    if (!string.IsNullOrEmpty(input.DynamicFilter))
                    {
                        var jsonSerializerSettings = new JsonSerializerSettings
                        {
                            ContractResolver =
                                new CamelCasePropertyNamesContractResolver()
                        };
                        var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                        if (filRule?.Rules != null) dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                    }

                    tenderView.TenderDetails = dataAsQueryable.ToList();
                }
            }

            return allReturnList;
        }

        public async Task<FileDto> GetTenderDetailsExport(GetTicketInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                var setting = await _tenantSettingsService.GetAllSettings();
                input.DatetimeFormat = setting.Connect.DateTimeFormat;
                input.DateFormat = setting.Connect.SimpleDateFormat;
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.TENDER,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.TENDER,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value,
                            TicketInput = input
                        });
                }
                else
                {
                    return await _exporter.ExportToFileTenderDetail(input, this);
                }
            }

            return null;
        }

        public async Task<PagedResultOutput<WorkPeriodSummaryOutput>> GetWorkPeriodByLocationForDatesWithPaging(
            WorkPeriodSummaryInputWithPaging input)
        {
            var allW = _workPeriod.GetAll();

            var locations = new List<int>();
            if (input.Locations != null && input.Locations.Any())
                locations = input.Locations.Select(a => a.Id).ToList();
            if (locations.Any())
                allW = allW.Where(a => locations.Contains(a.LocationId));

            var nextDay = input.EndDate.AddDays(1);

            allW = allW.Where(
                a =>
                    DbFunctions.TruncateTime(a.StartTime) >=
                    DbFunctions.TruncateTime(input.StartDate)
                    &&
                    DbFunctions.TruncateTime(a.EndTime) <
                    DbFunctions.TruncateTime(nextDay));

            var locLIst = await _lRepo.GetAllListAsync();

            var output = (await allW.ToListAsync()).OrderBy(t => t.Id).Select(workPeriod => new WorkPeriodSummaryOutput
            {
                Wid = workPeriod.Id,
                ReportStartDay = workPeriod.StartTime,
                ReportEndDay = workPeriod.EndTime,
                LocationId = workPeriod.LocationId,
                LocationName = locLIst.First(t => t.Id == workPeriod.LocationId).Name,
                LocationCode = locLIst.First(t => t.Id == workPeriod.LocationId).Code,
                DayInformation = workPeriod.WorkPeriodInformations,
                Total = workPeriod.TotalSales,
                TotalTicketCount = workPeriod.TotalTicketCount
            }).ToList();

            if (!input.Sorting.Equals("Id"))
                output = output.OrderBy(input.Sorting).ToList();

            var outputPage = output.AsQueryable().PageBy(input).ToList();

            return new PagedResultOutput<WorkPeriodSummaryOutput>(output.Count(), outputPage);
        }

        public async Task<FileDto> GetWorkPeriodByLocationForDatesWithPagingToExcel(WorkPeriodSummaryInputWithPaging input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                var setting = await _tenantSettingsService.GetAllSettings();
                input.DatetimeFormat = setting.Connect.DateTimeFormat;
                input.DateFormat = setting.Connect.SimpleDateFormat;
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.PLANWORKDAY,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.PLANWORKDAY,
                            WorkPeriodSummaryInputWithPagingInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                }
                else
                {
                    return await _exporter.ExportToFilePlanWorkDay(input, this);
                }
            }

            return null;
        }


        public async Task<PagedResultOutput<WorkPeriodSummaryOutput>> GetWorkPeriodByLocationDailyForDatesWithPaging(
            WorkPeriodSummaryInputWithPaging input)
        {
            var allW = _workPeriod.GetAll();

            var locations = new List<int>();
            if (input.Locations != null && input.Locations.Any())
                locations = input.Locations.Select(a => a.Id).ToList();
            if (locations.Any())
                allW = allW.Where(a => locations.Contains(a.LocationId));

            var nextDay = input.EndDate.AddDays(1);

            allW = allW.Where(
                a =>
                    DbFunctions.TruncateTime(a.StartTime) >=
                    DbFunctions.TruncateTime(input.StartDate)
                    &&
                    DbFunctions.TruncateTime(a.EndTime) <
                    DbFunctions.TruncateTime(nextDay));

            var locLIst = await _lRepo.GetAllListAsync();

            var output = (await allW.ToListAsync()).OrderBy(t => t.Id).Select(workPeriod => new WorkPeriodSummaryOutput
            {
                Wid = workPeriod.Id,
                ReportStartDay = workPeriod.StartTime,
                ReportEndDay = workPeriod.EndTime,
                LocationId = workPeriod.LocationId,
                LocationName = locLIst.First(t => t.Id == workPeriod.LocationId).Name,
                LocationCode = locLIst.First(t => t.Id == workPeriod.LocationId).Code,
                DayInformation = workPeriod.WorkPeriodInformations,
                Total = workPeriod.TotalSales,
                TotalTicketCount = workPeriod.TotalTicketCount
            }).ToList();

            if (!input.Sorting.Equals("Id"))
                output = output.OrderBy(input.Sorting).ToList();
            var ws = new WorkShiftDto();
            ws.UniqueId = "ALL";
            ws.infoId = "ALL";
            foreach (var item in output)
            {
                item.WorkShiftsTemp = item.WorkShifts;
                item.WorkShiftsTemp.Add(ws);
            }

            var outputPage = output.AsQueryable().PageBy(input).ToList();

            return new PagedResultOutput<WorkPeriodSummaryOutput>(output.Count(), outputPage);
        }


        public IQueryable<Order> GetAllOrders(GetItemInput input)
        {
            if (AbpSession.UserId.HasValue && AbpSession.TenantId.HasValue)
            {
                input.UserId = AbpSession.UserId.Value;
                input.TenantId = AbpSession.TenantId.Value;
            }

            var tickets = GetAllTickets(input)
                .WhereIf(!string.IsNullOrEmpty(input.TerminalName),
                    e => e.TerminalName.Contains(input.TerminalName))
                ;

            var orders = tickets.SelectMany(a => a.Orders)
                .WhereIf(input.FilterByDepartment.Any(), e => input.FilterByDepartment.Contains(e.DepartmentName))
                .WhereIf(input.FilterByGroup.Any(),
                    e => e.MenuItem.Category.ProductGroup != null &&
                         input.FilterByGroup.Contains(e.MenuItem.Category.ProductGroup.Id))
                .WhereIf(input.FilterByCategory.Any(),
                    e => e.MenuItem.Category != null && input.FilterByCategory.Contains(e.MenuItem.Category.Id))
                .WhereIf(!input.FilterByTag.IsNullOrEmpty(),
                    e => input.FilterByTag.Any(t => e.MenuItem.Tag.Contains(t)));

            if (input.Locations.Any())
            {
                var locationIds = input.Locations.Select(l => l.Id).ToList();
                orders = orders.Where(o => locationIds.Contains(o.Location_Id));
            }

            if (input.MenuItemIds != null && input.MenuItemIds.Any())
                orders = orders
                    .WhereIf(input.MenuItemIds.Any(), o => input.MenuItemIds.Contains(o.MenuItemId));
            if (input.MenuItemPortionIds != null && input.MenuItemPortionIds.Any())
                orders = orders
                    .WhereIf(input.MenuItemPortionIds.Any(),
                        o => input.MenuItemPortionIds.Contains(o.MenuItemPortionId));

            //if (!input.Void && !input.Gift && !input.Comp)
            //    orders = orders.Where(a => a.CalculatePrice && a.DecreaseInventory);

            if (input.Void || input.Gift || input.Comp || input.Refund)
                orders = orders.Where(x =>
                !x.CalculatePrice && !x.IncreaseInventory &&
                (!string.IsNullOrEmpty(x.OrderStates)
                && ((input.Void && x.OrderStates.Contains("Void") && !x.DecreaseInventory) ||
                (input.Gift && x.OrderStates.Contains("Gift") && x.DecreaseInventory) ||
                (input.Comp && x.OrderStates.Contains("Comp") && x.DecreaseInventory) ||
                (input.Refund && x.OrderStates.Contains("Refund") && !x.DecreaseInventory)
                )));

            if (!string.IsNullOrEmpty(input.LastModifiedUserName))
                orders = orders.Where(a => a.CreatingUserName.Contains(input.LastModifiedUserName));
            if (input.FilterByTerminal.Any())
                orders = orders.Where(a => input.FilterByTerminal.Contains(a.Ticket.TerminalName));
            if (input.FilterByUser.Any())
                orders = orders.Where(a => input.FilterByUser.Contains(a.CreatingUserName));

            if (!string.IsNullOrEmpty(input.DynamicFilter))
            {
                var jsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver =
                        new CamelCasePropertyNamesContractResolver()
                };
                var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                if (filRule?.Rules != null)
                {
                    foreach (var lst in filRule.Rules)
                        if (lst.Field == "Void")
                        {
                            orders = orders.Where(a =>
                                !a.CalculatePrice && !a.IncreaseInventory && !a.DecreaseInventory &&
                                !string.IsNullOrEmpty(a.OrderStates) && a.OrderStates.Contains("Void"));
                        }
                        else if (lst.Field == "Gift")
                        {
                            orders =
                                orders.Where(a =>
                                    !a.CalculatePrice && !a.IncreaseInventory && a.DecreaseInventory &&
                                    !string.IsNullOrEmpty(a.OrderStates) && a.OrderStates.Contains("Gift"));
                        }

                        else if (lst.Field == "Comp")
                        {
                            orders =
                                orders.Where(
                                    a =>
                                        !a.CalculatePrice && !a.IncreaseInventory && a.DecreaseInventory &&
                                        !string.IsNullOrEmpty(a.OrderStates) && a.OrderStates.Contains("Comp"));
                        }
                        else if (lst.Field == "DepartmentName")
                        {
                            var deptId = int.Parse(lst.Value);
                            var rsdepartment = _departmentRepo.FirstOrDefault(t => t.Id == deptId);
                            if (rsdepartment != null)
                                orders =
                                    orders.Where(e => e.DepartmentName.Contains(rsdepartment.Name));
                        }
                        else if (lst.Field == "TicketNo")
                        {
                            orders =
                                orders.Where(t => t.Ticket.TicketNumber == lst.Value);
                        }

                    filRule.Rules = filRule.Rules.Where(t =>
                        t.Field != "Void" && t.Field != "Comp" && t.Field != "Gift" && t.Field != "DepartmentName" &&
                        t.Field != "TicketNo").ToList();
                    if (filRule.Rules != null && filRule.Rules.Count > 0)
                        orders = orders.BuildQuery(filRule);
                }
            }

            return orders;
        }

        #region Location Comparison

        public async Task<FileDto> GetLocationComparisonReportToExcel(GetTicketInput input)
        {
            var tickets = await GetAllTicketsForTicketInput(input).ToListAsync();

            var dateGroup = tickets.GroupBy(t => t.LastPaymentTimeTruc);
            var locations = await _lRepo.GetAllListAsync();

            var locationGroup = dateGroup.Select(g =>
            {
                var dto = new LocationComparisonReportOuptut
                {
                    Date = g.Key,
                    LocationDetails = g.GroupBy(l => new { l.LocationId, l.Location?.Name })
                        .Select(e => new LocationComparisonDetails
                        {
                            LocationId = e.Key.LocationId,
                            LocationName = e.Key.Name,
                            SubTotal = e.Sum(t => t.TotalAmount)
                        }).ToList()
                };
                var locationNotin = locations.Where(l => !dto.LocationDetails.Select(e => e.LocationId).Contains(l.Id))
                    .Select(e => new LocationComparisonDetails
                    {
                        LocationId = e.Id,
                        LocationName = e.Name,
                        GrossSales = 0,
                        SubTotal = 0
                    }).ToList();
                dto.LocationDetails.AddRange(locationNotin);
                dto.LocationDetails = dto.LocationDetails.OrderBy(e => e.LocationName).ToList();
                return dto;
            }).ToList();

            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.LOCATIONCOMPARISON,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });

                    if (backGroundId > 0)
                    {
                        input.LocationGroup.UserId = AbpSession.UserId.Value;

                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.LOCATIONCOMPARISON,
                            LocationComparisons = locationGroup,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                    }
                }
                else
                {
                    return _exporter.ExportLocationComparison(locationGroup);
                }
            }

            return null;
        }

        #endregion Location Comparison

        public async Task<FileDto> GetABBReportExport(ABBReprintReportInputDto input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var setting = await _tenantSettingsService.GetAllSettings();
                    input.SimpleDateFormat = setting.Connect.SimpleDateFormat;

                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.ABBREPRINT,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.ABBREPRINT,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value,
                            ABBReprintReportInput = input
                        });
                }
                else
                {
                    return await _exporter.ExportABBReprintReportToFile(input, this);
                }
            }

            return null;
        }

        public async Task<PagedResultOutput<ABBReprintReportOutputDto>> GetABBReprintReport(
            ABBReprintReportInputDto input)
        {
            try
            {
                var result = new PagedResultOutput<ABBReprintReportOutputDto>();

                var objects = new List<ABBReprintReportOutputDto>();


                if (string.IsNullOrWhiteSpace(input.SimpleDateFormat))
                {
                    var setting = await _tenantSettingsService.GetAllSettings();
                    input.SimpleDateFormat = setting.Connect.SimpleDateFormat;
                }

                var tickets = GetAllTicketsForABBReprintReportInput(input);

                var count = 1;

                if (!string.IsNullOrWhiteSpace(input.TicketNumber))
                    tickets = tickets.Where(x => x.TicketNumber == input.TicketNumber);
                foreach (var ticket in tickets)
                {
                    var location = await _lRepo.GetAsync(ticket.LocationId);
                    var payment = _paymentRepo.GetAll().Where(x => x.TicketId == ticket.Id).FirstOrDefault();
                    var cashier = "";
                    if (payment != null)
                    {
                        cashier = payment?.PaymentUserName;
                    }
                    else
                    {
                        var lstPayment = ticket.GetLogTicketLogs(DinePlanLogTypes.PAYMENT);
                        if (lstPayment != null && lstPayment.Count() > 0)
                        {
                            var paymentLog = lstPayment.OrderByDescending(x => x.DateTime).FirstOrDefault();
                            cashier = paymentLog?.UserName;
                        }
                    }

                    var ticketStateValues = ticket.GetTicketStateValues();

                    var ticketLogs = ticket.GetLogTicketLogs(DinePlanLogTypes.PRINTDUPLICATETICKET);

                    var status = "";

                    foreach (var ticketStateValue in ticketStateValues)
                        if (ticketStateValue.StateName == ABBReprintConsts.Status)
                            status = ticketStateValue.State;

                    if (ticketLogs != null && ticketLogs.Count() > 1)
                        Console.WriteLine(1);

                    if (ticketLogs != null && ticketLogs.Count() > 0 &&
                        (input.SaleStatus == SaleStatusConsts.All && !input.IsMoreThanOneReprintChecked
                         || input.SaleStatus != SaleStatusConsts.All && !input.IsMoreThanOneReprintChecked &&
                         input.SaleStatus == status
                         || input.SaleStatus == SaleStatusConsts.All && input.IsMoreThanOneReprintChecked &&
                         ticketLogs.Count() > 1
                         || input.SaleStatus != SaleStatusConsts.All && input.IsMoreThanOneReprintChecked &&
                         input.SaleStatus == status && ticketLogs.Count() > 1))
                    {
                        objects.Add(new ABBReprintReportOutputDto
                        {
                            PlantCode = location?.Code,
                            PlantName = location?.Name,
                            Cashier = cashier,
                            Date = ticket.TicketCreatedTime.ToString(input.SimpleDateFormat),
                            TicketNumber = ticket.TicketNumber,
                            Status = status,
                            TerminalName = ticket.TerminalName,
                            NoOfReprint = ticketLogs.Count(),
                            Total = ticket.TotalAmount
                        });
                        count++;
                    }
                }

                // filter by query builder
                var mapdateAsQueryable = objects.AsQueryable();
                if (!string.IsNullOrEmpty(input.DynamicFilter))
                {
                    var jsonSerializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver =
                            new CamelCasePropertyNamesContractResolver()
                    };
                    var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                    if (filRule?.Rules != null) mapdateAsQueryable = mapdateAsQueryable.BuildQuery(filRule);
                }

                result.Items = mapdateAsQueryable.OrderBy(input.Sorting).PageBy(input).ToList();
                result.TotalCount = count;

                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<PagedResultOutput<ExternalLogListDto>> GetAllExternalLog(GetItemInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var allItems = _externalLogRepo.GetAll();
                if (input.AuditTypes != null && input.AuditTypes.Any())
                {
                    var allTypes = input.AuditTypes.Select(a => Convert.ToInt32(a.Value)).ToArray();
                    allItems = allItems.Where(a => allTypes.Contains(a.AuditType));
                }

                DateTime? StartDate, EndDate;

                StartDate = input.StartDate;
                EndDate = input.EndDate;
                if (input.StartDate != null && input.EndDate != null)
                    allItems =
                        allItems.Where(
                            a =>
                                a.ExternalLogTrunc >=
                                input.StartDate
                                &&
                                a.ExternalLogTrunc <=
                                input.EndDate);

                var sortMenuItems = allItems.AsQueryable()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList();

                var allListDtos = sortMenuItems.MapTo<List<ExternalLogListDto>>();
                var allItemCount = await allItems.CountAsync();

                return new PagedResultOutput<ExternalLogListDto>(
                    allItemCount,
                    allListDtos
                );
            }
        }

        public Transaction.Ticket GetTicketByNumber(string ticketNumber)
        {
            var ticket = _ticketManager.GetAll().Where(x => x.TicketNumber.Equals(ticketNumber)).FirstOrDefault();
            return ticket;
        }

        public async Task<ItemStatsDto> GetItemSalesNotFilterByDynamicFilter(GetItemInput input)
        {
            var returnOutput = new ItemStatsDto();
            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                var orders = GetAllOrdersNotFilterByDynamicFilter(input);
                var output = from c in orders
                             group c by new { c.MenuItemPortionId, Location = c.Location_Id }
                    into g
                             select new { g.Key.Location, MenuItemPortion = g.Key.MenuItemPortionId, Items = g };

                var menuListOutputDots = new List<MenuListDto>();

                var dicLocations = new Dictionary<int, string>();
                var dicCategory = new Dictionary<int, string>();
                var dicGroups = new Dictionary<int, string>();
                var aliasCodes = new Dictionary<int, string>();
                var portionIds = new Dictionary<int, string>();

                foreach (var dto in output)
                {
                    var firObj = dto.Items.First();
                    if (firObj != null)
                        try
                        {
                            var lname = "";
                            if (dicLocations.Any() && dicLocations.ContainsKey(dto.Location))
                            {
                                lname = dicLocations[dto.Location];
                            }
                            else
                            {
                                var lSer = _lRepo.Get(dto.Location);
                                if (lSer == null) continue;

                                lname = lSer.Name;
                                dicLocations.Add(dto.Location, lname);
                            }

                            var menuItem = firObj.MenuItemId;
                            var cateDto = GetCategoryDto(dicCategory, dicGroups, aliasCodes, menuItem);
                            var menL = new MenuListDto
                            {
                                AliasCode = cateDto.AliasCode,
                                LocationName = lname,
                                MenuItemPortionId = dto.MenuItemPortion,
                                MenuItemPortionName = firObj.PortionName,
                                MenuItemId = firObj.MenuItemId,
                                MenuItemName = firObj.MenuItemName,
                                CategoryId = cateDto.CatId,
                                CategoryName = cateDto.CatgoryName,
                                GroupId = cateDto.GroupId,
                                GroupName = cateDto.GroupName,
                                Quantity = dto.Items.Sum(a => a.Quantity),
                                Tags = firObj.MenuItem?.Tag,
                                Total2 = dto.Items.Sum(a => a.GetTotal()),
                                Price = dto.Items.Sum(a => a.Quantity * a.Price) / dto.Items.Sum(b => b.Quantity),
                                TaxPrice = dto.Items.Sum(a => a.Quantity * a.GetTaxTotal()) /
                                           dto.Items.Sum(b => b.Quantity)
                            };

                            var allTags = dto.Items.SelectMany(a => a.TransactionOrderTags)
                                .Where(b => !b.AddTagPriceToOrderPrice && b.MenuItemPortionId == 0);

                            if (allTags.Any())
                            {
                                menL.OrderTagPrice =
                                    allTags.Sum(a => a.Price * (a.Order != null ? a.Order.Quantity : 1)) /
                                    dto.Items.Sum(b => b.Quantity);
                                menL.IsOrderTag = false;

                                if (input.IsItemSalesReport)
                                {
                                    var menuOrderTag = new MenuListDto
                                    {
                                        IsOrderTag = true,
                                        AliasCode = cateDto.AliasCode,
                                        LocationName = lname,
                                        MenuItemPortionId = dto.MenuItemPortion,
                                        MenuItemPortionName = firObj.PortionName,
                                        MenuItemId = firObj.MenuItemId,
                                        MenuItemName = firObj.MenuItemName,
                                        CategoryId = cateDto.CatId,
                                        CategoryName = cateDto.CatgoryName,
                                        GroupId = cateDto.GroupId,
                                        GroupName = cateDto.GroupName,
                                        Tags = firObj.MenuItem?.Tag
                                    };
                                    menuOrderTag.Quantity = dto.Items
                                        .Where(e => allTags.Select(t => t.OrderId).Contains(e.Id)).Sum(a => a.Quantity);
                                    menuOrderTag.OrderTagPrice = menL.OrderTagPrice;
                                    menuOrderTag.Price =
                                        dto.Items.Where(e => allTags.Select(t => t.OrderId).Contains(e.Id))
                                            .Sum(a => a.Quantity * a.Price) / menuOrderTag.Quantity +
                                        menuOrderTag.OrderTagPrice;
                                    menuOrderTag.TaxValue =
                                        dto.Items.Where(e => allTags.Select(t => t.OrderId).Contains(e.Id))
                                            .Sum(c => c.GetTaxablePrice()) - menuOrderTag.Price * menuOrderTag.Quantity;
                                    menuOrderTag.TaxPrice =
                                        dto.Items.Where(e => allTags.Select(t => t.OrderId).Contains(e.Id))
                                            .Sum(a => a.Quantity * a.GetTaxTotal()) / dto.Items.Sum(b => b.Quantity);

                                    menuListOutputDots.Add(menuOrderTag);

                                    menL.OrderTagPrice = 0;
                                    menL.Quantity = dto.Items.Where(e => !allTags.Select(t => t.OrderId).Contains(e.Id))
                                        .Sum(a => a.Quantity);
                                    menL.Price =
                                        dto.Items.Where(e => !allTags.Select(t => t.OrderId).Contains(e.Id))
                                            .Sum(a => a.Quantity * a.Price) / menL.Quantity + menL.OrderTagPrice;
                                    menL.TaxValue =
                                        dto.Items.Where(e => !allTags.Select(t => t.OrderId).Contains(e.Id))
                                            .Sum(c => c.GetTaxablePrice()) - menL.Price * menL.Quantity;
                                }
                            }

                            if (!input.IsItemSalesReport || !allTags.Any())
                            {
                                var finalPrice = menL.Price + menL.OrderTagPrice;
                                menL.Price = finalPrice;

                                var taxvalue = dto.Items.Sum(c => c.GetTaxablePrice()) - finalPrice * menL.Quantity;
                                menL.TaxValue = taxvalue;
                            }

                            menuListOutputDots.Add(menL);

                            var itemTags = dto.Items.SelectMany(a => a.TransactionOrderTags)
                                .Where(b => !b.AddTagPriceToOrderPrice && b.MenuItemPortionId > 0);
                            foreach (var transactionOrderTag in itemTags)
                            {
                                var portionId = transactionOrderTag.MenuItemPortionId;

                                var lastMenuListDto =
                                    menuListOutputDots.LastOrDefault(a => a.MenuItemPortionId == portionId);
                                if (lastMenuListDto == null)
                                {
                                    var myItemid = 0;
                                    var portionName = "";

                                    if (portionIds.Any() && portionIds.ContainsKey(portionId))
                                    {
                                        var portionNameSplit = portionIds[menuItem];
                                        var splitName = portionNameSplit.Split('@');
                                        if (splitName.Any())
                                        {
                                            myItemid = Convert.ToInt32(splitName[0]);
                                            portionName = splitName[1];
                                        }
                                    }
                                    else
                                    {
                                        var lSer = _poRepository.Get(portionId);
                                        if (lSer != null)
                                        {
                                            myItemid = lSer.MenuItemId;
                                            portionName = lSer.Name;
                                            portionIds.Add(lSer.Id, myItemid + "@" + portionName);
                                        }
                                    }

                                    var myCategoryDto = GetCategoryDto(dicCategory, dicGroups, aliasCodes, myItemid);
                                    var myTagList = new MenuListDto
                                    {
                                        IsOrderTag = true,
                                        AliasCode = myCategoryDto.AliasCode,
                                        LocationName = lname,
                                        MenuItemPortionId = portionId,
                                        MenuItemPortionName = portionName,
                                        MenuItemId = myItemid,
                                        MenuItemName = transactionOrderTag.TagValue,
                                        CategoryId = myCategoryDto.CatId,
                                        CategoryName = myCategoryDto.CatgoryName,
                                        GroupId = myCategoryDto.GroupId,
                                        GroupName = myCategoryDto.GroupName,
                                        Quantity = transactionOrderTag.Quantity,
                                        Tags = "",
                                        Price = transactionOrderTag.Price,
                                        TaxPrice = dto.Items.Sum(a => a.Quantity * a.GetTaxTotal()) /
                                                   dto.Items.Sum(b => b.Quantity)
                                    };
                                    menuListOutputDots.Add(myTagList);
                                }
                                else
                                {
                                    lastMenuListDto.Quantity += transactionOrderTag.Quantity;
                                    lastMenuListDto.Price =
                                        (lastMenuListDto.Price * lastMenuListDto.Quantity +
                                         transactionOrderTag.Price * transactionOrderTag.Quantity) /
                                        (lastMenuListDto.Quantity + transactionOrderTag.Quantity);
                                }
                            }
                        }
                        catch (Exception exception)
                        {
                            var mess = exception.Message;
                        }
                }

                if (input.NoSales) menuListOutputDots = GetNoSales(menuListOutputDots);


                if (input.Stats)
                {
                    returnOutput.Quantities = GetItemByQuantity(orders);
                    returnOutput.Quantity = returnOutput.Quantities.Count() < 10 ? returnOutput.Quantities.Count : 10;

                    returnOutput.Totals = GetItemByTotal(orders);
                    returnOutput.Total = returnOutput.Totals.Count() < 10 ? returnOutput.Totals.Count : 10;
                }


                if (!input.Sorting.Equals("Id"))
                    menuListOutputDots = menuListOutputDots.OrderBy(input.Sorting).ToList();

                var totalCount = menuListOutputDots.Count();

                if (input.IsSummary)
                    menuListOutputDots = menuListOutputDots.AsQueryable().PageBy(input).ToList();

                returnOutput.MenuList = new PagedResultOutput<MenuListDto>(
                    totalCount,
                    menuListOutputDots
                );
            }

            return returnOutput;
        }

        public IQueryable<Order> GetAllOrdersExchange(GetItemInput input, bool isApplyFilter = true)
        {
            if (AbpSession.UserId.HasValue && AbpSession.TenantId.HasValue)
            {
                input.UserId = AbpSession.UserId.Value;
                input.TenantId = AbpSession.TenantId.Value;
            }

            var tickets = GetAllTickets(input);
            var orders = tickets.SelectMany(a => a.Orders)
                .WhereIf(input.FilterByDepartment.Any(), e => input.FilterByDepartment.Contains(e.DepartmentName))
                .WhereIf(input.FilterByGroup.Any(),
                    e => e.MenuItem.Category.ProductGroup != null &&
                         input.FilterByGroup.Contains(e.MenuItem.Category.ProductGroup.Id))
                .WhereIf(input.FilterByCategory.Any(),
                    e => e.MenuItem.Category != null && input.FilterByCategory.Contains(e.MenuItem.Category.Id))
                .WhereIf(!input.FilterByTag.IsNullOrEmpty(),
                    e => input.FilterByTag.Any(t => e.MenuItem.Tag.Contains(t)))
                .Where(x => x.OrderStates.Contains("Exchange"));

            if (input.Locations.Any())
            {
                var locationIds = input.Locations.Select(l => l.Id).ToList();
                orders = orders.Where(o => locationIds.Contains(o.Location_Id));
            }

            if (input.MenuItemIds != null && input.MenuItemIds.Any())
                orders = orders
                    .WhereIf(input.MenuItemIds.Any(), o => input.MenuItemIds.Contains(o.MenuItemId));
            if (input.MenuItemPortionIds != null && input.MenuItemPortionIds.Any())
                orders = orders
                    .WhereIf(input.MenuItemPortionIds.Any(),
                        o => input.MenuItemPortionIds.Contains(o.MenuItemPortionId));

            if (!input.Void && !input.Gift && !input.Comp)
                orders = orders.Where(a => a.CalculatePrice && a.DecreaseInventory);

            if (!string.IsNullOrEmpty(input.LastModifiedUserName))
                orders = orders.Where(a => a.CreatingUserName.Contains(input.LastModifiedUserName));
            if (input.FilterByTerminal.Any())
                orders = orders.Where(a => input.FilterByTerminal.Contains(a.Ticket.TerminalName));
            if (input.FilterByUser.Any())
                orders = orders.Where(a => input.FilterByUser.Contains(a.CreatingUserName));

            if (isApplyFilter && !string.IsNullOrEmpty(input.DynamicFilter))
            {
                var jsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver =
                        new CamelCasePropertyNamesContractResolver()
                };
                var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                if (filRule?.Rules != null)
                    foreach (var lst in filRule.Rules)
                        if (lst.Field == "Reason")
                            orders =
                                orders.Where(t => t.OrderLog.Contains(lst.Value));
                        else if (lst.Field == "ReceiptNumber")
                            orders =
                                orders.Where(t => t.Ticket.TicketNumber == lst.Value);
                        else if (lst.Field == "Detail")
                            orders =
                                orders.Where(t => t.Description.Contains(lst.Value));
            }

            return orders;
        }

        private async Task FillDataForLoactions(GetItemInput input, List<CategoryReportDto> items)
        {
            if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var locationIds = input.LocationGroup.Locations.Select(e => e.Id).ToList();
                var locations = await _lRepo.GetAll()
                    .Where(l => locationIds.Contains(l.Id))
                    .Select(loc => new SimpleLocationDto
                    {
                        Id = loc.Id,
                        Code = loc.Code,
                        Name = loc.Name,
                        CompanyRefId = loc.CompanyRefId
                    })
                    .ToListAsync();
                items.ForEach(item =>
                {
                    var locationNoData = locations
                        .Where(c => !item.LocationReports.Select(l => l.LocationId).Contains(c.Id))
                        .Select(e => new LocationReportDto
                        {
                            LocationId = e.Id,
                            LocationName = e.Name,
                            Quantity = 0M,
                            TotalAmount = 0M
                        }).ToList();
                    item.LocationReports.AddRange(locationNoData);
                    item.LocationReports = item.LocationReports.OrderBy(l => l.LocationName).ToList();
                });
            }
        }

        private TCategoryDto GetCategoryDto(Dictionary<int, string> dicCategory, Dictionary<int, string> dicGroups,
            Dictionary<int, string> aliasCodes, int menuItem)
        {
            var returnD = new TCategoryDto();

            if (dicCategory.Any() && dicCategory.ContainsKey(menuItem))
            {
                var catSplitname = dicCategory[menuItem];
                var splitName = catSplitname.Split('@');
                if (splitName.Any())
                {
                    returnD.CatId = Convert.ToInt32(splitName[0]);
                    returnD.CatgoryName = splitName[1];
                    returnD.CatgoryCode = splitName[2];
                }
            }
            else
            {
                var lSer = _mrRepo.Get(menuItem);
                if (lSer != null)
                {
                    returnD.CatId = lSer.CategoryId ?? 0;
                    returnD.CatgoryName = lSer.Category.Name;
                    returnD.CatgoryCode = lSer.Category.Code;
                    dicCategory.Add(menuItem, returnD.CatId + "@" + returnD.CatgoryName + "@" + returnD.CatgoryCode);
                }
            }

            if (dicGroups.Any() && dicGroups.ContainsKey(menuItem))
            {
                var catSplitname = dicGroups[menuItem];
                var splitName = catSplitname.Split('@');
                if (splitName.Any())
                {
                    returnD.GroupId = Convert.ToInt32(splitName[0]);
                    returnD.GroupName = splitName[1];
                }
            }
            else
            {
                var lSer = _mrRepo.Get(menuItem);
                if (lSer != null)
                {
                    returnD.GroupId = lSer.Category?.ProductGroupId ?? 0;
                    returnD.GroupName = lSer.Category?.ProductGroup?.Name;
                    dicGroups.Add(menuItem, returnD.GroupId + "@" + returnD.GroupName);
                }
            }

            if (aliasCodes.Any() && aliasCodes.ContainsKey(menuItem))
            {
                returnD.AliasCode = aliasCodes[menuItem];
            }
            else
            {
                var alisCodeSet = _mrRepo.Get(menuItem);
                aliasCodes.Add(menuItem, alisCodeSet.AliasCode);
                returnD.AliasCode = alisCodeSet.AliasCode;
            }

            return returnD;
        }

        private ItemTagStatsDto GetItemByTag(GetItemInput input, List<MenuListDto> menuListDtos)
        {
            var groups = new List<string>();
            var chartOutput = new List<BarChartOutputDto>();
            var oDto = new List<ItemTagReportDto>();

            if (menuListDtos != null && menuListDtos.Any())
            {
                var groupDto = menuListDtos.GroupBy(i => i.Tag).Select(g => new { Tag = g.Key, Items = g });

                foreach (var dto in groupDto)
                    if (dto.Tag != null)
                    {
                        var quantity = dto.Items.Sum(a => a.Quantity);
                        var total = dto.Items.Sum(a => a.Quantity * a.Price);
                        oDto.Add(new ItemTagReportDto
                        {
                            TagName = dto.Tag,
                            Quantity = quantity,
                            Total = total,
                            TotalItems = dto.Items.ToList()
                        });
                    }
            }


            var dataAsQueryable = oDto.AsQueryable();
            if (!string.IsNullOrEmpty(input.DynamicFilter))
            {
                var jsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver =
                        new CamelCasePropertyNamesContractResolver()
                };
                var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                if (filRule?.Rules != null) dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
            }

            oDto = dataAsQueryable.ToList();
            groups = oDto.Select(x => x.TagName).ToList();
            foreach (var item in oDto)
                chartOutput.Add(new BarChartOutputDto
                {
                    name = item.TagName,
                    data = new List<decimal> { item.Total }
                });

            var menuItemCount = oDto.Count();
            var itemTags = oDto.ToList();

            if (!input.IsExport) itemTags = oDto.AsQueryable().PageBy(input).ToList();

            var result = new ItemTagStatsDto
            {
                ItemTagList = new PagedResultOutput<ItemTagReportDto>(menuItemCount, itemTags),
                ItemTags = groups,
                ChartOutput = chartOutput
            };
            return result;
        }

        private List<ItemSale> GetOrderItems(Order comboOrder, List<Order> allComboOrdersWithItems)
        {
            var refOrders = allComboOrdersWithItems
                .Where(t => t.OrderRef == comboOrder.OrderRef && t.Id != comboOrder.Id)
                .OrderBy(o => o.Id).ToList();

            var result = refOrders.Select(t => new ItemSale
            {
                Id = t.MenuItemId,
                Name = t.MenuItemName,
                Quantity = t.Quantity,
                Amount = t.Quantity * t.Price,
                GroupName = ""
            }).ToList();
            return result;
        }

        private List<ItemSale> GetOrderTags(Order order)
        {
            if (string.IsNullOrEmpty(order.OrderTags)) return new List<ItemSale>();

            var orderTagIems = JsonConvert.DeserializeObject<List<OrderTagValue>>(order.OrderTags);

            return orderTagIems.Select(t => new ItemSale
            {
                Id = t.TagSync,
                Name = t.TagValue,
                Quantity = t.Quantity,
                Amount = t.Quantity * t.Price
            }).ToList();
        }

        private void BuildHour(IdHourDto myHourList, IGrouping<DateTime, Transaction.Ticket> myTickets)
        {
            myHourList.Total = myTickets.Sum(a => a.TotalAmount);
            myHourList.Tickets = myTickets.Count();

            myHourList.Hours = myTickets
                .GroupBy(x => x.LastPaymentTime.Hour)
                .Select(g => new HourTotalDto
                {
                    Hour = g.Key,
                    Total = g.Sum(x => x.TotalAmount),
                    Tickets = g.Count()
                }).ToList();

            var allHours = myHourList.Hours.Select(a => a.Hour);
            for (var i = 0; i <= 23; i++)
            {
                var hh = allHours.Contains(i);
                if (!hh)
                    myHourList.Hours.Add(new HourTotalDto
                    {
                        Hour = i,
                        Total = 0M,
                        Tickets = 0
                    });
            }

            myHourList.Hours = myHourList.Hours.OrderBy(a => a.Hour).ToList();
        }

        private async Task<List<MenuListDto>> GetMLD(IQueryable<Order> old)
        {
            var outputDtos = new List<MenuListDto>();
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var output = from c in old
                             group c by new { c.MenuItemPortionId, c.DepartmentName }
                    into g
                             select new { MenuItemPortion = g.Key.MenuItemPortionId, g.Key.DepartmentName, Items = g };

                var arrMenuItemPortionRefIds = output.Select(t => t.MenuItemPortion).ToList();
                var rsPortions = await _poRepository.GetAllListAsync(t => arrMenuItemPortionRefIds.Contains(t.Id));
                var arrMenuIds = rsPortions.Select(t => t.MenuItemId).ToList();
                var rsMenu = await _mrRepo.GetAllListAsync(t => arrMenuIds.Contains(t.Id));
                var arrCategoryRefIds =
                    rsMenu.Where(t => t.CategoryId.HasValue).Select(t => t.CategoryId.Value).ToList();
                var rsCategory = await _catRepository.GetAllListAsync(t => arrCategoryRefIds.Contains(t.Id));

                foreach (var dto in output)
                {
                    #region Replace AddItems //////await AddItems(dto.MenuItemPortion, dto.DepartmentName, dto.Items.AsQueryable(), outputDtos);

                    var portion = rsPortions.FirstOrDefault(t => t.Id == dto.MenuItemPortion);
                    if (portion != null)
                        try
                        {
                            var menuItem = rsMenu.FirstOrDefault(t => t.Id == portion.MenuItemId);
                            if (menuItem != null)
                            {
                                var myCategory = rsCategory.FirstOrDefault(t => t.Id == menuItem.CategoryId);

                                outputDtos.Add(new MenuListDto
                                {
                                    MenuItemPortionId = dto.MenuItemPortion,
                                    MenuItemPortionName = portion.Name,
                                    MenuItemId = portion.MenuItemId,
                                    MenuItemName = menuItem.Name,
                                    CategoryId = myCategory != null ? myCategory.Id : 0,
                                    CategoryName = myCategory != null ? myCategory.Name : "",
                                    Quantity = dto.Items.Sum(a => a.Quantity),
                                    DepartmentName = dto.DepartmentName,
                                    Price = dto.Items.Sum(a => a.Quantity * a.Price) / dto.Items.Sum(b => b.Quantity)
                                });
                            }
                        }
                        catch (Exception exception)
                        {
                            var mess = exception.Message;
                        }

                    #endregion Replace AddItems //////await AddItems(dto.MenuItemPortion, dto.DepartmentName, dto.Items.AsQueryable(), outputDtos);

                    /*
                    var allTags = dto.Items.Select(a => a.OrderTags);
                    if (allTags != null && allTags.Any())
                    {
                        rsPortions = await _poRepository.GetAllListAsync();
                        rsCategory = await _catRepository.GetAllListAsync();
                        var rsMenuItems = new List<MenuItem>();

                        foreach (var allTag in allTags)
                        {
                            if (string.IsNullOrEmpty(allTag))
                            {
                                continue;
                            }
                            List<ReOrderTagValue> tTake = new List<ReOrderTagValue>();
                            try
                            {
                                tTake = JsonConvert.DeserializeObject<List<ReOrderTagValue>>(allTag);
                            }
                            catch (Exception)
                            {
                                // ignored
                            }

                            if (tTake != null && tTake.Any())
                            {
                                foreach (var orderTagValue in tTake.Where(a => a.MI > 0))
                                {
                                    var portionId = 0;
                                    var menuItem = rsMenuItems.LastOrDefault(a => a.Id == orderTagValue.MI);
                                    if (menuItem != null && menuItem.Portions.Any())
                                    {
                                        var myPortion = menuItem.Portions.First();
                                        portionId = myPortion.Id;
                                    }
                                    else
                                    {
                                        menuItem = _mrRepo.Get(orderTagValue.MI);
                                        if (menuItem != null)
                                        {
                                            var myPortion = menuItem.Portions.First();
                                            portionId = myPortion.Id;
                                            rsMenuItems.Add(menuItem);
                                        }
                                        else
                                        {
                                            continue;
                                        }
                                    }

                                    if (portionId > 0)
                                    {
                                        #region Replace AddOrderItems ////await AddOrderItems(portionId, dto.DepartmentName, orderTagValue, outputDtos);

                                        portion = rsPortions.FirstOrDefault(t => t.Id == portionId);
                                        var myCategory = rsCategory.FirstOrDefault(t => t.Id == menuItem.CategoryId);

                                        var myTag = outputDtos.FirstOrDefault(a =>
                                            a.MenuItemPortionId == portionId &&
                                            a.DepartmentName.Equals(dto.DepartmentName));

                                        if (myTag == null)
                                        {
                                            outputDtos.Add(new MenuListDto
                                            {
                                                MenuItemPortionId = portionId,
                                                MenuItemPortionName = portion.Name,
                                                MenuItemId = portion.MenuItemId,
                                                MenuItemName = menuItem.Name,
                                                CategoryId = myCategory != null ? myCategory.Id : 0,
                                                CategoryName = myCategory != null ? myCategory.Name : "",
                                                Quantity = orderTagValue.Q,
                                                DepartmentName = dto.DepartmentName,
                                                Price = orderTagValue.PR
                                            });
                                        }
                                        else
                                        {
                                            myTag.Quantity += orderTagValue.Q;
                                            myTag.Price =
                                                (myTag.Price * myTag.Quantity + orderTagValue.PR * orderTagValue.Q) /
                                                (myTag.Quantity + orderTagValue.Q);
                                        }

                                        #endregion Replace AddOrderItems ////await AddOrderItems(portionId, dto.DepartmentName, orderTagValue, outputDtos);
                                    }
                                }
                            }
                        }
                    }
                    */
                }
            }

            return outputDtos;
        }

        private void ProcessSales(List<IdDayItemOutputDto> output, GetDayItemInput input, ItemStatsDto allSales,
            string StartDate)
        {
            foreach (var allSale in allSales.MenuList.Items)
            {
                IdDayItemOutputDto por = null;

                if (input != null && input.ByLocation)
                    por =
                        output.SingleOrDefault(a =>
                            a.PortionId == allSale.MenuItemPortionId && a.LocationName != null &&
                            a.LocationName.Equals(allSale.LocationName));
                else
                    por = output.SingleOrDefault(
                        a => a.PortionId == allSale.MenuItemPortionId);

                if (por == null)
                {
                    por = new IdDayItemOutputDto
                    {
                        IsOrderTag = allSale.IsOrderTag,
                        MenuItemId = allSale.MenuItemId,
                        MenuItemName = allSale.MenuItemName,
                        PortionId = allSale.MenuItemPortionId,
                        PortionName = allSale.MenuItemPortionName,
                        CategoryId = allSale.CategoryId,
                        CategoryName = allSale.CategoryName,
                        CategoryCode = allSale.CategoryCode,
                        AliasCode = allSale.AliasCode,
                        Tag = allSale.Tags
                    };
                    if (input.ByLocation)
                        por.LocationName = allSale.LocationName;

                    output.Add(por);
                }

                var dd = por.Items.SingleOrDefault(a => a.Date.Equals(StartDate));
                if (dd == null)
                {
                    por.Items.Add(new DayWiseItemDto
                    {
                        Date = StartDate,
                        Quantity = allSale.Quantity,
                        Price = allSale.Price
                    });
                }
                else
                {
                    dd.Price = (dd.Quantity * dd.Price +
                                allSale.Quantity * allSale.Price) / (dd.Quantity + allSale.Quantity);
                    dd.Quantity += allSale.Quantity;
                }
            }
        }

        private void BuildHour(IdHourDto myHourList, IGrouping<object, Transaction.Ticket> myTickets)
        {
            myHourList.Total = myTickets.Sum(a => a.TotalAmount);
            myHourList.Tickets = myTickets.Count();
            myHourList.Hours = myTickets
                .GroupBy(x => x.CreationTime.Hour)
                .Select(g => new HourTotalDto
                {
                    Hour = g.Key,
                    Total = g.Sum(x => x.TotalAmount),
                    Tickets = g.Count()
                }).ToList();

            var allHours = myHourList.Hours.Select(a => a.Hour);

            for (var i = 0; i <= 23; i++)
            {
                var hh = allHours.Contains(i);
                if (!hh)
                    myHourList.Hours.Add(new HourTotalDto
                    {
                        Hour = i,
                        Total = 0M,
                        Tickets = 0
                    });
            }

            myHourList.Hours = myHourList.Hours.OrderBy(a => a.Hour).ToList();
        }

        private void BuildHour(IdHourDto myHourList, IGrouping<int?, Transaction.Ticket> myTickets)
        {
            myHourList.Total = myTickets.Sum(a => a.TotalAmount);
            myHourList.Tickets = myTickets.Count();
            myHourList.Hours = myTickets
                .GroupBy(x => x.CreationTime.Hour)
                .Select(g => new HourTotalDto
                {
                    Hour = g.Key,
                    Total = g.Sum(x => x.TotalAmount),
                    Tickets = g.Count()
                }).ToList();

            var allHours = myHourList.Hours.Select(a => a.Hour);

            for (var i = 0; i < 23; i++)
            {
                var hh = allHours.Contains(i);
                if (!hh)
                    myHourList.Hours.Add(new HourTotalDto
                    {
                        Hour = i,
                        Total = 0M,
                        Tickets = 0
                    });
            }

            myHourList.Hours = myHourList.Hours.OrderBy(a => a.Hour).ToList();
        }

        private void BuildHour(HourDto myHourList, IQueryable<Transaction.Ticket> myTickets)
        {
            if (myTickets.Any())
            {
                myHourList.Total = myTickets.Sum(a => a.TotalAmount);
                myHourList.Tickets = myTickets.Count();
                myHourList.Hours = myTickets
                    .GroupBy(x => x.LastPaymentTime.Hour)
                    .Select(g => new HourTotalDto
                    {
                        Hour = g.Key,
                        Tickets = g.Count(),
                        Total = g.Sum(x => x.TotalAmount)
                    }).ToList();

                var allHours = myHourList.Hours.Select(a => a.Hour);

                for (var i = 0; i < 24; i++)
                {
                    var hh = allHours.Contains(i);
                    if (!hh)
                        myHourList.Hours.Add(new HourTotalDto
                        {
                            Hour = i,
                            Total = 0M,
                            Tickets = 0
                        });
                }

                myHourList.Hours = myHourList.Hours.OrderBy(a => a.Hour).ToList();
            }
        }

        public List<ChartOutputDto> GetItemByQuantity(IQueryable<MenuListDto> input)
        {
            var result = input
                .GroupBy(l => l.MenuItemName)
                .Select(cl => new ChartOutputDto
                {
                    name = cl.Key,
                    y = cl.Sum(c => c.Quantity)
                }).OrderByDescending(item => item.y).Take(10).ToList();
            return result;
        }

        public List<ChartOutputDto> GetItemByQuantity(IQueryable<Order> input)
        {
            var result = input
                .GroupBy(l => l.MenuItemName)
                .Select(cl => new ChartOutputDto
                {
                    name = cl.Key,
                    y = cl.Sum(c => c.Quantity)
                }).OrderByDescending(item => item.y).Take(10).ToList();
            return result;
        }

        public List<ChartOutputDto> GetItemByTotal(IQueryable<MenuListDto> input)
        {
            var result = input
                .GroupBy(l => l.MenuItemName)
                .Select(cl => new ChartOutputDto
                {
                    name = cl.Key,
                    y = cl.Sum(c => c.Price * c.Quantity)
                }).OrderByDescending(item => item.y).Take(10).ToList();
            return result;
        }

        public List<ChartOutputDto> GetItemByTotal(IQueryable<Order> input)
        {
            var result = input
                .GroupBy(l => l.MenuItemName)
                .Select(cl => new ChartOutputDto
                {
                    name = cl.Key,
                    y = cl.Sum(c => c.Price * c.Quantity)
                }).OrderByDescending(item => item.y).Take(10).ToList();
            return result;
        }

        private DashboardOrderDto GetStatics(IQueryable<Order> orders)
        {
            var orderDto = new DashboardOrderDto();
            try
            {
                if (!orders.Any()) return orderDto;
                var menuListOutputDots = GetMenuListDtoOutput(orders);
                orderDto.TotalAmount = menuListOutputDots.Sum(a => a.Total);
                orderDto.TotalOrderCount = orders.Count();
                orderDto.TotalItemSold = orders.Sum(a => a.Quantity);
            }
            catch (Exception ex)
            {
                // ignored
            }

            return orderDto;
        }

        private DashboardOrderDto GetStaticsExchangeReport(List<Order> orders)
        {
            var orderDto = new DashboardOrderDto();
            try
            {
                if (!orders.Any()) return orderDto;
                orderDto.TotalAmount = orders.DefaultIfEmpty().Sum(a => a.Price * a.Quantity);
                orderDto.TotalOrderCount = orders.Count();
                orderDto.TotalItemSold = orders.Sum(a => a.Quantity);
            }
            catch (Exception ex)
            {
                // ignored
            }

            return orderDto;
        }

        private DashboardOrderDto CreateDashBoardExchangeReport(List<OrderExchangeViewDto> orders)
        {
            var orderDto = new DashboardOrderDto();
            try
            {
                if (!orders.Any()) return orderDto;
                orderDto.TotalAmount = orders.DefaultIfEmpty().Sum(a => a.Amount);
                orderDto.TotalOrderCount = orders.Count();
                orderDto.TotalItemSold = orders.Sum(a => a.Quantity);
            }
            catch (Exception ex)
            {
                // ignored
            }

            return orderDto;
        }


        private DashboardTicketDto GetStatics(IQueryable<Transaction.Ticket> allTickets)
        {
            var ticketDto = new DashboardTicketDto();

            if (!allTickets.Any()) return ticketDto;
            try
            {
                ticketDto.TotalTicketCount = allTickets.Count();
                ticketDto.TotalAmount = allTickets.DefaultIfEmpty().Sum(a => a.TotalAmount);
                ticketDto.AverageTicketAmount = ticketDto.TotalAmount / ticketDto.TotalTicketCount;
                ticketDto.TotalOrderCount = allTickets.Sum(a => a.Orders.Count);
                ticketDto.TotalItemSold = 0;
            }
            catch (Exception)
            {
                // ignored
            }

            return ticketDto;
        }

        private List<MenuListDto> GetNoSales(List<MenuListDto> outputDtos)
        {
            var allPortions = _poRepository.GetAll().ToList();
            var missinPortion = allPortions.Where(e => outputDtos.All(a => a.MenuItemPortionId != e.Id)).ToList();

            if (missinPortion.Any()) outputDtos.Clear();

            try
            {
                foreach (var missP in missinPortion)
                    if (missP?.MenuItem?.Category != null)
                        outputDtos.Add(new MenuListDto
                        {
                            LocationName = L("All"),
                            MenuItemPortionId = missP.Id,
                            MenuItemPortionName = missP.Name,
                            MenuItemId = missP.MenuItemId,
                            MenuItemName = missP.MenuItem.Name,
                            CategoryId = missP.MenuItem.CategoryId ?? 0,
                            CategoryName = missP.MenuItem.Category.Name ?? "",
                            Quantity = 0,
                            Price = 0,
                            Hour = 0
                        });
            }
            catch (Exception exception)
            {
                var mess = exception.Message;
            }

            outputDtos = outputDtos.OrderBy(a => a.LocationName).ToList();
            return outputDtos;
        }

        public List<MenuListDto> Get(List<MenuListDto> outputDtos)
        {
            var allPortions = _poRepository.GetAll().ToList();
            var missinPortion = allPortions.Where(e => outputDtos.All(a => a.MenuItemPortionId != e.Id)).ToList();

            if (missinPortion.Any()) outputDtos.Clear();

            try
            {
                foreach (var missP in missinPortion)
                    if (missP?.MenuItem?.Category != null)
                        outputDtos.Add(new MenuListDto
                        {
                            LocationName = L("All"),
                            MenuItemPortionId = missP.Id,
                            MenuItemPortionName = missP.Name,
                            MenuItemId = missP.MenuItemId,
                            MenuItemName = missP.MenuItem.Name,
                            CategoryId = missP.MenuItem.CategoryId ?? 0,
                            CategoryName = missP.MenuItem.Category.Name ?? "",
                            Quantity = 0,
                            Price = 0,
                            Hour = 0
                        });
            }
            catch (Exception exception)
            {
                var mess = exception.Message;
            }

            outputDtos = outputDtos.OrderBy(a => a.LocationName).ToList();
            return outputDtos;
        }

        public IQueryable<Transaction.Ticket> GetAllTicketsForABBReprintReportInput(ABBReprintReportInputDto input)
        {
            var tickets = GetAllTickets(input);

            if (input.Payments != null && input.Payments.Any())
                tickets = tickets.Include(a => a.Payments)
                    .WhereIf(input.Payments.Any(),
                        p => p.Payments.Any(pt => input.Payments.Contains(pt.PaymentTypeId)));

            if (input.Transactions != null && input.Transactions.Any())
            {
                var transactionTypes = input.Transactions.Select(a => Convert.ToInt32(a.Value)).ToArray();
                tickets = from p in tickets.Include(a => a.Transactions)
                          where p.Transactions.Any(pt => transactionTypes.Contains(pt.TransactionTypeId))
                          select p;
            }

            if (!string.IsNullOrEmpty(input.TerminalName))
                tickets = tickets.Where(a => a.TerminalName.Contains(input.TerminalName));

            if (!string.IsNullOrEmpty(input.Department))
                tickets = tickets.Where(a => a.DepartmentName.Contains(input.Department));
            if (input.Departments != null && input.Departments.Any())
            {
                var allDeparments = input.Departments.Select(a => a.DisplayText).ToArray();
                tickets = tickets.Where(a => a.DepartmentName != null && allDeparments.Contains(a.DepartmentName));
            }

            if (input.TicketTags != null && input.TicketTags.Any())
            {
                var allDeparments = input.TicketTags.Select(a => a.DisplayText).ToArray();
                tickets = from c in tickets
                          where allDeparments.Any(i => c.TicketTags.Contains(i))
                          select c;
            }

            if (!string.IsNullOrEmpty(input.LastModifiedUserName))
                tickets = tickets.Where(a => a.LastModifiedUserName.Contains(input.LastModifiedUserName));

            if (input.Refund) tickets = tickets.Where(a => a.TicketStates.Contains("Refund"));

            return tickets;
        }

        #region Export

        public async Task<FileDto> GetTallyExport(GetTicketInput input)
        {
            FileDto reDto = null;

            input.OutputType = "EXPORT";
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var ttNames = await _ttService.GetAllItems();
                var ttList = ttNames.Items.Select(a => a.Name).ToList();
                var ptNames = await _ptService.GetAllItems();
                var ptList = ptNames.Items.Select(a => a.Name).ToList();

                if (input.Locations == null)
                {
                    var allLocations = await _locService.GetLocationsForLoginUser();
                    var mySimpleLocations = new List<SimpleLocationDto>();
                    foreach (var myLo in allLocations.Items)
                        mySimpleLocations.Add(new SimpleLocationDto
                        {
                            Id = myLo.Id,
                            Name = myLo.Name,
                            Code = myLo.Code
                        });

                    input.Locations = mySimpleLocations;
                }

                reDto = await _exporter.ExportToTallyOutput(input, this, ttList, ptList);
            }

            return reDto;
        }

        public async Task<FileDto> GetFranchiseExcel(GetTicketInput input)
        {
            FileDto reDto = null;
            var setting = await _tenantSettingsService.GetAllSettings();
            input.DatetimeFormat = setting.Connect.DateTimeFormat;
            input.DateFormat = setting.Connect.SimpleDateFormat;
            input.OutputType = "EXPORT";
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var ttNames = await _ttService.GetAllItems();
                var ttList = ttNames.Items.Select(a => a.Name).ToList();
                var ptNames = await _ptService.GetAllItems();
                var ptList = ptNames.Items.Select(a => a.Name).ToList();

                var location = await _locService.GetLocationsForLoginUser();
                var lList =
                    location.Items.Select(
                            a => new SharingPercentageDto { Id = a.Id, Percentage = a.SharingPercentage, Name = a.Name })
                        .ToList();
                var menuItems = _poRepository.GetAll().Select(a => new MenuItemListDto
                {
                    Id = a.Id,
                    Name = a.MenuItem.Name,
                    CategoryName = a.MenuItem.Category.Name
                }).ToList();

                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.FRANCHISE,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });

                    if (backGroundId > 0)
                    {
                        var ticketInput = new GetTicketInput();
                        ticketInput = input;
                        ticketInput.UserId = AbpSession.UserId.Value;
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.FRANCHISE,
                            TicketInput = ticketInput,
                            TTList = ttList,
                            PTList = ptList,
                            LList = lList,
                            MenuItemList = menuItems,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                    }
                }
                else
                {
                    reDto = await _exporter.ExportFranchise(input, this, ttList, ptList, lList, menuItems);
                    return reDto;
                }
            }

            return null;
        }

        public async Task<FileDto> GetAllToExcel(GetTicketInput input)
        {
            input.OutputType = "EXPORT";
            input.Portrait = false;
            var setting = await _tenantSettingsService.GetAllSettings();
            input.DatetimeFormat = setting.Connect.DateTimeFormat;
            input.DateFormat = setting.Connect.SimpleDateFormat;
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                var ttNames = new List<string>();
                var ptNames = new List<string>();
                var tagnames = new List<string>();

                using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                {
                    var ttList = await _ttService.GetAllItems();
                    ttNames = ttList.Items.Select(a => a.Name).ToList();

                    var ptList = await _ptService.GetAllItems();
                    ptNames = ptList.Items.Select(a => a.Name).ToList();
                    tagnames = _tagGroupRepo.GetAll().Select(a => a.Name).ToList();
                }

                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.TICKETSALES,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });

                    if (backGroundId > 0)
                    {
                        var tInput = new TicketReportInput
                        {
                            TicketInput = input,
                            TicketTags = tagnames,
                            PaymentTypes = ptNames,
                            TransactionTypes = ttNames
                        };
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.TICKETSALES,
                            TicketReportInput = tInput,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                    }
                }
                else
                {
                    var output = await _exporter.ExportTickets(input, this, ttNames, ptNames, tagnames,
                        AbpSession.TenantId.Value);
                    return output;
                }
            }

            return null;
        }

        public async Task<FileDto> GetTicketSyncToExcel(GetTicketSyncInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                input.MaxResultCount = 10000;
                var setting = await _tenantSettingsService.GetAllSettings();
                input.DatetimeFormat = setting.Connect.DateTimeFormat;
                input.DateFormat = setting.Connect.SimpleDateFormat;
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.TICKETSYNCS,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });

                    if (backGroundId > 0)
                    {
                        var tInput = new TicketSyncReportInput
                        {
                            TicketSyncInput = input
                        };
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.TICKETSYNCS,
                            TicketSyncReportInput = tInput,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                    }
                }
                else
                {
                    var output = await _exporter.ExportTicketSyncs(input, this);
                    return output;
                }
            }

            return null;
        }



        public async Task<FileDto> GetRefundDetaiTicketsToExcel(GetTicketInput input, bool onlystatus = false)
        {
            input.OutputType = "EXPORT";
            input.Portrait = false;
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                var setting = await _tenantSettingsService.GetAllSettings();
                input.DatetimeFormat = setting.Connect.DateTimeFormat;
                input.DateFormat = setting.Connect.SimpleDateFormat;
                var ttNames = new List<string>();
                var ptNames = new List<string>();
                var tagnames = new List<string>();

                using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                {
                    var ttList = await _ttService.GetAllItems();
                    ttNames = ttList.Items.Select(a => a.Name).ToList();

                    var ptList = await _ptService.GetAllItems();
                    ptNames = ptList.Items.Select(a => a.Name).ToList();
                    tagnames = _tagGroupRepo.GetAll().Select(a => a.Name).ToList();
                }

                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.REFUNDTICKETDETAIL,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });

                    if (backGroundId > 0)
                    {
                        var tInput = new TicketReportInput
                        {
                            TicketInput = input,
                            TicketTags = tagnames,
                            PaymentTypes = ptNames,
                            TransactionTypes = ttNames
                        };
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.REFUNDTICKETDETAIL,
                            TicketReportInput = tInput,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                    }
                }
                else
                {
                    var output = await _exporter.ExportRefundTicketDetail(input, this);
                    return output;
                }
            }

            return null;
        }


        public async Task<FileDto> GetTopOrBottomItemSalesToExcel(GetItemInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.TOPORBOTTOMITEMSALES,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.TOPORBOTTOMITEMSALES,
                            ItemInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                }
                else
                {
                    return await GetTopOrBottomItemSalesToExcelInBackground(input);
                }
            }

            return null;
        }

        public async Task<FileDto> GetTopOrBottomItemSalesToExcelInBackground(GetItemInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                var dtos = await GetTopOrBottomItemSales(input);
                return _exporter.ExportTopOrBottomItemSales(input, dtos.MenuList.Items.ToList());
            }
        }


        public async Task<FileDto> GetItemTagSalesToExel(GetItemInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.ITEMTAGSALES,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.ITEMTAGSALES,
                            ItemInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                }
                else
                {
                    return await _exporter.ExportToFileItemTagSales(input, this);
                }
            }

            return null;
        }
        public async Task<FileDto> GetItemTagSalesDetailToExel(GetItemInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                var setting = await _tenantSettingsService.GetAllSettings();
                input.DatetimeFormat = setting.Connect.DateTimeFormat;
                input.DateFormat = setting.Connect.SimpleDateFormat;
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.ITEMTAGSALESDETAIL,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.ITEMTAGSALESDETAIL,
                            ItemInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                }
                else
                {
                    return await _exporter.ExportToFileItemTagSalesDetail(input, this);
                }
            }

            return null;
        }
        #endregion Export

        #region Schedule

        public async Task<PagedResultDto<SchelduleReportOutput>> GetScheduleReport(GetItemInput input)
        {
            var returnOutput = new List<SchelduleReportOutput>();

            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                var schedules = await GetScheduleSetting(input.Location);
                var orders = GetAllOrders(input);

                var output = from c in orders
                             group c by new { c.MenuItemId }
                    into g
                             select new { g.Key.MenuItemId, Items = g };

                foreach (var dto in output)
                {
                    var firObj = dto.Items.First();

                    if (firObj != null)
                        try
                        {
                            var scheduleReportDto = new SchelduleReportOutput
                            {
                                MenuItemId = firObj.MenuItemId,
                                MenuItemName = firObj.MenuItemName
                            };

                            scheduleReportDto.ScheduleReportDatas = schedules.Select(s =>
                            {
                                var data = new ScheduleReportData
                                {
                                    ScheduleName = s.ScheduleName,
                                    FromTime = s.FromTime,
                                    ToTime = s.ToTime
                                };

                                var ticketBySchedule = dto.Items.Where(i =>
                                    i.Ticket.LastPaymentTime.TimeOfDay >= s.FromTime &&
                                    i.Ticket.LastPaymentTime.TimeOfDay < s.ToTime);
                                data.Quantity = ticketBySchedule.Sum(a => a.Quantity);
                                data.Price = data.Quantity == 0
                                    ? 0M
                                    : ticketBySchedule.Sum(a => a.Quantity * a.Price) / data.Quantity;
                                return data;
                            }).ToList();

                            returnOutput.Add(scheduleReportDto);
                        }
                        catch (Exception exception)
                        {
                            var mess = exception.Message;
                        }
                }

                returnOutput = returnOutput.OrderBy(e => e.MenuItemName).ToList();
            }

            if (!input.Sorting.Equals("Id"))
                returnOutput = returnOutput.OrderBy(input.Sorting).ToList();

            // var outputDtosNew = outputDtos.GroupBy(c => c.Hour).ToList();

            var outputPageDtos = returnOutput;
            if (input.IsSummary)
                outputPageDtos = returnOutput.Skip(input.SkipCount)
                    .Take(input.MaxResultCount)
                    .ToList();

            return new PagedResultDto<SchelduleReportOutput>(returnOutput.Count(), outputPageDtos);
        }

        public async Task<FileDto> GetScheduleReportExcel(GetItemInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.SCHEDULEREPORT,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });

                    if (backGroundId > 0)
                    {
                        input.LocationGroup.UserId = AbpSession.UserId.Value;

                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.SCHEDULEREPORT,
                            ItemInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                    }
                }
                else
                {
                    return await _exporter.ExportScheduleReportToFile(input, this);
                }
            }

            return null;
        }

        public async Task<FileDto> GetLocationScheduleReportExcel(GetItemInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.SCHEDULEREPORT,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });

                    if (backGroundId > 0)
                    {
                        input.LocationGroup.UserId = AbpSession.UserId.Value;

                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.SCHEDULEREPORT,
                            ItemInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                    }
                }
                else
                {
                    return await _exporter.ExportScheduleReportToFile(input, this);
                }
            }

            return null;
        }


        public async Task<List<ScheduleReportData>> GetScheduleSetting(int? locationId = null)
        {
            var schedules = new List<ScheduleSetting>();
            if (locationId.HasValue && locationId > 0)
            {
                schedules = await _locService.GetLocationSchedulesByLocation(locationId.Value);
            }
            else
            {
                var scheduleSetting = await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.Schedules);
                schedules = JsonConvert.DeserializeObject<List<ScheduleSetting>>(scheduleSetting);
            }

            var listScheduleReport = schedules.Select(e =>
            {
                var dto = new ScheduleReportData
                {
                    FromTime = new TimeSpan(e.StartHour, e.StartMinute, 0),
                    ToTime = new TimeSpan(e.EndHour, e.EndMinute, 0)
                };

                dto.ScheduleName = $@"{e.Name} ({dto.FromTime.ToString(@"hh\:mm")} - {dto.ToTime.ToString(@"hh\:mm")})";
                return dto;
            }).ToList();

            return listScheduleReport;
        }

        #endregion Schedule
    }
}