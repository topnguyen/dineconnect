﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.Threading;
using Abp.Timing;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Job;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace DinePlan.DineConnect.Connect.Ticket.Implementation
{
    public class PostAppService : DineConnectAppServiceBase, IPostAppService
    {
        private readonly ISyncAppService _syncAppService;
        private readonly IRepository<PostData> _postRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ILocationAppService _locationService;
        private readonly ITicketAppService _ticketAppService;
        private readonly IRepository<Transaction.Ticket> _ticketRepo;
        public PostAppService(IRepository<PostData> postRepository
            , IUnitOfWorkManager unitOfWorkManager
            , ILocationAppService locationService
            , ITicketAppService ticketAppService
            , IRepository<Transaction.Ticket> ticketRepo
            , ISyncAppService syncAppService)
        {
            _postRepository = postRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _locationService = locationService;
            _ticketAppService = ticketAppService;
            _ticketRepo = ticketRepo;
            _syncAppService = syncAppService;
        }
        public async Task<PostOutput> PostContent(PostInput input)
        {

            PostOutput output = new PostOutput();
            try
            {
                var postItem = _postRepository.FirstOrDefault(s => s.TicketId == input.TicketId);
                if (postItem == null)
                {
                    output.SyncId = await _postRepository.InsertAndGetIdAsync(new PostData()
                    {
                        LocationId = input.LocationId,
                        //TenantId = input.TenantId,
                        Contents = input.Contents,
                        ContentType = input.ContentType,
                        TicketId = input.TicketId,
                        Synced = false,
                        SyncedId = 0,
                        ReProcessCount = 0,
                        RePass = 0,
                        ReFail = 0
                    }) ;                  
                }
                else
                {
                    if (!string.IsNullOrEmpty(input.Contents) && !string.IsNullOrEmpty(postItem.Contents))
                    {
                        var myTicket = JsonConvert.DeserializeObject<CreateOrUpdateTicketInput>(postItem.Contents);
                        var dataInput = JsonConvert.DeserializeObject<CreateOrUpdateTicketInput>(input.Contents);
                        if (myTicket.Ticket.LastPaymentTime != dataInput.Ticket.LastPaymentTime)
                        {
                            postItem.Contents = input.Contents;
                            await _postRepository.UpdateAsync(postItem);
                        }
                    }
                    output.SyncId = postItem.Id;
                }
            }
            catch (Exception e)
            {
                output.ErrorDesc = e.Message;
            }

            return output;
        }

        public async Task<PagedResultOutput<PostDataOutput>> PullContents(PostDataInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.MustHaveTenant))
            {
                var locations = GetLocations(_locationService, input);

                var allItems = _postRepository.GetAll().Include(t => t.Location).Where(i => (input.Processed == null || i.Processed == input.Processed)
                              && (locations.Count == 0 || locations.Contains(i.LocationId))
                              && i.ContentType == input.ContentType
                              && i.TenantId == input.TenantId);

                var sortMenuItems = await allItems.ToListAsync();
                var allListPostDataOutputs = sortMenuItems.MapTo<List<PostDataOutput>>();
                var allListPostDataOutputs2 = allListPostDataOutputs.Where(s =>
                (s.TicketItem.Ticket.TicketCreatedTime >= input.StartDate)
                && (s.TicketItem.Ticket.TicketCreatedTime <= input.EndDate)
                && (s.TicketItem.Ticket.TicketCreatedTime.Hour * 60 + s.TicketItem.Ticket.TicketCreatedTime.Minute) >= input.Start_Number 
                && (s.TicketItem.Ticket.TicketCreatedTime.Hour * 60 + s.TicketItem.Ticket.TicketCreatedTime.Minute) <= input.End_Number);
                var dataAsQueryable = allListPostDataOutputs2.AsQueryable();
                if (!string.IsNullOrEmpty(input.DynamicFilter))
                {
                    var jsonSerializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver =
                            new CamelCasePropertyNamesContractResolver()
                    };
                    var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                    if (filRule?.Rules != null)
                    {
                        try
                        {

                            foreach (var rule in filRule?.Rules.ToList())
                            {
                                if (rule.Id == "TicketItem.Ticket.TicketCreatedTime" && rule.Operator == "equal")
                                {
                                    DateTime result = DateTime.ParseExact(rule.Value.Trim(), "yyyy-MM-dd", CultureInfo.InvariantCulture);
                                    dataAsQueryable = dataAsQueryable.Where(s => s.TicketItem.Ticket.TicketCreatedTime.ToString("yyyy-MM-dd") == result.ToString("yyyy-MM-dd"));
                                    filRule?.Rules.Remove(rule);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            throw e;
                        }
                    }
                    if (filRule?.Rules != null)
                    {
                        dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                    }
                }
                var allItemCount2 = dataAsQueryable.Count();
                var dataOuput = dataAsQueryable.OrderBy(input.Sorting).AsQueryable()
                    .PageBy(input)
                    .ToList();
                return new PagedResultOutput<PostDataOutput>(
                    allItemCount2,
                    dataOuput
                );
            }
        }

        public async Task<ApiPostDataOutput> ApiPostData(ApiLocationInput locationInput)
        {
            var output = new ApiPostDataOutput();
            var postdata = new List<ApiPostData>();
            var postDatas = await _postRepository.GetAllListAsync(a => a.TenantId == locationInput.TenantId
                            && a.LocationId == locationInput.LocationId
                            && a.Processed == true
                            && a.Synced == false
                            && a.SyncedId != 0);
            foreach (var post in postDatas)
            {
                output.PostData.Add(new ApiPostData
                {
                    SyncId = post.SyncedId,
                    TicketId = post.TicketId
                });
            }
            return output;
        }
        public async Task UpdatePostData(ApiUpdatePostData input)
        {
            foreach (var post in input.TicketId)
            {
                var findPost = await _postRepository.FirstOrDefaultAsync(s => s.TicketId == post);
                findPost.Synced = true;
                await _postRepository.UpdateAsync(findPost);
            }
        }
        public PostDataOutput PullContent(int id)
        {
            var item = _postRepository
                   .GetAll().FirstOrDefault(t => t.Id == id);
            if (item != null)
            {
                return item.MapTo<PostDataOutput>();
            }
            return new PostDataOutput();
        }

        public async Task UpdateContents(PostDataOutput input)
        {
            if (input.Id != 0)
            {
                var findTicket = _postRepository.Get(input.Id);
                findTicket.Contents = input.Contents;
                await _postRepository.UpdateAsync(findTicket);
            }
        }

        [UnitOfWork]
        public void SyncTicket(int postdataId)
        {
            var myRe = _postRepository.Get(postdataId);
            try
            {
                var output = AsyncHelper.RunSync(() =>
                     _ticketAppService.SyncTicket(new PostInput()
                     {
                         ContentType = Convert.ToInt32(PostContentType.Ticket),
                         Contents = myRe.Contents
                     }));

                if (output > 0)
                {
                    myRe.ResultReProcess = true;
                    myRe.Processed = true;
                    myRe.SyncedId = output;
                    myRe.RePass += 1;
                    _syncAppService.UpdateSync(SyncConsts.POSTDATA);
                }
                else
                {
                    myRe.ResultReProcess = false;
                    myRe.ReFail += 1;
                    myRe.Processed = false;
                }
                myRe.ReProcessCount += 1;
            }
            catch (Exception ex)
            {
                myRe.ReProcessCount += 1;
                myRe.ResultReProcess = false;
                myRe.ReFail += 1;
                myRe.Processed = false;
                myRe.AddErrorContent(
                    new ErrorValue()
                    {
                        Contents = ex.Message,
                        ErrorDate = Clock.Now
                    }, true);
            }
        }        
    }
}
