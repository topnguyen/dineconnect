﻿using Abp.Configuration;
using Abp.Timing;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Ticket.Central.Dto;
using DinePlan.DineConnect.Connect.WorkPeriod.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using OpenHtmlToPdf;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.Central
{
    public class CentralReportExporter : FileExporterBase, ICentralReportExporter
    {
        private readonly SettingManager _settingManager;
        private readonly ILocationManager _locManager;
        public CentralReportExporter(SettingManager settingManager, ILocationManager locationManager)
        {
            _settingManager = settingManager;
            _roundDecimals = _settingManager.GetSettingValue<int>(AppSettings.ConnectSettings.Decimals);
            _locManager = locationManager;
        }

        #region Public Methods

        public async Task<FileDto> CentralDailyExcel(CentralExportInput input, CentralDayReportData centralDay)
        {
            var file = new FileDto(
                centralDay.LocationDetails.Code + "-Day-" + input.DateReport.ToString(_simpleDateFormat) + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Daily Sales"));
                sheet.OutLineApplyStyle = true;
                sheet.Column(3).Style.Numberformat.Format = "#,##0.00";
                sheet.Column(5).Style.Numberformat.Format = "#,##0.00";
                sheet.Column(6).Style.Numberformat.Format = "#,##0.00";
                sheet.Column(11).Style.Numberformat.Format = "#,##0.00";
                sheet.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                sheet.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                sheet.Column(6).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                sheet.Column(11).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                for (var i = 1; i <= 12; i++)
                {
                    sheet.Column(i).AutoFit();
                    sheet.Column(i).Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    sheet.Column(i).Style.Border.Bottom.Color.SetColor(Color.White);
                    sheet.Column(i).Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    sheet.Column(i).Style.Border.Left.Color.SetColor(Color.White);
                }

                var row = AddReportHeader(sheet, input, centralDay);
                row = CreateSummaryRow(sheet, row, centralDay);
                row++;
                if (centralDay.Departments != null)
                    row = CreateDepartmentSumaryRow(sheet, row, centralDay.Departments);
                if (centralDay.SalesGroups != null)
                    row = CreateGroupSummaryRow(sheet, row, centralDay.SalesGroups);
                if (centralDay.SalesCategories != null)
                    row = CreateCategorySummaryRow(sheet, row, centralDay.SalesCategories);
                if (centralDay.PeriodData != null)
                    row = CreateSlotPeriodRevenueRow(sheet, row, centralDay.PeriodData);
                if (centralDay.DiscountData != null)
                    row = CreateDiscountSummaryRow(sheet, row, centralDay.DiscountData);
                if (centralDay.WorkTimePaTable != null)
                    row = CreateTenderSummaryRow(sheet, row, centralDay.WorkTimePaTable.details);

                CreateNoCollectionSummaryRow(sheet, row, new List<object>());

                for (var i = 1; i <= 12; i++) sheet.Column(i).AutoFit();

                Save(excelPackage, file);
                return file;
            }
        }

        public async Task<FileDto> CentralDayExport(CentralExportInput input, ICentralReportAppService centralAppService)
        {
            var file = new FileDto("Day-" + DateTime.Now.ToString(_simpleDateFormat),
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            var centralDay = await centralAppService.GetCentralDayReport(input.WorkPeriodId);
            switch (input.ExportOutputType)
            {
                case ExportType.HTML:
                    WriteHtml(file, centralDay, input, TypeCentralExport.CentralDay, null, new List<string>());
                    break;

                case ExportType.PDF:
                    {
                        WriteHtml(file, centralDay, input, TypeCentralExport.CentralDay, null, new List<string>());
                        // Convert to pdf
                        var filePath = Path.Combine(Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken));
                        if (File.Exists(filePath))
                        {
                            var allContent = File.ReadAllText(filePath);
                            if (!string.IsNullOrEmpty(allContent))
                            {
                                var pdf = Pdf.From(allContent).OfSize(PaperSize.A4);

                                var output = pdf.Content(filePath, AppFolders.PdfExeFile);
                                file.FileName = Path.GetFileNameWithoutExtension(file.FileName) + ".pdf";
                                file.FileType = MimeTypeNames.ApplicationPdf;

                                var myPath = Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken);
                                File.WriteAllBytes(myPath, output);
                            }
                        }

                        break;
                    }

                case ExportType.Excel:
                    return await CentralDailyExcel(input, centralDay);

                default:
                    WriteHtml(file, centralDay, input, TypeCentralExport.CentralDay, null, null);
                    break;
            }

            return file;
        }

        private void DailyReconcilePlant(ExcelWorksheet sheet, DailyReconciliationReportOutput outPut, ref int rowCount, WorkPeriodSummaryInput input)
        {
            var colCount = 1;
            var headers = new List<string>
                {
                    L("Tender"),
                        L("PortionPercent"),
                        L("System"),
                        L("Actual"),
                        L("Diff")
                };
            if (outPut.Data != null && outPut.Data.WorkShiftDailyReconciliationList != null && outPut.Data.WorkShiftDailyReconciliationList.Count > 0)
            {
                AddHeader(sheet, rowCount++, 1, L("Date") + " : " + outPut.WorkPeriodInfo.StartTime.ToString(input.DateFormat));
            }

            if (outPut.AllWorkShiftData != null && outPut.AllWorkShiftData.Count > 0)
            {
                foreach (var lst in outPut.AllWorkShiftData)
                {
                    if (lst.WorkShiftDailyReconciliationList != null && lst.WorkShiftDailyReconciliationList.Count > 0)
                    {
                        AddHeaderWithMergeWithColumnCountColor(sheet, rowCount++, colCount, 4, lst.WorkShiftTiming, Color.Green, Color.White);
                        AddHeaderWithColor(sheet, rowCount++, Color.Gray, Color.White, headers.ToArray());
                        foreach (var sublst in lst.WorkShiftDailyReconciliationList)
                        {
                            colCount = 1;
                            sheet.Cells[rowCount, colCount++].Value = sublst.PaymentType;
                            sheet.Cells[rowCount, colCount++].Value = sublst.Portion + "%";
                            sheet.Cells[rowCount, colCount++].Value = GetDecimalFormat(sublst.SystemAmount);
                            sheet.Cells[rowCount, colCount++].Value = GetDecimalFormat(sublst.ActualAmount);
                            sheet.Cells[rowCount, colCount++].Value = GetDecimalFormat(sublst.Difference);
                            for (var colInRow = 2; colInRow <= 5; colInRow++)
                                sheet.Cells[rowCount, colInRow].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                            rowCount++;
                        }
                        colCount = 1;
                        AddHeaderWithColor(sheet, rowCount, colCount++, Color.Gray, Color.White, lst.ShiftBasedTotalDailyReconciliation.PaymentType);
                        AddHeaderWithColor(sheet, rowCount, colCount++, Color.Gray, Color.White, lst.ShiftBasedTotalDailyReconciliation.Portion.ToString() + "%");
                        AddHeaderWithColor(sheet, rowCount, colCount++, Color.Gray, Color.White, GetDecimalFormat(lst.ShiftBasedTotalDailyReconciliation.SystemAmount));
                        AddHeaderWithColor(sheet, rowCount, colCount++, Color.Gray, Color.White, GetDecimalFormat(lst.ShiftBasedTotalDailyReconciliation.ActualAmount));
                        AddHeaderWithColor(sheet, rowCount, colCount++, Color.Gray, Color.White, GetDecimalFormat(lst.ShiftBasedTotalDailyReconciliation.Difference));
                        for (var colInRow = 2; colInRow <= 5; colInRow++)
                            sheet.Cells[rowCount, colInRow].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        rowCount = rowCount + 2;
                        colCount = 1;
                    }
                }



            }
            if (outPut.Data != null && outPut.Data.WorkShiftDailyReconciliationList != null && outPut.Data.WorkShiftDailyReconciliationList.Count > 0)
            {
                sheet.Row(rowCount).Height = 30;
                sheet.Cells[rowCount, colCount].Style.WrapText = true;
                AddHeaderWithMergeWithColumnCountColor(sheet, rowCount++, colCount, 4, outPut.Data.WorkShiftTiming, Color.Green, Color.White);
                AddHeaderWithColor(sheet, rowCount++, Color.Gray, Color.White, headers.ToArray());
                foreach (var lst in outPut.Data.WorkShiftDailyReconciliationList)
                {
                    colCount = 1;
                    sheet.Cells[rowCount, colCount++].Value = lst.PaymentType;
                    sheet.Cells[rowCount, colCount++].Value = lst.Portion + "%";
                    sheet.Cells[rowCount, colCount++].Value = GetDecimalFormat(lst.SystemAmount);
                    sheet.Cells[rowCount, colCount++].Value = GetDecimalFormat(lst.ActualAmount);
                    sheet.Cells[rowCount, colCount++].Value = GetDecimalFormat(lst.Difference);
                    for (var colInRow = 2; colInRow <= 5; colInRow++)
                        sheet.Cells[rowCount, colInRow].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                    rowCount++;
                }
                colCount = 1;
                AddHeaderWithColor(sheet, rowCount, colCount++, Color.Gray, Color.White, outPut.Data.ShiftBasedTotalDailyReconciliation.PaymentType);
                AddHeaderWithColor(sheet, rowCount, colCount++, Color.Gray, Color.White, outPut.Data.ShiftBasedTotalDailyReconciliation.Portion.ToString() + "%");
                AddHeaderWithColor(sheet, rowCount, colCount++, Color.Gray, Color.White, GetDecimalFormat(outPut.Data.ShiftBasedTotalDailyReconciliation.SystemAmount));
                AddHeaderWithColor(sheet, rowCount, colCount++, Color.Gray, Color.White, GetDecimalFormat(outPut.Data.ShiftBasedTotalDailyReconciliation.ActualAmount));
                AddHeaderWithColor(sheet, rowCount, colCount++, Color.Gray, Color.White, GetDecimalFormat(outPut.Data.ShiftBasedTotalDailyReconciliation.Difference));
                for (var colInRow = 2; colInRow <= 5; colInRow++)
                    sheet.Cells[rowCount, colInRow].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                colCount = 1;
                rowCount++;

                colCount = 1;
                AddHeaderWithColor(sheet, rowCount, colCount++, Color.Gray, Color.White, L("Number") + L("Of") + L("Bill"));
                sheet.Cells[rowCount, colCount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                AddHeaderWithColor(sheet, rowCount, colCount, Color.Gray, Color.White, outPut.NumberOfBill.ToString());
                sheet.Cells[rowCount, colCount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                rowCount++;

                colCount = 1;
                AddHeaderWithColor(sheet, rowCount, colCount++, Color.Gray, Color.White, L("AvgPerBill"));
                sheet.Cells[rowCount, colCount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                AddHeaderWithColor(sheet, rowCount, colCount, Color.Gray, Color.White, GetDecimalFormat(outPut.AvgPerBill));
                sheet.Cells[rowCount, colCount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                rowCount++;
            }
            rowCount++;
        }

        public async Task<FileDto> DailyReconciliationExportAll(WorkPeriodSummaryInput input, ICentralReportAppService centralAppService)
        {
            var file = new FileDto("Day-" + input.DateReport.ToString("yyyy-MM-dd") + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("DailyReconciliation"));
                sheet.OutLineApplyStyle = true;
                var data = await centralAppService.GetDailyReconciliationAll(input);

                var rowCount = 1;

                AddHeader(sheet, rowCount++, L("DailyReconciliation") + " : " + input.DateReport.ToString(input.DateFormat));

                foreach (var outputs in data)
                {
                    if (outputs.Details.Count == 0) continue;
                    rowCount++;
                    AddHeader(sheet, rowCount++, 1, L("PlantCode") + " : " + outputs.LocationCode);
                    AddHeader(sheet, rowCount++, 1, L("PlantName") + " : " + outputs.LocationName);
                    rowCount++;
                    foreach (var output in outputs.Details)
                    {
                        DailyReconcilePlant(sheet, output, ref rowCount , input);
                    }

                    AddSummary(sheet, outputs.Summary, ref rowCount);
                }

                for (var i = 1; i <= 9; i++)
                {
                    sheet.Column(i).AutoFit();
                }

                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

        private void AddSummary(ExcelWorksheet sheet, DailyReconciliationReportOutput outPut, ref int rowCount)
        {
            rowCount++;
            var headers = new List<string>
                {
                    L("Tender"),
                        L("PortionPercent"),
                        L("System"),
                        L("Actual"),
                        L("Diff") };

            var colCount = 1;
            AddHeaderWithColor(sheet, rowCount++, Color.Gray, Color.White, headers.ToArray());

            foreach (var lst in outPut.Data.WorkShiftDailyReconciliationList)
            {
                colCount = 1;
                sheet.Cells[rowCount, colCount++].Value = lst.PaymentType;
                sheet.Cells[rowCount, colCount++].Value = lst.Portion + "%";
                sheet.Cells[rowCount, colCount++].Value = GetDecimalFormat(lst.SystemAmount);
                sheet.Cells[rowCount, colCount++].Value = GetDecimalFormat(lst.ActualAmount);
                sheet.Cells[rowCount, colCount++].Value = GetDecimalFormat(lst.Difference);
                for (var colInRow = 2; colInRow <= 5; colInRow++)
                    sheet.Cells[rowCount, colInRow].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                rowCount++;
            }
            colCount = 1;
            AddHeaderWithColor(sheet, rowCount, colCount++, Color.Gray, Color.White, outPut.Data.ShiftBasedTotalDailyReconciliation.PaymentType);
            AddHeaderWithColor(sheet, rowCount, colCount++, Color.Gray, Color.White, outPut.Data.ShiftBasedTotalDailyReconciliation.Portion.ToString() + "%");
            AddHeaderWithColor(sheet, rowCount, colCount++, Color.Gray, Color.White, GetDecimalFormat(outPut.Data.ShiftBasedTotalDailyReconciliation.SystemAmount));
            AddHeaderWithColor(sheet, rowCount, colCount++, Color.Gray, Color.White, GetDecimalFormat(outPut.Data.ShiftBasedTotalDailyReconciliation.ActualAmount));
            AddHeaderWithColor(sheet, rowCount, colCount++, Color.Gray, Color.White, GetDecimalFormat(outPut.Data.ShiftBasedTotalDailyReconciliation.Difference));

            for (var colInRow = 2; colInRow <= 5; colInRow++)
                sheet.Cells[rowCount, colInRow].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            rowCount++;

            colCount = 1;
            AddHeaderWithColor(sheet, rowCount, colCount++, Color.Gray, Color.White, L("Number") + L("Of") + L("Bill"));
            sheet.Cells[rowCount, colCount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            AddHeaderWithColor(sheet, rowCount, colCount, Color.Gray, Color.White, outPut.NumberOfBill.ToString());
            sheet.Cells[rowCount, colCount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            rowCount++;
            colCount = 1;
            AddHeaderWithColor(sheet, rowCount, colCount++, Color.Gray, Color.White, L("AvgPerBill"));
            sheet.Cells[rowCount, colCount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            AddHeaderWithColor(sheet, rowCount, colCount, Color.Gray, Color.White, GetDecimalFormat(outPut.AvgPerBill));
            sheet.Cells[rowCount, colCount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
            rowCount++;
        }

        public async Task<FileDto> CentralShiftDayExport(CentralShiftExportInput input,
            ICentralReportAppService centralAppService)
        {
            var file = new FileDto("Shift-" + DateTime.Now.ToString(_simpleDateFormat),
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            var centralShiftDay = await centralAppService.GetCentralShiftReport(input.CentralShiftReportInput);

            var workPeriod = input.WorkPeriod.WorkShifts
                .SingleOrDefault(i => i.UniqueId == input.CentralShiftReportInput.workShiftId);

            var workShiftTime = "";
            if (workPeriod != null)
                workShiftTime += workPeriod.StartDate.ToString("g") + " - " + workPeriod.EndDate.ToString("g");
            switch (input.ExportOutputType)
            {
                case ExportType.HTML:
                    WriteHtml(file, centralShiftDay, input, TypeCentralExport.CentralShift, workShiftTime, null);
                    break;

                case ExportType.PDF:
                    {
                        WriteHtml(file, centralShiftDay, input, TypeCentralExport.CentralShift, workShiftTime, null);
                        // Convert to pdf
                        var filePath = Path.Combine(Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken));
                        if (File.Exists(filePath))
                        {
                            var allContent = File.ReadAllText(filePath);
                            if (!string.IsNullOrEmpty(allContent))
                            {
                                var pdf = Pdf.From(allContent).OfSize(PaperSize.A4);

                                var output = pdf.Content(filePath, AppFolders.PdfExeFile);
                                file.FileName = Path.GetFileNameWithoutExtension(file.FileName) + ".pdf";
                                file.FileType = MimeTypeNames.ApplicationPdf;

                                var myPath = Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken);
                                File.WriteAllBytes(myPath, output);
                            }
                        }

                        break;
                    }

                case ExportType.Excel:
                    WriteHtml(file, centralShiftDay, input, TypeCentralExport.CentralShift, workShiftTime, null);
                    break;

                default:
                    WriteHtml(file, centralShiftDay, input, TypeCentralExport.CentralShift, workShiftTime, null);
                    break;
            }

            return file;
        }

        public async Task<FileDto> CentralTerminalDayExport(CentralTerminalExportInput input,
            ICentralReportAppService centralAppService)
        {
            var file = new FileDto("Terminal-" + DateTime.Now.ToString(_simpleDateFormat),
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            var centralTerminalDay = await centralAppService.GetCentralTerminalReport(input.CentralTerminalReportInput);

            switch (input.ExportOutputType)
            {
                case ExportType.HTML:
                    WriteHtml(file, centralTerminalDay, input, TypeCentralExport.CentralTerminal, null,
                        input.CentralTerminalReportInput.terminals);
                    break;

                case ExportType.PDF:
                    {
                        WriteHtml(file, centralTerminalDay, input, TypeCentralExport.CentralTerminal, null,
                            input.CentralTerminalReportInput.terminals);
                        // Convert to pdf
                        var filePath = Path.Combine(Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken));
                        if (File.Exists(filePath))
                        {
                            var allContent = File.ReadAllText(filePath);
                            if (!string.IsNullOrEmpty(allContent))
                            {
                                var pdf = Pdf.From(allContent).OfSize(PaperSize.A4);

                                var output = pdf.Content(filePath, AppFolders.PdfExeFile);
                                file.FileName = Path.GetFileNameWithoutExtension(file.FileName) + ".pdf";
                                file.FileType = MimeTypeNames.ApplicationPdf;

                                var myPath = Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken);
                                File.WriteAllBytes(myPath, output);
                            }
                        }

                        break;
                    }

                case ExportType.Excel:
                    WriteHtml(file, centralTerminalDay, input, TypeCentralExport.CentralTerminal, null,
                        input.CentralTerminalReportInput.terminals);
                    break;

                default:
                    WriteHtml(file, centralTerminalDay, input, TypeCentralExport.CentralTerminal, null,
                        input.CentralTerminalReportInput.terminals);
                    break;
            }

            return file;
        }

        #endregion Public Methods

        #region Private Methods

        private void WriteHtml(FileDto file, CentralDayReportData centralDay, IDateCentralExport input,
            TypeCentralExport type, string workShiftTime, List<string> terminals)
        {
            var strHtmlBuilder = new StringBuilder();

            var decimalConfig = GetNumberDecimalDisplay(_roundDecimals);

            var headerReport = @"";
            switch (type)
            {
                case TypeCentralExport.CentralDay:
                    headerReport += @"<h1>CentralDay Report View Model</h1>
                                      <h2> (Day: {date_report})</h2>";
                    break;

                case TypeCentralExport.CentralShift:
                    headerReport += @"<h1>CentralShift Report View Model</h1>
                                      <h2> (Work Shift: {shift})</h2>";

                    if (workShiftTime != null) headerReport = headerReport.Replace("{shift}", workShiftTime);
                    break;

                case TypeCentralExport.CentralTerminal:
                    headerReport += @"<h1>CentralTerminal Report View Model</h1>
                                      <h2> (Day: {date_report})</h2>
                                      <h2> (Terminal: {terminal})</h2>";
                    if (terminals != null)
                    {
                        var delimiter = ", ";
                        headerReport = headerReport.Replace("{terminal}", string.Join(delimiter, terminals));
                    }

                    break;
            }

            headerReport = headerReport.Replace("{date_report}", input.DateReport.ToShortDateString());

            // Sales Table
            var salesTable = @"<table>
        <tr class=""title__table"">
            <th colspan=""2""> SALES </th>
        </tr>
        <tr class=""table__highlight"">
            <th>Sub Total</th>
            <th class=""pull-right"">{sub_total}</th>
        </tr>
        <tr>
            <th>Tax</th>
            <th class=""pull-right"">{tax}</th>
        </tr>
        <tr>
            <th>Discount</th>
            <th class=""pull-right"">{discount}</th>
        </tr>
        <tr>
            <th>Total Sales</th>
            <th class=""pull-right"">{TotalSales}</th>
        </tr>

        <tr>
            <th>Ticket Count</th>
            <th class=""pull-right"">{TicketCount}</th>
        </tr>
        <tr>
            <th>Sales Average</th>
            <th class=""pull-right"">{sales_average}</th>
        </tr>
</table>";

            if (centralDay.TicketTypeReportData != null)
                salesTable = salesTable.Replace("{TotalSales}",
                        string.Format(decimalConfig, centralDay.TicketTypeReportData.TotalSales))
                    .Replace("{discount}",
                        centralDay.TicketTypeReportData.Transactions.ContainsKey("DISCOUNT")
                            ? string.Format(decimalConfig, centralDay.TicketTypeReportData.Transactions["DISCOUNT"])
                            : "0")
                    .Replace("{sub_total}", string.Format(decimalConfig, centralDay.TicketTypeReportData.SubTotal))
                    .Replace("{TicketCount}",
                        string.Format(decimalConfig, centralDay.TicketTypeReportData.TicketCount))
                    .Replace("{tax}",
                        string.Format(decimalConfig, centralDay.TicketTypeReportData.Tax))
                    .Replace("{sales_average}", string.Format(decimalConfig, centralDay.TicketTypeReportData.Average));

            // Department Table
            var department = @"<table>
                                <tr class=""title__table"">
                                    <th colspan=""3""> Department </th>
                                </tr>
                                <tr class=""table__header"">
                                    <th>Department</th>
                                    <th class=""pull-right"">TicketCount</th>
                                    <th class=""pull-right"">Total</th>
                                </tr>";
            foreach (var d in centralDay.Departments)
            {
                var value = @"<tr>
                                <th>{department}</th>
                                <th class=""pull-right"">{TicketCount}</th>
                                <th class=""pull-right"">{total}</th>
                              </tr>";

                value = value.Replace("{department}",
                        (string)d.GetType().GetProperty("depatName")?.GetValue(d, null))
                    .Replace("{TicketCount}", (string)d.GetType().GetProperty("count")?.GetValue(d, null))
                    .Replace("{total}",
                        string.Format(decimalConfig, d.GetType().GetProperty("amount")?.GetValue(d, null)));
                department += value;
            }

            department += @"<tr class=""table__highlight"">
                              <th colspan=""2"">TOTAL</th>
                              <th class=""pull-right"">{total_all}</th>
                          </tr>
                          </table>";
            var listDepart = centralDay.Departments.Select(c => new
            {
                Amount = (decimal)c.GetType().GetProperty("amount")?.GetValue(c, null)
            }).ToList();
            var totalDepart = listDepart.Sum(c => c.Amount);
            department = department.Replace("{total_all}", string.Format(decimalConfig, totalDepart));

            // Payment Table
            var paymentTable = @"<table>
                                <tr class=""title__table"">
                                    <th colspan=""3""> Payments </th>
                                </tr>
                                <tr class=""table__header"">
                                    <th>Payment Type</th>
                                    <th class=""pull-right"">Count</th>
                                    <th class=""pull-right"">Actual</th>
                                </tr>";
            foreach (var p in centralDay.PaymentTypeReportData.details)
            {
                var value = @"<tr>
                                <th>{payment_type}</th>
                                <th class=""pull-right"">{count}</th>
                                <th class=""pull-right"">{actual}</th>
                              </tr>";

                value = value.Replace("{payment_type}", p.paymentName)
                    .Replace("{count}", p.count.ToString())
                    .Replace("{actual}", string.Format(decimalConfig, p.actualSales));
                paymentTable += value;
            }

            paymentTable += @"<tr class=""table__highlight"">
                              <th colspan=""2"">TOTAL</th>
                              <th class=""pull-right"">{total_all}</th>
                          </tr>
                          </table>";

            paymentTable = paymentTable.Replace("{total_all}",
                string.Format(decimalConfig, centralDay.PaymentTypeReportData.details.Sum(c => c.actualSales)));

            // Cash table
            var cash = @"<table>
                                <tr class=""title__table"">
                                    <th colspan=""3""> Cash </th>
                                </tr>
                                <tr class=""table__header"">
                                    <th>Type</th>
                                    <th class=""pull-right"">Count</th>
                                    <th class=""pull-right"">Total</th>
                                </tr>
                                <tr>
                                    <th>Cash</th>
                                    <th></th>
                                    <th class=""pull-right"">{cash_total}</th>
                                </tr>
                                <tr>
                                    <th>Cash In</th>
                                    <th class=""pull-right"">{cash_in_count}</th>
                                    <th class=""pull-right"">{cash_in_total}</th>
                                </tr>
                                <tr>
                                    <th>Cash Out</th>
                                    <th class=""pull-right"">{cash_out_count}</th>
                                    <th class=""pull-right"">{cash_out_total}</th>
                                </tr>
                                <tr>
                                    <th>Cash In Drawer</th>
                                    <th class=""pull-right"">{cash_dra_count}</th>
                                    <th class=""pull-right"">{cash_dra_total}</th>
                                </tr>
                                </table>";

            cash = cash.Replace("{cash_total}", string.Format(decimalConfig, centralDay.CashReportData.cashAmount))
                .Replace("{cash_in_count}", string.Format(decimalConfig, centralDay.CashReportData.cashIn))
                .Replace("{cash_in_total}", string.Format(decimalConfig, centralDay.CashReportData.allIncomeAmount))
                .Replace("{cash_out_count}", string.Format(decimalConfig, centralDay.CashReportData.cashOut))
                .Replace("{cash_out_total}", string.Format(decimalConfig, centralDay.CashReportData.allExpAmount))
                .Replace("{cash_dra_count}", "0")
                .Replace("{cash_dra_total}", string.Format(decimalConfig, centralDay.CashReportData.cashInDrawer));

            //Work time
            var workTime = @"<table>
                                <tr class=""title__table"">
                                    <th colspan=""4""> Work Time </th>
                                </tr>
                                <tr class=""table__header"">
                                    <th>PaymentType</th>
                                    <th class=""pull-right"">Actual</th>
                                    <th class=""pull-right"">In Hand</th>
                                    <th class=""pull-right"">Difference</th>
                                </tr>";

            foreach (var w in centralDay.WorkTimePaTable.details)
            {
                var value = @"<tr>
                                <th>{payment_type}</th>
                                <th class=""pull-right"">{actual}</th>
                                <th class=""pull-right"">{in_hand}</th>
                                <th class=""pull-right"">{difference}</th>
                              </tr>";

                value = value.Replace("{payment_type}", w.paymentName)
                    .Replace("{actual}", string.Format(decimalConfig, w.actualTotal))
                    .Replace("{in_hand}", string.Format(decimalConfig, w.inSalesTotal))
                    .Replace("{difference}", string.Format(decimalConfig, w.differenceTotal));

                workTime += value;
            }

            workTime += @"</table>";

            // General Information
            var generalInfo = @"<table>
                                <tr class=""title__table"">
                                    <th colspan=""2""> General Information </th>
                                </tr>
                                <tr class=""table__highlight"">
                                    <th colspan=""2"">Items</th>
                                </tr>
                                <tr>
                                    <th>PaidItemsCount</th>
                                    <th class=""pull-right"">{item_count}</th>
                                </tr>
                                <tr>
                                    <th>PaidItemsTotal</th>
                                    <th class=""pull-right"">{item_total}</th>
                                </tr>
                                <tr>
                                    <th>ItemsCount</th>
                                    <th class=""pull-right"">{count}</th>
                                </tr>";
            generalInfo = generalInfo.Replace("{item_count}",
                    string.Format(decimalConfig, centralDay.ItemsData.totalPaidItems))
                .Replace("{item_total}", string.Format(decimalConfig, centralDay.ItemsData.totalPaidItemTotal))
                .Replace("{count}", string.Format(decimalConfig, centralDay.ItemsData.itemCount));

            generalInfo += @"<tr class=""table__highlight"">
                                <th colspan=""2"">Order</th>
                             </tr>
                             <tr>
                                <th>PaidOrdersCount</th>
                                <th class=""pull-right"">{order_count}</th>
                             </tr>
                             <tr>
                                <th>PaidOrdersTotal</th>
                                <th class=""pull-right"">{order_total}</th>
                             </tr>
                             <tr>
                                <th>OrderCount</th>
                                <th class=""pull-right"">{countOd}</th>
                             </tr>";
            generalInfo = generalInfo.Replace("{order_count}",
                    centralDay.OrdersReportData.totalPaidItemsorderCount.ToString())
                .Replace("{order_total}",
                    string.Format(decimalConfig, centralDay.OrdersReportData.totalPaidItemsorderValue))
                .Replace("{countOd}", centralDay.OrdersReportData.orderCount.ToString());

            if (centralDay.OrdersReportData.orderStateReports != null)
                foreach (var g in centralDay.OrdersReportData.orderStateReports)
                {
                    var value = @"<tr>
                                <th>{state_name}</th>
                                <th class=""pull-right"">{state_value}</th>
                              </tr>";

                    value = value.Replace("{state_name}", g.state + "(" + g.count + ")")
                        .Replace("{state_value}", string.Format(decimalConfig, g.amount));

                    generalInfo += value;
                }

            generalInfo += @"<tr class=""table__highlight"">
                                <th colspan=""2"">Tickets</th>
                             </tr>";
            if (centralDay.TicketsReportData.ticketStateReports != null)
                foreach (var ti in centralDay.TicketsReportData.ticketStateReports)
                {
                    var value = @"<tr>
                                <th>{ticket_state}</th>
                                <th class=""pull-right"">{ticket_amount}</th>
                              </tr>";

                    value = value.Replace("{ticket_state}", ti.state + "(" + ti.count + ")")
                        .Replace("{ticket_amount}", string.Format(decimalConfig, ti.amount));

                    generalInfo += value;
                }

            generalInfo += @" <tr class=""table__highlight"">
                                <th colspan=""2"">Tickets</th>
                             </tr>
                             <tr>
                                <th>Start Ticket Number</th>
                                <th class=""pull-right"">{start_number}</th>
                             </tr>
                             <tr>
                                <th>End Ticket Number</th>
                                <th class=""pull-right"">{end_number}</th>
                             </tr>
                             </table>";

            generalInfo = generalInfo.Replace("{start_number}",
                    centralDay.TicketsReportData.firstTicketNumber)
                .Replace("{end_number}", centralDay.TicketsReportData.lastTicketNumber);

            //Ticket Tags
            var ticketTag = @"<table>
                                <tr class=""title__table"">
                                    <th colspan=""2""> Ticket Tags </th>
                                </tr>";
            if (centralDay.TicketTagsReportData != null)
                foreach (var ttag in centralDay.TicketTagsReportData)
                {
                    ticketTag += @"<tr class=""table__highlight"">
                                    <th>{group_name}</th>
                               </tr>";
                    ticketTag = ticketTag.Replace("{group_name}", ttag.groupName);
                    if (ttag.isDecimal)
                    {
                        ticketTag += @"<tr>
                                    <th>Ticket Count</th>
                                    <th class=""pull-right"">{t_count}</th>
                                  </tr>
                                  <tr>
                                    <th>Rate</th>
                                    <th class=""pull-right"">{t_rate}</th>
                                  </tr>
                                  <tr>
                                    <th>Total</th>
                                    <th class=""pull-right"">{t_amount}</th>
                                  </tr>";
                        ticketTag = ticketTag.Replace("{t_count}", ttag.tCount.ToString())
                            .Replace("{t_rate}", string.Format(decimalConfig, ttag.rate))
                            .Replace("{t_amount}", string.Format(decimalConfig, ttag.amnt));
                    }

                    if (!ttag.isDecimal)
                    {
                        foreach (var subRow in ttag.subRows)
                        {
                            ticketTag += @"<tr>
                                        <th>{sr_tag_name}</th>
                                        <th class=""pull-right"">{sr_amount}</th>
                                    </tr>";
                            ticketTag = ticketTag.Replace("{sr_tag_name}", subRow.TagName)
                                .Replace("{sr_amount}", string.Format(decimalConfig, subRow.Amount));
                        }

                        ticketTag += @"<tr>
                                        <th>Total ({gr_name})</th>
                                        <th class=""pull-right"">{total_amount}</th>
                                    </tr>";
                        ticketTag = ticketTag.Replace("{gr_name}", ttag.groupName)
                            .Replace("{total_amount}", string.Format(decimalConfig, ttag.totalAmount));

                        if (ttag.isInterger)
                        {
                            ticketTag += @"<tr>
                                        <th>Total ({gr_name})</th>
                                        <th class=""pull-right"">{sum}</th>
                                    </tr>";
                            ticketTag = ticketTag.Replace("{gr_name}", ttag.groupName)
                                .Replace("{sum}", string.Format(decimalConfig, ttag.sum));
                        }

                        if (ttag.sum > 0)
                        {
                            ticketTag += @"<tr>
                                        <th>Avg/ ({gr_name})</th>
                                        <th class=""pull-right"">{average}</th>
                                    </tr>";
                            ticketTag = ticketTag.Replace("{gr_name}", ttag.groupName)
                                .Replace("{average}", string.Format(decimalConfig, ttag.average));
                        }
                    }
                }

            ticketTag += @"</table>";

            //User Sales
            var userSales = @" <table>
                                <tr class=""title__table"">
                                    <th colspan=""2""> User Sales </th>
                                </tr>";
            if (centralDay.OwnersReportData != null)
                foreach (var us in centralDay.OwnersReportData.details)
                {
                    var value = @"<tr>
                                <th>{user_name}</th>
                                <th class=""pull-right"">{user_amount}</th>
                              </tr>";

                    value = value.Replace("{user_name}", us.UserName)
                        .Replace("{user_amount}", string.Format(decimalConfig, us.Amount));

                    userSales += value;
                }

            userSales += @"<tr class=""table__highlight"">
                              <th>Total (Without Rounding)</th>
                              <th class=""pull-right"">{total}</th>
                          </tr>
                          </table>";
            userSales = userSales.Replace("{total}",
                string.Format(decimalConfig, centralDay.OwnersReportData?.totalOwnersAmount));

            //User Details
            var userDetails = "";

            if (centralDay.UsersIncomeReportData != null)
                foreach (var userIncome in centralDay.UsersIncomeReportData)
                {
                    userDetails += @"<table>
                                <tr class=""title__table"">
                                    <th colspan=""3""> Settled By {payment_user_name}</th>
                                </tr>";
                    userDetails = userDetails.Replace("{payment_user_name}", userIncome.paymentUserName);

                    foreach (var payment in userIncome.paymentTypes)
                    {
                        userDetails += @"<tr>
                                        <th>{payment_name}</th>
                                        <th class=""pull-right"">% {percent}</th>
                                        <th class=""pull-right"">{amount}</th>
                                    </tr>";
                        userDetails = userDetails.Replace("{payment_name}", payment.paymentName)
                            .Replace("{percent}", string.Format(decimalConfig, payment.percent))
                            .Replace("{amount}", string.Format(decimalConfig, payment.amount));
                    }

                    userDetails += @"<tr class=""table__highlight"">
                                    <th>TOTAL</th>
                                    <th class=""pull-right"">% {percent_total}</th>
                                    <th class=""pull-right"">{total}</th>
                                </tr>
                                </table>";

                    userDetails = userDetails.Replace("{percent_total}",
                            string.Format(decimalConfig, userIncome.paymentTypes.Sum(c => c.percent)))
                        .Replace("{total}",
                            string.Format(decimalConfig,
                                string.Format(decimalConfig, userIncome.paymentTypes.Sum(c => c.amount))));
                }

            // Create HTML index
            strHtmlBuilder.Append("<html>");
            strHtmlBuilder.Append("<head>");
            strHtmlBuilder.Append(@"<style>
                    table {
                        font-family: arial, sans-serif;
                        border-collapse: collapse;
                        width: 50%;
                        margin: auto;
                        margin-top: 10px;
                    }

                    td, th {
                        border: 1px solid #dddddd;
                        text-align: left;
                        padding: 8px;
                    }
                    .title__table {
                        background-color: #008000;
                        text-transform: uppercase;
                        color: #FFFFFF;
                    }

                    .title__table > th {
                        text-align: center;
                    }

                    .table__highlight {
                        background-color: #808080;
                        text-transform: uppercase;
                        color: #FFFFFF;
                        font-weight: bold;
                    }
                    .table__header {
                        background-color: #808080;
                        color: #FFFFFF;
                    }
                    .pull-right {
                        text-align: right;
                    }
                    h1, h2 {
                        text-align: center;
                    }
                    </style>");
            strHtmlBuilder.Append("</head>");
            strHtmlBuilder.Append("<body>");

            // Add Heading
            strHtmlBuilder.Append(headerReport);

            // Add Table
            strHtmlBuilder.Append(salesTable);
            strHtmlBuilder.Append(department);
            strHtmlBuilder.Append(paymentTable);
            strHtmlBuilder.Append(cash);
            strHtmlBuilder.Append(workTime);
            strHtmlBuilder.Append(generalInfo);
            strHtmlBuilder.Append(ticketTag);
            strHtmlBuilder.Append(userSales);
            strHtmlBuilder.Append(userDetails);

            strHtmlBuilder.Append("</body>");
            strHtmlBuilder.Append("</html>");

            if (strHtmlBuilder.Length > 0)
                File.WriteAllText(Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken),
                    strHtmlBuilder.ToString());

            file.FileType = MimeTypeNames.ApplicationXhtmlXml;
            file.FileName = Path.GetFileNameWithoutExtension(file.FileName) + ".html";
        }

        //private void WriteHtmlDailyReconciliation(FileDto file, DailyReconciliationReportOutput dailyReconciliation, CentralShiftReportInput input,
        //    string workShiftTime, List<string> terminals)
        //{
        //    var strHtmlBuilder = new StringBuilder();

        //    var decimalConfig = GetNumberDecimalDisplay(_roundDecimals);

        //    var headerReport = @"";
        //    if (input.workShiftId == "0")
        //    {
        //        headerReport += @"<h1>Daily Reconciliation Report View Model</h1>
        //                        <h2> (Day: {dateReport})</h2>";
        //        headerReport = headerReport.Replace("{dateReport}", input.DateReport.ToShortDateString());
        //    }
        //    else
        //    {
        //        headerReport += @"<h1>Daily Reconciliation Report View Model</h1>
        //                        <h2> (WorkShift: {dateReport})</h2>";
        //        headerReport = headerReport.Replace("{dateReport}", dailyReconciliation.Total.WorkShift);
        //    }    
        //    // DailyReconciliation Table
        //    var dailyReconciliationTable = @"<table>
        //                        <tr class=""title__table"">
        //                            <th colspan=""5""> Daily Reconciliation </th>
        //                        </tr>
        //                        <tr class=""table__header"">
        //                            <th>TENDER</th>
        //                            <th class=""pull-right"">PORTION (%)</th>
        //                            <th class=""pull-right"">AMOUNT</th>
        //                            <th class=""pull-right"">ACTUAL</th>
        //                            <th class=""pull-right"">DIFF</th>
        //                        </tr>";
        //    foreach (var d in dailyReconciliation.Data)
        //    {
        //        var value = @"<tr>
        //                        <th>{PaymentType}</th>
        //                        <th class=""pull-right"">{Portion}</th>
        //                        <th class=""pull-right"">{SystemAmount}</th>
        //                        <th class=""pull-right"">{ActualAmount}</th>
        //                        <th class=""pull-right"">{Difference}</th>
        //                      </tr>";

        //        value = value.Replace("{PaymentType}",
        //                d.PaymentType)
        //                    .Replace("{Portion}",
        //                d.Portion.ToString() + "%")
        //                    .Replace("{SystemAmount}",
        //                d.SystemAmount.ToString())
        //                    .Replace("{ActualAmount}",
        //                d.ActualAmount.ToString())
        //                    .Replace("{Difference}",
        //                d.Difference.ToString());
        //        dailyReconciliationTable += value;
        //    }
        //    if (dailyReconciliation.Total != null)
        //    {
        //        dailyReconciliationTable += @"<tr class=""table__highlight"">
        //                      <th colspan=""1"">TOTAL</th>
        //                      <th class=""pull-right"">{portion_all}</th>
        //                      <th class=""pull-right"">{amount_all}</th>
        //                      <th class=""pull-right"">{actual_all}</th>
        //                      <th class=""pull-right"">{diff_all}</th>
        //                  </tr>";

        //        dailyReconciliationTable = dailyReconciliationTable.Replace("{portion_all}",
        //                                dailyReconciliation.Total.Portion + "%")
        //                                .Replace("{amount_all}",
        //                                dailyReconciliation.Total.SystemAmount.ToString())
        //                                .Replace("{actual_all}",
        //                                dailyReconciliation.Total.ActualAmount.ToString())
        //                                .Replace("{diff_all}",
        //                                dailyReconciliation.Total.Difference.ToString());

        //        dailyReconciliationTable += @"<tr class=""table__highlight"">
        //                      <th colspan=""1"">[NUMBEROFBILL]</th>
        //                      <th class=""pull-right"">{NumberOfBill}</th>
        //                  </tr>";
        //        dailyReconciliationTable = dailyReconciliationTable.Replace("{NumberOfBill}",
        //                                string.Format(decimalConfig, dailyReconciliation.NumberOfBill));

        //        dailyReconciliationTable += @"<tr class=""table__highlight"">
        //                      <th colspan=""1"">AVG. PER BILL</th>
        //                      <th class=""pull-right"">{AvgPerBill}</th>
        //                  </tr>
        //                  </table>";
        //        dailyReconciliationTable = dailyReconciliationTable.Replace("{AvgPerBill}",
        //                                string.Format(decimalConfig, dailyReconciliation.AvgPerBill));
        //    }
        //    // Create HTML index
        //    strHtmlBuilder.Append("<html>");
        //    strHtmlBuilder.Append("<head>");
        //    strHtmlBuilder.Append(@"<style>
        //            table {
        //                font-family: arial, sans-serif;
        //                border-collapse: collapse;
        //                width: 50%;
        //                margin: auto;
        //                margin-top: 10px;
        //            }

        //            td, th {
        //                border: 1px solid #dddddd;
        //                text-align: left;
        //                padding: 8px;
        //            }
        //            .title__table {
        //                background-color: #008000;
        //                text-transform: uppercase;
        //                color: #FFFFFF;
        //            }

        //            .title__table > th {
        //                text-align: center;
        //            }

        //            .table__highlight {
        //                background-color: #808080;
        //                text-transform: uppercase;
        //                color: #FFFFFF;
        //                font-weight: bold;
        //            }
        //            .table__header {
        //                background-color: #808080;
        //                color: #FFFFFF;
        //            }
        //            .pull-right {
        //                text-align: right;
        //            }
        //            h1, h2 {
        //                text-align: center;
        //            }
        //            </style>");
        //    strHtmlBuilder.Append("</head>");
        //    strHtmlBuilder.Append("<body>");

        //    // Add Heading
        //    strHtmlBuilder.Append(headerReport);

        //    // Add Table
        //    strHtmlBuilder.Append(dailyReconciliationTable);

        //    strHtmlBuilder.Append("</body>");
        //    strHtmlBuilder.Append("</html>");

        //    if (strHtmlBuilder.Length > 0)
        //        File.WriteAllText(Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken),
        //            strHtmlBuilder.ToString());

        //    file.FileType = MimeTypeNames.ApplicationXhtmlXml;
        //    file.FileName = Path.GetFileNameWithoutExtension(file.FileName) + ".html";
        //}

        private string GetNumberDecimalDisplay(int number)
        {
            var str = @"{0:0.";

            for (var i = 0; i < number; i++) str += @"0";

            str += @"}";
            return str;
        }

        private int CreateSummaryRow(ExcelWorksheet sheet, int row, CentralDayReportData centralDay)
        {
            row++;
            AddSubHeader(sheet, row, L("Summary"));
            row++;
            row = BindingValueToAvgCell(sheet, row, centralDay.TicketTypeReportData.Average, L("Avg/Receipt"));
            row = BindingValueToCell(sheet, row, centralDay.TicketTypeReportData.ItemSales, L("Sales"));

            row = BindingValueToAvgCell(sheet, row, centralDay.TicketTypeReportData.AvgItemCount, L("Avg/Item"));
            row = BindingValueToCell(sheet, row, centralDay.TicketTypeReportData.ItemDiscountTotal, L("ItemDiscount"));

            row = BindingValueToAvgCell(sheet, row, centralDay.TicketTypeReportData.AvePaxCount, L("Avg/Pax"));
            row = BindingValueToCell(sheet, row, centralDay.TicketTypeReportData.TicketDiscountTotal, L("TicketDiscount"));

            row = BindingValueToCell(sheet, row, centralDay.TicketTypeReportData.RoundingTotal, L("Rounding"));

            row = BindingValueToAvgCell(sheet, row, centralDay.TicketTypeReportData.AvgItemCountReceipt,
                L("AvgItemCount/Receipt"));
            row = BindingValueToCell(sheet, row, centralDay.TicketTypeReportData.SubTotal, L("NetSales"));

            row = BindingValueToAvgCell(sheet, row, centralDay.TicketTypeReportData.TotalVoid, L("TotalVoid"));
            row = BindingValueToCell(sheet, row, centralDay.TicketTypeReportData.Tax,
                L("Tax"), true);

            row = BindingValueToAvgCell(sheet, row, centralDay.TicketTypeReportData.TotalComp, L("Total Wastage"));
            row = BindingValueToCell(sheet, row, centralDay.TicketTypeReportData.TotalSales,
                L("TotalSales"), true);

            row = BindingValueToAvgCell(sheet, row, centralDay.TicketTypeReportData.TicketCount,
                L("TotalReceiptCount"));
            row++;

            row = BindingValueToAvgCell(sheet, row, centralDay.TicketTypeReportData.ItemCount, L("TotalItemCount"), false, false);
            row++;
            row = BindingValueToAvgCell(sheet, row, centralDay.TicketTypeReportData.PaxCount, L("TotalPax"), false, false);
            row++;
            row = BindingValueToCell(sheet, row, centralDay.TicketTypeReportData.TotalSales, L("TotalCollection"),
                false,
                true);
            return row;
        }

        private int CreateDepartmentSumaryRow(ExcelWorksheet sheet, int row, List<object> data)
        {
            var listDepart = data.Select(c => new DepartmentSales
            {
                Name = (string)c.GetType().GetProperty("depatName")?.GetValue(c, null),
                Amount = (decimal)c.GetType().GetProperty("amount")?.GetValue(c, null),
                TotalTicketCount = (string)c.GetType().GetProperty("count")?.GetValue(c, null),
                ItemCount = (decimal)c.GetType().GetProperty("itemCount")?.GetValue(c, null)
            }).ToList();

            row++;
            AddSubHeader(sheet, row, L("DepartmentSummary"));

            row++;

            AddDetail(sheet, row, 3, $"{L("Sales")} ($)");
            AddDetail(sheet, row, 5, L("ItemCount"));
            row++;

            AddObjects(
                sheet, row, listDepart,
                _ => _.Name,
                _ => "$",
                _ => _.NetSales,
                _ => "",
                _ => _.ItemCount
            );

            row += listDepart.Count();

            sheet.Cells[row, 1].Value = L("Total");
            sheet.Cells[row, 2].Value = "$";
            sheet.Cells[row, 3].Value = listDepart.Sum(d => d.NetSales);
            sheet.Cells[row, 5].Value = listDepart.Sum(d => d.ItemCount);
            sheet.Cells[row, 1, row, 7].Style.Font.Bold = true;

            return row;
        }

        private int CreateGroupSummaryRow(ExcelWorksheet sheet, int row, List<SalesItemSumaryDto> data)
        {
            row += 2;

            AddSubHeader(sheet, row++, L("SaleGroupSummary"));
            AddDetail(sheet, row, 3, $"{L("Sales")} ($)");
            AddDetail(sheet, row, 4, $"{L("Discount")} ($)");
            AddDetail(sheet, row, 5, L("ItemCount"));

            row++;

            AddObjects(
                sheet, row, data,
                _ => _.Name,
                _ => "$",
                _ => _.Amount,
                _ => _.Discount,
                _ => _.ItemCount
            );

            row = row + data.Count();
            sheet.Cells[row, 1].Value = L("Total");
            sheet.Cells[row, 2].Value = "$";
            sheet.Cells[row, 3].Value = data.Sum(d => d.Amount);
            sheet.Cells[row, 4].Value = data.Sum(d => d.Discount);
            sheet.Cells[row, 5].Value = data.Sum(d => d.ItemCount);
            sheet.Cells[row, 1, row, 7].Style.Font.Bold = true;

            return row;
        }

        private int CreateCategorySummaryRow(ExcelWorksheet sheet, int row, List<SalesItemSumaryDto> data)
        {
            row += 2;
            AddSubHeader(sheet, row++, L("CategorySummary"));
            AddDetail(sheet, row, 3, $"{L("Sales")} ($)");
            AddDetail(sheet, row, 4, $"{L("Discount")} ($)");
            AddDetail(sheet, row, 5, L("ItemCount"));
            row++;

            AddObjects(
                sheet, row, data,
                _ => _.Name,
                _ => "$",
                _ => _.Amount,
                _ => _.Discount,
                _ => _.ItemCount
            );

            row = row + data.Count();
            sheet.Cells[row, 1].Value = L("Total");
            sheet.Cells[row, 2].Value = "$";
            sheet.Cells[row, 3].Value = data.Sum(d => d.Amount);
            sheet.Cells[row, 4].Value = data.Sum(d => d.Discount);
            sheet.Cells[row, 5].Value = data.Sum(d => d.ItemCount);
            sheet.Cells[row, 1, row, 7].Style.Font.Bold = true;

            return row;
        }

        private int CreateSlotPeriodRevenueRow(ExcelWorksheet sheet, int row, List<SalesItemSumaryDto> data)
        {
            row += 2;
            AddSubHeader(sheet, row++, L("SlotPeriodRevenue"));
            AddDetail(sheet, row, 3, $"{L("Sales")} ($)");
            AddDetail(sheet, row, 5, L("ItemCount"));
            AddDetail(sheet, row, 6, L("ReceiptCount"));
            row++;

            AddObjects(
                sheet, row, data,
                _ => _.Name,
                _ => "$",
                _ => _.Amount,
                _ => "",
                _ => _.ItemCount,
                _ => _.TicketCount
            );

            row += data.Count();
            sheet.Cells[row, 1].Value = L("Total");
            sheet.Cells[row, 2].Value = "$";
            sheet.Cells[row, 3].Value = data.Sum(d => d.Amount);
            sheet.Cells[row, 5].Value = data.Sum(d => d.ItemCount);
            sheet.Cells[row, 6].Value = data.Sum(d => d.TicketCount);
            sheet.Cells[row, 1, row, 7].Style.Font.Bold = true;
            return row;
        }

        private int CreateDiscountSummaryRow(ExcelWorksheet sheet, int row, List<SalesItemSumaryDto> data)
        {
            row += 2;
            AddSubHeader(sheet, row++, L("DiscountSummary"));
            AddDetail(sheet, row, 3, $"{L("Amount")} ($)");
            AddDetail(sheet, row, 5, L("Qty"));
            row++;

            AddObjects(
                sheet, row, data,
                _ => _.Name,
                _ => "$",
                _ => _.Amount,
                _ => "",
                _ => _.TicketCount
            );

            row += data.Count();
            sheet.Cells[row, 1].Value = L("Total");
            sheet.Cells[row, 2].Value = "$";
            sheet.Cells[row, 3].Value = data.Sum(d => d.Amount);
            sheet.Cells[row, 5].Value = data.Sum(d => d.TicketCount);
            sheet.Cells[row, 1, row, 7].Style.Font.Bold = true;
            return row;
        }

        private int CreateTenderSummaryRow(ExcelWorksheet sheet, int row, List<PaymentTypeByWorkTimeRowData> data)
        {
            row += 2;
            AddSubHeader(sheet, row++, L("TenderSummary"));

            row++;

            AddObjects(
                sheet, row, data,
                _ => _.paymentName,
                _ => "$",
                _ => _.actualTotal
            );

            row += data.Count();
            sheet.Cells[row, 1].Value = L("TotalTender");
            sheet.Cells[row, 2].Value = "$";
            sheet.Cells[row, 3].Value = data.Sum(d => d.actualTotal);
            sheet.Cells[row, 1, row, 7].Style.Font.Bold = true;
            row = row + 2;

            sheet.Cells[row, 1, row, 7].Style.Font.Bold = true;
            AddDetail(sheet, row++, 1, L("CashDeclaration"));

            var cashDeclar = data.Where(t => t.paymentName.ToUpper().Equals("CASH")).FirstOrDefault();
            AddDetail(sheet, row, 1, L("TotalCashierDeclaration"));
            AddDetail(sheet, row, 2, "$");
            AddDetail(sheet, row++, 3, cashDeclar.inSalesTotal);

            AddDetail(sheet, row, 1, L("TotalPOSDeclaration"));
            AddDetail(sheet, row, 2, "$");
            AddDetail(sheet, row++, 3, cashDeclar.actualTotal);

            AddDetail(sheet, row, 1, L("CashExcess/Shortage"));
            AddDetail(sheet, row, 2, "$");
            AddDetail(sheet, row, 3, cashDeclar.differenceTotal);

            return row;
        }

        private int CreateNoCollectionSummaryRow(ExcelWorksheet sheet, int row, List<object> data)
        {
            row += 2;
            AddSubHeader(sheet, row++, L("NoCollectionSummary"));
            AddDetail(sheet, row, 3, $"{L("Amount")} ($)");
            AddDetail(sheet, row, 5, L("Qty"));
            row++;

            //AddObjects(
            //   sheet, row, listDepart,
            //   _ => _.Name,
            //   _ => "$",
            //   _ => _.Amount,
            //   _ => "",
            //   _ => Int32.Parse(_.TotalTicketCount)
            //   );

            row += data.Count();
            sheet.Cells[row, 1].Value = L("Total");
            sheet.Cells[row, 2].Value = "$";
            //sheet.Cells[row, 3].Value = listDepart.Sum(d => d.Amount);
            sheet.Cells[row, 1, row, 7].Style.Font.Bold = true;
            return row;
        }

        private static void AddSubHeader(ExcelWorksheet sheet, int row, string name)
        {
            sheet.Cells[row, 1].Value = name;

            sheet.Cells[row, 1].Style.Font.Bold = true;
            sheet.Cells[row, 1, row, 12].Merge = true;
            sheet.Cells[row, 1, row, 12].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[row, 1, row, 12].Style.Fill.BackgroundColor.SetColor(Color.Gray);
            sheet.Cells[row, 1, row, 12].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            sheet.Cells[row, 1, row, 12].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            sheet.Cells[row, 1, row, 12].Style.Border.Bottom.Color.SetColor(Color.Black);
            sheet.Cells[row, 1, row, 12].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            sheet.Cells[row, 1, row, 12].Style.Border.Left.Color.SetColor(Color.Black);
        }

        private int BindingValueToCell(ExcelWorksheet sheet, int row, object value, string name, bool border = false,
            bool bold = false)
        {
            sheet.Cells[row, 1].Value = name;
            sheet.Cells[row, 2].Value = "$";
            sheet.Cells[row, 3].Value = value;
            if (border)
            {
                sheet.Cells[row, 3].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                sheet.Cells[row, 3].Style.Border.Bottom.Color.SetColor(Color.Black);
            }

            if (bold) sheet.Cells[row, 1, row, 7].Style.Font.Bold = true;
            row++;
            return row;
        }

        private int BindingValueToAvgCell(ExcelWorksheet sheet, int row, object value, string name, bool bold = false, bool displayDollar = true)
        {
            sheet.Cells[row, 9].Value = name;
            if (displayDollar)
                sheet.Cells[row, 10].Value = "$";
            sheet.Cells[row, 11].Value = value;

            if (bold) sheet.Cells[row, 1, row, 7].Style.Font.Bold = true;
            return row;
        }

        private int AddReportHeader(ExcelWorksheet sheet, CentralExportInput input, CentralDayReportData centralData)
        {
            sheet.Cells[1, 1].Value = centralData.LocationDetails.CompanyName;
            sheet.Cells[1, 9].Value = "Printed Date: " + Clock.Now.ToString(_simpleDateFormat);
            sheet.Cells[1, 1].Style.Font.Bold = true;

            sheet.Cells[2, 1].Value = centralData.LocationDetails.Name;
            sheet.Cells[2, 1, 2, 2].Merge = true;
            sheet.Cells[2, 1].Style.Font.Bold = true;

            sheet.Cells[3, 1].Value = input.DateReport.ToLongDateString();
            sheet.Cells[3, 1, 3, 5].Merge = true;
            return 4;
        }

        #endregion Private Methods
    }

    public enum TypeCentralExport
    {
        CentralDay,
        CentralShift,
        CentralTerminal
    }
}