﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using DinePlan.DineConnect.Connect.Ticket.Central.Dto;
using DinePlan.DineConnect.Connect.WorkPeriod.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Ticket.Central
{
    public interface ICentralReportAppService : IApplicationService
    {
        Task<CentralDayReportData> GetCentralDayReport(int workPeriodId);
        Task<CentralDayReportData> GetCentralShiftReport(CentralShiftReportInput input);
        Task<DailyReconciliationReportOutput> GetDailyReconciliation(CentralShiftReportInput input);
        Task<List<DailyReconciliationReportAllOutput>> GetDailyReconciliationAll(WorkPeriodSummaryInput input);
        Task<CentralDayReportData> GetCentralTerminalReport(CentralTerminalReportInput input);
        Task<FileDto> GetCentralDayExport(CentralExportInput input);
        Task<FileDto> GetDailyReconciliationExport(CentralShiftReportInput input);
        Task<FileDto> GetDailyReconciliationAllExport(WorkPeriodSummaryInput input);
        Task<FileDto> GetCentralShiftExport(CentralShiftExportInput input);
        Task<FileDto> GetCentralTerminalExport(CentralTerminalExportInput input);

    }
}
