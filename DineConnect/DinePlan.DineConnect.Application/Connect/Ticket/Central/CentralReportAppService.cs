﻿using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Connect.Tag;
using DinePlan.DineConnect.Connect.Ticket.Central.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Ticket.Implementation;
using DinePlan.DineConnect.Connect.Till;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Connect.WorkPeriod.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Helper;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Report;
using DinePlan.DineConnect.Utilities;
using Newtonsoft.Json;
using System;
using DinePlan.DineConnect.Configuration.Tenants;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using PromotionDetailValue = DinePlan.DineConnect.Connect.Transaction.PromotionDetailValue;
using TicketTagInfo = DinePlan.DineConnect.Connect.Ticket.Implementation.TicketTagInfo;

namespace DinePlan.DineConnect.Connect.Ticket.Central
{
    public class CentralReportAppService : DineConnectAppServiceBase, ICentralReportAppService
    {
        private const string PAYMENT_TYPE_CASH = "CASH";
        private string[] DISCOUNTNAMES = { "DISCOUNT" };
        private string[] ROUNDTOTALS = { "ROUND+", "ROUND-", "ROUNDING" };

        private readonly IRepository<Master.Department> _dRepo;
        private readonly IRepository<TransactionType> _tRe;
        private readonly IRepository<Period.WorkPeriod> _workPeriod;
        private readonly IRepository<Transaction.Ticket> _ticketRepository;
        private readonly IRepository<PaymentType> _paymentTypeRepository;
        private readonly IRepository<TillTransaction> _tillTransactionRepository;
        private readonly IRepository<Promotion> _promotionRepository;
        private readonly IRepository<TicketTagGroup> _ticketTagGroupRepository;
        private readonly ICentralReportExporter _exporter;
        private readonly IRepository<Master.Location> _locationRepo;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IReportBackgroundAppService _rbas;
        private readonly IBackgroundJobManager _bgm;
        private readonly IConnectReportAppService _connectReportAppService;
        private readonly ITenantSettingsAppService _tenantSettingsService;
        public CentralReportAppService(
            IRepository<TransactionType> tRepo,
             IRepository<Master.Location> locationRepo,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<Master.Department> drepo,
            IRepository<Period.WorkPeriod> workPeriod,
            IRepository<Transaction.Ticket> ticketRepository,
            IRepository<PaymentType> paymentTypeRepository,
            IRepository<TillTransaction> tillTransactionRepository,
            IRepository<Promotion> promotionRepository,
            IRepository<TicketTagGroup> ticketTagGroupRepository,
            ICentralReportExporter exporter,
            IReportBackgroundAppService rbas,
            IBackgroundJobManager bgm,
            IConnectReportAppService connectReportAppService,
             ITenantSettingsAppService tenantSettingsService)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _tRe = tRepo;
            _dRepo = drepo;
            _workPeriod = workPeriod;
            _ticketRepository = ticketRepository;
            _paymentTypeRepository = paymentTypeRepository;
            _tillTransactionRepository = tillTransactionRepository;
            _promotionRepository = promotionRepository;
            _ticketTagGroupRepository = ticketTagGroupRepository;
            _exporter = exporter;
            _rbas = rbas;
            _bgm = bgm;
            _locationRepo = locationRepo;
            _connectReportAppService = connectReportAppService;
            _tenantSettingsService = tenantSettingsService;
        }

        #region Public Methods

        public async Task<FileDto> GetCentralDayExport(CentralExportInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                var setting = await _tenantSettingsService.GetAllSettings();
                input.DatetimeFormat = setting.Connect.DateTimeFormat;
                input.DateFormat = setting.Connect.SimpleDateFormat;
                input.TenantId = AbpSession.TenantId.Value;
                input.UserId = AbpSession.UserId.Value;

                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.DAILYSALES,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });

                    if (backGroundId > 0)
                    {
                        await _bgm.EnqueueAsync<CentralReportJob, CentralReportInputArgs>(new CentralReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.DAILYSALES,
                            CentralExportInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                    }
                }
                else
                    return await _exporter.CentralDayExport(input, this);
            }
            return null;
        }

        public async Task<FileDto> GetDailyReconciliationExport(CentralShiftReportInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                input.TenantId = AbpSession.TenantId.Value;
                input.UserId = AbpSession.UserId.Value;

                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.DAILYRECONCILITION,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });

                    //var _input = new CentralExportInput()
                    //{
                    //    WorkPeriodId = input.workPeriodId,
                    //    DateReport = input.DateReport,
                    //    RunInBackground = input.RunInBackground,
                    //    ReportDescription = input.ReportDescription,
                    //    TenantId = input.TenantId,
                    //    UserId = input.UserId
                    //};
                    if (backGroundId > 0)
                    {
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.DAILYRECONCILITION,
                            CentralShiftReportInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                    }
                }
                else
                {
                    //return await _exporter.DailyReconciliationExport(input, this);
                }

            }
            return null;
        }

        public async Task<FileDto> GetDailyReconciliationAllExport(WorkPeriodSummaryInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                input.TenantId = AbpSession.TenantId.Value;
                input.DateReport = DateTime.Now;

                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.DAILYRECONCILITIONALL,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                    });


                    if (backGroundId > 0)
                    {
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.DAILYRECONCILITIONALL,
                            WorkPeriodSummaryInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                    }
                }
                else
                {
                    return await _exporter.DailyReconciliationExportAll(input, this);
                }

            }
            return null;
        }

        public async Task<FileDto> GetCentralShiftExport(CentralShiftExportInput input)
        {
            return await _exporter.CentralShiftDayExport(input, this);
        }

        public async Task<FileDto> GetCentralTerminalExport(CentralTerminalExportInput input)
        {
            return await _exporter.CentralTerminalDayExport(input, this);
        }

        public async Task<CentralDayReportData> GetCentralDayReport(int workPeriodId)
        {
            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                var workPeriod = await _workPeriod.FirstOrDefaultAsync(t => t.Id == workPeriodId);
                var allTickets = GetWorkPeriodTickets(workPeriod);
                var result = new CentralDayReportData();
                if (workPeriod != null)
                {
                    var location = workPeriod.Location;
                    result.LocationDetails = location.MapTo<LocationListDto>();
                }
                await RenderDayAndTimeReport(allTickets.ToList(), result, workPeriod);
                return result;
            }
        }

        public async Task<CentralDayReportData> GetCentralShiftReport(CentralShiftReportInput input)
        {
            var workPeriod = await _workPeriod.FirstOrDefaultAsync(t => t.Id == input.workPeriodId);
            List<WorkShiftDto> shiftDto = null;
            WorkShiftDto currentWorkShiftDto = null;

            if (!string.IsNullOrEmpty(workPeriod.WorkPeriodInformations))
            {
                shiftDto = JsonConvert.DeserializeObject<List<WorkShiftDto>>(workPeriod.WorkPeriodInformations);
                currentWorkShiftDto = shiftDto.FirstOrDefault(t => t.UniqueId == input.workShiftId);
            }

            var allTickets = GetCurrentWorkTimeTicketsForTerminal(currentWorkShiftDto, workPeriod);
            var result = new CentralDayReportData();
            if (workPeriod != null)
            {
                var location = workPeriod.Location;
                result.LocationDetails = location.MapTo<LocationListDto>();
            }

            await RenderDayAndTimeReport(allTickets.ToList(), result, workPeriod);
            return result;
        }

        public async Task<List<DailyReconciliationReportAllOutput>> GetDailyReconciliationAll(WorkPeriodSummaryInput input)
        {
            var result = new List<DailyReconciliationReportOutput>();
            var workperiods = await _connectReportAppService.GetWorkPeriodByLocationForDates(input);
            foreach (var workperiod in workperiods)
            {
                var output = await GetDailyReconciliation(new CentralShiftReportInput()
                {
                    workPeriodId = workperiod.Id
                });
                result.Add(output);
            }

            return result.GroupBy(t => new { t.WorkPeriodInfo.LocationName, t.WorkPeriodInfo.LocationCode }).Select(t => new DailyReconciliationReportAllOutput()
            {
                LocationName = t.Key.LocationName,
                LocationCode = t.Key.LocationCode,
                //Date =  t.Key.Date,
                Details = t.ToList(),

            }).ToList();

        }

        public async Task<DailyReconciliationReportOutput> GetDailyReconciliation(CentralShiftReportInput input)
        {
            var count = 0;
            WorkPeriodListDto workPeriodListDto = new WorkPeriodListDto();
            var workPeriod = await _workPeriod.FirstOrDefaultAsync(t => t.Id == input.workPeriodId);
            workPeriodListDto = workPeriod.MapTo<WorkPeriodListDto>();
            List<WorkShiftDto> shiftDto = null;
            WorkShiftDto currentWorkShiftDto = null;
            var listDailyReconciliationDtos = new List<DailyReconciliationDto>();
            DailyReconciliationReportOutputDto allDailyReconciliationReportOutputDto = new DailyReconciliationReportOutputDto();
            List<DailyReconciliationReportOutputDto> dailyReconciliationReportOutputlst = new List<DailyReconciliationReportOutputDto>();
            DailyReconciliationDto totalDailyReconciliationDto = null, shiftBasedtotalDailyReconciliationDto = null;
            var result = new DailyReconciliationReportOutput();

            if (!string.IsNullOrEmpty(workPeriod.WorkPeriodInformations))
            {
                shiftDto = JsonConvert.DeserializeObject<List<WorkShiftDto>>(workPeriod.WorkPeriodInformations);
                //if (input.workShiftId != "0")
                //{
                //    currentWorkShiftDto = shiftDto.FirstOrDefault(t => t.UniqueId == input.workShiftId);

                //    if (currentWorkShiftDto != null)
                //    {
                //        var allPaymentInfos = currentWorkShiftDto.PaymentInfos;

                //        if (allPaymentInfos.Any())
                //        {
                //            var winSalesTotal = 0M;
                //            var wdifferenceTotal = 0M;
                //            var wactualTotal = 0M;
                //            var sumInSalesTotal = allPaymentInfos.GroupBy(a => a.PaymentType).Sum(x => x.Sum(a => a.Actual));

                //            var pt = await _paymentTypeRepository.GetAllListAsync();

                //            foreach (var infos in allPaymentInfos.GroupBy(a => a.PaymentType))
                //            {
                //                var actualTotal = infos.Sum(a => a.Actual);
                //                var inSalesTotal = infos.Sum(a => a.Entered);
                //                var differenceTotal = inSalesTotal - actualTotal;

                //                var pType = pt.LastOrDefault(a => a.Id == infos.Key);

                //                if (pType != null && (actualTotal > 0M || inSalesTotal > 0M))
                //                    listDailyReconciliationDtos.Add(new DailyReconciliationDto()
                //                    {
                //                        PaymentType = pType.Name,
                //                        Portion = sumInSalesTotal != 0 ? Math.Round(inSalesTotal / sumInSalesTotal * 100, 2) : 0,
                //                        ActualAmount = actualTotal,
                //                        SystemAmount = inSalesTotal,
                //                        Difference = differenceTotal
                //                    });

                //                wactualTotal += actualTotal;
                //                winSalesTotal += inSalesTotal;
                //                wdifferenceTotal += differenceTotal;
                //            }
                //            if (currentWorkShiftDto != null)
                //            {
                //                var allTickets = GetCurrentWorkTimeTicketsForTerminal(currentWorkShiftDto, workPeriod);
                //                if (allTickets != null)
                //                {
                //                    var incomeCalculator = GetTicketsIncomeCalculator(allTickets.ToList());

                //                    foreach (var paymentName in incomeCalculator.PaymentNames)
                //                    {
                //                        count = count + incomeCalculator.GetCount(paymentName);
                //                    }
                //                }
                //            }

                //            totalDailyReconciliationDto = new DailyReconciliationDto()
                //            {
                //                PaymentType = L("Total"),
                //                ActualAmount = winSalesTotal,
                //                SystemAmount = wactualTotal,
                //                Difference = wdifferenceTotal,
                //                Portion = 100,
                //                WorkShift = currentWorkShiftDto.infoId
                //            };
                //        }
                //    }
                //}
                //else
                //{

                //}
                if (shiftDto != null && shiftDto.Any())
                {
                    foreach (var lst in shiftDto)
                    {
                        List<DailyReconciliationDto> dailyReconciliationlistDtos = new List<DailyReconciliationDto>();
                        var shiftTiming = "Terminal:" + lst.TerminalName
                            + " Shift Start By: " + lst.StartUser + " , "
                            + " Shift End By : " + lst.StopUser + " , "
                            + " Shift Start : " + lst.StartDate.ToString("hh:mm tt") + " , "
                            + "Shift End : " + lst.EndDate.ToString("hh:mm tt");
                        var shiftBasedInfos = lst.PaymentInfos;
                        if (shiftBasedInfos.Any())
                        {
                            var winSalesTotal = 0M;
                            var wdifferenceTotal = 0M;
                            var wactualTotal = 0M;
                            var sumInSalesTotal = shiftBasedInfos.Sum(x => x.Actual);

                            var pt = await _paymentTypeRepository.GetAllListAsync();

                            foreach (var infos in shiftBasedInfos.GroupBy(a => a.PaymentType))
                            {
                                var systemAmountTotal = infos.Sum(a => a.Actual);
                                var userAmountTotal = infos.Sum(a => a.Entered);
                                var differenceTotal = userAmountTotal - systemAmountTotal;

                                var pType = pt.LastOrDefault(a => a.Id == infos.Key);

                                if (pType != null && (systemAmountTotal > 0M || userAmountTotal > 0M))
                                    dailyReconciliationlistDtos.Add(new DailyReconciliationDto()
                                    {
                                        PaymentType = pType.Name,
                                        Portion = sumInSalesTotal != 0 ? Math.Round(systemAmountTotal / sumInSalesTotal * 100, 2) : 0,
                                        SystemAmount = systemAmountTotal,
                                        ActualAmount = userAmountTotal,
                                        Difference = differenceTotal
                                    });

                                winSalesTotal += systemAmountTotal;
                                wactualTotal += userAmountTotal;
                                wdifferenceTotal += differenceTotal;
                            }
                            shiftBasedtotalDailyReconciliationDto = new DailyReconciliationDto()
                            {
                                PaymentType = L("Total"),
                                ActualAmount = wactualTotal,
                                SystemAmount = winSalesTotal,
                                Difference = wdifferenceTotal,
                                Portion = 100,
                                WorkShift = null
                            };
                            DailyReconciliationReportOutputDto dailyReconciliationReportOutputDto = new DailyReconciliationReportOutputDto
                            {
                                WorkShiftTiming = shiftTiming,
                                WorkShiftDailyReconciliationList = dailyReconciliationlistDtos,
                                ShiftBasedTotalDailyReconciliation = shiftBasedtotalDailyReconciliationDto
                            };
                            dailyReconciliationReportOutputlst.Add(dailyReconciliationReportOutputDto);
                        }
                    }
                    var allPaymentInfos = shiftDto.SelectMany(a => a.PaymentInfos);

                    if (allPaymentInfos.Any())
                    {
                        var shiftTiming = "Day Start : " + shiftDto.FirstOrDefault().StartDate.ToString(GetDateTimeFormat()) + " , " + "Day End : " + shiftDto.LastOrDefault().EndDate.ToString(GetDateTimeFormat()) + Environment.NewLine + "Day Start By : " + shiftDto.FirstOrDefault().StartUser + " , " + "Day End By: " + shiftDto.LastOrDefault().StopUser;
                        var winSalesTotal = 0M;
                        var wdifferenceTotal = 0M;
                        var wactualTotal = 0M;
                        var sumInSalesTotal = allPaymentInfos.GroupBy(a => a.PaymentType).Sum(x => x.Sum(a => a.Actual));

                        var pt = await _paymentTypeRepository.GetAllListAsync();

                        foreach (var infos in allPaymentInfos.GroupBy(a => a.PaymentType))
                        {
                            var systemAmountTotal = infos.Sum(a => a.Actual);
                            var userAmountTotal = infos.Sum(a => a.Entered);
                            var differenceTotal = userAmountTotal - systemAmountTotal;

                            var pType = pt.LastOrDefault(a => a.Id == infos.Key);

                            if (pType != null && (systemAmountTotal > 0M || userAmountTotal > 0M))
                                listDailyReconciliationDtos.Add(new DailyReconciliationDto()
                                {
                                    PaymentType = pType.Name,
                                    Portion = sumInSalesTotal != 0 ? Math.Round(systemAmountTotal / sumInSalesTotal * 100, 2) : 0,
                                    SystemAmount = systemAmountTotal,
                                    ActualAmount = userAmountTotal,
                                    Difference = differenceTotal
                                });

                            winSalesTotal += systemAmountTotal;
                            wactualTotal += userAmountTotal;
                            wdifferenceTotal += differenceTotal;
                        }
                        foreach (var item in shiftDto)
                        {
                            if (item != null)
                            {
                                var allTickets = GetCurrentWorkTimeTicketsForTerminal(item, workPeriod);
                                if (allTickets != null)
                                {
                                    var incomeCalculator = GetTicketsIncomeCalculator(allTickets.ToList());

                                    foreach (var paymentName in incomeCalculator.PaymentNames)
                                    {
                                        count = count + incomeCalculator.GetCount(paymentName);
                                    }
                                }
                            }
                        }

                        totalDailyReconciliationDto = new DailyReconciliationDto()
                        {
                            PaymentType = L("Total"),
                            ActualAmount = wactualTotal,
                            SystemAmount = winSalesTotal,
                            Difference = wdifferenceTotal,
                            Portion = 100,
                            WorkShift = null
                        };
                        allDailyReconciliationReportOutputDto = new DailyReconciliationReportOutputDto
                        {
                            WorkShiftTiming = shiftTiming,
                            WorkShiftDailyReconciliationList = listDailyReconciliationDtos,
                            ShiftBasedTotalDailyReconciliation = totalDailyReconciliationDto
                        };
                    }
                }
            }

            result.Data = allDailyReconciliationReportOutputDto;
            result.AllWorkShiftData = dailyReconciliationReportOutputlst;
            result.NumberOfBill = workPeriod.TotalTicketCount;
            result.TotalSales = allDailyReconciliationReportOutputDto.ShiftBasedTotalDailyReconciliation.SystemAmount;
            //result.AvgPerBill = ;
            result.WorkPeriodInfo = workPeriodListDto;
            return result;
        }

        public async Task<CentralDayReportData> GetCentralTerminalReport(CentralTerminalReportInput input)
        {
            var workPeriod = await _workPeriod.FirstOrDefaultAsync(t => t.Id == input.workPeriodId);

            List<WorkShiftDto> allWorkTimes = null;

            if (!string.IsNullOrEmpty(workPeriod.WorkPeriodInformations))
            {
                allWorkTimes = JsonConvert.DeserializeObject<List<WorkShiftDto>>(workPeriod.WorkPeriodInformations);
                allWorkTimes = allWorkTimes.Where(t => input.terminals.Contains(t.TerminalName)).ToList();
            }

            var result = new CentralDayReportData();
            var allTickets = GetWorkPeriodTerminalTickets(workPeriod, input.terminals);
            if (workPeriod != null)
            {
                var location = workPeriod.Location;
                result.LocationDetails = location.MapTo<LocationListDto>();
            }

            await RenderDayAndTimeReport(allTickets.ToList(), result, workPeriod);
            return result;
        }

        public IQueryable<Transaction.Ticket> GetWorkPeriodTerminalTickets(Period.WorkPeriod workPeriod, List<string> terminals)
        {
            var allTickets = GetTicketsForWorkPeriod(workPeriod);

            return allTickets.Where(
                x => x.LastPaymentTime >= workPeriod.StartTime && x.LastPaymentTime <= workPeriod.EndTime &&
                     !x.PreOrder && !x.Credit &&
                     (x.Payments.Any(a => terminals.Contains(a.TerminalName)) ||
                      x.TotalAmount == 0M && terminals.Contains(x.TerminalName)));
        }

        #endregion Public Methods

        #region Private Methods

        private async Task<TicketTypeReportData> CreateTicketTypeInfo(IEnumerable<Transaction.Ticket> tickets)
        {
            var allTaxValues = _tRe.GetAll().Where(a => a.Tax).Select(a => a.Id).ToList();

            var returnObject = new TicketTypeReportData
            {
                TotalSales = tickets.Sum(a => a.TotalAmount),
                Tax = tickets.Where(a => a.TotalAmount > 0).SelectMany(a => a.Transactions).
                    Where(a => allTaxValues.Contains(a.TransactionTypeId)).Sum(a => a.Amount),
                Transactions = new Dictionary<string, decimal>(),
                OrderTotal = tickets.SelectMany(a => a.Orders).Sum(a => a.GetTotal())
            };

            var allTransactions = tickets.SelectMany(x => x.Transactions);
            allTransactions.GroupBy(x => x.TransactionType).ToList().ForEach(
                x => { returnObject.Transactions.Add(x.Key.Name, x.Sum(y => y.Amount)); });

            returnObject.TicketCount = tickets.Count(a => a.TotalAmount > 0);
            returnObject.ItemDiscountTotal = tickets.Where(a => a.TotalAmount > 0).SelectMany(a => a.Orders).SelectMany(a => a.GetOrderPromotionList()).Sum(a => a.PromotionAmount);
            returnObject.ItemSales = tickets.SelectMany(a => a.Orders).Sum(b => b.GetTotal());

            returnObject.TicketDiscountTotal = tickets.Where(a => a.TotalAmount > 0)
                .SelectMany(a => a.GetTicketPromotionList()).Sum(a => a.PromotionAmount);

            var tranAmount = tickets.Where(a => a.TotalAmount > 0).SelectMany(a => a.Transactions)
                .Where(a => DISCOUNTNAMES.Contains(a.TransactionType.Name.ToUpper())).Sum(a => a.Amount);
            returnObject.TicketDiscountTotal += tranAmount;

            var roundTotal = tickets.Where(a => a.TotalAmount > 0).SelectMany(a => a.Transactions)
                .Where(a => ROUNDTOTALS.Contains(a.TransactionType.Name.ToUpper())).Sum(a => a.Amount);
            returnObject.RoundingTotal += roundTotal;

            returnObject.ItemCount = tickets.Where(a => a.TotalAmount > 0).SelectMany(a => a.Orders)
                .Where(a => a.CalculatePrice && a.DecreaseInventory).Sum(a => a.Quantity);

            returnObject.PaxCount = GetPaxCount(tickets.Where(a => a.TotalAmount > 0));
            returnObject.TotalVoid = tickets.SelectMany(a => a.Orders)
                .Where(a => !a.CalculatePrice && !a.DecreaseInventory && a.OrderStates.Contains("Void")).Sum(a => a.Quantity);

            returnObject.TotalComp = tickets.SelectMany(a => a.Orders)
                .Where(a => !a.CalculatePrice && a.DecreaseInventory && a.OrderStates.Contains("Comp")).Sum(a => a.Quantity);

            returnObject.TotalComp = tickets.SelectMany(a => a.Orders)
                .Where(a => !a.CalculatePrice && a.DecreaseInventory && a.OrderStates.Contains("Gift")).Sum(a => a.Quantity);

            returnObject.TotalRefund = tickets.SelectMany(a => a.Orders)
                .Where(a => !a.CalculatePrice && !a.DecreaseInventory && a.OrderStates.Contains("Refund")).Sum(a => a.Quantity);

            return returnObject;
        }

        private decimal GetPaxCount(IEnumerable<Transaction.Ticket> dDto)
        {
            var totalPaxCount = 0M;
            var toaalll = dDto.Where(a => !string.IsNullOrEmpty(a.TicketTags))
                .Select(a => a.TicketTags);
            foreach (var tag in toaalll)
                if (!string.IsNullOrEmpty(tag))
                {
                    var myTag = JsonConvert.DeserializeObject<List<TicketTagValue>>(tag);
                    var lastTag = myTag?.LastOrDefault(a =>
                        a.TagName.Equals("COVERS") || a.TagName.Equals("PAX"));
                    if (lastTag != null) totalPaxCount += int.Parse(lastTag.TagValue);
                }
            // Gosei Hoan Update pax calculation
            if (totalPaxCount == 0)
            {
                totalPaxCount = dDto.SelectMany(t => t.Orders).Sum(o => o.NumberOfPax * o.Quantity);
            }
            return totalPaxCount;
        }

        private IQueryable<Transaction.Ticket> GetWorkPeriodTickets(Period.WorkPeriod workPeriod)
        {
            return GetTicketsForWorkPeriod(workPeriod);
        }

        private IQueryable<Transaction.Ticket> GetTicketsForWorkPeriod(Period.WorkPeriod workPeriod)
        {
            var allTickets = _ticketRepository.GetAll()
                .Include(e => e.Orders.Select(o => o.MenuItem))
                .Where(x => !x.PreOrder && !x.Credit && x.LocationId == workPeriod.LocationId);

            var wpStartDay = workPeriod.StartTime.AddDays(-2);
            var wpendDay = workPeriod.EndTime.AddDays(2);

            allTickets = allTickets.Where(a =>
                a.LastPaymentTimeTruc >= wpStartDay && a.LastPaymentTimeTruc <= wpendDay);

            allTickets = allTickets.Where(
                x => x.LastPaymentTime >= workPeriod.StartTime && x.LastPaymentTime <= workPeriod.EndTime);

            return allTickets;
        }

        private IQueryable<Transaction.Ticket> GetCurrentWorkTimeTicketsForTerminal(WorkShiftDto workShift,
            Period.WorkPeriod workPeriod)
        {
            var allTickets = GetTicketsForWorkPeriod(workPeriod);

            return allTickets.Where(
                x => x.LastPaymentTime >= workShift.StartDate && x.LastPaymentTime <= workShift.EndDate &&
                     !x.PreOrder && !x.Credit
                     && (x.Payments.Any(a => a.TerminalName.Equals(workShift.TerminalName))) || x.TerminalName.Equals(workShift.TerminalName));
        }

        private IEnumerable<DepartmentSales> GetDepartmentSales(IEnumerable<Transaction.Ticket> AllTickets,
            IEnumerable<Master.Department> AllDepartments)
        {
            var result = AllTickets
                .GroupBy(l => l.DepartmentName)
                .Select(csLine => new DepartmentSales
                {
                    Name = csLine.Key,
                    TotalTicketCount = csLine.Count().ToString(),
                    Amount = csLine.Sum(c => c.TotalAmount),
                    ItemCount = csLine.SelectMany(c => c.Orders).Sum(o => o.Quantity),
                    TaxTotal = csLine.Sum(a => a.GetTaxTotal())
                });

            foreach (var departmentSalese in result)
            {
                var depat = AllDepartments.SingleOrDefault(a => a.Name.Equals(departmentSalese.Name));
                departmentSalese.Name = "NO_DEPT";
                if (depat != null) departmentSalese.Name = depat.Name;
            }

            return result;
        }

        private IEnumerable<SalesItemSumaryDto> GetGroupSales(IEnumerable<Transaction.Ticket> allTickets, int type)
        {
            IEnumerable<IGrouping<string, Order>> groups = null;


            switch (type)
            {
                case 1:
                    groups = allTickets.SelectMany(e => e.Orders).Where(a => a.CalculatePrice && a.DecreaseInventory)
                        .GroupBy(e => e.MenuItem.Category.ProductGroup?.Name);
                    break;

                case 2:
                    groups = allTickets.SelectMany(e => e.Orders).Where(a => a.CalculatePrice && a.DecreaseInventory).GroupBy(e => e.MenuItem.Category.Name);
                    break;

                default:
                    break;
            }

            return groups.Select(
                g => new SalesItemSumaryDto
                {
                    Name = g.Key,
                    Amount = g.Sum(e => e.GetTotal()),
                    TaxAmount = g.Sum(e => e.GetTaxTotal() * e.Quantity),
                    ItemCount = g.Sum(e => e.Quantity),
                    Discount = g.SelectMany(e => e.GetOrderPromotionList()).Sum(a => a.PromotionAmount)
                });
        }

        private AmountCalculator GetTicketsIncomeCalculator(IEnumerable<Transaction.Ticket> allTickets)
        {
            var groups = allTickets
                .SelectMany(x => x.Payments)
                .GroupBy(x => x.PaymentTypeId)
                .Select(x => new TenderedAmount
                {
                    PaymentName = GetPaymentTypeName(x.Key),
                    Amount = x.Sum(y => y.Amount),
                    ActualAmount = x.Sum(y => y.TenderedAmount),
                    PaymentCount = x.Count(),
                });

            return new AmountCalculator(groups)
            {
            };
        }

        private string GetPaymentTypeName(int paymentTypeId)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var pt = _paymentTypeRepository.FirstOrDefault(x => x.Id == paymentTypeId);
                return pt != null ? pt.Name : "";
            }

            return "";
        }

        private IEnumerable<TillTransaction> GetTillTransactionsByWorkPeriod(Period.WorkPeriod workPeriod)
        {
            return
                _tillTransactionRepository.GetAll().Where(
                        x => x.TransactionTime >= workPeriod.StartTime && x.TransactionTime <= workPeriod.EndTime &&
                             !x.Status)
                    .Include(x => x.TillAccount);
        }

        private IEnumerable<TillTransaction> GetTillTransactionsByWorkTime(Period.WorkPeriod workPeriod,
            WorkShiftDto workShift)
        {
            return
                _tillTransactionRepository.GetAll().Where(
                        x => x.CreationTime >= workShift.StartDate && x.WorkPeriodId.Equals(workPeriod.Id))
                    .Include(x => x.TillAccount);
        }

        private CashReportData RenderCashTransactions(decimal cashAmount, IEnumerable<TillTransaction> tillTransAll,
            decimal floatAmount, int floatCount)
        {
            var cashReprortData = new CashReportData();
            cashReprortData.cashAmount = cashAmount;

            var allIncome = tillTransAll.Where(a => a.TillAccount.TillAccountTypeId == (int)TillAccounTypes.Income);
            var allIncomeAmount = allIncome.Sum(a => a.TotalAmount);
            allIncomeAmount += floatAmount;

            var allExp = tillTransAll.Where(a => a.TillAccount.TillAccountTypeId == (int)TillAccounTypes.Expense);
            var allExpAmount = allExp.Sum(a => a.TotalAmount);

            var differ = cashAmount + allIncomeAmount - allExpAmount;

            cashReprortData.cashIn = allIncome.Count() + floatCount;
            cashReprortData.allIncomeAmount = allIncomeAmount;
            cashReprortData.cashOut = allExp.Count();
            cashReprortData.allExpAmount = allExpAmount;
            cashReprortData.cashInDrawer = differ;

            return cashReprortData;
        }

        private async Task RenderDayAndTimeReport(IEnumerable<Transaction.Ticket> allTickets,
            CentralDayReportData result, Period.WorkPeriod workPeriod)
        {
            //ticket types
            result.TicketTypeReportData = await CreateTicketTypeInfo(allTickets);

            var allDepartments = await _dRepo.GetAllListAsync();
            var departmentSales = GetDepartmentSales(allTickets.Where(a => a.TotalAmount > 0), allDepartments).OrderBy(t => t.Name).ToList();
            result.Departments = new List<object>();
            departmentSales.ForEach(dept => result.Departments.Add(new
            {
                depatName = dept.Name,
                count = dept.TotalTicketCount,
                amount = dept.Amount,
                itemCount = dept.ItemCount
            }));

            result.SalesGroups = GetGroupSales(allTickets.Where(a => a.TotalAmount > 0), 1).OrderBy(t => t.Name).ToList();
            result.SalesCategories = GetGroupSales(allTickets.Where(a => a.TotalAmount > 0), 2).OrderBy(t => t.Name).ToList();
            result.PeriodData = await GetPeriodData(allTickets.Where(a => a.TotalAmount > 0), workPeriod);
            result.DiscountData = GetDiscountData(allTickets.Where(a => a.TotalAmount > 0));
            //payments
            var incomeCalculator = GetTicketsIncomeCalculator(allTickets);

            var cashAmount = 0M;
            var totalSales = 0M;

            var paymentTypeRows = new List<PaymentTypeRowData>();
            foreach (var paymentName in incomeCalculator.PaymentNames)
            {
                var actualSales = incomeCalculator.GetAmount(paymentName);
                if (paymentName.ToUpper().Equals(PAYMENT_TYPE_CASH)) cashAmount = actualSales;

                totalSales += actualSales;
                paymentTypeRows.Add(new PaymentTypeRowData
                {
                    paymentName = paymentName,
                    count = incomeCalculator.GetCount(paymentName),
                    actualSales = actualSales
                });
            }

            result.PaymentTypeReportData = new PaymentTypeReportData
            {
                totalSales = totalSales,
                details = paymentTypeRows
            };

            // Cash Transactions
            var tillTransAll = GetTillTransactionsByWorkPeriod(workPeriod);

            List<WorkShiftDto> shiftDto = null;

            if (!string.IsNullOrEmpty(workPeriod.WorkPeriodInformations))
                shiftDto = JsonConvert.DeserializeObject<List<WorkShiftDto>>(workPeriod.WorkPeriodInformations);

            result.CashReportData = RenderCashTransactions(cashAmount, tillTransAll, shiftDto.Sum(a => a.Float),
                shiftDto.Count());

            //WorkTimePaTable
            if (shiftDto != null && shiftDto.Any())
            {
                var allPaymentInfos = shiftDto.SelectMany(a => a.PaymentInfos);

                if (allPaymentInfos.Any())
                {
                    var winSalesTotal = 0M;
                    var wdifferenceTotal = 0M;
                    var wactualTotal = 0M;

                    var pt = await _paymentTypeRepository.GetAllListAsync();

                    var details = new List<PaymentTypeByWorkTimeRowData>();

                    foreach (var infos in allPaymentInfos.GroupBy(a => a.PaymentType))
                    {
                        var actualTotal = infos.Sum(a => a.Actual);
                        var inSalesTotal = infos.Sum(a => a.Entered);
                        var differenceTotal = inSalesTotal - actualTotal;

                        var pType = pt.LastOrDefault(a => a.Id == infos.Key);

                        if (pType != null && (actualTotal > 0M || inSalesTotal > 0M))
                            details.Add(new PaymentTypeByWorkTimeRowData
                            {
                                paymentName = pType.Name,
                                actualTotal = actualTotal,
                                inSalesTotal = inSalesTotal,
                                differenceTotal = differenceTotal
                            });

                        wactualTotal += actualTotal;
                        winSalesTotal += inSalesTotal;
                        wdifferenceTotal += differenceTotal;
                    }

                    result.WorkTimePaTable = new WorkTimePaTable
                    {
                        wactualTotal = wactualTotal,
                        winSalesTotal = winSalesTotal,
                        wdifferenceTotal = wdifferenceTotal,
                        details = details
                    };
                }
            }

            //tickets promotions
            if (true)
            {
                var allPromotionOrders = allTickets
                    .Where(a => a.TotalAmount > 0M && !string.IsNullOrEmpty(a.TicketPromotionDetails))
                    .SelectMany(a => JsonHelper.Deserialize<List<PromotionDetailValue>>(a.TicketPromotionDetails));

                if (allPromotionOrders.Any())
                {
                    var promotionData = new PromotionReportData();

                    var totalCount = 0M;
                    var totalSum = 0M;

                    foreach (var allPro in allPromotionOrders.GroupBy(a => a.PromotionId))
                    {
                        var quantity = allPro.Count();
                        var tAmount = allPro.Sum(a => Math.Abs(a.PromotionAmount));

                        var keyName = allPro.First().PromotionName;

                        promotionData.details.Add(new PromotionReportRowData
                        {
                            promotionName = keyName,
                            quantity = quantity,
                            amount = tAmount
                        });

                        totalCount += quantity;
                        totalSum += tAmount;
                    }

                    promotionData.totalCount = totalCount;
                    promotionData.totalSum = totalSum;

                    result.TicketsPromotionReportData = promotionData;
                }
            }

            //orders promotions
            if (true)
            {
                var allPromotionOrders = allTickets.SelectMany(x => x.Orders)
                    .Where(a => a.CalculatePrice && a.IsPromotionOrder && a.PromotionSyncId > 0)
                    .GroupBy(x => x.PromotionSyncId);

                if (allPromotionOrders.Any())
                {
                    var orderPromotionData = new PromotionReportData();

                    var allPromotions = await _promotionRepository.GetAllListAsync();

                    var totalCount = 0M;
                    var totalSum = 0M;

                    foreach (var allPro in allPromotionOrders)
                    {
                        var promotion = allPromotions.FirstOrDefault(t => t.Id == allPro.Key);

                        if (promotion != null)
                        {
                            var quantity = allPro.Sum(a => a.Quantity);
                            var tAmount = allPro.Sum(a => a.PromotionAmount * a.Quantity);

                            orderPromotionData.details.Add(new PromotionReportRowData
                            {
                                promotionName = promotion.Name,
                                quantity = quantity,
                                amount = tAmount
                            });

                            totalCount += quantity;
                            totalSum += tAmount;
                        }
                    }

                    orderPromotionData.totalCount = totalCount;
                    orderPromotionData.totalSum = totalSum;

                    result.OrdersPromotionReportData = orderPromotionData;
                }
            }

            //tickets count by types
            var ticketGroups = allTickets.GroupBy(x => new { x.TicketTypeName })
                .Select(x => new TicketTypeInfo
                {
                    TicketTypeName = x.Key.TicketTypeName,
                    TicketCount = x.Count(),
                });

            result.TicketCountByTicketTypeData = new List<TicketCountByTicketTypeData>();

            if (ticketGroups.Count() > 1)
                foreach (var ticketTypeInfo in ticketGroups)
                    result.TicketCountByTicketTypeData.Add(new TicketCountByTicketTypeData
                    {
                        ticketTypeName = ticketTypeInfo.TicketTypeName,
                        count = ticketTypeInfo.TicketCount
                    });

            //items
            var totalPaidOrders = allTickets.SelectMany(x => x.Orders)
                .Where(a => a.CalculatePrice && a.DecreaseInventory);
            var totalPaidItemsitemCount = totalPaidOrders.Sum(a => a.Quantity);
            var totalPaidItemsitemValue = totalPaidOrders.Sum(x => x.GetValue());

            result.ItemsData = new ItemsData();

            if (totalPaidOrders.Any())
            {
                result.ItemsData.totalPaidItems = totalPaidItemsitemCount;
                result.ItemsData.totalPaidItemTotal = totalPaidItemsitemValue;
            }

            var itemCount = allTickets.Sum(x => x.Orders.Sum(a => a.Quantity));
            result.ItemsData.itemCount = itemCount;

            var orderStates = allTickets
                .SelectMany(x => x.Orders)
                .SelectMany(x => x.GetOrderStateValues()).Distinct();

            result.OrdersReportData = new OrdersReportData();

            if (orderStates.Any())
            {
                result.OrdersReportData.orderStateReports = new List<StateReportDataRow>();

                foreach (var orderState in orderStates
                    .OrderBy(x => x.OrderKey)
                    .ThenBy(x => x.StateValue))
                {
                    var items = allTickets.SelectMany(x => x.Orders)
                        .Where(x => x.IsInState(orderState.StateName, orderState.State, orderState.StateValue));
                    var amount = items.Sum(x => x.GetValue());
                    var quantity = items.Sum(a => a.Quantity);
                    var count = items.Count();

                    result.OrdersReportData.orderStateReports.Add(new StateReportDataRow
                    {
                        state = orderState.State,
                        stateValue = orderState.StateValue,
                        quantity = quantity,
                        amount = amount,
                        count = count
                    });
                }
            }

            result.OrdersReportData.orderCount = allTickets.Sum(x => x.Orders.Count);

            var totalPaidItemsorderCount = totalPaidOrders.Count();
            var totalPaidItemsorderValue = totalPaidOrders.Sum(x => x.GetValue());

            if (totalPaidItemsorderCount > 0)
            {
                result.OrdersReportData.totalPaidItemsorderCount = totalPaidItemsorderCount;
                result.OrdersReportData.totalPaidItemsorderValue = totalPaidItemsorderValue;
            }

            var ticketStates = allTickets
                .SelectMany(x => x.GetTicketStateValues()).Distinct();

            var ticketCount = 0;

            result.TicketsReportData = new TicketsReportData();
            if (ticketStates.Any())
            {
                result.TicketsReportData.ticketStateReports = new List<StateReportDataRow>();

                foreach (var ticketStateValue in ticketStates)
                {
                    var value = ticketStateValue;
                    var items = allTickets.Where(x => x.IsInState(value.StateName, value.State));
                    var amount = items.Sum(x => x.GetPlainSum());

                    var discount = items
                        .SelectMany(x => x.Transactions)
                        .Where(x => x.TransactionType.Name == "DISCOUNT")
                        .Sum(e => e.Amount);

                    var count = items.Count();

                    if (amount > 0) ticketCount += count;

                    result.TicketsReportData.ticketStateReports.Add(new StateReportDataRow
                    {
                        state = ticketStateValue.State,
                        count = count,
                        amount = amount + discount
                    });
                }
            }

            var firstTicket = allTickets.Any() ? allTickets.First() : null;
            result.TicketsReportData.firstTicketNumber = firstTicket != null ? firstTicket.TicketNumber : "0";

            var lastTicket = allTickets.Any() ? allTickets.Last() : null;
            result.TicketsReportData.lastTicketNumber = lastTicket != null ? lastTicket.TicketNumber : "0";

            if (true)
                if (ticketGroups.Count() > 1)
                {
                    var data = new List<TicketTypeInfoByPaymentTypeReportData>();
                    foreach (var ticketTypeInfo in ticketGroups)
                    {
                        var dinfo = ticketTypeInfo;

                        var groups = allTickets
                            .Where(x => x.TicketTypeName == dinfo.TicketTypeName)
                            .SelectMany(x => x.Payments)
                            .GroupBy(x => new { x.PaymentType.Name })
                            .Select(x => new TenderedAmount { PaymentName = x.Key.Name, Amount = x.Sum(y => y.Amount) });

                        var ticketTypeAmountCalculator = new AmountCalculator(groups);

                        var item = new TicketTypeInfoByPaymentTypeReportData
                        {
                            ticketType = ticketTypeInfo.TicketTypeName,
                            paymentTypes = new List<PaymentTypeRowData>(),
                        };

                        foreach (var paymentName in ticketTypeAmountCalculator.PaymentNames)
                            item.paymentTypes.Add(new PaymentTypeRowData
                            {
                                paymentName = paymentName,
                                percent = ticketTypeAmountCalculator.GetPercent(paymentName),
                                amount = ticketTypeAmountCalculator.GetAmount(paymentName)
                            });

                        item.totalAmount = ticketTypeInfo.Amount;

                        var ddiscounts = allTickets
                            .Where(x => x.TicketTypeName == dinfo.TicketTypeName)
                            .Sum(x => x.GetPreTaxServicesTotal());

                        item.ddiscounts = Math.Abs(ddiscounts);

                        item.tax = ticketTypeInfo.Tax;
                    }
                }

            if (true)
            {
                if (allTickets.Where(a => a.TotalAmount > 0M).Select(x => x.GetTagData())
                    .Where(x => !string.IsNullOrEmpty(x)).Distinct().Any())
                {
                    var dict = new Dictionary<string, List<Transaction.Ticket>>();

                    foreach (var ticket in allTickets.Where(x => x.IsTagged))
                        foreach (var tag in ticket.GetTicketTagValues().Select(x => x.TagName + ":" + x.TagValue))
                        {
                            if (!dict.ContainsKey(tag))
                                dict.Add(tag, new List<Transaction.Ticket>());
                            dict[tag].Add(ticket);
                        }

                    var TicketTagGroups = await _ticketTagGroupRepository.GetAllListAsync();
                    var tagGroups =
                        dict.Select(
                            x =>
                                new TicketTagInfo
                                {
                                    Amount = x.Value.Sum(y => y.TotalAmount),
                                    TicketCount = x.Value.Count,
                                    TagName = x.Key
                                }).OrderBy(x => x.TagName);

                    var tagGrp = tagGroups.GroupBy(x => x.TagName.Split(':')[0]);

                    if (tagGrp.Any())
                    {
                        var allKeys = tagGrp.Select(a => a.Key);
                        if (TicketTagGroups.Any(a => allKeys.Contains(a.Name)))
                        {
                        }
                    }

                    var ticketTags = new List<TicketTagGroupData>();
                    foreach (var grp in tagGrp)
                    {
                        var tag = TicketTagGroups.FirstOrDefault(x => x.Name == grp.Key);
                        if (tag == null) continue;

                        var groupData = new TicketTagGroupData()
                        {
                            groupName = grp.Key,
                            subRows = new List<TicketTagInfo>()
                        };

                        groupData.isDecimal = tag.IsDecimal;
                        if (tag.IsDecimal)
                        {
                            groupData.tCount = grp.Sum(x => x.TicketCount);
                            groupData.tSum = grp.Sum(x => Convert.ToDecimal(x.TagName.Split(':')[1]) * x.TicketCount);
                            groupData.amnt = grp.Sum(x => x.Amount);
                            groupData.rate = groupData.tSum / groupData.amnt;

                            continue;
                        }

                        foreach (var ticketTagInfo in grp)
                            groupData.subRows.Add(new TicketTagInfo
                            {
                                TagName = ticketTagInfo.TagName.Split(':')[1],
                                TicketCount = ticketTagInfo.TicketCount,
                                Amount = ticketTagInfo.Amount
                            });

                        groupData.totalAmount = grp.Sum(x => x.Amount);

                        var sum = 0m;

                        groupData.isInterger = tag.IsInteger;
                        if (tag.IsInteger)
                            try
                            {
                                foreach (var my in grp.Where(a => a.Amount > 0M))
                                {
                                    var allSplit = my.TagName.Split(':');

                                    if (allSplit != null && allSplit.Length > 1)
                                        if (DineConnectUtil.IsDecimal(allSplit[1]))
                                            sum += Convert.ToDecimal(allSplit[1]) * my.TicketCount;
                                }

                                groupData.sum = sum;
                            }
                            catch (FormatException)
                            {
                            }
                        else
                            sum = grp.Sum(x => x.TicketCount);

                        if (sum > 0) groupData.average = groupData.totalAmount / sum;

                        ticketTags.Add(groupData);
                    }

                    result.TicketTagsReportData = ticketTags;
                }

                var owners = allTickets.SelectMany(ticket => ticket.Orders.Where(x => !x.IncreaseInventory).ToList())
                    .GroupBy(x => new { x.CreatingUserName })
                    .Select(
                        x =>
                            new UserInfo
                            {
                                UserName = x.Key.CreatingUserName,
                                Amount = x.Sum(y => y.GetTotal())
                            });

                if (owners.Any())
                {
                    var totalOwnersAmount = 0M;
                    foreach (var ownerInfo in owners) totalOwnersAmount += ownerInfo.Amount;

                    result.OwnersReportData = new UserInfoReportData
                    {
                        details = owners.ToList(),
                        totalOwnersAmount = totalOwnersAmount
                    };
                }
            }

            //#region RefundOwners
            var refundOwners =
                allTickets.SelectMany(ticket => ticket.Orders.Where(x => x.IncreaseInventory).ToList())
                    .GroupBy(x => new { x.CreatingUserName })
                    .Select(
                        x =>
                            new UserInfo
                            {
                                UserName = x.Key.CreatingUserName,
                                Amount = x.Sum(y => y.GetTotal())
                            });

            if (refundOwners.Any())
            {
                var totalRefundAmount = 0M;
                foreach (var ownerInfo in refundOwners) totalRefundAmount += ownerInfo.Amount;
                result.RefundOwnersReportData = new UserInfoReportData
                {
                    details = refundOwners.ToList(),
                    totalOwnersAmount = totalRefundAmount
                };
            }

            //#endregion
            if (true)
            {
                var uInfo =
                    allTickets.SelectMany(x => x.Payments)
                        .Select(x => x.PaymentUserName)
                        .Distinct()
                        .Select(x => new UserInfo { UserName = x });

                if (uInfo.Count() > 1)
                {
                    var co = 0;
                    var paymentByUsersReportData = new List<UserNameByPaymentTypeReportData>();
                    foreach (var userInfo in uInfo)
                    {
                        var userIncomeCalculator = GetIncomeCalculatorByUser(allTickets, userInfo.UserName);
                        if (userIncomeCalculator.TotalAmount > 0M)
                        {
                            var item = new UserNameByPaymentTypeReportData
                            {
                                paymentUserName = userInfo.UserName,
                                paymentTypes = new List<PaymentTypeRowData>(),
                                totalAmount = userIncomeCalculator.TotalAmount
                            };

                            foreach (var paymentName in userIncomeCalculator.PaymentNames)
                                item.paymentTypes.Add(new PaymentTypeRowData
                                {
                                    paymentName = paymentName,
                                    percent = userIncomeCalculator.GetPercent(paymentName),
                                    amount = userIncomeCalculator.GetAmount(paymentName)
                                });

                            paymentByUsersReportData.Add(item);
                        }
                    }

                    result.UsersIncomeReportData = paymentByUsersReportData;
                }
            }
        }

        private AmountCalculator GetIncomeCalculatorByUser(IEnumerable<Transaction.Ticket> tickets,
            string paymentUserName)
        {
            var groups = tickets
                .SelectMany(x => x.Payments.Where(y => y.PaymentUserName == paymentUserName))
                .Where(x => x.Amount >= 0)
                .GroupBy(x => x.PaymentTypeId)
                .Select(x => new TenderedAmount
                { PaymentName = GetPaymentTypeName(x.Key), Amount = x.Sum(y => y.Amount) });

            return new AmountCalculator(groups);
        }

        private async Task<List<SalesItemSumaryDto>> GetPeriodData(IEnumerable<Transaction.Ticket> tickets, Period.WorkPeriod period)
        {
            var schedules = await GetScheduleSetting(period.LocationId);

            if (schedules != null)
            {
                var result = schedules.Select(s =>
                {
                    var ticketBySchedule = tickets.Where(i =>
                        i.LastPaymentTime.TimeOfDay > s.FromTime && i.LastPaymentTime.TimeOfDay < s.ToTime);
                    var data = new SalesItemSumaryDto
                    {
                        Name = s.ScheduleName,
                        ItemCount = ticketBySchedule.SelectMany(t => t.Orders).Distinct().Sum(a => a.Quantity),
                        Amount = ticketBySchedule.SelectMany(t => t.Orders).Distinct().Sum(a => a.Quantity * a.Price),
                        TicketCount = ticketBySchedule.Count()
                    };
                    return data;
                }).ToList();
                return result;
            }

            return null;
        }

        private async Task<List<ScheduleReportData>> GetScheduleSetting(int locationId)
        {
            var location = _locationRepo.Get(locationId);
            var allSchedules = location.Schedules;

            if (allSchedules != null && allSchedules.Any())
            {
                List<ScheduleReportData> sch = new List<ScheduleReportData>();

                foreach (var e in allSchedules)
                {
                    ScheduleReportData s1 = new ScheduleReportData()
                    {
                        FromTime = new TimeSpan(e.StartHour, e.StartMinute - 1, 59),
                        ToTime = new TimeSpan(e.EndHour, e.EndMinute, 0),
                    };
                    s1.ScheduleName = $@"{e.Name}";
                    sch.Add(s1);
                }
                return sch;
            }
            else
            {
                var scheduleSetting = await SettingManager.GetSettingValueAsync(AppSettings.ConnectSettings.Schedules);

                if (!string.IsNullOrEmpty(scheduleSetting))
                {
                    List<ScheduleSetting> schedules;
                    schedules = JsonConvert.DeserializeObject<List<ScheduleSetting>>(scheduleSetting);
                    var listScheduleReport = schedules.Select(e =>
                    {
                        var dto = new ScheduleReportData
                        {
                            FromTime = new TimeSpan(e.StartHour, e.StartMinute - 1, 59),
                            ToTime = new TimeSpan(e.EndHour, e.EndMinute, 0),
                        };

                        dto.ScheduleName = $@"{e.Name}";
                        return dto;
                    }).OrderBy(e => e.FromTime).ToList();

                    return listScheduleReport;
                }
            }

            return null;
        }

        private List<SalesItemSumaryDto> GetDiscountData(IEnumerable<Transaction.Ticket> tickets)
        {
            var ticketPros = tickets.Where(t => t.TicketPromotionDetails != null)
                .SelectMany(
                    a => JsonConvert.DeserializeObject<List<PromotionDetailValue>>(a.TicketPromotionDetails))
                .ToList();

            var orderPros = tickets.SelectMany(t => t.Orders)
                .Where(t => t.OrderPromotionDetails != null)
                .SelectMany(a => JsonConvert.DeserializeObject<List<PromotionDetailValue>>(a.OrderPromotionDetails))
                .ToList();

            var allPromos = ticketPros.Concat(orderPros).ToList();

            var result = allPromos.GroupBy(p => p.PromotionName).Select(s =>
            {
                var data = new SalesItemSumaryDto
                {
                    Name = s.Key,
                    Amount = s.Sum(a => a.PromotionAmount),
                    TicketCount = s.Count()
                };
                return data;
            }).ToList();
            return result;
        }

        #endregion Private Methods
    }
}