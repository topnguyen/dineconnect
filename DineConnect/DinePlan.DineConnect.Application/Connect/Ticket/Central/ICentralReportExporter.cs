﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Ticket.Central.Dto;
using DinePlan.DineConnect.Connect.Ticket.Implementation;
using DinePlan.DineConnect.Connect.WorkPeriod.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Ticket.Central
{
    public interface ICentralReportExporter
    {
        Task<FileDto> CentralDayExport(CentralExportInput input, ICentralReportAppService centralAppService);        
        Task<FileDto> DailyReconciliationExportAll(WorkPeriodSummaryInput input, ICentralReportAppService centralAppService); 
        Task<FileDto> CentralShiftDayExport(CentralShiftExportInput input, ICentralReportAppService centralAppService);
        Task<FileDto> CentralTerminalDayExport(CentralTerminalExportInput input, ICentralReportAppService centralAppService);

    }
}
