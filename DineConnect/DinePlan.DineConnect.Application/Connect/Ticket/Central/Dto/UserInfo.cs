﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.Central.Dto
{
    public class UserInfo
    {
        public string UserName { get; set; }
        public decimal Amount { get; set; }
        public int UserId { get; set; }
    }
}
