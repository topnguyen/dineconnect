﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.Central.Dto
{
    public class OrdersReportData
    {
        public List<StateReportDataRow> orderStateReports { get; set; }

        public int orderCount { get; set; }

        public int totalPaidItemsorderCount { get; set; }

        public decimal totalPaidItemsorderValue { get; set; }
    }
}
