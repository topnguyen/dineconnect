﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.Central.Dto
{
    public class CashReportData
    {
        public int cashIn { get; set; }
        public decimal allIncomeAmount { get; set; }
        public int cashOut { get; set; }
        public decimal allExpAmount { get; set; }
        public decimal cashInDrawer { get; set; }
        public decimal cashAmount { get; set; }
    }
}
