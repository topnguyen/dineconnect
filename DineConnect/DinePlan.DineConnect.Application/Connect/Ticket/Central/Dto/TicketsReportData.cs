﻿using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Ticket.Central.Dto
{
    public class TicketsReportData
    {
        public List<StateReportDataRow> ticketStateReports;

        public string firstTicketNumber;

        public string lastTicketNumber;
    }
}