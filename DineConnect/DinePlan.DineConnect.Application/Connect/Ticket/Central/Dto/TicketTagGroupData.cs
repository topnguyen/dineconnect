﻿using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Ticket.Implementation
{
    public class TicketTagGroupData
    {
        public int tCount { get; set; }

        public decimal tSum { get; set; }

        public decimal amnt { get; set; }

        public decimal rate { get; set; }

        public decimal sum { get; set; }

        public decimal average { get; set; }

        public List<TicketTagInfo> subRows { get; set; }

        public TicketTagGroupData()
        {
        }

        public string groupName { get; set; }

        public decimal totalAmount { get; set; }

        public bool isDecimal { get; set; }

        public bool isInterger { get; set; }
    }

    public class TicketTagInfo
    {
        public string TagName { get; set; }

        public int TicketCount { get; set; }

        public decimal Amount { get; set; }
    }
}