﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.Central.Dto
{
    public class ItemsData
    {
        public decimal totalPaidItems { get; set; }

        public decimal totalPaidItemTotal { get; set; }

        public decimal itemCount { get; set; }
    }
}
