﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.Central.Dto
{
    public class StateReportDataRow
    {
        public int count;

        public string state { get; set; }

        public string stateValue { get; set; }

        public decimal quantity { get; set; }

        public decimal amount { get; set; }
    }
}
