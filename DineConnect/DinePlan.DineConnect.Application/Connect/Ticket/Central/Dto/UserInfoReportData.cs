﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.Central.Dto
{
    public class UserInfoReportData
    {
        public List<UserInfo> details { get; set; }
        public decimal totalOwnersAmount { get; set; }
    }
}
