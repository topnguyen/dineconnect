﻿using Abp.Runtime.Validation;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;

namespace DinePlan.DineConnect.Connect.Ticket.Central.Dto
{

    public class CentralShiftReportInput :IFileExport, IBackgroundExport
    {
        public int workPeriodId { get; set; }
        public string workShiftId { get; set; }
        public DateTime DateReport { get; set; }
        public bool RunInBackground { get; set; }
        public int TenantId { get; set; }
        public long UserId { get; set; }
        public string ReportDescription { get; set; }
        public ExportType ExportOutputType { get; set; }
        public bool Portrait { get; set; }
        public OpenHtmlToPdf.PaperSize PaperSize { get; set; }
    }

    public class CentralTerminalReportInput
    {
        public int workPeriodId { get; set; }

        public List<string> terminals { get; set; }
    }
}
