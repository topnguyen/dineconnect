﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.Central.Dto
{
    public class PaymentTypeReportData
    {
        public decimal totalSales { get; set; }

        public List<PaymentTypeRowData> details { get; set; }
    }
}
