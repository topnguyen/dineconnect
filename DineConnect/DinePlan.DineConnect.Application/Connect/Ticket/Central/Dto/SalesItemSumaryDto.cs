﻿namespace DinePlan.DineConnect.Connect.Ticket.Central.Dto
{
    public class SalesItemSumaryDto
    {
        public string Name { get; set; }

        public decimal Amount { get; set; }
        public decimal TaxAmount { get; set; }

        public decimal Discount { get; set; }

        public decimal ItemCount { get; set; }
        public decimal TicketCount { get; set; }
        public decimal NetSales => Amount- Discount - TaxAmount;
    }
}