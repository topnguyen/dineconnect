﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.Central.Dto
{
    public class TicketCountByTicketTypeData
    {
        public string ticketTypeName { get; set; }

        public int count { get; set; }
    }
}
