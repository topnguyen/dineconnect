﻿using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Ticket.Central.Dto
{
    public class WorkTimePaTable
    {
        public decimal wactualTotal { get; set; }
        public decimal winSalesTotal { get; set; }
        public decimal wdifferenceTotal { get; set; }
        public List<PaymentTypeByWorkTimeRowData> details { get; set; }
    }
}