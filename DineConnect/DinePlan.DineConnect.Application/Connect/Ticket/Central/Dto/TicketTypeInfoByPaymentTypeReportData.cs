﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.Central.Dto
{
   public class TicketTypeInfoByPaymentTypeReportData
    {
        internal object ddiscounts;
        internal decimal tax;

        public string ticketType { get; set; }

        public List<PaymentTypeRowData> paymentTypes { get; set; }

        public decimal totalAmount { get; set; }
    }
}
