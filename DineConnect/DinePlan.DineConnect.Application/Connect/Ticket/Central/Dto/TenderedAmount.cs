﻿namespace DinePlan.DineConnect.Connect.Ticket.Central.Dto
{
    public class TenderedAmount
    {
        public string PaymentName { get; set; }
        public decimal Amount { get; set; }
        public decimal ActualAmount { get; set; }
        public int PaymentCount { get; set; }
    }
}