﻿namespace DinePlan.DineConnect.Connect.Ticket.Central.Dto
{
    public class PaymentTypeRowData
    {
        public string paymentName { get; set; }

        public int count { get; set; }

        public decimal percent { get; set; }

        public decimal amount { get; set; }

        public decimal actualSales { get; set; }
    }

    public class PaymentTypeByWorkTimeRowData
    {
        public string paymentName { get; set; }

        public decimal actualTotal { get; set; }

        public decimal inSalesTotal { get; set; }

        public decimal differenceTotal { get; set; }
    }
}