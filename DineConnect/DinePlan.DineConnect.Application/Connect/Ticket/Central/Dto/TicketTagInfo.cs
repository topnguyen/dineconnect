﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.Central.Dto
{
    public class TicketTagInfo
    {
        private string _tagName;
        public decimal Amount { get; set; }
        public int TicketCount { get; set; }
        public List<Transaction.Ticket> TicketList { get; set; }

        public string TagName
        {
            get { return !string.IsNullOrEmpty(_tagName) ? _tagName.Trim() : "[Ticket]"; }
            set { _tagName = value; }
        }

    }
}
