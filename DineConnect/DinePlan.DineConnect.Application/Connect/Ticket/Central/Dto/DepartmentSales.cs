﻿namespace DinePlan.DineConnect.Connect.Ticket.Central.Dto
{
    public class DepartmentSales
    {
        public string Name { get; set; }
        public string TotalTicketCount { get; set; }
        public decimal Amount { get; set; }
        public decimal ItemCount { get; set; }
        public decimal NetSales => Amount - TaxTotal;

        public decimal TaxTotal { get; set; }


    }
}