﻿using DinePlan.DineConnect.Connect.Ticket.Implementation;
using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Master.Dtos;

namespace DinePlan.DineConnect.Connect.Ticket.Central.Dto
{
    public class CentralDayReportData
    {
        public LocationListDto LocationDetails{ get; set; }

        public TicketTypeReportData TicketTypeReportData { get; set; }

        public List<object> Departments { get; set; }

        public PaymentTypeReportData PaymentTypeReportData { get; set; }

        public CashReportData CashReportData { get; internal set; }

        public WorkTimePaTable WorkTimePaTable { get; internal set; }

        public PromotionReportData TicketsPromotionReportData { get; set; }

        public PromotionReportData OrdersPromotionReportData { get; set; }

        public List<TicketCountByTicketTypeData> TicketCountByTicketTypeData { get; set; }

        public ItemsData ItemsData { get; set; }

        public OrdersReportData OrdersReportData { get; set; }

        public TicketsReportData TicketsReportData { get; set; }

        public UserInfoReportData OwnersReportData { get; set; }

        public UserInfoReportData RefundOwnersReportData { get; set; }

        public List<UserNameByPaymentTypeReportData> UsersIncomeReportData { get; internal set; }

        public List<TicketTagGroupData> TicketTagsReportData { get; set; }

        public List<SalesItemSumaryDto> SalesGroups { get; set; }

        public List<SalesItemSumaryDto> SalesCategories { get; set; }
        public List<SalesItemSumaryDto> PeriodData { get; set; }
        public List<SalesItemSumaryDto> DiscountData { get; set; }

        public CentralDayReportData()
        {
            SalesGroups = new List<SalesItemSumaryDto>();
            SalesCategories = new List<SalesItemSumaryDto>();
            PeriodData = new List<SalesItemSumaryDto>();
            DiscountData = new List<SalesItemSumaryDto>();
            UsersIncomeReportData = new List<UserNameByPaymentTypeReportData>();
            TicketTagsReportData = new List<TicketTagGroupData>();
            TicketCountByTicketTypeData = new List<TicketCountByTicketTypeData>();
            Departments = new List<object>();
        }
    }
}