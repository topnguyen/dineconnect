﻿using DinePlan.DineConnect.Connect.WorkPeriod.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DinePlan.DineConnect.Connect.Ticket.Central.Dto
{
    public class DailyReconciliationDto
    {
        public string PaymentType { get; set; }
        public decimal Portion { get; set; }
        public decimal SystemAmount { get; set; }
        public decimal ActualAmount { get; set; }
        public decimal Difference { get; set; }
        public string Reason { get; set; }
        public string Note { get; set; }
        public string WorkShift { get; set; }
    }

    public class DailyReconciliationReportOutput
    {
        public DailyReconciliationReportOutputDto Data { get; set; }
        public List<DailyReconciliationReportOutputDto> AllWorkShiftData { get; set; }
        public int NumberOfBill { get; set; }
        public decimal AvgPerBill
        {
            get
            {
                return NumberOfBill != 0 ? Math.Round(TotalSales / NumberOfBill, 2) : 0;
            }
        }
        public decimal TotalSales { get; set; }
        public WorkPeriodListDto WorkPeriodInfo { get; set; }
    }

    public class DailyReconciliationReportAllOutput
    {
        public string LocationName { get; set; }
        public string LocationCode { get; set; }
        public DateTime Date { get; set; }

        public List<DailyReconciliationReportOutput> Details { get; set; }

        public DailyReconciliationReportOutput Summary
        {
            get
            {
                var result = new DailyReconciliationReportOutput()
                {
                    NumberOfBill = Details.Sum(d => d.NumberOfBill),
                    TotalSales = Details.Sum(d => d.TotalSales),
                    Data = new DailyReconciliationReportOutputDto()
                };

                var paymentTypes = Details.Where(t => t.Data.WorkShiftDailyReconciliationList != null).SelectMany(t => t.Data.WorkShiftDailyReconciliationList).GroupBy(t => t.PaymentType).ToList();
                paymentTypes.ForEach(p =>
                {
                    result.Data.WorkShiftDailyReconciliationList.Add(new DailyReconciliationDto()
                    {
                        PaymentType = p.Key,
                        SystemAmount = p.ToList().Sum(t => t.SystemAmount),
                        ActualAmount = p.ToList().Sum(t => t.ActualAmount),
                        Difference = p.ToList().Sum(t => t.Difference),

                    });
                });
                var total = new DailyReconciliationDto()
                {
                    PaymentType = "Total",
                    ActualAmount = result.Data.WorkShiftDailyReconciliationList.Sum(t => t.ActualAmount),
                    SystemAmount = result.Data.WorkShiftDailyReconciliationList.Sum(t => t.SystemAmount),
                    Difference = result.Data.WorkShiftDailyReconciliationList.Sum(t => t.Difference),
                    Portion = 100,
                    WorkShift = null
                };
                result.Data.ShiftBasedTotalDailyReconciliation = total;
                foreach (var item in result.Data.WorkShiftDailyReconciliationList)
                {
                    item.Portion = total.SystemAmount != 0 ? Math.Round(item.SystemAmount / total.SystemAmount * 100, 2) : 0;
                }

                return result;
            }

        }
    }

    public class DailyReconciliationReportOutputDto
    {
        public DailyReconciliationReportOutputDto()
        {
            WorkShiftDailyReconciliationList = new List<DailyReconciliationDto>();
            ShiftBasedTotalDailyReconciliation = new DailyReconciliationDto();
        }
        public string WorkShiftTiming { get; set; }
        public List<DailyReconciliationDto> WorkShiftDailyReconciliationList { get; set; }
        public DailyReconciliationDto ShiftBasedTotalDailyReconciliation { get; set; }
    }

    public class GetDailyReconciliationAllInput
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}