﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.Central.Dto
{
    public class PromotionReportData
    {
        public decimal totalCount { get; set; }

        public decimal totalSum { get; set; }

        public List<PromotionReportRowData> details { get; set; }

        public PromotionReportData()
        {
            details = new List<PromotionReportRowData>();
        }
    }

    public class PromotionReportRowData
    {
        public string promotionName { get; set; }

        public decimal quantity { get; set; }

        public decimal amount { get; set; }
    }
}
