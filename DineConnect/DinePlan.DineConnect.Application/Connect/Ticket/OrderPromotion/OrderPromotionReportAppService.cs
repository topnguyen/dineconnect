﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Ticket.OrderPromotion.Exporting;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Report;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.OrderPromotion
{
    public class OrderPromotionReportAppService : DineConnectAppServiceBase, IOrderPromotionReportAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IConnectReportAppService _connectReportAppService;
        private readonly IRepository<Promotion> _promotionRepository;
        private readonly IBackgroundJobManager _backgroundJobManager;
        private readonly IReportBackgroundAppService _reportBackgroundAppService;
        private readonly IOrderPromotionReportExporter _exporter;

        public OrderPromotionReportAppService(
            IUnitOfWorkManager unitOfWorkManager
            , IConnectReportAppService connectReportAppService
            , IRepository<Promotion> promotionRepository
            , IBackgroundJobManager backgroundJobManager
            , IReportBackgroundAppService reportBackgroundAppService
            , IOrderPromotionReportExporter exporter
            )
        {
            _unitOfWorkManager = unitOfWorkManager;
            _connectReportAppService = connectReportAppService;
            _promotionRepository = promotionRepository;
            _backgroundJobManager = backgroundJobManager;
            _reportBackgroundAppService = reportBackgroundAppService;
            _exporter = exporter;
        }

        public async Task<OrderStatsDto> GetPromotionOrders(GetItemInput input)
        {
            var returnOutput = new OrderStatsDto();
            var allOrders = _connectReportAppService.GetAllOrders(input);
            var orders = allOrders.Where(a => a.IsPromotionOrder 
                                              && !string.IsNullOrEmpty(a.OrderPromotionDetails)
                                              && a.CalculatePrice 
                                              && a.DecreaseInventory);

            if (input.Promotions != null && input.Promotions.Any())
            {
                var allIds = input.Promotions.Select(a => a.Id);
                var searchedList = (await orders.ToListAsync()).Where(a => allIds.Any(p => a.GetOrderPromotionList().Select(e => e.PromotionSyncId).Contains(p)));

                orders = searchedList.AsQueryable();
            }

            returnOutput.DashBoardDto = GetPromoStatics(orders);
            var sortTickets = orders.OrderBy(input.Sorting).PageBy(input);
            List<OrderListDto> outputDtos = sortTickets.ToList().MapTo<List<OrderListDto>>();

            var menuItemCount = orders.Count();
            returnOutput.OrderList = new PagedResultOutput<OrderListDto>(
                menuItemCount,
                outputDtos
            );
            return returnOutput;
        }

        public async Task<FileDto> GetPromotionOrdersExcel(GetItemInput input)
        {
            input.OutputType = "EXPORT";

            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _reportBackgroundAppService.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.ORDERPROMOTION,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                        await _backgroundJobManager.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.ORDERPROMOTION,
                            ItemInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                }
                else
                {
                    return await ExportInBackground(input);
                }
            }

            return null;
        }

        public async Task<FileDto> ExportInBackground(GetItemInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                return await _exporter.ExportToFilePromotionOrders(input, this);
            }
        }


        private DashboardOrderDto GetPromoStatics(IQueryable<Order> orders)
        {
            var orderDto = new DashboardOrderDto();
            try
            {
                if (!orders.Any()) return orderDto;
                orderDto.TotalAmount = orders.DefaultIfEmpty().Sum(a => a.PromotionAmount * a.Quantity);
                orderDto.TotalOrderCount = orders.Count();
                orderDto.TotalItemSold = orders.Sum(a => a.Quantity);
            }
            catch (Exception ex)
            {
                // ignored
            }

            return orderDto;
        }
    }
}