﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.OrderPromotion.Exporting
{
    public interface IOrderPromotionReportExporter
    {
        Task<FileDto> ExportToFilePromotionOrders(GetItemInput input, IOrderPromotionReportAppService appService);
    }
}