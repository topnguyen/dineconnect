﻿using System;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;

namespace DinePlan.DineConnect.Connect.Ticket.OrderPromotion.Exporting
{
    public class OrderPromotionReportExporter : FileExporterBase, IOrderPromotionReportExporter
    {
        private readonly ITenantSettingsAppService _tenantSettingsService;
        public OrderPromotionReportExporter(ITenantSettingsAppService tenantSettingsService)
        {
            _tenantSettingsService = tenantSettingsService;
        }
        public async Task<FileDto> ExportToFilePromotionOrders(GetItemInput input,
            IOrderPromotionReportAppService appService)
        {
            var file = new FileDto("Orders-" + DateTime.Now.ToString("yyyy-MMMM-dd") + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                var setting = await _tenantSettingsService.GetAllSettings();
                var dateTimeFormat = setting.Connect.DateTimeFormat;
                var dateFormat = setting.Connect.SimpleDateFormat;
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Promotion"));
                sheet.OutLineApplyStyle = true;

                AddHeader(
                    sheet,
                    L("Location"),
                    L("TicketId"),
                    L("Order"),
                    L("Department"),
                    L("PromotionName"),
                    L("MenuItem"),
                    L("Portion"),
                    L("Quantity"),
                    L("UnitPrice"),
                    L("Unit") + " " + L("Discounted") + " " + L("Price"),
                    L("Unit") + " " + L("PromotionAmount"),
                    L("Total") +" "+ L("PromotionAmount"),
                    L("LineTotal"),
                    L("User"),
                    L("Date"),
                    L("Time"),
                    L("Note")
                );
                var rowCount = 2;
                var colCount = 1;
                var totalPromoAmount = 0M;
                var totalQuantity = 0M;
                var secondTime = false;
                var ticketAvailable = false;
                while (true)
                {
                    input.MaxResultCount = 100;
                    if (secondTime) input.NotCorrectDate = true;
                    var orderDetailDtos = await appService.GetPromotionOrders(input);
                    if (!secondTime) secondTime = true;

                    if (DynamicQueryable.Any(orderDetailDtos.OrderList.Items))
                    {
                        ticketAvailable = true;
                        totalQuantity += orderDetailDtos.OrderList.Items.Sum(a => a.Quantity);
                        foreach (var order in orderDetailDtos.OrderList.Items)
                        {
                            decimal UnitPromotionAmount=order.OriginalPrice-order.Price;
                            foreach (var promotionDetailValue in order.GetPromotionDetails())
                            {
                                colCount = 1;
                                sheet.Cells[rowCount, colCount++].Value = order.LocationName;
                                sheet.Cells[rowCount, colCount++].Value = order.TicketId;
                                sheet.Cells[rowCount, colCount++].Value = order.OrderId;
                                sheet.Cells[rowCount, colCount++].Value = order.DepartmentName;
                                sheet.Cells[rowCount, colCount++].Value = promotionDetailValue.PromotionName;
                                sheet.Cells[rowCount, colCount++].Value = order.MenuItemName;
                                sheet.Cells[rowCount, colCount++].Value = order.PortionName;
                                sheet.Cells[rowCount, colCount++].Value = order.Quantity;
                                sheet.Cells[rowCount, colCount++].Value = order.OriginalPrice;
                                sheet.Cells[rowCount, colCount++].Value = order.Price;
                                sheet.Cells[rowCount, colCount++].Value = UnitPromotionAmount;
                                sheet.Cells[rowCount, colCount++].Value =
                                    promotionDetailValue.PromotionAmount * order.Quantity;
                                sheet.Cells[rowCount, colCount++].Value =
                                    order.Quantity * (order.Price + promotionDetailValue.PromotionValue);
                                sheet.Cells[rowCount, colCount++].Value = order.CreatingUserName;
                                sheet.Cells[rowCount, colCount++].Value =
                                    order.OrderCreatedTime.ToString(dateFormat);
                                sheet.Cells[rowCount, colCount++].Value = order.OrderCreatedTime.ToString(dateFormat);
                                sheet.Cells[rowCount, colCount++].Value = promotionDetailValue.PromotionNote;

                                totalPromoAmount += promotionDetailValue.PromotionAmount * order.Quantity;
                                rowCount++;
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                   
                    input.SkipCount += input.MaxResultCount;
                }

                if (ticketAvailable)
                {
                    sheet.Cells[rowCount, 1].Value = L("Total");
                    sheet.Cells[rowCount, 8].Value = totalQuantity;
                    sheet.Cells[rowCount, 12].Value = totalPromoAmount;
                }

                for (var i = 1; i <= colCount + 2; i++) sheet.Column(i).AutoFit();
                    Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }
    }
}