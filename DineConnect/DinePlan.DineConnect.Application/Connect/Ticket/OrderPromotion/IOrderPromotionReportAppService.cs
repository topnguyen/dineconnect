﻿using Abp.Application.Services;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.OrderPromotion
{
    public interface IOrderPromotionReportAppService   : IApplicationService
    {
        Task<FileDto> ExportInBackground(GetItemInput input);

        Task<OrderStatsDto> GetPromotionOrders(GetItemInput input);

        Task<FileDto> GetPromotionOrdersExcel(GetItemInput input);
    }
}