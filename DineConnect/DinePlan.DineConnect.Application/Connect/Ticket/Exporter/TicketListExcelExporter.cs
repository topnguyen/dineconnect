﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Features;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Castle.Core.Internal;
using DinePlan.DineConnect.Authorization.Users;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Ticket.Implementation;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Connect.WorkPeriod.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.Handler.Mapping;
using DinePlan.DineConnect.Features;
using DinePlan.DineConnect.Net.MimeTypes;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace DinePlan.DineConnect.Connect.Ticket.Exporter
{
    public class TicketListExcelExporter : FileExporterBase, ITicketListExcelExporter
    {
        private readonly ILocationAppService _locationAppService;
        private readonly SettingManager _settingManager;
        private readonly IRepository<Company> _comRepo;
        private readonly IRepository<Master.Location> _locRepo;
        private string timeSpanFormat = "hh\\:mm\\:ss";

        public TicketListExcelExporter(SettingManager settingManager,
            IRepository<Master.Location> loRepository
            , IRepository<Company> comRepository
            , ILocationAppService locationAppService
        )
        {
            _locRepo = loRepository;
            _comRepo = comRepository;
            _locationAppService = locationAppService;
            _settingManager = settingManager;
            _roundDecimals = _settingManager.GetSettingValue<int>(AppSettings.ConnectSettings.Decimals);
        }

        public IFeatureChecker FeatureChecker { protected get; set; }
        public UserManager UserManager { get; set; }

        public async Task<FileDto> ExportTickets(GetTicketInput input, IConnectReportAppService appService,
            int tenantId)
        {
            var builder = new StringBuilder();
            builder.Append(L("Tickets"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append(" - ");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                await GetTickets(excelPackage, input, appService, null, null, null, null, tenantId);
                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

        public async Task<FileDto> ExportTickets(GetTicketInput input, IConnectReportAppService appService,
            List<string> tt, List<string> pp, List<string> tagnames, int tenantId)
        {
            var builder = new StringBuilder();
            builder.Append(L("Tickets"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append("_");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                if (input.Refund)
                    await GetRefundTickets(excelPackage, input, appService, tt, pp, null, tagnames);
                else
                    await GetTickets(excelPackage, input, appService, tt, pp, null, tagnames, tenantId);

                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

        public async Task<FileDto> ExportTicketSyncs(GetTicketSyncInput input, IConnectReportAppService appService)
        {
            var builder = new StringBuilder();
            builder.Append(L("TicketSyncs"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append("_");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                await GetTicketSyncs(excelPackage, input, appService);


                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }


        public async Task<FileDto> ExportRefundTicketDetail(GetTicketInput input, IConnectReportAppService appService)
        {
            var builder = new StringBuilder();
            builder.Append(L("RefundDetail"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append("_");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                var dtos = await appService.GetAllTicketsForTicketInput(input)
                    .SelectMany(t => t.Orders.Select(o => new RefundTicketDetail
                    {
                        Id = t.Id,
                        TicketId = t.TicketId,
                        LastPaymentTime = t.LastPaymentTime,
                        InvoiceNo = t.InvoiceNo,
                        DepartmentName = t.DepartmentName,
                        Note = t.Note,
                        Employee = t.LastModifiedUserName,
                        TerminalName = t.TerminalName,
                        TotalAmount = t.TotalAmount,
                        Quantity = o.Quantity,
                        MenuItem = o.MenuItemName,
                        SubAmount = o.Price * o.Quantity - o.PromotionAmount
                    })).Distinct()
                    .OrderBy(t => new { t.LastPaymentTime, t.InvoiceNo })
                    .ToListAsync();
                GetRefundTicketDetail(excelPackage, input, dtos, input.DateFormat, input.DatetimeFormat);

                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

        public async Task<FileDto> ExportOrders(GetItemInput input, IConnectReportAppService appService)
        {
            var file = new FileDto(
                "Orders_" + input.StartDate.ToString("yyyy-MM-dd") + "_" + input.EndDate.ToString("yyyy-MM-dd") +
                ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            if (input.Void)
                return await EportVoidOrders(input, appService, file);
            return await EportOrdersSummary(input, appService, file);
        }

        public async Task<FileDto> ExportExchangeReport(GetItemInput input, IConnectReportAppService appService)
        {
            var file = new FileDto(
                "Exchange_Report_" + input.StartDate.ToString("yyyy-MM-dd") + "_" +
                input.EndDate.ToString("yyyy-MM-dd") + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            if (input.ExportOutputType == 0)
            {
                using (var excelPackage = new ExcelPackage())
                {
                    await GetExportExchange(excelPackage, input, appService);
                    Save(excelPackage, file);
                }

                return ProcessFile(input, file);
            }

            var contentHTML = await ExportDatatableToHtml(input, appService);
            var result = ProcessFileHTML(file, contentHTML);
            return result;
        }

        public async Task<FileDto> ExportToDepartmentSummary(GetTicketInput input, IConnectReportAppService appService,
            List<string> tt, List<string> pp)
        {
            var builder = new StringBuilder();
            builder.Append(L("Departments"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append(" - ");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                await GetDepartmentSummary(excelPackage, input, appService, tt, pp);
                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

        public async Task<FileDto> ExportToFileItems(GetItemInput input, IConnectReportAppService appService)
        {
            var builder = new StringBuilder();

            if (input.Void)
                builder.Append(L("Void"));
            else if (input.Gift)
                builder.Append(L("Gift"));
            else if (input.Comp)
                builder.Append(L("Comp"));
            else
                builder.Append(L("Item"));
            if (input.ByLocation)
            {
                builder.Append("-");

                builder.Append(L("Location"));
                builder.Append("-");
                builder.Append(input.StartDate.Month);
            }
            else
            {
                if (input?.Duration != null && input.Duration.Equals("M"))
                {
                    builder.Append(L("Summary"));
                    builder.Append("-");
                    builder.Append(input.StartDate.Month);
                }
                else
                {
                    builder.Append("-");
                    builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                        ? input.StartDate.ToString(_simpleDateFormat)
                        : DateTime.Now.ToString(_simpleDateFormat));
                    builder.Append(" - ");
                    builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                        ? input.EndDate.ToString(_simpleDateFormat)
                        : DateTime.Now.ToString(_simpleDateFormat));
                }
            }

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                if (!input.ByLocation)
                    await GetItemsForDate(excelPackage, input, appService);
                else
                    await GetItemsForLocation(excelPackage, input, appService);
                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

        public async Task<FileDto> ExportToFileItemCombos(GetItemInput input, IConnectReportAppService appService)
        {
            var file = new FileDto(
                "ItemCombos_" + input.StartDate.ToString("yyyy-MM-dd") + "_" + input.EndDate.ToString("yyyy-MM-dd") +
                ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                await GetItemComboSales(excelPackage, input, appService);
                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

        public async Task<FileDto> ExportToFileItemsByPrice(GetItemInput input, IConnectReportAppService appService)
        {
            var builder = new StringBuilder();
            builder.Append(L("Items"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append(" - ");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                await GetItemsSummaryByPrice(excelPackage, input, appService);
                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

        public async Task<FileDto> ExportToFileLocationSales(GetTicketInput input, IConnectReportAppService _appService)
        {
            var builder = new StringBuilder();
            builder.Append(L("ThailandSalesExcel"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append(" - ");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                await GetLocationSaleSummary(excelPackage, input, _appService);
                Save(excelPackage, file);
            }

            return ProcessFileUsingSpire(input, file);
        }

        public async Task<FileDto> ExportToFileLocationSalesDetail(GetTicketInput input,
            IConnectReportAppService _appService)
        {
            var builder = new StringBuilder();
            builder.Append(L("ThailandSalesDetailExcel"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append(" - ");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                await GetLocationSaleDetailSummary(excelPackage, input, _appService);
                Save(excelPackage, file);
            }

            return ProcessFileUsingSpire(input, file);
        }

        public async Task<FileDto> ExportToFileItemsHours(GetItemInput input, IConnectReportAppService _appService)
        {
            var builder = new StringBuilder();
            builder.Append(L("ItemHourlySalesExcel"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append(" - ");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                await GetItemsHoursSaleSummary(excelPackage, input, _appService);
                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

        public async Task<FileDto> ExportToFileItemsSummary(GetItemInput input, IConnectReportAppService appService)
        {
            var builder = new StringBuilder();
            builder.Append(L("Items"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append(" - ");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                await GetItemsSummary(excelPackage, input, appService);
                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

        public async Task<FileDto> ExportToFileItemAndOrderTagSales(GetItemInput input,
            IConnectReportAppService appService)
        {
            var builder = new StringBuilder();
            builder.Append(L("Items"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append(" - ");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                await CreateFileForItemAndOrderTagSales(excelPackage, input, appService);
                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

        public async Task<FileDto> ExportItemSalesForWorkPeriodExcel(GetItemInput input,
            ConnectReportAppService _appServie, string location)
        {
            var file =
                new FileDto("WorkPeriod_Items-" + location + "-" + input.StartDate.ToString("yyyy-MMMM-dd") + ".xlsx",
                    MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            var allPeriods = await _appServie.GetWorkPeriodByLocationForDates(new WorkPeriodSummaryInput
            {
                StartDate = input.StartDate,
                EndDate = input.EndDate,
                LocationId = input.Location
            });

            if (allPeriods.Any())
                using (var excelPackage = new ExcelPackage())
                {
                    foreach (var period in allPeriods)
                    {
                        var sheet =
                            excelPackage.Workbook.Worksheets.Add(period.StartTime.ToShortTimeString() + "-" +
                                                                 period.Wid +
                                                                 ":");
                        sheet.OutLineApplyStyle = true;
                        var row = 1;

                        AddDetail(sheet, row, 1, L("StartTime"));
                        AddDetail(sheet, row, 2, period.StartTime.ToString("f"));

                        AddDetail(sheet, row, 3, L("EndTime"));
                        AddDetail(sheet, row, 4, period.EndTime.ToString("f"));

                        row++;

                        AddDetail(sheet, row, 1, L("TotalTickets"));
                        AddDetail(sheet, row, 2, period.TotalTicketCount);

                        AddDetail(sheet, row, 3, L("TotalSales"));
                        AddDetail(sheet, row, 4,
                            Math.Round(period.TotalSales, _roundDecimals, MidpointRounding.AwayFromZero));

                        row++;
                        row++;
                        row++;

                        AddHeader(
                            sheet,
                            row++,
                            L("Category"),
                            L("MenuItemName"),
                            L("Portion"),
                            L("Quantity"),
                            L("Price"),
                            L("Total")
                        );

                        if (period.Id != null) input.WorkPeriodId = period.Id;

                        var outPut = await _appServie.GetItemSalesForWorkPeriod(input);
                        if (DynamicQueryable.Any(outPut))
                            foreach (var old in outPut)
                            {
                                var colCount = 1;
                                sheet.Cells[row, colCount++].Value = old.Category;
                                sheet.Cells[row, colCount++].Value = old.MenuItemName;
                                sheet.Cells[row, colCount++].Value = old.PortionName;
                                sheet.Cells[row, colCount++].Value = old.Quantity;
                                sheet.Cells[row, colCount++].Value = Math.Round(old.Price, _roundDecimals,
                                    MidpointRounding.AwayFromZero);
                                sheet.Cells[row, colCount++].Value = Math.Round(old.Total, _roundDecimals,
                                    MidpointRounding.AwayFromZero);
                                row++;
                            }

                        for (var i = 1; i <= 1; i++) sheet.Column(i).AutoFit();
                    }

                    Save(excelPackage, file);
                }

            return ProcessFile(input, file);
        }

        public async Task<FileDto> ExportItemSalesForDepartmentExcel(GetItemInput input,
            IConnectReportAppService appService)
        {
            var file =
                new FileDto("Department" + "-" + input.StartDate.ToString("yyyy-MMMM-dd") + ".xlsx",
                    MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                var row = 1;
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Items"));
                sheet.OutLineApplyStyle = true;
                var itemSales = await appService.GetItemSalesByDepartment(input);
                var departmentNameList = itemSales.MenuList.Items
                    .GroupBy(i => i.DepartmentName)
                    .Select(c => c.Key)
                    .Distinct()
                    .ToList();

                var outPut = itemSales.MenuList.Items
                    .GroupBy(c => new
                    {
                        c.CategoryName,
                        c.MenuItemPortionName,
                        c.MenuItemName
                    })
                    .ToList();

                var colCount = 1;
                sheet.Cells[row, colCount++].Value = "";
                sheet.Cells[row, colCount++].Value = "";
                sheet.Cells[row, colCount++].Value = "";

                foreach (var departmentName in departmentNameList)
                {
                    sheet.Cells[row, colCount++].Value = departmentName;
                    colCount++;
                }

                row = 2;
                var headers = new List<string> { L("Category"), L("MenuItemName"), L("Portion") };

                for (var i = 0; i < departmentNameList.Count; i++)
                {
                    headers.Add(L("Quantity"));
                    headers.Add(L("Total"));
                }

                AddHeader(
                    sheet,
                    row++,
                    headers.ToArray()
                );

                foreach (var old in outPut)
                {
                    colCount = 1;
                    sheet.Cells[row, colCount++].Value = old.Key.CategoryName;
                    sheet.Cells[row, colCount++].Value = old.Key.MenuItemName;
                    sheet.Cells[row, colCount++].Value = old.Key.MenuItemPortionName;
                    foreach (var item in old)
                    {
                        var index = 0;
                        foreach (var departmentName in departmentNameList)
                        {
                            index++;
                            if (item.DepartmentName == departmentName)
                            {
                                colCount = (index - 1) * 2 + 4;
                                sheet.Cells[row, colCount++].Value = Math.Round(item.Quantity, _roundDecimals,
                                    MidpointRounding.AwayFromZero);
                                sheet.Cells[row, colCount++].Value = Math.Round(item.Total, _roundDecimals,
                                    MidpointRounding.AwayFromZero);
                            }
                        }
                    }

                    row++;
                }

                sheet.Cells[row, 1, row, 3 + 2 * departmentNameList.Count].Style.Font.Bold = true;
                sheet.Cells[row, 1].Value = L("Total");
                colCount = 2;
                sheet.Cells[row, colCount++].Value = "";
                sheet.Cells[row, colCount++].Value = "";

                var groupByDepartment = itemSales.MenuList.Items
                    .GroupBy(c => c.DepartmentName)
                    .ToList();
                foreach (var old in groupByDepartment)
                {
                    var key = old.Key;
                    var index = 0;
                    foreach (var departmentName in departmentNameList)
                    {
                        index++;
                        if (key == departmentName)
                        {
                            colCount = (index - 1) * 2 + 4;
                            sheet.Cells[row, colCount++].Value = Math.Round(old.Sum(a => a.Quantity), _roundDecimals,
                                MidpointRounding.AwayFromZero);
                            sheet.Cells[row, colCount++].Value = Math.Round(old.Sum(a => a.Total), _roundDecimals,
                                MidpointRounding.AwayFromZero);
                        }
                    }
                }

                for (var i = 1; i <= 3 + 2 * departmentNameList.Count; i++) sheet.Column(i).AutoFit();
                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

        public async Task<FileDto> ExportToFileTenderDetail(GetTicketInput input, IConnectReportAppService appService)
        {
            var file = new FileDto(
                "Tender_" + input.StartDate.ToString("yyyy-MM-dd") + "_" + input.EndDate.ToString("yyyy-MM-dd") +
                ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Discount"));
                sheet.OutLineApplyStyle = true;
                var rowHeader = AddReportHeader(sheet, "Tender Report", input);
                input.MaxResultCount = 1000000000;
                var tenderItems = await appService.GetTenderDetailsForBackground(input);

                var row = rowHeader;
                row++;
                foreach (var tender in tenderItems)
                {
                    var colCount = 1;
                    row += 2;
                    sheet.Cells[row, 1, row, 12].Merge = true;
                    sheet.Cells[row, 1, row, 12].Style.Font.Bold = true;
                    sheet.Cells[row, 1, row, 12].Style.Font.Size = 14;
                    sheet.Cells[row, colCount].Value = "Settled Under " + tender.PaymentTypeName;
                    var headers = new List<string>
                    {
                        L("Date"),
                        L("Time"),
                        L("TicketNo"),
                        L("InvoiceNo"),
                        L("Terminal"),

                        L("Table"),
                        L("Employee"),
                        L("CardNumber"),
                        L("ReferenceNo"),
                        L("SubTotal"),
                        L("Discount"),
                        L("ServiceCharge"),
                        L("Vat"),
                        L("Total")
                    };

                    row++;
                    AddHeader(sheet, row, headers.ToArray());
                    foreach (var item in tender.TenderDetails)
                    {
                        row++;
                        colCount = 1;
                        sheet.Cells[row, colCount++].Value = item.DateTime.ToString(input.DateFormat);
                        sheet.Cells[row, colCount++].Value = item.DateTime.ToString(_timeFormat);
                        sheet.Cells[row, colCount++].Value = item.TicketNo;
                        sheet.Cells[row, colCount++].Value = item.InvoiceNo;
                        sheet.Cells[row, colCount++].Value = item.Terminal;

                        sheet.Cells[row, colCount++].Value = item.Table;
                        sheet.Cells[row, colCount++].Value = item.Employee;
                        sheet.Cells[row, colCount++].Value = item.CardNumber;
                        sheet.Cells[row, colCount++].Value = item.ReferenceNo;
                        sheet.Cells[row, colCount++].Value = Math.Round(item.SubTotal, _roundDecimals,
                            MidpointRounding.AwayFromZero);
                        sheet.Cells[row, colCount++].Value = Math.Round(item.Discount, _roundDecimals,
                            MidpointRounding.AwayFromZero);
                        sheet.Cells[row, colCount++].Value = Math.Round(item.ServiceCharge, _roundDecimals,
                            MidpointRounding.AwayFromZero);
                        sheet.Cells[row, colCount++].Value =
                            Math.Round(item.Vat, _roundDecimals, MidpointRounding.AwayFromZero);
                        sheet.Cells[row, colCount++].Value =
                            Math.Round(item.Total, _roundDecimals, MidpointRounding.AwayFromZero);
                    }

                    row++;
                    sheet.Cells[row, 1].Value = L("Total");
                    sheet.Cells[row, 10].Value = Math.Round(tender.TenderDetails.Sum(t => t.SubTotal), _roundDecimals,
                        MidpointRounding.AwayFromZero);
                    sheet.Cells[row, 11].Value = Math.Round(tender.TenderDetails.Sum(t => t.Discount), _roundDecimals,
                        MidpointRounding.AwayFromZero);
                    sheet.Cells[row, 12].Value = Math.Round(tender.TenderDetails.Sum(t => t.ServiceCharge),
                        _roundDecimals, MidpointRounding.AwayFromZero);
                    sheet.Cells[row, 13].Value = Math.Round(tender.TenderDetails.Sum(t => t.Vat), _roundDecimals,
                        MidpointRounding.AwayFromZero);
                    sheet.Cells[row, 14].Value = Math.Round(tender.TenderDetails.Sum(t => t.Total), _roundDecimals,
                        MidpointRounding.AwayFromZero);

                    sheet.Cells[row, 1, row, 12].Style.Font.Bold = true;
                }

                for (var i = 1; i <= 12; i++) sheet.Column(i).AutoFit();
                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

        public async Task<FileDto> ExportScheduleReportToFile(GetItemInput input, IConnectReportAppService appService)
        {
            var file = new FileDto("Schedule-" + DateTime.Now.ToString("yyyy-MMMM-dd") + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Items"));
                sheet.OutLineApplyStyle = true;

                var rowCount = 2;
                var outPut = await appService.GetScheduleReport(input);

                var schedules = await appService.GetScheduleSetting(input.Location);

                sheet.Cells[rowCount, 1, rowCount, schedules.Count() * 2 + 1].Style.Font.Bold = true;
                sheet.Cells[rowCount + 1, 1, rowCount + 1, schedules.Count() * 2 + 1].Style.Font.Bold = true;

                sheet.Cells[rowCount, 1].Value = L("MenuItem");

                var col = 2;
                schedules.ForEach(s =>
                {
                    sheet.Cells[rowCount, col].Value = s.ScheduleName;
                    sheet.Cells[rowCount, col, rowCount, col + 1].Merge = true;

                    sheet.Cells[rowCount + 1, col].Value = L("Quantity");
                    sheet.Cells[rowCount + 1, col + 1].Value = L("Amount");

                    col = col + 2;
                });
                rowCount += 2;

                foreach (var item in outPut.Items)
                {
                    var colInList = 1;
                    sheet.Cells[rowCount, colInList].Value = item.MenuItemName;

                    item.ScheduleReportDatas.ForEach(s =>
                    {
                        colInList++;
                        sheet.Cells[rowCount, colInList].Value = s.Quantity;
                        sheet.Cells[rowCount, colInList + 1].Value = s.Amount;

                        colInList++;
                        col = col + 2;
                    });

                    rowCount++;
                }

                sheet.Cells[rowCount, 1].Value = L("Total");
                sheet.Cells[rowCount, 1, rowCount, schedules.Count() * 2 + 1].Style.Font.Bold = true;

                for (var i = 1; i <= schedules.Count(); i++)
                {
                    sheet.Cells[rowCount, i * 2].Value =
                        outPut.Items.Select(e => e.ScheduleReportDatas[i - 1]).Sum(e => e.Quantity);
                    sheet.Cells[rowCount, i * 2 + 1].Value =
                        outPut.Items.Select(e => e.ScheduleReportDatas[i - 1]).Sum(e => e.Amount);
                }

                for (var i = 1; i <= 30; i++) sheet.Column(i).AutoFit();

                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

        public async Task<FileDto> ExportToFileGroup(GetItemInput input, IConnectReportAppService appService)
        {
            var file = new FileDto(
                "Group-" + input.StartDate.ToString("yyyy-MM-dd") + "-" + input.EndDate.ToString("yyyy-MM-dd") +
                ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Items"));
                sheet.OutLineApplyStyle = true;

                AddHeader(
                    sheet,
                    L("Group"),
                    L("Quantity"),
                    L("Total")
                );

                var rowCount = 2;
                var outPut = await appService.GetGroupSales(input);
                if (outPut?.GroupList?.Items != null && DynamicQueryable.Any(outPut.GroupList.Items))
                {
                    var groups = outPut.GroupList.Items.MapTo<IList<GroupReportDto>>();
                    AddObjects(
                        sheet, rowCount, groups,
                        _ => _.GroupName,
                        _ => _.Quantity,
                        _ => _.Total
                    );

                    var totalRow = outPut.GroupList.Items.Count() + 2;

                    sheet.Cells[totalRow, 1, totalRow, 5].Style.Font.Bold = true;
                    sheet.Cells[totalRow, 1].Value = "Total";
                    sheet.Cells[totalRow, 2].Value = groups.Select(g => g.Quantity).Sum();
                    sheet.Cells[totalRow, 3].Value = groups.Select(g => g.Total).Sum();

                    input.SkipCount = input.SkipCount + input.MaxResultCount;
                }

                for (var i = 1; i <= 1; i++) sheet.Column(i).AutoFit();

                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

        public async Task<FileDto> ExportToSalesSummary(GetTicketInput input, IConnectReportAppService appService,
            List<string> tt, List<string> pp, List<string> dts, List<string> proList, int tenantId)
        {
            var file = new FileDto(
                "Summary_" + input.StartDate.ToString("yyyy-MM-dd") + "_" + input.EndDate.ToString("yyyy-MM-dd") +
                ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Tickets"));
                sheet.OutLineApplyStyle = true;
                var rowCount = AddReportHeader(sheet, L("SaleSummary_r"), input);

                input.PaymentName = true;

                var outPut = await appService.GetSaleSummary(input);
                input.Sorting = "LocationId";
                var headers = new List<string>
                {
                    L("Date"), L("Location"), L("TotalTickets"), L("TotalCovers"), L("TotalOrderCovers"),
                    L("TotalAmount")
                };
                headers.AddRange(dts);
                headers.AddRange(pp);
                headers.AddRange(proList);
                headers.Add("TotalPromotion");
                headers.AddRange(tt);

                AddHeader(
                    sheet, rowCount,
                    headers.ToArray()
                );

                rowCount++;

                decimal total = 0;
                var ticketCount = 0;
                var ticketCoverCount = 0M;
                var totalOrdersCount = 0M;

                var i = 0;
                var paymentTotal = new Dictionary<string, decimal>();
                var deptTotal = new Dictionary<string, decimal>();
                var transTotal = new Dictionary<string, decimal>();
                var proTotal = new Dictionary<string, decimal>();
                var proAllTotal = 0M;

                if (DynamicQueryable.Any(outPut.Sale.Items))
                    for (i = 0; i < outPut.Sale.TotalCount; i++)
                    {
                        sheet.Cells[i + rowCount, 1].Value = outPut.Sale.Items[i].Date.ToString(input.DateFormat);
                        sheet.Cells[i + rowCount, 2].Value = outPut.Sale.Items[i].Location;
                        sheet.Cells[i + rowCount, 3].Value = outPut.Sale.Items[i].TotalTicketCount;
                        sheet.Cells[i + rowCount, 4].Value = outPut.Sale.Items[i].TotalCovers;
                        sheet.Cells[i + rowCount, 5].Value = outPut.Sale.Items[i].TotalOrderCount;
                        sheet.Cells[i + rowCount, 6].Value = outPut.Sale.Items[i].Total;
                        total += outPut.Sale.Items[i].Total;
                        ticketCount += outPut.Sale.Items[i].TotalTicketCount;
                        ticketCoverCount += outPut.Sale.Items[i].TotalCovers;
                        totalOrdersCount += outPut.Sale.Items[i].TotalOrderCount;

                        var index = 7;
                        var colFromHex = ColorTranslator.FromHtml("#E9E2E0");
                        foreach (var ppt in dts)
                        {
                            var ppAmount = 0M;
                            if (outPut.Sale.Items[i].Departments.Any(a => a.DepartmentName.Equals(ppt)))
                                ppAmount =
                                    outPut.Sale.Items[i].Departments.First(a => a.DepartmentName.Equals(ppt)).Total;

                            sheet.Cells[i + rowCount, index].Value = ppAmount;
                            sheet.Cells[i + rowCount, index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            sheet.Cells[i + rowCount, index].Style.Fill.BackgroundColor.SetColor(colFromHex);

                            if (deptTotal.ContainsKey(ppt))
                                deptTotal[ppt] += ppAmount;
                            else
                                deptTotal[ppt] = ppAmount;
                            index++;
                        }

                        colFromHex = ColorTranslator.FromHtml("#FCC9BE");

                        foreach (var ppt in pp)
                        {
                            var ppAmount = 0M;
                            if (outPut.Sale.Items[i].Payments.ContainsKey(ppt))
                                ppAmount = outPut.Sale.Items[i].Payments[ppt];

                            sheet.Cells[i + rowCount, index].Value = ppAmount;
                            sheet.Cells[i + rowCount, index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            sheet.Cells[i + rowCount, index].Style.Fill.BackgroundColor.SetColor(colFromHex);
                            if (paymentTotal.ContainsKey(ppt))
                                paymentTotal[ppt] += ppAmount;
                            else
                                paymentTotal[ppt] = ppAmount;

                            index++;
                        }

                        colFromHex = ColorTranslator.FromHtml("#C5C5C5");

                        foreach (var pro in proList)
                        {
                            var proAmount = 0M;
                            if (outPut.Sale.Items[i].Promotions.ContainsKey(pro))
                                proAmount = outPut.Sale.Items[i].Promotions[pro];

                            sheet.Cells[i + rowCount, index].Value = proAmount;
                            sheet.Cells[i + rowCount, index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            sheet.Cells[i + rowCount, index].Style.Fill.BackgroundColor.SetColor(colFromHex);

                            if (proTotal.ContainsKey(pro))
                                proTotal[pro] += proAmount;
                            else
                                proTotal[pro] = proAmount;

                            index++;
                        }


                        var proAllAmount = 0M;
                        proAllAmount = outPut.Sale.Items[i].Promotions.Where(p => proList.Contains(p.Key))
                            .Sum(p => Math.Abs(p.Value));
                        sheet.Cells[i + rowCount, index].Value = proAllAmount;
                        proAllTotal += Math.Abs(proAllAmount);
                        index++;


                        colFromHex = ColorTranslator.FromHtml("#FCEA0E");

                        foreach (var ttt in tt)
                        {
                            var ttAmount = 0M;

                            if (outPut.Sale.Items[i].Transactions.ContainsKey(ttt))
                                ttAmount = outPut.Sale.Items[i].Transactions[ttt];

                            sheet.Cells[i + rowCount, index].Value = ttAmount;
                            sheet.Cells[i + rowCount, index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            sheet.Cells[i + rowCount, index].Style.Fill.BackgroundColor.SetColor(colFromHex);

                            if (transTotal.ContainsKey(ttt))
                                transTotal[ttt] += ttAmount;
                            else
                                transTotal[ttt] = ttAmount;

                            index++;
                        }
                    }

                var tindex = 2;

                sheet.Cells[i + rowCount, tindex++].Value = L("Total");
                sheet.Cells[i + rowCount, tindex++].Value = ticketCount;
                sheet.Cells[i + rowCount, tindex++].Value = ticketCoverCount;
                sheet.Cells[i + rowCount, tindex++].Value = totalOrdersCount;

                sheet.Cells[i + rowCount, tindex++].Value = total;
                foreach (var ppt in dts)
                {
                    if (deptTotal.ContainsKey(ppt))
                        sheet.Cells[i + rowCount, tindex].Value = deptTotal[ppt];

                    tindex++;
                }

                foreach (var ppt in pp)
                {
                    if (paymentTotal.ContainsKey(ppt))
                        sheet.Cells[i + rowCount, tindex].Value = paymentTotal[ppt];
                    tindex++;
                }

                foreach (var pro in proList)
                {
                    if (proTotal.ContainsKey(pro))
                        sheet.Cells[i + rowCount, tindex].Value = proTotal[pro];
                    tindex++;
                }


                sheet.Cells[i + rowCount, tindex].Value = proAllTotal;
                tindex++;

                foreach (var ppt in tt)
                {
                    if (transTotal.ContainsKey(ppt))
                        sheet.Cells[i + rowCount, tindex].Value = transTotal[ppt];
                    tindex++;
                }

                sheet.Cells[i + rowCount, 1, i + rowCount, tindex].Style.Font.Bold = true;
                sheet.Cells[i + rowCount, 1, i + rowCount, tindex].Style.Font.Size = 16;

                for (i = 1; i <= 21; i++) sheet.Column(i).AutoFit();
                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

        public async Task<FileDto> ExportToSalesSecondSummary(GetTicketInput input, IConnectReportAppService appService,
            List<string> pp, int tenantId)
        {
            var file = new FileDto(
                "Summary_" + input.StartDate.ToString("yyyy-MM-dd") + "_" + input.EndDate.ToString("yyyy-MM-dd") +
                ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Tickets"));
                sheet.OutLineApplyStyle = true;
                var rowCount = AddReportHeader(sheet, L("SaleSummary_r"), input);

                input.PaymentName = true;

                var outPut = await appService.GetSaleSummary(input);
                input.Sorting = "LocationId";
                var headers = new List<string> { L("Date"), L("Location"), L("TotalTickets") };

                headers.AddRange(pp);

                AddHeader(
                    sheet, rowCount,
                    headers.ToArray()
                );

                rowCount++;

                decimal total = 0;
                var ticketCount = 0;
                var ticketCoverCount = 0M;

                var i = 0;
                var pptotal = new Dictionary<string, decimal>();
                var dttotal = new Dictionary<string, decimal>();
                var ttotal = new Dictionary<string, decimal>();
                var proTotal = new Dictionary<string, decimal>();
                var totalNetSales = 0M;
                var totalGrossSales = 0M;
                var tindex = 2;
                if (DynamicQueryable.Any(outPut.Sale.Items))
                    for (i = 0; i < outPut.Sale.TotalCount; i++)
                    {
                        var netsales = 0M;
                        if (outPut.Sale.Items[i].Transactions.ContainsKey("SALE"))
                        {
                            netsales = outPut.Sale.Items[i].Transactions["SALE"];
                            netsales = Math.Round(netsales, _roundDecimals, MidpointRounding.AwayFromZero);
                        }

                        var grosssales = 0M;
                        var mainTotal = outPut.Sale.Items[i].Total;

                        if (mainTotal > 0)
                        {
                            grosssales = mainTotal;
                            grosssales = Math.Round(grosssales, _roundDecimals, MidpointRounding.AwayFromZero);
                        }

                        totalNetSales += netsales;
                        totalGrossSales += grosssales;
                        tindex = 1;
                        sheet.Cells[i + rowCount, tindex++].Value =
                            outPut.Sale.Items[i].Date.ToString(input.DateFormat);
                        sheet.Cells[i + rowCount, tindex++].Value = outPut.Sale.Items[i].Location;
                        sheet.Cells[i + rowCount, tindex++].Value = outPut.Sale.Items[i].TotalTicketCount;
                        //sheet.Cells[i + rowCount, 4].Value = outPut.Sale.Items[i].TotalCovers;
                        //sheet.Cells[i + rowCount, 5].Value = outPut.Sale.Items[i].Total;
                        //sheet.Cells[i + rowCount, tindex++].Value = netsales;
                        //sheet.Cells[i + rowCount, tindex++].Value = grosssales;
                        total += outPut.Sale.Items[i].Total;
                        ticketCount += outPut.Sale.Items[i].TotalTicketCount;
                        ticketCoverCount += outPut.Sale.Items[i].TotalCovers;

                        //var index = 8;

                        var colFromHex = ColorTranslator.FromHtml("#FCC9BE");

                        foreach (var ppt in pp)
                        {
                            var ppAmount = 0M;
                            if (outPut.Sale.Items[i].Payments.ContainsKey(ppt))
                                ppAmount = outPut.Sale.Items[i].Payments[ppt];

                            sheet.Cells[i + rowCount, tindex].Value = ppAmount;
                            sheet.Cells[i + rowCount, tindex].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            sheet.Cells[i + rowCount, tindex].Style.Fill.BackgroundColor.SetColor(colFromHex);
                            if (pptotal.ContainsKey(ppt))
                                pptotal[ppt] += ppAmount;
                            else
                                pptotal[ppt] = ppAmount;

                            tindex++;
                        }
                    }

                tindex = 2;
                sheet.Cells[i + rowCount, tindex++].Value = L("Total");
                sheet.Cells[i + rowCount, tindex++].Value = ticketCount;
                //sheet.Cells[i + rowCount, tindex++].Value = ticketCoverCount;
                //sheet.Cells[i + rowCount, tindex++].Value = total;
                //sheet.Cells[i + rowCount, tindex++].Value = totalNetSales;
                //sheet.Cells[i + rowCount, tindex++].Value = totalGrossSales;

                foreach (var ppt in pp)
                {
                    if (pptotal.ContainsKey(ppt))
                        sheet.Cells[i + rowCount, tindex].Value = pptotal[ppt];
                    tindex++;
                }

                sheet.Cells[i + rowCount, 1, i + rowCount, tindex].Style.Font.Bold = true;
                sheet.Cells[i + rowCount, 1, i + rowCount, tindex].Style.Font.Size = 16;

                for (i = 1; i <= 21; i++) sheet.Column(i).AutoFit();
                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

        public async Task<FileDto> ExportToSalesSummaryNew(GetTicketInput input, IConnectReportAppService appService,
            List<string> tt, List<string> pp, List<string> dts, List<string> proList, int tenantId)
        {
            var countryCode = FeatureChecker.GetValue(tenantId, AppFeatures.ConnectCountry);

            var file = new FileDto(
                "Summary_New_" + input.StartDate.ToString("yyyy-MM-dd") + "_" + input.EndDate.ToString("yyyy-MM-dd") +
                ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Tickets"));
                sheet.OutLineApplyStyle = true;
                var rowCount = AddReportHeader(sheet, L("SaleSummary_r"), input);

                input.PaymentName = true;

                var outPut = await appService.GetSaleSummary(input);
                input.Sorting = "LocationId";
                var headers = new List<string>
                    { L("Date"), L("Location"), L("TotalTickets"), L("TotalCovers"), L("TotalAmount") };
                headers.AddRange(dts);
                headers.AddRange(pp);
                headers.AddRange(proList);
                headers.Add("Total Promotion");
                headers.Add("Net Sales After Rounding");
                headers.Add("Total GST amount for discount");
                headers.Add("Total Discount amount without GST");
                headers.Add("Sales Food");
                headers.Add("Sales Beverage");
                headers.Add("Voucher Excess");
                headers.AddRange(tt);
                if (countryCode.Equals("TH"))
                    headers.Add(L("NetSales"));

                AddHeader(
                    sheet, rowCount,
                    headers.ToArray()
                );

                rowCount++;

                decimal total = 0;
                var ticketCount = 0;
                var ticketCoverCount = 0M;

                var i = 0;
                var pptotal = new Dictionary<string, decimal>();
                var dttotal = new Dictionary<string, decimal>();
                var ttotal = new Dictionary<string, decimal>();
                var proTotal = new Dictionary<string, decimal>();
                var proAllTotal = 0M;
                var totalAllNetSaleRounding = 0M;
                var totalAllGSTAmount = 0M;
                var totalAllDiscountWithoutGST = 0M;
                var totalAllSalesFood = 0M;
                var totalAllSalesBeverage = 0M;
                var totalAllVoucherExcess = 0M;
                if (DynamicQueryable.Any(outPut.Sale.Items))
                    for (i = 0; i < outPut.Sale.TotalCount; i++)
                    {
                        var index = 1;

                        sheet.Cells[i + rowCount, index++].Value = outPut.Sale.Items[i].Date.ToString(input.DateFormat);
                        sheet.Cells[i + rowCount, index++].Value = outPut.Sale.Items[i].Location;
                        sheet.Cells[i + rowCount, index++].Value = outPut.Sale.Items[i].TotalTicketCount;
                        sheet.Cells[i + rowCount, index++].Value = outPut.Sale.Items[i].TotalCovers;
                        sheet.Cells[i + rowCount, index++].Value = outPut.Sale.Items[i].Total;
                        total += outPut.Sale.Items[i].Total;
                        ticketCount += outPut.Sale.Items[i].TotalTicketCount;
                        ticketCoverCount += outPut.Sale.Items[i].TotalCovers;

                        var locationId = new List<SimpleLocationDto>();
                        locationId.Add(new SimpleLocationDto { Id = outPut.Sale.Items[i].LocationId });
                        var menuListOutputDots = await appService.GetGroupSales(new GetItemInput
                        {
                            StartDate = outPut.Sale.Items[i].Date, EndDate = outPut.Sale.Items[i].Date, Sorting = "Id",
                            Locations = locationId
                        });
                        var beverage = menuListOutputDots.GroupList.Items
                            .Where(s => s.GroupName.ToUpper().Contains("BEVERAGE")).Sum(s => s.Total);
                        outPut.Sale.Items[i].VoucherExpress = outPut.Sale.Items[i].PaymentAccess
                            .Where(x => x.Key.ToUpper().Contains("VOUCHER")).Sum(x => x.Value);
                        outPut.Sale.Items[i].SalesBeverage = beverage;

                        foreach (var ppt in dts)
                        {
                            var ppAmount = 0M;
                            if (outPut.Sale.Items[i].Departments.Any(a => a.DepartmentName.Equals(ppt)))
                                ppAmount =
                                    outPut.Sale.Items[i].Departments.First(a => a.DepartmentName.Equals(ppt)).Total;

                            sheet.Cells[i + rowCount, index].Value = ppAmount;
                            if (dttotal.ContainsKey(ppt))
                                dttotal[ppt] += ppAmount;
                            else
                                dttotal[ppt] = ppAmount;
                            index++;
                        }

                        foreach (var ppt in pp)
                        {
                            var ppAmount = 0M;
                            if (outPut.Sale.Items[i].Payments.ContainsKey(ppt))
                                ppAmount = outPut.Sale.Items[i].Payments[ppt];

                            sheet.Cells[i + rowCount, index].Value = ppAmount;

                            if (pptotal.ContainsKey(ppt))
                                pptotal[ppt] += ppAmount;
                            else
                                pptotal[ppt] = ppAmount;

                            index++;
                        }

                        foreach (var pro in proList)
                        {
                            var proAmount = 0M;
                            if (outPut.Sale.Items[i].Promotions.ContainsKey(pro))
                                proAmount = outPut.Sale.Items[i].Promotions[pro];

                            sheet.Cells[i + rowCount, index].Value = proAmount;

                            if (proTotal.ContainsKey(pro))
                                proTotal[pro] += proAmount;
                            else
                                proTotal[pro] = proAmount;

                            index++;
                        }

                        var proAllAmount = 0M;
                        proAllAmount = outPut.Sale.Items[i].Promotions.Where(p => proList.Contains(p.Key))
                            .Sum(p => Math.Abs(p.Value));
                        sheet.Cells[i + rowCount, index].Value = proAllAmount;
                        proAllTotal += Math.Abs(proAllAmount);
                        index++;
                        var roundingAmount = 0M;
                        if (outPut.Sale.Items[i].Transactions.ContainsKey("ROUND-"))
                            roundingAmount = outPut.Sale.Items[i].Transactions["ROUND-"];
                        if (roundingAmount > 0) roundingAmount = -roundingAmount;
                        var salesAmount = 0M;
                        if (outPut.Sale.Items[i].Transactions.ContainsKey("SALE"))
                            salesAmount = outPut.Sale.Items[i].Transactions["SALE"];
                        var mainTotal = outPut.Sale.Items[i].Total;
                        var paymentAmount = 0M;
                        if (outPut.Sale.Items[i].Transactions.ContainsKey("PAYMENT"))
                            paymentAmount = outPut.Sale.Items[i].Transactions["PAYMENT"];

                        var taxAmount1 = 0M;
                        if (outPut.Sale.Items[i].Transactions.ContainsKey("TAX"))
                            taxAmount1 = outPut.Sale.Items[i].Transactions["TAX"];

                        if (mainTotal > 0)
                        {
                            mainTotal = mainTotal - mainTotal * 7 / 107;
                            mainTotal = Math.Round(mainTotal, _roundDecimals, MidpointRounding.AwayFromZero);
                        }

                        // netSaleAfterRounding
                        var netSaleAfterRounding = paymentAmount - taxAmount1;
                        sheet.Cells[i + rowCount, index].Value = netSaleAfterRounding;
                        totalAllNetSaleRounding += Math.Abs(netSaleAfterRounding);
                        index++;

                        //totalGSTamountfordiscount
                        var totalGSTamountfordiscount = Math.Round(proAllAmount * 7 / 107, _roundDecimals,
                            MidpointRounding.AwayFromZero);
                        sheet.Cells[i + rowCount, index].Value = totalGSTamountfordiscount;
                        totalAllGSTAmount += Math.Abs(totalGSTamountfordiscount);
                        index++;

                        //totalDiscountamountwithoutGST
                        var totalDiscountamountwithoutGST = proAllAmount - totalGSTamountfordiscount;
                        sheet.Cells[i + rowCount, index].Value = totalDiscountamountwithoutGST;
                        totalAllDiscountWithoutGST += Math.Abs(totalDiscountamountwithoutGST);
                        index++;

                        //sales food                        
                        var salesFood = netSaleAfterRounding + totalGSTamountfordiscount + roundingAmount -
                                        Math.Round(outPut.Sale.Items[i].SalesBeverage, _roundDecimals,
                                            MidpointRounding.AwayFromZero);
                        sheet.Cells[i + rowCount, index].Value = salesFood;
                        totalAllSalesFood += Math.Abs(salesFood);
                        index++;

                        //sales beverage                        
                        var salesBeverage = Math.Round(outPut.Sale.Items[i].SalesBeverage, _roundDecimals,
                            MidpointRounding.AwayFromZero);
                        sheet.Cells[i + rowCount, index].Value = salesBeverage;
                        totalAllSalesBeverage += Math.Abs(salesBeverage);
                        index++;
                        //sales excess
                        //
                        var voucherExcess = Math.Round(outPut.Sale.Items[i].VoucherExpress, _roundDecimals,
                            MidpointRounding.AwayFromZero);
                        sheet.Cells[i + rowCount, index].Value = voucherExcess;
                        totalAllVoucherExcess += Math.Abs(voucherExcess);
                        index++;
                        var taxAmount = outPut.Sale.Items[i].Total - mainTotal;
                        taxAmount = Math.Round(taxAmount, _roundDecimals, MidpointRounding.AwayFromZero);

                        foreach (var ttt in tt)
                        {
                            var ttAmount = 0M;
                            if (outPut.Sale.Items[i].Transactions.ContainsKey(ttt))
                                ttAmount = outPut.Sale.Items[i].Transactions[ttt];


                            if ((ttt == "ROUND-" || ttt == "DISCOUNT") && ttAmount > 0) ttAmount = -ttAmount;

                            sheet.Cells[i + rowCount, index].Value = ttAmount;

                            if (ttotal.ContainsKey(ttt))
                                ttotal[ttt] += ttAmount;
                            else
                                ttotal[ttt] = ttAmount;

                            index++;
                        }

                        if (mainTotal != 0M)
                        {
                            if (ttotal.ContainsKey("NETSALE"))
                                ttotal["NETSALE"] += mainTotal;
                            else
                                ttotal["NETSALE"] = mainTotal;
                            if (countryCode.Equals("TH"))
                                sheet.Cells[i + rowCount, index].Value = mainTotal;
                        }
                    }

                var tindex = 2;

                sheet.Cells[i + rowCount, tindex++].Value = L("Total");
                sheet.Cells[i + rowCount, tindex++].Value = ticketCount;
                sheet.Cells[i + rowCount, tindex++].Value = ticketCoverCount;
                sheet.Cells[i + rowCount, tindex++].Value = total;
                foreach (var ppt in dts)
                {
                    if (dttotal.ContainsKey(ppt))
                        sheet.Cells[i + rowCount, tindex].Value = dttotal[ppt];

                    tindex++;
                }

                foreach (var ppt in pp)
                {
                    if (pptotal.ContainsKey(ppt))
                        sheet.Cells[i + rowCount, tindex].Value = pptotal[ppt];
                    tindex++;
                }

                foreach (var pro in proList)
                {
                    if (proTotal.ContainsKey(pro))
                        sheet.Cells[i + rowCount, tindex].Value = proTotal[pro];
                    tindex++;
                }

                //foreach (var pro in proList)
                //{
                //if (proTotal.ContainsKey(pro))
                sheet.Cells[i + rowCount, tindex].Value = proAllTotal;
                tindex++;
                //}
                sheet.Cells[i + rowCount, tindex].Value = totalAllNetSaleRounding;
                tindex++;

                sheet.Cells[i + rowCount, tindex].Value = totalAllGSTAmount;
                tindex++;

                sheet.Cells[i + rowCount, tindex].Value = totalAllDiscountWithoutGST;
                tindex++;
                sheet.Cells[i + rowCount, tindex].Value = totalAllSalesFood;
                tindex++;
                sheet.Cells[i + rowCount, tindex].Value = totalAllSalesBeverage;
                tindex++;
                sheet.Cells[i + rowCount, tindex].Value = totalAllVoucherExcess;
                tindex++;
                foreach (var ppt in tt)
                {
                    if (ttotal.ContainsKey(ppt))
                        sheet.Cells[i + rowCount, tindex].Value = ttotal[ppt];
                    tindex++;
                }

                if (ttotal.ContainsKey("NETSALE") && countryCode.Equals("TH"))
                {
                    sheet.Cells[i + rowCount, tindex].Value = ttotal["NETSALE"];
                    sheet.Cells[i + rowCount, tindex].Style.Font.Bold = true;
                    sheet.Cells[i + rowCount, tindex].Style.Font.Size = 16;
                }

                sheet.Cells[i + rowCount, 1, i + rowCount, tindex].Style.Font.Bold = true;
                sheet.Cells[i + rowCount, 1, i + rowCount, tindex].Style.Font.Size = 16;

                for (i = 1; i <= 21; i++) sheet.Column(i).AutoFit();
                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

        public async Task<FileDto> ExportFranchise(GetTicketInput input,
            IConnectReportAppService connectReportAppService,
            List<string> ttList, List<string> ptList, List<SharingPercentageDto> lList, List<MenuItemListDto> menuItems)

        {
            var builder = new StringBuilder();
            builder.Append(L("Tickets"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append(" - ");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                await GetAllOrders(excelPackage, input, connectReportAppService, ttList, ptList, lList, menuItems);
                await GetItems(excelPackage, new GetItemInput
                {
                    StartDate = input.StartDate,
                    EndDate = input.EndDate,
                    Locations = input.Locations,
                    UserId = input.UserId,
                    RunInBackground = input.RunInBackground
                }, connectReportAppService);

                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

        public async Task<FileDto> ExportToTallyOutput(GetTicketInput input, ConnectReportAppService cService,
            List<string> ttList, List<string> ptList)
        {
            var builder = new StringBuilder();
            builder.Append(L("Tally"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append(" - ");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            var delimiter = "#";

            if (File.Exists(@"C:/_Templates/Tally/Tally.txt"))
                using (var package = new ExcelPackage())
                {
                    var sheet = package.Workbook.Worksheets.Add("Sales Accounts");
                    sheet.OutLineApplyStyle = true;
                    var headers = new List<string> { "LOCATION CODE", "" };

                    if (input.Locations.Count >= 1) headers.AddRange(input.Locations.Select(loca => loca.Code));

                    AddHeader(sheet, headers.ToArray());
                    var rowCount = 2;
                    sheet.Cells[rowCount, 2].Value = "DATE";
                    var lCount = 3;
                    foreach (var locationListDto in input.Locations)
                        sheet.Cells[rowCount, lCount++].Value = input.StartDate.ToString();

                    rowCount++;

                    var logs = File.ReadAllLines(@"C:/_Templates/Tally/Tally.txt")
                        .Where(line => !string.IsNullOrEmpty(line))
                        .Select(line => line.Split(delimiter.ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                        .Distinct();

                    var colCount = 1;
                    foreach (var stringse in logs)
                    {
                        sheet.Cells[rowCount, 1].Value = stringse[0] ?? "";
                        sheet.Cells[rowCount, 2].Value = stringse[1] ?? "";
                        rowCount++;
                    }

                    colCount = 3;
                    foreach (var locationListDto in input.Locations)
                    {
                        rowCount = 3;

                        var allPayments = await cService.GetPayments(new GetPaymentInput
                        {
                            Locations = new List<SimpleLocationDto> { locationListDto },
                            StartDate = input.StartDate,
                            EndDate = input.EndDate
                        });

                        var allTrans = await cService.GetTransactions(new GetTransactionInput
                        {
                            Locations = new List<SimpleLocationDto> { locationListDto },
                            StartDate = input.StartDate,
                            EndDate = input.EndDate
                        });

                        var allDepts = await cService.GetDepartmentSales(new GetItemInput
                        {
                            Locations = new List<SimpleLocationDto> { locationListDto },
                            StartDate = input.StartDate,
                            EndDate = input.EndDate
                        });

                        var allCats = await cService.GetCategorySales(new GetItemInput
                        {
                            Locations = new List<SimpleLocationDto> { locationListDto },
                            StartDate = input.StartDate,
                            EndDate = input.EndDate
                        });

                        foreach (var stringse in logs)
                            if (stringse.Length > 2)
                            {
                                var lastStr = stringse[2];
                                if (!string.IsNullOrEmpty(lastStr))
                                {
                                    var splitC = lastStr.Split(':');

                                    if (splitC.Length <= 1)
                                        continue;

                                    var ids = new List<int>();
                                    var mainString = splitC[1];
                                    var firstC = mainString[0];
                                    var remStr = splitC[1].Substring(1);
                                    if (!string.IsNullOrEmpty(remStr))
                                    {
                                        if (remStr.Length > 1)
                                            ids = remStr.Split(',').Select(int.Parse).ToList();
                                        else
                                            ids.Add(Convert.ToInt32(remStr));
                                    }

                                    if (firstC.Equals('P'))
                                    {
                                        var sumValue =
                                            ids.Select(
                                                    id => allPayments.Payments.Items.FirstOrDefault(a => a.Id == id))
                                                .Where(payee => payee != null)
                                                .Sum(payee => payee.Amount);
                                        sheet.Cells[rowCount++, colCount].Value = sumValue;
                                    }
                                    else if (firstC.Equals('T'))
                                    {
                                        var sumValue =
                                            ids.Select(
                                                    id => allTrans.Transactions.Items.FirstOrDefault(a => a.Id == id))
                                                .Where(payee => payee != null)
                                                .Sum(payee => payee.Amount);
                                        sheet.Cells[rowCount++, colCount].Value = sumValue;
                                    }
                                    else if (firstC.Equals('C'))
                                    {
                                        var sumValue =
                                            ids.Select(
                                                    id =>
                                                        allCats.CategoryList.Items.FirstOrDefault(
                                                            a => a.CategoryId == id))
                                                .Where(payee => payee != null)
                                                .Sum(payee => payee.Total);
                                        sheet.Cells[rowCount++, colCount].Value = sumValue;
                                    }
                                    else if (firstC.Equals('D'))
                                    {
                                        var sumValue =
                                            ids.Select(
                                                    id => allDepts.DepartmentList.Items.FirstOrDefault(a => a.Id == id))
                                                .Where(payee => payee != null)
                                                .Sum(payee => payee.Total);
                                        sheet.Cells[rowCount++, colCount].Value = sumValue;
                                    }
                                }
                            }

                        colCount++;
                    }

                    Save(package, file);
                }

            return ProcessFile(input, file);
        }

        public async Task<FileDto> ExportToTallyForDepartment(GetTicketInput input, ConnectReportAppService cService)
        {
            var builder = new StringBuilder();
            builder.Append(L("Tally"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append(" - ");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            var delimiter = "#";

            if (File.Exists(@"C:/_Templates/Tally/Tally.txt"))
                using (var package = new ExcelPackage())
                {
                    var sheet = package.Workbook.Worksheets.Add("Sales Accounts");
                    sheet.OutLineApplyStyle = true;
                    var headers = new List<string> { "LOCATION CODE", "" };

                    if (input.Locations.Count >= 1) headers.AddRange(input.Locations.Select(loca => loca.Code));
                    AddHeader(sheet, headers.ToArray());
                    var rowCount = 2;
                    sheet.Cells[rowCount, 2].Value = "DATE";
                    var lCount = 3;
                    foreach (var locationListDto in input.Locations)
                        sheet.Cells[rowCount, lCount++].Value = input.StartDate.ToString(input.DateFormat);

                    rowCount++;

                    var logs = File.ReadAllLines(@"C:/_Templates/Tally/Department.txt")
                        .Where(line => !string.IsNullOrEmpty(line))
                        .Select(line => line.Split(delimiter.ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
                        .Distinct();

                    var colCount = 1;
                    foreach (var stringse in logs)
                    {
                        sheet.Cells[rowCount, 1].Value = stringse[0] ?? "";
                        sheet.Cells[rowCount, 2].Value = stringse[1] ?? "";
                        rowCount++;
                    }

                    colCount = 3;
                    var allDepts = await cService.GetDeparmentSalesSummaryForTally(input);

                    foreach (var li in input.Locations)
                    {
                        rowCount = 3;
                        foreach (var stringse in logs)
                            if (stringse.Length > 2)
                            {
                                var lastStr = stringse[2];
                                if (!string.IsNullOrEmpty(lastStr) && lastStr.Contains(":"))
                                {
                                    var first = lastStr.Split(':')[0];
                                    var second = lastStr.Split(':')[1];

                                    var loc =
                                        allDepts.SingleOrDefault(
                                            a => a.Location.Equals(li.Code) && a.DepartmentName.Equals(first));

                                    if (!string.IsNullOrEmpty(second) && loc != null)
                                    {
                                        var firstC = second[0];
                                        var remStr = second.Substring(1);
                                        var id = 0;
                                        try
                                        {
                                            id = Convert.ToInt32(remStr);
                                        }
                                        catch (Exception ex)
                                        {
                                            id = 0;
                                        }

                                        if (!string.IsNullOrEmpty(remStr))
                                        {
                                            if (firstC.Equals('P'))
                                            {
                                                if (loc.Payments.ContainsKey(id))
                                                    sheet.Cells[rowCount++, colCount].Value =
                                                        Math.Abs(loc.Payments[id]);
                                                else
                                                    rowCount++;
                                            }
                                            else if (firstC.Equals('T'))
                                            {
                                                if (loc.Transactions.ContainsKey(id))
                                                    sheet.Cells[rowCount++, colCount].Value =
                                                        Math.Abs(loc.Transactions[id]);
                                                else
                                                    rowCount++;
                                            }
                                        }
                                        else
                                        {
                                            rowCount++;
                                        }
                                    }
                                    else
                                    {
                                        rowCount++;
                                    }
                                }
                                else
                                {
                                    rowCount++;
                                }
                            }
                            else
                            {
                                rowCount++;
                            }

                        colCount++;
                    }

                    Save(package, file);
                }

            return ProcessFile(input, file);
        }


        public FileDto ExportTopOrBottomItemSales(GetItemInput input, List<MenuListDto> dtos)
        {
            var builder = new StringBuilder();
            builder.Append(L("TopOrBottomItemSales"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append("_");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("TopOrBottomItemSales"));
                sheet.OutLineApplyStyle = true;

                var row = 2;

                AddHeader(sheet,
                    L("Location"),
                    L("Category"),
                    L("MenuItemName"),
                    L("Portion"),
                    L("Price"),
                    L("Quantity"),
                    L("TotalAmount")
                );

                AddObjects(sheet, row, dtos,
                    _ => _.LocationName,
                    _ => _.CategoryName,
                    _ => _.MenuItemName,
                    _ => _.MenuItemPortionName,
                    _ => Math.Round(_.Price, _roundDecimals, MidpointRounding.AwayFromZero),
                    _ => _.Quantity,
                    _ => Math.Round(_.Total, _roundDecimals, MidpointRounding.AwayFromZero)
                );

                row = dtos.Count + row++;
                sheet.Cells[row, 1].Value = L("Total");
                sheet.Cells[row, 6].Value = dtos.Sum(t => t.Quantity);
                //sheet.Cells[row, 16].Value = dtos.Sum(t => t.Quantity);
                sheet.Cells[row, 7].Value =
                    Math.Round(dtos.Sum(t => t.Total), _roundDecimals, MidpointRounding.AwayFromZero);

                sheet.Cells[row, 1, row, 20].Style.Font.Bold = true;
                for (var i = 1; i <= 20; i++) sheet.Column(i).AutoFit();

                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

        public async Task<FileDto> ExportToFileItemTagSales(GetItemInput input, IConnectReportAppService appService)
        {
            var builder = new StringBuilder();
            builder.Append(L("ItemTagSales"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append(" - ");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                await CreateFileForItemTagSales(excelPackage, input, appService);
                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

        public async Task<FileDto> ExportToFileItemTagSalesDetail(GetItemInput input,
            IConnectReportAppService appService)
        {
            var builder = new StringBuilder();
            builder.Append(L("ItemTagSalesDetail"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append(" - ");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                await CreateFileForItemTagSalesDetail(excelPackage, input, appService);
                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

        public async Task<FileDto> ExportABBReprintReportToFile(ABBReprintReportInputDto input,
            IConnectReportAppService appService)
        {
            var builder = new StringBuilder();
            builder.Append(L("ABBReprint"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append("_");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("ABBReprint"));
                sheet.OutLineApplyStyle = true;
                var outPut = await appService.GetABBReprintReport(input);

                var row = 2;

                AddHeader(sheet,
                    L("PlantCode"),
                    L("PlantName"),
                    L("Cashier"),
                    L("Date"),
                    L("TicketNumber"),
                    L("Status"),
                    L("NoOfReprint"),
                    L("TerminalName"),
                    L("Total")
                );

                AddObjects(sheet, row, outPut.Items.ToList(),
                    _ => _.PlantCode,
                    _ => _.PlantName,
                    _ => _.Cashier,
                    _ => _.Date,
                    _ => _.TicketNumber,
                    _ => _.Status,
                    _ => _.NoOfReprint,
                    _ => _.TerminalName,
                    _ => _.Total
                );

                for (var i = 1; i <= 9; i++) sheet.Column(i).AutoFit();

                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

        #region Location Comparison

        public FileDto ExportLocationComparison(List<LocationComparisonReportOuptut> dtos)
        {
            var file = new FileDto("LocationComparisonReport-" + DateTime.Now.ToString("yyyy-MMMM-dd") + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Collections"));
                sheet.OutLineApplyStyle = true;
                var locations = new List<string>();
                locations = dtos.FirstOrDefault()?.LocationDetails.Select(l => l.LocationName).ToList();

                var headers = new List<string>
                {
                    L("Date"),
                    L("Day"),
                    L("Week")
                };
                headers.AddRange(locations);
                headers.Add(L("Total"));

                AddHeader(sheet, true, headers.ToArray());

                var row = 2;
                foreach (var item in dtos)
                {
                    var col = 1;
                    AddDetail(sheet, row, col++, item.Date.ToString("dd-MM-yyyy"));
                    AddDetail(sheet, row, col++, item.Date.ToString("ddd"));
                    AddDetail(sheet, row, col++, item.WeekNumber);
                    item.LocationDetails.ForEach(l =>
                    {
                        AddDetail(sheet, row, col++,
                            Math.Round(l.SubTotal, _roundDecimals, MidpointRounding.AwayFromZero));
                    });

                    AddDetail(sheet, row, col++,
                        Math.Round(item.LocationDetails.Sum(l => l.SubTotal), _roundDecimals,
                            MidpointRounding.AwayFromZero));

                    row++;
                }

                for (var i = 1; i <= headers.Count() + 2; i++) sheet.Column(i).AutoFit();
                Save(excelPackage, file);
            }

            return file;
        }

        #endregion Location Comparison


        public async Task<FileDto> ExportToFilePlanWorkDay(WorkPeriodSummaryInputWithPaging input,
            IConnectReportAppService appService)
        {
            var builder = new StringBuilder();
            builder.Append(L("PlanWorkDay"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append("_");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("PlanWorkDay"));
                sheet.OutLineApplyStyle = true;
                var outPut = await appService.GetWorkPeriodByLocationForDatesWithPaging(input);

                var row = 2;

                AddHeader(sheet,
                    L("LocationName"),
                    L("LocationCode"),
                    L("WorkDay")
                );

                AddObjects(sheet, row, outPut.Items.ToList(),
                    _ => _.LocationName,
                    _ => _.LocationCode,
                    _ => _.ReportStartDay.ToString(input.DateFormat)
                );

                for (var i = 1; i <= 3; i++) sheet.Column(i).AutoFit();

                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

        private FileDto ProcessFileHTML(FileDto file, string fileHTML)
        {
            File.WriteAllText(Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken), fileHTML);
            file.FileType = MimeTypeNames.ApplicationXhtmlXml;
            file.FileName = Path.GetFileNameWithoutExtension(file.FileName) + ".html";
            return file;
        }

        private async Task<string> ExportDatatableToHtml(GetItemInput input, IConnectReportAppService appService)
        {
            input.MaxResultCount = 10000;
            var data = await appService.GetOrdersExchange(input);
            var dataGroup = data.ListItem;

            var textTitle = L("ExchangeReport") + "<br/>" + L("BusinessDate") + " " +
                            input.StartDate.ToString(input.DateFormat) + " to " +
                            input.EndDate.ToString(input.DateFormat);
            var strHTMLBuilder = new StringBuilder();
            strHTMLBuilder.Append("<html>");
            strHTMLBuilder.Append("<head>");
            strHTMLBuilder.Append("</head>");

            if (dataGroup.Count() == 1)
            {
                var first = dataGroup.FirstOrDefault();
                textTitle += "<br/>" + first.PlantName;
            }

            strHTMLBuilder.Append(@"<style> 
             body  { 
              font:400 12px 'Tahoma';
              font-size: 12px;
              padding:10px;
            }
            table  { 
                margin-top: 10px;
                border-collapse: collapse;
                width: 100%;
                margin-bottom: 10px;
            }

            table td, table th {
                border: 0.5px solid black;
                padding: 8px;
            }
            table th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                color: black;
            }
            thead {
                display: table-header-group;
            }
            tfoot {
                display: table-row-group;
            }
            tr{
                page-break-inside: avoid;
                font-size: 14;
            }
            .text-right{
              text-align:right;
            }

			.margin_bottom_0 {
				margin-bottom: 0 !important;
			}
            .margin_top_0 {
				margin-top: 0 !important;
			}

			.border_bottom_none {
				border-bottom: none !important;
			}

			.custom_table_date {
				margin-bottom: 0px !important;
			}

			.display_flex {
				display: flex;
			}
			.w-100 {
				width: 100%;
			}
	
			.float-right {
				float: right;
			}

			.font_weight_bold {
				font-weight: bold;
			}
            .font_14{
				font-size: 14;
             }
            .text-center{
               text-align: center;          
            }
            tr:nth-child(odd) {
              background-color: #f3f3f3;
            }

            .background_color_white{
               background-color: white !important;
            }

            //table tr:hover {background-color: #ddd;}
            </style>");


            strHTMLBuilder.Append("<body>");
            strHTMLBuilder.Append("<div>");
            // add header
            strHTMLBuilder.Append("<table class='table table-bordered custom_table_date border_bottom_none'>");
            strHTMLBuilder.Append(
                "<tr class='font_14 background_color_white' style='height: 80px;vertical-align: top;'>");
            strHTMLBuilder.Append(
                $"<td colspan='15'  class='border_bottom_none text-center font_weight_bold'>{textTitle}</td>");
            strHTMLBuilder.Append("</tr>");
            strHTMLBuilder.Append("</br>");

            // add content
            strHTMLBuilder.Append("<tr class='font_14' style='margin-top:10px'>");
            strHTMLBuilder.Append($"<th class='text-center'>{L("PlantCode")}</th>");
            strHTMLBuilder.Append($"<th class='text-center'>{L("PlantName")}</th>");
            strHTMLBuilder.Append($"<th class='text-center'>{L("DateTime")}</th>");
            strHTMLBuilder.Append($"<th class='text-center'>{L("SellerName")}</th>");
            strHTMLBuilder.Append($"<th class='text-center'>{L("ReceiptNumber")}</th>");
            strHTMLBuilder.Append($"<th class='text-center'>{L("CustomerName")}</th>");
            strHTMLBuilder.Append($"<th class='text-center'>{L("Tender")}</th>");
            strHTMLBuilder.Append($"<th class='text-center'>{L("Material_Group")}</th>");
            strHTMLBuilder.Append($"<th class='text-center'>{L("MaterialCode")}</th>");
            strHTMLBuilder.Append($"<th class='text-center'>{L("MaterialName")}</th>");
            strHTMLBuilder.Append($"<th class='text-center'>{L("Quantity")}</th>");
            strHTMLBuilder.Append($"<th class='text-center'>{L("Amount")}</th>");
            strHTMLBuilder.Append($"<th class='text-center'>{L("ExchangeDate")}</th>");
            strHTMLBuilder.Append($"<th class='text-center'>{L("ExchangeBy")}</th>");
            strHTMLBuilder.Append($"<th class='text-center'>{L("Reason")}</th>");
            strHTMLBuilder.Append("</tr>");
            foreach (var item in dataGroup.OrderBy(s => s.PlantCode))
            {
                foreach (var subItem in item.ListItem.OrderBy(s => s.DateTimeSort))
                {
                    strHTMLBuilder.Append("<tr>");
                    strHTMLBuilder.Append($"<td> {subItem.PlantCode}</td> ");
                    strHTMLBuilder.Append($"<td> {subItem.PlantName}</td> ");
                    strHTMLBuilder.Append($"<td> {subItem.DateTimeSort.ToString(input.DatetimeFormat)}</td> ");
                    strHTMLBuilder.Append($"<td> {subItem.SellerName}</td> ");
                    strHTMLBuilder.Append($"<td> {subItem.ReceiptNumber}</td> ");
                    strHTMLBuilder.Append($"<td> {subItem.CustomerName}</td> ");
                    strHTMLBuilder.Append($"<td> {subItem.Tender}</td> ");
                    strHTMLBuilder.Append($"<td> {subItem.MaterialGroup}</td> ");
                    strHTMLBuilder.Append($"<td> {subItem.MaterialCode}</td> ");
                    strHTMLBuilder.Append($"<td> {subItem.MaterialName}</td> ");
                    strHTMLBuilder.Append($"<td class='text-right'> {Convert.ToInt16(subItem.Quantity)}</td> ");
                    strHTMLBuilder.Append(
                        $"<td class='text-right'> {subItem.Amount.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td> ");
                    strHTMLBuilder.Append($"<td> {subItem.Exchange.ToString(input.DatetimeFormat)}</td> ");
                    strHTMLBuilder.Append($"<td> {subItem.ExchangeBy}</td> ");
                    strHTMLBuilder.Append($"<td> {subItem.Reason}</td> ");
                    strHTMLBuilder.Append("</tr>");
                }

                strHTMLBuilder.Append("<tr>");
                strHTMLBuilder.Append("<td> </td> ");
                strHTMLBuilder.Append("<td> </td> ");
                strHTMLBuilder.Append("<td> </td> ");
                strHTMLBuilder.Append("<td> </td> ");
                strHTMLBuilder.Append("<td> </td> ");
                strHTMLBuilder.Append("<td></td> ");
                strHTMLBuilder.Append("<td> </td> ");
                strHTMLBuilder.Append("<td> </td> ");
                strHTMLBuilder.Append("<td> </td> ");
                strHTMLBuilder.Append($"<td class='font_weight_bold'>{L("Total")} </td> ");
                strHTMLBuilder.Append(
                    $"<td class='text-right font_weight_bold'> {Convert.ToInt16(item.Quantity)}</td> ");
                strHTMLBuilder.Append(
                    $"<td class='text-right font_weight_bold'> {item.Amount.ToString(ConnectConsts.ConnectConsts.NumberFormat)}</td> ");
                strHTMLBuilder.Append("<td> </td> ");
                strHTMLBuilder.Append("<td> </td> ");
                strHTMLBuilder.Append("<td></td> ");
                strHTMLBuilder.Append("</tr>");
            }


            strHTMLBuilder.Append("</table>");
            strHTMLBuilder.Append("</div>");
            var Htmltext = strHTMLBuilder.ToString();
            return Htmltext;
        }


        private async Task<FileDto> EportOrdersSummary(GetItemInput input, IConnectReportAppService appService,
            FileDto file)
        {
            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Orders"));
                sheet.OutLineApplyStyle = true;
                var headers = new List<string>
                {
                    L("Location"),
                    L("TicketId"),
                    L("TicketNumber"),
                    L("Order"),
                    L("MenuItem"),
                    L("TagValue"),
                    L("Portion"),
                    L("Quantity"),
                    L("Price"),
                    L("LineTotal"),
                    L("TaxPrice"),
                    L("User"),
                    L("Date"),
                    L("Time"),
                    L("Department"),
                    L("Note")
                };
                var addOperationUser = false;
                if (input.Void || input.Comp || input.Gift)
                {
                    addOperationUser = true;
                    headers.Add(L("OperationUser"));
                }

                AddHeader(
                    sheet,
                    true, headers.ToArray()
                );

                var rowCount = 2;
                var secondTime = false;
                var allQuan = 0M;
                var allTot = 0M;

                while (true)
                {
                    input.MaxResultCount = 100;
                    if (secondTime) input.NotCorrectDate = true;
                    var outPut = await appService.GetOrders(input);

                    if (!secondTime) secondTime = true;

                    if (DynamicQueryable.Any(outPut.OrderList.Items))
                    {
                        rowCount = input.SkipCount + 2;

                        foreach (var old in outPut.OrderList.Items)
                        {
                            allQuan += old.Quantity;
                            var total = old.Price * old.Quantity;
                            allTot += total;

                            var colCount = 1;
                            sheet.Cells[rowCount, colCount++].Value = old.LocationName;
                            sheet.Cells[rowCount, colCount++].Value = old.TicketId;
                            sheet.Cells[rowCount, colCount++].Value = old.TicketNumber;
                            sheet.Cells[rowCount, colCount++].Value = old.OrderNumber;
                            sheet.Cells[rowCount, colCount++].Value = old.MenuItemName;
                            sheet.Cells[rowCount, colCount++].Value = "";
                            sheet.Cells[rowCount, colCount++].Value = old.PortionName;
                            sheet.Cells[rowCount, colCount++].Value = old.Quantity;
                            sheet.Cells[rowCount, colCount++].Value = old.Price;
                            sheet.Cells[rowCount, colCount++].Value = old.Quantity * old.Price;
                            sheet.Cells[rowCount, colCount++].Value = old.TaxPrice;
                            sheet.Cells[rowCount, colCount++].Value = old.CreatingUserName;
                            sheet.Cells[rowCount, colCount++].Value = old.OrderCreatedTime.ToString(input.DateFormat);
                            sheet.Cells[rowCount, colCount++].Value = old.OrderCreatedTime.ToString(_timeFormat);
                            sheet.Cells[rowCount, colCount++].Value = old.DepartmentName;
                            sheet.Cells[rowCount, colCount++].Value = old.Note;
                            if (addOperationUser) sheet.Cells[rowCount, colCount++].Value = old.OperationUser;
                            foreach (var totd in old.TransactionOrderTags)
                            {
                                colCount = 1;
                                rowCount++;
                                allQuan += old.Quantity;
                                total = totd.Price * old.Quantity;
                                allTot += total;

                                sheet.Cells[rowCount, colCount++].Value = "";
                                sheet.Cells[rowCount, colCount++].Value = "";
                                sheet.Cells[rowCount, colCount++].Value = "";
                                sheet.Cells[rowCount, colCount++].Value = "";
                                sheet.Cells[rowCount, colCount++].Value = totd.TagValue;
                                sheet.Cells[rowCount, colCount++].Value = "";
                                sheet.Cells[rowCount, colCount++].Value = old.Quantity;
                                sheet.Cells[rowCount, colCount++].Value = totd.Price;
                                sheet.Cells[rowCount, colCount++].Value = old.Quantity * totd.Price;
                                sheet.Cells[rowCount, colCount++].Value = "";
                                sheet.Cells[rowCount, colCount++].Value = "";
                            }

                            rowCount++;
                        }

                        input.SkipCount = input.SkipCount + input.MaxResultCount;
                    }
                    else
                    {
                        break;
                    }
                }

                rowCount++;
                sheet.Cells[rowCount, 1].Value = L("Total");
                sheet.Cells[rowCount, 8].Value = allQuan;
                sheet.Cells[rowCount, 10].Value = allTot;

                for (var i = 1; i <= 20; i++) sheet.Column(i).AutoFit();

                var col = 1;
                foreach (var item in headers)
                {
                    sheet.Cells[1, col].Style.Font.Bold = true;
                    sheet.Cells[1, col].Style.Font.Size = 15;

                    sheet.Cells[rowCount, col].Style.Font.Bold = true;
                    sheet.Cells[rowCount, col].Style.Font.Size = 16;

                    col++;
                }

                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

        private int AddReportHeader(ExcelWorksheet sheet, string nameOfReport, IDateInput input)
        {
            var brandName = "";
            var locationCode = "";
            var locationName = "";
            var branch = "All";
            var isAllLocation =
                input.LocationGroup?.Locations?.Count() + input.LocationGroup?.Groups?.Count() +
                input.LocationGroup?.LocationTags?.Count() == _locRepo.GetAll().Count();

            if (isAllLocation)
            {
                branch = "All";
            }
            else
            {
                if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
                    branch = input.LocationGroup.Locations.Select(l => l.Name).JoinAsString(", ");
                if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
                    branch = input.LocationGroup.Groups.Select(l => l.Name).JoinAsString(", ");
                if (input.LocationGroup?.LocationTags != null && input.LocationGroup.LocationTags.Any())
                    branch = input.LocationGroup.LocationTags.Select(l => l.Name).JoinAsString(", ");
            }

            Master.Location myLocation = null;

            if (input.Location > 0)
                myLocation = _locRepo.Get(input.Location);

            if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var simpleL = input.LocationGroup.Locations.First();
                if (simpleL != null) myLocation = _locRepo.Get(simpleL.Id);
            }

            if (myLocation != null)
            {
                var myCompany = _comRepo.Get(myLocation.CompanyRefId);
                if (myCompany != null) brandName = myCompany.Name;
                locationCode = myLocation.Code;
                locationName = myLocation.Name;
            }

            sheet.Cells[1, 1].Value = nameOfReport;
            sheet.Cells[1, 1, 1, 12].Merge = true;
            sheet.Cells[1, 1, 1, 13].Style.Font.Bold = true;
            sheet.Cells[1, 1, 1, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            sheet.Cells[2, 1].Value = L("Brand");
            sheet.Cells[2, 2].Value = brandName;
            sheet.Cells[2, 6].Value = "Business Date:";
            sheet.Cells[2, 7].Value = input.StartDate.ToString(input.DateFormat) + " to " +
                                      input.EndDate.ToString(input.DateFormat);
            sheet.Cells[3, 1].Value = "Branch Name:";
            sheet.Cells[3, 2].Value = branch;
            sheet.Cells[3, 6].Value = "Printed On:";
            sheet.Cells[3, 7].Value = DateTime.Now.ToString(input.DatetimeFormat);

            sheet.Cells[1, 1, 4, 7].Style.Font.Size = 10;

            return 6;
        }

        private async Task<int> AddReportTicketSyncHeader(ExcelWorksheet sheet, string nameOfReport, IDateInput input)
        {
            var brandName = "";
            var locationCode = "";
            var locationName = "";
            var branch = "All";

            var isAllLocation =
                input.LocationGroup?.Locations?.Count() + input.LocationGroup?.Groups?.Count() +
                input.LocationGroup?.LocationTags?.Count() == _locRepo.GetAll().Count();

            if (isAllLocation)
            {
                branch = "All";
            }
            else
            {
                if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
                    branch = input.LocationGroup.Locations.Select(l => l.Name).JoinAsString(", ");
                if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
                    branch = input.LocationGroup.Groups.Select(l => l.Name).JoinAsString(", ");
                if (input.LocationGroup?.LocationTags != null && input.LocationGroup.LocationTags.Any())
                    branch = input.LocationGroup.LocationTags.Select(l => l.Name).JoinAsString(", ");
            }

            Master.Location myLocation = null;

            if (input.Location > 0)
                myLocation = _locRepo.Get(input.Location);

            if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var simpleL = input.LocationGroup.Locations.First();
                if (simpleL != null) myLocation = _locRepo.Get(simpleL.Id);
            }

            if (myLocation != null)
            {
                var myCompany = _comRepo.Get(myLocation.CompanyRefId);
                if (myCompany != null) brandName = myCompany.Name;
                locationCode = myLocation.Code;
                locationName = myLocation.Name;
            }

            sheet.Cells[1, 1].Value = nameOfReport;
            sheet.Cells[1, 1, 1, 8].Merge = true;
            sheet.Cells[1, 1, 1, 16].Style.Font.Bold = true;
            sheet.Cells[1, 1, 1, 16].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            sheet.Cells[2, 1].Value = L("Brand");
            sheet.Cells[2, 2].Value = brandName;

            sheet.Cells[2, 6].Value = "Business Date:";
            sheet.Cells[2, 7].Value = input.StartDate.ToString(input.DateFormat) + " to " +
                                      input.EndDate.ToString(input.DateFormat);
            sheet.Cells[3, 1].Value = "Branch Name:";
            sheet.Cells[3, 2].Value = branch;
            sheet.Cells[3, 6].Value = "Printed On:";
            sheet.Cells[3, 7].Value = DateTime.Now.ToString(input.DatetimeFormat);

            sheet.Cells[1, 1, 4, 7].Style.Font.Size = 10;

            return 6;
        }

        private async Task<FileDto> EportVoidOrders(GetItemInput input, IConnectReportAppService appService,
            FileDto file)
        {
            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("VoidOrders"));
                sheet.OutLineApplyStyle = true;
                var row = AddReportHeader(sheet, "Void Report", input);
                var headers = new List<string>
                {
                    L("Date"),
                    L("Time"),
                    L("TicketNumber"),
                    L("Department"),
                    L("Table"),
                    L("MenuName"),
                    L("Qty"),
                    L("Note"),
                    L("OrderUser"),
                    L("VoidUser"),
                    L("Terminal"),
                    L("SubTotal")
                };

                AddHeader(
                    sheet, row++, headers.ToArray()
                );

                var rowCount = 7;
                var secondTime = false;
                var allQuan = 0M;
                var allTot = 0M;

                while (true)
                {
                    input.MaxResultCount = 100;
                    if (secondTime) input.NotCorrectDate = true;
                    var outPut = await appService.GetOrders(input);

                    if (!secondTime) secondTime = true;

                    if (outPut.OrderList.Items.Any())
                    {
                        foreach (var old in outPut.OrderList.Items)
                        {
                            var colCount = 1;
                            var total = old.Price * old.Quantity;
                            allTot += total;
                            allQuan += old.Quantity;

                            sheet.Cells[rowCount, colCount++].Value = old.OrderCreatedTime.ToString(input.DateFormat);
                            sheet.Cells[rowCount, colCount++].Value = old.OrderCreatedTime.ToString(_timeFormat);
                            sheet.Cells[rowCount, colCount++].Value = old.TicketNumber;
                            sheet.Cells[rowCount, colCount++].Value = old.DepartmentName;
                            sheet.Cells[rowCount, colCount++].Value = "";

                            //if(old.Ticket!=null && !string.IsNullOrEmpty(old.Ticket.TicketEntities))
                            //{
                            //  var allEntities =
                            //        JsonConvert.DeserializeObject<IList<TicketEntityMap>>(old.Ticket.TicketEntities);
                            //  var lastEntity = allEntities.LastOrDefault(a => a.EntityTypeId == 2);
                            //  sheet.Cells[rowCount, colCount++].Value = lastEntity != null ? lastEntity.EntityName : "";
                            //}
                            //else
                            //{
                            //    sheet.Cells[rowCount, colCount++].Value ="";
                            //}

                            sheet.Cells[rowCount, colCount++].Value = old.MenuItemName;
                            sheet.Cells[rowCount, colCount++].Value = old.Quantity;
                            sheet.Cells[rowCount, colCount++].Value = old.Note;
                            sheet.Cells[rowCount, colCount++].Value = old.CreatingUserName;
                            sheet.Cells[rowCount, colCount++].Value = old.OperationUser;
                            sheet.Cells[rowCount, colCount++].Value = old.TerminalName;
                            sheet.Cells[rowCount, colCount++].Value = Math.Round(old.Quantity * old.Price,
                                _roundDecimals, MidpointRounding.AwayFromZero);

                            rowCount++;
                        }

                        input.SkipCount = input.SkipCount + input.MaxResultCount;
                    }
                    else
                    {
                        break;
                    }
                }

                rowCount++;
                sheet.Cells[rowCount, 1].Value = L("Total");
                sheet.Cells[rowCount, 7].Value = allQuan;
                sheet.Cells[rowCount, 12].Value = Math.Round(allTot, _roundDecimals, MidpointRounding.AwayFromZero);

                for (var i = 1; i <= 20; i++) sheet.Column(i).AutoFit();

                var col = 1;
                foreach (var item in headers)
                {
                    sheet.Cells[rowCount, col].Style.Font.Bold = true;
                    sheet.Cells[rowCount, col].Style.Font.Size = 15;

                    col++;
                }

                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

        private async Task GetExportExchange(ExcelPackage excelPackage, GetItemInput input,
            IConnectReportAppService appService)
        {
            var secondTime = false;
            input.MaxResultCount = 10000;
            if (secondTime) input.NotCorrectDate = true;
            var outPut = await appService.GetOrdersExchange(input);
            var outputGroup = outPut.ListItem;
            if (!secondTime) secondTime = true;
            var sheet = excelPackage.Workbook.Worksheets.Add(L("ExchangeReport"));
            sheet.OutLineApplyStyle = true;
            // Add header
            var textTitle = L("ExchangeReport") + "<br/>" + L("BusinessDate") + " " +
                            input.StartDate.ToString(input.DateFormat) + " to " +
                            input.EndDate.ToString(input.DateFormat);
            if (outputGroup.Count() == 1)
            {
                var first = outputGroup.FirstOrDefault();
                textTitle += "<br/>" + first.PlantName;
            }

            var textTitleWithNewLine = textTitle.Replace("<br/>", Environment.NewLine);
            sheet.Cells[1, 1].Value = textTitleWithNewLine;
            sheet.Cells[1, 1, 5, 16].Merge = true;
            sheet.Cells[1, 1, 5, 16].Style.WrapText = true;
            sheet.Cells[1, 1, 5, 16].Style.Font.Bold = true;
            sheet.Cells[1, 1, 5, 16].Style.Font.Size = 15;
            sheet.Cells[1, 1, 5, 16].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            sheet.Cells[1, 1].Style.Border.BorderAround(ExcelBorderStyle.None);
            sheet.Cells[1, 1, 5, 16].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
            sheet.Cells[1, 1, 5, 16].Style.WrapText = true;
            var headers = new List<string>
            {
                L("PlantCode"),
                L("PlantName"),
                L("DateTime"),
                L("SellerName"),
                L("ReceiptNumber"),
                L("CustomerName"),
                L("Tender"),
                L("Material_Group"),
                L("MaterialCode"),
                L("MaterialName"),
                L("Quantity"),
                L("Amount"),
                L("ExchangeDate"),
                L("ExchangeBy"),
                L("Reason")
            };
            var rowCount = 6;
            var cellHeader = 1;
            foreach (var itemhead in headers)
            {
                sheet.Cells[rowCount, cellHeader].Value = itemhead;
                sheet.Cells[rowCount, cellHeader].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                sheet.Cells[rowCount, cellHeader].Style.Font.Bold = true;
                sheet.Cells[rowCount, cellHeader].Style.Font.Size = 15;
                cellHeader++;
            }

            rowCount++;

            foreach (var item in outputGroup.ToList().OrderBy(s => s.PlantCode))
            {
                foreach (var old in item.ListItem.OrderBy(s => s.DateTimeSort))
                {
                    var colCount = 1;
                    sheet.Cells[rowCount, colCount++].Value = old.PlantCode;
                    sheet.Cells[rowCount, colCount++].Value = old.PlantName;
                    sheet.Cells[rowCount, colCount++].Value = old.DateTimeSort.ToString(input.DatetimeFormat);
                    sheet.Cells[rowCount, colCount++].Value = old.SellerName;
                    sheet.Cells[rowCount, colCount++].Value = old.ReceiptNumber;
                    sheet.Cells[rowCount, colCount++].Value = old.CustomerName;
                    sheet.Cells[rowCount, colCount++].Value = old.Tender;
                    sheet.Cells[rowCount, colCount++].Value = old.MaterialGroup;
                    sheet.Cells[rowCount, colCount++].Value = old.MaterialCode;
                    sheet.Cells[rowCount, colCount++].Value = old.MaterialName;
                    sheet.Cells[rowCount, colCount++].Value = old.Quantity;
                    sheet.Cells[rowCount, colCount++].Value =
                        old.Amount.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    sheet.Cells[rowCount, colCount++].Value = old.Exchange.ToString(input.DatetimeFormat);
                    sheet.Cells[rowCount, colCount++].Value = old.ExchangeBy;
                    sheet.Cells[rowCount, colCount++].Value = old.Reason;

                    for (var colInRow = 1; colInRow <= 15; colInRow++)
                    {
                        sheet.Cells[rowCount, colInRow].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        sheet.Cells[rowCount, colInRow].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    }

                    rowCount++;
                }

                var listSum = new List<string>
                {
                    " ", " ", " ", " ", " ", " ", " ", " ", " ", L("Total"), Convert.ToInt16(item.Quantity).ToString(),
                    item.Amount.ToString(ConnectConsts.ConnectConsts.NumberFormat), " ", " ", " "
                };
                var sumCol = 1;
                foreach (var itemSum in listSum)
                {
                    sheet.Cells[rowCount, sumCol].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    sheet.Cells[rowCount, sumCol].Value = itemSum;
                    sheet.Cells[rowCount, sumCol].Style.Font.Bold = true;
                    sheet.Cells[rowCount, sumCol].Style.Font.Size = 15;
                    sumCol++;
                }

                rowCount++;
            }

            for (var i = 1; i <= 15; i++) sheet.Column(i).AutoFit();
        }

        private async Task GetLocationSaleDetailSummary(ExcelPackage package, GetTicketInput input,
            IConnectReportAppService appService)
        {
            var sheet = package.Workbook.Worksheets.Add(L("sale_tax_report_detail"));
            sheet.OutLineApplyStyle = true;
            var outItem = await appService.GetLocationSalesDetail(input);
            var row = AddReportHeader(sheet, "Thai Tax Detail", input);
            row++;
            var headers = new List<string>
            {
                L("Date"),
                L("RegNo"),
                L("TaxInvoiceNo"),
                L("Employee"),
                L("Detail"),
                L("SalesAmount"),
                L("SalesNonVat"),
                L("SalesExcVat"),
                L("Service"),
                L("Vat"),
                L("Payment")
            };

            AddHeader(
                sheet, row++, headers.ToArray()
            );

            SetBorders(sheet.Cells[8, 1, 8, 11]);
            var rowCount = 9;

            foreach (var pos in outItem.Items)
            {
                var colCount = 1;

                sheet.Cells[rowCount, colCount++].Value = pos.Date.ToString(input.DatetimeFormat);
                sheet.Cells[rowCount, colCount++].Value = pos.RegNo;
                sheet.Cells[rowCount, colCount++].Value = pos.TaxInvoiceNo;
                sheet.Cells[rowCount, colCount++].Value = pos.Employee;
                sheet.Cells[rowCount, colCount++].Value = pos.Detail;
                sheet.Cells[rowCount, colCount++].Value =
                    Math.Round(pos.SalesAmount, _roundDecimals, MidpointRounding.AwayFromZero);
                sheet.Cells[rowCount, colCount++].Value =
                    Math.Round(pos.SalesNonVat, _roundDecimals, MidpointRounding.AwayFromZero);
                sheet.Cells[rowCount, colCount++].Value =
                    Math.Round(pos.SalesExcVat, _roundDecimals, MidpointRounding.AwayFromZero);
                sheet.Cells[rowCount, colCount++].Value =
                    Math.Round(pos.Service, _roundDecimals, MidpointRounding.AwayFromZero);
                sheet.Cells[rowCount, colCount++].Value =
                    Math.Round(pos.Vat, _roundDecimals, MidpointRounding.AwayFromZero);
                sheet.Cells[rowCount, colCount++].Value = pos.Payment;

                rowCount++;
            }

            sheet.Cells[rowCount, 1].Value = L("Total");
            sheet.Cells[rowCount, 1, rowCount, 11].Style.Font.Bold = true;

            sheet.Cells[rowCount, 6].Value = Math.Round(outItem.Items.Sum(c => c.SalesAmount), _roundDecimals,
                MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, 7].Value = Math.Round(outItem.Items.Sum(c => c.SalesNonVat), _roundDecimals,
                MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, 8].Value = Math.Round(outItem.Items.Sum(c => c.SalesExcVat), _roundDecimals,
                MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, 9].Value = Math.Round(outItem.Items.Sum(c => c.Service), _roundDecimals,
                MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, 10].Value = Math.Round(outItem.Items.Sum(c => c.Vat), _roundDecimals,
                MidpointRounding.AwayFromZero);

            sheet.Column(11).Width = 15;
            for (var i = 1; i <= 10; i++) sheet.Column(i).AutoFit();
        }

        private async Task GetLocationSaleSummary(ExcelPackage package, GetTicketInput input,
            IConnectReportAppService appService)
        {
            var sheet = package.Workbook.Worksheets.Add(L("sale_tax_report"));
            sheet.OutLineApplyStyle = true;
            var outItem = await appService.GetLocationSales(input);
            var row = AddReportHeader(sheet, L("ThaiTaxSummary"), input);

            var headers = new List<string>
            {
                L("Date"),
                L("RegNo"),
                L("TaxInvoiceNo"),
                L("SaleAmt"),
                L("SaleNonVat"),
                L("SaleVat"),
                L("Service"),
                L("Vat"),
                L("Discount"),
                L("VoucherExcess")
            };
            AddHeader(
                sheet, row, headers.ToArray()
            );
            var rowCount = row;
            rowCount++;
            var colCount = 1;
            foreach (var pos in outItem.Items)
            {
                colCount = 1;
                sheet.Cells[rowCount, colCount++].Value = pos.Date.ToString(input.DateFormat);
                sheet.Cells[rowCount, colCount++].Value = pos.RegNo;
                sheet.Cells[rowCount, colCount++].Value = pos.TaxInvoiceNo;
                sheet.Cells[rowCount, colCount++].Value =
                    Math.Round(pos.SaleAmt, _roundDecimals, MidpointRounding.AwayFromZero);
                sheet.Cells[rowCount, colCount++].Value =
                    Math.Round(pos.SaleNonVat, _roundDecimals, MidpointRounding.AwayFromZero);
                sheet.Cells[rowCount, colCount++].Value =
                    Math.Round(pos.SaleVat, _roundDecimals, MidpointRounding.AwayFromZero);
                sheet.Cells[rowCount, colCount++].Value =
                    Math.Round(pos.Service, _roundDecimals, MidpointRounding.AwayFromZero);
                sheet.Cells[rowCount, colCount++].Value =
                    Math.Round(pos.Vat, _roundDecimals, MidpointRounding.AwayFromZero);
                sheet.Cells[rowCount, colCount++].Value =
                    Math.Round(pos.Discount, _roundDecimals, MidpointRounding.AwayFromZero);
                sheet.Cells[rowCount, colCount++].Value =
                    Math.Round(pos.VoucherExcess, _roundDecimals, MidpointRounding.AwayFromZero);

                rowCount++;
            }

            sheet.Cells[rowCount, 1].Value = L("Total");
            sheet.Cells[rowCount, 1, rowCount, 9].Style.Font.Bold = true;

            colCount = 2;
            sheet.Cells[rowCount, colCount++].Value = "";
            sheet.Cells[rowCount, colCount++].Value = "";
            sheet.Cells[rowCount, colCount++].Value = Math.Round(outItem.Items.Sum(c => c.SaleAmt), _roundDecimals,
                MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, colCount++].Value = Math.Round(outItem.Items.Sum(c => c.SaleNonVat), _roundDecimals,
                MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, colCount++].Value = Math.Round(outItem.Items.Sum(c => c.SaleVat), _roundDecimals,
                MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, colCount++].Value = Math.Round(outItem.Items.Sum(c => c.Service), _roundDecimals,
                MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, colCount++].Value = Math.Round(outItem.Items.Sum(c => c.Vat), _roundDecimals,
                MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, colCount++].Value = Math.Round(outItem.Items.Sum(c => c.Discount), _roundDecimals,
                MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, colCount++].Value = Math.Round(outItem.Items.Sum(c => c.VoucherExcess),
                _roundDecimals, MidpointRounding.AwayFromZero);

            for (var i = 1; i <= 9; i++) sheet.Column(i).AutoFit();
        }

        private static void SetBordersBottom(ExcelRange range)
        {
            range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        }

        private static void SetBorders(ExcelRange range)
        {
            range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        }

        private async Task GetItemsHoursSaleSummary(ExcelPackage package, GetItemInput input,
            IConnectReportAppService appService)
        {
            var sheet = package.Workbook.Worksheets.Add(L("HourlySales"));
            sheet.OutLineApplyStyle = true;

            ItemStatsDto itemHourlySales;

            if (input.RunInBackground)
                itemHourlySales = await appService.GetItemHourlySalesForBackground(input);
            else
                itemHourlySales = await appService.GetItemHourlySales(input);

            var outItemHours = itemHourlySales.MenuList.Items.GroupBy(c => c.Hour).ToList();

            var hoursList = itemHourlySales.MenuList.Items.GroupBy(c => c.Hour)
                .Select(c => c.Key).Distinct().ToList();

            var outItem = itemHourlySales.MenuList.Items.GroupBy(c => new
            {
                c.MenuItemName,
                c.MenuItemPortionName,
                c.CategoryName
            }).Select(gcs => new
            {
                gcs.Key.CategoryName,
                gcs.Key.MenuItemName,
                gcs.Key.MenuItemPortionName,
                Children = gcs.ToList()
            });

            var headers = new List<string>
            {
                L("Category"),
                L("MenuItem"),
                L("Portion")
            };

            foreach (var pos in hoursList)
            {
                headers.Add(pos.ToString());
                headers.Add("");
                headers.Add("");
            }

            AddHeader(
                sheet, headers.ToArray()
            );

            var rowCount = 2;
            var colCount = 4;
            foreach (var pos in hoursList)
            {
                sheet.Cells[rowCount, colCount++].Value = "Quantity";
                sheet.Cells[rowCount, colCount++].Value = "Price";
                sheet.Cells[rowCount, colCount++].Value = "Amount";
            }

            rowCount++;

            foreach (var pos in outItem)
            {
                colCount = 1;
                sheet.Cells[rowCount, colCount++].Value = pos.CategoryName;
                sheet.Cells[rowCount, colCount++].Value = pos.MenuItemName;
                sheet.Cells[rowCount, colCount++].Value = pos.MenuItemPortionName;

                foreach (var child in pos.Children.GroupBy(a => a.Hour))
                {
                    colCount = (hoursList.IndexOf(child.Key) + 1) * 3 + 1;
                    sheet.Cells[rowCount, colCount++].Value = child.Sum(a => a.Quantity);
                    sheet.Cells[rowCount, colCount++].Value = child.Sum(a => a.Price);
                    sheet.Cells[rowCount, colCount++].Value = child.Sum(a => a.Total);
                }

                rowCount++;
            }

            sheet.Cells[rowCount, 1].Value = L("Total");
            colCount = 2;
            sheet.Cells[rowCount, colCount++].Value = "";
            sheet.Cells[rowCount, colCount++].Value = "";

            foreach (var old in outItemHours)
            {
                colCount = (hoursList.IndexOf(old.Key) + 1) * 3 + 1;
                sheet.Cells[rowCount, colCount++].Value = old.Sum(a => a.Quantity);
                sheet.Cells[rowCount, colCount++].Value = "";
                sheet.Cells[rowCount, colCount++].Value = old.Sum(a => a.Total);
            }

            for (var i = 1; i <= 1; i++) sheet.Column(i).AutoFit();
        }

        private static List<DiscountOutput> GetFormattedTicketTagGroupList(List<DiscountOutput> dtos)
        {
            var formattedList = new List<DiscountOutput>();
            var location = "";
            var department = "";

            dtos.Select(item =>
            {
                if (item.Location == location)
                {
                    item.Location = "";
                }
                else
                {
                    location = item.Location;
                    return item;
                }

                if (item.DepartmentName == department)
                {
                    item.DepartmentName = "";
                }
                else
                {
                    department = item.DepartmentName;

                    return item;
                }

                return item;
            });
            dtos.GroupBy(c => new { c.DepartmentName, c.Location })
                .ForEach(g =>
                {
                    location = g.Key.Location;
                    department = g.Key.DepartmentName;
                    var items = g.Select(c => new DiscountOutput());

                    formattedList.AddRange(items);
                });

            return formattedList.ToList();
        }

        private async Task GetDepartmentSummary(ExcelPackage package, GetTicketInput input,
            IConnectReportAppService appService, List<string> tt, List<string> pp)
        {
            var output = await appService.GetDepartmentSalesSummary(input);
            var sheet = package.Workbook.Worksheets.Add(L("Tickets"));
            sheet.OutLineApplyStyle = true;

            var rowCount = AddReportHeader(sheet, L("DepartmentSummary"), input);

            var headers = new List<string>
            {
                L("LocationName"),
                L("DepartmentName")
            };

            headers.AddRange(pp);
            headers.AddRange(tt);
            AddReportHeader(sheet, "Department Summary Report", input);
            AddHeader(
                sheet, rowCount,
                headers.ToArray()
            );

            rowCount++;

            foreach (var dept in output)
            {
                var colCount = 1;

                sheet.Cells[rowCount, colCount++].Value = dept.Location;
                sheet.Cells[rowCount, colCount++].Value = dept.DepartmentName;
                foreach (var p in pp)
                {
                    var pValue = 0M;
                    if (dept.Payments.ContainsKey(p)) pValue = dept.Payments[p];
                    sheet.Cells[rowCount, colCount++].Value = pValue;
                }

                foreach (var t in tt)
                {
                    var tValue = 0M;
                    if (dept.Transactions.ContainsKey(t)) tValue = dept.Transactions[t];
                    sheet.Cells[rowCount, colCount++].Value = tValue;
                }

                rowCount++;
            }
        }

        private async Task GetTickets(ExcelPackage package, GetTicketInput ticketInput,
            IConnectReportAppService appService,
            List<string> tt, List<string> pp, List<SharingPercentageDto> sList, List<string> tagnames,
            int tenantId)
        {
            var countryCode = FeatureChecker.GetValue(tenantId, AppFeatures.ConnectCountry);
            if (string.IsNullOrEmpty(countryCode)) countryCode = "SG";
            var sheet = package.Workbook.Worksheets.Add(L("Tickets"));
            sheet.OutLineApplyStyle = true;

            var headers = new List<string>
            {
                L("Location"),
                L("Id"),
                L("TicketNo"),
                L("RefTicket"),
                L("InvoiceNo"),
                L("Covers"),
                L("RefNo"),
                L("Credit"),
                L("TicketCreatedTime"),
                L("LastOrderTime"),
                L("LastPaymentTime"),
                L("DepartmentName"),
                L("Note"),
                L("LastModifiedUserName"),
                L("TerminalName"),
                L("TaxIncluded"),
                L("TotalAmount"),
                L("TableNumber"),
                L("Pax")
            };
            if (sList != null && sList.Any()) headers.Add(L("FranchiseAmount"));

            headers.AddRange(pp);
            headers.AddRange(tt);
            if (countryCode.Equals("TH"))
                headers.Add(L("NetSale").ToUpper());

            headers.AddRange(tagnames);

            var myRerowCount = AddReportHeader(sheet, "Tickets Report", ticketInput);

            var rowCount = myRerowCount;
            rowCount++;

            AddHeader(
                sheet, rowCount,
                headers.ToArray()
            );
            rowCount++;

            var i = 0;
            decimal total = 0;
            decimal totalPax = 0;
            var franchiseTotal = new Dictionary<int, decimal>();
            var franchiseTTotal = new Dictionary<int, decimal>();
            var pptotal = new Dictionary<string, decimal>();
            var ttotal = new Dictionary<string, decimal>();
            var ticketAvailabe = false;

            var finalCount = rowCount;
            var tickets = await appService.GetAllTicketsForTicketInput(ticketInput).ToListAsync();
            tickets = tickets.OrderBy(ticketInput.Sorting).ToList();
            var outPut = tickets.MapTo<List<TicketListDto>>();

            if (DynamicQueryable.Any(outPut))
            {
                ticketAvailabe = true;
                finalCount += outPut.Count;
                for (i = 0; i < outPut.Count; i++)
                {
                    var tableName = "";
                    int pax = outPut[i].Orders.Sum(s => s.NumberOfPax);
                    totalPax += pax;
                    if (!string.IsNullOrEmpty(outPut[i].TicketEntities))
                    {
                        var allEntities =
                            JsonConvert.DeserializeObject<IList<TicketEntityMap>>(outPut[i].TicketEntities);
                        var lastEntity = allEntities.LastOrDefault(a => a.EntityTypeId == 2);

                        if (lastEntity != null) tableName = lastEntity.EntityName;
                    }
                    var colCount = 1;
                    sheet.Cells[i + rowCount, colCount++].Value = outPut[i].LocationName;
                    sheet.Cells[i + rowCount, colCount++].Value = outPut[i].Id;
                    sheet.Cells[i + rowCount, colCount++].Value = outPut[i].TicketNumber;
                    sheet.Cells[i + rowCount, colCount++].Value = outPut[i].ReferenceTicket;

                    sheet.Cells[i + rowCount, colCount++].Value = outPut[i].InvoiceNo;
                    sheet.Cells[i + rowCount, colCount++].Value = outPut[i].Covers;
                    sheet.Cells[i + rowCount, colCount++].Value = outPut[i].ReferenceNumber;
                    sheet.Cells[i + rowCount, colCount++].Value = outPut[i].Credit;

                    sheet.Cells[i + rowCount, colCount++].Value =
                        outPut[i].TicketCreatedTime.ToString(ticketInput.DatetimeFormat);
                    sheet.Cells[i + rowCount, colCount++].Value =
                        outPut[i].LastOrderTime.ToString(ticketInput.DatetimeFormat);
                    sheet.Cells[i + rowCount, colCount++].Value =
                        outPut[i].LastPaymentTime.ToString(ticketInput.DatetimeFormat);

                    sheet.Cells[i + rowCount, colCount++].Value = outPut[i].DepartmentName;
                    sheet.Cells[i + rowCount, colCount++].Value = outPut[i].Note;
                    sheet.Cells[i + rowCount, colCount++].Value = outPut[i].LastModifiedUserName;
                    sheet.Cells[i + rowCount, colCount++].Value = outPut[i].TerminalName;
                    sheet.Cells[i + rowCount, colCount++].Value = outPut[i].TaxIncluded;
                    sheet.Cells[i + rowCount, colCount++].Value = outPut[i].TotalAmount;
                    sheet.Cells[i + rowCount, colCount++].Value = tableName;
                    sheet.Cells[i + rowCount, colCount++].Value = pax;
                    total += outPut[i].TotalAmount;

                    if (sList != null && sList.Any())
                    {
                        var perCe = sList.SingleOrDefault(a => a.Id == outPut[i].LocationId);
                        if (perCe != null)
                        {
                            var cValue = perCe.Percentage / 100 *
                                         outPut[i].TotalAmount;

                            sheet.Cells[i + rowCount, colCount++].Value = cValue;

                            if (franchiseTotal.ContainsKey(perCe.Id))
                            {
                                franchiseTotal[perCe.Id] += cValue;
                                franchiseTTotal[perCe.Id] += outPut[i].TotalAmount;
                            }
                            else
                            {
                                franchiseTotal.Add(perCe.Id, cValue);
                                franchiseTTotal.Add(perCe.Id, outPut[i].TotalAmount);
                            }
                        }
                        else
                        {
                            sheet.Cells[i + rowCount, colCount++].Value = "";
                        }
                    }

                    var colFromHex = ColorTranslator.FromHtml("#FCC9BE");

                    foreach (var ppt in pp)
                    {
                        var ee =
                            outPut[i].Payments.Where(
                                a => a.PaymentTypeName.Equals(ppt));

                        var ppamount = ee.Select(a => a.Amount).Sum();
                        var myCount = colCount++;
                        sheet.Cells[i + rowCount, myCount].Value = ppamount;
                        sheet.Cells[i + rowCount, myCount].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        sheet.Cells[i + rowCount, myCount].Style.Fill.BackgroundColor.SetColor(colFromHex);

                        if (pptotal.ContainsKey(ppt))
                            pptotal[ppt] += ppamount;
                        else
                            pptotal[ppt] = ppamount;
                    }

                    var mainTotal = outPut[i].TotalAmount;

                    if (mainTotal > 0)
                    {
                        mainTotal = mainTotal - mainTotal * 7 / 107;
                        mainTotal = Math.Round(mainTotal, _roundDecimals, MidpointRounding.AwayFromZero);
                    }

                    var taxAmount = outPut[i].TotalAmount - mainTotal;
                    taxAmount = Math.Round(taxAmount, _roundDecimals, MidpointRounding.AwayFromZero);
                    colFromHex = ColorTranslator.FromHtml("#FCEA0E");

                    foreach (var ppt in tt)
                        try
                        {
                            var transAmount = 0M;
                            if (ppt.ToUpper() == "PAYMENT")
                            {
                                transAmount = outPut[i].Payments.Sum(x => x.Amount);
                            }
                            else
                            {
                                var ee =
                                    outPut[i].Transactions.Where(a =>
                                        a.TransactionTypeName != null && a.TransactionTypeName.Equals(ppt));

                                transAmount = ee.Select(a => a.Amount).Sum();
                            }

                            var myCount = colCount++;
                            sheet.Cells[i + rowCount, myCount].Value = transAmount;
                            sheet.Cells[i + rowCount, myCount].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            sheet.Cells[i + rowCount, myCount].Style.Fill.BackgroundColor.SetColor(colFromHex);

                            if (ttotal.ContainsKey(ppt))
                                ttotal[ppt] += transAmount;
                            else
                                ttotal[ppt] = transAmount;
                        }
                        catch (Exception e)
                        {
                        }

                    if (mainTotal != 0M)
                    {
                        if (ttotal.ContainsKey("NETSALE"))
                            ttotal["NETSALE"] += mainTotal;
                        else
                            ttotal["NETSALE"] = mainTotal;
                        if (countryCode.Equals("TH"))
                            sheet.Cells[i + rowCount, colCount++].Value = mainTotal;
                    }

                    if (outPut[i].TicketTags != null && tagnames.Any())
                    {
                        var allTags =
                            JsonConvert.DeserializeObject<List<TicketTagValue>>(
                                outPut[i].TicketTags);
                        if (allTags != null && allTags.Count > 0)
                            foreach (var ttNames in tagnames)
                            {
                                var myTag = allTags.FirstOrDefault(a => a.TagName.Equals(ttNames));
                                sheet.Cells[i + rowCount, colCount++].Value = myTag != null ? myTag.TagValue : "";
                            }
                    }
                }

                rowCount = tickets.Count() + 2 + myRerowCount;
            }

            if (ticketAvailabe)
            {
                var tindex = 19;

                sheet.Cells[finalCount, 1].Value = L("Total");
                sheet.Cells[finalCount, 1].Style.Font.Bold = true;
                sheet.Cells[finalCount, 1].Style.Font.Size = 16;


                sheet.Cells[finalCount, 19].Value = totalPax;
                sheet.Cells[finalCount, 19].Style.Font.Bold = true;
                sheet.Cells[finalCount, 19].Style.Font.Size = 16;

                sheet.Cells[finalCount, 17].Value = total;
                sheet.Cells[finalCount, 17].Style.Font.Bold = true;
                sheet.Cells[finalCount, 17].Style.Font.Size = 16;
                foreach (var ppt in pp)
                {
                    tindex++;
                    sheet.Cells[finalCount, tindex].Style.Font.Bold = true;
                    sheet.Cells[finalCount, tindex].Style.Font.Size = 16;

                    sheet.Cells[finalCount, tindex].Value = pptotal[ppt];
                }

                foreach (var ppt in tt)
                {
                    tindex++;
                    sheet.Cells[finalCount, tindex].Style.Font.Bold = true;
                    sheet.Cells[finalCount, tindex].Style.Font.Size = 16;

                    sheet.Cells[finalCount, tindex].Value = ttotal[ppt];
                }

                tindex++;
                if (ttotal.ContainsKey("NETSALE") && countryCode.Equals("TH"))
                {
                    sheet.Cells[finalCount, tindex].Value = ttotal["NETSALE"];
                    sheet.Cells[finalCount, tindex].Style.Font.Bold = true;
                    sheet.Cells[finalCount, tindex].Style.Font.Size = 16;
                }

                for (i = 1; i <= 30; i++) sheet.Column(i).AutoFit();
            }

            if (franchiseTotal.Any() && sList != null && sList.Any())
            {
                var franChise = package.Workbook.Worksheets.Add(L("Franchise"));
                franChise.OutLineApplyStyle = true;

                var fHeaders = new List<string>
                {
                    L("Id"),
                    L("LocationName"),
                    L("TotalAmount"),
                    L("SharingTotal")
                };
                AddHeader(
                    franChise,
                    fHeaders.ToArray()
                );
                rowCount = 2;
                foreach (var key in franchiseTotal.Keys)
                {
                    franChise.Cells[rowCount, 1].Value = key;
                    franChise.Cells[rowCount, 2].Value = sList.First(a => a.Id == key).Name;
                    franChise.Cells[rowCount, 3].Value = franchiseTTotal[key];
                    franChise.Cells[rowCount, 4].Value = franchiseTotal[key];
                    rowCount++;
                }

                for (i = 1; i <= 1; i++) franChise.Column(i).AutoFit();
            }
        }

        private async Task GetTicketSyncs(ExcelPackage package, GetTicketSyncInput ticketInput,
            IConnectReportAppService appService)
        {
            var sheet = package.Workbook.Worksheets.Add(L("TicketSyncs"));
            sheet.OutLineApplyStyle = true;
            var headers = new List<string>
            {
                L("LocationCode"),
                L("TicketSyncCount"),
                L("TicketTotalAmount"),
                L("WorkStartDate"),
                L("WorkCloseDate"),
                L("TotalTickets"),
                L("WorkTotalAmount"),
                L("Difference")
            };


            var myRerowCount = await AddReportTicketSyncHeader(sheet, L("TicketSyncsReport"), ticketInput);

            var rowCount = myRerowCount;
            rowCount++;

            AddHeader(
                sheet, rowCount, headers.ToArray()
            );

            rowCount++;

            var ticketSyncs = await appService.GetTicketSyncs(ticketInput);

            if (ticketSyncs != null)
                foreach (var item in ticketSyncs.Items)
                {
                    sheet.Cells[rowCount, 1].Value = item.LocationCode;
                    sheet.Cells[rowCount, 2].Value = item.TicketSyncCount;
                    sheet.Cells[rowCount, 3].Value = item.TicketTotalAmount;
                    sheet.Cells[rowCount, 4].Value = item.WorkStartDate.ToString(ticketInput.DateFormat);
                    sheet.Cells[rowCount, 5].Value = item.WorkCloseDate.ToString(ticketInput.DateFormat);
                    sheet.Cells[rowCount, 6].Value = item.TotalTickets;
                    sheet.Cells[rowCount, 7].Value = item.WorkTotalAmount;
                    sheet.Cells[rowCount, 8].Value = item.IsDifference ? "True" : "False";
                    rowCount++;
                }
        }


        private async Task GetRefundTickets(ExcelPackage package, GetTicketInput ticketInput,
            IConnectReportAppService appService,
            List<string> tt, List<string> pp, List<SharingPercentageDto> sList, List<string> tagnames)
        {
            var sheet = package.Workbook.Worksheets.Add(L("RefundTickets"));
            sheet.OutLineApplyStyle = true;
            var headers = new List<string>
            {
                L("Date"),
                L("Location"),
                L("TicketNo"),
                L("InvoiceNo"),
                L("Department"),
                L("Note"),
                L("Employee"),
                L("Terminal"),
                L("TableNumber"),
                L("Pax"),
                L("TotalAmount"),                
                L("Payments")
            };

            var rowCount = AddReportHeader(sheet, "Refund Report", ticketInput);

            AddHeader(
                sheet, rowCount,
                headers.ToArray()
            );

            rowCount++;

            var i = 0;
            decimal total = 0;
            int totalPax = 0;
            var secondTime = false;
            var ticketAvailabe = false;
            while (true)
            {
                ticketInput.MaxResultCount = 100;
                if (secondTime) ticketInput.NotCorrectDate = true;

                var outPut = await appService.GetTickets(ticketInput);

                if (!secondTime) secondTime = true;

                if (DynamicQueryable.Any(outPut.TicketList.Items))
                {
                    ticketAvailabe = true;

                    foreach (var item in outPut.TicketList.Items)
                    {
                        var tableName = "";
                        int pax = item.Orders.Sum(s => s.NumberOfPax);
                        totalPax += pax;
                        if (!string.IsNullOrEmpty(item.TicketEntities))
                        {
                            var allEntities =
                                JsonConvert.DeserializeObject<IList<TicketEntityMap>>(item.TicketEntities);
                            var lastEntity = allEntities.LastOrDefault(a => a.EntityTypeId == 2);

                            if (lastEntity != null) tableName = lastEntity.EntityName;
                        }
                        var colCount = 1;
                        sheet.Cells[rowCount, colCount++].Value =
                            item.LastPaymentTime.ToString(ticketInput.DatetimeFormat);
                        sheet.Cells[rowCount, colCount++].Value = item.LocationName;
                        sheet.Cells[rowCount, colCount++].Value = item.TicketNumber;
                        sheet.Cells[rowCount, colCount++].Value = item.InvoiceNo;
                        sheet.Cells[rowCount, colCount++].Value = item.DepartmentName;
                        sheet.Cells[rowCount, colCount++].Value = item.Note;
                        sheet.Cells[rowCount, colCount++].Value = item.LastModifiedUserName;
                        sheet.Cells[rowCount, colCount++].Value = item.TerminalName;
                        sheet.Cells[rowCount, colCount++].Value = tableName;
                        sheet.Cells[rowCount, colCount++].Value = pax;
                        var lastRefund = item.TicketTagValues.LastOrDefault(a => a.TagName.Equals("Refund"));
                        if (lastRefund != null)
                        {
                            sheet.Cells[rowCount, colCount++].Value = lastRefund.TagValue;
                            total += Convert.ToDecimal(lastRefund.TagValue);
                        }

                        lastRefund = item.TicketTagValues.LastOrDefault(a => a.TagName.Equals("CancelPayments"));
                        if (lastRefund != null)
                            sheet.Cells[rowCount, colCount++].Value =
                                lastRefund.TagValue.Replace("\u000d\u000a", Environment.NewLine);

                        rowCount++;
                    }

                    ticketInput.SkipCount = ticketInput.SkipCount + ticketInput.MaxResultCount;
                    //rowCount = ticketInput.SkipCount + 2;
                }
                else
                {
                    break;
                }
            }

            if (ticketAvailabe)
            {
                var tindex = 11;
                rowCount++;
                sheet.Cells[rowCount, 1].Value = L("Total");
                sheet.Cells[rowCount, 10].Value = totalPax;
                sheet.Cells[rowCount, 10].Style.Font.Bold = true;
                sheet.Cells[rowCount, 10].Style.Font.Size = 16;

                sheet.Cells[rowCount, 1].Value = L("Total");
                sheet.Cells[rowCount, tindex].Value = total;
                sheet.Cells[rowCount, tindex].Style.Font.Bold = true;
                sheet.Cells[rowCount, tindex].Style.Font.Size = 16;

                for (i = 1; i <= 30; i++) sheet.Column(i).AutoFit();
            }
        }

        private void GetRefundTicketDetail(ExcelPackage package, GetTicketInput ticketInput,
            List<RefundTicketDetail> details, string dateFormat, string datetimeFormat)
        {
            var sheet = package.Workbook.Worksheets.Add(L("RefundDetail"));
            sheet.OutLineApplyStyle = true;
            var row = AddReportHeader(sheet, L("RefundDetail"), ticketInput);
            row++;
            var headers = new List<string>
            {
                L("Date"),
                L("Time"),
                L("InvoiceNo"),
                L("Department"),
                L("MenuItem"),
                L("Qty"),
                L("Note"),
                L("Employee"),
                L("Terminal"),
                L("SubTotal")
            };

            AddHeader(
                sheet, row++, headers.ToArray()
            );
            AddObjects(sheet, row++, details,
                _ => _.LastPaymentTime.ToString(dateFormat),
                _ => _.LastPaymentTime.ToString(_timeFormat),
                _ => _.InvoiceNo,
                _ => _.DepartmentName,
                _ => _.MenuItem,
                _ => _.Quantity,
                _ => _.Note,
                _ => _.Employee,
                _ => _.TerminalName,
                _ => Math.Round(_.SubAmount, _roundDecimals, MidpointRounding.AwayFromZero)
            );

            row = details.Count + row++;
            sheet.Cells[row, 1].Value = L("Total");
            sheet.Cells[row, 6].Value = details.Sum(t => t.Quantity);
            sheet.Cells[row, 10].Value =
                Math.Round(details.Sum(t => t.SubAmount), _roundDecimals, MidpointRounding.AwayFromZero);

            sheet.Cells[row, 1, row, 12].Style.Font.Bold = true;
            for (var i = 1; i <= 15; i++) sheet.Column(i).AutoFit();
        }

        private async Task GetAllOrders(ExcelPackage package, GetTicketInput ticketInput,
            IConnectReportAppService appService,
            List<string> tt, List<string> pp, List<SharingPercentageDto> sList, List<MenuItemListDto> menuItems)
        {
            var sheet = package.Workbook.Worksheets.Add(L("All"));
            sheet.OutLineApplyStyle = true;
            var headers = new List<string>
            {
                L("Id"),
                L("TicketId"),
                L("LocationName"),
                L("DepartmentName"),
                L("CreatedDate"),
                L("CreatedTime"),
                L("OrderId"),
                L("Category"),
                L("MenuItem"),
                L("Portion"),
                L("Quantity"),
                L("Price"),
                L("TotalAmount"),
                L("Note")
            };
            if (sList != null && sList.Any()) headers.Add(L("FranchiseAmount"));
            headers.AddRange(pp);
            headers.AddRange(tt);
            headers.Add(L("ItemTax"));
            var rowCount = AddReportHeader(sheet, "Orders Report", ticketInput);

            AddHeader(
                sheet, rowCount,
                headers.ToArray()
            );
            var franchiseTotal = new Dictionary<int, decimal>();
            var franchiseTTotal = new Dictionary<int, decimal>();

            var pptotal = new Dictionary<string, decimal>();
            var ttotal = new Dictionary<string, decimal>();
            var secondTime = false;

            while (true)
            {
                ticketInput.MaxResultCount = 100;
                if (secondTime) ticketInput.NotCorrectDate = true;
                var outPut = await appService.GetTickets(ticketInput);

                if (!secondTime) secondTime = true;
                if (DynamicQueryable.Any(outPut.TicketList.Items))
                {
                    foreach (var t in outPut.TicketList.Items)
                    foreach (var orderListDto in t.Orders.Where(a => a.CalculatePrice))
                    {
                        var colIndex = 1;

                        sheet.Cells[rowCount, colIndex++].Value = t.Id;
                        sheet.Cells[rowCount, colIndex++].Value = t.TicketId;
                        sheet.Cells[rowCount, colIndex++].Value = t.LocationName;
                        sheet.Cells[rowCount, colIndex++].Value = t.DepartmentName;
                        sheet.Cells[rowCount, colIndex++].Value = t.TicketCreatedTime.ToString(ticketInput.DateFormat);
                        sheet.Cells[rowCount, colIndex++].Value = t.TicketCreatedTime.ToString(ticketInput.DateFormat);
                        sheet.Cells[rowCount, colIndex++].Value = orderListDto.OrderId;
                        sheet.Cells[rowCount, colIndex++].Value =
                            menuItems.First(a => a.Id == orderListDto.MenuItemPortionId).CategoryName;
                        sheet.Cells[rowCount, colIndex++].Value = orderListDto.MenuItemName;
                        sheet.Cells[rowCount, colIndex++].Value = orderListDto.PortionName;
                        sheet.Cells[rowCount, colIndex++].Value = orderListDto.Quantity;
                        sheet.Cells[rowCount, colIndex++].Value = orderListDto.Price;
                        sheet.Cells[rowCount, colIndex++].Value = orderListDto.Price * orderListDto.Quantity;
                        sheet.Cells[rowCount, colIndex++].Value = t.Note;

                        if (sList != null && sList.Any())
                        {
                            var perCe = sList.SingleOrDefault(a => a.Id == t.LocationId);
                            if (perCe != null)
                            {
                                var cValue = perCe.Percentage / 100 *
                                             t.TotalAmount;
                                sheet.Cells[rowCount, colIndex++].Value = cValue;
                                if (franchiseTotal.ContainsKey(perCe.Id))
                                {
                                    franchiseTotal[perCe.Id] += cValue;
                                    franchiseTTotal[perCe.Id] += t.TotalAmount;
                                }
                                else
                                {
                                    franchiseTotal.Add(perCe.Id, cValue);
                                    franchiseTTotal.Add(perCe.Id, t.TotalAmount);
                                }
                            }
                            else
                            {
                                sheet.Cells[rowCount, colIndex++].Value = "";
                            }
                        }

                        foreach (var ppt in pp)
                        {
                            var ee =
                                t.Payments.FirstOrDefault(
                                    a => a.PaymentTypeName.Equals(ppt));

                            var ppamount = ee?.Amount ?? 0M;
                            sheet.Cells[rowCount, colIndex++].Value = ppamount;

                            if (pptotal.ContainsKey(ppt))
                                pptotal[ppt] += ppamount;
                            else
                                pptotal[ppt] = ppamount;
                        }

                        foreach (var ppt in tt)
                            try
                            {
                                var ee =
                                    t.Transactions.FirstOrDefault(a =>
                                        a.TransactionTypeName != null && a.TransactionTypeName.Equals(ppt));

                                var ttamount = ee?.Amount ?? 0M;
                                sheet.Cells[rowCount, colIndex++].Value = ttamount;

                                if (ttotal.ContainsKey(ppt))
                                    ttotal[ppt] += ttamount;
                                else
                                    ttotal[ppt] = ttamount;
                            }
                            catch (Exception)
                            {
                            }

                        var taxes = orderListDto.TaxValues;
                        if (taxes.Any())
                            foreach (var ppt in tt)
                            foreach (var taxValue in taxes)
                                if (ppt.Equals(taxValue.TaxTemplateName))
                                {
                                    var myTax = taxValue.TaxRate + 100;
                                    var taxVale = t.TotalAmount * taxValue.TaxRate / myTax;
                                    sheet.Cells[rowCount, colIndex++].Value =
                                        Math.Round(taxVale, 2, MidpointRounding.AwayFromZero);
                                }

                        rowCount++;
                    }

                    ticketInput.SkipCount = ticketInput.SkipCount + ticketInput.MaxResultCount;
                }
                else
                {
                    break;
                }
            }

            for (var i = 1; i <= 1; i++) sheet.Column(i).AutoFit();

            if (franchiseTotal.Any() && sList != null && sList.Any())
            {
                var franChise = package.Workbook.Worksheets.Add(L("Franchise"));
                franChise.OutLineApplyStyle = true;

                var fHeaders = new List<string>
                {
                    L("Id"),
                    L("LocationName"),
                    L("TotalAmount"),
                    L("SharingTotal")
                };
                rowCount = AddReportHeader(sheet, "Franchise Report", ticketInput);

                AddHeader(
                    franChise, rowCount,
                    fHeaders.ToArray()
                );
                foreach (var key in franchiseTotal.Keys)
                {
                    franChise.Cells[rowCount, 1].Value = key;
                    franChise.Cells[rowCount, 2].Value = sList.First(a => a.Id == key).Name;
                    franChise.Cells[rowCount, 3].Value = franchiseTTotal[key];
                    franChise.Cells[rowCount, 4].Value = franchiseTotal[key];
                    rowCount++;
                }

                for (var i = 1; i <= 1; i++) franChise.Column(i).AutoFit();
            }
        }

        private async Task GetItems(ExcelPackage package, GetItemInput input, IConnectReportAppService appService)
        {
            var sheet = package.Workbook.Worksheets.Add(L("Items"));
            sheet.OutLineApplyStyle = true;
            var rowCount = AddReportHeader(sheet, "Items Report", input);

            AddHeader(
                sheet, rowCount,
                L("Location"),
                L("Category"),
                L("MenuItem"),
                L("Portion"),
                L("Quantity"),
                L("Price"),
                L("Total")
            );

            var outPut = new ItemStatsDto();
            if (input.RunInBackground)
                outPut = await appService.GetItemSalesForBackground(input);
            else
                outPut = await appService.GetItemSales(input);

            if (DynamicQueryable.Any(outPut.MenuList.Items))
            {
                AddObjects(
                    sheet, rowCount, outPut.MenuList.Items.MapTo<IList<MenuListDto>>(),
                    _ => _.LocationName,
                    _ => _.CategoryName,
                    _ => _.MenuItemName,
                    _ => _.MenuItemPortionName,
                    _ => _.Quantity,
                    _ => _.Price,
                    _ => _.Total
                );
                input.SkipCount = input.SkipCount + input.MaxResultCount;
            }

            for (var i = 1; i <= 20; i++) sheet.Column(i).AutoFit();
        }

        private async Task GetItemsForDate(ExcelPackage package, GetItemInput input,
            IConnectReportAppService appService)
        {
            var sheet = package.Workbook.Worksheets.Add(L("Items"));
            sheet.OutLineApplyStyle = true;

            var startOfDate = input.StartDate;
            var endOfDate = input.EndDate;

            if (input.Duration != null && input.Duration.Equals("M"))
            {
                var myDate = input.StartDate;
                startOfDate = new DateTime(myDate.Year, myDate.Month, 1);
                endOfDate = startOfDate.AddMonths(1);
            }

            if (input.Locations == null || !input.Locations.Any())
                if (input.LocationGroup != null && !input.LocationGroup.Group)
                    input.Locations = input.LocationGroup.Locations;
            var outPut = await appService.GetDaysItemSales(new GetDayItemInput
            {
                StartDate = startOfDate,
                EndDate = endOfDate,
                Void = input.Void,
                Gift = input.Gift,
                Comp = input.Comp,
                Locations = input.Locations,
                Sorting = "CategoryName",
                Stats = false,
                FilterByCategory = input.FilterByCategory,
                FilterByDepartment = input.FilterByDepartment,
                FilterByGroup = input.FilterByGroup,
                LocationGroup = input.LocationGroup,
                FilterByTag = input.FilterByTag,
                MenuItemIds = input.MenuItemIds,
                MenuItemPortionIds = input.MenuItemPortionIds,
                DynamicFilter = input.DynamicFilter
            });
            var rowCount = AddReportHeader(sheet, "Item Sales Report", input);
            rowCount = AddHeaderForDate(sheet, rowCount++, false, input, outPut);
            rowCount = CreateItemsForDateRows(input, sheet, outPut, rowCount, outPut.Items);

            for (var i = 1; i <= 30; i++) sheet.Column(i).AutoFit();
        }

        private int CreateItemsForDateRows(GetItemInput input, ExcelWorksheet sheet, IdDayItemOutput outPut,
            int rowCount, List<IdDayItemOutputDto> items)
        {
            var allDates = outPut.AllDates.ToArray();
            if (input.Duration.Equals("S"))
                foreach (var pos in items.GroupBy(a => a.CategoryName))
                foreach (var tets in pos)
                {
                    var colCount = 1;

                    sheet.Cells[rowCount, colCount++].Value = tets.AliasCode;
                    sheet.Cells[rowCount, colCount++].Value = tets.CategoryCode;
                    sheet.Cells[rowCount, colCount++].Value = tets.CategoryName;
                    sheet.Cells[rowCount, colCount++].Value = tets.MenuItemName;
                    sheet.Cells[rowCount, colCount++].Value = tets.Tag;
                    sheet.Cells[rowCount, colCount++].Value = tets.PortionName;

                    if (input.ExportOutput.Equals("T"))
                    {
                        sheet.Cells[rowCount, colCount++].Value = tets.Items.Sum(a => a.Total);
                    }
                    else if (input.ExportOutput.Equals("Q"))
                    {
                        sheet.Cells[rowCount, colCount++].Value = tets.Items.Sum(a => a.Quantity);
                    }
                    else
                    {
                        sheet.Cells[rowCount, colCount++].Value = tets.Items.Sum(a => a.Quantity);
                        sheet.Cells[rowCount, colCount++].Value = tets.Items.Sum(a => a.Total);
                    }

                    rowCount++;
                }
            else
                foreach (var pos in items)
                {
                    var colCount = 1;
                    sheet.Cells[rowCount, colCount++].Value = pos.AliasCode;
                    sheet.Cells[rowCount, colCount++].Value = pos.CategoryCode;
                    sheet.Cells[rowCount, colCount++].Value = pos.CategoryName;
                    sheet.Cells[rowCount, colCount++].Value = pos.MenuItemName;
                    sheet.Cells[rowCount, colCount++].Value = pos.Tag;
                    sheet.Cells[rowCount, colCount++].Value = pos.PortionName;

                    foreach (var myDate in allDates)
                    {
                        var outputD = pos.Items.FirstOrDefault(a => a.Date.Equals(myDate));
                        if (outputD != null)
                        {
                            if (input.ExportOutput.Equals("T"))
                            {
                                sheet.Cells[rowCount, colCount++].Value = Math.Round(outputD.Total, _roundDecimals,
                                    MidpointRounding.AwayFromZero);
                            }
                            else if (input.ExportOutput.Equals("Q"))
                            {
                                sheet.Cells[rowCount, colCount++].Value = Math.Round(outputD.Quantity, _roundDecimals,
                                    MidpointRounding.AwayFromZero);
                            }
                            else
                            {
                                if (outputD.Quantity > 0M)
                                {
                                    sheet.Cells[rowCount, colCount++].Value =
                                        Math.Round(outputD.Quantity, _roundDecimals, MidpointRounding.AwayFromZero);
                                    sheet.Cells[rowCount, colCount++].Value = Math.Round(outputD.Total, _roundDecimals,
                                        MidpointRounding.AwayFromZero);
                                }
                                else
                                {
                                    sheet.Cells[rowCount, colCount++].Value = 0;
                                    sheet.Cells[rowCount, colCount++].Value = Math.Round(outputD.Total, _roundDecimals,
                                        MidpointRounding.AwayFromZero);
                                }
                            }
                        }
                        else
                        {
                            if (input.ExportOutput.Equals("B"))
                            {
                                sheet.Cells[rowCount, colCount++].Value = 0;
                                sheet.Cells[rowCount, colCount++].Value = 0;
                            }
                            else
                            {
                                sheet.Cells[rowCount, colCount++].Value = 0;
                            }
                        }
                    }

                    if (input.ExportOutput.Equals("T")) // Daily report - Grand Total
                    {
                        var grandTotal = pos.Items.Sum(a => a.Total);
                        sheet.Cells[rowCount, colCount++].Value =
                            Math.Round(grandTotal, _roundDecimals, MidpointRounding.AwayFromZero);
                    }

                    if (input.ExportOutput.Equals("Q")) // Daily report - Grand Total
                    {
                        var grandTotal = pos.Items.Sum(a => a.Quantity);
                        sheet.Cells[rowCount, colCount++].Value =
                            Math.Round(grandTotal, _roundDecimals, MidpointRounding.AwayFromZero);
                    }

                    if (input.ExportOutput.Equals("B")) // Daily report - Grand Total
                    {
                        var grandTotalQ = pos.Items.Sum(a => a.Quantity);
                        var grandTotalT = pos.Items.Sum(a => a.Total);
                        sheet.Cells[rowCount, colCount++].Value = Math.Round(grandTotalQ, _roundDecimals,
                            MidpointRounding.AwayFromZero);
                        sheet.Cells[rowCount, colCount++].Value = Math.Round(grandTotalT, _roundDecimals,
                            MidpointRounding.AwayFromZero);
                    }

                    rowCount++;
                }

            var col = 1;
            sheet.Cells[rowCount, col++].Value = L("Total");
            sheet.Cells[rowCount, col++].Value = "";
            sheet.Cells[rowCount, col++].Value = "";
            sheet.Cells[rowCount, col++].Value = "";
            sheet.Cells[rowCount, col++].Value = "";
            sheet.Cells[rowCount, col++].Value = "";
            var allSal = items.SelectMany(a => a.Items);
            if (!input.Duration.Equals("S"))
            {
                foreach (var allDate in outPut.AllDates)
                {
                    var sumQuantity = allSal.Where(a => a.Date.Equals(allDate));

                    if (input.ExportOutput.Equals("B"))
                    {
                        if (sumQuantity.Any())
                        {
                            var quA = sumQuantity.Sum(a => a.Quantity);
                            var toT = sumQuantity.Sum(a => a.Total);

                            sheet.Cells[rowCount, col++].Value =
                                Math.Round(quA, _roundDecimals, MidpointRounding.AwayFromZero);
                            sheet.Cells[rowCount, col++].Value =
                                Math.Round(toT, _roundDecimals, MidpointRounding.AwayFromZero);
                        }
                    }
                    else
                    {
                        if (input.ExportOutput.Equals("T"))
                        {
                            var quA = sumQuantity.Sum(a => a.Total);
                            sheet.Cells[rowCount, col++].Value =
                                Math.Round(quA, _roundDecimals, MidpointRounding.AwayFromZero);
                        }
                        else
                        {
                            var quA = sumQuantity.Sum(a => a.Quantity);
                            sheet.Cells[rowCount, col++].Value =
                                Math.Round(quA, _roundDecimals, MidpointRounding.AwayFromZero);
                        }
                    }
                }

                if (input.ExportOutput.Equals("T")) // Daily report - Grand Total
                {
                    var grandTotal = allSal.Sum(a => a.Total);
                    sheet.Cells[rowCount, col++].Value =
                        Math.Round(grandTotal, _roundDecimals, MidpointRounding.AwayFromZero);
                }

                if (input.ExportOutput.Equals("Q")) // Daily report - Grand Total
                {
                    var grandTotal = allSal.Sum(a => a.Quantity);
                    sheet.Cells[rowCount, col++].Value =
                        Math.Round(grandTotal, _roundDecimals, MidpointRounding.AwayFromZero);
                }

                if (input.ExportOutput.Equals("B")) // Daily report - Grand Total
                {
                    var grandTotalQ = allSal.Sum(a => a.Quantity);
                    var grandTotalT = allSal.Sum(a => a.Total);
                    sheet.Cells[rowCount, col++].Value =
                        Math.Round(grandTotalQ, _roundDecimals, MidpointRounding.AwayFromZero);
                    sheet.Cells[rowCount, col++].Value =
                        Math.Round(grandTotalT, _roundDecimals, MidpointRounding.AwayFromZero);
                }
            }
            else
            {
                var quA = allSal.Sum(a => a.Quantity);
                var toT = allSal.Sum(a => a.Total);
                if (input.ExportOutput.Equals("T"))
                {
                    sheet.Cells[rowCount, col++].Value = Math.Round(toT, _roundDecimals, MidpointRounding.AwayFromZero);
                }
                else if (input.ExportOutput.Equals("Q"))
                {
                    sheet.Cells[rowCount, col++].Value = Math.Round(quA, _roundDecimals, MidpointRounding.AwayFromZero);
                }
                else
                {
                    sheet.Cells[rowCount, col++].Value = Math.Round(quA, _roundDecimals, MidpointRounding.AwayFromZero);
                    sheet.Cells[rowCount, col++].Value = Math.Round(toT, _roundDecimals, MidpointRounding.AwayFromZero);
                }
            }

            return rowCount++;
        }

        private async Task GetItemsForLocation(ExcelPackage package, GetItemInput input,
            IConnectReportAppService appService)
        {
            var sheet = package.Workbook.Worksheets.Add(L("Items"));
            sheet.OutLineApplyStyle = true;

            var startOfDate = input.StartDate;
            var endOfDate = input.EndDate;

            if (input.Duration.Equals("M"))
            {
                var myDate = input.StartDate;
                startOfDate = new DateTime(myDate.Year, myDate.Month, 1);
                endOfDate = startOfDate.AddMonths(1);
            }

            var locationsByUser =
                _locationAppService.GetLocationsForUser(new GetLocationInputBasedOnUser { UserId = input.UserId });

            var outPut = await appService.GetDaysItemSales(new GetDayItemInput
            {
                StartDate = startOfDate,
                EndDate = endOfDate,
                Void = input.Void,
                Gift = input.Gift,
                Comp = input.Comp,
                Locations = input.Locations,
                Sorting = "CategoryName",
                Stats = false,
                ByLocation = true,
                LocationsByUser = locationsByUser,
                TenantId = input.TenantId.Value,
                UserId = input.UserId,
                FilterByCategory = input.FilterByCategory,
                FilterByDepartment = input.FilterByDepartment,
                FilterByGroup = input.FilterByGroup,
                LocationGroup = input.LocationGroup,
                FilterByTag = input.FilterByTag,
                MenuItemIds = input.MenuItemIds,
                MenuItemPortionIds = input.MenuItemPortionIds,
                DynamicFilter = input.DynamicFilter
            });
            var rowCount = AddReportHeader(sheet, "Item Sales", input);

            rowCount = AddHeaderForLocation(sheet, rowCount++, false, input, outPut);

            CreateItemForLocationRows(input, sheet, outPut.Items, outPut, rowCount);

            for (var i = 1; i <= 20; i++) sheet.Column(i).AutoFit();
        }

        private int AddHeaderForDate(ExcelWorksheet sheet, int rowCount, bool isOrderTag, GetItemInput input,
            IdDayItemOutput outPut)
        {
            var headers = new List<string>
            {
                L("AliasCode"),
                L("CategoryCode"),
                L("Category"),
                L("MenuItem"),
                L("ItemTag"),
                L("Portion")
            };

            if (isOrderTag)
            {
                sheet.Cells[rowCount, 1, rowCount, headers.Count].Style.Font.Bold = true;
                rowCount += 2;

                sheet.Cells[rowCount, 1].Value = "Order Tag Item";
                sheet.Cells[rowCount, 1, rowCount, headers.Count].Merge = true;
                sheet.Cells[rowCount, 1, rowCount, headers.Count].Style.Font.Bold = true;
            }

            if (input.Duration.Equals("S"))
            {
                if (input.ExportOutput.Equals("T"))
                {
                    headers.Add(L("Total"));
                }
                else if (input.ExportOutput.Equals("Q"))
                {
                    headers.Add(L("Quantity"));
                }
                else
                {
                    headers.Add(L("Quantity"));
                    headers.Add(L("Total"));
                }

                rowCount++;
                AddHeader(
                    sheet, rowCount, headers.ToArray()
                );

                rowCount++;
            }
            else
            {
                if (input.ExportOutput.Equals("B"))
                {
                    foreach (var allDate in outPut.AllDates)
                    {
                        headers.Add(allDate);
                        headers.Add("");
                    }

                    rowCount++;
                    AddHeader(
                        sheet, rowCount, headers.ToArray()
                    );
                    rowCount++;
                    headers = new List<string>
                    {
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""
                    };

                    foreach (var allDate in outPut.AllDates)
                    {
                        headers.Add(L("Quantity"));
                        headers.Add(L("Total"));
                    }

                    headers.Add(L("GrandTotal") + " " + L("Quantity"));
                    headers.Add(L("GrandTotal") + " " + L("Total"));
                    var colCount = 1;
                    foreach (var t in headers) sheet.Cells[rowCount, colCount++].Value = t;

                    rowCount++;
                }
                else
                {
                    headers.AddRange(outPut.AllDates);
                    headers.Add(L("GrandTotal"));
                    rowCount++;
                    AddHeader(
                        sheet, rowCount, headers.ToArray()
                    );
                    rowCount++;
                }
            }

            return rowCount++;
        }

        private int AddHeaderForLocation(ExcelWorksheet sheet, int rowCount, bool isOrderTag, GetItemInput input,
            IdDayItemOutput outPut)
        {
            var headers = new List<string>
            {
                L("Category"),
                L("Code"),
                L("MenuItem"),
                L("Portion")
            };

            if (isOrderTag)
            {
                sheet.Cells[rowCount, 1, rowCount, headers.Count].Style.Font.Bold = true;
                rowCount += 2;

                sheet.Cells[rowCount, 1].Value = "Order Tag Item";
                sheet.Cells[rowCount, 1, rowCount, headers.Count].Merge = true;
                sheet.Cells[rowCount, 1, rowCount, headers.Count].Style.Font.Bold = true;
            }

            if (input.ExportOutput.Equals("B"))
            {
                foreach (var locationT in outPut.AllLocations.Keys)
                {
                    headers.Add(locationT);
                    headers.Add("");
                }

                headers.Add(L("QtyTotal"));
                headers.Add(L("SumTotal"));
            }
            else
            {
                headers.AddRange(outPut.AllLocations.Keys);
                headers.Add(L("Grand"));
            }

            var colCount = 1;
            rowCount++;
            AddHeader(
                sheet, rowCount, headers.ToArray()
            );

            rowCount++;
            if (input.ExportOutput.Equals("B"))
            {
                colCount = 1;
                headers = new List<string>
                {
                    "",
                    "",
                    "",
                    ""
                };

                foreach (var allDate in outPut.AllLocations)
                {
                    headers.Add(L("Quantity"));
                    headers.Add(L("Total"));
                }

                foreach (var t in headers)
                    sheet.Cells[rowCount, colCount++].Value = t;

                rowCount++;
            }

            return rowCount;
        }

        private async Task CreateFileForItemAndOrderTagSales(ExcelPackage package, GetItemInput input,
            IConnectReportAppService appService)
        {
            var sheet = package.Workbook.Worksheets.Add(L("Items Sale"));
            sheet.OutLineApplyStyle = true;

            var outPut = await appService.GetItemSales(input);
            var headers = new List<string>
            {
                L("AliasCode"),
                L("MenuItem"),
                L("Portion"),
                L("Quantity"),
                L("PriceExcludeTax"),
                L("AmountExcludeTax"),
                L("Tax"),
                L("AmountIncludeTax")
            };
            var rowCount = AddReportHeader(sheet, "Item Sales Report", input);

            AddHeader(sheet, rowCount, headers.ToArray());

            rowCount = CreateRowsForItemAndOrderTag(sheet, outPut.MenuList.Items.Where(i => !i.IsOrderTag).ToList(),
                rowCount);

            //rowCount++;
            //sheet.Cells[rowCount, 1].Value = "Order Tag Sales";
            //sheet.Cells[rowCount, 1, rowCount, 5].Merge = true;
            //sheet.Cells[rowCount, 1, rowCount, 30].Style.Font.Bold = true;
            //rowCount++;

            //AddHeader(sheet, rowCount, headers.ToArray());

            //rowCount = CreateRowsForItemAndOrderTag(sheet, outPut.MenuList.Items.Where(i => i.IsOrderTag).ToList(), rowCount);

            for (var i = 1; i <= 30; i++) sheet.Column(i).AutoFit();
        }

        private int CreateRowsForItemAndOrderTag(ExcelWorksheet sheet, List<MenuListDto> outPut, int rowCount)
        {
            rowCount++;
            var amountET = 0M;
            var totalTax = 0M;
            var amountIT = 0M;

            foreach (var pos in outPut.GroupBy(e => new { e.MenuItemId, e.MenuItemPortionId }).Select(g => g.ToList()))
            {
                var colCount = 1;
                var quantity = pos.Sum(p => p.Quantity);
                sheet.Cells[rowCount, colCount++].Value = pos.First().AliasCode;
                sheet.Cells[rowCount, colCount++].Value = pos.First().MenuItemName;
                sheet.Cells[rowCount, colCount++].Value = pos.First().MenuItemPortionName;
                sheet.Cells[rowCount, colCount++].Value = quantity;
                //var priceExcludeTax = pos.First().IncludedTax ? pos.First().Price - pos.First().TaxPrice : pos.First().Price;
                var priceExcludeTax = pos.Sum(p => p.TaxPrice);
                var amountExcludeTax = pos.Sum(p => p.LineNoTaxTotal);
                var tax = pos.Sum(p => p.ActualTax);
                var amountIncludeTax = pos.Sum(p => p.Total);

                amountET += amountExcludeTax;
                totalTax += tax;
                amountIT += amountIncludeTax;

                sheet.Cells[rowCount, colCount++].Value =
                    Math.Round(priceExcludeTax, _roundDecimals, MidpointRounding.AwayFromZero);
                sheet.Cells[rowCount, colCount++].Value =
                    Math.Round(amountExcludeTax, _roundDecimals, MidpointRounding.AwayFromZero);
                sheet.Cells[rowCount, colCount++].Value =
                    Math.Round(tax, _roundDecimals, MidpointRounding.AwayFromZero);
                sheet.Cells[rowCount, colCount++].Value =
                    Math.Round(amountIncludeTax, _roundDecimals, MidpointRounding.AwayFromZero);

                rowCount++;
            }

            sheet.Cells[rowCount, 1].Value = "Total";
            sheet.Cells[rowCount, 4].Value = outPut.Sum(i => i.Quantity);
            sheet.Cells[rowCount, 6].Value = Math.Round(amountET, _roundDecimals, MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, 7].Value = Math.Round(totalTax, _roundDecimals, MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, 8].Value = Math.Round(amountIT, _roundDecimals, MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, 1, rowCount, 30].Style.Font.Bold = true;

            return rowCount + 1;
        }

        private int CreateItemForLocationRows(GetItemInput input, ExcelWorksheet sheet, List<IdDayItemOutputDto> items,
            IdDayItemOutput outPut, int rowCount)
        {
            int colCount;

            var sumDc = new Dictionary<string, decimal>();
            var quaDc = new Dictionary<string, decimal>();

            var grandQuantity = 0M;
            var grandTotal = 0M;
            foreach (var itemT in items.GroupBy(e => e.PortionId).Select(g => g.FirstOrDefault()))
            {
                colCount = 1;
                sheet.Cells[rowCount, colCount++].Value = itemT.CategoryName;
                sheet.Cells[rowCount, colCount++].Value = itemT.AliasCode;
                sheet.Cells[rowCount, colCount++].Value = itemT.MenuItemName;
                sheet.Cells[rowCount, colCount++].Value = itemT.PortionName;

                foreach (var locationT in outPut.AllLocations.Values)
                {
                    var myLocItems =
                        items.Where(a => a.LocationName.Equals(locationT) && a.PortionId == itemT.PortionId);

                    var quantity = 0M;
                    var total = 0M;
                    if (myLocItems.Any())
                    {
                        quantity = myLocItems.SelectMany(a => a.Items).Sum(a => a.Quantity);
                        total = myLocItems.SelectMany(a => a.Items).Sum(a => a.Total);

                        if (sumDc.Any() && sumDc.ContainsKey(locationT))
                        {
                            var sumC = sumDc[locationT];
                            sumC += total;
                            sumDc[locationT] = sumC;

                            var quav = quaDc[locationT];
                            quav += quantity;
                            quaDc[locationT] = quav;
                        }
                        else
                        {
                            sumDc[locationT] = total;
                            quaDc[locationT] = quantity;
                        }
                    }

                    if (input.ExportOutput.Equals("B"))
                    {
                        sheet.Cells[rowCount, colCount++].Value = quantity;
                        sheet.Cells[rowCount, colCount++].Value = total;
                    }
                    else if (input.ExportOutput.Equals("T"))
                    {
                        sheet.Cells[rowCount, colCount++].Value = total;
                    }
                    else
                    {
                        sheet.Cells[rowCount, colCount++].Value = quantity;
                    }
                }

                var allPItens = items.Where(a => a.PortionId == itemT.PortionId).SelectMany(a => a.Items);

                grandQuantity += allPItens.Sum(a => a.Quantity);
                grandTotal += allPItens.Sum(a => a.Total);
                if (input.ExportOutput.Equals("B"))
                {
                    sheet.Cells[rowCount, colCount++].Value = allPItens.Sum(a => a.Quantity);
                    sheet.Cells[rowCount, colCount++].Value = allPItens.Sum(a => a.Total);
                }
                else if (input.ExportOutput.Equals("T"))
                {
                    sheet.Cells[rowCount, colCount++].Value = allPItens.Sum(a => a.Total);
                }
                else
                {
                    sheet.Cells[rowCount, colCount++].Value = allPItens.Sum(a => a.Quantity);
                }

                rowCount++;
            }

            sheet.Cells[rowCount, 1].Value = L("Total");
            colCount = 5;
            foreach (var locationT in outPut.AllLocations.Values)
                switch (input.ExportOutput)
                {
                    case "B":
                        if (quaDc.Any() && quaDc.ContainsKey(locationT))
                            sheet.Cells[rowCount, colCount++].Value = quaDc[locationT];
                        else
                            sheet.Cells[rowCount, colCount++].Value = 0;

                        if (sumDc.Any() && sumDc.ContainsKey(locationT))
                            sheet.Cells[rowCount, colCount++].Value = sumDc[locationT];
                        else
                            sheet.Cells[rowCount, colCount++].Value = 0;
                        break;

                    case "T":
                        if (sumDc.Any() && sumDc.ContainsKey(locationT))
                            sheet.Cells[rowCount, colCount++].Value = sumDc[locationT];
                        else
                            sheet.Cells[rowCount, colCount++].Value = 0;
                        break;

                    default:
                        if (quaDc.Any() && quaDc.ContainsKey(locationT))
                            sheet.Cells[rowCount, colCount++].Value = quaDc[locationT];
                        else
                            sheet.Cells[rowCount, colCount++].Value = 0;
                        break;
                }

            switch (input.ExportOutput)
            {
                case "B":
                    sheet.Cells[rowCount, colCount++].Value = grandQuantity;
                    sheet.Cells[rowCount, colCount++].Value = grandTotal;
                    break;

                case "T":
                    sheet.Cells[rowCount, colCount++].Value = grandTotal;
                    break;

                default:
                    sheet.Cells[rowCount, colCount++].Value = grandQuantity;
                    break;
            }

            sheet.Cells[rowCount, 1, rowCount, colCount].Style.Font.Bold = true;

            return rowCount++;
        }

        private async Task GetItemsSummary(ExcelPackage package, GetItemInput input,
            IConnectReportAppService appService)
        {
            var sheet = package.Workbook.Worksheets.Add(L("Summary"));
            sheet.OutLineApplyStyle = true;
            var outPut = await appService.GetPortionSales(input);
            var headers = new List<string>
            {
                L("Category"),
                L("AliasCode"),
                L("AliasName"),
                L("MenuItem")
            };
            if (outPut != null && outPut.PortionNames.Any() && outPut.Portions.Any())
            {
                foreach (var pname in outPut.PortionNames)
                {
                    headers.Add(pname);
                    headers.Add("");
                    headers.Add("");
                }

                var rowCount = AddReportHeader(sheet, "Item Summary Report", input);

                AddHeader(
                    sheet, rowCount, headers.ToArray()
                );

                rowCount++;

                foreach (var pos in outPut.Portions)
                {
                    var colCount = 1;
                    sheet.Cells[rowCount, colCount++].Value = pos.Category;
                    sheet.Cells[rowCount, colCount++].Value = pos.AliasCode;
                    sheet.Cells[rowCount, colCount++].Value = pos.AliasName;
                    sheet.Cells[rowCount, colCount++].Value = pos.MenuItemName;
                    foreach (var portionName in outPut.PortionNames)
                    {
                        var dePos = pos.Details.SingleOrDefault(a => a.PortionName.Equals(portionName));
                        if (dePos != null)
                        {
                            sheet.Cells[rowCount, colCount++].Value = Math.Round(dePos.TotalQuantity, _roundDecimals,
                                MidpointRounding.AwayFromZero);
                            sheet.Cells[rowCount, colCount++].Value = Math.Round(dePos.Price, _roundDecimals,
                                MidpointRounding.AwayFromZero);
                            sheet.Cells[rowCount, colCount++].Value = Math.Round(dePos.TotalQuantity * dePos.Price,
                                _roundDecimals, MidpointRounding.AwayFromZero);
                        }
                        else
                        {
                            sheet.Cells[rowCount, colCount++].Value = "";
                            sheet.Cells[rowCount, colCount++].Value = "";
                            sheet.Cells[rowCount, colCount++].Value = "";
                        }
                    }

                    rowCount++;
                }

                for (var i = 1; i <= 1; i++) sheet.Column(i).AutoFit();
            }
        }

        private async Task GetItemsSummaryByPrice(ExcelPackage package, GetItemInput input,
            IConnectReportAppService appService)
        {
            var sheet = package.Workbook.Worksheets.Add(L("Summary"));
            sheet.OutLineApplyStyle = true;
            input.Sorting = "MenuItemId";
            var headers = new List<string>
            {
                L("Category"),
                L("AliasCode"),
                L("MenuItem"),
                L("Portion"),
                L("Price"),
                L("Quantity"),
                L("Total")
            };

            var rowCount = AddReportHeader(sheet, "Item Price Summary Report", input);
            rowCount++;

            AddHeader(
                sheet, rowCount, headers.ToArray()
            );
            rowCount++;
            var outPut = await appService.GetItemSalesByPrice(input);
            if (DynamicQueryable.Any(outPut.MenuList.Items))
            {
                MenuListDto myListDto = null;

                foreach (var pos in outPut.MenuList.Items)
                {
                    var colCount = 1;
                    sheet.Cells[rowCount, colCount++].Value = pos.CategoryName;
                    sheet.Cells[rowCount, colCount++].Value = pos.AliasCode;

                    sheet.Cells[rowCount, colCount++].Value = pos.MenuItemName;
                    sheet.Cells[rowCount, colCount++].Value = pos.MenuItemPortionName;
                    sheet.Cells[rowCount, colCount++].Value = pos.Price;
                    sheet.Cells[rowCount, colCount++].Value = pos.Quantity;
                    sheet.Cells[rowCount, colCount++].Value = pos.Total;

                    if (myListDto != null && myListDto.MenuItemName.Equals(pos.MenuItemName))
                    {
                        sheet.Row(rowCount).Style.Fill.PatternType = ExcelFillStyle.Solid;
                        sheet.Row(rowCount).Style.Fill.BackgroundColor.SetColor(Color.Yellow);
                    }

                    myListDto = pos;

                    rowCount++;
                }
            }

            for (var i = 1; i <= 1; i++) sheet.Column(i).AutoFit();
        }

        private async Task GetItemComboSales(ExcelPackage package, GetItemInput input,
            IConnectReportAppService appService)
        {
            var sheet = package.Workbook.Worksheets.Add(L("ItemCombo"));
            sheet.OutLineApplyStyle = true;
            var rowCount = AddReportHeader(sheet, "Item Combo Sales Report", input);

            var headers = new List<string>
            {
                L("ItemCombo"),
                "",
                L("Group"),
                L("MenuItem"),
                L("Quantity"),
                L("Amount")
            };

            var itemSales = await appService.GetItemComboSales(input);

            var output = itemSales.GroupBy(i => new { i.Location, i.LocationCode })
                .Select(g => new ItemComboSalesDto
                {
                    Location = g.Key.Location,
                    LocationCode = g.Key.LocationCode,
                    ItemSales = g.ToList()
                }).ToList();

            var row = 5;

            foreach (var item in output)
            {
                var colCount = 1;
                row += 2;
                sheet.Cells[row, 1, row, 12].Merge = true;
                sheet.Cells[row, 1, row, 12].Style.Font.Bold = true;
                sheet.Cells[row, 1, row, 12].Style.Font.Size = 14;
                sheet.Cells[row, colCount].Value = $"Branch: {item.LocationCode} {item.Location} ";

                row++;
                AddHeader(sheet, row, headers.ToArray());
                foreach (var itemSale in item.ItemSales)
                {
                    row++;
                    colCount = 1;
                    sheet.Cells[row, colCount++].Value = "";
                    sheet.Cells[row, colCount++].Value = itemSale.Name;
                    sheet.Cells[row, colCount++].Value = "";
                    sheet.Cells[row, colCount++].Value = "";
                    sheet.Cells[row, colCount++].Value = itemSale.Quantity;
                    sheet.Cells[row, colCount++].Value =
                        Math.Round(itemSale.Amount, _roundDecimals, MidpointRounding.AwayFromZero);

                    var group = itemSale.OrderItems.GroupBy(o => o.GroupName)
                        .Select(g => new
                        {
                            GroupName = g.Key,
                            OrderItems = g.ToList()
                        }).ToList();
                    foreach (var g in group)
                    {
                        row++;

                        colCount = 1;
                        sheet.Cells[row, colCount++].Value = "";
                        sheet.Cells[row, colCount++].Value = "";
                        sheet.Cells[row, colCount++].Value = g.GroupName;

                        foreach (var orderItem in g.OrderItems)
                        {
                            row++;

                            colCount = 1;
                            sheet.Cells[row, colCount++].Value = "";
                            sheet.Cells[row, colCount++].Value = "";
                            sheet.Cells[row, colCount++].Value = "";
                            sheet.Cells[row, colCount++].Value = orderItem.Name;
                            sheet.Cells[row, colCount++].Value = orderItem.Quantity;
                            sheet.Cells[row, colCount++].Value = Math.Round(orderItem.Amount, _roundDecimals,
                                MidpointRounding.AwayFromZero);
                        }
                    }
                }

                row++;
                sheet.Cells[row, 1].Value = L("Total");
                sheet.Cells[row, 5].Value = "";
                sheet.Cells[row, 6].Value =
                    Math.Round(item.ItemSales.Sum(t => t.Amount + t.OrderItems.Sum(o => o.Amount)), _roundDecimals,
                        MidpointRounding.AwayFromZero);

                sheet.Cells[row, 1, row, 12].Style.Font.Bold = true;
            }

            for (var i = 1; i <= 11; i++) sheet.Column(i).AutoFit();
        }

        private async Task CreateFileForItemTagSales(ExcelPackage package, GetItemInput input,
            IConnectReportAppService appService)
        {
            var sheet = package.Workbook.Worksheets.Add(L("Item Tag Sales"));
            sheet.OutLineApplyStyle = true;
            var outPut = await appService.GetItemTagSales(input);

            var rowCount = AddReportHeader(sheet, "Item Tag Sales Report", input);

            AddHeader(
                sheet, rowCount,
                L("Tag"),
                L("Quantity"),
                L("Amount")
            );
            rowCount++;
            if (DynamicQueryable.Any(outPut.ItemTagList.Items))
            {
                AddObjects(
                    sheet, rowCount, outPut.ItemTagList.Items.ToList(),
                    _ => _.TagName,
                    _ => _.Quantity,
                    _ => Math.Round(_.Total, _roundDecimals, MidpointRounding.AwayFromZero)
                );

                rowCount = rowCount + outPut.ItemTagList.TotalCount + 1;
            }

            sheet.Cells[rowCount, 1].Value = "Total";
            sheet.Cells[rowCount, 2].Value = outPut.ItemTagList.Items.Sum(i => i.Quantity);
            sheet.Cells[rowCount, 3].Value = Math.Round(outPut.ItemTagList.Items.Sum(i => i.Total), _roundDecimals,
                MidpointRounding.AwayFromZero);
            sheet.Cells[rowCount, 1, rowCount, 5].Style.Font.Bold = true;

            for (var i = 1; i <= 5; i++) sheet.Column(i).AutoFit();
        }

        private async Task CreateFileForItemTagSalesDetail(ExcelPackage package, GetItemInput input,
            IConnectReportAppService appService)
        {
            var sheet = package.Workbook.Worksheets.Add(L("Item Tag Sales"));
            sheet.OutLineApplyStyle = true;
            var outPut = await appService.GetItemTagSales(input);

            var rowCount = AddReportHeader(sheet, "Item Tag Sales Detail Report", input);

            AddHeader(
                sheet, rowCount,
                L("Tag"),
                L("MenuItemName"),
                L("Portion"),
                L("Price"),
                L("Quantity"),
                L("Total")
            );
            var rowtotal = 1;
            rowCount++;
            foreach (var tag in outPut.ItemTagList.Items.OrderBy(s => s.TagName))
            {
                if (DynamicQueryable.Any(tag.TotalItems))
                {
                    AddObjects(
                        sheet, rowCount, tag.TotalItems,
                        _ => _.Tag,
                        _ => _.MenuItemName,
                        _ => _.MenuItemPortionName,
                        _ => Math.Round(_.Price, _roundDecimals, MidpointRounding.AwayFromZero),
                        _ => _.Quantity,
                        _ => Math.Round(_.Price * _.Quantity, _roundDecimals, MidpointRounding.AwayFromZero)
                    );
                    rowCount = rowCount + tag.TotalItems.Count + 1;
                }

                rowtotal = rowCount - 1;
                sheet.Cells[rowtotal, 1].Value = "Total";
                sheet.Cells[rowtotal, 5].Value = tag.TotalItems.Sum(i => i.Quantity);
                sheet.Cells[rowtotal, 6].Value = Math.Round(tag.TotalItems.Sum(i => i.Quantity * i.Price),
                    _roundDecimals, MidpointRounding.AwayFromZero);
                sheet.Cells[rowtotal, 1, rowCount, 6].Style.Font.Bold = true;
                rowCount++;
            }

            rowtotal = rowCount - 1;
            sheet.Cells[rowtotal, 1].Value = "All Total";
            sheet.Cells[rowtotal, 5].Value = outPut.ItemTagList.Items.Sum(s => s.Quantity);
            sheet.Cells[rowtotal, 6].Value = Math.Round(outPut.ItemTagList.Items.Sum(i => i.Total), _roundDecimals,
                MidpointRounding.AwayFromZero);
            sheet.Cells[rowtotal, 1, rowCount, 6].Style.Font.Bold = true;
            rowCount++;
            for (var i = 1; i <= 6; i++) sheet.Column(i).AutoFit();
        }
    }
}