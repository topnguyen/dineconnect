﻿using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Ticket.Exporter
{
    public class ConnectReportExcelExporter : FileExporterBase, IConnectReportExporter
    {
        public FileDto OrderExport(List<OrderListDto> dtos)
        {
            return CreateExcelPackage(
                "OrderList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Tickets"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("OrderId"),
                        L("OrderNumber"),
                        L("DepartmentName"),
                        L("MenuItemName"),
                        L("PortionName"),
                        L("Price"),
                        L("Quantity"),
                        L("Note"),
                        L("IncreaseInventory"),
                        L("DecreaseInventory"),
                        L("CreatingUserName"),
                        L("OrderCreatedDate"),
                        L("OrderCreatedTime"),
                        L("IsPromotionOrder"),
                        L("PromotionAmount")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.OrderId,
                        _ => _.OrderNumber,
                        _ => _.DepartmentName,
                        _ => _.MenuItemName,
                        _ => _.PortionName,
                        _ => _.Price,
                        _ => _.Quantity,
                        _ => _.Note,
                        _ => _.IncreaseInventory,
                        _ => _.DecreaseInventory,
                        _ => _.CreatingUserName,
                        _ => _.OrderCreatedTime.ToShortDateString(),
                        _ => _.OrderCreatedTime.ToShortTimeString(),
                        _ => _.IsPromotionOrder,
                        _ => _.PromotionAmount
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}