﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using Abp.Collections.Extensions;
using Abp.Configuration;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace DinePlan.DineConnect.Connect.Ticket.Exporter
{
    public class CustomerReportExporter : FileExporterBase, ICustomerReportExporter
    {
        private readonly IRepository<Company> _comRepo;

        private readonly ILocationAppService _locationAppService;
        private readonly IRepository<Master.Location> _locRepo;
        private readonly SettingManager _settingManager;
        private readonly ITenantSettingsAppService _tenantSettingsService;
        private readonly IConnectReportAppService _crs;
        private string _timeSpanFormat = "hh:mm:ss";
        private int roundDecimals = 2;
        private decimal taxValue = 1.07M;
        public CustomerReportExporter(SettingManager settingManager,
            IRepository<Master.Location> loRepository
            , IRepository<Company> comRepository
            , ILocationAppService locationAppService, IConnectReportAppService crs,
            ITenantSettingsAppService tenantSettingsService
        )
        {
            _locRepo = loRepository;
            _comRepo = comRepository;
            _locationAppService = locationAppService;
            _settingManager = settingManager;
            _roundDecimals = _settingManager.GetSettingValue<int>(AppSettings.ConnectSettings.Decimals);
            _tenantSettingsService = tenantSettingsService;
            _crs = crs;
        }


        public async Task<FileDto> ExportDetails(GetTicketInput input)
        {
            var builder = new StringBuilder();
            builder.Append(L("Details"));
            builder.Append("-");
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString("dd/MM/yyyy")
                : DateTime.Now.ToString("dd/MM/yyyy"));
            builder.Append(" - ");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString("dd/MM/yyyy")
                : DateTime.Now.ToString("dd/MM/yyyy"));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var package = new ExcelPackage())
            {
                var sheet = package.Workbook.Worksheets.Add(L("Details"));
                sheet.OutLineApplyStyle = true;

                var headers = new List<string>
                {
                    "LOCATIONCODE",
                    "NAME",
                    "BILLNO",
                    "DATE",
                    "TIME",
                    "ITEM",
                    "QTY",
                    "RATE",
                    "BASEAMOUNT",
                    "SGST",
                    "CGST",
                    "ROUND",
                    "DISCOUNT",
                    "TOTAL",
                    "SALESCHANNEL",
                    "PAYMENT"
                };


                AddHeader(
                    sheet,
                    headers.ToArray()
                );

                var rowCount = 1;
                decimal total = 0;
                decimal roundtotal = 0;
                decimal discountTotal = 0;
                decimal sgstTotal = 0;
                decimal cgstTotal = 0;
                decimal orderTotal = 0;

                var secondTime = false;

                while (true)
                {
                    input.OutputType = "EXPORT";
                    input.MaxResultCount = 100;
                    input.Sorting = "TicketNumber";
                    if (secondTime) input.NotCorrectDate = true;
                    TicketStatsDto outPut = null;
                    try
                    {
                        outPut = await _crs.GetTickets(input);
                    }
                    catch (Exception)
                    {
                        break;
                    }

                    if (!secondTime) secondTime = true;

                    if (outPut.TicketList.Items.Any())
                    {
                        foreach (var myticket in outPut.TicketList.Items)
                        {
                            PaymentListDto firstPay = null;
                            if (myticket.Payments != null && myticket.Payments.Any()) firstPay = myticket.Payments[0];

                            if (firstPay == null)
                                continue;

                            foreach (var order in myticket.Orders)
                            {
                                rowCount++;
                                var colCount = 1;
                                sheet.Cells[rowCount, colCount++].Value = myticket.LocationCode;
                                sheet.Cells[rowCount, colCount++].Value = myticket.LocationName;
                                sheet.Cells[rowCount, colCount++].Value = " " + myticket.TicketNumber;
                                sheet.Cells[rowCount, colCount++].Value =
                                    myticket.TicketCreatedTime.ToString("dd/MM/yyyy");
                                sheet.Cells[rowCount, colCount++].Value =
                                    myticket.TicketCreatedTime.ToShortTimeString();
                                sheet.Cells[rowCount, colCount++].Value = order.MenuItemName;
                                sheet.Cells[rowCount, colCount++].Value = order.Quantity;
                                sheet.Cells[rowCount, colCount++].Value = order.Price;
                                sheet.Cells[rowCount, colCount++].Value = order.LineTotal;
                                var taxPrice = Math.Round(order.TaxPrice / 2, 2, MidpointRounding.AwayFromZero);
                                sheet.Cells[rowCount, colCount++].Value = taxPrice;
                                sheet.Cells[rowCount, colCount++].Value = taxPrice;

                                sgstTotal += order.TaxPrice / 2;
                                cgstTotal += order.TaxPrice / 2;
                                sheet.Cells[rowCount, colCount++].Value = 0;
                                sheet.Cells[rowCount, colCount++].Value = 0;
                                var allTotal = order.LineTotal + taxPrice + taxPrice;
                                sheet.Cells[rowCount, colCount++].Value = allTotal;
                                sheet.Cells[rowCount, colCount++].Value = order.DepartmentName;
                                if (myticket.Payments.Any())
                                    sheet.Cells[rowCount, colCount++].Value = firstPay.PaymentTypeName;
                                orderTotal += allTotal;
                            }

                            var transAmount = 0M;
                            foreach (var tra in myticket.Transactions)
                            {
                                if (tra.TransactionTypeName.ToUpper().Equals("ROUND+")) transAmount += tra.Amount;

                                if (tra.TransactionTypeName.ToUpper().Equals("ROUND-")) transAmount += tra.Amount;
                            }

                            var discountAmount = 0M;

                            foreach (var tra in myticket.Transactions)
                                if (tra.TransactionTypeName.ToUpper().Equals("DISCOUNT"))
                                    discountAmount += tra.Amount;
                            sheet.Cells[rowCount, 12].Value = transAmount;
                            sheet.Cells[rowCount, 13].Value = discountAmount;
                            sheet.Cells[rowCount, 14].Value = Convert.ToDecimal(sheet.Cells[rowCount, 14].Value) +
                                                              transAmount + discountAmount;

                            discountTotal += discountAmount;
                            roundtotal += transAmount;
                            total += myticket.TotalAmount;
                        }

                        input.SkipCount = input.SkipCount + input.MaxResultCount;
                    }
                    else
                    {
                        break;
                    }
                }

                rowCount++;
                sheet.Cells[rowCount, 2].Value = L("Total");
                sheet.Cells[rowCount, 10].Value = sgstTotal;
                sheet.Cells[rowCount, 11].Value = cgstTotal;
                sheet.Cells[rowCount, 12].Value = roundtotal;
                sheet.Cells[rowCount, 13].Value = discountTotal;
                sheet.Cells[rowCount, 14].Value = orderTotal + roundtotal + discountTotal;
                Save(package, file);
            }

            return file;
        }



        public async Task<FileDto> ExportWritersCafe(GetItemInput input, List<WritersCafeList> dtos)
        {
            var builder = new StringBuilder();
            builder.Append(L("WritersCafe"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append("_");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("WritersCafe"));
                sheet.OutLineApplyStyle = true;

                var row = 2;

                AddHeader(sheet,
                    L("Location"),
                    L("Date"),
                    L("BillNo"),
                    L("TransactionId"),
                    L("TableNo"),
                    L("OrderTakenTime"),
                    L("BillGeneratedTime"),
                    L("TransactionClosedTime"),
                    L("OrderType"),
                    L("PaymentType"),
                    L("MenuItemName"),
                    L("Category"),
                    L("Price"),
                    L("Quantity"),
                    L("Discount"),
                    L("PackingCharge"),
                    L("TotalAmount"),
                    L("Pax")
                );

                AddObjects(sheet, row, dtos,
                    _ => _.Location,
                    _ => _.Date.ToString(_simpleDateFormat),
                    _ => _.BillNo,
                    _ => _.TransactionId,
                    _ => _.TableNo,
                    _ => _.OrderTakenTime.ToString(_timeFormat),
                    _ => _.BillGeneratedTime.ToString(_timeFormat),
                    _ => _.TransactionClosedTime.ToString(_timeFormat),
                    _ => _.OrderType,
                    _ => _.PaymentType,
                    _ => _.MenuItemName,
                    _ => _.CategoryName,
                    _ => Math.Round(_.Price, _roundDecimals, MidpointRounding.AwayFromZero),
                    _ => _.Quantity,
                    _ => Math.Round(_.Discount, _roundDecimals, MidpointRounding.AwayFromZero),
                    _ => _.PackingCharge,
                    _ => Math.Round(_.TotalAmount, _roundDecimals, MidpointRounding.AwayFromZero),
                    _ => _.Pax
                );

                row = dtos.Count + row++;
                sheet.Cells[row, 1].Value = L("Total");
                sheet.Cells[row, 14].Value = dtos.Sum(t => t.Quantity);
                sheet.Cells[row, 15].Value = dtos.Sum(t => t.Discount);
                //sheet.Cells[row, 16].Value = dtos.Sum(t => t.Quantity);
                sheet.Cells[row, 17].Value = Math.Round(dtos.Sum(t => t.TotalAmount), _roundDecimals,
                    MidpointRounding.AwayFromZero);

                sheet.Cells[row, 1, row, 20].Style.Font.Bold = true;
                for (var i = 1; i <= 20; i++) sheet.Column(i).AutoFit();

                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

        public async Task<FileDto> ExportWritersCafeForTicket(GetTicketInput input, List<WritersCafeList> dtos)
        {
            var builder = new StringBuilder();
            builder.Append(L("WritersCafe"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append("_");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("WritersCafe"));
                sheet.OutLineApplyStyle = true;

                var row = 2;

                AddHeader(sheet,
                    "Bill No",
                    "Cover",
                    "Printed Time",
                    "Amout",
                    "Modification Done",
                    "Re Print Time",
                    "Amount After Modification",
                    "Settlement Time",
                    "Settlement Mode");

                AddObjects(sheet, row, dtos,
                    _ => _.BillNo,
                    _ => _.Pax,
                    _ => _.PrintTime.ToString(_timeFormat),
                    _ => Math.Round(_.Amount, _roundDecimals, MidpointRounding.AwayFromZero),
                    _ => _.ModificationDone,
                    _ => _.RePrintTime.ToString(_timeFormat),
                    _ => Math.Round(_.AmountAfterModification, _roundDecimals, MidpointRounding.AwayFromZero),
                    _ => _.BillGeneratedTime.ToString(_timeFormat),
                    _ => _.PaymentType
                );

                row = dtos.Count + row++;
                sheet.Cells[row, 1].Value = L("Total");
                sheet.Cells[row, 4].Value = dtos.Sum(t => t.Amount);
                sheet.Cells[row, 7].Value = dtos.Sum(t => t.AmountAfterModification);

                sheet.Cells[row, 1, row, 20].Style.Font.Bold = true;
                for (var i = 1; i <= 20; i++) sheet.Column(i).AutoFit();

                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }


        public async Task<FileDto> ExportToSalesSummary(GetTicketInput input,
            List<string> tt, List<string> pp, List<string> dts, List<string> proList, int tenantId)
        {
            var file = new FileDto(
                "Summary_" + input.StartDate.ToString("yyyy-MM-dd") + "_" + input.EndDate.ToString("yyyy-MM-dd") +
                ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Tickets"));
                sheet.OutLineApplyStyle = true;
                var rowCount = AddReportHeader(sheet, L("SaleSummary_r"), input);

                input.PaymentName = true;

                var outPut = await _crs.GetSaleSummary(input);
                input.Sorting = "LocationId";
                var headers = new List<string>
                {
                    L("Date"), L("Location"), L("TotalTickets"), L("TotalCovers"), L("TotalOrderCovers"),
                    L("TotalAmount")
                };
                headers.AddRange(dts);
                headers.AddRange(pp);
                headers.AddRange(proList);
                headers.Add("TotalPromotion");
                headers.AddRange(tt);
                headers.AddRange(new List<string>{"Nett Sales", "Tax","Total Sales"});

                AddHeader(
                    sheet, rowCount,
                    headers.ToArray()
                );

                rowCount++;

                decimal total = 0;
                var ticketCount = 0;
                var ticketCoverCount = 0M;
                var totalOrdersCount = 0M;

                var i = 0;
                var paymentTotal = new Dictionary<string, decimal>();
                var deptTotal = new Dictionary<string, decimal>();
                var transTotal = new Dictionary<string, decimal>();
                var proTotal = new Dictionary<string, decimal>();
                var proAllTotal = 0M;
                var totalTaxAmount = 0M;

                if (DynamicQueryable.Any(outPut.Sale.Items))
                    for (i = 0; i < outPut.Sale.TotalCount; i++)
                    {
                        sheet.Cells[i + rowCount, 1].Value = outPut.Sale.Items[i].Date.ToString(input.DateFormat);
                        sheet.Cells[i + rowCount, 2].Value = outPut.Sale.Items[i].Location;
                        sheet.Cells[i + rowCount, 3].Value = outPut.Sale.Items[i].TotalTicketCount;
                        sheet.Cells[i + rowCount, 4].Value = outPut.Sale.Items[i].TotalCovers;
                        sheet.Cells[i + rowCount, 5].Value = outPut.Sale.Items[i].TotalOrderCount;
                        sheet.Cells[i + rowCount, 6].Value = outPut.Sale.Items[i].Total;
                        total += outPut.Sale.Items[i].Total;
                        ticketCount += outPut.Sale.Items[i].TotalTicketCount;
                        ticketCoverCount += outPut.Sale.Items[i].TotalCovers;
                        totalOrdersCount += outPut.Sale.Items[i].TotalOrderCount;

                        var index = 7;
                        var colFromHex = ColorTranslator.FromHtml("#E9E2E0");
                        foreach (var ppt in dts)
                        {
                            var ppAmount = 0M;
                            if (outPut.Sale.Items[i].Departments.Any(a => a.DepartmentName.Equals(ppt)))
                                ppAmount =
                                    outPut.Sale.Items[i].Departments.First(a => a.DepartmentName.Equals(ppt)).Total;

                            sheet.Cells[i + rowCount, index].Value = ppAmount;
                            sheet.Cells[i + rowCount, index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            sheet.Cells[i + rowCount, index].Style.Fill.BackgroundColor.SetColor(colFromHex);

                            if (deptTotal.ContainsKey(ppt))
                                deptTotal[ppt] += ppAmount;
                            else
                                deptTotal[ppt] = ppAmount;
                            index++;
                        }

                        colFromHex = ColorTranslator.FromHtml("#FCC9BE");

                        foreach (var ppt in pp)
                        {
                            var ppAmount = 0M;
                            if (outPut.Sale.Items[i].Payments.ContainsKey(ppt))
                                ppAmount = outPut.Sale.Items[i].Payments[ppt];

                            sheet.Cells[i + rowCount, index].Value = ppAmount;
                            sheet.Cells[i + rowCount, index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            sheet.Cells[i + rowCount, index].Style.Fill.BackgroundColor.SetColor(colFromHex);
                            if (paymentTotal.ContainsKey(ppt))
                                paymentTotal[ppt] += ppAmount;
                            else
                                paymentTotal[ppt] = ppAmount;

                            index++;
                        }

                        colFromHex = ColorTranslator.FromHtml("#C5C5C5");

                        foreach (var pro in proList)
                        {
                            var proAmount = 0M;
                            if (outPut.Sale.Items[i].Promotions.ContainsKey(pro))
                                proAmount = outPut.Sale.Items[i].Promotions[pro];

                            sheet.Cells[i + rowCount, index].Value = proAmount;
                            sheet.Cells[i + rowCount, index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            sheet.Cells[i + rowCount, index].Style.Fill.BackgroundColor.SetColor(colFromHex);

                            if (proTotal.ContainsKey(pro))
                                proTotal[pro] += proAmount;
                            else
                                proTotal[pro] = proAmount;

                            index++;
                        }


                        var proAllAmount = 0M;
                        proAllAmount = outPut.Sale.Items[i].Promotions.Where(p => proList.Contains(p.Key))
                            .Sum(p => Math.Abs(p.Value));
                        sheet.Cells[i + rowCount, index].Value = proAllAmount;
                        proAllTotal += Math.Abs(proAllAmount);
                        index++;

                        var mainTotal = 0M ;

                      
                        colFromHex = ColorTranslator.FromHtml("#FCEA0E");

                        foreach (var tType in tt)
                        {
                            var ttAmount = 0M;

                            if (outPut.Sale.Items[i].Transactions.ContainsKey(tType))
                                ttAmount = outPut.Sale.Items[i].Transactions[tType];

                            sheet.Cells[i + rowCount, index].Value = ttAmount;
                            sheet.Cells[i + rowCount, index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            sheet.Cells[i + rowCount, index].Style.Fill.BackgroundColor.SetColor(colFromHex);

                            if (transTotal.ContainsKey(tType))
                                transTotal[tType] += ttAmount;
                            else
                                transTotal[tType] = ttAmount;

                            index++;
                        }
                        if (outPut.Sale.Items[i].Total > 0)
                        {
                            mainTotal = outPut.Sale.Items[i].Total/taxValue;
                            mainTotal = Math.Round(mainTotal, _roundDecimals, MidpointRounding.AwayFromZero);
                        }
                        var taxAmount = outPut.Sale.Items[i].Total - mainTotal;
                        taxAmount = Math.Round(taxAmount, _roundDecimals, MidpointRounding.AwayFromZero);
                        totalTaxAmount += taxAmount;

                        if (mainTotal != 0M)
                        {
                            if (transTotal.ContainsKey("NETSALE"))
                            {
                                transTotal["NETSALE"] += mainTotal;
                            }
                            else
                            {
                                transTotal["NETSALE"] = mainTotal;
                            }
                            sheet.Cells[i + rowCount, index++].Value = mainTotal;
                            sheet.Cells[i + rowCount, index++].Value = taxAmount;
                            sheet.Cells[i + rowCount, index].Value = outPut.Sale.Items[i].Total;
                        }
                    }

                var tindex = 2;

                sheet.Cells[i + rowCount, tindex++].Value = L("Total");
                sheet.Cells[i + rowCount, tindex++].Value = ticketCount;
                sheet.Cells[i + rowCount, tindex++].Value = ticketCoverCount;
                sheet.Cells[i + rowCount, tindex++].Value = totalOrdersCount;

                sheet.Cells[i + rowCount, tindex++].Value = total;
                foreach (var ppt in dts)
                {
                    if (deptTotal.ContainsKey(ppt))
                        sheet.Cells[i + rowCount, tindex].Value = deptTotal[ppt];

                    tindex++;
                }

                foreach (var ppt in pp)
                {
                    if (paymentTotal.ContainsKey(ppt))
                        sheet.Cells[i + rowCount, tindex].Value = paymentTotal[ppt];
                    tindex++;
                }

                foreach (var pro in proList)
                {
                    if (proTotal.ContainsKey(pro))
                        sheet.Cells[i + rowCount, tindex].Value = proTotal[pro];
                    tindex++;
                }


                sheet.Cells[i + rowCount, tindex].Value = proAllTotal;
                tindex++;

                foreach (var ppt in tt)
                {
                    if (transTotal.ContainsKey(ppt))
                        sheet.Cells[i + rowCount, tindex].Value = transTotal[ppt];
                    tindex++;
                }

                sheet.Cells[i + rowCount, tindex++].Value = transTotal["NETSALE"];
                sheet.Cells[i + rowCount, tindex++].Value = totalTaxAmount;
                sheet.Cells[i + rowCount, tindex++].Value = total;


                for (i = 1; i <= 21; i++) sheet.Column(i).AutoFit();
                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }


        #region PrivateMethods

        private int AddReportHeader(ExcelWorksheet sheet, string nameOfReport, IDateInput input)
        {
            var brandName = "";
            var locationCode = "";
            var locationName = "";
            var branch = "All";
            var isAllLocation =
                input.LocationGroup?.Locations?.Count() + input.LocationGroup?.Groups?.Count() +
                input.LocationGroup?.LocationTags?.Count() == _locRepo.GetAll().Count();

            if (isAllLocation)
            {
                branch = "All";
            }
            else
            {
                if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
                    branch = input.LocationGroup.Locations.Select(l => l.Name).JoinAsString(", ");
                if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
                    branch = input.LocationGroup.Groups.Select(l => l.Name).JoinAsString(", ");
                if (input.LocationGroup?.LocationTags != null && input.LocationGroup.LocationTags.Any())
                    branch = input.LocationGroup.LocationTags.Select(l => l.Name).JoinAsString(", ");
            }

            Master.Location myLocation = null;

            if (input.Location > 0)
                myLocation = _locRepo.Get(input.Location);

            if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var simpleL = input.LocationGroup.Locations.First();
                if (simpleL != null) myLocation = _locRepo.Get(simpleL.Id);
            }

            if (myLocation != null)
            {
                var myCompany = _comRepo.Get(myLocation.CompanyRefId);
                if (myCompany != null) brandName = myCompany.Name;
                locationCode = myLocation.Code;
                locationName = myLocation.Name;
            }

            sheet.Cells[1, 1].Value = nameOfReport;
            sheet.Cells[1, 1, 1, 12].Merge = true;
            sheet.Cells[1, 1, 1, 13].Style.Font.Bold = true;
            sheet.Cells[1, 1, 1, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            sheet.Cells[2, 1].Value = L("Brand");
            sheet.Cells[2, 2].Value = brandName;
            sheet.Cells[2, 6].Value = "Business Date:";
            sheet.Cells[2, 7].Value = input.StartDate.ToString(input.DateFormat) + " to " +
                                      input.EndDate.ToString(input.DateFormat);
            sheet.Cells[3, 1].Value = "Branch Name:";
            sheet.Cells[3, 2].Value = branch;
            sheet.Cells[3, 6].Value = "Printed On:";
            sheet.Cells[3, 7].Value = DateTime.Now.ToString(input.DatetimeFormat);

            sheet.Cells[1, 1, 4, 7].Style.Font.Size = 10;

            return 6;
        }

        #endregion
    }
}