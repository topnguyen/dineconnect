﻿using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.Exporter
{
	public class ConsolidatedExporter : FileExporterBase, IConsolidatedExporter
	{
		private readonly ITenantSettingsAppService _tenantSettingsService;      
        public ConsolidatedExporter(ITenantSettingsAppService tenantSettingsService)
        {
			_tenantSettingsService = tenantSettingsService;
		}
		public async Task<FileDto> ExportConsolidatedReportToFile(GetTicketInput input, IConsolidatedAppService appService)
		{
			var builder = new StringBuilder();
			builder.Append(L("Consolidated"));
			builder.Append(L("-"));
			builder.Append(!input.StartDate.Equals(DateTime.MinValue)
				? input.StartDate.ToString(_simpleDateFormat)
				: DateTime.Now.ToString(_simpleDateFormat));
			builder.Append("_");
			builder.Append(!input.EndDate.Equals(DateTime.MinValue)
				? input.EndDate.ToString(_simpleDateFormat)
				: DateTime.Now.ToString(_simpleDateFormat));

			var file = new FileDto(builder + ".xlsx",
				MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
			using (var excelPackage = new ExcelPackage())
			{
				await GetConsolidatedExport(excelPackage, input, appService);
				Save(excelPackage, file);
			}

			return ProcessFile(input, file);
		}

		private async Task GetConsolidatedExport(ExcelPackage package, GetTicketInput ticketInput, IConsolidatedAppService appService)
		{
			var worksheet = package.Workbook.Worksheets.Add(L("Consolidated"));
			worksheet.OutLineApplyStyle = true;
			var setting = await _tenantSettingsService.GetAllSettings();
			var dateTimeFormat = setting.Connect.DateTimeFormat;
			var dateFormat = setting.Connect.SimpleDateFormat;
			var row = 1;
			worksheet.Cells[row, 2].Value = L("FromDate");
			worksheet.Cells[row, 3].Value = ticketInput.StartDate.ToString(dateFormat);
			worksheet.Cells[$"A{row}:E{row}"].Style.Font.Bold = true;

			row++;
			worksheet.Cells[row, 2].Value = L("EndDate");
			worksheet.Cells[row, 3].Value = ticketInput.EndDate.ToString(dateFormat);
			worksheet.Cells[$"A{row}:E{row}"].Style.Font.Bold = true;


			row++;
			worksheet.Cells[row, 1].Value = L("LocationID");
			worksheet.Cells[row, 3].Value = L("WithOutTax");
			worksheet.Cells[row, 4].Value = L("WithTax");
			worksheet.Cells[row, 5].Value = L("Total");
			worksheet.Cells[$"A{row}:E{row}"].Style.Font.Bold = true;


			var dtos = await appService.GetConsolidated(ticketInput);

			row++;
			foreach (var dataBy_Location in dtos.ListItem)
			{
				worksheet.Cells[row, 1].Value = dataBy_Location.LocationID;
				worksheet.Cells[row, 2].Value = dataBy_Location.LocationName;
				worksheet.Cells[$"A{row}:E{row}"].Style.Font.Bold = true;

				row++;
				foreach (var dataBy_Department in dataBy_Location.ListItem)
				{
					//set color date
					worksheet.Cells[row, 2].Value = dataBy_Department.DepartmentName;
					worksheet.Cells[row, 3].Value = dataBy_Department.TotalWithOutTax.ToString(ConnectConsts.ConnectConsts.NumberFormat);
					worksheet.Cells[row, 4].Value = dataBy_Department.TotalWithTax.ToString(ConnectConsts.ConnectConsts.NumberFormat);
					worksheet.Cells[row, 5].Value = dataBy_Department.TotalAmount.ToString(ConnectConsts.ConnectConsts.NumberFormat);
					row++;
				}
				row++;
			}
			worksheet.Cells[row, 2].Value = L("Total");
			worksheet.Cells[row, 3].Value = dtos.TotalWithOutTax.ToString(ConnectConsts.ConnectConsts.NumberFormat);
			worksheet.Cells[row, 4].Value = dtos.TotalWithTax.ToString(ConnectConsts.ConnectConsts.NumberFormat);
			worksheet.Cells[row, 5].Value = dtos.TotalAmount.ToString(ConnectConsts.ConnectConsts.NumberFormat);
			worksheet.Cells[$"A{row}:E{row}"].Style.Font.Bold = true;

			for (var i = 1; i <= 5; i++)
			{
				worksheet.Column(i).AutoFit();
			}
		}
	}
}
