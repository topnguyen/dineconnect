﻿using Abp.Application.Services;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket
{
	public interface IConsolidatedAppService: IApplicationService
	{
		Task<ConsolidatedDto> GetConsolidated(GetTicketInput input);
		Task<FileDto> GetConsolidatedExport(GetTicketInput input);
	}
}
