﻿using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Ticket.Implementation;
using DinePlan.DineConnect.Connect.WorkPeriod.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket
{
    public interface ITicketListExcelExporter
    {
        Task<FileDto> ExportTickets(GetTicketInput allListDtos, IConnectReportAppService ticketAppService,
            List<string> tt, List<string> pp, List<string> tagnames, int tenantId);

        Task<FileDto> ExportTicketSyncs(GetTicketSyncInput input, IConnectReportAppService appService);
        Task<FileDto> ExportTickets(GetTicketInput argsTicketInput, IConnectReportAppService appService, int tenantId);

        Task<FileDto> ExportOrders(GetItemInput input, IConnectReportAppService connectReportAppService);
        Task<FileDto> ExportExchangeReport(GetItemInput input, IConnectReportAppService appService);

        Task<FileDto> ExportToFileItems(GetItemInput input, IConnectReportAppService connectReportAppService);
        Task<FileDto> ExportToSalesSecondSummary(GetTicketInput input, IConnectReportAppService appService, List<string> pp, int tenantId);
        Task<FileDto> ExportToFileItemsByPrice(GetItemInput input, IConnectReportAppService connectReportAppService);

        Task<FileDto> ExportToFileItemsHours(GetItemInput input, IConnectReportAppService connectReportAppService);

        Task<FileDto> ExportToFileLocationSales(GetTicketInput input, IConnectReportAppService connectReportAppService);

        Task<FileDto> ExportToFileLocationSalesDetail(GetTicketInput input, IConnectReportAppService connectReportAppService);

        Task<FileDto> ExportToSalesSummary(GetTicketInput input, IConnectReportAppService connectReportAppService, List<string> ttList, List<string> ptList, List<string> dtList, List<string> proList, int tenantId);
        Task<FileDto> ExportToSalesSummaryNew(GetTicketInput input, IConnectReportAppService connectReportAppService, List<string> ttList, List<string> ptList, List<string> dtList, List<string> proList, int tenantId);
        Task<FileDto> ExportFranchise(GetTicketInput input, IConnectReportAppService _cService, List<string> ttList, List<string> ptList, List<SharingPercentageDto> lList, List<MenuItemListDto> menuItems);

        Task<FileDto> ExportToDepartmentSummary(GetTicketInput input, IConnectReportAppService connectReportAppService, List<string> tt, List<string> pp);

        Task<FileDto> ExportToTallyForDepartment(GetTicketInput input, ConnectReportAppService connectReportAppService);

        Task<FileDto> ExportToFileItemsSummary(GetItemInput input, IConnectReportAppService connectReportAppService);

        Task<FileDto> ExportItemSalesForWorkPeriodExcel(GetItemInput input, ConnectReportAppService connectReportAppService, string nameLocation);

        Task<FileDto> ExportToTallyOutput(GetTicketInput input, ConnectReportAppService connectReportAppService, List<string> ttList, List<string> ptList);

        Task<FileDto> ExportItemSalesForDepartmentExcel(GetItemInput input, IConnectReportAppService connectReportAppService);

        Task<FileDto> ExportToFileTenderDetail(GetTicketInput input, IConnectReportAppService connectReportAppService);

        Task<FileDto> ExportToFileGroup(GetItemInput input, IConnectReportAppService appService);

        Task<FileDto> ExportToFileItemCombos(GetItemInput input, IConnectReportAppService appService);

        FileDto ProcessFile(IFileExport input, FileDto file);

        Task<FileDto> ExportRefundTicketDetail(GetTicketInput input, IConnectReportAppService appService);


        Task<FileDto> ExportToFileItemAndOrderTagSales(GetItemInput input, IConnectReportAppService appService);

        FileDto ExportTopOrBottomItemSales(GetItemInput input, List<MenuListDto> items);


        Task<FileDto> ExportToFileItemTagSales(GetItemInput input, IConnectReportAppService appService);

        Task<FileDto> ExportToFileItemTagSalesDetail(GetItemInput input, IConnectReportAppService appService);

        Task<FileDto> ExportScheduleReportToFile(GetItemInput input, IConnectReportAppService appService);

        FileDto ExportLocationComparison(List<LocationComparisonReportOuptut> dtos);

        Task<FileDto> ExportABBReprintReportToFile(ABBReprintReportInputDto input, IConnectReportAppService appService);

        Task<FileDto> ExportToFilePlanWorkDay(WorkPeriodSummaryInputWithPaging input, IConnectReportAppService appService);


    }
}