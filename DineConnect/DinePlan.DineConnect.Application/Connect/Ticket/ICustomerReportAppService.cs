﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.WorkPeriod.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Transaction.Dtos;

namespace DinePlan.DineConnect.Connect.Ticket
{
    public  interface ICustomerReportAppService : IApplicationService
    {
        Task<FileDto> GetTicketDetailExport(GetTicketInput input);

        Task<FileDto> GetWritersCafeForTicketToExcel(GetTicketInput input);
        Task<FileDto> GetWritersCafeToExcel(GetItemInput input);


        Task<FileDto> GetSalesSummaryForPB(GetTicketInput input);
        Task<FileDto> GetItemSummaryForPB(GetItemInput input);


    }

}
