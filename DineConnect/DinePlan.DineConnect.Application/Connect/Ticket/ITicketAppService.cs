﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Credit.Dtos;
using DinePlan.DineConnect.Connect.Departments.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Ticket
{
    public  interface ITicketAppService : IApplicationService
    {
        Task<TicketStatsDto> GetAll(GetTicketInput inputDto);
        Task<TicketListDto> GetInputTicket(NullableIdInput input);
        Task<PaymentStatsDto> GetPayments(GetPaymentInput input);
        Task<TransactionStatsDto> GetTransactions(GetTransactionInput input);
        Task<TicketTransactionOutput> GetTicketTransactions(GetTicketInput input);
        Task<CreateOrUpdateTicketOutput> CreateOrUpdateTicket(CreateOrUpdateTicketInput input);
        void CreateTicket(Transaction.Ticket ticket);
        Task<DashboardTicketDto> GetDashboard();
        Task<TicketStatsDto> GetDashboardChart(GetChartInput input);
        List<ChartOutputDto> GetTransactionChart(GetChartInput input);
        List<ChartOutputDto> GetPaymentChart(GetChartInput input);
        List<ChartOutputDto> GetDepartmentChart(GetChartInput input);
        List<ChartOutputDto> GetItemChart(GetChartInput input);
        Task<PagedResultOutput<PaymentListDto>> GetPaymentForTicketId(int ticketId);
        Task<PagedResultOutput<TicketTransactionListDto>> GetTransactionForTicketId(int ticketId);
        Task<PagedResultOutput<OrderListDto>> GetOrderForTicketId(int ticketId);
      
        Task UpdateMembersAndPoints();
        Task<NullableIdInput> GetTicketIdOnLocation(GetTicketIdInput input);

        Task<ApiCreditOutput> ApiGetCreditTicketTotal(ApiCreditInput input);
        Task<ApiCreditOutput> ApiApplyCreditTicketTotal(ApiCreditInput input);

        Task<int> SyncTicket(PostInput postInput);
    }


}
