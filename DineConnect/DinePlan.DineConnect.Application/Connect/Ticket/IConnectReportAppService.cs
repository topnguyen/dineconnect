﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Auditing.Dto;
using DinePlan.DineConnect.Connect.Ticket.CategoryReport.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Connect.WorkPeriod.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Transaction.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket
{
    public interface IConnectReportAppService : IApplicationService
    {
        #region API

        Task<DepartmentStatsDto> ApiGetDepartmentSales(GetItemInput input);
        Task<GroupStatsDto> ApiGetGroupSales(GetItemInput input);
        Task<CategoryStatsDto> ApiGetCategorySales(GetItemInput input);
        Task<ItemStatsDto> ApiGetItemSales(GetItemInput input);

        #endregion
        #region Tickets
        IQueryable<Transaction.Ticket> GetAllTickets(IDateInput input);
        IQueryable<Transaction.Ticket> GetAllTicketsForTicketInput(GetTicketInput input);
        IQueryable<Transaction.Ticket> GetAllTicketsForTicketInputNoFilterByDynamicFilter(GetTicketInput input);
        Task<List<AllTicketListDto>> GetFullTickets(GetTicketInput input);
        Task<TicketHourListDto> GetHourlySalesByDate(GetTicketInput input);

        Task<TicketHourListDto> GetHourlySalesSummary(GetTicketInput input);

        Task<TicketStatsDto> GetTickets(GetTicketInput input, bool onlystatus = false);

        Task<PagedResultOutput<GetTicketSyncOutput>> GetTicketSyncs(GetTicketSyncInput input);
        Task<TicketTransactionOutput> GetTicketTransactions(GetTicketInput input);

        Task<PaymentStatsDto> GetPayments(GetPaymentInput input);

        Task<TransactionStatsDto> GetTransactions(GetTransactionInput input);

        Task<TicketStatsDto> GetDashboardChart(GetChartInput input);

        Task<IdHourListDto> GetHourlySalesByRange(GetTicketInput input);

        Task<List<IdLocationHourListDto>> GetLocationHourlySalesByRange(GetTicketInput input);

        Task<PagedResultOutput<TicketViewTax>> GetLocationSales(GetTicketInput input);

        Task<PagedResultOutput<TicketViewTaxDetail>> GetLocationSalesDetail(GetTicketInput input);
        Task<PagedResultOutput<ABBReprintReportOutputDto>> GetABBReprintReport(ABBReprintReportInputDto input);
        Task<PagedResultOutput<ExternalLogListDto>> GetAllExternalLog(GetItemInput input);


        #region Export

        Task<FileDto> GetAllToExcel(GetTicketInput input);
        Task<FileDto> GetTicketSyncToExcel(GetTicketSyncInput input);
        Task<FileDto> GetFranchiseExcel(GetTicketInput inputDto);
        Task<FileDto> GetSummaryExcel(GetTicketInput input);
        Task<FileDto> GetSummaryNewExcel(GetTicketInput input);
        Task<FileDto> GetSummarySecondExcel(GetTicketInput input);
        Task<FileDto> GetSummaryOutput(GetTicketInput input);
        Task<FileDto> GetTallyExport(GetTicketInput inputDto);
        Task<FileDto> GetLocationSalesExcel(GetTicketInput input);
        Task<FileDto> GetLocationSalesDetailExcel(GetTicketInput input);
        Task<FileDto> GetRefundDetaiTicketsToExcel(GetTicketInput input, bool onlystatus = false);
        Task<FileDto> GetABBReportExport(ABBReprintReportInputDto input);
        Task<FileDto> GetExchangeReportExcel(GetItemInput input);
        Task<FileDto> GetExternalLogExcel(GetItemInput input);

        #endregion

        #endregion
        #region Orders
        Task<FileDto> GetOrdersExcel(GetItemInput inputDto);
        IQueryable<Order> GetAllOrders(GetItemInput input);
        IQueryable<Order> GetAllOrdersNotFilterByDynamicFilter(GetItemInput input);
        Task<OrderStatsDto> GetOrders(GetItemInput input);
        Task<OrderExchangeDto> GetOrdersExchange(GetItemInput input);
        Task<ListResultOutput<ComboboxItemDto>> GetReason();
        #endregion

        #region Department

        Task<DepartmentStatsDto> GetDepartmentSales(GetItemInput input);
        Task<FileDto> GetDepartmentExcel(GetTicketInput input);
        Task<FileDto> GetDepartmentTallyExcel(GetTicketInput input);
        Task<List<DepartmentSummaryDto>> GetDepartmentSalesSummary(GetTicketInput input);
        Task<List<DepartmentTallySummaryDto>> GetDeparmentSalesSummaryForTally(GetTicketInput input);

        #endregion
        #region Group
        Task<GroupStatsDto> GetGroupSales(GetItemInput input);
        Task<FileDto> GetGroupExcel(GetItemInput input);

        #endregion
        #region Category
        Task<CategoryStatsDto> GetCategorySales(GetItemInput input);

        #endregion
        #region Item
        Task<AllItemSalesDto> GetAllItemSales(GetItemInput input);
        Task<IdDayItemOutput> GetDaysItemSales(GetDayItemInput input);
        Task<IdHourListDto> GetDayOfMonthHourlySales(GetTicketInput input);
        Task<IdHourListDto> GetDayOfWeekHourlySales(GetTicketInput input);
        Task<ItemStatsDto> GetItemSales(GetItemInput input);

        Task<ItemStatsDto> GetItemSalesForBackground(GetItemInput input);
        Task<ItemStatsDto> GetItemSalesByPrice(GetItemInput input);
        Task<ItemStatsDto> GetItemSalesByDepartment(GetItemInput input);
        Task<ItemStatsDto> GetItemHourlySales(GetItemInput input);
        Task<ItemStatsDto> GetItemHourlySalesForBackground(GetItemInput input);
        Task<ItemStatsDto> GetItemSalesSummary(GetItemInput input);
        Task<PortionSalesOutput> GetPortionSales(GetItemInput input);
        Task<ItemStatsDto> GetTopOrBottomItemSales(GetItemInput input);
        Task<ItemTagStatsDto> GetItemTagSales(GetItemInput input);
        Task<List<ItemSale>> GetItemComboSales(GetItemInput input);
        #region Export

        Task<FileDto> GetHourlySalesExport(GetTicketInput input);
        Task<FileDto> GetItemSalesForWorkPeriodExcel(GetItemInput input);
        Task<FileDto> GetItemComboSalesExcel(GetItemInput input);
        Task<FileDto> GetItemSalesByPriceExcel(GetItemInput input);
        Task<FileDto> GetItemsExcel(GetItemInput input);
        Task<FileDto> GetItemsSummaryExcel(GetItemInput input);
        Task<FileDto> GetItemSalesByDepartmentExcel(GetItemInput input);
        Task<FileDto> GetItemHourlySalesExcel(GetItemInput input);
        Task<HourDto> GetHourlySales(GetTicketInput input);
        Task<FileDto> GetItemsAndOrderTagExcel(GetItemInput input);
        Task<FileDto> GetTopOrBottomItemSalesToExcel(GetItemInput input);
        Task<FileDto> GetItemTagSalesToExel(GetItemInput input);
        Task<FileDto> GetItemTagSalesDetailToExel(GetItemInput input);

        Task<FileDto> GetTopOrBottomItemSalesToExcelInBackground(GetItemInput input);

        #endregion

        #endregion

        #region Summary
        Task<IdHourListDto> GetLocationHourySales(GetTicketInput input);
        Task<FileDto> GetLocationComparisonReportToExcel(GetTicketInput input);
        Task<PrintOutIn40Cols> GetSaleSummaryPrint(GetTicketInput input);
        Task<SaleSummaryStatsDto> GetSaleSummary(GetTicketInput input);
        Task<List<IdLocationHourListDto>> GetLocationDayOfMonthHourlySales(GetTicketInput input);
        Task<List<IdLocationHourListDto>> GetLocationDayOfWeekHourlySales(GetTicketInput input);


        #endregion
        #region WorkPeriod

        Task<IList<WorkPeriodListDto>> GetWorkPeriodByLocationForDates(WorkPeriodSummaryInput input);
        Task<List<WorkPeriodItemSales>> GetItemSalesForWorkPeriod(GetItemInput input);
        Task<PagedResultOutput<WorkPeriodSummaryOutput>> GetWorkPeriodByLocationForDatesWithPaging(WorkPeriodSummaryInputWithPaging input);
        Task<FileDto> GetWorkPeriodByLocationForDatesWithPagingToExcel(WorkPeriodSummaryInputWithPaging input);
        Task<PagedResultOutput<WorkPeriodSummaryOutput>> GetWorkPeriodByLocationDailyForDatesWithPaging(WorkPeriodSummaryInputWithPaging input);

        #endregion

        #region Tender
        Task<PagedResultOutput<TenderDetail>> GetTenderDetails(GetTicketInput input);
        Task<List<TenderViewDto>> GetTenderDetailsForBackground(GetTicketInput input);
        Task<FileDto> GetTenderDetailsExport(GetTicketInput input);

        #endregion
        #region Schedule

        Task<PagedResultDto<SchelduleReportOutput>> GetScheduleReport(GetItemInput input);

        Task<List<ScheduleReportData>> GetScheduleSetting(int? locationId = null);

        Task<FileDto> GetScheduleReportExcel(GetItemInput input);
        Transaction.Ticket GetTicketByNumber(string ticketNumber);

		#endregion

    }
}