﻿using Abp.Application.Services;
using DinePlan.DineConnect.Connect.Ticket.CategoryReport.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.CategoryReport
{
    public interface ICategoryReportAppService : IApplicationService
    {
        Task<FileDto> GetCategoryExcel(GetItemInput input);

        Task<CategoryStatsDto> GetCategorySales(GetItemInput input);
    }
}