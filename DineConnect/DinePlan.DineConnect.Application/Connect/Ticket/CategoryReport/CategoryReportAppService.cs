﻿using Abp.Application.Services.Dto;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Connect.Ticket.CategoryReport.Dto;
using DinePlan.DineConnect.Connect.Ticket.CategoryReport.Exporting;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Report;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.CategoryReport
{
    public class CategoryReportAppService : DineConnectAppServiceBase, ICategoryReportAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IConnectReportAppService _connectReportAppService;
        private readonly IRepository<Master.Location> _locationRepository;
        private readonly IBackgroundJobManager _backgroundJobManager;
        private readonly IReportBackgroundAppService _reportBackgroundAppService;
        private readonly ICategoryReportExporter _exporter;

        public CategoryReportAppService(
            IUnitOfWorkManager unitOfWorkManager
            , IConnectReportAppService connectReportAppService
            , IRepository<Master.Location> locationRepository
            , IBackgroundJobManager backgroundJobManager
            , IReportBackgroundAppService reportBackgroundAppService
            , ICategoryReportExporter exporter
            )
        {
            _unitOfWorkManager = unitOfWorkManager;
            _connectReportAppService = connectReportAppService;
            _locationRepository = locationRepository;
            _backgroundJobManager = backgroundJobManager;
            _reportBackgroundAppService = reportBackgroundAppService;
            _exporter = exporter;
        }

        public async Task<CategoryStatsDto> GetCategorySales(GetItemInput input)
        {
            var returnOutput = new CategoryStatsDto();

            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                if (input.Void) input.Refund = true;
                var returnOrders = await _connectReportAppService.GetItemSales(input);               
                var categoryGroupings = returnOrders.MenuList.Items.GroupBy(o => new
                {
                    CategoryId =
                    o.CategoryId,
                    CategoryName = o.CategoryName,
                    ProductGroupName = string.IsNullOrEmpty(o.GroupName) ? null : o.GroupName
                }).ToList();

                returnOutput.Categories = categoryGroupings.Select(c => c.Key.CategoryName).ToList();

                returnOutput.ChartOutput = categoryGroupings
                    .Select(c => new BarChartOutputDto
                    {
                        name = c.Key.CategoryName,
                        data = new List<decimal> { c.Sum(e => e.Total) }
                    }).ToList();


                var items = categoryGroupings.Select(g =>
                    new CategoryReportDto
                    {
                        Id = (int)g.Key.CategoryId,
                        CategoryId = (int)g.Key.CategoryId,
                        CategoryName = g.Key.CategoryName,
                        ProductGroupName = g.Key.ProductGroupName,
                        Quantity = g.Sum(c => c.Quantity),
                        Total = g.Sum(e => e.Quantity * e.Price + e.Quantity * e.ListOrderTagValues.Sum(s => s.Price)) ,
                        MenuItems = g.GroupBy(c => c.MenuItemId).Select(e => new MenuReportView
                        {
                            AliasCode = e.First().AliasCode,
                            MenuItemName = e.First().MenuItemName,
                            Quantity = e.Sum(a => a.Quantity),
                            Price = e.Sum(a => a.Quantity * a.Price + a.Quantity * e.SelectMany(s => s.ListOrderTagValues).Sum(s => s.Price)),
                        }).ToList(),
                        LocationReports = g.GroupBy(c => new { c.LocationId }).Select(e => new LocationReportDto
                        {
                            LocationId = e.Key.LocationId,
                            LocationName = e.First().LocationCode,
                            Quantity = e.Sum(a => a.Quantity),
                            TotalAmount = e.Sum(a => a.Total)
                        }).ToList()
                    });

                var dataAsQueryable = items.AsQueryable();
                if (!string.IsNullOrEmpty(input.DynamicFilter))
                {
                    var jsonSerializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver =
                            new CamelCasePropertyNamesContractResolver()
                    };
                    var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                    if (filRule?.Rules != null)
                    {
                        dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                    }
                }
                var menuItemCount = dataAsQueryable.Count();

                var result = new List<CategoryReportDto>();
                result = input.IsExport ? dataAsQueryable.OrderBy(input.Sorting).ToList() : dataAsQueryable.AsQueryable().OrderBy(input.Sorting).PageBy(input).ToList();
                await FillDataForLoactions(input, result);
                returnOutput.CategoryList = new PagedResultOutput<CategoryReportDto>(
                    menuItemCount,
                    result
                );
                return returnOutput;
            }
        }

        public async Task<FileDto> GetCategoryExcel(GetItemInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _reportBackgroundAppService.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.CATEGORYSALES,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });

                    if (backGroundId > 0)
                    {
                        input.LocationGroup.UserId = AbpSession.UserId.Value;

                        await _backgroundJobManager.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.CATEGORYSALES,
                            ItemInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                    }
                }
                else
                {
                    return await _exporter.ExportToFileCategory(input, this);
                }
            }

            return null;
        }

        private async Task FillDataForLoactions(GetItemInput input, List<CategoryReportDto> items)
        {
            if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var locationIds = input.LocationGroup.Locations.Select(e => e.Id).ToList();
                var locations = await _locationRepository.GetAll()
                    .Where(l => locationIds.Contains(l.Id))
                    .Select(loc => new SimpleLocationDto
                    {
                        Id = loc.Id,
                        Code = loc.Code,
                        Name = loc.Name,
                        CompanyRefId = loc.CompanyRefId
                    })
                    .ToListAsync();
                items.ForEach(item =>
                {
                    var locationNoData = locations.Where(c => !item.LocationReports.Select(l => l.LocationId).Contains(c.Id))
                       .Select(e => new LocationReportDto
                       {
                           LocationId = e.Id,
                           LocationName = e.Name,
                           Quantity = 0M,
                           TotalAmount = 0M
                       }).ToList();
                    item.LocationReports.AddRange(locationNoData);
                    item.LocationReports = item.LocationReports.OrderBy(l => l.LocationName).ToList();
                });
            }
        }
    }
}