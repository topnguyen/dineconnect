﻿using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.CategoryReport.Exporting
{
    public interface ICategoryReportExporter
    {
        Task<FileDto> ExportToFileCategory(GetItemInput input, ICategoryReportAppService appService);
    }
}