﻿using Abp.AutoMapper;
using DinePlan.DineConnect.Connect.Ticket.CategoryReport.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.CategoryReport.Exporting
{
    public class CategoryReportExporter : FileExporterBase, ICategoryReportExporter
    {
        public async Task<FileDto> ExportToFileCategory(GetItemInput input, ICategoryReportAppService appService)
        {
            Dictionary<string,int> colCountDics = new Dictionary<string, int>();

            var file = new FileDto("Category-" + input.StartDate.ToString("yyyy-MM-dd")+"-"+input.EndDate.ToString("yyyy-MM-dd") + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Items"));
                sheet.OutLineApplyStyle = true;
                var rowCount = 1;

                var outPut = await appService.GetCategorySales(input);
                if (outPut?.CategoryList?.Items != null && DynamicQueryable.Any(outPut.CategoryList.Items))
                {
                    var col = 3;
                    AddDetail(sheet, 2, 1, L("Group"));
                    AddDetail(sheet, 2, 2, L("Category"));

                    var allCate = outPut.CategoryList.Items.ToList();
                    var allLo = allCate.OrderByDescending(a => a.LocationReports.Count());
                    int colCount = 1;
                    foreach (var location in allLo.SelectMany(a=>a.LocationReports.Select(b=>b.LocationName)))
                    {
                        if (!colCountDics.ContainsKey(location))
                        {
                            colCountDics.Add(location,colCount++);
                            AddDetail(sheet, rowCount, col, location);
                            sheet.Cells[rowCount, col, rowCount, col + 1].Merge = true;

                            AddDetail(sheet, rowCount + 1, col++, L("Quantity"));
                            AddDetail(sheet, rowCount + 1, col++, L("Total"));
                        }
                    }

                    AddDetail(sheet, rowCount, col, L("Total"));
                    sheet.Cells[rowCount, col, rowCount, col + 1].Merge = true;

                    AddDetail(sheet, rowCount + 1, col++, L("Quantity"));
                    AddDetail(sheet, rowCount + 1, col++, L("Total"));

                    sheet.Row(2).Style.Font.Bold = true;

                    rowCount += 2;
                    var cates = outPut.CategoryList.Items.MapTo<IList<CategoryReportDto>>();
                    BindingValueForRowInCategoryReport(sheet, rowCount, cates, allLo.First().LocationReports.Count(),colCountDics);
                }

                for (var i = 1; i <= 1; i++) sheet.Column(i).AutoFit();

                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

        private void BindingValueForRowInCategoryReport(ExcelWorksheet sheet, int rowCount, IList<CategoryReportDto> cates, int colCount, Dictionary<string,int> colDics)
        {

            foreach (var item in cates)
            {
                AddDetail(sheet, rowCount, 1, item.ProductGroupName);
                AddDetail(sheet, rowCount, 2, item.CategoryName);
               
                foreach (var location in item.LocationReports.OrderBy(a => a.LocationName))
                {
                    int col = colDics[location.LocationName] * 2 + 1;
                    AddDetail(sheet, rowCount, col++, location.Quantity);
                    AddDetail(sheet, rowCount, col++, location.TotalAmount);
                }

                var finalCount = colDics.Count()*2+3 ;
                AddDetail(sheet, rowCount, finalCount++, item.Quantity);
                AddDetail(sheet, rowCount, finalCount++, item.Total);

                rowCount++;
            }
            sheet.Row(rowCount).Style.Font.Bold = true;
        }
    }
}