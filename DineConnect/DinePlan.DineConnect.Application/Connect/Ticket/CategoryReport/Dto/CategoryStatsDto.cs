﻿using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Ticket.CategoryReport.Dto
{
    public class CategoryStatsDto
    {
        public PagedResultOutput<CategoryReportDto> CategoryList { get; set; }
        public List<string> Categories { get; set; }
        public List<BarChartOutputDto> ChartOutput { get; set; }
    }
}