﻿using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Ticket
{
    public interface IConnectReportExporter
    {
        FileDto OrderExport(List<OrderListDto> dtos);
    }
}