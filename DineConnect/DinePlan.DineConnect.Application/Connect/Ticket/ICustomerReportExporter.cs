﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Ticket.Implementation;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Ticket
{
    public interface ICustomerReportExporter
    {
        Task<FileDto> ExportDetails(GetTicketInput input);

        Task<FileDto> ExportWritersCafe(GetItemInput input, List<WritersCafeList> dtos);
        Task<FileDto> ExportWritersCafeForTicket(GetTicketInput input, List<WritersCafeList> dtos);

        Task<FileDto> ExportToSalesSummary(GetTicketInput input,  List<string> ttList, List<string> ptList, List<string> dtList, List<string> proList, int abpSessionTenantId);
    }
}