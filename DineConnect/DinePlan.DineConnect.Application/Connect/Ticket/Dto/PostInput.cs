﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Connect.Ticket.Dto
{
    public class PostInput : IInputDto
    {
        public int LocationId { get; set; }
        public int TicketId { get; set; }
        public int TenantId { get; set; }
        public string Contents { get; set; }
        public int ContentType { get; set; }

    }

    public class PostOutput : IOutputDto
    {
        public int SyncId { get; set; }
        public string ErrorDesc { get; set; }

    }

    public class PostDataInput : PagedAndSortedInputDto, IShouldNormalize, ILocationFilterDto
    {
        public int IdReProcess { get; set; }
        public bool IsReProcessed { get; set; }
        public bool? NoFilter { get; set; }
        public int ContentType { get; set; }
        public int TotalCount { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
                EndDate = EndDate?.Date.AddDays(1).AddMilliseconds(-1);
            }
        }

        public bool? Processed { get; set; }
        public LocationGroupDto LocationGroup { get; set; }

        public int UserId { get; set; }

        public int? TenantId { get; set; }

        public int StartHour { get; set; }
        public int EndHour { get; set; }
        public int StartMinute { get; set; }
        public int EndMinute { get; set; }
        public double Start_Number => Convert.ToDouble(StartHour * 60  + StartMinute);
        public double End_Number => Convert.ToDouble(EndHour * 60 + EndMinute );
        public string Start_String => StartHour + ":" + StartMinute;
        public string End_String => EndHour + ":" + EndMinute;

        public string DynamicFilter { get; set; }

    }

    public class ApiPostDataOutput
    {
        public ApiPostDataOutput()
        {
            PostData = new List<ApiPostData>();
        }
        public IList<ApiPostData> PostData { get; set; }
    }
    public class ApiPostData
    {
        public int? TicketId { get; set; }
        public int SyncId { get; set; }
    }
    public class ApiUpdatePostData 
    {
        public List<int> TicketId { get; set; }
        public ApiUpdatePostData()
        {
            TicketId = new List<int>();
        }
    }
    [AutoMapTo(typeof(PostData))]
    public class PostDataOutput
    {
        public bool IsReProcess { get; set; }
        public string Contents { get; set; }
        public int ContentType { get; set; }
        public int Id { get; set; }
        public bool Processed { get; set; }
        public string Operations
        {
            get
            {
                return (Processed == false) ? "UnProcess" : "Process";
            }
        }
        public CreateOrUpdateTicketInput TicketItem
        {
            get
            {
                return JsonConvert.DeserializeObject<CreateOrUpdateTicketInput>(Contents);
            }
        }
        public LocationListDto Location { get; set; }

        public string Errors { get; set; }

        public List<ErrorValue> PreErrorValues => string.IsNullOrWhiteSpace(Errors)
                                                ? new List<ErrorValue>() : (JsonConvert.DeserializeObject<List<ErrorValue>>(Errors)).OrderByDescending(t => t.ErrorDate).ToList();
    }
    public class ReprocessOutput
    {
        public int Passed { get; set; }
        public int Failed { get; set; }
        public int TotalTransaction { get; set; }
    }
}
