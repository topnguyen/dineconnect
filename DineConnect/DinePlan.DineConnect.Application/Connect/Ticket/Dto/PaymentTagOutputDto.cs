﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.Dto
{
    public class PaymentTagOutputDto
    {
        public List<PaymentTagInfo> Tags { get; set; }

        public PaymentTagOutputDto()
        {
            Tags = new List<PaymentTagInfo>();
        }
    }

    public class PaymentTagInfo
    {
        public string TicketNo { get; set; }    
        public decimal TotalAmount { get; set; }    
        public string InvoiceNo { get; set; }  
        public string Terminal { get; set; }    
        public string PaymentName { get; set; }    
        public string UserName { get; set; }   
        public string UserCode { get; set; }    
        public bool Refund{ get; set; }    

        public PaymentFrame PaymentFrame { get; set; }    
        public DateTime PaymentTime { get; set; }    
        public string LocationCode { get; set; }    

    }
}
