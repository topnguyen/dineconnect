﻿using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Ticket.Dto
{
    public class ChartOutputDto
    {
        public string name { get; set; }
        public decimal y { get; set; }
    }

    public class BarChartOutputDto
    {
        public string name { get; set; }
        public List<decimal> data { get; set; }
    }
}