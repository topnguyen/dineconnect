﻿namespace DinePlan.DineConnect.Connect.Ticket.Dto
{
    public class DiscountOutput
    {
        public int CodeDiscount { get; set; }
        public string DiscountName { get; set; }
        public string Note { get; set; }
        public string TicketNo { get; set; }
        public string InvoiceNo { get; set; }
        public decimal Total { get; set; }
        public decimal Quantity { get; set; }
        public string Location { get; set; }
        public int? LocationID { get; set; }
        public string DepartmentName { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal Amount => TotalPrice + Total;
    }
}