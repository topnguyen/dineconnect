﻿using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Ticket.Dto
{
    public class DashboardTransactionDto
    {
        public DashboardTransactionDto()
        {
            Dates = new List<string>();
            Totals = new List<decimal>();
            TicketCounts = new List<int>();
        }
        public List<string> Dates { get; set; }
        public List<decimal> Totals { get; set; }
        public List<int> TicketCounts { get; set; }
    }
}
