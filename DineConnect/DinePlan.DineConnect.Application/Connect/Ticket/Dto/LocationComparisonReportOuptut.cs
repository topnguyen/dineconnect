﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace DinePlan.DineConnect.Connect.Ticket.Dto
{
    public class LocationComparisonReportOuptut : EntityDto
    {
        public DateTime Date { get; set; }

        public string Day { get; set; }

        public string WeekNumber
        {
            get
            {
                var currentCulture = CultureInfo.CurrentCulture;
                return "W" + currentCulture.Calendar.GetWeekOfYear(Date, currentCulture.DateTimeFormat.CalendarWeekRule, currentCulture.DateTimeFormat.FirstDayOfWeek);
            }
        }

        public List<LocationComparisonDetails> LocationDetails { get; set; }

        public LocationComparisonReportOuptut()
        {
            LocationDetails = new List<LocationComparisonDetails>();
        }
    }

    public class LocationComparisonDetails
    {
        public int LocationId { get; set; }

        public string LocationName { get; set; }

        public decimal SubTotal { get; set; }

        public decimal GrossSales { get; set; }//=> Tickets.Sum(t => t.GetPlainSum());
    }
}