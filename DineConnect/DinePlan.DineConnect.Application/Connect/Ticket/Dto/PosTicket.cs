﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.Dto
{
    public class AccountTransactionValue
    {
        public int AccountTypeId { get; set; }
        public int AccountId { get; set; }
        public DateTime Date { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal Exchange { get; set; }
        public int AccountTransactionId { get; set; }
        public int AccountTransactionDocumentId { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
    }

    public class SourceTransactionValue
    {
        public int AccountTypeId { get; set; }
        public int AccountId { get; set; }
        public DateTime Date { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal Exchange { get; set; }
        public int AccountTransactionId { get; set; }
        public int AccountTransactionDocumentId { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
    }

    public class TargetTransactionValue
    {
        public int AccountTypeId { get; set; }
        public int AccountId { get; set; }
        public DateTime Date { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal Exchange { get; set; }
        public int AccountTransactionId { get; set; }
        public int AccountTransactionDocumentId { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
    }

    public class AccountTransaction
    {
        public decimal Amount { get; set; }
        public decimal ExchangeRate { get; set; }
        public int AccountTransactionDocumentId { get; set; }
        public int AccountTransactionTypeId { get; set; }
        public int SourceAccountTypeId { get; set; }
        public int TargetAccountTypeId { get; set; }
        public bool IsReversed { get; set; }
        public bool Reversable { get; set; }
        public decimal Balance { get; set; }
        public List<AccountTransactionValue> AccountTransactionValues { get; set; }
        public SourceTransactionValue SourceTransactionValue { get; set; }
        public TargetTransactionValue TargetTransactionValue { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
    }

    public class TransactionDocument
    {
        public DateTime Date { get; set; }
        public int DocumentTypeId { get; set; }
        public List<AccountTransaction> AccountTransactions { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
    }

    public class PosOrder
    {
        public string UniqueId { get; set; }
        public bool IsSelected { get; set; }
        public int TicketId { get; set; }
        public int WarehouseId { get; set; }
        public int DepartmentId { get; set; }
        public int MenuItemId { get; set; }
        public int MenuItemPortionId { get; set; }
        public string MenuItemName { get; set; }
        public string PortionName { get; set; }
        public decimal Price { get; set; }
        public decimal OldPrice { get; set; }
        public decimal PortionPrice { get; set; }
        public decimal OriginalPrice { get; set; }
        public decimal Quantity { get; set; }
        public int PortionCount { get; set; }
        public object Note { get; set; }
        public bool Locked { get; set; }
        public bool CalculatePrice { get; set; }
        public bool DecreaseInventory { get; set; }
        public bool IncreaseInventory { get; set; }
        public int OrderNumber { get; set; }
        public string CreatingUserName { get; set; }
        public string OrderDesc { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public int AccountTransactionTypeId { get; set; }
        public object ProductTimerValueId { get; set; }
        public object ProductTimerValue { get; set; }
        public string PriceTag { get; set; }
        public object Tag { get; set; }
        public int UpsellingOrderRef { get; set; }
        public object IsUpselling { get; set; }
        public bool IsParent { get; set; }
        public decimal AveragePrice { get; set; }
        public int TotalIndex { get; set; }
        public int ItemIndex { get; set; }
        public string Taxes { get; set; }
        public object OrderTags { get; set; }
        public string OrderStates { get; set; }
        public decimal SelectedQuantity { get; set; }
        public string Description { get; set; }
        public string OrderKey { get; set; }
        public object AlternateId { get; set; }
        public object TicketNumber { get; set; }
        public string QuantityKey { get; set; }
        public string OrderTagGroupKey { get; set; }
        public bool IsPromotionOrder { get; set; }
        public int PromotionSyncId { get; set; }
        public string PromotionDetails { get; set; }
        public int OrderRef { get; set; }
        public int MenuItemType { get; set; }
        public string SelfOrderingImagePath { get; set; }
        public string ItemDescription { get; set; }
        public int Id { get; set; }
    }

    public class AccountTransactionValue2
    {
        public int AccountTypeId { get; set; }
        public int AccountId { get; set; }
        public DateTime Date { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal Exchange { get; set; }
        public int AccountTransactionId { get; set; }
        public int AccountTransactionDocumentId { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
    }

    public class SourceTransactionValue2
    {
        public int AccountTypeId { get; set; }
        public int AccountId { get; set; }
        public DateTime Date { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal Exchange { get; set; }
        public int AccountTransactionId { get; set; }
        public int AccountTransactionDocumentId { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
    }

    public class TargetTransactionValue2
    {
        public int AccountTypeId { get; set; }
        public int AccountId { get; set; }
        public DateTime Date { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal Exchange { get; set; }
        public int AccountTransactionId { get; set; }
        public int AccountTransactionDocumentId { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
    }

    public class AccountTransaction2
    {
        public decimal Amount { get; set; }
        public decimal ExchangeRate { get; set; }
        public int AccountTransactionDocumentId { get; set; }
        public int AccountTransactionTypeId { get; set; }
        public int SourceAccountTypeId { get; set; }
        public int TargetAccountTypeId { get; set; }
        public bool IsReversed { get; set; }
        public bool Reversable { get; set; }
        public decimal Balance { get; set; }
        public List<AccountTransactionValue2> AccountTransactionValues { get; set; }
        public SourceTransactionValue2 SourceTransactionValue { get; set; }
        public TargetTransactionValue2 TargetTransactionValue { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
    }

    public class PosPayment
    {
        public int PaymentTypeId { get; set; }
        public int TicketId { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public decimal TenderedAmount { get; set; }
        public int AccountTransactionId { get; set; }
        public int TerminalId { get; set; }
        public AccountTransaction2 AccountTransaction { get; set; }
        public decimal Amount { get; set; }
        public int UserId { get; set; }
        public string Description { get; set; }
        public int Id { get; set; }
    }

    public class PosTicket
    {
        public int OrderHour { get; set; }
        public string Amount { get; set; }
        public string TicketNumber { get; set; }
        public string QueueNumber { get; set; }
        public string ReferenceNumber { get; set; }
        public string InvoiceNo { get; set; }
        public string FullTaxInvoiceNo { get; set; }
        public string TicketLog1 { get; set; }
        public DateTime Date { get; set; }
        public DateTime LastOrderDate { get; set; }
        public string LastOrderDateString { get; set; }
        public DateTime LastPaymentDate { get; set; }
        public bool IsClosed { get; set; }
        public bool IsLocked { get; set; }
        public decimal RemainingAmount { get; set; }
        public decimal TotalAmount { get; set; }
        public int DepartmentId { get; set; }
        public int TicketTypeId { get; set; }
        public bool Credit { get; set; }
        public bool Training { get; set; }
        public string Note { get; set; }
        public string LastModifiedUserName { get; set; }
        public string TicketTags { get; set; }
        public string TicketStates { get; set; }
        public string TicketLogs { get; set; }
        public decimal ExchangeRate { get; set; }
        public bool TaxIncluded { get; set; }
        public int TerminalId { get; set; }
        public int TransactionDocument_Id { get; set; }
        public TransactionDocument TransactionDocument { get; set; }
        public List<object> TicketEntities { get; set; }
        public bool PreOrder { get; set; }
        public List<PosOrder> Orders { get; set; }
        public List<object> TicketQueues { get; set; }
        public string ReferenceInvoices { get; set; }
        public List<object> Calculations { get; set; }
        public List<PosPayment> Payments { get; set; }
        public List<object> ChangePayments { get; set; }
        public List<object> PaidItems { get; set; }
        public string UserString { get; set; }
        public bool CanSubmit { get; set; }
        public bool IsTagged { get; set; }
        public List<PosOrder> SelectedOrders { get; set; }
        public int SyncId { get; set; }
        public string SyncTime { get; set; }
        public string AlternateId { get; set; }
        public bool SyncTried { get; set; }
        public int WorkPeriodId { get; set; }
        public string AlternateUpdationTime { get; set; }
        public bool Silent { get; set; }
        public DateTime LastUpdateTime { get; set; }
        public string QuickDineTicketId { get; set; }
        public string QuickDineUpdationTime { get; set; }
        public bool QuickDineTicketStatusUpdated { get; set; }
        public string PromotionDetails { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
    }
}