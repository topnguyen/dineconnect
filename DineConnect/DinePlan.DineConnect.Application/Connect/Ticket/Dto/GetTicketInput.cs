﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities.Auditing;
using Abp.Extensions;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Discount.Dtos;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter;
using OpenHtmlToPdf;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Ticket.Dto
{
    public interface IDateInput
    {
        DateTime StartDate { get; set; }
        DateTime EndDate { get; set; }
        int Location { get; set; }
        long UserId { get; set; }
        bool NotCorrectDate { get; set; }
        List<SimpleLocationDto> Locations { get; set; }
        LocationGroupDto LocationGroup { get; set; }
        bool Credit { get; set; }
        bool Refund { get; set; }
        string TicketNo { get; set; }
        string DateFormat { get; set; }
        string DatetimeFormat { get; set; }
    }

    public class GetTicketInput : MaxPagedAndSortedInputDto, IShouldNormalize, IDateInput, IFileExport, IBackgroundExport
    {
        public GetTicketInput()
        {
            Locations = new List<SimpleLocationDto>();
            LocationGroup = new LocationGroupDto();
            Promotions = new List<PromotionListDto>();
        }

        public List<PromotionListDto> Promotions { get; set; }
        public List<string> PaymentTags { get; set; }

        public List<int> Payments { get; set; }
        public List<ComboboxItemDto> Transactions { get; set; }
        public List<ComboboxItemDto> Departments { get; set; }
        public List<ComboboxItemDto> TicketTags { get; set; }
        public List<ComboboxItemDto> AuditTypes { get; set; }
        public string Department { get; set; }
        public string TerminalName { get; set; }
        public string LastModifiedUserName { get; set; }
        public string OutputType { get; set; }
        public string ExportType { get; set; }
        public bool ByLocation { get; set; }
        public string Duration { get; set; }
        public int TenantId { get; set; }
        public string ExportOutput { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Location { get; set; }
        public long UserId { get; set; }
        public List<SimpleLocationDto> Locations { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public bool NotCorrectDate { get; set; }
        public bool Credit { get; set; }
        public bool Refund { get; set; }
        public string TicketNo { get; set; }

        public ExportType ExportOutputType { get; set; }
        public PaperSize PaperSize { get; set; }
        public bool Portrait { get; set; }

        public void Normalize()
        {
            if (Sorting.IsNullOrWhiteSpace())
            {
                Sorting = "Id";
            }
            else
            {
                Sorting = Sorting.Replace("locationName", "Location.Name");
            }
        }

        public bool RunInBackground { get; set; }
        public string ReportDescription { get; set; }
        public FileDto FileDto { get; set; }

        public bool PaymentName { get; set; }

        public bool IncludeTax { get; set; }
        public bool ReportDisplay { get; set; }

        public string DynamicFilter { get; set; }
        public string DateFormat { get; set; }
        public string DatetimeFormat { get; set; }

    }

    public class GetTicketSyncInput : PagedAndSortedInputDto, IFileExport, IDateInput, IBackgroundExport
    {
        public GetTicketSyncInput()
        {
            Locations = new List<SimpleLocationDto>();
            LocationGroup = new LocationGroupDto();
        }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Location { get; set; }
        public List<SimpleLocationDto> Locations { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public ExportType ExportOutputType { get; set; }
        public PaperSize PaperSize { get; set; }
        public bool Portrait { get; set; }
        public long UserId { get; set; }
        public bool NotCorrectDate { get; set; }
        public bool Credit { get; set; }
        public bool Refund { get; set; }
        public string TicketNo { get; set; }
        public bool RunInBackground { get; set; }
        public string ReportDescription { get; set; }
        public string DynamicFilter { get; set; }
        public string DateFormat { get; set; }
        public string DatetimeFormat { get; set; }
    }



    public class GetTicketSyncOutput : AuditedEntity
    {
        public string LocationCode { get; set; }
        public string LocationName { get; set; }
        public int TicketSyncCount { get; set; }
        public decimal TicketTotalAmount { get; set; }
        public DateTime WorkStartDate { get; set; }
        public DateTime WorkCloseDate { get; set; }
        public int TotalTickets { get; set; }
        public decimal WorkTotalAmount { get; set; }
        public bool IsDifference { get; set; }
        // format date
        public DateTime FormatWorkStartDate
        {
            get
            {
                var date = new DateTime(WorkStartDate.Year, WorkStartDate.Month, WorkStartDate.Day);
                return date;
            }
        }
        public DateTime FormatWorkCloseDate
        {
            get
            {
                var date = new DateTime(WorkCloseDate.Year, WorkCloseDate.Month, WorkCloseDate.Day);
                return date;
            }
        }

    }

    public class SaleTargetTicketInput : GetTicketInput
    {
        public string FileName { get; set; }
    }

    public class GetItemInput : PagedAndSortedInputDto, IShouldNormalize, IDateInput, IFileExport, IBackgroundExport
    {
        public bool IncludeTax { get; set; }
        public List<string> FilterByUser { get; set; }
        public List<string> FilterByTerminal { get; set; }
        public List<string> FilterByDepartment { get; set; }
        public List<int?> FilterByCategory { get; set; }
        public List<int?> FilterByGroup { get; set; }
        public List<string> FilterByTag { get; set; }
        public bool IsItemSalesReport { get; set; }
        public bool IsSummary { get; set; }
        public string LastModifiedUserName { get; set; }

        public bool IncludeAll { get; set; }
        public bool Gift { get; set; }
        public bool Void { get; set; }
        public bool Comp { get; set; }
        public bool Sales { get; set; }
        public bool Exchange { get; set; }
        public bool NoSales { get; set; }
        public bool Stats { get; set; }
        public bool ByLocation { get; set; }
        public string Duration { get; set; }
        public string ExportOutput { get; set; }
        public int WorkPeriodId { get; set; }
        public string OutputType { get; set; }
        public List<PromotionListDto> Promotions { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Location { get; set; }
        public long UserId { get; set; }
        public bool NotCorrectDate { get; set; }
        public List<SimpleLocationDto> Locations { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public bool Credit { get; set; }
        public bool Refund { get; set; }
        public string TicketNo { get; set; }
        public ExportType ExportOutputType { get; set; }
        public PaperSize PaperSize { get; set; }
        public bool Portrait { get; set; }
        public List<int> MenuItemIds { get; set; }
        public List<int> MenuItemPortionIds { get; set; }
        public void Normalize()
        {
            if (Sorting.IsNullOrWhiteSpace())
            {
                Sorting = "Id";
            }
        }

        public bool RunInBackground { get; set; }
        public bool IsExport { get; set; }
        public string ReportDescription { get; set; }
        public int? TenantId { get; set; }

        public int TakeNumber { get; set; }
        public string TakeType { get; set; }
        public string Ranking { get; set; }

        public string TakeString
        {
            get { return $"{Ranking} {TakeType}"; }
        }
        public string DynamicFilter { get; set; }
        public List<ComboboxItemDto> AuditTypes { get; set; }
        public string TerminalName { get; set; }
        public bool TaxIncluded { get; set; }
        public string DateFormat { get; set; }
        public string DatetimeFormat { get; set; }

        public GetItemInput()
        {
            FilterByCategory = new List<int?>();
            FilterByGroup = new List<int?>();
            FilterByDepartment = new List<string>();
            Locations = new List<SimpleLocationDto>();
            FilterByTerminal = new List<string>();
            FilterByUser = new List<string>();
            MenuItemIds = new List<int>();
        }
    }

    public class GetDayItemInput : PagedAndSortedInputDto, IDateInput, IShouldNormalize
    {
        public bool Gift { get; set; }
        public bool Void { get; set; }
        public bool Comp { get; set; }
        public bool NoSales { get; set; }
        public bool ByLocation { get; set; }
        public List<DateTime> AllDates { get; set; }
        public List<int> MenuPortions { get; set; }
        public bool Stats { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Location { get; set; }
        public int TenantId { get; set; }
        public long UserId { get; set; }
        public List<SimpleLocationDto> Locations { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public bool Credit { get; set; }
        public bool Refund { get; set; }
        public string TicketNo { get; set; }
        public bool NotCorrectDate { get; set; }
        public List<SimpleLocationDto> LocationsByUser { get; set; }
        public List<string> FilterByDepartment { get; set; }
        public List<int?> FilterByCategory { get; set; }
        public List<int?> FilterByGroup { get; set; }
        public List<string> FilterByTag { get; set; }
        public List<int> MenuItemIds { get; set; }

        public List<int> MenuItemPortionIds { get; set; }
        public string DynamicFilter { get; set; }
        public string DateFormat { get; set ; }
        public string DatetimeFormat { get ; set ; }

        public void Normalize()
        {
            if (Sorting.IsNullOrWhiteSpace())
            {
                Sorting = "Id";
            }
        }
    }

    public class GetPaymentInput : PagedAndSortedInputDto, IShouldNormalize, IDateInput
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<SimpleLocationDto> Locations { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public bool NotCorrectDate { get; set; }
        public bool Credit { get; set; }
        public bool Refund { get; set; }
        public string TicketNo { get; set; }
        public int Location { get; set; }
        public long UserId { get; set; }
        public string DateFormat { get; set ; }
        public string DatetimeFormat { get; set ; }

        public void Normalize()
        {
            if (Sorting.IsNullOrWhiteSpace())
            {
                Sorting = "Id";
            }
        }
    }

    public class GetTransactionInput : PagedAndSortedInputDto, IShouldNormalize, IDateInput
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<SimpleLocationDto> Locations { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public int Location { get; set; }
        public long UserId { get; set; }
        public bool NotCorrectDate { get; set; }
        public bool Credit { get; set; }
        public bool Refund { get; set; }
        public string TicketNo { get; set; }

        public void Normalize()
        {
            if (Sorting.IsNullOrWhiteSpace())
            {
                Sorting = "Id";
            }
        }
        public string DateFormat { get; set; }
        public string DatetimeFormat { get; set; }
    }

    public class GetTicketIdInput
    {
        public int Location { get; set; }
        public string TicketNumber { get; set; }
    }

    public class GetPaymentOutput
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
        public string Name { get; set; }
    }

    public class GetTransactionOutput
    {
        public int Id { get; set; }
        public int TicketId { get; set; }
        public decimal Amount { get; set; }
        public string Name { get; set; }
    }

    public class GetChartInput : IDateInput
    {
        public int Location { get; set; }
        public long UserId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<SimpleLocationDto> Locations { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public bool NotCorrectDate { get; set; }
        public bool Credit { get; set; }
        public bool Refund { get; set; }
        public string TicketNo { get; set; }
        public string DateFormat { get ; set; }
        public string DatetimeFormat { get; set ; }
    }

    public class TicketStatus
    {
        public int Id { get; set; }
        public string OrderNumber { get; set; }
        public string OrderTime { get; set; }
        public string PackingTime { get; set; }
        public string SosType { get; set; }
        public string TerminalId { get; set; }
    }




}