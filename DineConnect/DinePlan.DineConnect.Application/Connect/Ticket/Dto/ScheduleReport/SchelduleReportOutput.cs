﻿using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Ticket.Dto
{
    public class SchelduleReportOutput
    {
        public SchelduleReportOutput()
        {
            ScheduleReportDatas = new List<ScheduleReportData>();
        }

        public int MenuItemId { get; set; }

        public string MenuItemName { get; set; }

        public List<ScheduleReportData> ScheduleReportDatas { get; set; }
    }
}