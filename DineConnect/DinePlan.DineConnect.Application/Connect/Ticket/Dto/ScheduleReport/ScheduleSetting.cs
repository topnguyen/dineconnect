﻿using Abp.AutoMapper;
using DinePlan.DineConnect.Connect.Master;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Connect.Ticket.Dto
{
    [AutoMap(typeof(LocationSchdule))]
    public class ScheduleSetting
    {
        [JsonProperty("startHour")]
        public int StartHour { get; set; }

        [JsonProperty("endHour")]
        public int EndHour { get; set; }

        [JsonProperty("startMinute")]
        public int StartMinute { get; set; }

        [JsonProperty("endMinute")]
        public int EndMinute { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}