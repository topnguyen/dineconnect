﻿using Abp.Application.Services.Dto;
using Abp.Extensions;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Discount.Dtos;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter;
using OpenHtmlToPdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.Dto
{
    public class ABBReprintReportOutputDto
    {
        public string PlantCode { get; set; }
        public string PlantName { get; set; }
        public string Cashier { get; set; }
        public string Date { get; set; }
        public string TicketNumber { get; set; }
        public string Status { get; set; }
        public int NoOfReprint { get; set; }
        public string TerminalName { get; set; }
        public decimal Total { get; set; }
    }

    public class ABBReprintReportInputDto : PagedAndSortedInputDto, IShouldNormalize, IDateInput, IFileExport, IBackgroundExport
    {


        public ABBReprintReportInputDto()
        {
            Locations = new List<SimpleLocationDto>();
            LocationGroup = new LocationGroupDto();
            Promotions = new List<PromotionListDto>();
        }

        public bool IsMoreThanOneReprintChecked { get; set; }
        public string SaleStatus { get; set; }
        public string TicketNumber { get; set; }
        public List<PromotionListDto> Promotions { get; set; }
        public List<string> PaymentTags { get; set; }

        public List<int> Payments { get; set; }
        public List<ComboboxItemDto> Transactions { get; set; }
        public List<ComboboxItemDto> Departments { get; set; }
        public List<ComboboxItemDto> TicketTags { get; set; }
        public string Department { get; set; }
        public string TerminalName { get; set; }
        public string LastModifiedUserName { get; set; }
        public string OutputType { get; set; }
        public string ExportType { get; set; }
        public bool ByLocation { get; set; }
        public string Duration { get; set; }
        public int TenantId { get; set; }
        public string ExportOutput { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Location { get; set; }
        public long UserId { get; set; }
        public List<SimpleLocationDto> Locations { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public bool NotCorrectDate { get; set; }
        public bool Credit { get; set; }
        public bool Refund { get; set; }
        public string TicketNo { get; set; }

        public ExportType ExportOutputType { get; set; }
        public PaperSize PaperSize { get; set; }
        public bool Portrait { get; set; }

        public void Normalize()
        {
            if (Sorting.IsNullOrWhiteSpace())
            {
                Sorting = "PlantCode";
            }
        }

        public bool RunInBackground { get; set; }
        public string ReportDescription { get; set; }
        public FileDto FileDto { get; set; }

        public bool PaymentName { get; set; }

        public bool IncludeTax { get; set; }
        public bool ReportDisplay { get; set; }

        public string DynamicFilter { get; set; }
        public string SimpleDateFormat { get; set; }
        public string DateFormat { get ; set; }
        public string DatetimeFormat { get ; set ; }
    }

    public class ABBReprintConsts
    {
        public static string Status = "Status";
    }

    public class SaleStatusConsts
    {
        public static string All = "All";
        public static string Sale = "Sale";
        public static string Refund = "Refund";
        public static string Return = "Return";
    }
}
