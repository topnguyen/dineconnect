﻿namespace DinePlan.DineConnect.Connect.Ticket.Dto
{
    public class CreateOrUpdateTicketOutput 
    {
        public int Ticket { get; set; }
    }
}
