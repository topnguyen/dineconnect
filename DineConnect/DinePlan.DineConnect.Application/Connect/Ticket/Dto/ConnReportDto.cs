﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.Dto
{
   internal class TCategoryDto
    {
        public int CatId { get; set; }
        public string CatgoryName { get; set; }

        public string CatgoryCode { get; set; }
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public string AliasCode { get; set; }
        public string LocationName { get; set; }
        public int PortionId { get; set; }
    }

    public class SimpleKeyValuePair
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }
}
