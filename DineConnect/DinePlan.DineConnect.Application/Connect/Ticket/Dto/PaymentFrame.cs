﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.Dto
{
    public class PaymentFrame 
    {
        public string ResponseCode { get; set; }
        public string AuthorizationCode { get; set; }
        public string TypeOfCard { get; set; }
        public string AccountNumber{get; set;}
        public string Last4DigitsCard{get; set;}
        public string Description { get; set; }
        public string ApprovalCode { get; set; }
        public string TerminalId { get; set; }
        public string MerchantId { get; set; }
        public string CardIssuerName { get; set; }
        public string CardNo { get; set; }
        public string CardExpiry { get; set; }
        public string BatchNo { get; set; }
        public string ReferenceNo { get; set; }
        public string TransactionNo { get; set; }
    }
}
