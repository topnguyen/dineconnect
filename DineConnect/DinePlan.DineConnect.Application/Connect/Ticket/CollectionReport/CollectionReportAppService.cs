﻿using Abp.Application.Services.Dto;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Connect.Tag;
using DinePlan.DineConnect.Connect.Ticket.Central.Dto;
using DinePlan.DineConnect.Connect.Ticket.CollectionReport.Exporting;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Report;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace DinePlan.DineConnect.Connect.Ticket.CollectionReport
{
    public class CollectionReportAppService : DineConnectAppServiceBase, ICollectionReportAppService
    {
        private readonly IBackgroundJobManager _backgroundJobManager;
        private readonly IRepository<Master.Location> _locationRepository;
        private readonly IRepository<Transaction.Ticket> _ticketRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ILocationAppService _locationService;
        private readonly IConnectReportAppService _connectReportAppService;
        private readonly ICollectionReportExporter _collectionReportExporter;
        private readonly IRepository<TicketTagGroup> _tagGroupRepo;
        private readonly IReportBackgroundAppService _reportBackgroundAppService;
        private readonly ITenantSettingsAppService _tenantSettingsService;
        private string[] DISCOUNTNAMES = { "DISCOUNT" };
        private string[] ROUNDTOTALS = { "ROUND+", "ROUND-", "ROUNDING" };

        public CollectionReportAppService(IBackgroundJobManager backgroundJobManager
            , IRepository<Transaction.Ticket> ticketRepository
            , IRepository<Master.Location> locationRepository
            , IUnitOfWorkManager unitOfWorkManager
            , ITenantSettingsAppService tenantSettingsService
            , ILocationAppService locationService
            , IConnectReportAppService connectReportAppService
            , ICollectionReportExporter collectionReportExporter
            , IRepository<TicketTagGroup> tagGroupRepo
            , IReportBackgroundAppService reportBackgroundAppService
        )
        {
            _backgroundJobManager = backgroundJobManager;
            _ticketRepository = ticketRepository;
            _locationRepository = locationRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _locationService = locationService;
            _connectReportAppService = connectReportAppService;
            _collectionReportExporter = collectionReportExporter;
            _tagGroupRepo = tagGroupRepo;
            _reportBackgroundAppService = reportBackgroundAppService;
            _tenantSettingsService = tenantSettingsService;
        }

        #region Collection Report

        public async Task<PagedResultOutput<TicketTypeReportData>> GetCollectionReport(GetTicketInput input)
        {
            var orderByLocation = await GetCollectionObjects(input);
            var resultList = orderByLocation.AsQueryable().OrderBy(input.Sorting).PageBy(input).ToList();
            return new PagedResultOutput<TicketTypeReportData>(orderByLocation.Count(), resultList);
        }

        public async Task<FileDto> GetCollectionReportForExport(GetTicketInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                var setting = await _tenantSettingsService.GetAllSettings();
                input.DatetimeFormat = setting.Connect.DateTimeFormat;
                input.DateFormat = setting.Connect.SimpleDateFormat;
                if (input.RunInBackground)
                {
                    var backGroundId = await _reportBackgroundAppService.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.CollectionReport,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });

                    if (backGroundId > 0)
                    {
                        input.LocationGroup.UserId = AbpSession.UserId.Value;

                        await _backgroundJobManager.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.CollectionReport,
                            TicketInput = input,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                    }
                }
                else
                {
                    return await GetCollectionReportForExportInternal(input);
                }
            }

            return null;
        }

        public async Task<FileDto> GetCollectionReportForExportInternal(GetTicketInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                var orderByLocation = await GetCollectionObjects(input);

                var resultList = orderByLocation.OrderBy(input.Sorting).ToList();

                return _collectionReportExporter.ExportCollectionReport(resultList);
            }
        }

        private async Task<IEnumerable<TicketTypeReportData>> GetCollectionObjects(GetTicketInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                var allReportOutput = new List<TicketTypeReportData>();
                var allTickets = _connectReportAppService.GetAllTicketsForTicketInputNoFilterByDynamicFilter(input);

                foreach (var tickets in allTickets.GroupBy(o => new { o.LocationId, o.Location.Name }))
                {
                    var returnObject = new TicketTypeReportData
                    {
                        Id = tickets.Key.LocationId,
                        LocationId = tickets.Key.LocationId,
                        LocationName = tickets.Key.Name,
                        TotalSales = tickets.Sum(a => a.TotalAmount),
                        Tax = tickets.Sum(a => a.CalculateTax(a.GetPlainSum(),
                            a.Transactions.Where(x => DISCOUNTNAMES.Contains(x.TransactionType.Name.ToUpper()))
                                .Sum(b => b.Amount))),
                        OrderTotal = tickets.SelectMany(a => a.Orders).Sum(a => a.GetTotal()),

                        TicketCount = tickets.Count(),
                        ItemCount = tickets.SelectMany(t => t.Orders).Where(a => a.CalculatePrice && a.DecreaseInventory)
                            .Sum(o => o.Quantity),
                        PaxCount = GetPaxCount(tickets.ToList()),
                    };

                    returnObject.TicketCount = tickets.Count(a => a.TotalAmount > 0);
                    returnObject.ItemDiscountTotal = tickets.Where(a => a.TotalAmount > 0).SelectMany(a => a.Orders)
                        .SelectMany(a => a.GetOrderPromotionList()).Sum(a => a.PromotionAmount);
                    returnObject.ItemSales = tickets.SelectMany(a => a.Orders).Sum(b => b.GetTotal());

                    returnObject.TicketDiscountTotal = tickets.Where(a => a.TotalAmount > 0)
                        .SelectMany(a => a.GetTicketPromotionList()).Sum(a => a.PromotionAmount);

                    var tranAmount = tickets.Where(a => a.TotalAmount > 0).SelectMany(a => a.Transactions)
                        .Where(a => DISCOUNTNAMES.Contains(a.TransactionType.Name.ToUpper())).Sum(a => a.Amount);
                    returnObject.TicketDiscountTotal += tranAmount;

                    var roundTotal = tickets.Where(a => a.TotalAmount > 0).SelectMany(a => a.Transactions)
                        .Where(a => ROUNDTOTALS.Contains(a.TransactionType.Name.ToUpper())).Sum(a => a.Amount);
                    returnObject.RoundingTotal += roundTotal;

                    returnObject.ItemCount = tickets.Where(a => a.TotalAmount > 0).SelectMany(a => a.Orders)
                        .Where(a => a.CalculatePrice && a.DecreaseInventory).Sum(a => a.Quantity);

                    returnObject.PaxCount = GetPaxCount(tickets.Where(a => a.TotalAmount > 0));

                    allReportOutput.Add(returnObject);
                }
                // filter by DynamicFilter

                var dataAsQueryable = allReportOutput.AsQueryable();
                if (!string.IsNullOrEmpty(input.DynamicFilter))
                {
                    var jsonSerializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver =
                            new CamelCasePropertyNamesContractResolver()
                    };
                    var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                    if (filRule?.Rules != null)
                    {
                        dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                    }
                }

                return dataAsQueryable.ToList();
            }
        }

        private decimal GetPaxCount(IEnumerable<Transaction.Ticket> dDto)
        {
            if (_tagGroupRepo.Count() > 1)
            {
                var totalPaxCount = 0M;
                var toaalll = dDto.Where(a => !string.IsNullOrEmpty(a.TicketTags))
                    .Select(a => a.TicketTags);
                foreach (var tag in toaalll)
                    if (!string.IsNullOrEmpty(tag))
                    {
                        var myTag = JsonConvert.DeserializeObject<List<TicketTagValue>>(tag);
                        var lastTag = myTag?.LastOrDefault(a =>
                            a.TagName.Equals("COVERS") || a.TagName.Equals("PAX"));
                        if (lastTag != null) totalPaxCount += int.Parse(lastTag.TagValue);
                    }
                // Gosei Hoan Update pax calculation
                if (totalPaxCount == 0)
                {
                    totalPaxCount = dDto.SelectMany(t => t.Orders).Sum(o => o.NumberOfPax * o.Quantity);
                }
                return totalPaxCount;
            }

            return 0;
        }
    }

    #endregion Collection Report
}