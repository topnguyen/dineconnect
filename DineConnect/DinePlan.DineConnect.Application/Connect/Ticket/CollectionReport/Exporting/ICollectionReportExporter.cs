﻿using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Ticket.Central.Dto;

namespace DinePlan.DineConnect.Connect.Ticket.CollectionReport.Exporting
{
    public interface ICollectionReportExporter
    {
        FileDto ExportCollectionReport(List<TicketTypeReportData> dtos);
    }
}