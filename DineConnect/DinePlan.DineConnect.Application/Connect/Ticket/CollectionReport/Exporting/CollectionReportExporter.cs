﻿using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using DinePlan.DineConnect.Connect.Ticket.Central.Dto;

namespace DinePlan.DineConnect.Connect.Ticket.CollectionReport.Exporting
{
    public class CollectionReportExporter : FileExporterBase, ICollectionReportExporter
    {
        public FileDto ExportCollectionReport(List<TicketTypeReportData> dtos)
        {
            var file = new FileDto("CollectionReport-" + DateTime.Now.ToString("yyyy-MMMM-dd") + ".xlsx",
                  MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Collections"));
                sheet.OutLineApplyStyle = true;

                AddHeader(
                    sheet,
                    L("Description")
                    );

                var col = 1;
                BindingDataToCol(sheet, col++);

                foreach (var item in dtos)
                {
                    BindingDataToCol(sheet, col, item);
                    col++;
                }

                var totalCol = new TicketTypeReportData
                {
                    LocationName = L("Total"),
                    ItemSales = dtos.Sum(d => d.ItemSales),
                    TicketDiscountTotal = dtos.Sum(d => d.TicketDiscountTotal),
                    ItemDiscountTotal = dtos.Sum(d => d.ItemDiscountTotal),
                    TicketCount = dtos.Sum(d => d.TicketCount),
                    ItemCount = dtos.Sum(d => d.ItemCount),
                    PaxCount = dtos.Sum(d => d.PaxCount),
                    Tax = dtos.Sum(d=>d.Tax),
                    SubTotalSum = dtos.Sum(a=>a.SubTotal),
                    TotalSales = dtos.Sum(a=>a.TotalSales)
                };

                BindingDataToCol(sheet, col, totalCol, true);

                sheet.Row(1).Style.Font.Bold = true;
                for (var i = 1; i <= dtos.Count + 2; i++)
                {
                    sheet.Column(i).AutoFit();
                }
                Save(excelPackage, file);
            }
            return file;
        }

        private void BindingDataToCol(ExcelWorksheet sheet, int col, TicketTypeReportData data = null, bool addSubTotal =false)
        {
            var row = 1;
            if (data == null)
            {
                row++;
                AddDetail(sheet, row++, col, L("Sales"));
                AddDetail(sheet, row++, col, L("TicketDiscount"));
                AddDetail(sheet, row++, col, L("OrderDiscount"));
                AddDetail(sheet, row++, col, L("NetSales"));
                AddDetail(sheet, row++, col, L("Tax"));
                AddDetail(sheet, row++, col, L("Total"));
                AddDetail(sheet, row++, col, L("TicketCount"));
                AddDetail(sheet, row++, col, L("ItemCount"));
                AddDetail(sheet, row++, col, L("TotalPAX"));
                AddDetail(sheet, row++, col, L("Avg/Ticket"));
                AddDetail(sheet, row++, col, L("SalesTotal/Item"));
                AddDetail(sheet, row++, col, L("Avg/Pax"));
                AddDetail(sheet, row++, col, L("AvgItems/Ticket"));
            }
            else
            {
                AddDetail(sheet, row++, col, data.LocationName);
                AddDetail(sheet, row++, col, Math.Round(data.ItemSales, _roundDecimals, MidpointRounding.AwayFromZero));
                AddDetail(sheet, row++, col, Math.Round(data.TicketDiscountTotal, _roundDecimals, MidpointRounding.AwayFromZero));
                AddDetail(sheet, row++, col, Math.Round(data.ItemDiscountTotal, _roundDecimals, MidpointRounding.AwayFromZero));
                AddDetail(sheet, row++, col,
                    addSubTotal
                        ? Math.Round(data.SubTotalSum, _roundDecimals, MidpointRounding.AwayFromZero)
                        : Math.Round(data.SubTotal, _roundDecimals, MidpointRounding.AwayFromZero));

                AddDetail(sheet, row++, col, Math.Round(data.Tax, _roundDecimals, MidpointRounding.AwayFromZero));
                AddDetail(sheet, row++, col, Math.Round(data.TotalSales, _roundDecimals, MidpointRounding.AwayFromZero));
                AddDetail(sheet, row++, col, data.TicketCount);
                AddDetail(sheet, row++, col, data.ItemCount);
                AddDetail(sheet, row++, col, data.PaxCount);
                AddDetail(sheet, row++, col,Math.Round(data.Average, _roundDecimals, MidpointRounding.AwayFromZero));
                AddDetail(sheet, row++, col, Math.Round(data.AvgItemCount, _roundDecimals, MidpointRounding.AwayFromZero));
                AddDetail(sheet, row++, col, Math.Round(data.AvePaxCount, _roundDecimals, MidpointRounding.AwayFromZero));
                AddDetail(sheet, row++, col, Math.Round(data.AvgItemCountReceipt, _roundDecimals, MidpointRounding.AwayFromZero));
            }
        }
    }
}