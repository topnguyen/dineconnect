﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Ticket.Central.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.CollectionReport
{
    public interface ICollectionReportAppService : IApplicationService
    {
        Task<PagedResultOutput<TicketTypeReportData>> GetCollectionReport(GetTicketInput input);

        Task<FileDto> GetCollectionReportForExport(GetTicketInput input);

        Task<FileDto> GetCollectionReportForExportInternal(GetTicketInput input);
    }
}