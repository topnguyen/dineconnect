﻿using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.TicketPromotion.Exporting
{
    public interface ITicketPromotionReportExporter
    {
        Task<FileDto> ExportToFileDiscountDetail(GetTicketInput input, ITicketPromotionReportAppService reportAppService);
        Task<FileDto> ExportToFileTicketPromotionDetail(GetTicketInput input, ITicketPromotionReportAppService appService);
    }
}