﻿using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.TicketPromotion.Exporting
{
    public class TicketPromotionReportExporter : FileExporterBase, ITicketPromotionReportExporter
    {
        private IRepository<Master.Location> _locRepo;
        private IRepository<Master.Company> _comRepo;
        private readonly ITenantSettingsAppService _tenantSettingsService;

        public TicketPromotionReportExporter(IRepository<Master.Location> locRepo, IRepository<Master.Company> comRepo, ITenantSettingsAppService tenantSettingsService)
        {
            _locRepo = locRepo;
            _comRepo = comRepo;
            _tenantSettingsService = tenantSettingsService;
        }

        public async Task<FileDto> ExportToFileDiscountDetail(GetTicketInput input, ITicketPromotionReportAppService appService)
        {
            var file = new FileDto("Discount-" + DateTime.Now.ToString("yyyy-MMMM-dd") + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                var setting = await _tenantSettingsService.GetAllSettings();
                var dateTimeFormat = setting.Connect.DateTimeFormat;
                var dateFormat = setting.Connect.SimpleDateFormat;
                var sheet = excelPackage.Workbook.Worksheets.Add(L("Discount"));
                sheet.OutLineApplyStyle = true;

                var row = AddReportHeader(sheet, "Discount Report", input, dateTimeFormat, dateFormat);

                var discountItems = await appService.GetDiscountDetails(input);

                var query = discountItems.Items
                    .GroupBy(c => c.Location)
                    .ToList();

                var headers = new List<string>
                {
                    "",
                    "",
                    L("CodeDiscount"),
                    L("DiscountName"),
                    L("Note"),
                    L("Quantity"),
                    L("%Qty"),
                    L("TotalAmount"),
                    L("%Ttl")
                };
                AddHeader(
                    sheet, row++, headers.ToArray()
                );

                foreach (var item in query)
                {
                    sheet.Cells[row, 1, row, 9].Style.Font.Bold = true;

                    sheet.Cells[row, 1].Value = item.Key;
                    sheet.Cells[row, 1, row, 4].Merge = true;
                    sheet.Cells[row, 5].Value = "";
                    sheet.Cells[row, 6].Value = item.Sum(g => g.Quantity);
                    sheet.Cells[row, 7].Value = Math.Round(100 * item.Sum(c => c.Quantity) / discountItems.Items.Sum(c => c.Quantity), _roundDecimals, MidpointRounding.AwayFromZero) + "%";
                    sheet.Cells[row, 8].Value = Math.Round(item.Sum(c => c.Total), _roundDecimals, MidpointRounding.AwayFromZero);
                    sheet.Cells[row, 9].Value = Math.Round(100 * item.Sum(c => c.Total) / discountItems.Items.Sum(c => c.Total), _roundDecimals, MidpointRounding.AwayFromZero) + "%";
                    foreach (var depart in item.GroupBy(c => c.DepartmentName).ToList())
                    {
                        row++;

                        sheet.Cells[row, 1, row, 9].Style.Font.Bold = true;

                        sheet.Cells[row, 1].Value = $"{L("Department")}: {depart.Key}";
                        sheet.Cells[row, 1, row, 4].Merge = true;
                        sheet.Cells[row, 5].Value = "";
                        sheet.Cells[row, 6].Value = depart.Sum(g => g.Quantity);
                        sheet.Cells[row, 7].Value = Math.Round(100 * depart.Sum(c => c.Quantity) / discountItems.Items.Sum(c => c.Quantity), _roundDecimals, MidpointRounding.AwayFromZero) + "%";
                        sheet.Cells[row, 8].Value = Math.Round(depart.Sum(c => c.Total), _roundDecimals, MidpointRounding.AwayFromZero);
                        sheet.Cells[row, 9].Value = Math.Round(100 * depart.Sum(c => c.Total) / discountItems.Items.Sum(c => c.Total), _roundDecimals, MidpointRounding.AwayFromZero) + "%";

                        foreach (var s in depart.ToList())
                        {
                            row++;
                            sheet.Cells[row, 3].Value = s.CodeDiscount;
                            sheet.Cells[row, 4].Value = s.DiscountName;
                            sheet.Cells[row, 5].Value = s.Note;
                            sheet.Cells[row, 6].Value = s.Quantity;
                            sheet.Cells[row, 7].Value = Math.Round(100 * s.Quantity / discountItems.Items.Sum(c => c.Quantity), _roundDecimals, MidpointRounding.AwayFromZero) + "%";
                            sheet.Cells[row, 8].Value = Math.Round(s.Total, _roundDecimals, MidpointRounding.AwayFromZero);
                            sheet.Cells[row, 9].Value = Math.Round(100 * s.Total / discountItems.Items.Sum(c => c.Total), _roundDecimals, MidpointRounding.AwayFromZero) + "%";
                        }
                    }
                    row++;
                }

                sheet.Cells[row, 1, row, 8].Style.Font.Bold = true;
                sheet.Cells[row, 1].Value = L("Total");
                sheet.Cells[row, 6].Value = discountItems.Items.Sum(c => c.Quantity);
                sheet.Cells[row, 8].Value = Math.Round(discountItems.Items.Sum(c => c.Total), _roundDecimals, MidpointRounding.AwayFromZero);

                sheet.Column(7).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                sheet.Column(9).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                for (var i = 1; i <= 11; i++) sheet.Column(i).AutoFit();
                Save(excelPackage, file);
            }
            return ProcessFile(input, file);
        }

        public async Task<FileDto> ExportToFileTicketPromotionDetail(GetTicketInput input, ITicketPromotionReportAppService appService)
        {
            var file = new FileDto("TicketPromotionDetail-" + DateTime.Now.ToString("yyyy-MMMM-dd") + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            using (var excelPackage = new ExcelPackage())
            {
                var setting = await _tenantSettingsService.GetAllSettings();
                var dateTimeFormat = setting.Connect.DateTimeFormat;
                var dateFormat = setting.Connect.SimpleDateFormat;
                var sheet = excelPackage.Workbook.Worksheets.Add(L("TicketPromotionDetail"));
                sheet.OutLineApplyStyle = true;

                var row = AddReportHeader(sheet, L("TicketPromotionDetailReport"), input, dateTimeFormat, dateFormat);

                var discountItems = await appService.GetTicketPromotionDetails(input);
                var data = discountItems.Items;
                var headers = new List<string>
                {
                    L("Location"),
                    L("DepartmentName"),
                    L("DiscountName"),
                    L("Note"),
                    L("TicketNo"),
                    L("InvoiceNo"),
                    L("Quantity"),
                    L("TotalPrice"),
                    L("DiscountAmount"),
                    L("Amount")
                };
                AddHeader(
                    sheet, row++, headers.ToArray()
                );

                foreach (var item in data)
                {
                    sheet.Cells[row, 1].Value = item.Location;
                    sheet.Cells[row, 2].Value = item.DepartmentName;
                    sheet.Cells[row, 3].Value = item.DiscountName;
                    sheet.Cells[row, 4].Value = item.Note;
                    sheet.Cells[row, 5].Value = item.TicketNo;
                    sheet.Cells[row, 6].Value = item.InvoiceNo;
                    sheet.Cells[row, 7].Value = item.Quantity.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    sheet.Cells[row, 8].Value = item.TotalPrice.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    sheet.Cells[row, 9].Value = item.Total.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    sheet.Cells[row, 10].Value = item.Amount.ToString(ConnectConsts.ConnectConsts.NumberFormat);
                    row++;
                }

                sheet.Cells[row, 1, row, 11].Style.Font.Bold = true;
                sheet.Cells[row, 6].Value = L("Total");
                sheet.Cells[row, 7].Value = data.Sum(c => c.Quantity).ToString(ConnectConsts.ConnectConsts.NumberFormat);
                sheet.Cells[row, 8].Value = data.Sum(c => c.TotalPrice).ToString(ConnectConsts.ConnectConsts.NumberFormat);
                sheet.Cells[row, 9].Value = data.Sum(c => c.Total).ToString(ConnectConsts.ConnectConsts.NumberFormat);
                sheet.Cells[row, 10].Value = data.Sum(c => c.Amount).ToString(ConnectConsts.ConnectConsts.NumberFormat);

                for (var i = 1; i <= 10; i++) sheet.Column(i).AutoFit();
                Save(excelPackage, file);
            }
            return ProcessFile(input, file);
        }

        private int AddReportHeader(ExcelWorksheet sheet, string nameOfReport, IDateInput input, string datetimeFormat, string dateFormat)
        {
            var brandName = "";
            var locationCode = "";
            var locationName = "";
            var branch = "All";

            var isAllLocation = (input.LocationGroup?.Locations?.Count() + input.LocationGroup?.Groups?.Count() + input.LocationGroup?.LocationTags?.Count()) == _locRepo.GetAll().Count();

            if (isAllLocation)
            {
                branch = "All";
            }
            else
            {
                if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
                {
                    branch = input.LocationGroup.Locations.Select(l => l.Name).JoinAsString(", ");
                }
                if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
                {
                    branch = input.LocationGroup.Groups.Select(l => l.Name).JoinAsString(", ");
                }
                if (input.LocationGroup?.LocationTags != null && input.LocationGroup.LocationTags.Any())
                {
                    branch = input.LocationGroup.LocationTags.Select(l => l.Name).JoinAsString(", ");
                }
            }

            Master.Location myLocation = null;

            if (input.Location > 0)
                myLocation = _locRepo.Get(input.Location);

            if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var simpleL = input.LocationGroup.Locations.First();
                if (simpleL != null)
                {
                    myLocation = _locRepo.Get(simpleL.Id);
                }
            }

            if (myLocation != null)
            {
                Master.Company myCompany = _comRepo.Get(myLocation.CompanyRefId);
                if (myCompany != null)
                {
                    brandName = myCompany.Name;
                }
                locationCode = myLocation.Code;
                locationName = myLocation.Name;
            }

            sheet.Cells[1, 1].Value = nameOfReport;
            sheet.Cells[1, 1, 1, 12].Merge = true;
            sheet.Cells[1, 1, 1, 16].Style.Font.Bold = true;
            sheet.Cells[1, 1, 1, 16].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            sheet.Cells[2, 1].Value = L("Brand");
            sheet.Cells[2, 2].Value = brandName;

            sheet.Cells[2, 6].Value = "Business Date:";
            sheet.Cells[2, 7].Value = input.StartDate.ToString(dateFormat) + " to " + input.EndDate.ToString(dateFormat);
            sheet.Cells[3, 1].Value = "Branch Name:";
            sheet.Cells[3, 2].Value = branch;
            sheet.Cells[3, 6].Value = "Printed On:";
            sheet.Cells[3, 7].Value = DateTime.Now.ToString(datetimeFormat);

            sheet.Cells[1, 1, 4, 7].Style.Font.Size = 10;

            return 6;
        }
    }
}