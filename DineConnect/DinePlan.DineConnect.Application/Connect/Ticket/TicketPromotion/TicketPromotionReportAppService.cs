﻿using Abp.Application.Services.Dto;
using Abp.BackgroundJobs;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Connect.Discount;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Ticket.TicketPromotion.Exporting;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Report;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.TicketPromotion
{
    public class TicketPromotionReportAppService : DineConnectAppServiceBase, ITicketPromotionReportAppService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IConnectReportAppService _connectReportAppService;
        private readonly IRepository<Promotion> _promotionRepository;
        private readonly IBackgroundJobManager _backgroundJobManager;
        private readonly IReportBackgroundAppService _reportBackgroundAppService;
        private readonly ITicketPromotionReportExporter _exporter;

        public TicketPromotionReportAppService(
            IUnitOfWorkManager unitOfWorkManager
            , IConnectReportAppService connectReportAppService
            , IRepository<Promotion> promotionRepository
            , IBackgroundJobManager backgroundJobManager
            , IReportBackgroundAppService reportBackgroundAppService
            , ITicketPromotionReportExporter exporter
            )
        {
            _unitOfWorkManager = unitOfWorkManager;
            _connectReportAppService = connectReportAppService;
            _promotionRepository = promotionRepository;
            _backgroundJobManager = backgroundJobManager;
            _reportBackgroundAppService = reportBackgroundAppService;
            _exporter = exporter;
        }

        public async Task<PagedResultOutput<DiscountOutput>> GetDiscountDetails(GetTicketInput input)
        {
            var query =  _connectReportAppService.GetAllTickets(input)
                .Where(a => a.TotalAmount > 0M)
                .WhereIf(!string.IsNullOrEmpty(input.TerminalName), x=>x.TerminalName.Contains(input.TerminalName))
                .WhereIf(!string.IsNullOrEmpty(input.LastModifiedUserName), x=>x.LastModifiedUserName.Contains(input.LastModifiedUserName))
                .ToList();

            // Select Items
            var result = new List<DiscountOutput>();
            foreach (var item in query.Where(a => a.TicketPromotionDetails != null))
            {
                var promotionDetails = JsonConvert.DeserializeObject<List<PromotionDetailValue>>(item.TicketPromotionDetails);

                promotionDetails.ForEach(promotionDetail =>
                result.Add(new DiscountOutput
                {
                    CodeDiscount = promotionDetail.PromotionSyncId,
                    DiscountName = promotionDetail.PromotionName,
                    Note = promotionDetail.PromotionNote,
                    TicketNo = item.TicketNumber,
                    InvoiceNo = item.InvoiceNo,
                    Total = promotionDetail.PromotionAmount,
                    Quantity = item.Orders.Sum(a => a.Quantity),
                    DepartmentName = item.DepartmentName,
                    Location = $"{item.Location?.Code} - {item.Location?.Name}",
                    LocationID = item.Location?.Id
                }));
            }


            if (input.Promotions != null && input.Promotions.Any())
            {
                var allIds = input.Promotions.Select(a => a.Id);
                result = result.Where(a => allIds.Contains(a.CodeDiscount)).ToList();
            }

            if (input.Sorting == "Id") input.Sorting = "DiscountName";
            result = result
                .GroupBy(r => new { r.CodeDiscount, r.DiscountName, r.Location, r.DepartmentName , r.LocationID})
                .Select(g => new DiscountOutput
                {
                    CodeDiscount = g.Key.CodeDiscount,
                    DiscountName = g.Key.DiscountName,
                    Note = string.Join(",", g.Where(s=>!string.IsNullOrEmpty(s.Note)).Select(s =>s.Note).Distinct()),
                    Total = g.Sum(a => a.Total),
                    Quantity = g.Count(),
                    DepartmentName = g.Key.DepartmentName,
                    Location = g.Key.Location  ,
                    LocationID = g.Key.LocationID
                })
                .OrderBy(input.Sorting)
                .ToList();
            // filter query builder
            var dataAsQueryable = result.AsQueryable();
            if (!string.IsNullOrEmpty(input.DynamicFilter))
            {
                var jsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver =
                        new CamelCasePropertyNamesContractResolver()
                };
                var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                if (filRule?.Rules != null)
                {
                    dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                }
            }


            var countTotal = dataAsQueryable.Count();

            return new PagedResultOutput<DiscountOutput>(countTotal, dataAsQueryable.ToList());
        }

        public async Task<PagedResultOutput<DiscountOutput>> GetTicketPromotionDetails(GetTicketInput input)
        {
            var query = _connectReportAppService.GetAllTickets(input)
                .Where(a => a.TotalAmount > 0M)
                .WhereIf(!string.IsNullOrEmpty(input.TerminalName), x => x.TerminalName.Contains(input.TerminalName))
                .WhereIf(!string.IsNullOrEmpty(input.LastModifiedUserName), x => x.LastModifiedUserName.Contains(input.LastModifiedUserName))
                .ToList();

            // Select Items
            var result = new List<DiscountOutput>();
            foreach (var item in query.Where(a => a.TicketPromotionDetails != null))
            {
                var promotionDetails = JsonConvert.DeserializeObject<List<PromotionDetailValue>>(item.TicketPromotionDetails);

                promotionDetails.ForEach(promotionDetail =>
                result.Add(new DiscountOutput
                {
                    Note = promotionDetail.PromotionNote,
                    CodeDiscount = promotionDetail.PromotionSyncId,
                    DiscountName = promotionDetail.PromotionName,
                    TicketNo = item.TicketNumber,
                    InvoiceNo = item.InvoiceNo,
                    Total = promotionDetail.PromotionAmount,
                    Quantity = item.Orders.Sum(a => a.Quantity),
                    DepartmentName = item.DepartmentName,
                    Location = item.Location?.Code,
                    LocationID = item.Location?.Id,
                    TotalPrice = item.Orders.Sum(x=>x.Price)
                }));
            }


            if (input.Promotions != null && input.Promotions.Any())
            {
                var allIds = input.Promotions.Select(a => a.Id);
                result = result.Where(a => allIds.Contains(a.CodeDiscount)).ToList();
            }

            if (input.Sorting == "Id") input.Sorting = "DiscountName";
            var dataAsQueryable = result.OrderBy(input.Sorting).AsQueryable();
            
            if (!string.IsNullOrEmpty(input.DynamicFilter))
            {
                var jsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver =
                        new CamelCasePropertyNamesContractResolver()
                };
                var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                if (filRule?.Rules != null)
                {
                    dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                }
            }


            var countTotal = dataAsQueryable.Count();

            var output = new PagedResultOutput<DiscountOutput>(countTotal, dataAsQueryable.ToList());

            return await Task.FromResult(output);
        }


        public async Task<FileDto> GetDiscountDetailExport(GetTicketInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _reportBackgroundAppService.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.DISCOUNT,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                        await _backgroundJobManager.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.DISCOUNT,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value,
                            TicketInput = input
                        });
                }
                else
                {
                    return await _exporter.ExportToFileDiscountDetail(input, this);
                }
            }

            return null;
        }

        public async Task<FileDto> GetTicketPromotionDetailExport(GetTicketInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                if (input.RunInBackground)
                {
                    var backGroundId = await _reportBackgroundAppService.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.TICKETPROMOTIONDETAIL,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                        await _backgroundJobManager.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.TICKETPROMOTIONDETAIL,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value,
                            TicketInput = input
                        });
                }
                else
                {
                    return await _exporter.ExportToFileTicketPromotionDetail(input, this);
                }
            }

            return null;
        }

    }
}