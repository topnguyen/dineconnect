﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Ticket.TicketPromotion
{
    public interface ITicketPromotionReportAppService : IApplicationService
    {
        Task<PagedResultOutput<DiscountOutput>> GetDiscountDetails(GetTicketInput input);
        Task<FileDto> GetDiscountDetailExport(GetTicketInput input);
        Task<PagedResultOutput<DiscountOutput>> GetTicketPromotionDetails(GetTicketInput input);
        Task<FileDto> GetTicketPromotionDetailExport(GetTicketInput input);
    }
}