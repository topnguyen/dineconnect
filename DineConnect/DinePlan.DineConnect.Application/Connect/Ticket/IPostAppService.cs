﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Credit.Dtos;
using DinePlan.DineConnect.Connect.Departments.Dtos;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Ticket
{
    public interface IPostAppService : IApplicationService
    {
        Task<PostOutput> PostContent(PostInput input);
        Task<PagedResultOutput<PostDataOutput>> PullContents(PostDataInput input);
        PostDataOutput PullContent(int id);
        void SyncTicket(int postdataId);
        Task UpdateContents(PostDataOutput input);
        Task<ApiPostDataOutput> ApiPostData(ApiLocationInput locationInput);
        Task UpdatePostData(ApiUpdatePostData input);
    }
}

