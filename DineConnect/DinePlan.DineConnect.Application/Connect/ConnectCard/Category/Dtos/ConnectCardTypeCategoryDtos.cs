﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
namespace DinePlan.DineConnect.Connect.Card.Dtos
{
    [AutoMapFrom(typeof(ConnectCardTypeCategory))]
    public class ConnectCardTypeCategoryListDto : FullAuditedEntityDto
    {
        //TODO: DTO ConnectCardTypeCategory Properties Missing
        public string Name { get; set; }
        public string Code { get; set; }
    }
    [AutoMapTo(typeof(ConnectCardTypeCategory))]
    public class ConnectCardTypeCategoryEditDto
    {
        public int? Id { get; set; }
        //TODO: DTO ConnectCardTypeCategory Properties Missing
        public string Name { get; set; }
        public string Code { get; set; }
    }

    public class GetConnectCardTypeCategoryInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }
    public class GetConnectCardTypeCategoryForEditOutput : IOutputDto
    {
        public ConnectCardTypeCategoryEditDto ConnectCardTypeCategory { get; set; }
    }
    public class CreateOrUpdateConnectCardTypeCategoryInput : IInputDto
    {
        [Required]
        public ConnectCardTypeCategoryEditDto ConnectCardTypeCategory { get; set; }
    }
}

