﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Card.Dtos;
using DinePlan.DineConnect.Connect.ConnectCard.CardType.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.ConnectCard.Category
{
    public interface IConnectCardTypeCategoryAppService : IApplicationService
    {
        Task<PagedResultOutput<ConnectCardTypeCategoryListDto>> GetAll(GetConnectCardTypeCategoryInput inputDto);
        Task<FileDto> GetAllToExcel(GetConnectCardTypeCategoryInput input);
        Task<GetConnectCardTypeCategoryForEditOutput> GetConnectCardTypeCategoryForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateConnectCardTypeCategory(CreateOrUpdateConnectCardTypeCategoryInput input);
        Task DeleteConnectCardTypeCategory(IdInput input);
        Task<ListResultOutput<ConnectCardTypeCategoryListDto>> GetConnectCardTypeCategorys();
    }
}