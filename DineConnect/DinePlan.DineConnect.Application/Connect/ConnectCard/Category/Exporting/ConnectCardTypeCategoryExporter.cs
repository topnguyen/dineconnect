﻿using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Card.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.ConnectCard.Category.Exporting
{
    public class ConnectCardTypeCategoryListExcelExporter : FileExporterBase, IConnectCardTypeCategoryListExcelExporter
    {
        public FileDto ExportToFile(List<ConnectCardTypeCategoryListDto> dtos)
        {
            return CreateExcelPackage(
                "ConnectCardTypeCategoryList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("ConnectCardTypeCategory"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                         L("Code"),
                          L("Name")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                         _ => _.Code,
                          _ => _.Name
                        );

                    for (var i = 1; i <= 1; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}