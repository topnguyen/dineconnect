﻿using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Card.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.ConnectCard.Category.Exporting
{
    public interface IConnectCardTypeCategoryListExcelExporter
    {
        FileDto ExportToFile(List<ConnectCardTypeCategoryListDto> dtos);
    }
}