﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Card;
using DinePlan.DineConnect.Connect.Card.Dtos;
using DinePlan.DineConnect.Connect.ConnectCard.Category.Exporting;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.ConnectCard.Category
{
    public class ConnectCardTypeCategoryAppService : DineConnectAppServiceBase, IConnectCardTypeCategoryAppService
    {
        private readonly IConnectCardTypeCategoryListExcelExporter _connectcardtypecategoryExporter;
        private readonly IRepository<ConnectCardTypeCategory> _connectcardtypecategoryRepo;
        private readonly IConnectCardTypeCategoryManager _connectcardtypecategoryManager;

        public ConnectCardTypeCategoryAppService(IRepository<ConnectCardTypeCategory> connectcardtypecategoryRepo,
            IConnectCardTypeCategoryListExcelExporter connectcardtypecategoryExporter,
            IConnectCardTypeCategoryManager connectcardtypecategoryManager)
        {
            
            _connectcardtypecategoryExporter = connectcardtypecategoryExporter;
            _connectcardtypecategoryRepo = connectcardtypecategoryRepo;
            _connectcardtypecategoryManager = connectcardtypecategoryManager;
        }

        public async Task<PagedResultOutput<ConnectCardTypeCategoryListDto>> GetAll(GetConnectCardTypeCategoryInput input)
        {
            var allItems = _connectcardtypecategoryRepo
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                     p => p.Name.ToUpper().Contains(input.Filter.ToUpper())||p.Code.ToUpper().Contains(input.Filter.ToUpper())
                );
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<ConnectCardTypeCategoryListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<ConnectCardTypeCategoryListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetConnectCardTypeCategoryInput input)
        {
            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<ConnectCardTypeCategoryListDto>>();
            return _connectcardtypecategoryExporter.ExportToFile(allListDtos);
        }

        public async Task<GetConnectCardTypeCategoryForEditOutput> GetConnectCardTypeCategoryForEdit(NullableIdInput input)
        {
            ConnectCardTypeCategoryEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _connectcardtypecategoryRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<ConnectCardTypeCategoryEditDto>();
            }
            else
            {
                editDto = new ConnectCardTypeCategoryEditDto();
            }

            return new GetConnectCardTypeCategoryForEditOutput
            {
                ConnectCardTypeCategory = editDto
            };
        }

        public async Task CreateOrUpdateConnectCardTypeCategory(CreateOrUpdateConnectCardTypeCategoryInput input)
        {
            if (input.ConnectCardTypeCategory.Id.HasValue)
            {
                await UpdateConnectCardTypeCategory(input);
            }
            else
            {
                await CreateConnectCardTypeCategory(input);
            }
        }

        public async Task DeleteConnectCardTypeCategory(IdInput input)
        {
            await _connectcardtypecategoryRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateConnectCardTypeCategory(CreateOrUpdateConnectCardTypeCategoryInput input)
        {
            var item = await _connectcardtypecategoryRepo.GetAsync(input.ConnectCardTypeCategory.Id.Value);
            var dto = input.ConnectCardTypeCategory;
            item.Name = dto.Name;
            item.Code = dto.Code;
            CheckErrors(await _connectcardtypecategoryManager.CreateSync(item));
        }

        protected virtual async Task CreateConnectCardTypeCategory(CreateOrUpdateConnectCardTypeCategoryInput input)
        {
            var dto = input.ConnectCardTypeCategory.MapTo<ConnectCardTypeCategory>();
            CheckErrors(await _connectcardtypecategoryManager.CreateSync(dto));
        }
        public async Task<ListResultOutput<ConnectCardTypeCategoryListDto>> GetConnectCardTypeCategorys()
        {
            var connectcardtypeCategory = _connectcardtypecategoryRepo.GetAll();
            return new ListResultOutput<ConnectCardTypeCategoryListDto>(connectcardtypeCategory.MapTo<List<ConnectCardTypeCategoryListDto>>());
        }
    }
}
