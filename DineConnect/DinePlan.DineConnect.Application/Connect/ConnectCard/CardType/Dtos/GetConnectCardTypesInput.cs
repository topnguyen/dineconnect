﻿using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.ConnectCard.CardType.Dtos
{
    public class GetConnectCardTypesInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public bool IsDeleted { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
}