﻿using Abp.AutoMapper;
using DinePlan.DineConnect.Connect.Card;
using DinePlan.DineConnect.Filter;

namespace DinePlan.DineConnect.Connect.ConnectCard.CardType.Dtos
{
    [AutoMapFrom(typeof(ConnectCardType))]
    public class ConnectCardTypeListDto 
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool UseOneTime { get; set; }

        public int Oid { get; set; }

        public int Length { get; set; }

        public int CardCount { get; set; }
    }
}