﻿using System;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.Location;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.ConnectCard.CardType.Dtos
{
    [AutoMapTo(typeof(Connect.Card.ConnectCardType))]
    public class ConnectCardTypeEditDto : ConnectEditDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }

        public bool UseOneTime { get; set; }

        public int Oid { get; set; }

        public int Length { get; set; }
        public int? ConnectCardTypeCategoryId { get; set; }
        public int? TenantId { get; set; }
        public string Tag { get; set; }
        public DateTime ValidTill { get; set; }

        public ConnectCardTypeEditDto()
        {
            ValidTill = DateTime.Now.AddDays(30);
        }


    }

    public class CreateOrUpdateConnectCardTypeInput : IInputDto
    {
        [Required]
        public ConnectCardTypeEditDto ConnectCardType { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
    }
}