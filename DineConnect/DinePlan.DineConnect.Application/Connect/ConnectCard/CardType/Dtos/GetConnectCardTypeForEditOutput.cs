﻿using DinePlan.DineConnect.Connect.ConnectCard.Card.Dtos;
using DinePlan.DineConnect.Connect.Location;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.ConnectCard.CardType.Dtos
{
    public class GetConnectCardTypeForEditOutput
    {
        public GetConnectCardTypeForEditOutput()
        {
            ConnectCardType = new ConnectCardTypeEditDto();
            CardList = new List<ConnectCardListDto>();
            LocationGroup = new LocationGroupDto();
        }

        public ConnectCardTypeEditDto ConnectCardType { get; set; }

        public List<ConnectCardListDto> CardList { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
    }
}