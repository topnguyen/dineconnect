﻿using System;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Connect.Card;
using DinePlan.DineConnect.Connect.ConnectCard.Card.Dtos;
using DinePlan.DineConnect.Connect.ConnectCard.CardType.Dtos;
using DinePlan.DineConnect.Connect.ConnectCard.CardType.Exporting;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Abp.Authorization;

namespace DinePlan.DineConnect.Connect.ConnectCard.CardType
{
	public class ConnectCardTypeAppService : DineConnectAppServiceBase, IConnectCardTypeAppService
	{
		private readonly IRepository<ConnectCardType> _cardTypeRepository;
		private readonly IRepository<Connect.Card.ConnectCard> _connectCardRepository;
		private readonly IConnectCardTypeExporter _connectCardTypeExporter;
		private readonly IUnitOfWorkManager _unitOfWorkManager;

		public ConnectCardTypeAppService(
			IRepository<ConnectCardType> cardTypeRepository
			, IRepository<Connect.Card.ConnectCard> connectCardRepository
			, IConnectCardTypeExporter connectCardTypeExporter
			, IUnitOfWorkManager unitOfWorkManager
			)
		{
			_cardTypeRepository = cardTypeRepository;
			_connectCardRepository = connectCardRepository;
			_connectCardTypeExporter = connectCardTypeExporter;
			_unitOfWorkManager = unitOfWorkManager;
		}

		public async Task<PagedResultDto<ConnectCardTypeListDto>> GetCardTypes(GetConnectCardTypesInput input)
		{
			using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
			{
				var allItems = _cardTypeRepository
					.GetAll()
					.Where(x => x.IsDeleted == input.IsDeleted)
					.WhereIf(
						!input.Filter.IsNullOrEmpty(),
						p => p.Name.ToUpper().Contains(input.Filter.ToUpper()) || p.Tag.ToUpper().Contains(input.Filter.ToUpper())
					);
				var sortMenuItems = await allItems
					.OrderBy(input.Sorting)
					.PageBy(input)
					.ToListAsync();

				var allListDtos = sortMenuItems.MapTo<List<ConnectCardTypeListDto>>();

				var allItemCount = await allItems.CountAsync();

				return new PagedResultOutput<ConnectCardTypeListDto>(
					allItemCount,
					allListDtos
				);
			}
		}

		public async Task<FileDto> GetAllToExcel()
		{
			var allList = await _cardTypeRepository.GetAll().ToListAsync();
			var allListDtos = allList.MapTo<List<ConnectCardTypeListDto>>();
			return _connectCardTypeExporter.ExportToFile(allListDtos);
		}

		public async Task<GetConnectCardTypeForEditOutput> GetConnectCardTypeForEdit(int? id)
		{
			GetConnectCardTypeForEditOutput output = new GetConnectCardTypeForEditOutput();
			if (id.HasValue)
			{
				var connectCardType = await _cardTypeRepository.GetAll().FirstOrDefaultAsync(c => c.Id == id.Value);

				output.ConnectCardType = connectCardType.MapTo<ConnectCardTypeEditDto>();
				var connectCards = await _connectCardRepository.GetAll()
					.Where(t => t.ConnectCardTypeId == id.Value)
					.OrderByDescending(t => t.CreationTime)
					.ToListAsync();

				output.CardList = connectCards.MapTo<List<ConnectCardListDto>>();
				UpdateLocationAndNonLocationForEdit(output.ConnectCardType, output.LocationGroup);
			}
			return output;
		}

		[AbpAuthorize]
		public async Task<GetConnectCardTypeForEditOutput> CreateOrUpdateConnectCardType(CreateOrUpdateConnectCardTypeInput input)
		{
            int? id;

            try
            {
                input.ConnectCardType.TenantId = AbpSession.TenantId;
                ValidateForUpdateConnectCardType(input);
                if (input.ConnectCardType.Id.HasValue)
                {
                    id = await UpdateConnectCardType(input);
                }
                else
                {
                    id = await CreateConnectCardType(input);
                }
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
            return await GetConnectCardTypeForEdit(id);

        }
		[AbpAuthorize]
		public async Task DeleteConnectCardType(NullableIdInput input)
		{
			if (input.Id.HasValue)
			{
				await _cardTypeRepository.DeleteAsync(input.Id.Value);
			}
			else
			{
				throw new UserFriendlyException("No Input for Card");
			}
		}


		[AbpAuthorize]
		public async Task ActivateItem(IdInput input)
		{
            try
            {
                if (input.Id > 0)
                {
                    using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                    {
                        var item = await _cardTypeRepository.GetAsync(input.Id);
                        if (item != null)
                        {
                            item.IsDeleted = false;
                            await _cardTypeRepository.UpdateAsync(item);
                        }
                    }
                }
                else
                {
                    throw new UserFriendlyException("No Input for Card");
                }
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);

            }

			


		}

		public async Task<List<ConnectCardTypeListDto>> GetAllCardTypes()
		{
			var myTags = await _cardTypeRepository.GetAll().ToListAsync();
			return myTags.MapTo<List<ConnectCardTypeListDto>>();
		}

		private async Task<int?> UpdateConnectCardType(CreateOrUpdateConnectCardTypeInput input)
		{
			var connectCardType = await _cardTypeRepository.GetAsync(input.ConnectCardType.Id.Value);
			var dto = input.ConnectCardType;
			dto.MapTo(connectCardType);

			if (input.ConnectCardType.TenantId.HasValue)
			{
				dto.TenantId = input.ConnectCardType.TenantId.Value;
			}

			await _cardTypeRepository.UpdateAsync(connectCardType);

			return connectCardType.Id;
		}

		private async Task<int?> CreateConnectCardType(CreateOrUpdateConnectCardTypeInput input)
		{
			var dto = input.ConnectCardType.MapTo<ConnectCardType>();

			if (input.ConnectCardType.TenantId.HasValue)
			{
				dto.TenantId = input.ConnectCardType.TenantId.Value;
			}

			return await _cardTypeRepository.InsertAndGetIdAsync(dto);
		}

		private void ValidateForUpdateConnectCardType(CreateOrUpdateConnectCardTypeInput input)
		{

            if (input.ConnectCardType != null && string.IsNullOrEmpty(input.ConnectCardType.Name))
            {
                throw new UserFriendlyException(L("NameIsEmpty", input.ConnectCardType.Name));
            }
            if (input.ConnectCardType != null && input.ConnectCardType.Name.Length>29)
            {
                throw new UserFriendlyException(L("NameLengthNotMatch", input.ConnectCardType.Name));
            }
			var exists = _cardTypeRepository.GetAll().WhereIf(input.ConnectCardType != null && input.ConnectCardType.Id.HasValue, c => c.Id != input.ConnectCardType.Id).Where(a => a.Name.Equals(input.ConnectCardType.Name)).ToList();
			if (exists.Any())
            {
                if (input.ConnectCardType != null)
                    throw new UserFriendlyException(L("NameAlreadyExists", input.ConnectCardType.Name));
            }
		}
	}
}