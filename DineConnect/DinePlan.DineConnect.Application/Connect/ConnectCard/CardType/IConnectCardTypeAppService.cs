﻿using System.Collections.Generic;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.ConnectCard.CardType.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ConnectCard.CardType
{
    public interface IConnectCardTypeAppService : IApplicationService
    {
        Task<PagedResultDto<ConnectCardTypeListDto>> GetCardTypes(GetConnectCardTypesInput input);

        Task<FileDto> GetAllToExcel();

        Task<GetConnectCardTypeForEditOutput> GetConnectCardTypeForEdit(int? id);

        Task<GetConnectCardTypeForEditOutput> CreateOrUpdateConnectCardType(CreateOrUpdateConnectCardTypeInput input);

        Task DeleteConnectCardType(NullableIdInput input);


        Task ActivateItem(IdInput input);

        Task<List<ConnectCardTypeListDto>> GetAllCardTypes();
    }
}