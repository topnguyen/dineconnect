﻿using DinePlan.DineConnect.Connect.ConnectCard.CardType.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.ConnectCard.CardType.Exporting
{
    public interface IConnectCardTypeExporter
    {
        FileDto ExportToFile(List<ConnectCardTypeListDto> dtos);
    }
}