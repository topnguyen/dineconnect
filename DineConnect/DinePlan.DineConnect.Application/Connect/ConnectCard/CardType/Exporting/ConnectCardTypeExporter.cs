﻿using DinePlan.DineConnect.Connect.ConnectCard.CardType.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.ConnectCard.CardType.Exporting
{
    public class ConnectCardTypeExporter : FileExporterBase, IConnectCardTypeExporter
    {
        public FileDto ExportToFile(List<ConnectCardTypeListDto> dtos)
        {
            return CreateExcelPackage(
                "ConnectCardTypeList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("ConnectCardType"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Name"),
                        L("Length"),
                        L("UseOneTime"),
                        L("Oid")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                        _ => _.Name,
                        _ => _.Length,
                        _ => _.UseOneTime,
                        _ => _.Oid
                        );

                    for (var i = 1; i <= 6; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}