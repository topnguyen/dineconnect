﻿using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Card;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.Member.Dtos.DinePlan.DineConnect.Engage.Dtos;
using DinePlan.DineConnect.MultiTenancy;
using DinePlan.DineConnect.MultiTenancy.Dto;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.ConnectCard.Card.Dtos
{
    public class GetConnectCardsInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public int? ConnectCardTypeId { get; set; }

        public bool? Redeemed { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
    }
    public class ImportConnectCardsInBackgroundInputDto
    {
        public ImportConnectCardsInBackgroundInputDto()
        {
            ImportCardDataDetail = new ImportCardDataDetailEditDto();
        }
        public List<ConnectCardEditDto> ConnectCard { get; set; }
        public string FileName { get; set; }
        public bool Active { get; set; }
        public int TenantId { get; set; }
        public string TenantName { get; set; }
        public string JobName { get; set; }
        public ImportCardDataDetailEditDto ImportCardDataDetail { get; set; }
    }
    public class ExportConnectCardImportStatusDto
    {
        public List<ConnectCardEditDto> DuplicateExistInExcel { get; set; }
        public List<ConnectCardEditDto> DuplicateExistInDB { get; set; }
        public List<ConnectCardEditDto> ImportedCardsList { get; set; }
        public List<string> ErrorList { get; set; }
        public bool IsFailed { get; set; }
    }

    [AutoMapFrom(typeof(ImportCardDataDetail))]
    public class ImportCardDataDetailListDto
    {
        public int? Id { get; set; }
        public string JobName { get; set; }
        public string FileName { get; set; }
        public string FileLocation { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string OutputJsonDto { get; set; }
        public int TenantId { get; set; }
        public string Remarks { get; set; }
        public string TenantName { get; set; }
        public bool IsFailed { get; set; }
    }

    [AutoMapTo(typeof(ImportCardDataDetail))]
    public class ImportCardDataDetailEditDto
    {
        public int? Id { get; set; }
        public string JobName { get; set; }
        public string FileName { get; set; }
        public string FileLocation { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string OutputJsonDto { get; set; }
        public int TenantId { get; set; }
    }
    public class GetImportCardDataDetailInputDto
    {
        public ImportCardDataDetailListDto ImportCardDataDetail { get; set; }
    }
    public class GetConnectCardDataFromFileOutputDto
    {
        public List<ConnectCardEditDto> ConnectCards { get; set; }
        public MessageOutputDto MessageOutput { get; set; }
        
        // TODO: 1 import faild, 0 success
        public bool IsFailed { get; set; }
    }
}