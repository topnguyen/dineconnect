﻿using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;

namespace DinePlan.DineConnect.Connect.ConnectCard.Card.Dtos
{
    [AutoMapFrom(typeof(Connect.Card.ConnectCard))]
    public class ConnectCardListDto : CreationAuditedEntity<int?>
    {
        public virtual int ConnectCardTypeId { get; set; }

        public string ConnectCardTypeName { get; set; }

        public string CardNo { get; set; }

        public int TenantId { get; set; }

        public bool Redeemed { get; set; }
        public bool Active { get; set; }
        public bool Printed { get; set; }
        public bool IsSelected { get; set; }
        public string Remarks { get; set; }
    }
}