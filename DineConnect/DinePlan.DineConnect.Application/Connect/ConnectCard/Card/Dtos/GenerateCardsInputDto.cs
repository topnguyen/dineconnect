﻿using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.ConnectCard.Card.Dtos
{
    public class GenerateCardsInputDto : IInputDto
    {
        public virtual int ConnectCardTypeId { get; set; }
        public virtual string Prefix { get; set; }
        public virtual string Suffix { get; set; }
        public virtual int CardNumberOfDigits { get; set; }
        public virtual int CardStartingNumber { get; set; }
        public virtual int CardCount { get; set; }
        public virtual int? TenantId { get; set; }
        public virtual long? UserId { get; set; }
    }
}