﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using DinePlan.DineConnect.Connect.ConnectCard.CardType.Dtos;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.ConnectCard.Card.Dtos
{
    [AutoMapTo(typeof(Connect.Card.ConnectCard))]
    public class ConnectCardEditDto : CreationAuditedEntity<int?>
    {
        public int? Id { get; set; }
        public virtual int ConnectCardTypeId { get; set; }
        public string ConnectCardTypeName { get; set; }
        public string CardNo { get; set; }

        public int? TenantId { get; set; }

        public bool Redeemed { get; set; }
        public bool Active { get; set; }
        public string Remarks { get; set; }
    }

    public class GetCardInfo
    {
        public ConnectCardEditDto Card { get; set; }
        public ConnectCardTypeEditDto CardType { get; set; }
    }

    public class ValidateCardInput : IInputDto
    {
        public string CardNumber { get; set; }
        public int LocationId { get; set; }
        public int TenantId { get; set; }
        public int CardTypeId { get; set; }

    }

    public class ValidateCardOutput : IOutputDto
    {
        public int ConnectCardTypeId { get; set; }
        public string ErrorMessage { get; set; }

    }

    public class ApplyCardInput : IInputDto
    {
        public string CardNumber { get; set; }
        public int LocationId { get; set; }
        public int TenantId { get; set; }
        public string TicketNumber { get; set; }
        public int PromotionId { get; set; }

    }

    public class ApplyCardOutput : IOutputDto
    {
        public string ErrorMessage { get; set; }
        public int ReferenceId { get; set; }
    }
    public class ConnectCardStatusDto
    {
        public PagedResultOutput<ConnectCardEditDto> ConnectCards { get; set; }
    }
    public class ConnectCardOutputDto
    {
        public ConnectCardEditDto ConnectCard { get; set; }
    }
    public class ConnectCardInputDto:IInputDto
    {
        public List<ConnectCardDto> MultipleConnectCardId { get; set; }
    }
    public class ConnectCardDto
    {
        public int ConnectCardId { get; set; }
    }
}