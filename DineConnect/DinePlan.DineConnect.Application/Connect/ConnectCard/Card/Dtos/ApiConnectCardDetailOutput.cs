﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;

namespace DinePlan.DineConnect.Connect.ConnectCard.Card.Dtos
{
    public class ApiConnectCardDetailOutput : IOutputDto
    {
        public string Tag { get; set; }
        public string Status { get; set; }
        public bool Error{ get; set; }
        public int CardTypeId { get; set; }


    }
}
