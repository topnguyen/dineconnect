﻿using Abp.AutoMapper;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Connect.Card;
using DinePlan.DineConnect.Connect.ConnectCard.Card.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.MultiTenancy;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ConnectCard.Card.Exporting
{
    public class ConnectCardExporter : FileExporterBase, IConnectCardExporter
    {
        private readonly IRepository<ImportCardDataDetail> _importCardDataDetailRepo;
        public TenantManager TenantManager { get; set; }
        public ConnectCardExporter(
            IRepository<ImportCardDataDetail> importCardDataDetailRepo
            )
        {
            _importCardDataDetailRepo = importCardDataDetailRepo;
        }
        public FileDto ExportImportCardDataDetails(List<ImportCardDataDetailListDto> dtos)
        {
            return CreateExcelPackage(
                "ImportCardDataDetail.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("ImportCardDataDetail"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Job") + L("Name"),
                        L("FileName"),
                        L("StartDate"),
                        L("EndDate"),
                        L("Remarks")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.JobName,
                        _ => _.FileName,
                        _ => _.StartTime,
                        _ => _.EndTime,
                        _ => _.Remarks
                        );
                    var startDateTimeColumn = sheet.Column(4);
                    startDateTimeColumn.Style.Numberformat.Format = "mm-dd-yyyy hh:mm:ss";
                    var endDateTimeColumn = sheet.Column(5);
                    endDateTimeColumn.Style.Numberformat.Format = "mm-dd-yyyy hh:mm:ss";
                    for (var i = 1; i <= 10; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
        public FileDto ExportToFile(List<ConnectCardListDto> dtos)
        {
            return CreateExcelPackage(
                "ConnectCardList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("ConnectCard"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("CardNo"),
                        L("CardType"),
                        L("Redeemed"),
                        L("Active"),
                        L("CreationTime")
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.CardNo,
                        _ => _.ConnectCardTypeName,
                        _ => _.Redeemed,
                        _ => _.Active,
                        _ => _.CreationTime

                        );

                    var creationTimeColumn = sheet.Column(5);
                    creationTimeColumn.Style.Numberformat.Format = "yyyy-mm-dd hh:mm";

                    for (var i = 1; i <= 5; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
        public FileDto ExportConnectCardImportStatus(ExportConnectCardImportStatusDto dtos)
        {
            return CreateExcelPackage(
            L("ConnectCard") + ".xlsx",
            excelPackage =>
            {

                int row = 1; int col = 1;
                #region Imported Cards
                {
                    row = 1; col = 1;
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Imported"));
                    sheet.Column(col).AutoFit();
                    AddDetail(sheet, row, col++, L("CardNumber"));
                    sheet.Column(col).AutoFit();
                    AddDetail(sheet, row, col++, L("CardType"));

                    foreach (var detail in dtos.ImportedCardsList)
                    {
                        row++;
                        col = 1;
                        AddDetail(sheet, row, col++, detail.CardNo);
                        sheet.Column(col).AutoFit();
                        AddDetail(sheet, row, col++, detail.ConnectCardTypeName);
                        sheet.Column(col).AutoFit();
                    }
                }
                #endregion
                #region Duplicate In Excel
                {
                    row = 1; col = 1;
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("RecordAlreadyExistInExcel"));
                    sheet.Column(col).AutoFit();
                    AddDetail(sheet, row, col++, L("CardNumber"));
                    sheet.Column(col).AutoFit();
                    AddDetail(sheet, row, col++, L("CardType"));

                    foreach (var detail in dtos.DuplicateExistInExcel)
                    {
                        row++;
                        col = 1;
                        AddDetail(sheet, row, col++, detail.CardNo);
                        sheet.Column(col).AutoFit();
                        AddDetail(sheet, row, col++, detail.ConnectCardTypeName);
                        sheet.Column(col).AutoFit();
                    }
                }
                #endregion
                #region Duplicate In DB
                {
                    row = 1; col = 1;
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("RecordAlreadyExistsInDB"));
                    sheet.Column(col).AutoFit();
                    AddDetail(sheet, row, col++, L("CardNumber"));
                    sheet.Column(col).AutoFit();
                    AddDetail(sheet, row, col++, L("CardType"));

                    foreach (var detail in dtos.DuplicateExistInDB)
                    {
                        row++;
                        col = 1;
                        AddDetail(sheet, row, col++, detail.CardNo);
                        sheet.Column(col).AutoFit();
                        AddDetail(sheet, row, col++, detail.ConnectCardTypeName);
                        sheet.Column(col).AutoFit();
                    }
                }

                #endregion
                #region ErrorList
                if (dtos.ErrorList != null && dtos.ErrorList.Count > 0)
                {
                    row = 1; col = 1;
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Error"));
                    sheet.Column(col).AutoFit();
                    AddDetail(sheet, row, col++, L("ErrorInfo"));

                    foreach (var detail in dtos.ErrorList)
                    {
                        row++;
                        col = 1;
                        AddDetail(sheet, row, col++, detail);
                        sheet.Column(col).AutoFit();
                    }
                }

                #endregion
            });
        }
    }
}