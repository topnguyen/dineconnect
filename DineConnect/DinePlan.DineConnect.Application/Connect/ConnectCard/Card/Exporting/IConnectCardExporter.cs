﻿using DinePlan.DineConnect.Connect.ConnectCard.Card.Dtos;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ConnectCard.Card.Exporting
{
    public interface IConnectCardExporter
    {
        FileDto ExportImportCardDataDetails(List<ImportCardDataDetailListDto> dtos);
        FileDto ExportToFile(List<ConnectCardListDto> dtos);
        FileDto ExportConnectCardImportStatus(ExportConnectCardImportStatusDto dtos);
    }
}