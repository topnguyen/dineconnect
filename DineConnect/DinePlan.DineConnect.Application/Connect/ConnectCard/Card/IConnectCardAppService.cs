﻿using System.Collections.Generic;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.ConnectCard.Card.Dtos;
using DinePlan.DineConnect.Connect.ConnectCard.CardType.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Master.Dtos;

namespace DinePlan.DineConnect.Connect.ConnectCard.Card
{
    public interface IConnectCardAppService : IApplicationService
    {
        Task GenerateConnectCardsInBackground(GenerateCardsInputDto input);

        Task GenerateConnectCards(GenerateCardsInputDto input);

        Task<PagedResultOutput<ConnectCardListDto>> GetAllConnectCard(GetConnectCardsInput input);

        Task<FileDto> GetAllToExcel();

        Task<List<ConnectCardTypeListDto>> GetConnectCardTypes();

        Task<ConnectCardEditDto> GetConnectCardForEdit(NullableIdInput input);

        Task<ConnectCardEditDto> CreateOrUpdateConnectCard(ConnectCardEditDto input);

        Task DeleteConnectCard(NullableIdInput input);

        Task<ValidateCardOutput> ApiValidateCard(ValidateCardInput input);
        Task<ValidateCardOutput> ApiActivateCard(ValidateCardInput input);

        Task<ApplyCardOutput> ApiApplyCard(ApplyCardInput input);
        Task<FileDto> ExportImportCardDataDetail(GetConnectCardsInput input);
        Task<FileDto> CanViewImportCardDataDetail(GetImportCardDataDetailInputDto input);

        Task<ApiConnectCardDetailOutput> ApiConnectCardDetail(ApiLocationInput connectCardListDtos);

        Task UpdateConnectCardInfo(ConnectCardOutputDto input);
        Task DeleteMultiCards(ConnectCardInputDto input);
        Task ImportConnectCards(ImportConnectCardsInBackgroundInputDto input);
        Task<GetConnectCardDataFromFileOutputDto> ImportConnectCardInBackGround(ImportConnectCardsInBackgroundInputDto input);
        Task<List<ImportCardDataDetailListDto>> GetImportCardDataDetail(GetConnectCardsInput input);
        Task<GetCardInfo> ApiGetConnectCardByType(ValidateCardInput input);
    }
}