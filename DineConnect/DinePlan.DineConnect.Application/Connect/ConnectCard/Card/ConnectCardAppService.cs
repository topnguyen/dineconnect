﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Common;
using DinePlan.DineConnect.Connect.Card;
using DinePlan.DineConnect.Connect.ConnectCard.Card.Dtos;
using DinePlan.DineConnect.Connect.ConnectCard.Card.Exporting;
using DinePlan.DineConnect.Connect.ConnectCard.CardType.Dtos;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.Member.Dtos.DinePlan.DineConnect.Engage.Dtos;
using DinePlan.DineConnect.Job.Connect;
using DinePlan.DineConnect.Utility;
using FluentValidation;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;

namespace DinePlan.DineConnect.Connect.ConnectCard.Card
{
    public class ConnectCardAppService : DineConnectAppServiceBase, IConnectCardAppService
    {
        private readonly IBackgroundJobManager _backgroundJobManager;
        private readonly IConnectCardExporter _connectCardExporter;
        private readonly IRepository<ConnectCardRedemption> _connectCardRedeemRepository;
        private readonly IRepository<Connect.Card.ConnectCard> _connectCardRepository;
        private readonly IRepository<ConnectCardType> _connectCardTypeRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IBackgroundJobManager _bgm;
        private readonly IRepository<ImportCardDataDetail> _importCardDataDetailRepository;

        public ConnectCardAppService(
            IRepository<ConnectCardType> connectCardTypeRepository
            , IRepository<Connect.Card.ConnectCard> connectCardRepository
            , IRepository<ConnectCardRedemption> connectCardRedeemRepository
            , IConnectCardExporter connectCardExporter
            , IBackgroundJobManager backgroundJobManager
            , IUnitOfWorkManager unitOfWorkManager,
            IBackgroundJobManager bgm,
            IRepository<ImportCardDataDetail> importCardDataDetailRepository)
        {
            _connectCardTypeRepository = connectCardTypeRepository;
            _connectCardRepository = connectCardRepository;
            _connectCardRedeemRepository = connectCardRedeemRepository;
            _connectCardExporter = connectCardExporter;
            _backgroundJobManager = backgroundJobManager;
            _unitOfWorkManager = unitOfWorkManager;
            _bgm = bgm;
            _importCardDataDetailRepository = importCardDataDetailRepository;
        }

        public async Task<PagedResultOutput<ConnectCardListDto>> GetAllConnectCard(GetConnectCardsInput input)
        {
            if (!input.Filter.IsNullOrEmpty()) input.Filter = Regex.Replace(input.Filter.Trim(), @"\s+", " ");

            var allItems = _connectCardRepository
                .GetAll()
                .Where(c => c.TenantId == AbpSession.TenantId)
                .WhereIf(input.Redeemed.HasValue, p => p.Redeemed == input.Redeemed)
                .WhereIf(!input.Filter.IsNullOrEmpty(),
                    p => p.CardNo.Contains(input.Filter) || p.ConnectCardType.Name.Contains(input.Filter))
                .WhereIf(input.ConnectCardTypeId.HasValue, p => p.ConnectCardTypeId == input.ConnectCardTypeId);

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<ConnectCardListDto>>();
            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<ConnectCardListDto>(allItemCount, allListDtos);
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _connectCardRepository.GetAllListAsync(c => c.TenantId == AbpSession.TenantId);
            var allListDtos = allList.MapTo<List<ConnectCardListDto>>();
            return _connectCardExporter.ExportToFile(allListDtos);
        }

        public async Task<List<ConnectCardTypeListDto>> GetConnectCardTypes()
        {
            var lst = await _connectCardTypeRepository.GetAllListAsync();

            return lst
                .MapTo<List<ConnectCardTypeListDto>>();
        }

        public async Task<ConnectCardEditDto> GetConnectCardForEdit(NullableIdInput input)
        {
            var editDto = new ConnectCardEditDto();

            if (input.Id.HasValue)
            {
                var hDto = await _connectCardRepository.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<ConnectCardEditDto>();
            }
            else
            {
                editDto = new ConnectCardEditDto();
            }

            return editDto;
        }

        public async Task<ConnectCardEditDto> CreateOrUpdateConnectCard(ConnectCardEditDto input)
        {
            var cardType = await _connectCardTypeRepository.FirstOrDefaultAsync(t => t.Id == input.ConnectCardTypeId);

            input.Redeemed = cardType.UseOneTime ? input.Redeemed : false;
            if (input.Redeemed) input.Active = false;
            if (input.TenantId != null)
            {
                input.TenantId = AbpSession.TenantId;
            }
            else
            {
                input.TenantId = cardType.TenantId;
            }

            if (input.Id.HasValue)
                return await UpdateConnectCard(input);
            return await CreateConnectCard(input);
        }

        public async Task GenerateConnectCards(GenerateCardsInputDto input)
        {
            input.TenantId = AbpSession.TenantId;
            input.UserId = AbpSession.UserId;
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.MustHaveTenant, AppConsts.ConnectFilter))
            {
                var ct = await _connectCardTypeRepository.FirstOrDefaultAsync(t =>
                    t.Id == input.ConnectCardTypeId && t.TenantId == input.TenantId);
                if (ct == null) throw new UserFriendlyException(L("CardTypeIdNotExist"));

                if (input.CardNumberOfDigits + (input.Prefix.IsNullOrEmpty() ? 0 : input.Prefix.Length) +
                    (input.Suffix.IsNullOrEmpty() ? 0 : input.Suffix.Length) > ct.Length)
                    throw new UserFriendlyException(L("CardDigitsShouldNotBeGreaterThanNDigits", ct.Length));
            }

            await _backgroundJobManager.EnqueueAsync<GenerateConnectCardJob, GenerateCardsInputDto>(input);
        }

        [UnitOfWork]
        public async Task GenerateConnectCardsInBackground(GenerateCardsInputDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.MustHaveTenant, AppConsts.ConnectFilter))
            {
                var ct = await _connectCardTypeRepository.FirstOrDefaultAsync(t =>
                    t.Id == input.ConnectCardTypeId && t.TenantId == input.TenantId);
                if (ct == null) throw new UserFriendlyException(L("CardTypeIdNotExist"));
                for (var i = 0; i < input.CardCount; i++)
                {
                    var myStr = input.CardStartingNumber + i;
                    var cardNumber = myStr.ToString().Trim();
                    var digitsDiff = input.CardNumberOfDigits - cardNumber.Trim().Length;

                    if (digitsDiff < 0)
                        throw new UserFriendlyException(L("CardDigitsLessThanCardNumberLength", cardNumber,
                            cardNumber.Trim().Length, input.CardNumberOfDigits));

                    cardNumber = cardNumber.Trim().PadLeft(input.CardNumberOfDigits, '0');

                    var cardNumberInSpecifiedDigits = $"{input.Prefix}{cardNumber}{input.Suffix}";

                    var connectCard = new Connect.Card.ConnectCard
                    {
                        ConnectCardTypeId = input.ConnectCardTypeId,
                        CardNo = cardNumberInSpecifiedDigits,
                        Redeemed = false,
                        TenantId = ct.TenantId,
                        CreatorUserId = input.UserId
                    };

                    CheckErrors(await ValidateForCreateSync(connectCard));
                }
            }
        }

        public async Task DeleteConnectCard(NullableIdInput input)
        {
            if (input.Id.HasValue)
                await _connectCardRepository.DeleteAsync(input.Id.Value);
        }

        public async Task<ValidateCardOutput> ApiValidateCard(ValidateCardInput input)
        {
            var validator = new ApiConnectCardValidateValidator();
            var result = validator.Validate(input);

            if (result.IsValid)
            {
                return await ValidateInputOfCard(input);
            }

            return new ValidateCardOutput
            {
                ErrorMessage = FluentValidatoryExtension.ValidationResult(result.Errors)
            };

        }

        public async Task<ValidateCardOutput> ApiActivateCard(ValidateCardInput input)
        {
            var validator = new ApiConnectCardValidateValidator();
            var result = validator.Validate(input);

            if (result.IsValid)
            {
                var myOutput = await ValidateInputOfCard(input);
                if (string.IsNullOrEmpty(myOutput.ErrorMessage))
                {
                    var myCards =
                        _connectCardRepository.GetAll().Where(a => a.CardNo.Equals(input.CardNumber)
                                                                   && a.TenantId == input.TenantId &&
                                                                   a.ConnectCardTypeId == input.CardTypeId);

                    if (myCards.Any())
                    {
                        var myLast = myCards.ToList().LastOrDefault();
                        if (myLast != null && !myLast.Active)
                        {
                            myLast.Active = true;
                            await _connectCardRepository.UpdateAsync(myLast);
                        }
                        else
                        {
                            myOutput.ErrorMessage = "Card is already activated";
                        }
                    }
                }

                return myOutput;
            }

            return new ValidateCardOutput
            {
                ErrorMessage = FluentValidatoryExtension.ValidationResult(result.Errors)
            };
        }

        public async Task<ApplyCardOutput> ApiApplyCard(ApplyCardInput input)
        {
            var output = new ApplyCardOutput();

            if (input != null && !string.IsNullOrEmpty(input.CardNumber))
            {
                var myCards =
                    _connectCardRepository.GetAll().Where(a => a.CardNo.Equals(input.CardNumber)
                                                               && a.TenantId == input.TenantId);

                if (myCards.Any())
                {
                    var myLast = myCards.LastOrDefault();
                    if (!myLast.Redeemed && myLast.Active)
                    {
                        var addRedeem = false;

                        if (myLast.ConnectCardType != null && myLast.ConnectCardType.ValidTill < DateTime.Now)
                        {
                            addRedeem = false;
                            output.ErrorMessage = "Card is expired";
                        }

                        if (myLast.ConnectCardType != null && myLast.ConnectCardType.UseOneTime)
                        {
                            myLast.Redeemed = true;
                            addRedeem = true;
                        }
                        else
                        {
                            var allRedeemAva =
                                _connectCardRedeemRepository.GetAll().Where(a =>
                                    a.ConnectCardId == myLast.Id && a.TicketNumber.Equals(input.TicketNumber));

                            if (allRedeemAva.Any())
                                output.ErrorMessage = "Already Redeemed";
                            else
                                addRedeem = true;
                        }

                        if (addRedeem)
                        {
                            var myId = await _connectCardRedeemRepository.InsertAndGetIdAsync(new ConnectCardRedemption
                            {
                                ConnectCardId = myLast.Id,
                                LocationId = input.LocationId,
                                RedemptionDate = DateTime.Now,
                                RedemptionReferences = input.PromotionId.ToString()
                            });
                        }
                    }
                    else
                    {
                        output.ErrorMessage = "Card has been redeemed";
                    }
                }
                else
                {
                    output.ErrorMessage = "No Card Available";
                }
            }
            else
            {
                output.ErrorMessage = "Empty Card Number";
            }

            return output;
        }

        public async Task<ApiConnectCardDetailOutput> ApiConnectCardDetail(ApiLocationInput input)
        {
            var validator = new ApiConnectCardDetailValidator();
            var result = validator.Validate(input);

            if (result.IsValid)
            {
                var myCards = await _connectCardRepository.GetAll().Where(a =>
                        a.Active && !a.Redeemed && a.CardNo.Equals(input.Tag) && a.TenantId == input.TenantId)
                    .ToListAsync();
                if (myCards.Any())
                {
                    var myLastCard = myCards.LastOrDefault();

                    if (!myLastCard.Active)
                        return new ApiConnectCardDetailOutput
                        {
                            Error = true,
                            Status = "Voucher is not Active : " + input.Tag
                        };

                    if (myLastCard.Redeemed)
                        return new ApiConnectCardDetailOutput
                        {
                            Error = true,
                            Status = "Voucher is not Redeemed : " + input.Tag
                        };

                    var myCardCategory = myLastCard.ConnectCardType;

                    if (myCardCategory == null)
                    {
                        var myCardTypes = _connectCardTypeRepository.GetAll()
                            .Where(a => a.Id == myLastCard.ConnectCardTypeId && a.TenantId == input.TenantId).ToList();

                        if (!myCardTypes.Any())
                            return new ApiConnectCardDetailOutput
                            {
                                Error = true,
                                Status = "Card Category is deleted or not Available : " + input.Tag
                            };
                    }


                    if (myCardCategory != null)
                    {
                        if (myCardCategory.ValidTill < DateTime.Now)
                            return new ApiConnectCardDetailOutput
                            {
                                Error = true,
                                Status = "Card Type is expired"
                            };
                        return new ApiConnectCardDetailOutput
                        {
                            Tag = myCardCategory.Name,
                            Error = false,
                            Status = "",
                            CardTypeId = myLastCard.ConnectCardTypeId
                        };
                    }
                }
                else
                {
                    return new ApiConnectCardDetailOutput
                    {
                        Error = true,
                        Status = "Not found Voucher No : " + input.Tag
                    };
                }
            }

            return new ApiConnectCardDetailOutput
            {
                Error = true,
                Status = FluentValidatoryExtension.ValidationResult(result.Errors)
            };
        }

        public async Task<GetCardInfo> ApiGetConnectCardByType(ValidateCardInput input)
        {
            var output = new GetCardInfo();
            try
            {
                if (input != null && input.CardTypeId > 0)
                {
                    var cardType = await _connectCardTypeRepository.GetAsync(input.CardTypeId);
                    var card = _connectCardRepository.GetAll()
                                .FirstOrDefault(a => a.ConnectCardTypeId == input.CardTypeId && !a.Printed
                                                && a.TenantId == input.TenantId && a.Active && !a.Redeemed);

                    if (card == null) return output;

                    output.CardType = cardType.MapTo<ConnectCardTypeEditDto>();
                    output.Card = new ConnectCardEditDto
                    {
                        Active = card.Active,
                        CardNo = card.CardNo,
                        ConnectCardTypeId = card.ConnectCardTypeId,
                        Id = card.Id,
                        Redeemed = card.Redeemed,
                        TenantId = card.TenantId
                    };

                    card.Printed = true;
                    _connectCardRepository.UpdateAsync(card);
                }
            }
            catch
            {
            }
            return output;
        }
        public async Task UpdateConnectCardInfo(ConnectCardOutputDto input)
        {
            var dto = input.ConnectCard;
            var item = await _connectCardRepository.FirstOrDefaultAsync(t => t.Id == input.ConnectCard.Id);
            dto.MapTo(item);
            await _connectCardRepository.UpdateAsync(item);
        }

        public async Task DeleteMultiCards(ConnectCardInputDto input)
        {
            if (input.MultipleConnectCardId.Count > 0)
            {
                var connectCards = await _connectCardRepository.GetAllListAsync();
                foreach (var lst in input.MultipleConnectCardId)
                {
                    var rsconnectCards = connectCards.FirstOrDefault(t => t.Id == lst.ConnectCardId);
                    if (rsconnectCards != null)
                        if (!rsconnectCards.Redeemed)
                            await _connectCardRepository.DeleteAsync(lst.ConnectCardId);
                }
            }
        }


        private async Task<ValidateCardOutput> ValidateInputOfCard(ValidateCardInput input)
        {
            var output = new ValidateCardOutput();

            if (input != null && !string.IsNullOrEmpty(input.CardNumber))
            {
                var myCards =
                    _connectCardRepository.GetAll().Where(a => a.CardNo.Equals(input.CardNumber)
                                                               && a.TenantId == input.TenantId &&
                                                               a.ConnectCardTypeId == input.CardTypeId);

                if (myCards.Any())
                {
                    var myLast = myCards.ToList().LastOrDefault();

                    if (myLast != null)
                    {
                        if (myLast.Redeemed)
                        {
                            output.ErrorMessage = "Card has been redeemed";
                            return output;
                        }

                        var myCardTypes = _connectCardTypeRepository.GetAll().Where(a =>
                            a.Id.Equals(input.CardTypeId) && a.TenantId == input.TenantId);

                        if (myCardTypes.Any())
                        {
                            var myLastType = myCardTypes.ToList().LastOrDefault();

                            if (myLastType != null)
                            {
                                if (myLastType.ValidTill < DateTime.Now)
                                {
                                    output.ErrorMessage = "Card Type has Expired";
                                    return output;
                                }
                            }
                            else
                            {
                                output.ErrorMessage = "No Card Type Available";
                                return output;

                            }
                        }
                        else
                        {
                            output.ErrorMessage = "No Card Type Available";
                            return output;

                        }

                        output.ErrorMessage = "";
                        output.ConnectCardTypeId = input.CardTypeId;
                    }
                    else
                    {
                        output.ErrorMessage = "No Card Available";
                        return output;

                    }
                }
                else
                {
                    output.ErrorMessage = "No Card Available";
                    return output;
                }
            }

            return output;
        }

        private async Task<IdentityResult> ValidateForCreateSync(Connect.Card.ConnectCard connectCard)
        {
            var cardType =
                await _connectCardTypeRepository.FirstOrDefaultAsync(t => t.Id == connectCard.ConnectCardTypeId);

            if (connectCard.CardNo.Length > cardType.Length)
                throw new UserFriendlyException(L("CardDigitsLessThanCardNumberLength", connectCard.CardNo,
                    connectCard.CardNo.Length, cardType.Length));

            var exists = _connectCardRepository.GetAll()
                .Where(c => c.TenantId == AbpSession.TenantId)
                .Where(a => a.CardNo.ToUpper().Equals(connectCard.CardNo.ToUpper()))
                .WhereIf(connectCard.Id != 0, a => a.Id != connectCard.Id);

            if (exists.Any()) throw new UserFriendlyException(L("CardNumberAlreadyExists", connectCard.CardNo));
            if (connectCard.Id == 0) await _connectCardRepository.InsertOrUpdateAsync(connectCard);
            return IdentityResult.Success;
        }

        private async Task<ConnectCardEditDto> CreateConnectCard(ConnectCardEditDto input)
        {
            var dto = input.MapTo<Connect.Card.ConnectCard>();

            CheckErrors(await ValidateForCreateSync(dto));
            return dto.MapTo<ConnectCardEditDto>();
        }

        private async Task<ConnectCardEditDto> UpdateConnectCard(ConnectCardEditDto input)
        {
            var item = await _connectCardRepository.GetAsync(input.Id.Value);

            input.MapTo(item);

            CheckErrors(await ValidateForCreateSync(item));

            return item.MapTo<ConnectCardEditDto>();
        }
        public async Task ImportConnectCards(ImportConnectCardsInBackgroundInputDto input)
        {
            #region Insert ImportCard Data Detail
            DateTime? startDateTime = DateTime.Now;
            var importedfileName = Path.GetFileName(input.FileName);
            var currentPath = Path.GetFullPath(input.FileName);
            currentPath = Directory.GetParent(currentPath).FullName;
            var importedfilePath = currentPath;

            ImportCardDataDetailEditDto importCardDataDetailDto = new ImportCardDataDetailEditDto
            {
                JobName = input.JobName,
                FileName = importedfileName,
                FileLocation = importedfilePath,
                StartTime = startDateTime,
                EndTime = null,
                OutputJsonDto = null,
                TenantId = input.TenantId
            };
            input.ImportCardDataDetail = importCardDataDetailDto;

            var dto = input.ImportCardDataDetail.MapTo<ImportCardDataDetail>();
            var retId = await _importCardDataDetailRepository.InsertAndGetIdAsync(dto);
            input.ImportCardDataDetail.Id = retId;
            #endregion
            await _bgm.EnqueueAsync<ImportConnectCardJob, ImportConnectCardJobArgs>(new ImportConnectCardJobArgs
            {
                ConnectCard = input.ConnectCard,
                FileName = input.FileName,
                Active = input.Active,
                TenantId = input.TenantId,
                TenantName = input.TenantName,
                JobName = input.JobName,
                ImportCardDataDetail = input.ImportCardDataDetail
            });
        }
        public async Task<GetConnectCardDataFromFileOutputDto> ImportConnectCardInBackGround(ImportConnectCardsInBackgroundInputDto input)
        {

            var duplicateExistsInDB = new List<ConnectCardEditDto>();
            var duplicateExistsInExcel = new List<ConnectCardEditDto>();
            var importedCardsList = new List<ConnectCardEditDto>();

            FileInfo localfileName = new FileInfo(input.FileName);
            var data = new ExcelData(input.FileName, localfileName.OpenRead());
            var dtos = await GetConnectCardDataFromFile(data, input.Active);
            var allItems = dtos.ConnectCards.MapTo<List<ConnectCardEditDto>>();

            var rsconnectcard = await _connectCardRepository.GetAllListAsync();
            foreach (var lst in allItems)
            {
                var alreadyExists = rsconnectcard.FirstOrDefault(t => t.CardNo == lst.CardNo);
                if (alreadyExists != null)
                {
                    var exists = duplicateExistsInDB.Any(t => t.CardNo == lst.CardNo);
                    if (!exists)
                    {
                        lst.Remarks = L("RecordAlreadyExistInDb");
                        duplicateExistsInDB.Add(lst);
                    }
                }
            }

            var duplicates = allItems.GroupBy(i => i.CardNo).Where(g => g.Count() > 1).ToList();
            if (duplicates.Count > 0)
            {
                foreach (var lst in duplicates)
                {
                    var firstRec = lst.LastOrDefault();
                    firstRec.Remarks = L("RecordAlreadyExistInExcel");
                    duplicateExistsInExcel.Add(firstRec);
                }
            }
            var connectCard = new PagedResultOutput<ConnectCardEditDto>()
            {
                Items = duplicateExistsInExcel,
                TotalCount = duplicateExistsInExcel.Count
            };
            foreach (var existRec in connectCard.Items)
            {
                allItems.Remove(existRec);
            }
            foreach (var lst in allItems)
            {
                if (lst.Id == null)
                {
                    if (lst.ConnectCardTypeId > 0)
                    {
                        var result = await CreateOrUpdateConnectCard(lst);
                        importedCardsList.Add(lst);
                    }

                }
            }
            ExportConnectCardImportStatusDto exportConnectCardImportStatusDto = new ExportConnectCardImportStatusDto
            {
                DuplicateExistInExcel = duplicateExistsInExcel,
                DuplicateExistInDB = duplicateExistsInDB,
                ImportedCardsList = importedCardsList,
                ErrorList = dtos.MessageOutput.ErrorMessageList,
                IsFailed = dtos.IsFailed
            };

            var objectJson = JsonConvert.SerializeObject(exportConnectCardImportStatusDto);
            input.ImportCardDataDetail.OutputJsonDto = objectJson;
            DateTime? endDateTime = DateTime.Now;
            input.ImportCardDataDetail.EndTime = endDateTime;

            var dto = input.ImportCardDataDetail.MapTo<ImportCardDataDetail>();
            await _importCardDataDetailRepository.UpdateAsync(dto);
            return dtos;

        }
        public async Task<GetConnectCardDataFromFileOutputDto> GetConnectCardDataFromFile(ExcelData data, bool active)
        {
            string errorMessage = "";
            MessageOutputDto messageOutput = new MessageOutputDto();
            List<string> errorList = new List<string>();
            var currentRow = 1;
            var failed = false;
            var dtos = new List<ConnectCardEditDto>();
			try
			{
                var lstRecord = data.GetData();
                foreach (var row in lstRecord)
                {
                    currentRow++;

                    if (row["CardNumber"].ToString().Length == 0)
                    {
                        errorMessage = L("ErrRow", currentRow) + L("CardNumberIsNull");
                        errorList.Add(errorMessage);
                        continue;
                    }

                    var cardNumber = row["CardNumber"]?.ToString().Trim() ?? "";
                    if (cardNumber.IsNullOrEmpty())
                    {
                        errorMessage = L("ErrRow", currentRow) + L("CardNumber");
                        errorList.Add(errorMessage);
                        continue;
                    }

                    string connectCardTypeName = row["CardType"]?.ToString() ?? "";
                    connectCardTypeName = connectCardTypeName.Trim().ToUpper();
                    if (connectCardTypeName.Length == 0)
                    {
                        errorMessage = L("ErrRow", currentRow) + L("CardType") + " : " + connectCardTypeName + " " + L("NotExists");
                        errorList.Add(errorMessage);
                        continue;
                    }

                    int? id = null, cardTypeId = null; bool redeemedValue = false;
                    var exists = await _connectCardTypeRepository.FirstOrDefaultAsync(t => t.Name == connectCardTypeName);
                    if (exists != null)
                    {
                        cardTypeId = exists.Id;
                        connectCardTypeName = exists.Name;
                        if (exists.UseOneTime == true)
                        {
                            redeemedValue = false;
                        }
                    }
                    else
                    {
                        cardTypeId = null;
                    }

                    if (cardTypeId == null)
                    {
                        errorMessage = L("ErrRow", currentRow) + L("CardType") + " : " + connectCardTypeName + " " + L("NotExists");
                        errorList.Add(errorMessage);
                        continue;
                    }
                    if (exists != null && exists.Length < cardNumber.Length)
                    {
                        errorMessage = L("ErrRow", currentRow) + L("CardDigitsLessThanCardNumberLength", cardNumber, cardNumber.Length, exists.Length);
                        errorList.Add(errorMessage);
                        continue;
                    }

                    var alreadyExists = await _connectCardRepository.FirstOrDefaultAsync(t => t.CardNo == cardNumber);
                    if (alreadyExists != null)
                    {
                        id = alreadyExists.Id;
                    }

                    var connectCard = new ConnectCardEditDto
                    {
                        Id = id,
                        ConnectCardTypeId = cardTypeId != null ? cardTypeId.Value : 0,
                        ConnectCardTypeName = connectCardTypeName,
                        CardNo = cardNumber,
                        Redeemed = redeemedValue,
                        Active = active
                    };
                    dtos.Add(connectCard);
                }
            } catch(Exception ex)
			{
                failed = true;
            }
               
            messageOutput.ErrorMessageList = errorList;
            return new GetConnectCardDataFromFileOutputDto { ConnectCards = dtos, MessageOutput = messageOutput , IsFailed = failed };
        }

        public async Task<List<ImportCardDataDetailListDto>> GetImportCardDataDetail(GetConnectCardsInput input)
        {
            var allItems = await _importCardDataDetailRepository.GetAllListAsync();
            var sortMenuItems = allItems
                .OrderBy(input.Sorting);
            var allListDtos = sortMenuItems.MapTo<List<ImportCardDataDetailListDto>>();
            foreach (var lst in allListDtos)
            {
                string remarks = " ";
                if (lst.OutputJsonDto != null && lst.EndTime.HasValue)
                {
                    var jsonOutput = JsonConvert.DeserializeObject<ExportConnectCardImportStatusDto>(lst.OutputJsonDto);
                    if (jsonOutput != null && !jsonOutput.IsFailed )
                    {
                        remarks = remarks + "Inserted Cards : " + jsonOutput.ImportedCardsList.Count + " , " + "Duplicate Exists in Excel : " + jsonOutput.DuplicateExistInExcel.Count + " , " + "Duplicate Exists in DB : " + jsonOutput.DuplicateExistInDB.Count + " , Error : " + jsonOutput.ErrorList.Count;
                        lst.IsFailed = false;
                    }
                    else
					{
                        remarks = L("Can’tImport");
                        lst.IsFailed = true;
                    }
                } 
                else
                {
                    remarks = remarks + "In Process";
                    lst.IsFailed = true;
                }

                lst.Remarks = remarks;
            }
            return allListDtos;
        }
        public async Task<FileDto> ExportImportCardDataDetail(GetConnectCardsInput input)
        {
            var allItems = await GetImportCardDataDetail(input);
            var allListDtos = allItems.MapTo<List<ImportCardDataDetailListDto>>();
            return _connectCardExporter.ExportImportCardDataDetails(allListDtos);
        }
        public async Task<FileDto> CanViewImportCardDataDetail(GetImportCardDataDetailInputDto input)
        {
            var importCard = await _importCardDataDetailRepository.FirstOrDefaultAsync(t => t.Id == input.ImportCardDataDetail.Id);
            var exportConnectCardImportStatusDto = JsonConvert.DeserializeObject<ExportConnectCardImportStatusDto>(importCard.OutputJsonDto);
            return _connectCardExporter.ExportConnectCardImportStatus(exportConnectCardImportStatusDto);
        }
    }

    public class ApiConnectCardDetailValidator : AbstractValidator<ApiLocationInput>
    {
        public ApiConnectCardDetailValidator()
        {
            RuleFor(x => x.Tag).NotEmpty();
            RuleFor(x => x.TenantId).NotEqual(0);
            RuleFor(x => x.LocationId).NotEqual(0);
        }
    }

    public class ApiConnectCardValidateValidator : AbstractValidator<ValidateCardInput>
    {
        public ApiConnectCardValidateValidator()
        {
            RuleFor(x => x.CardNumber).NotEmpty();
            RuleFor(x => x.TenantId).NotEqual(0);
            RuleFor(x => x.LocationId).NotEqual(0);
            RuleFor(x => x.CardTypeId).NotEmpty();
        }
    }
}