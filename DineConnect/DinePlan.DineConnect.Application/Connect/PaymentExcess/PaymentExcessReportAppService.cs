﻿using System;
using Abp.Application.Services.Dto;
using Abp.BackgroundJobs;
using Abp.Collections.Extensions;
using Abp.Extensions;
using DinePlan.DineConnect.Connect.PaymentExcess.Dtos;
using DinePlan.DineConnect.Connect.PaymentExcess.Exporting;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Report;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using DinePlan.DineConnect.Connect.Master;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Configuration.Tenants;

namespace DinePlan.DineConnect.Connect.PaymentExcess
{
    public class PaymentExcessReportAppService : DineConnectAppServiceBase, IPaymentExcessReportAppService
    {
        private readonly IConnectReportAppService _connectReportApp;
        private readonly IReportBackgroundAppService _rbas;
        private readonly IBackgroundJobManager _bgm;
        private readonly IPaymentExcessReportExporter _exporter;
        private readonly IRepository<PaymentType> _payRepository;
        private readonly ITenantSettingsAppService _tenantSettingsService;
        public PaymentExcessReportAppService(IConnectReportAppService connectReportApp,IRepository<PaymentType> payRepository,
            IReportBackgroundAppService rbas,
            IBackgroundJobManager bgm,
            IPaymentExcessReportExporter exporter,
             ITenantSettingsAppService tenantSettingsService)
        {
            _payRepository = payRepository;
            _connectReportApp = connectReportApp;
            _rbas = rbas;
            _bgm = bgm;
            _exporter = exporter;
            _tenantSettingsService = tenantSettingsService;
        }

        public async Task<PagedResultDto<PaymentExcessReportOutput>> GetPaymentExcessSummary(GetTicketInput input)
        {
            List<PaymentExcessReportOutput> returnOutpu = GetPaymentExcess(input);

            var finalResult = returnOutpu
               .OrderBy(input.Sorting)
               .Skip(input.SkipCount)
               .Take(input.MaxResultCount)
               .ToList();

            return new PagedResultDto<PaymentExcessReportOutput>(returnOutpu.Count(), finalResult);
        }

        private List<PaymentExcessReportOutput> GetPaymentExcess(GetTicketInput input)
        {
            var currentTickets = _connectReportApp.GetAllTicketsForTicketInputNoFilterByDynamicFilter(input)
                            .Where(a => a.TotalAmount > 0M);

            var allPayments = currentTickets
                .SelectMany(x => x.Payments)
                .GroupBy(p => p.PaymentType)
                .Select(g => new PaymentExcessReportOutput
                {
                    Id = g.Key.Id,
                    PaymentType = g.Key.Name,
                    Actual = g.Sum(a => a.Amount),
                    Tendered = g.Sum(a => a.TenderedAmount),
                    PaymentTags = g.Key.PaymentTag,
                    Payments = g.ToList()
                });


            List<PaymentExcessReportOutput> returnOutpu = new List<PaymentExcessReportOutput>();
            if (input.PaymentTags.Any())
            {
                allPayments = allPayments.Where(g => input.PaymentTags.Contains(g.PaymentTags));
            }

            foreach (var summaryDto in allPayments.ToList())
            {
                if (summaryDto.Excess > 0M)
                {
                    returnOutpu.Add(summaryDto);
                }
            }

            // filter by dynamic Filter
            var dataAsQueryable = returnOutpu.AsQueryable();
            if (!string.IsNullOrEmpty(input.DynamicFilter))
            {
                var jsonSerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver =
                        new CamelCasePropertyNamesContractResolver()
                };
                var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                if (filRule?.Rules != null)
                {
                    dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                }
            }

            returnOutpu = dataAsQueryable.ToList();
            return returnOutpu;
        }

        public async Task<FileDto> GetPaymentExcessExport(GetTicketInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                var setting = await _tenantSettingsService.GetAllSettings();
                input.DatetimeFormat = setting.Connect.DateTimeFormat;
                input.DateFormat = setting.Connect.SimpleDateFormat;

                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.PAYMENTEXCESSSUMMARY,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.PAYMENTEXCESSSUMMARY,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value,
                            TicketInput = input
                        });
                }
                else
                {
                    return await _exporter.ExportPaymentExcessSummary(input, this);
                }
            }

            return null;
        }

        public async Task<List<PaymentExcessSummaryDto>> GetPaymentExcessExportInternal(GetTicketInput input)
        {
            var currentTickets = _connectReportApp.GetAllTicketsForTicketInputNoFilterByDynamicFilter(input)
                .Where(a => a.TotalAmount > 0M);

            List<int> myPaymentTypes = new List<int>();

            if (input.PaymentTags != null && input.PaymentTags.Any())
            {
                myPaymentTypes = _payRepository.GetAll().Where(a => input.PaymentTags.Contains(a.PaymentTag)).Select(a=>a.Id).ToList();
            }

            var allTickets = currentTickets.GroupBy(p => new {p.Location});
            List<PaymentExcessSummaryDto> firstList = new List<PaymentExcessSummaryDto>();

            foreach (var myTicket in allTickets)
            {
                var allPayment = myTicket.SelectMany(a => a.Payments).Where(a => (a.TenderedAmount - a.Amount) > 0);

                if (myPaymentTypes.Any())
                {
                    allPayment = allPayment.Where(a => myPaymentTypes.Contains(a.PaymentTypeId));
                }

                if (allPayment.Any())
                {
                    firstList.Add(new PaymentExcessSummaryDto
                    {
                        Id = 1,
                        PaymentTypeName = "Excess",
                        Actual = allPayment.Sum(a => a.Amount),
                        Quantity = allPayment.Count(),
                        Tendered = allPayment.Sum(a => a.TenderedAmount),
                        PaymentTags = "",
                        LocationName = myTicket.Key.Location.Name
                    });
                }
            }

            return firstList;
        }

        public async Task<FileDto> GetPaymentExcessDetailExport(GetTicketInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                var setting = await _tenantSettingsService.GetAllSettings();
                input.DatetimeFormat = setting.Connect.DateTimeFormat;
                input.DateFormat = setting.Connect.SimpleDateFormat;

                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.PAYMENTEXCESSDETAIL,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.PAYMENTEXCESSDETAIL,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value,
                            TicketInput = input
                        });
                }
                else
                {
                    return await _exporter.ExportPaymentExcessDetail(input, this);
                }
            }

            return null;
        }

        public async Task<List<PaymentExcessDetailDto>> GetPaymentExcessDetaiInternal(GetTicketInput input)
        {
            var groupByPayment = GetPaymentExcess(input);
            var result = groupByPayment
                //.WhereIf(input.PaymentTags.Any(), g => g.PaymentTags.Any(l => input.PaymentTags.Contains(l)))
                .SelectMany(g => g.Payments
                .GroupBy(t => t.TicketId)
                    .Select(t =>
                    {
                        var dto =
                        new PaymentExcessDetailDto
                        {
                            Id = t.Key,
                            PaymentTypeName = g.PaymentType,
                            LocationName = t.First().Ticket.Location.Name,
                            TicketNumber = t.First().Ticket.TicketNumber,
                            Date = t.First().Ticket.LastPaymentTime,
                            InvoiceNo = t.First().Ticket.InvoiceNo,
                            DepartmentName = t.First().Ticket.DepartmentName
                        };
                        dto.TotalAmount = t.Sum(x => x.Amount);
                        dto.TendedAmount = t.Sum(x => x.TenderedAmount);
                        var pFrame = t.First().PaymentTags;

                        if (pFrame != null)
                        {
                            try
                            {
                                PaymentFrame frame = JsonConvert.DeserializeObject<PaymentFrame>(pFrame);
                                dto.ReferenceNumber = frame != null? frame.Description : "";
                            }
                            catch (Exception)
                            {
                                dto.ReferenceNumber = "";
                            }
                            
                        }
                        return dto;
                    }).ToList()
                ).OrderBy(input.Sorting)
                .ToList();
            return result.Where(a=>a.Excess>0M).ToList();
        }
    }
}