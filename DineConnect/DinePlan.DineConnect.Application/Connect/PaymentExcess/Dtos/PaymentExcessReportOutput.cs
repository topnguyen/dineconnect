﻿using Abp.Extensions;
using DinePlan.DineConnect.Connect.Transaction;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace DinePlan.DineConnect.Connect.PaymentExcess.Dtos
{
    public class PaymentExcessReportOutput
    {
        public int Id { get; set; }

        public string PaymentType { get; set; }

        public decimal Actual { get; set; }

        public decimal Tendered { get; set; }

        public decimal Excess => Tendered - Actual;

        public string PaymentTags { get; set; }

        public List<string> PaymentTagValues
        {
            get
            {
                if (PaymentTags == null)
                    return new List<string>();
                return PaymentTags.Split(",").ToList();
            }
        }

        [JsonIgnore]
        public List<Payment> Payments { get; set; } = new List<Payment>();
    }
}