﻿using System;

namespace DinePlan.DineConnect.Connect.PaymentExcess.Dtos
{
    public class PaymentExcessSummaryDto
    {
        public int Id { get; set; }
        public string LocationName { get; set; }

        public string PaymentTypeName { get; set; }

        public int Quantity { get; set; }
        public decimal TotalAmount => Excess;

        public decimal Actual { get; set; }
        public decimal Tendered { get; set; }
        public string PaymentTags { get; set; }
        public decimal Excess => Tendered - Actual;
    }

    public class PaymentExcessDetailDto
    {
        public int Id { get; set; }
        public string LocationName { get; set; }

        public string PaymentTypeName { get; set; }

        public DateTime Date { get; set; }
        public string TicketNumber { get; set; }
        public string InvoiceNo { get; set; }
        public string ReferenceNumber { get; set; }
        public string DepartmentName { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal TendedAmount { get; set; }

        public decimal Excess
        {
            get
            {
                return TendedAmount - TotalAmount;
            }
        }
    }
}