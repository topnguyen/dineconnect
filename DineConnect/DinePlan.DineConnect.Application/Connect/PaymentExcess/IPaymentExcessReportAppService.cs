﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.PaymentExcess.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.PaymentExcess
{
    public interface IPaymentExcessReportAppService : IApplicationService
    {
        Task<PagedResultDto<PaymentExcessReportOutput>> GetPaymentExcessSummary(GetTicketInput input);

        Task<List<PaymentExcessSummaryDto>> GetPaymentExcessExportInternal(GetTicketInput input);

        Task<FileDto> GetPaymentExcessExport(GetTicketInput input);

        Task<FileDto> GetPaymentExcessDetailExport(GetTicketInput input);

        Task<List<PaymentExcessDetailDto>> GetPaymentExcessDetaiInternal(GetTicketInput input);
    }
}