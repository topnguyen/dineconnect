﻿using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.PaymentExcess.Exporting
{
    public interface IPaymentExcessReportExporter
    {
        Task<FileDto> ExportPaymentExcessSummary(GetTicketInput input, IPaymentExcessReportAppService appService);

        Task<FileDto> ExportPaymentExcessDetail(GetTicketInput input, IPaymentExcessReportAppService appService);
    }
}