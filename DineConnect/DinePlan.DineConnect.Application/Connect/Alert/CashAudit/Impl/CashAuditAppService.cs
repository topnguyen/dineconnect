﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Castle.Core.Logging;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Common;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Alert.CashAudit.DTO;
using DinePlan.DineConnect.Connect.Alert.CashAudit.Exporting;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Report;
using FluentValidation;
using FluentValidation.Results;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Linq.Dynamic;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Configuration.Tenants;

namespace DinePlan.DineConnect.Connect.Alert.CashAudit.Impl
{
    public class CashAuditAppService : DineConnectAppServiceBase, ICashAuditAppService
    {
        private IRepository<Audit.CashAudit> _cRepo;
        private readonly ILogger _logger;
        private readonly ILocationAppService _locService;
        private readonly IRepository<Master.Location> _lRepo;
        private readonly ICashAuditReportExporter _exporter;
        private readonly IBackgroundJobManager _bgm;
        private readonly IReportBackgroundAppService _rbas;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ITenantSettingsAppService _tenantSettingsService;
     
        public CashAuditAppService(IRepository<Audit.CashAudit> cRepo,
            ILogger logger,
            ILocationAppService locService,
             IRepository<Master.Location> lRepo ,
             ICashAuditReportExporter exporter,
             IBackgroundJobManager bgm,
             IReportBackgroundAppService rbas ,
             IUnitOfWorkManager unitOfWorkManager,
             ITenantSettingsAppService tenantSettingsService
            )
        {
            _cRepo = cRepo;
            _logger = logger;
            _locService = locService;
            _lRepo = lRepo;
            _exporter = exporter;
            _bgm = bgm;
            _rbas = rbas;
            _unitOfWorkManager = unitOfWorkManager;
            _tenantSettingsService = tenantSettingsService;
        }
        public async Task<ApiCashAuditOutput> ApiPushCashAudit(ApiCashAuditInput input)
        {
            ApiCashAuditInputValidator validator = new ApiCashAuditInputValidator();
            ValidationResult result = validator.Validate(input);
            ApiCashAuditOutput output = new ApiCashAuditOutput();

            bool updatePeriod = false;
            if (result.IsValid)
            {
                if (input.LocalId > 0)
                {
                    var count =
                        await
                            _cRepo.LongCountAsync(
                                a =>
                                    a.LocalId == input.LocalId&& a.LocationId == input.LocationId);
                    updatePeriod = count > 0;
                    if (updatePeriod)
                    {
                        var period =
                            await _cRepo.FirstOrDefaultAsync(
                                a =>
                                    a.LocalId == input.LocalId && a.LocationId == input.LocationId);

                        if (period == null)
                        {
                            updatePeriod = false;
                        }
                        else
                        {
                            input.Id = period.Id;
                        }
                    }
                }
                if (updatePeriod)
                {
                    await UpdateAudit(input);
                    output.AuditId = input.Id;
                }
                else
                {
                    var pId = await CreateAudit(input);
                    output.AuditId = pId;
                }
                return output;
            }
            return new ApiCashAuditOutput
            {
                Status = true,
                Message = FluentValidatoryExtension.ValidationResult(result.Errors)

            };
        }

        private async Task UpdateAudit(ApiCashAuditInput input)
        {
            var item = await _cRepo.GetAsync(input.Id);
            item.TenantId  = input.TenantId;
            item.LocationId  = input.LocationId;
            item.LocalId  = input.LocalId;
            item.AuditTime  = input.AuditTime;
            item.User  = input.User;
            item.LoginBy  = input.LoginBy;
            item.ApprovedBy  = input.ApprovedBy;
            item.AuditValue  = input.AuditValue;
            item.ActualAmount  = input.ActualAmount;
            item.CashSales  = input.CashSales;
            item.FloatAmount  = input.FloatAmount;
            item.UniqueGuid  = input.UniqueGuid;
            item.CashIn  = input.CashIn;
            item.CashOut  = input.CashOut;
            item.Denominations  = input.Denominations;
            item.ReasonId  = input.ReasonId;
            item.Remark  = input.Remark;
            item.WorkPeriodId  = input.WorkPeriodId;
            item.TerminalName  = input.TerminalName;
            item.PettyCash = input.PettyCash;

            await _cRepo.UpdateAsync(item);
        }

        private async Task<int> CreateAudit(ApiCashAuditInput input)
        {
            try
            {
                var dto = input.MapTo<Audit.CashAudit>();
                var iResult = await _cRepo.InsertAndGetIdAsync(dto);
                return iResult;
            }
            catch (Exception exception)
            {
                _logger.Fatal("---Error in Create Audit Log--");
                _logger.Fatal("Exception ", exception);
                _logger.Fatal("Input " + input.LocationId);
                _logger.Fatal("Input " + input.LocalId);
                _logger.Fatal("---End Error in Create Audit Log--");
            }
            return 0;
        }
        public async Task<PagedResultOutput<GetCashAuditOutput>> GetCashAudits(GetCashAuditInput input)
        {
            var result = new PagedResultOutput<GetCashAuditOutput>();
            try
            {
                var cashAudits = GetCashAuditForCondition(input);
                if (input.Sorting == null) input.Sorting = "id";

                var mapdata = from ca in cashAudits
                              join l in _lRepo.GetAll()
                                  on ca.LocationId equals l.Id
                                  into ls
                              from p in ls.DefaultIfEmpty()
                              select new GetCashAuditOutput
                              {
                                  Actual = ca.ActualAmount,
                                  Amount = ca.FloatAmount + ca.PettyCash + ca.CashSales + ca.CashIn - ca.CashOut,
                                  ApprovedBy = ca.ApprovedBy,
                                  CreationTime = ca.AuditTime,
                                  POS = ca.TerminalName,
                                  DoneBy = ca.LoginBy,
                                  Reason = ca.ReasonId,
                                  ReMarks = ca.Remark,
                                  Id = ca.Id,
                                  Difference = ca.ActualAmount - (ca.FloatAmount + ca.PettyCash + ca.CashSales + ca.CashIn - ca.CashOut),
                                  LocationCode = p == null ? "" : p.Code,
                                  LocationName = p == null ? "" : p.Name
                              };

                // filter by query builder

                if (!string.IsNullOrEmpty(input.DynamicFilter))
                {
                    var jsonSerializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver =
                            new CamelCasePropertyNamesContractResolver()
                    };
                    var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                    if (filRule?.Rules != null) mapdata = mapdata.BuildQuery(filRule);
                }

                var ouput = mapdata.OrderBy(input.Sorting).PageBy(input).ToList();
                var totalCount = mapdata.Count();

                result = new PagedResultOutput<GetCashAuditOutput>
                {
                    Items = ouput,
                    TotalCount = totalCount
                };

                return await Task.FromResult(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private IQueryable<Audit.CashAudit> GetCashAuditForCondition(GetCashAuditInput input)
        {
            IQueryable<Audit.CashAudit> cashAudit;

            var correctDate = CorrectInputDate(input);
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                cashAudit = _cRepo.GetAll();
                if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
                {
                    if (correctDate)
                    {
                        cashAudit =
                            cashAudit.Where(
                                a =>
                                    a.AuditTime >=
                                    input.StartDate
                                    &&
                                    a.AuditTime <=
                                    input.EndDate);
                    }
                    else
                    {
                        var mySt = input.StartDate.Date;
                        var myEn = input.EndDate;

                        cashAudit =
                            cashAudit.Where(
                                a =>
                                    a.AuditTime >= mySt
                                    &&
                                    a.AuditTime <= myEn);
                    }
                }
            }

            if (input.Location > 0)
            {
                cashAudit = cashAudit.Where(a => a.LocationId == input.Location);
            }
            else if (input.Locations != null && input.Locations.Any())
            {
                var locations = input.Locations.Select(a => a.Id).ToList();
                cashAudit = cashAudit.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.LocationTag == true && input.LocationGroup.LocationTags.Any())
            {
                var locations = _locService.GetLocationForUserAndLocationTag(new LocationGroupDto
                {
                    LocationTags = input.LocationGroup.LocationTags,
                    LocationTag = true
                });
                cashAudit = cashAudit.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
                     && !input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                if (locations.Any()) cashAudit = cashAudit.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false
                });
                cashAudit = cashAudit.Where(a => locations.Contains(a.LocationId));
            }
            else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true
                });
                cashAudit = cashAudit.Where(a => locations.Contains(a.LocationId));
            }

            if (input.LocationGroup != null)
            {
                if (input.LocationGroup.NonLocations != null && input.LocationGroup.NonLocations.Any())
                {
                    var nonlocations = input.LocationGroup.NonLocations.Select(a => a.Id).ToList();
                    cashAudit = cashAudit.Where(a => !nonlocations.Contains(a.LocationId));
                }
            }

            else if (input.LocationGroup == null)
            {
                var locations = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = new List<SimpleLocationDto>(),
                    Group = false,
                    UserId = input.UserId
                });
                if (input.UserId > 0)
                    cashAudit = cashAudit.Where(a => locations.Contains(a.LocationId));
            }

            return cashAudit;
        }

        public async Task<FileDto> GetCashAuditToExcel(CashAuditExportInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                var setting = await _tenantSettingsService.GetAllSettings();
                input.DatetimeFormat = setting.Connect.DateTimeFormat;
                input.DateFormat = setting.Connect.SimpleDateFormat;

                input.MaxResultCount = 10000;
                input.UserId = AbpSession.UserId.Value;
                input.TenantId = AbpSession.TenantId.Value;
                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.CASHAUDIT,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });

                    if (backGroundId > 0)
                    {
                        var tInput = new CashAuditReportInput
                        {
                            CashAuditInput = input,
                        };
                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.CASHAUDIT,
                            CashAuditReportInput = tInput,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value
                        });
                    }
                }
                else
                {
                    var output = await _exporter.ExportCashAudits(input, this);
                    return output;
                }
            }

            return null;
        }
        public bool CorrectInputDate(IDateInput input)
        {
            if (input.NotCorrectDate) return true;
            var returnStatus = true;

            var operateHours = 0M;

            if (input.Locations != null && input.Locations.Any() && input.Locations.Count() == 1)
                operateHours = _lRepo.Get(input.Locations.First().Id).ExtendedBusinessHours;

            if (operateHours == 0M && input.Location > 0)
                operateHours = _lRepo.Get(input.Location).ExtendedBusinessHours;
            if (operateHours == 0M && input.LocationGroup != null && input.LocationGroup.Locations.Any() &&
                input.LocationGroup.Locations.Count() == 1)
                operateHours = _lRepo.Get(input.LocationGroup.Locations.First().Id).ExtendedBusinessHours;
            if (operateHours == 0M)
                operateHours = SettingManager.GetSettingValue<decimal>(AppSettings.ConnectSettings.OperateHours);

            if (!input.StartDate.Equals(DateTime.MinValue) && !input.EndDate.Equals(DateTime.MinValue))
            {
                if (operateHours == 0M) returnStatus = false;
                input.EndDate = input.EndDate.AddDays(1);
                input.StartDate = input.StartDate.AddHours(Convert.ToDouble(operateHours));
                input.EndDate = input.EndDate.AddHours(Convert.ToDouble(operateHours)).AddMinutes(-1);
            }
            else
            {
                returnStatus = false;
            }

            return returnStatus;
        }
    }

    public class ApiCashAuditInputValidator : AbstractValidator<ApiCashAuditInput>
    {
        public ApiCashAuditInputValidator()
        {
            RuleFor(x => x.TenantId).NotEqual(0);
            RuleFor(x => x.LocationId).NotEqual(0);

        }

    }

    
}