﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Alert.CashAudit.DTO;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Alert.CashAudit
{
    public interface ICashAuditAppService : IApplicationService
    {
        Task<ApiCashAuditOutput> ApiPushCashAudit(ApiCashAuditInput input);
        Task<PagedResultOutput<GetCashAuditOutput>> GetCashAudits(GetCashAuditInput input);
        Task<FileDto> GetCashAuditToExcel(CashAuditExportInput input);
    }
}