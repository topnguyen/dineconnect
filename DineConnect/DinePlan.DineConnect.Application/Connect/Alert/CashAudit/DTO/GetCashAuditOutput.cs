﻿using Abp.Domain.Entities.Auditing;
using System;
namespace DinePlan.DineConnect.Connect.Alert.CashAudit.DTO
{
    public class GetCashAuditOutput : AuditedEntity
    {
        public string LocationCode { get; set; }
        public string LocationName { get; set; }
        public string POS { get; set; }
        public decimal Amount { get; set; }
        public decimal Actual { get; set; }
        public decimal Difference { get; set; }
        public string Reason { get; set; }
        public string ReMarks { get; set; }
        public string ApprovedBy { get; set; }
        public string DoneBy { get; set; }
        public DateTime FormatDateCreationTime
        {
            get
            {
                return new DateTime(CreationTime.Year, CreationTime.Month, CreationTime.Day);
            }
        }

    }
}
