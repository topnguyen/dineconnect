﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace DinePlan.DineConnect.Connect.Alert.CashAudit.DTO
{

    public class ApiCashAuditOutput : IOutputDto
    {
        public int AuditId { get; set; }
        public bool Status { get; set; }
        public string Message { get; set; }


    }
    [AutoMapTo(typeof(Audit.CashAudit))]
    public class ApiCashAuditInput : IInputDto
    {
        public int Id { get; set; }
        public int TenantId { get; set; }
        public int LocationId { get; set; }
        public int LocalId { get; set; }
        public DateTime AuditTime { get; set; }
        public string User { get; set; }
        public string LoginBy { get; set; }
        public string ApprovedBy { get; set; }
        public decimal AuditValue { get; set; }
        public decimal ActualAmount { get; set; }
        public decimal CashSales { get; set; }
        public decimal FloatAmount { get; set; }
        public Guid UniqueGuid { get; set; }
        public decimal CashIn { get; set; }
        public decimal CashOut { get; set; }
        public string Denominations { get; set; }
        public string ReasonId { get; set; }
        public string Remark { get; set; }
        public int WorkPeriodId { get; set; }
        public string TerminalName { get; set; }
        public decimal PettyCash { get; set; }
    }
}
