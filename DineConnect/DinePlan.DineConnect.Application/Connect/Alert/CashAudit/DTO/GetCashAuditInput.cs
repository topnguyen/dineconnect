﻿using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter;
using OpenHtmlToPdf;
using System;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Alert.CashAudit.DTO
{
    public class GetCashAuditInput : PagedAndSortedInputDto, IFileExport, IDateInput, IBackgroundExport
    {
        public GetCashAuditInput()
        {
            Locations = new List<SimpleLocationDto>();
            LocationGroup = new LocationGroupDto();
        }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Location { get; set; }
        public List<SimpleLocationDto> Locations { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public ExportType ExportOutputType { get; set; }
        public PaperSize PaperSize { get; set; }
        public bool Portrait { get; set; }
        public long UserId { get; set; }
        public bool NotCorrectDate { get; set; }
        public bool Credit { get; set; }
        public bool Refund { get; set; }
        public string TicketNo { get; set; }
        public bool RunInBackground { get; set; }
        public string ReportDescription { get; set; }
        public string DynamicFilter { get; set; }
        public string DateFormat { get; set; }
        public string DatetimeFormat { get; set; }

    }

    public class CashAuditExportInput : GetCashAuditInput
    {
        public int TenantId { get; set; }
    }
}
