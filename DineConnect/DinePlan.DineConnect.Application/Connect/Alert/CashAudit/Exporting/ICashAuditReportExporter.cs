﻿using DinePlan.DineConnect.Connect.Alert.CashAudit.DTO;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Alert.CashAudit.Exporting
{
    public interface ICashAuditReportExporter
    {
        Task<FileDto> ExportCashAudits(CashAuditExportInput input, ICashAuditAppService appService);
    }
}