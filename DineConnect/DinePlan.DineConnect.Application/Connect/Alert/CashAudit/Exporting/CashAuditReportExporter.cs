﻿using Abp.Collections.Extensions;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Localization;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Alert.CashAudit.DTO;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Alert.CashAudit.Exporting
{
    public class CashAuditReportExporter : FileExporterBase, ICashAuditReportExporter
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly IApplicationLanguageManager _applicationLanguageManager;
        private readonly IApplicationLanguageTextManager _applicationLanguageTextManager;
        private IRepository<Master.Location> _locRepo;
        private IRepository<Master.Company> _comRepo;
        private readonly ILocationAppService _locService;
        private readonly SettingManager _settingManager;
        public CashAuditReportExporter(IRepository<Master.Location> locRepo, IRepository<Master.Company> comRepo,
             ILocationAppService locService, SettingManager settingManager, IApplicationLanguageManager applicationLanguageManager,
             IApplicationLanguageTextManager applicationLanguageTextManager, ILocalizationManager localizationManager)
        {
            _localizationManager = localizationManager;
            _applicationLanguageManager = applicationLanguageManager;
            _applicationLanguageTextManager = applicationLanguageTextManager;
            _locRepo = locRepo;
            _comRepo = comRepo;
            _locService = locService;
            _settingManager = settingManager;
        }

        async Task<string> GetLanguageTilte(int tenantId, string key)
        {
            var currentCulture = CultureInfo.GetCultureInfo(_localizationManager.CurrentLanguage.Name);
            var text = _applicationLanguageTextManager.GetStringOrNull(tenantId, "DineConnect", currentCulture, key);

            if (string.IsNullOrEmpty(text))
            {
                text = L(key);
                if(string.IsNullOrEmpty(text))
                {
                    var defaultLanguageCode = "en";
                    var df = await _applicationLanguageManager.GetDefaultLanguageOrNullAsync(tenantId);
                    if (df != null)
                    {
                        defaultLanguageCode = df.Name;
                    }
                    var defaultCulture = CultureInfo.GetCultureInfo(defaultLanguageCode);

                    text = _applicationLanguageTextManager.GetStringOrNull(tenantId, "DineConnect", defaultCulture, key);
                }
            }
            
            return text;
        }
        public async Task<FileDto> ExportCashAudits(CashAuditExportInput input, ICashAuditAppService appService)
        {
            var builder = new StringBuilder();
            builder.Append(await GetLanguageTilte(input.TenantId, "CashAudits"));
            builder.Append(L("-"));
            builder.Append(!input.StartDate.Equals(DateTime.MinValue)
                ? input.StartDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));
            builder.Append("_");
            builder.Append(!input.EndDate.Equals(DateTime.MinValue)
                ? input.EndDate.ToString(_simpleDateFormat)
                : DateTime.Now.ToString(_simpleDateFormat));

            var file = new FileDto(builder + ".xlsx",
                MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);

            if (input.ExportOutputType == 0)
            {
                using (var excelPackage = new ExcelPackage())
                {
                    await GetCashAudit(excelPackage, input, appService);
                    Save(excelPackage, file);
                }
                return ProcessFile(input, file);
            }
            else
            {
                var contentHTML = await ExportDatatableToHtml(input, appService, input);
                var result = ProcessFileHTML(file, contentHTML);
                return result;
            }
        }

        private FileDto ProcessFileHTML(FileDto file, string fileHTML)
        {
            File.WriteAllText(Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken), fileHTML);
            file.FileType = MimeTypeNames.ApplicationXhtmlXml;
            file.FileName = Path.GetFileNameWithoutExtension(file.FileName) + ".html";
            return file;
        }
        private async Task<string> ExportDatatableToHtml(GetCashAuditInput cashAuditInput, ICashAuditAppService appService,
            CashAuditExportInput input)
        {
            var businessDate = input.StartDate.ToString(input.DateFormat) + " to " + input.EndDate.ToString(input.DateFormat);
            var dtos = await appService.GetCashAudits(cashAuditInput);
            StringBuilder strHTMLBuilder = new StringBuilder();
            strHTMLBuilder.Append("<html>");
            strHTMLBuilder.Append("<head>");
            strHTMLBuilder.Append("</head>");

            strHTMLBuilder.Append(@"<style> 
             body  { 
              font:400 12px 'Tahoma';
              font-size: 12px;
              padding:10px;
            }
            table  { 
                margin-top: 10px;
                border-collapse: collapse;
                width: 100%;
                margin-bottom: 10px;
            }

            table td, table th {
                border: 1px solid #ddd;
                padding: 8px;
            }
            table th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #364150;
                color: white;
            }
            thead {
                display: table-header-group;
            }
            tfoot {
                display: table-row-group;
            }
            tr{
                page-break-inside: avoid;
            }
            .text-right{
              text-align:right;
            }

			.margin_bottom_0 {
				margin-bottom: 0 !important;
			}
            .margin_top_0 {
				margin-top: 0 !important;
			}

			.border_bottom_none {
				border-bottom: none !important;
			}

			.custom_table_date {
				margin-bottom: 0px !important;
			}

			.display_flex {
				display: flex;
			}
			.w-100 {
				width: 100%;
			}
	
			.float-right {
				float: right;
			}

			.font_weight_bold {
				font-weight: bold;
			}
            .font_14{
				font-size: 14;
             }
            .text-center{
               text-align: center;          
            }

            table tr:hover {background-color: #ddd;}
            </style>");


            strHTMLBuilder.Append("<body>");
            strHTMLBuilder.Append("<div>");

            var branchName = GetBranchName(input);
            var brand = GetBrand(input);

            // add header
            strHTMLBuilder.Append("<table class='table table-bordered custom_table_date border_bottom_none'>");
            strHTMLBuilder.Append("<tr class='font_14'>");
            strHTMLBuilder.Append($"<td colspan='12' class='border_bottom_none text-center font_weight_bold'>{await GetLanguageTilte(input.TenantId, "CashAuditsReport")}</td>");
            strHTMLBuilder.Append("</tr>");

            strHTMLBuilder.Append("<tr class='font_14'>");
            strHTMLBuilder.Append($"<td colspan='1'> {await GetLanguageTilte(input.TenantId, "Brand")}</td>");
            strHTMLBuilder.Append($"<td colspan='6'> {brand}</td> ");
            strHTMLBuilder.Append($"<td colspan='1'> { await GetLanguageTilte(input.TenantId, "BusinessDate")}</td> ");
            strHTMLBuilder.Append($"<td colspan='4'> { businessDate }</td> ");
            strHTMLBuilder.Append("</tr>");

            strHTMLBuilder.Append("<tr class=' font_14'>");
            strHTMLBuilder.Append($"<td colspan='1'> {await GetLanguageTilte(input.TenantId, "BranchName")}</td> ");
            strHTMLBuilder.Append($"<td colspan='6'> {branchName}</td> ");
            strHTMLBuilder.Append($"<td colspan='1'> { await GetLanguageTilte(input.TenantId, "PrintedOn")}</td> ");
            strHTMLBuilder.Append($"<td colspan='4'> { DateTime.Now.ToString(input.DatetimeFormat)}</td> ");
            strHTMLBuilder.Append("</tr>");

            strHTMLBuilder.Append("<tr class='font_14'>");
            strHTMLBuilder.Append("</tr>");

            strHTMLBuilder.Append("<tr class=' font_14'>");
            strHTMLBuilder.Append($"<th class=' border_bottom_none text-center font_weight_bold'>{await GetLanguageTilte(input.TenantId, "LocationCode")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{await GetLanguageTilte(input.TenantId, "Date")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{await GetLanguageTilte(input.TenantId, "Time")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{await GetLanguageTilte(input.TenantId, "Terminal")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{await GetLanguageTilte(input.TenantId, "CATotalCash_R")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{await GetLanguageTilte(input.TenantId, "Actual")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{await GetLanguageTilte(input.TenantId, "Difference")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{await GetLanguageTilte(input.TenantId, "Reason")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{await GetLanguageTilte(input.TenantId, "Remarks")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{await GetLanguageTilte(input.TenantId, "ApprovedBy")}</th>");
            strHTMLBuilder.Append($"<th class='border_bottom_none text-center font_weight_bold'>{await GetLanguageTilte(input.TenantId, "DoneBy")}</th>");
            strHTMLBuilder.Append("</tr>");

            // add content
            foreach (var item in dtos.Items)
            {
                strHTMLBuilder.Append("<tr class='font_14'>");
                strHTMLBuilder.Append($"<td> { item.LocationCode}</td> ");
                strHTMLBuilder.Append($"<td> { item.CreationTime.ToString(input.DateFormat)}</td>");
                strHTMLBuilder.Append($"<td> { item.CreationTime.ToString("HH:mm") }</td>");
                strHTMLBuilder.Append($"<td> { item.POS }</td>");
                strHTMLBuilder.Append($"<td class='text-right'> { item.Amount}</td>");
                strHTMLBuilder.Append($"<td class='text-right'> { item.Actual}</td>");
                strHTMLBuilder.Append($"<td class='text-right'> { item.Difference}</td>");
                strHTMLBuilder.Append($"<td> { item.Reason }</td>");
                strHTMLBuilder.Append($"<td> { item.ReMarks }</td>");
                strHTMLBuilder.Append($"<td> { item.ApprovedBy }</td>");
                strHTMLBuilder.Append($"<td> { item.DoneBy }</td>");
                strHTMLBuilder.Append("</tr>");
            }
            strHTMLBuilder.Append("</table>");
            strHTMLBuilder.Append("</div>");
            string Htmltext = strHTMLBuilder.ToString();
            return Htmltext;
        }


        private async Task GetCashAudit(ExcelPackage package, CashAuditExportInput input, ICashAuditAppService appService)
        {

            var sheet = package.Workbook.Worksheets.Add(await GetLanguageTilte(input.TenantId, "CashAudit"));
            sheet.OutLineApplyStyle = true;

            var headers = new List<string>
            {
                await GetLanguageTilte(input.TenantId, "LocationCode"),
                await GetLanguageTilte(input.TenantId, "Date"),
                await GetLanguageTilte(input.TenantId, "Time"),
                await GetLanguageTilte(input.TenantId, "Terminal"),
                await GetLanguageTilte(input.TenantId, "CATotalCash_R"),
                await GetLanguageTilte(input.TenantId, "Actual"),
                await GetLanguageTilte(input.TenantId, "Difference"),
                await GetLanguageTilte(input.TenantId, "Reason"),
                await GetLanguageTilte(input.TenantId, "Remarks"),
                await GetLanguageTilte(input.TenantId, "ApprovedBy"),
                await GetLanguageTilte(input.TenantId, "DoneBy"),
            };


            int myRerowCount = await AddReportTicketSyncHeader(sheet, await GetLanguageTilte(input.TenantId, "CashAuditsReport"), input);
            int rowCount = myRerowCount;
            rowCount++;


            var cellHeader = 1;
            foreach (var itemhead in headers)
            {
                sheet.Cells[rowCount, cellHeader].Value = itemhead;
                sheet.Cells[rowCount, cellHeader].Style.Font.Bold = true;
                sheet.Cells[rowCount, cellHeader].Style.Font.Size = 12;
                cellHeader++;
            }

            rowCount++;

            var cashAudits = await appService.GetCashAudits(input);

            if (cashAudits != null)
            {
                foreach (var item in cashAudits.Items)
                {
                    sheet.Cells[rowCount, 1].Value = item.LocationCode;
                    sheet.Cells[rowCount, 2].Value = item.CreationTime.ToString(input.DateFormat);
                    sheet.Cells[rowCount, 3].Value = item.CreationTime.ToString("HH:mm");
                    sheet.Cells[rowCount, 4].Value = item.POS;
                    sheet.Cells[rowCount, 5].Value = item.Amount;
                    sheet.Cells[rowCount, 6].Value = item.Actual;
                    sheet.Cells[rowCount, 7].Value = item.Difference;
                    sheet.Cells[rowCount, 8].Value = item.Reason;
                    sheet.Cells[rowCount, 9].Value = item.ReMarks;
                    sheet.Cells[rowCount, 10].Value = item.ApprovedBy;
                    sheet.Cells[rowCount, 11].Value = item.DoneBy;
                    sheet.Cells[rowCount, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet.Cells[rowCount, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    sheet.Cells[rowCount, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    rowCount++;
                }
            }
            for (int i = 1; i <= 11; i++)
            {
                sheet.Column(i).AutoFit();
            }
        }

        private async Task<int> AddReportTicketSyncHeader(ExcelWorksheet sheet, string nameOfReport, CashAuditExportInput input)
        {
            var brandName = GetBranchName(input);
            var brand = GetBrand(input);

            sheet.Cells[1, 1].Value = nameOfReport;
            sheet.Cells[1, 1, 1, 8].Merge = true;
            sheet.Cells[1, 1, 1, 16].Style.Font.Bold = true;
            sheet.Cells[1, 1, 1, 16].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            sheet.Cells[2, 1].Value = await GetLanguageTilte(input.TenantId, "Brand");
            sheet.Cells[2, 2].Value = brand;

            sheet.Cells[2, 6].Value = await GetLanguageTilte(input.TenantId, "BusinessDate");
            sheet.Cells[2, 7].Value = input.StartDate.ToString(input.DateFormat) + " to " + input.EndDate.ToString(input.DateFormat);
            sheet.Cells[3, 1].Value = await GetLanguageTilte(input.TenantId, "BranchName");
            sheet.Cells[3, 2].Value = brandName;
            sheet.Cells[3, 6].Value = await GetLanguageTilte(input.TenantId, "PrintedOn");
            sheet.Cells[3, 7].Value = DateTime.Now.ToString(input.DatetimeFormat);

            sheet.Cells[1, 1, 4, 7].Style.Font.Size = 14;

            return 6;
        }

        private string GetBranchName(CashAuditExportInput input)
        {
            var branchName = "All";
            var isAllLocation = (input.LocationGroup?.Locations?.Count() + input.LocationGroup?.Groups?.Count() + input.LocationGroup?.LocationTags?.Count()) == _locRepo.GetAll().Count();

            if (isAllLocation)
            {
                branchName = "All";
            }
            else
            {
                if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
                {
                    branchName = input.LocationGroup.Locations.Select(l => l.Name).JoinAsString(", ");
                }
                if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
                {
                    branchName = input.LocationGroup.Groups.Select(l => l.Name).JoinAsString(", ");
                }
                if (input.LocationGroup?.LocationTags != null && input.LocationGroup.LocationTags.Any())
                {
                    branchName = input.LocationGroup.LocationTags.Select(l => l.Name).JoinAsString(", ");
                }
            }

            return branchName;
        }

        private string GetBrand(CashAuditExportInput input)
        {
            var brand = "";
            var locationIds = new List<int>();
            if (input.Location > 0)
            {
                locationIds.Add(input.Location);
            }
            else if (input.Locations != null && input.Locations.Any())
            {
                var locationId = input.Locations.Select(a => a.Id).ToList();
                locationIds.AddRange(locationId);
            }
            else if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
                     && !input.LocationGroup.Locations.Any())
            {
                var locationId = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false,
                    UserId = input.UserId
                });
                locationIds.AddRange(locationId);
            }
            else if (input.LocationGroup?.Locations != null && input.LocationGroup.Locations.Any())
            {
                var locationId = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Locations = input.LocationGroup.Locations,
                    Group = false,
                    UserId = input.UserId
                });
                locationIds.AddRange(locationId);
            }
            else if (input.LocationGroup?.Groups != null && input.LocationGroup.Groups.Any())
            {
                var locationId = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                {
                    Groups = input.LocationGroup.Groups,
                    Group = true,
                    UserId = input.UserId
                });
                locationIds.AddRange(locationId);
            }

            var companyIds = _locRepo.GetAll().Where(x => locationIds.Any(a => a == x.Id) && (input.TenantId == 0 || x.TenantId == input.TenantId)).Select(x => x.CompanyRefId).ToList();
            var companyIdsDistinct = companyIds.Distinct();
            var companies = _comRepo.GetAll().Where(x => companyIds.Any(a => a == x.Id) && (input.TenantId == 0 || x.TenantId == input.TenantId)).Select(x => x.Name);
            brand = companies.JoinAsString(", ");
            return brand;
        }
    }
}
