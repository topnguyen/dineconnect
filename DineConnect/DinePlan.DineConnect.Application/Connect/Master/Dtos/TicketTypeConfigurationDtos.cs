﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Master.Dtos
{
    [AutoMapFrom(typeof(TicketTypeConfiguration))]
    public class TicketTypeConfigurationListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
    }

    [AutoMapTo(typeof(TicketTypeConfiguration))]
    public class TicketTypeConfigurationEditDto : ConnectEditDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public virtual Collection<TicketTypeEditDto> TicketTypes { get; set; }
    }

    public class GetTicketTypeConfigurationInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public bool? AcceptChange { get; set; }
        public string Location { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
        public GetTicketTypeConfigurationInput()
        {
            LocationGroup = new LocationGroupDto();
        }

        public LocationGroupDto LocationGroup { get; set; }
    }

    public class GetTicketTypeConfigurationForEditOutput : IOutputDto
    {
        public TicketTypeConfigurationEditDto TicketTypeConfiguration { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public List<ComboboxItemDto> Departments { get; set; }

        public GetTicketTypeConfigurationForEditOutput()
        {
            LocationGroup = new LocationGroupDto();
            TicketTypeConfiguration = new TicketTypeConfigurationEditDto();
        }
    }

    public class CreateTicketTypeConfigurationInput : IInputDto
    {
        [Required]
        public TicketTypeConfigurationEditDto TicketTypeConfiguration { get; set; }

        public LocationGroupDto LocationGroup { get; set; }

        public CreateTicketTypeConfigurationInput()
        {
            LocationGroup = new LocationGroupDto();
        }
    }

    public class TicketTypeFilterInputDto
    {
        public int? Id { get; set; }
        public int? FutureDataId { get; set; }
        public LocationGroupDto LocationGroupToBeFiltered { get; set; }
    }
}