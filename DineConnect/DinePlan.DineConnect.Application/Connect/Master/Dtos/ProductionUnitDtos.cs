﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using DinePlan.DineConnect.House;

namespace DinePlan.DineConnect.Connect.Master.Dtos
{
    [AutoMapFrom(typeof(ProductionUnit))]
    public class ProductionUnitListDto : FullAuditedEntityDto
    {
        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }
        public string Name { get; set; }

        public string Code { get; set; }

        public string MaterialAllowed { get; set; }

        public string IsActive { get; set; }

        public int TenantId { get; set; }

    }

    [AutoMapTo(typeof(ProductionUnit))]
    public class ProductionUnitEditDto
    {
        public int? Id { get; set; }
        public int LocationRefId { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public string MaterialAllowed { get; set; }

        public string IsActive { get; set; }

        public int TenantId { get; set; }

    }

    public class GetProductionUnitInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
    public class GetProductionUnitForEditOutput : IOutputDto
    {
        public ProductionUnitEditDto ProductionUnit { get; set; }
    }
    public class CreateOrUpdateProductionUnitInput : IInputDto
    {
        [Required]
        public ProductionUnitEditDto ProductionUnit { get; set; }
    }
}

