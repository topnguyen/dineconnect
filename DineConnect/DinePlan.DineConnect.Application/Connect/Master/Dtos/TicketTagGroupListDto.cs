﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Tag;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Filter;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Master.Dtos
{
    [AutoMapFrom(typeof(TicketTagGroup))]
    public class TicketTagGroupListDto : ConnectFullMultiTenantAuditEntity
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public string Departments { get; set; }
        public int DataType { get; set; }
        public string DisplayDepartments { get; set; }
        public string Tag { get; set; }
        public Collection<TicketTagListDto> Tags { get; set; }
    }

    [AutoMapTo(typeof(TicketTagGroup))]
    public class TicketTagGroupEditDto : ConnectEditDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }

        public string Departments { get; set; }

        public bool SaveFreeTags { get; set; }
        public bool FreeTagging { get; set; }
        public bool ForceValue { get; set; }
        public bool AskBeforeCreatingTicket { get; set; }

        public int DataType { get; set; }
        public int TenantId { get; set; }
        public int SubTagId { get; set; }
        public bool IsAlphanumeric => DataType == 0;
        public bool IsInteger => DataType == 1;
        public bool IsDecimal => DataType == 2;

        public TicketTagGroupEditDto()
        {
            Tags = new Collection<TicketTagListDto>();
        }

        public ICollection<TicketTagListDto> Tags { get; set; }
    }

    [AutoMapTo(typeof(TicketTag))]
    public class TicketTagListDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string AlternateName { get; set; }
    }

    public class GetTicketTagGroupInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }

        
        public GetTicketTagGroupInput()
        {
            LocationGroup = new LocationGroupDto();
        }

        public LocationGroupDto LocationGroup { get; set; }
    }

    public class GetTicketTagGroupForEditOutput : IOutputDto
    {
        public GetTicketTagGroupForEditOutput()
        {
            LocationGroup = new LocationGroupDto();
        }

        public TicketTagGroupEditDto Tag { get; set; }
        public List<ComboboxItemDto> Departments { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
    }

    public class CreateOrUpdateTicketTagGroupInput : IInputDto
    {
        public CreateOrUpdateTicketTagGroupInput()
        {
            LocationGroup = new LocationGroupDto();
        }

        [Required]
        public TicketTagGroupEditDto Tag { get; set; }

        public LocationGroupDto LocationGroup { get; set; }
        public List<ComboboxItemDto> Departments { get; set; }
    }
}