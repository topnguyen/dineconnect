﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;

namespace DinePlan.DineConnect.Connect.Master.Dtos
{
    [AutoMapFrom(typeof(LocationTag))]
    public class LocationTagListDto : FullAuditedEntityDto
    {
        public LocationTagListDto()
        {
            Locations = new HashSet<LocationListDto>();
        }
        public string Code { get; set; }
        public virtual string Name { get; set; }
        public virtual int LocationCount { get; set; }
        public ICollection<LocationListDto> Locations { get; set; }
    }

    [AutoMapFrom(typeof(LocationTag))]
    public class SimpleLocationTagDto
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
    }

    [AutoMapTo(typeof(LocationTag))]
    public class LocationTagEditDto
    {
        public int? Id { get; set; }
        public virtual string Name { get; set; }
        public string Code { get; set; }
    }

    public class GetLocationTagInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public int? LocationTagId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }
}