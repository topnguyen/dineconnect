﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Master.Dtos
{
    [AutoMapFrom(typeof(TransactionType))]
    public class TransactionTypeListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
    }

    [AutoMapTo(typeof(TransactionType))]
    public class TransactionTypeEditDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public bool Tax { get; set; }
        public string AccountCode { get; set; }
        public bool HideOnReport { get; set; }

    }

    public class GetTransactionTypeInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }

    public class GetTransactionTypeForEditOutput : IOutputDto
    {
        public TransactionTypeEditDto TransactionType { get; set; }
    }

    public class CreateOrUpdateTransactionTypeInput : IInputDto
    {
        [Required]
        public TransactionTypeEditDto TransactionType { get; set; }
    }

    public class ApiTransactionTypeInput
    {
        public int TenantId { get; set; }
    }

    public class ApiTransactionTypeOutput
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}