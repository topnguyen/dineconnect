﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Taxes;
using DinePlan.DineConnect.Dto;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Master.Dtos
{
    [AutoMapFrom(typeof(DinePlanTax))]
    public class DinePlanTaxListDto : FullAuditedEntityDto
    {
        public string TaxName { get; set; }
        public int? TransactionTypeId { get; set; }
        public string TransactionTypeName { get; set; }
    }

    [AutoMapTo(typeof(DinePlanTax))]
    public class DinePlanTaxEditDto
    {
        public DinePlanTaxEditDto()
        {
            DinePlanTaxLocations = new Collection<DinePlanTaxLocationEditDto>();
        }
        public int? Id { get; set; }
        public string TaxName { get; set; }
        public int? TransactionTypeId { get; set; }
        public bool FutureData { get; set; }
        public virtual Collection<DinePlanTaxLocationEditDto> DinePlanTaxLocations { get; set; }
    }

    [AutoMapFrom(typeof(DinePlanTaxLocation))]
    public class DinePlanTaxLocationListDto : FullAuditedEntityDto
    {
        public int DinePlanTaxRefId { get; set; }

        public int LocationRefId { get; set; }
        public Location Locations { get; set; }

        public decimal TaxPercentage { get; set; }
    }

    [AutoMapTo(typeof(DinePlanTaxLocation))]
    public class DinePlanTaxLocationEditDto : ConnectEditDto
    {
        public DinePlanTaxLocationEditDto()
        {
            DinePlanTaxMappings = new Collection<DinePlanTaxMappingEditDto>();
            LocationGroup = new LocationGroupDto();
        }
        public int? Id { get; set; }
        public int DinePlanTaxRefId { get; set; }
        public int LocationRefId { get; set; }
        public decimal TaxPercentage { get; set; }
        public string LocationRefName { get; set; }
        public Collection<DinePlanTaxMappingEditDto> DinePlanTaxMappings { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public int FutureDataSno { get; set; }
        public int TenantId { get; set; }
        public bool CloneFlag { get; set; }

        public List<SimpleLocationDto> LocationObjects
        {
            get
            {
                if (string.IsNullOrEmpty(Locations))
                    return new List<SimpleLocationDto>();
                return JsonConvert.DeserializeObject<List<SimpleLocationDto>>(Locations);
            }
        }
    }

    [AutoMapFrom(typeof(DinePlanTaxMapping))]
    public class DinePlanTaxMappingListDto : FullAuditedEntityDto
    {
        public int DinePlanTaxLocationId { get; set; }

        public int? DepartmentId { get; set; }

        public int? CategoryId { get; set; }

        public int? MenuItemId { get; set; }

    }

    [AutoMapTo(typeof(DinePlanTaxMapping))]
    public class DinePlanTaxMappingEditDto
    {
        public int? Id { get; set; }
        public int DinePlanTaxLocationId { get; set; }

        public int? DepartmentId { get; set; }
        public int? CategoryId { get; set; }

        public int? MenuItemId { get; set; }
        public string MenuItemGroupCode { get; set; }
        public string CategoryName { get; set; }
    }

    public class DinePlanTaxReturnDto
    {
        public int DinePlanTaxLocationRefId { get; set; }
        public int TaxRefId { get; set; }
        public string TaxName { get; set; }

        public int LocationRefId { get; set; }
        public string LocationRefName { get; set; }
        public decimal TaxPercentage { get; set; }
        public int? DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public int? CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int? MenuItemId { get; set; }
        public string MenuItemName { get; set; }
    }

    public class ApiTaxOutput
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TransactionTypeId { get; set; }
        public string TransactionTypeName { get; set; }

        public decimal Percentage { get; set; }

        public int? DepartmentId { get; set; }
        public string DepartmentName { get; set; }

        public int? CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int? MenuItemId { get; set; }
        public string MenuItemName { get; set; }
    }

    public class GetDinePlanTaxInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime Desc";
            }
        }

        public GetDinePlanTaxInput()
        {
            LocationGroup = new LocationGroupDto();
        }

        public LocationGroupDto LocationGroup { get; set; }
    }

    public class GetDinePlanTaxForEditOutput : IOutputDto
    {
        public DinePlanTaxEditDto DinePlanTax { get; set; }
        public DinePlanTaxLocationEditDto DinePlanTaxLocation { get; set; }
    }

    public class DeleteDinePlanTaxInput : IInputDto
    {
        public int? Id { get; set; }
        public int? FutureDataId { get; set; }
        public DinePlanTaxLocationEditDto DinePlanTaxLocation { get; set; }
    }

    public class GetDinePlanTaxLocationForEditOutput : IOutputDto
    {
        public DinePlanTaxLocationEditDto DinePlanTaxLocation { get; set; }
        public List<DinePlanTaxMappingEditDto> TaxTemplateMapping { get; set; }
    }

    public class CreateOrUpdateDinePlanTaxLocationInput : IInputDto
    {
        public int? FutureDataId { get; set; }
        public DinePlanTaxEditDto DinePlanTax { get; set; }
        public DinePlanTaxLocationEditDto DinePlanTaxLocation { get; set; }
    }

    public class DinePlanTaxIdAndLocationId : IInputDto
    {
        public int DinePlanTaxRefId { get; set; }
        public int LocationRefId { get; set; }

        public decimal TaxPercentage { get; set; }
    }



    public class GetLocationTaxesForGivenTaxInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int DinePlanTaxRefId { get; set; }
        public string FilterText { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "LocationRefName";
            }
        }
    }

}