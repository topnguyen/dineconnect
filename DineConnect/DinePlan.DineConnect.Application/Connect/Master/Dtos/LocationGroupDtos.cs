﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Master.Dtos
{
    [AutoMapFrom(typeof(LocationGroup))]
    public class LocationGroupListDto : FullAuditedEntityDto
    {
        public LocationGroupListDto()
        {
            Locations = new HashSet<LocationListDto>();
        }
        public string Code { get; set; }
        public virtual string Name { get; set; }
        public virtual int LocationCount { get; set; }
        public ICollection<LocationListDto> Locations { get; set; }
    }

    [AutoMapFrom(typeof(LocationGroup))]
    public class SimpleLocationGroupDto
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
    }

    [AutoMapTo(typeof(LocationGroup))]
    public class LocationGroupEditDto
    {
        public int? Id { get; set; }
        public virtual string Name { get; set; }
        public string Code { get; set; }
    }

    public class GetLocationGroupInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public int? LocationGroupId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }

    public class GetLocationGroupForEditOutput : IOutputDto
    {
        public LocationGroupEditDto LocationGroup { get; set; }
    }

    public class CreateOrUpdateLocationGroupInput : IInputDto
    {
        [Required]
        public LocationGroupEditDto LocationGroup { get; set; }
    }
}