﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Master.Dtos
{
    [AutoMapFrom(typeof(Calculation))]
    public class CalculationListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
        public string ButterHeader { get; set; }
        public int SortOrder { get; set; }
        public int FontSize { get; set; }
        public decimal Amount { get; set; }
        public bool IncludeTax { get; set; }
        public bool DecreaseAmount { get; set; }
        public string Departments { get; set; }
        public string Locations { get; set; }
    }

    [AutoMapTo(typeof(Calculation))]
    public class CalculationEditDto : ConnectEditDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string ButtonHeader { get; set; }
        public int SortOrder { get; set; }
        public int FontSize { get; set; }
        public decimal Amount { get; set; }
        public bool IncludeTax { get; set; }
        public bool DecreaseAmount { get; set; }
        public string Departments { get; set; }
        public int TransactionTypeId { get; set; }
        public int CalculationTypeId { get; set; }
        public int ConfirmationType { get; set; }

        public CalculationEditDto()
        {
            ConfirmationType = 0;
            CalculationTypeId = 0;
            Amount = 0M;
        }
    }

    public class ApiCalculationOutput
    {
        public List<ApiCalculation> Calculations { get; set; }

        public ApiCalculationOutput()
        {
            Calculations = new List<ApiCalculation>();
        }
    }

    [AutoMapTo(typeof(Calculation))]
    public class ApiCalculation
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string ButtonHeader { get; set; }
        public int SortOrder { get; set; }
        public int FontSize { get; set; }
        public decimal Amount { get; set; }
        public bool IncludeTax { get; set; }
        public bool DecreaseAmount { get; set; }
        public string Departments { get; set; }
        public string Locations { get; set; }
        public bool Group { get; set; }
        public int TransactionTypeId { get; set; }
        public string TransactionTypeName { get; set; }
        public int CalculationTypeId { get; set; }
        public int ConfirmationType { get; set; }
    }

    public class GetCalculationInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public GetCalculationInput()
        {
            LocationGroup = new LocationGroupDto();
        }

        public LocationGroupDto LocationGroup { get; set; }
        public string Filter { get; set; }

        public string Operation { get; set; }
        public string Location { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    public class GetCalculationForEditOutput : IOutputDto
    {
        public GetCalculationForEditOutput()
        {
            LocationGroup = new LocationGroupDto();
            Calculation = new CalculationEditDto();
        }

        public CalculationEditDto Calculation { get; set; }
        public List<ComboboxItemDto> Departments { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
    }

    public class CreateOrUpdateCalculationInput : IInputDto
    {
        public CreateOrUpdateCalculationInput()
        {
            LocationGroup = new LocationGroupDto();
        }

        [Required]
        public CalculationEditDto Calculation { get; set; }

        public LocationGroupDto LocationGroup { get; set; }
        public List<ComboboxItemDto> Departments { get; set; }
    }
}