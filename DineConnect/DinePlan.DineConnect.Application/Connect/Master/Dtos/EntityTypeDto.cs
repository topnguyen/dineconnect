﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Entity;
using DinePlan.DineConnect.Dto;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Master.Dtos
{
    [AutoMapFrom(typeof(EntityType))]
    public class EntityTypeListDto : FullAuditedEntityDto
    {
        public virtual string Name { get; set; }
        public virtual string EntityName { get; set; }
        public virtual string PrimaryFieldName { get; set; }
        public virtual string PrimaryFieldFormat { get; set; }
        public virtual string DisplayFormat { get; set; }
    }

    [AutoMapTo(typeof(EntityType))]
    public class EntityTypeEditDto
    {
        public EntityTypeEditDto()
        {
            Fields = new Collection<EntityCustomFieldEditDto>();
        }

        public int? Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string EntityName { get; set; }
        public virtual string PrimaryFieldName { get; set; }
        public virtual string PrimaryFieldFormat { get; set; }
        public virtual string DisplayFormat { get; set; }
        public Collection<EntityCustomFieldEditDto> Fields { get; set; }
    }

    [AutoMapTo(typeof(EntityCustomField))]
    public class EntityCustomFieldEditDto
    {
        public int? Id { get; set; }
        public virtual string Name { get; set; }
        public virtual int FieldType { get; set; }
        public virtual string EditingFormat { get; set; }
        public virtual string ValueSource { get; set; }
        public virtual bool Hidden { get; set; }
    }

    public class GetEntityTypeInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    public class GetEntityTypeForEditOutput : IOutputDto
    {
        public EntityTypeEditDto EntityType { get; set; }
    }

    public class CreateOrUpdateEntityTypeInput : IInputDto
    {
        [Required]
        public EntityTypeEditDto EntityType { get; set; }
    }
}