﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Master.Dtos
{
    [AutoMapFrom(typeof(ForeignCurrency))]
    public class ForeignCurrencyListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
        public string CurrencySymbol { get; set; }
        public decimal ExchangeRate { get; set; }
        public int PaymentTypeId { get; set; }
        public decimal Rounding { get; set; }
    }

    [AutoMapTo(typeof(ForeignCurrency))]
    public class ForeignCurrencyEditDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string CurrencySymbol { get; set; }
        public decimal ExchangeRate { get; set; }
        public decimal Rounding { get; set; }
        public int PaymentTypeId { get; set; }
    }

    public class GetForeignCurrencyInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }

        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    public class GetForeignCurrencyForEditOutput : IOutputDto
    {
        public ForeignCurrencyEditDto ForeignCurrency { get; set; }
    }

    public class CreateOrUpdateForeignCurrencyInput : IInputDto
    {
        [Required]
        public ForeignCurrencyEditDto ForeignCurrency { get; set; }
    }
}