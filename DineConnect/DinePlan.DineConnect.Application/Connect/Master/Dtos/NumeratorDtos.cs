﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Master.Dtos
{
    [AutoMapFrom(typeof(Numerator))]
    public class NumeratorListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
        public string NumberFormat { get; set; }
        public string OutputFormat { get; set; }
        public  bool ResetNumerator { get; set; }
        public  ResetNumeratorType ResetNumeratorType { get; set; }
    }

    [AutoMapTo(typeof(Numerator))]
    public class NumeratorEditDto : ConnectEditDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string NumberFormat { get; set; }
        public string OutputFormat { get; set; }
        public  bool ResetNumerator { get; set; }
        public  ResetNumeratorType ResetNumeratorType { get; set; }
    }

    public class NumeratorInput : PagedAndSortedInputDto
    {
        public string Filter { get; set; }
        public bool? AcceptChange { get; set; }
        public string Location { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
        public NumeratorInput()
        {
            LocationGroup = new LocationGroupDto();
        }

        public LocationGroupDto LocationGroup { get; set; }
    }

    public class GetNumeratorForEditOutput : IOutputDto
    {
        public NumeratorEditDto Numerator { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public List<ComboboxItemDto> Departments { get; set; }

        public GetNumeratorForEditOutput()
        {
            LocationGroup = new LocationGroupDto();
            Numerator = new NumeratorEditDto();
        }
    }

    public class CreateOrUpdateNumeratorInput : IInputDto
    {
        [Required]
        public NumeratorEditDto Numerator { get; set; }

        public LocationGroupDto LocationGroup { get; set; }

        public CreateOrUpdateNumeratorInput()
        {
            LocationGroup = new LocationGroupDto();
        }
    }

    public class ApiNumeratorOutput
    {
        public int Id { get; set; }
        public int TenantId { get; set; }
        public string Name { get; set; }
        public string NumberFormat { get; set; }
        public string OutputFormat { get; set; }
        public bool ResetNumerator { get; set; }
        public ResetNumeratorType ResetNumeratorType { get; set; }
    }
}