﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities;
using Abp.Extensions;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Addons.Dto;
using DinePlan.DineConnect.Dto;
using Humanizer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Master.Dtos
{
    [AutoMapFrom(typeof(Location), typeof(LocationListDto))]
    public class SimpleLocationDto
    {
        public virtual int Id { get; set; }
        public virtual string Code { get; set; }
        public virtual string Name { get; set; }
        public virtual long OrganizationUnitId { get; set; }
        public virtual int CompanyRefId { get; set; }
        public int? AvgPriceLocationRefId { get; set; } // Null means, current location else mentioned locaiton id
    }

    [AutoMapFrom(typeof(Location))]
    public class LocationScheduleDto
    {
        public string CompanyName { get; set; }
        public virtual string Name { get; set; }
        public virtual string Address1 { get; set; }
        public virtual string Address2 { get; set; }
        public virtual string Address3 { get; set; }
        public virtual string City { get; set; }
        public virtual string State { get; set; }
        public virtual string Country { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual string Fax { get; set; }
        public virtual string Website { get; set; }
        public virtual string Email { get; set; }
        public List<ScheduleListDto> Schedules { get; set; }
    }

    [AutoMapFrom(typeof(Location))]
    public class LocationListDto : FullAuditedEntityDto
    {
        public int CompanyRefId { get; set; }
        public Company Company { get; set; }

        public string CompanyName
        {
            get { return Company?.Name; }
            set { }
        }

        public virtual string Code { get; set; }
        public virtual string Name { get; set; }
        public virtual string Address1 { get; set; }
        public virtual string Address2 { get; set; }
        public virtual string Address3 { get; set; }
        public virtual string City { get; set; }
        public virtual string State { get; set; }
        public virtual string Country { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual decimal SharingPercentage { get; set; }
        public virtual string Fax { get; set; }
        public virtual string Website { get; set; }
        public virtual string Email { get; set; }
        public virtual bool IsPurchaseAllowed { get; set; }
        public virtual bool IsProductionAllowed { get; set; }
        public virtual int DefaultRequestLocationRefId { get; set; }
        public virtual string RequestedLocations { get; set; }
        public virtual DateTime? HouseTransactionDate { get; set; }
        public virtual string TallyCompanyName { get; set; }
        public bool IsProductionUnitAllowed { get; set; }

        public List<ProductionUnitListDto> ProductionUnitList { get; set; }
        public virtual int ExtendedBusinessHours { get; set; }
        public virtual bool IsDayCloseRecursiveUptoDateAllowed { get; set; }
        public int? LocationGroupId { get; set; }
        public string LocationGroupName { get; set; }
        public List<ScheduleListDto> Schedules { get; set; }
        public bool DoesDayCloseRunInBackGround { get; set; }
        public DateTime? BackGroundStartTime { get; set; }
        public DateTime? BackGrandEndTime { get; set; }
        public decimal TransferRequestLockHours { get; set; }
        public decimal TransferRequestGraceHours { get; set; }
        public string PostalCode { get; set; }
        public int? AvgPriceLocationRefId { get; set; } // Null means, current location else mentioned locaiton id
        public string AvgPriceLocationRefCode { get; set; }
        public int? DayCloseSalesStockAdjustmentLocationRefId { get; set; }
        public string DayCloseSalesStockAdjustmentLocationRefCode { get; set; }

    }

    public class SharingPercentageDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Percentage { get; set; }
    }

    [AutoMap(typeof(Location))]
    public class LocationEditDto : FullAuditedEntityDto
    {
        public int? Id { get; set; }
        public int CompanyRefId { get; set; }
        public Company companies { get; set; }
        public string Code { get; set; }
        public string CompanyCode { get; set; }
        public string Name { get; set; }
        public virtual string Address1 { get; set; }
        public virtual string Address2 { get; set; }
        public virtual string Address3 { get; set; }
        public virtual string City { get; set; }
        public virtual string State { get; set; }
        public virtual string Country { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual string Fax { get; set; }
        public virtual string Website { get; set; }
        public virtual string Email { get; set; }
        public virtual bool IsPurchaseAllowed { get; set; }
        public virtual bool IsProductionAllowed { get; set; }
        public virtual int DefaultRequestLocationRefId { get; set; }
        public virtual string RequestedLocations { get; set; }
        public virtual DateTime? HouseTransactionDate { get; set; }
        public virtual decimal SharingPercentage { get; set; }
        public virtual bool TaxInclusive { get; set; }
        public virtual bool ChangeTax { get; set; }

        public virtual string AddOn { get; set; }
        public virtual string Currency { get; set; }
        public bool IsProductionUnitAllowed { get; set; }
        public virtual int ExtendedBusinessHours { get; set; }
        public virtual bool IsDayCloseRecursiveUptoDateAllowed { get; set; }
        public int? LocationGroupId { get; set; }

        public virtual List<LocationSchduleEditDto> Schedules { get; set; }
        public string ReceiptHeader { get; set; }
        public string ReceiptFooter { get; set; }

        public virtual List<LocationGroupEditDto> LocationGroups { get; set; }
        public virtual List<LocationTagEditDto> LocationTags { get; set; }
        public string PostalCode { get; set; }
        public int? LocationBranchId { get; set; }
        public string Mid { get; set; }

        public bool EnableHouse { get; set; }


        public LocationEditDto()
        {
            Schedules = new List<LocationSchduleEditDto>();
            LocationGroups = new List<LocationGroupEditDto>();
            LocationTags = new List<LocationTagEditDto>();
        }
        public decimal TransferRequestLockHours { get; set; }
        public decimal TransferRequestGraceHours { get; set; }
        public int? AvgPriceLocationRefId { get; set; } // Null means, current location else mentioned locaiton id
        public int? DayCloseSalesStockAdjustmentLocationRefId { get; set; }
    }

    [AutoMap(typeof(LocationBranch))]
    public class LocationBranchEditDto : FullAuditedEntityDto
    {
        public int? Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string District { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Fax { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
        public string BranchTaxCode { get; set; }
    }

    [AutoMap(typeof(LocationSchdule))]
    public class LocationSchduleEditDto : Entity<int?>
    {
        public string Name { get; set; }
        public int StartHour { get; set; }
        public int StartMinute { get; set; }
        public int EndHour { get; set; }
        public int LocationId { get; set; }
        public int EndMinute { get; set; }
    }

    [AutoMap(typeof(LocationSchdule))]
    public class ScheduleListDto
    {
        public string Name { get; set; }
        public int StartHour { get; set; }
        public int StartMinute { get; set; }
        public int EndHour { get; set; }
        public int EndMinute { get; set; }
        public int LocationId { get; set; }
    }

    public class CheckLocationInput
    {
        public int LocationId { get; set; }
        public string Locations { get; set; }
        public string NonLocations { get; set; }
        public bool Group { get; set; }
        public int Type { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool LocationTag { get; set; }
    }

    public class BrandsAndLocationInput
    {
        public string Group { get; set; }
        public int TenantId { get; set; }
    }

    public class BrandsAndLocationOutputList
    {
        public BrandsAndLocationOutputList()
        {
            Brands = new List<BrandsAndLocationOutput>();
        }

        public List<BrandsAndLocationOutput> Brands { get; set; }
    }

    public class BrandsAndLocationOutput
    {
        public int BrandId { get; set; }
        public string BrandName { get; set; }
        public string BrandCode { get; set; }

        public List<SimpleLocationDto> AllLocations { get; set; }

        public BrandsAndLocationOutput()
        {
            AllLocations = new List<SimpleLocationDto>();
        }
    }

    public class GetLocationInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public string Operation { get; set; }
        public int LocationRefId { get; set; }
        public List<ComboboxItemDto> CompanyList { get; set; }
        public List<ComboboxItemDto> LocationGroups { get; set; }
        public long UserId { get; set; }
        public int? LocationGroupId { get; set; }
        public int? LocationTagId { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }

    public class UserLocationDto
    {
        public long UserId { get; set; }
        public List<LocationListDto> Locations { get; set; }
        public int? DefaultLocationRefId { get; set; }
    }

    public class GetLocationInputBasedOnUser
    {
        public long UserId { get; set; }
        public List<ComboboxItemDto> CompanyList { get; set; }
        public string LocationName { get; set; }
    }

    public class GetLocationForEditOutput
    {
        public LocationEditDto Location { get; set; }
        public LocationBranchEditDto LocationBranch { get; set; }
        public List<LocationListDto> RequestLocations { get; set; }
        public UrbanPiperAddOn UrbanPiper { get; set; }
    }

    public class CreateOrUpdateLocationInput
    {
        public LocationEditDto Location { get; set; }
        public LocationBranchEditDto LocationBranch { get; set; }
        public List<LocationListDto> RequestLocations { get; set; }
        public UrbanPiperAddOn UrbanPiper { get; set; }
    }

    public class MenuItemToLocationInput
    {
        [Range(1, long.MaxValue)] public int MenuItemId { get; set; }

        [Range(1, long.MaxValue)] public long OrganizationUnitId { get; set; }
    }

    public class CurrentDateDto
    {
        public DateTime CurrentDate { get; set; }
    }

    public class AccountDateDto
    {
        public int Id { get; set; }
        public DateTime AccountDate { get; set; }
        public bool DoesDayCloseRunInBackGround { get; set; }
        public DateTime? BackGroundDayCloseStartTime { get; set; }
        public DateTime? LastUpdateTime { get; set; }
        public long? CurrentUserId { get; set; }
    }

    public class LocationWithDate
    {
        public LocationWithDate()
        {
            UomTypeFilters = new List<ComboboxItemDto>();
            LocationInfo = new LocationListDto();
        }
        public int LocationRefId { get; set; }
        public LocationListDto LocationInfo { get; set; }
        public DateTime StockTakenDate { get; set; }
        public bool IsHighValueItemOnly { get; set; }
        public bool MultipleUomAllowed { get; set; }
        public List<ComboboxItemDto> UomTypeFilters { get; set; }
        public bool ExcludeMaterialWhichStockCycleNotAssigned { get; set; }
        public bool CustomReportFlag { get; set; }
        public DateTime PrintedTime { get; set; }
        public long? PreparedUserId { get; set; }
        public string PreparedUserName { get; set; }
        public bool DownLoadMultipleUOMMultipleSupplierMaterialOnly { get; set; }
    }

    public class ApiLocationOutput
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }

    public class ApiUserInput
    {
        public int UserId { get; set; }
    }

    public class ApiLocationInput 
    {
        public int OrganizationId { get; set; }
        public bool IsFuturedata { get; set; } = false;
        public int LocationId { get; set; }
        public string Filter { get; set; }
        public string Tag { get; set; }
        public int TenantId { get; set; }
        public int PriceTagId { get; set; }
        public int Id { get; set; }
        public DateTime LastSyncTime { get; set; }
        public TimeZoneInfo TimeZone { get; set; }
        public long UserId { get; set; }
        public int[] RefObjects { get; set; }

    }

    public class UpdateCallInput
    {
        public int TicketId { get; set; }
        public int LocationId { get; set; }
        public int LocalTicketId { get; set; }
        public int DeliveryStatus { get; set; }
    }

    public class CallStatusInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int Location { get; set; }
        public int Status { get; set; }
        public int Member { get; set; }
        public int TicketId { get; set; }

        public void Normalize()
        {
            if (Sorting.IsNullOrWhiteSpace())
            {
                Sorting = "Id";
            }
        }
    }

    public class CallStatusOutput
    {
        public int NotAssigned { get; set; }
        public int Preparing { get; set; }
        public int Delivering { get; set; }
        public int Delivered { get; set; }
    }

    [AutoMapFrom(typeof(Delivery.DeliveryTicket))]
    public class DeliveryTicketList
    {
        public int Id { get; set; }
        public decimal TotalAmount { get; set; }
        public DateTime CreationTime { get; set; }
        public string ElapsedTime => (DateTime.Now - CreationTime).Humanize();
        public string Address { get; set; }
        public string Name { get; set; }
        public string Locality { get; set; }
        public string LocationName { get; set; }
        public string TicketNumber { get; set; }

    }

    public class UpdateLocationGroup
    {
        public int? LocationGroupId { get; set; }
        public List<int> LocationIds { get; set; }
    }

    public class ApiLocationSetting
    {
        public bool TaxInclusive { get; set; }
        public bool ChangeTax { get; set; }
        public int Id { get; set; }
        public string Code { get; set; }
        public string CompanyName { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string CompanyCode { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
        public string LocationTaxCode { get; set; }
        public string CompanyTaxCode { get; set; }
        public string CompanyAddress1 { get; set; }
        public string CompanyAddress2 { get; set; }
        public string CompanyAddress3 { get; set; }
        public string CompanyCity { get; set; }
        public string CompanyState { get; set; }
        public string CompanyCountry { get; set; }
        public string CompanyPhoneNumber { get; set; }
        public string CompanyWebsite { get; set; }
        public string CompanyEmail { get; set; }
        public string PostalCode { get; set; }
        public string BranchId { get; set; }
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public string BranchAddress1 { get; set; }
        public string BranchAddress2 { get; set; }
        public string BranchAddress3 { get; set; }
        public string BranchCity { get; set; }
        public string BranchState { get; set; }
        public string BranchPostalCode { get; set; }
        public string BranchCountry { get; set; }
        public string BranchWebsite { get; set; }
        public string BranchEmail { get; set; }
        public string BranchPhoneNumber { get; set; }
        public string BranchLocationTaxCode { get; set; }
        public string District { get; set; }
        public string BranchDistrict { get; set; }
        public string AddOn { get; set; }

        public List<ScheduleListDto> LocationSchedules { get; set; }
    }

    public class ApiReceiptDetails
    {
        public string ReceiptHeader { get; set; }
        public string ReceiptFooter { get; set; }
    }

    public class UpdateLocationTag
    {
        public int? LocationTagId { get; set; }
        public List<int> LocationIds { get; set; }
    }

   


}