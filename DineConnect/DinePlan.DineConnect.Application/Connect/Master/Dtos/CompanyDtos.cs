﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Dto;
using System;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Master.Dtos
{
    [AutoMapFrom(typeof(Company))]
    public class CompanyListDto : FullAuditedEntityDto
    {
        //TODO: DTO Company Properties Missing
        //YOU CAN REFER ATTRIBUTES IN
        public virtual string Code { get; set; }

        public virtual string Name { get; set; }
        public virtual string Address1 { get; set; }
        public virtual string Address2 { get; set; }
        public virtual string Address3 { get; set; }
        public virtual string City { get; set; }
        public virtual string State { get; set; }
        public virtual string Country { get; set; }
        public virtual string PostalCode { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual string FaxNumber { get; set; }
        public virtual string UserSerialNumber { get; set; }
        public virtual string GovtApprovalId { get; set; }
        public virtual Guid? CompanyProfilePictureId { get; set; }
    }

    [AutoMapTo(typeof(Company))]
    public class CompanyEditDto
    {
        public int? Id { get; set; }

        //TODO: DTO Company Properties Missing
        public virtual string Code { get; set; }

        public virtual string Name { get; set; }
        public virtual string Address1 { get; set; }
        public virtual string Address2 { get; set; }
        public virtual string Address3 { get; set; }
        public virtual string City { get; set; }
        public virtual string State { get; set; }
        public virtual string Country { get; set; }
        public virtual string PostalCode { get; set; }
        public virtual string PhoneNumber { get; set; }
        public virtual string FaxNumber { get; set; }
        public virtual string UserSerialNumber { get; set; }
        public virtual string GovtApprovalId { get; set; }
        public virtual Guid? CompanyProfilePictureId { get; set; }
    }

    public class PictureDto : IInputDto
    {
        public int Id { get; set; }
        public int CompId { get; set; }
        public string Data { get; set; }
    }

    public class GetCompanyInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public string Operation { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

    public class GetCompanyForEditOutput : IOutputDto
    {
        public CompanyEditDto Company { get; set; }
    }

    public class CreateOrUpdateCompanyInput : IInputDto
    {
        [Required]
        public CompanyEditDto Company { get; set; }
    }
}