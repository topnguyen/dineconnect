﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Master.Dtos
{
    [AutoMapFrom(typeof(PaymentType))]
    public class PaymentTypeListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
        public bool AcceptChange { get; set; }
        public string Group { get; set; }
        public  bool AutoPayment { get; set; }
        public  bool DisplayInShift { get; set; }
    }

    [AutoMapTo(typeof(PaymentType))]
    public class PaymentTypeEditDto : ConnectEditDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public bool AcceptChange { get; set; }
        public bool Hide { get; set; }
        public string AccountCode { get; set; }
        public int PaymentProcessor { get; set; }
        public string Processors { get; set; }
        public string PaymentGroup { get; set; }
        public string Files { get; set; }
        public string Departments { get; set; }
        public string PaymentTag { get; set; }
        public bool DisplayInShift { get; set; }
        public bool AutoPayment { get; set; }

        public bool NoRefund { get; set; }
        public PaymentTenderType PaymentTenderType { get; set; }


    }

    public class GetPaymentTypeInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public bool? AcceptChange { get; set; }
        public string Location { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
        public GetPaymentTypeInput()
        {
            LocationGroup = new LocationGroupDto();
        }

        public LocationGroupDto LocationGroup { get; set; }
    }

    public class GetPaymentTypeForEditOutput : IOutputDto
    {
        public PaymentTypeEditDto PaymentType { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public List<ComboboxItemDto> Departments { get; set; }

        public GetPaymentTypeForEditOutput()
        {
            LocationGroup = new LocationGroupDto();
            PaymentType = new PaymentTypeEditDto();
        }
    }

    public class CreateOrUpdatePaymentTypeInput : IInputDto
    {
        [Required]
        public PaymentTypeEditDto PaymentType { get; set; }

        public LocationGroupDto LocationGroup { get; set; }

        public CreateOrUpdatePaymentTypeInput()
        {
            LocationGroup = new LocationGroupDto();
        }
    }

    public class ApiPaymentTypeInput
    {
        public int TenantId { get; set; }
        public int LocationId { get; set; }
        public DateTime LastSyncTime { get; set; }
        public TimeZoneInfo TimeZone { get; set; }
    }

    public class ApiPaymentTypeOutput
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool AcceptChange { get; set; }
        public int SortOrder { get; set; }
        public int PaymentProcessor { get; set; }
        public string PaymentProcessorName { get; set; }

        public string Processors { get; set; }
        public string AccountCode { get; set; }
        public string Group { get; set; }
        public bool NoRefund { get; set; }
        public string Files { get; set; }
        public Guid? DownloadImage { get; set; }
        public string Departments { get; set; }
        public bool AutoPayment { get; set; }
        public bool DisplayInShift { get; set; }
        public string ButtonColor { get; set; }
        public PaymentTenderType PaymentTenderType { get; set; }
        public string PaymentTag { get; set; }
    }
}