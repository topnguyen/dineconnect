﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Master.Dtos
{
    [AutoMapFrom(typeof(TicketType))]
    public class TicketTypeListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
        public Numerator InvoiceNumerator { get; set; }
        public Numerator TicketNumerator { get; set; }
        public Numerator OrderNumerator { get; set; }
        public Numerator PromotionNumerator { get; set; }
        public Numerator FullTaxNumerator { get; set; }
        public Numerator QueueNumerator { get; set; }
        public TransactionType SaleTransactionType { get; set; }
    }

    [AutoMapTo(typeof(TicketType))]
    public class TicketTypeEditDto : ConnectEditDto
    {
        public int? Id { get; set; }
        public virtual int TicketTypeConfigurationId { get; set; }
        public virtual bool IsTaxIncluded { get; set; }
        public virtual bool IsPreOrder { get; set; }
        public virtual bool IsAllowZeroPriceProducts { get; set; }
        public string Name { get; set; }
        public int? ScreenMenuId { get; set; }
        public int? TicketInvoiceNumeratorId { get; set; }
        public string InvoiceNumeratorName { get; set; }
        public int? TicketNumeratorId { get; set; }
        public string TicketNumeratorName { get; set; }
        public int? OrderNumeratorId { get; set; }
        public string OrderNumeratorName { get; set; }
        public int? PromotionNumeratorId { get; set; }
        public string PromotionNumeratorName { get; set; }
        public int? FullTaxNumeratorId { get; set; }
        public string FullTaxNumeratorName { get; set; }
        public int? QueueNumeratorId { get; set; }
        public string QueueNumeratorName { get; set; }
        public int? ReturnNumeratorId { get; set; }
        public string ReturnNumeratorName { get; set; }

        public int? SaleTransactionType_Id { get; set; }
        public string SaleTransactionTypeName { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public TicketTypeEditDto()
        {
            LocationGroup = new LocationGroupDto();
        }
    }

    public class GetTicketTypeInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public bool? AcceptChange { get; set; }
        public string Location { get; set; }
        public bool IsDeleted { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
        public GetTicketTypeInput()
        {
            LocationGroup = new LocationGroupDto();
        }

        public LocationGroupDto LocationGroup { get; set; }
    }

    public class GetTicketTypeForEditOutput : IOutputDto
    {
        public TicketTypeEditDto TicketType { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public List<ComboboxItemDto> Departments { get; set; }

        public GetTicketTypeForEditOutput()
        {
            LocationGroup = new LocationGroupDto();
            TicketType = new TicketTypeEditDto();
        }
    }

    public class CreateOrUpdateTicketTypeInput : IInputDto
    {
        public TicketTypeEditDto TicketType { get; set; }
    }

    public class GetTicketTypeOutputDto : IOutputDto
    {
        public TicketTypeConfigurationEditDto TicketTypeConfiguration { get; set; }
        public TicketTypeEditDto TicketType { get; set; }

    }
}