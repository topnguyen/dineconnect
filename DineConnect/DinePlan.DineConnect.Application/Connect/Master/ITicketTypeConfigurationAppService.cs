﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master
{
    public interface ITicketTypeConfigurationAppService : IApplicationService
    {
        PagedResultOutput<TicketTypeConfigurationListDto> GetAll(GetTicketTypeConfigurationInput input);

        Task<int> CreateOrUpdateTicketTypeConfiguration(CreateTicketTypeConfigurationInput input);
    }
}