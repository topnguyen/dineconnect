﻿using System.Collections.Generic;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Tag;

namespace DinePlan.DineConnect.Connect.Master
{
    public interface ITicketTagGroupAppService : IApplicationService
    {
        Task<PagedResultOutput<TicketTagGroupListDto>> GetAll(GetTicketTagGroupInput inputDto);

        Task<FileDto> GetAllToExcel();

        Task<GetTicketTagGroupForEditOutput> GetTagForEdit(NullableIdInput nullableIdInput);

        Task CreateOrUpdateTagGroup(CreateOrUpdateTicketTagGroupInput input);

        Task DeleteTicketTagGroup(IdInput input);

        Task<ListResultOutput<TicketTagGroupListDto>> GetNames();


        Task<ListResultOutput<TicketTagListDto>> GetAllTags();

        Task ActivateItem(IdInput input);

        Task<ListResultOutput<TicketTagGroupEditDto>> ApiGetTags(ApiLocationInput locationInput);

        Task<List<TicketTagGroup>> ApiGetAllTicketTagGroupsForSync(ApiLocationInput input);

    }
}