﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master
{
    public interface IProductionUnitAppService : IApplicationService
    {
        Task<PagedResultOutput<ProductionUnitListDto>> GetAll(GetProductionUnitInput inputDto);
        Task<FileDto> GetAllToExcel();
        Task<GetProductionUnitForEditOutput> GetProductionUnitForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdateProductionUnit(CreateOrUpdateProductionUnitInput input);
        Task DeleteProductionUnit(IdInput input);

        Task<ListResultOutput<ProductionUnitListDto>> GetCodes();
    }
}
