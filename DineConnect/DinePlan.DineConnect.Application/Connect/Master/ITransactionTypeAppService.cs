﻿using System.Collections.Generic;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master
{
    public interface ITransactionTypeAppService : IApplicationService
    {
        Task<PagedResultOutput<TransactionTypeListDto>> GetAll(GetTransactionTypeInput inputDto);

        Task<FileDto> GetAllToExcel();

        Task<GetTransactionTypeForEditOutput> GetTransactionTypeForEdit(NullableIdInput nullableIdInput);

        Task<int> CreateOrUpdateTransactionType(CreateOrUpdateTransactionTypeInput input);

        Task DeleteTransactionType(IdInput input);

        IQueryable<TransactionType> GetEntities();

        Task CreateTransactionTypeForTenant(int tenantId);

        Task<PagedResultOutput<TransactionTypeEditDto>> GetAllItems();

        Task<ListResultOutput<ApiTransactionTypeOutput>> ApiGetAll(ApiTransactionTypeInput inputDto);

        Task ActivateItem(IdInput input);

        Task<PagedResultOutput<TransactionTypeListDto>> ApiGetTransactionTypes(GetTransactionTypeInput inputDto);


        Task<List<TransactionType>> ApiGetAllTransactionTypesForSync(ApiLocationInput input);

    }
}