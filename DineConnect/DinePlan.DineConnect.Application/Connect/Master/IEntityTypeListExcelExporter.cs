﻿using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master
{
    public interface IEntityTypeListExcelExporter
    {
        FileDto ExportToFile(List<EntityTypeListDto> dtos);
    }
}