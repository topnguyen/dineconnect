﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master
{
    public interface IForeignCurrencyAppService : IApplicationService
    {
        Task<PagedResultOutput<ForeignCurrencyListDto>> GetAll(GetForeignCurrencyInput inputDto);

        Task<FileDto> GetAllToExcel();

        Task<GetForeignCurrencyForEditOutput> GetForeignCurrencyForEdit(NullableIdInput nullableIdInput);

        Task CreateOrUpdateForeignCurrency(CreateOrUpdateForeignCurrencyInput input);

        Task DeleteForeignCurrency(IdInput input);

        Task<ListResultOutput<ForeignCurrencyListDto>> ApiGetAll(ApiLocationInput locationInput);

        Task ActivateItem(IdInput input);
    }
}