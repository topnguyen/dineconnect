﻿using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master
{
    public interface ILocationGroupExcelExporter
    {
        FileDto ExportToFile(List<LocationGroupListDto> dtos);
    }
}
