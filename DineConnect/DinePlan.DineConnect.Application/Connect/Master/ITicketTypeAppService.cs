﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master
{
    public interface ITicketTypeAppService : IApplicationService
    {
        Task<FileDto> GetAllToExcel(GetTicketTypeInput input);

        Task<PagedResultOutput<TicketTypeConfigurationListDto>> GetAll(GetTicketTypeInput input);

        Task<GetTicketTypeOutputDto> GetTicketTypeForEdit(TicketTypeFilterInputDto input);

        Task DeleteTicketType(IdInput input);

        Task ActivateItem(IdInput input);

        Task<IdInput> AddOrEditTicketType(CreateOrUpdateTicketTypeInput input);

        Task<ListResultOutput<TicketTypeEditDto>> ApiGetTicketTypes(ApiLocationInput locationInput);
        Task DeleteTicketTypeMapping(IdInput input);
    }
}