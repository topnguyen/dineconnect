﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations.Dtos;

namespace DinePlan.DineConnect.Connect.Master
{
    public interface IDinePlanTaxAppService : IApplicationService
    {
        Task<PagedResultOutput<DinePlanTaxListDto>> GetAll(GetDinePlanTaxInput inputDto);

        Task<FileDto> GetAllToExcel(GetDinePlanTaxInput input);

        Task<GetDinePlanTaxForEditOutput> GetDinePlanTaxForEdit(FilterInputDto input);
        Task<IdInput> CreateOrUpdateDinePlanTax(CreateOrUpdateDinePlanTaxLocationInput input);

        Task DeleteDinePlanTax(IdInput input);
        Task<ListResultOutput<ComboboxItemDto>> GetDinePlanTaxForCombobox(FilterInputDto input);
        Task<List<ApiTaxOutput>> ApiGetTaxes(ApiLocationInput input);

        Task ActivateItem(IdInput input);
        Task<IdInput> AddOrEditDinePlanTaxLocation(CreateOrUpdateDinePlanTaxLocationInput input);
        Task DeleteDinePlanTaxLocation(DeleteDinePlanTaxInput input);
    }
}