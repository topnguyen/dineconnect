﻿using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using System.Collections.Generic;
using System.Linq;

namespace DinePlan.DineConnect.Connect.Master.Exporter
{
    public class TicketTagGroupExcelExporter : FileExporterBase, ITicketTagGroupExcelExporter

    {
        public FileDto ExportToFile(List<TicketTagGroupListDto> dtos)
        {
            var list = GetFormattedTicketTagGroupList(dtos);

            return CreateExcelPackage(
                "TicketTagGroupList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("TicketTagGroups"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Name"),
                        L("Tag"),
                        L("Departments"),
                        L("CreationTime")
                        );

                    AddObjects(
                        sheet, 2, list,
                        _ => _.Id,
                        _ => _.Name,
                        _ => _.Tag,
                        _ => _.DisplayDepartments,
                        _ => _.CreationTime
                        );

                    //Formatting cells

                    var creationTimeColumn = sheet.Column(5);
                    creationTimeColumn.Style.Numberformat.Format = "mm-dd-yyyy hh:mm:ss";

                    for (var i = 1; i <= 7; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }

        private static List<TicketTagGroupListDto> GetFormattedTicketTagGroupList(IEnumerable<TicketTagGroupListDto> dtos)
        {
            var formattedList = new List<TicketTagGroupListDto>();

            foreach (var item in dtos)
            {
                formattedList.Add(item);

                var tags = item.Tags.Select(tag => new TicketTagGroupListDto
                {
                    Id = null,
                    Name = "",
                    DisplayDepartments = "",
                    Tag = tag.Name
                });
                formattedList.AddRange(tags);
            }

            return formattedList.ToList();
        }
    }
}