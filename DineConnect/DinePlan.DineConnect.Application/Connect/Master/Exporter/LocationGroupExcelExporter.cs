﻿using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.Exporter
{
    class LocationGroupExcelExporter : FileExporterBase, ILocationGroupExcelExporter
    {
        public FileDto ExportToFile(List<LocationGroupListDto> dtos)
        {
            return CreateExcelPackage(
                "LocationGroup.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("LocationGroup"));
                    sheet.OutLineApplyStyle = true;

                    int row = 1; int col = 1;

                    #region Header Print
                    AddDetail(sheet, row, col++, "Code");
                    sheet.Column(col).AutoFit();
                    AddDetail(sheet, row, col++, "Location Group");
                    sheet.Column(col).AutoFit();
                    AddDetail(sheet, row, col++, "Location Name");
                    sheet.Column(col).AutoFit();
                    #endregion

                    #region Detail Print

                    foreach (var lst in dtos)
                    {
                        row++;
                        col = 1;
                        AddDetail(sheet, row, col++, lst.Code);
                        sheet.Column(col).AutoFit();
                        AddDetail(sheet, row, col++, lst.Name);
                        sheet.Column(col).AutoFit();
                        foreach (var sublst in lst.Locations)
                        {
                            row++;
                            AddDetail(sheet, row, col, sublst.Name);
                            sheet.Column(col).AutoFit();
                        }
                        row++;
                    }
                    #endregion

                });
        }
    }
}

