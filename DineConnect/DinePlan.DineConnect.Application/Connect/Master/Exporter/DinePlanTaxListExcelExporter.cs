﻿
using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master.Exporter
{
    public class DinePlanTaxListExcelExporter : FileExporterBase, IDinePlanTaxListExcelExporter
    {
        public FileDto ExportToFile(List<DinePlanTaxListDto> dtos)
        {
            return CreateExcelPackage(
                "DinePlanTaxList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("DinePlanTax"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Id"),
                        L("Name"),
                        "Transaction Type"
                        );

                    AddObjects(
                        sheet, 2, dtos,
                        _ => _.Id,
                         _ => _.TaxName,
                          _ => _.TransactionTypeName
                        );

                    for (var i = 1; i <= 10; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}