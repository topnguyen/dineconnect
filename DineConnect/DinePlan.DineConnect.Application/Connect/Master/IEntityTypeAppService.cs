﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master
{
    public interface IEntityTypeAppService : IApplicationService
    {
        Task CreateEntityForTenant(int tenantId);

        Task<PagedResultOutput<EntityTypeListDto>> GetAll(GetEntityTypeInput inputDto);

        Task<FileDto> GetAllToExcel();

        Task<GetEntityTypeForEditOutput> GetEntityTypeForEdit(NullableIdInput nullableIdInput);

        Task CreateOrUpdateEntityType(CreateOrUpdateEntityTypeInput input);

        Task DeleteEntityType(IdInput input);

        Task<ListResultOutput<EntityTypeListDto>> GetIds();

        Task ActivateItem(IdInput input);
    }
}