﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master
{
    public interface ILocationTagAppService : IApplicationService
    {
        Task<PagedResultOutput<LocationTagListDto>> GetAll(GetLocationTagInput input);

        Task<FileDto> GetAllToExcel(GetLocationTagInput input);

        Task<LocationTagEditDto> GetLocationTagForEdit(NullableIdInput input);

        Task CreateOrUpdateLocationTag(LocationTagEditDto input);

        Task DeleteLocationTag(IdInput input);
    }
}