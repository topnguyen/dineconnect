﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master
{
    public interface ICalculationAppService : IApplicationService
    {
        Task<PagedResultOutput<CalculationListDto>> GetAll(GetCalculationInput inputDto);

        Task<FileDto> GetAllToExcel();

        Task<GetCalculationForEditOutput> GetCalculationForEdit(NullableIdInput nullableIdInput);

        Task CreateOrUpdateCalculation(CreateOrUpdateCalculationInput input);

        Task DeleteCalculation(IdInput input);

        Task<ListResultOutput<CalculationListDto>> GetNames();

        Task<ApiCalculationOutput> ApiGetCalculations(ApiLocationInput locationInput);

        Task ActivateItem(IdInput input);
    }
}