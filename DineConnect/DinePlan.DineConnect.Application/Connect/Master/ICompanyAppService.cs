﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master
{
    public interface ICompanyAppService : IApplicationService
    {
        Task<PagedResultOutput<CompanyListDto>> GetAll(GetCompanyInput inputDto);

        Task<FileDto> GetAllToExcel();

        Task<GetCompanyForEditOutput> GetCompanyForEdit(NullableIdInput nullableIdInput);

        Task<IdInput> CreateOrUpdateCompany(CreateOrUpdateCompanyInput input);

        Task DeleteCompany(IdInput input);

        Task<ListResultOutput<CompanyListDto>> GetNames();

        Task<IdInput> GetCompanyIdByName(string companyname);

        Task ActivateItem(IdInput input);

    }
}