﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Authorization.Users.Dto;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Organizations.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master
{
    public interface ILocationAppService : IApplicationService
    {
        Task<BrandsAndLocationOutputList> ApiGetBrandsAndLocation(BrandsAndLocationInput inputDto);

        Task<PagedResultDto<SimpleLocationDto>> GetSimpleAll(GetLocationInput inputDto);

        Task<bool> IsLocationExists(CheckLocationInput inputDto);

        List<int> GetLocationForUserAndGroup(LocationGroupDto locationDto);
        List<int> GetLocationForUserAndLocationTag(LocationGroupDto locationDto);
        Task<PagedResultDto<LocationListDto>> GetAll(GetLocationInput inputDto);

        Task<ListResultDto<LocationListDto>> GetLocations();

        Task<ListResultDto<LocationScheduleDto>> GetLocationAndSchedules(GetLocationInput input);

        Task<ListResultDto<LocationListDto>> GetLocationsForLoginUser();

        Task<ListResultDto<SimpleLocationDto>> GetSimpleLocations();

        Task<FileDto> GetAllToExcel();

        Task<GetLocationForEditOutput> GetLocationForEdit(NullableIdInput NullableIdDto);

        Task CreateOrUpdateLocation(CreateOrUpdateLocationInput input);

        Task DeleteLocation(EntityDto input);

        Task CreatLocations(List<LocationEditDto> inputDto);

        Task<ListResultDto<SimpleLocationDto>> GetLocationsByLoginUser();

        Task AddItemToLocation(MenuItemToLocationInput input);

        Task RemoveItemFromLocation(MenuItemToLocationInput input);

        Task<bool> IsItemInLocation(MenuItemToLocationInput input);

        Task<int> GetLocationCount();

        Task<PagedResultDto<LocationMenuItemListDto>> GetMenuItemForLocation(GetOrganizationUnitUsersInput input);

        Task<PagedResultDto<LocationListDto>> GetLocationBasedOnUser(GetLocationInputBasedOnUser input);

        EntityDto GetLocationIdByName(string locationName);

        Task<LocationListDto> GetLocationByName(string locationName);

        Task<LocationListDto> GetLocationById(EntityDto location);

        Task<List<LocationListDto>> GetLocationsFromJson(GetObjectFromString argJsonText);

        Task<ListResultDto<ComboboxItemDto>> GetLocationForCombobox();

        Task<ListResultDto<ComboboxItemDto>> GetCompanyForCombobox();

        Task<ListResultDto<ApiLocationOutput>> ApiGetLocationForUserTest(ApiUserInput input);

        Task<ListResultDto<ApiLocationOutput>> ApiGetLocationForUser(ApiUserInput input);

        Task<ListResultDto<ApiLocationOutput>> ApiGetPurchaseLocationForUser(ApiUserInput input);

        Task<ListResultDto<ComboboxItemDto>> GetLocationForProudctionUnitCombobox();

        Task<PagedResultDto<LocationListDto>> GetLocationWithProductionUnits(GetLocationInput input);

        Task<PagedResultDto<ProductionUnitListDto>> GetProductionUnits(EntityDto input);

        Task<UserEditDto> GetUserInfoBasedOnUserId(EntityDto input);

        Task<PagedResultDto<LocationListDto>> GetLocationsForGivenLocationGroupId(GetLocationGroupInput input);

        Task<ListResultDto<ComboboxItemDto>> GetLocationGroupForCombobox();

        Task<ListResultDto<ComboboxItemDto>> GetLocationsByLocationGroupName(string name);

        Task<ListResultDto<ComboboxItemDto>> GetLocationsByLocationGroupCode(string code);
        Task<ListResultDto<ComboboxItemDto>> GetLocationsByLocationGroupCodeAndLastSyncTime(string tag, DateTime lastSyncTime);

        Task UpdateLocationGroup(UpdateLocationGroup input);

        Task RemoveLocationFromLocationGroup(UpdateLocationGroup input);

        Task<PagedResultDto<LocationListDto>> LocationListWithOutGroup(GetLocationInput input);

        Task<PagedResultDto<SimpleLocationDto>> GetLocationsAllForGivenOrganisationList(GetLocationInput input);

        Task<int> GetOrgnizationIdForLocationId(int locationId);

        Task<Company> GetCompanyInfo(ApiLocationInput locationInput);

        Task<ApiLocationSetting> ApiGetLocationSettings(ApiLocationInput locationInput);

        Task<ApiReceiptDetails> ApiLocationReceiptDetails(ApiLocationInput locationInput);

        Task<PagedResultDto<LocationListDto>> GetLocationsForGivenLocationTagId(GetLocationTagInput input);

        Task UpdateLocationTag(UpdateLocationTag input);

        Task<PagedResultDto<LocationListDto>> LocationListWithOutTag(GetLocationInput input);

        Task RemoveLocationFromLocationTag(UpdateLocationTag input);

        Task ActivateItem(EntityDto input);

        List<SimpleLocationDto> GetLocationsForUser(GetLocationInputBasedOnUser input);

        Task<List<ScheduleSetting>> GetLocationSchedulesByLocation(int locationId);

        Task<ListResultDto<ComboboxItemDto>> GetLocationsByLocationTagName(string name);

        Task<ListResultDto<SimpleLocationDto>> GetLocationsForLocationGroupDto(LocationGroupDto lgDto);

        Task UpdateLocationReceipt(CreateOrUpdateLocationInput input);

        Task<List<Location>> ApiGetAllLocationsForSync(ApiLocationInput input);

    }
}