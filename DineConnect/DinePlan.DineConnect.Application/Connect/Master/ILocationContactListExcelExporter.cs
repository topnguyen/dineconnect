﻿
using System.Collections.Generic;
using DinePlan.DineConnect.House.Master.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;

namespace DinePlan.DineConnect.Connect.Master
{
    public interface ILocationContactListExcelExporter
    {
        FileDto ExportToFile(List<LocationContactListDto> dtos);
    }
}