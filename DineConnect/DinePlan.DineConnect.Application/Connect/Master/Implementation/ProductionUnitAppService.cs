﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;
using Abp.UI;
using DinePlan.DineConnect.House.Impl;
using DinePlan.DineConnect.Utility.Exporter;
using DinePlan.DineConnect.House;
using DinePlan.DineConnect.Utility;

namespace DinePlan.DineConnect.Connect.Master.Implementation
{
    public class ProductionUnitAppService : DineConnectAppServiceBase, IProductionUnitAppService
    {

        private readonly IExcelExporter _exporter;
        private readonly IProductionUnitManager _productionunitManager;
        private readonly IRepository<ProductionUnit> _productionunitRepo;
        private readonly IRepository<Location> _locationRepo;

        public ProductionUnitAppService(IProductionUnitManager productionunitManager,
            IRepository<ProductionUnit> productionUnitRepo,
            IRepository<Location> locationRepo,
            IExcelExporter exporter)
        {
            _productionunitManager = productionunitManager;
            _productionunitRepo = productionUnitRepo;
            _exporter = exporter;
            _locationRepo = locationRepo;
        }

        public async Task<PagedResultOutput<ProductionUnitListDto>> GetAll(GetProductionUnitInput input)
        {
            var allItems = (from pu in _productionunitRepo.GetAll()
                            join lo in _locationRepo.GetAll() on pu.LocationRefId equals lo.Id
                            select new ProductionUnitListDto
                            {
                                Id = pu.Id,
                                LocationRefId = lo.Id,
                                LocationRefName = lo.Name,
                                Code = pu.Code,
                                Name = pu.Name,
                                IsActive = pu.IsActive
                            }).WhereIf(!input.Filter.IsNullOrEmpty(), t => t.Code.Contains(input.Filter)).
                                  WhereIf(!input.Filter.IsNullOrEmpty(), t => t.LocationRefName.Contains(input.Filter));

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<ProductionUnitListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<ProductionUnitListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel()
        {


            var allList = await _productionunitRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<ProductionUnitListDto>>();
            var baseE = new BaseExportObject()
            {
                ExportObject = allListDtos,
                ColumnNames = new string[] { "Id" }
            };
            return _exporter.ExportToFile(baseE, L("ProductionUnit"));

        }

        public async Task<GetProductionUnitForEditOutput> GetProductionUnitForEdit(NullableIdInput input)
        {
            ProductionUnitEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _productionunitRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<ProductionUnitEditDto>();
            }
            else
            {
                editDto = new ProductionUnitEditDto();
            }

            return new GetProductionUnitForEditOutput
            {
                ProductionUnit = editDto
            };
        }

        public async Task CreateOrUpdateProductionUnit(CreateOrUpdateProductionUnitInput input)
        {
            if (input.ProductionUnit.Id.HasValue)
            {
                await UpdateProductionUnit(input);
            }
            else
            {
                await CreateProductionUnit(input);
            }
        }

        public async Task DeleteProductionUnit(IdInput input)
        {
            await _productionunitRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateProductionUnit(CreateOrUpdateProductionUnitInput input)
        {
            var item = await _productionunitRepo.GetAsync(input.ProductionUnit.Id.Value);
            var dto = input.ProductionUnit;
            item.Name = dto.Name;
            item.LocationRefId = dto.LocationRefId;
            item.Code = dto.Code;
            item.IsActive = dto.IsActive;



            //TODO: SERVICE ProductionUnit Update Individually

            CheckErrors(await _productionunitManager.CreateSync(item));
        }

        protected virtual async Task CreateProductionUnit(CreateOrUpdateProductionUnitInput input)
        {
            var dto = input.ProductionUnit.MapTo<ProductionUnit>();

            CheckErrors(await _productionunitManager.CreateSync(dto));
        }

        public async Task<ListResultOutput<ProductionUnitListDto>> GetCodes()
        {
            var lstProductionUnit = await _productionunitRepo.GetAll().ToListAsync();
            return new ListResultOutput<ProductionUnitListDto>(lstProductionUnit.MapTo<List<ProductionUnitListDto>>());
        }
    }
}

