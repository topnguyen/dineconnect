﻿using Abp.Application.Features;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Collections.Extensions;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Organizations;
using Abp.UI;
using Castle.Core.Logging;
using DinePlan.DineConnect.Addons.Dto;
using DinePlan.DineConnect.Authorization.Users.Dto;
using DinePlan.DineConnect.Configuration;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Menu.Dtos;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.EntityFramework;
using DinePlan.DineConnect.Features;
using DinePlan.DineConnect.House;
using DinePlan.DineConnect.House.MaterialStock;
using DinePlan.DineConnect.House.Transaction.Dtos;
using DinePlan.DineConnect.Job.House;
using DinePlan.DineConnect.Organizations.Dto;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Utility.Exporter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Linq.Dynamic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.Implementation
{
    public class LocationAppService : DineConnectAppServiceBase, ILocationAppService
    {
        private readonly IRepository<Company> _companyRepo;
        private readonly IExcelExporter _excelExporter;
        private readonly IRepository<LocationGroup> _locationGroupRepo;
        private readonly ILocationManager _locationManager;
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<LocationSchdule> _locationScheduleRepo;
        private readonly ILogger _logger;
        private readonly IMaterialStockAppService _matLocStockAppService;
        private readonly IRepository<OrganizationUnit, long> _oum;
        private readonly IRepository<ProductionUnit> _prodUnitRepo;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly ISyncAppService _syncAppService;
        private readonly IRepository<LocationTag> _locationTagRepo;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly FeatureChecker _featureChecker;
        private readonly DineConnectDbContext _context;
        private readonly IRepository<LocationBranch> _branchRepo;
        private readonly IBackgroundJobManager _bgm;

        public LocationAppService(IExcelExporter excelExporter, IRepository<OrganizationUnit, long> oum,
            ILogger logger, FeatureChecker featureChecker, IRepository<LocationBranch> branchRepo,
            ISyncAppService syncAppService,
            ILocationManager locationManager, IRepository<Location> locationRepo,
            IRepository<LocationSchdule> locationScheduleRepo,
            IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository,
            IMaterialStockAppService matLocStockAppService,
            IRepository<Company> companyRepo,
            IRepository<ProductionUnit> prodUnitRepo,
            IRepository<LocationGroup> locationGroupRepo,
            IRepository<LocationTag> locationTagRepo,
            DineConnectDbContext context,
            IUnitOfWorkManager unitOfWorkManager,
            IBackgroundJobManager bgm
            )
        {
            _context = context;
            _featureChecker = featureChecker;
            _locationRepo = locationRepo;
            _excelExporter = excelExporter;
            _oum = oum;
            _logger = logger;
            _locationManager = locationManager;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _companyRepo = companyRepo;
            _matLocStockAppService = matLocStockAppService;
            _prodUnitRepo = prodUnitRepo;
            _locationScheduleRepo = locationScheduleRepo;
            _locationGroupRepo = locationGroupRepo;
            _locationTagRepo = locationTagRepo;
            _unitOfWorkManager = unitOfWorkManager;
            _syncAppService = syncAppService;
            _branchRepo = branchRepo;
            _bgm = bgm;
        }

        public async Task<UserEditDto> GetUserInfoBasedOnUserId(EntityDto input)
        {
            UserEditDto output;

            var user = await UserManager.GetUserByIdAsync(input.Id);
            output = user.MapTo<UserEditDto>();

            return output;
        }

        public async Task<BrandsAndLocationOutputList> ApiGetBrandsAndLocation(BrandsAndLocationInput inputDto)
        {
            BrandsAndLocationOutputList output = new BrandsAndLocationOutputList();

            var locationGroupAsBrand = await
                    SettingManager.GetSettingValueForTenantAsync<bool>(AppSettings.ConnectSettings.LocationGroupAsBrand, inputDto.TenantId);

            if (locationGroupAsBrand)
            {
                if (!string.IsNullOrEmpty(inputDto.Group))
                {
                    var lastLoc = await _locationGroupRepo.FirstOrDefaultAsync(a => a.Code != null && a.Code.Equals(inputDto.Group) && a.TenantId == inputDto.TenantId);
                    if (lastLoc != null)
                    {
                        foreach (var myLoc in lastLoc.Locations)
                        {
                            if (!myLoc.QuickDineInActive)
                            {
                                output.Brands.Add(new BrandsAndLocationOutput()
                                {
                                    BrandId = myLoc.Id,
                                    BrandName = myLoc.Name,
                                    BrandCode = myLoc.Code
                                });
                            }
                        }
                    }
                }
            }
            else
            {
                var allCompanies = _companyRepo.GetAll().Where(a=>a.TenantId == inputDto.TenantId);

                foreach (var myCom in allCompanies)
                {
                    var myBra = new BrandsAndLocationOutput()
                    {
                        BrandId = myCom.Id,
                        BrandName = myCom.Name,
                        BrandCode = myCom.Code
                    };
                    foreach (var myLoc in _locationRepo.GetAll().Where(a => a.CompanyRefId == myCom.Id && a.TenantId == inputDto.TenantId))
                    {
                        if (!myLoc.QuickDineInActive)
                        {
                            myBra.AllLocations.Add(new SimpleLocationDto()
                            {
                                Code = myLoc.Code,
                                Id = myLoc.Id,
                                CompanyRefId = myLoc.CompanyRefId,
                                Name = myLoc.Name
                            });
                        }
                    }
                    output.Brands.Add(myBra);
                }
            }

            return output;
        }

        public async Task<PagedResultDto<SimpleLocationDto>> GetSimpleAll(GetLocationInput input)
        {
            var allLocations = GetLocationByUserId(input.UserId);

            var allItems = (from loc in allLocations
                            select new SimpleLocationDto
                            {
                                Id = loc.Id,
                                Code = loc.Code,
                                Name = loc.Name,
                                CompanyRefId = loc.CompanyRefId
                            }).WhereIf(
                                !input.Filter.IsNullOrEmpty(),
                                p => p.Name.Contains(input.Filter)
                                     || (p.Code != null
                                     && p.Code.Contains(input.Filter)) || p.Id.ToString().Contains(input.Filter));
            if (input.LocationRefId > 0)
            {
                allItems = allItems.Where(t => t.Id == input.LocationRefId);
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<SimpleLocationDto>>();
            var allItemCount = await allItems.CountAsync();
            return new PagedResultDto<SimpleLocationDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<bool> IsLocationExists(CheckLocationInput input)
        {
            if (!string.IsNullOrEmpty(input.NonLocations))
            {
                var allLocations =
                    JsonConvert.DeserializeObject<List<SimpleLocationDto>>(input.NonLocations);
                var allIds = allLocations.Select(a => a.Id).ToList();
                return !allIds.Contains(input.LocationId);
            }

            if (input.Group && !string.IsNullOrEmpty(input.Locations))
            {
                var myG = JsonConvert.DeserializeObject<List<SimpleLocationGroupDto>>(input.Locations);
                var listId = new List<int>();
                foreach(var group in myG)
                {
                    var locationGroup = await _locationGroupRepo.GetAll()
                        .Include(lg => lg.Locations)
                        .Where(s=>s.Id == group.Id)
                        .FirstOrDefaultAsync();
                    listId.AddRange(locationGroup.Locations.Select(s=>s.Id));
                }    
               
                var location = await _locationRepo.GetAsync(input.LocationId);

                if (location != null && location.LocationGroups.Any())
                {
                    return listId.Any(a => a == location.Id);
                }
                return false;
            }
            if (input.LocationTag && !string.IsNullOrEmpty(input.Locations))
            {
                var myG = JsonConvert.DeserializeObject<List<SimpleLocationGroupDto>>(input.Locations);
                var allIds = new List<int>();
                foreach(var group in myG)
                {
                    var findLocationTag = await _locationTagRepo.GetAll()
                                 .Include(lg => lg.Locations)
                                 .Where(lg => lg.Id == group.Id)
                                 .FirstOrDefaultAsync();
                    allIds.AddRange(findLocationTag.Locations.Select(s => s.Id));
                }
               
                var locationTag = await _locationRepo.GetAsync(input.LocationId);

                if (locationTag != null && locationTag.LocationTags.Any())
                {
                    return allIds.Any(a => a == locationTag.Id);
                }
                return false;
            }
            if (!input.Group && !input.LocationTag && !string.IsNullOrEmpty(input.Locations))
            {
                var allLocations =
                    JsonConvert.DeserializeObject<List<SimpleLocationDto>>(input.Locations);

                var allIds = allLocations.Select(a => a.Id).ToList();
                return allIds.Any(a => a == input.LocationId);
            }
            return true;
        }

        public List<int> GetLocationForUserAndGroup(LocationGroupDto dto)
        {
            var rLocations = new List<int>();
            if (!dto.Group && !dto.Locations.Any())
            {
                var userId = AbpSession.UserId ?? dto.UserId;
                if (userId > 0)
                {
                    var allLocations = GetLocationsForUser(new GetLocationInputBasedOnUser { UserId = userId });
                    if (allLocations != null && allLocations.Any())
                    {
                        rLocations = allLocations.Select(a => a.Id).ToList();
                    }
                }
            }
            else if (!dto.Group)
            {
                rLocations = dto.Locations.Select(a => a.Id).ToList();
            }
            else
            {
                var allLgids = dto.Groups.Select(a => a.Id).ToList();
                var allLoca =
                    _locationRepo.GetAll()
                        .Where(a => a.LocationGroups.Where(lg => allLgids.Contains(lg.Id)).Any());

                var userId = AbpSession.UserId ?? dto.UserId;
                if (userId > 0)
                {
                    var allLocations = GetLocationsForUser(new GetLocationInputBasedOnUser { UserId = userId }).Select(a => a.Id).ToList();
                    rLocations = allLoca.Where(a => allLocations.Contains(a.Id)).Select(a => a.Id).ToList();
                }
            }
            return rLocations;
        }

        public List<int> GetLocationForUserAndLocationTag(LocationGroupDto dto)
        {
            var rLocations = new List<int>();
            if (!dto.LocationTag && !dto.Locations.Any())
            {
                var userId = AbpSession.UserId ?? dto.UserId;
                if (userId > 0)
                {
                    var allLocations = GetLocationsForUser(new GetLocationInputBasedOnUser { UserId = userId });
                    if (allLocations != null && allLocations.Any())
                    {
                        rLocations = allLocations.Select(a => a.Id).ToList();
                    }
                }
            }
            else if (!dto.LocationTag)
            {
                rLocations = dto.Locations.Select(a => a.Id).ToList();
            }
            else
            {
                var allLgids = dto.LocationTags.Select(a => a.Id).ToList();
                var allLoca =
                    _locationRepo.GetAll()
                        .Where(a => a.LocationTags.Where(lg => allLgids.Contains(lg.Id)).Any());

                var userId = AbpSession.UserId ?? dto.UserId;
                if (userId > 0)
                {
                    var allLocations = GetLocationsForUser(new GetLocationInputBasedOnUser { UserId = userId }).Select(a => a.Id).ToList();
                    rLocations = allLoca.Where(a => allLocations.Contains(a.Id)).Select(a => a.Id).ToList();
                }
            }
            return rLocations;
        }


        public async Task<PagedResultDto<LocationListDto>> GetAll(GetLocationInput input)
        {
            Debug.WriteLine("LocationAppService GetAll Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                if (input.MaxResultCount == 0 || input.MaxResultCount == 20)
                {
                    input.MaxResultCount = AppConsts.MaxPageSize;
                }

                var allItems = (from loc in _locationRepo.GetAll().
                        Where(i => i.IsDeleted == input.IsDeleted)
                                join com in _companyRepo.GetAll()
                                    on loc.CompanyRefId equals com.Id
                                select new LocationListDto
                                {
                                    Id = loc.Id,
                                    Company = com,
                                    CompanyName = com.Name,
                                    CompanyRefId = com.Id,
                                    Code = loc.Code,
                                    Name = loc.Name,
                                    Address1 = loc.Address1,
                                    City = loc.City,
                                    HouseTransactionDate = loc.HouseTransactionDate,
                                    PhoneNumber = loc.PhoneNumber,
                                    Country = loc.Country,
                                    ExtendedBusinessHours = loc.ExtendedBusinessHours,
                                    DoesDayCloseRunInBackGround = loc.DoesDayCloseRunInBackGround,
                                    BackGroundStartTime = loc.BackGroundStartTime,
                                    BackGrandEndTime = loc.BackGrandEndTime,
                                    TransferRequestLockHours = loc.TransferRequestLockHours,
                                    TransferRequestGraceHours = loc.TransferRequestGraceHours,
                                    AvgPriceLocationRefId = loc.AvgPriceLocationRefId,
                                    DayCloseSalesStockAdjustmentLocationRefId = loc.DayCloseSalesStockAdjustmentLocationRefId
                                }).WhereIf(
                        !input.Filter.IsNullOrEmpty(),
                        p => p.Name.Contains(input.Filter)
                             || p.Code != null && p.Code.Contains(input.Filter)
                             || p.Id.ToString().Contains(input.Filter)
                    );

                var sortMenuItems = await allItems
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var allListDtos = sortMenuItems.MapTo<List<LocationListDto>>();

                var allItemCount = await allItems.CountAsync();

                if (allListDtos.Where(t => t.AvgPriceLocationRefId.HasValue).Any())
                {
                    foreach (var gp in allListDtos.Where(t => t.AvgPriceLocationRefId.HasValue).GroupBy(t => t.AvgPriceLocationRefId))
                    {
                        var avgPriceLocation = await _locationRepo.FirstOrDefaultAsync(t => t.Id == gp.Key);
                        foreach (var loc in gp.ToList())
                        {
                            loc.AvgPriceLocationRefCode = avgPriceLocation.Code + " - " + avgPriceLocation.Name;
                        }
                    }
                }

                if (allListDtos.Where(t => t.DayCloseSalesStockAdjustmentLocationRefId.HasValue).Count() > 0)
                {
                    foreach (var gp in allListDtos.Where(t => t.DayCloseSalesStockAdjustmentLocationRefId.HasValue).GroupBy(t => t.DayCloseSalesStockAdjustmentLocationRefId))
                    {
                        var daycloseStkAdjLocation = await _locationRepo.FirstOrDefaultAsync(t => t.Id == gp.Key);
                        foreach (var loc in gp.ToList())
                        {
                            loc.DayCloseSalesStockAdjustmentLocationRefCode = daycloseStkAdjLocation.Code + " - " + daycloseStkAdjLocation.Name;
                        }
                    }
                }

                return new PagedResultDto<LocationListDto>(
                    allItemCount,
                    allListDtos
                    );
            }
        }

        public EntityDto GetLocationIdByName(string locationName)
        {
            var lstLocId = _locationRepo.GetAllList(a => a.Name.Equals(locationName)).ToList().FirstOrDefault();

            return new EntityDto
            {
                Id = lstLocId?.Id ?? 0
            };
        }

        public async Task<LocationListDto> GetLocationByName(string locationName)
        {
            var allLo = await _locationRepo.GetAllListAsync(a => a.Name.Equals(locationName));
            if (allLo.Any())
            {
                var loc = allLo.FirstOrDefault();
                if (loc == null)
                    return null;

                return new LocationListDto
                {
                    Id = loc.Id,
                    Code = loc.Code,
                    Name = loc.Name,
                    City = loc.City
                };
            }
            return null;
        }

        public async Task<LocationListDto> GetLocationById(EntityDto location)
        {
            var output = new LocationListDto();
            if (location != null && location.Id > 0)
            {
                var loc = await _locationRepo.FirstOrDefaultAsync(location.Id);
                if (loc != null)
                    output = loc.MapTo<LocationListDto>();
            }
            return output;
        }

        public async Task<PagedResultDto<LocationListDto>> GetLocationBasedOnUser(GetLocationInputBasedOnUser input)
        {
            Debug.WriteLine("LocationAppService GetLocationBasedOnUser() Start Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));

            IQueryable<Location> allItems;
            var userorg = await _userOrganizationUnitRepository.GetAllListAsync(a => a.UserId == input.UserId);
            allItems = _locationRepo.GetAll().Where(a => !a.IsDeleted);
            if (input.CompanyList != null && input.CompanyList.Count > 0)
            {
                var tempCompanies = input.CompanyList.Select(t => t.Value).ToArray();
                var arrCompanies = tempCompanies.Select(lst => int.Parse(lst)).ToList();
                allItems = allItems.Where(t => arrCompanies.Contains(t.CompanyRefId));
            }
            if (!input.LocationName.IsNullOrEmpty())
            {
                allItems =
                    allItems.Where(t => t.Name.Contains(input.LocationName) || t.Code.Contains(input.LocationName));
            }
            var sortMenuItems = await allItems.ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<LocationListDto>>();

            if (userorg.Count > 0)
            {
                List<long> arrRefIds = userorg.Select(t => t.OrganizationUnitId).ToList();

                allListDtos = allListDtos.Where(t => arrRefIds.Contains((long)t.Id)).ToList();  //(List<LocationListDto>)(from loc in allListDtos 
                
            }

            var allItemCount = await allItems.CountAsync();
            Debug.WriteLine("LocationAppService GetLocationBasedOnUser() End Time " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss:ffff"));
            return new PagedResultDto<LocationListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<ListResultDto<ApiLocationOutput>> ApiGetLocationForUserTest(ApiUserInput input)
        {
            IQueryable<Location> allItems;

            var user = await UserManager.GetUserByIdAsync(input.UserId);
            if (user == null)
            {
                return null;
            }

            var userorg = await _userOrganizationUnitRepository.GetAllListAsync(a => a.UserId == input.UserId);
            if (userorg.Count == 0)
            {
                allItems = _locationRepo.GetAll();
            }
            else
            {
                allItems = from loc in _locationRepo.GetAll()
                           join ou in _userOrganizationUnitRepository.GetAll().Where(a => a.UserId == input.UserId)
                               on loc.Id equals ou.OrganizationUnitId
                           select loc;
            }
            var sortLocationOutput = await allItems.ToListAsync();

            var locationOuputs = sortLocationOutput.Select(sortL => new ApiLocationOutput
            {
                Id = sortL.Id,
                Code = sortL.Code,
                Name = sortL.Name
            }).ToList();

            return new ListResultDto<ApiLocationOutput>(locationOuputs);
        }

        [AbpAuthorize]
        public async Task<ListResultDto<ApiLocationOutput>> ApiGetLocationForUser(ApiUserInput input)
        {
            IQueryable<Location> allItems;

            var user = await UserManager.GetUserByIdAsync(input.UserId);
            if (user == null)
            {
                return null;
            }

            var userorg = await _userOrganizationUnitRepository.GetAllListAsync(a => a.UserId == input.UserId);
            if (userorg.Count == 0)
            {
                allItems = _locationRepo.GetAll();
            }
            else
            {
                allItems = from loc in _locationRepo.GetAll()
                           join ou in _userOrganizationUnitRepository.GetAll().Where(a => a.UserId == input.UserId)
                               on loc.Id equals ou.OrganizationUnitId
                           select loc;
            }
            var sortLocationOutput = await allItems.ToListAsync();

            var locationOuputs = sortLocationOutput.Select(sortL => new ApiLocationOutput
            {
                Id = sortL.Id,
                Code = sortL.Code,
                Name = sortL.Name
            }).ToList();

            return new ListResultDto<ApiLocationOutput>(locationOuputs);
        }

        public async Task<ListResultDto<ApiLocationOutput>> ApiGetPurchaseLocationForUser(ApiUserInput input)
        {
            IQueryable<Location> allItems;

            var user = await UserManager.GetUserByIdAsync(input.UserId);
            if (user == null)
            {
                return null;
            }

            var userorg = await _userOrganizationUnitRepository.GetAllListAsync(a => a.UserId == input.UserId);
            if (userorg.Count == 0)
            {
                allItems = _locationRepo.GetAll();
            }
            else
            {
                allItems = from loc in _locationRepo.GetAll()
                           join ou in _userOrganizationUnitRepository.GetAll().Where(a => a.UserId == input.UserId)
                               on loc.Id equals ou.OrganizationUnitId
                           select loc;
            }
            var sortLocationOutput = await allItems.Where(a => a.IsPurchaseAllowed).ToListAsync();

            var locationOuputs = sortLocationOutput.Select(sortL => new ApiLocationOutput
            {
                Id = sortL.Id,
                Code = sortL.Code,
                Name = sortL.Name
            }).ToList();

            return new ListResultDto<ApiLocationOutput>(locationOuputs);
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await GetAll(new GetLocationInput { MaxResultCount = AppConsts.MaxPageSize, SkipCount=0,Sorting="Id" });

            var allListDtos = allList.Items.MapTo<List<LocationListDto>>();
            var baseE = new BaseExportObject
            {
                ExportObject = allListDtos,
                ColumnNames =
                    new[]
                    {
                        "CompanyName", "Code", "Name", "Address1", "Address2", "Address3", "City", "State", "Country",
                        "PhoneNumber", "Fax", "Website", "Email", "AvgPriceLocationRefCode", "DayCloseSalesStockAdjustmentLocationRefCode"
                    }
            };

            return _excelExporter.ExportToFile(baseE, L("Location"));
        }

        public async Task<GetLocationForEditOutput> GetLocationForEdit(NullableIdInput input)
        {
            LocationEditDto editDto;
            LocationBranchEditDto editLocationBranchDto = new LocationBranchEditDto();

            if (input.Id.HasValue)
            {
                var hDto = await _locationRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<LocationEditDto>();
                var rslocationBranch = await _branchRepo.FirstOrDefaultAsync(t => t.Id == editDto.LocationBranchId);
                if (rslocationBranch != null)
                {
                    editLocationBranchDto = rslocationBranch.MapTo<LocationBranchEditDto>();
                }
            }
            else
            {
                editDto = new LocationEditDto();
            }

            var allLocations = new List<LocationListDto>();

            if (!string.IsNullOrEmpty(editDto.RequestedLocations))
            {
                allLocations = JsonConvert.DeserializeObject<List<LocationListDto>>(editDto.RequestedLocations);
            }

            UrbanPiperAddOn uAddOn = null;
            AddOnOutput aAddOn;

            if (await _featureChecker.IsEnabledAsync(AbpSession.TenantId ?? 0, AppFeatures.UrbanPiper))
            {
                aAddOn = new AddOnOutput();
                if (string.IsNullOrEmpty(editDto.AddOn))
                {
                    uAddOn = new UrbanPiperAddOn();
                    aAddOn.UrbanPiper = uAddOn;
                }
                else
                {
                    aAddOn = JsonConvert.DeserializeObject<AddOnOutput>(editDto.AddOn);
                    uAddOn = aAddOn.UrbanPiper;
                }
                editDto.AddOn = JsonConvert.SerializeObject(aAddOn);
            }

            return new GetLocationForEditOutput
            {
                Location = editDto,
                LocationBranch = editLocationBranchDto,
                RequestLocations = allLocations,
                UrbanPiper = uAddOn
            };
        }

        [UnitOfWork]
        public async Task CreateOrUpdateLocation(CreateOrUpdateLocationInput input)
        {
            await ValidateForCreateOrUpdate(input.Location);

            var allLocations = "";
            if (input.RequestLocations != null && input.RequestLocations.Any())
            {
                allLocations = JsonConvert.SerializeObject(input.RequestLocations);
            }

            if (input.Location.Id.HasValue)
            {
                if (await _featureChecker.IsEnabledAsync(AbpSession.TenantId ?? 0, AppFeatures.UrbanPiper))
                {
                    if (input.UrbanPiper != null)
                    {
                        AddOnOutput addOnOutput = new AddOnOutput { UrbanPiper = input.UrbanPiper };
                        input.Location.AddOn = JsonConvert.SerializeObject(addOnOutput);
                    }
                }
                await UpdateLocation(input, allLocations);
            }
            else
            {
                await CreateLocation(input.Location, input.LocationBranch, allLocations, false);
            }

            await _syncAppService.UpdateSync(SyncConsts.LOCATION);
        }

        [UnitOfWork]
        public async Task UpdateLocationReceipt(CreateOrUpdateLocationInput input)
        {
            if (input.Location?.Id == null) return;
            var loc = _locationRepo.Get(input.Location.Id.Value);

            if (loc != null && loc.OrganizationUnitId > 0)
            {
                try
                {
                    loc.ReceiptFooter = input.Location.ReceiptFooter;
                    loc.ReceiptHeader = input.Location.ReceiptHeader;
                    await _locationRepo.UpdateAsync(loc);
                }
                catch (Exception e)
                {
                    Logger.Fatal("Update Location", e);
                }

            }

        }
        public async Task<List<Location>> ApiGetAllLocationsForSync(ApiLocationInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                return await _locationRepo.GetAll().Where(a => a.TenantId == input.TenantId)
                    .Include(a=>a.LocationBranch)
                    .Include(a=>a.MenuItems)
                    .Include(a=>a.Prices)
                    .Include(a=>a.Schedules)

                    .Include(a=>a.LocationGroups)
                    .Include(a=>a.LocationTags).Include(a=>a.Company).ToListAsync();
            }
        }

        public async Task DeleteLocation(EntityDto input)
        {
            var loc = _locationRepo.Get(input.Id);
            if (loc != null && loc.OrganizationUnitId > 0)
            {
                try
                {
                    var oum = _oum.Get(loc.OrganizationUnitId);
                    await _oum.DeleteAsync(oum.Id);
                }
                catch (Exception e)
                {
                    Logger.Fatal("Delete Location", e);
                }

            }

            await _locationRepo.DeleteAsync(input.Id);
        }

        public async Task CreatLocations(List<LocationEditDto> inputDto)
        {
            LocationBranchEditDto locationBranchEditDto = new LocationBranchEditDto();
            var locationCount = FeatureChecker.GetValue(AppFeatures.ConnectLocationCount).To<int>();
            var currentLocationCount = _locationRepo.Count();
            if (currentLocationCount >= Convert.ToInt32(locationCount))
            {
                throw new UserFriendlyException("Contact Admin - Location Creation is not Allowed");
            }
            foreach (var locationEditDto in inputDto.OrderBy(a => a.CompanyRefId))
            {
                await CreateLocation(locationEditDto, locationBranchEditDto, "", true);
            }
        }

        public async Task AddItemToLocation(MenuItemToLocationInput input)
        {
            var location =
                await _locationRepo.FirstOrDefaultAsync(a => a.Id.Equals(input.OrganizationUnitId));

            location?.MenuItems.Add(new LocationMenuItem
            {
                MenuItemId = input.MenuItemId
            });
        }

        public async Task RemoveItemFromLocation(MenuItemToLocationInput input)
        {
            var location =
                await _locationRepo.FirstOrDefaultAsync(a => a.Id.Equals(input.OrganizationUnitId));

            var menuItem = location?.MenuItems.Find(a => a.Id.Equals(input.MenuItemId));
            if (menuItem != null)
            {
                try
                {
                    location.MenuItems.Remove(menuItem);
                }
                catch (Exception exception)
                {
                    var t = exception.Message;
                }
            }
        }

        public async Task<bool> IsItemInLocation(MenuItemToLocationInput input)
        {
            var location =
                await _locationRepo.FirstOrDefaultAsync(a => a.Id.Equals(input.OrganizationUnitId));
            var menuItem = location?.MenuItems.Find(a => a.MenuItemId.Equals(input.MenuItemId));
            return menuItem != null;
        }

        [AbpAuthorize]
        public async Task<int> GetLocationCount()
        {
            return await _locationRepo.CountAsync();
        }

        public async Task<PagedResultDto<LocationMenuItemListDto>> GetMenuItemForLocation(
            GetOrganizationUnitUsersInput input)
        {
            var location =
                await _locationRepo.FirstOrDefaultAsync(a => a.Id.Equals(input.Id));

            if (location == null || !DynamicQueryable.Any(location.MenuItems))
            {
                return new PagedResultDto<LocationMenuItemListDto>(
                    0,
                    null
                    );
            }
            ;
            var sortMenuItems = location.MenuItems;

            var allListDtos = sortMenuItems.MapTo<List<LocationMenuItemListDto>>();

            var allItemCount = DynamicQueryable.Count(sortMenuItems);

            return new PagedResultDto<LocationMenuItemListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<ListResultDto<LocationListDto>> GetLocations()
        {
            var lstLocation = await _locationRepo.GetAll().ToListAsync();
            return new ListResultDto<LocationListDto>(lstLocation.MapTo<List<LocationListDto>>());
        }

        public async Task<ListResultDto<SimpleLocationDto>> GetSimpleLocations()
        {
            var lstLocation = await _locationRepo.GetAll().ToListAsync();
            return new ListResultDto<SimpleLocationDto>(lstLocation.MapTo<List<SimpleLocationDto>>());
        }

        public async Task<ListResultDto<LocationScheduleDto>> GetLocationAndSchedules(GetLocationInput input)
        {
            var allItems = _locationRepo
              .GetAll()
              .WhereIf(
                  !input.Filter.IsNullOrWhiteSpace(),
                  p => p != null && (p.Name.Contains(input.Filter))
              );
            ;
            return new ListResultDto<LocationScheduleDto>(allItems.MapTo<List<LocationScheduleDto>>());
        }

        public async Task<ListResultDto<LocationListDto>> GetLocationsForLoginUser()
        {
            IQueryable<Location> allItems = null;
            if (AbpSession.UserId.HasValue)
            {
                allItems = GetLocationByUserId(AbpSession.UserId.Value);
            }
            if (allItems == null)
            {
                allItems = _locationRepo.GetAll();
            }
            var lstLocation = await allItems.ToListAsync();
            return new ListResultDto<LocationListDto>(lstLocation.MapTo<List<LocationListDto>>());
        }

        public async Task<ListResultDto<SimpleLocationDto>> GetLocationsByLoginUser()
        {
            IQueryable<Location> allItems = null;
            if (AbpSession.UserId.HasValue)
            {
                allItems = GetLocationByUserId(AbpSession.UserId.Value);
            }
            if (allItems == null || !allItems.Any())
            {
                allItems = _locationRepo.GetAll();
            }
            var lstLocation = await allItems.ToListAsync();
            return new ListResultDto<SimpleLocationDto>(lstLocation.MapTo<List<SimpleLocationDto>>());
        }

        public async Task<List<LocationListDto>> GetLocationsFromJson(GetObjectFromString input)
        {
            var allLocations = new List<LocationListDto>();

            var text = input.ToString();

            if (!string.IsNullOrEmpty(input.ObjectString))
            {
                allLocations = JsonConvert.DeserializeObject<List<LocationListDto>>(input.ObjectString);
            }
            return allLocations;
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetLocationForCombobox()
        {
            var lstLocation = await _locationRepo.GetAll().ToListAsync();

            return
                new ListResultDto<ComboboxItemDto>(
                    lstLocation.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetLocationGroupForCombobox()
        {
            var lstLocationGroup = await _locationGroupRepo.GetAll().ToListAsync();

            return
                new ListResultDto<ComboboxItemDto>(
                    lstLocationGroup.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetLocationsByLocationGroupCode(string code)
        {
            var locGroup = await _locationGroupRepo.FirstOrDefaultAsync(a => a.Code != null &&
                                                                             a.Code.Equals(code));
            if (locGroup != null)
            {
                var locationGroup = await _locationGroupRepo.GetAll()
                    .Include(lg => lg.Locations)
                    .WhereIf(code != null, lg => lg.Id == locGroup.Id)
                    .FirstOrDefaultAsync();

                if (locationGroup != null)
                {
                    var query = locationGroup.Locations.Select(l => new ComboboxItemDto
                    {
                        DisplayText = l.Name,
                        Value = l.Id.ToString(),
                    }).ToList();

                    return new ListResultDto<ComboboxItemDto>(query);
                }
            }
            return null;
        }


        public async Task<ListResultDto<ComboboxItemDto>> GetLocationsByLocationGroupCodeAndLastSyncTime(string code, DateTime lastSyncTime)
        {
            var locGroup = await _locationGroupRepo.FirstOrDefaultAsync(a => a.Code != null &&
                                                                             a.Code.Equals(code));
            if (locGroup != null)
            {
                var locationGroup = await _locationGroupRepo.GetAll()
                    .Include(lg => lg.Locations)
                    .WhereIf(code != null, lg => lg.Id == locGroup.Id)
                    .FirstOrDefaultAsync();

                if (locationGroup != null)
                {
                    var query = locationGroup.Locations.Select(l => new ComboboxItemDto
                    {
                        DisplayText = l.Name,
                        Value = l.Id.ToString(),
                    }).ToList();

                    return new ListResultDto<ComboboxItemDto>(query);
                }
            }
            return null;
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetLocationsByLocationGroupName(string name)
        {
            var locGroup = await _locationGroupRepo.FirstOrDefaultAsync(a => a.Name.Equals(name));
            if (locGroup != null)
            {
                var locationGroup = await _locationGroupRepo.GetAll()
                    .Include(lg => lg.Locations)
                    .WhereIf(name != null, lg => lg.Id == locGroup.Id)
                    .FirstOrDefaultAsync();

                if (locationGroup != null)
                {
                    var query = locationGroup.Locations.Select(l => new ComboboxItemDto
                    {
                        DisplayText = l.Name,
                        Value = l.Id.ToString(),
                    }).ToList();

                    return new ListResultDto<ComboboxItemDto>(query);
                }
            }
            return null;
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetLocationsByLocationTagName(string name)
        {
            var locGroup = await _locationTagRepo.FirstOrDefaultAsync(a => a.Name.Equals(name));
            if (locGroup != null)
            {
                var locationGroup = await _locationTagRepo.GetAll()
                    .Include(lg => lg.Locations)
                    .WhereIf(name != null, lg => lg.Id == locGroup.Id)
                    .FirstOrDefaultAsync();

                if (locationGroup != null)
                {
                    var query = locationGroup.Locations.Select(l => new ComboboxItemDto
                    {
                        DisplayText = l.Name,
                        Value = l.Id.ToString(),
                    }).ToList();

                    return new ListResultDto<ComboboxItemDto>(query);
                }
            }
            return null;
        }

        public async Task<ListResultDto<SimpleLocationDto>> GetLocationsForLocationGroupDto(LocationGroupDto input)
        {
            List<SimpleLocationDto> sLocations = new List<SimpleLocationDto>();

            if (input.Locations.Any() && !input.LocationTag && !input.Group)
            {
                sLocations = input.Locations;
            }
            if (input.LocationTag && input.LocationTags.Any())
            {
                foreach (var simpleLocationGroupDto in input.LocationTags)
                {
                    var allLocations =
                        await GetLocationsByLocationTagName(simpleLocationGroupDto.Name);
                    if (allLocations.Items.Any())
                    {
                        sLocations.AddRange(allLocations.Items.Select(a => new SimpleLocationDto()
                        {
                            Code = a.DisplayText,
                            Name = a.DisplayText,
                            Id = Convert.ToInt32(a.Value)
                        }));
                    }
                }
            }
            if (input.Group && input.Groups.Any())
            {
                foreach (var simpleLocationGroupDto in input.Groups)
                {
                    var allLocations =
                        await GetLocationsByLocationGroupName(simpleLocationGroupDto.Name);
                    if (allLocations.Items.Any())
                    {
                        sLocations.AddRange(allLocations.Items.Select(a => new SimpleLocationDto()
                        {
                            Code = a.DisplayText,
                            Name = a.DisplayText,
                            Id = Convert.ToInt32(a.Value)
                        }));
                    }
                }
            }

            return new ListResultOutput<SimpleLocationDto>(sLocations);
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetLocationForProudctionUnitCombobox()
        {
            var lstLocation = await _locationRepo.GetAll().Where(t => t.IsProductionUnitAllowed).ToListAsync();

            return
                new ListResultDto<ComboboxItemDto>(
                    lstLocation.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetCompanyForCombobox()
        {
            var lst = await _companyRepo.GetAll().ToListAsync();
            return
                new ListResultDto<ComboboxItemDto>(
                    lst.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }

        public async Task<PagedResultDto<LocationListDto>> GetLocationWithProductionUnits(GetLocationInput input)
        {
            var allItems = (from loc in _locationRepo.GetAll().Where(t => t.IsProductionUnitAllowed)
                            join com in _companyRepo.GetAll()
                                on loc.CompanyRefId equals com.Id
                            select new LocationListDto
                            {
                                Id = loc.Id,
                                Company = com,
                                CompanyName = com.Name,
                                CompanyRefId = com.Id,
                                Code = loc.Code,
                                Name = loc.Name,
                                Address1 = loc.Address1,
                                Address2 = loc.Address2,
                                Address3 = loc.Address3,
                                City = loc.City,
                                State = loc.State,
                                Country = loc.Country,
                                PhoneNumber = loc.PhoneNumber
                            }).WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Name.Equals(input.Filter)
                );

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<LocationListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultDto<LocationListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<PagedResultDto<ProductionUnitListDto>> GetProductionUnits(EntityDto input)
        {
            var allItems = (from pu in _prodUnitRepo.GetAll().Where(t => t.LocationRefId == input.Id)
                            join lo in _locationRepo.GetAll() on pu.LocationRefId equals lo.Id
                            select new ProductionUnitListDto
                            {
                                Id = pu.Id,
                                LocationRefId = lo.Id,
                                LocationRefName = lo.Name,
                                Code = pu.Code,
                                Name = pu.Name,
                                IsActive = pu.IsActive
                            });

            var sortMenuItems = await allItems
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<ProductionUnitListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultDto<ProductionUnitListDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<PagedResultDto<LocationListDto>> GetLocationsForGivenLocationGroupId(GetLocationGroupInput input)
        {
            var locationGroup = await _locationGroupRepo.GetAll()
                .Include(lg => lg.Locations)
                .WhereIf(input.LocationGroupId.HasValue, lg => lg.Id == input.LocationGroupId.Value)
                .FirstOrDefaultAsync();
            var query = locationGroup.Locations.Select(l => new LocationListDto
            {
                LocationGroupId = locationGroup.Id,
                LocationGroupName = locationGroup.Name,
                Id = l.Id,
                Name = l.Name,
                Code = l.Code
            });

            if (!input.Filter.IsNullOrEmpty())
                query = query.Where(t => t.Name.ToUpper().Contains(input.Filter.ToUpper()) || t.Code.ToUpper().Contains(input.Filter.ToUpper()));


            var totalCount = query.Count();
            var items = query.OrderBy(input.Sorting).Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

            return new PagedResultDto<LocationListDto>(totalCount, items);
        }

        public async Task UpdateLocationGroup(UpdateLocationGroup input)
        {
            var locGroup = await _locationGroupRepo.FirstOrDefaultAsync(t => t.Id == input.LocationGroupId);
            if (locGroup == null)
            {
                throw new UserFriendlyException(L("LocationGroupIdNotExist", input.LocationGroupId));
            }

            var locs = await _locationRepo.GetAll().Where(t => input.LocationIds.Contains(t.Id)).ToListAsync();
            foreach (var lst in locs)
            {
                var loc = await _locationRepo.GetAsync(lst.Id);
                loc.LocationGroups.Add(locGroup);

                await _locationRepo.UpdateAsync(loc);
            }
        }

        public async Task RemoveLocationFromLocationGroup(UpdateLocationGroup input)
        {
            var locGroup = await _locationGroupRepo.FirstOrDefaultAsync(t => t.Id == input.LocationGroupId);
            if (locGroup == null)
            {
                throw new UserFriendlyException(L("LocationGroupIdNotExist", input.LocationGroupId));
            }

            var locs = await _locationRepo.GetAll().Where(t => input.LocationIds.Contains(t.Id)).ToListAsync();
            foreach (var lst in locs)
            {
                var loc = await _locationRepo.GetAsync(lst.Id);
                loc.LocationGroups.Remove(locGroup);

                await _locationRepo.UpdateAsync(loc);
            }
        }

        public async Task<PagedResultDto<LocationListDto>> LocationListWithOutGroup(GetLocationInput input)
        {
            if (input.MaxResultCount == 0 || input.MaxResultCount == 20)
            {
                input.MaxResultCount = AppConsts.MaxPageSize;
            }
            if (!input.Filter.IsNullOrEmpty())
            {
                input.Filter = Regex.Replace(input.Filter.Trim(), @"\s+", " ");
            }
            var allItems = (from loc in _locationRepo.GetAll().Where(l => !l.LocationGroups.Where(lg => lg.Id == input.LocationGroupId).Any())
                            join com in _companyRepo.GetAll()
                                on loc.CompanyRefId equals com.Id
                            select new LocationListDto
                            {
                                Id = loc.Id,
                                Company = com,
                                CompanyName = com.Name,
                                CompanyRefId = com.Id,
                                Code = loc.Code,
                                Name = loc.Name,
                                City = loc.City,
                                HouseTransactionDate = loc.HouseTransactionDate,
                                PhoneNumber = loc.PhoneNumber,
                                Country = loc.Country,
                                ExtendedBusinessHours = loc.ExtendedBusinessHours
                            }).WhereIf(!input.Filter.IsNullOrEmpty(), p => p.Name.Contains(input.Filter)
                               || p.Id.ToString().Contains(input.Filter)
                               || p.Code.Contains(input.Filter)
                );

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<LocationListDto>>();
            var allItemCount = await allItems.CountAsync();

            return new PagedResultDto<LocationListDto>(allItemCount, allListDtos);
        }

        public async Task<PagedResultDto<SimpleLocationDto>> GetLocationsAllForGivenOrganisationList(GetLocationInput input)
        {
            var allItems = _locationRepo.GetAll();

            if (input.CompanyList.Count > 0)
            {
                var tempCompanies = input.CompanyList.Select(t => t.Value).ToArray();
                var arrCompanies = new List<int>();
                foreach (var lst in tempCompanies)
                {
                    arrCompanies.Add(int.Parse(lst));
                }

                allItems = allItems.Where(t => arrCompanies.Contains(t.CompanyRefId));
            }

            if (input.LocationGroups.Count > 0)
            {
                var locationGroupIds = input.LocationGroups.Select(t => int.Parse(t.Value)).ToList();
                var locationIdByGroups = await _locationGroupRepo.GetAll()
                    .Where(e => locationGroupIds.Contains(e.Id))
                    .SelectMany(e => e.Locations)
                    .Select(e => e.Id)
                    .ToListAsync();

                allItems = allItems.Where(t => locationIdByGroups.Contains(t.Id));
            }

            var loclist = (from loc in allItems
                           select new SimpleLocationDto
                           {
                               Id = loc.Id,
                               Code = loc.Code,
                               Name = loc.Name,
                               OrganizationUnitId = loc.Id,
                               CompanyRefId = loc.CompanyRefId
                           }).WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Name.Contains(input.Filter)
                         || p.Code != null && p.Code.Contains(input.Filter)
                );
            var sortMenuItems = await loclist
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<SimpleLocationDto>>();
            var allItemCount = await allItems.CountAsync();
            return new PagedResultDto<SimpleLocationDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<int> GetOrgnizationIdForLocationId(int locationId)
        {
            var myLocation = await _locationRepo.GetAll().FirstOrDefaultAsync(e => e.Id == locationId);
            if (myLocation != null)
            {
                return myLocation.CompanyRefId;
            }
            return 0;
        }

        private IQueryable<Location> GetLocationByUserId(long userId)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                IQueryable<Location> allItems;

                var userorg = _userOrganizationUnitRepository.GetAll().Where(a => a.UserId == userId);
                if (!userorg.Any())
                {
                    allItems = _locationRepo.GetAll();
                }
                else
                {
                    allItems = from loc in _locationRepo.GetAll().Where(a => !a.IsDeleted)
                               join ou in userorg
                                   on loc.Id equals ou.OrganizationUnitId
                               select loc;
                }
                return allItems;
            }
        }

        public List<SimpleLocationDto> GetLocationsForUser(GetLocationInputBasedOnUser input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                var allItems = GetLocationByUserId(input.UserId);

                if (input.CompanyList != null && input.CompanyList.Count > 0)
                {
                    var tempCompanies = input.CompanyList.Select(t => t.Value).ToArray();
                    var arrCompanies = tempCompanies.Select(lst => int.Parse(lst)).ToList();

                    allItems = allItems.Where(t => arrCompanies.Contains(t.CompanyRefId));
                }

                if (!input.LocationName.IsNullOrEmpty())
                {
                    allItems =
                        allItems.Where(t => t.Name.Contains(input.LocationName) || t.Code.Contains(input.LocationName));
                }

                var sortMenuItems = allItems.ToList();

                var allListDtos = sortMenuItems.MapTo<List<SimpleLocationDto>>();
                return allListDtos;
            }
        }

        protected virtual async Task UpdateLocation(CreateOrUpdateLocationInput input, string allLocations)
        {
            var item = await _locationRepo.GetAsync(input.Location.Id.Value);
            var dto = input.Location;
            item.IsProductionUnitAllowed = item.IsProductionAllowed && dto.IsProductionUnitAllowed;
            item.AddOn = input.Location.AddOn;
            item.AvgPriceLocationRefId = input.Location.AvgPriceLocationRefId;
            item.DayCloseSalesStockAdjustmentLocationRefId = input.Location.DayCloseSalesStockAdjustmentLocationRefId;

            var tobeRemoved = item.Schedules.Where(a => !a.Id.Equals(0) && !dto.Schedules.Select(s => s.Id).Contains(a.Id)).ToList();
            foreach (var tagItem in tobeRemoved)
            {
                await _locationScheduleRepo.DeleteAsync(tagItem.Id);
            }

            dto.MapTo(item);

            var locationGroupIds = dto.LocationGroups.Select(g => g.Id).ToList();
            item.LocationGroups = await _locationGroupRepo.GetAllListAsync(e => locationGroupIds.Contains(e.Id));
            dto.LocationGroups.Where(x => x.Id == 0).ToList().ForEach(x => item.LocationGroups.Add(new LocationGroup() {
                Name = x.Name,
                Code = x.Code
            }));

            var locationTagIds = dto.LocationTags.Select(g => g.Id).ToList();
            item.LocationTags = await _locationTagRepo.GetAllListAsync(e => locationTagIds.Contains(e.Id));
            dto.LocationTags.Where(x => x.Id == 0).ToList().ForEach(x => item.LocationTags.Add(new LocationTag() {
                Name = x.Name,
                Code = x.Code
            }));

            item.RequestedLocations = allLocations;
            item.Schedules = new HashSet<LocationSchdule>();

            foreach (var schedule in dto.Schedules)
            {
                if (schedule.Id.HasValue)
                {
                    var sexist = await _locationScheduleRepo.GetAsync(schedule.Id.Value);
                    schedule.MapTo(sexist);
                    item.Schedules.Add(sexist);
                }
                else
                    item.Schedules.Add(schedule.MapTo<LocationSchdule>());
            }
            var locationbranchdto = input.LocationBranch.MapTo<LocationBranch>();
            if (item.LocationBranchId.HasValue)
            {
                await _branchRepo.UpdateAsync(locationbranchdto);
            }
            else
            {
                if (!string.IsNullOrEmpty(input.LocationBranch.Code))
                {
                    var retId = await _branchRepo.InsertAndGetIdAsync(locationbranchdto);
                    item.LocationBranchId = retId;
                }
            }

            await _locationRepo.UpdateAsync(item);

            if(input.Location.EnableHouse)
                await _matLocStockAppService.IncludeAllMaterial(new IdInput { Id = item.Id });
        }

        protected virtual async Task CreateLocation(LocationEditDto input, LocationBranchEditDto locationBranchDto, string allLocations, bool nocheck)
        {
            if (!nocheck)
            {
                var locationCount = FeatureChecker.GetValue(AppFeatures.ConnectLocationCount).To<int>();
                var currentLocationCount = _locationRepo.Count();
                if (currentLocationCount >= Convert.ToInt32(locationCount))
                {
                    throw new UserFriendlyException("Contact Admin - Location Creation is not Allowed");
                }
            }
            var dto = input.MapTo<Location>();
            dto.RequestedLocations = allLocations;
            CheckErrors(await _locationManager.CreateSync(dto));
            if (!string.IsNullOrEmpty(locationBranchDto.Code))
            {
                var overallocation = locationBranchDto.MapTo<LocationBranch>();
                var retId = await _branchRepo.InsertAndGetIdAsync(overallocation);
                dto.LocationBranchId = retId;
            }
            try
            {
                string insQry = "SET IDENTITY_INSERT [dbo].[AbpOrganizationUnits] ON; " +
                    "INSERT INTO  [dbo].[AbpOrganizationUnits] ([Id],[TenantId],[ParentId],[Code],[DisplayName],[IsDeleted],[CreationTime],[CreatorUserId]) VALUES(" + dto.Id.ToString() + "," + AbpSession.TenantId.Value.ToString() + ", NULL ,'" + input.Name + "','" + input.Name + "',0,GETDATE()," + AbpSession.UserId.Value.ToString() + ");" +
                    " SET IDENTITY_INSERT [dbo].[AbpOrganizationUnits] OFF; ";
                await _context.Database.ExecuteSqlCommandAsync(insQry);
            }
            catch (Exception exception)
            {
                _logger.Fatal("Create Location", exception);
            }

            dto.OrganizationUnitId = dto.Id;

            foreach (var group in input.LocationGroups)
            {
                dto.LocationGroups.Add(await _locationGroupRepo.GetAsync(group.Id.Value));
            }

            foreach (var tag in input.LocationTags)
            {
                dto.LocationTags.Add(await _locationTagRepo.GetAsync(tag.Id.Value));
            }

            CheckErrors(await _locationManager.CreateSync(dto));

            try
            {
                var houseExists = FeatureChecker.GetValue(AppFeatures.House).To<bool>();
                if (houseExists && input.EnableHouse)
                {
                    if (AbpSession.TenantId != null)
                        await _bgm
                            .EnqueueAsync<HouseLocationWiseMaterialInitialiseJob,
                                HouseLocationWiseMaterialInitialiseJobArgs>(
                                new HouseLocationWiseMaterialInitialiseJobArgs
                                {
                                    LocationRefId = dto.Id,
                                    MaterialRefId = null,
                                    UserId = AbpSession.UserId,
                                    TenantId = AbpSession.TenantId.Value
                                });
                }
            }
            catch (Exception exception)
            {
                throw new UserFriendlyException(exception.Message);
            }
        }

        public async Task<ListResultDto<ComboboxItemDto>> GetProductionUnitsOfLocationInCombobox(EntityDto input)
        {
            var lstLocation = await _locationRepo.GetAll().Where(t => t.IsProductionUnitAllowed).ToListAsync();

            return
                new ListResultDto<ComboboxItemDto>(
                    lstLocation.Select(e => new ComboboxItemDto(e.Id.ToString(), e.Name)).ToList());
        }

        public async Task SetHouseTransactionDate()
        {
            var locs = _locationRepo.GetAll().Where(t => t.HouseTransactionDate == null);
        }

        public async Task<Company> GetCompanyInfo(ApiLocationInput locationInput)
        {
            Location loc = await _locationRepo.FirstOrDefaultAsync(t => t.Id == locationInput.LocationId);
            if (loc == null)
            {
                throw new UserFriendlyException(L("Location") + " " + L("Error"));
            }

            Company company = await _companyRepo.FirstOrDefaultAsync(t => t.Id == loc.CompanyRefId);
            if (company == null)
            {
                throw new UserFriendlyException(L("Company") + " " + L("Error"));
            }

            return company;
        }

        public async Task<ApiLocationSetting> ApiGetLocationSettings(ApiLocationInput locationInput)
        {
            var api = new ApiLocationSetting();

            var location = _locationRepo.GetAll().Include(t => t.Schedules).FirstOrDefault(t => t.Id == locationInput.LocationId);
            if (location != null)
            {
                api.ChangeTax = location.ChangeTax;
                api.TaxInclusive = location.TaxInclusive;

                var myCompany = location.Company;

                if (myCompany == null)
                {
                    myCompany = await _companyRepo.GetAsync(location.CompanyRefId);
                }

                if (myCompany != null)
                {
                    api.CompanyCode = myCompany.Code;
                    api.CompanyName = myCompany.Name;
                    api.CompanyAddress1 = myCompany.Address1;
                    api.CompanyAddress2 = myCompany.Address2;
                    api.CompanyAddress3 = myCompany.Address3;
                    api.CompanyCity = myCompany.City;
                    api.CompanyState = myCompany.State;
                    api.CompanyCountry = myCompany.Country;
                    api.CompanyPhoneNumber = myCompany.PhoneNumber;
                    api.CompanyWebsite = myCompany.Website;
                    api.CompanyEmail = myCompany.Email;
                    api.CompanyTaxCode = myCompany.GovtApprovalId;
                }

                api.Id = location.Id;
                api.Code = location.Code;
                api.Name = location.Name;
                api.Address1 = location.Address1;
                api.Address2 = location.Address2;
                api.Address3 = location.Address3;
                api.District = location.District;
                api.City = location.City;

                api.State = location.State;
                api.PostalCode = location.PostalCode;
                api.Country = location.Country;
                api.Website = location.Website;
                api.Email = location.Email;
                api.PhoneNumber = location.PhoneNumber;
                api.LocationTaxCode = location.LocationTaxCode;
                api.AddOn = location.AddOn;
                api.LocationSchedules = location.Schedules.MapTo<List<ScheduleListDto>>();

                var myBranch = location.LocationBranch;

                if (myBranch == null && location.LocationBranchId != null)
                {
                    myBranch = await _branchRepo.GetAsync(location.LocationBranchId.Value);
                }

                if (myBranch != null)
                {
                    api.BranchId = myBranch.Id.ToString();
                    api.BranchCode = myBranch.Code;
                    api.BranchName = myBranch.Name;
                    api.BranchAddress1 = myBranch.Address1;
                    api.BranchAddress2 = myBranch.Address2;
                    api.BranchAddress3 = myBranch.Address3;
                    api.BranchDistrict = myBranch.District;
                    api.BranchCity = myBranch.City;
                    api.BranchState = myBranch.State;
                    api.BranchPostalCode = myBranch.PostalCode;
                    api.BranchCountry = myBranch.Country;
                    api.BranchWebsite = myBranch.Website;
                    api.BranchEmail = myBranch.Email;
                    api.BranchPhoneNumber = myBranch.PhoneNumber;
                    api.BranchLocationTaxCode = myBranch.BranchTaxCode;
                }


            }
            return api;
        }

        public async Task<ApiReceiptDetails> ApiLocationReceiptDetails(ApiLocationInput locationInput)
        {
            var api = new ApiReceiptDetails();

            var location = await _locationRepo.GetAsync(locationInput.LocationId);

            if (location != null)
            {
                api.ReceiptFooter = location.ReceiptFooter;
                api.ReceiptHeader = location.ReceiptHeader;
            }
            return api;
        }

        public async Task<PagedResultDto<LocationListDto>> GetLocationsForGivenLocationTagId(GetLocationTagInput input)
        {
            var locationTag = await _locationTagRepo.GetAll()
                .Include(lg => lg.Locations)
                .WhereIf(input.LocationTagId.HasValue, lg => lg.Id == input.LocationTagId.Value)
                .FirstOrDefaultAsync();

            var query = locationTag.Locations.Select(l => new LocationListDto
            {
                LocationGroupId = locationTag.Id,
                LocationGroupName = locationTag.Name,
                Id = l.Id,
                Name = l.Name,
                Code = l.Code
            });
            if (!input.Filter.IsNullOrEmpty())
                query = query.Where(t => t.Name.ToUpper().Contains(input.Filter.ToUpper()) || t.Code.ToUpper().Contains(input.Filter.ToUpper()));
            var totalCount = query.Count();
            var items = query.OrderBy(input.Sorting).Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

            return new PagedResultDto<LocationListDto>(totalCount, items);
        }

        public async Task UpdateLocationTag(UpdateLocationTag input)
        {
            var locTag = await _locationTagRepo.FirstOrDefaultAsync(t => t.Id == input.LocationTagId);
            if (locTag == null)
            {
                throw new UserFriendlyException(L("LocationTagIdNotExist", input.LocationTagId));
            }

            var locs = await _locationRepo.GetAll().Where(t => input.LocationIds.Contains(t.Id)).ToListAsync();
            foreach (var lst in locs)
            {
                var loc = await _locationRepo.GetAsync(lst.Id);
                loc.LocationTags.Add(locTag);

                await _locationRepo.UpdateAsync(loc);
            }
        }

        public async Task<PagedResultDto<LocationListDto>> LocationListWithOutTag(GetLocationInput input)
        {
            if (input.MaxResultCount == 0 || input.MaxResultCount == 20)
            {
                input.MaxResultCount = AppConsts.MaxPageSize;
            }
            if (!input.Filter.IsNullOrEmpty())
            {
                input.Filter = Regex.Replace(input.Filter.Trim(), @"\s+", " ");
            }

            var query = _locationRepo.GetAll()
                .Where(l => !l.LocationTags.Where(lg => lg.Id == input.LocationTagId).Any())
                .WhereIf(!input.Filter.IsNullOrEmpty(), l => l.Name.Contains(input.Filter)
                || l.Id.ToString().Contains(input.Filter)
                || l.Code.Contains(input.Filter)
                );

            var result = await query
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<LocationListDto>(await query.CountAsync(), result.MapTo<List<LocationListDto>>());
        }

        public async Task RemoveLocationFromLocationTag(UpdateLocationTag input)
        {
            var locTag = await _locationTagRepo.FirstOrDefaultAsync(t => t.Id == input.LocationTagId);
            if (locTag == null)
            {
                throw new UserFriendlyException(L("LocationTagIdNotExist", input.LocationTagId));
            }

            var locs = await _locationRepo.GetAll().Where(t => input.LocationIds.Contains(t.Id)).ToListAsync();
            foreach (var lst in locs)
            {
                var loc = await _locationRepo.GetAsync(lst.Id);
                loc.LocationTags.Remove(locTag);

                await _locationRepo.UpdateAsync(loc);
            }
        }

        public async Task ActivateItem(EntityDto input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _locationRepo.GetAsync(input.Id);
                item.IsDeleted = false;

                await _locationRepo.UpdateAsync(item);
            }
        }

        private async Task ValidateForCreateOrUpdate(LocationEditDto location)
        {
            if (!location.Id.HasValue)
            {
                if (_locationRepo.GetAll().Any(a => a.Name.Equals(location.Name)))
                {
                    throw new UserFriendlyException(L("NameAlreadyExists_f"), location.Name);
                }

                if (_locationRepo.GetAll().Any(a => a.Code.Equals(location.Code)))
                {
                    throw new UserFriendlyException(L("CodeAlreadyExists"), location.Code);
                }
            }
            else
            {
                List<Location> lst = await _locationRepo.GetAll().Where(a => a.Name.Equals(location.Name) && a.Id != location.Id).ToListAsync();
                if (lst.Any())
                {
                    throw new UserFriendlyException(L("NameAlreadyExists_f"), location.Name);
                }

                lst = _locationRepo.GetAll().Where(a => a.Code.Equals(location.Code) && a.Id != location.Id).ToList();
                if (lst.Any())
                {
                    throw new UserFriendlyException(L("CodeAlreadyExists"), location.Code);
                }
            }

            if (location.TransferRequestLockHours == 0 && location.TransferRequestGraceHours > 0)
            {
                throw new UserFriendlyException(L("TransferRequestLockHoursAlert"));
            }

            if (location.TransferRequestLockHours > 0 && location.TransferRequestGraceHours > 0 && location.TransferRequestGraceHours < location.TransferRequestLockHours)
            {
                throw new UserFriendlyException(L("TransferRequestGraceHoursAlert"));
            }

            if (location.AvgPriceLocationRefId.HasValue && (location.AvgPriceLocationRefId.Value == 0 || location.AvgPriceLocationRefId.Value == location.Id))
            {
                location.AvgPriceLocationRefId = null;
            }

            if (location.DayCloseSalesStockAdjustmentLocationRefId.HasValue && (location.DayCloseSalesStockAdjustmentLocationRefId.Value == 0 || location.DayCloseSalesStockAdjustmentLocationRefId.Value == location.Id))
            {
                location.DayCloseSalesStockAdjustmentLocationRefId = null;
            }

        }

        public async Task<List<ScheduleSetting>> GetLocationSchedulesByLocation(int locationId)
        {
            var list = await _locationScheduleRepo.GetAllListAsync(e => e.LocationId == locationId);

            return list.MapTo<List<ScheduleSetting>>();
        }
    }
}