﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Utility.Exporter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Processor;

namespace DinePlan.DineConnect.Connect.Master.Implementation
{
    public class PaymentTypeAppService : DineConnectAppServiceBase, IPaymentTypeAppService
    {
        private readonly IExcelExporter _exporter;
        private readonly IPaymentTypeManager _paManager;
        private readonly IRepository<PaymentType> _paymenttypeManager;
        private readonly ISyncAppService _syncAppService;
        private readonly ILocationAppService _locAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public PaymentTypeAppService(IRepository<PaymentType> paymenttypeManager,
            ILocationAppService locAppService,
            IExcelExporter exporter,
            ISyncAppService syncAppService,
            IPaymentTypeManager paManager,
            IUnitOfWorkManager unitOfWorkManager)
        {
            _paymenttypeManager = paymenttypeManager;
            _exporter = exporter;
            _syncAppService = syncAppService;
            _paManager = paManager;
            _unitOfWorkManager = unitOfWorkManager;
            _locAppService = locAppService;
        }

        public async Task<PagedResultOutput<PaymentTypeListDto>> GetAll(GetPaymentTypeInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                IQueryable<PaymentType> allItems = null;
                if (input == null)
                {
                    allItems = _paymenttypeManager.GetAll();
                }
                else
                {
                    allItems = _paymenttypeManager
                        .GetAll()
                        .Where(i => i.IsDeleted == input.IsDeleted)
                        .WhereIf(
                            !input.Filter.IsNullOrEmpty(),
                            p => p.Name.Contains(input.Filter))
                        .WhereIf(
                            !input.Location.IsNullOrEmpty(),
                            p => p.Locations.Contains(input.Location))
                        .WhereIf(
                            input.AcceptChange.HasValue,
                            p => p.AcceptChange == input.AcceptChange);
                }

                var sortMenuItems = allItems
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList();
                
                var allItemCount = sortMenuItems.Count();

                if(input.LocationGroup!=null && (input.LocationGroup.Locations.Any() || input.LocationGroup.LocationTags.Any() || input.LocationGroup.Groups.Any()|| input.LocationGroup.NonLocations.Any()))
                {
                    var allMyEnItems = SearchLocation(allItems, input.LocationGroup).OfType<PaymentType>();

                     allItemCount = allMyEnItems.Count();

                     sortMenuItems = allMyEnItems.AsQueryable()
                        .OrderBy(input.Sorting)
                        .PageBy(input)
                        .ToList();
                }

                var allItemDtos = sortMenuItems.MapTo<List<PaymentTypeListDto>>();

                return new PagedResultOutput<PaymentTypeListDto>(
                    allItemCount,
                    allItemDtos
                );
               
            }
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _paymenttypeManager.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<PaymentTypeListDto>>();
            var baseE = new BaseExportObject
            {
                ExportObject = allListDtos,
                ColumnNames = new[] { "Id", "Name" }
            };
            return _exporter.ExportToFile(baseE, L("PaymentType"));
        }

        public async Task<GetPaymentTypeForEditOutput> GetPaymentTypeForEdit(NullableIdInput input)
        {
            GetPaymentTypeForEditOutput output = new
                GetPaymentTypeForEditOutput();
            var allLocations = new List<ComboboxItemDto>();

            PaymentTypeEditDto editDto;

            if (input.Id.HasValue)
            {
                var paymentType = await _paymenttypeManager.GetAsync(input.Id.Value);
                editDto = paymentType.MapTo<PaymentTypeEditDto>();
            }
            else
            {
                editDto = new PaymentTypeEditDto();
            }

            output.Departments = new List<ComboboxItemDto>();

            if (!string.IsNullOrEmpty(editDto.Departments))
            {
                output.Departments = JsonConvert.DeserializeObject<List<ComboboxItemDto>>(editDto.Departments);
            }

            UpdateLocationAndNonLocationForEdit(editDto, output.LocationGroup);

            output.PaymentType = editDto;
            return output;
        }

        public async Task<int> CreateOrUpdatePaymentType(CreateOrUpdatePaymentTypeInput input)
        {
            var output = 0;
            if (input.PaymentType.Id.HasValue && input.PaymentType.Id > 0)
            {
                await UpdatePaymentType(input);
                output = input.PaymentType.Id.Value;
            }
            else
            {
                output = await CreatePaymentType(input);
            }
            await _syncAppService.UpdateSync(SyncConsts.PAYMENTTYPE);

            return output;
        }

        public IQueryable<PaymentType> GetEntities()
        {
            var menuItems = _paymenttypeManager.GetAll();
            return menuItems;
        }

        public async Task CreatePaymentForTenant(int tenantId)
        {
            if (await _paymenttypeManager.CountAsync(a => a.TenantId.Equals(tenantId)) <= 0)
            {
                var paymentType = new PaymentType { Name = "CASH", TenantId = tenantId };
                await _paymenttypeManager.InsertAndGetIdAsync(paymentType);

                paymentType = new PaymentType { Name = "VISA", TenantId = tenantId };
                await _paymenttypeManager.InsertAndGetIdAsync(paymentType);

                paymentType = new PaymentType { Name = "MASTER", TenantId = tenantId };
                await _paymenttypeManager.InsertAndGetIdAsync(paymentType);

                paymentType = new PaymentType { Name = "VOUCHER", TenantId = tenantId };
                await _paymenttypeManager.InsertAndGetIdAsync(paymentType);

                paymentType = new PaymentType { Name = "MEMBER", TenantId = tenantId };
                await _paymenttypeManager.InsertAndGetIdAsync(paymentType);

                paymentType = new PaymentType { Name = "CARD", TenantId = tenantId };
                await _paymenttypeManager.InsertAndGetIdAsync(paymentType);
            }
            await _syncAppService.UpdateSync(SyncConsts.PAYMENTTYPE);
        }

        public async Task<PagedResultOutput<PaymentTypeEditDto>> GetAllItems()
        {
            var allItems = _paymenttypeManager.GetAll();
            var allListDtos = allItems.MapTo<List<PaymentTypeEditDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<PaymentTypeEditDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<ListResultOutput<ApiPaymentTypeOutput>> ApiGetAll(ApiPaymentTypeInput input)
        {
             var localPaymentTypes = await _paymenttypeManager.GetAllListAsync(a => a.TenantId == input.TenantId);

            List<ApiPaymentTypeOutput> paymentTypeOutputs = new List<ApiPaymentTypeOutput>();
            foreach (var sortL in localPaymentTypes)
            {
                if (await _locAppService.IsLocationExists(new CheckLocationInput
                {
                    LocationId = input.LocationId,
                    Locations = sortL.Locations,
                    Group = sortL.Group,
                    NonLocations = sortL.NonLocations,
                    LocationTag = sortL.LocationTag

                }))
                {
                    var myPt = new ApiPaymentTypeOutput
                    {
                        Id = sortL.Id,
                        AcceptChange = sortL.AcceptChange,
                        SortOrder = sortL.SortOrder,
                        AccountCode = sortL.AccountCode,
                        PaymentProcessor = sortL.PaymentProcessor,
                        Name = sortL.Name,
                        Processors = sortL.Processors,
                        ButtonColor = sortL.ButtonColor,
                        Group = sortL.PaymentGroup,
                        NoRefund = sortL.NoRefund,
                        AutoPayment = sortL.AutoPayment,
                        DisplayInShift = sortL.DisplayInShift,
                        Files = sortL.Files,
                        DownloadImage = sortL.DownloadImage,
                        Departments = sortL.Departments,
                        PaymentTenderType = sortL.PaymentTenderType,
                        PaymentTag = sortL.PaymentTag

                    };
                    
                    if (!string.IsNullOrEmpty(sortL.ProcessorName))
                    {
                        myPt.PaymentProcessorName = sortL.ProcessorName;
                    }
                    else
                    {
                        myPt.PaymentProcessorName = sortL.PaymentProcessor > 0
                            ? PaymentTypeProcessors.Processors[sortL.PaymentProcessor - 1]
                            : "";
                    }

                    paymentTypeOutputs.Add(myPt);
                }
            }
            return new ListResultOutput<ApiPaymentTypeOutput>(paymentTypeOutputs);
        }

        public async Task DeletePaymentType(IdInput input)
        {
            await _paymenttypeManager.DeleteAsync(input.Id);
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _paymenttypeManager.GetAsync(input.Id);
                item.IsDeleted = false;

                await _paymenttypeManager.UpdateAsync(item);
            }
        }

        protected virtual async Task UpdatePaymentType(CreateOrUpdatePaymentTypeInput input)
        {
            var item = await _paymenttypeManager.GetAsync(input.PaymentType.Id.Value);
            var dto = input.PaymentType;
            dto.MapTo(item);
            item.DownloadImage = Guid.NewGuid();
            item.PaymentGroup = input.PaymentType.PaymentGroup;
            item.PaymentTag = input.PaymentType.PaymentTag;
            item.ProcessorName = input.PaymentType.PaymentProcessor > 0
                ? PaymentTypeProcessors.Processors[input.PaymentType.PaymentProcessor - 1]
                : "";

            item.PaymentTenderType = input.PaymentType.PaymentTenderType;
            UpdateLocationAndNonLocation(item, input.LocationGroup);
        }

        protected virtual async Task<int> CreatePaymentType(CreateOrUpdatePaymentTypeInput input)
        {
            var item = input.PaymentType.MapTo<PaymentType>();

            item.DownloadImage = Guid.NewGuid();
            item.ProcessorName = input.PaymentType.PaymentProcessor > 0
                ? PaymentTypeProcessors.Processors[input.PaymentType.PaymentProcessor - 1]
                : "";
            UpdateLocationAndNonLocation(item, input.LocationGroup);

            return await _paymenttypeManager.InsertAndGetIdAsync(item);
        }

        public ListResultDto<ComboboxItemDto> GetPaymentTypeTenders()
        {
            var returnList = new List<ComboboxItemDto>();

            var i = 0;
            foreach (var name in Enum.GetNames(typeof(PaymentTenderType)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }

        public async Task<List<PaymentType>> ApiGetAllPaymentTypesForSync(ApiLocationInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                return await _paymenttypeManager.GetAll().Where(a=>a.TenantId == input.TenantId).ToListAsync();
            }
        }

    }
}