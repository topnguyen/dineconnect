﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using AutoMapper;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Master.Exporter;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Connect.Tag;
using DinePlan.DineConnect.Dto;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.Implementation
{
    public class TicketTagGroupAppService : DineConnectAppServiceBase, ITicketTagGroupAppService
    {
        private readonly ITicketTagGroupExcelExporter _exporter;
        private readonly ISyncAppService _syncAppService;
        private readonly ITicketTagGroupManager _tickettaggroupManager;
        private readonly IRepository<TicketTagGroup> _tickettaggroupRepo;
        private readonly IRepository<TicketTag> _tagRepo;

        private readonly ILocationAppService _locationAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public TicketTagGroupAppService(ITicketTagGroupManager tickettaggroupManager,
            ILocationAppService locationAppService,
            IRepository<TicketTagGroup> ticketTagGroupRepo,
            IRepository<TicketTag> tagRepo,
            ISyncAppService syncAppService,
            ITicketTagGroupExcelExporter exporter,
            IUnitOfWorkManager unitOfWorkManager
            )
        {
            _tickettaggroupManager = tickettaggroupManager;
            _tickettaggroupRepo = ticketTagGroupRepo;
            _exporter = exporter;
            _unitOfWorkManager = unitOfWorkManager;
            _syncAppService = syncAppService;
            _locationAppService = locationAppService;
            _tagRepo = tagRepo;
        }

        public async Task<PagedResultOutput<TicketTagGroupListDto>> GetAll(GetTicketTagGroupInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var allItems = _tickettaggroupRepo.GetAll()
                    .Where(i => i.IsDeleted == input.IsDeleted);

                if (input.Operation == "SEARCH")
                {
                    allItems = allItems
                        .WhereIf(
                            !input.Filter.IsNullOrEmpty(),
                            p => p.Name.Equals(input.Filter)
                        );
                }
                else
                {
                    allItems = allItems
                      
                        .WhereIf(
                            !input.Filter.IsNullOrEmpty(),
                            p => p.Name.Contains(input.Filter)
                        );
                }

                var allMyEnItems = SearchLocation(allItems, input.LocationGroup).OfType<TicketTagGroup>();

                var allItemCount = allMyEnItems.Count();

                var sortMenuItems = allMyEnItems.AsQueryable()
                    .PageBy(input)
                    .ToList();


                var allListDtos = sortMenuItems.MapTo<List<TicketTagGroupListDto>>();
                foreach (var loca in allListDtos)
                {
                    if (string.IsNullOrEmpty(loca.Departments)) loca.DisplayDepartments = "";
                    else
                    {
                        var allLo = JsonConvert.DeserializeObject<List<ComboboxItemDto>>(loca.Departments);
                        loca.DisplayDepartments = string.Join(", ", allLo.Select(a => a.DisplayText));
                    }
                }
                allListDtos = allListDtos.OrderBy(input.Sorting).ToList();
                return new PagedResultOutput<TicketTagGroupListDto>(
                    allItemCount,
                    allListDtos
                    );
            }
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _tickettaggroupRepo.GetAll()
               .OrderBy(t => t.Id)
               .ToListAsync();

            var result = allList.MapTo<List<TicketTagGroupListDto>>();

            foreach (var loca in result)
            {
                if (string.IsNullOrEmpty(loca.Departments)) loca.DisplayDepartments = "";
                else
                {
                    var allLo = JsonConvert.DeserializeObject<List<ComboboxItemDto>>(loca.Departments);
                    loca.DisplayDepartments = allLo.Select(a => a.DisplayText).JoinAsString(", ");
                }
            }

            return _exporter.ExportToFile(result);
        }

        public async Task<GetTicketTagGroupForEditOutput> GetTagForEdit(NullableIdInput input)
        {
            TicketTagGroupEditDto editDto;
            var output = new GetTicketTagGroupForEditOutput();

            if (input.Id.HasValue)
            {
                var hDto = await _tickettaggroupRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<TicketTagGroupEditDto>();
            }
            else
            {
                editDto = new TicketTagGroupEditDto();
            }
            UpdateLocationAndNonLocationForEdit(editDto, output.LocationGroup);

            var allDepartments = new List<ComboboxItemDto>();
            if (!string.IsNullOrEmpty(editDto.Departments))
            {
                allDepartments = JsonConvert.DeserializeObject<List<ComboboxItemDto>>(editDto.Departments);
            }

            output.Departments = allDepartments;
            output.Tag = editDto;

            return output;
        }

        public async Task CreateOrUpdateTagGroup(CreateOrUpdateTicketTagGroupInput input)
        {
            if (input?.LocationGroup == null)
                return;

            var allDepartments = "";
            if (input.Tag != null && input.Departments != null && input.Departments.Any())
            {
                allDepartments = JsonConvert.SerializeObject(input.Departments);
            }
            if (input.Tag != null && input.Tag.Id.HasValue)
            {
                await UpdateTicketTagGroup(input, allDepartments);
            }
            else
            {
                if (input.Tag != null) 
                    input.Tag.Departments = allDepartments;
                await CreateTicketTagGroup(input);
            }
            await _syncAppService.UpdateSync(SyncConsts.TICKETTAG);
        }

        public async Task DeleteTicketTagGroup(IdInput input)
        {
            await _tickettaggroupRepo.DeleteAsync(input.Id);
        }

        public async Task<ListResultOutput<TicketTagGroupListDto>> GetNames()
        {
            var lstTicketTagGroup = await _tickettaggroupRepo.GetAll().ToListAsync();
            return new ListResultOutput<TicketTagGroupListDto>(lstTicketTagGroup.MapTo<List<TicketTagGroupListDto>>());
        }

        protected virtual async Task UpdateTicketTagGroup(CreateOrUpdateTicketTagGroupInput input,
            string depts)
        {
            var item = await _tickettaggroupRepo.GetAsync(input.Tag.Id.Value);
            var dto = input.Tag;

            item.Name = dto.Name;
            item.SortOrder = dto.SortOrder;
            item.Departments = depts;
            item.SaveFreeTags = dto.SaveFreeTags;
            item.FreeTagging = dto.FreeTagging;
            item.ForceValue = dto.ForceValue;
            item.AskBeforeCreatingTicket = dto.AskBeforeCreatingTicket;
            item.DataType = dto.DataType;
            item.SubTagId = dto.SubTagId;
            item.LastModificationTime = DateTime.Now;

            UpdateLocationAndNonLocation(item, input.LocationGroup);
            var oids = new List<int>();
            if (dto?.Tags != null && dto.Tags.Any())
            {                
                foreach (var otdto in dto.Tags)
                {
                    if (otdto.Id == null)
                    {
                        item.Tags.Add(Mapper.Map<TicketTagListDto, TicketTag>(otdto));
                    }
                    else
                    {
                        oids.Add(otdto.Id.Value);
                        var fromUpdation = item.Tags.SingleOrDefault(a => a.Id.Equals(otdto.Id));
                        UpdateTag(fromUpdation, otdto);
                    }
                }              
            }
            var tobeRemoved = item.Tags.Where(a => a.Id != null && !a.Id.Equals(0) && !oids.Contains(a.Id)).ToList();
            foreach (var tagItem in tobeRemoved)
            {
                await _tickettaggroupManager.DeleteTagAsync(tagItem.Id);
            }
            CheckErrors(await _tickettaggroupManager.CreateSync(item));
        }

        private void UpdateTag(TicketTag to, TicketTagListDto from)
        {
            to.Name = from.Name;
            to.AlternateName = from.AlternateName;
        }

        protected virtual async Task CreateTicketTagGroup(CreateOrUpdateTicketTagGroupInput input)
        {
            var dto = input.Tag.MapTo<TicketTagGroup>();
            UpdateLocationAndNonLocation(dto, input.LocationGroup);
            CheckErrors(await _tickettaggroupManager.CreateSync(dto));
        }

        public async Task<ListResultOutput<TicketTagGroupEditDto>> ApiGetTags(ApiLocationInput locationInput)
        {
            var orgId = await _locationAppService.GetOrgnizationIdForLocationId(locationInput.LocationId);
            if (orgId > 0)
            {
                CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);
            }

           

            var allItems = await _tickettaggroupRepo.GetAllListAsync(a => a.TenantId == locationInput.TenantId);

            var allListDtos = allItems.MapTo<List<TicketTagGroupEditDto>>();
            var returnDto = new List<TicketTagGroupEditDto>();

            if (!string.IsNullOrEmpty(locationInput.Tag))
            {
                var allLocations = await _locationAppService.GetLocationsByLocationGroupCode(locationInput.Tag);
                if (allLocations != null && allLocations.Items.Any())
                    foreach (var ticketTagList in allListDtos)
                        if (!ticketTagList.Group && !string.IsNullOrEmpty(ticketTagList.Locations) &&
                            !ticketTagList.LocationTag)
                        {
                            var scLocations =
                                JsonConvert.DeserializeObject<List<SimpleLocationGroupDto>>(ticketTagList.Locations);
                            if (scLocations.Any())
                                foreach (var i in scLocations.Select(a => a.Id))
                                    if (allLocations.Items.Select(a => a.Value).Contains(i.ToString()))
                                        returnDto.Add(ticketTagList);
                        }
                        else
                        {
                            returnDto.Add(ticketTagList);
                        }
            }

            else if (locationInput.LocationId > 0)
            {
                foreach (var myDto in allListDtos)
                {
                    if (await _locationAppService.IsLocationExists(new CheckLocationInput
                    {
                        LocationId = locationInput.LocationId,
                        Locations = myDto.Locations,
                        Group = myDto.Group,
                        NonLocations = myDto.NonLocations,
                        LocationTag = myDto.LocationTag

                    }))
                    {
                        returnDto.Add(myDto);
                    }
                }
            }
            else
            {
                returnDto = allListDtos;
            }
            return new ListResultOutput<TicketTagGroupEditDto>(returnDto);
        }

        public async Task<ListResultOutput<TicketTagListDto>> GetAllTags()
        {
            var myTagGroup = _tickettaggroupRepo.GetAll().Select(a => a.Id).ToArray();
            var allTags = _tagRepo.GetAll().Where(a => myTagGroup.Contains(a.TicketTagGroupId));
            return new ListResultOutput<TicketTagListDto>(allTags.MapTo<List<TicketTagListDto>>());
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _tickettaggroupRepo.GetAsync(input.Id);
                item.IsDeleted = false;

                await _tickettaggroupRepo.UpdateAsync(item);
            }
        }

        
        public async Task<List<TicketTagGroup>> ApiGetAllTicketTagGroupsForSync(ApiLocationInput input)
        {

            var orgId = await _locationAppService.GetOrgnizationIdForLocationId(input.LocationId);
            if (orgId > 0) CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);
            return await _tickettaggroupRepo.GetAll().Where(a=>a.TenantId == input.TenantId).Include(a=>a.Tags)
                .Include(a=>a.Tags).ToListAsync();

       
        }
    }
}