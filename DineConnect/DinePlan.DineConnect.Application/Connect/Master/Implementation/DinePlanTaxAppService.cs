﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using AutoMapper;
using DinePlan.DineConnect.Connect.Future;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Master.Exporter;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Connect.Taxes;
using DinePlan.DineConnect.Dto;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Prints.PrintConfigurations.Dtos;

namespace DinePlan.DineConnect.Connect.Master.Implementation
{
    public class DinePlanTaxAppService : DineConnectAppServiceBase, IDinePlanTaxAppService
    {
        private readonly IRepository<Category> _categoryRepo;
        private readonly IRepository<Department> _departmentRepo;
        private readonly IDinePlanTaxListExcelExporter _dineplantaxExporter;
        private readonly IRepository<DinePlanTaxLocation> _dineplantaxlocationRepo;
        private readonly IDinePlanTaxManager _dineplantaxManager;
        private readonly IRepository<DinePlanTaxMapping> _dineplantaxmappingRepo;
        private readonly IRepository<DinePlanTax> _dineplantaxRepo;
        private readonly IRepository<Location> _locationRepo;
        private readonly IRepository<MenuItem> _menuitemRepo;
        private readonly ISyncAppService _syncAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<TransactionType> _transactiontypeManager;
        private readonly IRepository<FutureDateInformation> _futureDateRepository;
        private readonly ILocationAppService _locAppService;


        public DinePlanTaxAppService(ISyncAppService syncAppService, IDinePlanTaxManager dineplantaxManager, ILocationAppService locAppService,
            IRepository<DinePlanTax> dinePlanTaxesRepo,
            IRepository<DinePlanTaxLocation> dineplantaxlocationRepo,
            IRepository<DinePlanTaxMapping> dineplantaxmappingRepo,
            IRepository<MenuItem> menuitemRepo,
            IRepository<Category> categoryRepo,
            IRepository<Location> locationRepo,
            IDinePlanTaxListExcelExporter dineplantaxExporter,
            IRepository<Department> departmentRepo,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<TransactionType> transactiontypeManager,
            IRepository<FutureDateInformation> futureDateRepository
            )
        {
            _dineplantaxManager = dineplantaxManager;
            _dineplantaxRepo = dinePlanTaxesRepo;
            _dineplantaxExporter = dineplantaxExporter;
            _dineplantaxlocationRepo = dineplantaxlocationRepo;
            _dineplantaxmappingRepo = dineplantaxmappingRepo;
            _menuitemRepo = menuitemRepo;
            _categoryRepo = categoryRepo;
            _locationRepo = locationRepo;
            _syncAppService = syncAppService;
            _departmentRepo = departmentRepo;
            _unitOfWorkManager = unitOfWorkManager;
            _transactiontypeManager = transactiontypeManager;
            _locAppService = locAppService;
            _futureDateRepository = futureDateRepository;
        }

        public async Task<PagedResultOutput<DinePlanTaxListDto>> GetAll(GetDinePlanTaxInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                IQueryable<DinePlanTaxListDto> allItems;
                var dineplantax = _dineplantaxRepo
                    .GetAll().Where(i => i.IsDeleted == input.IsDeleted);
                allItems = (from tax in dineplantax
                            join trans in _transactiontypeManager.GetAll()
                            on tax.TransactionTypeId equals trans.Id
                            select new DinePlanTaxListDto()
                            {
                                Id = tax.Id,
                                TaxName = tax.TaxName,
                                TransactionTypeId = tax.TransactionTypeId,
                                TransactionTypeName = trans.Name,
                                CreationTime = tax.CreationTime
                            }).WhereIf(
                                    !input.Filter.IsNullOrEmpty(),
                                    p => p.TaxName.Contains(input.Filter)
                   );

                var sortMenuItems = await allItems.OrderBy(input.Sorting).PageBy(input).ToListAsync();

                var allListDtos = sortMenuItems.MapTo<List<DinePlanTaxListDto>>();
                var totalCount = await allItems.CountAsync();

                return new PagedResultOutput<DinePlanTaxListDto>(totalCount, allListDtos);
            }
        }

        public async Task<FileDto> GetAllToExcel(GetDinePlanTaxInput input)
        {
            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<DinePlanTaxListDto>>();
            return _dineplantaxExporter.ExportToFile(allListDtos);
        }

        public async Task<GetDinePlanTaxForEditOutput> GetDinePlanTaxForEdit(FilterInputDto input)
        {
            DinePlanTaxEditDto editDto;
            Collection<DinePlanTaxLocationEditDto> valuesOutput = new Collection<DinePlanTaxLocationEditDto>();
            if (input.Id.HasValue || input.FutureDataId.HasValue)
            {
                List<SimpleLocationDto> filterLocation = new List<SimpleLocationDto>();
                List<SimpleLocationGroupDto> filterLocationGroup = new List<SimpleLocationGroupDto>();
                List<int> filterLocationRefIds = new List<int>();
                List<int> filterLocationGroupRefIds = new List<int>();
                List<int> filterLocationTagRefIds = new List<int>();
                if (input.LocationGroupToBeFiltered != null)
                {
                    filterLocationRefIds = input.LocationGroupToBeFiltered.Locations.Select(t => t.Id).ToList();
                    filterLocationGroupRefIds = input.LocationGroupToBeFiltered.Groups.Select(t => t.Id).ToList();
                    filterLocationTagRefIds = input.LocationGroupToBeFiltered.LocationTags.Select(t => t.Id).ToList();
                }
                DinePlanTax hDto = new DinePlanTax();
                List<DinePlanTaxLocation> dineplantaxlocation = new List<DinePlanTaxLocation>();

                input.FutureData = input.FutureData || input.FutureDataId != null;
                //if (input.Id.HasValue && input.FutureDataId == null)
                //{
                hDto = await _dineplantaxRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<DinePlanTaxEditDto>();
                dineplantaxlocation = await _dineplantaxlocationRepo.GetAllListAsync(t => t.DinePlanTaxRefId == input.Id.Value);              
                var tempValuesFilter = dineplantaxlocation.MapTo<Collection<DinePlanTaxLocationEditDto>>();
                var rscategory = await _categoryRepo.GetAllListAsync();
                var rsmenuItem = await _menuitemRepo.GetAllListAsync();

                foreach (var lst in tempValuesFilter)
                {
                    if (lst.DinePlanTaxMappings.Count > 0)
                    {
                        foreach (var sublst in lst.DinePlanTaxMappings)
                        {
                            if (sublst.CategoryId != null)
                            {
                                var category = rscategory.FirstOrDefault(t => t.Id == sublst.CategoryId);
                                if (category != null)
                                {
                                    sublst.CategoryName = category.Name;
                                }
                            }
                            if (sublst.MenuItemId != null)
                            {
                                var menuItem = rsmenuItem.FirstOrDefault(t => t.Id == sublst.MenuItemId);
                                if (menuItem != null)
                                {
                                    sublst.MenuItemGroupCode = menuItem.Name;
                                }
                            }
                        }
                    }
                    UpdateLocationAndNonLocationForEdit(lst, lst.LocationGroup);
                    if (filterLocationRefIds.Count > 0 || filterLocationGroupRefIds.Count > 0 || filterLocationTagRefIds.Count > 0)
                    {
                        if (lst.LocationGroup.Locations.Select(t => t.Id).Intersect(filterLocationRefIds).ToList().Count > 0)
                            valuesOutput.Add(lst);
                        else if (lst.LocationGroup.Groups.Select(t => t.Id).Intersect(filterLocationGroupRefIds).ToList().Count > 0)
                            valuesOutput.Add(lst);
                        else if (lst.LocationGroup.LocationTags.Select(t => t.Id).Intersect(filterLocationTagRefIds).ToList().Count > 0)
                            valuesOutput.Add(lst);
                    }
                    else
                    {
                        valuesOutput.Add(lst);
                    }
                }
                editDto.DinePlanTaxLocations = valuesOutput;
            }
            else
            {
                editDto = new DinePlanTaxEditDto();
            }

            return new GetDinePlanTaxForEditOutput
            {
                DinePlanTax = editDto,
                DinePlanTaxLocation = new DinePlanTaxLocationEditDto()
            };
        }
        public async Task<IdInput> CreateOrUpdateDinePlanTax(CreateOrUpdateDinePlanTaxLocationInput input)
        {
            IdInput output = null;
            if (input.DinePlanTax.Id.HasValue)
            {
                output = await UpdateDinePlanTax(input);
            }
            else
            {
                output = await CreateDinePlanTax(input);
            }
            await _syncAppService.UpdateSync(SyncConsts.TAX);
            return output;
        }
        protected virtual async Task<IdInput> UpdateDinePlanTax(CreateOrUpdateDinePlanTaxLocationInput input)
        {
            Collection<DinePlanTaxLocationEditDto> valuesOutput = new Collection<DinePlanTaxLocationEditDto>();
            var item = await _dineplantaxRepo.GetAsync(input.DinePlanTax.Id.Value);           
            var dto = input.DinePlanTax;
            item.TaxName = dto.TaxName;
            item.FutureData = dto.FutureData;
            CheckErrors(await _dineplantaxManager.CreateSync(item));
            await _syncAppService.UpdateSync(SyncConsts.TAX);
            //}
            return new IdInput { Id = item.Id };
        }

        protected virtual async Task<IdInput> CreateDinePlanTax(CreateOrUpdateDinePlanTaxLocationInput input)
        {
            Collection<DinePlanTaxLocationEditDto> valuesOutput = new Collection<DinePlanTaxLocationEditDto>();
            var dto = input.DinePlanTax.MapTo<DinePlanTax>();           
            var checkTran = _dineplantaxRepo.GetAll().Where(s => s.TransactionTypeId == input.DinePlanTax.TransactionTypeId);
            if (!checkTran.Any())
            {
                CheckErrors(await _dineplantaxManager.CreateSync(dto));
                await _syncAppService.UpdateSync(SyncConsts.TAX);
            }
            //}

            return new IdInput { Id = dto.Id };
        }
        public async Task DeleteDinePlanTax(IdInput input)
        {
            var dinetaxlocations =
               await _dineplantaxlocationRepo.GetAll().Where(t => t.DinePlanTaxRefId == input.Id).ToListAsync();

            if (dinetaxlocations == null)
            {
                throw new UserFriendlyException("TAXNOTFOUND");
            }

            foreach (var dinetaxlocation in dinetaxlocations)
            {
                foreach (var map in dinetaxlocation.DinePlanTaxMappings.ToArray())
                {
                    await _dineplantaxmappingRepo.DeleteAsync(map.Id);
                }

                await _dineplantaxlocationRepo.DeleteAsync(dinetaxlocation.Id);
            }

            await _dineplantaxRepo.DeleteAsync(input.Id);
            await _syncAppService.UpdateSync(SyncConsts.TAX);
        }


        public async Task<ListResultOutput<ComboboxItemDto>> GetDinePlanTaxForCombobox(FilterInputDto input)
        {
            var lstTax = (await _dineplantaxRepo.GetAllListAsync()).Where(t => t.FutureData == input.FutureData);
            var result = new ListResultOutput<ComboboxItemDto>(lstTax.Select(e => new ComboboxItemDto(e.Id.ToString(), e.TaxName)).ToList());
            return result;
        }

        public async Task<List<ApiTaxOutput>> ApiGetTaxes(ApiLocationInput input)
        {
            var loc = _locationRepo.Get(input.LocationId);
            if (loc != null)
            {
                CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", loc.CompanyRefId);
            }

            var output = new List<ApiTaxOutput>();
            if (input.TenantId == 0)
            {
                var location = await _locationRepo.GetAsync(input.LocationId);
                if (location != null)
                {
                    input.TenantId = location.TenantId;
                }
            }

            var taxLocations = await _dineplantaxlocationRepo.GetAllListAsync(a => a.TenantId == input.TenantId && a.DinePlanTax.FutureData == false);

            var myTaxLocations = new List<DinePlanTaxLocation>();

            if (input.LocationId != 0)
            {
                foreach (var taxLocation in taxLocations)
                {
                    if (await _locAppService.IsLocationExists(new CheckLocationInput
                    {
                        LocationId = input.LocationId,
                        Locations = taxLocation.Locations,
                        Group = taxLocation.Group,
                        NonLocations = taxLocation.NonLocations,
                        LocationTag = taxLocation.LocationTag

                    }))
                    {
                        myTaxLocations.Add(taxLocation);
                    }
                }
            }
            else
            {
                myTaxLocations = taxLocations.ToList();
            }


            foreach (var myTax in myTaxLocations)
            {
                var dinePlantax = _dineplantaxRepo.Get(myTax.DinePlanTaxRefId);
                TransactionType tType = null;
                if (dinePlantax.TransactionTypeId != null)
                {
                    tType = _transactiontypeManager.Get(dinePlantax.TransactionTypeId.Value);
                }
                if (tType == null)
                    continue;

                foreach (var mapping in _dineplantaxmappingRepo.GetAll().Where(a => a.DinePlanTaxLocationId == myTax.Id))
                {
                    var detail = new ApiTaxOutput
                    {
                        Id = myTax.Id,
                        Name = dinePlantax.TaxName,
                        Percentage = myTax.TaxPercentage,
                        TransactionTypeId = tType.Id,
                        TransactionTypeName = tType.Name
                    };
                    MenuItem mItem = null;
                    Category cate = null;
                    Department dept = null;

                    if (mapping.MenuItemId != null)
                    {
                        mItem = await _menuitemRepo.FirstOrDefaultAsync(t => t.Id == mapping.MenuItemId);
                    }
                    if (mapping.CategoryId != null)
                    {
                        cate = _categoryRepo.FirstOrDefault(t => t.Id == mapping.CategoryId);
                    }
                    if (mapping.DepartmentId != null)
                    {
                        dept = await _departmentRepo.FirstOrDefaultAsync(t => t.Id == mapping.DepartmentId);
                    }

                    if (mItem != null)
                    {
                        detail.MenuItemId = mItem.Id;
                        detail.MenuItemName = mItem.Name;
                    }
                    if (cate != null)
                    {
                        detail.CategoryId = cate.Id;
                        detail.CategoryName = cate.Name;
                    }
                    if (dept != null)
                    {
                        detail.DepartmentId = dept.Id;
                        detail.DepartmentName = dept.Name;
                    }
                    output.Add(detail);
                }
            }
            return output;
        }


        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _dineplantaxRepo.GetAsync(input.Id);
                item.IsDeleted = false;

                await _dineplantaxRepo.UpdateAsync(item);
            }
        }


        public async Task<IdInput> AddOrEditDinePlanTaxLocation(CreateOrUpdateDinePlanTaxLocationInput input)
        {
            await BusinessRulesForDinePlanTaxLocation(input);
            Collection<DinePlanTaxLocationEditDto> valuesOutput = new Collection<DinePlanTaxLocationEditDto>();         
            if (input.DinePlanTaxLocation.Id.HasValue && input.DinePlanTaxLocation.Id > 0)
            {
                var existEntry = await _dineplantaxlocationRepo.FirstOrDefaultAsync(t => t.Id == input.DinePlanTaxLocation.Id.Value);
                existEntry.TaxPercentage = input.DinePlanTaxLocation.TaxPercentage;
                UpdateLocationAndNonLocation(existEntry, input.DinePlanTaxLocation.LocationGroup);

                if (input.DinePlanTaxLocation.DinePlanTaxMappings != null && input.DinePlanTaxLocation.DinePlanTaxMappings.Any())
                {
                    var oids = new List<int>();
                    foreach (var otdto in input.DinePlanTaxLocation.DinePlanTaxMappings)
                    {
                        if (otdto.Id != null && otdto.Id > 0)
                        {
                            oids.Add(otdto.Id.Value);
                            var fromUpdation = existEntry.DinePlanTaxMappings.SingleOrDefault(a => a.Id.Equals(otdto.Id));
                            UpdateTaxMappingPortion(fromUpdation, otdto);
                        }
                        else
                        {
                            existEntry.DinePlanTaxMappings.Add(Mapper.Map<DinePlanTaxMappingEditDto, DinePlanTaxMapping>(otdto));
                        }
                    }
                    var tobeRemoved = existEntry.DinePlanTaxMappings.Where(a => !a.Id.Equals(0) && !oids.Contains(a.Id)).ToList();
                    foreach (var dinePlanTaxMapping in tobeRemoved)
                        try
                        {
                            await _dineplantaxmappingRepo.DeleteAsync(dinePlanTaxMapping.Id);
                        }
                        catch (Exception exception)
                        {
                            throw new UserFriendlyException(exception.Message);
                        }
                }
                else
                {
                    foreach (var dineplantaxmapping in existEntry.DinePlanTaxMappings) await _dineplantaxmappingRepo.DeleteAsync(dineplantaxmapping.Id);
                }

                return new IdInput { Id = existEntry.Id };
            }
            else
            {
                var dto = input.DinePlanTaxLocation.MapTo<DinePlanTaxLocation>();
                UpdateLocationAndNonLocation(dto, input.DinePlanTaxLocation.LocationGroup);
                await _dineplantaxlocationRepo.InsertOrUpdateAndGetIdAsync(dto);
                return new IdInput { Id = dto.Id };
            }
        }
        private void UpdateTaxMappingPortion(DinePlanTaxMapping fromUpdation, DinePlanTaxMappingEditDto otdto)
        {

            fromUpdation.DepartmentId = otdto.DepartmentId;
            fromUpdation.CategoryId = otdto.CategoryId;
            fromUpdation.MenuItemId = otdto.MenuItemId;
        }

        public async Task BusinessRulesForDinePlanTaxLocation(CreateOrUpdateDinePlanTaxLocationInput input)
        {
            List<SimpleLocationDto> newLocationToBeAdded = new List<SimpleLocationDto>();
            List<SimpleLocationGroupDto> newLocationGroupToBeAdded = new List<SimpleLocationGroupDto>();
            List<SimpleLocationTagDto> newLocationTagToBeAdded = new List<SimpleLocationTagDto>();
            Collection<DinePlanTaxLocationEditDto> dinePlanTaxLocationDtos = new Collection<DinePlanTaxLocationEditDto>();
            var locationGroupDto = input.DinePlanTaxLocation.LocationGroup;
            if (locationGroupDto.Locations.Count > 0)
            {
                newLocationToBeAdded.AddRange(locationGroupDto.Locations);
            }
            if (locationGroupDto.Groups.Count > 0)
            {
                newLocationGroupToBeAdded.AddRange(locationGroupDto.Groups);
            }
            if (locationGroupDto.LocationTags.Count > 0)
            {
                newLocationTagToBeAdded.AddRange(locationGroupDto.LocationTags);
            }
            if (input.FutureDataId.HasValue && input.FutureDataId > 0)
            {
                var futureData = await _futureDateRepository.FirstOrDefaultAsync(t => t.Id == input.FutureDataId);
                if (!string.IsNullOrEmpty(futureData.ObjectContents))
                {
                    var rsAlreadyExists = JsonConvert.DeserializeObject<GetDinePlanTaxForEditOutput>(futureData.ObjectContents);
                    dinePlanTaxLocationDtos = rsAlreadyExists.DinePlanTax.DinePlanTaxLocations.MapTo<Collection<DinePlanTaxLocationEditDto>>();
                    if (dinePlanTaxLocationDtos.Count > 0)
                    {
                        foreach (var lst in dinePlanTaxLocationDtos)
                        {
                            if (lst.FutureDataSno == input.DinePlanTaxLocation.FutureDataSno)
                            {
                                dinePlanTaxLocationDtos.Remove(lst);
                                break;
                            }
                        }
                    }
                }
                else
                {
                    return;
                }

            }
            else
            {
                var iQPv = _dineplantaxlocationRepo.GetAll().Where(t => t.DinePlanTaxRefId == input.DinePlanTaxLocation.DinePlanTaxRefId);
                if (input.DinePlanTaxLocation.Id.HasValue)
                    iQPv = iQPv.Where(t => t.Id != input.DinePlanTaxLocation.Id);
                var otherProgramValues = await iQPv.ToListAsync();
                dinePlanTaxLocationDtos = otherProgramValues.MapTo<Collection<DinePlanTaxLocationEditDto>>();
            }
            List<SimpleLocationDto> locationAlreadyMapped = new List<SimpleLocationDto>();
            List<SimpleLocationGroupDto> locationGroupAlreadyMapped = new List<SimpleLocationGroupDto>();
            List<SimpleLocationTagDto> locationTagAlreadyMapped = new List<SimpleLocationTagDto>();
            foreach (var lst in dinePlanTaxLocationDtos)
            {
                UpdateLocationAndNonLocationForEdit(lst, lst.LocationGroup);
                if (lst.LocationGroup.Locations.Count > 0)
                    locationAlreadyMapped.AddRange(lst.LocationGroup.Locations);
                if (lst.LocationGroup.Groups.Count > 0)
                    locationGroupAlreadyMapped.AddRange(lst.LocationGroup.Groups);
                if (lst.LocationGroup.LocationTags.Count > 0)
                    locationTagAlreadyMapped.AddRange(lst.LocationGroup.LocationTags);

            }

            if (locationAlreadyMapped.Count > 0)
            {
                var intersectLocations = locationAlreadyMapped.Select(t => t.Id).ToList().Intersect(newLocationToBeAdded.Select(t => t.Id).ToList()).ToList();
                if (intersectLocations.Count > 0)
                {
                    var locationAlreadyExists = locationAlreadyMapped.FindAll(t => intersectLocations.Contains(t.Id)).ToList();
                    string locationAlreadExists = string.Join(",", locationAlreadyExists.Select(t => t.Name));
                    throw new UserFriendlyException("This Location(s) :" + locationAlreadExists + " Already mapped with Another Values");
                }
            }
            if (locationGroupAlreadyMapped.Count > 0)
            {
                var intersectLocationGroups = locationGroupAlreadyMapped.Select(t => t.Id).ToList().Intersect(newLocationGroupToBeAdded.Select(t => t.Id).ToList()).ToList();
                if (intersectLocationGroups.Count > 0)
                {
                    var locationGroupAlreadyExists = locationGroupAlreadyMapped.FindAll(t => intersectLocationGroups.Contains(t.Id)).ToList();
                    string locationgroupAlreadyExists = string.Join(",", locationGroupAlreadyExists.Select(t => t.Name));
                    throw new UserFriendlyException("This Location Group(s) :" + locationgroupAlreadyExists + " Already mapped with Another Values");
                }
            }
            if (locationTagAlreadyMapped.Count > 0)
            {
                var intersectLocationTags = locationTagAlreadyMapped.Select(t => t.Id).ToList().Intersect(newLocationTagToBeAdded.Select(t => t.Id).ToList()).ToList();
                if (intersectLocationTags.Count > 0)
                {
                    var locationTagAlreadyExists = locationGroupAlreadyMapped.FindAll(t => intersectLocationTags.Contains(t.Id)).ToList();
                    string locationtagAlreadyExists = string.Join(",", locationTagAlreadyExists.Select(t => t.Name));
                    throw new UserFriendlyException("This Location Tag(s) :" + locationtagAlreadyExists + " Already mapped with Another Values");
                }
            }
        }

        public async Task DeleteDinePlanTaxLocation(DeleteDinePlanTaxInput input)
        {            
            var dinetaxlocations =
                            await _dineplantaxlocationRepo.GetAll().Where(t => t.Id == input.Id).ToListAsync();

            if (dinetaxlocations == null)
            {
                throw new UserFriendlyException("TAXNOTFOUND");
            }

            foreach (var dinetaxlocation in dinetaxlocations)
            {
                foreach (var map in dinetaxlocation.DinePlanTaxMappings.ToArray())
                {
                    await _dineplantaxmappingRepo.DeleteAsync(map.Id);
                }

                await _dineplantaxlocationRepo.DeleteAsync(dinetaxlocation.Id);
            }

            await _syncAppService.UpdateSync(SyncConsts.TAX);
            // }
        }
    }
}