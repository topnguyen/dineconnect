﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Utility.Exporter;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.Implementation
{
    public class LocationGroupAppService : DineConnectAppServiceBase, ILocationGroupAppService
    {
        private readonly ILocationGroupExcelExporter _locationgroupexporter;
        private readonly ILocationGroupManager _locationgroupManager;
        private readonly IRepository<LocationGroup> _locationgroupRepo;
        private readonly IRepository<Location> _locationRepo;

        public LocationGroupAppService(ILocationGroupManager locationgroupManager,
            IRepository<LocationGroup> locationGroupRepo,
            IRepository<Location> locationRepo,
            ILocationGroupExcelExporter locationgroupexporter)
        {
            _locationgroupManager = locationgroupManager;
            _locationgroupRepo = locationGroupRepo;
            _locationgroupexporter = locationgroupexporter;
            _locationRepo = locationRepo;
        }

        public async Task<PagedResultOutput<LocationGroupListDto>> GetAll(GetLocationGroupInput input)
        {
            var allItems = _locationgroupRepo.GetAll();
            if (input.Operation == "SEARCH")
            {
                allItems = _locationgroupRepo
               .GetAll()
               .Include(lg => lg.Locations)
               .WhereIf(!input.Filter.IsNullOrEmpty(), p => p.Name.Contains(input.Filter) || p.Code.Contains(input.Filter) || p.Id.ToString().Contains(input.Filter));
            }
            else
            {
                allItems = _locationgroupRepo
               .GetAll()
               .Include(lg => lg.Locations)
               .WhereIf(!input.Filter.IsNullOrEmpty(), p => p.Name.Contains(input.Filter) || p.Code.Contains(input.Filter) || p.Id.ToString().Contains(input.Filter));
            }
            foreach(var lst in allItems)
            {
                lst.Locations.Select(l => new LocationListDto
                {
                    LocationGroupId = lst.Id,
                    LocationGroupName = lst.Name,
                    Id = l.Id,
                    Name = l.Name,
                    Code = l.Code
                });
            }
            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<LocationGroupListDto>>();
            var allItemCount = await allItems.CountAsync();

            foreach (var lst in allListDtos)
            {
                lst.LocationCount = await _locationRepo.GetAll().CountAsync(t => t.LocationGroups.Where(lg => lg.Id == input.LocationGroupId).Any());
            }

            return new PagedResultOutput<LocationGroupListDto>(allItemCount, allListDtos);
        }

        public async Task<PagedResultOutput<SimpleLocationGroupDto>> GetSimpleAll(GetLocationGroupInput input)
        {
            var allItems = _locationgroupRepo
              .GetAll()
              .WhereIf(
                  !input.Filter.IsNullOrEmpty(),
                  p => p.Name.Contains(input.Filter)
                       || (p.Code != null
                           && p.Code.Contains(input.Filter)) || p.Id.ToString().Contains(input.Filter));

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.MapTo<List<SimpleLocationGroupDto>>();
            var allItemCount = await allItems.CountAsync();
            return new PagedResultOutput<SimpleLocationGroupDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetLocationGroupInput input)
        {
            input.MaxResultCount = 10000;
            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<LocationGroupListDto>>();
            return _locationgroupexporter.ExportToFile(allListDtos);
        }

        public async Task<GetLocationGroupForEditOutput> GetLocationGroupForEdit(NullableIdInput input)
        {
            LocationGroupEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _locationgroupRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<LocationGroupEditDto>();
            }
            else
            {
                editDto = new LocationGroupEditDto();
            }

            return new GetLocationGroupForEditOutput
            {
                LocationGroup = editDto
            };
        }

        public async Task CreateOrUpdateLocationGroup(CreateOrUpdateLocationGroupInput input)
        {
            if (input.LocationGroup.Id.HasValue)
            {
                await UpdateLocationGroup(input);
            }
            else
            {
                await CreateLocationGroup(input);
            }
        }

        public async Task DeleteLocationGroup(IdInput input)
        {
            var locations = await _locationRepo.GetAllListAsync(t => t.LocationGroups.Where(lt => lt.Id == input.Id).Any());

            foreach (var location in locations)
            {
                location.LocationGroups.Remove(await _locationgroupRepo.GetAsync(input.Id));
                await _locationRepo.UpdateAsync(location);
            }
            await _locationgroupRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateLocationGroup(CreateOrUpdateLocationGroupInput input)
        {
            var item = await _locationgroupRepo.GetAsync(input.LocationGroup.Id.Value);
            var dto = input.LocationGroup;
            item.Name = dto.Name;
            item.Code = dto.Code;

            //TODO: SERVICE LocationGroup Update Individually

            CheckErrors(await _locationgroupManager.CreateSync(item));
        }

        protected virtual async Task CreateLocationGroup(CreateOrUpdateLocationGroupInput input)
        {
            var dto = input.LocationGroup.MapTo<LocationGroup>();

            CheckErrors(await _locationgroupManager.CreateSync(dto));
        }

        public async Task<ListResultOutput<LocationGroupListDto>> GetNames()
        {
            var lstLocationGroup = await _locationgroupRepo.GetAll().ToListAsync();
            return new ListResultOutput<LocationGroupListDto>(lstLocationGroup.MapTo<List<LocationGroupListDto>>());
        }
    }
}