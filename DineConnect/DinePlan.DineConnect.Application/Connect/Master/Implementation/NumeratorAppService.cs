﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Utility.Exporter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Processor;

namespace DinePlan.DineConnect.Connect.Master.Implementation
{
    public class NumeratorAppService : DineConnectAppServiceBase, INumeratorAppService
    {
        private readonly IExcelExporter _exporter;
        private readonly IPaymentTypeManager _paManager;
        private readonly IRepository<Numerator> _numeratorManager;
        private readonly ISyncAppService _syncAppService;
        private readonly ILocationAppService _locAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public NumeratorAppService(IRepository<Numerator> numeratorManager,
            ILocationAppService locAppService,
            IExcelExporter exporter,
            ISyncAppService syncAppService,
            IPaymentTypeManager paManager,
            IUnitOfWorkManager unitOfWorkManager)
        {
            _numeratorManager = numeratorManager;
            _exporter = exporter;
            _syncAppService = syncAppService;
            _paManager = paManager;
            _unitOfWorkManager = unitOfWorkManager;
            _locAppService = locAppService;
        }

        public PagedResultOutput<NumeratorListDto> GetAll(NumeratorInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                IQueryable<Numerator> allItems = _numeratorManager.GetAll().Where(x => x.IsDeleted == input.IsDeleted)
                    .WhereIf(
                            !input.Filter.IsNullOrEmpty(),
                            p => p.Name.Contains(input.Filter));

                var allItemCount = allItems.Count();

                var sortMenuItems = allItems.AsQueryable().ToList();

                var allListDtos = sortMenuItems.MapTo<List<NumeratorListDto>>();

                return new PagedResultOutput<NumeratorListDto>(allItemCount, allListDtos);
            }
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _numeratorManager.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<NumeratorListDto>>();
            var baseE = new BaseExportObject
            {
                ExportObject = allListDtos,
                ColumnNames = new[] { "Id", "Name", "NumberFormat", "TicketFormat" }
            };
            return _exporter.ExportToFile(baseE, L("Numerator"));
        }

        public async Task<GetNumeratorForEditOutput> GetNumeratorForEdit(NullableIdInput input)
        {
            GetNumeratorForEditOutput output = new
                GetNumeratorForEditOutput();
            var allLocations = new List<ComboboxItemDto>();

            NumeratorEditDto editDto;

            if (input.Id.HasValue)
            {
                var numerator = await _numeratorManager.GetAsync(input.Id.Value);
                editDto = numerator.MapTo<NumeratorEditDto>();
            }
            else
            {
                editDto = new NumeratorEditDto();
            }

            UpdateLocationAndNonLocationForEdit(editDto, output.LocationGroup);

            output.Numerator = editDto;
            if(input.Id == null)
            {
                output.Numerator.NumberFormat = "#";
                output.Numerator.OutputFormat = "[Number]";
            }
            return output;
        }

        public async Task<int> CreateOrUpdateNumerator(CreateOrUpdateNumeratorInput input)
        {
            var output = 0;
            if (input.Numerator.Id.HasValue && input.Numerator.Id > 0)
            {
                await UpdateNumerator(input);
                output = input.Numerator.Id.Value;
            }
            else
            {
                output = await CreateNumerator(input);
            }

            return output;
        }

        protected virtual async Task UpdateNumerator(CreateOrUpdateNumeratorInput input)
        {
            var item = await _numeratorManager.GetAsync(input.Numerator.Id.Value);
            var dto = input.Numerator;
            dto.MapTo(item);
            item.Name = input.Numerator.Name;
            item.NumberFormat = input.Numerator.NumberFormat;
            item.OutputFormat = input.Numerator.OutputFormat;
            item.ResetNumerator = input.Numerator.ResetNumerator;
            item.ResetNumeratorType = input.Numerator.ResetNumeratorType;
        }

        protected virtual async Task<int> CreateNumerator(CreateOrUpdateNumeratorInput input)
        {
            var item = input.Numerator.MapTo<Numerator>();

            item.Name = input.Numerator.Name;
            item.NumberFormat = input.Numerator.NumberFormat;
            item.OutputFormat = input.Numerator.OutputFormat;
            return await _numeratorManager.InsertAndGetIdAsync(item);
        }

        public async Task DeleteNumerator(IdInput input)
        {
            await _numeratorManager.DeleteAsync(input.Id);
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _numeratorManager.GetAsync(input.Id);
                item.IsDeleted = false;

                await _numeratorManager.UpdateAsync(item);
            }
        }

        public async Task<ListResultOutput<ApiNumeratorOutput>> ApiGetNumerators(ApiLocationInput locationInput)
        {
            var allListDtos = new List<ApiNumeratorOutput>();

            if (locationInput.RefObjects==null)
            {
                if (locationInput.LocationId > 0)
                {
                    locationInput.RefObjects = new int[]{locationInput.LocationId};
                }
                else
                {
                    return null;
                }
            }

            if (!locationInput.RefObjects.Any())
            {
                return null;
            }

            var allItems = await _numeratorManager.GetAllListAsync(a => a.TenantId == locationInput.TenantId 
                                                                        && locationInput.RefObjects.Contains(a.Id));


            foreach (var numerator in allItems)
            {
                allListDtos.Add(new ApiNumeratorOutput
                {
                    Id = numerator.Id,
                    Name = numerator.Name,
                    OutputFormat = numerator.OutputFormat,
                    NumberFormat = numerator.NumberFormat,
                    ResetNumerator = numerator.ResetNumerator,
                    ResetNumeratorType = numerator.ResetNumeratorType,
                    TenantId = numerator.TenantId
                });
            }

            return new ListResultOutput<ApiNumeratorOutput>(allListDtos);
        }
        public ListResultDto<ComboboxItemDto> GetResetNumeratorTypes()
        {
            var returnList = new List<ComboboxItemDto>();

            var i = 0;
            foreach (var name in Enum.GetNames(typeof(ResetNumeratorType)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }
    }
}