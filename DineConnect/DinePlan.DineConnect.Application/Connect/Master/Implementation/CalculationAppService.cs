﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Connect.Tag;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Utility.Exporter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.Implementation
{
    public class CalculationAppService : DineConnectAppServiceBase, ICalculationAppService
    {
        private readonly IExcelExporter _exporter;
        private readonly ISyncAppService _syncAppService;
        private readonly ICalculationManager _calculationManager;
        private readonly IRepository<Calculation> _calculationRepo;
        private readonly ILocationAppService _locationAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public CalculationAppService(ICalculationManager calculationManager,
            ILocationAppService locationAppService,
            IRepository<Calculation> calculationRepo,
            ISyncAppService syncAppService,
            IExcelExporter exporter,
            IUnitOfWorkManager unitOfWorkManager)
        {
            _calculationManager = calculationManager;
            _calculationRepo = calculationRepo;
            _exporter = exporter;
            _unitOfWorkManager = unitOfWorkManager;
            _syncAppService = syncAppService;
            _locationAppService = locationAppService;
        }

        public async Task<PagedResultOutput<CalculationListDto>> GetAll(GetCalculationInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var allItems = _calculationRepo.GetAll().WhereIf(
                            !input.Filter.IsNullOrEmpty(),
                            p => p.Name.Equals(input.Filter)
                        );
                var allMyEnItems = SearchLocation(allItems, input.LocationGroup).OfType<Calculation>();

                var allItemCount = allMyEnItems.Count();

                var sortMenuItems = allMyEnItems.AsQueryable()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList();

                var allListDtos = sortMenuItems.MapTo<List<CalculationListDto>>();
                return new PagedResultOutput<CalculationListDto>(
                    allItemCount,
                    allListDtos
                    );
            }
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _calculationRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<CalculationListDto>>();
            var baseE = new BaseExportObject
            {
                ExportObject = allListDtos,
                ColumnNames = new[] { "Id", "Name" }
            };
            return _exporter.ExportToFile(baseE, L("Calculation"));
        }

        public async Task<GetCalculationForEditOutput> GetCalculationForEdit(NullableIdInput input)
        {
            CalculationEditDto editDto;
            var output = new GetCalculationForEditOutput();

            if (input.Id.HasValue)
            {
                var hDto = await _calculationRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<CalculationEditDto>();
            }
            else
            {
                editDto = new CalculationEditDto();
            }

            UpdateLocationAndNonLocationForEdit(editDto, output.LocationGroup);

            var allDepartments = new List<ComboboxItemDto>();
            if (!string.IsNullOrEmpty(editDto.Departments))
            {
                allDepartments = JsonConvert.DeserializeObject<List<ComboboxItemDto>>(editDto.Departments);
            }

            output.Departments = allDepartments;
            output.Calculation = editDto;

            return output;
        }

        public async Task CreateOrUpdateCalculation(CreateOrUpdateCalculationInput input)
        {
            if (input?.LocationGroup == null)
                return;

            /*Start Group*/
            var allLocations = "";
            if (input?.LocationGroup != null)
            {
                if (input.LocationGroup.Group && input.LocationGroup.Groups.Any())
                {
                    allLocations = JsonConvert.SerializeObject(input.LocationGroup.Groups);
                }
                else if (!input.LocationGroup.Group && input.LocationGroup.Locations.Any())
                {
                    allLocations = JsonConvert.SerializeObject(input.LocationGroup.Locations);
                }
            }
            /*End Group*/

            var allDepartments = "";
            if (input.Calculation != null && input.Departments != null && input.Departments.Any())
            {
                allDepartments = JsonConvert.SerializeObject(input.Departments);
            }
            if (input.Calculation?.Id != null)
            {
                await UpdateCalculation(input, allDepartments);
            }
            else
            {
                if (input.Calculation != null)
                {
                    input.Calculation.Departments = allDepartments;
                    await CreateCalculation(input);
                }
            }
            await _syncAppService.UpdateSync(SyncConsts.CALCULATION);
        }

        public async Task DeleteCalculation(IdInput input)
        {
            await _calculationRepo.DeleteAsync(input.Id);
        }

        public async Task<ListResultOutput<CalculationListDto>> GetNames()
        {
            var lstCalculation = await _calculationRepo.GetAll().ToListAsync();
            return new ListResultOutput<CalculationListDto>(lstCalculation.MapTo<List<CalculationListDto>>());
        }

        protected virtual async Task UpdateCalculation(CreateOrUpdateCalculationInput input,
            string depts)
        {
            var item = await _calculationRepo.GetAsync(input.Calculation.Id.Value);
            var dto = input.Calculation;

            item.Name = dto.Name;
            item.ButtonHeader = dto.ButtonHeader;
            item.SortOrder = dto.SortOrder;
            item.FontSize = dto.FontSize;
            item.ConfirmationType = dto.ConfirmationType;
            item.CalculationTypeId = dto.CalculationTypeId;
            item.Amount = dto.Amount;
            item.IncludeTax = dto.IncludeTax;
            item.DecreaseAmount = dto.DecreaseAmount;
            item.TransactionTypeId = dto.TransactionTypeId;
            item.Departments = depts;
            UpdateLocationAndNonLocation(item, input.LocationGroup);

            CheckErrors(await _calculationManager.CreateSync(item));
        }

        private void UpdateTag(TicketTag to, TicketTagListDto from)
        {
            to.Name = from.Name;
            to.AlternateName = from.AlternateName;
        }

        protected virtual async Task CreateCalculation(CreateOrUpdateCalculationInput input)
        {
            var dto = input.Calculation.MapTo<Calculation>();
            UpdateLocationAndNonLocation(dto, input.LocationGroup);
            CheckErrors(await _calculationManager.CreateSync(dto));
        }

        public async Task<ApiCalculationOutput> ApiGetCalculations(ApiLocationInput locationInput)
        {
            ApiCalculationOutput output = new ApiCalculationOutput();

            try
            {
                var orgId = await _locationAppService.GetOrgnizationIdForLocationId(locationInput.LocationId);
                if (orgId > 0)
                {
                    CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);
                }

                var allItems = await _calculationRepo.GetAllListAsync(a => a.TenantId == locationInput.TenantId);


                var returnDto = new List<ApiCalculation>();
                if (locationInput.LocationId > 0)
                {
                    foreach (var myDto in allItems)
                    {
                        if (await _locationAppService.IsLocationExists(new CheckLocationInput
                        {
                            LocationId = locationInput.LocationId,
                            Locations = myDto.Locations,
                            Group = myDto.Group,
                            NonLocations = myDto.NonLocations,
                            LocationTag = myDto.LocationTag,

                        }))
                        {
                            returnDto.Add(myDto.MapTo<ApiCalculation>());
                        }
                    }
                }
                else
                {
                    returnDto = allItems.MapTo<List<ApiCalculation>>();
                }
                output.Calculations = returnDto;
            }
            catch (Exception exception)
            {
                var mess = exception.Message;
            }
            return output;
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _calculationRepo.GetAsync(input.Id);
                item.IsDeleted = false;

                await _calculationRepo.UpdateAsync(item);
            }
        }
    }
}