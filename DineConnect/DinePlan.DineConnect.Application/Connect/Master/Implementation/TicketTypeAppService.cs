﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Utility.Exporter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Castle.Core.Logging;
using DinePlan.DineConnect.Connect.Sync;

namespace DinePlan.DineConnect.Connect.Master.Implementation
{
    public class TicketTypeAppService : DineConnectAppServiceBase, ITicketTypeAppService
    {
        private readonly IExcelExporter _exporter;
        private readonly IRepository<TicketType> _ticketTypeManager;
        private readonly IRepository<TicketTypeConfiguration> _ticketTypeConfigurationManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ILocationAppService _locationAppService;
        private readonly ISyncAppService _syncAppService;
        private readonly ILogger _logger;

        public TicketTypeAppService(IRepository<TicketType> ticketTypeManager, ISyncAppService syncAppService,ILogger logger,
            IRepository<TicketTypeConfiguration> ticketTypeConfigurationManager,
            IExcelExporter exporter,
            IUnitOfWorkManager unitOfWorkManager,
            ILocationAppService locationAppService)
        {
            _ticketTypeManager = ticketTypeManager;
            _ticketTypeConfigurationManager = ticketTypeConfigurationManager;
            _exporter = exporter;
            _unitOfWorkManager = unitOfWorkManager;
            _locationAppService = locationAppService;
            _syncAppService = syncAppService;
            _logger = logger;
        }

        public async Task<PagedResultOutput<TicketTypeConfigurationListDto>> GetAll(GetTicketTypeInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                IQueryable<TicketTypeConfiguration> allItems = _ticketTypeConfigurationManager.GetAll()
                    .Where(x => x.IsDeleted == input.IsDeleted)
                    .WhereIf(!input.Filter.IsNullOrEmpty(),
                                p => p.Name.Contains(input.Filter));
                if (!input.Location.IsNullOrWhiteSpace())
                {
                    var tickettype = _ticketTypeManager.GetAll().Where(i => i.IsDeleted == input.IsDeleted).WhereIf(
                    !input.Location.IsNullOrWhiteSpace(),
                    p => p.Locations.Contains(input.Location));
                    var tickettypeIds = tickettype.Select(t => t.TicketTypeConfigurationId).ToList();
                    allItems = allItems.Where(t => tickettypeIds.Contains(t.Id));
                }
                var allItemCount = allItems.Count();

                var sortMenuItems = allItems.AsQueryable().ToList();

                var allListDtos = sortMenuItems.MapTo<List<TicketTypeConfigurationListDto>>();

                return new PagedResultOutput<TicketTypeConfigurationListDto>(allItemCount, allListDtos);
            }
        }

        public async Task<FileDto> GetAllToExcel(GetTicketTypeInput input)
        {
            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<TicketTypeConfigurationListDto>>();
            var baseE = new BaseExportObject
            {
                ExportObject = allListDtos,
                ColumnNames = new[] { "Id", "Name"}
            };
            return _exporter.ExportToFile(baseE, L("TicketType"));
        }

        public async Task DeleteTicketType(IdInput input)
        {
            var rstickettype=await _ticketTypeManager.GetAllListAsync(t=>t.TicketTypeConfigurationId==input.Id);
            if (rstickettype == null)
            {
                throw new UserFriendlyException("TICKETTYPENOTFOUND");
            }

            foreach (var tickettype in rstickettype)
            {

                await _ticketTypeManager.DeleteAsync(tickettype.Id);
            }
            await _ticketTypeConfigurationManager.DeleteAsync(input.Id);
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var rstickettypelist=await _ticketTypeManager.GetAllListAsync(t=>t.TicketTypeConfigurationId==input.Id);
                foreach (var tickettype in rstickettypelist)
                {
                    var rstickettype=rstickettypelist.FirstOrDefault(t=>t.Id==tickettype.Id);
                    if (rstickettype != null)
                    {
                        rstickettype.IsDeleted = false;
                        rstickettype.DeleterUserId = null;
                        rstickettype.DeletionTime = null;
                        await _ticketTypeManager.UpdateAsync(rstickettype);
                    }
                }
                var item = await _ticketTypeConfigurationManager.GetAsync(input.Id);
                item.IsDeleted = false;

                await _ticketTypeConfigurationManager.UpdateAsync(item);
            }
        }

        public async Task<GetTicketTypeOutputDto> GetTicketTypeForEdit(TicketTypeFilterInputDto input)
        {
            TicketTypeConfigurationEditDto editDto;

            try
            {
                if (input.Id.HasValue)
                {
                    List<SimpleLocationDto> filterLocation = new List<SimpleLocationDto>();
                    List<SimpleLocationGroupDto> filterLocationGroup = new List<SimpleLocationGroupDto>();
                    List<int> filterLocationRefIds = new List<int>();
                    List<int> filterLocationGroupRefIds = new List<int>();
                    List<int> filterLocationTagRefIds = new List<int>();
                    if (input.LocationGroupToBeFiltered != null)
                    {
                        filterLocationRefIds = input.LocationGroupToBeFiltered.Locations.Select(t => t.Id).ToList();
                        filterLocationGroupRefIds = input.LocationGroupToBeFiltered.Groups.Select(t => t.Id).ToList();
                        filterLocationTagRefIds = input.LocationGroupToBeFiltered.LocationTags.Select(t => t.Id).ToList();
                    }

                    var ticketTypes = await _ticketTypeManager.GetAllListAsync(t => t.TicketTypeConfigurationId == input.Id.Value);
                    var tempValuesFilters = ticketTypes.MapTo<Collection<TicketTypeEditDto>>();

                    var ticketTypeConfiguration = await _ticketTypeConfigurationManager.FirstOrDefaultAsync(e => e.Id == input.Id.Value);
                    editDto = ticketTypeConfiguration.MapTo<TicketTypeConfigurationEditDto>();

                    Collection<TicketTypeEditDto> valuesOutput = new Collection<TicketTypeEditDto>();
                    foreach (var lst in tempValuesFilters)
                    {
                        UpdateLocationAndNonLocationForEdit(lst, lst.LocationGroup);
                        if (filterLocationRefIds.Count > 0 || filterLocationGroupRefIds.Count > 0 || filterLocationTagRefIds.Count > 0)
                        {
                            if (lst.LocationGroup.Locations.Select(t => t.Id).Intersect(filterLocationRefIds).ToList().Count > 0)
                                valuesOutput.Add(lst);
                            else if (lst.LocationGroup.Groups.Select(t => t.Id).Intersect(filterLocationGroupRefIds).ToList().Count > 0)
                                valuesOutput.Add(lst);
                            else if (lst.LocationGroup.LocationTags.Select(t => t.Id).Intersect(filterLocationTagRefIds).ToList().Count > 0)
                                valuesOutput.Add(lst);
                        }
                        else
                        {
                            valuesOutput.Add(lst);
                        }
                    }

                    editDto.TicketTypes = valuesOutput;
                }

                else
                {
                    editDto = new TicketTypeConfigurationEditDto();
                }

                return new GetTicketTypeOutputDto
                {
                    TicketTypeConfiguration = editDto,
                    TicketType = new TicketTypeEditDto()
                };
            }
            catch (Exception ex)
            {
                _logger.Fatal("Save TicketType : " , ex);
            }

            return null;
        }

        public async Task<IdInput> AddOrEditTicketType(CreateOrUpdateTicketTypeInput input)
        {
            var ticketType = input.TicketType;
            if (ticketType.Id.HasValue)
            {
                var existEntry = await _ticketTypeManager.FirstOrDefaultAsync(t => t.Id == ticketType.Id.Value);
                ticketType.MapTo(existEntry);
                UpdateLocationAndNonLocation(existEntry, ticketType.LocationGroup);
                await _ticketTypeManager.UpdateAsync(existEntry);
                await _syncAppService.UpdateSync(SyncConsts.TICKETTYPE);
                return new IdInput { Id = existEntry.Id };
            }
            else
            {
                var dto = ticketType.MapTo<TicketType>();
                UpdateLocationAndNonLocation(dto, ticketType.LocationGroup);
                await _ticketTypeManager.InsertOrUpdateAndGetIdAsync(dto);
                await _syncAppService.UpdateSync(SyncConsts.TICKETTYPE);
                return new IdInput { Id = dto.Id };
            }


        }
        public async Task DeleteTicketTypeMapping(IdInput input)
        {
            await _ticketTypeManager.DeleteAsync(t => t.Id == input.Id);
        }
        public async Task<ListResultOutput<TicketTypeEditDto>> ApiGetTicketTypes(ApiLocationInput locationInput)
        {
            var orgId = await _locationAppService.GetOrgnizationIdForLocationId(locationInput.LocationId);
            if (orgId > 0)
            {
                CurrentUnitOfWork.SetFilterParameter("ConnectFilter", "oid", orgId);
            }

            var allItems = await _ticketTypeManager.GetAllListAsync(a => a.TenantId == locationInput.TenantId);

            var allListDtos = allItems.MapTo<List<TicketTypeEditDto>>();
            var returnDto = new List<TicketTypeEditDto>();

            if (!string.IsNullOrEmpty(locationInput.Tag))
            {
                var allLocations = await _locationAppService.GetLocationsByLocationGroupCode(locationInput.Tag);
                if (allLocations != null && allLocations.Items.Any())
                    foreach (var ticketTypeList in allListDtos)
                        if (!ticketTypeList.Group && !string.IsNullOrEmpty(ticketTypeList.Locations) &&
                            !ticketTypeList.LocationTag)
                        {
                            var scLocations =
                                JsonConvert.DeserializeObject<List<SimpleLocationGroupDto>>(ticketTypeList.Locations);
                            if (scLocations.Any())
                                foreach (var i in scLocations.Select(a => a.Id))
                                    if (allLocations.Items.Select(a => a.Value).Contains(i.ToString()))
                                        returnDto.Add(ticketTypeList);
                        }
                        else
                        {
                            returnDto.Add(ticketTypeList);
                        }
            }

            else if (locationInput.LocationId > 0)
            {
                foreach (var myDto in allListDtos)
                {
                    if (await _locationAppService.IsLocationExists(new CheckLocationInput
                    {
                        LocationId = locationInput.LocationId,
                        Locations = myDto.Locations,
                        Group = myDto.Group,
                        NonLocations = myDto.NonLocations,
                        LocationTag = myDto.LocationTag

                    }))
                    {
                        returnDto.Add(myDto);
                    }
                }
            }
            else
            {
                returnDto = allListDtos;
            }

            foreach(var ticketTypeEditDto in returnDto)
            {
                var ticketTypeConfiguration = await _ticketTypeConfigurationManager.FirstOrDefaultAsync(e => e.Id == ticketTypeEditDto.TicketTypeConfigurationId);
                if (ticketTypeConfiguration != null)
                    ticketTypeEditDto.Name = ticketTypeConfiguration.Name;
            }
            return new ListResultOutput<TicketTypeEditDto>(returnDto);
        }
    }
}