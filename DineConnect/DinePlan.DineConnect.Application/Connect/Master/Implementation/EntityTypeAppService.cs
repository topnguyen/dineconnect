﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using AutoMapper;
using DinePlan.DineConnect.Connect.Entity;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.Implementation
{
    public class EntityTypeAppService : DineConnectAppServiceBase, IEntityTypeAppService
    {
        private readonly IEntityTypeListExcelExporter _entitytypeExporter;
        private readonly IEntityTypeManager _entitytypeManager;
        private readonly IRepository<EntityType> _entitytypeRepo;
        private readonly IRepository<EntityCustomField> _entityCustomManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public EntityTypeAppService(IEntityTypeManager entitytypeManager,
            IRepository<EntityType> entityTypeRepo,
            IEntityTypeListExcelExporter entitytypeExporter,
            IRepository<EntityCustomField> entityCustomManager,
            IUnitOfWorkManager unitOfWorkManager)
        {
            _entitytypeManager = entitytypeManager;
            _entitytypeRepo = entityTypeRepo;
            _entitytypeExporter = entitytypeExporter;
            _entityCustomManager = entityCustomManager;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task CreateEntityForTenant(int tenantId)
        {
            var memberType = new EntityType { Name = "Members", EntityName = "Member", DisplayFormat = "[Name]-[Phone]", PrimaryFieldName = "Phone", TenantId = tenantId };
            var member = _entitytypeRepo.InsertAndGetId(memberType);

            var mncf = new EntityCustomField()
            {
                EntityTypeId = member,
                FieldType = 0,
                Name = "Name"
            };
            await _entityCustomManager.InsertAsync(mncf);

            var address = new EntityCustomField()
            {
                EntityTypeId = member,
                FieldType = 1,
                Name = "Address"
            };
            await _entityCustomManager.InsertAsync(address);
        }

        public async Task<PagedResultOutput<EntityTypeListDto>> GetAll(GetEntityTypeInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var allItems = _entitytypeRepo.GetAll()
                    .Where(i => i.IsDeleted == input.IsDeleted);

                if (input.Operation == "SEARCH")
                {
                    allItems = allItems
                        .WhereIf(
                            !input.Filter.IsNullOrEmpty(),
                            p => p.Id.Equals(input.Filter)
                        );
                }

                var sortMenuItems = await allItems
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var allListDtos = sortMenuItems.MapTo<List<EntityTypeListDto>>();

                var allItemCount = await allItems.CountAsync();

                return new PagedResultOutput<EntityTypeListDto>(
                    allItemCount,
                    allListDtos
                    );
            }
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _entitytypeRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<EntityTypeListDto>>();
            return _entitytypeExporter.ExportToFile(allListDtos);
        }

        public async Task<GetEntityTypeForEditOutput> GetEntityTypeForEdit(NullableIdInput input)
        {
            EntityTypeEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _entitytypeRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<EntityTypeEditDto>();
            }
            else
            {
                editDto = new EntityTypeEditDto();
            }

            return new GetEntityTypeForEditOutput
            {
                EntityType = editDto
            };
        }

        public async Task CreateOrUpdateEntityType(CreateOrUpdateEntityTypeInput input)
        {
            if (input.EntityType.Id.HasValue)
            {
                await UpdateEntityType(input);
            }
            else
            {
                await CreateEntityType(input);
            }
        }

        public async Task DeleteEntityType(IdInput input)
        {
            await _entitytypeRepo.DeleteAsync(input.Id);
        }

        public async Task<ListResultOutput<EntityTypeListDto>> GetIds()
        {
            var lstEntityType = await _entitytypeRepo.GetAll().ToListAsync();
            return new ListResultOutput<EntityTypeListDto>(lstEntityType.MapTo<List<EntityTypeListDto>>());
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _entitytypeRepo.GetAsync(input.Id);
                item.IsDeleted = false;

                await _entitytypeRepo.UpdateAsync(item);
            }
        }

        protected virtual async Task UpdateEntityType(CreateOrUpdateEntityTypeInput input)
        {
            var item = await _entitytypeRepo.GetAsync(input.EntityType.Id.Value);
            var dto = input.EntityType;

            item.Name = dto.Name;
            item.DisplayFormat = dto.DisplayFormat;
            item.EntityName = dto.EntityName;
            item.PrimaryFieldFormat = dto.PrimaryFieldFormat;
            item.PrimaryFieldName = dto.PrimaryFieldName;

            if (dto?.Fields != null && dto.Fields.Any())
            {
                var oids = new List<int>();
                foreach (var otdto in dto.Fields)
                {
                    if (otdto.Id == null)
                    {
                        item.Fields.Add(Mapper.Map<EntityCustomFieldEditDto, EntityCustomField>(otdto));
                    }
                    else
                    {
                        oids.Add(otdto.Id.Value);
                        var fromUpdation = item.Fields.SingleOrDefault(a => a.Id.Equals(otdto.Id));
                        UpdateCustomField(fromUpdation, otdto);
                    }
                }

                List<EntityCustomField> tobeRemoved = item.Fields.Where(a => !a.Id.Equals(0) && !oids.Contains(a.Id)).ToList();
                foreach (var customField in tobeRemoved)
                {
                    await _entityCustomManager.DeleteAsync(customField.Id);
                }
            }

            try
            {
                await _entitytypeRepo.UpdateAsync(item);
            }
            catch (Exception exception)
            {
                var mess = exception.Message;
            }
        }

        private void UpdateCustomField(EntityCustomField fromUpdation, EntityCustomFieldEditDto otdto)
        {
            fromUpdation.Name = otdto.Name;
            fromUpdation.EditingFormat = otdto.EditingFormat;
            fromUpdation.FieldType = otdto.FieldType;
            fromUpdation.Hidden = otdto.Hidden;
            fromUpdation.ValueSource = otdto.ValueSource;
        }

        protected virtual async Task CreateEntityType(CreateOrUpdateEntityTypeInput input)
        {
            var dto = input.EntityType.MapTo<EntityType>();
            CheckErrors(await _entitytypeManager.CreateSync(dto));
        }
    }
}