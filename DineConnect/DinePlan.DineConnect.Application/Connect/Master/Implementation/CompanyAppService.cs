﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Utility.Exporter;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Sync;

namespace DinePlan.DineConnect.Connect.Master.Implementation
{
    public class CompanyAppService : DineConnectAppServiceBase, ICompanyAppService
    {
        private readonly IExcelExporter _exporter;
        private readonly ICompanyManager _companyManager;
        private readonly IRepository<Company> _companyRepo;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ISyncAppService _syncAppService;

        public CompanyAppService(ICompanyManager companyManager, ISyncAppService syncAppService,
            IRepository<Company> companyRepo,
            IExcelExporter companyExporter,
            IUnitOfWorkManager unitOfWorkManager)
        {
            _companyManager = companyManager;
            _companyRepo = companyRepo;
            _exporter = companyExporter;
            _unitOfWorkManager = unitOfWorkManager;
            _syncAppService = syncAppService;
        }

        public async Task<PagedResultOutput<CompanyListDto>> GetAll(GetCompanyInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var allItems = _companyRepo
                .GetAll()
                .Where(i => i.IsDeleted == input.IsDeleted);

                if (input.Operation == "SEARCH")
                {
                    allItems = allItems
                   .WhereIf(
                       !input.Filter.IsNullOrEmpty(),
                       p => p.Name.Equals(input.Filter)
                   );
                }
                else
                {
                    allItems = allItems
                   .WhereIf(
                       !input.Filter.IsNullOrEmpty(),
                       p => p.Name.Contains(input.Filter)
                   );
                }
                var sortMenuItems = await allItems
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var allListDtos = sortMenuItems.MapTo<List<CompanyListDto>>();

                var allItemCount = await allItems.CountAsync();

                return new PagedResultOutput<CompanyListDto>(
                    allItemCount,
                    allListDtos
                    );
            }
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _companyRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<CompanyListDto>>();

            var baseE = new BaseExportObject()
            {
                ColumnNames = new string[] { "Code", "Name", "Address1", "Address2", "Address3", "City", "State", "Country", "PostalCode", "PhoneNumber", "FaxNumber" },
                ExportObject = allListDtos
            };

            return _exporter.ExportToFile(baseE, L("Organization"));
        }

        public async Task<GetCompanyForEditOutput> GetCompanyForEdit(NullableIdInput input)
        {
            CompanyEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _companyRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<CompanyEditDto>();
            }
            else
            {
                editDto = new CompanyEditDto();
            }

            return new GetCompanyForEditOutput
            {
                Company = editDto
            };
        }

        public async Task<IdInput> CreateOrUpdateCompany(CreateOrUpdateCompanyInput input)
        {
            if (input.Company.Id.HasValue)
            {
                return await UpdateCompany(input);
            }
            else
            {
                return await CreateCompany(input);
            }

            await _syncAppService.UpdateSync(SyncConsts.LOCATION);

        }

        public async Task DeleteCompany(IdInput input)
        {
            await _companyRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task<IdInput> UpdateCompany(CreateOrUpdateCompanyInput input)
        {
            var item = await _companyRepo.GetAsync(input.Company.Id.Value);
            var dto = input.Company;

            //TODO: SERVICE Company Update Individually
            item.Name = dto.Name;
            item.Code = dto.Code;
            item.Address1 = dto.Address1;
            item.Address2 = dto.Address2;
            item.Address3 = dto.Address3;
            item.City = dto.City;
            item.State = dto.State;
            item.Country = dto.Country;
            item.FaxNumber = dto.FaxNumber;
            item.PhoneNumber = dto.PhoneNumber;
            item.UserSerialNumber = dto.UserSerialNumber;
            item.GovtApprovalId = dto.GovtApprovalId;
            item.CompanyProfilePictureId = dto.CompanyProfilePictureId;

            CheckErrors(await _companyManager.CreateSync(item));

            return new IdInput
            {
                Id = item.Id
            };
        }

        protected virtual async Task<IdInput> CreateCompany(CreateOrUpdateCompanyInput input)
        {
            var dto = input.Company.MapTo<Company>();

            CheckErrors(await _companyManager.CreateSync(dto));

            return new IdInput
            {
                Id = dto.Id
            };
        }

        public async Task<ListResultOutput<CompanyListDto>> GetNames()
        {
            var lstCompany = await _companyRepo.GetAll().ToListAsync();
            return new ListResultOutput<CompanyListDto>(lstCompany.MapTo<List<CompanyListDto>>());
        }

        public async Task<IdInput> GetCompanyIdByName(string companyname)
        {
            var lstId = await _companyRepo.GetAll().Where(a => a.Name.Equals(companyname) && a.TenantId == AbpSession.TenantId).ToListAsync();

            return new IdInput
            {
                Id = lstId[0].Id
            };
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _companyRepo.GetAsync(input.Id);
                item.IsDeleted = false;

                await _companyRepo.UpdateAsync(item);
            }
        }
    }
}