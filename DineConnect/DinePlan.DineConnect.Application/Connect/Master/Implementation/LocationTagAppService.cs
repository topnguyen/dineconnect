﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Utility.Exporter;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.Implementation
{
    public class LocationTagAppService : DineConnectAppServiceBase, ILocationTagAppService
    {
        private readonly ILocationTagExcelExporter _locationtagexporter;
        private readonly IRepository<LocationTag> _locationTagRepository;
        private readonly IRepository<Location> _locationRepository;

        public LocationTagAppService(
            ILocationTagExcelExporter locationtagexporter,
            IRepository<LocationTag> locationGroupRepository,
            IRepository<Location> locationRepository)
        {
            _locationtagexporter = locationtagexporter;
            _locationTagRepository = locationGroupRepository;
            _locationRepository = locationRepository;
        }

        public async Task<PagedResultOutput<LocationTagListDto>> GetAll(GetLocationTagInput input)
        {
            var query = _locationTagRepository.GetAll()
                .Include(lg => lg.Locations)
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Name.Contains(input.Filter)
                         || (p.Code != null
                             && p.Code.Contains(input.Filter)) || p.Id.ToString().Contains(input.Filter));
            foreach (var lst in query)
            {
                lst.Locations.Select(l => new LocationListDto
                {
                    LocationGroupId = lst.Id,
                    LocationGroupName = lst.Name,
                    Id = l.Id,
                    Name = l.Name,
                    Code = l.Code
                });
            }
            var result = await query
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            return new PagedResultOutput<LocationTagListDto>(await query.CountAsync(), result.MapTo<List<LocationTagListDto>>());
        }

        public async Task<FileDto> GetAllToExcel(GetLocationTagInput input)
        {
            input.MaxResultCount = 10000;
            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<LocationTagListDto>>();
            return _locationtagexporter.ExportToFile(allListDtos);
        }

        public async Task<LocationTagEditDto> GetLocationTagForEdit(NullableIdInput input)
        {
            if (input.Id.HasValue)
            {
                var locationTag = await _locationTagRepository.GetAsync(input.Id.Value);

                return locationTag.MapTo<LocationTagEditDto>();
            }

            return new LocationTagEditDto();
        }

        public async Task CreateOrUpdateLocationTag(LocationTagEditDto input)
        {
            await ValidateForEdit(input);

            if (input.Id.HasValue)
            {
                await UpdateLocationTag(input);
            }
            else
            {
                await CreateLocationTag(input);
            }
        }

        public async Task DeleteLocationTag(IdInput input)
        {
            var locations = await _locationRepository.GetAllListAsync(t => t.LocationTags.Where(lt => lt.Id == input.Id).Any());

            foreach (var location in locations)
            {
                location.LocationTags.Remove(await _locationTagRepository.GetAsync(input.Id));
                await _locationRepository.UpdateAsync(location);
            }
            await _locationTagRepository.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateLocationTag(LocationTagEditDto input)
        {
            var dto = await _locationTagRepository.GetAsync(input.Id.Value);

            //dto = input.MapTo<LocationTag>();

            //dto.TenantId = (int)AbpSession.TenantId;
            dto.Name = input.Name;
            dto.Code = input.Code;

            await _locationTagRepository.UpdateAsync(dto);
        }

        protected virtual async Task CreateLocationTag(LocationTagEditDto input)
        {
            var dto = input.MapTo<LocationTag>();

            await _locationTagRepository.InsertAsync(dto);
        }

        private async Task ValidateForEdit(LocationTagEditDto input)
        {
            var exists = _locationTagRepository.GetAll().WhereIf(input.Id.HasValue, m => m.Id != input.Id);

            if (await exists.AnyAsync(o => o.Name.Equals(input.Name)))
            {
                throw new UserFriendlyException("NameAlreadyExists");
            }
            if (exists.Any(o => o.Code.Equals(input.Code)))
            {
                throw new UserFriendlyException("CodeAlreadyExists");
            }
        }

        //public async Task<ListResultOutput<LocationTagListDto>> GetNames()
        //{
        //    var lstLocationTag = await _locationTagRepository.GetAll().ToListAsync();
        //    return new ListResultOutput<LocationTagListDto>(lstLocationTag.MapTo<List<LocationTagListDto>>());
        //}
    }
}