﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Utility.Exporter;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.Implementation
{
    public class ForeignCurrencyAppService : DineConnectAppServiceBase, IForeignCurrencyAppService
    {
        private readonly IExcelExporter _exporter;
        private readonly IForeignCurrencyManager _foreigncurrencyManager;
        private readonly IRepository<ForeignCurrency> _foreigncurrencyRepo;
        private readonly ISyncAppService _syncAppService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public ForeignCurrencyAppService(IForeignCurrencyManager foreigncurrencyManager
            , ISyncAppService syncAppService
            , IRepository<ForeignCurrency> foreignCurrencyRepo
            , IExcelExporter exporter
            , IUnitOfWorkManager unitOfWorkManager
            )
        {
            _foreigncurrencyManager = foreigncurrencyManager;
            _foreigncurrencyRepo = foreignCurrencyRepo;
            _exporter = exporter;
            _unitOfWorkManager = unitOfWorkManager;
            _syncAppService = syncAppService;
        }

        public async Task<PagedResultOutput<ForeignCurrencyListDto>> GetAll(GetForeignCurrencyInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var allItems = _foreigncurrencyRepo.GetAll()
                    .Where(f => f.IsDeleted == input.IsDeleted);

                if (input.Operation == "SEARCH")
                {
                    allItems = allItems
                        .WhereIf(
                            !input.Filter.IsNullOrEmpty(),
                            p => p.Id.Equals(input.Filter)
                        );
                }
                else
                {
                    allItems = allItems
                        .WhereIf(
                            !input.Filter.IsNullOrEmpty(),
                            p => p.Name.Contains(input.Filter)
                        );
                }
                var sortMenuItems = await allItems
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var allListDtos = sortMenuItems.MapTo<List<ForeignCurrencyListDto>>();

                var allItemCount = await allItems.CountAsync();

                return new PagedResultOutput<ForeignCurrencyListDto>(
                    allItemCount,
                    allListDtos
                    );
            }
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _foreigncurrencyRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<ForeignCurrencyListDto>>();
            var baseE = new BaseExportObject
            {
                ExportObject = allListDtos,
                ColumnNames = new[] { "Id", "Name", "CurrencySymbol", "ExchangeRate" }
            };
            return _exporter.ExportToFile(baseE, L("ForeignCurrency"));
        }

        public async Task<GetForeignCurrencyForEditOutput> GetForeignCurrencyForEdit(NullableIdInput input)
        {
            ForeignCurrencyEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _foreigncurrencyRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<ForeignCurrencyEditDto>();
            }
            else
            {
                editDto = new ForeignCurrencyEditDto();
            }

            return new GetForeignCurrencyForEditOutput
            {
                ForeignCurrency = editDto
            };
        }

        public async Task CreateOrUpdateForeignCurrency(CreateOrUpdateForeignCurrencyInput input)
        {
            if (input.ForeignCurrency.Id.HasValue)
            {
                await UpdateForeignCurrency(input);
            }
            else
            {
                await CreateForeignCurrency(input);
            }
            await _syncAppService.UpdateSync(SyncConsts.FORIEGNCURRENCY);
        }

        public async Task DeleteForeignCurrency(IdInput input)
        {
            await _foreigncurrencyRepo.DeleteAsync(input.Id);
        }

        public async Task<ListResultOutput<ForeignCurrencyListDto>> ApiGetAll(ApiLocationInput locationInput)
        {
           

            var lstForeignCurrency = await _foreigncurrencyRepo.GetAllListAsync(a => a.TenantId == locationInput.TenantId);

            return new ListResultOutput<ForeignCurrencyListDto>(lstForeignCurrency.MapTo<List<ForeignCurrencyListDto>>());
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _foreigncurrencyRepo.GetAsync(input.Id);
                item.IsDeleted = false;

                await _foreigncurrencyRepo.UpdateAsync(item);
            }
        }

        protected virtual async Task UpdateForeignCurrency(CreateOrUpdateForeignCurrencyInput input)
        {
            var item = await _foreigncurrencyRepo.GetAsync(input.ForeignCurrency.Id.Value);
            var dto = input.ForeignCurrency;

            item.ExchangeRate = dto.ExchangeRate;
            item.Name = dto.Name;
            item.PaymentTypeId = dto.PaymentTypeId;
            item.Rounding = dto.Rounding;
            item.CurrencySymbol = dto.CurrencySymbol;
            CheckErrors(await _foreigncurrencyManager.CreateSync(item));
        }

        protected virtual async Task CreateForeignCurrency(CreateOrUpdateForeignCurrencyInput input)
        {
            var dto = input.ForeignCurrency.MapTo<ForeignCurrency>();

            CheckErrors(await _foreigncurrencyManager.CreateSync(dto));
        }
    }
}