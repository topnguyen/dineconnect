﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.House;
using DinePlan.DineConnect.House.Impl;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.Implementation
{
    public class LocationContactAppService : DineConnectAppServiceBase, ILocationContactAppService
    {
        private readonly ILocationContactListExcelExporter _locationcontactExporter;
        private readonly ILocationContactManager _locationcontactManager;
        private readonly IRepository<LocationContact> _locationcontactRepo;
        private readonly IRepository<Location> _locationManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public LocationContactAppService(ILocationContactManager locationcontactManager,
            IRepository<LocationContact> locationContactRepo,
            ILocationContactListExcelExporter locationcontactExporter,
            IRepository<Location> locationManager,
            IUnitOfWorkManager unitOfWorkManager)
        {
            _locationcontactManager = locationcontactManager;
            _locationcontactRepo = locationContactRepo;
            _locationcontactExporter = locationcontactExporter;
            _locationManager = locationManager;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task<PagedResultOutput<LocationContactListDto>> GetAll(GetLocationContactInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var allItems = _locationcontactRepo.GetAll()
                    .Where(i => i.IsDeleted == input.IsDeleted);

                if (!string.IsNullOrEmpty(input.Operation) && input.Operation.Equals("SEARCH"))
                {
                    allItems = allItems
                   .WhereIf(
                       !input.Filter.IsNullOrEmpty(),
                       p => p.Id.Equals(input.Filter)
                   );
                }
                else
                {
                    allItems = allItems
                   .WhereIf(
                       !input.Filter.IsNullOrEmpty(),
                       p => p.Id.ToString().Contains(input.Filter)
                   );
                }
                var sortMenuItems = await allItems
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var allListDtos = sortMenuItems.MapTo<List<LocationContactListDto>>();

                var allItemCount = await allItems.CountAsync();

                return new PagedResultOutput<LocationContactListDto>(
                    allItemCount,
                    allListDtos
                    );
            }
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _locationcontactRepo.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<LocationContactListDto>>();
            return _locationcontactExporter.ExportToFile(allListDtos);
        }

        public async Task<GetLocationContactForEditOutput> GetLocationContactForEdit(NullableIdInput input)
        {
            LocationContactEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _locationcontactRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<LocationContactEditDto>();
            }
            else
            {
                editDto = new LocationContactEditDto();
            }

            return new GetLocationContactForEditOutput
            {
                LocationContact = editDto
            };
        }

        public async Task CreateOrUpdateLocationContact(CreateOrUpdateLocationContactInput input)
        {
            if (input.LocationContact.Id.HasValue)
            {
                await UpdateLocationContact(input);
            }
            else
            {
                await CreateLocationContact(input);
            }
        }

        public async Task DeleteLocationContact(IdInput input)
        {
            await _locationcontactRepo.DeleteAsync(input.Id);
        }

        protected virtual async Task UpdateLocationContact(CreateOrUpdateLocationContactInput input)
        {
            var item = await _locationcontactRepo.GetAsync(input.LocationContact.Id.Value);
            var dto = input.LocationContact;

            //TODO: SERVICE LocationContact Update Individually

            item.ContactPersonName = dto.ContactPersonName;
            item.Priority = dto.Priority;
            item.LocationRefId = dto.LocationRefId;
            item.PhoneNumber = dto.PhoneNumber;
            item.Designation = dto.Designation;
            item.Email = dto.Email;
            item.EmpRefCode = dto.EmpRefCode;
            CheckErrors(await _locationcontactManager.CreateSync(item));
        }

        protected virtual async Task CreateLocationContact(CreateOrUpdateLocationContactInput input)
        {
            var dto = input.LocationContact.MapTo<LocationContact>();
            await _locationcontactRepo.InsertAsync(dto);
            CheckErrors(await _locationcontactManager.CreateSync(dto));
        }

        public async Task<ListResultOutput<LocationContactViewDto>> GetContactPerson(IdInput idinput)
        {
            //var lstLocationContact = await _locationcontactRepo.GetAll().ToListAsync();

            var allItems = (from loccon in _locationcontactRepo.GetAll()
                            where loccon.LocationRefId == idinput.Id
                            join loc in _locationManager.GetAll() on
                            loccon.LocationRefId equals loc.Id
                            select new LocationContactViewDto()
                            {
                                ContactPerson = loccon.ContactPersonName,
                                LocationName = loc.Name,
                                Priority = loccon.Priority,
                                Id = loccon.Id,
                            });

            var sortMenuItems = await allItems.ToListAsync();

            var allListDtos = sortMenuItems.ToList(); // sortMenuItems.MapTo<List<UnitConversionListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new ListResultOutput<LocationContactViewDto>(allListDtos.MapTo<List<LocationContactViewDto>>());
        }

        public async Task<PagedResultOutput<LocationContactViewDto>> GetView(GetLocationContactInput input)
        {
            var allItems = (from loccon in _locationcontactRepo.GetAll()
                            join loc in _locationManager.GetAll() on
                            loccon.LocationRefId equals loc.Id
                            select new LocationContactViewDto()
                            {
                                ContactPerson = loccon.ContactPersonName,
                                LocationName = loc.Name,
                                Priority = loccon.Priority,
                                Id = loccon.Id,
                                LocationRefId = loc.Id,
                                Designation = loccon.Designation,
                                Email = loccon.Email,
                                EmpRefCode = loccon.EmpRefCode,
                                PhoneNumber = loccon.PhoneNumber
                            }).WhereIf(
                               !input.Filter.IsNullOrEmpty(),
                               p => p.LocationRefId.ToString().Equals(input.Filter)
               );

            var sortMenuItems = await allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var allListDtos = sortMenuItems.ToList(); // sortMenuItems.MapTo<List<UnitConversionListDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<LocationContactViewDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _locationcontactRepo.GetAsync(input.Id);
                item.IsDeleted = false;

                await _locationcontactRepo.UpdateAsync(item);
            }
        }
    }
}