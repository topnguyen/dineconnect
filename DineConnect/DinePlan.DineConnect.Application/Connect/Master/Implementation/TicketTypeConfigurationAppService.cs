﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using DinePlan.DineConnect.Connect.Master.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.Implementation
{
    public class TicketTypeConfigurationAppService : DineConnectAppServiceBase, ITicketTypeConfigurationAppService
    {
        private readonly IRepository<TicketTypeConfiguration> _ticketTypeConfigurationManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public TicketTypeConfigurationAppService(IRepository<TicketTypeConfiguration> TicketTypeConfigurationManager, IUnitOfWorkManager unitOfWorkManager)
        {
            _ticketTypeConfigurationManager = TicketTypeConfigurationManager;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public PagedResultOutput<TicketTypeConfigurationListDto> GetAll(GetTicketTypeConfigurationInput input)
        {
            using(_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                IQueryable<TicketTypeConfiguration> allItems = _ticketTypeConfigurationManager.GetAll().Where(x => x.IsDeleted == input.IsDeleted);

                var allItemCount = allItems.Count();

                var sortMenuItems = allItems.AsQueryable().ToList();

                var allListDtos = sortMenuItems.MapTo<List<TicketTypeConfigurationListDto>>();

                return new PagedResultOutput<TicketTypeConfigurationListDto>(allItemCount, allListDtos);
            }
        }

        public async Task<int> CreateOrUpdateTicketTypeConfiguration(CreateTicketTypeConfigurationInput input)
        {
            int returnId;

            if (input.TicketTypeConfiguration.Id.HasValue)
            {
                returnId = await UpdateTicketTypeConfiguration(input);
            }
            else
            {
                returnId = await CreateTicketTypeConfiguration(input);
            }
            return returnId;
        }

        private async Task<int> CreateTicketTypeConfiguration(CreateTicketTypeConfigurationInput input)
        {
            var printConfiguration = input.TicketTypeConfiguration.MapTo<TicketTypeConfiguration>();
            var retId = await _ticketTypeConfigurationManager.InsertAndGetIdAsync(printConfiguration);
            return retId;
        }

        private async Task<int> UpdateTicketTypeConfiguration(CreateTicketTypeConfigurationInput input)
        {
            var dto = input.TicketTypeConfiguration;
            var item = await _ticketTypeConfigurationManager.GetAsync(input.TicketTypeConfiguration.Id.Value);

            dto.MapTo(item);

            await _ticketTypeConfigurationManager.UpdateAsync(item);
            return dto.Id.Value;
        }
    }
}