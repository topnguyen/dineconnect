﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Utility.Exporter;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master.Implementation
{
    public class TransactionTypeAppService : DineConnectAppServiceBase, ITransactionTypeAppService
    {
        private readonly IExcelExporter _excelExporter;
        private readonly ISyncAppService _syncAppService;
        private readonly IRepository<TransactionType> _transactiontypeManager;
        private readonly ITransactionTypeManager _tManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public TransactionTypeAppService(IRepository<TransactionType> transactiontypeManager,
            IExcelExporter excelExporter, ISyncAppService syncAppService, ITransactionTypeManager tManager
            , IUnitOfWorkManager unitOfWorkManager
            )
        {
            _transactiontypeManager = transactiontypeManager;
            _excelExporter = excelExporter;
            _syncAppService = syncAppService;
            _tManager = tManager;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task<PagedResultOutput<TransactionTypeListDto>> GetAll(GetTransactionTypeInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                IQueryable<TransactionType> allItems = null;
                if (input == null)
                {
                    allItems = _transactiontypeManager.GetAll();
                }
                else
                {
                    allItems = _transactiontypeManager
                        .GetAll()
                        .Where(i => i.IsDeleted == input.IsDeleted)
                        .WhereIf(
                            !input.Filter.IsNullOrEmpty(),
                            p => p.Name.Contains(input.Filter)
                        );
                }
                var sortMenuItems = await allItems
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var allListDtos = sortMenuItems.MapTo<List<TransactionTypeListDto>>();

                var allItemCount = await allItems.CountAsync();

                return new PagedResultOutput<TransactionTypeListDto>(
                    allItemCount,
                    allListDtos
                    );
            }
        }

        public async Task<FileDto> GetAllToExcel()
        {
            var allList = await _transactiontypeManager.GetAll().ToListAsync();
            var allListDtos = allList.MapTo<List<TransactionTypeListDto>>();
            var baseE = new BaseExportObject
            {
                ExportObject = allListDtos,
                ColumnNames = new[] { "Id", "Name" }
            };
            return _excelExporter.ExportToFile(baseE, L("TransactionType"));
        }

        public async Task<GetTransactionTypeForEditOutput> GetTransactionTypeForEdit(NullableIdInput input)
        {
            TransactionTypeEditDto editDto;

            if (input.Id.HasValue)
            {
                var transactionType = await _transactiontypeManager.GetAsync(input.Id.Value);
                editDto = transactionType.MapTo<TransactionTypeEditDto>();
            }
            else
            {
                editDto = new TransactionTypeEditDto();
            }

            return new GetTransactionTypeForEditOutput
            {
                TransactionType = editDto
            };
        }

        public async Task<int> CreateOrUpdateTransactionType(CreateOrUpdateTransactionTypeInput input)
        {
            var retunrValue = 0;
            if (input.TransactionType.Id.HasValue && input.TransactionType.Id > 0)
            {
                await UpdateTransactionType(input);
                retunrValue = input.TransactionType.Id.Value;
            }
            else
            {
                retunrValue = await CreateTransactionType(input);
            }
            await _syncAppService.UpdateSync(SyncConsts.TRANSACTIONTYPE);

            return retunrValue;
        }

        public async Task DeleteTransactionType(IdInput input)
        {
            await _transactiontypeManager.DeleteAsync(input.Id);
        }

        public IQueryable<TransactionType> GetEntities()
        {
            return _transactiontypeManager.GetAll();
        }

        public async Task CreateTransactionTypeForTenant(int tenantId)
        {
            if (await _transactiontypeManager.CountAsync(a => a.TenantId.Equals(tenantId)) <= 0)
            {
                TransactionType transactionType = new TransactionType { Name = "PAYMENT", TenantId = tenantId };
                await _transactiontypeManager.InsertAndGetIdAsync(transactionType);

                transactionType = new TransactionType { Name = "SALE", TenantId = tenantId };
                await _transactiontypeManager.InsertAndGetIdAsync(transactionType);

                transactionType = new TransactionType { Name = "SERVICE CHARGE", TenantId = tenantId };
                await _transactiontypeManager.InsertAndGetIdAsync(transactionType);

                transactionType = new TransactionType { Name = "DISCOUNT", TenantId = tenantId };
                await _transactiontypeManager.InsertAndGetIdAsync(transactionType);

                transactionType = new TransactionType { Name = "ROUNDING", TenantId = tenantId };
                await _transactiontypeManager.InsertAndGetIdAsync(transactionType);

                transactionType = new TransactionType { Name = "ROUND+", TenantId = tenantId };
                await _transactiontypeManager.InsertAndGetIdAsync(transactionType);

                transactionType = new TransactionType { Name = "ROUND-", TenantId = tenantId };
                await _transactiontypeManager.InsertAndGetIdAsync(transactionType);

                transactionType = new TransactionType { Name = "TAX", TenantId = tenantId };
                await _transactiontypeManager.InsertAndGetIdAsync(transactionType);
            }
            await _syncAppService.UpdateSync(SyncConsts.TRANSACTIONTYPE);
        }

        public async Task<PagedResultOutput<TransactionTypeEditDto>> GetAllItems()
        {
            var allItems = _transactiontypeManager.GetAll();
            var allListDtos = allItems.MapTo<List<TransactionTypeEditDto>>();

            var allItemCount = await allItems.CountAsync();

            return new PagedResultOutput<TransactionTypeEditDto>(
                allItemCount,
                allListDtos
                );
        }

        public async Task<ListResultOutput<ApiTransactionTypeOutput>> ApiGetAll(ApiTransactionTypeInput inputDto)
        {
            var lstPaymentType = await _transactiontypeManager.GetAll().ToListAsync();

            var locationOuputs = lstPaymentType.Select(sortL => new ApiTransactionTypeOutput
            {
                Id = sortL.Id,
                Name = sortL.Name
            }).ToList();
            return new ListResultOutput<ApiTransactionTypeOutput>(locationOuputs);
        }

        public async Task ActivateItem(IdInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var item = await _transactiontypeManager.GetAsync(input.Id);
                item.IsDeleted = false;

                await _transactiontypeManager.UpdateAsync(item);
            }
        }

        public async Task<PagedResultOutput<TransactionTypeListDto>> ApiGetTransactionTypes(GetTransactionTypeInput inputDto)
        {
            return await GetAll(inputDto);
        }

        protected virtual async Task UpdateTransactionType(CreateOrUpdateTransactionTypeInput input)
        {
            var item = await _transactiontypeManager.GetAsync(input.TransactionType.Id.Value);
            var dto = input.TransactionType;
            item.Name = dto.Name;
            item.Tax = dto.Tax;
            item.AccountCode = dto.AccountCode;
            try
            {
                await _tManager.CreateSync(item);
            }
            catch (Exception exception)
            {
                Logger.Error("TRANSACTION TYPE UPDATION", exception);
            }
        }

        protected virtual async Task<int> CreateTransactionType(CreateOrUpdateTransactionTypeInput input)
        {
            var dto = input.TransactionType.MapTo<TransactionType>();
            try
            {
                await _tManager.CreateSync(dto);
                return 0;
            }
            catch (Exception exception)
            {
                Logger.Error("TRANSACTION TYPE CREATION", exception);
            }
            return 0;
        }

        public async Task<List<TransactionType>> ApiGetAllTransactionTypesForSync(ApiLocationInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                return await _transactiontypeManager.GetAll().Where(a=>a.TenantId == input.TenantId).ToListAsync();
            }
        }

    }
}