﻿
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master
{
	public interface ILocationGroupAppService : IApplicationService
	{
		Task<PagedResultOutput<LocationGroupListDto>> GetAll(GetLocationGroupInput inputDto);
        Task<PagedResultOutput<SimpleLocationGroupDto>> GetSimpleAll(GetLocationGroupInput inputDto);

        Task<FileDto> GetAllToExcel(GetLocationGroupInput input);
		Task<GetLocationGroupForEditOutput> GetLocationGroupForEdit(NullableIdInput nullableIdInput);
		Task CreateOrUpdateLocationGroup(CreateOrUpdateLocationGroupInput input);
		Task DeleteLocationGroup(IdInput input);

		Task<ListResultOutput<LocationGroupListDto>> GetNames();
	}
}
