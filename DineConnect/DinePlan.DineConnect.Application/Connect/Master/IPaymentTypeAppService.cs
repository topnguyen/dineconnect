﻿using System.Collections.Generic;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master
{
    public interface IPaymentTypeAppService : IApplicationService
    {
        Task<PagedResultOutput<PaymentTypeListDto>> GetAll(GetPaymentTypeInput inputDto);

        Task<FileDto> GetAllToExcel();

        Task<GetPaymentTypeForEditOutput> GetPaymentTypeForEdit(NullableIdInput nullableIdInput);

        Task<int> CreateOrUpdatePaymentType(CreateOrUpdatePaymentTypeInput input);

        Task DeletePaymentType(IdInput input);

        IQueryable<PaymentType> GetEntities();

        Task CreatePaymentForTenant(int tenantId);

        Task<PagedResultOutput<PaymentTypeEditDto>> GetAllItems();

        Task<ListResultOutput<ApiPaymentTypeOutput>> ApiGetAll(ApiPaymentTypeInput inputDto);

        Task ActivateItem(IdInput input);
        ListResultDto<ComboboxItemDto> GetPaymentTypeTenders();

        Task<List<PaymentType>> ApiGetAllPaymentTypesForSync(ApiLocationInput input);


    }
}