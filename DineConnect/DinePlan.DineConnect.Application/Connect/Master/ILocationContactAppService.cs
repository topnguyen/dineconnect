﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master
{
    public interface ILocationContactAppService : IApplicationService
    {
        Task<PagedResultOutput<LocationContactListDto>> GetAll(GetLocationContactInput inputDto);

        Task<FileDto> GetAllToExcel();

        Task<GetLocationContactForEditOutput> GetLocationContactForEdit(NullableIdInput nullableIdInput);

        Task CreateOrUpdateLocationContact(CreateOrUpdateLocationContactInput input);

        Task DeleteLocationContact(IdInput input);

        Task<ListResultOutput<LocationContactViewDto>> GetContactPerson(IdInput idinput);

        Task<PagedResultOutput<LocationContactViewDto>> GetView(GetLocationContactInput input);

        Task ActivateItem(IdInput input);
    }
}