﻿
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Microsoft.AspNet.Identity;

namespace DinePlan.DineConnect.Connect.Master
{
    public interface ILocationRepository : IRepository<Location>
    {
        Task<IdentityResult> CreateAsync(Location location);
    }
}