﻿using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master
{
    public interface ITicketTagGroupExcelExporter
    {
        FileDto ExportToFile(List<TicketTagGroupListDto> dtos);
    }
}