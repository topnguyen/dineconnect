﻿using System.Collections.Generic;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;

namespace DinePlan.DineConnect.Connect.Master.Exporter
{
    public interface IDinePlanTaxListExcelExporter
    {
        FileDto ExportToFile(List<DinePlanTaxListDto> dtos);
    }
}
