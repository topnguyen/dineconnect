﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Dto;
using System.Linq;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Master
{
    public interface INumeratorAppService : IApplicationService
    {
        Task<FileDto> GetAllToExcel();

        PagedResultOutput<NumeratorListDto> GetAll(NumeratorInput input);

        Task<GetNumeratorForEditOutput> GetNumeratorForEdit(NullableIdInput nullableIdInput);

        Task<int> CreateOrUpdateNumerator(CreateOrUpdateNumeratorInput input);

        Task DeleteNumerator(IdInput input);

        Task ActivateItem(IdInput input);

        Task<ListResultOutput<ApiNumeratorOutput>> ApiGetNumerators(ApiLocationInput locationInput);
        ListResultDto<ComboboxItemDto> GetResetNumeratorTypes();
    }
}