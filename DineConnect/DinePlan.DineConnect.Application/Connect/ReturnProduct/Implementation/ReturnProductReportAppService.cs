﻿using Abp.Application.Services.Dto;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Castle.Core.Logging;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Configuration.Tenants;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Connect.ReturnProduct.Dtos;
using DinePlan.DineConnect.Connect.ReturnProduct.Implementation;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.Handler.Mapping;
using DinePlan.DineConnect.Helper;
using DinePlan.DineConnect.Job.ConnectReportJob;
using DinePlan.DineConnect.Report;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ReturnProduct.Exporting
{
    public class ReturnProductReportAppService: DineConnectAppServiceBase, IReturnProductReportAppService
    {
        private readonly IBackgroundJobManager _bgm;
        private readonly IConnectReportAppService _cService;
        private readonly ILogger _logger;
        private readonly IReturnProductReportExcelExporter _returnProductReportExcelExporter;
        private readonly IReportBackgroundAppService _rbas;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<MenuItem> _menuitemRepo;
        private readonly ITenantSettingsAppService _tenantSettingsService;
        private readonly ILocationAppService _locService;
        private readonly IRepository<Master.Location> _locationRepo;
        

        public ReturnProductReportAppService(
            IReturnProductReportExcelExporter returnProductReportExcelExporter,
            IUnitOfWorkManager unitOfWorkManager,
            IConnectReportAppService cService,
            ILogger logger,
            IReportBackgroundAppService reportBackgroundAppService,
            IBackgroundJobManager backgroundJobManager,
            IRepository<MenuItem> menuitemRepo,
            ITenantSettingsAppService tenantSettingsService,
            ILocationAppService locService,
            IRepository<Master.Location> locationRepo)
        {
            _returnProductReportExcelExporter = returnProductReportExcelExporter;
            _unitOfWorkManager = unitOfWorkManager;
            _cService = cService;
            _logger = logger;
            _rbas = reportBackgroundAppService;
            _bgm = backgroundJobManager;
            _menuitemRepo = menuitemRepo;
            _tenantSettingsService = tenantSettingsService;
            _locService = locService;
            _locationRepo = locationRepo;
        }
        public async Task<ReturnProductReportOutput> GetReturnProductReport(GetReturnProductReportInput input)
        {
            var cInput = new GetChartInput
            {
                StartDate = input.StartDate,
                EndDate = input.EndDate,
                LocationGroup = input.LocationGroup
            };
            var tickets = _cService.GetAllTickets(cInput).Where(x => x.TotalAmount < 0);
            
            try
            {

                var returnProduceOrders = tickets.SelectMany(x => x.Orders);
                var filterOrders = await returnProduceOrders.OrderBy(input.Sorting).PageBy(input).ToListAsync();
                var dtos = new List<ReturnProductReportDto>();
                var totalAmount = 0M;
                var totalOrderCount = 0;
                foreach (var order in filterOrders)
                {
                    var ticket = _cService.GetAllTickets(cInput).FirstOrDefault(a => a.Id.Equals(order.TicketId));
                    if (ticket == null) continue;
                    var menu = new MenuItem();
                    using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.MustHaveTenant, AppConsts.ConnectFilter))
                    {
                        menu = _menuitemRepo.GetAll()?.FirstOrDefault(x => x.Id == order.MenuItemId);
                    }
                    if (menu == null) continue;

                    var ticketReference = new Transaction.Ticket();

                    if (!String.IsNullOrWhiteSpace(ticket.ReferenceNumber))
                        ticketReference = _cService.GetTicketByNumber(ticket.ReferenceNumber);
                    else
                        continue;

                    if (ticketReference == null)
                        continue;

                    var payment = ticketReference.Payments.FirstOrDefault(x => x.TicketId == ticketReference.Id);
                    if (payment == null)
                        continue;

                    var ticketEntities = JsonConvert.DeserializeObject<IList<TicketEntityMap>>(ticketReference.TicketEntities);
                    var entity = ticketEntities.SingleOrDefault(a => a.EntityTypeId.Equals(1));

                    var ticketStateValue = ticketReference.GetTicketStateValues().FirstOrDefault();
                    var returnProduct = JsonHelper.Deserialize<ReturnProductReport>(ticketStateValue.StateValue);
                    
                    string paymentName = payment.PaymentType != null ? payment.PaymentType.Name : "";

                    var setting = await _tenantSettingsService.GetAllSettings();
                    var dateTimeFormat = setting.Connect.DateTimeFormat;
                    decimal amount = 0;
                    if (order != null)
                        amount = order.Price * Math.Abs(order.Quantity);
                    totalAmount += amount;
                    totalOrderCount ++;

                    // get location 
                    var location = await _locationRepo.FirstOrDefaultAsync(ticket.LocationId);

                    dtos.Add(new ReturnProductReportDto
                    {
                        DateTimeString = ticketReference.LastPaymentTime.ToString(dateTimeFormat),
                        Cashier = payment.PaymentUserName,
                        TicketNumber = ticket.TicketNumber,
                        CustomerName = entity != null ? entity.EntityName : "",
                        MenuCode = menu?.AliasCode,// order.MenuItemId.ToString(),
                        MenuName = order.MenuItemName,
                        PaymentType = paymentName,
                        Quantity = Math.Abs(order.Quantity),
                        RefundType = returnProduct?.RefundType == ReturnProductReportConsts.Payment ? paymentName : returnProduct?.RefundType,
                        ReturnProductDateString = ticketStateValue.LastUpdateTime.ToString(dateTimeFormat),
                        ReturnProductBy = returnProduct?.ReturnProductBy,
                        Reason = order?.Note,
                        Detail = "",
                        Amount = amount,
                        LocationCode = location?.Code,
                        LocationName = location?.Name,
                    });
                }

                // filter query builder 
                var dataAsQueryable = dtos.AsQueryable();

                if (!string.IsNullOrEmpty(input.DynamicFilter))
                {
                    var jsonSerializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver =
                            new CamelCasePropertyNamesContractResolver()
                    };
                    var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                    if (filRule?.Rules != null)
                    {
                        dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                    }
                }

                var total = dataAsQueryable.Count();
                var result = dataAsQueryable.ToList();

                var totalAmountOrder = result.Sum(x => x.Amount);

                return new ReturnProductReportOutput
                {
                    Data = new PagedResultOutput<ReturnProductReportDto>(total, result),
                    DashBoardOrderDto = new DashboardOrderDto
                    {
                        TotalAmount = totalAmountOrder,
                        TotalOrderCount = total,
                    }
                };
            }
            catch (Exception ex)
            {
                _logger.Error("GetReturnProductReport", ex);
            }

            return new ReturnProductReportOutput
            {
                Data = new PagedResultOutput<ReturnProductReportDto>(0, new List<ReturnProductReportDto>()),
                DashBoardOrderDto = new DashboardOrderDto()
            };
        }

        public async Task<FileDto> GetReturnProductExcel(GetReturnProductReportInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                var setting = await _tenantSettingsService.GetAllSettings();
                input.DateTimeFormat = setting.Connect.DateTimeFormat;

                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.RETURNPRODUCT,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                    {
                        if (input.LocationGroup != null && !input.LocationGroup.Group && !input.LocationGroup.Groups.Any()
                        && !input.LocationGroup.Locations.Any())
                        {
                            var locationIds = _locService.GetLocationForUserAndGroup(new LocationGroupDto
                            {
                                Locations = input.LocationGroup.Locations,
                                Group = false
                            });

                            input.Locations = new List<Master.Dtos.SimpleLocationDto>();
                            foreach (var locationId in locationIds)
                            {
                                var loc = await _locationRepo.FirstOrDefaultAsync(locationId);

                                input.Locations.Add(new Master.Dtos.SimpleLocationDto()
                                {
                                    Id = loc.Id
                                });
                            }
                        }    


                        await _bgm.EnqueueAsync<ConnectReportJob, ConnectReportInputArgs>(new ConnectReportInputArgs
                        {
                            BackGroundId = backGroundId,
                            ReportName = ReportNames.RETURNPRODUCT,
                            UserId = AbpSession.UserId.Value,
                            TenantId = AbpSession.TenantId.Value,
                            GetReturnProductReportInput = input
                        });
                    }
                }
                else
                {
                    return await _returnProductReportExcelExporter.ExportReturnProduct(input, _cService);
                }
            }

            return null;
        }
    }
}
