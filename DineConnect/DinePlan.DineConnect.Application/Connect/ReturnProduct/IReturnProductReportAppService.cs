﻿using Abp.Application.Services;
using DinePlan.DineConnect.Connect.OrderTag.Dtos;
using DinePlan.DineConnect.Connect.ReturnProduct.Dtos;
using DinePlan.DineConnect.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ReturnProduct.Implementation
{
    public interface IReturnProductReportAppService: IApplicationService
    {
        Task<ReturnProductReportOutput> GetReturnProductReport(GetReturnProductReportInput input);
        Task<FileDto> GetReturnProductExcel(GetReturnProductReportInput input);
    }
}
