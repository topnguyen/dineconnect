﻿using Abp.Application.Services.Dto;
using Abp.Extensions;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter;
using OpenHtmlToPdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ReturnProduct.Dtos
{
    public class ReturnProductReportDto
    {
        public DateTime DateTime { get; set; }
        public string DateTimeString { get; set; }
        public string Cashier { get; set; }
        public string TicketNumber { get; set; }
        public string CustomerName { get; set; }
        public string MenuCode { get; set; }
        public string MenuName { get; set; }
        public string PaymentType { get; set; }
        public decimal Quantity { get; set; }
        public string RefundType { get; set; }
        public DateTime ReturnProductDate { get; set; }
        public string ReturnProductDateString { get; set; }
        public string ReturnProductBy { get; set; }
        public string Reason { get; set; }
        public string Detail { get; set; }
        public decimal Amount { get; set; }
        public string LocationCode { get; set; }
        public string LocationName { get; set; }
    }

    public class ReturnProductReport
    {
        public string RefundType { get; set; }
        public string ReturnProductBy { get; set; }
    }

    public class ReturnProductReportOutput
    {
        public PagedResultOutput<ReturnProductReportDto> Data { get; set; }
        public DashboardOrderDto DashBoardOrderDto { get; set; }
    }

    public class ReturnProductReportConsts
    {
        public static string Payment = "Payment";
    }

    [Serializable]
    public class GetReturnProductReportInput : PagedAndSortedInputDto, IShouldNormalize, IFileExport
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool RunInBackground { get; set; }
        public string ReportDescription { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public List<SimpleLocationDto> Locations { get; set; }
        public string ReportName { get; set; }
        public string DateTimeFormat { get; set; }
        public int BackGroundId { get; set; }
        public ExportType ExportOutputType { get; set; }
        public PaperSize PaperSize { get; set; }
        public bool Portrait { get; set; }
        public List<int> OrderTagGroupIds { get; set; }
        public string DynamicFilter { get; set; }
        public void Normalize()
        {
            if (Sorting.IsNullOrWhiteSpace())
            {
                Sorting = "Ticket.TicketNumber";
            }
            if (Sorting.Contains("ticketNo"))
            {
                Sorting = Sorting.Replace("ticketNo", "Ticket.TicketNumber");
            }
            if (Sorting.Contains("locationCode"))
            {
                Sorting = Sorting.Replace("locationCode", "Ticket.Ticket.Location.Code");
            }
            if (Sorting.Contains("orderId"))
            {
                Sorting = Sorting.Replace("orderId", "Id");
            }
            if (Sorting.Contains("orderTagName"))
            {
                Sorting = Sorting.Replace("orderTagName", "OrderLog");
            }
            if (Sorting.Contains("user"))
            {
                Sorting = Sorting.Replace("user", "Ticket.LastModifiedUserName");
            }
        }
    }
}
