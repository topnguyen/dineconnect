﻿using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Castle.DynamicLinqQueryBuilder;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.ReturnProduct.Dtos;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Engage.Handler.Mapping;
using DinePlan.DineConnect.Helper;
using DinePlan.DineConnect.Net.MimeTypes;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.ReturnProduct.Exporting
{
    public class ReturnProductReportExcelExporter: FileExporterBase, IReturnProductReportExcelExporter
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IConnectReportAppService _cService;
        private readonly IRepository<MenuItem> _menuitemRepo;
        private readonly IRepository<Master.Location> _locationRepo;

        public ReturnProductReportExcelExporter(
            IUnitOfWorkManager unitOfWorkManager,
            IConnectReportAppService cService,
            IRepository<MenuItem> menuitemRepo,
            IRepository<Master.Location> locationRepo
            )
        {
            _unitOfWorkManager = unitOfWorkManager;
            _cService = cService;
            _menuitemRepo = menuitemRepo;
            _locationRepo = locationRepo;
        }

        public async Task<FileDto> ExportReturnProduct(GetReturnProductReportInput input, IConnectReportAppService connectReportService)
        {
            var start = input.StartDate.ToString("ddMMyyyy");
            var end = input.EndDate.ToString("ddMMyyyy");
            var file = new FileDto($"Return_Product_Report_{start}-{end}.xlsx", MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add($"Return Product Report");
                sheet.OutLineApplyStyle = true;

                AddHeader(sheet,
                        L("LocationCode"),
                        L("LocationName"),
                        L("DateTime"),
                        L("Cashier"),
                        L("TicketNumber"),
                        L("CustomerName"),
                        L("MenuCode"),
                        L("MenuName"),
                        L("PaymentType"),
                        L("Quantity"),
                        L("RefundType"),
                        L("ReturnProductDate"),
                        L("ReturnProductBy"),
                        L("Reason"),
                        L("Detail"),
                        L("Amount")
                        );

                var cInput = new GetChartInput
                {
                    StartDate = input.StartDate,
                    EndDate = input.EndDate,
                    LocationGroup = input.LocationGroup,
                    Locations = input.Locations
                };
                var tickets = _cService.GetAllTickets(cInput).Where(x => x.TotalAmount < 0);

                var returnProduceOrders = tickets.SelectMany(x => x.Orders);
                var filterOrders = await returnProduceOrders.OrderBy(input.Sorting).PageBy(input).ToListAsync();
                var dtos = new List<ReturnProductReportDto>();

                foreach (var order in filterOrders)
                {
                    var ticket = _cService.GetAllTickets(cInput).FirstOrDefault(a => a.Id.Equals(order.TicketId));
                    if (ticket == null) continue;
                    var menu = new MenuItem();
                    using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.MustHaveTenant, AppConsts.ConnectFilter))
                    {
                        menu = _menuitemRepo.GetAll()?.FirstOrDefault(x => x.Id == order.MenuItemId);
                    }
                    if (menu == null) continue;

                    var ticketReference = new Transaction.Ticket();

                    if (!String.IsNullOrWhiteSpace(ticket.ReferenceNumber))
                        ticketReference = _cService.GetTicketByNumber(ticket.ReferenceNumber);
                    else
                        continue;

                    if (ticketReference == null)
                        continue;

                    var ticketEntities = JsonConvert.DeserializeObject<IList<TicketEntityMap>>(ticketReference.TicketEntities);
                    var entity = ticketEntities.SingleOrDefault(a => a.EntityTypeId.Equals(1));
                    var payment = ticketReference.Payments.FirstOrDefault(x => x.TicketId == ticketReference.Id);

                    var ticketStateValue = ticketReference.GetTicketStateValues().FirstOrDefault();
                    var returnProduct = JsonHelper.Deserialize<ReturnProductReport>(ticketStateValue.StateValue);

                    string paymentName = payment.PaymentType != null ? payment.PaymentType.Name : "";

                    decimal amount = 0;
                    if (order != null)
                        amount = order.Price * Math.Abs(order.Quantity);

                    // get location 
                    var location = await _locationRepo.FirstOrDefaultAsync(ticket.LocationId);

                    dtos.Add(new ReturnProductReportDto
                    {
                        DateTimeString = ticketReference.LastPaymentTime.ToString(input.DateTimeFormat),
                        Cashier = payment.PaymentUserName,
                        TicketNumber = ticketReference.TicketNumber,
                        CustomerName = entity != null ? entity.EntityName : "",
                        MenuCode = menu?.AliasCode,// order.MenuItemId.ToString(),
                        MenuName = order.MenuItemName,
                        PaymentType = paymentName,
                        Quantity = Math.Abs(order.Quantity),
                        RefundType = returnProduct?.RefundType == ReturnProductReportConsts.Payment ? paymentName : returnProduct?.RefundType,
                        ReturnProductDateString = ticketStateValue.LastUpdateTime.ToString(input.DateTimeFormat),
                        ReturnProductBy = returnProduct?.ReturnProductBy,
                        Reason = order?.Note,
                        Detail = "",
                        Amount = amount ,
                        LocationCode = location?.Code,
                        LocationName = location?.Name,
                    });
                }
                // filter query builder 
                var dataAsQueryable = dtos.AsQueryable();

                if (!string.IsNullOrEmpty(input.DynamicFilter))
                {
                    var jsonSerializerSettings = new JsonSerializerSettings
                    {
                        ContractResolver =
                            new CamelCasePropertyNamesContractResolver()
                    };
                    var filRule = JsonConvert.DeserializeObject<FilterRule>(input.DynamicFilter);
                    if (filRule?.Rules != null)
                    {
                        dataAsQueryable = dataAsQueryable.BuildQuery(filRule);
                    }
                }
                var result = dataAsQueryable.ToList();
                AddObjects(sheet, 2, result,
                            _ => _.LocationCode,
                            _ => _.LocationName,
                            _ => _.DateTimeString,
                            _ => _.Cashier,
                            _ => _.TicketNumber,
                            _ => _.CustomerName,
                            _ => _.MenuCode,
                            _ => _.MenuName,
                            _ => _.PaymentType,
                            _ => Math.Abs(_.Quantity),
                            _ => _.RefundType,
                            _ => _.ReturnProductDateString,
                            _ => _.ReturnProductBy,
                            _ => _.Reason,
                            _ => "",
                            _ => _.Amount
                            );

                //Formatting cells

                for (var i = 1; i <= 16; i++)
                {
                    sheet.Column(i).AutoFit();
                }

                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }
    }
}
