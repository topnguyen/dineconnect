﻿using Abp.Domain.Repositories;
using DinePlan.DineConnect.Connect.Ticket;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.ReturnProduct.Dtos;

namespace DinePlan.DineConnect.Connect.ReturnProduct.Exporting
{
    public interface IReturnProductReportExcelExporter
    {
        Task<FileDto> ExportReturnProduct(GetReturnProductReportInput input, IConnectReportAppService connectReportService);
    }
}
