﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Exporter;
using Newtonsoft.Json;
using OpenHtmlToPdf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace DinePlan.DineConnect.Connect.Discount.Dtos
{
    [AutoMapFrom(typeof(Promotion))]
    public class PromotionListDto : FullAuditedEntityDto
    {
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string StartDateString => StartDate.ToString("yyyy-MM-dd");
        public string EndDateString => EndDate.ToString("yyyy-MM-dd");
        public DateTime CreateDateTime { get; set; }
        public int PromotionTypeId { get; set; }
        public bool Limi1PromotionPerItem { get; set; }
        public  bool Active { get; set; }
        public  bool IsDeleted { get; set; }

        public string PromotionType
        {
            get
            {
                if (PromotionTypeId == 0)
                    return "Not assigned";
                return ((PromotionTypes)PromotionTypeId).ToString();
            }
        }

        public string Locations { get; set; }

        public string LocationsName
        {
            get
            {
                if (!string.IsNullOrEmpty(Locations))
                {
                    var locations = JsonConvert.DeserializeObject<List<SimpleLocationDto>>(Locations);
                    if (locations != null && locations.Any())
                        return locations.Select(l => l.Name).JoinAsString(",");
                    return "";
                }

                return "";
            }
        }
        public Collection<PromotionScheduleEditDto> PromotionSchedules { get; set; }
        public Collection<PromotionRestrictItemEditDto> PromotionRestrictItems { get; set; }
    }

    [AutoMapTo(typeof(Promotion))]
    public class PromotionEditDto : ConnectEditDto
    {
        public PromotionEditDto()
        {
            PromotionSchedules = new Collection<PromotionScheduleEditDto>();
            PromotionRestrictItems = new Collection<PromotionRestrictItemEditDto>();
            ConnectCardTypeId = 0;
            CombineAllPro = true;
        }

        public int? Id { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreationTime { get; set; }
        public int PromotionTypeId { get; set; }
        public bool Active { get; set; }
        public string Filter { get; set; }
        public int SortOrder { get; set; }
        public string PromotionContents { get; set; }
        public bool Limi1PromotionPerItem { get; set; }
        public bool HasQuota { get; set; }
        public bool CombineAllPro { get; set; }
        public string CombineProValues { get; set; }
        public Collection<PromotionScheduleEditDto> PromotionSchedules { get; set; }
        public Collection<PromotionRestrictItemEditDto> PromotionRestrictItems { get; set; }
        public int? ConnectCardTypeId { get; set; }
        public int? PromotionCategoryId { get; set; }
        public bool DisplayPromotion { get; set; }
        public int PromotionPosition { get; set; }

        public void AddSchedule(int startHour, int startMinute, int endHour, int endMinute)
        {
            PromotionSchedules.Add(new PromotionScheduleEditDto
            {
                StartHour = startHour,
                StartMinute = startMinute,
                EndHour = endHour,
                EndMinute = endMinute
            });
        }

        public  bool IgnorePromotionLimitation { get; set; }
        public  bool ApplyOffline { get; set; }
        public string Description { get; set; }
    }

    [AutoMapTo(typeof(PromotionRestrictItem))]
    public class PromotionRestrictItemEditDto
    {
        public PromotionRestrictItemEditDto()
        {
        }

        public int? Id { get; set; }
        public int MenuItemId { get; set; }
        public string Name { get; set; }
    }

    [AutoMapTo(typeof(PromotionSchedule))]
    public class PromotionScheduleEditDto
    {
        private string _Days;
        private string _MonthDays;

        public PromotionScheduleEditDto()
        {
            _Days = null;
            _MonthDays = null;
        }

        public int? Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int StartHour { get; set; }
        public int StartMinute { get; set; }
        public int EndHour { get; set; }
        public int EndMinute { get; set; }
        public List<ComboboxItemDto> AllDays { get; set; }
        public List<ComboboxItemDto> AllMonthDays { get; set; }

        public string Days
        {
            get
            {
                if (AllDays == null || !AllDays.Any())
                    return _Days;
                return string.Join(",", AllDays.Select(a => a.Value).ToArray());
            }
            set { _Days = value; }
        }

        public string MonthDays
        {
            get
            {
                if (AllMonthDays == null || !AllMonthDays.Any())
                {
                    return _MonthDays;
                }
                return string.Join(",", AllMonthDays.Select(a => a.Value).ToArray());
            }
            set { _MonthDays = value; }
        }
    }

    [AutoMapTo(typeof(TimerPromotion))]
    public class TimerPromotionEditDto
    {
        public int? Id { get; set; }
        public int PriceTagId { get; set; }
    }

    [AutoMapTo(typeof(FixedPromotion))]
    public class FixedPromotionEditDto
    {
        public int? Id { get; set; }
        public int CategoryId { get; set; }
        public int ProductGroupId { get; set; }
        public int MenuItemId { get; set; }
        public int PromotionValueType { get; set; }
        public decimal PromotionValue { get; set; }
        public string MenuItemGroupCode { get; set; }
        public string CategoryName { get; set; }
        public string ProductGroupName { get; set; }
        public int MenuItemPortionId { get; set; }
        public string PortionName { get; set; }

    }

    [AutoMapTo(typeof(FreePromotion))]
    public class FreePromotionEditDto
    {
        public int? Id { get; set; }
        public bool AllFrom { get; set; }
        public int FromCount { get; set; }
        public int ToCount { get; set; }
        public Collection<FreePromotionExecutionEditDto> FreePromotionExecutions { get; set; }
        public Collection<FreePromotionExecutionEditDto> From { get; set; }
        public Collection<FreePromotionExecutionEditDto> To { get; set; }

        public FreePromotionEditDto()
        {
            FreePromotionExecutions = new Collection<FreePromotionExecutionEditDto>();
            From = new Collection<FreePromotionExecutionEditDto>();
            To = new Collection<FreePromotionExecutionEditDto>();
            FromCount = 1;
            ToCount = 1;
        }
    }

    [AutoMapTo(typeof(FreePromotionExecution))]
    public class FreePromotionExecutionEditDto
    {
        public int? Id { get; set; }
        public int MenuItemId { get; set; }
        public string PortionName { get; set; }
        public int MenuItemPortionId { get; set; }
        public bool From { get; set; }
        public int CategoryId { get; set; }
        public int ProductGroupId { get; set; }
        public string CategoryName { get; set; }
        public string MenuItemGroupCode { get; set; }
        public string ProductGroupName { get; set; }
    }

    [AutoMapTo(typeof(FreeValuePromotion))]
    public class FreeValuePromotionEditDto
    {
        public int? Id { get; set; }
        public bool AllFrom { get; set; }
        public int FromCount { get; set; }
        public Collection<FreeValuePromotionExecutionEditDto> FreeValuePromotionExecutions { get; set; }
        public Collection<FreeValuePromotionExecutionEditDto> From { get; set; }
        public Collection<FreeValuePromotionExecutionEditDto> To { get; set; }

        public FreeValuePromotionEditDto()
        {
            FreeValuePromotionExecutions = new Collection<FreeValuePromotionExecutionEditDto>();
            From = new Collection<FreeValuePromotionExecutionEditDto>();
            To = new Collection<FreeValuePromotionExecutionEditDto>();
            FromCount = 1;
        }
    }

    [AutoMapTo(typeof(FreeValuePromotionExecution))]
    public class FreeValuePromotionExecutionEditDto
    {
        public int? Id { get; set; }
        public int MenuItemId { get; set; }
        public string PortionName { get; set; }
        public int MenuItemPortionId { get; set; }
        public bool From { get; set; }
        public virtual int ValueType { get; set; }
        public virtual decimal Value { get; set; }
        public int CategoryId { get; set; }
        public int ProductGroupId { get; set; }
        public string CategoryName { get; set; }
        public string MenuItemGroupCode { get; set; }
        public string ProductGroupName { get; set; }
    }

    [AutoMapTo(typeof(FreeItemPromotion))]
    public class FreeItemPromotionEditDto
    {
        public int? Id { get; set; }
        public int ItemCount { get; set; }
        public bool Confirmation { get; set; }
        public decimal TotalAmount { get; set; }
        public bool MultipleTimes { get; set; }
        public Collection<FreeItemPromotionExecutionEditDto> Executions { get; set; }

        public FreeItemPromotionEditDto()
        {
            Executions = new Collection<FreeItemPromotionExecutionEditDto>();
        }
    }

    [AutoMapTo(typeof(FreeItemPromotionExecution))]
    public class FreeItemPromotionExecutionEditDto
    {
        public int? Id { get; set; }
        public int MenuItemId { get; set; }
        public string PortionName { get; set; }
        public int MenuItemPortionId { get; set; }
        public decimal Price { get; set; }
    }

    public class GetPromotionInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public string Operation { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<ComboboxItemDto> PromotionTypes { get; set; }
        public bool Deleted { get; set; }
        public bool AllPromotion { get; set; }
        public LocationGroupDto LocationGroup { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "SortOrder";
            }
        }
        public int TenantId { get; set; }
    }

    public class GetPromotionForEditOutput : IOutputDto
    {
        public GetPromotionForEditOutput()
        {
            Promotion = new PromotionEditDto();

            FixPromotions = new List<FixedPromotionEditDto>();
            FixPromotionsPer = new List<FixedPromotionEditDto>();
            TimerPromotion = new TimerPromotionEditDto();
            FreePromotion = new FreePromotionEditDto();
            DemandPromotion = new DemandPromotionEditDto();
            FreeItemPromotion = new FreeItemPromotionEditDto();
            TicketDiscountPromotion = new TicketDiscountPromotionDto();
            TicketDiscountPromotionDistribution = new TicketDiscountPromotionDistributionEditDto();
            LocationGroup = new LocationGroupDto();
            BuyXAndYAtZValuePromotion = new BuyXAndYAtZValuePromotionEditDto();
            StepDiscountPromotion = new StepDiscountPromotionEditDto();
            PromotionQuotas = new Collection<PromotionQuotaDto>();
        }

        public PromotionEditDto Promotion { get; set; }
        public TimerPromotionEditDto TimerPromotion { get; set; }
        public List<FixedPromotionEditDto> FixPromotions { get; set; }
        public List<FixedPromotionEditDto> FixPromotionsPer { get; set; }
        public FreePromotionEditDto FreePromotion { get; set; }
        public FreeValuePromotionEditDto FreeValuePromotion { get; set; }
        public LocationGroupDto LocationGroup { get; set; }

        public DemandPromotionEditDto DemandPromotion { get; set; }
        public FreeItemPromotionEditDto FreeItemPromotion { get; set; }

        public TicketDiscountPromotionDto TicketDiscountPromotion { get; set; }
        public BuyXAndYAtZValuePromotionEditDto BuyXAndYAtZValuePromotion { get; set; }
        public TicketDiscountPromotionDistributionEditDto TicketDiscountPromotionDistribution { get; set; }
        public StepDiscountPromotionEditDto StepDiscountPromotion { get; set; }
        public Collection<PromotionQuotaDto> PromotionQuotas { get; set; }
        public string Filter { get; set; }
    }

    public class CreateOrUpdatePromotionInput : IInputDto
    {
        [Required]
        public PromotionEditDto Promotion { get; set; }

        public TimerPromotionEditDto TimerPromotion { get; set; }
        public List<FixedPromotionEditDto> FixPromotions { get; set; }
        public List<FixedPromotionEditDto> FixPromotionsPer { get; set; }
        public FreePromotionEditDto FreePromotion { get; set; }
        public FreeValuePromotionEditDto FreeValuePromotion { get; set; }
        public DemandPromotionEditDto DemandPromotion { get; set; }
        public FreeItemPromotionEditDto FreeItemPromotion { get; set; }
        public TicketDiscountPromotionDto TicketDiscountPromotion { get; set; }
        public BuyXAndYAtZValuePromotionEditDto BuyXAndYAtZValuePromotion { get; set; }
        public StepDiscountPromotionEditDto StepDiscountPromotion { get; set; }
        public Collection<PromotionQuotaDto> PromotionQuotas { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public string Filter { get; set; }
    }

    [AutoMapTo(typeof(TicketDiscountPromotion))]
    public class TicketDiscountPromotionDto
    {
        public TicketDiscountPromotionDto()
        {
            PromotionValueType = 1;
            TicketDiscountPromotionDistributions = new Collection<TicketDiscountPromotionDistributionEditDto>();
        }

        public int? Id { get; set; }
        public int PromotionValueType { get; set; }
        public string ButtonCaption { get; set; }
        public decimal PromotionValue { get; set; }
        public bool AskReference { get; set; }
        public bool AuthenticationRequired { get; set; }
        public bool ApplyAsPayment { get; set; }

        public bool PromotionApplyOnce { get; set; }
        public int? PromotionApplyType { get; set; }

        public Collection<TicketDiscountPromotionDistributionEditDto> TicketDiscountPromotionDistributions { get; set; }
    }

    [AutoMapTo(typeof(DemandDiscountPromotion))]
    public class DemandPromotionEditDto
    {
        public DemandPromotionEditDto()
        {
            PromotionValueType = 1;
            DemandPromotionExecutions = new Collection<DemandPromotionExecutionEditDto>();
        }

        public int? Id { get; set; }
        public int PromotionValueType { get; set; }
        public string ButtonCaption { get; set; }
        public string ButtonColor { get; set; }
        public decimal PromotionValue { get; set; }
        public bool PromotionOverride { get; set; }
        public bool AuthenticationRequired { get; set; }
        public bool AskReference { get; set; }
        public bool PromotionApplyOnce { get; set; }
        public int? PromotionApplyType { get; set; }
        public string Group { get; set; }
        public Collection<DemandPromotionExecutionEditDto> DemandPromotionExecutions { get; set; }
    }

    [AutoMapTo(typeof(DemandPromotionExecution))]
    public class DemandPromotionExecutionEditDto
    {
        public int? Id { get; set; }
        public int CategoryId { get; set; }
        public int MenuItemId { get; set; }
        public string CategoryName { get; set; }
        public string MenuItemGroupCode { get; set; }
        public int ProductGroupId { get; set; }
        public string ProductGroupName { get; set; }
        public int MenuItemPortionId { get; set; }
        public string PortionName { get; set; }
    }

    public class PromotionFilter
    {
        public string Id { get; set; }
        public string Label { get; set; }
        public string Type { get; set; }
        public string Input { get; set; }
        public string Operator { get; set; }
        public string Operators { get; set; }
        public string Values { get; set; }
        public decimal Value { get; set; }
    }

    public class ApiPromotionDefinitionInput : IInputDto
    {
        public int TenantId { get; set; }
        public int PromotionId { get; set; }
        public int LocationId { get; set; }
    }
    public class ApiPromotionStatusUpdate : ApiPromotionDefinitionInput
    {
       public bool Active { get; set; }


    }
    public class ApiCardTypeInput : ApiPromotionDefinitionInput
    {
        public int CardTypeId { get; set; }
        public string CardNumber { get; set; }
        public string TicketNumber { get; set; }
        public bool IsPromotion { get; set; }


    }
    public class ApiCardTypeOutput :IOutputDto
    {
        public bool Valid { get; set; }
        public string Reason { get; set; }

    }
    public class ApiPromotionOutput : IOutputDto
    {
        public List<PromotionEditDto> Promotions { get; set; }
    }

    public class ApiTimerPromotionOutput : IOutputDto
    {
        public string PriceTag { get; set; }
    }

    public class ApiFixedPromotionOutput : IOutputDto
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int MenuItemId { get; set; }
        public int ValueType { get; set; }
        public decimal Value { get; set; }
        public int ProductGroupId { get; set; }
        public int MenuItemPortionId { get; set; }
    }

    public class BuyXAndYAtZValuePromotionEditDto
    {
        public BuyXAndYAtZValuePromotionEditDto()
        {
            BuyXAndYAtZValueExecutions = new Collection<BuyXAndYAtZValuePromotionExecutionEditDto>();
        }
        public int ItemCount { get; set; }
        public decimal Value { get; set; }
        public Collection<BuyXAndYAtZValuePromotionExecutionEditDto> BuyXAndYAtZValueExecutions { get; set; }
    }

    public class BuyXAndYAtZValuePromotionExecutionEditDto
    {
        public int? Id { get; set; }
        public int ProductGroupId { get; set; }
        public string ProductGroupName { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int MenuItemId { get; set; }
        public string MenuItemGroupCode { get; set; }
        public int MenuItemPortionId { get; set; }
        public string PortionName { get; set; }
    }

    public class StepDiscountPromotionEditDto
    {
        public StepDiscountPromotionEditDto()
        {
            StepDiscountExecutions = new Collection<StepDiscountPromotionExecutionEditDto>();
        }
        public int StepDiscountType { get; set; }
        public bool ApplyForEachQuantity { get; set; }
        public Collection<StepDiscountPromotionExecutionEditDto> StepDiscountExecutions { get; set; }
        public Collection<StepDiscountPromotionMappingExecutionEditDto> StepDiscountMappingExecutions { get; set; }
    }
    public class StepDiscountPromotionExecutionEditDto
    {
        public int? Id { get; set; }
        public decimal PromotionStepTypeValue { get; set; }
        public int PromotionValueType { get; set; }
        public string PromotionValueTypeName { get; set; }
        public decimal PromotionValue { get; set; }
    }
    public class StepDiscountPromotionMappingExecutionEditDto
    {
        public int? Id { get; set; }
        public int ProductGroupId { get; set; }
        public int CategoryId { get; set; }
        public int MenuItemId { get; set; }
        public int MenuItemPortionId { get; set; }
        public string CategoryName { get; set; }
        public string MenuItemGroupCode { get; set; }
        public string ProductGroupName { get; set; }
        public string PortionName { get; set; }
    }
    public class TicketDiscountPromotionDistributionEditDto:ConnectEditDto
    {
        public int? Id { get; set; }
        public int ProductGroupId { get; set; }
        public int CategoryId { get; set; }
        public int MenuItemId { get; set; }
        public string CategoryName { get; set; }
        public string MenuItemGroupCode { get; set; }
        public string ProductGroupName { get; set; }
        public int MenuItemPortionId { get; set; }
        public string PortionName { get; set; }
        public decimal Percentage { get; set; }
    }

    [AutoMapTo(typeof(PromotionQuota))]
    public class PromotionQuotaDto : ConnectEditDto
    {
        public int? Id { get; set; }
        public int PromotionId { get; set; }
        public int QuotaAmount { get; set; }
        public int QuotaUsed { get; set; }
        public int ResetType { get; set; }
        public int? ResetWeekValue { get; set; }
        public DateTime? ResetMonthValue { get; set; }
        public DateTime? LastResetDate { get; set; }
        public LocationGroupDto LocationGroup { get; set; }
        public string Plants { get; set; }
        public bool Each { get; set; }
    }

    public class CustomLocationGroupDto
    {
        public CustomLocationGroupDto()
        {
            Locations = new List<QuotaLocationDto>();
            Groups = new List<SimpleLocationGroupDto>();
            Group = false;
            LocationTags = new List<SimpleLocationTagDto>();
            LocationTag = false;
            NonLocations = new List<SimpleLocationDto>();
        }

        public List<QuotaLocationDto> Locations { get; set; }
        public List<SimpleLocationGroupDto> Groups { get; set; }
        public List<SimpleLocationTagDto> LocationTags { get; set; }
        public List<SimpleLocationDto> NonLocations { get; set; }

        public bool Group { get; set; }
        public bool LocationTag { get; set; }
        public long UserId { get; set; }
    }

    [AutoMapTo(typeof(SimpleLocationDto), typeof(Master.Location))]
    public class QuotaLocationDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public long OrganizationUnitId { get; set; }
        public int CompanyRefId { get; set; }
        public int QuotaUsed { get; set; }
    }

    public class GetQuotaDetailInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int PromotionId { get; set; }
        public int PromotionQuotaId { get; set; }
        public int LocationId { get; set; }
        public string DynamicFilter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Plant";
            }
        }
    }

    public class GetCombinePromotionInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int  PromotionQuotaId { get; set; }
        public int LocationId { get; set; }
        public string DynamicFilter { get; set; }
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "name";
            }
        }
    }
    public class QuotaDetail
    {
        public int PromotionId { get; set; }
        public int PlantId { get; set; }
        public string PlantNo { get; set; }
        public string Plant { get; set; }
        public int QuotaId { get; set; }
        public int Quota { get; set; }
        public int TotalUsed { get; set; }
    }

    public class QuotaDetailInfo
    {
        public PagedResultOutput<QuotaDetail> QuotaDetails { get; set; }
        public DashboardOrderDto DashBoardDto { get; set; }
    }

    public class QuotaTicket
    {
        public int TicketId { get; set; }
        public string TicketNo { get; set; }
        public string InvoiceNo { get; set; }
        public string TaxInvoiceNo { get; set; }
        public int PlantId { get; set; }
        public string Plant { get; set; }
        public string PlantName { get; set; }
        public string PosNo { get; set; }
        public string SaleMode { get; set; }
        public string SaleChannel { get; set; }
        public string User { get; set; }
        public DateTime Date { get; set; }
        public int QuotaUsed { get; set; }
        public string BlueCardId { get; set; }
        public decimal TotalAmount { get; set; }
    }
    public class QuotaTicketInfo
    {
        public PagedResultOutput<QuotaTicket> QuotaTickets { get; set; }
        public DashboardOrderDto DashBoardDto { get; set; }
    }
    public class QuotaExportInput : PagedAndSortedInputDto, IShouldNormalize, IFileExport
    {
        public int PromotionQuotaId { get; set; }
        public int LocationId { get; set; }
        public bool RunInBackground { get; set; }
        public string ReportDescription { get; set; }
        public string ReportName { get; set; }
        public int BackGroundId { get; set; }
        public ExportType ExportOutputType { get; set; }
        public PaperSize PaperSize { get; set; }
        public bool Portrait { get; set; }
        public IList<QuotaDetail> Quotas { get; set; }
        public IList<QuotaTicket> QuotaTickets { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Plant";
            }
        }
    }

    public class CombinePromotionDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

}