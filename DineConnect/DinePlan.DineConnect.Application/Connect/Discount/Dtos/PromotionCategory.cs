﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Dto;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace DinePlan.DineConnect.Connect.Discount.Dtos
{
    [AutoMapFrom(typeof(PromotionCategory))]
    public class PromotionCategoryListDto : FullAuditedEntityDto
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Tag { get; set; }

    }
    [AutoMapTo(typeof(PromotionCategory))]
    public class PromotionCategoryEditDto : ConnectEditDto
    {
        public int? Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Tag { get; set; }

    }

    public class GetPromotionCategoryInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime DESC";
            }
        }
        public GetPromotionCategoryInput()
        {
        }

    }
    public class GetPromotionCategoryForEditOutput : IOutputDto
    {
        public GetPromotionCategoryForEditOutput()
        {
        }
        public PromotionCategoryEditDto PromotionCategory { get; set; }
    }
    public class CreateOrUpdatePromotionCategoryInput : IInputDto
    {
        [Required]
        
        public PromotionCategoryEditDto PromotionCategory { get; set; }
       
    }
}
