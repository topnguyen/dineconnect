﻿using System;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Utility.Exporter;
using System.Collections.Generic;
using System.Linq;
using Abp.Linq.Extensions;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Linq.Dynamic;
using Abp.UI;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Connect.Discount.Dtos;

namespace DinePlan.DineConnect.Connect.Discount.Implementation
{
    public class PromotionCategoryAppService : DineConnectAppServiceBase, IPromotionCategoryAppService
    {
        private readonly IPromotionCategoryManager _promotionCategoryManager;
        private readonly IRepository<PromotionCategory> _promotionCategoryRepo;
        private readonly IExcelExporter _exporter;
        private readonly IRepository<Promotion> _promotionRepo;

        public PromotionCategoryAppService(IPromotionCategoryManager promotionCategoryManager,
            IRepository<PromotionCategory> promotionCategoryRepo,
            IRepository<Promotion> promotionRepo,
            IExcelExporter exporter)
        {
            _promotionCategoryManager = promotionCategoryManager;
            _promotionCategoryRepo = promotionCategoryRepo;
            _exporter = exporter;
            _promotionRepo = promotionRepo;
        }

        public async Task<PagedResultOutput<PromotionCategoryListDto>> GetAll(GetPromotionCategoryInput input)
        {
            var allItems = _promotionCategoryRepo
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Name.Contains(input.Filter)
                );

            var sortMenuItems = allItems.AsQueryable()
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToList();

            var allListDtos = sortMenuItems.MapTo<List<PromotionCategoryListDto>>();
            return new PagedResultOutput<PromotionCategoryListDto>(
                allItems.Count(),
                allListDtos
                );
        }

        public async Task<FileDto> GetAllToExcel(GetPromotionCategoryInput input)
        {
            var allList = await GetAll(input);
            var allListDtos = allList.Items.MapTo<List<PromotionCategoryListDto>>();
            var baseE = new BaseExportObject
            {
                ExportObject = allListDtos,
                ColumnNames = new string[] { "Id", "Code", "Name", "Tag" },
                ColumnDisplayNames = new string[] { "Id", "Code", "Name", "Tag" }
            };
            return _exporter.ExportToFile(baseE, L("PromotionCategory"));
        }

        public async Task<GetPromotionCategoryForEditOutput> GetPromotionCategoryForEdit(NullableIdInput input)
        {
            var output = new GetPromotionCategoryForEditOutput();
            PromotionCategoryEditDto editDto;

            if (input.Id.HasValue)
            {
                var hDto = await _promotionCategoryRepo.GetAsync(input.Id.Value);
                editDto = hDto.MapTo<PromotionCategoryEditDto>();
            }
            else
            {
                editDto = new PromotionCategoryEditDto();
            }
            output.PromotionCategory = editDto;
            return output;
        }

        public async Task CreateOrUpdatePromotionCategory(CreateOrUpdatePromotionCategoryInput input)
        {
            if (input.PromotionCategory.Id.HasValue)
            {
                await UpdatePromotionCategory(input);
            }
            else
            {
                await CreatePromotionCategory(input);
            }
        }

        public async Task<bool> DeletePromotionCategory(IdInput input)
        {
            var anyPromo = _promotionRepo.GetAll().Where(a =>
                      a.PromotionCategoryId != null && a.PromotionCategoryId == input.Id && !a.IsDeleted);

            if (anyPromo.Any())
            {
                anyPromo = anyPromo.Where(a => a.EndDate > DateTime.Now);
                if (!anyPromo.Any())
                {
                    await _promotionCategoryRepo.DeleteAsync(input.Id);                  
                }
                else
                {
                    return false;
                }            
            }
            else
            {
                await _promotionCategoryRepo.DeleteAsync(input.Id);
            }
            return true;
        }

        protected virtual async Task UpdatePromotionCategory(CreateOrUpdatePromotionCategoryInput input)
        {
            var item = await _promotionCategoryRepo.GetAsync(input.PromotionCategory.Id.Value);
            var dto = input.PromotionCategory;
            item.Name = dto.Name;
            item.Code = dto.Code;
            item.Tag = dto.Tag;

            CheckErrors(await _promotionCategoryManager.CreateOrUpdateSync(item));
        }

        protected virtual async Task CreatePromotionCategory(CreateOrUpdatePromotionCategoryInput input)
        {
            var dto = input.PromotionCategory.MapTo<PromotionCategory>();
            CheckErrors(await _promotionCategoryManager.CreateOrUpdateSync(dto));

        }
        public async Task<ListResultOutput<PromotionCategoryListDto>> GetPromotionCategorys()
        {
            var promotionCategory = _promotionCategoryRepo.GetAll();
            return new ListResultOutput<PromotionCategoryListDto>(promotionCategory.MapTo<List<PromotionCategoryListDto>>());
        }
    }
}
