﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using AutoMapper;
using DinePlan.DineConnect.Connect.Discount.Dtos;
using DinePlan.DineConnect.Connect.Location;
using DinePlan.DineConnect.Connect.Master;
using DinePlan.DineConnect.Connect.Master.Dtos;
using DinePlan.DineConnect.Connect.Menu;
using DinePlan.DineConnect.Connect.Report;
using DinePlan.DineConnect.Connect.Report.Dtos;
using DinePlan.DineConnect.Connect.Sync;
using DinePlan.DineConnect.Connect.Ticket.Dto;
using DinePlan.DineConnect.Connect.Transaction;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Helper;
using DinePlan.DineConnect.Job;
using DinePlan.DineConnect.Report;
using DinePlan.DineConnect.Utility;
using DinePlan.DineConnect.Utility.Exporter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Card;
using System.Diagnostics;
using Abp.Authorization;
using Itenso.TimePeriod;
using System.Text;

namespace DinePlan.DineConnect.Connect.Discount.Implementation
{
    public class PromotionAppService : DineConnectAppServiceBase, IPromotionAppService
    {
        private readonly IRepository<Category> _cRepository;
        private readonly IRepository<DemandPromotionExecution> _demExePromoRepo;
        private readonly IRepository<DemandDiscountPromotion> _dptRepository;

        private readonly IExcelExporter _exporter;
        private readonly IRepository<FreeItemPromotionExecution> _fiePromotion;
        private readonly IRepository<FreeItemPromotion> _fiPromotion;
        private readonly IRepository<FixedPromotion> _fixedPromotionRepo;
        private readonly IRepository<FreePromotionExecution> _frePromotion;
        private readonly IRepository<FreePromotion> _frPromotion;
        private readonly IRepository<FreeValuePromotion> _frvPromotion;
        private readonly IRepository<FreeValuePromotionExecution> _frvPromotionExecution;
        private readonly ILocationAppService _locAppService;
        private readonly IRepository<MenuItem> _menuManager;
        private readonly IRepository<MenuItemPortion> _menuItemPortionManager;
        private readonly IRepository<PromotionRestrictItem> _promoRestrictRepo;
        private readonly IPromotionManager _promotionManager;
        private readonly IRepository<Promotion> _promotionRepo;
        private readonly IRepository<PromotionSchedule> _promotionSchedule;
        private readonly IRepository<PriceTag> _ptRepository;
        private readonly ISyncAppService _syncAppService;
        private readonly IRepository<TicketDiscountPromotion> _tdRepository;
        private readonly IRepository<TimerPromotion> _tRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<ProductGroup> _productGroupRepo;
        private readonly IRepository<PromotionQuota> _pqRepository;
        private readonly IRepository<Transaction.Ticket> _ticketRepository;
        private readonly IReportBackgroundAppService _rbas;
        private readonly IBackgroundJobManager _bgm;
        private readonly IPromotionExporter _promoExporter;
        private readonly IRepository<Master.Location> _locationRepo;
        private readonly IRepository<ConnectCardType> _connectCardTypeRepo;
        private readonly IRepository<Card.ConnectCard> _connectCardRepo;
        private readonly IRepository<Card.ConnectCardRedemption> _connectCardRedemptionRepo;



        public PromotionAppService(IPromotionManager promotionManager, ILocationAppService locAppService,
            IRepository<DemandPromotionExecution> demExePromoRepo,
            IRepository<Promotion> promotionRepo, IRepository<TimerPromotion> tRepository,
            IRepository<PromotionRestrictItem> promoRestrictRepo,
            IRepository<PriceTag> ptRepository, IRepository<FreePromotion> frRepository,
            IRepository<FreeValuePromotion> frvPromotion, IRepository<MenuItem> menuManager,
            IRepository<MenuItemPortion> menuItemPortionManager,
            IRepository<FreeValuePromotionExecution> frvPromotionExecution,
            IRepository<FreePromotionExecution> frePromotion,
            IRepository<PromotionSchedule> promotionScheduleRepo, ISyncAppService sps,
            IRepository<FixedPromotion> fRepository,
            IRepository<Category> cRepo, IExcelExporter exporter,
            IRepository<DemandDiscountPromotion> dptRepository, IRepository<FreeItemPromotion> fiPromotion,
            IRepository<TicketDiscountPromotion> tdRepository, IUnitOfWorkManager unitofWorkManager,
            IRepository<FreeItemPromotionExecution> fiePromotion,
            IRepository<ProductGroup> productGroupRepo,
            IRepository<PromotionQuota> pqRepo,
            IRepository<Transaction.Ticket> ticketRepository,
            IReportBackgroundAppService reportBackgroundAppService,
            IBackgroundJobManager backgroundJobManager,
            IPromotionExporter promotionExporter, IRepository<Master.Location> locationRepo,IRepository<ConnectCardType> connectCardTypeRepo,
            IRepository<Card.ConnectCard> connectCardRepo,IRepository<Card.ConnectCardRedemption> connectCardRedemptionRepo)
        {
            _promoExporter = promotionExporter;
            _bgm = backgroundJobManager;
            _rbas = reportBackgroundAppService;
            _promoRestrictRepo = promoRestrictRepo;
            _tdRepository = tdRepository;
            _locAppService = locAppService;
            _promotionManager = promotionManager;
            _ptRepository = ptRepository;
            _promotionRepo = promotionRepo;
            _tRepository = tRepository;
            _fixedPromotionRepo = fRepository;
            _frPromotion = frRepository;
            _frePromotion = frePromotion;
            _frvPromotion = frvPromotion;
            _frvPromotionExecution = frvPromotionExecution;
            _dptRepository = dptRepository;
            _promotionSchedule = promotionScheduleRepo;
            _exporter = exporter;
            _syncAppService = sps;
            _cRepository = cRepo;
            _fiPromotion = fiPromotion;
            _fiePromotion = fiePromotion;
            _demExePromoRepo = demExePromoRepo;
            _menuManager = menuManager;
            _menuItemPortionManager = menuItemPortionManager;
            _unitOfWorkManager = unitofWorkManager;
            _productGroupRepo = productGroupRepo;
            _pqRepository = pqRepo;
            _ticketRepository = ticketRepository;
            _locationRepo = locationRepo;
            _connectCardTypeRepo = connectCardTypeRepo;
            _connectCardRepo = connectCardRepo;
            _connectCardRedemptionRepo = connectCardRedemptionRepo;

        }

        public IAppFolders AppFolders { get; set; }

        public async Task<ListResultOutput<PromotionListDto>> GetIds()
        {
            var lstPromotion = await _promotionRepo.GetAll().ToListAsync();
            return new ListResultOutput<PromotionListDto>(lstPromotion.MapTo<List<PromotionListDto>>());
        }

        public async Task<PagedResultOutput<PromotionListDto>> GetPromotions(GetPromotionInput input)
        {
            var allItems = _promotionRepo.GetAll()
                .WhereIf(
                            !input.Filter.IsNullOrEmpty(),
                            p => p.Name.Contains(input.Filter)
                        );

            var sortMenuItems = await allItems
                     .OrderBy(input.Sorting)
                     .PageBy(input)
                     .ToListAsync();
            return
                new PagedResultOutput<PromotionListDto>(await allItems.CountAsync(), sortMenuItems.MapTo<List<PromotionListDto>>());
        }

        public async Task<FileDto> GetAllToExcel(string sorting)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var allList = await _promotionRepo.GetAll().OrderBy(sorting).ToListAsync();
                var allListDtos = allList.MapTo<List<PromotionListDto>>();

                var baseE = new BaseExportObject
                {
                    ExportObject = allListDtos,
                    ColumnNames = new[]
                    {
                        "Id", "Name", "PromotionType", "StartDateString", "EndDateString", "LocationsName", "IsDeleted",
                        "Active"
                    }
                };
                return _exporter.ExportToFile(baseE, L("Promotion"));
            }
        }

        public async Task<FileDto> GetExportPromotions(ApiLocationInput input)
        {
            if (input.TenantId == 0) input.TenantId = AbpSession.TenantId ?? 0;
            var allPromotions = await ApiGetPromotions(input);
            var allContent = JsonConvert.SerializeObject(allPromotions);

            var file = new FileDto("Promotions.json", "application/json");
            var filePath = Path.Combine(AppFolders.TempFileDownloadFolder, file.FileToken);
            try
            {
                File.WriteAllText(filePath, allContent);
            }
            catch (Exception exception)
            {
                throw new UserFriendlyException(exception.Message);
            }

            return file;
        }

        public async Task DeletePromotion(IdInput input)
        {
            await _pqRepository.DeleteAsync(x => x.PromotionId == input.Id);
            await _promotionRepo.DeleteAsync(input.Id);
        }

        public async Task SaveSortOrder(int[] all)
        {
            var i = 1;
            foreach (var promoId in all)
            {
                var cate = await _promotionRepo.GetAsync(promoId);
                cate.SortOrder = i++;
                await _promotionRepo.UpdateAsync(cate);
            }
        }

        public async Task<PagedResultOutput<PromotionListDto>> GetAll(GetPromotionInput input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var allItems = _promotionRepo.GetAll().Where(i => i.IsDeleted == input.Deleted);

                if (input.Operation == "SEARCH")
                    allItems = allItems

                        .WhereIf(
                            !input.Filter.IsNullOrEmpty(),
                            p => p.Name.Equals(input.Filter)
                        );
                else
                    allItems = allItems

                        .WhereIf(
                            !input.Filter.IsNullOrEmpty(),
                            p => p.Name.Contains(input.Filter)
                        );
                if (!input.Deleted && input.PromotionTypes != null && input.PromotionTypes.Any())
                {
                    var allTypes = input.PromotionTypes.Select(a => Convert.ToInt32(a.Value)).ToArray();
                    allItems = allItems.Where(a => allTypes.Contains(a.PromotionTypeId));
                }


               
                var allMyEnItems = SearchLocation(allItems, input.LocationGroup).OfType<Promotion>();

                if (!input.Deleted && !input.AllPromotion)
                {
                    allMyEnItems = allMyEnItems.Where(x =>
                    {
                        TimeRange firstRange = new TimeRange(
                            new DateTime(input.StartDate.Year, input.StartDate.Month,input.StartDate.Day),
                            new DateTime(input.EndDate.Year, input.EndDate.Month,input.EndDate.Day)
                        );

                        TimeRange secondRange = new TimeRange(
                            new DateTime(x.StartDate.Year, x.StartDate.Month,x.StartDate.Day),
                            new DateTime(x.EndDate.Year, x.EndDate.Month,x.EndDate.Day)
                        );

                        var relation = firstRange.GetRelation(secondRange);
                        return relation != PeriodRelation.Before &&
                               relation != PeriodRelation.After;

                    });
                }


                var allItemCount = allMyEnItems.Count();
                
                if (!string.IsNullOrEmpty(input.Sorting))
                {
                    input.Sorting = "SortOrder";
                }

                var sortMenuItems = allMyEnItems.AsQueryable()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToList();

                var allListDtos = sortMenuItems.MapTo<List<PromotionListDto>>();

                return new PagedResultOutput<PromotionListDto>(
                    allItemCount,
                    allListDtos
                );
            }
        }

        public async Task<GetPromotionForEditOutput> GetPromotionForEdit(NullableIdInput input)
        {
            try
            {
                PromotionEditDto editDto;
                if (input.Id.HasValue)
                {
                    var hDto = await _promotionRepo.GetAsync(input.Id.Value);
                    editDto = hDto.MapTo<PromotionEditDto>();
                    if (hDto.ConnectCardTypeId == null)
                        editDto.ConnectCardTypeId = 0;
                }
                else
                {
                    editDto = new PromotionEditDto();
                }

                foreach (var edi in editDto.PromotionSchedules)
                {
                    edi.AllDays = new List<ComboboxItemDto>();

                    if (!string.IsNullOrEmpty(edi.Days))
                        foreach (var day in edi.Days.Split(","))
                        {
                            var myday = (DayOfWeek)Convert.ToInt32(day);

                            edi.AllDays.Add(new ComboboxItemDto
                            {
                                DisplayText = myday.ToString(),
                                Value = day
                            });
                        }

                    edi.AllMonthDays = new List<ComboboxItemDto>();

                    if (!string.IsNullOrEmpty(edi.MonthDays))
                        foreach (var day in edi.MonthDays.Split(","))
                        {
                            var myday = Convert.ToInt32(day);

                            edi.AllMonthDays.Add(new ComboboxItemDto
                            {
                                DisplayText = myday.ToString(),
                                Value = day
                            });
                        }
                }

                var output = new GetPromotionForEditOutput
                {
                    Promotion = editDto,
                    TimerPromotion = await GetTimerPromotion(input),
                    FixPromotions = await GetFixedPromotion(input),
                    FixPromotionsPer = await GetFixedPromotion(input),
                    FreePromotion = await GetFreePromotion(input),
                    FreeValuePromotion = await GetFreeValuePromotion(input),
                    DemandPromotion = await GetDemandPromotion(input),
                    FreeItemPromotion = await GetFreeItemPromotion(input),
                    TicketDiscountPromotion = await GetTicketDiscountPromotion(input),
                    PromotionQuotas = await GetPromotionQuotaForEdit(input),
                    Filter = editDto.Filter,
                    LocationGroup = { Group = editDto.Group }
                };

                var rscategory = await _cRepository.GetAllListAsync();
                var rsmenuItem = await _menuManager.GetAllListAsync();
                var rsproductGroupItem = await _productGroupRepo.GetAllListAsync();
                var rsmenuItemPortion = await _menuItemPortionManager.GetAllListAsync();
                if (output.FixPromotions != null && output.FixPromotions.Any())
                {
                    foreach (var lst in output.FixPromotions)
                    {
                        if (lst.CategoryId != 0)
                        {
                            var category = rscategory.FirstOrDefault(t => t.Id == lst.CategoryId);
                            if (category != null)
                            {
                                lst.CategoryName = category.Name;
                            }
                        }
                        if (lst.MenuItemId != 0)
                        {
                            var menuItem = rsmenuItem.FirstOrDefault(t => t.Id == lst.MenuItemId);
                            if (menuItem != null)
                            {
                                lst.MenuItemGroupCode = menuItem.Name;
                            }
                        }
                        if (lst.ProductGroupId != 0)
                        {
                            var productGroup = rsproductGroupItem.FirstOrDefault(t => t.Id == lst.ProductGroupId);
                            if (productGroup != null)
                            {
                                lst.ProductGroupName = productGroup.Name;
                            }
                        }
                        if (lst.MenuItemPortionId != 0)
                        {
                            var menuItemPortion = rsmenuItemPortion.FirstOrDefault(t => t.Id == lst.MenuItemPortionId);
                            if (menuItemPortion != null)
                            {
                                var menuItem = rsmenuItem.FirstOrDefault(t => t.Id == menuItemPortion.MenuItemId);
                                if (menuItem != null)
                                {
                                    lst.PortionName = menuItemPortion.MenuItem.Name + "(" + menuItemPortion.Name + ")";
                                }
                            }
                        }
                    }
                }
                if (output.FixPromotionsPer != null && output.FixPromotionsPer.Any())
                {
                    foreach (var lst in output.FixPromotionsPer)
                    {
                        if (lst.CategoryId != 0)
                        {
                            var category = rscategory.FirstOrDefault(t => t.Id == lst.CategoryId);
                            if (category != null)
                            {
                                lst.CategoryName = category.Name;
                            }
                        }
                        if (lst.MenuItemId != 0)
                        {
                            var menuItem = rsmenuItem.FirstOrDefault(t => t.Id == lst.MenuItemId);
                            if (menuItem != null)
                            {
                                lst.MenuItemGroupCode = menuItem.Name;
                            }
                        }
                        if (lst.ProductGroupId != 0)
                        {
                            var productGroup = rsproductGroupItem.FirstOrDefault(t => t.Id == lst.ProductGroupId);
                            if (productGroup != null)
                            {
                                lst.ProductGroupName = productGroup.Name;
                            }
                        }
                        if (lst.MenuItemPortionId != 0)
                        {
                            var menuItemPortion = rsmenuItemPortion.FirstOrDefault(t => t.Id == lst.MenuItemPortionId);
                            if (menuItemPortion != null)
                            {
                                var menuItem = rsmenuItem.FirstOrDefault(t => t.Id == menuItemPortion.MenuItemId);
                                if (menuItem != null)
                                {
                                    lst.PortionName = menuItemPortion.MenuItem.Name + "(" + menuItemPortion.Name + ")";
                                }
                            }
                        }
                    }

                }
                if (output.FreePromotion != null)
                {
                    if (output.FreePromotion.From != null && output.FreePromotion.From.Any())
                    {
                        foreach (var lst in output.FreePromotion.From)
                        {
                            if (lst.CategoryId != 0)
                            {
                                var category = rscategory.FirstOrDefault(t => t.Id == lst.CategoryId);
                                if (category != null)
                                {
                                    lst.CategoryName = category.Name;
                                }
                            }
                            if (lst.MenuItemId != 0)
                            {
                                var menuItem = rsmenuItem.FirstOrDefault(t => t.Id == lst.MenuItemId);
                                if (menuItem != null)
                                {
                                    lst.MenuItemGroupCode = menuItem.Name;
                                }
                            }
                            if (lst.ProductGroupId != 0)
                            {
                                var productGroup = rsproductGroupItem.FirstOrDefault(t => t.Id == lst.ProductGroupId);
                                if (productGroup != null)
                                {
                                    lst.ProductGroupName = productGroup.Name;
                                }
                            }
                            if (lst.MenuItemPortionId != 0)
                            {
                                var menuItemPortion = rsmenuItemPortion.FirstOrDefault(t => t.Id == lst.MenuItemPortionId);
                                if (menuItemPortion != null)
                                {
                                    var menuItem = rsmenuItem.FirstOrDefault(t => t.Id == menuItemPortion.MenuItemId);
                                    if (menuItem != null)
                                    {
                                        lst.PortionName = menuItemPortion.MenuItem.Name + "(" + menuItemPortion.Name + ")";
                                    }
                                }
                            }
                        }
                    }
                    if (output.FreePromotion.To != null && output.FreePromotion.To.Any())
                    {
                        foreach (var lst in output.FreePromotion.To)
                        {
                            if (lst.CategoryId != 0)
                            {
                                var category = rscategory.FirstOrDefault(t => t.Id == lst.CategoryId);
                                if (category != null)
                                {
                                    lst.CategoryName = category.Name;
                                }
                            }
                            if (lst.MenuItemId != 0)
                            {
                                var menuItem = rsmenuItem.FirstOrDefault(t => t.Id == lst.MenuItemId);
                                if (menuItem != null)
                                {
                                    lst.MenuItemGroupCode = menuItem.Name;
                                }
                            }
                            if (lst.ProductGroupId != 0)
                            {
                                var productGroup = rsproductGroupItem.FirstOrDefault(t => t.Id == lst.ProductGroupId);
                                if (productGroup != null)
                                {
                                    lst.ProductGroupName = productGroup.Name;
                                }
                            }
                            if (lst.MenuItemPortionId != 0)
                            {
                                var menuItemPortion = rsmenuItemPortion.FirstOrDefault(t => t.Id == lst.MenuItemPortionId);
                                if (menuItemPortion != null)
                                {
                                    var menuItem = rsmenuItem.FirstOrDefault(t => t.Id == menuItemPortion.MenuItemId);
                                    if (menuItem != null)
                                    {
                                        lst.PortionName = menuItemPortion.MenuItem.Name + "(" + menuItemPortion.Name + ")";
                                    }
                                }
                            }
                        }
                    }
                }
                if (output.FreeValuePromotion != null)
                {
                    if (output.FreeValuePromotion.From != null && output.FreeValuePromotion.From.Any())
                    {
                        foreach (var lst in output.FreeValuePromotion.From)
                        {
                            if (lst.CategoryId != 0)
                            {
                                var category = rscategory.FirstOrDefault(t => t.Id == lst.CategoryId);
                                if (category != null)
                                {
                                    lst.CategoryName = category.Name;
                                }
                            }
                            if (lst.MenuItemId != 0)
                            {
                                var menuItem = rsmenuItem.FirstOrDefault(t => t.Id == lst.MenuItemId);
                                if (menuItem != null)
                                {
                                    lst.MenuItemGroupCode = menuItem.Name;
                                }
                            }
                            if (lst.ProductGroupId != 0)
                            {
                                var productGroup = rsproductGroupItem.FirstOrDefault(t => t.Id == lst.ProductGroupId);
                                if (productGroup != null)
                                {
                                    lst.ProductGroupName = productGroup.Name;
                                }
                            }
                            if (lst.MenuItemPortionId != 0)
                            {
                                var menuItemPortion = rsmenuItemPortion.FirstOrDefault(t => t.Id == lst.MenuItemPortionId);
                                if (menuItemPortion != null)
                                {
                                    var menuItem = rsmenuItem.FirstOrDefault(t => t.Id == menuItemPortion.MenuItemId);
                                    if (menuItem != null)
                                    {
                                        lst.PortionName = menuItemPortion.MenuItem.Name + "(" + menuItemPortion.Name + ")";
                                    }
                                }
                            }
                        }
                    }
                    if (output.FreeValuePromotion.To != null && output.FreeValuePromotion.To.Any())
                    {
                        foreach (var lst in output.FreeValuePromotion.To)
                        {
                            if (lst.CategoryId != 0)
                            {
                                var category = rscategory.FirstOrDefault(t => t.Id == lst.CategoryId);
                                if (category != null)
                                {
                                    lst.CategoryName = category.Name;
                                }
                            }
                            if (lst.MenuItemId != 0)
                            {
                                var menuItem = rsmenuItem.FirstOrDefault(t => t.Id == lst.MenuItemId);
                                if (menuItem != null)
                                {
                                    lst.MenuItemGroupCode = menuItem.Name;
                                }
                            }
                            if (lst.ProductGroupId != 0)
                            {
                                var productGroup = rsproductGroupItem.FirstOrDefault(t => t.Id == lst.ProductGroupId);
                                if (productGroup != null)
                                {
                                    lst.ProductGroupName = productGroup.Name;
                                }
                            }
                            if (lst.MenuItemPortionId != 0)
                            {
                                var menuItemPortion = rsmenuItemPortion.FirstOrDefault(t => t.Id == lst.MenuItemPortionId);
                                if (menuItemPortion != null)
                                {
                                    var menuItem = rsmenuItem.FirstOrDefault(t => t.Id == menuItemPortion.MenuItemId);
                                    if (menuItem != null)
                                    {
                                        lst.PortionName = menuItemPortion.MenuItem.Name + "(" + menuItemPortion.Name + ")";
                                    }
                                }
                            }
                        }
                    }
                }
                if (output.DemandPromotion != null)
                {
                    if (output.DemandPromotion.DemandPromotionExecutions != null && output.DemandPromotion.DemandPromotionExecutions.Any())
                    {
                        foreach (var lst in output.DemandPromotion.DemandPromotionExecutions)
                        {
                            if (lst.CategoryId != 0)
                            {
                                var category = rscategory.FirstOrDefault(t => t.Id == lst.CategoryId);
                                if (category != null)
                                {
                                    lst.CategoryName = category.Name;
                                }
                            }
                            if (lst.MenuItemId != 0)
                            {
                                var menuItem = rsmenuItem.FirstOrDefault(t => t.Id == lst.MenuItemId);
                                if (menuItem != null)
                                {
                                    lst.MenuItemGroupCode = menuItem.Name;
                                }
                            }
                            if (lst.ProductGroupId != 0)
                            {
                                var productGroup = rsproductGroupItem.FirstOrDefault(t => t.Id == lst.ProductGroupId);
                                if (productGroup != null)
                                {
                                    lst.ProductGroupName = productGroup.Name;
                                }
                            }
                            if (lst.MenuItemPortionId != 0)
                            {
                                var menuItemPortion = rsmenuItemPortion.FirstOrDefault(t => t.Id == lst.MenuItemPortionId);
                                if (menuItemPortion != null)
                                {
                                    var menuItem = rsmenuItem.FirstOrDefault(t => t.Id == menuItemPortion.MenuItemId);
                                    if (menuItem != null)
                                    {
                                        lst.PortionName = menuItemPortion.MenuItem.Name + "(" + menuItemPortion.Name + ")";
                                    }
                                }
                            }
                        }

                    }
                }
                if (output.FreeItemPromotion != null)
                {
                    if (output.FreeItemPromotion.Executions != null && output.FreeItemPromotion.Executions.Any())
                    {
                        foreach (var lst in output.FreeItemPromotion.Executions)
                        {

                            if (lst.MenuItemPortionId != 0)
                            {
                                var menuItem = rsmenuItem.FirstOrDefault(t => t.Id == lst.MenuItemPortionId);
                                if (menuItem != null)
                                {
                                    lst.PortionName = menuItem.Name;
                                }
                            }
                        }
                    }
                }
                if (editDto.Id > 0)
                {
                    if (editDto.PromotionTypeId == 9)
                    {
                        output.DemandPromotion = !string.IsNullOrEmpty(editDto.PromotionContents)
                            ? JsonConvert.DeserializeObject<DemandPromotionEditDto>(editDto.PromotionContents) : new DemandPromotionEditDto();
                    }

                    if (editDto.PromotionTypeId == 10 || editDto.PromotionTypeId == 8)
                    {
                        output.TicketDiscountPromotion = !string.IsNullOrEmpty(editDto.PromotionContents)
                            ? JsonConvert.DeserializeObject<TicketDiscountPromotionDto>(editDto.PromotionContents) : new TicketDiscountPromotionDto();
                    }
                    if (editDto.PromotionTypeId == 11)
                    {
                        output.BuyXAndYAtZValuePromotion = !string.IsNullOrEmpty(editDto.PromotionContents)
                            ? JsonConvert.DeserializeObject<BuyXAndYAtZValuePromotionEditDto>(editDto.PromotionContents) : new BuyXAndYAtZValuePromotionEditDto();
                    }
                    if (editDto.PromotionTypeId == 12)
                    {
                        output.StepDiscountPromotion = !string.IsNullOrEmpty(editDto.PromotionContents)
                            ? JsonConvert.DeserializeObject<StepDiscountPromotionEditDto>(editDto.PromotionContents) : new StepDiscountPromotionEditDto();
                        if (output.StepDiscountPromotion.StepDiscountExecutions.Any())
                        {
                            foreach (var lst in output.StepDiscountPromotion.StepDiscountExecutions)
                            {
                                if (lst.PromotionValueType == (int)PromotionValueTypes.Percentage)
                                {
                                    lst.PromotionValueTypeName = "Percentage";
                                }
                                else
                                {
                                    lst.PromotionValueTypeName = "Value";
                                }

                            }
                        }
                    }
                }

                UpdateLocationAndNonLocationForEdit(editDto, output.LocationGroup);
                foreach (var single in output.Promotion.PromotionRestrictItems)
                {
                    var mItem = _menuManager.Get(single.MenuItemId);
                    if (mItem != null)
                        single.Name = mItem.Name;
                }
                return output;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Error when get Promotion information!");
            }
        }

        public async Task CreateOrUpdatePromotion(CreateOrUpdatePromotionInput input)
        {

            if (input.Promotion.ConnectCardTypeId.HasValue)
            {
                if (input.Promotion.ConnectCardTypeId.Value == 0)
                {
                    input.Promotion.ConnectCardTypeId = null;
                }
            }

            if (input.Promotion.Id.HasValue)
                await UpdatePromotion(input);
            else
                await CreatePromotion(input);
            await _syncAppService.UpdateSync(SyncConsts.PROMOTION);
            await UpdatePromotions(input);
        }

        public async Task<ApiPromotionOutput> ApiGetPromotions(ApiLocationInput input)
        {
            var orgId = await _locAppService.GetOrgnizationIdForLocationId(input.LocationId);
            if (orgId > 0) CurrentUnitOfWork.SetFilterParameter(AppConsts.ConnectFilter, "oid", orgId);
            
            var listPromotions = await _promotionRepo.GetAllListAsync(a => a.TenantId == input.TenantId && a.EndDate >= DateTime.Now);

            List<Promotion> allOutputPromotions = new List<Promotion>();
            List<int> allLocations = new List<int>();
            if (input.LocationId >= 0)
            {
                allLocations.Add(input.LocationId);
            }

            if (!string.IsNullOrEmpty(input.Tag))
            {
                var myLocs = await _locAppService.GetLocationsByLocationGroupCode(input.Tag);
                if (myLocs != null && myLocs.Items.Any())
                {
                    allLocations.AddRange(myLocs.Items.Select(allLocationsItem =>
                        Convert.ToInt32(allLocationsItem.Value)));
                }
            }

            try
            {
                if (listPromotions.Any() && allLocations.Any())
                {
                    foreach (var myLocation in allLocations)
                    {
                        foreach (var myPromo in listPromotions)
                        {
                            if (await _locAppService.IsLocationExists(new CheckLocationInput
                            {
                                LocationId = myLocation,
                                Locations = myPromo.Locations,
                                Group = myPromo.Group,
                                NonLocations = myPromo.NonLocations,
                                LocationTag = myPromo.LocationTag

                            }))
                            {
                                allOutputPromotions.Add(myPromo);
                            }
                        }
                    }
                }
                else
                {
                    allOutputPromotions = listPromotions;
                }
            }
            catch (Exception exception)
            {
                var mess = exception.Message;
            }

            if (allOutputPromotions.Any())
            {
                var returnVa = allOutputPromotions.MapTo<List<PromotionEditDto>>();
                foreach (var item in returnVa)
                {
                    var quotas = await _pqRepository.GetAllListAsync(x => x.PromotionId == item.Id && !string.IsNullOrEmpty(x.Plants));
                    item.HasQuota = quotas.Any();
                }
                return new ApiPromotionOutput
                {
                    Promotions = returnVa
                };
            }

            return null;
        }

        public async Task<ApiTimerPromotionOutput> ApiGetTimerPromotions(ApiPromotionDefinitionInput input)
        {
            var orgId = await _locAppService.GetOrgnizationIdForLocationId(input.LocationId);
            if (orgId > 0) CurrentUnitOfWork.SetFilterParameter(AppConsts.ConnectFilter, "oid", orgId);

            var output = new ApiTimerPromotionOutput();
            var timer = await _tRepository.FirstOrDefaultAsync(a => a.PromotionId == input.PromotionId);
            if (timer.PriceTagId > 0)
            {
                var pTag = await _ptRepository.FirstOrDefaultAsync(a => a.Id == timer.PriceTagId);

                if (pTag != null) output.PriceTag = pTag.Name;
            }

            return output;
        }

        public async Task<FreePromotionEditDto> ApiGetFreePromotion(ApiPromotionDefinitionInput input)
        {
            var orgId = await _locAppService.GetOrgnizationIdForLocationId(input.LocationId);
            if (orgId > 0) CurrentUnitOfWork.SetFilterParameter(AppConsts.ConnectFilter, "oid", orgId);

            var returnDto = new FreePromotionEditDto();
            var ti = await _frPromotion.FirstOrDefaultAsync(a => a.PromotionId == input.PromotionId);
            if (ti != null) returnDto = ti.MapTo<FreePromotionEditDto>();
            return returnDto;
        }

        public async Task<FreeValuePromotionEditDto> ApiGetFreeValuePromotion(ApiPromotionDefinitionInput input)
        {
            var orgId = await _locAppService.GetOrgnizationIdForLocationId(input.LocationId);
            if (orgId > 0) CurrentUnitOfWork.SetFilterParameter(AppConsts.ConnectFilter, "oid", orgId);

            var returnDto = new FreeValuePromotionEditDto();
            var ti = await _frvPromotion.FirstOrDefaultAsync(a => a.PromotionId == input.PromotionId);
            if (ti != null) returnDto = ti.MapTo<FreeValuePromotionEditDto>();
            return returnDto;
        }

        public async Task<FreeItemPromotionEditDto> ApiGetFreeItemPromotion(ApiPromotionDefinitionInput input)
        {
            var orgId = await _locAppService.GetOrgnizationIdForLocationId(input.LocationId);
            if (orgId > 0) CurrentUnitOfWork.SetFilterParameter(AppConsts.ConnectFilter, "oid", orgId);
            var returnDto = new FreeItemPromotionEditDto();
            var ti = await _fiPromotion.FirstOrDefaultAsync(a => a.PromotionId == input.PromotionId);
            if (ti != null) returnDto = ti.MapTo<FreeItemPromotionEditDto>();
            return returnDto;
        }

        public async Task<List<ApiFixedPromotionOutput>> ApiGetFixedPromotion(ApiPromotionDefinitionInput input)
        {
            var orgId = await _locAppService.GetOrgnizationIdForLocationId(input.LocationId);
            if (orgId > 0) CurrentUnitOfWork.SetFilterParameter(AppConsts.ConnectFilter, "oid", orgId);

            var output = new List<ApiFixedPromotionOutput>();

            var promos = await _fixedPromotionRepo.GetAllListAsync(a => a.PromotionId == input.PromotionId);
            if (promos.Any())
                foreach (var fp in promos)
                {
                    var pro = new ApiFixedPromotionOutput
                    {
                        CategoryId = fp.CategoryId,
                        MenuItemId = fp.MenuItemId,
                        Value = fp.PromotionValue,
                        ValueType = fp.PromotionValueType,
                        ProductGroupId = fp.ProductGroupId,
                        MenuItemPortionId = fp.MenuItemPortionId
                    };
                    if (fp.CategoryId > 0)
                    {
                        var cate = _cRepository.Get(fp.CategoryId);
                        if (cate != null) pro.CategoryName = cate.Name;
                    }

                    output.Add(pro);
                }

            return output;
        }

        public async Task<DemandPromotionEditDto> ApiGetDemandPromotion(ApiPromotionDefinitionInput input)
        {
            var orgId = await _locAppService.GetOrgnizationIdForLocationId(input.LocationId);
            if (orgId > 0) CurrentUnitOfWork.SetFilterParameter(AppConsts.ConnectFilter, "oid", orgId);

            var output = new DemandPromotionEditDto();
            var ti = await _dptRepository.FirstOrDefaultAsync(a => a.PromotionId == input.PromotionId);
            if (ti != null) output = ti.MapTo<DemandPromotionEditDto>();

            foreach (var all in output.DemandPromotionExecutions)
                if (all.CategoryId > 0)
                {
                    var cate = _cRepository.Get(all.CategoryId);
                    if (cate != null) all.CategoryName = cate.Name;
                }

            return output;
        }

        public async Task<TicketDiscountPromotionDto> ApiTicketDiscountPromotion(ApiPromotionDefinitionInput input)
        {
            var orgId = await _locAppService.GetOrgnizationIdForLocationId(input.LocationId);
            if (orgId > 0) CurrentUnitOfWork.SetFilterParameter(AppConsts.ConnectFilter, "oid", orgId);

            var output = new TicketDiscountPromotionDto();
            var ti = await _tdRepository.FirstOrDefaultAsync(a => a.PromotionId == input.PromotionId);
            if (ti != null) output = ti.MapTo<TicketDiscountPromotionDto>();
            return output;
        }
        #region Card
        public async Task<ApiCardTypeOutput> ApiValidateCard(ApiCardTypeInput input)
        {
            var orgId = await _locAppService.GetOrgnizationIdForLocationId(input.LocationId);
            if (orgId > 0) CurrentUnitOfWork.SetFilterParameter(AppConsts.ConnectFilter, "oid", orgId);

            return ValidateCard(input);
        }
        [AbpAuthorize]
        public async Task<ApiPromotionStatusUpdate> ApiUpdatePromotionStatus(ApiPromotionStatusUpdate input)
        {
            try
            {
                
                if (input.PromotionId > 0)
                {
                    using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete,
                         AppConsts.ConnectFilter))
                    {
                        var myPromo = await _promotionRepo.GetAsync(input.PromotionId);
                        if (myPromo != null)
                        {
                            myPromo.Active = input.Active;
                            await _promotionRepo.UpdateAsync(myPromo);
                            return new ApiPromotionStatusUpdate()
                            {
                                Active = myPromo.Active,
                                PromotionId = myPromo.Id,
                                TenantId = myPromo.TenantId
                            };
                        }

                        throw new UserFriendlyException("Promotion is not available : " + input.PromotionId);
                    }
                }
                throw new UserFriendlyException("Promotion Id is not available in the Input");
                    

            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
            
        }

        [AbpAuthorize]
        public async Task<ApiPromotionStatusUpdate> ApiReActivatePromotion(ApiPromotionStatusUpdate input)
        {
            try
            {
                if (input.PromotionId > 0)
                {
                    using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete,
                        AppConsts.ConnectFilter))
                    {
                        var item= await _promotionRepo.GetAsync(input.PromotionId);
                        if (item == null)
                        {
                            throw new UserFriendlyException("Promotion is not available "+input.PromotionId);
                        }
                        item.IsDeleted = false;
                        item.Active = input.Active;

                        await _promotionRepo.UpdateAsync(item);

                        return new ApiPromotionStatusUpdate()
                        {
                            Active = item.Active,
                            PromotionId = item.Id,
                            TenantId = item.TenantId
                        };
                    }
                }
                throw new UserFriendlyException("Promotion Id is not available in the Input");
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
            
        }

        public async Task<ApiCardTypeOutput> ApiUpdateCard(ApiCardTypeInput input)
        {
            var orgId = await _locAppService.GetOrgnizationIdForLocationId(input.LocationId);
            if (orgId > 0) CurrentUnitOfWork.SetFilterParameter(AppConsts.ConnectFilter, "oid", orgId);

            var myValidCard = ValidateCard(input);

            if (myValidCard.Valid)
            {
                var myCardTypeList =  _connectCardTypeRepo.GetAll().Where(a => a.Id == input.CardTypeId
                    && a.TenantId == input.TenantId).ToList();

                var myCardType = myCardTypeList.LastOrDefault();
                if (myCardType!=null && 
                    myCardType.UseOneTime && myCardType.ValidTill>=DateTime.Now)
                {
                    var myCardList = _connectCardRepo.GetAll().Where(a => a.TenantId.Equals(input.TenantId) && a.ConnectCardTypeId == myCardType.Id &&
                                                                              a.CardNo.Equals(input.CardNumber)).ToList();
                    var myCard = myCardList.LastOrDefault();

                    if (myCard != null)
                    {
                        myCard.Redeemed = true;
                        await _connectCardRepo.UpdateAsync(myCard);

                        ConnectCardRedemption cardRedemption  = new ConnectCardRedemption()
                        {
                            ConnectCardId = myCard.Id,
                            RedemptionDate = DateTime.Now,
                            LocationId = input.LocationId,
                            TicketNumber = input.TicketNumber,
                        };

                        await _connectCardRedemptionRepo.InsertAsync(cardRedemption);
                    }

                   
                }
            }
            return myValidCard;
        }

        public async Task<ApiCardTypeOutput> ApiReverseAppliedCard(ApiCardTypeInput input)
        {
            ApiCardTypeOutput cardOutput = new ApiCardTypeOutput();
            try
            {
                var orgId = await _locAppService.GetOrgnizationIdForLocationId(input.LocationId);
                if (orgId > 0) CurrentUnitOfWork.SetFilterParameter(AppConsts.ConnectFilter, "oid", orgId);

                var myCardTypeList = _connectCardRedemptionRepo.GetAll().Where(a =>
                    a.LocationId.Equals(input.LocationId) && a.TicketNumber.Equals(input.TicketNumber)).ToList();

                foreach (var cardInstance in myCardTypeList)
                {
                    var myCard = await _connectCardRepo.GetAsync(cardInstance.ConnectCardId);
                    if (myCard != null)
                    {
                        myCard.Redeemed = false;
                    }
                };

                foreach (var connectCardRedemption in myCardTypeList.ToArray())
                {
                    await _connectCardRedemptionRepo.DeleteAsync(connectCardRedemption);
                }

                cardOutput.Valid = true;
                return cardOutput;
            }
            catch (Exception e)
            {
               cardOutput.Valid = false;
               cardOutput.Reason = e.Message;
            }

            return cardOutput;

        }

        private ApiCardTypeOutput ValidateCard(ApiCardTypeInput input)
        {
            ApiCardTypeOutput cardOutput = new ApiCardTypeOutput()
            {
                Valid = false,
                Reason =  "[Empty]"
            };

            if (string.IsNullOrEmpty(input.CardNumber))
            {
                cardOutput.Reason = "Empty Card Number";
                return cardOutput;
            }

            if (input.TenantId == 0 )
            {
                cardOutput.Reason = "No Tenant";
                return cardOutput;
            }

            if (input.CardTypeId == 0)
            {
                cardOutput.Reason = "No Card Type";
                return cardOutput;
            }

            if (string.IsNullOrEmpty(input.TicketNumber))
            {
                cardOutput.Reason = "No Ticket Number";
                return cardOutput;
            }

            if (input.IsPromotion)
            {
                var myPromotionList = _promotionRepo.GetAll()
                    .Where(a => a.Id == input.PromotionId && a.TenantId == input.TenantId).ToList();

                var myPromotion = myPromotionList.LastOrDefault();
                if (myPromotion == null)
                {
                    cardOutput.Reason = "Empty Promotion";
                    return cardOutput;
                }

                if (!myPromotion.ConnectCardTypeId.HasValue)
                {
                    cardOutput.Reason = "No CardType for Promotion";
                    return cardOutput;
                }

                if (myPromotion.ConnectCardTypeId != input.CardTypeId)
                {
                    cardOutput.Reason = "Invalid Card Type";
                    return cardOutput;
                }
            }

            var myCardTypeList = _connectCardTypeRepo.GetAll().Where(a => a.Id == input.CardTypeId && a.TenantId == input.TenantId)
                    .ToList();
            var myCardType = myCardTypeList.LastOrDefault();

            if (myCardType == null)
            {
                cardOutput.Reason = "Invalid CardType";
                return cardOutput;
            }

            if (myCardType.ValidTill<DateTime.Now)
            {
                cardOutput.Reason = "Card is expired";
                return cardOutput;
            }

            var myCardList = _connectCardRepo.GetAll().Where(a => a.TenantId.Equals(input.TenantId) && a.ConnectCardTypeId == myCardType.Id &&
                                                                          a.CardNo.Equals(input.CardNumber)).ToList();
            var myCard = myCardList.LastOrDefault();
            if (myCard == null)
            {
                cardOutput.Reason = "No Card";
                return cardOutput;
            }
            if (!myCard.Active)
            {
                cardOutput.Reason = "Card is not Active";
                return cardOutput;
            }
            if (myCard.Redeemed)
            {
                cardOutput.Reason = "Already Redeemed Card";
                return cardOutput;
            }

            cardOutput.Reason = "";
            cardOutput.Valid = true;

            return cardOutput;
        }
        #endregion
        private async Task<List<FixedPromotionEditDto>> GetFixedPromotion(NullableIdInput input)
        {
            var retuDto = new List<FixedPromotionEditDto>();
            if (input.Id.HasValue)
            {
                var ti = await _fixedPromotionRepo.GetAllListAsync(a => a.PromotionId == input.Id.Value);
                retuDto = ti.MapTo<List<FixedPromotionEditDto>>();
            }

            return retuDto;
        }

        private async Task<FreeValuePromotionEditDto> GetFreeValuePromotion(NullableIdInput input)
        {
            var returnDto = new FreeValuePromotionEditDto();
            var ti = await _frvPromotion.FirstOrDefaultAsync(a => a.PromotionId == input.Id.Value);
            if (ti != null)
            {
                returnDto = ti.MapTo<FreeValuePromotionEditDto>();
                if (returnDto.FreeValuePromotionExecutions.Any())
                {
                    returnDto.From =
                        new Collection<FreeValuePromotionExecutionEditDto>(
                            returnDto.FreeValuePromotionExecutions.Where(a => a.From).ToList());
                    returnDto.To =
                        new Collection<FreeValuePromotionExecutionEditDto>(
                            returnDto.FreeValuePromotionExecutions.Where(a => !a.From).ToList());
                }
            }

            return returnDto;
        }

        private async Task<FreePromotionEditDto> GetFreePromotion(NullableIdInput input)
        {
            var returnDto = new FreePromotionEditDto();
            var ti = await _frPromotion.FirstOrDefaultAsync(a => a.PromotionId == input.Id.Value);
            if (ti != null)
            {
                returnDto = ti.MapTo<FreePromotionEditDto>();
                if (returnDto.FreePromotionExecutions.Any())
                {
                    returnDto.From =
                        new Collection<FreePromotionExecutionEditDto>(
                            returnDto.FreePromotionExecutions.Where(a => a.From).ToList());
                    returnDto.To =
                        new Collection<FreePromotionExecutionEditDto>(
                            returnDto.FreePromotionExecutions.Where(a => !a.From).ToList());
                }
            }

            return returnDto;
        }

        private async Task<TimerPromotionEditDto> GetTimerPromotion(NullableIdInput input)
        {
            var retuDto = new TimerPromotionEditDto();
            if (input.Id.HasValue)
            {
                var ti = await _tRepository.FirstOrDefaultAsync(a => a.PromotionId == input.Id.Value);
                if (ti != null) retuDto = ti.MapTo<TimerPromotionEditDto>();
            }

            return retuDto;
        }

        private async Task<DemandPromotionEditDto> GetDemandPromotion(NullableIdInput input)
        {
            var returnDto = new DemandPromotionEditDto();
            if (input.Id.HasValue)
            {
                var ti = await _dptRepository.FirstOrDefaultAsync(a => a.PromotionId == input.Id.Value);
                if (ti != null) returnDto = ti.MapTo<DemandPromotionEditDto>();
            }

            return returnDto;
        }

        private async Task<TicketDiscountPromotionDto> GetTicketDiscountPromotion(NullableIdInput input)
        {
            var returnDto = new TicketDiscountPromotionDto();
            if (input.Id.HasValue)
            {
                var ti = await _tdRepository.FirstOrDefaultAsync(a => a.PromotionId == input.Id.Value);
                if (ti != null) returnDto = ti.MapTo<TicketDiscountPromotionDto>();
            }

            return returnDto;
        }

        private async Task<FreeItemPromotionEditDto> GetFreeItemPromotion(NullableIdInput input)
        {
            var retuDto = new FreeItemPromotionEditDto();
            if (input.Id.HasValue)
            {
                var ti = await _fiPromotion.FirstOrDefaultAsync(a => a.PromotionId == input.Id.Value);
                if (ti != null) retuDto = ti.MapTo<FreeItemPromotionEditDto>();
            }

            return retuDto;
        }

        protected virtual async Task CreatePromotion(CreateOrUpdatePromotionInput input)
        {
            
            var dto = input.Promotion.MapTo<Promotion>();
            dto.Filter = input.Filter;
            UpdateLocationAndNonLocation(dto, input.LocationGroup);

            foreach (var dtoPromotionSchedule in dto.PromotionSchedules)
            {
                if (dtoPromotionSchedule.StartDate.Equals(DateTime.MinValue) ||
                    dtoPromotionSchedule.StartDate.Equals(DateTime.MaxValue))
                {
                    dtoPromotionSchedule.StartDate = dto.StartDate;
                    dtoPromotionSchedule.EndDate = dto.EndDate;

                }
            }
            
            CheckErrors(await _promotionManager.CreateOrUpdateSync(dto));
        }

        protected virtual async Task UpdatePromotion(CreateOrUpdatePromotionInput input)
        {
            var item = await _promotionRepo.GetAsync(input.Promotion.Id.Value);
            var dto = input.Promotion;

            item.Name = dto.Name;
            item.StartDate = dto.StartDate;
            item.EndDate = dto.EndDate;
            item.PromotionTypeId = dto.PromotionTypeId;
            item.Active = dto.Active;
            item.Filter = input.Filter;
            item.PromotionContents = input.Promotion.PromotionContents;
            item.ConnectCardTypeId = input.Promotion.ConnectCardTypeId;
            item.PromotionCategoryId = input.Promotion.PromotionCategoryId;
            item.Limi1PromotionPerItem = input.Promotion.Limi1PromotionPerItem;
            item.CombineAllPro = input.Promotion.CombineAllPro;
            item.CombineProValues = input.Promotion.CombineProValues;
            item.DisplayPromotion = input.Promotion.DisplayPromotion;
            item.PromotionPosition = input.Promotion.PromotionPosition;
            item.IgnorePromotionLimitation = input.Promotion.IgnorePromotionLimitation;
            item.ApplyOffline = input.Promotion.ApplyOffline;
            item.Description = input.Promotion.Description;

            UpdateLocationAndNonLocation(item, input.LocationGroup);

            if (dto?.PromotionSchedules != null && dto.PromotionSchedules.Any())
            {
                var oids = new List<int>();
                foreach (var otdto in dto.PromotionSchedules)
                {
                    if (otdto.Id == null)
                    {
                        item.PromotionSchedules.Add(Mapper.Map<PromotionScheduleEditDto, PromotionSchedule>(otdto));
                    }
                    else
                    {
                        oids.Add(otdto.Id.Value);
                        var fromUpdation = item.PromotionSchedules.SingleOrDefault(a => a.Id.Equals(otdto.Id));
                        UpdateSchedule(fromUpdation, otdto);
                    }
                }

                var tobeRemoved =
                    item.PromotionSchedules.Where(a => a.Id != null && !a.Id.Equals(0) && !oids.Contains(a.Id))
                        .ToList();
                foreach (var tagItem in tobeRemoved) await _promotionSchedule.DeleteAsync(tagItem.Id);
            }
            else
            {
                foreach (var tagItem in item.PromotionSchedules) await _promotionSchedule.DeleteAsync(tagItem.Id);
            }

            if (dto.PromotionRestrictItems.Any())
            {
                var oids = new List<int>();
                foreach (var otdto in dto.PromotionRestrictItems)
                    if (otdto.Id == null || otdto.Id == 0)
                    {
                        item.PromotionRestrictItems.Add(otdto.MapTo<PromotionRestrictItem>());
                    }
                    else
                    {
                        oids.Add(otdto.Id.Value);
                        var fromUpdation = item.PromotionRestrictItems.SingleOrDefault(a => a.Id.Equals(otdto.Id));
                        fromUpdation.MenuItemId = otdto.MenuItemId;
                        fromUpdation.PromotionId = dto.Id.Value;
                    }

                var tobeRemoved = item.PromotionRestrictItems.Where(a => !a.Id.Equals(0) && !oids.Contains(a.Id))
                        .ToList();
                tobeRemoved.ForEach(e => _promoRestrictRepo.DeleteAsync(e));
            }
            else
            {
                item.PromotionRestrictItems.ToList().ForEach(e => _promoRestrictRepo.DeleteAsync(e));
            }

            foreach (var itemPro in item.PromotionSchedules)
            {
                if (itemPro.StartDate.Equals(DateTime.MinValue) ||
                    itemPro.StartDate.Equals(DateTime.MaxValue))
                {
                    itemPro.StartDate = dto.StartDate;
                    itemPro.EndDate = dto.EndDate;

                }
            }

            item.LastModificationTime = DateTime.Now;

            CheckErrors(await _promotionManager.CreateOrUpdateSync(item));
        }

        private async Task UpdatePromotions(CreateOrUpdatePromotionInput input)
        {
            var promoId = 0;

            if (input.Promotion.Id.HasValue)
            {
                promoId = input.Promotion.Id.Value;
            }
            else
            {
                var prom = await _promotionRepo.GetAllListAsync(a => a.Name.Equals(input.Promotion.Name));
                if (prom.Any()) promoId = prom.First().Id;
            }

            if (promoId > 0)
            {
                if (input.Promotion.PromotionTypeId == 1 && input.TimerPromotion != null)
                    await UpdateTimerPromotion(input.TimerPromotion, promoId);

                if (input.Promotion.PromotionTypeId == 2 && input.FixPromotionsPer != null &&
                    input.FixPromotionsPer.Any())
                    await UpdateFixedPromotion(input.FixPromotionsPer, promoId, 2);

                if (input.Promotion.PromotionTypeId == 3 && input.FixPromotions != null && input.FixPromotions.Any())
                    await UpdateFixedPromotion(input.FixPromotions, promoId, 1);
                if (input.Promotion.PromotionTypeId == 4 && input.FreePromotion != null)
                    await UpdateFreePromotion(input.FreePromotion, promoId);
                if (input.Promotion.PromotionTypeId == 5 && input.FreeValuePromotion != null)
                    await UpdateFreeValuePromotion(input.FreeValuePromotion, promoId);
                if (input.Promotion.PromotionTypeId == 6 && input.DemandPromotion != null)
                    await UpdateDemandPromotion(input.DemandPromotion, promoId);
                if (input.Promotion.PromotionTypeId == 7 && input.FreeItemPromotion != null)
                    await UpdateFreeItemPromotion(input.FreeItemPromotion, promoId);
                if (input.Promotion.PromotionTypeId == 8 && input.TicketDiscountPromotion != null)
                    await UpdateTicketDiscountPromotion(input.TicketDiscountPromotion, promoId);
                if (input.PromotionQuotas != null)
                    await UpdatePromotionQuotas(input.PromotionQuotas, promoId);
            }
        }

        private async Task UpdateFreePromotion(FreePromotionEditDto freePromotion, int promoId)
        {
            if (freePromotion.Id.HasValue)
            {
                var dto = await _frPromotion.GetAsync(freePromotion.Id.Value);
                dto.PromotionId = promoId;
                dto.AllFrom = freePromotion.AllFrom;
                dto.FromCount = freePromotion.FromCount;
                dto.ToCount = freePromotion.ToCount;

                if (dto.FreePromotionExecutions == null)
                    dto.FreePromotionExecutions = new List<FreePromotionExecution>();
                var restToRemove = new List<int>();
                foreach (var promos in freePromotion.From)
                {
                    var add = false;
                    if (promos.Id.HasValue)
                    {
                        var available = dto.FreePromotionExecutions.SingleOrDefault(a => a.Id == promos.Id);
                        if (available == null)
                        {
                            add = true;
                        }
                        else
                        {
                            available.CategoryId = promos.CategoryId;
                            available.ProductGroupId = promos.ProductGroupId;
                            available.MenuItemId = promos.MenuItemId;
                            available.MenuItemPortionId = promos.MenuItemPortionId;
                            restToRemove.Add(promos.Id.Value);
                        }
                    }
                    else
                    {
                        add = true;
                    }

                    if (add)
                        dto.FreePromotionExecutions.Add(new FreePromotionExecution
                        {
                            From = true,
                            CategoryId=promos.CategoryId,
                            ProductGroupId=promos.ProductGroupId,
                            MenuItemId = promos.MenuItemId,
                            MenuItemPortionId = promos.MenuItemPortionId
                        });
                }

                foreach (var promos in freePromotion.To)
                {
                    var add = false;
                    if (promos.Id.HasValue)
                    {
                        var available = dto.FreePromotionExecutions.SingleOrDefault(a => a.Id == promos.Id);
                        if (available == null)
                        {
                            add = true;
                        }
                        else
                        {
                            available.CategoryId = promos.CategoryId;
                            available.ProductGroupId = promos.ProductGroupId;
                            available.MenuItemId = promos.MenuItemId;
                            available.MenuItemPortionId = promos.MenuItemPortionId;
                            restToRemove.Add(promos.Id.Value);
                        }
                    }
                    else
                    {
                        add = true;
                    }

                    if (add)
                        dto.FreePromotionExecutions.Add(new FreePromotionExecution
                        {
                            From = false,
                            CategoryId = promos.CategoryId,
                            ProductGroupId = promos.ProductGroupId,
                            MenuItemId = promos.MenuItemId,
                            MenuItemPortionId = promos.MenuItemPortionId
                        });
                }

                var tobeRemoved =
                    dto.FreePromotionExecutions.Where(a => !a.Id.Equals(0) && !restToRemove.Contains(a.Id)).ToList();
                foreach (var freeExe in tobeRemoved) await _frePromotion.DeleteAsync(freeExe.Id);

                await _frPromotion.UpdateAsync(dto);
            }
            else
            {
                var dto = new FreePromotion
                {
                    PromotionId = promoId,
                    AllFrom = freePromotion.AllFrom,
                    FromCount = freePromotion.FromCount,
                    ToCount = freePromotion.ToCount,
                    FreePromotionExecutions = new List<FreePromotionExecution>()
                };

                foreach (var promos in freePromotion.From)
                    dto.FreePromotionExecutions.Add(new FreePromotionExecution
                    {
                        From = true,
                        CategoryId = promos.CategoryId,
                        ProductGroupId = promos.ProductGroupId,
                        MenuItemId = promos.MenuItemId,
                        MenuItemPortionId = promos.MenuItemPortionId
                    });
                foreach (var promos in freePromotion.To)
                    dto.FreePromotionExecutions.Add(new FreePromotionExecution
                    {
                        From = false,
                        CategoryId = promos.CategoryId,
                        ProductGroupId = promos.ProductGroupId,
                        MenuItemId = promos.MenuItemId,
                        MenuItemPortionId = promos.MenuItemPortionId
                    });
                await _frPromotion.InsertAsync(dto);
            }
        }

        private async Task UpdateFreeValuePromotion(FreeValuePromotionEditDto freePromotion, int promoId)
        {
            if (freePromotion.Id.HasValue)
            {
                var dto = await _frvPromotion.GetAsync(freePromotion.Id.Value);
                dto.PromotionId = promoId;
                dto.AllFrom = freePromotion.AllFrom;
                dto.FromCount = freePromotion.FromCount;

                if (dto.FreeValuePromotionExecutions == null)
                    dto.FreeValuePromotionExecutions = new List<FreeValuePromotionExecution>();
                var restToRemove = new List<int>();
                foreach (var promos in freePromotion.From)
                {
                    var add = false;
                    if (promos.Id.HasValue)
                    {
                        var available = dto.FreeValuePromotionExecutions.SingleOrDefault(a => a.Id == promos.Id);
                        if (available == null)
                        {
                            add = true;
                        }
                        else
                        {
                            available.CategoryId = promos.CategoryId;
                            available.ProductGroupId = promos.ProductGroupId;
                            available.MenuItemId = promos.MenuItemId;
                            available.MenuItemPortionId = promos.MenuItemPortionId;
                            restToRemove.Add(promos.Id.Value);
                        }
                    }
                    else
                    {
                        add = true;
                    }

                    if (add)
                        dto.FreeValuePromotionExecutions.Add(new FreeValuePromotionExecution
                        {
                            From = true,
                            CategoryId=promos.CategoryId,
                            ProductGroupId=promos.ProductGroupId,
                            MenuItemId = promos.MenuItemId,
                            MenuItemPortionId = promos.MenuItemPortionId
                            
                        });
                }

                foreach (var promos in freePromotion.To)
                {
                    var add = false;
                    if (promos.Id.HasValue)
                    {
                        var available = dto.FreeValuePromotionExecutions.SingleOrDefault(a => a.Id == promos.Id);
                        if (available == null)
                        {
                            add = true;
                        }
                        else
                        {
                            available.CategoryId = promos.CategoryId;
                            available.ProductGroupId = promos.ProductGroupId;
                            available.MenuItemId = promos.MenuItemId;
                            available.MenuItemPortionId = promos.MenuItemPortionId;
                            available.ValueType = promos.ValueType;
                            available.Value = promos.Value;
                            restToRemove.Add(promos.Id.Value);
                        }
                    }
                    else
                    {
                        add = true;
                    }

                    if (add)
                        dto.FreeValuePromotionExecutions.Add(new FreeValuePromotionExecution
                        {
                            From = false,
                            CategoryId = promos.CategoryId,
                            ProductGroupId = promos.ProductGroupId,
                            MenuItemId = promos.MenuItemId,
                            MenuItemPortionId = promos.MenuItemPortionId,
                            ValueType = promos.ValueType,
                            Value = promos.Value
                        });
                }

                var tobeRemoved =
                    dto.FreeValuePromotionExecutions.Where(a => !a.Id.Equals(0) && !restToRemove.Contains(a.Id))
                        .ToList();
                foreach (var freeExe in tobeRemoved) await _frvPromotionExecution.DeleteAsync(freeExe.Id);

                await _frvPromotion.UpdateAsync(dto);
            }
            else
            {
                var dto = new FreeValuePromotion
                {
                    PromotionId = promoId,
                    AllFrom = freePromotion.AllFrom,
                    FromCount = freePromotion.FromCount,
                    FreeValuePromotionExecutions = new List<FreeValuePromotionExecution>()
                };

                foreach (var promos in freePromotion.From)
                    dto.FreeValuePromotionExecutions.Add(new FreeValuePromotionExecution
                    {
                        From = true,
                        MenuItemId = promos.MenuItemId,
                        MenuItemPortionId = promos.MenuItemPortionId,
                        ValueType = promos.ValueType,
                        Value = promos.Value
                    });
                foreach (var promos in freePromotion.To)
                    dto.FreeValuePromotionExecutions.Add(new FreeValuePromotionExecution
                    {
                        From = false,
                        MenuItemId = promos.MenuItemId,
                        MenuItemPortionId = promos.MenuItemPortionId,
                        ValueType = promos.ValueType,
                        Value = promos.Value
                    });
                await _frvPromotion.InsertAsync(dto);
            }
        }

        private async Task UpdateDemandPromotion(DemandPromotionEditDto demandPromotion, int promoId)
        {
            if (demandPromotion.Id.HasValue)
            {
                var item = await _dptRepository.GetAsync(demandPromotion.Id.Value);
                item.PromotionId = promoId;
                item.ButtonCaption = demandPromotion.ButtonCaption;
                item.ButtonColor = demandPromotion.ButtonColor;
                item.PromotionOverride = demandPromotion.PromotionOverride;
                item.AskReference = demandPromotion.AskReference;
                item.PromotionValueType = demandPromotion.PromotionValueType;
                item.PromotionValue = demandPromotion.PromotionValue;
                item.AuthenticationRequired = demandPromotion.AuthenticationRequired;
                item.PromotionApplyOnce = demandPromotion.PromotionApplyOnce;
                item.PromotionApplyType = demandPromotion.PromotionApplyType.HasValue ? demandPromotion.PromotionApplyType.Value : 0;
                item.Group = demandPromotion.Group;
                if (item.DemandPromotionExecutions == null)
                    item.DemandPromotionExecutions = new Collection<DemandPromotionExecution>();
                var restToRemove = new List<int>();
                foreach (var promos in demandPromotion.DemandPromotionExecutions)
                {
                    var add = false;
                    if (promos.Id.HasValue)
                    {
                        var available = item.DemandPromotionExecutions.SingleOrDefault(a => a.Id == promos.Id);
                        if (available == null)
                        {
                            add = true;
                        }
                        else
                        {
                            available.MenuItemId = promos.MenuItemId;
                            available.CategoryId = promos.CategoryId;
                            available.ProductGroupId = promos.ProductGroupId;
                            available.MenuItemPortionId = promos.MenuItemPortionId;
                            restToRemove.Add(promos.Id.Value);
                        }
                    }
                    else
                    {
                        add = true;
                    }

                    if (add)
                        item.DemandPromotionExecutions.Add(new DemandPromotionExecution
                        {
                            MenuItemId = promos.MenuItemId,
                            CategoryId = promos.CategoryId,
                            ProductGroupId=promos.ProductGroupId,
                            MenuItemPortionId=promos.MenuItemPortionId
                        });
                }

                var tobeRemoved =
                    item.DemandPromotionExecutions.Where(a => !a.Id.Equals(0) && !restToRemove.Contains(a.Id)).ToList();

                foreach (var freeExe in tobeRemoved) await _demExePromoRepo.DeleteAsync(freeExe.Id);
                await _dptRepository.UpdateAsync(item);
            }
            else
            {
                var dto = demandPromotion.MapTo<DemandDiscountPromotion>();
                dto.PromotionId = promoId;
                await _dptRepository.InsertAsync(dto);
            }
        }

        private async Task UpdateFreeItemPromotion(FreeItemPromotionEditDto fit, int promoId)
        {
            if (fit.Id.HasValue)
            {
                var dto = await _fiPromotion.GetAsync(fit.Id.Value);
                dto.PromotionId = promoId;
                dto.ItemCount = fit.ItemCount;
                dto.Confirmation = fit.Confirmation;
                dto.TotalAmount = fit.TotalAmount;
                dto.MultipleTimes = fit.MultipleTimes;

                if (dto.Executions == null) dto.Executions = new List<FreeItemPromotionExecution>();
                var restToRemove = new List<int>();
                foreach (var promos in fit.Executions)
                {
                    var add = false;
                    if (promos.Id.HasValue)
                    {
                        var available = dto.Executions.SingleOrDefault(a => a.Id == promos.Id);
                        if (available == null)
                        {
                            add = true;
                        }
                        else
                        {
                            available.MenuItemId = promos.MenuItemId;
                            available.MenuItemPortionId = promos.MenuItemPortionId;
                            available.PortionName = promos.PortionName;
                            available.Price = promos.Price;
                            restToRemove.Add(promos.Id.Value);
                        }
                    }
                    else
                    {
                        add = true;
                    }

                    if (add)
                        dto.Executions.Add(new FreeItemPromotionExecution
                        {
                            MenuItemId = promos.MenuItemId,
                            MenuItemPortionId = promos.MenuItemPortionId,
                            PortionName = promos.PortionName,
                            Price = promos.Price
                        });
                }

                var tobeRemoved =
                    dto.Executions.Where(a => !a.Id.Equals(0) && !restToRemove.Contains(a.Id)).ToList();

                foreach (var freeExe in tobeRemoved) await _fiePromotion.DeleteAsync(freeExe.Id);

                await _fiPromotion.UpdateAsync(dto);
            }
            else
            {
                var dto = new FreeItemPromotion
                {
                    PromotionId = promoId,
                    ItemCount = fit.ItemCount,
                    Confirmation = fit.Confirmation,
                    TotalAmount = fit.TotalAmount,
                    MultipleTimes = fit.MultipleTimes,
                    Executions = new List<FreeItemPromotionExecution>()
                };

                foreach (var promos in fit.Executions)
                    dto.Executions.Add(new FreeItemPromotionExecution
                    {
                        MenuItemId = promos.MenuItemId,
                        MenuItemPortionId = promos.MenuItemPortionId,
                        PortionName = promos.PortionName,
                        Price = promos.Price
                    });
                await _fiPromotion.InsertAsync(dto);
            }
        }

        private async Task UpdateTicketDiscountPromotion(TicketDiscountPromotionDto demandPromotion, int promoId)
        {
            TicketDiscountPromotion ticketPromotion = null;
            if (demandPromotion.Id.HasValue)
            {
                ticketPromotion = await _tdRepository.GetAsync(demandPromotion.Id.Value);
                
            }else if (promoId > 0)
            {
                var allPromotions=  _tdRepository.GetAll().Where(a=>a.PromotionId == promoId).ToList();

                if (allPromotions.Any())
                {
                    ticketPromotion = allPromotions.LastOrDefault();
                }
            }

            if (ticketPromotion != null)
            {
                ticketPromotion.PromotionId = promoId;
                ticketPromotion.ButtonCaption = demandPromotion.ButtonCaption;
                ticketPromotion.AskReference = demandPromotion.AskReference;
                ticketPromotion.AuthenticationRequired = demandPromotion.AuthenticationRequired;
                ticketPromotion.PromotionValueType = demandPromotion.PromotionValueType;
                ticketPromotion.PromotionValue = demandPromotion.PromotionValue;
                ticketPromotion.PromotionApplyOnce = demandPromotion.PromotionApplyOnce;
                ticketPromotion.PromotionApplyType = demandPromotion.PromotionApplyType.HasValue ? demandPromotion.PromotionApplyType.Value : 0;
                await _tdRepository.UpdateAsync(ticketPromotion);
            }
            else
            {
                var dto = demandPromotion.MapTo<TicketDiscountPromotion>();
                dto.PromotionId = promoId;
                await _tdRepository.InsertAsync(dto);
            }
        }

        private async Task UpdateTimerPromotion(TimerPromotionEditDto timer, int prom)
        {
            TimerPromotion item = null;
            if (timer.Id.HasValue)
            {
                item = await _tRepository.GetAsync(timer.Id.Value);

            }else if (prom > 0)
            {
                var allPromotions=  _tRepository.GetAll().Where(a=>a.PromotionId == prom).ToList();

                if (allPromotions.Any())
                {
                    item = allPromotions.LastOrDefault();
                }
            }
            if(item!=null){

                item.PromotionId = prom;
                item.PriceTagId = timer.PriceTagId;
                await _tRepository.UpdateAsync(item);
            }
            else
            {
                var dto = timer.MapTo<TimerPromotion>();
                dto.PromotionId = prom;
                await _tRepository.InsertAsync(dto);
            }
        }

        private async Task UpdateFixedPromotion(List<FixedPromotionEditDto> fixes, int prom, int vType)
        {
            var allIds = new List<int>();

            foreach (var fi in fixes)
                if (fi.Id.HasValue)
                {
                    var item = await _fixedPromotionRepo.GetAsync(fi.Id.Value);
                    item.PromotionId = prom;
                    item.CategoryId = fi.CategoryId;
                    item.ProductGroupId = fi.ProductGroupId;
                    item.MenuItemId = fi.MenuItemId;
                    item.PromotionValue = fi.PromotionValue;
                    item.MenuItemPortionId = fi.MenuItemPortionId;
                    item.PromotionValueType = fi.PromotionValueType;
                    allIds.Add(fi.Id.Value);

                    await _fixedPromotionRepo.UpdateAsync(item);
                }
                else
                {
                    var item = fi.MapTo<FixedPromotion>();
                    item.PromotionId = prom;
                    item.CategoryId = fi.CategoryId;
                    item.ProductGroupId = fi.ProductGroupId;
                    item.MenuItemId = fi.MenuItemId;
                    item.PromotionValue = fi.PromotionValue;
                    item.PromotionValueType = fi.PromotionValueType;
                    item.MenuItemPortionId = fi.MenuItemPortionId;
                    await _fixedPromotionRepo.InsertAsync(item);
                }

            var allP =
                _fixedPromotionRepo.GetAllList(a => a.PromotionId == prom && !allIds.Contains(a.Id))
                    .Select(a => a.Id)
                    .ToArray();
            foreach (var ap in allP) await _fixedPromotionRepo.DeleteAsync(ap);
        }

        private void UpdateSchedule(PromotionSchedule to, PromotionScheduleEditDto from)
        {
            to.StartHour = from.StartHour;
            to.StartMinute = from.StartMinute;
            to.EndHour = from.EndHour;
            to.EndMinute = from.EndMinute;

            to.Days = from.Days;
            to.MonthDays = from.MonthDays;
        }

        private async Task<int> CheckQuotaUsed(GetQuotaDetailInput input, PromotionQuota pQuota, QuotaLocationDto quota)
        {
            if (quota.QuotaUsed == 0) return 0;
            try
            {
                var tickets = await _ticketRepository.GetAllListAsync(x => x.LocationId == quota.Id && x.LastPaymentTime >= pQuota.LastResetDate);
                var quotaCount = 0;

                foreach (var ticket in tickets)
                {
                    var pdvs = new List<PromotionDetailValue>();

                    if (!string.IsNullOrEmpty(ticket.TicketPromotionDetails))
                    {
                        var promotionDetails = JsonConvert.DeserializeObject<List<PromotionDetailValue>>(ticket.TicketPromotionDetails);
                        foreach (var op in promotionDetails.Where(x => x.PromotionSyncId == pQuota.PromotionId))
                        {
                            if (pdvs.All(x => x.Id != op.PromotionSyncId))
                                pdvs.Add(op);
                        }
                    }

                    foreach (var order in ticket.Orders)
                    {
                        var orderPromos = order.GetOrderPromotionList();
                        if (orderPromos.Count > 0)
                        {
                            foreach (var op in orderPromos.Where(x => x.PromotionSyncId == pQuota.PromotionId))
                            {
                                if (pdvs.All(x => x.Id != op.Id))
                                    pdvs.Add(op);
                            }
                        }
                        else
                        {
                            var promoTags = order.OrderTagValues;
                            foreach (var op in promoTags.Where(x => x.PromotionSyncId == pQuota.PromotionId))
                            {
                                if (pdvs.All(x => x.Id != op.MenuItemId))
                                    pdvs.Add(new PromotionDetailValue
                                    {
                                        PromotionSyncId = op.PromotionSyncId
                                    });
                            }
                        }
                    }

                    if (!pdvs.Any()) continue;

                    quotaCount += pdvs.Count;
                }

                if (quotaCount > quota.QuotaUsed)
                {
                    var locationQuotas = JsonConvert.DeserializeObject<CustomLocationGroupDto>(pQuota.LocationQuota);
                    var currentLocation = locationQuotas.Locations.FirstOrDefault(x => x.Id == quota.Id);
                    if (currentLocation != null)
                    {
                        currentLocation.QuotaUsed = quotaCount;
                    }
                    pQuota.LocationQuota = JsonConvert.SerializeObject(locationQuotas);
                    pQuota.QuotaUsed = locationQuotas.Locations.Sum(x => x.QuotaUsed);
                    _pqRepository.Update(pQuota);

                    return quotaCount;
                }

                return quota.QuotaUsed;
            }
            catch (Exception)
            {
                return quota.QuotaUsed;
            }
        }

        public async Task ActivateItem(IdInput input)
        {
            try
            {
                if(input.Id==0)
                    throw new UserFriendlyException("No Id in the Input");

                using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                {
                    var item = await _promotionRepo.GetAsync(input.Id);
                    if (item == null)
                    {
                        throw new UserFriendlyException("Promotion is not available "+input.Id);
                    }
                    item.IsDeleted = false;
                    await _promotionRepo.UpdateAsync(item);
                }
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(e.Message);
            }
          
        }


        public async Task<ListResultOutput<ComboboxItemDto>> GetPromotionStepDiscountTypes()
        {
            var returnList = new List<ComboboxItemDto>();

            var i = 1;
            foreach (var name in Enum.GetNames(typeof(PromotionStepDiscountTypes)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }
        public async Task<ListResultOutput<ComboboxItemDto>> GetPromotionApplyTypes()
        {
            var returnList = new List<ComboboxItemDto>();

            var i = 1;
            foreach (var name in Enum.GetNames(typeof(PromotionApplyTypes)))
            {
                returnList.Add(new ComboboxItemDto
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i++;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }

        #region Promotion Quota
        #region Get - Update - Reset
        public async Task<ListResultOutput<ComboboxItemDto>> GetResetType()
        {
            var returnList = new List<ComboboxItemDto>();

            int i = 0;
            foreach (string name in Enum.GetNames(typeof(ResetType)))
            {
                returnList.Add(new ComboboxItemDto()
                {
                    DisplayText = name,
                    Value = i.ToString()
                });
                i += 1;
            }
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }
        public async Task<ListResultOutput<ComboboxItemDto>> GetPromotionPositions()
        {
            var returnList = new List<ComboboxItemDto>
            { 
                new ComboboxItemDto{Value = "1", DisplayText = L("LoginOfDinePlan")},
                new ComboboxItemDto{Value = "2", DisplayText = L("ClickOnPOS")},
                new ComboboxItemDto{Value = "3", DisplayText = L("EveryTransaction")},
                new ComboboxItemDto{Value = "4", DisplayText = L("PaymentClick")},
                new ComboboxItemDto{Value = "5", DisplayText = L("LogoutOfDinePlan")},
                new ComboboxItemDto{Value = "6", DisplayText = L("StartingWorkPeriod")},
                new ComboboxItemDto{Value = "7", DisplayText = L("EndWorkPeriod")},
                new ComboboxItemDto{Value = "8", DisplayText = L("StartShift")},
                new ComboboxItemDto{Value = "9", DisplayText = L("EndShift")},
            };
            
            return new ListResultOutput<ComboboxItemDto>(returnList);
        }
        private async Task<Collection<PromotionQuotaDto>> GetPromotionQuotaForEdit(NullableIdInput input)
        {
            var returnDto = new Collection<PromotionQuotaDto>();
            if (input.Id.HasValue)
            {
                //await ResetQuota(input.Id.Value);
                var ti = await _pqRepository.GetAllListAsync(a => a.PromotionId == input.Id.Value);
                if (ti != null)
                {
                    returnDto = ti.MapTo<Collection<PromotionQuotaDto>>();
                    foreach (var dto in returnDto)
                    {
                        dto.LocationGroup = new LocationGroupDto
                                            {
                                                Group = dto.Group,
                                                Locations = JsonHelper.Deserialize<List<SimpleLocationDto>>(dto.Locations)
                                            };
                    }
                }
            }

            return returnDto;
        }

        private async Task UpdatePromotionQuotas(ICollection<PromotionQuotaDto> quotas, int promoId)
        {
            var allIds = new List<int>();
            foreach (var pq in quotas)
            {
                if (pq.Id.HasValue)
                {
                    var quota = await _pqRepository.GetAsync(pq.Id.Value);
                    quota.PromotionId = promoId;
                    quota.QuotaAmount = pq.QuotaAmount;
                    quota.ResetType = pq.ResetType;
                    quota.ResetWeekValue = pq.ResetWeekValue;
                    quota.ResetMonthValue = pq.ResetMonthValue;
                    quota.Plants = pq.Plants;
                    quota.Each = pq.Each;

                    UpdateLocationAndNonLocation(quota, pq.LocationGroup);
                    UpdateQuotaPlants(quota);
                    allIds.Add(pq.Id.Value);

                    await _pqRepository.UpdateAsync(quota);
                }
                else
                {
                    var quota = pq.MapTo<PromotionQuota>();
                    quota.PromotionId = promoId;
                    if(quota.ResetMonthValue.HasValue)
                        quota.ResetMonthValue = quota.ResetMonthValue.Value.ToLocalTime();
                        
                    UpdateLocationAndNonLocation(quota, pq.LocationGroup);
                    UpdateQuotaPlants(quota);
                    quota.LastResetDate = DateTime.Now;
                    await _pqRepository.InsertAsync(quota);
                }
            }

            var allP = _pqRepository.GetAllList(a => a.PromotionId == promoId && !allIds.Contains(a.Id))
                    .Select(a => a.Id)
                    .ToArray();
            foreach (var ap in allP) await _pqRepository.DeleteAsync(ap);
        }

        private void UpdateQuotaPlants(PromotionQuota quota)
        {
            try
            {
                var allLocations = _locationRepo.GetAllList();
                var locationIds = new List<int>();

                if(string.IsNullOrEmpty(quota.Locations))
                {
                    locationIds = allLocations.Select(x => x.Id).ToList();
                }
                else
                {
                    var lcs = JsonConvert.DeserializeObject<List<QuotaLocationDto>>(quota.Locations);
                    locationIds = lcs.Select(x => x.Id).ToList();
                }

                var group = new CustomLocationGroupDto
                {
                    Locations = new List<QuotaLocationDto>()
                };

                if (string.IsNullOrEmpty(quota.LocationQuota))
                {
                    foreach (var id in locationIds)
                    {
                        var lc = allLocations.FirstOrDefault(x => x.Id == id);
                        group.Locations.Add(new QuotaLocationDto
                        {
                            Id = id,
                            Code = lc.Code,
                            Name = lc.Name,
                            QuotaUsed = 0
                        });
                    }
                }
                else
                {
                    var existGroup = JsonConvert.DeserializeObject<CustomLocationGroupDto>(quota.LocationQuota);
                    
                    foreach (var id in locationIds)
                    {
                        var lc = allLocations.FirstOrDefault(x => x.Id == id);
                        var existG = existGroup?.Locations.FirstOrDefault(x => x.Id == id);
                        if(existG == null)
                        {
                            group.Locations.Add(new QuotaLocationDto
                            {
                                Id = id,
                                Code = lc.Code,
                                Name = lc.Name,
                                QuotaUsed = 0
                            });
                        }
                        else
                        {
                            group.Locations.Add(new QuotaLocationDto
                            {
                                Id = id,
                                Code = lc.Code,
                                Name = lc.Name,
                                QuotaUsed = existG.QuotaUsed
                            });
                        }
                        
                    }
                }

                quota.LocationQuota = JsonConvert.SerializeObject(group);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Cannot Update Promotion Quota");
            }
            
        }
        private DateTime? GetQuotaResetDate(PromotionQuota quota)
        {
            var currentDayEnd = DateTime.Today.AddDays(1).AddMinutes(-1);

            if(quota.ResetType == (int)ResetType.Daily)
            {
                return currentDayEnd;
            }
            if(quota.ResetType == (int)ResetType.Weekly)
            {
                while ((int)currentDayEnd.DayOfWeek != quota.ResetWeekValue)
                    currentDayEnd = currentDayEnd.AddDays(1);
                return currentDayEnd;
            }
            if(quota.ResetType == (int)ResetType.Monthly)
            {
                while (currentDayEnd.Day != quota.ResetMonthValue.Value.Day)
                    currentDayEnd = currentDayEnd.AddDays(1);
                return currentDayEnd;
            }
            if(quota.ResetType == (int)ResetType.Yearly)
            {
                var local = quota.ResetMonthValue.Value;

                while (currentDayEnd.Day != local.Day && currentDayEnd.Month != local.Month)
                    currentDayEnd = currentDayEnd.AddDays(1);
                return currentDayEnd;
            }
            return null;
        }
        
        public async Task ResetQuota()
        {
            var quotas = _pqRepository.GetAll();
            if (quotas == null) return;

            var today = DateTime.Today;
            foreach (var quota in quotas)
            {
                var rsDate = GetQuotaResetDate(quota);
                if (!rsDate.HasValue) continue;
                if (rsDate.Value.Day == today.Day && rsDate.Value.Month == today.Month)
                {
                    quota.QuotaUsed = 0;
                    quota.LastResetDate = rsDate;
                    if (!string.IsNullOrEmpty(quota.LocationQuota))
                    {
                        var group = JsonConvert.DeserializeObject<CustomLocationGroupDto>(quota.LocationQuota);
                        if (group != null && group.Locations != null)
                        {
                            foreach (var qt in group.Locations)
                                qt.QuotaUsed = 0;

                            quota.LocationQuota = JsonConvert.SerializeObject(group);
                        }
                    }

                    await _pqRepository.UpdateAsync(quota);
                }
            }
        }

        public async Task<QuotaDetailInfo> GetQuotaDetail(GetQuotaDetailInput input)
        {
            try
            {
                var output = new QuotaDetailInfo();

                var pq = await _pqRepository.GetAsync(input.PromotionQuotaId);
                if (pq == null) return output;
                
                var locations = await _locationRepo.GetAllListAsync();
                if (!string.IsNullOrEmpty(pq.Locations))
                {
                    var lcs = JsonConvert.DeserializeObject<IEnumerable<SimpleLocationDto>>(pq.Locations);
                    if (lcs != null)
                        locations = locations.Where(x => lcs.Any(l => l.Id == x.Id)).ToList();
                }

                var quotaList = locations.OrderBy(x => x.Name).Select(x => new QuotaDetail
                {
                    PromotionId = pq.PromotionId,
                    QuotaId = input.PromotionQuotaId,
                    PlantId = x.Id,
                    PlantNo = x.Code,
                    Plant = x.Name,
                    Quota = pq.QuotaAmount
                });

                if (!string.IsNullOrEmpty(pq.LocationQuota))
                {
                    var locationQuotas = JsonConvert.DeserializeObject<CustomLocationGroupDto>(pq.LocationQuota);
                    if (locationQuotas != null || locationQuotas.Locations != null)
                    {
                        var list = quotaList.ToList();
                        foreach (var ql in list)
                        {
                            var lq = locationQuotas.Locations.FirstOrDefault(x => x.Id == ql.PlantId);
                            if (lq == null) continue;

                            ql.TotalUsed = await CheckQuotaUsed(input, pq, lq);
                        }

                        quotaList = list.AsQueryable();
                    }
                }

                var total = pq.Each ? pq.QuotaAmount * locations.Count : pq.QuotaAmount;
                output.DashBoardDto = new DashboardOrderDto
                {
                    TotalAmount = total,
                    TotalItemSold = pq.QuotaUsed,
                    TotalOrderCount = total - pq.QuotaUsed
                };

                if (!input.Sorting.IsNullOrEmpty()) quotaList = quotaList.OrderBy(input.Sorting);
                if (input.SkipCount > 0) quotaList = quotaList.Skip(input.SkipCount);

                if (input.MaxResultCount > 0) quotaList = quotaList.Take(input.MaxResultCount);
                output.QuotaDetails = new PagedResultOutput<QuotaDetail>(quotaList.Count(), quotaList.ToList());

                return output;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Cannot get Promotion Quota");
            }
        }
        
        public async Task<QuotaTicketInfo> GetQuotaTickets(GetQuotaDetailInput input)
        {
            try
            {
                var output = new QuotaTicketInfo();
                if (input.LocationId == 0) return output;
                var quota = await _pqRepository.FirstOrDefaultAsync(input.PromotionQuotaId);
                if (quota == null) return output;

                var tickets = await _ticketRepository.GetAllListAsync(x => x.LocationId == input.LocationId && x.LastPaymentTime >= quota.LastResetDate);
                var ticketCount = 0;
                var orderCount = 0;
                var qts = new List<QuotaTicket>();

                foreach (var ticket in tickets)
                {
                    var pdvs = new List<PromotionDetailValue>();

                    if (!string.IsNullOrEmpty(ticket.TicketPromotionDetails))
                    {
                        var promotionDetails = JsonConvert.DeserializeObject<List<PromotionDetailValue>>(ticket.TicketPromotionDetails);
                        foreach (var op in promotionDetails.Where(x => x.PromotionSyncId == input.PromotionId))
                        {
                            if (!pdvs.Any(x => x.Id == op.PromotionSyncId))
                                pdvs.Add(op);
                        }
                    }

                    foreach (var order in ticket.Orders)
                    {
                        var orderPromos = order.GetOrderPromotionList();
                        if (orderPromos.Count > 0)
                        {
                            foreach (var op in orderPromos.Where(x => x.PromotionSyncId == input.PromotionId))
                            {
                                if (pdvs.All(x => x.Id != op.Id))
                                    pdvs.Add(op);
                            }
                        }
                        else
                        {
                            var promoTags = order.OrderTagValues;
                            foreach (var op in promoTags.Where(x => x.PromotionSyncId == input.PromotionId))
                            {
                                if (pdvs.All(x => x.Id != op.MenuItemId))
                                    pdvs.Add(new PromotionDetailValue
                                    {
                                        PromotionSyncId = op.PromotionSyncId
                                    });
                            }
                        }
                        
                    }
                    
                    if (!pdvs.Any()) continue;

                    var bc = HideCardInfo(ticket.GetTicketTagValue("Blue Card No"));
                    qts.Add(new QuotaTicket
                    {
                        TicketId = ticket.Id,
                        TicketNo = ticket.TicketNumber,
                        InvoiceNo = ticket.InvoiceNo,
                        TaxInvoiceNo = ticket.FullTaxInvoiceDetails,
                        PosNo = ticket.TerminalName,
                        SaleMode = ticket.DepartmentGroup,
                        SaleChannel = ticket.DepartmentName,
                        User = ticket.LastModifiedUserName,
                        Date = ticket.LastPaymentTime,
                        QuotaUsed = pdvs.Count(),
                        TotalAmount = ticket.TotalAmount,
                        BlueCardId = bc
                    });
                    ticketCount++;
                    orderCount += ticket.Orders.Count();
                }

                if (!qts.Any()) return output;
                var total = qts.Count();
                if (!input.Sorting.IsNullOrEmpty()) qts = qts.OrderBy(input.Sorting).ToList();
                if (input.SkipCount > 0) qts = qts.Skip(input.SkipCount).ToList();
                if (input.MaxResultCount > 0) qts = qts.Take(input.MaxResultCount).ToList();

                output.QuotaTickets = new PagedResultOutput<QuotaTicket>(
                    total,
                    qts
                );

                output.DashBoardDto = new DashboardOrderDto
                {
                    TotalAmount = ticketCount,
                    TotalItemSold = qts.Sum(x => x.QuotaUsed),
                    TotalOrderCount = orderCount
                };
                return output;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Cannot get data");
            }
        }

        string HideCardInfo(string cardNo)
        {
            if (string.IsNullOrEmpty(cardNo)) return null;
            var chars = cardNo.ToCharArray();
            var sb = new StringBuilder();

            for (int i = 0; i < chars.Length; i++)
            {
                if (i < chars.Length - 4)
                    sb.Append("x");
                else
                    sb.Append(chars[i]);
            }

            return sb.ToString();
        }

        public async Task<PagedResultOutput<CombinePromotionDto>> GetPromotionsCombine(GetCombinePromotionInput input)
        {
            var allItems = _promotionRepo.GetAll()
                .Where(x => x.Id != input.PromotionQuotaId)
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.Name.Contains(input.Filter));
            var list = allItems
                .OrderBy(input.Sorting)
                .PageBy(input)
                .Select(x => new CombinePromotionDto
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList();


            return new PagedResultOutput<CombinePromotionDto>(
                    await allItems.CountAsync(),
                    list
                );
        }

        #endregion

        #region Quota API
        public async Task<PromotionQuotaDto> ApiGetPromotionQuota(ApiPromotionDefinitionInput input)
        {
            var orgId = await _locAppService.GetOrgnizationIdForLocationId(input.LocationId);
            if (orgId > 0) CurrentUnitOfWork.SetFilterParameter(AppConsts.ConnectFilter, "oid", orgId);
            
            var quotas = await _pqRepository.GetAllListAsync(a => a.PromotionId == input.PromotionId);

            var dto = new PromotionQuotaDto {QuotaAmount = 0, QuotaUsed = 0};

            var locations = await _locationRepo.GetAllListAsync();
            foreach (var qt in quotas)
            {
                if (string.IsNullOrEmpty(qt.LocationQuota)) continue;
                
                dto.Id = qt.Id;
                dto.QuotaAmount = qt.QuotaAmount;
                var group = JsonHelper.Deserialize<CustomLocationGroupDto>(qt.LocationQuota);
                var qtlc = group?.Locations?.FirstOrDefault(x => x.Id == input.LocationId);
                if (qtlc != null)
                {
                    dto.QuotaUsed = qtlc.QuotaUsed;
                    break;
                }
            }
            return dto;
        }

        public async Task UpdateQuotaUsage(Transaction.Ticket ticket)
        {
            try
            {
                 var promotionDetails = new List<int>();
            if (!string.IsNullOrEmpty(ticket.TicketPromotionDetails))
                promotionDetails = JsonConvert.DeserializeObject<List<PromotionDetailValue>>(ticket.TicketPromotionDetails).Select(x => x.PromotionSyncId).ToList();
            foreach (var order in ticket.Orders)
            {
                if (!string.IsNullOrEmpty(order.OrderPromotionDetails))
                    promotionDetails.AddRange(order.OrderPromotionDetailsList.Select(x => x.PromotionSyncId));
                if (!string.IsNullOrEmpty(order.OrderTags))
                    promotionDetails.AddRange(order.OrderTagValues.Where(x => x.PromotionSyncId > 0).Select(x => x.PromotionSyncId));
            }

            var orgId = await _locAppService.GetOrgnizationIdForLocationId(ticket.LocationId);
            if (orgId > 0) CurrentUnitOfWork.SetFilterParameter(AppConsts.ConnectFilter, "oid", orgId);
            
            using (_unitOfWorkManager.Current.DisableFilter("ConnectFilter", AbpDataFilters.SoftDelete))
            {
                foreach (var id in promotionDetails.Distinct())
                {
                    var promotion = await _promotionRepo.GetAsync(id);
                    if (promotion != null && promotion.ApplyOffline) continue;

                    var quotas = await _pqRepository.GetAll().Where(x => x.PromotionId == id).ToListAsync();
                    if (quotas.Count == 0) continue;

                    foreach (var quota in quotas)
                    {
                        if (string.IsNullOrEmpty(quota.LocationQuota)) continue;
                        var group = JsonConvert.DeserializeObject<CustomLocationGroupDto>(quota.LocationQuota);
                        var lc = group?.Locations?.FirstOrDefault(x => x.Id == ticket.LocationId);
                        if (lc != null)
                        {
                            if (ticket.IsInState("*", "Refund") && lc.QuotaUsed > 0)
                            {
                                lc.QuotaUsed -= 1;
                                quota.QuotaUsed -= 1;
                            }
                            else
                            {
                                lc.QuotaUsed += 1;
                                quota.QuotaUsed += 1;
                            }

                            quota.LocationQuota = JsonConvert.SerializeObject(group);
                            _pqRepository.Update(quota);
                        }
                    }
                }
            }
            }
            catch (Exception)
            {
                // ignored
            }
        }
        #endregion

        #region Export
        public async Task<FileDto> GetQuotasExcel(QuotaExportInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                var dtos = await GetQuotaDetail(new GetQuotaDetailInput
                {
                    PromotionQuotaId = input.PromotionQuotaId,
                    LocationId = input.LocationId
                });
                input.Quotas = dtos.QuotaDetails.Items.ToList();

                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.QUOTAS,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                    {
                        input.BackGroundId = backGroundId;
                        input.ReportName = ReportNames.QUOTAS;
                        await _bgm.EnqueueAsync<QuotaExportJob, QuotaExportInput>(input);
                    }
                }
                else
                {
                    return await _promoExporter.ExportQuota(input);
                }
            }

            return null;
        }
        
        public async Task<FileDto> GetQuotaTicketsExcel(QuotaExportInput input)
        {
            if (AbpSession.UserId != null && AbpSession.TenantId != null)
            {
                var dtos = await GetQuotaTickets(new GetQuotaDetailInput
                {
                    LocationId = input.LocationId
                });
                input.QuotaTickets = dtos.QuotaTickets.Items.ToList();

                if (input.RunInBackground)
                {
                    var backGroundId = await _rbas.CreateOrUpdate(new CreateBackgroundReportInput
                    {
                        ReportName = ReportNames.QUOTATICKETS,
                        Completed = false,
                        UserId = AbpSession.UserId.Value,
                        TenantId = AbpSession.TenantId.Value,
                        ReportDescription = input.ReportDescription
                    });
                    if (backGroundId > 0)
                    {
                        input.BackGroundId = backGroundId;
                        input.ReportName = ReportNames.QUOTATICKETS;
                        await _bgm.EnqueueAsync<QuotaExportJob, QuotaExportInput>(input);
                    }
                }
                else
                {
                    return await _promoExporter.ExportQuotaTicket(input);
                }
            }

            return null;
        }
        #endregion
        #endregion
    }
}