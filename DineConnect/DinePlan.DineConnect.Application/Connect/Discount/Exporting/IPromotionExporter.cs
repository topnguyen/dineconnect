﻿using DinePlan.DineConnect.Connect.Discount.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Discount
{
    public interface IPromotionExporter
    {
        Task<FileDto> ExportQuota(QuotaExportInput input);
        Task<FileDto> ExportQuotaTicket(QuotaExportInput input);
    }
}