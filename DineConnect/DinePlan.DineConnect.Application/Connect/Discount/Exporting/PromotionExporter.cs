﻿using System.Threading.Tasks;
using DinePlan.DineConnect.Connect.Discount.Dtos;
using DinePlan.DineConnect.DataExporting.Excel.EpPlus;
using DinePlan.DineConnect.Dto;
using DinePlan.DineConnect.Net.MimeTypes;
using OfficeOpenXml;

namespace DinePlan.DineConnect.Connect.Discount.Exporting
{
    public class PromotionExporter : FileExporterBase, IPromotionExporter
    {
        public async Task<FileDto> ExportQuota(QuotaExportInput input)
        {
            var file = new FileDto("PlantQuotas.xlsx", MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("PlantQuotas"));
                sheet.OutLineApplyStyle = true;

                AddHeader(
                        sheet,
                        L("Location"),
                        L("Quota"),
                        L("Used")
                        );

                AddObjects(
                    sheet, 2, input.Quotas,
                    _ => _.Plant,
                    _ => _.Quota,
                    _ => _.TotalUsed
                    );

                for (var i = 1; i <= 3; i++)
                {
                    sheet.Column(i).AutoFit();
                }

                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }
        
        public async Task<FileDto> ExportQuotaTicket(QuotaExportInput input)
        {
            var file = new FileDto("QuotaDetail.xlsx", MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet);
            using (var excelPackage = new ExcelPackage())
            {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("QuotaDetail"));
                sheet.OutLineApplyStyle = true;

                AddHeader(
                        sheet,
                        L("Id"),
                        L("TicketNo"),
                        L("Location"),
                        L("Terminal"),
                        L("SaleMode"),
                        L("User"),
                        L("Date"),
                        L("Time"),
                        L("BlueCardId"),
                        L("TotalAmount")
                        );

                AddObjects(
                    sheet, 2, input.QuotaTickets,
                    _ => _.TicketId,
                    _ => _.TicketNo,
                    _ => _.Plant,
                    _ => _.PosNo,
                    _ => _.SaleMode,
                    _ => _.User,
                    _ => _.Date.ToString(_simpleDateFormat),
                    _ => _.Date.ToString(_timeFormat),
                    _ => _.BlueCardId,
                    _ => _.TotalAmount
                    );

                for (var i = 1; i <= 10; i++)
                {
                    sheet.Column(i).AutoFit();
                }

                Save(excelPackage, file);
            }

            return ProcessFile(input, file);
        }

    }
}
