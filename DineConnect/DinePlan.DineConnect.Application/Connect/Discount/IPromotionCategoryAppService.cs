﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DinePlan.DineConnect.Connect.Discount.Dtos;
using DinePlan.DineConnect.Dto;
using System.Threading.Tasks;

namespace DinePlan.DineConnect.Connect.Discount
{
    public interface IPromotionCategoryAppService : IApplicationService
    {
        Task<PagedResultOutput<PromotionCategoryListDto>> GetAll(GetPromotionCategoryInput inputDto);
        Task<FileDto> GetAllToExcel(GetPromotionCategoryInput input);
        Task<GetPromotionCategoryForEditOutput> GetPromotionCategoryForEdit(NullableIdInput nullableIdInput);
        Task CreateOrUpdatePromotionCategory(CreateOrUpdatePromotionCategoryInput input);
        Task<bool> DeletePromotionCategory(IdInput input);
        Task<ListResultOutput<PromotionCategoryListDto>> GetPromotionCategorys();
    }
}
